﻿using Library.AppCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Goods
{
    public partial class DUIHoliday : Form
    {
        //ThanhPT
        Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection selection;
        //StringBuilder strGoods = new StringBuilder();

        private DataTable dtbHolidays = null;
        private DataTable dtbShop = null;
        private DataTable dtbGoodsGroup = null;
        private DataTable dtbGoods = null;
        private DataTable dtbProduct = null;

        //Table
        private DataTable dtbTableGoods = null;

        private DataTable dtbResourceStore = null;
        private DataTable dtbResourceShopGoodsExperience = null;


        private PLC.Goods.FLCHolidays plcObject;
        private PLC.Goods.PLCGoodsTsvq plcGoodsTsvq;
        private PLC.Goods.WSHolidays.ResultMessage objResultMessage = null;

        private ActionMode currentAction = ActionMode.NONE;
        private ERP.Inventory.DUI.Goods.DUIGoodsTsvq.SQL_COMMON sql_common;
        public DUIHoliday()
        {
            sql_common = new DUIGoodsTsvq.SQL_COMMON();
            InitializeComponent();
            //SearchDataStore();       
            //enableControl(true);
            //search();
            

            //ThanhPT
            plcObject = new PLC.Goods.FLCHolidays();
            plcGoodsTsvq = new PLC.Goods.PLCGoodsTsvq();
            string user_shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
            object[] objKeywords = new object[]{
                    "@shop_id", user_shop_id,
            };
            dtbShop = plcObject.LoadShopById(objKeywords);
            gridControl1.DataSource = dtbShop;
            if (selection == null)
            {
                selection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection(true, true, gridView1);
                selection.CheckMarkColumn.VisibleIndex = 0;
                selection.CheckMarkColumn.Width = 40;
                selection.ClearSelection();
            }
        }

        private void SearchDataStore()
        {
            dtbResourceStore = SearchCommon(sql_common.SQL_SHOP, null);
            tblShop.DataSource = dtbResourceStore;
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
        }

        private void onSearch()
        {
            if (currentAction == ActionMode.NONE)
            {
                currentAction = ActionMode.SEARCH_MODE;
                clearDetailValue();
                enableControl(false);
                search();
            }
            else
            {
                search();
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ Delete du lieu cho cac hop text box
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        private void clearDetailValue()
        {



        }

        private void enableControl(bool enable)
        {
            btnAdd.Visible = enable;
            btnModify.Visible = enable;
            btnDelete.Visible = enable;
            btnExit.Visible = enable;

            btnOK.Visible =!enable;
            btnCancel.Visible = !enable;

            tblShop.Enabled = enable;
            tblList.Enabled = enable;
            dtpFromDate.Enabled = !enable;
            dtpToDate.Enabled = !enable;
           // formData.setFieldEnabled(!enable);
            if (currentAction == ActionMode.ADD_MODE || currentAction == ActionMode.MODIFY_MODE)
            {

                if (currentAction == ActionMode.ADD_MODE)
                {

                    dtpFromDate.Value = DateTime.Now;
                }
                else
                {
                }
            }
            if (currentAction == ActionMode.SEARCH_MODE)
            {
            }
            else
            {
                //btnSearch.Visible = false;
            }
        }

        public bool search()
        {
            try
            {
                if (grdStore.GetFocusedDataRow() == null) { 
                    return true;
                }

                string shop_id = "", goods_id = "", product_type = "", goods_group_id = "";
                DataRow dtRow = grdStore.GetFocusedDataRow();
                shop_id = dtRow["stock_id"].ToString();
                if (currentAction != ActionMode.SEARCH_MODE && currentAction != ActionMode.NONE)
                {
                    
                }
                else
                {
                }
                
                object[] objKeywords = new object[]{
                    "@shop_id", shop_id,
                    "@goods_id", goods_id,
                    "@product_type",product_type,
                    "@goods_group_id",goods_group_id,
                };
                //dtbResourceShopGoodsExperience = plcObject.LoadDataShopGoodsExperience(objKeywords);
                tblList.DataSource = dtbResourceShopGoodsExperience;
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }

                
               
                return true;
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                   e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
                // TODO: handle exception
                return false;
            }
        }

        private void fillDetailValue()
        {
            try
            {
                if (currentAction == ActionMode.SEARCH_MODE || currentAction == ActionMode.SEARCH_MODE) return;
                if (grdList.GetFocusedDataRow() == null) return;
                DataRow data = grdList.GetFocusedDataRow();
                string from_date = data["from_date"].ToString();
                string to_date = data["to_date"].ToString();

                if (from_date != null && !from_date.Equals(""))
                {
                    dtpFromDate.Checked = true;

                    dtpToDate.Value = DateTime.ParseExact(from_date , "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtpFromDate.Checked = false;
                }
                    dtpFromDate_ValueChanged(null, null);

                    if (to_date != null && !to_date.Equals(""))
                {
                    dtpToDate.Checked = true;
                    dtpToDate.Value = DateTime.ParseExact(to_date , "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtpToDate.Checked = false;
                }

                dtpToDate_ValueChanged(null, null);

                string goodType = data["product_type"].ToString();
                string goodGroup = data["goods_group_id"].ToString();
                string goodsId = data["goods_code"].ToString();

            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                       ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }

        private bool isSelectAll()
        {
            //int iRowIndex = tblShop.getSelectedRow();
            //Vector<String> data = tblShop.getRow(iRowIndex);
            //if (data.get(2).equals(""))
            //{
            //    return true;
            //}
            //else
            //{
               return false;
            //}
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            currentAction = ActionMode.NONE;
            //search();
            //fillDetailValue();
            //enableControl(true);
            actionAdd(false);
            actionAddButton(true);
            onChangeAction(ACTION.ACTION_NONE);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Bạn có chắc chắn muốn thoát chức năng?", "Thông báo"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                == System.Windows.Forms.DialogResult.No)
            {
            }
            else
            {
                this.Close();
            }
        }

        public void actionAdd(Boolean active)
        {
            btnOK.Visible = active;
            btnCancel.Visible = active;
            txtNameEvent.Enabled = active;
            dtpFromDate.Enabled = active;
            dtpToDate.Enabled = active;
            this.nudHeSo.Enabled = active;
            cbCN.Enabled = active;
            cbT2.Enabled = active;
            cbT3.Enabled = active;
            cbT4.Enabled = active;
            cbT5.Enabled = active;
            cbT6.Enabled = active;
            cbT7.Enabled = active;
            cboGoods.Enabled = active;
            cboGoodsGroup.Enabled = active;
            cboProductType.Enabled = active;
            btnAddGoods.Enabled = active;
            btnAddAllGoods.Enabled = active;
            btnRemoveGoods.Enabled = active;
            btnRemoveGoodsAll.Enabled = active;
            btnImport.Enabled = active;
            tblShop.Enabled = active;
            gridControl1.Enabled = active;
        }

        public void actionAddButton(Boolean active){
            this.btnAdd.Visible = active;
            this.btnModify.Visible = active;
            this.btnDelete.Visible = active;
            this.btnExit.Visible = active;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            currentAction = ActionMode.ADD_MODE;
            actionAdd(true);
            actionAddButton(false);
            refreshData();
            //if (isSelectAll()) return;
            //currentAction = ActionMode.ADD_MODE;
            //clearDetailValue();
            //enableControl(false);
            //onChangeAction(ACTION.ACTION_ADD);
            
        }

        private void refreshData()
        {
            this.txtNameEvent.Text = "";
            this.dtpFromDate.Refresh();
            this.dtpToDate.Refresh();
            this.nudHeSo.Value = 0;
            checkBoxActive(false);
            this.cboProductType.ResetDefaultValue();
            this.cboGoodsGroup.ResetDefaultValue();
            this.cboGoods.ResetDefaultValue();

            dtbTableGoods.Rows.Clear();
            dtbTableGoods.AcceptChanges();
            tblShop.DataSource = dtbTableGoods;
            

            if (selection != null)
                selection.ClearSelection();
            this.Refresh();

        }

        private string getDays()
        {
            StringBuilder strDay = new StringBuilder();


            if(this.cbT2.Checked){
                strDay.Append("T2,");
            } 
            if (this.cbT3.Checked)
            {
                strDay.Append("T3,");
            }
            if (this.cbT4.Checked)
            {
                strDay.Append("T4,");
            }
            if (this.cbT5.Checked)
            {
                strDay.Append("T5,");
            }
            if (this.cbT6.Checked)
            {
                strDay.Append("T6,");
            }
            if (this.cbT7.Checked)
            {
                strDay.Append("T7,");
            }
            if (this.cbCN.Checked)
            {
                strDay.Append("CN,");
            }
            return strDay.ToString();
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            try
            {
                currentAction = ActionMode.MODIFY_MODE;
                if (grdList.GetFocusedDataRow() == null || grdList.GetFocusedDataRow()["EVENT_LIST_ID"].ToString() == "")
                {
                    MessageBox.Show("Bạn phải chọn bản ghi để sửa", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                DataTable dtGoodsAddEvent = plcObject.GetGoodsByEvent(grdList.GetFocusedDataRow()["EVENT_LIST_ID"].ToString());
                dtbTableGoods = dtGoodsAddEvent.Copy();
                tblShop.DataSource = dtbTableGoods;
                this.txtNameEvent.Text = grdList.GetFocusedDataRow()["NAME"].ToString();
                IFormatProvider theCultureInfo = new System.Globalization.CultureInfo("vi-VN", true);
                this.dtpFromDate.Value = Convert.ToDateTime(grdList.GetFocusedDataRow()["FROM_DATE"].ToString(), theCultureInfo);
                this.dtpToDate.Value = Convert.ToDateTime(grdList.GetFocusedDataRow()["TO_DATE"].ToString(), theCultureInfo);
                this.nudHeSo.Value = Convert.ToDecimal(grdList.GetFocusedDataRow()["RATE"].ToString());
                checkBoxActive(false);
                this.cboProductType.ResetDefaultValue();
                this.cboGoodsGroup.ResetDefaultValue();
                this.cboGoods.ResetDefaultValue();


                string days = grdList.GetFocusedDataRow()["DAYS"].ToString();
                if (!string.IsNullOrEmpty(days))
                {
                    string[] arrDay = days.Split(',');
                    foreach (string day in arrDay)
                    {
                        setValueCheckBox(day);
                    }
                }
                string shopCodes = grdList.GetFocusedDataRow()["SHOP_ID_LIST"].ToString();
                if (!string.IsNullOrEmpty(shopCodes))
                {
                    string[] arrShopCodes = shopCodes.Split(',');
                    StringBuilder lstShop = new StringBuilder();
                    foreach (string shopCode in arrShopCodes)
                    {
                        if (!string.IsNullOrEmpty(shopCode))
                        {
                            Boolean isExist = false;
                            for (int i = 0; i < dtbShop.Rows.Count; i++)
                            {

                                DataRow row = dtbShop.Rows[i];
                                string shopId = row["SHOP_ID"].ToString();
                                if (shopId.Equals(shopCode))
                                {
                                    selection.SelectRow(i, true);
                                    isExist = true;
                                    break;
                                }
                            }
                            if (isExist == false)
                            {
                                lstShop.Append(shopCode + ","); 
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(lstShop.ToString()))
                    {
                        object[] objKeywords = new object[]
                        {
                          "@command", "SELECT shop_id, shop_code, shop_name name, stock_id, 'FALSE' checked FROM shop_stock"
                        };

                        DataTable shopAll = plcGoodsTsvq.SearchData(objKeywords);
                        if (shopAll != null && shopAll.Rows.Count > 0)
                        {
                            string[] arrShop = lstShop.ToString().Split(',');
                            foreach (string shopId in arrShop)
                            {
                                if (!string.IsNullOrEmpty(shopId))
                                {
                                    for (int i = 0; i < shopAll.Rows.Count; i++)
                                    {
                                        DataRow row = shopAll.Rows[i];
                                        if (shopId.Equals(row["shop_id"].ToString()))
                                        {
                                            DataRow data = dtbShop.NewRow();

                                            data["SHOP_ID"] = row["shop_id"].ToString();
                                            data["SHOP_CODE"] = row["shop_code"].ToString();
                                            data["NAME"] = row["name"].ToString();
                                            dtbShop.Rows.InsertAt(data, 0);
                                            selection.SelectRow(0, false);
                                            dtbHolidays.AcceptChanges();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                }
                actionAdd(true);
                actionAddButton(false);

                
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                 ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }


        public void checkBoxActive(Boolean active)
        {
            this.cbCN.Checked = active;
            this.cbT2.Checked = active;
            this.cbT3.Checked = active;
            this.cbT4.Checked = active;
            this.cbT5.Checked = active;
            this.cbT6.Checked = active;
            this.cbT7.Checked = active;
        }

        public void setValueCheckBox(string day)
        {
            

            switch(day) {
                case "CN":
                    this.cbCN.Checked = true;
                    break;
                case "T2":
                    this.cbT2.Checked = true;
                    break;
                case "T3":
                    this.cbT3.Checked = true;
                    break;
                case "T4":
                    this.cbT4.Checked = true;
                    break;
                case "T5":
                    this.cbT5.Checked = true;
                    break;
                case "T6":
                    this.cbT6.Checked = true;
                    break;
                case "T7":
                    this.cbT7.Checked = true;
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ Bat su kien dau vao
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        public void onChangeAction(ACTION iNewAction)
        {
            if (iNewAction == ACTION.ACTION_NONE)
            {
               
            }
            else if (iNewAction == ACTION.ACTION_ADD || iNewAction == ACTION.ACTION_MODIFY)
            {
            }
            if (iNewAction == ACTION.ACTION_SAVE)
            {
                enableControl(true);
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ ADD
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        public bool add()
        {
            try
            {
                string from_date = "";
                string to_date = "";
                string goods_id = "";
                string goods_group_id = "";
                string product_type = "";
                string user_id = "";
                string shop_id = "";
                string user_shop_id = "";
                user_shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
                user_id = Library.AppCore.SystemConfig.objSessionUser.UserName;
                shop_id = grdStore.GetFocusedDataRow()["stock_id"].ToString();
                if (dtpFromDate.Checked)
                {
                    from_date = dtpFromDate.Value.ToString("dd/MM/yyyy");
                }
                if (dtpToDate.Checked)
                {
                    to_date = dtpToDate.Value.ToString("dd/MM/yyyy");
                }
                object[] objKeywords = new object[]{
                    "@from_date", from_date,
                    "@to_date", to_date,
                    "@goods_id", goods_id,
                    "@goods_group_id", goods_group_id,
                    "@product_type", product_type,
                    "@user_id",user_id,
                    "@shop_id",shop_id,
                    "@user_shop_id",user_shop_id,
                };
                string[] strResult = null;// plcObject.Insert(objKeywords).Split('|');
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                bool iUpdate = false;
                long id = 0;
                for (int i = 0; i < strResult.Count(); i++)
                {
                    if (strResult[i].Equals("id"))
                    {
                        if (strResult[i + 1].Equals("0"))
                        {
                            iUpdate = true;
                        }
                        else
                        {
                            id = long.Parse(strResult[i + 1]);
                        }

                    }
                }

                if (iUpdate)
                {
                    int iRowIndex = findRow(ref id, goods_id);
                    if (iRowIndex > -1) {
                        id = long.Parse(dtbResourceShopGoodsExperience.Rows[iRowIndex]["SHOP_GOODS_EXPERIENCE_ID"].ToString());
                        dtbResourceShopGoodsExperience.Rows[iRowIndex].Delete();
                       
                    }
               
                }

                DataRow data = dtbResourceShopGoodsExperience.NewRow();

                data["SHOP_GOODS_EXPERIENCE_ID"] = id;
                data["from_date"] = from_date;
                data["TO_DATE"] = to_date;
               
                dtbResourceShopGoodsExperience.Rows.Add(data);
                dtbResourceShopGoodsExperience.AcceptChanges();
                //if (tblList.getRowCount() > 0)
                //{
                //    int selected = tblList.getRowCount() - 1;
                //    tblList.setRowSelectionInterval(selected, selected);
                //}
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                  e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
                return false;
            }
            return true;
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ Modify
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        public bool modify()
        {
            try
            {
                string from_date = "";
                string to_date = "";
                string goods_id = "";
                string goods_group_id = "";
                string product_type = "";
                string user_id = "";
                string shop_id = "";
                string user_shop_id = "";
                string shop_goods_experience_id = "";

                user_shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
                user_id = Library.AppCore.SystemConfig.objSessionUser.UserName;
                shop_id = grdStore.GetFocusedDataRow()["stock_id"].ToString();
                shop_goods_experience_id = grdList.GetFocusedDataRow()["SHOP_GOODS_EXPERIENCE_ID"].ToString();
                if (dtpFromDate.Checked)
                {
                    from_date = dtpFromDate.Value.ToString("dd/MM/yyyy");
                }
                if (dtpToDate.Checked)
                {
                    to_date = dtpToDate.Value.ToString("dd/MM/yyyy");
                }
                
                object[] objKeywords = new object[]{
                    "@shop_goods_experience_id",shop_goods_experience_id,
                    "@from_date", from_date,
                    "@to_date", to_date,
                    "@shop_id", shop_id,
                    "@goods_id", goods_id,
                    "@goods_group_id", goods_group_id,
                    "@product_type", product_type,
                };

               //plcObject.Update(objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
               

                DataRow data = grdList.GetFocusedDataRow();
                data["from_date"] = from_date;
                data["TO_DATE"] = to_date;

                dtbResourceShopGoodsExperience.AcceptChanges();
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                  e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
                return false;
            }
            return true;
        }
        private int findRow(ref long id,string goods_id)
        {
            int index = -1;
            for (int i = 0; i < dtbResourceShopGoodsExperience.Rows.Count; i++)
            {

                DataRow row = dtbResourceShopGoodsExperience.Rows[i];
                string cellGoodsCode = row["goods_code"].ToString();
                if (cellGoodsCode.Equals(goods_id))
                {
                    id = long.Parse(row["SHOP_GOODS_EXPERIENCE_ID"].ToString());
                    return i;
                }
            }
            return index;
        }
        private bool validInputDetail()
        {
            if (grdStore.GetFocusedDataRow().Equals(null))
            {
                MessageBox.Show("Bạn phải chọn Siêu thị", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
           
            if (!dtpFromDate.Checked)
            {
                MessageBox.Show("Bạn phải nhập từ ngày", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (DateTime.Now.CompareTo(dtpFromDate.Value) > 1)
            {
                MessageBox.Show("Ngày ở trường từ ngày phải lớn hơn ngày hệ thống","Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                dtpFromDate.Focus();
                return false;
            }
            if (dtpToDate.Checked)
            {
                if ((DateTime.Now.CompareTo(dtpToDate.Value) > 1))
                {
                    MessageBox.Show("Ngày ở trường đến ngày phải lớn hơn ngày hệ thống", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dtpToDate.Focus();
                    return false;
                }
                if (dtpFromDate.Value.CompareTo(dtpToDate.Value) >= 1)
                {
                   
                    MessageBox.Show("Ngày ở trường đến ngày phải lớn hơn từ ngày", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dtpToDate.Focus();
                    return false;
                }
            }
            bool exists = validExists();
            if (exists)
                {
                    if (MessageBox.Show(this, "Mặt hàng trải nghiệm đã có bạn có muốn cập nhật", "Thông báo"
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                    == System.Windows.Forms.DialogResult.No)
                    {
                        dtpToDate.Focus();
                        return false;
                    }
                    else
                    {
                    }

                }
            return true;
        }

        private bool validExists() {
            try
            {
                string shop_id = "";
                string shop_goods_experirnce_id = "";
                string goods_id = "";

                shop_id = grdStore.GetFocusedDataRow()["stock_id"].ToString();
                if (currentAction == ActionMode.MODIFY_MODE && grdList.GetFocusedDataRow() != null)
                {
                    shop_goods_experirnce_id = grdList.GetFocusedDataRow()["shop_goods_experience_id"].ToString();
                }

                Dictionary<string, string> map = new Dictionary<string, string>() {
                                                { ":shop_id", shop_id},
                                                { ":shop_goods_experience_id", shop_goods_experirnce_id},
                                                { ":goods_id", goods_id}};
                DataTable dtCheck = SearchCommon(sql_common.SQL_CHECK_EXIST, map);
                return dtCheck.Rows.Count > 0;
                
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                  e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
                return false;
            }
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (this.dtpFromDate.Checked != true)
            {
                this.dtpFromDate.CustomFormat = " ";
            }
            else
            {
                this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            }
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            if (this.dtpToDate.Checked != true)
            {
                this.dtpToDate.CustomFormat = " ";
            }
            else
            {
                this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            }
        }

        private void tblList_Click(object sender, EventArgs e)
        {
            //fillDetailValue();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //onDelete();
            try
            {
                if (grdList.GetFocusedDataRow() == null || grdList.GetFocusedDataRow()["EVENT_LIST_ID"].ToString() == "")
                {
                    MessageBox.Show("Bạn phải chọn bản ghi để xóa", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (MessageBox.Show(this, "Bạn có muốn xóa không", "Xác nhận"
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                    == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }
                object[] objKeywords = new object[]{
                    "@event_id", grdList.GetFocusedDataRow()["EVENT_LIST_ID"].ToString(),
                };
                this.plcObject.DeleteEventHoliday(objKeywords);

                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                else
                {
                    dtbHolidays.Rows.Remove(grdList.GetFocusedDataRow());
                    dtbHolidays.AcceptChanges();

                    MessageBox.Show("Bạn đã xóa thành công ngày nghỉ lễ", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                //remove();
                //enableControl(true);
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                 ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }

        }

        private void onDelete()
        {
            try
            {
                if (grdList.GetFocusedDataRow() == null || grdList.GetFocusedDataRow()["shop_goods_experience_id"].ToString() == "")
                {
                    MessageBox.Show("Bạn phải chọn bản ghi để xóa", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (MessageBox.Show(this, "Bạn có muốn xóa không", "Xác nhận"
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                    == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }
               
                remove();
                enableControl(true);
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                 e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ Remove
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        public bool remove() {
		try {
            string shop_goods_experience_id = grdList.GetFocusedDataRow()["shop_goods_experience_id"].ToString();

            //plcObject.Delete(new object[]{
            //        "@shop_goods_experience_id", shop_goods_experience_id,});
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }

            for (int i = 0; i < dtbResourceShopGoodsExperience.Rows.Count; i++)
            {

                DataRow row = dtbResourceShopGoodsExperience.Rows[i];
                string cellId = row["shop_goods_experience_id"].ToString();
                if (shop_goods_experience_id.Equals(cellId))
                {
                    dtbResourceShopGoodsExperience.Rows.Remove(row);
                    break;
                }
            }

		} catch (Exception e) {
			Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                 e.Message);
            Console.WriteLine("Exception source: {0}", e.Source);
		}
		return true;
	}

        private void btnImport_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(import));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
         
        }

        private void import()
        {
            OpenFileDialog Openfile = new OpenFileDialog();
            Openfile.Filter = "Excel 2003|*.xls|Excel New|*.xlsx";
            //Openfile.Filter = "Excel Files|*.xls;*.xlsx";
            //Openfile.Title = "Select a Cursor File";
            //Openfile.InitialDirectory = "C:";
            DialogResult result = Openfile.ShowDialog();
            if (result == DialogResult.Cancel) return;
            try
            {
                DataTable dt = new DataTable();
                dt = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(Openfile.FileName);

                frmShopGoodsExperienceImportExcel frm = new frmShopGoodsExperienceImportExcel();
                frm.SourceTable = dt;
                frm.DtbShopResource = dtbResourceStore.Copy();
                
                frm.ShowDialog();
                if (frm.IsUpdate)
                {
                object[][] arrayObject = frm.LsImport.ToArray();

                //plcObject.ImportByExcel(arrayObject);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                MessageBox.Show("Import "+ frm.LsImport.Count+" Bản ghi!", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception objex)
            {
                MessageBox.Show(objex.Message.ToString(), "Lỗi nạp thông tin từ file Excel!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboGoodsGroup_Load(object sender, EventArgs e)
        {

        }


        private DataTable SearchCommon(StringBuilder sqlSelect, Dictionary<string, string> dictParam)
        {
            StringBuilder sql = new StringBuilder(sqlSelect.ToString());
            if (dictParam != null && dictParam.Count > 0)
            {
                foreach (KeyValuePair<string, string> item in dictParam)
                {
                    sql.Replace(item.Key, "'" + item.Value + "'");
                }
            }

            object[] objKeywords = new object[]
            {
              "@command",sql.ToString()
            };
            return plcGoodsTsvq.SearchData(objKeywords);
        }

        public class SQL_COMMON
        {
            public StringBuilder SQL_SHOP;
            public StringBuilder SQL_PRODUCT_TYPE;
            public StringBuilder SQL_GOOD_GROUP;
            public StringBuilder SQL_GOOD;
            public StringBuilder SQL_CHECK_EXIST;
            public SQL_COMMON()
            {
                SQL_SHOP = new StringBuilder("select s.shop_code, s.shop_name name, s.stock_id, s.shop_id from shop_stock s where s.shop_type_vtt=1 and s.shop_type = 10 and s.live=1 order by s.shop_code");
                SQL_PRODUCT_TYPE = new StringBuilder("SELECT CODE, NAME FROM ap_domain a WHERE a.type='PRODUCT_TYPE'");

                SQL_GOOD_GROUP = new StringBuilder();
                SQL_GOOD_GROUP.Append(" select goods_group_id, 'TRUE',goods_group_code, name		");
                SQL_GOOD_GROUP.Append(" from   goods_group                                          ");
                SQL_GOOD_GROUP.Append(" where status = '1'                                          ");
                SQL_GOOD_GROUP.Append(" and (product_type = :product_type or :product_type is null) ");
                SQL_GOOD_GROUP.Append(" and product_type not in (6,7,9,12,15)                       ");
                //SQL_GOOD_GROUP.Append(" CONNECT BY PRIOR goods_group_id = parent_goods_group_id     ");
                SQL_GOOD_GROUP.Append(" order by goods_group_code                                   ");

                SQL_GOOD = new StringBuilder();
                SQL_GOOD.Append("select distinct d.value code, d.value name, d.value  from goods g, goods_dividing_properties d where 1=1 ");
                SQL_GOOD.Append(" and trim(g.goods_id) = trim(d.goods_id)                                                       ");
                SQL_GOOD.Append(" and d.code = 'couple_goods'                                                                   ");
                SQL_GOOD.Append(" and g.goods_group_id  = :goods_group_id                                                       ");

                SQL_CHECK_EXIST = new StringBuilder();
                SQL_CHECK_EXIST.Append(" select ex.shop_goods_experience_id,                 				");
                SQL_CHECK_EXIST.Append("     g.value goods_code,                                            ");
                SQL_CHECK_EXIST.Append("     g.value goods_name,                                            ");
                SQL_CHECK_EXIST.Append("     ex.goods_group_id,                                             ");
                SQL_CHECK_EXIST.Append("     ex.product_type,                                               ");
                SQL_CHECK_EXIST.Append("     to_char(ex.from_date,'dd/mm/yyyy') from_date,                  ");
                SQL_CHECK_EXIST.Append("     to_char(ex.to_date,'dd/mm/yyyy') to_date                       ");
                SQL_CHECK_EXIST.Append("  from shop_goods_experience ex, goods_dividing_properties g        ");
                SQL_CHECK_EXIST.Append("  where (:shop_id is null or ex.shop_id = :shop_id)                 ");
                SQL_CHECK_EXIST.Append("  and ex.couple_goods_code = g.value                                ");
                SQL_CHECK_EXIST.Append("  and ( :goods_id is null or  ex.couple_goods_code = :goods_id)     ");
                SQL_CHECK_EXIST.Append("  and (:shop_goods_experience_id is null                            ");
                SQL_CHECK_EXIST.Append("	or ex.shop_goods_experience_id <> :shop_goods_experience_id )   ");
            }

        }

        public enum ActionMode
        {
            NONE, VIEW_MODE, ADD_MODE, MODIFY_MODE, SEARCH_MODE
        }
        public enum ACTION
        {
            ACTION_NONE, ACTION_ADD, ACTION_MODIFY, ACTION_SAVE
        }

        private void DUIHoliday_Load(object sender, EventArgs e)
        {
            //ThanhPT
            dtbHolidays = plcObject.LoadHolidays();
            this.tblList.DataSource = dtbHolidays;

            object[] objKeywords = new object[]
            {
              "@command", "SELECT '' VALUE,'' NAME from DUAL"
            };

            dtbTableGoods = plcGoodsTsvq.SearchData(objKeywords);
            dtbTableGoods.Rows.RemoveAt(0);
            tblShop.DataSource = dtbTableGoods;
        }

        private void cboProductType_Load(object sender, EventArgs e)
        {
            dtbProduct = SearchCommon(sql_common.SQL_PRODUCT_TYPE, null);
            cboProductType.InitControl(false, dtbProduct, "CODE", "NAME", "");
        }

        private void cboProductType_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            if (cboProductType.ColumnID == null || cboProductType.ColumnID.ToString().Equals(""))
            {
                dtbGoodsGroup = null;
            }
            else
            {
                Dictionary<string, string> map = new Dictionary<string, string>() {
                                                {":product_type", cboProductType.ColumnID.ToString()},};
                dtbGoodsGroup = SearchCommon(sql_common.SQL_GOOD_GROUP, map);

            }
            cboGoodsGroup.InitControl(false, dtbGoodsGroup, "goods_group_id", "NAME", "");
        }

        private void cboGoodsGroup_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            if (cboProductType.ColumnID == null || cboProductType.ColumnID.ToString().Equals("") 
                || cboGoodsGroup.ColumnID == null || cboGoodsGroup.ColumnID.ToString().Equals(""))
            {
                dtbGoods = null;
            }
            else
            {
                object[] objKeywords = new object[]{
                    "@product_id", cboProductType.ColumnID,
                    "@goood_group_id", cboGoodsGroup.ColumnID,
                };
                dtbGoods = plcObject.LoadGoods(objKeywords);
            }
            this.cboGoods.InitControl(true, dtbGoods, "value", "name", "");
        }

        private void btnAddGoods_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cboGoods.ColumnIDList != null && this.cboGoods.ColumnIDList != "")
                {
                    string strGoodsTmp = this.cboGoods.ColumnIDList.ToString().Replace("><", "|").Replace("<", "").Replace(">", "");
                    String[] arrGoods = strGoodsTmp.Split('|');
                    foreach (String goods in arrGoods)
                    {
                        DataRow data = this.dtbTableGoods.NewRow();

                        data["VALUE"] = goods;
                        data["NAME"] = goods;
                        //strGoods.Append("|" + goods + "|");
                        dtbTableGoods.Rows.Add(data);
                    }
                    dtbTableGoods.AcceptChanges();
                }
                else
                {
                    MessageBox.Show("Bạn phải chọn Hàng hóa", "Thông báo",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message, ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }

        private void btnAddAllGoods_Click(object sender, EventArgs e)
        {
            try
            {
                //this.strGoods.Clear();
                if (dtbGoods != null)
                {
                    this.dtbTableGoods.Clear();
                    for (int i = 0; i < dtbGoods.Rows.Count; i++)
                    {
                        DataRow data = this.dtbTableGoods.NewRow();
                        DataRow row = dtbGoods.Rows[i];

                        data["VALUE"] = row["VALUE"].ToString();
                        data["NAME"] = row["NAME"].ToString();
                        //this.strGoods.Append("|" + row["VALUE"].ToString() + "|");
                        dtbTableGoods.Rows.Add(data);
                    }
                    
                    dtbTableGoods.AcceptChanges();
                }
                else
                {
                    MessageBox.Show("Hàng hóa không có dữ liệu", "Thông báo",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message, ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }

        private void btnRemoveGoods_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow dtRowGoods = this.grdStore.GetFocusedDataRow();
                if (dtRowGoods == null || dtRowGoods["VALUE"].ToString() == "")
                {
                    MessageBox.Show("Bạn phải chọn bản ghi Hàng hóa", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                //string stockId = dtRowGoods["STOCK_ID"].ToString();
                //this.strGoods.Replace("|" + stockId + "|", "");
                dtbTableGoods.Rows.Remove(dtRowGoods);
                dtbTableGoods.AcceptChanges();
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                  ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }

        private void btnRemoveGoodsAll_Click(object sender, EventArgs e)
        {
            try
            {
                dtbTableGoods.Rows.Clear();
                dtbTableGoods.AcceptChanges();
                tblShop.DataSource = dtbTableGoods;
                //this.strGoods.Clear();
                this.Refresh();
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                  ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }

        private void btnOK_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (txtNameEvent.Text == null || txtNameEvent.Text.Trim().Equals(""))
                {
                    MessageBox.Show("Tên sự kiện không được trống", "Thông báo",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (this.nudHeSo.Value == null)
                {
                    MessageBox.Show("Hệ số không được trống", "Thông báo",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (this.dtbTableGoods == null || this.dtbTableGoods.Rows.Count <= 0)
                {
                    MessageBox.Show("Danh sách Hàng hóa không được trống", "Thông báo",
                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                StringBuilder shopIds = new StringBuilder();
                StringBuilder shopCodes = new StringBuilder();
                if (this.dtbShop == null || this.dtbShop.Rows.Count <= 0)
                {
                    MessageBox.Show("Danh sách Siêu thị không được trống", "Thông báo",
                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (selection == null || selection.SelectedCount <= 0)
                    {
                        MessageBox.Show("Vui lòng tích chọn siêu thị", "Thông báo",
                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {

                        for (int i = 0; i < selection.SelectedCount; i++)
                        {
                            DataRowView objRow = selection.GetSelectedRow(i) as DataRowView;
                            shopIds.Append("|" + objRow["SHOP_ID"] + "|");
                            shopCodes.Append("|" + objRow["SHOP_CODE"] + "|");
                        }
                    }
                }

                string fromDate = "";
                if (dtpFromDate.Checked && dtpFromDate.Value != null)
                {
                    fromDate = dtpFromDate.Value.ToString("dd/MM/yyyy");
                }
                else
                {
                    MessageBox.Show("Từ ngày không được để trống", "Thông báo",
                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string toDate = "";
                if (dtpToDate.Checked && dtpToDate.Value != null)
                {
                    toDate = dtpToDate.Value.ToString("dd/MM/yyyy");
                }
                else
                {
                    MessageBox.Show("Đến ngày không được để trống", "Thông báo",
                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string strDay = getDays();
                if (String.IsNullOrEmpty(strDay))
                {
                    MessageBox.Show("Phải tích ngày trong tuần ", "Thông báo",
                          MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string user_shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
                string user_id = Library.AppCore.SystemConfig.objSessionUser.UserName;
                string goodsTmp = "";
                for (int i = 0; i < dtbTableGoods.Rows.Count; i++)
                {
                    DataRow row = dtbTableGoods.Rows[i];
                    if (!goodsTmp.Contains(row["VALUE"].ToString() + "|"))
                    {
                        goodsTmp += row["VALUE"].ToString() + "|";
                    }
                }

                //string goodsTmp = this.strGoods.ToString().Replace("||", "|");
                
                string shopIdsTmp = shopIds.ToString().Replace("||", "|");

                string days = strDay.Remove(strDay.Length - 1);
                if (currentAction == ActionMode.ADD_MODE)
                {
                    string idEvent = plcObject.AddEventHolidays(goodsTmp, shopIdsTmp, fromDate, toDate,
                        txtNameEvent.Text.Trim(), days, this.nudHeSo.Value.ToString(), user_id, user_shop_id);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError || string.IsNullOrEmpty(idEvent))
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                            SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                    else
                    {
                        DataRow data = dtbHolidays.NewRow();

                        data["EVENT_LIST_ID"] = idEvent;
                        data["NAME"] = txtNameEvent.Text.Trim();
                        data["RATE"] = this.nudHeSo.Value.ToString();
                        data["FROM_DATE"] = fromDate;
                        data["TO_DATE"] = toDate;
                        data["DAYS"] = days;
                        data["SHOP_LIST"] = shopCodes.ToString().Replace("||", ",").Replace("|", "");
                        data["SHOP_ID_LIST"] = shopIds.ToString().Replace("||", ",").Replace("|", "");
                        data["USER_NAME"] = user_id;

                        dtbHolidays.Rows.InsertAt(data, 0);
                        dtbHolidays.AcceptChanges();

                        MessageBox.Show("Thêm ngày nghỉ lễ thành công!", "Thông báo",
                       MessageBoxButtons.OK, MessageBoxIcon.Information);
                        refreshData();
                        actionAdd(false);
                        actionAddButton(true);
                    }
                }
                else
                {
                    string idEvent = grdList.GetFocusedDataRow()["EVENT_LIST_ID"].ToString();
                    plcObject.UpdateEventHoliday(goodsTmp, shopIdsTmp, fromDate, toDate,
                        txtNameEvent.Text.Trim(), days, this.nudHeSo.Value.ToString(), user_id, user_shop_id, idEvent);

                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                            SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                    else
                    {
                        DataRow data = dtbHolidays.NewRow();

                        data["EVENT_LIST_ID"] = idEvent;
                        data["NAME"] = txtNameEvent.Text.Trim();
                        data["RATE"] = this.nudHeSo.Value.ToString();
                        data["FROM_DATE"] = fromDate;
                        data["TO_DATE"] = toDate;
                        data["DAYS"] = days;
                        data["SHOP_LIST"] = shopCodes.ToString().Replace("||", ",").Replace("|", "");
                        data["SHOP_ID_LIST"] = shopIds.ToString().Replace("||", ",").Replace("|", "");
                        data["USER_NAME"] = user_id;
                        dtbHolidays.Rows.Remove(grdList.GetFocusedDataRow());
                        dtbHolidays.Rows.InsertAt(data, 0);
                        dtbHolidays.AcceptChanges();

                        MessageBox.Show("Cập nhật ngày nghỉ lễ thành công!", "Thông báo",
                       MessageBoxButtons.OK, MessageBoxIcon.Information);
                        refreshData();
                        actionAdd(false);
                        actionAddButton(true);
                    }
                }

            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                  ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }
    }
    
   

}

