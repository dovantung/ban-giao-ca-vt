﻿using GemBox.Spreadsheet;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;



namespace ERP.Inventory.DUI.Goods
{
    public partial class frmImportGoodsBlock : Form
    {

        private PLC.Goods.FLCGoodsBlock plcGoodsBlock;

        public frmImportGoodsBlock()
        {
            InitializeComponent();
            plcGoodsBlock = new PLC.Goods.FLCGoodsBlock();
            btnUpdate.Enabled = false;
        }
        DataTable dtbGroupGoods;
        private void frmImportGoodsBlock_Load(object sender, EventArgs e)
        {
            this.cboTypeUpdate.Items.Add("Mã ghép");
            this.cboTypeUpdate.Items.Add("Block hàng hóa");
            this.cboTypeUpdate.Items.Add("Mức hàng thiếu min");
            this.cboTypeUpdate.Items.Add("Import Group Block");
            this.cboTypeUpdate.SelectedIndex = 0;
            DevExpress.Data.CurrencyDataController.DisableThreadingProblemsDetection = true;
            dtbCoupleGoodsResource = plcGoodsBlock.LoadGoods();
            dtbGroupGoods = plcGoodsBlock.LoadGroupGoods();

            string[] fieldNames = new string[] { "COUPLE_GOODS", "VALUE", "ISERROR", "NOTE" };

            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(this.gridControl1, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(this.gridView1);

            try
            {
                gridView1.Columns["COUPLE_GOODS"].Caption = "Mã sản phẩm";
                gridView1.Columns["VALUE"].Caption = "Giá trị mã ghép, block";

                gridView1.Columns["ISERROR"].Caption = "Lỗi";
                gridView1.Columns["NOTE"].Caption = "Ghi lỗi";

                gridView1.OptionsView.ColumnAutoWidth = false;
                gridView1.Columns["COUPLE_GOODS"].Width = 150;
                gridView1.Columns["VALUE"].Width = 230;

                gridView1.Columns["ISERROR"].Width = 80;
                gridView1.Columns["NOTE"].Width = 500;

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsError = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsError.ValueChecked = true;
                chkCheckBoxIsError.ValueUnchecked = false;
                gridView1.Columns["ISERROR"].ColumnEdit = chkCheckBoxIsError;

                gridView1.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                gridView1.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                gridView1.OptionsView.ShowAutoFilterRow = true;
                gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                gridView1.OptionsView.ShowFooter = true;
                gridView1.OptionsView.ShowGroupPanel = false;
                gridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                gridView1.ColumnPanelRowHeight = 40;

                gridView1.Columns["NOTE"].SummaryItem.FieldName = "ISERROR";
                gridView1.Columns["NOTE"].SummaryItem.DisplayFormat = "{0:n0}";
                gridView1.Columns["NOTE"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;

                //grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "NOTE";
                gridView1.Columns["ISERROR"].SummaryItem.DisplayFormat = "Tổng số dòng:";
                gridView1.Columns["ISERROR"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới danh sách");
                SystemErrorWS.Insert("Lỗi định dạng lưới danh sách", objExce.ToString(), this.Name + " -> FormatGridIMEI", DUIInventory_Globals.ModuleName);
            }


        }

        private void cboTypeUpdate_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            type = cboTypeUpdate.SelectedIndex.ToString();
            gridControl1.DataSource = null;
            btnUpdate.Enabled = false;
                if (!type.Equals(typeprevious))
                {
                    if (type.Equals("3") && !typeprevious.Equals("3"))
                    {
                        string[] fieldNames = new string[] { "COUPLE_GOODS", "NAME", "VALUE", "ISERROR", "NOTE" };
                        Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(this.gridControl1, true, false, true, true, fieldNames);
                        Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(this.gridView1);
                        try
                        {

                            gridView1.Columns["COUPLE_GOODS"].Caption = "Id NHÓM SẢN PHẨM";
                            gridView1.Columns["NAME"].Caption = "TÊN NHÓM SẢN PHẨM";
                            gridView1.Columns["VALUE"].Caption = "Giá trị block";

                            gridView1.Columns["ISERROR"].Caption = "Lỗi";
                            gridView1.Columns["NOTE"].Caption = "Ghi lỗi";

                            gridView1.OptionsView.ColumnAutoWidth = false;
                            gridView1.Columns["COUPLE_GOODS"].Width = 150;
                            gridView1.Columns["NAME"].Width = 150;
                            gridView1.Columns["VALUE"].Width = 230;

                            gridView1.Columns["ISERROR"].Width = 80;
                            gridView1.Columns["NOTE"].Width = 500;

                            DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsError = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                            chkCheckBoxIsError.ValueChecked = true;
                            chkCheckBoxIsError.ValueUnchecked = false;
                            gridView1.Columns["ISERROR"].ColumnEdit = chkCheckBoxIsError;

                            gridView1.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                            gridView1.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                            gridView1.OptionsView.ShowAutoFilterRow = true;
                            gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                            gridView1.OptionsView.ShowFooter = true;
                            gridView1.OptionsView.ShowGroupPanel = false;
                            gridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                            gridView1.ColumnPanelRowHeight = 40;

                            gridView1.Columns["NOTE"].SummaryItem.FieldName = "ISERROR";
                            gridView1.Columns["NOTE"].SummaryItem.DisplayFormat = "{0:n0}";
                            gridView1.Columns["NOTE"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;

                            //grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "NOTE";
                            gridView1.Columns["ISERROR"].SummaryItem.DisplayFormat = "Tổng số dòng:";
                            gridView1.Columns["ISERROR"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
                            this.Refresh();
                        }
                        catch (Exception objExce)
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới danh sách");
                            SystemErrorWS.Insert("Lỗi định dạng lưới danh sách", objExce.ToString(), this.Name + " -> FormatGridIMEI", DUIInventory_Globals.ModuleName);
                        }
                    }
                    else if (!type.Equals("3") && typeprevious.Equals("3"))
                    {
                        string[] fieldNames = new string[] { "COUPLE_GOODS", "VALUE", "ISERROR", "NOTE" };

                        Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(this.gridControl1, true, false, true, true, fieldNames);
                        Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(this.gridView1);

                        try
                        {
                            gridView1.Columns["COUPLE_GOODS"].Caption = "Mã sản phẩm";
                            gridView1.Columns["VALUE"].Caption = "Giá trị mã ghép, block";

                            gridView1.Columns["ISERROR"].Caption = "Lỗi";
                            gridView1.Columns["NOTE"].Caption = "Ghi lỗi";

                            gridView1.OptionsView.ColumnAutoWidth = false;
                            gridView1.Columns["COUPLE_GOODS"].Width = 150;
                            gridView1.Columns["VALUE"].Width = 230;

                            gridView1.Columns["ISERROR"].Width = 80;
                            gridView1.Columns["NOTE"].Width = 500;

                            DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsError = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                            chkCheckBoxIsError.ValueChecked = true;
                            chkCheckBoxIsError.ValueUnchecked = false;
                            gridView1.Columns["ISERROR"].ColumnEdit = chkCheckBoxIsError;

                            gridView1.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                            gridView1.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                            gridView1.OptionsView.ShowAutoFilterRow = true;
                            gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                            gridView1.OptionsView.ShowFooter = true;
                            gridView1.OptionsView.ShowGroupPanel = false;
                            gridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                            gridView1.ColumnPanelRowHeight = 40;

                            gridView1.Columns["NOTE"].SummaryItem.FieldName = "ISERROR";
                            gridView1.Columns["NOTE"].SummaryItem.DisplayFormat = "{0:n0}";
                            gridView1.Columns["NOTE"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;

                            //grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "NOTE";
                            gridView1.Columns["ISERROR"].SummaryItem.DisplayFormat = "Tổng số dòng:";
                            gridView1.Columns["ISERROR"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
                            this.Refresh();
                        }
                        catch (Exception objExce)
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới danh sách");
                            SystemErrorWS.Insert("Lỗi định dạng lưới danh sách", objExce.ToString(), this.Name + " -> FormatGridIMEI", DUIInventory_Globals.ModuleName);
                        }
                    }
                }
                typeprevious = type;
            
        }


        private void btnChoose_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(importGoodsBlock));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
        }
        DataTable dt = new DataTable();
        private void importGoodsBlock()
        {
            //List<object[]> lsImport = new List<object[]>();
            OpenFileDialog Openfile = new OpenFileDialog();
            Openfile.Filter = "Excel 2003|*.xls|Excel New|*.xlsx";
            DialogResult result = Openfile.ShowDialog();
            if (result == DialogResult.Cancel) return;
            try
            {
                dt = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(Openfile.FileName);

                if (InvokeRequired)
                {
                    Invoke(new MethodInvoker(updateGUI));
                }
                else
                {
                    gridControl1.DataSource = dt;
                    ValidateData(ref dt);
                    if (lsImport != null && lsImport.Count > 0)
                    {
                        btnUpdate.Enabled = true;
                    }
                }
            }
            catch (Exception objex)
            {
                MessageBox.Show(objex.Message.ToString(), "Lỗi nạp thông tin từ file Excel!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void updateGUI()
        {
            gridControl1.DataSource = dt;
            ValidateData(ref dt);
            if (lsImport != null && lsImport.Count > 0)
            {
                btnUpdate.Enabled = true;
            }
        }

        private List<object[]> lsImport;

        private DataTable dtbCoupleGoodsResource;
        private bool ValidateData(ref DataTable dtbSource)
        {
            dtbSource.Rows.RemoveAt(0);
            int i = 0;
            if (type.Equals("3"))
            {
                dtbSource.Columns[i++].ColumnName = "COUPLE_GOODS";
                dtbSource.Columns[i++].ColumnName = "NAME";
                dtbSource.Columns[i++].ColumnName = "VALUE";

            }
            else
            {
                dtbSource.Columns[i++].ColumnName = "COUPLE_GOODS";
                dtbSource.Columns[i++].ColumnName = "VALUE";

            }
                DataColumn dtcIsError = new DataColumn("ISERROR", typeof(bool));
                dtcIsError.DefaultValue = false;
                dtbSource.Columns.Add(dtcIsError);
                DataColumn dtcNote = new DataColumn("NOTE", typeof(string));
                dtbSource.Columns.Add(dtcNote);
                dtbSource.Columns["COUPLE_GOODS"].SetOrdinal(dtbSource.Columns["COUPLE_GOODS"].Ordinal + 1);
            

            lsImport = new List<object[]>();
            foreach (DataRow row in dtbSource.Rows)
            {
                validateRowImport(row);
            }
            dtbSource.AcceptChanges();
            return true;
        }

        private void validateRowImport(DataRow row)
        {
            string errNote = "";
            type = cboTypeUpdate.SelectedIndex.ToString();
            string goodsIdTmp = "";
            if (type.Equals("3"))
            {
                goodsIdTmp = findGoodsGroupIdsByGoodsGroupCode(row["COUPLE_GOODS"].ToString());
            }
            else
            {
                goodsIdTmp = findGoodsIdsByGoodsCode(row["COUPLE_GOODS"].ToString());
            }
            
            if (type.Equals("0"))
            {
                if (goodsIdTmp == null || goodsIdTmp == "")
                {
                    errNote += "Không tìm thấy hàng có mã " + row["COUPLE_GOODS"].ToString() + " ; ";
                }
                if (row["VALUE"].ToString().Equals(null)
                               || row["VALUE"].ToString().Equals(""))
                {
                    errNote += "Mã ghép không được trống ; ";
                }
            }
            else if (type.Equals("1"))
            {
                long value;
                if (goodsIdTmp == null || goodsIdTmp == "")
                {
                    errNote += "Không tìm thấy hàng có mã " + row["COUPLE_GOODS"].ToString() + " ; ";
                }
                if (row["VALUE"].ToString().Equals(null)
                               || row["VALUE"].ToString().Equals("")
                               || !long.TryParse(row["VALUE"].ToString(), out value))
                {
                    errNote += "Không đúng định dạng số " + row["VALUE"].ToString() + " ; ";
                }
            }
            else if (type.Equals("2"))
            {
                long value;
                string coupleCode = findCoupleCode(row["COUPLE_GOODS"].ToString());
                if (coupleCode == null || coupleCode == "")
                {
                    errNote += "Không tìm thấy hàng có mã ghép " + row["COUPLE_GOODS"].ToString() + " ; ";
                }
                goodsIdTmp = row["COUPLE_GOODS"].ToString();
                
                if (row["VALUE"].ToString().Equals(null)
                               || row["VALUE"].ToString().Equals("")
                               || !long.TryParse(row["VALUE"].ToString(), out value))
                {
                    errNote += "Không đúng định dạng số " + row["VALUE"].ToString() + " ; ";
                }
            }
            else if (type.Equals("3"))
            {
                if (goodsIdTmp == null || goodsIdTmp == "")
                {
                    errNote += "Không tìm thấy nhóm hàng có mã " + row["COUPLE_GOODS"].ToString() + " ; ";
                }
                long value;
                if (row["VALUE"].ToString().Equals(null)
                               || row["VALUE"].ToString().Equals("")
                               || !long.TryParse(row["VALUE"].ToString(), out value))
                {
                    errNote += "Không đúng định dạng số " + row["VALUE"].ToString() + " ; ";
                }

            }
            
            if (!errNote.Equals(""))
            {
                row["IsError"] = true;
                row["NOTE"] = errNote;
            }
            else
            {
                string user_shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
                string user_id = Library.AppCore.SystemConfig.objSessionUser.UserName;
                object[] objKeywordsImport = null;

                
                string code = "";
                string goodsId = "";
                if (type.Equals("0"))
                {
                    code = "couple_goods";
                    goodsId = goodsIdTmp;
                }
                else if (type.Equals("1"))
                {
                    code = "block";
                    goodsId = goodsIdTmp;
                }
                else if (type.Equals("2"))
                {
                    code = "goods_min";
                    goodsIdTmp = row["COUPLE_GOODS"].ToString();
                }
                if (type.Equals("3"))
                {
                    objKeywordsImport = new object[]{
                         "@group_id", goodsIdTmp,
                         "@block", row["VALUE"].ToString(),
                        };
                }
                else
                {
                    objKeywordsImport = new object[]{
                         "@type", type,
                         "@good_id", goodsIdTmp,
                         "@code", code,
                         "@block", row["VALUE"].ToString() == null ? "" : row["VALUE"].ToString(),
                        };
                }
                    
                lsImport.Add(objKeywordsImport);
            }
        }

        private string findGoodsGroupIdsByGoodsGroupCode(string goodsGroupCode)
        {
            if (dtbGroupGoods == null || dtbGroupGoods.Rows.Count == 0)
            {
                return "";
            }
            for (int i = 0; i < dtbGroupGoods.Rows.Count; i++)
            {
                if (dtbGroupGoods.Rows[i]["goods_group_id"].ToString().Equals(goodsGroupCode))
                {
                    return dtbGroupGoods.Rows[i]["goods_group_id"].ToString();
                }
            }
            return "";
        }
        private string findGoodsIdsByGoodsCode(string goodsCode)
        {
            if (dtbCoupleGoodsResource == null || dtbCoupleGoodsResource.Rows.Count == 0)
            {
                return "";
            }
            for (int i = 0; i < dtbCoupleGoodsResource.Rows.Count; i++)
            {
                if (dtbCoupleGoodsResource.Rows[i]["goods_code"].ToString().Equals(goodsCode))
                {
                    return dtbCoupleGoodsResource.Rows[i]["goods_id"].ToString();
                }
            }
            return "";
        }

        private string findCoupleCode(string coupleCode)
        {
            if (dtbCoupleGoodsResource == null || dtbCoupleGoodsResource.Rows.Count == 0)
            {
                return "";
            }
            for (int i = 0; i < dtbCoupleGoodsResource.Rows.Count; i++)
            {
                if (dtbCoupleGoodsResource.Rows[i]["couple_goods"].ToString().Equals(coupleCode))
                {
                    return dtbCoupleGoodsResource.Rows[i]["couple_goods"].ToString();
                }
            }
            return "";
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Chương trình sẽ chỉ lấy những dữ liệu hợp lệ. Bạn có muốn tiếp tục không?", "Thống báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                object[][] arrayObject = lsImport.ToArray();
                plcGoodsBlock.ImportGoodsBlockByExcel(arrayObject, type);

                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                MessageBox.Show("Import " + lsImport.Count + " Bản ghi!", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Bạn có chắc chắn muốn thoát chức năng?", "Thông báo", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
            {
            }
            else
            {
                this.Close();
            }
        }

        private void btnExportFileTem_Click(object sender, EventArgs e)
        {
            type = cboTypeUpdate.SelectedIndex.ToString();
            Thread thdSyncRead = new Thread(new ThreadStart(exportTemplate));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
        }
        string type = "";
        string typeprevious = "0";
        private void exportTemplate()
        {
                downloadFile();
        }

        private void downloadFile()
        {
            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strFileName = string.Empty;
            string temName = "TemplateBlock";
            if (type.Equals("0"))
            {
                temName = "TemplateCouple";

            }
            else if (type.Equals("1"))
            {
                temName = "TemplateBlock";

            }
            else if (type.Equals("2"))
            {
                temName = "TemplateMin";

            }
            else if (type.Equals("3"))
            {
                temName = "TemplateGroupBlock";

            }
            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\" + temName + ".xls";
            if (CheckFileTemplate(strFileTemplate, ref strPathFileName, temName) == false)
                return;
            strFileName = System.IO.Path.GetFileNameWithoutExtension(strPathFileName);
            if (saveFile(strFileTemplate, strFileName, strPathFileName))
            {
                OpenFileExport(strPathFileName);
                return;
            }
        }

        private bool saveFile(string strFileTemplate, string strFileName, string strPathFileName)
        {
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
                workbook.SaveExcel(strPathFileName);
                return true;
            }
            catch
            {
                return false;
            }
        }
        SaveFileDialog saveDlg;
        private Boolean CheckFileTemplate(String strFileTemplate, ref string strPathFileName, string temName)
        {
            try
            {
                saveDlg = new SaveFileDialog();

                saveDlg.Filter = "Excel Worksheet file(*.xlsx)|*.xls";
                saveDlg.AddExtension = true;
                saveDlg.RestoreDirectory = true;
                saveDlg.Title = "Chọn vị trí lưu file?";
                saveDlg.FileName = temName + "_" + System.DateTime.Now.Date.ToString("dd-MM-yyyy");
                if (saveDlg.ShowDialog() == DialogResult.OK)
                {
                    strPathFileName = System.IO.Path.ChangeExtension(saveDlg.FileName, ".xls");
                }
                if (string.IsNullOrEmpty(strPathFileName)) return false;

                if (System.IO.File.Exists(strPathFileName))
                {
                    try
                    {
                        System.IO.File.Delete(strPathFileName);
                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show("File Excel đang được mở, Bạn phải đóng lại trước khi mở file tiếp", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }

                if (System.IO.File.Exists(strFileTemplate) == false)
                {
                    MessageBox.Show("Tập tin mẫu không tồn tại");
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void OpenFileExport(String strPathFileName)
        {
            try
            {
                if (MessageBox.Show("Bạn có muốn mở file vừa lưu?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    System.Diagnostics.Process.Start(strPathFileName);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnExportData_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = this.Cursor = Cursors.WaitCursor;
                Thread thdSyncRead = new Thread(new ThreadStart(exportData));
                thdSyncRead.SetApartmentState(ApartmentState.STA);
                thdSyncRead.Start();

                this.Cursor = this.Cursor = Cursors.Default;

            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                 ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }

        private void exportData()
        {
            DataTable dt = plcGoodsBlock.ExportData();

            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strFileName = string.Empty;
            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\export_data_goods_block.xlsx";
            if (CheckFileTemplate(strFileTemplate, ref strPathFileName) == false)
                return;
            strFileName = System.IO.Path.GetFileNameWithoutExtension(strPathFileName);

            if (exportFile(dt, strFileTemplate, strFileName, strPathFileName))
            {
                OpenFileExport(strPathFileName);
                return;
            }
        }

        private bool exportFile(DataTable dtbResourceDevideItem, string strFileTemplate, string strFileName, string strPathFileName)
        {
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
            }
            catch { }

            if (workbook != null)
            {
                ExcelWorksheet sheet = (ExcelWorksheet)workbook.Worksheets[0];
                if (sheet != null)
                {
                    #region Insert Header
                    //DateTime dateTime = DateTime.UtcNow.Date;
                    //string strReviewedDate = string.Format(@"Ngày: {0}", dateTime.ToString("dd/MM/yyyy"));
                    //InsertValueCell(ref sheet, "M", 1, strReviewedDate, false, false);
                    
                    int colHeader = 1;
                    int iRowHeader = 1;
                    foreach(DataColumn column in dtbResourceDevideItem.Columns){
                        InsertValueCell(ref sheet, colHeader, iRowHeader, column.ColumnName, false, false);
                        colHeader++;
                    }

                    #endregion
                    int iRowStart = 2;
                    int iRowCurrent = iRowStart;
                    for (int i = 0; i < dtbResourceDevideItem.Rows.Count; i++)
                    {
                        int tmp = 1;
                        //InsertValueCell(ref sheet, tmp, iRowCurrent, intSTT.ToString(), false, false);
                        //tmp++;
                        foreach (object item in dtbResourceDevideItem.Rows[i].ItemArray)
                        {
                            InsertValueCell(ref sheet, tmp, iRowCurrent, item, false, false);
                            tmp++;
                        }
                        
                        //AddRowDetail(ref sheet, dtbResourceDevideItem.Rows[i], ref iRowCurrent, ref intSTT);
                        iRowCurrent++;
                    }
                }
            }
            try
            {
                workbook.SaveExcel(strPathFileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void InsertValueCell(ref ExcelWorksheet worksheet, int column, int iRow, Object value, Boolean isAdd = false, Boolean isBold = false)
        {
            try
            {
                ExcelCell cell = worksheet.Cells[iRow, column];
                SetValueRange(ref cell, value, isAdd, isBold);
            }
            catch { }
        }

        private void SetValueRange(ref ExcelCell cell, Object value, Boolean isAdd, Boolean isBold)
        {
            try
            {
                if (isBold)
                    cell.Style.Font.Weight = ExcelFont.BoldWeight;
            }
            catch { }

            if (cell != null)
            {
                try
                {
                    if (isAdd)
                        cell.Value = cell.Value.ToString() + " " + value;
                    else
                        cell.Value = value;

                    if (value.ToString().StartsWith("="))
                        cell.Formula = value.ToString();
                }
                catch { }
            }
        }

        private Boolean CheckFileTemplate(String strFileTemplate, ref string strPathFileName)
        {
            try
            {
                saveDlg = new SaveFileDialog();

                saveDlg.Filter = "Excel Worksheet file(*.xlsx)|*.xls";
                saveDlg.AddExtension = true;
                saveDlg.RestoreDirectory = true;
                saveDlg.Title = "Chọn vị trí lưu file?";
                saveDlg.FileName = "Export_data_goods_block_" + System.DateTime.Now.Date.ToString("dd-MM-yyyy");
                if (saveDlg.ShowDialog() == DialogResult.OK)
                {
                    strPathFileName = System.IO.Path.ChangeExtension(saveDlg.FileName, ".xlsx");
                }
                if (string.IsNullOrEmpty(strPathFileName)) return false;

                if (System.IO.File.Exists(strPathFileName))
                {
                    try
                    {
                        System.IO.File.Delete(strPathFileName);
                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show("File Excel đang được mở, Bạn phải đóng lại trước khi mở file tiếp", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }

                if (System.IO.File.Exists(strFileTemplate) == false)
                {
                    MessageBox.Show("Tập tin mẫu không tồn tại");
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }


        private void btnGenDevidingSource_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, " Bạn có chắc chắn muốn cập nhật lại nguồn chia theo mã ghép mới?", "Thông báo"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                == System.Windows.Forms.DialogResult.No)
            {

            }
            else
            {
                String error = plcGoodsBlock.ResumCoupleGoods();
                if (!string.IsNullOrEmpty(error))
                {
                    MessageBox.Show("Lỗi tính lại nguồn chia: " + error);
                }
                else
                {
                    MessageBox.Show("Tính lại nguồn chia thành công", "Thông báo",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            
        }
    }
}
