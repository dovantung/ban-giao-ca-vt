﻿using GemBox.Spreadsheet;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Goods
{
    public partial class DUIGoodsDmtb : Form
    {
        private DataTable dtbResourceStore = null;
        private DataTable dtbResourceType = null;
        private DataTable dtbResourceGroup = null;
        private DataTable dtbResourceGoods = null;
        private DataTable dtbResourceGoodsDMTB = null;

        private PLC.Goods.PLCGoodsDmtb goodsDmtb = new PLC.Goods.PLCGoodsDmtb();

        private ActionMode currentAction = ActionMode.NONE;
        public DUIGoodsDmtb()
        {
            InitializeComponent();
            SearchDataStore();       
            enableControl(true);
            search();
            
        }

        private void SearchDataStore()
        {   
            dtbResourceStore = goodsDmtb.SearchDataStore();
            //dtbResourceStore = Library.AppCore.DataSource.GetDataSource.GetStore();
            tblShop.DataSource = dtbResourceStore;
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
        }

        private void cbbType_Load(object sender, EventArgs e)
        {
            object[] objKeywords = new object[]{"@TYPE", "PRODUCT_TYPE"};
            dtbResourceType = goodsDmtb.LoadDataApDomain(objKeywords);
            cboProductType.InitControl(false, dtbResourceType, "CODE", "NAME", "");
        }

        private void cbbType_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            
                object[] objKeywords = new object[] { "@product_type", cboProductType.ColumnID };
                dtbResourceGroup = goodsDmtb.LoadDataGoodsGroup(objKeywords);
                cbbGroups_SelectionChangeCommitted(null, null);
            
            cboGoodsGroup.InitControl(false, dtbResourceGroup, "goods_group_id", "NAME", "");
        }

        private void cbbGroups_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            
            object[] objKeywords = new object[] { "@goods_group_id", cboGoodsGroup.ColumnID };
            dtbResourceGoods = goodsDmtb.LoadDataGoods(objKeywords);
            pnlGoods.InitControl(false, dtbResourceGoods, "code", "name", "");
        }

        private void cbbGoods_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            string vstrGoodName = "";
            if (pnlGoods.ColumnID.Equals(null) || pnlGoods.ColumnID.Equals(""))
            {
            }
            else
            {
                vstrGoodName = pnlGoods.ColumnName;
            }
          
        }

        private void grdStores_Click(object sender, EventArgs e)
        {
            search();
           
        }

        private void onSearch()
        {
            if (currentAction == ActionMode.NONE)
            {
                currentAction = ActionMode.SEARCH_MODE;
                clearDetailValue();
                enableControl(false);
                search();
            }
            else
            {
                search();
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ Delete du lieu cho cac hop text box
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        private void clearDetailValue()
        {      
            txtValue.Value = 0;
            cboProductType.Refresh();
            cboGoodsGroup.Refresh();
            pnlGoods.Refresh();


        }

        private void enableControl(bool enable)
        {
            btnAdd.Visible = enable;
            btnModify.Visible = enable;
            btnDelete.Visible = enable;
            btnExit.Visible = enable;
            btnImport.Visible = enable;
            btnSearch.Visible = enable;

            btnOK.Visible =!enable;
            btnCancel.Visible = !enable;

            tblShop.Enabled = enable;
            tblList.Enabled = enable;
            pnlGoods.Enabled =!enable;
            dtpFromDate.Enabled = !enable;
            dtpToDate.Enabled = !enable;
            txtValue.Enabled = !enable;
           // formData.setFieldEnabled(!enable);
            if (currentAction == ActionMode.ADD_MODE || currentAction == ActionMode.MODIFY_MODE)
            {

                if (currentAction == ActionMode.ADD_MODE)
                {

                    dtpFromDate.Value = DateTime.Now;
                    pnlGoods.Focus();
                }
                else
                {
                    pnlGoods.Focus();
                }
            }
            if (currentAction == ActionMode.SEARCH_MODE)
            {
                btnExport.Visible = true;
                btnSearch.Visible = true;
            }
            else
            {
                btnExport.Visible = true;
                //btnSearch.Visible = false;
            }
        }

        public bool search()
        {
            try
            {
                if (grdStore.GetFocusedDataRow() == null) { 
                    return true;
                }

                string shop_id = "", goods_id = "", product_type = "", goods_group_id = "";
                DataRow dtRow = grdStore.GetFocusedDataRow();
                shop_id = dtRow["stock_id"].ToString();
                if (currentAction != ActionMode.SEARCH_MODE && currentAction != ActionMode.NONE)
                {
                    if (pnlGoods.ColumnID.Equals(null) || pnlGoods.ColumnID.Equals(""))
                        return true;
                    else
                    {
                        goods_id = pnlGoods.ColumnID;
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(cboProductType.ColumnID))
                    { }
                    else
                    {
                        product_type = cboProductType.ColumnID.ToString();
                    }
                    if (String.IsNullOrEmpty(cboGoodsGroup.ColumnID))
                    { }
                    else
                    {
                        goods_group_id = cboGoodsGroup.ColumnID.ToString();
                    }
                }
                
                object[] objKeywords = new object[]{
                    "@shop_id", shop_id,
                    "@goods_id", goods_id,
                    "@product_type",product_type,
                    "@goods_group_id",goods_group_id,
                };
                dtbResourceGoodsDMTB = goodsDmtb.LoadDataGoodsDMTB(objKeywords);
                tblList.DataSource = dtbResourceGoodsDMTB;
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }

                
               
                return true;
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                   e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
                // TODO: handle exception
                return false;
            }
        }

        private void fillDetailValue()
        {
            try
            {
                if (currentAction == ActionMode.SEARCH_MODE || currentAction == ActionMode.SEARCH_MODE) return;
                if (grdList.GetFocusedDataRow() == null) return;
                DataRow data = grdList.GetFocusedDataRow();
                string from_date = data["from_date"].ToString();
                string to_date = data["to_date"].ToString();

                if (from_date != null && !from_date.Equals(""))
                {
                    dtpFromDate.Checked = true;

                    dtpToDate.Value = DateTime.ParseExact(from_date , "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtpFromDate.Checked = false;
                }
                    dtpFromDate_ValueChanged(null, null);

                    if (to_date != null && !to_date.Equals(""))
                {
                    dtpToDate.Checked = true;
                    dtpToDate.Value = DateTime.ParseExact(to_date , "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtpToDate.Checked = false;
                }

                dtpToDate_ValueChanged(null, null);
                txtValue.Value = long.Parse(data["VALUE"].ToString());

                string goodType = data["product_type"].ToString();
                string goodGroup = data["goods_group_id"].ToString();
                string goodsId = data["goods_code"].ToString();
                cboProductType.SetValue(long.Parse(goodType));
                cbbType_SelectionChangeCommitted(null, null);
                cboGoodsGroup.SetValue(long.Parse(goodGroup));
                cbbGroups_SelectionChangeCommitted(null,null);
                pnlGoods.SetValue(goodsId);
                 

            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                       ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
            }
        }

        private bool isSelectAll()
        {
            //int iRowIndex = tblShop.getSelectedRow();
            //Vector<String> data = tblShop.getRow(iRowIndex);
            //if (data.get(2).equals(""))
            //{
            //    return true;
            //}
            //else
            //{
               return false;
            //}
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            currentAction = ActionMode.NONE;
            search();
            fillDetailValue();
            enableControl(true);
            onChangeAction(ACTION.ACTION_NONE);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Bạn có chắc chắn muốn thoát chức năng?", "Thông báo"
                , MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                == System.Windows.Forms.DialogResult.No)
            {
            }
            else
            {
                this.Close();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (isSelectAll()) return;
            currentAction = ActionMode.ADD_MODE;
            clearDetailValue();
            enableControl(false);
            onChangeAction(ACTION.ACTION_ADD);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            onSearch();
            currentAction = ActionMode.NONE;
            onChangeAction(ACTION.ACTION_NONE);
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            if (isSelectAll()) return;
            currentAction = ActionMode.MODIFY_MODE;
            enableControl(false);
            onChangeAction(ACTION.ACTION_MODIFY);
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ Bat su kien dau vao
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        public void onChangeAction(ACTION iNewAction)
        {
            if (iNewAction == ACTION.ACTION_NONE)
            {
                fillDetailValue();
                enableControl(true);
                tblList.Enabled = true;
               // pnlGoods.txtCode.setEnabled(false);
                //formData.setFieldEnabled(false);
                pnlGoods.Enabled = false;
                dtpFromDate.Enabled = false;
                dtpToDate.Enabled = false;
                txtValue.Enabled = false;
            }
            else if (iNewAction == ACTION.ACTION_ADD || iNewAction == ACTION.ACTION_MODIFY)
            {
                pnlGoods.Enabled = true;
                dtpFromDate.Enabled = true;
                dtpToDate.Enabled = true;
                txtValue.Enabled = true;
                //formData.setFieldEnabled(true);
                pnlGoods.Enabled = true;
                enableControl(false);
                if (iNewAction == ACTION.ACTION_ADD)
                {
                    clearDetailValue();
                    dtpFromDate.Value = DateTime.Now;
                    //pnlGoods.Focus();
                    cboProductType.Focus();
                }
                else
                {
                    pnlGoods.Focus();
                }
            }
            if (iNewAction == ACTION.ACTION_SAVE)
            {
                enableControl(true);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (currentAction == ActionMode.ADD_MODE || currentAction == ActionMode.MODIFY_MODE)
            {
                if (!validInputDetail()) return;
            }
            if (currentAction == ActionMode.ADD_MODE)
            {
                add();
            }
            else if (currentAction == ActionMode.MODIFY_MODE)
            {
                modify();
            }
            currentAction = ActionMode.NONE;
            enableControl(true);
            onChangeAction(ACTION.ACTION_SAVE);
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ ADD
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        public bool add()
        {
            try
            {
                string from_date = "";
                string to_date = "";
                string goods_id = "";
                string value = "";
                string user_id = "";
                string shop_id = "";
                string user_shop_id = "";
                user_shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
                user_id = Library.AppCore.SystemConfig.objSessionUser.UserName;
                value = txtValue.Value.ToString();
                shop_id = grdStore.GetFocusedDataRow()["stock_id"].ToString();
                goods_id = pnlGoods.ColumnID;
                if (dtpFromDate.Checked)
                {
                    from_date = dtpFromDate.Value.ToString("dd/MM/yyyy");
                }
                if (dtpToDate.Checked)
                {
                    to_date = dtpToDate.Value.ToString("dd/MM/yyyy");
                }
                object[] objKeywords = new object[]{
                    "@from_date", from_date,
                    "@to_date", to_date,
                    "@goods_id", goods_id,
                    "@value",value,
                    "@user_id",user_id,
                    "@shop_id",shop_id,
                    "@user_shop_id",user_shop_id,
                };
                string[] strResult = goodsDmtb.Insert(objKeywords).Split('|');
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                bool iUpdate = false;
                long id = 0;
                long sumGroup = 0;
                long inventValue = 0;
                for (int i = 0; i < strResult.Count(); i++)
                {
                    if (strResult[i].Equals("id"))
                    {
                        if (strResult[i + 1].Equals("0"))
                        {
                            iUpdate = true;
                        }
                        else
                        {
                            id = long.Parse(strResult[i + 1]);
                        }

                    }
                    if (strResult[i].Equals("sum_result"))
                    {
                        long.TryParse(strResult[i + 1], out sumGroup);
                        //sumGroup = long.Parse(strResult[i + 1]);
                    }
                    if (strResult[i].Equals("invent_value"))
                    {
                        long.TryParse(strResult[i + 1], out inventValue);
                        //inventValue = long.Parse(strResult[i + 1]);
                    }
                   
                }

                if (iUpdate)
                {
                    int iRowIndex = findRow(shop_id, goods_id);
                    if (iRowIndex > -1) {
                        id = long.Parse(dtbResourceGoodsDMTB.Rows[iRowIndex]["goods_dmtb_id"].ToString());
                        dtbResourceGoodsDMTB.Rows[iRowIndex].Delete();
                       
                    }
               
                }

                DataRow data = dtbResourceGoodsDMTB.NewRow();

                data["goods_dmtb_id"] = id;
                data["goods_code"]=pnlGoods.ColumnID;
                data["goods_name"]=pnlGoods.ColumnID;
                string groupCode = cboGoodsGroup.ColumnID.ToString();
                data["goods_group_id"] = groupCode;
                data["product_type"] = cboProductType.ColumnID;
                data["VALUE"]= value;
                data["from_date"] = from_date;
                data["TO_DATE"] = to_date;
                if (!grdStore.GetFocusedDataRow()["shop_code"].ToString().Equals(null)
                    && !grdStore.GetFocusedDataRow()["shop_code"].ToString().Equals(""))
                {
                    data["shop_code"] = grdStore.GetFocusedDataRow()["shop_code"].ToString();
                }
                else
                {
                   data["shop_code"] = "AGG1";
                }
                String groupName = "";
                if (!cboGoodsGroup.ColumnName.ToString().Equals(null)
                    && !cboGoodsGroup.ColumnName.ToString().Equals(""))
                {
                    groupName = cboGoodsGroup.ColumnName;
                }
                data["name"] = groupName;
                data["total_group"] = sumGroup;
                double percent = sumGroup == 0 ? 0 : (double.Parse(txtValue.Value.ToString()) / sumGroup) * 100;
                data["percent"]= String.Concat(percent.ToString()," %");
                data["invent_value"] = inventValue;
             
                dtbResourceGoodsDMTB.Rows.Add(data);
                dtbResourceGoodsDMTB.AcceptChanges();
                //if (tblList.getRowCount() > 0)
                //{
                //    int selected = tblList.getRowCount() - 1;
                //    tblList.setRowSelectionInterval(selected, selected);
                //}
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                  e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
                return false;
            }
            return true;
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ Modify
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        public bool modify()
        {
            try
            {
                string from_date = "";
                string to_date = "";
                string goods_id = "";
                string value = "";
                string user_id = "";
                string shop_id = "";
                string user_shop_id = "";
                string goods_dmtb_id = "";
                string goods_group = "";
                string product_type = "";

                user_shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
                user_id = Library.AppCore.SystemConfig.objSessionUser.UserName;
                value = txtValue.Value.ToString();
                shop_id = grdStore.GetFocusedDataRow()["stock_id"].ToString();
                product_type = cboGoodsGroup.ColumnID == null ? "":cboGoodsGroup.ColumnID.ToString();
                goods_group = cboGoodsGroup.ColumnID == null ? "" : cboGoodsGroup.ColumnID.ToString();
                goods_id = pnlGoods.ColumnID;
                goods_dmtb_id = grdList.GetFocusedDataRow()["goods_dmtb_id"].ToString();
                if (dtpFromDate.Checked)
                {
                    from_date = dtpFromDate.Value.ToString("dd/MM/yyyy");
                }
                if (dtpToDate.Checked)
                {
                    to_date = dtpToDate.Value.ToString("dd/MM/yyyy");
                }
                
                object[] objKeywords = new object[]{
                    "@goods_dmtb_id",goods_dmtb_id,
                    "@from_date", from_date,
                    "@to_date", to_date,
                    "@shop_id", shop_id,
                    "@goods_id", goods_id,
                    "@value",value,
                };

                string[] strResult = goodsDmtb.Update(objKeywords).Split('|');
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                long sumGroup = 0;
                long inventValue = 0;
                for (int i = 0; i < strResult.Count(); i++)
                {
                   
                    if (strResult[i].Equals("sum_result"))
                    {
                        sumGroup = long.Parse(strResult[i + 1]);
                    }
                    if (strResult[i].Equals("invent_value"))
                    {
                        inventValue = long.Parse(strResult[i + 1]);
                    }

                }

                DataRow data = grdList.GetFocusedDataRow();
                data["goods_code"] = pnlGoods.ColumnID;
                data["goods_name"] = pnlGoods.ColumnID;
                string groupCode = cboGoodsGroup.ColumnID.ToString();
                data["goods_group_id"] = groupCode;
                data["product_type"] = cboProductType.ColumnID;
                data["VALUE"] = value;
                data["from_date"] = from_date;
                data["TO_DATE"] = to_date;
                if (!grdStore.GetFocusedDataRow()["shop_code"].ToString().Equals(null)
                    && !grdStore.GetFocusedDataRow()["shop_code"].ToString().Equals(""))
                {
                    data["shop_code"] = grdStore.GetFocusedDataRow()["shop_code"].ToString();
                }
                else
                {
                    data["shop_code"] = "AGG1";
                }
                String groupName = "";
                if (!cboGoodsGroup.ColumnName.ToString().Equals(null)
                    && !cboGoodsGroup.ColumnName.ToString().Equals(""))
                {
                    groupName = cboGoodsGroup.ColumnName;
                }
                data["name"] = groupName;
                data["total_group"] = sumGroup;
                double percent = sumGroup == 0 ? 0 : (double.Parse(txtValue.Value.ToString()) / sumGroup) * 100;
                data["percent"] = String.Concat(percent.ToString(), " %");
                data["invent_value"] = inventValue;

                dtbResourceGoodsDMTB.AcceptChanges();
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                  e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
                return false;
            }
            return true;
        }
        private int findRow(String shop_id, String goods_id)
        {
            int index = -1;
            for (int i = 0; i < dtbResourceGoodsDMTB.Rows.Count; i++)
            {

                DataRow row = dtbResourceGoodsDMTB.Rows[i];
                string cellGoodsCode = row["goods_code"].ToString();
                string cellShopCode = row["shop_code"].ToString();
                if (cellGoodsCode.Equals(goods_id) && cellShopCode.Equals(shop_id))
                {
                    return i;
                }
            }
            return index;
        }
        private bool validInputDetail()
        {
            if (grdStore.GetFocusedDataRow().Equals(null))
            {
                MessageBox.Show("Bạn phải chọn Siêu thị", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                txtValue.Focus();
                return false;
            }
            if (txtValue.Value.Equals(null)
                || txtValue.Value.ToString().Equals("") || txtValue.Value < 1)
            {
                MessageBox.Show("Bạn phải nhập Định mức trưng bày", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                txtValue.Focus();
                return false;
            }
            if (!dtpFromDate.Checked)
            {
                MessageBox.Show("Bạn phải nhập từ ngày", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (DateTime.Now.CompareTo(dtpFromDate.Value) >= 1)
            {
                MessageBox.Show("Ngày ở trường từ ngày phải lớn hơn ngày hệ thống","Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                dtpFromDate.Focus();
                return false;
            }
            if (dtpToDate.Checked)
            {
                if ((DateTime.Now.CompareTo(dtpToDate.Value) >= 1))
                {
                    MessageBox.Show("Ngày ở trường đến ngày phải lớn hơn ngày hệ thống", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dtpToDate.Focus();
                    return false;
                }
                if (dtpFromDate.Value.CompareTo(dtpToDate.Value) >= 1)
                {
                   
                    MessageBox.Show("Ngày ở trường đến ngày phải lớn hơn từ ngày", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dtpToDate.Focus();
                    return false;
                }
            }
            string[] validExistResult = validExists().Split('|');
            bool exists = false;
            long sumGroup = 0;
            long sumAllInGroup = 0;
            for (int i = 0; i < validExistResult.Count(); i++)
            {
                if (validExistResult[i].Equals("check_exist"))
                {
                    if (validExistResult[i + 1].Equals("1"))
                    {
                        exists = true;
                    }
                    
                }
                if (validExistResult[i].Equals("sum_result"))
                {
                    sumGroup = long.Parse(validExistResult[i + 1]);
                }
                if (validExistResult[i].Equals("sum_other_result"))
                {
                    sumAllInGroup = long.Parse(validExistResult[i + 1]);
                }
            }

            if (exists)
                {
                    if (MessageBox.Show(this, "Định mức của mặt hàng này đã có bạn có muốn cập nhật", "Thông báo"
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                    == System.Windows.Forms.DialogResult.No)
                    {
                        dtpToDate.Focus();
                        return false;
                    }
                    else
                    {
                    }

                }
            String goodCode = pnlGoods.ColumnID;
            if (cboProductType.ColumnID.ToString().Equals("6"))
            {
                long dmtb = long.Parse(txtValue.Value.ToString());
                if (dmtb + sumAllInGroup > sumGroup)
                {
                    MessageBox.Show("Tổng định mức trưng bày " + (dmtb + sumAllInGroup).ToString() + " lớn hơn tổng định mức nhóm hàng : " + sumGroup.ToString(), "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtValue.Focus();
                    return false;
                }
            }
            return true;
        }

        private string validExists() {
            try
            {
                string shop_id = "";
                string goods_dmtb_id = "";
                string goods_id = "";
                string from_date = "";
                string to_date = "";
                string value = ""; 

                
                //value = txtValue.Value.ToString();

                shop_id = grdStore.GetFocusedDataRow()["stock_id"].ToString();
                goods_id = pnlGoods.ColumnID;
                if (currentAction == ActionMode.MODIFY_MODE && grdList.GetFocusedDataRow() != null)
                {
                    goods_dmtb_id = grdList.GetFocusedDataRow()["goods_dmtb_id"].ToString();
                }
                if (dtpFromDate.Checked)
                {
                    from_date = dtpFromDate.Value.ToString("dd/MM/yyyy");
                }
                if (dtpToDate.Checked)
                {
                    to_date = dtpToDate.Value.ToString("dd/MM/yyyy");
                }

                object[] objKeywords = new object[]{
                    "@shop_id", shop_id,
                    "@goods_dmtb_id", goods_dmtb_id,
                    "@goods_id", goods_id,
                    "@from_date",from_date,
                    "@to_date",to_date,
                    "@value",value,
                };
                return goodsDmtb.CheckGoodsDmtbExists(objKeywords);
                
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                  e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
                pnlGoods.Focus();
                return "";
            }
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            if (this.dtpFromDate.Checked != true)
            {
                this.dtpFromDate.CustomFormat = " ";
            }
            else
            {
                this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            }
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            if (this.dtpToDate.Checked != true)
            {
                this.dtpToDate.CustomFormat = " ";
            }
            else
            {
                this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            }
        }

        private void tblList_Click(object sender, EventArgs e)
        {
            fillDetailValue();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            onDelete();
        }

        private void onDelete()
        {
            try
            {
                if (grdList.GetFocusedDataRow() == null || grdList.GetFocusedDataRow()["goods_dmtb_id"].ToString() == "")
                {
                    MessageBox.Show("Bạn phải chọn bản ghi để xóa", "Thông báo",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (MessageBox.Show(this, "Bạn có muốn xóa không", "Xác nhận"
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                    == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }
               
                remove();
                enableControl(true);
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                 e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ Remove
        // * @ since 19/03/2017
        ////////////////////////////////////////////////////////////////////////////////
        public bool remove() {
		try {
			string goods_dmtb_id = grdList.GetFocusedDataRow()["goods_dmtb_id"].ToString();

            goodsDmtb.Delete(new object[]{
                    "@goods_dmtb_id", goods_dmtb_id,} );
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }

            for (int i = 0; i < dtbResourceGoodsDMTB.Rows.Count; i++)
            {

                DataRow row = dtbResourceGoodsDMTB.Rows[i];
                string cellId = row["goods_dmtb_id"].ToString();
                if (goods_dmtb_id.Equals(cellId))
                {
                    dtbResourceGoodsDMTB.Rows.Remove(row);
                    break;
                }
            }

					
				//  Update Graphical User Interface: update table.
                    //if (tblList.getRowCount() > 0) {
                    //    if (iSelected[0] < 0 ||
                    //        iSelected[0] >= tblList.getRowCount()) {
                    //        iSelected[0] = tblList.getRowCount() - 1;
                    //    }
                    //    tblList.changeSelectedRow(iSelected[0]);
                    //}

		} catch (Exception e) {
			Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                 e.Message);
            Console.WriteLine("Exception source: {0}", e.Source);
			pnlGoods.Focus();
		}
		return true;
	}

        private void btnImport_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(import));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
         
        }

        private void import()
        {
            OpenFileDialog Openfile = new OpenFileDialog();
            Openfile.Filter = "Excel 2003|*.xls|Excel New|*.xlsx";
            //Openfile.Filter = "Excel Files|*.xls;*.xlsx";
            //Openfile.Title = "Select a Cursor File";
            //Openfile.InitialDirectory = "C:";
            DialogResult result = Openfile.ShowDialog();
            if (result == DialogResult.Cancel) return;
            try
            {
                DataTable dt = new DataTable();
                dt = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(Openfile.FileName);

                frmGoodsDmtbImportExcel frm = new frmGoodsDmtbImportExcel();
                frm.SourceTable = dt;
                frm.CurImportMode = ImportMode.NUMBER_IMPORT;
                frm.DtbShopResource = dtbResourceStore.Copy();
                
                frm.ShowDialog();
                if (frm.IsUpdate)
                {
                object[][] arrayObject = frm.LsImport.ToArray();

                goodsDmtb.ImportByExcel(arrayObject);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                MessageBox.Show("Import "+ frm.LsImport.Count+" Bản ghi!", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception objex)
            {
                MessageBox.Show(objex.Message.ToString(), "Lỗi nạp thông tin từ file Excel!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboGoodsGroup_Load(object sender, EventArgs e)
        {

        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            Common.frmExportExcel frmExportExcel = new frmExport()
            {
                StrFileTemplateName = "goods_dmtb_report",
                StrFileTemplatePath = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\goods_dmtb_report.xlsx",
                DtbResourceSource = dtbResourceGoodsDMTB
            };
            frmExportExcel.btnExport_Click();
        }

        class frmExport : Common.frmExportExcel
        {

            public override void AddRowDetail(ref ExcelWorksheet sheet, DataRow row, ref int iRow, ref int iSTT)
            {
                InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
                InsertValueCell(ref sheet, "B", iRow, row["SHOP_CODE"].ToString(), false, false);
                InsertValueCell(ref sheet, "C", iRow, row["NAME"].ToString(), false, false);
                InsertValueCell(ref sheet, "D", iRow, row["GOODS_CODE"].ToString(), false, false);
                InsertValueCell(ref sheet, "E", iRow, row["GOODS_NAME"].ToString(), false, false);
                InsertValueCell(ref sheet, "F", iRow, row["VALUE"].ToString(), false, false);
                InsertValueCell(ref sheet, "G", iRow, row["FROM_DATE"].ToString(), false, false);
                InsertValueCell(ref sheet, "H", iRow, row["TO_DATE"].ToString(), false, false);
                InsertValueCell(ref sheet, "I", iRow, row["TOTAL_GROUP"].ToString(), false, false);
                InsertValueCell(ref sheet, "J", iRow, row["PERCENT"].ToString(), false, false);
                iSTT++;
                iRow++;
            }
        }

        private void btnDownTemplate_Click(object sender, EventArgs e)
        {
            Common.frmDownloadTemplateImport frmExportExcel = new Common.frmDownloadTemplateImport()
            {
                StrFileTemplateName = "TmpGoodsDmtb",
                StrFileTemplatePath = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\TmpGoodsDmtb.xlsx",
            };
            frmExportExcel.btnExportTemplate_Click();
        }
    }
    public enum ActionMode
    {
        NONE, VIEW_MODE, ADD_MODE, MODIFY_MODE, SEARCH_MODE
    }
    public enum ACTION
    {
        ACTION_NONE, ACTION_ADD, ACTION_MODIFY, ACTION_SAVE
    }
   

}
