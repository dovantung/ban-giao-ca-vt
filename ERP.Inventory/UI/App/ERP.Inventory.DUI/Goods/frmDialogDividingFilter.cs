﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Goods
{
    public partial class frmDialogDividingFilter : Form
    {

        private DataTable dtbResourceCenter = null; //Khu vuc
        private DataTable dtbResourceShop = null; //Sieu thi
        private DataTable dtbResourceGoodsGroup = null; //Nhom hang
        private DataTable dtbResourceDoubleCode = null; //Mã ghép
        private PLC.Goods.PLCGoodsTsvq plcObject;
        private frmGoodsDividingList frmGoodsDividingList;
        private frmGoodsDividingListPK frmGoodsDividingListPK;
        private string type;
        public frmDialogDividingFilter(frmGoodsDividingList frmGoodsDividingList, 
            DataTable dtbResourceShop, string type)
        {
            InitializeComponent();
            this.type = type;
            
            this.frmGoodsDividingList = frmGoodsDividingList;
            init(dtbResourceShop);
        }

        public void init(DataTable dtbResourceShop)
        {
            plcObject = new PLC.Goods.PLCGoodsTsvq();

            this.cboZone.Items.Add("");
            this.cboZone.Items.Add("VTMB");
            this.cboZone.Items.Add("VTMN");
            this.cboZone.SelectedIndex = 0;

            dtbResourceCenter = SearchCommon(new StringBuilder("select code, name from ap_domain where type  = '88' and status = 1 order by code"), null);
            cboCenter.InitControl(false, dtbResourceCenter, "CODE", "NAME", "");
            this.dtbResourceShop = dtbResourceShop.Copy();
            //dtbResourceShop = SearchCommon(new StringBuilder("select shop_id, shop_code, shop_name from shop"), null);
            cboShop.InitControl(false, dtbResourceShop, "shop_code", "NAME", "");

            dtbResourceGoodsGroup = SearchCommon(new StringBuilder("select goods_group_id, name from goods_group where COMPANY = 1 order by GOODS_GROUP_CODE"), null);
            this.cboGoodsGroup.InitControl(false, dtbResourceGoodsGroup, "GOODS_GROUP_ID", "NAME", "");

            dtbResourceDoubleCode = SearchCommon(new StringBuilder("select distinct value couple_goods, value from goods_dividing_properties p,goods g where code = 'couple_goods' and p.goods_id = g.goods_id and p.value is not null"), null);
            this.cboDoubleCode.InitControl(false, dtbResourceDoubleCode, "COUPLE_GOODS", "VALUE", "");
        }

        public frmDialogDividingFilter(frmGoodsDividingListPK frmGoodsDividingListPK,
            DataTable dtbResourceShop, string type)
        {
            InitializeComponent();
            this.type = type;
            this.frmGoodsDividingListPK = frmGoodsDividingListPK;
            init(dtbResourceShop);
        }

        public frmDialogDividingFilter()
        {
            // TODO: Complete member initialization
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
           this.Close();
        }

        private void frmDialogDividingFilter_Load(object sender, EventArgs e)
        {
            
        }

        private DataTable SearchCommon(StringBuilder sqlSelect, Dictionary<string, string> dictParam)
        {
            StringBuilder sql = new StringBuilder(sqlSelect.ToString());
            if (dictParam != null && dictParam.Count > 0)
            {
                foreach (KeyValuePair<string, string> item in dictParam)
                {
                    sql.Replace(item.Key, "'" + item.Value + "'");
                }
            }
            object[] objKeywords = new object[]
            {
              "@command",sql.ToString()
            };
            return plcObject.SearchData(objKeywords);
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            this.Close();
            if (type.Equals("frmGoodsDividingList"))
            {
               frmGoodsDividingList.filterGoodsDividingList(this.cboGoodsGroup.ColumnID.ToString(),
               this.txtGoodsCode.Text,
               this.cboDoubleCode.ColumnID.ToString(), this.cboShop.ColumnID.ToString(),
               this.cboCenter.ColumnID.ToString(), cboZone.SelectedItem.ToString());
            }
            else
            {
               frmGoodsDividingListPK.filterGoodsDividingList(this.cboGoodsGroup.ColumnID.ToString(),
               this.txtGoodsCode.Text,
               this.cboDoubleCode.ColumnID.ToString(), this.cboShop.ColumnID.ToString(),
               this.cboCenter.ColumnID.ToString(), cboZone.SelectedItem.ToString());
            }
           

        }

        public void refreshValue()
        {
                this.cboZone.Refresh();
                this.txtGoodsCode.Clear();
                this.cboShop.ResetDefaultValue();
                this.cboGoodsGroup.ResetDefaultValue();
                this.cboCenter.ResetDefaultValue();
                this.cboDoubleCode.ResetDefaultValue();
        }
    }
}
