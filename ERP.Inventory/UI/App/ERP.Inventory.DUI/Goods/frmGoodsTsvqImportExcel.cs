﻿using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Goods
{
    public partial class frmGoodsTsvqImportExcel : Form
    {
        #region Variable

        private bool bolIsUpdate = false;
        private DataTable dtbSource = new DataTable();
	    private ERP.Inventory.PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();
        private ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock = null;

        public bool IsUpdate
        {
            get { return bolIsUpdate; }
        }

        public DataTable SourceTable
        {
            get { return dtbSource; }
            set { dtbSource = value; }
        }

        private DataTable dtbShopResource;

        public DataTable DtbShopResource
        {
            get { return dtbShopResource; }
            set { dtbShopResource = value; }
        }

        private List<object[]> lsImport;

        public List<object[]> LsImport
        {
            get { return lsImport; }
            set { lsImport = value; }
        }

	    #endregion


        #region Constructor 

        public frmGoodsTsvqImportExcel()
        {
            InitializeComponent();
        }

        #endregion

        #region Event		 

        private void frmGoodsDmtbImportExcel_Load(object sender, EventArgs e)
        {
            string[] fieldNames = null;
          
               fieldNames = new string[] { "SHOP_CODE", "GOODS_CODE", "TSVQ", "FROM_DATE", "TO_DATE", "ISERROR", "NOTE" };

            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvIMEI);
           
                FormatGrid();
            
            grdData.DataSource = dtbSource;
            ValidateData(ref dtbSource);
            btnUpdate.Enabled = dtbSource.Rows.Count > 0;
        }

        private string findShopIdByCode(string shopCode)
        {
            if (dtbShopResource == null || dtbShopResource.Rows.Count == 0)
            {
                return "";
            }
            for (int i = 0; i < dtbShopResource.Rows.Count; i++)
            {
                if (dtbShopResource.Rows[i]["shop_code"].ToString().Equals(shopCode))
                {
                    return dtbShopResource.Rows[i]["stock_id"].ToString();
                }
            }
            return "";
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Chương trình sẽ chỉ lấy những dữ liệu hợp lệ. Bạn có muốn tiếp tục không?", "Thống báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                bolIsUpdate = true;
            }
            Close();
        }

        private void grvIMEI_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {

        }

        private void grvIMEI_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView View = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            bool bolIsError = Convert.ToBoolean(View.GetRowCellValue(e.RowHandle, View.Columns["ISERROR"]));
            if (bolIsError)
                e.Appearance.BackColor = Color.Pink;
            //else if (category == "N/A")
            //    e.Appearance.BackColor = Color.Pink;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        #region Method

        private bool FormatGrid()
        {
            try
            {
                grvIMEI.Columns["SHOP_CODE"].Caption = "MÃ SIÊU THỊ";
                grvIMEI.Columns["GOODS_CODE"].Caption = "MÃ GHÉP";
                grvIMEI.Columns["TSVQ"].Caption = "TSVQ";
                grvIMEI.Columns["FROM_DATE"].Caption = "TỪ NGÀY";
                grvIMEI.Columns["TO_DATE"].Caption = "ĐẾN NGÀY";
                grvIMEI.Columns["ISERROR"].Caption = "Lỗi";
                grvIMEI.Columns["NOTE"].Caption = "Ghi lỗi";

                grvIMEI.OptionsView.ColumnAutoWidth = false;
                grvIMEI.Columns["SHOP_CODE"].Width = 150;
                grvIMEI.Columns["GOODS_CODE"].Width = 230;
                grvIMEI.Columns["TSVQ"].Width = 100;
                grvIMEI.Columns["FROM_DATE"].Width = 180;
                grvIMEI.Columns["TO_DATE"].Width = 180;
                grvIMEI.Columns["ISERROR"].Width = 80;
                grvIMEI.Columns["NOTE"].Width = 500;

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsError = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsError.ValueChecked = true;
                chkCheckBoxIsError.ValueUnchecked = false;
                grvIMEI.Columns["ISERROR"].ColumnEdit = chkCheckBoxIsError;

                grvIMEI.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grvIMEI.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grvIMEI.OptionsView.ShowAutoFilterRow = true;
                grvIMEI.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grvIMEI.OptionsView.ShowFooter = true;
                grvIMEI.OptionsView.ShowGroupPanel = false;
                grvIMEI.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grvIMEI.ColumnPanelRowHeight = 40;

                grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "ISERROR";
                grvIMEI.Columns["NOTE"].SummaryItem.DisplayFormat = "{0:n0}";
                grvIMEI.Columns["NOTE"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;

               //grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "NOTE";
                grvIMEI.Columns["ISERROR"].SummaryItem.DisplayFormat = "Tổng số dòng:";
                grvIMEI.Columns["ISERROR"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới danh sách");
                SystemErrorWS.Insert("Lỗi định dạng lưới danh sách", objExce.ToString(), this.Name + " -> FormatGridIMEI", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private bool ValidateData(ref DataTable dtbSource)
        {
            dtbSource.Rows.RemoveAt(0);
            int i = 0;
            dtbSource.Columns[i++].ColumnName = "SHOP_CODE";
            dtbSource.Columns[i++].ColumnName = "GOODS_CODE";
            dtbSource.Columns[i++].ColumnName = "TSVQ";
            dtbSource.Columns[i++].ColumnName = "FROM_DATE";
            dtbSource.Columns[i++].ColumnName = "TO_DATE";

            DataColumn dtcIsError = new DataColumn("ISERROR", typeof(bool));
            dtcIsError.DefaultValue = false;
            dtbSource.Columns.Add(dtcIsError);
            DataColumn dtcNote = new DataColumn("NOTE", typeof(string));
            dtbSource.Columns.Add(dtcNote);
            DataColumn dtcStockId = new DataColumn("STOCK_ID", typeof(string));
            dtbSource.Columns.Add(dtcStockId);
            dtbSource.Columns["STOCK_ID"].ColumnMapping = MappingType.Hidden;
            dtbSource.Columns["GOODS_CODE"].SetOrdinal(dtbSource.Columns["GOODS_CODE"].Ordinal + 1);

            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = new MasterData.PLC.MD.WSProduct.Product();
            lsImport = new List<object[]>();
            foreach (DataRow row in dtbSource.Rows)
            {
                validateRowImport(row);
            }
            dtbSource.AcceptChanges();
            return true;
        }

        private void validateRowImport(DataRow row)
        {
            string errNote = "";
            string stockId = findShopIdByCode(row["SHOP_CODE"].ToString());
            long value;
            DateTime tempFromDate;
            DateTime tempToDate;
            if (stockId == null || stockId == "")
            {
                errNote += "Không đúng mã ST " + row["SHOP_CODE"].ToString() + " ; ";
            }
            if (row["TSVQ"].ToString().Equals(null)
                || row["TSVQ"].ToString().Equals("")
                || !long.TryParse(row["TSVQ"].ToString(), out value))
            {
                errNote += "Không đúng định dạng số " + row["TSVQ"].ToString() + " ; ";
            }
            if (!DateTime.TryParseExact(row["FROM_DATE"].ToString(),"dd/MM/yyyy",
                System.Globalization.CultureInfo.InvariantCulture,
                System.Globalization.DateTimeStyles.None, out tempFromDate))
            {
                errNote = errNote + " Định dạng từ ngày không đúng định dạng Ngày/tháng/năm : " + row["FROM_DATE"].ToString() + "; ";			
            }
            if (DateTime.TryParseExact(row["TO_DATE"].ToString(), "dd/MM/yyyy",
                System.Globalization.CultureInfo.InvariantCulture,
                System.Globalization.DateTimeStyles.None, out tempToDate))
            {
                errNote = errNote + " Định dạng tới ngày không đúng định dạng Ngày/tháng/năm : " + row["TO_DATE"].ToString() + "; ";
            }
            if (row["FROM_DATE"] != null && row["TO_DATE"] != null
                && !errNote.Contains(row["FROM_DATE"].ToString()) && !errNote.Contains(row["TO_DATE"].ToString())
                && tempFromDate.CompareTo(tempToDate) >= 1)
            {

                errNote = errNote + " Giá trị từ ngày phải nhỏ hơn đến ngày ;";
            }

            if (!errNote.Equals(""))
            {
                row["IsError"] = true;
                row["NOTE"] = errNote;
            }
            else
            {
                row["STOCK_ID"] = stockId;
                string user_shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
                string user_id = Library.AppCore.SystemConfig.objSessionUser.UserName;
                object[] objKeywordsImport = new object[]{
                            "@from_date", row["FROM_DATE"].ToString() == null ? "":row["FROM_DATE"].ToString(),
                            "@to_date", row["TO_DATE"].ToString() == null ? "":row["TO_DATE"].ToString(),
                            "@goods_id", row["GOODS_CODE"].ToString() == null ? "":row["GOODS_CODE"].ToString(),
                            "@tsvq",row["TSVQ"].ToString() == null ? "":row["TSVQ"].ToString(),
                            "@user_id",user_id,
                            "@shop_id",stockId,
                            "@user_shop_id",user_shop_id,
                        };
                lsImport.Add(objKeywordsImport);
            }
            
               
        }

        #endregion
    }

}
