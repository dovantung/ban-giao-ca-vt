﻿namespace ERP.Inventory.DUI.Goods
{
    partial class frmGoodsDividingList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gridControlShop = new DevExpress.XtraGrid.GridControl();
            this.gridViewShop = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colShopCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShopName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStockId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreateDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDivisionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRowCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repIsDeleted = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repIsFinished = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnExitCreate = new System.Windows.Forms.Button();
            this.btnExitSys = new System.Windows.Forms.Button();
            this.btnDellNC = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnAll = new System.Windows.Forms.Button();
            this.contextMenuAll = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripAll = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMB = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMN = new System.Windows.Forms.ToolStripMenuItem();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDeleteAll = new System.Windows.Forms.Button();
            this.btnDeleteShop = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.cboShop = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.gridControlDevideItem = new DevExpress.XtraGrid.GridControl();
            this.gridViewDevideItem = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCoupleGoods = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShopCo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShopnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDemoGoods = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSLG30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemain = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHcl = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDmtb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTsvq = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colParentShop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCenterName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDmsp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.testThanhPT = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button9 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnFilter = new System.Windows.Forms.Button();
            this.btnDeleteGoodsDivisionItem = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cboGoodsGroup = new Library.AppControl.ComboBoxControl.ComboBoxCustomString();
            this.cboProductType = new Library.AppControl.ComboBoxControl.ComboBoxCustomString();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTenLT = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnImportDevidingItem = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlShop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewShop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsDeleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsFinished)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.contextMenuAll.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDevideItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDevideItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testThanhPT)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Size = new System.Drawing.Size(1208, 515);
            this.splitContainer1.SplitterDistance = 420;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(420, 515);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Các lần tính nhu cầu";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.gridControlShop, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.grdData, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(414, 496);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // gridControlShop
            // 
            this.gridControlShop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlShop.Location = new System.Drawing.Point(3, 318);
            this.gridControlShop.MainView = this.gridViewShop;
            this.gridControlShop.Name = "gridControlShop";
            this.gridControlShop.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEdit1});
            this.gridControlShop.Size = new System.Drawing.Size(408, 175);
            this.gridControlShop.TabIndex = 15;
            this.gridControlShop.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewShop});
            // 
            // gridViewShop
            // 
            this.gridViewShop.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewShop.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewShop.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridViewShop.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewShop.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewShop.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewShop.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewShop.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewShop.Appearance.Preview.Options.UseFont = true;
            this.gridViewShop.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewShop.Appearance.Row.Options.UseFont = true;
            this.gridViewShop.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colShopCode,
            this.colShopName,
            this.colStockId});
            this.gridViewShop.GridControl = this.gridControlShop;
            this.gridViewShop.Name = "gridViewShop";
            this.gridViewShop.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridViewShop.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridViewShop.OptionsNavigation.UseTabKey = false;
            this.gridViewShop.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewShop.OptionsView.ColumnAutoWidth = false;
            this.gridViewShop.OptionsView.ShowAutoFilterRow = true;
            this.gridViewShop.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewShop.OptionsView.ShowFooter = true;
            this.gridViewShop.OptionsView.ShowGroupPanel = false;
            // 
            // colShopCode
            // 
            this.colShopCode.AppearanceCell.Options.UseTextOptions = true;
            this.colShopCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colShopCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colShopCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colShopCode.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colShopCode.Caption = "Mã siêu thị";
            this.colShopCode.FieldName = "SHOP_CODE";
            this.colShopCode.Name = "colShopCode";
            this.colShopCode.OptionsColumn.AllowEdit = false;
            this.colShopCode.OptionsColumn.AllowMove = false;
            this.colShopCode.OptionsColumn.ReadOnly = true;
            this.colShopCode.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colShopCode.Visible = true;
            this.colShopCode.VisibleIndex = 0;
            this.colShopCode.Width = 116;
            // 
            // colShopName
            // 
            this.colShopName.Caption = "Tên siêu thị";
            this.colShopName.FieldName = "NAME";
            this.colShopName.MinWidth = 10;
            this.colShopName.Name = "colShopName";
            this.colShopName.OptionsColumn.AllowEdit = false;
            this.colShopName.Visible = true;
            this.colShopName.VisibleIndex = 1;
            this.colShopName.Width = 306;
            // 
            // colStockId
            // 
            this.colStockId.Caption = "Id Siêu thị";
            this.colStockId.FieldName = "STOCK_ID";
            this.colStockId.Name = "colStockId";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit1.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit2.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit3.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 499;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // grdData
            // 
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(3, 3);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repIsDeleted,
            this.repIsFinished,
            this.repositoryItemCheckEdit8,
            this.repNote});
            this.grdData.Size = new System.Drawing.Size(408, 169);
            this.grdData.TabIndex = 12;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            this.grdData.Click += new System.EventHandler(this.grdData_Click_1);
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Preview.Options.UseFont = true;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colCreateDateTime,
            this.colDivisionStatus,
            this.colRowCount,
            this.colId});
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsView.AllowHtmlDrawHeaders = true;
            this.grdViewData.OptionsView.ColumnAutoWidth = false;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewData.OptionsView.ShowFooter = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            this.grdViewData.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.grdData_CustomColumnDisplayText);
            // 
            // colName
            // 
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colName.Caption = "Tên";
            this.colName.FieldName = "NAME";
            this.colName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.AllowMove = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 100;
            // 
            // colCreateDateTime
            // 
            this.colCreateDateTime.Caption = "Thời gian tạo";
            this.colCreateDateTime.FieldName = "CREATE_DATETIME";
            this.colCreateDateTime.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCreateDateTime.MinWidth = 10;
            this.colCreateDateTime.Name = "colCreateDateTime";
            this.colCreateDateTime.OptionsColumn.AllowEdit = false;
            this.colCreateDateTime.Visible = true;
            this.colCreateDateTime.VisibleIndex = 1;
            this.colCreateDateTime.Width = 140;
            // 
            // colDivisionStatus
            // 
            this.colDivisionStatus.Caption = "Trạng thái";
            this.colDivisionStatus.FieldName = "DIVISION_STATUS";
            this.colDivisionStatus.Name = "colDivisionStatus";
            this.colDivisionStatus.OptionsColumn.AllowEdit = false;
            this.colDivisionStatus.Visible = true;
            this.colDivisionStatus.VisibleIndex = 2;
            this.colDivisionStatus.Width = 97;
            // 
            // colRowCount
            // 
            this.colRowCount.Caption = "Số dòng";
            this.colRowCount.FieldName = "ROW_COUNT";
            this.colRowCount.Name = "colRowCount";
            this.colRowCount.OptionsColumn.AllowEdit = false;
            this.colRowCount.Visible = true;
            this.colRowCount.VisibleIndex = 3;
            this.colRowCount.Width = 73;
            // 
            // colId
            // 
            this.colId.Caption = "ID";
            this.colId.FieldName = "GOODS_DIVISION_HISTORY_ID";
            this.colId.Name = "colId";
            // 
            // repIsDeleted
            // 
            this.repIsDeleted.AutoHeight = false;
            this.repIsDeleted.Name = "repIsDeleted";
            this.repIsDeleted.ValueChecked = ((short)(1));
            this.repIsDeleted.ValueUnchecked = ((short)(0));
            // 
            // repIsFinished
            // 
            this.repIsFinished.AutoHeight = false;
            this.repIsFinished.Name = "repIsFinished";
            this.repIsFinished.ValueChecked = ((short)(1));
            this.repIsFinished.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            this.repositoryItemCheckEdit8.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit8.ValueUnchecked = ((short)(0));
            // 
            // repNote
            // 
            this.repNote.AutoHeight = false;
            this.repNote.MaxLength = 499;
            this.repNote.Name = "repNote";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnExitCreate);
            this.panel1.Controls.Add(this.btnExitSys);
            this.panel1.Controls.Add(this.btnDellNC);
            this.panel1.Controls.Add(this.btnCreate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 178);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(408, 49);
            this.panel1.TabIndex = 13;
            // 
            // btnExitCreate
            // 
            this.btnExitCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnExitCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExitCreate.Location = new System.Drawing.Point(153, 8);
            this.btnExitCreate.Name = "btnExitCreate";
            this.btnExitCreate.Size = new System.Drawing.Size(74, 27);
            this.btnExitCreate.TabIndex = 3;
            this.btnExitCreate.Text = "Thoát";
            this.btnExitCreate.UseVisualStyleBackColor = true;
            this.btnExitCreate.Visible = false;
            this.btnExitCreate.Click += new System.EventHandler(this.btnExitCreate_Click);
            // 
            // btnExitSys
            // 
            this.btnExitSys.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnExitSys.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExitSys.Location = new System.Drawing.Point(252, 8);
            this.btnExitSys.Name = "btnExitSys";
            this.btnExitSys.Size = new System.Drawing.Size(68, 27);
            this.btnExitSys.TabIndex = 2;
            this.btnExitSys.Text = "Thoát";
            this.btnExitSys.UseVisualStyleBackColor = true;
            this.btnExitSys.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // btnDellNC
            // 
            this.btnDellNC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnDellNC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDellNC.Location = new System.Drawing.Point(152, 8);
            this.btnDellNC.Name = "btnDellNC";
            this.btnDellNC.Size = new System.Drawing.Size(74, 27);
            this.btnDellNC.TabIndex = 1;
            this.btnDellNC.Text = "Xóa";
            this.btnDellNC.UseVisualStyleBackColor = true;
            this.btnDellNC.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreate.Location = new System.Drawing.Point(58, 8);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(68, 27);
            this.btnCreate.TabIndex = 0;
            this.btnCreate.Text = "Tạo mới";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnAll);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.btnDeleteAll);
            this.panel2.Controls.Add(this.btnDeleteShop);
            this.panel2.Controls.Add(this.btnAdd);
            this.panel2.Controls.Add(this.btnImport);
            this.panel2.Controls.Add(this.cboShop);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 233);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(408, 79);
            this.panel2.TabIndex = 14;
            // 
            // btnAll
            // 
            this.btnAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnAll.ContextMenuStrip = this.contextMenuAll;
            this.btnAll.Enabled = false;
            this.btnAll.Location = new System.Drawing.Point(81, 39);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(75, 31);
            this.btnAll.TabIndex = 6;
            this.btnAll.Text = "Tất cả";
            this.btnAll.UseVisualStyleBackColor = true;
            // 
            // contextMenuAll
            // 
            this.contextMenuAll.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripAll,
            this.testToolStripMB,
            this.toolStripMN});
            this.contextMenuAll.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.contextMenuAll.Name = "contextMenuAll";
            this.contextMenuAll.Size = new System.Drawing.Size(173, 70);
            // 
            // toolStripAll
            // 
            this.toolStripAll.Name = "toolStripAll";
            this.toolStripAll.Size = new System.Drawing.Size(172, 22);
            this.toolStripAll.Text = "Tất cả";
            this.toolStripAll.Click += new System.EventHandler(this.toolStripAll_Click);
            // 
            // testToolStripMB
            // 
            this.testToolStripMB.Name = "testToolStripMB";
            this.testToolStripMB.Size = new System.Drawing.Size(172, 22);
            this.testToolStripMB.Text = "Các siêu thị VTMB";
            this.testToolStripMB.Click += new System.EventHandler(this.testToolStripMB_Click);
            // 
            // toolStripMN
            // 
            this.toolStripMN.Name = "toolStripMN";
            this.toolStripMN.Size = new System.Drawing.Size(172, 22);
            this.toolStripMN.Text = "Các siêu thị VTMN";
            this.toolStripMN.Click += new System.EventHandler(this.toolStripMN_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Siêu thị";
            // 
            // btnDeleteAll
            // 
            this.btnDeleteAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnDeleteAll.Enabled = false;
            this.btnDeleteAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteAll.Location = new System.Drawing.Point(320, 39);
            this.btnDeleteAll.Name = "btnDeleteAll";
            this.btnDeleteAll.Size = new System.Drawing.Size(74, 31);
            this.btnDeleteAll.TabIndex = 4;
            this.btnDeleteAll.Text = "Xóa tất cả";
            this.btnDeleteAll.UseVisualStyleBackColor = true;
            this.btnDeleteAll.Click += new System.EventHandler(this.button8_Click);
            // 
            // btnDeleteShop
            // 
            this.btnDeleteShop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnDeleteShop.Enabled = false;
            this.btnDeleteShop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteShop.Location = new System.Drawing.Point(238, 39);
            this.btnDeleteShop.Name = "btnDeleteShop";
            this.btnDeleteShop.Size = new System.Drawing.Size(68, 31);
            this.btnDeleteShop.TabIndex = 3;
            this.btnDeleteShop.Text = "Xóa";
            this.btnDeleteShop.UseVisualStyleBackColor = true;
            this.btnDeleteShop.Click += new System.EventHandler(this.button7_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnAdd.Enabled = false;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(161, 39);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(68, 32);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnImport
            // 
            this.btnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnImport.Enabled = false;
            this.btnImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImport.Location = new System.Drawing.Point(6, 39);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(68, 31);
            this.btnImport.TabIndex = 0;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // cboShop
            // 
            this.cboShop.Enabled = false;
            this.cboShop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboShop.Location = new System.Drawing.Point(78, 6);
            this.cboShop.Margin = new System.Windows.Forms.Padding(0);
            this.cboShop.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboShop.Name = "cboShop";
            this.cboShop.Size = new System.Drawing.Size(303, 24);
            this.cboShop.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel5);
            this.groupBox2.Controls.Add(this.panel4);
            this.groupBox2.Controls.Add(this.panel3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox2.Size = new System.Drawing.Size(784, 515);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin lần tạo";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.gridControlDevideItem);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 158);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(778, 324);
            this.panel5.TabIndex = 4;
            // 
            // gridControlDevideItem
            // 
            this.gridControlDevideItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDevideItem.Location = new System.Drawing.Point(0, 0);
            this.gridControlDevideItem.MainView = this.gridViewDevideItem;
            this.gridControlDevideItem.Name = "gridControlDevideItem";
            this.gridControlDevideItem.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4,
            this.repositoryItemCheckEdit5,
            this.repositoryItemCheckEdit6,
            this.repositoryItemTextEdit2,
            this.repositoryItemCheckEdit7,
            this.testThanhPT});
            this.gridControlDevideItem.Size = new System.Drawing.Size(778, 324);
            this.gridControlDevideItem.TabIndex = 12;
            this.gridControlDevideItem.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDevideItem});
            // 
            // gridViewDevideItem
            // 
            this.gridViewDevideItem.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewDevideItem.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewDevideItem.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridViewDevideItem.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewDevideItem.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewDevideItem.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewDevideItem.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewDevideItem.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewDevideItem.Appearance.Preview.Options.UseFont = true;
            this.gridViewDevideItem.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewDevideItem.Appearance.Row.Options.UseFont = true;
            this.gridViewDevideItem.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCoupleGoods,
            this.colShopCo,
            this.colShopnName,
            this.colDemoGoods,
            this.colSLG30,
            this.colRemain,
            this.colHcl,
            this.gridColumn12,
            this.colDmtb,
            this.colTsvq,
            this.colDm,
            this.colNc,
            this.colParentShop,
            this.colCenterName,
            this.colDmsp});
            this.gridViewDevideItem.GridControl = this.gridControlDevideItem;
            this.gridViewDevideItem.Name = "gridViewDevideItem";
            this.gridViewDevideItem.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridViewDevideItem.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridViewDevideItem.OptionsNavigation.UseTabKey = false;
            this.gridViewDevideItem.OptionsSelection.InvertSelection = true;
            this.gridViewDevideItem.OptionsSelection.MultiSelect = true;
            this.gridViewDevideItem.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewDevideItem.OptionsView.ColumnAutoWidth = false;
            this.gridViewDevideItem.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDevideItem.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewDevideItem.OptionsView.ShowFooter = true;
            this.gridViewDevideItem.OptionsView.ShowGroupPanel = false;
            // 
            // colCoupleGoods
            // 
            this.colCoupleGoods.AppearanceCell.Options.UseTextOptions = true;
            this.colCoupleGoods.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colCoupleGoods.AppearanceHeader.Options.UseTextOptions = true;
            this.colCoupleGoods.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCoupleGoods.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCoupleGoods.Caption = "Mã ghép";
            this.colCoupleGoods.FieldName = "COUPLE_GOODS";
            this.colCoupleGoods.Name = "colCoupleGoods";
            this.colCoupleGoods.OptionsColumn.AllowEdit = false;
            this.colCoupleGoods.OptionsColumn.AllowMove = false;
            this.colCoupleGoods.OptionsColumn.ReadOnly = true;
            this.colCoupleGoods.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCoupleGoods.Visible = true;
            this.colCoupleGoods.VisibleIndex = 0;
            this.colCoupleGoods.Width = 100;
            // 
            // colShopCo
            // 
            this.colShopCo.Caption = "Mã siêu thị";
            this.colShopCo.FieldName = "SHOP_CODE";
            this.colShopCo.MinWidth = 10;
            this.colShopCo.Name = "colShopCo";
            this.colShopCo.OptionsColumn.AllowEdit = false;
            this.colShopCo.Visible = true;
            this.colShopCo.VisibleIndex = 1;
            this.colShopCo.Width = 103;
            // 
            // colShopnName
            // 
            this.colShopnName.Caption = "Siêu thị";
            this.colShopnName.FieldName = "SHOP_NAME";
            this.colShopnName.Name = "colShopnName";
            this.colShopnName.OptionsColumn.AllowEdit = false;
            this.colShopnName.Visible = true;
            this.colShopnName.VisibleIndex = 2;
            this.colShopnName.Width = 178;
            // 
            // colDemoGoods
            // 
            this.colDemoGoods.Caption = "Bàn trải nghiệm";
            this.colDemoGoods.FieldName = "DEMO_GOODS";
            this.colDemoGoods.Name = "colDemoGoods";
            this.colDemoGoods.OptionsColumn.AllowEdit = false;
            this.colDemoGoods.Width = 117;
            // 
            // colSLG30
            // 
            this.colSLG30.Caption = "Sản lượng 30 ngày";
            this.colSLG30.FieldName = "SLG30";
            this.colSLG30.Name = "colSLG30";
            this.colSLG30.OptionsColumn.AllowEdit = false;
            this.colSLG30.Visible = true;
            this.colSLG30.VisibleIndex = 3;
            // 
            // colRemain
            // 
            this.colRemain.Caption = "Tồn";
            this.colRemain.FieldName = "REMAIN";
            this.colRemain.Name = "colRemain";
            this.colRemain.OptionsColumn.AllowEdit = false;
            this.colRemain.Visible = true;
            this.colRemain.VisibleIndex = 4;
            // 
            // colHcl
            // 
            this.colHcl.Caption = "Hàng chủ lực";
            this.colHcl.FieldName = "HCL";
            this.colHcl.Name = "colHcl";
            this.colHcl.OptionsColumn.AllowEdit = false;
            this.colHcl.Visible = true;
            this.colHcl.VisibleIndex = 5;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Hàng quá 30 ngày";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 6;
            // 
            // colDmtb
            // 
            this.colDmtb.Caption = "ĐMTB";
            this.colDmtb.FieldName = "DMTB";
            this.colDmtb.Name = "colDmtb";
            this.colDmtb.OptionsColumn.AllowEdit = false;
            this.colDmtb.Visible = true;
            this.colDmtb.VisibleIndex = 7;
            // 
            // colTsvq
            // 
            this.colTsvq.Caption = "Tham số VQ";
            this.colTsvq.FieldName = "TSVQ";
            this.colTsvq.Name = "colTsvq";
            this.colTsvq.OptionsColumn.AllowEdit = false;
            this.colTsvq.Visible = true;
            this.colTsvq.VisibleIndex = 8;
            // 
            // colDm
            // 
            this.colDm.Caption = "Định mức";
            this.colDm.FieldName = "DM";
            this.colDm.Name = "colDm";
            this.colDm.OptionsColumn.AllowEdit = false;
            this.colDm.Visible = true;
            this.colDm.VisibleIndex = 9;
            // 
            // colNc
            // 
            this.colNc.Caption = "Nhu cầu";
            this.colNc.FieldName = "NC";
            this.colNc.Name = "colNc";
            this.colNc.OptionsColumn.AllowEdit = false;
            this.colNc.Visible = true;
            this.colNc.VisibleIndex = 10;
            // 
            // colParentShop
            // 
            this.colParentShop.Caption = "Kho tổng";
            this.colParentShop.FieldName = "PARENT_SHOP";
            this.colParentShop.Name = "colParentShop";
            this.colParentShop.OptionsColumn.AllowEdit = false;
            this.colParentShop.Visible = true;
            this.colParentShop.VisibleIndex = 11;
            // 
            // colCenterName
            // 
            this.colCenterName.Caption = "Khu vực";
            this.colCenterName.FieldName = "CENTER_NAME";
            this.colCenterName.Name = "colCenterName";
            this.colCenterName.OptionsColumn.AllowEdit = false;
            this.colCenterName.Visible = true;
            this.colCenterName.VisibleIndex = 12;
            this.colCenterName.Width = 162;
            // 
            // colDmsp
            // 
            this.colDmsp.Caption = "Danh mục";
            this.colDmsp.FieldName = "DMSP";
            this.colDmsp.Name = "colDmsp";
            this.colDmsp.OptionsColumn.AllowEdit = false;
            this.colDmsp.Visible = true;
            this.colDmsp.VisibleIndex = 13;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit4.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit5.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit6.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.MaxLength = 499;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            // 
            // testThanhPT
            // 
            this.testThanhPT.AutoHeight = false;
            this.testThanhPT.Name = "testThanhPT";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button9);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(3, 482);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(778, 30);
            this.panel4.TabIndex = 3;
            // 
            // button9
            // 
            this.button9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(326, 4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(112, 23);
            this.button9.TabIndex = 0;
            this.button9.Text = "Lấy File import mẫu";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.tableLayoutPanel2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 18);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(778, 140);
            this.panel3.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnFilter);
            this.panel6.Controls.Add(this.btnDeleteGoodsDivisionItem);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 93);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(778, 47);
            this.panel6.TabIndex = 1;
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(90, 11);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(103, 24);
            this.btnFilter.TabIndex = 1;
            this.btnFilter.Text = "Lọc thông tin";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // btnDeleteGoodsDivisionItem
            // 
            this.btnDeleteGoodsDivisionItem.Location = new System.Drawing.Point(6, 11);
            this.btnDeleteGoodsDivisionItem.Name = "btnDeleteGoodsDivisionItem";
            this.btnDeleteGoodsDivisionItem.Size = new System.Drawing.Size(78, 24);
            this.btnDeleteGoodsDivisionItem.TabIndex = 0;
            this.btnDeleteGoodsDivisionItem.Text = "Xóa";
            this.btnDeleteGoodsDivisionItem.UseVisualStyleBackColor = true;
            this.btnDeleteGoodsDivisionItem.Click += new System.EventHandler(this.btnDeleteGoodsDivisionItem_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.76871F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.42177F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.63946F));
            this.tableLayoutPanel2.Controls.Add(this.cboGoodsGroup, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.cboProductType, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtTenLT, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnSave, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnImportDevidingItem, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnExport, 2, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(778, 87);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // cboGoodsGroup
            // 
            this.cboGoodsGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboGoodsGroup.Enabled = false;
            this.cboGoodsGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGoodsGroup.Location = new System.Drawing.Point(169, 58);
            this.cboGoodsGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboGoodsGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboGoodsGroup.Name = "cboGoodsGroup";
            this.cboGoodsGroup.Padding = new System.Windows.Forms.Padding(3);
            this.cboGoodsGroup.Size = new System.Drawing.Size(424, 29);
            this.cboGoodsGroup.TabIndex = 8;
            // 
            // cboProductType
            // 
            this.cboProductType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboProductType.Enabled = false;
            this.cboProductType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductType.Location = new System.Drawing.Point(169, 29);
            this.cboProductType.Margin = new System.Windows.Forms.Padding(0);
            this.cboProductType.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboProductType.Name = "cboProductType";
            this.cboProductType.Padding = new System.Windows.Forms.Padding(3);
            this.cboProductType.Size = new System.Drawing.Size(424, 29);
            this.cboProductType.TabIndex = 7;
            this.cboProductType.SelectionChangeCommitted += new System.EventHandler(this.cbbType_SelectionChangeCommitted);
            this.cboProductType.Load += new System.EventHandler(this.cbbType_Load);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên lần tính";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTenLT
            // 
            this.txtTenLT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTenLT.Enabled = false;
            this.txtTenLT.Location = new System.Drawing.Point(172, 3);
            this.txtTenLT.Name = "txtTenLT";
            this.txtTenLT.Size = new System.Drawing.Size(418, 22);
            this.txtTenLT.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(163, 29);
            this.label2.TabIndex = 2;
            this.label2.Text = "Chọn nhóm hàng";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(163, 29);
            this.label3.TabIndex = 3;
            this.label3.Text = "Ngành hàng";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(596, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(179, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Khởi tạo danh sách";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnImportDevidingItem
            // 
            this.btnImportDevidingItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnImportDevidingItem.Location = new System.Drawing.Point(596, 32);
            this.btnImportDevidingItem.Name = "btnImportDevidingItem";
            this.btnImportDevidingItem.Size = new System.Drawing.Size(179, 23);
            this.btnImportDevidingItem.TabIndex = 5;
            this.btnImportDevidingItem.Text = "Import";
            this.btnImportDevidingItem.UseVisualStyleBackColor = true;
            this.btnImportDevidingItem.Click += new System.EventHandler(this.btnImportDevidingItem_Click);
            // 
            // btnExport
            // 
            this.btnExport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnExport.Location = new System.Drawing.Point(596, 61);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(179, 23);
            this.btnExport.TabIndex = 6;
            this.btnExport.Text = "Kết xuất";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // frmGoodsDividingList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1208, 515);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frmGoodsDividingList";
            this.Text = "Tính nhu cầu chia hàng serial";
            this.Load += new System.EventHandler(this.frmGoodsDividingList_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlShop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewShop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsDeleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsFinished)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.contextMenuAll.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDevideItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDevideItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testThanhPT)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        private void grdData_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "DIVISION_STATUS")
            {
                string strStatus = (e.Value ?? "").ToString().Trim();
                //|| string.IsNullOrEmpty(strStatus)
                if (strStatus == "0")
                {
                    e.DisplayText = "Chưa chia";
                    return;
                }
                else if (strStatus == "1")
                {
                    e.DisplayText = "Đã chia";
                    return;
                }
            }
        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colCreateDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repIsDeleted;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repIsFinished;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repNote;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnExitSys;
        private System.Windows.Forms.Button btnDellNC;
        private System.Windows.Forms.Button btnCreate;
        private DevExpress.XtraGrid.GridControl gridControlShop;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewShop;
        private DevExpress.XtraGrid.Columns.GridColumn colShopCode;
        private DevExpress.XtraGrid.Columns.GridColumn colShopName;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnDeleteAll;
        private System.Windows.Forms.Button btnDeleteShop;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button9;
        private DevExpress.XtraGrid.GridControl gridControlDevideItem;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDevideItem;
        private DevExpress.XtraGrid.Columns.GridColumn colCoupleGoods;
        private DevExpress.XtraGrid.Columns.GridColumn colShopCo;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTenLT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnImportDevidingItem;
        private System.Windows.Forms.Button btnExport;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomString cboProductType;

        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboShop;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomString cboGoodsGroup;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.Button btnDeleteGoodsDivisionItem;
        private DevExpress.XtraGrid.Columns.GridColumn colDivisionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colRowCount;
        private DevExpress.XtraGrid.Columns.GridColumn colShopnName;
        private DevExpress.XtraGrid.Columns.GridColumn colDemoGoods;
        private DevExpress.XtraGrid.Columns.GridColumn colSLG30;
        private DevExpress.XtraGrid.Columns.GridColumn colRemain;
        private DevExpress.XtraGrid.Columns.GridColumn colHcl;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn colDmtb;
        private DevExpress.XtraGrid.Columns.GridColumn colTsvq;
        private DevExpress.XtraGrid.Columns.GridColumn colDm;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colStockId;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraGrid.Columns.GridColumn colNc;
        private DevExpress.XtraGrid.Columns.GridColumn colParentShop;
        private DevExpress.XtraGrid.Columns.GridColumn colCenterName;
        private DevExpress.XtraGrid.Columns.GridColumn colDmsp;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit testThanhPT;
        private System.Windows.Forms.Button btnExitCreate;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.ContextMenuStrip contextMenuAll;
        private System.Windows.Forms.ToolStripMenuItem toolStripAll;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMB;
        private System.Windows.Forms.ToolStripMenuItem toolStripMN;
    }
}