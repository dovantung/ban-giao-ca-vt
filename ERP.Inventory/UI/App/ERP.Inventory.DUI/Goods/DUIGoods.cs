﻿using DevExpress.XtraGrid.Views.Base;
using GemBox.Spreadsheet;
using Library.AppCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Goods
{
    public partial class DUIGoods : Form
    {
        private DataTable dtbResourceDividing = null;
        private DataTable dtbResourceDivideCategory = null;

        private DataTable dtbResourceShop = null;
        private DataTable dtbResourceCoupleGoods = null;


        private PLC.Goods.PLCGoods goods = new PLC.Goods.PLCGoods();
        private bool bolIsLoadComplete = false;

        public DUIGoods()
        {
            InitializeComponent();
            SearchDataDividing();
            cboPage.SelectedIndex = 0;

            btnApply.Enabled = false;
            btnCreateList.Enabled = false;
            btnCancel.Visible = false;
        }

        private void SearchDataDividing()
        {
            object[] objKeywords = new object[] { "@TYPE", "3" };
            dtbResourceDividing = goods.SearchDataDividing(objKeywords);
            //dtbResourceStore = Library.AppCore.DataSource.GetDataSource.GetStore();
            tblDividingList.DataSource = dtbResourceDividing;
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
        }

        private void grdDividingList_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "DIVISION_STATUS")
            {
                //string IMEI = (grdDividingList.GetRowCellValue(e.RowHandle, "DIVISION_STATUS") ?? "").ToString();
                string strStatus = (e.Value ?? "").ToString().Trim();
                if (strStatus == "0" || string.IsNullOrEmpty(strStatus))
                {
                    e.DisplayText = "Chưa áp dụng";
                    return;
                }
                else
                {
                    e.DisplayText = "Áp dụng";
                    return;
                }
            }
        }

        private void tblDividingList_Click(object sender, EventArgs e)
        {
            search();
        }

        public bool search()
        {
            try
            {
                if (grdDividingList.GetFocusedDataRow() == null)
                {
                    return true;
                }

                pnSearchStatus.Visible = true;
                pnSearchStatus.Refresh();

                string goods_division_history_id = "",
                    page_size = "";
                DataRow dtRow = grdDividingList.GetFocusedDataRow();
                goods_division_history_id = dtRow["GOODS_DIVISION_HISTORY_ID"].ToString();
                page_size = cboPage.SelectedItem.ToString();

                object[] objKeywords = new object[]{
                    "@goods_division_history_id", goods_division_history_id,
                    "@goods_group","",
                    "@goods_code", "",
                    "@couple_goods_code","",
                    "@shop_code","",
                    "@center_code","",
                    "@zone","",
                    "@page_size",page_size,
                };
                dtbResourceDivideCategory = goods.SearchDataDivideCategory(objKeywords);
                tblList.DataSource = dtbResourceDivideCategory;
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                }


                bolIsLoadComplete = true;
                return true;
            }
            catch (Exception e)
            {
                bolIsLoadComplete = true;
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                   e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);
                // TODO: handle exception
                return false;
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            clearDetailValue();
            txtName.Enabled = true;
            if (dtbResourceDivideCategory != null)
            {
                dtbResourceDivideCategory.Rows.Clear();
                dtbResourceDivideCategory.AcceptChanges();
            }
            
            btnCreateList.Enabled = true;
            btnCancel.Visible = true;
            btnNew.Visible = false;
            btnRemove.Visible = false;
            btnImport.Enabled = false;
            btnExport.Enabled = false;
            btnDownTemplate.Enabled = false;
            tblDividingList.Enabled = false;
            txtName.Focus();
        }

        ////////////////////////////////////////////////////////////////////////////////
        // * @ Delete du lieu cho cac hop text box
        // * @ since 25/01/2007
        ////////////////////////////////////////////////////////////////////////////////
        private void clearDetailValue()
        {
            txtName.Text = "";

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Visible = false;
            btnNew.Visible = true;
            btnRemove.Visible = true;

            tblDividingList.Enabled = true;
            btnExport.Enabled = true;
            if (btnCreateList.Enabled)
                fillDetailValue();
            btnCreateList.Enabled = false;
            btnImport.Enabled = true;
            btnDownTemplate.Enabled = true;
        }

        private void fillDetailValue()
        {
            try
            {
                DataRow row = grdDividingList.GetFocusedDataRow();
                if (row == null)
                {
                    return;
                }

                string division_status = row["DIVISION_STATUS"].ToString();
                string goods_division_history_id = row["GOODS_DIVISION_HISTORY_ID"].ToString();
                string page_size = cboPage.SelectedItem.ToString();
                if (division_status.Equals("1"))
                {
                    btnApply.Enabled = false;
                }
             
                string goods_group = "", goods_code = "", couple_goods_code = "", shop_code = "", center_code = "", zone = "";
                if (frmDialogGoodsFilter != null && frmDialogGoodsFilter.IsSearch)
                {
                    frmDialogGoodsFilter.getValueSearch(ref goods_group, ref goods_code, ref couple_goods_code, ref shop_code, ref center_code, ref zone);
                }
                object[] objKeywords = new object[]{
                    "@goods_division_history_id", goods_division_history_id,
                    "@goods_group",goods_group,
                    "@goods_code", goods_code,
                    "@couple_goods_code",couple_goods_code,
                    "@shop_code",shop_code,
                    "@center_code",center_code,
                    "@zone",zone,
                    "@page_size",page_size,
                };
                dtbResourceDivideCategory = goods.SearchDataDivideCategory(objKeywords);
                tblList.DataSource = dtbResourceDivideCategory;
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                }

                txtName.Text = row["NAME"].ToString();
            }
            catch (Exception e)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, e.Message,
                  e.Message);
                Console.WriteLine("Exception source: {0}", e.Source);


            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tmrFormatFlex_Tick(object sender, EventArgs e)
        {
            if (bolIsLoadComplete)
            {
                bolIsLoadComplete = false;

                pnSearchStatus.Visible = false;
            }
        }

        private void btnCreateList_Click(object sender, EventArgs e)
        {
            if (txtName.Text == null || txtName.Text.Equals(""))
            {
                MessageBox.Show("Bạn cần nhập tên lần chia hàng", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            pnSearchStatus.Visible = true;
            pnSearchStatus.Refresh();

            string name = txtName.Text;
            string user_id = Library.AppCore.SystemConfig.objSessionUser.UserName;
            string shop_id = Library.AppCore.SystemConfig.objSessionUser.DepartmentID.ToString();
            object[] objKeywords = new object[]{
                    "@type","3",
                    "@name", name,
                    "@user_id",user_id,
                    "@shop_id", shop_id,
                };
            DataTable dtbResourceAdd = goods.InsertGoodsDivisionHistory(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
            if (dtbResourceAdd != null && dtbResourceAdd.Rows.Count > 0)
            {
                DataRow rowClone = dtbResourceDividing.NewRow();
                rowClone.ItemArray = dtbResourceAdd.Rows[0].ItemArray;
                dtbResourceDividing.Rows.InsertAt(rowClone, 0);
                dtbResourceDividing.AcceptChanges();
                grdDividingList.SelectRow(0);
                if (frmDialogGoodsFilter != null)
                {
                    frmDialogGoodsFilter.refreshValue();
                }
               
                tblDividingList_Click(null, null);
                //dtbResourceDividing.ImportRow(dtbResourceAdd.Rows[0]);ư

            }

            btnCreateList.Enabled = false;
            MessageBox.Show("Đã thực hiện thành công", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            //viewDetail();
            btnCancel_Click(null, null);
            bolIsLoadComplete = true;
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow row = grdDividingList.GetFocusedDataRow();
                if (row == null)
                {
                    return;
                }

                if (MessageBox.Show(this, "Bạn có thực sự muốn xóa bản ghi được chọn?", "Thông báo"
                  , MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                  == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }
                pnSearchStatus.Visible = true;
                pnSearchStatus.Refresh();
                string goods_division_history_id = row["GOODS_DIVISION_HISTORY_ID"].ToString();
                object[] objKeywords = new object[]{
                    "@goods_division_history_id", goods_division_history_id,
                };
                goods.DeleteCoupleGood(objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                dtbResourceDividing.Rows.Remove(row);
                dtbResourceDividing.AcceptChanges();
                if (dtbResourceDividing.Rows.Count == 0)
                {
                    clearDetailValue();
                }
                else
                {
                    grdDividingList.SelectRow(0);
                    tblDividingList_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, ex.Message,
                  ex.Message);
                Console.WriteLine("Exception source: {0}", ex.Source);
                txtName.Focus();
            }
            finally
            {
                bolIsLoadComplete = true;
            }

        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            pnSearchStatus.Visible = true;
            pnSearchStatus.Refresh();
            Thread thdSyncRead = new Thread(new ThreadStart(import));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
        }

        private void import()
        {
            try
            {
                DataRow row = grdDividingList.GetFocusedDataRow();
                if (row == null)
                {
                    MessageBox.Show("Bạn chưa chọn lần tạo danh mục", "Thông báo",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                OpenFileDialog Openfile = new OpenFileDialog();
                Openfile.Filter = "Excel 2003|*.xls|Excel New|*.xlsx";
                //Openfile.Filter = "Excel Files|*.xls;*.xlsx";
                //Openfile.Title = "Select a Cursor File";
                //Openfile.InitialDirectory = "C:";
                DialogResult result = Openfile.ShowDialog();
                if (result == DialogResult.Cancel) return;

                DataTable dt = new DataTable();
                dt = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(Openfile.FileName);

                if (dtbResourceShop == null)
                {
                    dtbResourceShop = goods.GetShops();
                }
                if (dtbResourceCoupleGoods == null)
                {
                    dtbResourceCoupleGoods = goods.GetCoupleGoods();
                }

                frmGoodsImportExcel frm = new frmGoodsImportExcel();
                frm.SourceTable = dt;
                frm.CurImportMode = ImportMode.IMPORT;
                frm.Goods_division_history_id = row["GOODS_DIVISION_HISTORY_ID"].ToString();
                frm.DtbShopResource = dtbResourceShop;
                frm.DtbCoupleGoodsResource = dtbResourceCoupleGoods;
                frm.ShowDialog();
                if (frm.IsUpdate)
                {
                    object[][] arrayObject = frm.LsImport.ToArray();

                    goods.ImportByExcel(arrayObject);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                            SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                    MessageBox.Show("Import " + frm.LsImport.Count + " Bản ghi!", "Thông báo",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception objex)
            {
                MessageBox.Show(objex.Message.ToString(), "Lỗi nạp thông tin từ file Excel!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                bolIsLoadComplete = true;
            }
        }

        ERP.Inventory.DUI.Goods.frmDialogGoodsFilter frmDialogGoodsFilter = null;
        private void btnFilter_Click(object sender, EventArgs e)
        {
            DataRow row = grdDividingList.GetFocusedDataRow();
            if (row == null)
            {
                MessageBox.Show("Bạn chưa chọn lần tạo danh mục", "Thông báo",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (frmDialogGoodsFilter == null)
            {
                if (dtbResourceShop == null)
                {
                    dtbResourceShop = goods.GetShops();
                }
                frmDialogGoodsFilter = new Goods.frmDialogGoodsFilter(dtbResourceShop);
                frmDialogGoodsFilter.StartPosition = FormStartPosition.CenterParent;
            }
            frmDialogGoodsFilter.IsSearch = false;
            frmDialogGoodsFilter.ShowDialog();
            if (frmDialogGoodsFilter.IsSearch)
            {
                fillDetailValue();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            Common.frmExportExcel frmExportExcel = new frmExport()
            {
                StrFileTemplateName = "dicide_his_report",
                StrFileTemplatePath = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\dicide_his_report.xlsx",
                DtbResourceSource = dtbResourceDivideCategory
            };
            frmExportExcel.btnExport_Click();
        }

        class frmExport : Common.frmExportExcel
        {

            public override void AddRowDetail(ref ExcelWorksheet sheet, DataRow row, ref int iRow, ref int iSTT)
            {
                InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
                InsertValueCell(ref sheet, "B", iRow, row["COUPLE_GOODS"].ToString(), false, false);
                InsertValueCell(ref sheet, "C", iRow, row["SHOP_CODE"].ToString(), false, false);
                InsertValueCell(ref sheet, "D", iRow, row["SHOP_NAME"].ToString(), false, false);
                InsertValueCell(ref sheet, "E", iRow, row["DMSP"].ToString(), false, false);
                InsertValueCell(ref sheet, "F", iRow, row["PARENT_SHOP"].ToString(), false, false);
                InsertValueCell(ref sheet, "G", iRow, row["CENTER_NAME"].ToString(), false, false);
                iSTT++;
                iRow++;
            }
        }

        private void btnDownTemplate_Click(object sender, EventArgs e)
        {
            Common.frmDownloadTemplateImport frmExportExcel = new Common.frmDownloadTemplateImport()
            {
                StrFileTemplateName = "import_danhmuc_chiahang",
                StrFileTemplatePath = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\import_danhmuc_chiahang.xlsx",
            };
            frmExportExcel.btnExportTemplate_Click();
        }
        
    }
}
