﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraGrid.Views.Grid;
using Library.AppCore;
using ERP.Inventory.PLC;
using Library.AppCore.LoadControls;
using System.Diagnostics;
using System.Data.Linq.SqlClient;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Threading;

namespace ERP.Inventory.DUI.Input
{
    /// <summary>
    /// Created by: Nguyễn Linh Tuấn
    /// Desc: Phiếu nhập
    /// 
    /// LÊ VĂN ĐÔNG: 11/12/2017 -> Bỏ ràng buộc đã hạch toàn thì không được check thực nhập
    /// LE VAN DONG: 20/03/2018 -> Khóa textbox nhập thông tin hóa đơn khi đơn hàng có hóa đơn
    /// Đặng Thế Hùng: 26/06/2018 -> Thêm column ProductStatusName (ẩn hiện theo mode edit)
    /// </summary>
    public partial class frmInputTransferOrderTypes : Form
    {
    }
}
