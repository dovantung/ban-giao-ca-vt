﻿using Library.AppCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI
{
    public partial class frmTestNew : Form
    {
        private SystemConfig objSystemConfig = new SystemConfig();

        public frmTestNew()
        {
            InitializeComponent();
            try
            {
                objSystemConfig.LoadConfig();

            }
            catch {
                MessageBox.Show("LoadConfig Failed!");
            }

            if (!objSystemConfig.NewLogin("administrator", "Erp123456"))
            {
                MessageBox.Show("Login Failed!");
            }
            else
            {
                MessageBox.Show("Login OK!");
            }
            //SystemConfig.GetWebServiceHost("WSHost_ERPTransactionServices").HostURL = @"http://localhost:3124/";
            SystemConfig.GetWebServiceHost("WSHost_ERPTransactionServices").HostURL = @"http://localhost/WebserviceHost/";
            SystemConfig.GetWebServiceHost("WSHost_ERPTransactionServices").IsHandShake = false;
        }

        private void quảnLýCácHàngTrảiNghiệmCủaBànTrảiNghiệmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(load4));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
        }

        private void quảnLýDanhSáchThamSốVòngQuayChiaHàngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(load5));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();

        }

        private void quảnLýDanhSáchĐịnhMứcTrưngBàyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(load6));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
            
        }

        private void tạoDanhMụcChiaHàngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(load7));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
            
        }

        private void chiaHàngSerialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(load8));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
           
        }

        private void điềuChuyểnNgangHàngSerialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(load9));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
            
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(load10));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
            
        }

        private void chiaHàngPhụKiệnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(load3));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
        }

        private void điềuChuyểnNgangHàngPhụKiệnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(load2));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
        }

        private void nhuCầuHàngPhụKiệnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread thdSyncRead = new Thread(new ThreadStart(load));
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
            
        }

        private void load()
        {
            ERP.Inventory.DUI.Goods.frmGoodsDividingListPK frm = new Goods.frmGoodsDividingListPK();
            frm.ShowDialog();
        }

        private void load2()
        {
            ERP.Inventory.DUI.Accessory.DUISameLevelAccessoryDividing frm = new Accessory.DUISameLevelAccessoryDividing();
            frm.ShowDialog();
        }

        private void load3()
        {
            ERP.Inventory.DUI.Accessory.DUIAccessoryDividing frm = new Accessory.DUIAccessoryDividing();
            frm.ShowDialog();
        }

        private void load4()
        {
            ERP.Inventory.DUI.Goods.DUIShopGoodsExperience frm = new Goods.DUIShopGoodsExperience();
            frm.ShowDialog();
        }

        private void load5()
        {
            ERP.Inventory.DUI.Goods.DUIGoodsTsvq frm = new Goods.DUIGoodsTsvq();
            frm.ShowDialog();
        }

        private void load6()
        {
            ERP.Inventory.DUI.Goods.DUIGoodsDmtb frm = new Goods.DUIGoodsDmtb();
            frm.ShowDialog();
        }

        private void load7()
        {
            ERP.Inventory.DUI.Goods.DUIGoods frm = new Goods.DUIGoods();
            frm.ShowDialog();
        }

        private void load8()
        {
            ERP.Inventory.DUI.Goods.DUIGoodsDividing frm = new Goods.DUIGoodsDividing();
            frm.ShowDialog();
        }

        private void load9()
        {
            ERP.Inventory.DUI.Goods.DUISameLevelDividing frm = new Goods.DUISameLevelDividing();
            frm.ShowDialog();
        }

        private void load10()
        {
            ERP.Inventory.DUI.Goods.frmGoodsDividingList frm = new Goods.frmGoodsDividingList();
            frm.ShowDialog();
        }
    }
}
