﻿namespace ERP.Inventory.DUI.PM
{
    partial class frmLotIMEISalesInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLotIMEISalesInfo));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip();
            this.mnuItemAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemReview = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemExport = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemViewComment = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabList = new System.Windows.Forms.TabPage();
            this.gpSearch = new System.Windows.Forms.GroupBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.chkIsDelete = new System.Windows.Forms.CheckBox();
            this.cboProductList = new ERP.MasterData.DUI.CustomControl.Product.cboProductList();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtKeyWords = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkActiveItem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkSystemItem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.tabDetail = new System.Windows.Forms.TabPage();
            this.tabCtrlDetail = new System.Windows.Forms.TabControl();
            this.tabDetailInfo = new System.Windows.Forms.TabPage();
            this.grpInfo = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.grdProduct = new DevExpress.XtraGrid.GridControl();
            this.grvProductSpec = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cboProduct = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cboProductSpec = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.btnSelectProduct = new System.Windows.Forms.Button();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtProductID = new System.Windows.Forms.TextBox();
            this.txtLotIMEISalesInfoID = new System.Windows.Forms.TextBox();
            this.chkIsReviewed = new System.Windows.Forms.CheckBox();
            this.chkIsComfirm = new System.Windows.Forms.CheckBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPrintVATContent = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dteEndWarantyDate = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.chkIsBrandNewWarranty = new System.Windows.Forms.CheckBox();
            this.chkIsSystem = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkIsActive = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtLotIMEISalesInfoName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabDetailIMEI = new System.Windows.Forms.TabPage();
            this.grdIMEI = new DevExpress.XtraGrid.GridControl();
            this.mnuIMEI = new System.Windows.Forms.ContextMenuStrip();
            this.mnuItemChooseIMEI = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemAddIMEI = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemDelIMEI = new System.Windows.Forms.ToolStripMenuItem();
            this.grvIMEI = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repChkIMEI_IsSytem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repTxtIMEI = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.SalePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.tabDetailImage = new System.Windows.Forms.TabPage();
            this.grdImages = new DevExpress.XtraGrid.GridControl();
            this.mnuIMAGE = new System.Windows.Forms.ContextMenuStrip();
            this.mnuAddImage = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEditImage = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDelImage = new System.Windows.Forms.ToolStripMenuItem();
            this.grvImages = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnMoveDown = new System.Windows.Forms.Button();
            this.btnEditImage = new System.Windows.Forms.Button();
            this.btnMoveUp = new System.Windows.Forms.Button();
            this.btnDelImage = new System.Windows.Forms.Button();
            this.btnAddNewImage = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnReview = new System.Windows.Forms.Button();
            this.btnUndo = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.mnuAction.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabList.SuspendLayout();
            this.gpSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActiveItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSystemItem)).BeginInit();
            this.tabDetail.SuspendLayout();
            this.tabCtrlDetail.SuspendLayout();
            this.tabDetailInfo.SuspendLayout();
            this.grpInfo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvProductSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboProductSpec)).BeginInit();
            this.tabDetailIMEI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdIMEI)).BeginInit();
            this.mnuIMEI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvIMEI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkIMEI_IsSytem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTxtIMEI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            this.tabDetailImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdImages)).BeginInit();
            this.mnuIMAGE.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuAction
            // 
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemAdd,
            this.mnuItemDelete,
            this.mnuItemEdit,
            this.mnuItemReview,
            this.mnuItemExport,
            this.mnuItemViewComment});
            this.mnuAction.Name = "contextMenuStrip1";
            this.mnuAction.Size = new System.Drawing.Size(168, 136);
            // 
            // mnuItemAdd
            // 
            this.mnuItemAdd.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemAdd.Image")));
            this.mnuItemAdd.Name = "mnuItemAdd";
            this.mnuItemAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.mnuItemAdd.Size = new System.Drawing.Size(167, 22);
            this.mnuItemAdd.Text = "Thêm";
            this.mnuItemAdd.Click += new System.EventHandler(this.mnuItemAdd_Click);
            // 
            // mnuItemDelete
            // 
            this.mnuItemDelete.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemDelete.Image")));
            this.mnuItemDelete.Name = "mnuItemDelete";
            this.mnuItemDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.mnuItemDelete.Size = new System.Drawing.Size(167, 22);
            this.mnuItemDelete.Text = "Xóa";
            this.mnuItemDelete.Click += new System.EventHandler(this.mnuItemDelete_Click);
            // 
            // mnuItemEdit
            // 
            this.mnuItemEdit.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemEdit.Image")));
            this.mnuItemEdit.Name = "mnuItemEdit";
            this.mnuItemEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.mnuItemEdit.Size = new System.Drawing.Size(167, 22);
            this.mnuItemEdit.Text = "Chỉnh sửa";
            this.mnuItemEdit.Click += new System.EventHandler(this.mnuItemEdit_Click);
            // 
            // mnuItemReview
            // 
            this.mnuItemReview.Image = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.mnuItemReview.Name = "mnuItemReview";
            this.mnuItemReview.Size = new System.Drawing.Size(167, 22);
            this.mnuItemReview.Text = "Duyệt";
            this.mnuItemReview.Click += new System.EventHandler(this.mnuItemReview_Click);
            // 
            // mnuItemExport
            // 
            this.mnuItemExport.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemExport.Image")));
            this.mnuItemExport.Name = "mnuItemExport";
            this.mnuItemExport.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.mnuItemExport.Size = new System.Drawing.Size(167, 22);
            this.mnuItemExport.Text = "Xuất excel";
            this.mnuItemExport.Click += new System.EventHandler(this.mnuItemExport_Click);
            // 
            // mnuItemViewComment
            // 
            this.mnuItemViewComment.Image = global::ERP.Inventory.DUI.Properties.Resources.search1;
            this.mnuItemViewComment.Name = "mnuItemViewComment";
            this.mnuItemViewComment.Size = new System.Drawing.Size(167, 22);
            this.mnuItemViewComment.Text = "Xem bình luận";
            this.mnuItemViewComment.Click += new System.EventHandler(this.mnuItemViewComment_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabList);
            this.tabControl.Controls.Add(this.tabDetail);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1025, 612);
            this.tabControl.TabIndex = 0;
            // 
            // tabList
            // 
            this.tabList.Controls.Add(this.gpSearch);
            this.tabList.Controls.Add(this.grdData);
            this.tabList.Location = new System.Drawing.Point(4, 25);
            this.tabList.Name = "tabList";
            this.tabList.Padding = new System.Windows.Forms.Padding(3);
            this.tabList.Size = new System.Drawing.Size(1017, 583);
            this.tabList.TabIndex = 0;
            this.tabList.Text = "Danh sách";
            this.tabList.UseVisualStyleBackColor = true;
            this.tabList.Enter += new System.EventHandler(this.tabList_Enter);
            // 
            // gpSearch
            // 
            this.gpSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gpSearch.Controls.Add(this.btnSearch);
            this.gpSearch.Controls.Add(this.chkIsDelete);
            this.gpSearch.Controls.Add(this.cboProductList);
            this.gpSearch.Controls.Add(this.dtpToDate);
            this.gpSearch.Controls.Add(this.dtpFromDate);
            this.gpSearch.Controls.Add(this.label9);
            this.gpSearch.Controls.Add(this.label11);
            this.gpSearch.Controls.Add(this.txtKeyWords);
            this.gpSearch.Controls.Add(this.label4);
            this.gpSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpSearch.Location = new System.Drawing.Point(3, 3);
            this.gpSearch.Margin = new System.Windows.Forms.Padding(4);
            this.gpSearch.Name = "gpSearch";
            this.gpSearch.Padding = new System.Windows.Forms.Padding(4);
            this.gpSearch.Size = new System.Drawing.Size(1011, 57);
            this.gpSearch.TabIndex = 0;
            this.gpSearch.TabStop = false;
            this.gpSearch.Text = "Tìm kiếm";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(900, 19);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(101, 24);
            this.btnSearch.TabIndex = 8;
            this.btnSearch.Text = "&Tìm kiếm";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // chkIsDelete
            // 
            this.chkIsDelete.AutoSize = true;
            this.chkIsDelete.Location = new System.Drawing.Point(834, 21);
            this.chkIsDelete.Name = "chkIsDelete";
            this.chkIsDelete.Size = new System.Drawing.Size(69, 20);
            this.chkIsDelete.TabIndex = 7;
            this.chkIsDelete.Text = "Đã xóa";
            this.chkIsDelete.UseVisualStyleBackColor = true;
            // 
            // cboProductList
            // 
            this.cboProductList.BackColor = System.Drawing.Color.Transparent;
            this.cboProductList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductList.Location = new System.Drawing.Point(618, 20);
            this.cboProductList.Margin = new System.Windows.Forms.Padding(4);
            this.cboProductList.Name = "cboProductList";
            this.cboProductList.ProductIDList = "";
            this.cboProductList.Size = new System.Drawing.Size(213, 23);
            this.cboProductList.TabIndex = 6;
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(430, 20);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(111, 22);
            this.dtpToDate.TabIndex = 4;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(317, 20);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(111, 22);
            this.dtpFromDate.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(540, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 16);
            this.label9.TabIndex = 5;
            this.label9.Text = "Sản phẩm:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(270, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 16);
            this.label11.TabIndex = 2;
            this.label11.Text = "Ngày:";
            // 
            // txtKeyWords
            // 
            this.txtKeyWords.Location = new System.Drawing.Point(84, 20);
            this.txtKeyWords.Margin = new System.Windows.Forms.Padding(4);
            this.txtKeyWords.Name = "txtKeyWords";
            this.txtKeyWords.Size = new System.Drawing.Size(185, 22);
            this.txtKeyWords.TabIndex = 1;
            this.txtKeyWords.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKeyWords_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 23);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Từ khóa:";
            // 
            // grdData
            // 
            this.grdData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdData.ContextMenuStrip = this.mnuAction;
            this.grdData.Location = new System.Drawing.Point(3, 64);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chkIsSelect,
            this.chkActiveItem,
            this.chkSystemItem});
            this.grdData.Size = new System.Drawing.Size(1011, 513);
            this.grdData.TabIndex = 1;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            this.grdData.DoubleClick += new System.EventHandler(this.grdData_DoubleClick);
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Preview.Options.UseFont = true;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewData.OptionsView.ShowFooter = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            // 
            // chkActiveItem
            // 
            this.chkActiveItem.AutoHeight = false;
            this.chkActiveItem.Name = "chkActiveItem";
            this.chkActiveItem.ValueChecked = ((short)(1));
            this.chkActiveItem.ValueUnchecked = ((short)(0));
            // 
            // chkSystemItem
            // 
            this.chkSystemItem.AutoHeight = false;
            this.chkSystemItem.Name = "chkSystemItem";
            this.chkSystemItem.ValueChecked = ((short)(1));
            this.chkSystemItem.ValueUnchecked = ((short)(0));
            // 
            // tabDetail
            // 
            this.tabDetail.Controls.Add(this.tabCtrlDetail);
            this.tabDetail.Location = new System.Drawing.Point(4, 25);
            this.tabDetail.Name = "tabDetail";
            this.tabDetail.Padding = new System.Windows.Forms.Padding(3);
            this.tabDetail.Size = new System.Drawing.Size(1017, 583);
            this.tabDetail.TabIndex = 1;
            this.tabDetail.Text = "Thông tin chi tiết";
            this.tabDetail.UseVisualStyleBackColor = true;
            this.tabDetail.Enter += new System.EventHandler(this.tabDetail_Enter);
            // 
            // tabCtrlDetail
            // 
            this.tabCtrlDetail.Controls.Add(this.tabDetailInfo);
            this.tabCtrlDetail.Controls.Add(this.tabDetailIMEI);
            this.tabCtrlDetail.Controls.Add(this.tabDetailImage);
            this.tabCtrlDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtrlDetail.Location = new System.Drawing.Point(3, 3);
            this.tabCtrlDetail.Name = "tabCtrlDetail";
            this.tabCtrlDetail.SelectedIndex = 0;
            this.tabCtrlDetail.Size = new System.Drawing.Size(1011, 577);
            this.tabCtrlDetail.TabIndex = 1;
            // 
            // tabDetailInfo
            // 
            this.tabDetailInfo.Controls.Add(this.grpInfo);
            this.tabDetailInfo.Location = new System.Drawing.Point(4, 25);
            this.tabDetailInfo.Name = "tabDetailInfo";
            this.tabDetailInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabDetailInfo.Size = new System.Drawing.Size(1003, 548);
            this.tabDetailInfo.TabIndex = 0;
            this.tabDetailInfo.Text = "Thông tin chung";
            this.tabDetailInfo.UseVisualStyleBackColor = true;
            // 
            // grpInfo
            // 
            this.grpInfo.Controls.Add(this.groupBox1);
            this.grpInfo.Controls.Add(this.txtLotIMEISalesInfoID);
            this.grpInfo.Controls.Add(this.chkIsReviewed);
            this.grpInfo.Controls.Add(this.chkIsComfirm);
            this.grpInfo.Controls.Add(this.txtNote);
            this.grpInfo.Controls.Add(this.label7);
            this.grpInfo.Controls.Add(this.txtPrintVATContent);
            this.grpInfo.Controls.Add(this.label6);
            this.grpInfo.Controls.Add(this.dteEndWarantyDate);
            this.grpInfo.Controls.Add(this.label18);
            this.grpInfo.Controls.Add(this.chkIsBrandNewWarranty);
            this.grpInfo.Controls.Add(this.chkIsSystem);
            this.grpInfo.Controls.Add(this.label1);
            this.grpInfo.Controls.Add(this.chkIsActive);
            this.grpInfo.Controls.Add(this.label2);
            this.grpInfo.Controls.Add(this.txtDescription);
            this.grpInfo.Controls.Add(this.txtLotIMEISalesInfoName);
            this.grpInfo.Controls.Add(this.label3);
            this.grpInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpInfo.Location = new System.Drawing.Point(3, 3);
            this.grpInfo.Name = "grpInfo";
            this.grpInfo.Size = new System.Drawing.Size(997, 542);
            this.grpInfo.TabIndex = 0;
            this.grpInfo.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.btnSelectProduct);
            this.groupBox1.Controls.Add(this.txtProductName);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtProductID);
            this.groupBox1.Location = new System.Drawing.Point(6, 45);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(923, 281);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin sản phẩm";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.grdProduct);
            this.panel2.Location = new System.Drawing.Point(13, 57);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(904, 218);
            this.panel2.TabIndex = 102;
            // 
            // grdProduct
            // 
            this.grdProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdProduct.Location = new System.Drawing.Point(0, 0);
            this.grdProduct.MainView = this.grvProductSpec;
            this.grdProduct.Name = "grdProduct";
            this.grdProduct.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.cboProduct,
            this.cboProductSpec,
            this.repositoryItemTextEdit1});
            this.grdProduct.Size = new System.Drawing.Size(904, 218);
            this.grdProduct.TabIndex = 0;
            this.grdProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvProductSpec});
            // 
            // grvProductSpec
            // 
            this.grvProductSpec.Appearance.FocusedRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvProductSpec.Appearance.FocusedRow.Options.UseFont = true;
            this.grvProductSpec.Appearance.HeaderPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvProductSpec.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvProductSpec.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvProductSpec.Appearance.Row.Options.UseFont = true;
            this.grvProductSpec.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5});
            this.grvProductSpec.GridControl = this.grdProduct;
            this.grvProductSpec.Name = "grvProductSpec";
            this.grvProductSpec.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn1.Caption = "Thông tin sản phẩm";
            this.gridColumn1.FieldName = "PRODUCTSPECNAME";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "Trạng thái";
            this.gridColumn2.ColumnEdit = this.cboProduct;
            this.gridColumn2.FieldName = "ProductSpecStatusID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // cboProduct
            // 
            this.cboProduct.AutoHeight = false;
            this.cboProduct.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboProduct.Name = "cboProduct";
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "Mô tả";
            this.gridColumn3.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn3.FieldName = "Note";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 1000;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn4.Caption = "Thông tin sản phẩm";
            this.gridColumn4.FieldName = "ProductSpecName";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Trạng thái sản phẩm";
            this.gridColumn5.FieldName = "PRODUCTSPECSTATUSNAME";
            this.gridColumn5.Name = "gridColumn5";
            // 
            // cboProductSpec
            // 
            this.cboProductSpec.AutoHeight = false;
            this.cboProductSpec.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboProductSpec.Name = "cboProductSpec";
            // 
            // btnSelectProduct
            // 
            this.btnSelectProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectProduct.Location = new System.Drawing.Point(386, 17);
            this.btnSelectProduct.Name = "btnSelectProduct";
            this.btnSelectProduct.Size = new System.Drawing.Size(28, 23);
            this.btnSelectProduct.TabIndex = 2;
            this.btnSelectProduct.Text = "...";
            this.btnSelectProduct.UseVisualStyleBackColor = true;
            this.btnSelectProduct.Click += new System.EventHandler(this.btnSelectProduct_Click);
            // 
            // txtProductName
            // 
            this.txtProductName.BackColor = System.Drawing.SystemColors.Info;
            this.txtProductName.Location = new System.Drawing.Point(570, 18);
            this.txtProductName.MaxLength = 20;
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.ReadOnly = true;
            this.txtProductName.Size = new System.Drawing.Size(270, 22);
            this.txtProductName.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(428, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 16);
            this.label8.TabIndex = 3;
            this.label8.Text = "Tên sản phẩm:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Sản phẩm:";
            // 
            // txtProductID
            // 
            this.txtProductID.BackColor = System.Drawing.SystemColors.Info;
            this.txtProductID.ContextMenuStrip = this.mnuAction;
            this.txtProductID.Location = new System.Drawing.Point(144, 18);
            this.txtProductID.MaxLength = 20;
            this.txtProductID.Name = "txtProductID";
            this.txtProductID.ReadOnly = true;
            this.txtProductID.Size = new System.Drawing.Size(251, 22);
            this.txtProductID.TabIndex = 1;
            // 
            // txtLotIMEISalesInfoID
            // 
            this.txtLotIMEISalesInfoID.BackColor = System.Drawing.SystemColors.Info;
            this.txtLotIMEISalesInfoID.Location = new System.Drawing.Point(150, 17);
            this.txtLotIMEISalesInfoID.MaxLength = 20;
            this.txtLotIMEISalesInfoID.Name = "txtLotIMEISalesInfoID";
            this.txtLotIMEISalesInfoID.ReadOnly = true;
            this.txtLotIMEISalesInfoID.Size = new System.Drawing.Size(270, 22);
            this.txtLotIMEISalesInfoID.TabIndex = 1;
            // 
            // chkIsReviewed
            // 
            this.chkIsReviewed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkIsReviewed.AutoSize = true;
            this.chkIsReviewed.Location = new System.Drawing.Point(316, 503);
            this.chkIsReviewed.Name = "chkIsReviewed";
            this.chkIsReviewed.Size = new System.Drawing.Size(62, 20);
            this.chkIsReviewed.TabIndex = 15;
            this.chkIsReviewed.Text = "Duyệt";
            this.chkIsReviewed.UseVisualStyleBackColor = true;
            this.chkIsReviewed.CheckedChanged += new System.EventHandler(this.chkIsReviewed_CheckedChanged);
            // 
            // chkIsComfirm
            // 
            this.chkIsComfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkIsComfirm.AutoSize = true;
            this.chkIsComfirm.Location = new System.Drawing.Point(150, 503);
            this.chkIsComfirm.Name = "chkIsComfirm";
            this.chkIsComfirm.Size = new System.Drawing.Size(82, 20);
            this.chkIsComfirm.TabIndex = 14;
            this.chkIsComfirm.Text = "Xác nhận";
            this.chkIsComfirm.UseVisualStyleBackColor = true;
            // 
            // txtNote
            // 
            this.txtNote.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtNote.BackColor = System.Drawing.Color.White;
            this.txtNote.Location = new System.Drawing.Point(150, 383);
            this.txtNote.MaxLength = 2000;
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtNote.Size = new System.Drawing.Size(779, 40);
            this.txtNote.TabIndex = 8;
            this.txtNote.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SelectTextAll);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 386);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 16);
            this.label7.TabIndex = 7;
            this.label7.Text = "Ghi chú:";
            // 
            // txtPrintVATContent
            // 
            this.txtPrintVATContent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtPrintVATContent.BackColor = System.Drawing.Color.White;
            this.txtPrintVATContent.Location = new System.Drawing.Point(150, 429);
            this.txtPrintVATContent.MaxLength = 2000;
            this.txtPrintVATContent.Multiline = true;
            this.txtPrintVATContent.Name = "txtPrintVATContent";
            this.txtPrintVATContent.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPrintVATContent.Size = new System.Drawing.Size(779, 40);
            this.txtPrintVATContent.TabIndex = 10;
            this.txtPrintVATContent.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SelectTextAll);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 432);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 32);
            this.label6.TabIndex = 9;
            this.label6.Text = "Nội dung in \r\nhoá đơn VAT:";
            // 
            // dteEndWarantyDate
            // 
            this.dteEndWarantyDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dteEndWarantyDate.CustomFormat = "dd/MM/yyyy";
            this.dteEndWarantyDate.Enabled = false;
            this.dteEndWarantyDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteEndWarantyDate.Location = new System.Drawing.Point(431, 472);
            this.dteEndWarantyDate.Name = "dteEndWarantyDate";
            this.dteEndWarantyDate.Size = new System.Drawing.Size(133, 22);
            this.dteEndWarantyDate.TabIndex = 13;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(313, 475);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(112, 16);
            this.label18.TabIndex = 12;
            this.label18.Text = "Ngày hết hạn BH:";
            // 
            // chkIsBrandNewWarranty
            // 
            this.chkIsBrandNewWarranty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkIsBrandNewWarranty.AutoSize = true;
            this.chkIsBrandNewWarranty.Location = new System.Drawing.Point(150, 474);
            this.chkIsBrandNewWarranty.Name = "chkIsBrandNewWarranty";
            this.chkIsBrandNewWarranty.Size = new System.Drawing.Size(151, 20);
            this.chkIsBrandNewWarranty.TabIndex = 11;
            this.chkIsBrandNewWarranty.Text = "Bảo hành chính hãng";
            this.chkIsBrandNewWarranty.UseVisualStyleBackColor = true;
            this.chkIsBrandNewWarranty.CheckedChanged += new System.EventHandler(this.chkIsBrandNewWarranty_CheckedChanged);
            this.chkIsBrandNewWarranty.Click += new System.EventHandler(this.chkIsBrandNewWarranty_Click);
            // 
            // chkIsSystem
            // 
            this.chkIsSystem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkIsSystem.Location = new System.Drawing.Point(563, 501);
            this.chkIsSystem.Name = "chkIsSystem";
            this.chkIsSystem.Size = new System.Drawing.Size(124, 24);
            this.chkIsSystem.TabIndex = 17;
            this.chkIsSystem.Text = "Hệ thống";
            this.chkIsSystem.UseVisualStyleBackColor = true;
            this.chkIsSystem.CheckedChanged += new System.EventHandler(this.chkIsSystem_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã thông tin lô IMEI:";
            // 
            // chkIsActive
            // 
            this.chkIsActive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkIsActive.Location = new System.Drawing.Point(431, 501);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(124, 24);
            this.chkIsActive.TabIndex = 16;
            this.chkIsActive.Text = "Kích hoạt";
            this.chkIsActive.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(434, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên thông tin lô IMEI:";
            // 
            // txtDescription
            // 
            this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtDescription.Location = new System.Drawing.Point(150, 332);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(779, 45);
            this.txtDescription.TabIndex = 6;
            this.txtDescription.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SelectTextAll);
            // 
            // txtLotIMEISalesInfoName
            // 
            this.txtLotIMEISalesInfoName.Location = new System.Drawing.Point(576, 17);
            this.txtLotIMEISalesInfoName.Name = "txtLotIMEISalesInfoName";
            this.txtLotIMEISalesInfoName.Size = new System.Drawing.Size(270, 22);
            this.txtLotIMEISalesInfoName.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 335);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Mô tả:";
            // 
            // tabDetailIMEI
            // 
            this.tabDetailIMEI.Controls.Add(this.grdIMEI);
            this.tabDetailIMEI.Location = new System.Drawing.Point(4, 25);
            this.tabDetailIMEI.Name = "tabDetailIMEI";
            this.tabDetailIMEI.Padding = new System.Windows.Forms.Padding(3);
            this.tabDetailIMEI.Size = new System.Drawing.Size(1003, 548);
            this.tabDetailIMEI.TabIndex = 1;
            this.tabDetailIMEI.Text = "Thông tin IMEI";
            this.tabDetailIMEI.UseVisualStyleBackColor = true;
            // 
            // grdIMEI
            // 
            this.grdIMEI.ContextMenuStrip = this.mnuIMEI;
            this.grdIMEI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdIMEI.Location = new System.Drawing.Point(3, 3);
            this.grdIMEI.MainView = this.grvIMEI;
            this.grdIMEI.Name = "grdIMEI";
            this.grdIMEI.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit2,
            this.repChkIMEI_IsSytem,
            this.repTxtIMEI,
            this.repositoryItemTextEdit2});
            this.grdIMEI.Size = new System.Drawing.Size(997, 542);
            this.grdIMEI.TabIndex = 36;
            this.grdIMEI.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvIMEI});
            // 
            // mnuIMEI
            // 
            this.mnuIMEI.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemChooseIMEI,
            this.mnuItemAddIMEI,
            this.mnuItemDelIMEI});
            this.mnuIMEI.Name = "contextMenuStrip1";
            this.mnuIMEI.Size = new System.Drawing.Size(148, 70);
            // 
            // mnuItemChooseIMEI
            // 
            this.mnuItemChooseIMEI.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.mnuItemChooseIMEI.Name = "mnuItemChooseIMEI";
            this.mnuItemChooseIMEI.Size = new System.Drawing.Size(147, 22);
            this.mnuItemChooseIMEI.Text = "Chọn IMEI";
            this.mnuItemChooseIMEI.Click += new System.EventHandler(this.mnuItemChooseIMEI_Click);
            // 
            // mnuItemAddIMEI
            // 
            this.mnuItemAddIMEI.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemAddIMEI.Image")));
            this.mnuItemAddIMEI.Name = "mnuItemAddIMEI";
            this.mnuItemAddIMEI.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.mnuItemAddIMEI.Size = new System.Drawing.Size(147, 22);
            this.mnuItemAddIMEI.Text = "Thêm";
            this.mnuItemAddIMEI.Click += new System.EventHandler(this.mnuItemAddIMEI_Click);
            // 
            // mnuItemDelIMEI
            // 
            this.mnuItemDelIMEI.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemDelIMEI.Image")));
            this.mnuItemDelIMEI.Name = "mnuItemDelIMEI";
            this.mnuItemDelIMEI.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.mnuItemDelIMEI.Size = new System.Drawing.Size(147, 22);
            this.mnuItemDelIMEI.Text = "Xóa";
            this.mnuItemDelIMEI.Click += new System.EventHandler(this.mnuItemDelIMEI_Click);
            // 
            // grvIMEI
            // 
            this.grvIMEI.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grvIMEI.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grvIMEI.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grvIMEI.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Transparent;
            this.grvIMEI.Appearance.FocusedRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvIMEI.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grvIMEI.Appearance.FocusedRow.Options.UseFont = true;
            this.grvIMEI.Appearance.HeaderPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvIMEI.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvIMEI.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvIMEI.Appearance.Preview.Options.UseFont = true;
            this.grvIMEI.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvIMEI.Appearance.Row.Options.UseFont = true;
            this.grvIMEI.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn9,
            this.gridColumn6,
            this.IMEI,
            this.SalePrice,
            this.gridColumn22,
            this.gridColumn7,
            this.gridColumn8});
            this.grvIMEI.GridControl = this.grdIMEI;
            this.grvIMEI.IndicatorWidth = 37;
            this.grvIMEI.Name = "grvIMEI";
            this.grvIMEI.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grvIMEI.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grvIMEI.OptionsNavigation.UseTabKey = false;
            this.grvIMEI.OptionsView.ColumnAutoWidth = false;
            this.grvIMEI.OptionsView.ShowAutoFilterRow = true;
            this.grvIMEI.OptionsView.ShowGroupPanel = false;
            this.grvIMEI.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvIMEI_CustomDrawRowIndicator);
            this.grvIMEI.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grvIMEI_ShowingEditor);
            this.grvIMEI.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grvIMEI_CellValueChanged);
            this.grvIMEI.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.grvIMEI_FilterEditorCreated);
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn9.Caption = "Chọn";
            this.gridColumn9.ColumnEdit = this.repChkIMEI_IsSytem;
            this.gridColumn9.FieldName = "IsSelect";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowMove = false;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 0;
            // 
            // repChkIMEI_IsSytem
            // 
            this.repChkIMEI_IsSytem.AutoHeight = false;
            this.repChkIMEI_IsSytem.Name = "repChkIMEI_IsSytem";
            this.repChkIMEI_IsSytem.CheckedChanged += new System.EventHandler(this.repChkIMEI_IsSytem_CheckedChanged);
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "STT";
            this.gridColumn6.FieldName = "OrderIndex";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn6.Width = 50;
            // 
            // IMEI
            // 
            this.IMEI.AppearanceHeader.Options.UseTextOptions = true;
            this.IMEI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IMEI.Caption = "IMEI";
            this.IMEI.ColumnEdit = this.repTxtIMEI;
            this.IMEI.FieldName = "IMEI";
            this.IMEI.Name = "IMEI";
            this.IMEI.OptionsColumn.AllowMove = false;
            this.IMEI.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.IMEI.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.IMEI.Visible = true;
            this.IMEI.VisibleIndex = 1;
            this.IMEI.Width = 200;
            // 
            // repTxtIMEI
            // 
            this.repTxtIMEI.AutoHeight = false;
            this.repTxtIMEI.MaxLength = 150;
            this.repTxtIMEI.Name = "repTxtIMEI";
            this.repTxtIMEI.Click += new System.EventHandler(this.repTxtIMEI_Click);
            this.repTxtIMEI.Validating += new System.ComponentModel.CancelEventHandler(this.repTxtIMEI_Validating);
            // 
            // SalePrice
            // 
            this.SalePrice.Caption = "Giá bán";
            this.SalePrice.DisplayFormat.FormatString = "#,##0";
            this.SalePrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SalePrice.FieldName = "SalePrice";
            this.SalePrice.Name = "SalePrice";
            this.SalePrice.OptionsColumn.ReadOnly = true;
            this.SalePrice.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.SalePrice.Visible = true;
            this.SalePrice.VisibleIndex = 2;
            this.SalePrice.Width = 120;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.Caption = "Hệ thống";
            this.gridColumn22.ColumnEdit = this.repChkIMEI_IsSytem;
            this.gridColumn22.FieldName = "IsSystem";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowMove = false;
            this.gridColumn22.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 3;
            this.gridColumn22.Width = 100;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "Đã xuất";
            this.gridColumn7.FieldName = "IsOutput";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowMove = false;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.Caption = "Ghi chú";
            this.gridColumn8.FieldName = "Note";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 5;
            this.gridColumn8.Width = 400;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // tabDetailImage
            // 
            this.tabDetailImage.Controls.Add(this.grdImages);
            this.tabDetailImage.Controls.Add(this.btnMoveDown);
            this.tabDetailImage.Controls.Add(this.btnEditImage);
            this.tabDetailImage.Controls.Add(this.btnMoveUp);
            this.tabDetailImage.Controls.Add(this.btnDelImage);
            this.tabDetailImage.Controls.Add(this.btnAddNewImage);
            this.tabDetailImage.Location = new System.Drawing.Point(4, 25);
            this.tabDetailImage.Name = "tabDetailImage";
            this.tabDetailImage.Padding = new System.Windows.Forms.Padding(3);
            this.tabDetailImage.Size = new System.Drawing.Size(1003, 548);
            this.tabDetailImage.TabIndex = 3;
            this.tabDetailImage.Text = "Danh sách ảnh lô IMEI";
            this.tabDetailImage.UseVisualStyleBackColor = true;
            // 
            // grdImages
            // 
            this.grdImages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdImages.ContextMenuStrip = this.mnuIMAGE;
            this.grdImages.Location = new System.Drawing.Point(6, 39);
            this.grdImages.MainView = this.grvImages;
            this.grdImages.Name = "grdImages";
            this.grdImages.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.repositoryItemCheckEdit2});
            this.grdImages.Size = new System.Drawing.Size(961, 587);
            this.grdImages.TabIndex = 35;
            this.grdImages.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvImages});
            this.grdImages.DoubleClick += new System.EventHandler(this.grdImages_DoubleClick);
            // 
            // mnuIMAGE
            // 
            this.mnuIMAGE.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAddImage,
            this.mnuEditImage,
            this.mnuDelImage});
            this.mnuIMAGE.Name = "contextMenuStrip1";
            this.mnuIMAGE.Size = new System.Drawing.Size(121, 70);
            // 
            // mnuAddImage
            // 
            this.mnuAddImage.Image = ((System.Drawing.Image)(resources.GetObject("mnuAddImage.Image")));
            this.mnuAddImage.Name = "mnuAddImage";
            this.mnuAddImage.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.mnuAddImage.ShowShortcutKeys = false;
            this.mnuAddImage.Size = new System.Drawing.Size(120, 22);
            this.mnuAddImage.Text = "Thêm";
            this.mnuAddImage.Click += new System.EventHandler(this.mnuAddImage_Click);
            // 
            // mnuEditImage
            // 
            this.mnuEditImage.Image = ((System.Drawing.Image)(resources.GetObject("mnuEditImage.Image")));
            this.mnuEditImage.Name = "mnuEditImage";
            this.mnuEditImage.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.mnuEditImage.ShowShortcutKeys = false;
            this.mnuEditImage.Size = new System.Drawing.Size(120, 22);
            this.mnuEditImage.Text = "Chỉnh sửa";
            this.mnuEditImage.Click += new System.EventHandler(this.mnuEditImage_Click);
            // 
            // mnuDelImage
            // 
            this.mnuDelImage.Image = ((System.Drawing.Image)(resources.GetObject("mnuDelImage.Image")));
            this.mnuDelImage.Name = "mnuDelImage";
            this.mnuDelImage.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.mnuDelImage.ShowShortcutKeys = false;
            this.mnuDelImage.Size = new System.Drawing.Size(120, 22);
            this.mnuDelImage.Text = "Xóa";
            this.mnuDelImage.Click += new System.EventHandler(this.mnuDelImage_Click);
            // 
            // grvImages
            // 
            this.grvImages.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grvImages.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grvImages.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grvImages.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Transparent;
            this.grvImages.Appearance.FocusedRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvImages.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grvImages.Appearance.FocusedRow.Options.UseFont = true;
            this.grvImages.Appearance.HeaderPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvImages.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvImages.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvImages.Appearance.Preview.Options.UseFont = true;
            this.grvImages.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvImages.Appearance.Row.Options.UseFont = true;
            this.grvImages.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18});
            this.grvImages.GridControl = this.grdImages;
            this.grvImages.Name = "grvImages";
            this.grvImages.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grvImages.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grvImages.OptionsNavigation.UseTabKey = false;
            this.grvImages.OptionsView.ShowAutoFilterRow = true;
            this.grvImages.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvImages.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Tên hình";
            this.gridColumn10.FieldName = "ImageName";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            this.gridColumn10.Width = 131;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Mô tả";
            this.gridColumn11.FieldName = "Description";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            this.gridColumn11.Width = 253;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "Hình lớn";
            this.gridColumn12.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn12.FieldName = "LargesizeImage";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn12.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            this.gridColumn12.Width = 108;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "Hình vừa";
            this.gridColumn13.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn13.FieldName = "MediumSizeImage";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn13.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 3;
            this.gridColumn13.Width = 107;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "Hình nhỏ";
            this.gridColumn14.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn14.FieldName = "SmallSizeImage";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn14.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 4;
            this.gridColumn14.Width = 107;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.Caption = "Mặc định";
            this.gridColumn15.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn15.FieldName = "IsDefault";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 5;
            this.gridColumn15.Width = 72;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.Caption = "Kích hoạt";
            this.gridColumn16.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn16.FieldName = "IsActive";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 6;
            this.gridColumn16.Width = 71;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.Caption = "Hệ thống";
            this.gridColumn17.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn17.FieldName = "IsSystem";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 7;
            this.gridColumn17.Width = 72;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.Caption = "Thứ tự";
            this.gridColumn18.FieldName = "OrderIndex";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 8;
            this.gridColumn18.Width = 85;
            // 
            // btnMoveDown
            // 
            this.btnMoveDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveDown.Image")));
            this.btnMoveDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMoveDown.Location = new System.Drawing.Point(970, 190);
            this.btnMoveDown.Name = "btnMoveDown";
            this.btnMoveDown.Size = new System.Drawing.Size(25, 33);
            this.btnMoveDown.TabIndex = 34;
            this.btnMoveDown.Text = "     Thêm";
            this.btnMoveDown.UseVisualStyleBackColor = true;
            this.btnMoveDown.Click += new System.EventHandler(this.btnMoveDown_Click);
            // 
            // btnEditImage
            // 
            this.btnEditImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditImage.Image = ((System.Drawing.Image)(resources.GetObject("btnEditImage.Image")));
            this.btnEditImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditImage.Location = new System.Drawing.Point(759, 8);
            this.btnEditImage.Name = "btnEditImage";
            this.btnEditImage.Size = new System.Drawing.Size(102, 25);
            this.btnEditImage.TabIndex = 32;
            this.btnEditImage.Text = "     Chỉnh sửa";
            this.btnEditImage.UseVisualStyleBackColor = true;
            this.btnEditImage.Click += new System.EventHandler(this.btnEditImage_Click);
            // 
            // btnMoveUp
            // 
            this.btnMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveUp.Image")));
            this.btnMoveUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMoveUp.Location = new System.Drawing.Point(970, 151);
            this.btnMoveUp.Name = "btnMoveUp";
            this.btnMoveUp.Size = new System.Drawing.Size(25, 33);
            this.btnMoveUp.TabIndex = 33;
            this.btnMoveUp.Text = "     Thêm";
            this.btnMoveUp.UseVisualStyleBackColor = true;
            this.btnMoveUp.Click += new System.EventHandler(this.btnMoveUp_Click);
            // 
            // btnDelImage
            // 
            this.btnDelImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelImage.Image = ((System.Drawing.Image)(resources.GetObject("btnDelImage.Image")));
            this.btnDelImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelImage.Location = new System.Drawing.Point(867, 8);
            this.btnDelImage.Name = "btnDelImage";
            this.btnDelImage.Size = new System.Drawing.Size(102, 25);
            this.btnDelImage.TabIndex = 31;
            this.btnDelImage.Text = "     Xóa";
            this.btnDelImage.UseVisualStyleBackColor = true;
            this.btnDelImage.Click += new System.EventHandler(this.btnDelImage_Click);
            // 
            // btnAddNewImage
            // 
            this.btnAddNewImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNewImage.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNewImage.Image")));
            this.btnAddNewImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddNewImage.Location = new System.Drawing.Point(656, 8);
            this.btnAddNewImage.Name = "btnAddNewImage";
            this.btnAddNewImage.Size = new System.Drawing.Size(98, 25);
            this.btnAddNewImage.TabIndex = 30;
            this.btnAddNewImage.Text = "     Thêm";
            this.btnAddNewImage.UseVisualStyleBackColor = true;
            this.btnAddNewImage.Click += new System.EventHandler(this.btnAddNewImage_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Info;
            this.panel1.Controls.Add(this.btnReview);
            this.panel1.Controls.Add(this.btnUndo);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.btnExportExcel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 612);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1025, 38);
            this.panel1.TabIndex = 1;
            // 
            // btnReview
            // 
            this.btnReview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReview.Image = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.btnReview.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReview.Location = new System.Drawing.Point(825, 7);
            this.btnReview.Name = "btnReview";
            this.btnReview.Size = new System.Drawing.Size(95, 25);
            this.btnReview.TabIndex = 4;
            this.btnReview.Text = "   Duyệt";
            this.btnReview.UseVisualStyleBackColor = true;
            this.btnReview.Click += new System.EventHandler(this.btnReview_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUndo.Image = ((System.Drawing.Image)(resources.GetObject("btnUndo.Image")));
            this.btnUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUndo.Location = new System.Drawing.Point(926, 7);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(95, 25);
            this.btnUndo.TabIndex = 5;
            this.btnUndo.Text = "   Bỏ qua";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(725, 7);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(95, 25);
            this.btnUpdate.TabIndex = 3;
            this.btnUpdate.Text = "   Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(624, 7);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(95, 25);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "   Xóa";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(524, 7);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(95, 25);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "   Chỉnh sửa";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(424, 7);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(95, 25);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "   Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportExcel.Location = new System.Drawing.Point(7, 7);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(95, 25);
            this.btnExportExcel.TabIndex = 6;
            this.btnExportExcel.Text = "    Xuất Excel";
            this.btnExportExcel.UseVisualStyleBackColor = true;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // frmLotIMEISalesInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 650);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmLotIMEISalesInfo";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý thông tin bán hàng lô IMEI";
            this.Load += new System.EventHandler(this.frmLotIMEISalesInfo_Load);
            this.mnuAction.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabList.ResumeLayout(false);
            this.gpSearch.ResumeLayout(false);
            this.gpSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActiveItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSystemItem)).EndInit();
            this.tabDetail.ResumeLayout(false);
            this.tabCtrlDetail.ResumeLayout(false);
            this.tabDetailInfo.ResumeLayout(false);
            this.grpInfo.ResumeLayout(false);
            this.grpInfo.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvProductSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboProductSpec)).EndInit();
            this.tabDetailIMEI.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdIMEI)).EndInit();
            this.mnuIMEI.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvIMEI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkIMEI_IsSytem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTxtIMEI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            this.tabDetailImage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdImages)).EndInit();
            this.mnuIMAGE.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuItemAdd;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDelete;
        private System.Windows.Forms.ToolStripMenuItem mnuItemEdit;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExport;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabList;
        private System.Windows.Forms.TabPage tabDetail;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnExportExcel;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkActiveItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkSystemItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpInfo;
        private System.Windows.Forms.CheckBox chkIsSystem;
        private System.Windows.Forms.CheckBox chkIsActive;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtLotIMEISalesInfoName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gpSearch;
        private System.Windows.Forms.TextBox txtKeyWords;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.Button btnSelectProduct;
        private System.Windows.Forms.TextBox txtProductID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkIsReviewed;
        private System.Windows.Forms.CheckBox chkIsComfirm;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPrintVATContent;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dteEndWarantyDate;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox chkIsBrandNewWarranty;
        private System.Windows.Forms.TabControl tabCtrlDetail;
        private System.Windows.Forms.TabPage tabDetailInfo;
        private System.Windows.Forms.TabPage tabDetailIMEI;
        private System.Windows.Forms.TabPage tabDetailImage;
        private DevExpress.XtraGrid.GridControl grdImages;
        private DevExpress.XtraGrid.Views.Grid.GridView grvImages;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private System.Windows.Forms.Button btnMoveDown;
        private System.Windows.Forms.Button btnEditImage;
        private System.Windows.Forms.Button btnMoveUp;
        private System.Windows.Forms.Button btnDelImage;
        private System.Windows.Forms.Button btnAddNewImage;
        private DevExpress.XtraGrid.GridControl grdIMEI;
        private DevExpress.XtraGrid.Views.Grid.GridView grvIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn IMEI;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repChkIMEI_IsSytem;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraGrid.GridControl grdProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView grvProductSpec;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboProduct;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboProductSpec;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtLotIMEISalesInfoID;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTxtIMEI;
        private System.Windows.Forms.ContextMenuStrip mnuIMEI;
        private System.Windows.Forms.ToolStripMenuItem mnuItemAddIMEI;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDelIMEI;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnReview;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private System.Windows.Forms.ToolStripMenuItem mnuItemReview;
        private MasterData.DUI.CustomControl.Product.cboProductList cboProductList;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ContextMenuStrip mnuIMAGE;
        private System.Windows.Forms.ToolStripMenuItem mnuAddImage;
        private System.Windows.Forms.ToolStripMenuItem mnuEditImage;
        private System.Windows.Forms.ToolStripMenuItem mnuDelImage;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private System.Windows.Forms.ToolStripMenuItem mnuItemChooseIMEI;
        private System.Windows.Forms.CheckBox chkIsDelete;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private System.Windows.Forms.ToolStripMenuItem mnuItemViewComment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn SalePrice;
    }
}