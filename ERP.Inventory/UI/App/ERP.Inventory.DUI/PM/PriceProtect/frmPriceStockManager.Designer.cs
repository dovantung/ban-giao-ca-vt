﻿namespace ERP.Inventory.DUI.PM.PriceProtect
{
    partial class frmPriceStockManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkSearchByVoucherDate = new System.Windows.Forms.CheckBox();
            this.dteVoucherToDate = new System.Windows.Forms.DateTimePicker();
            this.dteVoucherFromDate = new System.Windows.Forms.DateTimePicker();
            this.cboPriceProtectType = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label2 = new System.Windows.Forms.Label();
            this.chkIsViewDetail = new System.Windows.Forms.CheckBox();
            this.chkIsAdjustCostPrice = new System.Windows.Forms.CheckBox();
            this.chkIsHasVATPinvoice = new System.Windows.Forms.CheckBox();
            this.chkSearchByPostDate = new System.Windows.Forms.CheckBox();
            this.chkSearchByInputDate = new System.Windows.Forms.CheckBox();
            this.dteInputToDate = new System.Windows.Forms.DateTimePicker();
            this.dteInputFromDate = new System.Windows.Forms.DateTimePicker();
            this.dtePostToDate = new System.Windows.Forms.DateTimePicker();
            this.dtePostFromDate = new System.Windows.Forms.DateTimePicker();
            this.chkSearchByStockDate = new System.Windows.Forms.CheckBox();
            this.chkSearchByProtectDate = new System.Windows.Forms.CheckBox();
            this.dteProtectToDate = new System.Windows.Forms.DateTimePicker();
            this.dteProtectFromDate = new System.Windows.Forms.DateTimePicker();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboSearchBy = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cboCompany = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label3 = new System.Windows.Forms.Label();
            this.cboStore = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.chkIsDeleted = new System.Windows.Forms.CheckBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dteStockToDate = new System.Windows.Forms.DateTimePicker();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.dteStockFromDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuViewItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCreateItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel1.SuspendLayout();
            this.mnuAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkIsDeleted);
            this.panel1.Controls.Add(this.chkSearchByVoucherDate);
            this.panel1.Controls.Add(this.dteVoucherToDate);
            this.panel1.Controls.Add(this.dteVoucherFromDate);
            this.panel1.Controls.Add(this.cboPriceProtectType);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.chkIsViewDetail);
            this.panel1.Controls.Add(this.chkIsAdjustCostPrice);
            this.panel1.Controls.Add(this.chkIsHasVATPinvoice);
            this.panel1.Controls.Add(this.chkSearchByPostDate);
            this.panel1.Controls.Add(this.chkSearchByInputDate);
            this.panel1.Controls.Add(this.dteInputToDate);
            this.panel1.Controls.Add(this.dteInputFromDate);
            this.panel1.Controls.Add(this.dtePostToDate);
            this.panel1.Controls.Add(this.dtePostFromDate);
            this.panel1.Controls.Add(this.chkSearchByStockDate);
            this.panel1.Controls.Add(this.chkSearchByProtectDate);
            this.panel1.Controls.Add(this.dteProtectToDate);
            this.panel1.Controls.Add(this.dteProtectFromDate);
            this.panel1.Controls.Add(this.txtKeywords);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.cboSearchBy);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cboCompany);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cboStore);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnExportExcel);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.dteStockToDate);
            this.panel1.Controls.Add(this.cboStatus);
            this.panel1.Controls.Add(this.dteStockFromDate);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1139, 157);
            this.panel1.TabIndex = 0;
            // 
            // chkSearchByVoucherDate
            // 
            this.chkSearchByVoucherDate.AutoSize = true;
            this.chkSearchByVoucherDate.Location = new System.Drawing.Point(729, 69);
            this.chkSearchByVoucherDate.Name = "chkSearchByVoucherDate";
            this.chkSearchByVoucherDate.Size = new System.Drawing.Size(134, 20);
            this.chkSearchByVoucherDate.TabIndex = 30;
            this.chkSearchByVoucherDate.Text = "Ngày hạch toán 2:";
            this.chkSearchByVoucherDate.UseVisualStyleBackColor = true;
            this.chkSearchByVoucherDate.CheckedChanged += new System.EventHandler(this.chkSearchByVoucherDate_CheckedChanged);
            // 
            // dteVoucherToDate
            // 
            this.dteVoucherToDate.CustomFormat = "dd/MM/yyyy";
            this.dteVoucherToDate.Enabled = false;
            this.dteVoucherToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteVoucherToDate.Location = new System.Drawing.Point(996, 69);
            this.dteVoucherToDate.Name = "dteVoucherToDate";
            this.dteVoucherToDate.Size = new System.Drawing.Size(114, 22);
            this.dteVoucherToDate.TabIndex = 32;
            // 
            // dteVoucherFromDate
            // 
            this.dteVoucherFromDate.CustomFormat = "dd/MM/yyyy";
            this.dteVoucherFromDate.Enabled = false;
            this.dteVoucherFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteVoucherFromDate.Location = new System.Drawing.Point(869, 69);
            this.dteVoucherFromDate.Name = "dteVoucherFromDate";
            this.dteVoucherFromDate.Size = new System.Drawing.Size(121, 22);
            this.dteVoucherFromDate.TabIndex = 31;
            // 
            // cboPriceProtectType
            // 
            this.cboPriceProtectType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPriceProtectType.Location = new System.Drawing.Point(109, 38);
            this.cboPriceProtectType.Margin = new System.Windows.Forms.Padding(0);
            this.cboPriceProtectType.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboPriceProtectType.Name = "cboPriceProtectType";
            this.cboPriceProtectType.Size = new System.Drawing.Size(247, 24);
            this.cboPriceProtectType.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "Loại hỗ trợ:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // chkIsViewDetail
            // 
            this.chkIsViewDetail.AutoSize = true;
            this.chkIsViewDetail.Location = new System.Drawing.Point(729, 124);
            this.chkIsViewDetail.Name = "chkIsViewDetail";
            this.chkIsViewDetail.Size = new System.Drawing.Size(97, 20);
            this.chkIsViewDetail.TabIndex = 27;
            this.chkIsViewDetail.Text = "Xem chi  tiết";
            this.chkIsViewDetail.UseVisualStyleBackColor = true;
            // 
            // chkIsAdjustCostPrice
            // 
            this.chkIsAdjustCostPrice.AutoSize = true;
            this.chkIsAdjustCostPrice.Location = new System.Drawing.Point(869, 98);
            this.chkIsAdjustCostPrice.Name = "chkIsAdjustCostPrice";
            this.chkIsAdjustCostPrice.Size = new System.Drawing.Size(130, 20);
            this.chkIsAdjustCostPrice.TabIndex = 21;
            this.chkIsAdjustCostPrice.Text = "Đã xử lý giá vốn 2";
            this.chkIsAdjustCostPrice.UseVisualStyleBackColor = true;
            // 
            // chkIsHasVATPinvoice
            // 
            this.chkIsHasVATPinvoice.AutoSize = true;
            this.chkIsHasVATPinvoice.Location = new System.Drawing.Point(729, 98);
            this.chkIsHasVATPinvoice.Name = "chkIsHasVATPinvoice";
            this.chkIsHasVATPinvoice.Size = new System.Drawing.Size(114, 20);
            this.chkIsHasVATPinvoice.TabIndex = 20;
            this.chkIsHasVATPinvoice.Text = "Đã có hóa đơn";
            this.chkIsHasVATPinvoice.UseVisualStyleBackColor = true;
            // 
            // chkSearchByPostDate
            // 
            this.chkSearchByPostDate.AutoSize = true;
            this.chkSearchByPostDate.Location = new System.Drawing.Point(729, 40);
            this.chkSearchByPostDate.Name = "chkSearchByPostDate";
            this.chkSearchByPostDate.Size = new System.Drawing.Size(134, 20);
            this.chkSearchByPostDate.TabIndex = 13;
            this.chkSearchByPostDate.Text = "Ngày hạch toán 1:";
            this.chkSearchByPostDate.UseVisualStyleBackColor = true;
            this.chkSearchByPostDate.CheckedChanged += new System.EventHandler(this.chkSearchByPostDate_CheckedChanged);
            // 
            // chkSearchByInputDate
            // 
            this.chkSearchByInputDate.AutoSize = true;
            this.chkSearchByInputDate.Location = new System.Drawing.Point(729, 11);
            this.chkSearchByInputDate.Name = "chkSearchByInputDate";
            this.chkSearchByInputDate.Size = new System.Drawing.Size(96, 20);
            this.chkSearchByInputDate.TabIndex = 5;
            this.chkSearchByInputDate.Text = "Ngày nhập:";
            this.chkSearchByInputDate.UseVisualStyleBackColor = true;
            this.chkSearchByInputDate.CheckedChanged += new System.EventHandler(this.chkSearchByInputDate_CheckedChanged);
            // 
            // dteInputToDate
            // 
            this.dteInputToDate.CustomFormat = "dd/MM/yyyy";
            this.dteInputToDate.Enabled = false;
            this.dteInputToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteInputToDate.Location = new System.Drawing.Point(996, 11);
            this.dteInputToDate.Name = "dteInputToDate";
            this.dteInputToDate.Size = new System.Drawing.Size(114, 22);
            this.dteInputToDate.TabIndex = 7;
            // 
            // dteInputFromDate
            // 
            this.dteInputFromDate.CustomFormat = "dd/MM/yyyy";
            this.dteInputFromDate.Enabled = false;
            this.dteInputFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteInputFromDate.Location = new System.Drawing.Point(869, 11);
            this.dteInputFromDate.Name = "dteInputFromDate";
            this.dteInputFromDate.Size = new System.Drawing.Size(121, 22);
            this.dteInputFromDate.TabIndex = 6;
            // 
            // dtePostToDate
            // 
            this.dtePostToDate.CustomFormat = "dd/MM/yyyy";
            this.dtePostToDate.Enabled = false;
            this.dtePostToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtePostToDate.Location = new System.Drawing.Point(996, 40);
            this.dtePostToDate.Name = "dtePostToDate";
            this.dtePostToDate.Size = new System.Drawing.Size(114, 22);
            this.dtePostToDate.TabIndex = 15;
            // 
            // dtePostFromDate
            // 
            this.dtePostFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtePostFromDate.Enabled = false;
            this.dtePostFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtePostFromDate.Location = new System.Drawing.Point(869, 40);
            this.dtePostFromDate.Name = "dtePostFromDate";
            this.dtePostFromDate.Size = new System.Drawing.Size(121, 22);
            this.dtePostFromDate.TabIndex = 14;
            // 
            // chkSearchByStockDate
            // 
            this.chkSearchByStockDate.AutoSize = true;
            this.chkSearchByStockDate.Location = new System.Drawing.Point(362, 42);
            this.chkSearchByStockDate.Name = "chkSearchByStockDate";
            this.chkSearchByStockDate.Size = new System.Drawing.Size(112, 20);
            this.chkSearchByStockDate.TabIndex = 10;
            this.chkSearchByStockDate.Text = "Ngày chốt tồn:";
            this.chkSearchByStockDate.UseVisualStyleBackColor = true;
            this.chkSearchByStockDate.CheckedChanged += new System.EventHandler(this.chkSearchByStockDate_CheckedChanged);
            // 
            // chkSearchByProtectDate
            // 
            this.chkSearchByProtectDate.AutoSize = true;
            this.chkSearchByProtectDate.Checked = true;
            this.chkSearchByProtectDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSearchByProtectDate.Location = new System.Drawing.Point(362, 13);
            this.chkSearchByProtectDate.Name = "chkSearchByProtectDate";
            this.chkSearchByProtectDate.Size = new System.Drawing.Size(85, 20);
            this.chkSearchByProtectDate.TabIndex = 2;
            this.chkSearchByProtectDate.Text = "Ngày lập:";
            this.chkSearchByProtectDate.UseVisualStyleBackColor = true;
            this.chkSearchByProtectDate.CheckedChanged += new System.EventHandler(this.chkSearchByProtectDate_CheckedChanged);
            // 
            // dteProtectToDate
            // 
            this.dteProtectToDate.CustomFormat = "dd/MM/yyyy";
            this.dteProtectToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteProtectToDate.Location = new System.Drawing.Point(606, 11);
            this.dteProtectToDate.Name = "dteProtectToDate";
            this.dteProtectToDate.Size = new System.Drawing.Size(114, 22);
            this.dteProtectToDate.TabIndex = 4;
            // 
            // dteProtectFromDate
            // 
            this.dteProtectFromDate.CustomFormat = "dd/MM/yyyy";
            this.dteProtectFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteProtectFromDate.Location = new System.Drawing.Point(485, 11);
            this.dteProtectFromDate.Name = "dteProtectFromDate";
            this.dteProtectFromDate.Size = new System.Drawing.Size(114, 22);
            this.dteProtectFromDate.TabIndex = 3;
            // 
            // txtKeywords
            // 
            this.txtKeywords.Location = new System.Drawing.Point(109, 96);
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.Size = new System.Drawing.Size(247, 22);
            this.txtKeywords.TabIndex = 24;
            this.txtKeywords.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKeywords_KeyDown);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 16);
            this.label8.TabIndex = 23;
            this.label8.Text = "Từ khóa:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cboSearchBy
            // 
            this.cboSearchBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchBy.FormattingEnabled = true;
            this.cboSearchBy.Items.AddRange(new object[] {
            "Mã phiếu",
            "Mã hóa đơn từ đối tác",
            "Số hóa đơn từ đối tác",
            "Số biên bản",
            "Người tạo"});
            this.cboSearchBy.Location = new System.Drawing.Point(485, 96);
            this.cboSearchBy.Name = "cboSearchBy";
            this.cboSearchBy.Size = new System.Drawing.Size(235, 24);
            this.cboSearchBy.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(359, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 16);
            this.label6.TabIndex = 25;
            this.label6.Text = "Tìm kiếm theo:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cboCompany
            // 
            this.cboCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCompany.Location = new System.Drawing.Point(109, 9);
            this.cboCompany.Margin = new System.Windows.Forms.Padding(0);
            this.cboCompany.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(247, 24);
            this.cboCompany.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Công ty:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cboStore
            // 
            this.cboStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStore.Location = new System.Drawing.Point(109, 67);
            this.cboStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStore.Name = "cboStore";
            this.cboStore.Size = new System.Drawing.Size(247, 24);
            this.cboStore.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 16);
            this.label1.TabIndex = 16;
            this.label1.Text = "Kho chứng từ:";
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Enabled = false;
            this.btnExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportExcel.Location = new System.Drawing.Point(996, 118);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(114, 26);
            this.btnExportExcel.TabIndex = 29;
            this.btnExportExcel.Text = "     Xuất Excel";
            this.btnExportExcel.UseVisualStyleBackColor = true;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // chkIsDeleted
            // 
            this.chkIsDeleted.AutoSize = true;
            this.chkIsDeleted.Location = new System.Drawing.Point(996, 98);
            this.chkIsDeleted.Name = "chkIsDeleted";
            this.chkIsDeleted.Size = new System.Drawing.Size(68, 20);
            this.chkIsDeleted.TabIndex = 22;
            this.chkIsDeleted.Text = "Đã hủy";
            this.chkIsDeleted.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(869, 118);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(121, 26);
            this.btnSearch.TabIndex = 28;
            this.btnSearch.Text = "    Tìm kiếm";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dteStockToDate
            // 
            this.dteStockToDate.CustomFormat = "dd/MM/yyyy";
            this.dteStockToDate.Enabled = false;
            this.dteStockToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteStockToDate.Location = new System.Drawing.Point(606, 40);
            this.dteStockToDate.Name = "dteStockToDate";
            this.dteStockToDate.Size = new System.Drawing.Size(114, 22);
            this.dteStockToDate.TabIndex = 12;
            // 
            // cboStatus
            // 
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.Items.AddRange(new object[] {
            "Tất cả",
            "Khởi tạo",
            "Đã có biên bản"});
            this.cboStatus.Location = new System.Drawing.Point(485, 67);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Size = new System.Drawing.Size(235, 24);
            this.cboStatus.TabIndex = 19;
            // 
            // dteStockFromDate
            // 
            this.dteStockFromDate.CustomFormat = "dd/MM/yyyy";
            this.dteStockFromDate.Enabled = false;
            this.dteStockFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteStockFromDate.Location = new System.Drawing.Point(485, 40);
            this.dteStockFromDate.Name = "dteStockFromDate";
            this.dteStockFromDate.Size = new System.Drawing.Size(114, 22);
            this.dteStockFromDate.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(359, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 16);
            this.label7.TabIndex = 18;
            this.label7.Text = "Trạng thái:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // mnuAction
            // 
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuViewItem,
            this.mnuCreateItem,
            this.mnuItemExportExcel});
            this.mnuAction.Name = "mnuAction";
            this.mnuAction.Size = new System.Drawing.Size(221, 70);
            this.mnuAction.Opening += new System.ComponentModel.CancelEventHandler(this.mnuAction_Opening);
            // 
            // mnuViewItem
            // 
            this.mnuViewItem.Image = global::ERP.Inventory.DUI.Properties.Resources.search1;
            this.mnuViewItem.Name = "mnuViewItem";
            this.mnuViewItem.Size = new System.Drawing.Size(220, 22);
            this.mnuViewItem.Text = "Xem chi tiết";
            this.mnuViewItem.Click += new System.EventHandler(this.mnuViewItem_Click);
            // 
            // mnuCreateItem
            // 
            this.mnuCreateItem.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.mnuCreateItem.Name = "mnuCreateItem";
            this.mnuCreateItem.Size = new System.Drawing.Size(220, 22);
            this.mnuCreateItem.Text = "Tạo hóa đơn mua từ đối tác";
            this.mnuCreateItem.Click += new System.EventHandler(this.mnuCreateItem_Click);
            // 
            // mnuItemExportExcel
            // 
            this.mnuItemExportExcel.Enabled = false;
            this.mnuItemExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportExcel.Name = "mnuItemExportExcel";
            this.mnuItemExportExcel.Size = new System.Drawing.Size(220, 22);
            this.mnuItemExportExcel.Text = "Xuất Excel";
            this.mnuItemExportExcel.Click += new System.EventHandler(this.mnuItemExportExcel_Click);
            // 
            // grdData
            // 
            this.grdData.ContextMenuStrip = this.mnuAction;
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(0, 157);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEdit1});
            this.grdData.Size = new System.Drawing.Size(1139, 349);
            this.grdData.TabIndex = 2;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewData.Appearance.GroupFooter.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.GroupFooter.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.ColumnPanelRowHeight = 50;
            this.grdViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn1,
            this.gridColumn27,
            this.gridColumn46,
            this.gridColumn45,
            this.gridColumn44,
            this.gridColumn43,
            this.gridColumn42,
            this.gridColumn41,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn2,
            this.gridColumn26,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn40,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn62,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn6,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn61,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn59,
            this.gridColumn60,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58});
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsView.ColumnAutoWidth = false;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowFooter = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            this.grdViewData.DoubleClick += new System.EventHandler(this.grdViewData_DoubleClick);
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn17.AppearanceCell.Options.UseFont = true;
            this.gridColumn17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn17.AppearanceHeader.Options.UseFont = true;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn17.Caption = "Mã phiếu";
            this.gridColumn17.FieldName = "PRICEPROTECTID";
            this.gridColumn17.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn17.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "PRICEPROTECTID", "Tổng cộng:")});
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 0;
            this.gridColumn17.Width = 140;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn18.AppearanceCell.Options.UseFont = true;
            this.gridColumn18.Caption = "Ngày chốt tồn";
            this.gridColumn18.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColumn18.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn18.FieldName = "STOCKDATE";
            this.gridColumn18.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn18.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "PRICEPROTECTID", "{0:N0}")});
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 1;
            this.gridColumn18.Width = 90;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Mã NCC";
            this.gridColumn19.FieldName = "CUSTOMERID";
            this.gridColumn19.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 2;
            this.gridColumn19.Width = 85;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn20.AppearanceCell.Options.UseFont = true;
            this.gridColumn20.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn20.AppearanceHeader.Options.UseFont = true;
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn20.Caption = "Nhà cung cấp";
            this.gridColumn20.FieldName = "CUSTOMERNAME";
            this.gridColumn20.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 3;
            this.gridColumn20.Width = 200;
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn21.AppearanceCell.Options.UseFont = true;
            this.gridColumn21.Caption = "Loại hỗ trợ hàng tồn";
            this.gridColumn21.FieldName = "PRICEPROTECTTYPENAME";
            this.gridColumn21.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 4;
            this.gridColumn21.Width = 170;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn22.AppearanceCell.Options.UseFont = true;
            this.gridColumn22.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn22.AppearanceHeader.Options.UseFont = true;
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn22.Caption = "Số biên bản";
            this.gridColumn22.FieldName = "PRICEPROTECTNO";
            this.gridColumn22.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 5;
            this.gridColumn22.Width = 100;
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn23.AppearanceCell.Options.UseFont = true;
            this.gridColumn23.Caption = "Ngày hạch toán";
            this.gridColumn23.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColumn23.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn23.FieldName = "VOUCHERDATE";
            this.gridColumn23.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 6;
            this.gridColumn23.Width = 80;
            // 
            // gridColumn24
            // 
            this.gridColumn24.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn24.AppearanceCell.Options.UseFont = true;
            this.gridColumn24.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn24.AppearanceHeader.Options.UseFont = true;
            this.gridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn24.Caption = "Đã có hóa đơn";
            this.gridColumn24.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn24.FieldName = "ISINVOICE";
            this.gridColumn24.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 7;
            this.gridColumn24.Width = 70;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit1.ValueUnchecked = ((short)(0));
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Mã hóa đơn";
            this.gridColumn25.FieldName = "PINVOICEVOUCHERNO";
            this.gridColumn25.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 8;
            this.gridColumn25.Width = 150;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Số hóa đơn";
            this.gridColumn1.FieldName = "INVOICENO";
            this.gridColumn1.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 9;
            this.gridColumn1.Width = 70;
            // 
            // gridColumn27
            // 
            this.gridColumn27.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn27.AppearanceCell.Options.UseFont = true;
            this.gridColumn27.Caption = "Kho chứng từ";
            this.gridColumn27.FieldName = "STORENAME";
            this.gridColumn27.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 10;
            this.gridColumn27.Width = 200;
            // 
            // gridColumn46
            // 
            this.gridColumn46.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn46.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn46.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn46.Caption = "Trạng thái duyệt";
            this.gridColumn46.FieldName = "APPROVED";
            this.gridColumn46.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 11;
            this.gridColumn46.Width = 100;
            // 
            // gridColumn45
            // 
            this.gridColumn45.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn45.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn45.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn45.Caption = "Người duyệt";
            this.gridColumn45.FieldName = "APPROVEDUSER";
            this.gridColumn45.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 12;
            this.gridColumn45.Width = 180;
            // 
            // gridColumn44
            // 
            this.gridColumn44.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn44.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn44.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn44.Caption = "Ngày duyệt";
            this.gridColumn44.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.gridColumn44.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn44.FieldName = "APPROVEDDATE";
            this.gridColumn44.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 13;
            this.gridColumn44.Width = 120;
            // 
            // gridColumn43
            // 
            this.gridColumn43.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn43.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn43.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn43.Caption = "Đã tạo HĐ nội bộ lần 1";
            this.gridColumn43.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn43.FieldName = "ISHASINTERNALINVOICE_POSTDATE";
            this.gridColumn43.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 14;
            this.gridColumn43.Width = 95;
            // 
            // gridColumn42
            // 
            this.gridColumn42.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn42.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn42.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn42.Caption = "Đã tạo chứng từ điều chỉnh lần 1";
            this.gridColumn42.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn42.FieldName = "ISHASADJUSTVOUCHER_POSTDATE";
            this.gridColumn42.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 15;
            this.gridColumn42.Width = 116;
            // 
            // gridColumn41
            // 
            this.gridColumn41.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn41.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn41.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn41.Caption = "Đã phân bổ lần 1";
            this.gridColumn41.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn41.FieldName = "ISHASALLOCATEBYPOSTDATE";
            this.gridColumn41.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 16;
            this.gridColumn41.Width = 95;
            // 
            // gridColumn28
            // 
            this.gridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn28.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn28.Caption = "Đã tạo HĐ nội bộ lần 2";
            this.gridColumn28.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn28.FieldName = "ISHASINTERNALINVOICE";
            this.gridColumn28.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 17;
            this.gridColumn28.Width = 95;
            // 
            // gridColumn29
            // 
            this.gridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn29.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn29.Caption = "Đã xử lý giá vốn lần 2";
            this.gridColumn29.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn29.FieldName = "ISHASADJUSTVOUCHER";
            this.gridColumn29.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 18;
            this.gridColumn29.Width = 95;
            // 
            // gridColumn30
            // 
            this.gridColumn30.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn30.AppearanceCell.Options.UseFont = true;
            this.gridColumn30.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn30.AppearanceHeader.Options.UseFont = true;
            this.gridColumn30.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn30.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn30.Caption = "Nội dung";
            this.gridColumn30.FieldName = "NOTE";
            this.gridColumn30.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 19;
            this.gridColumn30.Width = 211;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Thành tiền";
            this.gridColumn2.DisplayFormat.FormatString = "N0";
            this.gridColumn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn2.FieldName = "MSTOTALAMOUNTNOVAT";
            this.gridColumn2.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MSTOTALAMOUNTNOVAT", "{0:n0}")});
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 20;
            this.gridColumn2.Width = 120;
            // 
            // gridColumn26
            // 
            this.gridColumn26.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn26.AppearanceCell.Options.UseFont = true;
            this.gridColumn26.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn26.AppearanceHeader.Options.UseFont = true;
            this.gridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn26.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn26.Caption = "Tổng tiền (Gồm VAT)";
            this.gridColumn26.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn26.FieldName = "MSTOTALAMOUNT";
            this.gridColumn26.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn26.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MSTOTALAMOUNT", "{0:N0}")});
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 21;
            this.gridColumn26.Width = 120;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.DisplayFormat.FormatString = "###,###,###,###,###";
            this.repositoryItemTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.Mask.EditMask = "###,###,###,###,###;";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColumn31
            // 
            this.gridColumn31.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn31.AppearanceCell.Options.UseFont = true;
            this.gridColumn31.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn31.AppearanceHeader.Options.UseFont = true;
            this.gridColumn31.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn31.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn31.Caption = "Người tạo";
            this.gridColumn31.FieldName = "CREATEDFULLNAME";
            this.gridColumn31.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 22;
            this.gridColumn31.Width = 180;
            // 
            // gridColumn32
            // 
            this.gridColumn32.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn32.AppearanceCell.Options.UseFont = true;
            this.gridColumn32.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn32.AppearanceHeader.Options.UseFont = true;
            this.gridColumn32.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn32.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn32.Caption = "Ngày tạo";
            this.gridColumn32.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColumn32.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn32.FieldName = "CREATEDDATE";
            this.gridColumn32.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 23;
            this.gridColumn32.Width = 120;
            // 
            // gridColumn33
            // 
            this.gridColumn33.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn33.AppearanceCell.Options.UseFont = true;
            this.gridColumn33.Caption = "Người cập nhật";
            this.gridColumn33.FieldName = "UPDATEDFULLNAME";
            this.gridColumn33.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn33.Width = 180;
            // 
            // gridColumn34
            // 
            this.gridColumn34.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn34.AppearanceCell.Options.UseFont = true;
            this.gridColumn34.Caption = "Ngày cập nhật";
            this.gridColumn34.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColumn34.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn34.FieldName = "UPDATEDDATE";
            this.gridColumn34.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn34.Width = 120;
            // 
            // gridColumn35
            // 
            this.gridColumn35.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn35.AppearanceCell.Options.UseFont = true;
            this.gridColumn35.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn35.AppearanceHeader.Options.UseFont = true;
            this.gridColumn35.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn35.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn35.Caption = "Người hủy";
            this.gridColumn35.FieldName = "DELETEDFULLNAME";
            this.gridColumn35.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn35.Width = 166;
            // 
            // gridColumn36
            // 
            this.gridColumn36.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn36.AppearanceCell.Options.UseFont = true;
            this.gridColumn36.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn36.AppearanceHeader.Options.UseFont = true;
            this.gridColumn36.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn36.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn36.Caption = "Ngày hủy";
            this.gridColumn36.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColumn36.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn36.FieldName = "DELETEDDATE";
            this.gridColumn36.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn36.Width = 120;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Mã sản phẩm";
            this.gridColumn3.FieldName = "PRODUCTID";
            this.gridColumn3.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 24;
            this.gridColumn3.Width = 120;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Tên sản phẩm";
            this.gridColumn4.FieldName = "PRODUCTNAME";
            this.gridColumn4.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 25;
            this.gridColumn4.Width = 200;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Số lượng hỗ trợ lần 1";
            this.gridColumn5.DisplayFormat.FormatString = "N0";
            this.gridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn5.FieldName = "INSTOCKQUANTITY";
            this.gridColumn5.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "INSTOCKQUANTITY", "{0:N0}")});
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 26;
            this.gridColumn5.Width = 130;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Đơn giá hỗ trợ (Chưa VAT) lần 1";
            this.gridColumn40.DisplayFormat.FormatString = "N0";
            this.gridColumn40.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn40.FieldName = "PRICENOVAT";
            this.gridColumn40.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 27;
            this.gridColumn40.Width = 130;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Đơn giá hỗ trợ (Gồm VAT) lần 1";
            this.gridColumn7.DisplayFormat.FormatString = "N1";
            this.gridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn7.FieldName = "PRICE";
            this.gridColumn7.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 28;
            this.gridColumn7.Width = 130;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Thành tiền lần 1";
            this.gridColumn8.DisplayFormat.FormatString = "N0";
            this.gridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn8.FieldName = "TOTALAMOUNTNOVAT";
            this.gridColumn8.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTNOVAT", "{0:N0}")});
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 29;
            this.gridColumn8.Width = 130;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Tổng tiền (Gồm VAT) lần 1";
            this.gridColumn9.DisplayFormat.FormatString = "n0";
            this.gridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn9.FieldName = "TOTALAMOUNT";
            this.gridColumn9.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn9.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNT", "{0:n0}")});
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 30;
            this.gridColumn9.Width = 130;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "SL tồn tại CN trung tâm lần 1";
            this.gridColumn10.DisplayFormat.FormatString = "N0";
            this.gridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn10.FieldName = "QUANTITYSTOCK";
            this.gridColumn10.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn10.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITYSTOCK", "{0:n0}")});
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 32;
            this.gridColumn10.Width = 130;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "SL tồn tại CN tỉnh lần 1";
            this.gridColumn11.DisplayFormat.FormatString = "N0";
            this.gridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn11.FieldName = "QUANTITYINSTOCKNONISCENTER";
            this.gridColumn11.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITYINSTOCKNONISCENTER", "{0:n0}")});
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 33;
            this.gridColumn11.Width = 130;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "SL bán tại CN trung tâm lần 1";
            this.gridColumn12.DisplayFormat.FormatString = "N0";
            this.gridColumn12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn12.FieldName = "QUANTITYBUY";
            this.gridColumn12.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn12.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITYBUY", "{0:n0}")});
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 34;
            this.gridColumn12.Width = 130;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "SL bán tại CN tỉnh lần 1";
            this.gridColumn13.DisplayFormat.FormatString = "N0";
            this.gridColumn13.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn13.FieldName = "QUANTITYBUYNONISCENTER";
            this.gridColumn13.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn13.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITYBUYNONISCENTER", "{0:n0}")});
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 35;
            this.gridColumn13.Width = 130;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "SL ghi nhận doanh thu khác lần 1";
            this.gridColumn14.DisplayFormat.FormatString = "N0";
            this.gridColumn14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn14.FieldName = "QUANTITYOTHERINCOME";
            this.gridColumn14.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn14.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITYOTHERINCOME", "{0:n0}")});
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 36;
            this.gridColumn14.Width = 130;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "GT tồn tại CN trung tâm lần 1";
            this.gridColumn15.DisplayFormat.FormatString = "N0";
            this.gridColumn15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn15.FieldName = "TOTALAMOUNTSTOCK";
            this.gridColumn15.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn15.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTSTOCK", "{0:n0}")});
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 37;
            this.gridColumn15.Width = 130;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "GT tồn tại CN tỉnh lần 1";
            this.gridColumn16.DisplayFormat.FormatString = "N0";
            this.gridColumn16.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn16.FieldName = "TOTALAMOUNTSTOCKNONISCENTER";
            this.gridColumn16.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn16.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTSTOCKNONISCENTER", "{0:n0}")});
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 38;
            this.gridColumn16.Width = 130;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "GT bán tại CN trung tâm lần 1";
            this.gridColumn37.DisplayFormat.FormatString = "N0";
            this.gridColumn37.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn37.FieldName = "TOTALAMOUNTBUY";
            this.gridColumn37.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn37.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTBUY", "{0:n0}")});
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 39;
            this.gridColumn37.Width = 130;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "GT bán tại CN tỉnh lần 1";
            this.gridColumn38.DisplayFormat.FormatString = "N0";
            this.gridColumn38.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn38.FieldName = "TOTALAMOUNTBUYNONISCENTER";
            this.gridColumn38.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn38.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTBUYNONISCENTER", "{0:N0}")});
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 40;
            this.gridColumn38.Width = 130;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "GT ghi nhận doanh thu khác lần 1";
            this.gridColumn39.DisplayFormat.FormatString = "N0";
            this.gridColumn39.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn39.FieldName = "TOTALAMOUNTOTHERINCOME";
            this.gridColumn39.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn39.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTOTHERINCOME", "{0:N0}")});
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 41;
            this.gridColumn39.Width = 130;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn6.Caption = "Số lượng hỗ trợ lần 2";
            this.gridColumn6.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn6.FieldName = "QUANTITY";
            this.gridColumn6.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY", "{0:#,##0}")});
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 42;
            this.gridColumn6.Width = 130;
            // 
            // gridColumn47
            // 
            this.gridColumn47.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn47.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn47.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn47.Caption = "Đơn giá hỗ trợ (Chưa VAT) lần 2";
            this.gridColumn47.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn47.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn47.FieldName = "PRICENOVATVOUCHER";
            this.gridColumn47.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn47.Visible = true;
            this.gridColumn47.VisibleIndex = 43;
            this.gridColumn47.Width = 130;
            // 
            // gridColumn48
            // 
            this.gridColumn48.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn48.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn48.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn48.Caption = "Đơn giá hỗ trợ (Gồm VAT) lần 2";
            this.gridColumn48.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn48.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn48.FieldName = "PRICEVOUCHER";
            this.gridColumn48.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 44;
            this.gridColumn48.Width = 130;
            // 
            // gridColumn49
            // 
            this.gridColumn49.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn49.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn49.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn49.Caption = "Thành tiền lần 2";
            this.gridColumn49.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn49.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn49.FieldName = "TOTALAMOUNTNOVATVOUCHER";
            this.gridColumn49.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn49.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTNOVATVOUCHER", "{0:#,##0}")});
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 45;
            this.gridColumn49.Width = 130;
            // 
            // gridColumn50
            // 
            this.gridColumn50.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn50.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn50.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn50.Caption = "Tổng tiền (Gồm VAT) lần 2";
            this.gridColumn50.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn50.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn50.FieldName = "TOTALAMOUNTVOUCHER";
            this.gridColumn50.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn50.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTVOUCHER", "{0:#,##0}")});
            this.gridColumn50.Visible = true;
            this.gridColumn50.VisibleIndex = 46;
            this.gridColumn50.Width = 130;
            // 
            // gridColumn51
            // 
            this.gridColumn51.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn51.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn51.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn51.Caption = "SL tồn tại CN trung tâm lần 2";
            this.gridColumn51.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn51.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn51.FieldName = "QUANTITYSTOCKVOUCHER";
            this.gridColumn51.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn51.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITYSTOCKVOUCHER", "{0:#,##0}")});
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 48;
            this.gridColumn51.Width = 130;
            // 
            // gridColumn52
            // 
            this.gridColumn52.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn52.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn52.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn52.Caption = "SL bán tại CN tỉnh lần 2";
            this.gridColumn52.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn52.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn52.FieldName = "QUANTITYINSTOCKNONISCENTER1";
            this.gridColumn52.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn52.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITYINSTOCKNONISCENTERVOUCHER", "{0:#,##0}")});
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 49;
            this.gridColumn52.Width = 130;
            // 
            // gridColumn59
            // 
            this.gridColumn59.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn59.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn59.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn59.Caption = "SL bán tại CN trung tâm lần 2";
            this.gridColumn59.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn59.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn59.FieldName = "QUANTITYBUYVOUCHER";
            this.gridColumn59.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn59.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITYBUYVOUCHER", "{0:#,##0}")});
            this.gridColumn59.Visible = true;
            this.gridColumn59.VisibleIndex = 50;
            this.gridColumn59.Width = 130;
            // 
            // gridColumn60
            // 
            this.gridColumn60.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn60.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn60.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn60.Caption = "SL bán tại CN tỉnh lần 2";
            this.gridColumn60.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn60.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn60.FieldName = "QUANTITYBUYNONISCENTER1";
            this.gridColumn60.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn60.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITYBUYNONISCENTERVOUCHER", "{0:#,##0}")});
            this.gridColumn60.Visible = true;
            this.gridColumn60.VisibleIndex = 51;
            this.gridColumn60.Width = 130;
            // 
            // gridColumn53
            // 
            this.gridColumn53.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn53.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn53.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn53.Caption = "SL ghi nhận doanh thu khác lần 2";
            this.gridColumn53.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn53.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn53.FieldName = "QUANTITYOTHERINCOMEVOUCHER";
            this.gridColumn53.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.AllowEdit = false;
            this.gridColumn53.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn53.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITYOTHERINCOMEVOUCHER", "{0:#,##0}")});
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 52;
            this.gridColumn53.Width = 130;
            // 
            // gridColumn54
            // 
            this.gridColumn54.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn54.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn54.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn54.Caption = "GT tồn tại CN trung tâm lần 2";
            this.gridColumn54.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn54.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn54.FieldName = "TOTALAMOUNTSTOCKVOUCHER";
            this.gridColumn54.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.AllowEdit = false;
            this.gridColumn54.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn54.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTSTOCKVOUCHER", "{0:#,##0}")});
            this.gridColumn54.Visible = true;
            this.gridColumn54.VisibleIndex = 53;
            this.gridColumn54.Width = 130;
            // 
            // gridColumn55
            // 
            this.gridColumn55.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn55.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn55.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn55.Caption = "GT tồn tại CN tỉnh lần 2";
            this.gridColumn55.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn55.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn55.FieldName = "TOTALAMOUNTSTOCKNONISCENTER1";
            this.gridColumn55.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn55.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTSTOCKNONISCENTERVOUCHER", "{0:#,##0}")});
            this.gridColumn55.Visible = true;
            this.gridColumn55.VisibleIndex = 54;
            this.gridColumn55.Width = 130;
            // 
            // gridColumn56
            // 
            this.gridColumn56.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn56.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn56.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn56.Caption = "GT bán tại CN trung tâm lần 2";
            this.gridColumn56.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn56.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn56.FieldName = "TOTALAMOUNTBUYVOUCHER";
            this.gridColumn56.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn56.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTBUYVOUCHER", "{0:#,##0}")});
            this.gridColumn56.Visible = true;
            this.gridColumn56.VisibleIndex = 55;
            this.gridColumn56.Width = 130;
            // 
            // gridColumn57
            // 
            this.gridColumn57.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn57.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn57.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn57.Caption = "GT tồn tại CN tỉnh lần 2";
            this.gridColumn57.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn57.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn57.FieldName = "TOTALAMOUNTBUYNONISCENTER1";
            this.gridColumn57.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn57.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTBUYNONISCENTERVOUCHER", "{0:#,##0}")});
            this.gridColumn57.Visible = true;
            this.gridColumn57.VisibleIndex = 56;
            this.gridColumn57.Width = 130;
            // 
            // gridColumn58
            // 
            this.gridColumn58.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn58.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn58.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn58.Caption = "GT ghi nhận doanh thu khác lần 2";
            this.gridColumn58.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn58.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn58.FieldName = "TOTALAMOUNTOTHERINCOMEVOUCHER";
            this.gridColumn58.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn58.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTOTHERINCOMEVOUCHER", "{0:#,##0}")});
            this.gridColumn58.Visible = true;
            this.gridColumn58.VisibleIndex = 57;
            this.gridColumn58.Width = 130;
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "SL chưa có phiếu nhập lần 2";
            this.gridColumn61.FieldName = "QUANTITYNOINPUTVOUCHER";
            this.gridColumn61.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.AllowEdit = false;
            this.gridColumn61.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn61.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITYNOINPUTVOUCHER", "{0:#,##0}")});
            this.gridColumn61.Visible = true;
            this.gridColumn61.VisibleIndex = 47;
            this.gridColumn61.Width = 130;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "SL chưa có phiếu nhập lần 1";
            this.gridColumn62.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn62.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn62.FieldName = "QUANTITYNOINPUT";
            this.gridColumn62.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn62.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITYNOINPUT", "{0:#,##0}")});
            this.gridColumn62.Visible = true;
            this.gridColumn62.VisibleIndex = 31;
            this.gridColumn62.Width = 130;
            // 
            // frmPriceStockManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1139, 506);
            this.Controls.Add(this.grdData);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmPriceStockManager";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý hỗ trợ giá tồn";
            this.Load += new System.EventHandler(this.frmPriceStockManager_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.mnuAction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cboStatus;
        private System.Windows.Forms.DateTimePicker dteStockFromDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DateTimePicker dteStockToDate;
        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuViewItem;
        private System.Windows.Forms.ToolStripMenuItem mnuCreateItem;
        private System.Windows.Forms.CheckBox chkIsDeleted;
        private System.Windows.Forms.Button btnExportExcel;
        private System.Windows.Forms.Label label3;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStore;
        private System.Windows.Forms.Label label1;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboCompany;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcel;
        private System.Windows.Forms.ComboBox cboSearchBy;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtKeywords;
        private System.Windows.Forms.CheckBox chkSearchByProtectDate;
        private System.Windows.Forms.DateTimePicker dteProtectToDate;
        private System.Windows.Forms.DateTimePicker dteProtectFromDate;
        private System.Windows.Forms.CheckBox chkSearchByStockDate;
        private System.Windows.Forms.CheckBox chkSearchByPostDate;
        private System.Windows.Forms.CheckBox chkSearchByInputDate;
        private System.Windows.Forms.DateTimePicker dteInputToDate;
        private System.Windows.Forms.DateTimePicker dteInputFromDate;
        private System.Windows.Forms.DateTimePicker dtePostToDate;
        private System.Windows.Forms.DateTimePicker dtePostFromDate;
        private System.Windows.Forms.CheckBox chkIsHasVATPinvoice;
        private System.Windows.Forms.CheckBox chkIsAdjustCostPrice;
        private System.Windows.Forms.CheckBox chkIsViewDetail;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboPriceProtectType;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private System.Windows.Forms.CheckBox chkSearchByVoucherDate;
        private System.Windows.Forms.DateTimePicker dteVoucherToDate;
        private System.Windows.Forms.DateTimePicker dteVoucherFromDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
    }
}