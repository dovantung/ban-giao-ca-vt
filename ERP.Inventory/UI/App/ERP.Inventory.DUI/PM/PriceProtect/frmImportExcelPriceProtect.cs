﻿using ERP.Inventory.PLC.WSPriceProtect;
using ERP.Inventory.PLC.WSProductInStock;
using ERP.MasterData.DUI.Search;
using Library.AppCore.LoadControls;
using Library.AppCore.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.PM.PriceProtect
{
    public partial class frmImportExcelPriceProtect : Form
    {

        public frmImportExcelPriceProtect(DataTable dtbData)
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
            grdData.DataSource = dtbData;
            grdData.RefreshDataSource();
        }
        private void frmImportExcelPriceProtect_Load(object sender, EventArgs e)
        {
            InitControl();
        }
        private void InitControl()
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
        }

        private void grdViewData_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column.FieldName != string.Empty)
            {
                DevExpress.XtraGrid.Views.Grid.GridView View = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                bool bolIsError = Convert.ToBoolean(View.GetRowCellValue(e.RowHandle, View.Columns["ISERROR"]));
                if (bolIsError)
                {
                    e.Appearance.BackColor = Color.Pink;
                }
            }
        }
    }
}

