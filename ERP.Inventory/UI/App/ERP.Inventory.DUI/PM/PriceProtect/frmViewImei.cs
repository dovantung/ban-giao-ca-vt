﻿using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.PM.PriceProtect
{
    public partial class frmViewImei : Form
    {
        public frmViewImei()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
        }
        private DataTable dtbSource = null;
        public DataTable DtbSource
        {
            get { return dtbSource; }
            set { dtbSource = value; }
        }

        private int intPriceProtectType = 1;
        public int PriceProtectType
        {
            get { return intPriceProtectType; }
            set { intPriceProtectType = value; }
        }

        private void frmViewImei_Load(object sender, EventArgs e)
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);

            string strIMEIList = string.Empty;
           


            string[] FieldName = new string[] { "StoreID", "StoreName", "ProductIMEI", "InputVoucherID", "InputDate", "IsValid" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, FieldName);
            grdViewData.Columns["ProductIMEI"].Caption = "IMEI";
            grdViewData.Columns["InputVoucherID"].Caption = "Mã phiếu nhập";
            grdViewData.Columns["InputDate"].Caption = "Ngày nhập";
            grdViewData.Columns["StoreID"].Caption = "Mã kho";
            grdViewData.Columns["StoreName"].Caption = "Tên kho";

            grdViewData.Columns["ProductIMEI"].Width = 160;
            grdViewData.Columns["InputVoucherID"].Width = 150;
            grdViewData.Columns["InputDate"].Width = 140;
            grdViewData.Columns["StoreID"].Width = 80;
            grdViewData.Columns["StoreName"].Width = 350;

            grdViewData.Columns["IsValid"].Visible = false;

            grdViewData.Columns["InputDate"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            grdViewData.Columns["InputDate"].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";

            grdViewData.Columns["StoreName"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            grdViewData.Columns["StoreName"].SummaryItem.DisplayFormat = "Tổng cộng:";

            grdViewData.Columns["ProductIMEI"].SummaryItem.FieldName = "ProductIMEI";
            grdViewData.Columns["ProductIMEI"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            grdViewData.Columns["ProductIMEI"].SummaryItem.DisplayFormat = "{0}";

            if (PriceProtectType == 4)
            {
                grdViewData.Columns["StoreID"].Visible = false;
                grdViewData.Columns["StoreName"].Visible = false;
                grdViewData.Columns["InputDate"].Visible = false;
                grdViewData.Columns["InputVoucherID"].Visible = false;
                grdViewData.Columns["InputDate"].Visible = false;
            }

            grdData.DataSource = dtbSource;

        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grdViewData_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            //GridView View = sender as GridView;
            //if (e.RowHandle >= 0)
            //{
            //    DataRow row = View.GetDataRow(e.RowHandle);
            //    if (Convert.ToBoolean(row["IsValid"].ToString()) == false)
            //    {
            //        e.Appearance.BackColor = Color.Salmon;
            //        e.Appearance.BackColor2 = Color.SeaShell;
            //    }
            //}
        }

        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            mnuItemExportExcel.Enabled = grdViewData.DataRowCount > 0;
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }

        private void frmViewImei_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.Escape)
                this.Close();
        }
    }
}
