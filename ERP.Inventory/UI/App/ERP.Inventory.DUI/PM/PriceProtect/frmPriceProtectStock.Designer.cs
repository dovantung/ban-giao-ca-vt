﻿namespace ERP.Inventory.DUI.PM.PriceProtect
{
    partial class frmPriceProtectStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemViewIMEI = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemInputIMEI = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemDel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemImportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemImportValues = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemExportTemplate = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.cboStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.label11 = new System.Windows.Forms.Label();
            this.cboInvoiceTransactionType = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.dtpVoucher = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpCallStock = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpInputToDate = new System.Windows.Forms.DateTimePicker();
            this.btnSearchBill = new System.Windows.Forms.Button();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.dtpInputFromDate = new System.Windows.Forms.DateTimePicker();
            this.dtpPriceProtectDate = new System.Windows.Forms.DateTimePicker();
            this.txtPriceProtectID = new System.Windows.Forms.TextBox();
            this.btnSearchProduct = new System.Windows.Forms.Button();
            this.lbBarcode = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbPriceProtectNO = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnReviewOrder = new DevExpress.XtraEditors.DropDownButton();
            this.popReivewStatus = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnUndo = new System.Windows.Forms.Button();
            this.btnCalStock = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.cboPriceProtectType = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.dtpPostdate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.chkIsSupplierCall = new System.Windows.Forms.CheckBox();
            this.txtPriceProtectNO = new System.Windows.Forms.TextBox();
            this.tabCustomer = new System.Windows.Forms.TabPage();
            this.grdCustomer = new DevExpress.XtraGrid.GridControl();
            this.mnuCustomer = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemAddCustomer = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemDeleteCustomer = new System.Windows.Forms.ToolStripMenuItem();
            this.grvCustomer = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repChecked = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textbox400 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textbox300 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textbox100 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textbox20 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colIsDefault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabAttachment = new System.Windows.Forms.TabPage();
            this.grdAttachment = new DevExpress.XtraGrid.GridControl();
            this.mnuAttachment = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuAddAttachment = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDelAttachment = new System.Windows.Forms.ToolStripMenuItem();
            this.grvAttachment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colView = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnView = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colDownload = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDownload = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPriceProtect1 = new System.Windows.Forms.TabPage();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInStockQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repPrice = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPriceNoVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repTotalAmount = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repVAT = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colTotalAmountNoVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVATAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityNoInput = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityStock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityInStockNonIsCenter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityBuy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityBuyNonIsCenter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityOtherIncome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountStock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountStockNonIsCenter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountBuy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountBuyNonIsCenter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountOtherIncome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabPriceProtect2 = new System.Windows.Forms.TabPage();
            this.grdDataPriceProtect1 = new DevExpress.XtraGrid.GridControl();
            this.mnuPriceProtect1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemViewIMEIProtect1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemInputIMEIProtect1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemImportValues1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemExportTemplateProtect1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemExportExcelProtect1 = new System.Windows.Forms.ToolStripMenuItem();
            this.grvDataPriceProtect1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuatity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPriceNoVatVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTotalAmountNoVatVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colVatAmountVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityNoInputVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityStockVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityInStockNonIsCenterVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityBuyVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityBuyNonIsCenterVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityOtherIncomeVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountStockVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountStockNonIsCenterVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountBuyVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountBuyNonIsCenterVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountOtherIncomeVoucher = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabDifferent = new System.Windows.Forms.TabPage();
            this.grdDifference = new DevExpress.XtraGrid.GridControl();
            this.grvDifference = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn36 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colQuantityNoInputDifference = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn20 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn21 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn22 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn23 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn24 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn25 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn26 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn27 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.mnuAction.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popReivewStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            this.tabCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCustomer)).BeginInit();
            this.mnuCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChecked)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textbox400)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textbox300)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textbox100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textbox20)).BeginInit();
            this.tabAttachment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAttachment)).BeginInit();
            this.mnuAttachment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvAttachment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownload)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPriceProtect1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repVAT)).BeginInit();
            this.tabPriceProtect2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDataPriceProtect1)).BeginInit();
            this.mnuPriceProtect1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvDataPriceProtect1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            this.tabDifferent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDifference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDifference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuAction
            // 
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemViewIMEI,
            this.mnuItemInputIMEI,
            this.mnuItemDel,
            this.toolStripSeparator1,
            this.mnuItemImportExcel,
            this.mnuItemImportValues,
            this.mnuItemExportTemplate,
            this.toolStripSeparator2,
            this.mnuItemExportExcel});
            this.mnuAction.Name = "mnuAction";
            this.mnuAction.Size = new System.Drawing.Size(172, 170);
            this.mnuAction.Opening += new System.ComponentModel.CancelEventHandler(this.mnuAction_Opening);
            // 
            // mnuItemViewIMEI
            // 
            this.mnuItemViewIMEI.Image = global::ERP.Inventory.DUI.Properties.Resources.ViewOutVoucher;
            this.mnuItemViewIMEI.Name = "mnuItemViewIMEI";
            this.mnuItemViewIMEI.Size = new System.Drawing.Size(171, 22);
            this.mnuItemViewIMEI.Text = "Xem chi tiết IMEI";
            this.mnuItemViewIMEI.Click += new System.EventHandler(this.mnuItemViewIMEI_Click);
            // 
            // mnuItemInputIMEI
            // 
            this.mnuItemInputIMEI.Enabled = false;
            this.mnuItemInputIMEI.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.mnuItemInputIMEI.Name = "mnuItemInputIMEI";
            this.mnuItemInputIMEI.Size = new System.Drawing.Size(171, 22);
            this.mnuItemInputIMEI.Text = "Nhập IMEI";
            this.mnuItemInputIMEI.Click += new System.EventHandler(this.mnuItemInputIMEI_Click);
            // 
            // mnuItemDel
            // 
            this.mnuItemDel.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.mnuItemDel.Name = "mnuItemDel";
            this.mnuItemDel.Size = new System.Drawing.Size(171, 22);
            this.mnuItemDel.Text = "Xóa";
            this.mnuItemDel.Click += new System.EventHandler(this.mnuItemDel_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(168, 6);
            // 
            // mnuItemImportExcel
            // 
            this.mnuItemImportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemImportExcel.Name = "mnuItemImportExcel";
            this.mnuItemImportExcel.Size = new System.Drawing.Size(171, 22);
            this.mnuItemImportExcel.Text = "Nhập từ Excel";
            this.mnuItemImportExcel.Click += new System.EventHandler(this.mnuItemImportExcel_Click);
            // 
            // mnuItemImportValues
            // 
            this.mnuItemImportValues.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemImportValues.Name = "mnuItemImportValues";
            this.mnuItemImportValues.Size = new System.Drawing.Size(171, 22);
            this.mnuItemImportValues.Text = "Nhập giá trị hỗ trợ";
            this.mnuItemImportValues.Click += new System.EventHandler(this.mnuItemImportValues_Click);
            // 
            // mnuItemExportTemplate
            // 
            this.mnuItemExportTemplate.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportTemplate.Name = "mnuItemExportTemplate";
            this.mnuItemExportTemplate.Size = new System.Drawing.Size(171, 22);
            this.mnuItemExportTemplate.Text = "Xuất excel mẫu";
            this.mnuItemExportTemplate.Click += new System.EventHandler(this.mnuItemExportTemplate_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(168, 6);
            // 
            // mnuItemExportExcel
            // 
            this.mnuItemExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportExcel.Name = "mnuItemExportExcel";
            this.mnuItemExportExcel.Size = new System.Drawing.Size(171, 22);
            this.mnuItemExportExcel.Text = "Xuất Excel";
            this.mnuItemExportExcel.Click += new System.EventHandler(this.mnuItemExportExcel_Click);
            // 
            // cboStoreID
            // 
            this.cboStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreID.Location = new System.Drawing.Point(106, 60);
            this.cboStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreID.Name = "cboStoreID";
            this.cboStoreID.Size = new System.Drawing.Size(200, 24);
            this.cboStoreID.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 62);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 16);
            this.label11.TabIndex = 17;
            this.label11.Text = "Kho chứng từ:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cboInvoiceTransactionType
            // 
            this.cboInvoiceTransactionType.Enabled = false;
            this.cboInvoiceTransactionType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInvoiceTransactionType.Location = new System.Drawing.Point(699, 60);
            this.cboInvoiceTransactionType.Margin = new System.Windows.Forms.Padding(0);
            this.cboInvoiceTransactionType.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInvoiceTransactionType.Name = "cboInvoiceTransactionType";
            this.cboInvoiceTransactionType.Size = new System.Drawing.Size(339, 24);
            this.cboInvoiceTransactionType.TabIndex = 22;
            // 
            // dtpVoucher
            // 
            this.dtpVoucher.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dtpVoucher.CustomFormat = "dd/MM/yyyy";
            this.dtpVoucher.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpVoucher.Location = new System.Drawing.Point(931, 6);
            this.dtpVoucher.Name = "dtpVoucher";
            this.dtpVoucher.Size = new System.Drawing.Size(107, 22);
            this.dtpVoucher.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(810, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 16);
            this.label10.TabIndex = 6;
            this.label10.Text = "Ngày hạch toán 2:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dtpCallStock
            // 
            this.dtpCallStock.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dtpCallStock.CustomFormat = "dd/MM/yyyy";
            this.dtpCallStock.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCallStock.Location = new System.Drawing.Point(699, 33);
            this.dtpCallStock.Name = "dtpCallStock";
            this.dtpCallStock.Size = new System.Drawing.Size(107, 22);
            this.dtpCallStock.TabIndex = 14;
            this.dtpCallStock.ValueChanged += new System.EventHandler(this.dtpCallStock_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(578, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 16);
            this.label9.TabIndex = 13;
            this.label9.Text = "Ngày chốt tồn:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(578, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 16);
            this.label4.TabIndex = 21;
            this.label4.Text = "Loại nghiệp vụ:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Loại hỗ trợ:";
            // 
            // dtpInputToDate
            // 
            this.dtpInputToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpInputToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInputToDate.Location = new System.Drawing.Point(488, 33);
            this.dtpInputToDate.Name = "dtpInputToDate";
            this.dtpInputToDate.Size = new System.Drawing.Size(86, 22);
            this.dtpInputToDate.TabIndex = 12;
            this.dtpInputToDate.ValueChanged += new System.EventHandler(this.dtpStockDateTo_ValueChanged);
            // 
            // btnSearchBill
            // 
            this.btnSearchBill.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.btnSearchBill.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearchBill.Location = new System.Drawing.Point(315, 141);
            this.btnSearchBill.Name = "btnSearchBill";
            this.btnSearchBill.Size = new System.Drawing.Size(131, 25);
            this.btnSearchBill.TabIndex = 29;
            this.btnSearchBill.Text = "    Chọn hóa đơn";
            this.btnSearchBill.UseVisualStyleBackColor = true;
            this.btnSearchBill.Visible = false;
            this.btnSearchBill.Click += new System.EventHandler(this.btnSearchBill_Click);
            // 
            // cboStatus
            // 
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatus.Enabled = false;
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.Items.AddRange(new object[] {
            "Khởi tạo",
            "Đã có biên bản"});
            this.cboStatus.Location = new System.Drawing.Point(395, 60);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Size = new System.Drawing.Size(179, 24);
            this.cboStatus.TabIndex = 20;
            this.cboStatus.SelectedIndexChanged += new System.EventHandler(this.cboStatus_SelectedIndexChanged);
            // 
            // dtpInputFromDate
            // 
            this.dtpInputFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpInputFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInputFromDate.Location = new System.Drawing.Point(395, 33);
            this.dtpInputFromDate.Name = "dtpInputFromDate";
            this.dtpInputFromDate.Size = new System.Drawing.Size(87, 22);
            this.dtpInputFromDate.TabIndex = 11;
            this.dtpInputFromDate.ValueChanged += new System.EventHandler(this.dtpStockDate_ValueChanged);
            // 
            // dtpPriceProtectDate
            // 
            this.dtpPriceProtectDate.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dtpPriceProtectDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpPriceProtectDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPriceProtectDate.Location = new System.Drawing.Point(395, 6);
            this.dtpPriceProtectDate.Name = "dtpPriceProtectDate";
            this.dtpPriceProtectDate.Size = new System.Drawing.Size(179, 22);
            this.dtpPriceProtectDate.TabIndex = 3;
            // 
            // txtPriceProtectID
            // 
            this.txtPriceProtectID.BackColor = System.Drawing.SystemColors.Info;
            this.txtPriceProtectID.Location = new System.Drawing.Point(106, 33);
            this.txtPriceProtectID.Name = "txtPriceProtectID";
            this.txtPriceProtectID.ReadOnly = true;
            this.txtPriceProtectID.Size = new System.Drawing.Size(200, 22);
            this.txtPriceProtectID.TabIndex = 9;
            // 
            // btnSearchProduct
            // 
            this.btnSearchProduct.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearchProduct.Location = new System.Drawing.Point(281, 141);
            this.btnSearchProduct.Name = "btnSearchProduct";
            this.btnSearchProduct.Size = new System.Drawing.Size(25, 25);
            this.btnSearchProduct.TabIndex = 28;
            this.btnSearchProduct.Click += new System.EventHandler(this.btnSearchProduct_Click);
            // 
            // lbBarcode
            // 
            this.lbBarcode.AutoSize = true;
            this.lbBarcode.Location = new System.Drawing.Point(8, 146);
            this.lbBarcode.Name = "lbBarcode";
            this.lbBarcode.Size = new System.Drawing.Size(63, 16);
            this.lbBarcode.TabIndex = 26;
            this.lbBarcode.Text = "Barcode:";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(106, 89);
            this.txtNote.MaxLength = 1000;
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNote.Size = new System.Drawing.Size(466, 50);
            this.txtNote.TabIndex = 24;
            // 
            // txtBarcode
            // 
            this.txtBarcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold);
            this.txtBarcode.Location = new System.Drawing.Point(106, 143);
            this.txtBarcode.MaxLength = 50;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(176, 21);
            this.txtBarcode.TabIndex = 27;
            this.txtBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarcode_KeyPress);
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Location = new System.Drawing.Point(8, 35);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(66, 16);
            this.lblCustomer.TabIndex = 8;
            this.lblCustomer.Text = "Mã phiếu:";
            this.lblCustomer.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 16);
            this.label8.TabIndex = 23;
            this.label8.Text = "Nội dung:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(312, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ngày lập:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(312, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 16);
            this.label7.TabIndex = 19;
            this.label7.Text = "Trạng thái:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(312, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Ngày nhập:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbPriceProtectNO
            // 
            this.lbPriceProtectNO.AutoSize = true;
            this.lbPriceProtectNO.Location = new System.Drawing.Point(810, 35);
            this.lbPriceProtectNO.Name = "lbPriceProtectNO";
            this.lbPriceProtectNO.Size = new System.Drawing.Size(83, 16);
            this.lbPriceProtectNO.TabIndex = 15;
            this.lbPriceProtectNO.Text = "Số biên bản:";
            this.lbPriceProtectNO.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Info;
            this.panel3.Controls.Add(this.btnReviewOrder);
            this.panel3.Controls.Add(this.btnUpdate);
            this.panel3.Controls.Add(this.btnUndo);
            this.panel3.Controls.Add(this.btnCalStock);
            this.panel3.Controls.Add(this.btnDelete);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 582);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1074, 35);
            this.panel3.TabIndex = 2;
            // 
            // btnReviewOrder
            // 
            this.btnReviewOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReviewOrder.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReviewOrder.Appearance.Options.UseFont = true;
            this.btnReviewOrder.DropDownControl = this.popReivewStatus;
            this.btnReviewOrder.Location = new System.Drawing.Point(673, 7);
            this.btnReviewOrder.Name = "btnReviewOrder";
            this.btnReviewOrder.Size = new System.Drawing.Size(95, 25);
            this.btnReviewOrder.TabIndex = 5;
            this.btnReviewOrder.Text = "Duyệt";
            // 
            // popReivewStatus
            // 
            this.popReivewStatus.Manager = this.barManager1;
            this.popReivewStatus.Name = "popReivewStatus";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MaxItemId = 0;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1074, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 617);
            this.barDockControlBottom.Size = new System.Drawing.Size(1074, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 617);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1074, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 617);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(875, 6);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(95, 25);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "   Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUndo.Image = global::ERP.Inventory.DUI.Properties.Resources.undo;
            this.btnUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUndo.Location = new System.Drawing.Point(976, 6);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(95, 25);
            this.btnUndo.TabIndex = 3;
            this.btnUndo.Text = "   Bỏ qua";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnCalStock
            // 
            this.btnCalStock.Enabled = false;
            this.btnCalStock.Image = global::ERP.Inventory.DUI.Properties.Resources.cal;
            this.btnCalStock.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCalStock.Location = new System.Drawing.Point(7, 6);
            this.btnCalStock.Name = "btnCalStock";
            this.btnCalStock.Size = new System.Drawing.Size(94, 24);
            this.btnCalStock.TabIndex = 0;
            this.btnCalStock.Text = "      Chốt tồn";
            this.btnCalStock.UseVisualStyleBackColor = true;
            this.btnCalStock.Click += new System.EventHandler(this.btnCalStock_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(774, 6);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(95, 25);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "    Hủy phiếu";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabGeneral);
            this.tabControl1.Controls.Add(this.tabCustomer);
            this.tabControl1.Controls.Add(this.tabAttachment);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1074, 200);
            this.tabControl1.TabIndex = 0;
            // 
            // tabGeneral
            // 
            this.tabGeneral.Controls.Add(this.cboPriceProtectType);
            this.tabGeneral.Controls.Add(this.dtpPostdate);
            this.tabGeneral.Controls.Add(this.label2);
            this.tabGeneral.Controls.Add(this.chkIsSupplierCall);
            this.tabGeneral.Controls.Add(this.txtPriceProtectNO);
            this.tabGeneral.Controls.Add(this.label3);
            this.tabGeneral.Controls.Add(this.cboStoreID);
            this.tabGeneral.Controls.Add(this.lbPriceProtectNO);
            this.tabGeneral.Controls.Add(this.label11);
            this.tabGeneral.Controls.Add(this.label5);
            this.tabGeneral.Controls.Add(this.cboInvoiceTransactionType);
            this.tabGeneral.Controls.Add(this.dtpVoucher);
            this.tabGeneral.Controls.Add(this.label10);
            this.tabGeneral.Controls.Add(this.label7);
            this.tabGeneral.Controls.Add(this.dtpCallStock);
            this.tabGeneral.Controls.Add(this.label1);
            this.tabGeneral.Controls.Add(this.label9);
            this.tabGeneral.Controls.Add(this.label8);
            this.tabGeneral.Controls.Add(this.label4);
            this.tabGeneral.Controls.Add(this.lblCustomer);
            this.tabGeneral.Controls.Add(this.txtBarcode);
            this.tabGeneral.Controls.Add(this.txtNote);
            this.tabGeneral.Controls.Add(this.dtpInputToDate);
            this.tabGeneral.Controls.Add(this.lbBarcode);
            this.tabGeneral.Controls.Add(this.btnSearchBill);
            this.tabGeneral.Controls.Add(this.btnSearchProduct);
            this.tabGeneral.Controls.Add(this.cboStatus);
            this.tabGeneral.Controls.Add(this.txtPriceProtectID);
            this.tabGeneral.Controls.Add(this.dtpInputFromDate);
            this.tabGeneral.Controls.Add(this.dtpPriceProtectDate);
            this.tabGeneral.Location = new System.Drawing.Point(4, 25);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tabGeneral.Size = new System.Drawing.Size(1066, 171);
            this.tabGeneral.TabIndex = 0;
            this.tabGeneral.Text = "Thông tin chung";
            this.tabGeneral.UseVisualStyleBackColor = true;
            this.tabGeneral.Click += new System.EventHandler(this.tabGeneral_Click);
            // 
            // cboPriceProtectType
            // 
            this.cboPriceProtectType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPriceProtectType.Location = new System.Drawing.Point(106, 4);
            this.cboPriceProtectType.Margin = new System.Windows.Forms.Padding(0);
            this.cboPriceProtectType.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboPriceProtectType.Name = "cboPriceProtectType";
            this.cboPriceProtectType.Size = new System.Drawing.Size(203, 24);
            this.cboPriceProtectType.TabIndex = 1;
            this.cboPriceProtectType.SelectionChangeCommitted += new System.EventHandler(this.cboPriceProtectType_SelectionChangeCommitted);
            // 
            // dtpPostdate
            // 
            this.dtpPostdate.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dtpPostdate.CustomFormat = "dd/MM/yyyy";
            this.dtpPostdate.Enabled = false;
            this.dtpPostdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPostdate.Location = new System.Drawing.Point(699, 6);
            this.dtpPostdate.Name = "dtpPostdate";
            this.dtpPostdate.Size = new System.Drawing.Size(107, 22);
            this.dtpPostdate.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(578, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ngày hạch toán 1:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // chkIsSupplierCall
            // 
            this.chkIsSupplierCall.AutoSize = true;
            this.chkIsSupplierCall.Location = new System.Drawing.Point(699, 91);
            this.chkIsSupplierCall.Name = "chkIsSupplierCall";
            this.chkIsSupplierCall.Size = new System.Drawing.Size(187, 20);
            this.chkIsSupplierCall.TabIndex = 25;
            this.chkIsSupplierCall.Text = "Chốt tồn theo nhà cung cấp";
            this.chkIsSupplierCall.UseVisualStyleBackColor = true;
            this.chkIsSupplierCall.CheckedChanged += new System.EventHandler(this.chkIsSupplierCall_CheckedChanged);
            // 
            // txtPriceProtectNO
            // 
            this.txtPriceProtectNO.BackColor = System.Drawing.SystemColors.Info;
            this.txtPriceProtectNO.Location = new System.Drawing.Point(899, 33);
            this.txtPriceProtectNO.MaxLength = 100;
            this.txtPriceProtectNO.Name = "txtPriceProtectNO";
            this.txtPriceProtectNO.ReadOnly = true;
            this.txtPriceProtectNO.Size = new System.Drawing.Size(139, 22);
            this.txtPriceProtectNO.TabIndex = 16;
            // 
            // tabCustomer
            // 
            this.tabCustomer.Controls.Add(this.grdCustomer);
            this.tabCustomer.Location = new System.Drawing.Point(4, 25);
            this.tabCustomer.Name = "tabCustomer";
            this.tabCustomer.Padding = new System.Windows.Forms.Padding(3);
            this.tabCustomer.Size = new System.Drawing.Size(1066, 171);
            this.tabCustomer.TabIndex = 1;
            this.tabCustomer.Text = "Nhà cung cấp";
            this.tabCustomer.UseVisualStyleBackColor = true;
            // 
            // grdCustomer
            // 
            this.grdCustomer.ContextMenuStrip = this.mnuCustomer;
            this.grdCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCustomer.Location = new System.Drawing.Point(3, 3);
            this.grdCustomer.MainView = this.grvCustomer;
            this.grdCustomer.Name = "grdCustomer";
            this.grdCustomer.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repChecked,
            this.textbox400,
            this.textbox300,
            this.textbox100,
            this.textbox20});
            this.grdCustomer.Size = new System.Drawing.Size(1060, 165);
            this.grdCustomer.TabIndex = 2;
            this.grdCustomer.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvCustomer});
            // 
            // mnuCustomer
            // 
            this.mnuCustomer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemAddCustomer,
            this.mnuItemDeleteCustomer});
            this.mnuCustomer.Name = "mnuAction";
            this.mnuCustomer.Size = new System.Drawing.Size(105, 48);
            // 
            // mnuItemAddCustomer
            // 
            this.mnuItemAddCustomer.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.mnuItemAddCustomer.Name = "mnuItemAddCustomer";
            this.mnuItemAddCustomer.Size = new System.Drawing.Size(104, 22);
            this.mnuItemAddCustomer.Text = "Thêm";
            this.mnuItemAddCustomer.Click += new System.EventHandler(this.mnuItemAddCustomer_Click);
            // 
            // mnuItemDeleteCustomer
            // 
            this.mnuItemDeleteCustomer.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.mnuItemDeleteCustomer.Name = "mnuItemDeleteCustomer";
            this.mnuItemDeleteCustomer.Size = new System.Drawing.Size(104, 22);
            this.mnuItemDeleteCustomer.Text = "Xóa";
            this.mnuItemDeleteCustomer.Click += new System.EventHandler(this.mnuItemDeleteCustomer_Click);
            // 
            // grvCustomer
            // 
            this.grvCustomer.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvCustomer.Appearance.FooterPanel.Options.UseFont = true;
            this.grvCustomer.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvCustomer.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvCustomer.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvCustomer.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvCustomer.ColumnPanelRowHeight = 30;
            this.grvCustomer.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn15,
            this.colCustomerName,
            this.gridColumn22,
            this.gridColumn29,
            this.gridColumn30,
            this.colIsDefault});
            this.grvCustomer.GridControl = this.grdCustomer;
            this.grvCustomer.Name = "grvCustomer";
            this.grvCustomer.OptionsNavigation.UseTabKey = false;
            this.grvCustomer.OptionsView.ColumnAutoWidth = false;
            this.grvCustomer.OptionsView.ShowGroupPanel = false;
            this.grvCustomer.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grvCustomer_ShowingEditor);
            this.grvCustomer.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grvCustomer_CellValueChanged);
            this.grvCustomer.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grvCustomer_CellValueChanging);
            this.grvCustomer.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.grvCustomer_InvalidRowException);
            this.grvCustomer.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.grvCustomer_ValidateRow);
            this.grvCustomer.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.grvCustomer_ValidatingEditor);
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Chọn";
            this.gridColumn5.ColumnEdit = this.repChecked;
            this.gridColumn5.FieldName = "IsSelected";
            this.gridColumn5.MaxWidth = 60;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Width = 60;
            // 
            // repChecked
            // 
            this.repChecked.AutoHeight = false;
            this.repChecked.Name = "repChecked";
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn15.AppearanceCell.Options.UseFont = true;
            this.gridColumn15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn15.AppearanceHeader.Options.UseFont = true;
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn15.Caption = "Mã nhà cung cấp";
            this.gridColumn15.FieldName = "CustomerID";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            this.gridColumn15.Width = 120;
            // 
            // colCustomerName
            // 
            this.colCustomerName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colCustomerName.AppearanceCell.Options.UseFont = true;
            this.colCustomerName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colCustomerName.AppearanceHeader.Options.UseFont = true;
            this.colCustomerName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCustomerName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCustomerName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCustomerName.Caption = "Tên nhà cung cấp";
            this.colCustomerName.ColumnEdit = this.textbox400;
            this.colCustomerName.FieldName = "CustomerName";
            this.colCustomerName.Name = "colCustomerName";
            this.colCustomerName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCustomerName.Visible = true;
            this.colCustomerName.VisibleIndex = 1;
            this.colCustomerName.Width = 330;
            // 
            // textbox400
            // 
            this.textbox400.AutoHeight = false;
            this.textbox400.MaxLength = 400;
            this.textbox400.Name = "textbox400";
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn22.AppearanceCell.Options.UseFont = true;
            this.gridColumn22.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn22.AppearanceHeader.Options.UseFont = true;
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn22.Caption = "Địa chỉ";
            this.gridColumn22.ColumnEdit = this.textbox300;
            this.gridColumn22.FieldName = "CustomerAddress";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            this.gridColumn22.Width = 260;
            // 
            // textbox300
            // 
            this.textbox300.AutoHeight = false;
            this.textbox300.MaxLength = 300;
            this.textbox300.Name = "textbox300";
            // 
            // gridColumn29
            // 
            this.gridColumn29.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn29.AppearanceCell.Options.UseFont = true;
            this.gridColumn29.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn29.AppearanceHeader.Options.UseFont = true;
            this.gridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn29.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn29.Caption = "Mã số thuế";
            this.gridColumn29.ColumnEdit = this.textbox100;
            this.gridColumn29.FieldName = "CustomerTaxID";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 3;
            this.gridColumn29.Width = 130;
            // 
            // textbox100
            // 
            this.textbox100.AutoHeight = false;
            this.textbox100.MaxLength = 100;
            this.textbox100.Name = "textbox100";
            // 
            // gridColumn30
            // 
            this.gridColumn30.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn30.AppearanceCell.Options.UseFont = true;
            this.gridColumn30.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn30.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn30.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn30.AppearanceHeader.Options.UseFont = true;
            this.gridColumn30.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn30.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn30.Caption = "Điện thoại";
            this.gridColumn30.ColumnEdit = this.textbox20;
            this.gridColumn30.FieldName = "CustomerPhone";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 4;
            this.gridColumn30.Width = 140;
            // 
            // textbox20
            // 
            this.textbox20.AutoHeight = false;
            this.textbox20.MaxLength = 20;
            this.textbox20.Name = "textbox20";
            // 
            // colIsDefault
            // 
            this.colIsDefault.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colIsDefault.AppearanceCell.Options.UseFont = true;
            this.colIsDefault.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colIsDefault.AppearanceHeader.Options.UseFont = true;
            this.colIsDefault.AppearanceHeader.Options.UseTextOptions = true;
            this.colIsDefault.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIsDefault.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIsDefault.Caption = "Mặc định";
            this.colIsDefault.ColumnEdit = this.repChecked;
            this.colIsDefault.FieldName = "IsDefault";
            this.colIsDefault.Name = "colIsDefault";
            this.colIsDefault.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colIsDefault.Visible = true;
            this.colIsDefault.VisibleIndex = 5;
            this.colIsDefault.Width = 80;
            // 
            // tabAttachment
            // 
            this.tabAttachment.Controls.Add(this.grdAttachment);
            this.tabAttachment.Location = new System.Drawing.Point(4, 25);
            this.tabAttachment.Name = "tabAttachment";
            this.tabAttachment.Padding = new System.Windows.Forms.Padding(3);
            this.tabAttachment.Size = new System.Drawing.Size(1066, 171);
            this.tabAttachment.TabIndex = 2;
            this.tabAttachment.Text = "Tập tin đính kèm";
            this.tabAttachment.UseVisualStyleBackColor = true;
            // 
            // grdAttachment
            // 
            this.grdAttachment.ContextMenuStrip = this.mnuAttachment;
            this.grdAttachment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdAttachment.Location = new System.Drawing.Point(3, 3);
            this.grdAttachment.MainView = this.grvAttachment;
            this.grdAttachment.Name = "grdAttachment";
            this.grdAttachment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnDownload,
            this.btnView});
            this.grdAttachment.Size = new System.Drawing.Size(1060, 165);
            this.grdAttachment.TabIndex = 2;
            this.grdAttachment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvAttachment});
            // 
            // mnuAttachment
            // 
            this.mnuAttachment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAddAttachment,
            this.mnuDelAttachment});
            this.mnuAttachment.Name = "mnuFlex";
            this.mnuAttachment.Size = new System.Drawing.Size(142, 48);
            // 
            // mnuAddAttachment
            // 
            this.mnuAddAttachment.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.mnuAddAttachment.Name = "mnuAddAttachment";
            this.mnuAddAttachment.Size = new System.Drawing.Size(141, 22);
            this.mnuAddAttachment.Text = "Thêm tập tin";
            this.mnuAddAttachment.Click += new System.EventHandler(this.mnuAddAttachment_Click);
            // 
            // mnuDelAttachment
            // 
            this.mnuDelAttachment.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.mnuDelAttachment.Name = "mnuDelAttachment";
            this.mnuDelAttachment.Size = new System.Drawing.Size(141, 22);
            this.mnuDelAttachment.Text = "Xóa tập tin";
            this.mnuDelAttachment.Click += new System.EventHandler(this.mnuDelAttachment_Click);
            // 
            // grvAttachment
            // 
            this.grvAttachment.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvAttachment.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvAttachment.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvAttachment.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvAttachment.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvAttachment.Appearance.Row.Options.UseFont = true;
            this.grvAttachment.ColumnPanelRowHeight = 30;
            this.grvAttachment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn14,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.colView,
            this.colDownload});
            this.grvAttachment.GridControl = this.grdAttachment;
            this.grvAttachment.Name = "grvAttachment";
            this.grvAttachment.OptionsView.ShowGroupPanel = false;
            this.grvAttachment.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grvAttachment_CellValueChanged);
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Tên tập tin";
            this.gridColumn14.FieldName = "FileName";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 0;
            this.gridColumn14.Width = 228;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Mô tả";
            this.gridColumn23.FieldName = "Description";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 1;
            this.gridColumn23.Width = 322;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Thời gian tạo";
            this.gridColumn24.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColumn24.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn24.FieldName = "CreatedDate";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 2;
            this.gridColumn24.Width = 140;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Nhân viên tạo";
            this.gridColumn25.FieldName = "CreatedUserName";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 3;
            this.gridColumn25.Width = 240;
            // 
            // colView
            // 
            this.colView.Caption = "Xem";
            this.colView.ColumnEdit = this.btnView;
            this.colView.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colView.Name = "colView";
            this.colView.OptionsColumn.AllowSize = false;
            this.colView.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colView.OptionsColumn.FixedWidth = true;
            this.colView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colView.Visible = true;
            this.colView.VisibleIndex = 4;
            this.colView.Width = 40;
            // 
            // btnView
            // 
            this.btnView.AutoHeight = false;
            this.btnView.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::ERP.Inventory.DUI.Properties.Resources.view, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Xem tập tin", null, null, false)});
            this.btnView.Name = "btnView";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // colDownload
            // 
            this.colDownload.Caption = "Tải";
            this.colDownload.ColumnEdit = this.btnDownload;
            this.colDownload.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colDownload.Name = "colDownload";
            this.colDownload.OptionsColumn.AllowMove = false;
            this.colDownload.OptionsColumn.AllowSize = false;
            this.colDownload.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colDownload.OptionsColumn.FixedWidth = true;
            this.colDownload.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colDownload.Visible = true;
            this.colDownload.VisibleIndex = 5;
            this.colDownload.Width = 40;
            // 
            // btnDownload
            // 
            this.btnDownload.AutoHeight = false;
            this.btnDownload.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::ERP.Inventory.DUI.Properties.Resources.Folder_Downloads_icon1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Tải tập tin", null, null, false)});
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPriceProtect1);
            this.tabControl2.Controls.Add(this.tabPriceProtect2);
            this.tabControl2.Controls.Add(this.tabDifferent);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 200);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1074, 382);
            this.tabControl2.TabIndex = 3;
            // 
            // tabPriceProtect1
            // 
            this.tabPriceProtect1.Controls.Add(this.grdData);
            this.tabPriceProtect1.Location = new System.Drawing.Point(4, 25);
            this.tabPriceProtect1.Name = "tabPriceProtect1";
            this.tabPriceProtect1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPriceProtect1.Size = new System.Drawing.Size(1066, 353);
            this.tabPriceProtect1.TabIndex = 0;
            this.tabPriceProtect1.Text = "Hỗ trợ lần 1";
            this.tabPriceProtect1.UseVisualStyleBackColor = true;
            // 
            // grdData
            // 
            this.grdData.ContextMenuStrip = this.mnuAction;
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(3, 3);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repPrice,
            this.repTotalAmount,
            this.repVAT});
            this.grdData.Size = new System.Drawing.Size(1060, 347);
            this.grdData.TabIndex = 1;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.ColumnPanelRowHeight = 65;
            this.grdViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn1,
            this.gridColumn2,
            this.colInStockQuantity,
            this.colPrice,
            this.colPriceNoVat,
            this.colVat,
            this.colTotalAmountNoVat,
            this.colVATAmount,
            this.colTotalAmount,
            this.colQuantityNoInput,
            this.colQuantityStock,
            this.colQuantityInStockNonIsCenter,
            this.colQuantityBuy,
            this.colQuantityBuyNonIsCenter,
            this.colQuantityOtherIncome,
            this.colTotalAmountStock,
            this.colTotalAmountStockNonIsCenter,
            this.colTotalAmountBuy,
            this.colTotalAmountBuyNonIsCenter,
            this.colTotalAmountOtherIncome,
            this.colQuantity});
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsView.ColumnAutoWidth = false;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowFooter = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            this.grdViewData.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.grdViewData_RowCellClick);
            this.grdViewData.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grdViewData_ShowingEditor);
            this.grdViewData.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grdViewData_CellValueChanged);
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn9.AppearanceCell.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn9.AppearanceHeader.Options.UseFont = true;
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn9.Caption = "Mã phiếu nhập";
            this.gridColumn9.FieldName = "InputVoucherID";
            this.gridColumn9.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn9.Width = 130;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn10.AppearanceCell.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn10.Caption = "Số hóa đơn";
            this.gridColumn10.FieldName = "InVoiceID";
            this.gridColumn10.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn10.Width = 85;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn1.AppearanceCell.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn1.Caption = "Mã sản phẩm";
            this.gridColumn1.FieldName = "ProductID";
            this.gridColumn1.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "ProductID", "Tổng cộng: {0:N0}")});
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 120;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn2.AppearanceCell.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "Tên sản phẩm";
            this.gridColumn2.FieldName = "ProductName";
            this.gridColumn2.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 230;
            // 
            // colInStockQuantity
            // 
            this.colInStockQuantity.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colInStockQuantity.AppearanceCell.Options.UseFont = true;
            this.colInStockQuantity.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colInStockQuantity.AppearanceHeader.Options.UseFont = true;
            this.colInStockQuantity.AppearanceHeader.Options.UseTextOptions = true;
            this.colInStockQuantity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInStockQuantity.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colInStockQuantity.Caption = "SL hỗ trợ";
            this.colInStockQuantity.FieldName = "InStockQuantity";
            this.colInStockQuantity.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colInStockQuantity.Name = "colInStockQuantity";
            this.colInStockQuantity.OptionsColumn.AllowEdit = false;
            this.colInStockQuantity.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colInStockQuantity.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "InStockQuantity", "{0:N0}")});
            this.colInStockQuantity.Visible = true;
            this.colInStockQuantity.VisibleIndex = 2;
            this.colInStockQuantity.Width = 80;
            // 
            // colPrice
            // 
            this.colPrice.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colPrice.AppearanceCell.Options.UseFont = true;
            this.colPrice.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colPrice.AppearanceHeader.Options.UseFont = true;
            this.colPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrice.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPrice.Caption = "Đơn giá hỗ trợ (gồm VAT)";
            this.colPrice.ColumnEdit = this.repPrice;
            this.colPrice.DisplayFormat.FormatString = "#,##0.##";
            this.colPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPrice.FieldName = "Price";
            this.colPrice.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colPrice.Name = "colPrice";
            this.colPrice.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPrice.Visible = true;
            this.colPrice.VisibleIndex = 3;
            this.colPrice.Width = 100;
            // 
            // repPrice
            // 
            this.repPrice.AutoHeight = false;
            this.repPrice.DisplayFormat.FormatString = "#,###,###,##0.##;";
            this.repPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repPrice.Mask.EditMask = "#,###,###,##0.##;";
            this.repPrice.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repPrice.Name = "repPrice";
            // 
            // colPriceNoVat
            // 
            this.colPriceNoVat.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colPriceNoVat.AppearanceCell.Options.UseFont = true;
            this.colPriceNoVat.AppearanceCell.Options.UseTextOptions = true;
            this.colPriceNoVat.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPriceNoVat.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colPriceNoVat.AppearanceHeader.Options.UseFont = true;
            this.colPriceNoVat.AppearanceHeader.Options.UseTextOptions = true;
            this.colPriceNoVat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceNoVat.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPriceNoVat.Caption = "Đơn giá hỗ trợ (chưa VAT)";
            this.colPriceNoVat.ColumnEdit = this.repTotalAmount;
            this.colPriceNoVat.DisplayFormat.FormatString = "#,##0.##";
            this.colPriceNoVat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPriceNoVat.FieldName = "PriceNoVat";
            this.colPriceNoVat.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colPriceNoVat.Name = "colPriceNoVat";
            this.colPriceNoVat.OptionsColumn.AllowEdit = false;
            this.colPriceNoVat.OptionsColumn.ReadOnly = true;
            this.colPriceNoVat.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPriceNoVat.Visible = true;
            this.colPriceNoVat.VisibleIndex = 4;
            this.colPriceNoVat.Width = 100;
            // 
            // repTotalAmount
            // 
            this.repTotalAmount.AutoHeight = false;
            this.repTotalAmount.DisplayFormat.FormatString = "#,###,###,##0.##;";
            this.repTotalAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repTotalAmount.Mask.EditMask = "#,###,###,##0.##;";
            this.repTotalAmount.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repTotalAmount.Name = "repTotalAmount";
            // 
            // colVat
            // 
            this.colVat.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colVat.AppearanceCell.Options.UseFont = true;
            this.colVat.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colVat.AppearanceHeader.Options.UseFont = true;
            this.colVat.AppearanceHeader.Options.UseTextOptions = true;
            this.colVat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVat.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVat.Caption = "Thuế suất (%)";
            this.colVat.ColumnEdit = this.repVAT;
            this.colVat.FieldName = "VatTypeID";
            this.colVat.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colVat.Name = "colVat";
            this.colVat.OptionsColumn.AllowEdit = false;
            this.colVat.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colVat.Visible = true;
            this.colVat.VisibleIndex = 6;
            this.colVat.Width = 80;
            // 
            // repVAT
            // 
            this.repVAT.AutoHeight = false;
            this.repVAT.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VATTYPEID", "Loại thuế"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VATTYPENAME", "Mức thuế")});
            this.repVAT.DisplayMember = "VATTYPENAME";
            this.repVAT.Name = "repVAT";
            this.repVAT.NullText = "--Chọn thuế suất HĐ--";
            this.repVAT.ValueMember = "VATTYPEID";
            // 
            // colTotalAmountNoVat
            // 
            this.colTotalAmountNoVat.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colTotalAmountNoVat.AppearanceCell.Options.UseFont = true;
            this.colTotalAmountNoVat.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colTotalAmountNoVat.AppearanceHeader.Options.UseFont = true;
            this.colTotalAmountNoVat.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountNoVat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountNoVat.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountNoVat.Caption = "Thành tiền";
            this.colTotalAmountNoVat.ColumnEdit = this.repTotalAmount;
            this.colTotalAmountNoVat.DisplayFormat.FormatString = "N0";
            this.colTotalAmountNoVat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalAmountNoVat.FieldName = "TotalAmountNoVat";
            this.colTotalAmountNoVat.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmountNoVat.Name = "colTotalAmountNoVat";
            this.colTotalAmountNoVat.OptionsColumn.AllowEdit = false;
            this.colTotalAmountNoVat.OptionsColumn.ReadOnly = true;
            this.colTotalAmountNoVat.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmountNoVat.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountNoVat", "{0:N0}")});
            this.colTotalAmountNoVat.Visible = true;
            this.colTotalAmountNoVat.VisibleIndex = 5;
            this.colTotalAmountNoVat.Width = 100;
            // 
            // colVATAmount
            // 
            this.colVATAmount.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colVATAmount.AppearanceCell.Options.UseFont = true;
            this.colVATAmount.AppearanceHeader.Options.UseTextOptions = true;
            this.colVATAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVATAmount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVATAmount.Caption = "Tiền thuế";
            this.colVATAmount.ColumnEdit = this.repTotalAmount;
            this.colVATAmount.FieldName = "VATAmount";
            this.colVATAmount.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colVATAmount.Name = "colVATAmount";
            this.colVATAmount.OptionsColumn.AllowEdit = false;
            this.colVATAmount.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colVATAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VATAmount", "{0:N0}")});
            this.colVATAmount.Visible = true;
            this.colVATAmount.VisibleIndex = 7;
            this.colVATAmount.Width = 100;
            // 
            // colTotalAmount
            // 
            this.colTotalAmount.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colTotalAmount.AppearanceCell.Options.UseFont = true;
            this.colTotalAmount.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colTotalAmount.AppearanceHeader.Options.UseFont = true;
            this.colTotalAmount.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmount.Caption = "Tổng tiền (gồm VAT)";
            this.colTotalAmount.ColumnEdit = this.repTotalAmount;
            this.colTotalAmount.DisplayFormat.FormatString = "N0";
            this.colTotalAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalAmount.FieldName = "TotalAmount";
            this.colTotalAmount.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmount.Name = "colTotalAmount";
            this.colTotalAmount.OptionsColumn.AllowEdit = false;
            this.colTotalAmount.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmount", "{0:N0}")});
            this.colTotalAmount.Visible = true;
            this.colTotalAmount.VisibleIndex = 8;
            this.colTotalAmount.Width = 100;
            // 
            // colQuantityNoInput
            // 
            this.colQuantityNoInput.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantityNoInput.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantityNoInput.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantityNoInput.Caption = "Số lượng chưa nhập";
            this.colQuantityNoInput.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantityNoInput.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityNoInput.FieldName = "QuantityNoInput";
            this.colQuantityNoInput.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantityNoInput.Name = "colQuantityNoInput";
            this.colQuantityNoInput.OptionsColumn.AllowEdit = false;
            this.colQuantityNoInput.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantityNoInput.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityNoInput", "{0:N0}")});
            this.colQuantityNoInput.Width = 80;
            // 
            // colQuantityStock
            // 
            this.colQuantityStock.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colQuantityStock.AppearanceCell.Options.UseFont = true;
            this.colQuantityStock.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colQuantityStock.AppearanceHeader.Options.UseFont = true;
            this.colQuantityStock.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantityStock.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantityStock.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantityStock.Caption = "SL tồn tại CN Trung tâm";
            this.colQuantityStock.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantityStock.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityStock.FieldName = "QuantityStock";
            this.colQuantityStock.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantityStock.Name = "colQuantityStock";
            this.colQuantityStock.OptionsColumn.AllowEdit = false;
            this.colQuantityStock.OptionsColumn.ReadOnly = true;
            this.colQuantityStock.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantityStock.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityStock", "{0:N0}")});
            this.colQuantityStock.Width = 80;
            // 
            // colQuantityInStockNonIsCenter
            // 
            this.colQuantityInStockNonIsCenter.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colQuantityInStockNonIsCenter.AppearanceCell.Options.UseFont = true;
            this.colQuantityInStockNonIsCenter.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colQuantityInStockNonIsCenter.AppearanceHeader.Options.UseFont = true;
            this.colQuantityInStockNonIsCenter.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantityInStockNonIsCenter.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantityInStockNonIsCenter.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantityInStockNonIsCenter.Caption = "SL tồn tại CN tỉnh";
            this.colQuantityInStockNonIsCenter.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantityInStockNonIsCenter.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityInStockNonIsCenter.FieldName = "QuantityInStockNonIsCenter";
            this.colQuantityInStockNonIsCenter.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantityInStockNonIsCenter.Name = "colQuantityInStockNonIsCenter";
            this.colQuantityInStockNonIsCenter.OptionsColumn.AllowEdit = false;
            this.colQuantityInStockNonIsCenter.OptionsColumn.ReadOnly = true;
            this.colQuantityInStockNonIsCenter.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantityInStockNonIsCenter.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityInStockNonIsCenter", "{0:N0}")});
            this.colQuantityInStockNonIsCenter.Width = 80;
            // 
            // colQuantityBuy
            // 
            this.colQuantityBuy.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colQuantityBuy.AppearanceCell.Options.UseFont = true;
            this.colQuantityBuy.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colQuantityBuy.AppearanceHeader.Options.UseFont = true;
            this.colQuantityBuy.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantityBuy.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantityBuy.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantityBuy.Caption = "SL bán tại CN trung tâm";
            this.colQuantityBuy.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantityBuy.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityBuy.FieldName = "QuantityBuy";
            this.colQuantityBuy.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantityBuy.Name = "colQuantityBuy";
            this.colQuantityBuy.OptionsColumn.AllowEdit = false;
            this.colQuantityBuy.OptionsColumn.ReadOnly = true;
            this.colQuantityBuy.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantityBuy.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityBuy", "{0:N0}")});
            this.colQuantityBuy.Width = 80;
            // 
            // colQuantityBuyNonIsCenter
            // 
            this.colQuantityBuyNonIsCenter.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colQuantityBuyNonIsCenter.AppearanceCell.Options.UseFont = true;
            this.colQuantityBuyNonIsCenter.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colQuantityBuyNonIsCenter.AppearanceHeader.Options.UseFont = true;
            this.colQuantityBuyNonIsCenter.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantityBuyNonIsCenter.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantityBuyNonIsCenter.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantityBuyNonIsCenter.Caption = "SL bán tại CN tỉnh";
            this.colQuantityBuyNonIsCenter.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantityBuyNonIsCenter.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityBuyNonIsCenter.FieldName = "QuantityBuyNonIsCenter";
            this.colQuantityBuyNonIsCenter.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantityBuyNonIsCenter.Name = "colQuantityBuyNonIsCenter";
            this.colQuantityBuyNonIsCenter.OptionsColumn.AllowEdit = false;
            this.colQuantityBuyNonIsCenter.OptionsColumn.ReadOnly = true;
            this.colQuantityBuyNonIsCenter.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantityBuyNonIsCenter.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityBuyNonIsCenter", "{0:N0}")});
            this.colQuantityBuyNonIsCenter.Width = 80;
            // 
            // colQuantityOtherIncome
            // 
            this.colQuantityOtherIncome.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantityOtherIncome.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantityOtherIncome.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantityOtherIncome.Caption = "SL ghi nhận doanh thu khác";
            this.colQuantityOtherIncome.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantityOtherIncome.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityOtherIncome.FieldName = "QuantityOtherIncome";
            this.colQuantityOtherIncome.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantityOtherIncome.Name = "colQuantityOtherIncome";
            this.colQuantityOtherIncome.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantityOtherIncome.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityOtherIncome", "{0:N0}")});
            this.colQuantityOtherIncome.Width = 85;
            // 
            // colTotalAmountStock
            // 
            this.colTotalAmountStock.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colTotalAmountStock.AppearanceCell.Options.UseFont = true;
            this.colTotalAmountStock.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colTotalAmountStock.AppearanceHeader.Options.UseFont = true;
            this.colTotalAmountStock.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountStock.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountStock.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountStock.Caption = "Giá trị hàng tồn tại CN trung tâm";
            this.colTotalAmountStock.ColumnEdit = this.repTotalAmount;
            this.colTotalAmountStock.DisplayFormat.FormatString = "N0";
            this.colTotalAmountStock.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalAmountStock.FieldName = "TotalAmountStock";
            this.colTotalAmountStock.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmountStock.Name = "colTotalAmountStock";
            this.colTotalAmountStock.OptionsColumn.AllowEdit = false;
            this.colTotalAmountStock.OptionsColumn.ReadOnly = true;
            this.colTotalAmountStock.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmountStock.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountStock", "{0:N0}")});
            this.colTotalAmountStock.Width = 120;
            // 
            // colTotalAmountStockNonIsCenter
            // 
            this.colTotalAmountStockNonIsCenter.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colTotalAmountStockNonIsCenter.AppearanceCell.Options.UseFont = true;
            this.colTotalAmountStockNonIsCenter.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colTotalAmountStockNonIsCenter.AppearanceHeader.Options.UseFont = true;
            this.colTotalAmountStockNonIsCenter.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountStockNonIsCenter.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountStockNonIsCenter.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountStockNonIsCenter.Caption = "Giá trị hàng tồn tại CN tỉnh";
            this.colTotalAmountStockNonIsCenter.ColumnEdit = this.repTotalAmount;
            this.colTotalAmountStockNonIsCenter.DisplayFormat.FormatString = "N0";
            this.colTotalAmountStockNonIsCenter.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalAmountStockNonIsCenter.FieldName = "TotalAmountStockNonIsCenter";
            this.colTotalAmountStockNonIsCenter.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmountStockNonIsCenter.Name = "colTotalAmountStockNonIsCenter";
            this.colTotalAmountStockNonIsCenter.OptionsColumn.AllowEdit = false;
            this.colTotalAmountStockNonIsCenter.OptionsColumn.ReadOnly = true;
            this.colTotalAmountStockNonIsCenter.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmountStockNonIsCenter.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountStockNonIsCenter", "{0:N0}")});
            this.colTotalAmountStockNonIsCenter.Width = 120;
            // 
            // colTotalAmountBuy
            // 
            this.colTotalAmountBuy.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colTotalAmountBuy.AppearanceCell.Options.UseFont = true;
            this.colTotalAmountBuy.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colTotalAmountBuy.AppearanceHeader.Options.UseFont = true;
            this.colTotalAmountBuy.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountBuy.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountBuy.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountBuy.Caption = "Giá trị hàng bán tại CN trung tâm";
            this.colTotalAmountBuy.ColumnEdit = this.repTotalAmount;
            this.colTotalAmountBuy.DisplayFormat.FormatString = "N0";
            this.colTotalAmountBuy.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalAmountBuy.FieldName = "TotalAmountBuy";
            this.colTotalAmountBuy.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmountBuy.Name = "colTotalAmountBuy";
            this.colTotalAmountBuy.OptionsColumn.AllowEdit = false;
            this.colTotalAmountBuy.OptionsColumn.ReadOnly = true;
            this.colTotalAmountBuy.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmountBuy.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountBuy", "{0:N0}")});
            this.colTotalAmountBuy.Width = 120;
            // 
            // colTotalAmountBuyNonIsCenter
            // 
            this.colTotalAmountBuyNonIsCenter.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colTotalAmountBuyNonIsCenter.AppearanceCell.Options.UseFont = true;
            this.colTotalAmountBuyNonIsCenter.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colTotalAmountBuyNonIsCenter.AppearanceHeader.Options.UseFont = true;
            this.colTotalAmountBuyNonIsCenter.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountBuyNonIsCenter.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountBuyNonIsCenter.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountBuyNonIsCenter.Caption = "Giá trị hàng bán tại CN tỉnh";
            this.colTotalAmountBuyNonIsCenter.ColumnEdit = this.repTotalAmount;
            this.colTotalAmountBuyNonIsCenter.DisplayFormat.FormatString = "N0";
            this.colTotalAmountBuyNonIsCenter.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalAmountBuyNonIsCenter.FieldName = "TotalAmountBuyNonIsCenter";
            this.colTotalAmountBuyNonIsCenter.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmountBuyNonIsCenter.Name = "colTotalAmountBuyNonIsCenter";
            this.colTotalAmountBuyNonIsCenter.OptionsColumn.AllowEdit = false;
            this.colTotalAmountBuyNonIsCenter.OptionsColumn.ReadOnly = true;
            this.colTotalAmountBuyNonIsCenter.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmountBuyNonIsCenter.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountBuyNonIsCenter", "{0:N0}")});
            this.colTotalAmountBuyNonIsCenter.Width = 120;
            // 
            // colTotalAmountOtherIncome
            // 
            this.colTotalAmountOtherIncome.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountOtherIncome.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountOtherIncome.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountOtherIncome.Caption = "Giá trị ghi nhận doanh thu khác";
            this.colTotalAmountOtherIncome.DisplayFormat.FormatString = "N0";
            this.colTotalAmountOtherIncome.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalAmountOtherIncome.FieldName = "TotalAmountOtherIncome";
            this.colTotalAmountOtherIncome.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmountOtherIncome.Name = "colTotalAmountOtherIncome";
            this.colTotalAmountOtherIncome.OptionsColumn.AllowEdit = false;
            this.colTotalAmountOtherIncome.OptionsColumn.ReadOnly = true;
            this.colTotalAmountOtherIncome.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmountOtherIncome.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountOtherIncome", "{0:N0}")});
            this.colTotalAmountOtherIncome.Width = 120;
            // 
            // colQuantity
            // 
            this.colQuantity.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colQuantity.AppearanceCell.Options.UseFont = true;
            this.colQuantity.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colQuantity.AppearanceHeader.Options.UseFont = true;
            this.colQuantity.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantity.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantity.Caption = "SL hỗ trợ 2";
            this.colQuantity.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantity.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Quantity", "{0:N0}")});
            this.colQuantity.Width = 80;
            // 
            // tabPriceProtect2
            // 
            this.tabPriceProtect2.Controls.Add(this.grdDataPriceProtect1);
            this.tabPriceProtect2.Location = new System.Drawing.Point(4, 25);
            this.tabPriceProtect2.Name = "tabPriceProtect2";
            this.tabPriceProtect2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPriceProtect2.Size = new System.Drawing.Size(1066, 353);
            this.tabPriceProtect2.TabIndex = 1;
            this.tabPriceProtect2.Text = "Hỗ trợ lần 2";
            this.tabPriceProtect2.UseVisualStyleBackColor = true;
            // 
            // grdDataPriceProtect1
            // 
            this.grdDataPriceProtect1.ContextMenuStrip = this.mnuPriceProtect1;
            this.grdDataPriceProtect1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDataPriceProtect1.Location = new System.Drawing.Point(3, 3);
            this.grdDataPriceProtect1.MainView = this.grvDataPriceProtect1;
            this.grdDataPriceProtect1.Name = "grdDataPriceProtect1";
            this.grdDataPriceProtect1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemLookUpEdit1});
            this.grdDataPriceProtect1.Size = new System.Drawing.Size(1060, 347);
            this.grdDataPriceProtect1.TabIndex = 2;
            this.grdDataPriceProtect1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvDataPriceProtect1});
            // 
            // mnuPriceProtect1
            // 
            this.mnuPriceProtect1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemViewIMEIProtect1,
            this.mnuItemInputIMEIProtect1,
            this.toolStripSeparator3,
            this.mnuItemImportValues1,
            this.mnuItemExportTemplateProtect1,
            this.toolStripSeparator4,
            this.mnuItemExportExcelProtect1});
            this.mnuPriceProtect1.Name = "mnuAction";
            this.mnuPriceProtect1.Size = new System.Drawing.Size(172, 126);
            this.mnuPriceProtect1.Opening += new System.ComponentModel.CancelEventHandler(this.mnuPriceProtect1_Opening);
            // 
            // mnuItemViewIMEIProtect1
            // 
            this.mnuItemViewIMEIProtect1.Image = global::ERP.Inventory.DUI.Properties.Resources.ViewOutVoucher;
            this.mnuItemViewIMEIProtect1.Name = "mnuItemViewIMEIProtect1";
            this.mnuItemViewIMEIProtect1.Size = new System.Drawing.Size(171, 22);
            this.mnuItemViewIMEIProtect1.Text = "Xem chi tiết IMEI";
            this.mnuItemViewIMEIProtect1.Click += new System.EventHandler(this.mnuItemViewIMEIProtect1_Click);
            // 
            // mnuItemInputIMEIProtect1
            // 
            this.mnuItemInputIMEIProtect1.Enabled = false;
            this.mnuItemInputIMEIProtect1.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.mnuItemInputIMEIProtect1.Name = "mnuItemInputIMEIProtect1";
            this.mnuItemInputIMEIProtect1.Size = new System.Drawing.Size(171, 22);
            this.mnuItemInputIMEIProtect1.Text = "Nhập IMEI";
            this.mnuItemInputIMEIProtect1.Click += new System.EventHandler(this.mnuItemInputIMEIProtect1_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(168, 6);
            // 
            // mnuItemImportValues1
            // 
            this.mnuItemImportValues1.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemImportValues1.Name = "mnuItemImportValues1";
            this.mnuItemImportValues1.Size = new System.Drawing.Size(171, 22);
            this.mnuItemImportValues1.Text = "Nhập giá trị hỗ trợ";
            this.mnuItemImportValues1.Click += new System.EventHandler(this.mnuItemImportValues1_Click);
            // 
            // mnuItemExportTemplateProtect1
            // 
            this.mnuItemExportTemplateProtect1.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportTemplateProtect1.Name = "mnuItemExportTemplateProtect1";
            this.mnuItemExportTemplateProtect1.Size = new System.Drawing.Size(171, 22);
            this.mnuItemExportTemplateProtect1.Text = "Xuất excel mẫu";
            this.mnuItemExportTemplateProtect1.Click += new System.EventHandler(this.mnuItemExportTemplate1_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(168, 6);
            // 
            // mnuItemExportExcelProtect1
            // 
            this.mnuItemExportExcelProtect1.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportExcelProtect1.Name = "mnuItemExportExcelProtect1";
            this.mnuItemExportExcelProtect1.Size = new System.Drawing.Size(171, 22);
            this.mnuItemExportExcelProtect1.Text = "Xuất Excel";
            this.mnuItemExportExcelProtect1.Click += new System.EventHandler(this.mnuItemExportExcelProtect1_Click);
            // 
            // grvDataPriceProtect1
            // 
            this.grvDataPriceProtect1.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvDataPriceProtect1.Appearance.FooterPanel.Options.UseFont = true;
            this.grvDataPriceProtect1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvDataPriceProtect1.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvDataPriceProtect1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvDataPriceProtect1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvDataPriceProtect1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grvDataPriceProtect1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvDataPriceProtect1.Appearance.Row.Options.UseFont = true;
            this.grvDataPriceProtect1.ColumnPanelRowHeight = 65;
            this.grvDataPriceProtect1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn3,
            this.bandedGridColumn4,
            this.colQuatity,
            this.colPriceVoucher,
            this.colPriceNoVatVoucher,
            this.colTotalAmountNoVatVoucher,
            this.bandedGridColumn8,
            this.colVatAmountVoucher,
            this.colTotalAmountVoucher,
            this.colQuantityNoInputVoucher,
            this.colQuantityStockVoucher,
            this.colQuantityInStockNonIsCenterVoucher,
            this.colQuantityBuyVoucher,
            this.colQuantityBuyNonIsCenterVoucher,
            this.colQuantityOtherIncomeVoucher,
            this.colTotalAmountStockVoucher,
            this.colTotalAmountStockNonIsCenterVoucher,
            this.colTotalAmountBuyVoucher,
            this.colTotalAmountBuyNonIsCenterVoucher,
            this.colTotalAmountOtherIncomeVoucher});
            this.grvDataPriceProtect1.GridControl = this.grdDataPriceProtect1;
            this.grvDataPriceProtect1.Name = "grvDataPriceProtect1";
            this.grvDataPriceProtect1.OptionsNavigation.UseTabKey = false;
            this.grvDataPriceProtect1.OptionsView.ColumnAutoWidth = false;
            this.grvDataPriceProtect1.OptionsView.ShowAutoFilterRow = true;
            this.grvDataPriceProtect1.OptionsView.ShowFooter = true;
            this.grvDataPriceProtect1.OptionsView.ShowGroupPanel = false;
            this.grvDataPriceProtect1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.grvDataPriceProtect1_RowCellClick);
            this.grvDataPriceProtect1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grvDataPriceProtect1_ShowingEditor);
            this.grvDataPriceProtect1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grvDataPriceProtect1_CellValueChanged);
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn1.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.bandedGridColumn1.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn1.Caption = "Mã phiếu nhập";
            this.bandedGridColumn1.FieldName = "InputVoucherID";
            this.bandedGridColumn1.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn1.Width = 130;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn2.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.bandedGridColumn2.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn2.Caption = "Số hóa đơn";
            this.bandedGridColumn2.FieldName = "InVoiceID";
            this.bandedGridColumn2.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn2.Width = 85;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn3.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.bandedGridColumn3.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn3.Caption = "Mã sản phẩm";
            this.bandedGridColumn3.FieldName = "ProductID";
            this.bandedGridColumn3.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn3.OptionsColumn.ReadOnly = true;
            this.bandedGridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "ProductID", "Tổng cộng: {0:N0}")});
            this.bandedGridColumn3.Visible = true;
            this.bandedGridColumn3.VisibleIndex = 0;
            this.bandedGridColumn3.Width = 120;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn4.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.bandedGridColumn4.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn4.Caption = "Tên sản phẩm";
            this.bandedGridColumn4.FieldName = "ProductName";
            this.bandedGridColumn4.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn4.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn4.OptionsColumn.ReadOnly = true;
            this.bandedGridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn4.Visible = true;
            this.bandedGridColumn4.VisibleIndex = 1;
            this.bandedGridColumn4.Width = 230;
            // 
            // colQuatity
            // 
            this.colQuatity.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colQuatity.AppearanceCell.Options.UseFont = true;
            this.colQuatity.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colQuatity.AppearanceHeader.Options.UseFont = true;
            this.colQuatity.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuatity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuatity.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuatity.Caption = "SL hỗ trợ";
            this.colQuatity.FieldName = "Quantity";
            this.colQuatity.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuatity.Name = "colQuatity";
            this.colQuatity.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuatity.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "InStockQuantity", "{0:N0}")});
            this.colQuatity.Visible = true;
            this.colQuatity.VisibleIndex = 2;
            this.colQuatity.Width = 80;
            // 
            // colPriceVoucher
            // 
            this.colPriceVoucher.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colPriceVoucher.AppearanceCell.Options.UseFont = true;
            this.colPriceVoucher.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colPriceVoucher.AppearanceHeader.Options.UseFont = true;
            this.colPriceVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colPriceVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPriceVoucher.Caption = "Đơn giá hỗ trợ (gồm VAT)";
            this.colPriceVoucher.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPriceVoucher.DisplayFormat.FormatString = "#,##0.##";
            this.colPriceVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPriceVoucher.FieldName = "PriceVoucher";
            this.colPriceVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colPriceVoucher.Name = "colPriceVoucher";
            this.colPriceVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPriceVoucher.Visible = true;
            this.colPriceVoucher.VisibleIndex = 3;
            this.colPriceVoucher.Width = 100;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.DisplayFormat.FormatString = "#,###,###,##0.##;";
            this.repositoryItemTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.Mask.EditMask = "#,###,###,##0.##;";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colPriceNoVatVoucher
            // 
            this.colPriceNoVatVoucher.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colPriceNoVatVoucher.AppearanceCell.Options.UseFont = true;
            this.colPriceNoVatVoucher.AppearanceCell.Options.UseTextOptions = true;
            this.colPriceNoVatVoucher.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPriceNoVatVoucher.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colPriceNoVatVoucher.AppearanceHeader.Options.UseFont = true;
            this.colPriceNoVatVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colPriceNoVatVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceNoVatVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPriceNoVatVoucher.Caption = "Đơn giá hỗ trợ (chưa VAT)";
            this.colPriceNoVatVoucher.ColumnEdit = this.repositoryItemTextEdit2;
            this.colPriceNoVatVoucher.DisplayFormat.FormatString = "#,##0.##";
            this.colPriceNoVatVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPriceNoVatVoucher.FieldName = "PriceNoVatVoucher";
            this.colPriceNoVatVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colPriceNoVatVoucher.Name = "colPriceNoVatVoucher";
            this.colPriceNoVatVoucher.OptionsColumn.AllowEdit = false;
            this.colPriceNoVatVoucher.OptionsColumn.ReadOnly = true;
            this.colPriceNoVatVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPriceNoVatVoucher.Visible = true;
            this.colPriceNoVatVoucher.VisibleIndex = 4;
            this.colPriceNoVatVoucher.Width = 100;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.DisplayFormat.FormatString = "#,###,###,##0.##;";
            this.repositoryItemTextEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit2.Mask.EditMask = "#,###,###,##0.##;";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colTotalAmountNoVatVoucher
            // 
            this.colTotalAmountNoVatVoucher.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colTotalAmountNoVatVoucher.AppearanceCell.Options.UseFont = true;
            this.colTotalAmountNoVatVoucher.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colTotalAmountNoVatVoucher.AppearanceHeader.Options.UseFont = true;
            this.colTotalAmountNoVatVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountNoVatVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountNoVatVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountNoVatVoucher.Caption = "Thành tiền";
            this.colTotalAmountNoVatVoucher.ColumnEdit = this.repositoryItemTextEdit2;
            this.colTotalAmountNoVatVoucher.DisplayFormat.FormatString = "N0";
            this.colTotalAmountNoVatVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalAmountNoVatVoucher.FieldName = "TotalAmountNoVatVoucher";
            this.colTotalAmountNoVatVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmountNoVatVoucher.Name = "colTotalAmountNoVatVoucher";
            this.colTotalAmountNoVatVoucher.OptionsColumn.AllowEdit = false;
            this.colTotalAmountNoVatVoucher.OptionsColumn.ReadOnly = true;
            this.colTotalAmountNoVatVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmountNoVatVoucher.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountNoVatVoucher", "{0:N0}")});
            this.colTotalAmountNoVatVoucher.Visible = true;
            this.colTotalAmountNoVatVoucher.VisibleIndex = 5;
            this.colTotalAmountNoVatVoucher.Width = 100;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn8.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.bandedGridColumn8.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn8.Caption = "Thuế suất (%)";
            this.bandedGridColumn8.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.bandedGridColumn8.FieldName = "VatTypeID";
            this.bandedGridColumn8.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn8.Visible = true;
            this.bandedGridColumn8.VisibleIndex = 6;
            this.bandedGridColumn8.Width = 80;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VATTYPEID", "Loại thuế"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VATTYPENAME", "Mức thuế")});
            this.repositoryItemLookUpEdit1.DisplayMember = "VATTYPENAME";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.NullText = "--Chọn thuế suất HĐ--";
            this.repositoryItemLookUpEdit1.ValueMember = "VATTYPEID";
            // 
            // colVatAmountVoucher
            // 
            this.colVatAmountVoucher.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colVatAmountVoucher.AppearanceCell.Options.UseFont = true;
            this.colVatAmountVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colVatAmountVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVatAmountVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVatAmountVoucher.Caption = "Tiền thuế";
            this.colVatAmountVoucher.ColumnEdit = this.repositoryItemTextEdit2;
            this.colVatAmountVoucher.FieldName = "VatAmountVoucher";
            this.colVatAmountVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colVatAmountVoucher.Name = "colVatAmountVoucher";
            this.colVatAmountVoucher.OptionsColumn.AllowEdit = false;
            this.colVatAmountVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colVatAmountVoucher.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VatAmountVoucher", "{0:N0}")});
            this.colVatAmountVoucher.Visible = true;
            this.colVatAmountVoucher.VisibleIndex = 7;
            this.colVatAmountVoucher.Width = 100;
            // 
            // colTotalAmountVoucher
            // 
            this.colTotalAmountVoucher.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colTotalAmountVoucher.AppearanceCell.Options.UseFont = true;
            this.colTotalAmountVoucher.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colTotalAmountVoucher.AppearanceHeader.Options.UseFont = true;
            this.colTotalAmountVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountVoucher.Caption = "Tổng tiền (gồm VAT)";
            this.colTotalAmountVoucher.ColumnEdit = this.repositoryItemTextEdit2;
            this.colTotalAmountVoucher.DisplayFormat.FormatString = "N0";
            this.colTotalAmountVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalAmountVoucher.FieldName = "TotalAmountVoucher";
            this.colTotalAmountVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmountVoucher.Name = "colTotalAmountVoucher";
            this.colTotalAmountVoucher.OptionsColumn.AllowEdit = false;
            this.colTotalAmountVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmountVoucher.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountVoucher", "{0:N0}")});
            this.colTotalAmountVoucher.Visible = true;
            this.colTotalAmountVoucher.VisibleIndex = 8;
            this.colTotalAmountVoucher.Width = 100;
            // 
            // colQuantityNoInputVoucher
            // 
            this.colQuantityNoInputVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantityNoInputVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantityNoInputVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantityNoInputVoucher.Caption = "Số lượng chưa nhập";
            this.colQuantityNoInputVoucher.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantityNoInputVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityNoInputVoucher.FieldName = "QuantityNoInputVoucher";
            this.colQuantityNoInputVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantityNoInputVoucher.Name = "colQuantityNoInputVoucher";
            this.colQuantityNoInputVoucher.OptionsColumn.AllowEdit = false;
            this.colQuantityNoInputVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantityNoInputVoucher.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityNoInputVoucher", "{0:N0}")});
            this.colQuantityNoInputVoucher.Width = 80;
            // 
            // colQuantityStockVoucher
            // 
            this.colQuantityStockVoucher.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colQuantityStockVoucher.AppearanceCell.Options.UseFont = true;
            this.colQuantityStockVoucher.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colQuantityStockVoucher.AppearanceHeader.Options.UseFont = true;
            this.colQuantityStockVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantityStockVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantityStockVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantityStockVoucher.Caption = "SL tồn tại CN Trung tâm";
            this.colQuantityStockVoucher.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantityStockVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityStockVoucher.FieldName = "QuantityStockVoucher";
            this.colQuantityStockVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantityStockVoucher.Name = "colQuantityStockVoucher";
            this.colQuantityStockVoucher.OptionsColumn.AllowEdit = false;
            this.colQuantityStockVoucher.OptionsColumn.ReadOnly = true;
            this.colQuantityStockVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantityStockVoucher.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityStockVoucher", "{0:N0}")});
            this.colQuantityStockVoucher.Width = 80;
            // 
            // colQuantityInStockNonIsCenterVoucher
            // 
            this.colQuantityInStockNonIsCenterVoucher.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colQuantityInStockNonIsCenterVoucher.AppearanceCell.Options.UseFont = true;
            this.colQuantityInStockNonIsCenterVoucher.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colQuantityInStockNonIsCenterVoucher.AppearanceHeader.Options.UseFont = true;
            this.colQuantityInStockNonIsCenterVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantityInStockNonIsCenterVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantityInStockNonIsCenterVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantityInStockNonIsCenterVoucher.Caption = "SL tồn tại CN tỉnh";
            this.colQuantityInStockNonIsCenterVoucher.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantityInStockNonIsCenterVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityInStockNonIsCenterVoucher.FieldName = "QuantityInStockNonIsCenterVoucher";
            this.colQuantityInStockNonIsCenterVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantityInStockNonIsCenterVoucher.Name = "colQuantityInStockNonIsCenterVoucher";
            this.colQuantityInStockNonIsCenterVoucher.OptionsColumn.AllowEdit = false;
            this.colQuantityInStockNonIsCenterVoucher.OptionsColumn.ReadOnly = true;
            this.colQuantityInStockNonIsCenterVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantityInStockNonIsCenterVoucher.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityInStockNonIsCenterVoucher", "{0:N0}")});
            this.colQuantityInStockNonIsCenterVoucher.Width = 80;
            // 
            // colQuantityBuyVoucher
            // 
            this.colQuantityBuyVoucher.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colQuantityBuyVoucher.AppearanceCell.Options.UseFont = true;
            this.colQuantityBuyVoucher.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colQuantityBuyVoucher.AppearanceHeader.Options.UseFont = true;
            this.colQuantityBuyVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantityBuyVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantityBuyVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantityBuyVoucher.Caption = "SL bán tại CN trung tâm";
            this.colQuantityBuyVoucher.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantityBuyVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityBuyVoucher.FieldName = "QuantityBuyVoucher";
            this.colQuantityBuyVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantityBuyVoucher.Name = "colQuantityBuyVoucher";
            this.colQuantityBuyVoucher.OptionsColumn.AllowEdit = false;
            this.colQuantityBuyVoucher.OptionsColumn.ReadOnly = true;
            this.colQuantityBuyVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantityBuyVoucher.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityBuyVoucher", "{0:N0}")});
            this.colQuantityBuyVoucher.Width = 80;
            // 
            // colQuantityBuyNonIsCenterVoucher
            // 
            this.colQuantityBuyNonIsCenterVoucher.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colQuantityBuyNonIsCenterVoucher.AppearanceCell.Options.UseFont = true;
            this.colQuantityBuyNonIsCenterVoucher.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colQuantityBuyNonIsCenterVoucher.AppearanceHeader.Options.UseFont = true;
            this.colQuantityBuyNonIsCenterVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantityBuyNonIsCenterVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantityBuyNonIsCenterVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantityBuyNonIsCenterVoucher.Caption = "SL bán tại CN tỉnh";
            this.colQuantityBuyNonIsCenterVoucher.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantityBuyNonIsCenterVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityBuyNonIsCenterVoucher.FieldName = "QuantityBuyNonIsCenterVoucher";
            this.colQuantityBuyNonIsCenterVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantityBuyNonIsCenterVoucher.Name = "colQuantityBuyNonIsCenterVoucher";
            this.colQuantityBuyNonIsCenterVoucher.OptionsColumn.AllowEdit = false;
            this.colQuantityBuyNonIsCenterVoucher.OptionsColumn.ReadOnly = true;
            this.colQuantityBuyNonIsCenterVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantityBuyNonIsCenterVoucher.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityBuyNonIsCenterVoucher", "{0:N0}")});
            this.colQuantityBuyNonIsCenterVoucher.Width = 80;
            // 
            // colQuantityOtherIncomeVoucher
            // 
            this.colQuantityOtherIncomeVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantityOtherIncomeVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantityOtherIncomeVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantityOtherIncomeVoucher.Caption = "SL ghi nhận doanh thu khác";
            this.colQuantityOtherIncomeVoucher.DisplayFormat.FormatString = "#,##0.####";
            this.colQuantityOtherIncomeVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityOtherIncomeVoucher.FieldName = "QuantityOtherIncomeVoucher";
            this.colQuantityOtherIncomeVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantityOtherIncomeVoucher.Name = "colQuantityOtherIncomeVoucher";
            this.colQuantityOtherIncomeVoucher.OptionsColumn.AllowEdit = false;
            this.colQuantityOtherIncomeVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantityOtherIncomeVoucher.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityOtherIncomeVoucher", "{0:N0}")});
            this.colQuantityOtherIncomeVoucher.Width = 85;
            // 
            // colTotalAmountStockVoucher
            // 
            this.colTotalAmountStockVoucher.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colTotalAmountStockVoucher.AppearanceCell.Options.UseFont = true;
            this.colTotalAmountStockVoucher.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colTotalAmountStockVoucher.AppearanceHeader.Options.UseFont = true;
            this.colTotalAmountStockVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountStockVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountStockVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountStockVoucher.Caption = "Giá trị hàng tồn tại CN trung tâm";
            this.colTotalAmountStockVoucher.ColumnEdit = this.repositoryItemTextEdit2;
            this.colTotalAmountStockVoucher.DisplayFormat.FormatString = "N0";
            this.colTotalAmountStockVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalAmountStockVoucher.FieldName = "TotalAmountStockVoucher";
            this.colTotalAmountStockVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmountStockVoucher.Name = "colTotalAmountStockVoucher";
            this.colTotalAmountStockVoucher.OptionsColumn.AllowEdit = false;
            this.colTotalAmountStockVoucher.OptionsColumn.ReadOnly = true;
            this.colTotalAmountStockVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmountStockVoucher.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountStockVoucher", "{0:N0}")});
            this.colTotalAmountStockVoucher.Width = 120;
            // 
            // colTotalAmountStockNonIsCenterVoucher
            // 
            this.colTotalAmountStockNonIsCenterVoucher.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colTotalAmountStockNonIsCenterVoucher.AppearanceCell.Options.UseFont = true;
            this.colTotalAmountStockNonIsCenterVoucher.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colTotalAmountStockNonIsCenterVoucher.AppearanceHeader.Options.UseFont = true;
            this.colTotalAmountStockNonIsCenterVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountStockNonIsCenterVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountStockNonIsCenterVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountStockNonIsCenterVoucher.Caption = "Giá trị hàng tồn tại CN tỉnh";
            this.colTotalAmountStockNonIsCenterVoucher.ColumnEdit = this.repositoryItemTextEdit2;
            this.colTotalAmountStockNonIsCenterVoucher.DisplayFormat.FormatString = "N0";
            this.colTotalAmountStockNonIsCenterVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalAmountStockNonIsCenterVoucher.FieldName = "TotalAmountStockNonIsCenterVoucher";
            this.colTotalAmountStockNonIsCenterVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmountStockNonIsCenterVoucher.Name = "colTotalAmountStockNonIsCenterVoucher";
            this.colTotalAmountStockNonIsCenterVoucher.OptionsColumn.AllowEdit = false;
            this.colTotalAmountStockNonIsCenterVoucher.OptionsColumn.ReadOnly = true;
            this.colTotalAmountStockNonIsCenterVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmountStockNonIsCenterVoucher.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountStockNonIsCenterVoucher", "{0:N0}")});
            this.colTotalAmountStockNonIsCenterVoucher.Width = 120;
            // 
            // colTotalAmountBuyVoucher
            // 
            this.colTotalAmountBuyVoucher.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colTotalAmountBuyVoucher.AppearanceCell.Options.UseFont = true;
            this.colTotalAmountBuyVoucher.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colTotalAmountBuyVoucher.AppearanceHeader.Options.UseFont = true;
            this.colTotalAmountBuyVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountBuyVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountBuyVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountBuyVoucher.Caption = "Giá trị hàng bán tại CN trung tâm";
            this.colTotalAmountBuyVoucher.ColumnEdit = this.repositoryItemTextEdit2;
            this.colTotalAmountBuyVoucher.DisplayFormat.FormatString = "N0";
            this.colTotalAmountBuyVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalAmountBuyVoucher.FieldName = "TotalAmountBuyVoucher";
            this.colTotalAmountBuyVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmountBuyVoucher.Name = "colTotalAmountBuyVoucher";
            this.colTotalAmountBuyVoucher.OptionsColumn.AllowEdit = false;
            this.colTotalAmountBuyVoucher.OptionsColumn.ReadOnly = true;
            this.colTotalAmountBuyVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmountBuyVoucher.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountBuyVoucher", "{0:N0}")});
            this.colTotalAmountBuyVoucher.Width = 120;
            // 
            // colTotalAmountBuyNonIsCenterVoucher
            // 
            this.colTotalAmountBuyNonIsCenterVoucher.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.colTotalAmountBuyNonIsCenterVoucher.AppearanceCell.Options.UseFont = true;
            this.colTotalAmountBuyNonIsCenterVoucher.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.colTotalAmountBuyNonIsCenterVoucher.AppearanceHeader.Options.UseFont = true;
            this.colTotalAmountBuyNonIsCenterVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountBuyNonIsCenterVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountBuyNonIsCenterVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountBuyNonIsCenterVoucher.Caption = "Giá trị hàng bán tại CN tỉnh";
            this.colTotalAmountBuyNonIsCenterVoucher.ColumnEdit = this.repositoryItemTextEdit2;
            this.colTotalAmountBuyNonIsCenterVoucher.DisplayFormat.FormatString = "N0";
            this.colTotalAmountBuyNonIsCenterVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalAmountBuyNonIsCenterVoucher.FieldName = "TotalAmountBuyNonIsCenterVoucher";
            this.colTotalAmountBuyNonIsCenterVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmountBuyNonIsCenterVoucher.Name = "colTotalAmountBuyNonIsCenterVoucher";
            this.colTotalAmountBuyNonIsCenterVoucher.OptionsColumn.AllowEdit = false;
            this.colTotalAmountBuyNonIsCenterVoucher.OptionsColumn.ReadOnly = true;
            this.colTotalAmountBuyNonIsCenterVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmountBuyNonIsCenterVoucher.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountBuyNonIsCenterVoucher", "{0:N0}")});
            this.colTotalAmountBuyNonIsCenterVoucher.Width = 120;
            // 
            // colTotalAmountOtherIncomeVoucher
            // 
            this.colTotalAmountOtherIncomeVoucher.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountOtherIncomeVoucher.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountOtherIncomeVoucher.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountOtherIncomeVoucher.Caption = "Giá trị ghi nhận doanh thu khác";
            this.colTotalAmountOtherIncomeVoucher.DisplayFormat.FormatString = "N0";
            this.colTotalAmountOtherIncomeVoucher.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalAmountOtherIncomeVoucher.FieldName = "TotalAmountOtherIncomeVoucher";
            this.colTotalAmountOtherIncomeVoucher.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmountOtherIncomeVoucher.Name = "colTotalAmountOtherIncomeVoucher";
            this.colTotalAmountOtherIncomeVoucher.OptionsColumn.AllowEdit = false;
            this.colTotalAmountOtherIncomeVoucher.OptionsColumn.ReadOnly = true;
            this.colTotalAmountOtherIncomeVoucher.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmountOtherIncomeVoucher.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountOtherIncomeVoucher", "{0:N0}")});
            this.colTotalAmountOtherIncomeVoucher.Width = 120;
            // 
            // tabDifferent
            // 
            this.tabDifferent.Controls.Add(this.grdDifference);
            this.tabDifferent.Location = new System.Drawing.Point(4, 25);
            this.tabDifferent.Name = "tabDifferent";
            this.tabDifferent.Size = new System.Drawing.Size(1066, 353);
            this.tabDifferent.TabIndex = 2;
            this.tabDifferent.Text = "Chênh lệch";
            this.tabDifferent.UseVisualStyleBackColor = true;
            // 
            // grdDifference
            // 
            this.grdDifference.ContextMenuStrip = this.mnuAction;
            this.grdDifference.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDifference.Location = new System.Drawing.Point(0, 0);
            this.grdDifference.MainView = this.grvDifference;
            this.grdDifference.Name = "grdDifference";
            this.grdDifference.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit2});
            this.grdDifference.Size = new System.Drawing.Size(1066, 353);
            this.grdDifference.TabIndex = 2;
            this.grdDifference.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvDifference});
            // 
            // grvDifference
            // 
            this.grvDifference.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvDifference.Appearance.FooterPanel.Options.UseFont = true;
            this.grvDifference.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvDifference.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvDifference.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvDifference.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvDifference.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grvDifference.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvDifference.Appearance.Row.Options.UseFont = true;
            this.grvDifference.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand,
            this.gridBand2,
            this.gridBand3,
            this.gridBand1});
            this.grvDifference.ColumnPanelRowHeight = 50;
            this.grvDifference.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn16,
            this.gridColumn13,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn36,
            this.bandedGridColumn10,
            this.bandedGridColumn9,
            this.bandedGridColumn6,
            this.bandedGridColumn7,
            this.bandedGridColumn5,
            this.bandedGridColumn11,
            this.bandedGridColumn12,
            this.bandedGridColumn13,
            this.bandedGridColumn14,
            this.bandedGridColumn15,
            this.bandedGridColumn16,
            this.bandedGridColumn17,
            this.colQuantityNoInputDifference,
            this.bandedGridColumn19,
            this.bandedGridColumn20,
            this.bandedGridColumn21,
            this.bandedGridColumn22,
            this.bandedGridColumn18,
            this.bandedGridColumn23,
            this.bandedGridColumn24,
            this.bandedGridColumn25,
            this.bandedGridColumn26,
            this.bandedGridColumn27});
            this.grvDifference.GridControl = this.grdDifference;
            this.grvDifference.Name = "grvDifference";
            this.grvDifference.OptionsNavigation.UseTabKey = false;
            this.grvDifference.OptionsView.ColumnAutoWidth = false;
            this.grvDifference.OptionsView.ShowAutoFilterRow = true;
            this.grvDifference.OptionsView.ShowFooter = true;
            this.grvDifference.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand
            // 
            this.gridBand.Columns.Add(this.gridColumn3);
            this.gridBand.Columns.Add(this.gridColumn4);
            this.gridBand.Columns.Add(this.gridColumn6);
            this.gridBand.Columns.Add(this.gridColumn7);
            this.gridBand.Name = "gridBand";
            this.gridBand.Width = 350;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn3.AppearanceCell.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "Mã phiếu nhập";
            this.gridColumn3.FieldName = "InputVoucherID";
            this.gridColumn3.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Width = 130;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn4.AppearanceCell.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn4.Caption = "Số hóa đơn";
            this.gridColumn4.FieldName = "InVoiceID";
            this.gridColumn4.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn4.Width = 85;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn6.AppearanceCell.Options.UseFont = true;
            this.gridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn6.Caption = "Mã sản phẩm";
            this.gridColumn6.FieldName = "ProductID";
            this.gridColumn6.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "ProductID", "Tổng cộng: {0:N0}")});
            this.gridColumn6.Visible = true;
            this.gridColumn6.Width = 120;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn7.AppearanceCell.Options.UseFont = true;
            this.gridColumn7.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn7.Caption = "Tên sản phẩm";
            this.gridColumn7.FieldName = "ProductName";
            this.gridColumn7.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn7.Visible = true;
            this.gridColumn7.Width = 230;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand2.Caption = "Hỗ trợ lần 1";
            this.gridBand2.Columns.Add(this.gridColumn8);
            this.gridBand2.Columns.Add(this.gridColumn11);
            this.gridBand2.Columns.Add(this.gridColumn12);
            this.gridBand2.Columns.Add(this.gridColumn16);
            this.gridBand2.Columns.Add(this.gridColumn13);
            this.gridBand2.Columns.Add(this.gridColumn17);
            this.gridBand2.Columns.Add(this.gridColumn18);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 660;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn8.AppearanceCell.Options.UseFont = true;
            this.gridColumn8.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn8.AppearanceHeader.Options.UseFont = true;
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn8.Caption = "SL hỗ trợ";
            this.gridColumn8.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.gridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn8.FieldName = "InStockQuantity";
            this.gridColumn8.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "InStockQuantity", "{0:#,##0;(#,##0)}")});
            this.gridColumn8.Visible = true;
            this.gridColumn8.Width = 80;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn11.AppearanceCell.Options.UseFont = true;
            this.gridColumn11.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn11.Caption = "Đơn giá hỗ trợ (gồm VAT)";
            this.gridColumn11.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn11.FieldName = "Price";
            this.gridColumn11.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn11.Visible = true;
            this.gridColumn11.Width = 100;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn12.AppearanceCell.Options.UseFont = true;
            this.gridColumn12.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn12.AppearanceHeader.Options.UseFont = true;
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn12.Caption = "Đơn giá hỗ trợ (chưa VAT)";
            this.gridColumn12.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn12.FieldName = "PriceNoVAT";
            this.gridColumn12.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn12.Visible = true;
            this.gridColumn12.Width = 100;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn16.AppearanceCell.Options.UseFont = true;
            this.gridColumn16.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn16.AppearanceHeader.Options.UseFont = true;
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn16.Caption = "Thành tiền";
            this.gridColumn16.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.gridColumn16.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn16.FieldName = "TotalAmountNoVat";
            this.gridColumn16.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn16.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountNoVat", "{0:#,##0;(#,##0)}")});
            this.gridColumn16.Visible = true;
            this.gridColumn16.Width = 100;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn13.AppearanceCell.Options.UseFont = true;
            this.gridColumn13.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn13.AppearanceHeader.Options.UseFont = true;
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn13.Caption = "Thuế suất (%)";
            this.gridColumn13.ColumnEdit = this.repositoryItemLookUpEdit2;
            this.gridColumn13.FieldName = "VatTypeID";
            this.gridColumn13.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn13.Visible = true;
            this.gridColumn13.Width = 80;
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VATTYPEID", "Loại thuế"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VATTYPENAME", "Mức thuế")});
            this.repositoryItemLookUpEdit2.DisplayMember = "VATTYPENAME";
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            this.repositoryItemLookUpEdit2.NullText = "--Chọn thuế suất HĐ--";
            this.repositoryItemLookUpEdit2.ValueMember = "VATTYPEID";
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn17.AppearanceCell.Options.UseFont = true;
            this.gridColumn17.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn17.AppearanceHeader.Options.UseFont = true;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn17.Caption = "Tiền thuế";
            this.gridColumn17.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.gridColumn17.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn17.FieldName = "VatAmount";
            this.gridColumn17.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn17.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VATAmount", "{0:#,##0;(#,##0)}")});
            this.gridColumn17.Visible = true;
            this.gridColumn17.Width = 100;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn18.AppearanceCell.Options.UseFont = true;
            this.gridColumn18.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn18.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn18.AppearanceHeader.Options.UseFont = true;
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn18.Caption = "Tổng tiền (gồm VAT)";
            this.gridColumn18.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.gridColumn18.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn18.FieldName = "TotalAmount";
            this.gridColumn18.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn18.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmount", "{0:#,##0;(#,##0)}")});
            this.gridColumn18.Visible = true;
            this.gridColumn18.Width = 100;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand3.Caption = "Hỗ trợ lần 2";
            this.gridBand3.Columns.Add(this.gridColumn36);
            this.gridBand3.Columns.Add(this.bandedGridColumn10);
            this.gridBand3.Columns.Add(this.bandedGridColumn9);
            this.gridBand3.Columns.Add(this.bandedGridColumn6);
            this.gridBand3.Columns.Add(this.bandedGridColumn7);
            this.gridBand3.Columns.Add(this.bandedGridColumn5);
            this.gridBand3.Columns.Add(this.bandedGridColumn11);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 655;
            // 
            // gridColumn36
            // 
            this.gridColumn36.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridColumn36.AppearanceCell.Options.UseFont = true;
            this.gridColumn36.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn36.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn36.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridColumn36.AppearanceHeader.Options.UseFont = true;
            this.gridColumn36.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn36.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn36.Caption = "SL hỗ trợ";
            this.gridColumn36.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.gridColumn36.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn36.FieldName = "Quantity";
            this.gridColumn36.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn36.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Quantity", "{0:#,##0;(#,##0)}")});
            this.gridColumn36.Visible = true;
            this.gridColumn36.Width = 80;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn10.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn10.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn10.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn10.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn10.Caption = "Đơn giá hỗ trợ (gồm VAT)";
            this.bandedGridColumn10.DisplayFormat.FormatString = "#,##0.####";
            this.bandedGridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn10.FieldName = "PriceVoucher";
            this.bandedGridColumn10.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn10.Visible = true;
            this.bandedGridColumn10.Width = 100;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn9.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn9.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn9.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn9.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn9.Caption = "Đơn giá hỗ trợ (chưa VAT)";
            this.bandedGridColumn9.DisplayFormat.FormatString = "#,##0.####";
            this.bandedGridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn9.FieldName = "PriceNoVatVoucher";
            this.bandedGridColumn9.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn9.Visible = true;
            this.bandedGridColumn9.Width = 100;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn6.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn6.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn6.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn6.Caption = "Thành tiền";
            this.bandedGridColumn6.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn6.FieldName = "TotalAmountNoVatVoucher";
            this.bandedGridColumn6.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountNoVatVoucher", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn6.Visible = true;
            this.bandedGridColumn6.Width = 100;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn7.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn7.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn7.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn7.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn7.Caption = "Thuế suất (%)";
            this.bandedGridColumn7.ColumnEdit = this.repositoryItemLookUpEdit2;
            this.bandedGridColumn7.FieldName = "VatTypeID";
            this.bandedGridColumn7.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn7.Visible = true;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn5.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn5.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn5.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn5.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn5.Caption = "Tiền thuế";
            this.bandedGridColumn5.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn5.FieldName = "VatAmountVoucher";
            this.bandedGridColumn5.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VatAmountVoucher", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn5.Visible = true;
            this.bandedGridColumn5.Width = 100;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn11.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn11.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn11.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn11.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn11.Caption = "Tổng tiền (gồm VAT)";
            this.bandedGridColumn11.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn11.FieldName = "TotalAmountVoucher";
            this.bandedGridColumn11.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn11.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountVoucher", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn11.Visible = true;
            this.bandedGridColumn11.Width = 100;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridBand1.Caption = "Chênh lệch";
            this.gridBand1.Columns.Add(this.bandedGridColumn12);
            this.gridBand1.Columns.Add(this.bandedGridColumn13);
            this.gridBand1.Columns.Add(this.bandedGridColumn14);
            this.gridBand1.Columns.Add(this.bandedGridColumn15);
            this.gridBand1.Columns.Add(this.bandedGridColumn16);
            this.gridBand1.Columns.Add(this.bandedGridColumn17);
            this.gridBand1.Columns.Add(this.colQuantityNoInputDifference);
            this.gridBand1.Columns.Add(this.bandedGridColumn19);
            this.gridBand1.Columns.Add(this.bandedGridColumn20);
            this.gridBand1.Columns.Add(this.bandedGridColumn21);
            this.gridBand1.Columns.Add(this.bandedGridColumn22);
            this.gridBand1.Columns.Add(this.bandedGridColumn18);
            this.gridBand1.Columns.Add(this.bandedGridColumn23);
            this.gridBand1.Columns.Add(this.bandedGridColumn24);
            this.gridBand1.Columns.Add(this.bandedGridColumn25);
            this.gridBand1.Columns.Add(this.bandedGridColumn26);
            this.gridBand1.Columns.Add(this.bandedGridColumn27);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 1895;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn12.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn12.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn12.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn12.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn12.Caption = "SL hỗ trợ";
            this.bandedGridColumn12.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn12.FieldName = "QuantityDifference";
            this.bandedGridColumn12.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn12.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn12.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityDifference", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn12.Visible = true;
            // 
            // bandedGridColumn13
            // 
            this.bandedGridColumn13.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn13.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn13.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn13.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn13.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn13.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn13.Caption = "Đơn giá hỗ trợ (gồm VAT)";
            this.bandedGridColumn13.DisplayFormat.FormatString = "#,##0.####";
            this.bandedGridColumn13.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn13.FieldName = "PriceDifference";
            this.bandedGridColumn13.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn13.Name = "bandedGridColumn13";
            this.bandedGridColumn13.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn13.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn13.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PriceDifference", "{0:#,##0.####}")});
            this.bandedGridColumn13.Visible = true;
            this.bandedGridColumn13.Width = 100;
            // 
            // bandedGridColumn14
            // 
            this.bandedGridColumn14.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn14.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn14.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn14.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn14.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn14.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn14.Caption = "Đơn giá hỗ trợ (chưa VAT)";
            this.bandedGridColumn14.DisplayFormat.FormatString = "#,##0.####";
            this.bandedGridColumn14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn14.FieldName = "PriceNoVATDifference";
            this.bandedGridColumn14.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn14.Name = "bandedGridColumn14";
            this.bandedGridColumn14.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn14.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn14.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PriceNoVATDifference", "{0:#,##0.####}")});
            this.bandedGridColumn14.Visible = true;
            this.bandedGridColumn14.Width = 100;
            // 
            // bandedGridColumn15
            // 
            this.bandedGridColumn15.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn15.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn15.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn15.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn15.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn15.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn15.Caption = "Thành tiền";
            this.bandedGridColumn15.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn15.FieldName = "TotalAmountNoVatDifference";
            this.bandedGridColumn15.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn15.Name = "bandedGridColumn15";
            this.bandedGridColumn15.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn15.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn15.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountNoVatDifference", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn15.Visible = true;
            this.bandedGridColumn15.Width = 100;
            // 
            // bandedGridColumn16
            // 
            this.bandedGridColumn16.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn16.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn16.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn16.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn16.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn16.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn16.Caption = "Tiền thuế";
            this.bandedGridColumn16.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn16.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn16.FieldName = "VatAmountDifference";
            this.bandedGridColumn16.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn16.Name = "bandedGridColumn16";
            this.bandedGridColumn16.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn16.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn16.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VatAmountDifference", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn16.Visible = true;
            this.bandedGridColumn16.Width = 100;
            // 
            // bandedGridColumn17
            // 
            this.bandedGridColumn17.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridColumn17.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn17.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn17.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn17.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bandedGridColumn17.Caption = "Tổng tiền (gồm VAT)";
            this.bandedGridColumn17.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn17.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn17.FieldName = "TotalAmountDifference";
            this.bandedGridColumn17.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn17.Name = "bandedGridColumn17";
            this.bandedGridColumn17.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn17.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn17.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountDifference", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn17.Visible = true;
            this.bandedGridColumn17.Width = 100;
            // 
            // colQuantityNoInputDifference
            // 
            this.colQuantityNoInputDifference.Caption = "Số lượng chưa nhập";
            this.colQuantityNoInputDifference.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.colQuantityNoInputDifference.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQuantityNoInputDifference.FieldName = "QuantityNoInputDifference";
            this.colQuantityNoInputDifference.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQuantityNoInputDifference.Name = "colQuantityNoInputDifference";
            this.colQuantityNoInputDifference.OptionsColumn.AllowEdit = false;
            this.colQuantityNoInputDifference.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantityNoInputDifference.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityNoInputDifference", "{0:#,##0;(#,##0)}")});
            this.colQuantityNoInputDifference.Visible = true;
            this.colQuantityNoInputDifference.Width = 120;
            // 
            // bandedGridColumn19
            // 
            this.bandedGridColumn19.Caption = "SL tồn tại CN Trung tâm";
            this.bandedGridColumn19.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn19.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn19.FieldName = "QuantityStockDifference";
            this.bandedGridColumn19.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn19.Name = "bandedGridColumn19";
            this.bandedGridColumn19.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn19.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn19.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityStockDifference", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn19.Visible = true;
            this.bandedGridColumn19.Width = 120;
            // 
            // bandedGridColumn20
            // 
            this.bandedGridColumn20.Caption = "SL tồn tại CN tỉnh";
            this.bandedGridColumn20.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn20.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn20.FieldName = "QuantityInStockNonIsCenterDifference";
            this.bandedGridColumn20.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn20.Name = "bandedGridColumn20";
            this.bandedGridColumn20.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn20.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn20.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityInStockNonIsCenterDifference", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn20.Visible = true;
            this.bandedGridColumn20.Width = 120;
            // 
            // bandedGridColumn21
            // 
            this.bandedGridColumn21.Caption = "SL bán tại CN trung tâm";
            this.bandedGridColumn21.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn21.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn21.FieldName = "QuantityBuyDifference";
            this.bandedGridColumn21.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn21.Name = "bandedGridColumn21";
            this.bandedGridColumn21.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn21.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn21.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityBuyDifference", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn21.Visible = true;
            this.bandedGridColumn21.Width = 120;
            // 
            // bandedGridColumn22
            // 
            this.bandedGridColumn22.Caption = "SL bán tại CN tỉnh";
            this.bandedGridColumn22.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn22.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn22.FieldName = "QuantityBuyNonIsCenterDifference";
            this.bandedGridColumn22.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn22.Name = "bandedGridColumn22";
            this.bandedGridColumn22.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn22.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn22.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityBuyNonIsCenterDifference", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn22.Visible = true;
            this.bandedGridColumn22.Width = 120;
            // 
            // bandedGridColumn18
            // 
            this.bandedGridColumn18.Caption = "SL ghi nhận doanh thu khác";
            this.bandedGridColumn18.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn18.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn18.FieldName = "QuantityOtherIncomeDifference";
            this.bandedGridColumn18.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn18.Name = "bandedGridColumn18";
            this.bandedGridColumn18.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn18.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn18.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuantityOtherIncomeDifference", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn18.Visible = true;
            this.bandedGridColumn18.Width = 120;
            // 
            // bandedGridColumn23
            // 
            this.bandedGridColumn23.Caption = "Giá trị hàng tồn tại CN trung tâm";
            this.bandedGridColumn23.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn23.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn23.FieldName = "TotalAmountStockDifference";
            this.bandedGridColumn23.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn23.Name = "bandedGridColumn23";
            this.bandedGridColumn23.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn23.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn23.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountStockDifference", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn23.Visible = true;
            this.bandedGridColumn23.Width = 120;
            // 
            // bandedGridColumn24
            // 
            this.bandedGridColumn24.Caption = "Giá trị hàng tồn tại CN tỉnh";
            this.bandedGridColumn24.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn24.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn24.FieldName = "TotalAmountStockNonIsCenterDifference";
            this.bandedGridColumn24.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn24.Name = "bandedGridColumn24";
            this.bandedGridColumn24.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn24.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn24.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountStockNonIsCenterDifference", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn24.Visible = true;
            this.bandedGridColumn24.Width = 120;
            // 
            // bandedGridColumn25
            // 
            this.bandedGridColumn25.Caption = "Giá trị hàng bán tại CN trung tâm";
            this.bandedGridColumn25.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn25.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn25.FieldName = "TotalAmountBuyDifference";
            this.bandedGridColumn25.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn25.Name = "bandedGridColumn25";
            this.bandedGridColumn25.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn25.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn25.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountBuyDifference", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn25.Visible = true;
            this.bandedGridColumn25.Width = 120;
            // 
            // bandedGridColumn26
            // 
            this.bandedGridColumn26.Caption = "Giá trị hàng bán tại CN tỉnh";
            this.bandedGridColumn26.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn26.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn26.FieldName = "TotalAmountBuyNonIsCenterDifference";
            this.bandedGridColumn26.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn26.Name = "bandedGridColumn26";
            this.bandedGridColumn26.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn26.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn26.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountBuyNonIsCenterDifference", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn26.Visible = true;
            this.bandedGridColumn26.Width = 120;
            // 
            // bandedGridColumn27
            // 
            this.bandedGridColumn27.Caption = "Giá trị ghi nhận doanh thu khác";
            this.bandedGridColumn27.DisplayFormat.FormatString = "#,##0;(#,##0)";
            this.bandedGridColumn27.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.bandedGridColumn27.FieldName = "TotalAmountOtherIncomeDifference";
            this.bandedGridColumn27.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.bandedGridColumn27.Name = "bandedGridColumn27";
            this.bandedGridColumn27.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn27.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn27.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAmountOtherIncomeDifference", "{0:#,##0;(#,##0)}")});
            this.bandedGridColumn27.Visible = true;
            this.bandedGridColumn27.Width = 120;
            // 
            // frmPriceProtectStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1074, 617);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmPriceProtectStock";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hỗ trợ giá hàng tồn";
            this.Load += new System.EventHandler(this.frmPriceProtectStock_Load);
            this.mnuAction.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popReivewStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            this.tabGeneral.PerformLayout();
            this.tabCustomer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdCustomer)).EndInit();
            this.mnuCustomer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChecked)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textbox400)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textbox300)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textbox100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textbox20)).EndInit();
            this.tabAttachment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAttachment)).EndInit();
            this.mnuAttachment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvAttachment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDownload)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPriceProtect1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repVAT)).EndInit();
            this.tabPriceProtect2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDataPriceProtect1)).EndInit();
            this.mnuPriceProtect1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvDataPriceProtect1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            this.tabDifferent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDifference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvDifference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbPriceProtectNO;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Button btnSearchProduct;
        private System.Windows.Forms.Label lbBarcode;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.TextBox txtPriceProtectID;
        private System.Windows.Forms.ComboBox cboStatus;
        private System.Windows.Forms.DateTimePicker dtpInputFromDate;
        private System.Windows.Forms.DateTimePicker dtpPriceProtectDate;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnCalStock;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDel;
        private System.Windows.Forms.ToolStripMenuItem mnuItemViewIMEI;
        private System.Windows.Forms.Button btnSearchBill;
        private System.Windows.Forms.DateTimePicker dtpInputToDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboInvoiceTransactionType;
        private System.Windows.Forms.DateTimePicker dtpCallStock;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpVoucher;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStoreID;
        private System.Windows.Forms.ToolStripMenuItem mnuItemImportExcel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabGeneral;
        private System.Windows.Forms.TabPage tabCustomer;
        private System.Windows.Forms.TabPage tabAttachment;
        private DevExpress.XtraGrid.GridControl grdCustomer;
        private DevExpress.XtraGrid.Views.Grid.GridView grvCustomer;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomerName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn colIsDefault;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repChecked;
        private DevExpress.XtraGrid.GridControl grdAttachment;
        private DevExpress.XtraGrid.Views.Grid.GridView grvAttachment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn colView;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnView;
        private DevExpress.XtraGrid.Columns.GridColumn colDownload;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDownload;
        private System.Windows.Forms.ContextMenuStrip mnuCustomer;
        private System.Windows.Forms.ToolStripMenuItem mnuItemAddCustomer;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDeleteCustomer;
        private System.Windows.Forms.ContextMenuStrip mnuAttachment;
        private System.Windows.Forms.ToolStripMenuItem mnuAddAttachment;
        private System.Windows.Forms.ToolStripMenuItem mnuDelAttachment;
        private System.Windows.Forms.TextBox txtPriceProtectNO;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit textbox400;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit textbox300;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit textbox100;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit textbox20;
        private System.Windows.Forms.CheckBox chkIsSupplierCall;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcel;
        private System.Windows.Forms.ToolStripMenuItem mnuItemImportValues;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportTemplate;
        private System.Windows.Forms.DateTimePicker dtpPostdate;
        private System.Windows.Forms.Label label2;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboPriceProtectType;
        private System.Windows.Forms.ToolStripMenuItem mnuItemInputIMEI;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPriceProtect1;
        private DevExpress.XtraGrid.GridControl grdDataPriceProtect1;
        private DevExpress.XtraGrid.Views.Grid.GridView grvDataPriceProtect1;
        private DevExpress.XtraGrid.Columns.GridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn bandedGridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colQuatity;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceVoucher;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceNoVatVoucher;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn bandedGridColumn8;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountNoVatVoucher;
        private DevExpress.XtraGrid.Columns.GridColumn colVatAmountVoucher;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountVoucher;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityStockVoucher;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityInStockNonIsCenterVoucher;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityBuyVoucher;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityBuyNonIsCenterVoucher;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityOtherIncomeVoucher;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountStockVoucher;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountStockNonIsCenterVoucher;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountBuyVoucher;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountBuyNonIsCenterVoucher;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountOtherIncomeVoucher;
        private System.Windows.Forms.TabPage tabPriceProtect2;
        private System.Windows.Forms.TabPage tabDifferent;
        private System.Windows.Forms.ContextMenuStrip mnuPriceProtect1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemViewIMEIProtect1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemInputIMEIProtect1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem mnuItemImportValues1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportTemplateProtect1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcelProtect1;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn colInStockQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colPrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceNoVat;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTotalAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colVat;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountNoVat;
        private DevExpress.XtraGrid.Columns.GridColumn colVATAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityStock;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityInStockNonIsCenter;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityBuy;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityBuyNonIsCenter;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityOtherIncome;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountStock;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountStockNonIsCenter;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountBuy;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountBuyNonIsCenter;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountOtherIncome;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityNoInput;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityNoInputVoucher;
        private DevExpress.XtraGrid.GridControl grdDifference;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView grvDifference;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn36;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn17;
        private DevExpress.XtraEditors.DropDownButton btnReviewOrder;
        private DevExpress.XtraBars.PopupMenu popReivewStatus;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQuantityNoInputDifference;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn23;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn24;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn26;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn27;
    }
}