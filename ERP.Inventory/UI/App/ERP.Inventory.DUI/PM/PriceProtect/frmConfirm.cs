﻿using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.PM.PriceProtect
{
    public partial class frmConfirm : Form
    {
        private bool bolIsConfirm = false;
        private decimal? decTotal = null;

        public decimal? Total
        {
            get { return decTotal; }
            set { decTotal = value; }
        }
        public bool IsConfirm
        {
            get { return bolIsConfirm; }
            set { bolIsConfirm = value; }
        }

        public frmConfirm()
        {
            InitializeComponent();
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            IsConfirm = false;
            this.Close();
        }
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtTotal.Text.Trim()))
            {
                MessageBoxObject.ShowWarningMessage(this,"Vui lòng nhập tổng tiền cần xác nhận!");
                txtTotal.Focus();
                return;
            }
            else
            {
                if(!string.IsNullOrEmpty(Total.ToString()))
                {
                    if (Convert.ToDecimal(txtTotal.Text) == Convert.ToDecimal(Total))
                    {
                        IsConfirm = true;
                        this.Close();
                    }
                    else
                    {
                        IsConfirm = false;
                        MessageBoxObject.ShowWarningMessage(this, "Số tiền nhập khác với tổng tiền phiếu!");
                        txtTotal.Focus();
                    }
                        
                }
            }
        }

        private void txtTotal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnConfirm_Click(null, null);
        }
    }
}
