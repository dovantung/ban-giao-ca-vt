﻿using ERP.Inventory.PLC.WSPriceProtect;
using ERP.Inventory.PLC.WSProductInStock;
using ERP.MasterData.DUI.Search;
using Library.AppCore.LoadControls;
using Library.AppCore.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.PM.PriceProtect
{
    public partial class frmPriceProtectByBill : Form
    {
        private List<PriceProtectDetail> lstPriceProtectDetail = null;
        private List<PriceProtectDetail_IMEI> lstPriceProtectDetailImei = null;
        private Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection objSelection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection();
        private Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        private DataTable dtbInputVoucherDetail = null;
        private DataTable dtbProductImei = null;
        private string strCustomerIDList = string.Empty;
        private string strProductIDList = string.Empty;
        private DateTime dteInputFromDate;
        private DateTime dteInputToDate;

        public DateTime InputFromDate
        {
            get { return dteInputFromDate; }
            set { dteInputFromDate = value; }
        }

        public DateTime InputToDate
        {
            get { return dteInputToDate; }
            set { dteInputToDate = value; }
        }
        public string CustomerIDList
        {
            get { return strCustomerIDList; }
            set { strCustomerIDList = value; }
        }

        public string ProductIDList
        {
            get { return strProductIDList; }
            set { strProductIDList = value; }
        }

        public List<PriceProtectDetail> PriceProtectDetailList
        {
            get { return lstPriceProtectDetail; }
            set { lstPriceProtectDetail = value; }
        }

        public List<PriceProtectDetail_IMEI> PriceProtectDetailImeiList
        {
            get { return lstPriceProtectDetailImei; }
            set { lstPriceProtectDetailImei = value; }
        }
        public frmPriceProtectByBill()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
        }
        private void frmPriceProtectByBill_Load(object sender, EventArgs e)
        {
            InitControl();
        }
        private void InitControl()
        {
            objSelection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection(true, true, grdViewData);
            objSelection.CheckMarkColumn.Width = 40;
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            dtpInputFromDate.Value = Convert.ToDateTime(InputFromDate);
            dtpInputToDate.Value = Convert.ToDateTime(InputToDate);
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            List<PriceProtectDetail> lstTemp = new List<PriceProtectDetail>();
            if (lstPriceProtectDetailImei == null)
                lstPriceProtectDetailImei = new List<PriceProtectDetail_IMEI>();
            else
                lstPriceProtectDetailImei.Clear();

            if (objSelection.SelectedCount > 0)
            {
                for(int i = 0; i < objSelection.SelectedCount; i++)
                {
                    DataRowView rowView = (DataRowView)objSelection.GetSelectedRow(i);

                    var item = lstTemp.Where(r => r.ProductID == rowView["PRODUCTID"].ToString().Trim() & r.ProductID == rowView["INPUTVOUCHERID"].ToString().Trim()).FirstOrDefault();
                    if (item != null)
                    {
                        item.InVoiceID += ";" + rowView["INVOICENO"].ToString().Trim();
                        item.Quantity += Convert.ToInt32(rowView["QUANTITY"]);
                        item.InStockQuantity += Convert.ToInt32(rowView["QUANTITY"]);
                    }
                    else
                    {
                        PriceProtectDetail obj = new PriceProtectDetail();
                        obj.InputVoucherID = rowView["INPUTVOUCHERID"].ToString().Trim();
                        obj.InVoiceID = rowView["INVOICENO"].ToString().Trim();
                        obj.ProductID = rowView["PRODUCTID"].ToString().Trim();
                        obj.ProductName = rowView["PRODUCTNAME"].ToString().Trim();
                        obj.VatPercent = Convert.ToDecimal(rowView["VATPERCENT"]);
                        obj.VatTypeID = Convert.ToInt32(rowView["VAT"]);
                        obj.Vat = Convert.ToInt32(rowView["VAT"]);
                        obj.IsRequestIMEI = Convert.ToBoolean(rowView["ISREQUESTIMEI"]);
                        obj.Quantity = Convert.ToInt32(rowView["QUANTITY"]);
                        obj.InStockQuantity = Convert.ToInt32(rowView["QUANTITY"]);
                        if (rowView["INVOICEDATE"] != null)
                            obj.InVoiceDate = Convert.ToDateTime(rowView["INVOICEDATE"]);
                        lstTemp.Add(obj);
                    }
                }   
            }
            else
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn phiếu cần hỗ trợ giá!");
                grdViewData.Focus();
                return;
            }

            // lấy danh sách imei
            PriceProtectDetail_IMEI objPriceProtectDetail_IMEI = null;
            int intQuantity = 0;
            foreach (var item in lstTemp)
            {
                if (!item.IsRequestIMEI)
                    continue;
                intQuantity = 0;
                DataRow[] rowIMEI = dtbProductImei.Select("PRODUCTID = '" + item.ProductID.Trim() + "' AND INPUTVOUCHERID = '" + item.InputVoucherID + "'");
                foreach (DataRow row in rowIMEI)
                {
                    intQuantity++;
                    if (intQuantity > item.InStockQuantity)
                        break;

                    objPriceProtectDetail_IMEI = new PriceProtectDetail_IMEI();
                    objPriceProtectDetail_IMEI.ProductID = row["PRODUCTID"].ToString().Trim();
                    objPriceProtectDetail_IMEI.StoreID = Convert.ToInt32(row["INPUTSTOREID"]);
                    objPriceProtectDetail_IMEI.StoreName = Convert.ToString(row["STORENAME"]);
                    objPriceProtectDetail_IMEI.ProductIMEI = Convert.ToString(row["IMEI"]).Trim();
                    objPriceProtectDetail_IMEI.InputVoucherID = Convert.ToString(row["INPUTVOUCHERID"]);
                    if (row["INPUTDATE"] != DBNull.Value)
                        objPriceProtectDetail_IMEI.InputDate = Convert.ToDateTime(row["INPUTDATE"]);
                    else
                        objPriceProtectDetail_IMEI.InputDate = null;
                    objPriceProtectDetail_IMEI.IsValid = true;
                    lstPriceProtectDetailImei.Add(objPriceProtectDetail_IMEI);
                }
            }

            if (lstTemp != null && lstTemp.Count() > 0)
            {
                PriceProtectDetailList = (from itm in lstTemp
                                         group itm by new { itm.ProductID, itm.ProductName, itm.Vat, itm.VatTypeID, itm.IsRequestIMEI } into g
                                         select new PriceProtectDetail
                                         {
                                             ProductID = g.Key.ProductID,
                                             ProductName = g.Key.ProductName,
                                             Vat = g.Key.Vat,
                                             VatTypeID = g.Key.VatTypeID,
                                             IsRequestIMEI = g.Key.IsRequestIMEI,
                                             InStockQuantity = g.Sum(r=>r.Quantity),
                                             Quantity = g.Sum(r => r.Quantity)
                                         }).ToList();

                foreach (var item in PriceProtectDetailList)
                {
                    string[] strID = lstTemp.Where(r => r.ProductID == item.ProductID).Select(r=>r.InputVoucherID).ToArray();
                    string[] strInvoice = lstTemp.Where(r => r.ProductID == item.ProductID).Select(r => r.InVoiceID).ToArray();
                    item.InputVoucherID = String.Join(";", strID);
                    item.InVoiceID = String.Join(";", strInvoice);
                }
            }

            if (chkIsSearchByInputDate.Checked)
            {
                InputFromDate = dtpInputFromDate.Value;
                InputToDate = dtpInputToDate.Value;
            }
            else
            {
                InputFromDate = dtpInvoiceFromDate.Value;
                InputToDate = dtpInvoiceToDate.Value;
            }

            this.DialogResult = DialogResult.OK;
        }

 
        private bool ValidateData()
        {
            DateTime dtpServer = Library.AppCore.Globals.GetServerDateTime();

            if (chkIsSearchByInputDate.Checked)
            {
                if (Convert.ToDateTime(dtpInputFromDate.Value).Date > Convert.ToDateTime(dtpInputToDate.Value).Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày nhập bắt đầu không được lớn hơn đến ngày!");
                    dtpInputFromDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }
                if (Convert.ToDateTime(dtpInputToDate.Value).Date > dtpServer.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày nhập kết thúc không được lớn hơn ngày hiện tại!");
                    dtpInputFromDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }
            }

            if (chkIsSearchByInvoiceDate.Checked)
            {
                if (Convert.ToDateTime(dtpInvoiceFromDate.Value).Date > Convert.ToDateTime(dtpInvoiceToDate.Value).Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày hóa đơn bắt đầu không được lớn hơn đến ngày!");
                    dtpInvoiceFromDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }
                if (Convert.ToDateTime(dtpInvoiceToDate.Value).Date > dtpServer.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày hóa đơn kết thúc không được lớn hơn ngày hiện tại!");
                    dtpInvoiceToDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }

            }
            return true;
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
                return;

            objSelection.ClearSelection();

            object[] objKeywords = new object[]
            {
                "@CUSTOMERIDLIST", strCustomerIDList,
                "@PRODUCTIDLIST", strProductIDList,
                "@ISSERACHBYINPUTDATE", chkIsSearchByInputDate.Checked,
                "@INPUTFROMDATE",dtpInputFromDate.Value,
                "@INPUTTODATE",dtpInputToDate.Value,
                "@ISSERACHBYINVOICEDATE", chkIsSearchByInvoiceDate.Checked,
                "@INVOICEFROMDATE", dtpInvoiceFromDate.Value,
                "@INVOICETODATE", dtpInvoiceToDate.Value
            };
            DataSet dsResult = new ERP.Report.PLC.PLCReportDataSource().GetDataSet("GETINPUTVOUCHER_BY_CUSTOMER", new string[] { "v_Out1", "v_Out2" }, objKeywords);
            if (dsResult == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi lấy thông tin phiếu nhập theo nhà cung cấp");
                grdData.DataSource = null;
            }
            else
            {
                dtbInputVoucherDetail = dsResult.Tables[0];
                dtbProductImei = dsResult.Tables[1];
                grdData.DataSource = dtbInputVoucherDetail;
            }
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }

        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            mnuItemExportExcel.Enabled = grdViewData.DataRowCount > 0;
        }

        private void chkIsSearchByInvoiceDate_CheckedChanged(object sender, EventArgs e)
        {
            dtpInvoiceFromDate.Enabled = dtpInvoiceToDate.Enabled = chkIsSearchByInvoiceDate.Checked;
        }

        private void chkIsSearchByInputDate_CheckedChanged(object sender, EventArgs e)
        {
            dtpInputFromDate.Enabled = dtpInputToDate.Enabled = chkIsSearchByInputDate.Checked;
        }
    }
}

