﻿using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using ERP.Inventory.PLC.WSPriceProtect;
using ERP.Inventory.PLC.WSProductInStock;
using ERP.MasterData.DUI.Search;
using System.Reflection;
using ERP.Report.DUI.Reports.Inventory.Stock;
using Library.AppCore;
using Library.AppCore.LoadControls;
using Library.AppCore.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERP.Inventory.DUI.Inventory;
using System.IO;
using System.Text.RegularExpressions;

namespace ERP.Inventory.DUI.PM.PriceProtect
{
    public partial class frmPriceProtectStock : Form
    {
        public frmPriceProtectStock()
        {
            InitializeComponent();
        }
        private bool bolIsEdit = false;
        private bool bolIsLoadForm = false;

        #region Khai báo quyền
        private string strPermissionCancel = "PM_PRICEPROTECT_DELETE";
        private string strPermissionInvoiceAdd = "PM_PRICEPROTECT_INVOICE_ADD";
        private string strPermissionApprove = "PM_PRICEPROTECT_APPROVE ";
        private bool bolPermissionInvoiceAdd = false;
        #endregion

        #region Variable

        private string strFMSAplication = "FMSAplication_ProERP_InputVoucherAttachment";
        private List<PriceProtect_Attachment> lstPriceProtect_Attachment = new List<PriceProtect_Attachment>();
        private List<PriceProtect_Customer> lstPriceProtect_Customer = new List<PriceProtect_Customer>();
        private List<ERP.MasterData.PLC.MD.WSProduct.Product> lstProduct = new List<MasterData.PLC.MD.WSProduct.Product>();

        private List<PriceProtectDetail> lstPriceProtectDetail = new List<PriceProtectDetail>();
        private List<PriceProtectDetail> lstPriceProtectDetail2 = new List<PriceProtectDetail>();

        private List<PriceProtectDetail_IMEI> lstPriceProtectDetail_IMEI = new List<PriceProtectDetail_IMEI>();
        private List<PriceProtectDetail_Allocate> lstPriceProtectDetail_Allocate = new List<PriceProtectDetail_Allocate>();
        private PLC.WSPriceProtect.PriceProtect objPriceProtect = new PLC.WSPriceProtect.PriceProtect();
        private PLC.WSPriceProtect.PriceProtectDetail objPriceProtectDetail = new PLC.WSPriceProtect.PriceProtectDetail();
        private PLC.WSPriceProtect.PriceProtectDetail_IMEI objPriceProtectDetail_IMEI = new PLC.WSPriceProtect.PriceProtectDetail_IMEI();
        private PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();
        private PLC.PM.PLCPriceProtect objPLCPriceProtect = new PLC.PM.PLCPriceProtect();
        private DataTable dtbInStock = null;
        private bool bolIsCalInStock = false;
        private PLC.WSPriceProtect.ResultMessage objResultMessage = new PLC.WSPriceProtect.ResultMessage();
        private string strPriceProtectID = string.Empty;
        private bool bolIsChangeStockDate = false;
        private DateTime? dtpStockDateTemp = null;

        private Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        private bool bolIsChange = false;
        private bool bolIsReLoad = true;
        private int intBeforeSelect = -1;
        private List<PriceProtectDetail> lstPriceProtectDetailCount = new List<PriceProtectDetail>();
        private DataTable dtbInStockByVoucher = null;
        private Report.PLC.WSReportDataSource.ResultMessage objResutlMessage = new Report.PLC.WSReportDataSource.ResultMessage();
        DataTable dtbInvoiceTransactionType = null;
        DataTable dtbVatType = null;
        private DateTime dteVoucherDate_Old;
        DataTable dtbImport = null;
        public string ProductId { get; set; }

        public bool IsChange
        {
            get { return bolIsChange; }
            set { bolIsChange = value; }
        }
        public bool IsEdit
        {
            get { return bolIsEdit; }
            set { bolIsEdit = value; }
        }
        public string PriceProtectID
        {
            get { return strPriceProtectID; }
            set { strPriceProtectID = value; }
        }

        #endregion

        private void frmPriceProtectStock_Load(object sender, EventArgs e)
        {
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
            dtpPriceProtectDate.MaxDate = Library.AppCore.Globals.GetServerDateTime();
            bolIsLoadForm = false;
            InitControl();
            LoadCombobox();
            mnuItemImportValues.Visible = IsEdit;
            mnuItemExportTemplate.Visible = IsEdit;
            mnuItemInputIMEI.Visible = !IsEdit;
            mnuItemImportExcel.Visible = !IsEdit;

            btnReviewOrder.Enabled = false;
            if (IsEdit && !string.IsNullOrEmpty(PriceProtectID))
            {
                objPriceProtect = new PLC.WSPriceProtect.PriceProtect();
                objPLCPriceProtect.LoadInfo(ref objPriceProtect, PriceProtectID);
                if (objPriceProtect != null)
                {
                    txtPriceProtectID.Text = objPriceProtect.PriceProtectID;
                    lstPriceProtectDetail = objPriceProtect.LstPriceProtectDetail.ToList();
                    lstPriceProtectDetail_IMEI = objPriceProtect.LstPriceProtectDetail_IMEI.ToList();
                    lstPriceProtect_Customer = objPriceProtect.ListPriceProtect_Customer.ToList();
                    lstPriceProtect_Attachment = objPriceProtect.ListPriceProtect_Attachment.ToList();
                    cboStoreID.SetValue(objPriceProtect.StoreID);

                    grdData.DataSource = lstPriceProtectDetail;
                    grdDataPriceProtect1.DataSource = lstPriceProtectDetail;
                    grdDifference.DataSource = lstPriceProtectDetail;
                    grdCustomer.DataSource = lstPriceProtect_Customer;
                    grdAttachment.DataSource = lstPriceProtect_Attachment;

                    txtNote.Text = objPriceProtect.Note;
                    cboStatus.SelectedIndex = objPriceProtect.Status;
                    txtPriceProtectNO.Text = objPriceProtect.PriceProtectNO;
                    chkIsSupplierCall.Checked = objPriceProtect.IsSupplierCal;
                    if (objPriceProtect.StockDate != null)
                        dtpCallStock.Value = Convert.ToDateTime(objPriceProtect.StockDate);

                    if (objPriceProtect.VoucherDate != null)
                        dtpVoucher.Value = Convert.ToDateTime(objPriceProtect.VoucherDate);
                    else
                        dtpVoucher.Value = Globals.GetServerDateTime();

                    if (objPriceProtect.PostDate != null)
                    {
                        dtpPostdate.Value = Convert.ToDateTime(objPriceProtect.PostDate);
                        if (objPriceProtect.PostDate.Value.Date < Globals.GetServerDateTime().Date)
                            dtpPostdate.Enabled = false;
                    }
                    else
                        dtpPostdate.Value = Globals.GetServerDateTime();

                    dteVoucherDate_Old = dtpVoucher.Value;
                    cboPriceProtectType.SetValue(objPriceProtect.PriceProtectTypeID);
                    cboInvoiceTransactionType.SetValue(objPriceProtect.InVoiceTransTypeID);

                    if (objPriceProtect.InputFromDate != null)
                        dtpInputFromDate.Value = Convert.ToDateTime(objPriceProtect.InputFromDate);
                    if (objPriceProtect.InputToDate != null)
                        dtpInputToDate.Value = Convert.ToDateTime(objPriceProtect.InputToDate);

                    EnableControl(IsEdit);

                    cboStatus.Enabled = objPriceProtect.Status == 0;
                    //cboStoreID.Enabled = objPriceProtect.Status == 0;
                    cboStoreID.Enabled = false;
                    mnuItemExportTemplate.Enabled = mnuItemImportValues.Enabled = objPriceProtect.Status == 1;
                    FormatColumnGrid();
                    cboInvoiceTransactionType.Enabled = false;
                    if (!objPriceProtect.IsApproved)
                    {
                        CreateReviewStatus();
                        dtpVoucher.Enabled = false;
                        cboStatus.Enabled = false;
                        btnReviewOrder.Visible = SystemConfig.objSessionUser.IsPermission(strPermissionInvoiceAdd);
                        btnReviewOrder.Enabled = SystemConfig.objSessionUser.IsPermission(strPermissionInvoiceAdd);
                        tabControl2.TabPages.Remove(tabPriceProtect2);
                        tabControl2.TabPages.Remove(tabDifferent);
                    }
                    else
                    {
                        tabControl2.TabPages.Remove(tabPriceProtect2);
                        tabControl2.TabPages.Remove(tabDifferent);
                        mnuItemExportTemplate.Enabled = mnuItemImportExcel.Enabled = mnuItemImportValues.Enabled = mnuItemInputIMEI.Enabled = false;
                        grdViewData.OptionsBehavior.Editable = false;
                        cboStoreID.Enabled = false;
                        dtpPostdate.Enabled = false;
                        grvCustomer.OptionsBehavior.Editable = false;
                        grvAttachment.OptionsBehavior.Editable = false;
                        mnuAttachment.Enabled = false;
                        tabControl2.TabPages.Remove(tabDifferent);

                        cboInvoiceTransactionType.Enabled = false;
                        if (objPriceProtect.IsHasAllocateByPostdate)
                        {
                            cboInvoiceTransactionType.Enabled = true;
                            tabControl2.TabPages.Add(tabPriceProtect2);
                        }

                        if (objPriceProtect.IsHasInternalInvoice || objPriceProtect.IsHasAdjustVoucher)
                        {
                            btnUpdate.Enabled = false;
                            btnDelete.Enabled = false;
                            txtPriceProtectNO.ReadOnly = true;
                            dtpPostdate.Enabled = false;
                            cboStatus.Enabled = false;
                            cboStoreID.Enabled = false;
                            grvDataPriceProtect1.OptionsBehavior.Editable = false;
                            grvCustomer.OptionsBehavior.Editable = false;
                            grvAttachment.OptionsBehavior.Editable = false;
                            cboInvoiceTransactionType.Enabled = false;
                            txtNote.ReadOnly = true;
                            mnuAttachment.Enabled = false;
                            mnuItemExportTemplateProtect1.Enabled = mnuItemImportValues1.Enabled = false;
                            tabControl2.TabPages.Add(tabDifferent);
                        }
                    }



                    if (objPriceProtect.IsDeleted) // Đã hủy
                    {
                        grdViewData.Columns["Quantity"].OptionsColumn.AllowEdit = false;
                        btnDelete.Enabled = false;
                        btnUpdate.Enabled = false;
                        cboStatus.Enabled = false;
                        cboStoreID.Enabled = false;
                        txtNote.Enabled = false;
                        grdViewData.OptionsBehavior.Editable = false;
                        grdViewData.OptionsBehavior.ReadOnly = false;
                        grvAttachment.OptionsBehavior.Editable = false;
                        txtPriceProtectNO.ReadOnly = true;
                        dtpVoucher.Enabled = false;
                        dtpInputFromDate.Enabled = false;
                        dtpInputToDate.Enabled = false;
                        txtNote.ReadOnly = true;
                        mnuAddAttachment.Enabled = false;
                        mnuItemExportTemplate.Enabled = mnuItemImportExcel.Enabled = mnuItemImportValues.Enabled = false;
                    }
                }
            }
            else
            {
                FormatGrid();
                grdData.DataSource = lstProduct;
                dtpCallStock.Value = Globals.GetServerDateTime().AddDays(-1);
                dtpVoucher.Value = dtpCallStock.Value.AddDays(1);
                dtpPostdate.MinDate = Globals.GetServerDateTime();
                dtpVoucher.MinDate = Globals.GetServerDateTime();
                tabControl2.TabPages.Remove(tabPriceProtect2);
                tabControl2.TabPages.Remove(tabDifferent);
            }
            grdData.RefreshDataSource();
            bolIsLoadForm = true;


            if (txtNote.ReadOnly)
                txtNote.BackColor = SystemColors.Info;
            if (txtPriceProtectNO.ReadOnly)
                txtPriceProtectNO.BackColor = SystemColors.Info;
        }

        #region Form Status
        /// <summary>
        /// Các trạng thái của Form
        /// </summary>
        enum FormStateType
        {
            LIST,
            ADD,
            EDIT
        }

        FormStateType intFormState = FormStateType.LIST;
        FormStateType FormState
        {
            set
            {
                intFormState = value;
                if (intFormState == FormStateType.LIST)
                {
                    btnUndo.Enabled = false;
                    btnUndo.Enabled = false;
                }
                else if (intFormState == FormStateType.ADD || intFormState == FormStateType.EDIT)
                {
                    if (intFormState == FormStateType.ADD)
                    {
                        FormatGrid();
                    }
                }
            }
            get
            {
                return intFormState;
            }
        }
        #endregion

        #region UPDATE DATA
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
                return;

            if (MessageBox.Show("Bạn muốn lưu phiếu hỗ trợ giá?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            UpdateData();
        }

        private void UpdateData()
        {
            try
            {
                objPriceProtect.PriceProtectDate = dtpPriceProtectDate.Value;
                var objCustomer = lstPriceProtect_Customer.FirstOrDefault(r => r.IsDefault);
                if (objCustomer != null)
                {
                    objPriceProtect.CustomerID = objCustomer.CustomerID;
                    objPriceProtect.CustomerName = objCustomer.CustomerName;
                    objPriceProtect.CustomerAddress = objCustomer.CustomerAddress;
                }

                objPriceProtect.Note = txtNote.Text.Trim();
                objPriceProtect.Status = cboStatus.SelectedIndex;
                objPriceProtect.StockDate = dtpCallStock.Value.Date; // Ngày chốt tồn
                objPriceProtect.StoreID = cboStoreID.StoreID;
                objPriceProtect.PriceProtectTypeID = cboPriceProtectType.ColumnID;
                objPriceProtect.InVoiceTransTypeID = cboInvoiceTransactionType.ColumnID;
                objPriceProtect.ListPriceProtect_Attachment = lstPriceProtect_Attachment.ToArray();
                objPriceProtect.ListPriceProtect_Customer = lstPriceProtect_Customer.ToArray();
                objPriceProtect.PriceProtectNO = txtPriceProtectNO.Text.Trim();
                objPriceProtect.IsSupplierCal = chkIsSupplierCall.Checked;
                if (!string.IsNullOrEmpty(objPriceProtect.PriceProtectNO))
                    objPriceProtect.VoucherDate = dtpVoucher.Value.Date;
                else
                    objPriceProtect.VoucherDate = null;

                if (objPriceProtect.IsApproved)
                    objPriceProtect.VoucherDate = dtpVoucher.Value.Date;
                else
                    objPriceProtect.VoucherDate = null;
                objPriceProtect.PostDate = dtpPostdate.Value.Date;
                if (objPriceProtect.PriceProtectTypeID == 1)
                {
                    objPriceProtect.InputFromDate = objPriceProtect.StockDate;
                    objPriceProtect.InputToDate = objPriceProtect.StockDate;
                }
                else
                {
                    objPriceProtect.InputFromDate = dtpInputFromDate.Value;
                    objPriceProtect.InputToDate = dtpInputToDate.Value;
                }

                objPriceProtect.IsRoadTrip = 1;
                objPriceProtect.LstPriceProtectDetail = lstPriceProtectDetail.Where(x => (x.Quantity >= 0 || x.InStockQuantity >= 0)).ToArray();

                if (!IsEdit) // thêm
                {
                    if (objPriceProtect.LstPriceProtectDetail != null && objPriceProtect.LstPriceProtectDetail.Count() > 0)
                    {
                        foreach (var obj in objPriceProtect.LstPriceProtectDetail)
                        {
                            if (obj.IsRequestIMEI)
                                obj.PriceProtectDetail_ImeiList = FilterImei(obj);
                        }
                    }
                    objPriceProtect.CreatedStoreID = SystemConfig.intDefaultStoreID;
                    objPriceProtect.CompanyID = SystemConfig.DefaultCompany.CompanyID;
                    bool bolIsResult = objPLCPriceProtect.Insert(ref objPriceProtect);
                    if (bolIsResult)
                    {
                        MessageBoxObject.ShowInfoMessage(this, "Thêm phiếu hỗ trợ hàng tồn thành công!");
                        ClearData();
                    }
                    else
                    {
                        MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                    }
                }
                else
                {
                    if (cboStatus.SelectedIndex == 1) // da co bien ban
                    {
                        decimal decTotal = 0;
                        foreach (PriceProtectDetail objUpdate in lstPriceProtectDetail)
                        {
                            decTotal += Convert.ToDecimal(objUpdate.TotalAmountVoucher);
                        }

                        frmConfirm frmCf = new frmConfirm();
                        frmCf.Total = Math.Round(decTotal, 0, MidpointRounding.AwayFromZero);

                        frmCf.ShowDialog();
                        if (frmCf.IsConfirm)
                        {
                            bool bolIsResult = objPLCPriceProtect.Update(objPriceProtect);
                            if (!bolIsResult)
                            {
                                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                            }
                            else
                            {
                                MessageBoxObject.ShowInfoMessage(this, "Cập nhật thông tin hỗ trợ hàng tồn thành công!");
                                if (bolPermissionInvoiceAdd)
                                    CreateInvoice(objPriceProtect);
                                IsChange = true;
                                this.Close();
                            }
                        }
                    }
                    else
                    {
                        bool bolIsResult = objPLCPriceProtect.Update(objPriceProtect);
                        if (!bolIsResult)
                        {
                            MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                        }
                        else
                        {
                            MessageBoxObject.ShowInfoMessage(this, "Cập nhật thông hỗ trợ hàng tồn thành công!");
                            IsChange = true;
                            this.Close();
                        }
                    }

                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi cập nhật thông tin hỗ trợ hàng tồn", objExce.ToString());
                SystemErrorWS.Insert("Lỗi cập nhật thông tin hỗ trợ hàng tồn", objExce.ToString(), this.Name + " -> btnUpdate_Click", DUIInventory_Globals.ModuleName);
            }
            grdData.RefreshDataSource();
        }

        private bool ValidateData()
        {
            DateTime dtpSever = Library.AppCore.Globals.GetServerDateTime();
            if (cboPriceProtectType.ColumnID < 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn loại hỗ trợ hàng tồn!");
                cboPriceProtectType.Focus();
                SendKeys.Send("{F4}");
                return false;
            }
            if (dtpPriceProtectDate.Value.Date > dtpSever.Date)
            {
                MessageBoxObject.ShowWarningMessage(this, "Ngày lập không được lớn hơn ngày hiện tại!");
                dtpPriceProtectDate.Focus();
                SendKeys.Send("{F4}");
                return false;
            }
            if (dtpCallStock.Value.Date >= dtpSever.Date)
            {
                MessageBoxObject.ShowWarningMessage(this, "Ngày chốt tồn phải nhỏ hơn ngày hiện tại!");
                dtpPriceProtectDate.Focus();
                SendKeys.Send("{F4}");
                return false;
            }


            if (dtpInputFromDate.Value != null && dtpInputToDate.Value != null)
            {
                if (Convert.ToDateTime(dtpInputFromDate.Value).Date > Convert.ToDateTime(dtpInputToDate.Value).Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày nhập\nNgày bắt đầu không được lớn hơn ngày kết thúc!");
                    dtpInputFromDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }
            }

            if (cboStoreID.StoreID <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho chứng từ!");
                cboStoreID.Focus();
                SendKeys.Send("{F4}");
                return false;
            }

            if (lstPriceProtect_Customer == null || lstPriceProtect_Customer.Count() == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn nhà cung cấp!");
                tabControl1.SelectedTab = tabCustomer;
                return false;
            }

            if (lstPriceProtect_Customer != null && lstPriceProtect_Customer.Count() > 0 && lstPriceProtect_Customer.Where(r => r.IsDefault).Count() == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn nhà cung cấp mặc định!");
                tabControl1.SelectedTab = tabCustomer;
                return false;
            }


            if (dtpPostdate.Enabled && dtpPostdate.Value.Date < dtpSever.Date)
            {
                MessageBoxObject.ShowWarningMessage(this, "Ngày hạch toán 1 phải lớn hơn hoặc bằng ngày hiện tại!");
                dtpVoucher.Focus();
                SendKeys.Send("{F4}");
                return false;
            }

            if (dtpVoucher.Value.Date < dtpSever.Date)
            {
                MessageBoxObject.ShowWarningMessage(this, "Ngày hạch toán 2 phải lớn hơn hoặc bằng ngày hiện tại!");
                dtpVoucher.Focus();
                SendKeys.Send("{F4}");
                return false;
            }


            if (IsEdit)
            {
                if (cboStatus.SelectedIndex == 1 && string.IsNullOrEmpty(txtPriceProtectNO.Text.Trim()))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập số biên bản!");
                    txtPriceProtectNO.Focus();
                    return false;
                }

                if (cboStatus.SelectedIndex == 1 && dtpVoucher.Value.Date < dtpSever.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày biên bản phải lớn hơn hoặc bằng ngày hiện tại!");
                    dtpVoucher.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }

                if (cboStatus.SelectedIndex == 1 && cboInvoiceTransactionType.ColumnID < 0)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn loại nghiệp vụ!");
                    cboInvoiceTransactionType.Focus();
                    return false;
                }

                if (cboStatus.SelectedIndex == 1 && dtpVoucher.Value.Date < dtpPostdate.Value.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày hạch toán 2 phải lớn hơn hoặc bằng ngày hạch toán 1!");
                    dtpVoucher.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }
            }
            else
            {
                if (grdViewData.RowCount == 0)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn sản phẩm hoặc bắn Barcode sản phẩm cần chốt tồn!");
                    txtBarcode.Focus();
                    return false;
                }

                if (cboPriceProtectType.ColumnID < 3 && !bolIsCalInStock)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng tính số lượng tồn sản phẩm trước khi cập nhật!");
                    txtBarcode.Focus();
                    return false;
                }

                if (cboPriceProtectType.ColumnID == 3)
                {
                    if (lstPriceProtectDetail.Where(r => string.IsNullOrEmpty(r.InputVoucherID)).Count() > 0)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn hóa đơn cần hỗ trợ giá!");
                        tabControl1.SelectedTab = tabGeneral;
                        btnSearchBill.Focus();
                        return false;
                    }
                }


                if (cboPriceProtectType.ColumnID == 1)
                {
                    foreach (var item in lstPriceProtectDetail)
                    {
                        if (item.Quantity < 0)
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Mã sản phẩm " + item.ProductID + " có số lượng hỗ trợ nhỏ hơn 0!");
                            return false;
                        }
                    }
                }

                if (cboPriceProtectType.ColumnID == 4)
                {
                    foreach (var item in lstPriceProtectDetail)
                    {
                        if (item.InStockQuantity < 0)
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Mã sản phẩm " + item.ProductID + " có số lượng hỗ trợ nhỏ hơn 0!");
                            return false;
                        }
                    }
                }

                //List<PriceProtectDetail> rlsDetail = (List<PriceProtectDetail>)grdData.DataSource;
                //foreach (var item in rlsDetail)
                //{
                //    if (item.Quantity <= 0)
                //    {
                //        MessageBoxObject.ShowWarningMessage(this, "Sản phẩm " + item.ProductName + " có số lượng hỗ trợ bằng 0!");
                //        btnCalStock.Focus();
                //        return false;
                //    }
                //}
            }

            grdData.RefreshDataSource();
            if (lstPriceProtectDetail == null || lstPriceProtectDetail.Count == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Bạn chưa chọn sản phẩm để tạo hỗ trợ giá!");
                txtBarcode.Focus();
                return false;
            }


            if (cboPriceProtectType.ColumnID < 3)
            {
                var objQuery = lstPriceProtectDetail.Exists(x => x.Quantity == null);
                if (objQuery)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Có sản phẩm chưa được chốt tồn. Vui lòng kiểm tra lại trước khi cập nhật!");
                    grdViewData.Focus();
                    return false;
                }
            }
            else
            {
                var objQuery = lstPriceProtectDetailCount.Exists(x => x.Quantity == null);
                if (objQuery)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Có sản phẩm chưa được chốt tồn. Vui lòng kiểm tra lại trước khi cập nhật!");
                    grdViewData.Focus();
                    return false;
                }
            }

            if (IsEdit && cboStatus.SelectedIndex == 1)
            {
                var objQueryConfirm = lstPriceProtectDetail.Exists(x => x.Price == null);
                if (objQueryConfirm)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập đơn giá bảo trợ(đ/bộ) cho tất cả sản phẩm!");
                    grdViewData.Focus();
                    return false;
                }
            }
            return true;
        }


        private PriceProtectDetail_IMEI[] FilterImei(PriceProtectDetail objDetail)
        {
            var rls = from r in lstPriceProtectDetail_IMEI
                      where r.IsValid == true & lstPriceProtectDetail.Exists(x => x.ProductID.Trim() == r.ProductID.Trim())
                      where r.ProductID.Trim() == objDetail.ProductID.Trim()
                      orderby r.IsCenterBranch descending
                      select r
                          ;
            return rls.ToArray();

            //if (cboTypeProtect.SelectedIndex == 0 || cboTypeProtect.SelectedIndex == 1)
            //{
            //    var rls = from r in lstPriceProtectDetail_IMEI
            //              where r.IsValid == true & lstPriceProtectDetail.Exists(x => x.ProductID.Trim() == r.ProductID.Trim())
            //              where r.ProductID.Trim() == objDetail.ProductID.Trim()
            //              orderby r.IsCenterBranch descending
            //              select r 
            //              ;
            //    return rls.ToArray();
            //}
            //else
            //{
            //    var rls = from r in lstPriceProtectDetail_IMEI
            //              where r.IsValid == true & lstPriceProtectDetail.Exists(x => x.ProductID.Trim() == r.ProductID.Trim() & x.InputVoucherID.Trim() == r.InputVoucherID.Trim() )//& x.InVoiceID.Trim() == r.invoice.Trim())
            //              where r.ProductID.Trim() == objDetail.ProductID.Trim() & r.InputVoucherID.Trim() == objDetail.InputVoucherID.Trim()
            //              orderby r.IsCenterBranch descending
            //              select r;
            //    return rls.ToArray();
            //}
        }
        #endregion

        #region TÌM KIẾM SẢN PHẨM
        private void btnSearchProduct_Click(object sender, EventArgs e)
        {
            if (!btnSearchProduct.Enabled)
                return;
            frmProductSearch frmProductSearch = ERP.MasterData.DUI.MasterData_Globals.frmProductSearch;
            frmProductSearch.MainGroupPermissionType = Library.AppCore.Constant.EnumType.MainGroupPermissionType.OUTORDER;
            frmProductSearch.IsRequestIMEIType = Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL;
            frmProductSearch.IsServiceType = Library.AppCore.Constant.EnumType.IsServiceType.NOT_ISSERVICE;
            frmProductSearch.ShowDialog(this);
            if (frmProductSearch.Product != null)
            {
                txtBarcode.Text = frmProductSearch.Product.ProductID;
                txtBarcode.Focus();
            }
        }

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!txtBarcode.Enabled)
                return;
            if (e.KeyChar == 13)
            {
                string strBarcode = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(txtBarcode.Text.Trim());
                if (strBarcode.Length < 1)
                    return;
                if (!CheckObject.CheckIMEI(strBarcode))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Mã sản phẩm hoặc IMEI không đúng định dạng");
                    return;
                }
                ERP.MasterData.PLC.MD.WSProduct.Product objProduct = new MasterData.PLC.MD.WSProduct.Product();
                objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(txtBarcode.Text.Trim());

                ProductInStock objProductInStock = objPLCProductInStock.LoadInfo(strBarcode, -1);
                PriceProtectDetail objPriceProtectDetail = new PriceProtectDetail();
                if (objProduct == null)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Mã sản phẩm không tồn tại!");
                    txtBarcode.Text = string.Empty;
                    txtBarcode.Focus();
                    return;
                }

                if (cboPriceProtectType.ColumnID == 2 && !objProduct.IsRequestIMEI)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Loại hỗ trợ giá theo ngày nhập chỉ áp dụng cho sản phẩm có IMEI!");
                    txtBarcode.Text = string.Empty;
                    txtBarcode.Focus();
                    return;
                }

                if (!lstPriceProtectDetail.Exists(x => x.ProductID == objProduct.ProductID))
                {
                    objPriceProtectDetail.VatTypeID = objProduct.VAT;
                    objPriceProtectDetail.VatPercent = objProduct.VATPercent;
                    objPriceProtectDetail.Vat = objProduct.VAT;
                    objPriceProtectDetail.ProductID = objProduct.ProductID;
                    objPriceProtectDetail.ProductName = objProduct.ProductName;
                    objPriceProtectDetail.IsRequestIMEI = objProduct.IsRequestIMEI;
                    lstPriceProtectDetail.Add(objPriceProtectDetail);
                }

                txtBarcode.Text = string.Empty;
                txtBarcode.Focus();
            }
            grdData.DataSource = lstPriceProtectDetail;
            grdData.RefreshDataSource();
            if (grdViewData.RowCount > 0)//GOGO
                btnCalStock.Enabled = true;
            else
                btnCalStock.Enabled = false;
        }

        #endregion

        #region Tập tin đính kèm
        private void mnuAttachment_Opening(object sender, CancelEventArgs e)
        {
            mnuDelAttachment.Enabled = false;// (!chkIsReviewed.Checked && !chkIsDeleted.Checked && grvAttachment.DataRowCount > 0);
        }

        private void mnuAddAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog objOpenFileDialog = new OpenFileDialog();
                objOpenFileDialog.Multiselect = true;
                objOpenFileDialog.Filter = "Doc Files (*.doc, *.docx)|*.doc; *docx|Excel Files (*.xls, *.xlsx)|*.xls; *.xlsx|PDF Files(*.pdf)|*.pdf|Compressed Files(*.zip, *.rar)|*.zip;*.rar|Image Files(*.jpg, *.jpe, *.jpeg, *.gif, *.png, *.bmp, *.tif, *.tiff)|*.jpg; *.jpe; *.jpeg; *.gif; *.png; *.bmp; *.tif; *.tiff";
                if (objOpenFileDialog.ShowDialog() == DialogResult.OK)
                {
                    String[] strLocalFilePaths = objOpenFileDialog.FileNames;
                    foreach (string strLocalFilePath in strLocalFilePaths)
                    {
                        PriceProtect_Attachment objAttachment = new PriceProtect_Attachment();
                        FileInfo objFileInfo = new FileInfo(strLocalFilePath);
                        var ListCheck = from o in lstPriceProtect_Attachment
                                        where o.FileName == objFileInfo.Name
                                        select o;
                        if (ListCheck.Count() > 0)
                            continue;
                        decimal FileSize = Math.Round(objFileInfo.Length / Convert.ToDecimal((1024 * 1024)), 2); //dung lượng tính theo MB
                        decimal FileSizeLimit = 2;//MB
                        if (FileSize > FileSizeLimit)
                        {
                            MessageBox.Show(this, "Vui lòng chọn tập tin đính kèm dung lượng không vượt quá 2 MB", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        objAttachment.FileName = objFileInfo.Name;
                        objAttachment.PriceProtectID = string.Empty;
                        objAttachment.FilePath = strLocalFilePath;
                        objAttachment.CreatedDate = Globals.GetServerDateTime();
                        objAttachment.CreatedUser = SystemConfig.objSessionUser.UserName;
                        objAttachment.CreatedUserName = SystemConfig.objSessionUser.UserName + " - " + SystemConfig.objSessionUser.FullName;
                        objAttachment.IsNew = true;
                        lstPriceProtect_Attachment.Add(objAttachment);
                    }
                    grdAttachment.RefreshDataSource();
                }
            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, "Lỗi thêm tập tin!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi thêm tập tin", objExce.ToString(), this.Name + " -> mnuDelAttachment_Click", DUIInventory_Globals.ModuleName);
            }
        }

        private void mnuDelAttachment_Click(object sender, EventArgs e)
        {
            if (!mnuDelAttachment.Enabled)
                return;

            try
            {
                if (grvAttachment.FocusedRowHandle < 0)
                    return;
                PriceProtect_Attachment objAttachment = (PriceProtect_Attachment)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
                if (objAttachment != null && lstPriceProtect_Attachment != null)
                {
                    if (MessageBox.Show(this, "Bạn muốn xóa file đính kèm này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        lstPriceProtect_Attachment.Remove(objAttachment);
                        //if (objAttachment.AttachmentID != null && objAttachment.AttachmentID.Trim() != string.Empty)
                        //{
                        //    if (lstPriceProtect_Attachment_Del == null)
                        //        lstPriceProtect_Attachment_Del = new List<PriceProtect_Attachment>();
                        //    lstPriceProtect_Attachment_Del.Add(objAttachment);
                        //}
                        grdAttachment.RefreshDataSource();
                    }
                }
            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, "Lỗi xóa tập tin!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi xóa tập tin", objExce.ToString(), this.Name + " -> mnuDelAttachment_Click", DUIInventory_Globals.ModuleName);
            }
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            if (grvAttachment.FocusedRowHandle < 0)
                return;
            PriceProtect_Attachment obj = (PriceProtect_Attachment)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
            if (obj.IsNew)
            {
                MessageBox.Show(this, "File chưa được cập nhật trên server!");
                return;
            }
            DownloadFile(obj.FileID, obj.FileName);
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            if (grvAttachment.FocusedRowHandle < 0)
                return;
            PriceProtect_Attachment objSaleOrder_Attachment = (PriceProtect_Attachment)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
            if (objSaleOrder_Attachment.IsNew == true)
            {
                return;
            }
            DownloadFile(objSaleOrder_Attachment, true);
        }

        /// <summary>
        /// Tải file đính kèm
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="strLocation"></param>
        /// <returns></returns>
        public bool DownloadFile(PriceProtect_Attachment objAttachment, bool bolIsView)
        {
            try
            {
                if (!bolIsView)
                {
                    SaveFileDialog dlgSaveFile = new SaveFileDialog();
                    dlgSaveFile.Title = "Lưu file tải về";
                    dlgSaveFile.FileName = objAttachment.FileName;
                    dlgSaveFile.Filter = "Office Files|*.doc;*.docx;*.xls;*.xlsx;*.pdf;*.ppt;*.zip;*.rar;*.jpg;*.jpe;*.jpeg;*.gif;*.png;*.bmp;*.tif;*.tiff;*.txt";
                    if (dlgSaveFile.ShowDialog() == DialogResult.OK)
                    {
                        if (dlgSaveFile.FileName != string.Empty)
                        {
                            ERP.FMS.DUI.DUIFileManager.DownloadFile(this, strFMSAplication, objAttachment.FileID, objAttachment.FilePath, dlgSaveFile.FileName);
                        }
                    }
                }
                else
                {
                    string strPathFile = System.IO.Path.GetTempPath() + "\\" + Guid.NewGuid().ToString();
                    DirectoryInfo di = Directory.CreateDirectory(strPathFile);
                    strPathFile = strPathFile + "\\" + objAttachment.FileName;
                    ERP.FMS.DUI.DUIFileManager.DownloadFile(this, strFMSAplication, objAttachment.FileID, objAttachment.FilePath, strPathFile);
                    System.Diagnostics.Process.Start(strPathFile);
                }
                return true;
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi tải file đính kèm", objExce.ToString(), this.Name + "-> DownloadFile", DUIInventory_Globals.ModuleName);
                MessageBox.Show("Lỗi tải file đính kèm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }
        /// <summary>
        /// Tải file đính kèm
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="strLocation"></param>
        /// <returns></returns>
        public bool DownloadFile(string strFileID, string strFileName)
        {
            try
            {
                SaveFileDialog dlgSaveFile = new SaveFileDialog();
                dlgSaveFile.Title = "Lưu file tải về";
                dlgSaveFile.FileName = strFileName;
                string strExtension = Path.GetExtension(strFileName);
                switch (strExtension)
                {
                    case ".xls":
                    case ".xlsx":
                        dlgSaveFile.Filter = "Excel Worksheets(*.xls, *.xlsx)|*.xls; *.xlsx";
                        break;
                    case ".doc":
                    case ".docx":
                        dlgSaveFile.Filter = "Word Documents(*.doc, *.docx)|*.doc; *.docx";
                        break;
                    case ".pdf":
                        dlgSaveFile.Filter = "PDF Files(*.pdf)|*.pdf";
                        break;
                    case ".zip":
                    case ".rar":
                        dlgSaveFile.Filter = "Compressed Files(*.zip, *.rar)|*.zip;*.rar";
                        break;
                    case ".jpg":
                    case ".jpe":
                    case ".jpeg":
                    case ".gif":
                    case ".png":
                    case ".bmp":
                    case ".tif":
                    case ".tiff":
                        dlgSaveFile.Filter = "Image Files(*.jpg, *.jpe, *.jpeg, *.gif, *.png, *.bmp, *.tif, *.tiff)|*.jpg; *.jpe; *.jpeg; *.gif; *.png; *.bmp; *.tif; *.tiff";
                        break;
                    default:
                        dlgSaveFile.Filter = "All Files (*)|*";
                        break;
                }
                if (dlgSaveFile.ShowDialog() == DialogResult.OK)
                {
                    if (dlgSaveFile.FileName != string.Empty)
                    {
                        ERP.FMS.DUI.DUIFileManager.DownloadFile(this, strFMSAplication, strFileID, string.Empty, dlgSaveFile.FileName);
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception objExce)
            {
                MessageBox.Show("Lỗi tải file đính kèm!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi tải file đính kèm", objExce.ToString(), this.Name + "-> DownloadFile", DUIInventory_Globals.ModuleName);
                return false;
            }
        }

        /// <summary>
        /// Đính kèm tập tin
        /// </summary>
        private bool UploadAttachment()
        {
            if (lstPriceProtect_Attachment == null || lstPriceProtect_Attachment.Count < 1)
                return true;
            //Tạo trạng thái chờ trong khi đính kèm tập tin
            try
            {
                var CheckIsNew = from o in lstPriceProtect_Attachment
                                 where o.IsNew == true
                                 select o;
                if (CheckIsNew.Count() <= 0)
                {
                    return true;
                }
                string strFilePath = string.Empty;
                string strFileID = string.Empty;
                foreach (PriceProtect_Attachment objPOreturn_Attachment in CheckIsNew)
                {
                    string strResult = ERP.FMS.DUI.DUIFileManager.UploadFile(this, strFMSAplication, objPOreturn_Attachment.LocalFilePath, ref strFileID, ref strFilePath);
                    objPOreturn_Attachment.FilePath = strFilePath;
                    objPOreturn_Attachment.FileID = strFileID;
                    if (!string.IsNullOrEmpty(strResult))
                    {
                        MessageBoxObject.ShowWarningMessage(this, strResult);
                        return false;
                    }
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi đính kèm tập tin ", objEx, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        #endregion

        #region Nhà cung cấp
        private void mnuItemAddCustomer_Click(object sender, EventArgs e)
        {
            if (!mnuItemAddCustomer.Enabled)
                return;

            ERP.MasterData.DUI.Search.frmCustomerSearch frm = new ERP.MasterData.DUI.Search.frmCustomerSearch();
            frm.CustomerType = Library.AppCore.Constant.EnumType.CustomerType.ISPROVIDER;
            frm.ShowDialog(this);
            if (frm.Customer != null)
            {
                if (lstPriceProtect_Customer != null && lstPriceProtect_Customer.Where(r => r.CustomerID == frm.Customer.CustomerID).Count() == 0)
                {
                    PriceProtect_Customer objCustomer = new PriceProtect_Customer();
                    objCustomer.CustomerID = frm.Customer.CustomerID;
                    objCustomer.CustomerName = frm.Customer.CustomerName;
                    objCustomer.CustomerAddress = frm.Customer.CustomerAddress;
                    objCustomer.CustomerTaxID = frm.Customer.CustomerTaxID;
                    objCustomer.CustomerPhone = frm.Customer.CustomerPhone;
                    objCustomer.CreatedUser = SystemConfig.objSessionUser.UserName;
                    objCustomer.IsDefault = (lstPriceProtect_Customer.Count() == 0);
                    lstPriceProtect_Customer.Add(objCustomer);
                    grdCustomer.RefreshDataSource();

                    if (grdViewData.RowCount > 0)
                        btnCalStock.Enabled = true;
                    if (!IsEdit && lstPriceProtectDetail != null && lstPriceProtectDetail.Count > 0 && chkIsSupplierCall.Checked && cboPriceProtectType.ColumnID != 4)
                    {
                        foreach (var item in lstPriceProtectDetail)
                        {
                            item.Quantity = null;
                            item.InStockQuantity = null;
                        }
                        bolIsCalInStock = false;
                    }
                    grdData.RefreshDataSource();
                }
            }
        }


        private void mnuItemDeleteCustomer_Click(object sender, EventArgs e)
        {
            if (!mnuItemDeleteCustomer.Enabled)
                return;

            if (grvCustomer.FocusedRowHandle < 0)
                return;
            PriceProtect_Customer objCustomer = (PriceProtect_Customer)grvCustomer.GetRow(grvCustomer.FocusedRowHandle);
            if (objCustomer != null && lstPriceProtect_Customer != null)
            {
                if (MessageBox.Show(this, "Bạn muốn xóa nhà cung cấp này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    lstPriceProtect_Customer.Remove(objCustomer);
                    if (lstPriceProtect_Customer.Count() > 0 && lstPriceProtect_Customer.Where(r => r.IsDefault).Count() == 0)
                        lstPriceProtect_Customer[0].IsDefault = true;
                    grdAttachment.RefreshDataSource();

                    if (grdViewData.RowCount > 0)
                        btnCalStock.Enabled = true;
                    if (!IsEdit && lstPriceProtectDetail != null && lstPriceProtectDetail.Count > 0 && chkIsSupplierCall.Checked)
                    {
                        foreach (var item in lstPriceProtectDetail)
                        {
                            item.Quantity = null;
                            item.InStockQuantity = null;
                        }
                        bolIsCalInStock = false;
                    }
                    grdData.RefreshDataSource();
                }
            }
        }

        private void grvCustomer_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
            if (view.FocusedRowHandle < 0) return;
            PriceProtect_Customer objReview = view.GetFocusedRow() as PriceProtect_Customer;
            if (objReview == null) return;
            if (string.IsNullOrEmpty(objReview.CustomerName))
            {
                e.Valid = false;
                view.SetColumnError(colCustomerName, "Bạn chưa nhập tên nhà cung cấp");
                e.ErrorText = "Bạn chưa nhập tên nhà cung cấp";
                view.FocusedColumn = colCustomerName;
            }
            if (e.Valid)
                view.ClearColumnErrors();
        }

        private void grvCustomer_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = sender as GridView;
            if (view.FocusedRowHandle < 0) return;
            int outResult = 0;
            if (view.FocusedColumn == colCustomerName)
            {
                if (e.Value != null && string.IsNullOrEmpty(e.Value.ToString().Trim()))
                {
                    e.Valid = false;
                    e.ErrorText = "Bạn chưa nhập tên nhà cung cấp";
                }
            }
        }

        private void grvCustomer_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        void grvCustomer_ShowingEditor(object sender, CancelEventArgs e)
        {
            //GridView view = sender as GridView;
            //if (view.FocusedColumn == colIsDefault)
            //    e.Cancel = true;
        }
        private void grvCustomer_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (grvCustomer.FocusedRowHandle < 0)
                return;
            var obj = (PriceProtect_Customer)grvCustomer.GetRow(grvCustomer.FocusedRowHandle);
            if (obj != null)
                obj.IsUpdate = true;
        }


        void grvCustomer_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Column.FieldName.ToUpper() == "ISDEFAULT")
            {
                bool bIsDefault = Convert.ToBoolean(e.Value);
                if (!bIsDefault)
                {
                    view.SetFocusedRowCellValue(colIsDefault, false);
                    view.ClearColumnErrors();
                }
                if (e.Column == colIsDefault)
                {
                    bool bIsDefault2 = Convert.ToBoolean(e.Value);
                    if (bIsDefault2)
                    {
                        for (int i = 0; i < grvCustomer.RowCount; i++)
                        {
                            DataRow dr = grvCustomer.GetDataRow(i);
                            grvCustomer.SetRowCellValue(i, colIsDefault, false);
                        }
                        view.SetFocusedRowCellValue(colIsDefault, true);
                    }
                }
            }
        }
        #endregion

        #region Method
        public void LoadComboStore()
        {
            DataTable dtbBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy();
            DataRow[] rowBranch = dtbBranch.Select("ISCENTERBRANCH = 1");

            Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            objStoreFilter.IsCheckPermission = true;
            if (rowBranch.Length > 0)
                objStoreFilter.OtherCondition = "BRANCHID = " + rowBranch[0]["BRANCHID"].ToString();
            cboStoreID.InitControl(false, objStoreFilter);
            cboStoreID.SetValue(SystemConfig.intDefaultStoreID);
        }

        private void LoadCombobox()
        {
            cboStatus.SelectedIndex = 0;
            dtbInvoiceTransactionType = Library.AppCore.DataSource.GetDataSource.GetInvoiceTransactionType().Copy();
            if (dtbInvoiceTransactionType != null)
            {
                DataRow[] row = dtbInvoiceTransactionType.Select("TRANSACTIONGROUPID = 4 OR TRANSACTIONGROUPID = 5");
                if (row.Count() > 0)
                    cboInvoiceTransactionType.InitControl(false, row.CopyToDataTable(), "INVOICETRANSTYPEID", "INVOICETRANSTYPENAME", "--Chọn loại nghiệp vụ--");
            }

            // Thue suất
            dtbVatType = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_INVOICEVAT_VATTYPE");
            if (dtbVatType != null)
            {
                repVAT.DataSource = dtbVatType.DefaultView.ToTable();
                repositoryItemLookUpEdit1.DataSource = dtbVatType.DefaultView.ToTable();
                repositoryItemLookUpEdit2.DataSource = dtbVatType.DefaultView.ToTable();
            }
            LoadComboStore();

            DataTable dtbProtectType = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_PRICEPROTECTTYPE_CACHE");
            cboPriceProtectType.InitControl(false, dtbProtectType, "PRICEPROTECTTYPEID", "PRICEPROTECTTYPENAME", "-- Chọn loại hỗ trợ --");
            cboPriceProtectType.SetValue(1);
            cboPriceProtectType_SelectionChangeCommitted(null, null);
        }

        private void InitControl()
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvDataPriceProtect1);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvAttachment);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvCustomer);
            bolPermissionInvoiceAdd = SystemConfig.objSessionUser.IsPermission(strPermissionInvoiceAdd);
            grdCustomer.DataSource = lstPriceProtect_Customer;
            grdAttachment.DataSource = lstPriceProtect_Attachment;
        }

        private void FormatGrid()
        {
            if (cboPriceProtectType.ColumnID != 3)
            {
                grdViewData.Columns["InputVoucherID"].Visible = false;
                grdViewData.Columns["InVoiceID"].Visible = false;
                grvDataPriceProtect1.Columns["InputVoucherID"].Visible = false;
                grvDataPriceProtect1.Columns["InVoiceID"].Visible = false;

                grvDifference.Columns["InputVoucherID"].Visible = false;
                grvDifference.Columns["InVoiceID"].Visible = false;

            }
            else
            {
                grdViewData.Columns["InputVoucherID"].Visible = true;
                grdViewData.Columns["InVoiceID"].Visible = true;

                grvDataPriceProtect1.Columns["InputVoucherID"].Visible = true;
                grvDataPriceProtect1.Columns["InVoiceID"].Visible = true;
                
                grvDifference.Columns["InputVoucherID"].Visible = true;
                grvDifference.Columns["InVoiceID"].Visible = true;

                grdViewData.Columns["InputVoucherID"].VisibleIndex = 2;
                grdViewData.Columns["InVoiceID"].VisibleIndex = 3;
                grvDataPriceProtect1.Columns["InputVoucherID"].VisibleIndex = 2;
                grvDataPriceProtect1.Columns["InVoiceID"].VisibleIndex = 3;
                grvDifference.Columns["InputVoucherID"].VisibleIndex = 2;
                grvDifference.Columns["InVoiceID"].VisibleIndex = 3;
            }
        }


        private void CreateInvoice(PLC.WSPriceProtect.PriceProtect objPriceProtect)
        {
            try
            {
                DialogResult diaLog = MessageBox.Show(this, "Bạn có muốn tạo hóa đơn hỗ trợ giá?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (diaLog == DialogResult.Yes)
                {
                    DataRow[] rowTransType = dtbInvoiceTransactionType.Select("INVOICETRANSTYPEID=" + cboInvoiceTransactionType.ColumnID);
                    if (rowTransType != null && rowTransType.Length > 0)
                    {
                        object[] objKeywords = new object[]
                        {
                            "@PRICEPROTECTIDLIST", "<" + objPriceProtect.PriceProtectID.Trim() + ">"
                        };
                        DataTable dtbData = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_PRICEPROTECT_SELVALUE", objKeywords);
                        if (dtbData != null && dtbData.Rows.Count > 0)
                        {
                            ERP.PrintVAT.DUI.InputPrintVAT.frmVATPinvoicePriceProtect frm = new ERP.PrintVAT.DUI.InputPrintVAT.frmVATPinvoicePriceProtect();
                            frm.Tag = "PRODUCTID=" + ProductId;
                            frm.PriceProtectIdList = "<" + objPriceProtect.PriceProtectID.Trim() + ">";
                            frm.CustomerID = objPriceProtect.CustomerID;
                            frm.PriceProtectNo = objPriceProtect.PriceProtectNO;
                            int intTransactionGroupId = -1;
                            int.TryParse(rowTransType[0]["TRANSACTIONGROUPID"].ToString(), out intTransactionGroupId);
                            if (intTransactionGroupId == 5)
                                intTransactionGroupId = 3;
                            if (intTransactionGroupId == 4)
                                intTransactionGroupId = 10;
                            frm.StoreIDSelect = cboStoreID.StoreID;
                            frm.TransactionGroupId = intTransactionGroupId;
                            frm.InitializeDataProductService(dtbData, ProductId, true);
                            frm.ShowDialog();
                        }

                    }
                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông", objExce.ToString());
                SystemErrorWS.Insert("Lỗi hủy phiếu thông tin hỗ trợ hàng tồn", objExce.ToString(), this.Name + " -> btnDelete_Click", DUIInventory_Globals.ModuleName);
            }
        }

        private void ClearData()
        {
            dtpPriceProtectDate.MaxDate = Globals.GetServerDateTime();
            dtpPriceProtectDate.Value = dtpPriceProtectDate.MaxDate;

            lstPriceProtect_Customer = new List<PriceProtect_Customer>();
            grdCustomer.DataSource = lstPriceProtect_Customer;

            txtNote.Text = string.Empty;
            dtpInputFromDate.Value = Globals.GetServerDateTime().AddDays(-1);
            lstPriceProtectDetail = new List<PriceProtectDetail>();
            if (lstPriceProtectDetail_IMEI != null)
                lstPriceProtectDetail_IMEI.Clear();
            grdData.DataSource = lstPriceProtectDetail;
            grdData.RefreshDataSource();
        }

        private bool ValidateCalStock()
        {
            DateTime dtpServer = Library.AppCore.Globals.GetServerDateTime();
            if (dtpCallStock.Value.Date >= dtpServer.Date)
            {
                MessageBoxObject.ShowWarningMessage(this, "Ngày chốt tồn phải nhỏ hơn ngày hiện tại!");
                dtpCallStock.Focus();
                SendKeys.Send("{F4}");
                return false;
            }
            if (cboPriceProtectType.ColumnID == 2)
            {
                if (dtpInputFromDate.Value != null && dtpInputToDate.Value != null)
                {
                    if (Convert.ToDateTime(dtpInputFromDate.Value).Date > Convert.ToDateTime(dtpInputToDate.Value).Date)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Ngày nhập:\nNgày bắt đầu không được lớn hơn ngày kết thúc!");
                        dtpInputFromDate.Focus();
                        SendKeys.Send("{F4}");
                        return false;
                    }
                }
            }

            if (chkIsSupplierCall.Checked && (lstPriceProtect_Customer == null || lstPriceProtect_Customer.Count() == 0))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn nhà cung cấp trước khi chốt tồn!");
                tabControl1.SelectedTab = tabCustomer;
                SendKeys.Send("{F4}");
                return false;
            }

            if (grdViewData.RowCount == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn sản phẩm trước khi muốn chốt tồn!");
                txtBarcode.Focus();
                return false;
            }

            if (cboPriceProtectType.ColumnID == 3)
            {
                if (lstPriceProtectDetail.Where(r => string.IsNullOrEmpty(r.InputVoucherID)).Count() > 0)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn hóa đơn cần hỗ trợ giá!");
                    tabControl1.SelectedTab = tabGeneral;
                    btnSearchBill.Focus();
                    return false;
                }

            }

            if (dtpInputToDate.Value != null)
            {
                if (Convert.ToDateTime(dtpInputToDate.Value).Date > dtpServer.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày nhập:\nĐến ngày không được lớn hơn ngày hiện tại!");
                    tabControl1.SelectedTab = tabGeneral;
                    dtpInputToDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }
            }
            if (dtpInputFromDate.Value != null && dtpInputToDate.Value != null)
            {
                if (Convert.ToDateTime(dtpInputFromDate.Value).Date > Convert.ToDateTime(dtpInputToDate.Value).Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày nhập:\nTừ ngày không được nhỏ hơn đến ngày!");
                    tabControl1.SelectedTab = tabGeneral;
                    dtpInputFromDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }
            }
            if (cboPriceProtectType.ColumnID == 2)
            {
                if (dtpInputToDate.Value != null && dtpCallStock.Value != null)
                {
                    if (Convert.ToDateTime(dtpInputToDate.Value).Date > Convert.ToDateTime(dtpCallStock.Value).Date)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Ngày chốt tồn phải lớn hơn ngày nhập!");
                        tabControl1.SelectedTab = tabGeneral;
                        dtpCallStock.Focus();
                        SendKeys.Send("{F4}");
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// CHỐT TỒN THEO TỒN KHO (INDEX = 0)
        /// </summary>
        private void CalInStock()
        {
            if (!ValidateCalStock())
                return;

            string strProductIDList = string.Empty;
            string strIMEIList = string.Empty;
            if (lstPriceProtectDetail.Count > 0)
            {
                foreach (var item in lstPriceProtectDetail)
                {
                    strProductIDList += "<" + item.ProductID.Trim() + ">";
                }
            }
            string strCustomerIDList = string.Empty;
            foreach (var itm in lstPriceProtect_Customer)
                strCustomerIDList += itm.CustomerID + ",";

            object[] objKeywords = new object[]
            {
                "@StockDate", dtpCallStock.Value,
                "@CustomerIDList", strCustomerIDList,
                "@PRODUCTIDLIST",strProductIDList,
                "@ISSUPPLIERCALL", chkIsSupplierCall.Checked
            };
            dtbInStock = null;
            dtbInStock = objPLCReportDataSource.GetHeavyDataSource("PM_PRICEPROTECT_CALLINSTOCK", objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                bolIsCalInStock = false;
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
            if (dtbInStock != null)
            {
                bolIsCalInStock = true;
                lstPriceProtectDetail_IMEI = new List<PriceProtectDetail_IMEI>();
                foreach (var obj in lstPriceProtectDetail)
                {
                    int intInStockQuantity = 0;
                    foreach (DataRow row in dtbInStock.Rows)
                    {
                        if (row["PRODUCTID"].ToString().Trim() == obj.ProductID.Trim())
                        {
                            intInStockQuantity += Convert.ToInt32(row["InstockQuantity"]); // so luong tồn
                            if (obj.IsRequestIMEI)
                            {
                                objPriceProtectDetail_IMEI = new PriceProtectDetail_IMEI();
                                objPriceProtectDetail_IMEI.StoreID = Convert.ToInt32(row["STOREID"]);
                                objPriceProtectDetail_IMEI.StoreName = row["STORENAME"].ToString();
                                objPriceProtectDetail_IMEI.ProductID = obj.ProductID;
                                objPriceProtectDetail_IMEI.ProductIMEI = Convert.ToString(row["IMEI"]);
                                objPriceProtectDetail_IMEI.InputVoucherID = Convert.ToString(row["INPUTVOUCHERID"]).Trim();
                                if (row["INPUTDATE"] != DBNull.Value)
                                    objPriceProtectDetail_IMEI.InputDate = Convert.ToDateTime(row["INPUTDATE"]);
                                else
                                    objPriceProtectDetail_IMEI.InputDate = null;

                                // Nếu chưa hỗ trợ thì IsValid = true / ngược lại là false
                                objPriceProtectDetail_IMEI.IsValid = true;
                                lstPriceProtectDetail_IMEI.Add(objPriceProtectDetail_IMEI);
                            }
                        }
                    }
                    obj.InStockQuantity = intInStockQuantity;
                    obj.Quantity = intInStockQuantity;
                }
                dtpStockDateTemp = Convert.ToDateTime(dtpInputFromDate.Value);
                grdData.RefreshDataSource();
            }
        }

        /// <summary>
        /// CHỐT TỒN THEO NGÀY NHẬP (INDEX = 1)
        /// </summary>
        private void CalStockByInputDate()
        {
            if (!ValidateCalStock())
                return;
            lstPriceProtectDetail = grdViewData.DataSource as List<PriceProtectDetail>;
            string strProductIDList = string.Empty;
            string strIMEIList = string.Empty;
            if (lstPriceProtectDetail.Count > 0)
            {
                foreach (var item in lstPriceProtectDetail)
                {
                    strProductIDList += "<" + item.ProductID.Trim() + ">";
                }
            }
            string strCustomerIDList = string.Empty;
            foreach (var itm in lstPriceProtect_Customer)
                strCustomerIDList += itm.CustomerID + ",";

            object[] objKeywords = new object[]
            {
                "@INPUTFROMDATE", dtpInputFromDate.Value,
                "@INPUTTODATE", dtpInputToDate.Value,
                "@CustomerIDList", strCustomerIDList,
                "@PRODUCTIDLIST",strProductIDList,
                "@ISSUPPLIERCALL", chkIsSupplierCall.Checked,
                "@CALSTOCKDATE", dtpCallStock.Value
            };

            DataTable dtbInStockByInputDate = objPLCReportDataSource.GetHeavyDataSource("PM_PRICEPROTECT_BYINPUTDATE", objKeywords);
            if (dtbInStockByInputDate == null)
            {
                bolIsCalInStock = false;
                MessageBoxObject.ShowWarningMessage(this, "Lỗi tính tồn sản phẩm theo ngày nhập!");
                return;
            }
            lstPriceProtectDetail_IMEI = new List<PriceProtectDetail_IMEI>();
            foreach (var obj in lstPriceProtectDetail)
            {
                int intQuantity = 0;
                foreach (DataRow row in dtbInStockByInputDate.Rows)
                {
                    if (row["PRODUCTID"].ToString().Trim() == obj.ProductID.Trim())
                    {
                        intQuantity += Convert.ToInt32(row["Quantity"]); // so luong nhập
                        if (obj.IsRequestIMEI)
                        {
                            objPriceProtectDetail_IMEI = new PriceProtectDetail_IMEI();
                            objPriceProtectDetail_IMEI.ProductID = obj.ProductID;
                            objPriceProtectDetail_IMEI.ProductIMEI = Convert.ToString(row["IMEI"]);
                            objPriceProtectDetail_IMEI.InputVoucherID = Convert.ToString(row["INPUTVOUCHERID"]).Trim();
                            objPriceProtectDetail_IMEI.StoreID = Convert.ToInt32(row["STOREID"]);
                            objPriceProtectDetail_IMEI.StoreName = Convert.ToString(row["STORENAME"]);

                            if (row["INPUTDATE"] != DBNull.Value)
                                objPriceProtectDetail_IMEI.InputDate = Convert.ToDateTime(row["INPUTDATE"]);
                            else
                                objPriceProtectDetail_IMEI.InputDate = null;
                            objPriceProtectDetail_IMEI.IsValid = true;

                            lstPriceProtectDetail_IMEI.Add(objPriceProtectDetail_IMEI);
                        }
                    }
                }
                obj.InStockQuantity = intQuantity;
                obj.Quantity = intQuantity;
            }
            dtpStockDateTemp = Convert.ToDateTime(dtpInputFromDate.Value);
            grdData.RefreshDataSource();
            bolIsCalInStock = true;
        }

        /// <summary>
        ///  CHỐT TỒN THEO HÓA ĐƠN (INDEX = 2)
        /// </summary>
        private void CalStockByVoucherID()
        {
            if (!ValidateCalStock())
                return;

            lstPriceProtectDetailCount = grdViewData.DataSource as List<PriceProtectDetail>;
            string strVoucherIDList = string.Empty;
            if (lstPriceProtectDetail.Count > 0)
            {
                foreach (var item in lstPriceProtectDetail)
                {
                    strVoucherIDList += "<" + item.InputVoucherID.Trim() + ">";
                }
            }

            dtbInStockByVoucher = null;
            object[] objKeywords = new object[]
            {
                "@INPUTVOUCHERIDLIST",strVoucherIDList,
                "@FROMDATE", dtpInputFromDate.Value,
                "@TODATE", dtpInputToDate.Value,
                "@ISREALINPUT",1
            };
            dtbInStockByVoucher = objPLCReportDataSource.GetHeavyDataSource("GETSTOCKVOUCHER_BYCUSTOMER", objKeywords);
            if (dtbInStockByVoucher == null)
            {
                bolIsCalInStock = false;
                MessageBoxObject.ShowWarningMessage(this, "Lỗi tính tồn sản phẩm theo hóa đơn!");
                return;
            }
            lstPriceProtectDetail_IMEI = new List<PriceProtectDetail_IMEI>();
            foreach (var obj in lstPriceProtectDetailCount)
            {
                int intQuantityTemp = 0;
                int intQuantityOut = 0;

                foreach (DataRow row in dtbInStockByVoucher.Rows)
                {
                    if (row["PRODUCTID"].ToString().Trim() == obj.ProductID.Trim() & row["INPUTVOUCHERID"].ToString().Trim() == obj.InputVoucherID.Trim())
                    {
                        intQuantityTemp += Convert.ToInt32(row["STOCKQUANTITY"]);
                        if (obj.IsRequestIMEI)
                        {
                            objPriceProtectDetail_IMEI = new PriceProtectDetail_IMEI();
                            objPriceProtectDetail_IMEI.ProductID = row["ProductID"].ToString();
                            objPriceProtectDetail_IMEI.StoreID = Convert.ToInt32(row["INPUTSTOREID"]);
                            objPriceProtectDetail_IMEI.StoreName = Convert.ToString(row["STORENAME"]);
                            objPriceProtectDetail_IMEI.ProductIMEI = Convert.ToString(row["PRODUCTIMEI"]);
                            objPriceProtectDetail_IMEI.InputVoucherID = Convert.ToString(row["INPUTVOUCHERID"]);
                            if (row["INPUTDATE"] != DBNull.Value)
                                objPriceProtectDetail_IMEI.InputDate = Convert.ToDateTime(row["INPUTDATE"]);
                            else
                                objPriceProtectDetail_IMEI.InputDate = null;
                            objPriceProtectDetail_IMEI.IsValid = true;
                            lstPriceProtectDetail_IMEI.Add(objPriceProtectDetail_IMEI);
                        }
                    }
                }

                obj.InStockQuantity = intQuantityTemp - intQuantityOut;
                obj.Quantity = intQuantityTemp - intQuantityOut;

                bolIsCalInStock = true;
            }
            dtpStockDateTemp = Convert.ToDateTime(dtpInputFromDate.Value);
            grdData.RefreshDataSource();
        }

        private void EnableControl(bool bolIsEdit)
        {
            //Những button chỉ hiện ở Mode xem chi tiết
            lbPriceProtectNO.Enabled = bolIsEdit;
            txtPriceProtectNO.Enabled = bolIsEdit;
            dtpPriceProtectDate.Enabled = bolIsEdit;
            dtpVoucher.Enabled = bolIsEdit;
            cboStatus.Enabled = bolIsEdit;
            txtPriceProtectID.ReadOnly = bolIsEdit;
            dtpInputFromDate.Enabled = !bolIsEdit;
            dtpInputToDate.Enabled = !bolIsEdit;
            dtpPriceProtectDate.Enabled = !bolIsEdit;
            mnuItemDel.Enabled = !bolIsEdit;
            cboPriceProtectType.Enabled = !bolIsEdit;
            dtpCallStock.Enabled = !bolIsEdit;
            mnuItemImportExcel.Enabled = !bolIsEdit;
            txtBarcode.ReadOnly = bolIsEdit;
            btnSearchBill.Enabled = !bolIsEdit;
            btnSearchProduct.Enabled = !bolIsEdit;
            btnDelete.Visible = bolIsEdit;
            btnDelete.Enabled = SystemConfig.objSessionUser.IsPermission(strPermissionCancel);
            cboInvoiceTransactionType.Enabled = bolIsEdit;
            //cboStoreID.Enabled = bolIsEdit;
            cboStoreID.Enabled = false;

            mnuCustomer.Enabled = !bolIsEdit;
            chkIsSupplierCall.Enabled = !bolIsEdit;
            btnCalStock.Enabled = !bolIsEdit;
        }

        private void Calculate(DevExpress.XtraGrid.Views.Grid.GridView grvControl, string strFieldName)
        {
            if (grdViewData.FocusedRowHandle >= 0)
            {
                var obj = (PriceProtectDetail)grvControl.GetRow(grdViewData.FocusedRowHandle);
                decimal decQuantity = 0;
                if (grvControl == grdViewData)
                    decQuantity = Convert.ToDecimal(obj.InStockQuantity);
                else
                    decQuantity = Convert.ToDecimal(obj.Quantity);
                switch (strFieldName)
                {
                    case "InStockQuantity": // Số lượng hố trợ lần 1
                        obj.TotalAmountNoVat = Math.Round(Convert.ToDecimal(decQuantity * obj.PriceNoVat), 0, MidpointRounding.AwayFromZero);
                        obj.TotalAmount = Math.Round(Convert.ToDecimal(decQuantity * obj.Price), 0, MidpointRounding.AwayFromZero);
                        obj.VATAmount = Math.Round(Convert.ToDecimal(obj.TotalAmount - obj.TotalAmountNoVat), 0, MidpointRounding.AwayFromZero);
                        break;

                    case "Price": // Đơn giá gồm VAT
                        obj.PriceNoVat = Math.Round(Convert.ToDecimal(obj.Price / (1 + (obj.Vat / 100))), 4, MidpointRounding.ToEven);
                        obj.TotalAmountNoVat = Math.Round(Convert.ToDecimal(decQuantity * obj.PriceNoVat), 0, MidpointRounding.AwayFromZero);
                        obj.TotalAmount = Math.Round(Convert.ToDecimal(decQuantity * obj.Price), 0, MidpointRounding.AwayFromZero);
                        obj.VATAmount = Math.Round(Convert.ToDecimal(obj.TotalAmount - obj.TotalAmountNoVat), 0, MidpointRounding.AwayFromZero);
                        break;
                    case "Quantity": // Số lượng hỗ trợ lần 2
                        obj.TotalAmountNoVatVoucher = Math.Round(Convert.ToDecimal(decQuantity * obj.PriceNoVatVoucher), 0, MidpointRounding.AwayFromZero);
                        obj.TotalAmountVoucher = Math.Round(Convert.ToDecimal(decQuantity * obj.PriceVoucher), 0, MidpointRounding.AwayFromZero);
                        obj.VatAmountVoucher = Math.Round(Convert.ToDecimal(obj.TotalAmountVoucher - obj.TotalAmountNoVatVoucher), 0, MidpointRounding.AwayFromZero);
                        break;
                    case "PriceVoucher": // Đơn giá gồm VAT
                        obj.PriceNoVatVoucher = Math.Round(Convert.ToDecimal(obj.PriceVoucher / (1 + (obj.Vat / 100))), 4, MidpointRounding.ToEven);
                        obj.TotalAmountNoVatVoucher = Math.Round(Convert.ToDecimal(decQuantity * obj.PriceNoVatVoucher), 0, MidpointRounding.AwayFromZero);
                        obj.TotalAmountVoucher = Math.Round(Convert.ToDecimal(decQuantity * obj.PriceVoucher), 0, MidpointRounding.AwayFromZero);
                        obj.VatAmountVoucher = Math.Round(Convert.ToDecimal(obj.TotalAmountVoucher - obj.TotalAmountNoVatVoucher), 0, MidpointRounding.AwayFromZero);
                        break;
                }
            }
        }

        private void ChangeTypeProtect()
        {
            if (!bolIsReLoad)
            {
                bolIsReLoad = true;
                return;
            }
            bool bolIsBill = cboPriceProtectType.ColumnID == 3;
            if (grdViewData.RowCount > 0)
            {
                if (MessageBox.Show(this, "Dữ liệu trên lưới sẽ bị xóa. Bạn có muốn tiếp tục thao tác?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                {
                    bolIsReLoad = false;
                    cboPriceProtectType.SetValue(intBeforeSelect);
                    cboPriceProtectType_SelectionChangeCommitted(null, null);
                }
                else
                    lstPriceProtectDetail = new List<PriceProtectDetail>();
                grdData.DataSource = lstPriceProtectDetail;
                grdData.RefreshDataSource();
            }
            FormatGrid();
        }

        /// <summary>
        /// Nhập danh sách IMEI
        /// </summary>
        /// <param name="bolIsDoubleClick"></param>
        private void InputIMEI(DevExpress.XtraGrid.Views.Grid.GridView grvControl, int NumOfAllocate = 1)
        {
            try
            {
                if (grvControl.FocusedRowHandle < 0)
                    return;
                PriceProtectDetail objView = (PriceProtectDetail)grvControl.GetRow(grvControl.FocusedRowHandle);
                List<PriceProtectDetail_IMEI> lstTmp = new List<PriceProtectDetail_IMEI>();
                if (bolIsEdit)
                {
                    if (NumOfAllocate == 1)
                        lstTmp = objView.PriceProtectDetail_ImeiList.ToList();
                    else
                        lstTmp = objView.PriceProtectDetail_ImeiList1.ToList();
                }

                if (objView == null || !objView.IsRequestIMEI)
                    return;
                ERP.Inventory.DUI.Input.frmInputVoucher_ValidBarcode frmInputVoucher_ValidBarcode1 = new ERP.Inventory.DUI.Input.frmInputVoucher_ValidBarcode();
                frmInputVoucher_ValidBarcode1.ProductID = objView.ProductID;
                frmInputVoucher_ValidBarcode1.ProductNameText = objView.ProductName;
                frmInputVoucher_ValidBarcode1.IsAutoCreateIMEI = false;
                frmInputVoucher_ValidBarcode1.IsRequirePincode = false;//True: Sản phẩm yêu cầu nhập PinCode
                frmInputVoucher_ValidBarcode1.IsInputFromRetailInputPrice = false;//Nhập từ máy cũ
                frmInputVoucher_ValidBarcode1.InputVoucherDetailIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>();
                frmInputVoucher_ValidBarcode1.AllIMEIList = new List<PLC.Input.WSInputVoucher.InputVoucherDetailIMEI>();
                frmInputVoucher_ValidBarcode1.IsRequireIMEI = NumOfAllocate == 1;
                if (frmInputVoucher_ValidBarcode1.ShowDialog() == DialogResult.OK)
                {
                    foreach (var objIMEI in frmInputVoucher_ValidBarcode1.InputVoucherDetailIMEIList.ToList())
                    {
                        if (bolIsEdit)
                        {
                            if (!lstTmp.Exists(x => x.ProductIMEI == objIMEI.IMEI.ToString()))
                            {
                                objPriceProtectDetail_IMEI = new PriceProtectDetail_IMEI();
                                objPriceProtectDetail_IMEI.ProductID = objIMEI.ProductID.ToString();
                                objPriceProtectDetail_IMEI.StoreID = cboStoreID.StoreID;
                                objPriceProtectDetail_IMEI.StoreName = cboStoreID.StoreName;
                                objPriceProtectDetail_IMEI.ProductIMEI = objIMEI.IMEI;
                                objPriceProtectDetail_IMEI.InputVoucherID = string.Empty;
                                objPriceProtectDetail_IMEI.InputDate = null;
                                objPriceProtectDetail_IMEI.IsValid = true;
                                objPriceProtectDetail_IMEI.NumOfAllocate = NumOfAllocate;
                                lstTmp.Add(objPriceProtectDetail_IMEI);
                            }
                        }
                        else
                        {
                            if (!lstPriceProtectDetail_IMEI.Exists(x => x.ProductIMEI == objIMEI.IMEI.ToString() && x.NumOfAllocate == NumOfAllocate))
                            {
                                objPriceProtectDetail_IMEI = new PriceProtectDetail_IMEI();
                                objPriceProtectDetail_IMEI.ProductID = objIMEI.ProductID.ToString();
                                objPriceProtectDetail_IMEI.StoreID = cboStoreID.StoreID;
                                objPriceProtectDetail_IMEI.StoreName = cboStoreID.StoreName;
                                objPriceProtectDetail_IMEI.ProductIMEI = objIMEI.IMEI;
                                objPriceProtectDetail_IMEI.InputVoucherID = string.Empty;
                                objPriceProtectDetail_IMEI.InputDate = null;
                                objPriceProtectDetail_IMEI.IsValid = true;
                                objPriceProtectDetail_IMEI.NumOfAllocate = NumOfAllocate;
                                lstPriceProtectDetail_IMEI.Add(objPriceProtectDetail_IMEI);
                            }
                        }
                    }
                    if (bolIsEdit)
                    {
                        if (NumOfAllocate == 1)
                        {
                            objView.PriceProtectDetail_ImeiList = lstTmp.ToArray();
                            int intInStockQuantity = objView.PriceProtectDetail_ImeiList.Where(w => w.ProductID.Trim() == objView.ProductID).Count();
                            PriceProtectDetail objPriceProtectDetail = lstPriceProtectDetail.Find(f => f.ProductID.Trim() == objView.ProductID.Trim());
                            objPriceProtectDetail.InStockQuantity = intInStockQuantity;
                            grvControl.RefreshData();
                        }
                        else
                        {
                            objView.PriceProtectDetail_ImeiList1 = lstTmp.ToArray();
                            int intQuantity = objView.PriceProtectDetail_ImeiList1.Where(w => w.ProductID.Trim() == objView.ProductID).Count();
                            PriceProtectDetail objPriceProtectDetail = lstPriceProtectDetail.Find(f => f.ProductID.Trim() == objView.ProductID.Trim());
                            objPriceProtectDetail.Quantity = intQuantity;
                            grvControl.RefreshData();
                        }
                    }
                    else
                    {
                        int intInStockQuantity = lstPriceProtectDetail_IMEI.Where(w => w.ProductID.Trim() == objView.ProductID).Count();
                        PriceProtectDetail objPriceProtectDetail = lstPriceProtectDetail.Find(f => f.ProductID.Trim() == objView.ProductID.Trim());
                        objPriceProtectDetail.InStockQuantity = intInStockQuantity;
                        grvControl.RefreshData();
                    }
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi nhập imei!", objEx.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi nhập imei!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool IsNumber(string pText)
        {
            if (string.IsNullOrEmpty(pText))
                return false;
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            return regex.IsMatch(pText);
        }


        private void CreateReviewStatus()
        {
            popReivewStatus.ItemLinks.Clear();
            //Chưa duyệt
            DevExpress.XtraBars.BarButtonItem barItem = new DevExpress.XtraBars.BarButtonItem();
            barItem.Caption = "Từ chối";
            barItem.Name = "0";
            barItem.Tag = 0;
            barItem.ImageIndex = 1;
            barItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnReviewOrder_ItemClick);
            popReivewStatus.AddItem(barItem);
            //Đồng ý
            DevExpress.XtraBars.BarButtonItem barItem1 = new DevExpress.XtraBars.BarButtonItem();
            barItem1.Caption = "Đồng ý";
            barItem1.Name = "1";
            barItem1.Tag = 1;
            barItem1.ImageIndex = 0;
            barItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(btnReviewOrder_ItemClick);
            popReivewStatus.AddItem(barItem1);
            btnReviewOrder.Enabled = barManager1.Items.Count > 0;
        }
        #endregion

        #region EVENT

        private void btnUndo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCalStock_Click(object sender, EventArgs e)
        {
            if (cboPriceProtectType.ColumnID < 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn loại hỗ trợ hàng tồn!");
                cboPriceProtectType.Focus();
                SendKeys.Send("{F4}");
                return;
            }
            if (cboPriceProtectType.ColumnID == 1)
            {
                CalInStock();
            }
            else if (cboPriceProtectType.ColumnID == 2)
            {
                CalStockByInputDate();
            }
            else if (cboPriceProtectType.ColumnID == 3)
            {
                CalStockByVoucherID();
            }

        }

        private void cboStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (bolIsEdit && cboStatus.SelectedIndex == 1)
            {
                txtPriceProtectNO.BackColor = SystemColors.Window;
                txtPriceProtectNO.ReadOnly = false;
            }
        }

        private void FormatColumnGrid()
        {
            if (!bolIsEdit)
            {
                //cboStoreID.Enabled = true;
                cboStoreID.Enabled = false;
                grdViewData.Columns["QuantityNoInput"].Visible = false;
                grdViewData.Columns["QuantityInStockNonIsCenter"].Visible = false;
                grdViewData.Columns["QuantityBuy"].Visible = false;
                grdViewData.Columns["QuantityBuyNonIsCenter"].Visible = false;
                grdViewData.Columns["QuantityOtherIncome"].Visible = false;

                grdViewData.Columns["TotalAmountStock"].Visible = false;
                grdViewData.Columns["TotalAmountStockNonIsCenter"].Visible = false;
                grdViewData.Columns["TotalAmountBuy"].Visible = false;
                grdViewData.Columns["TotalAmountBuyNonIsCenter"].Visible = false;
                grdViewData.Columns["TotalAmountOtherIncome"].Visible = false;

                grvDataPriceProtect1.Columns["QuantityNoInputVoucher"].Visible = false;
                grvDataPriceProtect1.Columns["QuantityInStockNonIsCenterVoucher"].Visible = false;
                grvDataPriceProtect1.Columns["QuantityBuyVoucher"].Visible = false;
                grvDataPriceProtect1.Columns["QuantityBuyNonIsCenterVoucher"].Visible = false;
                grvDataPriceProtect1.Columns["QuantityOtherIncomeVoucher"].Visible = false;

                grvDataPriceProtect1.Columns["TotalAmountStockVoucher"].Visible = false;
                grvDataPriceProtect1.Columns["TotalAmountStockNonIsCenterVoucher"].Visible = false;
                grvDataPriceProtect1.Columns["TotalAmountBuyVoucher"].Visible = false;
                grvDataPriceProtect1.Columns["TotalAmountBuyNonIsCenterVoucher"].Visible = false;
                grvDataPriceProtect1.Columns["TotalAmountOtherIncomeVoucher"].Visible = false;

                mnuItemImportValues.Enabled = mnuItemExportTemplate.Enabled = false;
            }
            else
            {
                int intIndex = 4;
                if (cboPriceProtectType.ColumnID == 3)
                    intIndex = 6;

                if (objPriceProtect != null)
                {
                    grdViewData.Columns["QuantityNoInput"].Visible = objPriceProtect.IsHasAllocateByPostdate;
                    grdViewData.Columns["QuantityStock"].Visible = objPriceProtect.IsHasAllocateByPostdate;
                    grdViewData.Columns["QuantityInStockNonIsCenter"].Visible = objPriceProtect.IsHasAllocateByPostdate;
                    grdViewData.Columns["QuantityBuy"].Visible = objPriceProtect.IsHasAllocateByPostdate;
                    grdViewData.Columns["QuantityBuyNonIsCenter"].Visible = objPriceProtect.IsHasAllocateByPostdate;
                    grdViewData.Columns["QuantityOtherIncome"].Visible = objPriceProtect.IsHasAllocateByPostdate;

                    grdViewData.Columns["TotalAmountStock"].Visible = objPriceProtect.IsHasAllocateByPostdate;
                    grdViewData.Columns["TotalAmountStockNonIsCenter"].Visible = objPriceProtect.IsHasAllocateByPostdate;
                    grdViewData.Columns["TotalAmountBuy"].Visible = objPriceProtect.IsHasAllocateByPostdate;
                    grdViewData.Columns["TotalAmountBuyNonIsCenter"].Visible = objPriceProtect.IsHasAllocateByPostdate;
                    grdViewData.Columns["TotalAmountOtherIncome"].Visible = objPriceProtect.IsHasAllocateByPostdate;

                    grvDataPriceProtect1.Columns["QuantityNoInputVoucher"].Visible = objPriceProtect.IsHasAdjustVoucher;
                    grvDataPriceProtect1.Columns["QuantityStockVoucher"].Visible = objPriceProtect.IsHasAdjustVoucher;
                    grvDataPriceProtect1.Columns["QuantityInStockNonIsCenterVoucher"].Visible = objPriceProtect.IsHasAdjustVoucher;
                    grvDataPriceProtect1.Columns["QuantityBuyVoucher"].Visible = objPriceProtect.IsHasAdjustVoucher;
                    grvDataPriceProtect1.Columns["QuantityBuyNonIsCenterVoucher"].Visible = objPriceProtect.IsHasAdjustVoucher;
                    grvDataPriceProtect1.Columns["QuantityOtherIncomeVoucher"].Visible = objPriceProtect.IsHasAdjustVoucher;

                    grvDataPriceProtect1.Columns["TotalAmountStockVoucher"].Visible = objPriceProtect.IsHasAdjustVoucher;
                    grvDataPriceProtect1.Columns["TotalAmountStockNonIsCenterVoucher"].Visible = objPriceProtect.IsHasAdjustVoucher;
                    grvDataPriceProtect1.Columns["TotalAmountBuyVoucher"].Visible = objPriceProtect.IsHasAdjustVoucher;
                    grvDataPriceProtect1.Columns["TotalAmountBuyNonIsCenterVoucher"].Visible = objPriceProtect.IsHasAdjustVoucher;
                    grvDataPriceProtect1.Columns["TotalAmountOtherIncomeVoucher"].Visible = objPriceProtect.IsHasAdjustVoucher;


                    if (objPriceProtect.IsHasAllocateByPostdate)
                    {
                        grdViewData.Columns["QuantityNoInput"].VisibleIndex = intIndex + 6;
                        grdViewData.Columns["QuantityStock"].VisibleIndex = intIndex + 7;
                        grdViewData.Columns["QuantityInStockNonIsCenter"].VisibleIndex = intIndex + 8;
                        grdViewData.Columns["QuantityBuy"].VisibleIndex = intIndex + 9;
                        grdViewData.Columns["QuantityBuyNonIsCenter"].VisibleIndex = intIndex + 10;
                        grdViewData.Columns["QuantityOtherIncome"].VisibleIndex = intIndex + 11;

                        grdViewData.Columns["TotalAmountStock"].VisibleIndex = intIndex + 12;
                        grdViewData.Columns["TotalAmountStockNonIsCenter"].VisibleIndex = intIndex + 13;
                        grdViewData.Columns["TotalAmountBuy"].VisibleIndex = intIndex + 14;
                        grdViewData.Columns["TotalAmountBuyNonIsCenter"].VisibleIndex = intIndex + 15;
                        grdViewData.Columns["TotalAmountOtherIncome"].VisibleIndex = intIndex + 16;
                    }

                    if (objPriceProtect.IsHasAdjustVoucher)
                    {
                        grvDataPriceProtect1.Columns["QuantityNoInputVoucher"].VisibleIndex = intIndex + 6;
                        grvDataPriceProtect1.Columns["QuantityStockVoucher"].VisibleIndex = intIndex + 7;
                        grvDataPriceProtect1.Columns["QuantityInStockNonIsCenterVoucher"].VisibleIndex = intIndex + 8;
                        grvDataPriceProtect1.Columns["QuantityBuyVoucher"].VisibleIndex = intIndex + 9;
                        grvDataPriceProtect1.Columns["QuantityBuyNonIsCenterVoucher"].VisibleIndex = intIndex + 10;
                        grvDataPriceProtect1.Columns["QuantityOtherIncomeVoucher"].VisibleIndex = intIndex + 11;

                        grvDataPriceProtect1.Columns["TotalAmountStockVoucher"].VisibleIndex = intIndex + 12;
                        grvDataPriceProtect1.Columns["TotalAmountStockNonIsCenterVoucher"].VisibleIndex = intIndex + 13;
                        grvDataPriceProtect1.Columns["TotalAmountBuyVoucher"].VisibleIndex = intIndex + 14;
                        grvDataPriceProtect1.Columns["TotalAmountBuyNonIsCenterVoucher"].VisibleIndex = intIndex + 15;
                        grvDataPriceProtect1.Columns["TotalAmountOtherIncomeVoucher"].VisibleIndex = intIndex + 16;
                    }
                    mnuItemImportValues.Enabled = mnuItemExportTemplate.Enabled = !objPriceProtect.IsHasAllocateByPostdate;
                }



                //gridControl.Columns["Price"].VisibleIndex = intIndex;
                //gridControl.Columns["PriceNoVat"].VisibleIndex = intIndex + 1;
                //gridControl.Columns["VatTypeID"].VisibleIndex = intIndex + 2;
                //gridControl.Columns["TotalAmountNoVat"].VisibleIndex = intIndex + 3;
                //gridControl.Columns["VATAmount"].VisibleIndex = intIndex + 4;
                //gridControl.Columns["TotalAmount"].VisibleIndex = intIndex + 5;


            }
        }

        private void dtpStockDate_ValueChanged(object sender, EventArgs e)
        {
            if (bolIsCalInStock)
            {
                if (dtpStockDateTemp != null)
                {
                    if (Convert.ToDateTime(dtpStockDateTemp).Date != Convert.ToDateTime(dtpInputFromDate.Value).Date)
                        bolIsChangeStockDate = true;
                }
            }
            if (bolIsChangeStockDate)
            {
                foreach (var item in lstPriceProtectDetail)
                {
                    item.Quantity = null;
                    item.InStockQuantity = null;
                }
                btnCalStock.Enabled = true;
                bolIsCalInStock = false;
            }
            grdData.RefreshDataSource();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show(this, "Bạn có chắc chắn muốn hủy phiếu hỗ trợ hàng tồn này?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return;
                else
                {
                    bool bolIsResult = objPLCPriceProtect.Delete(objPriceProtect);
                    if (bolIsResult)
                    {
                        MessageBoxObject.ShowInfoMessage(this, "Hủy phiếu hỗ trợ hàng tồn thành công!");
                        IsChange = true;
                        this.Close();
                    }
                    else
                        MessageBoxObject.ShowInfoMessage(this, "Lỗi hủy phiếu thông hỗ trợ hàng tồn!");
                }

            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi hủy phiếu thông tin hỗ trợ hàng tồn", objExce.ToString());
                SystemErrorWS.Insert("Lỗi hủy phiếu thông tin hỗ trợ hàng tồn", objExce.ToString(), this.Name + " -> btnDelete_Click", DUIInventory_Globals.ModuleName);
            }
        }

        private void btnSearchBill_Click(object sender, EventArgs e)
        {
            if (lstPriceProtect_Customer == null || lstPriceProtect_Customer.Count == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn nhà cung cấp!");
                tabControl1.SelectedTab = tabCustomer;
                SendKeys.Send("{F4}");
                return;
            }
            if (dtpInputFromDate.Value != null && dtpInputToDate.Value != null)
            {
                if (Convert.ToDateTime(dtpInputFromDate.Value).Date > Convert.ToDateTime(dtpInputToDate.Value).Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày bắt đầu không được lớn hơn ngày kết thúc!");
                    dtpInputFromDate.Focus();
                    SendKeys.Send("{F4}");
                    return;
                }
            }

            string strProductIDList = string.Empty;
            if (lstPriceProtectDetail != null && lstPriceProtectDetail.Count > 0)
            {
                foreach (var item in lstPriceProtectDetail)
                    strProductIDList += "<" + item.ProductID.Trim() + ">";
            }
            string strCustomerIDList = string.Empty;
            foreach (var itm in lstPriceProtect_Customer)
                strCustomerIDList += itm.CustomerID + ",";


            frmPriceProtectByBill frmPriceBill = new frmPriceProtectByBill();
            frmPriceBill.InputFromDate = Convert.ToDateTime(dtpInputFromDate.Value);
            frmPriceBill.InputToDate = Convert.ToDateTime(dtpInputToDate.Value);
            frmPriceBill.ProductIDList = strProductIDList;
            frmPriceBill.CustomerIDList = strCustomerIDList;
            if (frmPriceBill.ShowDialog() == DialogResult.OK)
            {
                if (frmPriceBill.PriceProtectDetailList != null && frmPriceBill.PriceProtectDetailList.Count > 0)
                    lstPriceProtectDetail = frmPriceBill.PriceProtectDetailList;
                if (frmPriceBill.PriceProtectDetailImeiList != null && frmPriceBill.PriceProtectDetailImeiList.Count > 0)
                    lstPriceProtectDetail_IMEI = frmPriceBill.PriceProtectDetailImeiList;
                dtpInputFromDate.Value = Convert.ToDateTime(frmPriceBill.InputFromDate);
                dtpInputToDate.Value = Convert.ToDateTime(frmPriceBill.InputToDate);
                grdData.DataSource = lstPriceProtectDetail;
                grdData.RefreshDataSource();
                FormatGrid();
            }

        }

        private void dtpStockDateTo_ValueChanged(object sender, EventArgs e)
        {
            if (bolIsCalInStock)
            {
                if (dtpStockDateTemp != null)
                {
                    if (Convert.ToDateTime(dtpStockDateTemp).Date != Convert.ToDateTime(dtpInputToDate.Value).Date)
                        bolIsChangeStockDate = true;
                }
            }
            if (bolIsChangeStockDate)
            {
                foreach (var item in lstPriceProtectDetail)
                {
                    item.Quantity = null;
                    item.InStockQuantity = null;
                }
                btnCalStock.Enabled = true;
                bolIsCalInStock = false;
            }
            grdData.RefreshDataSource();
        }

        private void dtpCallStock_ValueChanged(object sender, EventArgs e)
        {
            if (!IsEdit)
            {
                foreach (var item in lstPriceProtectDetail)
                {
                    item.Quantity = null;
                    item.InStockQuantity = null;
                }
            }
            grdData.RefreshDataSource();
            btnCalStock.Enabled = true;
            bolIsCalInStock = false;
        }

        private void grvAttachment_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (grvAttachment.FocusedRowHandle < 0)
                return;
            var obj = (PriceProtect_Attachment)grvAttachment.GetRow(grvAttachment.FocusedRowHandle);
            if (obj != null)
                obj.IsUpdate = true;
        }

        private void chkIsSupplierCall_CheckedChanged(object sender, EventArgs e)
        {
            if (!IsEdit && lstPriceProtectDetail != null && lstPriceProtectDetail.Count > 0)
            {
                foreach (var item in lstPriceProtectDetail)
                {
                    item.Quantity = null;
                    item.InStockQuantity = null;
                }
                bolIsCalInStock = false;
                grdData.RefreshDataSource();
            }

        }

        private void cboPriceProtectType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboPriceProtectType.ColumnID == 3 || cboPriceProtectType.ColumnID == 4)
            {
                chkIsSupplierCall.Checked = true;
                chkIsSupplierCall.Enabled = false;
            }
            else
            {
                chkIsSupplierCall.Enabled = true;
            }

            if (IsEdit)
                return;
            if (cboPriceProtectType.ColumnID == 1)
            {
                dtpInputFromDate.Enabled = false;
                dtpInputToDate.Enabled = false;
                dtpCallStock.Enabled = true;
                dtpVoucher.Enabled = false;
                dtpInputToDate.Value = Globals.GetServerDateTime().AddDays(-1);
                dtpInputFromDate.Value = Globals.GetServerDateTime().AddDays(-1);
                dtpCallStock.Value = Globals.GetServerDateTime().AddDays(-1);
                btnCalStock.Visible = true;
                lbBarcode.Visible = true;
                txtBarcode.Visible = true;
                btnSearchProduct.Visible = true;
                btnSearchBill.Visible = false;
            }
            else if (cboPriceProtectType.ColumnID == 2)
            {
                dtpInputFromDate.Enabled = true;
                dtpInputToDate.Enabled = true;
                dtpCallStock.Enabled = true;
                dtpVoucher.Enabled = false;
                dtpVoucher.Value = Globals.GetServerDateTime();
                btnCalStock.Visible = true;
                lbBarcode.Visible = true;
                txtBarcode.Visible = true;
                btnSearchProduct.Visible = true;
                btnSearchBill.Visible = false;
            }
            else if (cboPriceProtectType.ColumnID == 3)
            {
                dtpInputFromDate.Enabled = true;
                dtpInputToDate.Enabled = true;
                dtpCallStock.Enabled = false;
                dtpVoucher.Enabled = false;
                dtpCallStock.Value = Globals.GetServerDateTime().AddDays(-1);
                dtpVoucher.Value = Globals.GetServerDateTime();

                btnCalStock.Visible = false;
                lbBarcode.Visible = true;
                txtBarcode.Visible = true;
                btnSearchProduct.Visible = true;
                btnSearchBill.Visible = true;
                //btnSearchBill.Location = new Point(15, 156);

            }
            else if (cboPriceProtectType.ColumnID == 4)
            {
                dtpInputFromDate.Enabled = false;
                dtpInputToDate.Enabled = false;
                dtpCallStock.Enabled = false;
                dtpVoucher.Enabled = false;
                dtpCallStock.Value = Globals.GetServerDateTime().AddDays(-1);
                dtpVoucher.Value = Globals.GetServerDateTime();

                btnCalStock.Visible = false;
                lbBarcode.Visible = true;
                txtBarcode.Visible = true;
                btnSearchProduct.Visible = true;
                btnSearchBill.Visible = false;
                //btnSearchBill.Location = new Point(15, 156);
                colInStockQuantity.OptionsColumn.AllowEdit = true;
                colQuatity.OptionsColumn.AllowEdit = true;
                mnuItemInputIMEI.Visible = !bolIsEdit;
            }
            ChangeTypeProtect();
            intBeforeSelect = cboPriceProtectType.ColumnID;
        }

        #region MenuAction
        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            if (grdViewData.RowCount > 0 && grdViewData.FocusedRowHandle >= 0)
            {
                mnuItemViewIMEI.Enabled = true;
                PriceProtectDetail row = (PriceProtectDetail)grdViewData.GetRow(grdViewData.FocusedRowHandle);
                if (row != null)
                {
                    mnuItemViewIMEI.Enabled = (row.InStockQuantity > 0 && row.IsRequestIMEI);
                    mnuItemInputIMEI.Enabled = (row.IsRequestIMEI);

                }
                else
                    mnuItemViewIMEI.Enabled = false;

                if (IsEdit)
                {
                    mnuItemDel.Visible = false;
                }
                else
                {
                    mnuItemDel.Visible = true;
                    mnuItemDel.Enabled = true;
                }
            }
            else
            {
                mnuItemViewIMEI.Enabled = false;
                mnuItemDel.Enabled = false;
            }

            mnuItemExportExcel.Enabled = grdViewData.RowCount > 0;
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            if (!mnuItemExportExcel.Enabled)
                return;
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }

        private void mnuItemExportTemplate_Click(object sender, EventArgs e)
        {
            if (!mnuItemExportTemplate.Enabled)
                return;
            DataTable dtbData = new DataTable();
            dtbData.Columns.Add("Mã sản phẩm", typeof(String));
            dtbData.Columns.Add("Tên sản phẩm", typeof(String));
            if (cboPriceProtectType.ColumnID == 4)
                dtbData.Columns.Add("IMEI", typeof(String));
            dtbData.Columns.Add("Số lượng", typeof(Decimal));
            dtbData.Columns.Add("Đơn giá (Gồm VAT)", typeof(Decimal));
            foreach (var item in lstPriceProtectDetail)
            {
                DataRow row = dtbData.NewRow();
                row[0] = item.ProductID;
                row[1] = item.ProductName;
                dtbData.Rows.Add(row);
            }
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(this, dtbData);
        }

        private void mnuItemImportValues_Click(object sender, EventArgs e)
        {
            if (!mnuItemImportValues.Enabled)
                return;

            DataTable dtbExcelData = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable();
            if (dtbExcelData == null || dtbExcelData.Rows.Count < 1)
                return;

            DataRow rowCheck = dtbExcelData.Rows[0];
            if (rowCheck[0].ToString().Trim().ToLower() != "mã sản phẩm"
                || rowCheck[1].ToString().Trim().ToLower() != "tên sản phẩm"
                || rowCheck[2].ToString().Trim().ToLower() != "số lượng"
                || rowCheck[3].ToString().Trim().ToLower() != "đơn giá (gồm vat)"
                )
            {
                MessageBox.Show("File nhập không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            DataTable dtbImport = new DataTable();
            dtbExcelData.Rows.RemoveAt(0);
            dtbImport = dtbExcelData;
            dtbImport.Columns.Add("ERRORNOTE", typeof(String));
            dtbImport.Columns.Add("ISERROR", typeof(Boolean));

            dtbImport.Columns[0].ColumnName = "PRODUCTID";
            dtbImport.Columns[1].ColumnName = "PRODUCTNAME";
            dtbImport.Columns[2].ColumnName = "INSTOCKQUANTITY";
            dtbImport.Columns[3].ColumnName = "PRICE";

            bool bolError = false;
            string strProductID = string.Empty;
            string strQuantity = string.Empty;
            string strPrice = string.Empty;
            foreach (DataRow row in dtbImport.Rows)
            {
                strProductID = row["PRODUCTID"].ToString().Trim();
                if (string.IsNullOrEmpty(strProductID))
                {
                    row["ERRORNOTE"] = "Không nhập mã sản phẩm";
                    row["ISERROR"] = true;
                    bolError = true;
                    continue;
                }
                var obj = lstPriceProtectDetail.Where(r => r.ProductID == row["PRODUCTID"].ToString().Trim()).FirstOrDefault();
                if (obj == null)
                {
                    row["ERRORNOTE"] = "Mã sản phẩm không tồn tại trên lưới";
                    row["ISERROR"] = true;
                    bolError = true;
                    continue;
                }

                DataRow[] rowProductID = dtbImport.Select("PRODUCTID = '" + strProductID + "'");
                if (rowProductID.Length > 1)
                {
                    row["ERRORNOTE"] = "Mã sản phẩm bị trùng";
                    row["ISERROR"] = true;
                    bolError = true;
                    continue;
                }

                strQuantity = row["INSTOCKQUANTITY"].ToString();
                if (string.IsNullOrEmpty(strQuantity))
                {
                    row["ERRORNOTE"] = "Không nhập số lượng";
                    row["ISERROR"] = true;
                    bolError = true;
                    continue;
                }

                if (!IsNumber(strQuantity))
                {
                    row["ERRORNOTE"] = "Số lượng không đúng định dạng số";
                    row["ISERROR"] = true;
                    bolError = true;
                    continue;
                }

                strPrice = row["PRICE"].ToString();
                if (string.IsNullOrEmpty(strPrice))
                {
                    row["ERRORNOTE"] = "Không nhập đơn giá";
                    row["ISERROR"] = true;
                    bolError = true;
                    continue;
                }

                if (!IsNumber(strQuantity))
                {
                    row["ERRORNOTE"] = "Đơn giá không đúng định dạng số";
                    row["ISERROR"] = true;
                    bolError = true;
                    continue;
                }

                row["ISERROR"] = false;
            }

            if (bolError)
            {
                frmImportExcelPriceProtect frm = new frmImportExcelPriceProtect(dtbImport);
                frm.ShowDialog();
            }
            else
            {
                foreach (DataRow row in dtbImport.Rows)
                {
                    var obj = lstPriceProtectDetail.Where(r => r.ProductID == row["PRODUCTID"].ToString().Trim()).FirstOrDefault();

                    if (obj != null)
                    {
                        obj.InStockQuantity = Convert.ToInt32(row["INSTOCKQUANTITY"]);
                        obj.Price = Convert.ToInt32(row["PRICE"]);
                        obj.PriceNoVat = Math.Round(Convert.ToDecimal(obj.Price / (1 + (obj.Vat / 100))), 4, MidpointRounding.ToEven);
                        obj.TotalAmountNoVat = Math.Round(Convert.ToDecimal(obj.InStockQuantity * obj.PriceNoVat), 0, MidpointRounding.AwayFromZero);
                        obj.VATAmount = Math.Round(Convert.ToDecimal(obj.InStockQuantity * obj.PriceNoVat * obj.Vat / 100), 0, MidpointRounding.AwayFromZero);
                        obj.TotalAmount = Math.Round(Convert.ToDecimal(obj.TotalAmountNoVat + obj.VATAmount), 0, MidpointRounding.AwayFromZero);
                    }
                }
                grdViewData.RefreshData();
            }
        }

        private void mnuItemInputIMEI_Click(object sender, EventArgs e)
        {
            InputIMEI(grdViewData);
            Calculate(grdViewData, "InStockQuantity");
        }

        private void mnuItemImportExcel_Click(object sender, EventArgs e)
        {
            if (!mnuItemImportExcel.Enabled)
                return;

            if (dtbImport == null)
            {
                dtbImport = new DataTable();
                dtbImport.Columns.Add("PRODUCTID");
                dtbImport.Columns.Add("PRODUCTNAME");
                dtbImport.Columns.Add("IMEI");
                dtbImport.Columns.Add("QUANTITY");
                dtbImport.Columns.Add("PRICE");
                dtbImport.Columns.Add("VAT");
                dtbImport.Columns.Add("VATPERCENT");
                dtbImport.Columns.Add("ISREQUESTIMEI");
                dtbImport.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
                dtbImport.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
                dtbImport.Columns["IMEI"].Caption = "IMEI";
                dtbImport.Columns["QUANTITY"].Caption = "Số lượng";
                dtbImport.Columns["VAT"].Caption = "VAT";
                dtbImport.Columns["VATPERCENT"].Caption = "% nộp thuế";
                dtbImport.Columns["ISREQUESTIMEI"].Caption = "Yêu cầu nhập IMEI";
            }
            else
                dtbImport.Clear();

            ImportProductPriceProtect objImport = new ImportProductPriceProtect(cboPriceProtectType.ColumnID);
            //objImport. = dtbImport;
            DataTable dtbDataResult = null;
            objImport.Import(ref dtbDataResult);
            if (dtbDataResult != null)
            {
                if (lstPriceProtectDetail.Count > 0 && MessageBox.Show(this, "Lưới đã có dữ liệu, thao tác này sẽ xoá hết dữ liệu đang có và thêm mới dữ liệu import?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    lstPriceProtectDetail.Clear();
                    lstPriceProtectDetail_IMEI.Clear();
                    lstPriceProtectDetail_Allocate.Clear();
                }


                if (cboPriceProtectType.ColumnID == 4)
                {
                    DataTable dtbProductImport = dtbDataResult.DefaultView.ToTable(true, "PRODUCTID");
                    foreach (DataRow dr in dtbProductImport.Rows)
                    {
                        var lstProduct = dtbDataResult.Select("PRODUCTID = '" + dr["PRODUCTID"] + "'");
                        if (lstProduct.Any())
                        {
                            bool bolCheckImei = (lstProduct.Count(c => Convert.ToBoolean(c["ISREQUESTIMEI"])) > 0);
                            if (!lstPriceProtectDetail.Exists(x => x.ProductID == dr["PRODUCTID"].ToString()))
                            {
                                PriceProtectDetail objPriceProtectDetail = new PriceProtectDetail();
                                objPriceProtectDetail.ProductID = lstProduct[0]["PRODUCTID"].ToString();
                                objPriceProtectDetail.ProductName = lstProduct[0]["PRODUCTNAME"].ToString();
                                if (bolCheckImei)
                                    objPriceProtectDetail.InStockQuantity = lstProduct.Count();
                                else
                                    objPriceProtectDetail.InStockQuantity = Convert.ToInt32(lstProduct[0]["QUANTITY"]);

                                objPriceProtectDetail.Price = Convert.ToInt32(lstProduct[0]["PRICE"]);
                                objPriceProtectDetail.VatTypeID = Convert.ToInt32(lstProduct[0]["VAT"]);
                                objPriceProtectDetail.Vat = Convert.ToInt32(lstProduct[0]["VAT"]);
                                objPriceProtectDetail.PriceNoVat = Math.Round(Convert.ToDecimal(objPriceProtectDetail.Price / (1 + (objPriceProtectDetail.Vat / 100))), 4, MidpointRounding.ToEven);
                                objPriceProtectDetail.TotalAmountNoVat = Math.Round(Convert.ToDecimal(objPriceProtectDetail.InStockQuantity * objPriceProtectDetail.PriceNoVat), 0, MidpointRounding.AwayFromZero);
                                objPriceProtectDetail.TotalAmount = Math.Round(Convert.ToDecimal(objPriceProtectDetail.InStockQuantity * objPriceProtectDetail.Price), 0, MidpointRounding.AwayFromZero);
                                objPriceProtectDetail.VATAmount = Math.Round(Convert.ToDecimal(objPriceProtectDetail.TotalAmount - objPriceProtectDetail.TotalAmountNoVat), 0, MidpointRounding.AwayFromZero);


                                objPriceProtectDetail.VatPercent = Convert.ToInt32(lstProduct[0]["VATPERCENT"]);
                                objPriceProtectDetail.IsRequestIMEI = Convert.ToBoolean(lstProduct[0]["ISREQUESTIMEI"]);
                                lstPriceProtectDetail.Add(objPriceProtectDetail);
                                if (bolCheckImei)
                                {
                                    DataTable dtbIMEI = lstProduct.CopyToDataTable();
                                    foreach (DataRow row in dtbIMEI.Rows)
                                    {
                                        if (!lstPriceProtectDetail_IMEI.Exists(x => x.ProductIMEI == row["IMEI"].ToString()))
                                        {
                                            objPriceProtectDetail_IMEI = new PriceProtectDetail_IMEI();
                                            objPriceProtectDetail_IMEI.ProductID = row["ProductID"].ToString();
                                            objPriceProtectDetail_IMEI.StoreID = cboStoreID.StoreID;
                                            objPriceProtectDetail_IMEI.StoreName = cboStoreID.StoreName;
                                            objPriceProtectDetail_IMEI.ProductIMEI = Convert.ToString(row["IMEI"]);
                                            objPriceProtectDetail_IMEI.InputVoucherID = string.Empty;
                                            objPriceProtectDetail_IMEI.InputDate = null;
                                            objPriceProtectDetail_IMEI.IsValid = true;
                                            lstPriceProtectDetail_IMEI.Add(objPriceProtectDetail_IMEI);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (bolCheckImei)
                                {
                                    DataTable dtbIMEI = lstProduct.CopyToDataTable();
                                    foreach (DataRow row in dtbIMEI.Rows)
                                    {
                                        if (!lstPriceProtectDetail_IMEI.Exists(x => x.ProductIMEI == dr["IMEI"].ToString()))
                                        {
                                            objPriceProtectDetail_IMEI = new PriceProtectDetail_IMEI();
                                            objPriceProtectDetail_IMEI.ProductID = row["ProductID"].ToString();
                                            objPriceProtectDetail_IMEI.StoreID = cboStoreID.StoreID;
                                            objPriceProtectDetail_IMEI.StoreName = cboStoreID.StoreName;
                                            objPriceProtectDetail_IMEI.ProductIMEI = Convert.ToString(row["IMEI"]);
                                            objPriceProtectDetail_IMEI.InputVoucherID = string.Empty;
                                            objPriceProtectDetail_IMEI.InputDate = null;
                                            objPriceProtectDetail_IMEI.IsValid = true;
                                            lstPriceProtectDetail_IMEI.Add(objPriceProtectDetail_IMEI);
                                        }
                                    }
                                    int intInStockQuantity = lstPriceProtectDetail_IMEI.Where(w => w.ProductID.Trim() == dr["PRODUCTID"].ToString()).Count();
                                    PriceProtectDetail objPriceProtectDetail = lstPriceProtectDetail.Find(f => f.ProductID.Trim() == dr["PRODUCTID"].ToString().Trim());
                                    objPriceProtectDetail.InStockQuantity = intInStockQuantity;
                                }
                                else
                                {
                                    PriceProtectDetail objPriceProtectDetail = lstPriceProtectDetail.Find(f => f.ProductID.Trim() == dr["PRODUCTID"].ToString().Trim());
                                    objPriceProtectDetail.InStockQuantity += Convert.ToInt32(lstProduct[0]["QUANTITY"]);
                                }

                            }
                        }
                    }
                }
                else
                {
                    foreach (DataRow row in dtbDataResult.Rows)
                    {
                        if (!lstPriceProtectDetail.Exists(x => x.ProductID == row["PRODUCTID"].ToString()))
                        {
                            PriceProtectDetail objPriceProtectDetail = new PriceProtectDetail();
                            objPriceProtectDetail.ProductID = row["PRODUCTID"].ToString();
                            objPriceProtectDetail.ProductName = row["PRODUCTNAME"].ToString();
                            objPriceProtectDetail.Price = Convert.ToInt32(row["PRODUCTNAME"]);
                            objPriceProtectDetail.VatTypeID = Convert.ToInt32(row["VAT"]);
                            objPriceProtectDetail.Vat = Convert.ToInt32(row["VAT"]);
                            objPriceProtectDetail.VatPercent = Convert.ToInt32(row["VATPERCENT"]);
                            objPriceProtectDetail.IsRequestIMEI = Convert.ToBoolean(row["ISREQUESTIMEI"]);
                            lstPriceProtectDetail.Add(objPriceProtectDetail);
                        }
                    }
                }


                grdData.DataSource = lstPriceProtectDetail;
                grdData.RefreshDataSource();
            }
        }

        private void mnuItemDel_Click(object sender, EventArgs e)
        {
            if (grdViewData.RowCount == 0)
                return;
            PriceProtectDetail objPriceDetailTemp = (PriceProtectDetail)grdViewData.GetRow(grdViewData.FocusedRowHandle);
            if (objPriceDetailTemp != null)
                lstPriceProtectDetail.Remove(objPriceDetailTemp);
            grdData.RefreshDataSource();
            if (grdViewData.RowCount == 0)
                mnuItemDel.Enabled = false;
        }

        private void mnuItemViewIMEI_Click(object sender, EventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0)
                return;
            PriceProtectDetail objView = (PriceProtectDetail)grdViewData.GetRow(grdViewData.FocusedRowHandle);
            DataTable dtbSourceIMEI = new DataTable();
            if (objView != null)
            {
                if (bolIsEdit)
                {
                    if (objView.PriceProtectDetail_ImeiList.Count() > 0)
                        dtbSourceIMEI = ConvertCustom.ToDataTable(objView.PriceProtectDetail_ImeiList.ToList());
                }
                else
                {
                    dtbSourceIMEI = ConvertCustom.ToDataTable(lstPriceProtectDetail_IMEI);
                }
                if (dtbSourceIMEI != null && dtbSourceIMEI.Rows.Count > 0)
                {
                    dtbSourceIMEI = Library.AppCore.DataTableClass.Select(dtbSourceIMEI, "PRODUCTID = '" + objView.ProductID + "'");
                    //if (cboTypeProtect.SelectedIndex != 2)
                    //    dtbSourceIMEI = Library.AppCore.DataTableClass.Select(dtbSourceIMEI, "PRODUCTID = '" + objView.ProductID + "'");
                    //else
                    //    dtbSourceIMEI = Library.AppCore.DataTableClass.Select(dtbSourceIMEI, "INPUTVOUCHERID = '" + objView.InputVoucherID + "' AND PRODUCTID = '" + objView.ProductID + "'");

                    if (objView.IsRequestIMEI)
                    {
                        frmViewImei frm = new frmViewImei();
                        frm.DtbSource = dtbSourceIMEI;
                        frm.PriceProtectType = cboPriceProtectType.ColumnID;
                        frm.Text = "IMEI tồn kho của sản phẩm " + objView.ProductID + " - " + objView.ProductName;
                        frm.ShowDialog();
                    }
                    else
                    {
                        //frmViewDetailNotImei frm = new frmViewDetailNotImei();
                        //frm.DtbSource = dtbSourceIMEI;
                        //frm.Text = "Số lượng tồn kho của sản phẩm " + objView.ProductID + " - " + objView.ProductName;
                        //frm.ShowDialog();
                    }

                }
            }
        }
        #endregion

        #region grdViewData
        private void grdViewData_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                if (grdViewData.FocusedRowHandle < 0) return;
                if (e.Column.FieldName == "Price" ||
                    e.Column.FieldName == "InStockQuantity")
                {
                    Calculate(grdViewData, e.Column.FieldName);
                    grdViewData.RefreshData();
                }

            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi tạo dữ liệu", objExce.ToString());
                SystemErrorWS.Insert("Lỗi tạo dữ liệu", objExce.ToString(), this.Name + " -> grdViewDataProductService_CellValueChanged", DUIInventory_Globals.ModuleName);
            }
        }

        private void grdViewData_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0)
                return;
            PriceProtectDetail objView = (PriceProtectDetail)grdViewData.GetRow(grdViewData.FocusedRowHandle);
            if (objView.IsRequestIMEI)
            {
                if (grdViewData.FocusedColumn.FieldName.ToUpper() == "INSTOCKQUANTITY")
                {
                    e.Cancel = true;
                }
            }
        }

        private void grdViewData_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0)
                return;

            ERP.Inventory.PLC.WSPriceProtect.PriceProtectDetail objPriceProtectDetail = (ERP.Inventory.PLC.WSPriceProtect.PriceProtectDetail)grdViewData.GetFocusedRow();
            int intAllocateTypeID = -1;
            switch (e.Column.FieldName.ToUpper())
            {
                case "QUANTITYSTOCK":
                    if (objPriceProtectDetail.QuantityStock > 0)
                        intAllocateTypeID = 2;
                    break;
                case "QUANTITYBUY":
                    if (objPriceProtectDetail.QuantityBuy > 0)
                        intAllocateTypeID = 1;
                    break;
                case "QUANTITYINSTOCKNONISCENTER":
                    if (objPriceProtectDetail.QuantityInStockNonIsCenter > 0)
                        intAllocateTypeID = 3;
                    break;
                case "QUANTITYBUYNONISCENTER":
                    if (objPriceProtectDetail.QuantityBuyNonIsCenter > 0)
                        intAllocateTypeID = 4;
                    break;
                case "QUANTITYOTHERINCOME":
                    if (objPriceProtectDetail.QuantityOtherIncome > 0)
                        intAllocateTypeID = 5;
                    break;
            }

            if (intAllocateTypeID < 1)
                return;

            if (objPriceProtectDetail.IsRequestIMEI)
            {
                frmViewImei frm = new frmViewImei();
                frm.DtbSource = ConvertCustom.ToDataTable(objPriceProtectDetail.PriceProtectDetail_ImeiList.Where(r => r.AllocateTypeID == intAllocateTypeID).ToList());
                frm.Text = "Danh sách IMEI của sản phẩm " + objPriceProtectDetail.ProductID + " - " + objPriceProtectDetail.ProductName;
                frm.ShowDialog();
            }
            else
            {
                //frmViewDetailNotImei frm = new frmViewDetailNotImei();
                //frm.DtbSource = ConvertCustom.ToDataTable(objPriceProtectDetail.PriceProtectDetail_ImeiList.Where(r => r.AllocateTypeID == intAllocateTypeID).ToList());
                //frm.Text = "Số lượng tồn kho của sản phẩm " + objPriceProtectDetail.ProductID + " - " + objPriceProtectDetail.ProductName;
                //frm.ShowDialog();
            }
        }
        #endregion

        #region Event Grid PriceProtect 1
        private void grvDataPriceProtect1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                if (grvDataPriceProtect1.FocusedRowHandle < 0) return;
                if (e.Column.FieldName == "PriceVoucher" ||
                    e.Column.FieldName == "Quantity")
                {
                    Calculate(grvDataPriceProtect1, e.Column.FieldName);
                    grvDataPriceProtect1.RefreshData();
                }

            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi tạo dữ liệu", objExce.ToString());
                SystemErrorWS.Insert("Lỗi tạo dữ liệu", objExce.ToString(), this.Name + " -> grdViewDataProductService_CellValueChanged", DUIInventory_Globals.ModuleName);
            }
        }

        private void grvDataPriceProtect1_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if (grvDataPriceProtect1.FocusedRowHandle < 0)
                return;

            ERP.Inventory.PLC.WSPriceProtect.PriceProtectDetail objPriceProtectDetail = (ERP.Inventory.PLC.WSPriceProtect.PriceProtectDetail)grvDataPriceProtect1.GetFocusedRow();
            int intAllocateTypeID = -1;
            switch (e.Column.FieldName.ToUpper())
            {
                case "QUANTITYSTOCKVOUCHER":
                    if (objPriceProtectDetail.QuantityStockVoucher > 0)
                        intAllocateTypeID = 2;
                    break;
                case "QUANTITYBUYVOUCHER":
                    if (objPriceProtectDetail.QuantityBuyVoucher > 0)
                        intAllocateTypeID = 1;
                    break;
                case "QUANTITYINSTOCKNONISCENTERVOUCHER":
                    if (objPriceProtectDetail.QuantityInStockNonIsCenterVoucher > 0)
                        intAllocateTypeID = 3;
                    break;
                case "QUANTITYBUYNONISCENTERVOUCHER":
                    if (objPriceProtectDetail.QuantityBuyNonIsCenterVoucher > 0)
                        intAllocateTypeID = 4;
                    break;
                case "QUANTITYOTHERINCOMEVOUCHER":
                    if (objPriceProtectDetail.QuantityOtherIncomeVoucher > 0)
                        intAllocateTypeID = 5;
                    break;
            }

            if (intAllocateTypeID < 1)
                return;

            if (objPriceProtectDetail.IsRequestIMEI)
            {
                frmViewImei frm = new frmViewImei();
                frm.DtbSource = ConvertCustom.ToDataTable(objPriceProtectDetail.PriceProtectDetail_ImeiList.Where(r => r.AllocateTypeID == intAllocateTypeID).ToList());
                frm.Text = "Danh sách IMEI của sản phẩm " + objPriceProtectDetail.ProductID + " - " + objPriceProtectDetail.ProductName;
                frm.ShowDialog();
            }
            else
            {
                //frmViewDetailNotImei frm = new frmViewDetailNotImei();
                //frm.DtbSource = ConvertCustom.ToDataTable(objPriceProtectDetail.PriceProtectDetail_ImeiList.Where(r => r.AllocateTypeID == intAllocateTypeID).ToList());
                //frm.Text = "Số lượng tồn kho của sản phẩm " + objPriceProtectDetail.ProductID + " - " + objPriceProtectDetail.ProductName;
                //frm.ShowDialog();
            }
        }

        private void grvDataPriceProtect1_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (grvDataPriceProtect1.FocusedRowHandle < 0)
                return;
            PriceProtectDetail objView = (PriceProtectDetail)grvDataPriceProtect1.GetRow(grvDataPriceProtect1.FocusedRowHandle);
            if (objView.IsRequestIMEI)
            {
                if (grvDataPriceProtect1.FocusedColumn.FieldName.ToUpper() == "QUANTITY")
                {
                    e.Cancel = true;
                }
            }
        }
        #endregion

        #region mnuPriceProtect1
        private void mnuPriceProtect1_Opening(object sender, CancelEventArgs e)
        {
            mnuItemInputIMEIProtect1.Visible = cboPriceProtectType.ColumnID == 4;
            if (grvDataPriceProtect1.RowCount > 0 && grvDataPriceProtect1.FocusedRowHandle >= 0)
            {
                mnuItemViewIMEIProtect1.Enabled = true;
                PriceProtectDetail row = (PriceProtectDetail)grvDataPriceProtect1.GetRow(grvDataPriceProtect1.FocusedRowHandle);
                if (row != null)
                {
                    mnuItemViewIMEIProtect1.Enabled = (row.InStockQuantity > 0 && row.IsRequestIMEI);
                    mnuItemInputIMEIProtect1.Enabled = (row.IsRequestIMEI && !objPriceProtect.IsHasInternalInvoice);

                }
                else
                    mnuItemViewIMEIProtect1.Enabled = false;
            }
            else
            {
                mnuItemViewIMEIProtect1.Enabled = false;
            }
            mnuItemExportExcelProtect1.Enabled = grvDataPriceProtect1.RowCount > 0;
        }

        private void mnuItemViewIMEIProtect1_Click(object sender, EventArgs e)
        {
            if (grvDataPriceProtect1.FocusedRowHandle < 0)
                return;
            PriceProtectDetail objView = (PriceProtectDetail)grvDataPriceProtect1.GetRow(grvDataPriceProtect1.FocusedRowHandle);
            DataTable dtbSourceIMEI = new DataTable();
            if (objView != null)
            {
                if (objView.PriceProtectDetail_ImeiList1.Count() > 0)
                    dtbSourceIMEI = ConvertCustom.ToDataTable(objView.PriceProtectDetail_ImeiList1.ToList());
                if (dtbSourceIMEI != null && dtbSourceIMEI.Rows.Count > 0)
                {
                    dtbSourceIMEI = Library.AppCore.DataTableClass.Select(dtbSourceIMEI, "PRODUCTID = '" + objView.ProductID + "'");
                    if (objView.IsRequestIMEI)
                    {
                        frmViewImei frm = new frmViewImei();
                        frm.DtbSource = dtbSourceIMEI;
                        frm.PriceProtectType = cboPriceProtectType.ColumnID;
                        frm.Text = "IMEI tồn kho của sản phẩm " + objView.ProductID + " - " + objView.ProductName;
                        frm.ShowDialog();
                    }
                }
            }
        }

        private void mnuItemDelProtect1_Click(object sender, EventArgs e)
        {
            if (grvDataPriceProtect1.RowCount == 0)
                return;
            PriceProtectDetail objPriceDetailTemp = (PriceProtectDetail)grvDataPriceProtect1.GetRow(grvDataPriceProtect1.FocusedRowHandle);
            if (objPriceDetailTemp != null)
                lstPriceProtectDetail.Remove(objPriceDetailTemp);
            grdDataPriceProtect1.RefreshDataSource();
            if (grdViewData.RowCount == 0)
                mnuItemDel.Enabled = false;
        }

        private void mnuItemImportValues1_Click(object sender, EventArgs e)
        {
            if (!mnuItemImportValues1.Enabled)
                return;

            DataTable dtbExcelData = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable();
            if (dtbExcelData == null || dtbExcelData.Rows.Count < 1)
                return;

            DataRow rowCheck = dtbExcelData.Rows[0];
            if (rowCheck[0].ToString().Trim().ToLower() != "mã sản phẩm"
                || rowCheck[1].ToString().Trim().ToLower() != "tên sản phẩm"
                || rowCheck[2].ToString().Trim().ToLower() != "số lượng"
                || rowCheck[3].ToString().Trim().ToLower() != "đơn giá (gồm vat)"
                )
            {
                MessageBox.Show("File nhập không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            DataTable dtbImport = new DataTable();
            dtbExcelData.Rows.RemoveAt(0);
            dtbImport = dtbExcelData;
            dtbImport.Columns.Add("ERRORNOTE", typeof(String));
            dtbImport.Columns.Add("ISERROR", typeof(Boolean));

            dtbImport.Columns[0].ColumnName = "PRODUCTID";
            dtbImport.Columns[1].ColumnName = "PRODUCTNAME";
            dtbImport.Columns[2].ColumnName = "QUANTITY";
            dtbImport.Columns[3].ColumnName = "PRICE";

            bool bolError = false;
            string strProductID = string.Empty;
            string strQuantity = string.Empty;
            string strPrice = string.Empty;
            foreach (DataRow row in dtbImport.Rows)
            {
                strProductID = row["PRODUCTID"].ToString().Trim();
                if (string.IsNullOrEmpty(strProductID))
                {
                    row["ERRORNOTE"] = "Không nhập mã sản phẩm";
                    row["ISERROR"] = true;
                    bolError = true;
                    continue;
                }
                var obj = lstPriceProtectDetail.Where(r => r.ProductID == row["PRODUCTID"].ToString().Trim()).FirstOrDefault();
                if (obj == null)
                {
                    row["ERRORNOTE"] = "Mã sản phẩm không tồn tại trên lưới";
                    row["ISERROR"] = true;
                    bolError = true;
                    continue;
                }

                DataRow[] rowProductID = dtbImport.Select("PRODUCTID = '" + strProductID + "'");
                if (rowProductID.Length > 1)
                {
                    row["ERRORNOTE"] = "Mã sản phẩm bị trùng";
                    row["ISERROR"] = true;
                    bolError = true;
                    continue;
                }

                strQuantity = row["QUANTITY"].ToString();
                if (string.IsNullOrEmpty(strQuantity))
                {
                    row["ERRORNOTE"] = "Không nhập số lượng";
                    row["ISERROR"] = true;
                    bolError = true;
                    continue;
                }

                if (!IsNumber(strQuantity))
                {
                    row["ERRORNOTE"] = "Số lượng không đúng định dạng số";
                    row["ISERROR"] = true;
                    bolError = true;
                    continue;
                }

                strPrice = row["PRICE"].ToString();
                if (string.IsNullOrEmpty(strPrice))
                {
                    row["ERRORNOTE"] = "Không nhập đơn giá";
                    row["ISERROR"] = true;
                    bolError = true;
                    continue;
                }

                if (!IsNumber(strQuantity))
                {
                    row["ERRORNOTE"] = "Đơn giá không đúng định dạng số";
                    row["ISERROR"] = true;
                    bolError = true;
                    continue;
                }

                row["ISERROR"] = false;
            }

            if (bolError)
            {
                frmImportExcelPriceProtect frm = new frmImportExcelPriceProtect(dtbImport);
                frm.ShowDialog();
            }
            else
            {
                foreach (DataRow row in dtbImport.Rows)
                {
                    var obj = lstPriceProtectDetail.Where(r => r.ProductID == row["PRODUCTID"].ToString().Trim()).FirstOrDefault();

                    if (obj != null)
                    {
                        decimal decQuantity = Convert.ToInt32(row["QUANTITY"]);
                        obj.Quantity = Convert.ToInt32(row["QUANTITY"]);
                        obj.PriceVoucher = Convert.ToInt32(row["PRICE"]);
                        obj.PriceNoVatVoucher = Math.Round(Convert.ToDecimal(obj.PriceVoucher / (1 + (obj.Vat / 100))), 4, MidpointRounding.ToEven);
                        obj.TotalAmountNoVatVoucher = Math.Round(Convert.ToDecimal(decQuantity * obj.PriceNoVatVoucher), 0, MidpointRounding.AwayFromZero);
                        obj.TotalAmountVoucher = Math.Round(Convert.ToDecimal(decQuantity * obj.PriceVoucher), 0, MidpointRounding.AwayFromZero);
                        obj.VatAmountVoucher = Math.Round(Convert.ToDecimal(obj.TotalAmountVoucher - obj.TotalAmountNoVatVoucher), 0, MidpointRounding.AwayFromZero);
                    }
                }
                grvDataPriceProtect1.RefreshData();
            }
        }

        private void mnuItemExportTemplate1_Click(object sender, EventArgs e)
        {
            if (!mnuItemExportTemplateProtect1.Enabled)
                return;
            DataTable dtbData = new DataTable();
            dtbData.Columns.Add("Mã sản phẩm", typeof(String));
            dtbData.Columns.Add("Tên sản phẩm", typeof(String));
            if (cboPriceProtectType.ColumnID == 4)
                dtbData.Columns.Add("IMEI", typeof(String));
            dtbData.Columns.Add("Số lượng", typeof(Decimal));
            dtbData.Columns.Add("Đơn giá (Gồm VAT)", typeof(Decimal));
            foreach (var item in lstPriceProtectDetail)
            {
                DataRow row = dtbData.NewRow();
                row[0] = item.ProductID;
                row[1] = item.ProductName;
                dtbData.Rows.Add(row);
            }
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(this, dtbData);
        }

        private void mnuItemExportExcelProtect1_Click(object sender, EventArgs e)
        {
            if (!mnuItemExportExcelProtect1.Enabled)
                return;
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdDataPriceProtect1);
        }
        #endregion

        #endregion

        private void btnReviewOrder_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intReview = Convert.ToInt32(e.Item.Tag);
            var objResultMessage = objPLCPriceProtect.Appove(objPriceProtect.PriceProtectID, intReview);
            if (objResultMessage.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, objResultMessage.Message, objResultMessage.MessageDetail);
                return;
            }
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                return;
            }
            MessageBox.Show("Duyệt thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void tabGeneral_Click(object sender, EventArgs e)
        {

        }

        private void mnuItemInputIMEIProtect1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "IMEI của sản phẩm đã có sẽ bị xóa. Bạn có muốn tiếp tục?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                return;
            PriceProtectDetail objView = (PriceProtectDetail)grvDataPriceProtect1.GetRow(grvDataPriceProtect1.FocusedRowHandle);
            List<PriceProtectDetail_IMEI> objPriceProtectDetail_IMEIList = objView.PriceProtectDetail_ImeiList1.ToList();
            List<PriceProtectDetail_IMEI> objPriceProtectDetail_IMEIListClone = new List<PriceProtectDetail_IMEI>();
            
            objView.PriceProtectDetail_ImeiList1 = objPriceProtectDetail_IMEIListClone.ToArray();
            InputIMEI(grvDataPriceProtect1, 2);
            if (objView.PriceProtectDetail_ImeiList1.Count() == 0)
                objView.PriceProtectDetail_ImeiList1 = objPriceProtectDetail_IMEIList.ToArray();
            Calculate(grvDataPriceProtect1, "Quantity");
        }
    }
    static class ConvertCustom
    {
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.Name == "InputDate" || prop.Name == "AllocateDate")
                    table.Columns.Add(prop.Name, typeof(DateTime));
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
    }
}
