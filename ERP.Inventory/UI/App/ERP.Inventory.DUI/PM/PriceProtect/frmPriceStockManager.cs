﻿using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using ERP.Inventory.DUI.Inventory;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.PM.PriceProtect
{
    public partial class frmPriceStockManager : Form
    {
        private DataTable dtbSource = null;
        private DataTable dtbCompany = null;
        private PLC.PM.PLCPriceProtect objPLCPriceProtect = new PLC.PM.PLCPriceProtect();
        private PLC.WSPriceProtect.ResultMessage objResultMessage = new PLC.WSPriceProtect.ResultMessage();
        private Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection objSelection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection();
        private string strPermission_Edit = "PM_PRICEPROTECT_EDIT";
        private string strPermission_DeleteSearch = "PM_PRICEPROTECT_ISDELETED_SRH";
        private string strPermissionInvoiceAdd = "PM_PRICEPROTECT_INVOICE_ADD";
        private bool bolPermisstion_Edit = false;
        private bool bolPermissionInvoiceAdd = false;

        private string strProductID = string.Empty;
        private bool bolIsDelete = false;

        public frmPriceStockManager()
        {
            InitializeComponent();
        }

        private void LoadCombobox()
        {
            dtbCompany = Library.AppCore.DataSource.GetDataSource.GetCompany().Copy();
            cboCompany.InitControl(false, dtbCompany, "COMPANYID", "COMPANYNAME", "--Chọn công ty--");
            cboCompany.SetValue(SystemConfig.DefaultCompany.CompanyID);
            
            Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            objStoreFilter.IsCheckPermission = true;
            objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.ALL;
            cboStore.InitControl(true, objStoreFilter);
            cboStore.IsReturnAllWhenNotChoose = true;

            DataTable dtbProtectType = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_PRICEPROTECTTYPE_CACHE");
            cboPriceProtectType.InitControl(true, dtbProtectType, "PRICEPROTECTTYPEID", "PRICEPROTECTTYPENAME", "-- Tất cả --");

            cboStatus.SelectedIndex = 0;
        }
        private void InitControl()
        {
            objSelection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection(true, true, grdViewData);
            objSelection.CheckMarkColumn.Width = 40;
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            chkIsDeleted.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_DeleteSearch);
            bolPermissionInvoiceAdd = SystemConfig.objSessionUser.IsPermission(strPermissionInvoiceAdd);
            bolPermisstion_Edit = SystemConfig.objSessionUser.IsPermission(strPermission_Edit);
            GetParameterForm();
            cboSearchBy.SelectedIndex = 0;
            DateTime dteServerTime = Globals.GetServerDateTime();
            dteStockToDate.Value = dteServerTime;
            dteInputToDate.Value = dteServerTime;
            dteProtectToDate.Value = dteServerTime;
            dtePostToDate.Value = dteServerTime;
            dteVoucherToDate.Value = dteServerTime;
            dteStockFromDate.Value = dteInputFromDate.Value = dteProtectFromDate.Value = dtePostFromDate.Value = dteVoucherFromDate.Value = new DateTime(dteServerTime.Year, dteServerTime.Month, 1);
        }
        private bool GetParameterForm()
        {
            try
            {
                if (this.Tag == null || this.Tag.ToString() == string.Empty)
                {
                    MessageBoxObject.ShowWarningMessage(this, @"Chưa khai báo tham số cấu hình PRODUCTID = ?");
                    return false;
                }
                Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                if (hstbParam.ContainsKey("PRODUCTID"))
                    strProductID = (hstbParam["PRODUCTID"] ?? "").ToString().Trim();
                if (string.IsNullOrEmpty(strProductID))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Chưa khai báo tham số mã sản phẩm!");
                    return false;
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi lấy tham số cấu hình form " + this.Text, objExce, "frmPriceStockManager ->GetParameterForm");
                MessageBoxObject.ShowWarningMessage(this, "Lỗi khi lấy tham số cấu hình form " + this.Text);
                return false;
            }
            return true;
        }
        private bool ValidateData()
        {
            DateTime dteSysDate = Globals.GetServerDateTime();
            if (cboCompany.ColumnID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn công ty!");
                cboCompany.Focus();
                return false;
            }

            if (chkSearchByProtectDate.Checked)
            {
                if (dteProtectFromDate.Value.Date > dteProtectToDate.Value.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày lập bắt đầu không được lớn hơn đến ngày!");
                    dteProtectFromDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }

                if (dteProtectToDate.Value.Date > dteSysDate.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày lập bắt đầu không được lớn hơn ngày hiện tại!");
                    dteProtectToDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }
            }

            if (chkSearchByStockDate.Checked)
            {
                if (dteStockFromDate.Value.Date > dteStockToDate.Value.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày chốt tồn bắt đầu không được lớn hơn đến ngày!");
                    dteStockFromDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }

                if (dteStockToDate.Value.Date > dteSysDate.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày chốt tồn bắt đầu không được lớn hơn ngày hiện tại!");
                    dteStockToDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }
            }

            if (chkSearchByInputDate.Checked)
            {
                if (dteInputFromDate.Value.Date > dteInputToDate.Value.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày nhập bắt đầu không được lớn hơn đến ngày!");
                    dteInputFromDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }

                if (dteInputToDate.Value.Date > dteSysDate.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày nhập bắt đầu không được lớn hơn ngày hiện tại!");
                    dteInputToDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }
            }


            if (chkSearchByPostDate.Checked)
            {
                if (dtePostFromDate.Value.Date > dtePostToDate.Value.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày hạch toán 1 bắt đầu không được lớn hơn đến ngày!");
                    dtePostFromDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }

                if (dtePostToDate.Value.Date > dteSysDate.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày hạch toán 1 bắt đầu không được lớn hơn ngày hiện tại!");
                    dtePostToDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }
            }

            if (chkSearchByVoucherDate.Checked)
            {
                if (dteVoucherFromDate.Value.Date > dteVoucherToDate.Value.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày hạch toán 2 bắt đầu không được lớn hơn đến ngày!");
                    dteVoucherFromDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }

                if (dteVoucherToDate.Value.Date > dteSysDate.Date)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày hạch toán 2 bắt đầu không được lớn hơn ngày hiện tại!");
                    dteVoucherToDate.Focus();
                    SendKeys.Send("{F4}");
                    return false;
                }
            }

            return true;
        }
        private bool LoadData()
        {
            objSelection.ClearSelection();
            bolIsDelete = chkIsDeleted.Checked;
            dtbSource = null;
            object[] objKeywords = new object[]
            {
                "@CompanyID", cboCompany.ColumnID,
                "@IsSearchByProtectDate", chkSearchByProtectDate.Checked,
                "@ProtectFromDate", dteProtectFromDate.Value,
                "@ProtectToDate", dteProtectToDate.Value,
                "@IsSearchByInputDate", chkSearchByInputDate.Checked,
                "@InputFromDate", dteInputFromDate.Value,
                "@InputToDate", dteInputToDate.Value,
                "@PriceProtectTypeIDList", cboPriceProtectType.ColumnIDList,
                "@IsSearchByStockDate", chkSearchByStockDate.Checked,
                "@StockFromDate", dteStockFromDate.Value,
                "@StockToDate", dteStockToDate.Value,
                "@IsSearchByPostDate", chkSearchByPostDate.Checked,
                "@PostFromDate", dtePostFromDate.Value,
                "@PostToDate", dtePostToDate.Value,
                "@IsSearchByVoucherDate", chkSearchByVoucherDate.Checked,
                "@VoucherFromDate", dteVoucherFromDate.Value,
                "@VoucherToDate", dteVoucherToDate.Value,
                "@STOREIDLIST", cboStore.StoreIDList,
                "@Status", cboStatus.SelectedIndex -1,
                "@IsHasVATPinvoice", chkIsHasVATPinvoice.Checked,
                "@IsAdjustCostPrice", chkIsAdjustCostPrice.Checked,
                "@ISDELETED", bolIsDelete,
                "@KEYWORDS", txtKeywords.Text.Trim(),
                "@SearchBy", cboSearchBy.SelectedIndex,
                "@IsViewDetail", chkIsViewDetail.Checked
            };

            objResultMessage = objPLCPriceProtect.SearchData(ref dtbSource, objKeywords);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message,
                    Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }

            grdData.DataSource = dtbSource;
            grdData.RefreshDataSource();
            mnuItemExportExcel.Enabled = btnExportExcel.Enabled = grdViewData.DataRowCount > 0;

            FormatGrid();
            
            return true;
        }


        private void frmPriceStockManager_Load(object sender, EventArgs e)
        {
            InitControl();
            LoadCombobox();
            FormatGrid();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!btnSearch.Enabled)
                return;
            if (!ValidateData())
                return;
            
            btnSearch.Enabled = false;
            LoadData();
            btnSearch.Enabled = true;
            if (grdViewData.DataRowCount == 0)
                MessageBox.Show(this, "Không tìm thấy dữ liệu báo cáo!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void FormatGrid()
        {
            int index = 20;

            grdViewData.Columns["MSTOTALAMOUNTNOVAT"].Visible = !chkIsViewDetail.Checked;
            grdViewData.Columns["MSTOTALAMOUNT"].Visible = !chkIsViewDetail.Checked;
            grdViewData.Columns["PRODUCTID"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["PRODUCTNAME"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["INSTOCKQUANTITY"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["PRICENOVAT"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["PRICE"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["TOTALAMOUNTNOVAT"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["TOTALAMOUNT"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["QUANTITYNOINPUT"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["QUANTITYSTOCK"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["QUANTITYINSTOCKNONISCENTER"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["QUANTITYBUY"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["QUANTITYBUYNONISCENTER"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["QUANTITYOTHERINCOME"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["TOTALAMOUNTSTOCK"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["TOTALAMOUNTSTOCKNONISCENTER"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["TOTALAMOUNTBUY"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["TOTALAMOUNTBUYNONISCENTER"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["TOTALAMOUNTOTHERINCOME"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["DELETEDFULLNAME"].Visible = chkIsDeleted.Checked;
            grdViewData.Columns["DELETEDDATE"].Visible = chkIsDeleted.Checked;

            grdViewData.Columns["QUANTITY"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["PRICENOVATVOUCHER"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["PRICEVOUCHER"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["TOTALAMOUNTNOVATVOUCHER"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["TOTALAMOUNTVOUCHER"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["QUANTITYNOINPUTVOUCHER"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["QUANTITYSTOCKVOUCHER"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["QUANTITYINSTOCKNONISCENTER1"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["QUANTITYBUYVOUCHER"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["QUANTITYBUYNONISCENTER1"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["QUANTITYOTHERINCOMEVOUCHER"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["TOTALAMOUNTSTOCKVOUCHER"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["TOTALAMOUNTSTOCKNONISCENTER1"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["TOTALAMOUNTBUYVOUCHER"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["TOTALAMOUNTBUYNONISCENTER1"].Visible = chkIsViewDetail.Checked;
            grdViewData.Columns["TOTALAMOUNTOTHERINCOMEVOUCHER"].Visible = chkIsViewDetail.Checked;

            if (!chkIsViewDetail.Checked)
            {
                grdViewData.Columns["MSTOTALAMOUNTNOVAT"].VisibleIndex = index + 1;
                grdViewData.Columns["MSTOTALAMOUNT"].VisibleIndex = index + 2;

                grdViewData.Columns["CREATEDFULLNAME"].VisibleIndex = index + 3;
                grdViewData.Columns["CREATEDDATE"].VisibleIndex = index + 4;
                grdViewData.Columns["UPDATEDFULLNAME"].VisibleIndex = index + 5;
                grdViewData.Columns["UPDATEDDATE"].VisibleIndex = index + 6;
                if (chkIsDeleted.Checked)
                { 
                    grdViewData.Columns["DELETEDFULLNAME"].VisibleIndex = index + 7;
                    grdViewData.Columns["DELETEDDATE"].VisibleIndex = index + 8;
                }
            }
            else
            {
                grdViewData.Columns["PRODUCTID"].VisibleIndex = index + 1;
                grdViewData.Columns["PRODUCTNAME"].VisibleIndex = index + 2;
                grdViewData.Columns["INSTOCKQUANTITY"].VisibleIndex = index + 3;
                grdViewData.Columns["PRICENOVAT"].VisibleIndex = index + 4;
                grdViewData.Columns["PRICE"].VisibleIndex = index + 5;
                grdViewData.Columns["TOTALAMOUNTNOVAT"].VisibleIndex = index + 6;
                grdViewData.Columns["TOTALAMOUNT"].VisibleIndex = index + 7;
                grdViewData.Columns["QUANTITYNOINPUT"].VisibleIndex = index + 8;
                grdViewData.Columns["QUANTITYSTOCK"].VisibleIndex = index + 9;
                grdViewData.Columns["QUANTITYINSTOCKNONISCENTER"].VisibleIndex = index + 10;
                grdViewData.Columns["QUANTITYBUY"].VisibleIndex = index + 11;
                grdViewData.Columns["QUANTITYBUYNONISCENTER"].VisibleIndex = index + 12;
                grdViewData.Columns["QUANTITYOTHERINCOME"].VisibleIndex = index + 13;
                grdViewData.Columns["TOTALAMOUNTSTOCK"].VisibleIndex = index + 14;
                grdViewData.Columns["TOTALAMOUNTSTOCKNONISCENTER"].VisibleIndex = index + 15;
                grdViewData.Columns["TOTALAMOUNTBUY"].VisibleIndex = index + 16;
                grdViewData.Columns["TOTALAMOUNTBUYNONISCENTER"].VisibleIndex = index + 17;
                grdViewData.Columns["TOTALAMOUNTOTHERINCOME"].VisibleIndex = index + 18;

                grdViewData.Columns["QUANTITY"].VisibleIndex = index + 19;
                grdViewData.Columns["PRICENOVATVOUCHER"].VisibleIndex = index + 20;
                grdViewData.Columns["PRICEVOUCHER"].VisibleIndex = index + 21;
                grdViewData.Columns["TOTALAMOUNTNOVATVOUCHER"].VisibleIndex = index + 22;
                grdViewData.Columns["TOTALAMOUNTVOUCHER"].VisibleIndex = index + 23;
                grdViewData.Columns["QUANTITYNOINPUTVOUCHER"].VisibleIndex = index + 24;
                grdViewData.Columns["QUANTITYSTOCKVOUCHER"].VisibleIndex = index + 25;
                grdViewData.Columns["QUANTITYINSTOCKNONISCENTER1"].VisibleIndex = index + 26;
                grdViewData.Columns["QUANTITYBUYVOUCHER"].VisibleIndex = index + 27;
                grdViewData.Columns["QUANTITYBUYNONISCENTER1"].VisibleIndex = index + 28;
                grdViewData.Columns["QUANTITYOTHERINCOMEVOUCHER"].VisibleIndex = index + 29;
                grdViewData.Columns["TOTALAMOUNTSTOCKVOUCHER"].VisibleIndex = index + 30;
                grdViewData.Columns["TOTALAMOUNTSTOCKNONISCENTER1"].VisibleIndex = index + 31;
                grdViewData.Columns["TOTALAMOUNTBUYVOUCHER"].VisibleIndex = index + 32;
                grdViewData.Columns["TOTALAMOUNTBUYNONISCENTER1"].VisibleIndex = index + 33;
                grdViewData.Columns["TOTALAMOUNTOTHERINCOMEVOUCHER"].VisibleIndex = index + 34;

                if (chkIsDeleted.Checked)
                {
                    grdViewData.Columns["DELETEDFULLNAME"].VisibleIndex = index + 35;
                    grdViewData.Columns["DELETEDDATE"].VisibleIndex = index + 36;
                }
            }
        }
        private void grdViewData_DoubleClick(object sender, EventArgs e)
        {
            mnuViewItem_Click(null, null);
        }

        private void mnuViewItem_Click(object sender, EventArgs e)
        {
            if (!mnuViewItem.Enabled)
                return;

            if (grdViewData.FocusedRowHandle < 0)
                return;
            DataRow rowSelect = grdViewData.GetFocusedDataRow();
            if (rowSelect != null)
            {
                frmPriceProtectStock frmPrice = new frmPriceProtectStock();
                frmPrice.PriceProtectID = rowSelect["PRICEPROTECTID"].ToString();
                frmPrice.ProductId = strProductID;
                frmPrice.IsEdit = true;
                frmPrice.ShowDialog();
                if (frmPrice.IsChange)
                    LoadData();
            }
        }

        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            if (grdViewData.RowCount > 0 && !chkIsDeleted.Checked)
            {
                mnuCreateItem.Enabled = bolPermissionInvoiceAdd;
                mnuViewItem.Enabled = bolPermisstion_Edit;
            }
            else
            {
                mnuCreateItem.Enabled = false;
                mnuViewItem.Enabled = false;
            }

            if (objSelection.SelectedCount > 0)
            {
                for (int i = 0; i < objSelection.SelectedCount; i++)
                {
                    DataRowView rowSelect = objSelection.GetSelectedRow(i) as DataRowView;
                    if (rowSelect != null)
                    {
                        if (Convert.ToInt32(rowSelect["STATUS"].ToString()) == 0 || rowSelect["ISINVOICE"].ToString().Trim() == "1")
                            mnuCreateItem.Enabled = false;
                    }
                }
            }
            else if (grdViewData.FocusedRowHandle >= 0)
            {
                DataRow rowSelect = grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
                if (rowSelect != null)
                {
                    if (Convert.ToInt32(rowSelect["STATUS"].ToString()) == 0 || rowSelect["ISINVOICE"].ToString().Trim() == "1")
                        mnuCreateItem.Enabled = false;
                }
            }
        }

        private bool ValidateMultiSelect(ref string strPriceProductIDList, ref string strPriceProtectNoList, ref int intTransactionGroupId, ref int intCustomerID, ref int intStoreID)
        {
            int intCustomerId_Temp = -1;
            int intInvoiceTransGroupId_Temp = -1;
            int intStoreID_Temp = -1;
            for (int i = 0; i < objSelection.SelectedCount; i++)
            {
                DataRowView rowSelect = objSelection.GetSelectedRow(i) as DataRowView;
                if (Convert.ToInt32(rowSelect["STATUS"]) == 0)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Phiếu hỗ trợ số " + rowSelect["PRICEPROTECTID"].ToString() + " chưa có số biên bản!");
                    return false;
                }

                if (Convert.ToInt32(rowSelect["ISINVOICE"].ToString()) == 1)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Phiếu hỗ trợ số " + rowSelect["PRICEPROTECTID"].ToString() + " đã được lập hóa đơn!");
                    return false;
                }

                if (i > 0)
                {
                    if (Convert.ToInt32(rowSelect["CUSTOMERID"]) != intCustomerId_Temp)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Không được phép lập hóa đơn khác nhà cung cấp!");
                        return false;
                    }
                    if (Convert.ToInt32(rowSelect["TRANSACTIONGROUPID"]) != intInvoiceTransGroupId_Temp)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Không được phép lập hóa đơn khác nhóm nghiệp vụ hóa đơn!");
                        return false;
                    }
                }
                intCustomerId_Temp = Convert.ToInt32(rowSelect["CUSTOMERID"]);
                intInvoiceTransGroupId_Temp = Convert.ToInt32(rowSelect["TRANSACTIONGROUPID"]);
                intStoreID_Temp = Convert.ToInt32(rowSelect["STOREID"]);
                strPriceProductIDList += "<" + rowSelect["PRICEPROTECTID"].ToString().Trim() + ">";
                strPriceProtectNoList += rowSelect["PRICEPROTECTNO"].ToString().Trim() + ";";
            }
            intTransactionGroupId = intInvoiceTransGroupId_Temp;
            intCustomerID = intCustomerId_Temp;
            intStoreID = intStoreID_Temp;
            strPriceProtectNoList = strPriceProtectNoList.Substring(0, strPriceProtectNoList.Length - 1);
            return true;
        }

        private bool ValidateSelected(ref string strPriceProductIDList, ref string strPriceProtectNoList, ref int intTransactionGroupId, ref int intCustomerID, ref int intStoreID)
        {
            DataRow rowSelect = grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
            if (rowSelect != null)
            {
                if (Convert.ToInt32(rowSelect["STATUS"].ToString()) == 0)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Phiếu hỗ trợ số " + rowSelect["PRICEPROTECTID"].ToString() + " chưa có số biên bản!");
                    return false;
                }

                if (Convert.ToInt32(rowSelect["ISINVOICE"].ToString()) == 1)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Phiếu hỗ trợ số " + rowSelect["PRICEPROTECTID"].ToString() + " đã được lập hóa đơn!");
                    return false;
                }
            }

            strPriceProductIDList = "<" + rowSelect["PRICEPROTECTID"].ToString().Trim() + ">";
            strPriceProtectNoList = rowSelect["PRICEPROTECTNO"].ToString();
            intStoreID = Convert.ToInt32(rowSelect["STOREID"]);
            intTransactionGroupId = Convert.ToInt32(rowSelect["TRANSACTIONGROUPID"]);
            intCustomerID = Convert.ToInt32(rowSelect["CUSTOMERID"]);
            return true;
        }
        private void mnuCreateItem_Click(object sender, EventArgs e)
        {
            if (grdViewData.RowCount == 0)
                return;

            if (objSelection.SelectedCount == 0 && grdViewData.FocusedRowHandle < 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn phiếu để lập hóa đơn!");
                return;
            }

            string strPriceProtectIDList = string.Empty;
            string strPriceProtectNoList = string.Empty;
            int intTransactionGroupId = -1;
            int intCustomerID = -1;
            int intStoreID = -1;

            if (objSelection.SelectedCount > 0)
            {
                if (!ValidateMultiSelect(ref strPriceProtectIDList, ref strPriceProtectNoList, ref intTransactionGroupId, ref intCustomerID, ref intStoreID))
                    return;
            }
            else
            {
                if (!ValidateSelected(ref strPriceProtectIDList, ref strPriceProtectNoList, ref intTransactionGroupId, ref intCustomerID, ref intStoreID))
                    return;
            }

            object[] objKeywords = new object[]
           {
                "@PRICEPROTECTIDLIST", strPriceProtectIDList
           };

            DataTable dtbData = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_PRICEPROTECT_SELVALUE", objKeywords);
            if (dtbData != null && dtbData.Rows.Count > 0)
            {
                ERP.PrintVAT.DUI.InputPrintVAT.frmVATPinvoicePriceProtect frm = new PrintVAT.DUI.InputPrintVAT.frmVATPinvoicePriceProtect();
                frm.Tag = "PRODUCTID=" + strProductID;
                frm.PriceProtectIdList = strPriceProtectIDList;
                frm.PriceProtectNo = strPriceProtectNoList;
                frm.CustomerID = intCustomerID;
                frm.StoreIDSelect = intStoreID;
                frm.TransactionGroupId = (intTransactionGroupId == 4 ? 10 : 3);
                frm.InitializeDataProductService(dtbData, strProductID, true);
                frm.ShowDialog();
                if (frm.bolRefresh)
                    LoadData();
            }
            else
            {
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy dữ liệu để lập hóa đơn!");
            }
        }


        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (!btnExportExcel.Enabled)
                return;
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }

        private void grdData_Click(object sender, EventArgs e)
        {
            GridHitInfo info;
            Point pt = grdViewData.GridControl.PointToClient(Control.MousePosition);
            info = grdViewData.CalcHitInfo(pt);
            if (info.Column == objSelection.CheckMarkColumn)
            {
                if (info.InRowCell)
                {
                    if (info.RowHandle == DevExpress.XtraGrid.GridControl.AutoFilterRowHandle)
                        return;
                    DataRow drCheck = grdViewData.GetDataRow(info.RowHandle);
                    string strCustomerIDChecked = (drCheck["CUSTOMERID"] ?? "").ToString();
                    for (int i = 0; i < objSelection.SelectedCount; i++)
                    {
                        var dr = objSelection.GetSelectedRow(i) as DataRowView;
                        string strCustomerID = (dr["CUSTOMERID"] ?? "").ToString();
                        if (strCustomerID != strCustomerIDChecked)
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Không được phép chọn khác đối tượng nhà cung cấp!");
                            objSelection.SelectRow(info.RowHandle, false);
                            return;
                        }
                    }
                }
            }
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            if (!mnuItemExportExcel.Enabled)
                return;
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }

        private void txtKeywords_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSearch_Click(null, null);
        }


        private void chkSearchByVoucherDate_CheckedChanged(object sender, EventArgs e)
        {
            dteVoucherFromDate.Enabled = dteVoucherToDate.Enabled = chkSearchByVoucherDate.Checked;
        }

        private void chkSearchByInputDate_CheckedChanged(object sender, EventArgs e)
        {
            dteInputFromDate.Enabled = dteInputToDate.Enabled = chkSearchByInputDate.Checked;
        }

        private void chkSearchByStockDate_CheckedChanged(object sender, EventArgs e)
        {
            dteStockFromDate.Enabled = dteStockToDate.Enabled = chkSearchByStockDate.Checked;
        }

        private void chkSearchByProtectDate_CheckedChanged(object sender, EventArgs e)
        {
            dteProtectFromDate.Enabled = dteProtectToDate.Enabled = chkSearchByProtectDate.Checked;
        }

        private void chkSearchByPostDate_CheckedChanged(object sender, EventArgs e)
        {
            dtePostFromDate.Enabled = dtePostToDate.Enabled = chkSearchByPostDate.Checked;
        }
    }
}
