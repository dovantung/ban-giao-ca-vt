﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using DevExpress.XtraTreeList;
using System.Diagnostics;

namespace ERP.Inventory.DUI.PM
{
    public partial class frmLotIMEISaleSInfo_CommentManager : Form
    {
        #region Variable
        
        private string strPermission_Delete = "PM_LOTIMEISALESINFO_COMMENT_DELETE";
        private string strPermission_Edit = "PM_LOTIMEISALESINFO_COMMENT_EDIT";
        private string strPermission_Add = "PM_LOTIMEISALESINFO_COMMENT_ADD";
        private string strLotIMEISalesInfoID = string.Empty;
        private string strLotIMEISalesInfoName = string.Empty;
        private PLC.PM.PLCLotIMEISalesInfo_Comment objPLCLotIMEISalesInfo_Comment = new PLC.PM.PLCLotIMEISalesInfo_Comment();
        private DataTable dtbSource = null;
        private DataTable dtbDestination = null;
        private int intIDFocus = -1;
        private bool bolIsPermission_Add = false;
        private bool bolIsPermission_Edit = false;
        //private bool bolIsPermission_Add = false;
        #endregion
        #region Properties
        public string LotIMEISalesInfoName
        {
            //get { return strLotIMEISalesInfoName; }
            set { strLotIMEISalesInfoName = value; }
        }
        public string LotIMEISalesInfoID
        {
            //get { return strLotIMEISalesInfoID; }
            set { strLotIMEISalesInfoID = value; }
        }
        #endregion
        #region Constructor
        public frmLotIMEISaleSInfo_CommentManager()
        {
            InitializeComponent();
        }
        #endregion
        #region Method
        /// <summary>
        /// FocusRow
        /// </summary>
        /// <param name="intID"></param>
        private void FocusRow(int intID)
        {
            try
            {
                if (treeDepartmentFunction.AllNodesCount > 0)
                {
                    TreeListNode objTreeListNode = treeDepartmentFunction.FindNodeByFieldValue("COMMENTID", "intID");
                    if (objTreeListNode != null)
                    {
                        treeDepartmentFunction.SetFocusedNode(objTreeListNode);
                    }
                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi focus trên lưới");
                SystemErrorWS.Insert("Lỗi focus trên lưới", objExce.ToString(), this.Name + " -> FocusRow", DUIInventory_Globals.ModuleName);
                return;
            }
        }

        private void LoadData()
        {
            try
            {
                dtbSource = null;
                object[] objKeywords = new object[] { "@LOTIMEISALESINFOID", strLotIMEISalesInfoID };
                dtbSource = objPLCLotIMEISalesInfo_Comment.SearchData(objKeywords);
                if (dtbSource == null)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }

                dtbDestination = dtbSource.Clone();
                for (int i = 0; i < dtbSource.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dtbSource.Rows[i]["REPLYTOCOMMENTID"]) < 1)
                    {
                        DataRow row = dtbSource.Rows[i];
                        int intCommentID = Convert.ToInt32(row["CommentID"]);
                        dtbDestination.ImportRow(row);
                        DataRow[] lstRow = dtbSource.Select("REPLYTOCOMMENTID = " + intCommentID);
                        if (lstRow != null && lstRow.Length > 0)
                        {
                            for (int j = 0; j < lstRow.Length; j++)
                            {
                                dtbDestination.ImportRow(lstRow[j]);
                            }
                        }
                    }
                }
                if (dtbDestination == null || dtbDestination.Rows.Count < 1)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Lô IMEI chưa có thông tin bình luận");
                    this.Close();
                }
                treeDepartmentFunction.DataSource = dtbDestination;
                treeDepartmentFunction.ExpandAll();
            }
            catch (Exception ex)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp danh sách bình luận của lô IMEI!");
                SystemErrorWS.Insert("Lỗi nạp danh sách bình luận của lô IMEI!", ex.ToString(), this.Name + " -> RepairData", DUIInventory_Globals.ModuleName);
            }
        }

        private void EditData()
        {
            try
            {
                DevExpress.XtraTreeList.Nodes.TreeListNode objTreeListNode = treeDepartmentFunction.FocusedNode;
                if (objTreeListNode == null)
                    return;

                PLC.PM.WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment = new PLC.PM.WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment();
                objLotIMEISalesInfo_Comment.CommentID = Convert.ToDecimal(objTreeListNode.GetValue(colCommentID) == DBNull.Value ? "0" : objTreeListNode.GetValue(colCommentID));
                objLotIMEISalesInfo_Comment.ReplyToCommentID = Convert.ToDecimal(objTreeListNode.GetValue(colReplyToCommentID) == DBNull.Value ? "0" : objTreeListNode.GetValue(colReplyToCommentID));
                if (objTreeListNode.GetValue(colCommentDate) != DBNull.Value)
                    objLotIMEISalesInfo_Comment.CommentDate = Convert.ToDateTime(objTreeListNode.GetValue(colCommentDate));
                objLotIMEISalesInfo_Comment.CommentTitle = Convert.ToString(objTreeListNode.GetValue(colCommentTitle));
                objLotIMEISalesInfo_Comment.CommentContent = Convert.ToString(objTreeListNode.GetValue(colCommentContent));
                objLotIMEISalesInfo_Comment.CommentUser = Convert.ToString(objTreeListNode.GetValue(colCommentUser));
                objLotIMEISalesInfo_Comment.CommentEmail = Convert.ToString(objTreeListNode.GetValue(colCommentEmail));
                objLotIMEISalesInfo_Comment.PhoneNumber = Convert.ToString(objTreeListNode.GetValue(colPhoneNumber));
                objLotIMEISalesInfo_Comment.ModelRate = Convert.ToInt32(objTreeListNode.GetValue(colModelRate) == DBNull.Value ? "0" : objTreeListNode.GetValue(colModelRate));
                objLotIMEISalesInfo_Comment.IsActive = Convert.ToBoolean(objTreeListNode.GetValue(colIsActive) == DBNull.Value ? "0" : objTreeListNode.GetValue(colIsActive));
                objLotIMEISalesInfo_Comment.IsSystem = Convert.ToBoolean(objTreeListNode.GetValue(colIsSystem) == DBNull.Value ? "0" : objTreeListNode.GetValue(colIsSystem));
                objLotIMEISalesInfo_Comment.LotIMEISalesInfoID = Convert.ToString(objTreeListNode.GetValue(colLotIMEISalesInfoID));
                frmLotIMEISaleSInfo_Comment frm = new frmLotIMEISaleSInfo_Comment();
                frm.LotIMEISalesInfo_Comment = objLotIMEISalesInfo_Comment;
                intIDFocus = (int)objLotIMEISalesInfo_Comment.CommentID;
                frm.IsPermission_Add = bolIsPermission_Add;
                frm.IsPermission_Edit = bolIsPermission_Edit;
                frm.ShowDialog();
                if (frm.IsUpdated)
                {
                    MessageBox.Show(this, "Cập nhật bình luận lô IMEI thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadData();
                }
                this.FocusRow(this.intIDFocus);
            }
            catch (Exception objExc)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi chỉnh sửa bình luận lô IMEI!", objExc.ToString());
                SystemErrorWS.Insert("Lỗi chỉnh sửa bình luận lô IMEI!", objExc.ToString(), this.Name + " -> EditData", DUIInventory_Globals.ModuleName);
            }
        }

        private void DeleteData()
        {
            try
            {
                treeDepartmentFunction.PostEditor();
                DataTable dtbData = (DataTable)treeDepartmentFunction.DataSource;
                List<PLC.PM.WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment> lstLotIMEISalesInfo_Comment = new List<PLC.PM.WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment>();
                if (dtbData != null && dtbData.Rows.Count > 0)
                {
                    for (int i = 0; i < dtbData.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(dtbData.Rows[i]["ISSELECT"]))
                        {
                            PLC.PM.WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment = new PLC.PM.WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment();
                            objLotIMEISalesInfo_Comment.CommentID = Convert.ToInt32(dtbData.Rows[i]["COMMENTID"]);
                            objLotIMEISalesInfo_Comment.IsSystem = Convert.ToBoolean(dtbData.Rows[i]["ISSYSTEM"]);
                            objLotIMEISalesInfo_Comment.DeletedUser = SystemConfig.objSessionUser.UserName;
                            lstLotIMEISalesInfo_Comment.Add(objLotIMEISalesInfo_Comment);
                        }
                    }
                }
                if (lstLotIMEISalesInfo_Comment.Count == 0)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn dòng cần xóa");
                    return;
                }
                PLC.PM.WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment objResult = lstLotIMEISalesInfo_Comment.Find(item => item.IsSystem == true);
                if (objResult != null)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Bình luận " + objResult.CommentID + " là thuộc tính hệ thống");
                    return;
                }

                if (MessageBox.Show(this, "Bạn có chắc muốn xóa bình luận lô IMEI đang chọn không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                    return;
                bool bolResult = objPLCLotIMEISalesInfo_Comment.DeleteList(lstLotIMEISalesInfo_Comment);
                if (!bolResult)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                            SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                LoadData();
            }
            catch (Exception objExc)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi xóa bình luận lô IMEI!", objExc.ToString());
                SystemErrorWS.Insert("Lỗi xóa bình luận lô IMEI!", objExc.ToString(), this.Name + " -> DeleteData", DUIInventory_Globals.ModuleName);
            }
        }

        public void ExcelExport(TreeList trList)
        {
            using (var dialog = new SaveFileDialog())
            {
                dialog.Title = "Excel Export";
                dialog.Filter = "Excel2003 Excel|*.xls|Excel2007 Excel|*.xlsx";
                dialog.FileName = string.Format("Export_{0}", DateTime.Now.ToString("dd_MM_yyyy__HH_mm_ss"));
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    trList.OptionsPrint.AutoWidth = false;
                    trList.ExportToXlsx(dialog.FileName);
                    //Process.Start(dialog.FileName);
                }
            }
        }
        #endregion
        #region Event
        private void frmLotIMEISaleSInfo_CommentManager_Load(object sender, EventArgs e)
        {
            this.Text = "Quản lý bình luận thông tin bán hàng của lô IMEI " + strLotIMEISalesInfoID + " - " + strLotIMEISalesInfoName;
            mnuItemDel.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Delete);
            bolIsPermission_Edit = SystemConfig.objSessionUser.IsPermission(strPermission_Edit);
            bolIsPermission_Add = SystemConfig.objSessionUser.IsPermission(strPermission_Add);
            mnuItemEdit.Enabled = bolIsPermission_Edit || bolIsPermission_Add;
            if (!string.IsNullOrEmpty(strLotIMEISalesInfoID))
            {
                LoadData();
            }
        }
        
        private void mnuItemEdit_Click(object sender, EventArgs e)
        {
            EditData();
        }

        private void mnuItemDel_Click(object sender, EventArgs e)
        {
            DeleteData();
        }

        private void repTreeDepartment_IsSelect_CheckedChanged(object sender, EventArgs e)
        {
            Cursor curent = Cursor.Current;
            this.Cursor = Cursors.WaitCursor;
            CheckEdit chkIsSelect = (CheckEdit)sender;
            if (!treeDepartmentFunction.FocusedNode.HasChildren)
            {
                DevExpress.XtraTreeList.Nodes.TreeListNode fatherNode = treeDepartmentFunction.FocusedNode.ParentNode;
                if (fatherNode != null)
                {
                    if (Convert.ToBoolean(fatherNode.GetValue(colTreeDepartment_IsSelect)))
                        chkIsSelect.Checked = true;
                }
            }
            else
            {
                repTreeDepartment_Select_CheckedChanged(chkIsSelect.Checked, treeDepartmentFunction.FocusedNode);
            }
            //
            //bolIsEditUserDepartment = true;
            this.Cursor = curent;
        }

        private void repTreeDepartment_Select_CheckedChanged(bool bolSelect, DevExpress.XtraTreeList.Nodes.TreeListNode treeListNode)
        {
            try
            {
                treeListNode.SetValue(colTreeDepartment_IsSelect, bolSelect);
                if (treeListNode.HasChildren)
                {
                    for (int i = 0; i < treeListNode.Nodes.Count; i++)
                    {
                        DevExpress.XtraTreeList.Nodes.TreeListNode childNode = treeListNode.Nodes[i];
                        repTreeDepartment_Select_CheckedChanged(bolSelect, childNode);
                    }
                }
            }
            catch (Exception objExc)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi kiểm tra bình luận con", objExc.ToString());
                SystemErrorWS.Insert("Lỗi kiểm tra bình luận con", objExc.ToString(), this.Name + " -> repTreeDepartment_Select_CheckedChanged", DUIInventory_Globals.ModuleName);
            }
        }

        #endregion

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            ExcelExport(treeDepartmentFunction);
        }

        private void treeDepartmentFunction_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Control && e.KeyCode == Keys.C)
                {
                    TreeList treeList = (TreeList)sender;
                    Clipboard.SetText(treeList.FocusedNode.GetDisplayText(treeList.FocusedColumn));
                    e.Handled = true;
                }
            }
            catch
            {
            }
        }
    }
}
