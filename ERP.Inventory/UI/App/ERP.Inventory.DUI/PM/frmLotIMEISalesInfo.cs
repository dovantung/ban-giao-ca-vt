﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using ERP.Inventory.PLC.PM;
using ERP.Inventory.PLC.PM.WSLotIMEISalesInfo;
using Library.AppCore.LoadControls;
using System.IO;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
namespace ERP.Inventory.DUI.PM
{
    /// <summary>
    /// Created by  :   Cao Hũu Vũ Lam
    /// Description :   Quản lý thông tin lô IMEI
    /// </summary>
    public partial class frmLotIMEISalesInfo : Form
    {
        #region Khai báo biến
        private string strPermission_Add = "MD_LotIMEISalesInfo_Add";
        private string strPermission_Delete = "MD_LotIMEISalesInfo_Delete";
        private string strPermission_Edit = "MD_LotIMEISalesInfo_Edit";
        private string strPermission_ExportExcel = "MD_LotIMEISalesInfo_ExportExcel";
        private string strPermission_Review = "MD_LotIMEISalesInfo_Review";
        private string strPermission_UnReview = "MD_LotIMEISalesInfo_UnReview";
        private string strPermission_ViewComment = "PM_LOTIMEISALESINFO_COMMENT_VIEW";

        private bool bolPermission_ViewComment = false;
        private bool bolPermission_Add = false;
        private bool bolPermission_Delete = false;
        private bool bolPermission_Edit = false;
        private bool bolPermission_ExportExcel = false;
        private bool bolPermission_Review = false;
        private bool bolPermission_UnReview = false;
        private bool bolIsReview = false;
        private DateTime dtEndWarantyDate = DateTime.Now;
        private string strReviewedUser = string.Empty;
        private DateTime? dteReviewedDate = null;

        private DataTable dtbIMEI = null;
        private PLCLotIMEISalesInfo objPLCLotIMEISalesInfo = new PLCLotIMEISalesInfo();
        private List<LotIMEISalesInfo_Images> objProduct_ImagesList = new List<LotIMEISalesInfo_Images>();
        private List<LotIMEISalesInfo_Images> objProduct_ImagesList_Del = new List<LotIMEISalesInfo_Images>();
        #endregion

        #region Constructor
        public frmLotIMEISalesInfo()
        {
            InitializeComponent();
        }
        #endregion

        #region Các hàm sự kiện
        /// <summary>
        /// Hàm thay đổi focus khi nhấn Enter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeFocus(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                this.SelectNextControl(this.ActiveControl, true, true, true, true);
        }

        private void frmLotIMEISalesInfo_Load(object sender, EventArgs e)
        {
            KeyPreview = true;   
            InitControl();
            LoadPermission();

            if (SystemConfig.objSessionUser.UserName != "administrator")
            {
                chkIsSystem.Enabled = false;
            }
            FormatGrid();
            repChkIMEI_IsSytem.EditValueChanged += OnEditValueChanged;
            FormState = FormStateType.LIST;
        }
        void OnEditValueChanged(object sender, EventArgs e)
        {
            grvIMEI.PostEditor();
        }
        private void LoadPermission()
        {
            bolPermission_Add = SystemConfig.objSessionUser.IsPermission(strPermission_Add);
            bolPermission_Delete = SystemConfig.objSessionUser.IsPermission(strPermission_Delete);
            bolPermission_Edit = SystemConfig.objSessionUser.IsPermission(strPermission_Edit);
            bolPermission_ExportExcel = SystemConfig.objSessionUser.IsPermission(strPermission_ExportExcel);
            bolPermission_Review = SystemConfig.objSessionUser.IsPermission(strPermission_Review);
            bolPermission_UnReview = SystemConfig.objSessionUser.IsPermission(strPermission_UnReview);
            bolPermission_ViewComment = SystemConfig.objSessionUser.IsPermission(strPermission_ViewComment);
            mnuItemViewComment.Enabled = bolPermission_ViewComment;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!btnAdd.Enabled)
                return;
            ClearValueControl();
            FormState = FormStateType.ADD;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (!btnEdit.Enabled)
                return;
            if (grdViewData.FocusedRowHandle < 0)
                return;
            if (!EditData())
                return;
            FormState = FormStateType.EDIT;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (!btnDelete.Enabled)
                return;
            //if (grdViewData.FocusedRowHandle < 0)
            //{
            //    MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn thông tin bán hàng của lô IMEI để xóa!");
            //    return;
            //}
            //DataRow row = (DataRow)grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
            //if (Convert.ToBoolean(row["IsSystem"]))
            //{
            //    MessageBoxObject.ShowWarningMessage(this, "Không được xóa thông tin bán hàng của lô IMEI có thuộc tính hệ thống!");
            //    return;
            //}
            //if (MessageBox.Show(this, "Bạn có chắc muốn xóa thông tin bán hàng của lô IMEI đang chọn không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
            //    return;
            //if (!DeleteData())
            //    return;
            //LoadData();

            grdData.RefreshDataSource();
            DataTable dtbResource = (DataTable)grdData.DataSource;
            dtbResource.AcceptChanges();
            DataRow[] rows = dtbResource.Select("ISSELECT = 1");
            if (rows == null || rows.Length == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn thông tin cần xóa!");
                return;
            }
            for (int i = 0; i < dtbResource.Rows.Count; i++)
			{
                if (Convert.ToBoolean(dtbResource.Rows[i]["ISSELECT"]))
                {
                    if ( Convert.ToBoolean(dtbResource.Rows[i]["IsSystem"]))
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Không được xóa thông tin có thuộc tính hệ thống!");
                        grdViewData.FocusedRowHandle = i;
                        return;
                    }
                    if (Convert.ToBoolean(dtbResource.Rows[i]["IsReviewed"]))
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Không được xóa thông tin đã duyệt!");
                        grdViewData.FocusedRowHandle = i;
                        return;
                    }
                   
                }
			}
            if (!DeleteData())
            {
                return;
            }
            btnSearch_Click(null, null);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!btnUpdate.Enabled)
                return;
            if (!ValidateData())
                return;
            if (!UpdateData())
                return;
            LoadData();
            FormState = FormStateType.LIST;
        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            btnEdit_Click(null, null);
        }

        private void chkIsReviewed_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsReviewed.Checked && !chkIsActive.Checked)
            {
                MessageBoxObject.ShowWarningMessage(this, "Thông tin phải được kích hoạt trước khi duyệt!");
                chkIsReviewed.Checked = false;
            }
            if (intFormState == FormStateType.ADD) return;
            EnableControl(bolPermission_Edit && !bolIsReview && bolPermission_Review && !chkIsReviewed.Checked && !chkIsSystem.Checked); 
        }

        private void chkIsSystem_CheckedChanged(object sender, EventArgs e)
        {   
            if (intFormState == FormStateType.ADD) return;
            EnableControl(bolPermission_Edit && !bolIsReview && bolPermission_Review && !chkIsReviewed.Checked && !chkIsSystem.Checked); 
        }        
        private void mnuItemAdd_Click(object sender, EventArgs e)
        {
            btnAdd_Click(null, null);
        }

        private void mnuItemDelete_Click(object sender, EventArgs e)
        {
            btnDelete_Click(null, null);
        }

        private void mnuItemEdit_Click(object sender, EventArgs e)
        {
            btnEdit_Click(null, null);
        }

        private void mnuItemExport_Click(object sender, EventArgs e)
        {
            btnExportExcel_Click(null, null);
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }
        private void frmLotIMEISalesInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(this, "Bạn có chắc muốn đóng màn hình này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                e.Cancel = true;
        }
        private void frmLotIMEISalesInfo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.A && e.Modifiers == Keys.Control)
            {
                mnuItemAdd_Click(null, null);
            }
            else if (e.KeyCode == Keys.D && e.Modifiers == Keys.Control)
            {
                mnuItemDelete_Click(null, null);
            }
            else if (e.KeyCode == Keys.E && e.Modifiers == Keys.Control)
            {
                mnuItemEdit_Click(null, null);
            }
            else if (e.KeyCode == Keys.S && e.Modifiers == Keys.Control)
            {
                mnuItemExport_Click(null, null);
            }
        }
        private void btnUndo_Click(object sender, EventArgs e)
        {
            if (grvIMEI.Columns["IsOutput"] != null)
                grvIMEI.Columns["IsOutput"].FilterInfo = new ColumnFilterInfo();
            FormState = FormStateType.LIST;
        }
       
        private void txtLotIMEISalesInfoID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void tabDetail_Enter(object sender, EventArgs e)
        {
            if (FormState == FormStateType.LIST)
            {
                tabControl.SelectedTab = tabList;
            }
        }

        private void tabList_Enter(object sender, EventArgs e)
        {
            if (FormState == FormStateType.ADD || FormState == FormStateType.EDIT)
            {
                tabControl.SelectedTab = tabDetail;
            }
        }

        private void txtLotIMEISalesInfoID_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control == true & e.KeyCode == Keys.A)
            {
                txtLotIMEISalesInfoID.SelectAll();
            }

        }

        private bool ValidateSearch()
        {
            if (!ERP.MasterData.DUI.Common.CommonFunction.EndDateValidation(dtpFromDate.Value, dtpToDate.Value, false))
            {
                dtpToDate.Focus();
                MessageBox.Show(this, "Đến ngày không thể nhỏ hơn từ ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            DateTime dtFromDate = dtpFromDate.Value;
            dtFromDate = dtFromDate.AddMonths(3);
            DateTime dtToDate = dtpToDate.Value;
            if (dtFromDate.CompareTo(dtToDate) < 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chỉ xem báo cáo trong vòng 3 tháng!");
                dtpToDate.Focus();
                return false;
            }
            return true;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!ValidateSearch())
            {
                return;
            }
            LoadData();
            FormState = FormStateType.LIST;
        }

        private void txtKeyWords_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && btnSearch.Enabled)
            {
                btnSearch_Click(null, null);
            }
        }

        private void chkIsBrandNewWarranty_Click(object sender, EventArgs e)
        {
            dteEndWarantyDate.Enabled = chkIsBrandNewWarranty.Checked;
        }
        #endregion

        #region Các hàm hỗ trợ
        /// <summary>
        /// Khởi tạo cho các control
        /// Xét các thuộc tính
        /// </summary>
        private void InitControl()
        {
            txtLotIMEISalesInfoName.MaxLength = 200;
            txtDescription.MaxLength = 2000;

            txtLotIMEISalesInfoID.KeyPress += new KeyPressEventHandler(ChangeFocus);
            txtLotIMEISalesInfoName.KeyPress += new KeyPressEventHandler(ChangeFocus);

            string[] fieldNames = new string[] { "ISSELECT", "LOTIMEISALESINFOID", "LOTIMEISALESINFONAME", "PRODUCTID", "PRODUCTNAME", "DESCRIPTION", "ISBRANDNEWWARRANTY", "ENDWARRANTYDATE", "ISCONFIRM", "ISREVIEWED", "REVIEWEDUSER", "REVIEWEDDATE", "TOTALCOMMENT", "ISACTIVE", "ISSYSTEM", "CREATEDUSER", "CREATEDDATE", "UPDATEDUSER", "UPDATEDDATE" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvIMEI);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvProductSpec);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvImages);
            LoadDataForRepCombo();
        }
        /// <summary>
        /// Ẩn hiện control
        /// </summary>
        /// <param name="bolValue">true: hiện; false: ẩn</param>
        private void EnableControl(bool bolValue)
        {            
            txtLotIMEISalesInfoName.ReadOnly = !bolValue;
            btnSelectProduct.Enabled = bolValue;
            txtDescription.ReadOnly = !bolValue;
            txtNote.ReadOnly = !bolValue;
            txtPrintVATContent.ReadOnly = !bolValue;
            chkIsBrandNewWarranty.Enabled = bolValue;
            dteEndWarantyDate.Enabled = chkIsBrandNewWarranty.Checked && bolValue;
            chkIsActive.Enabled = bolValue;
            chkIsComfirm.Enabled = bolValue;
            //chkIsReviewed.Enabled = bolValue;
            if (bolValue)
            {
                txtLotIMEISalesInfoName.BackColor = SystemColors.Window;
                txtDescription.BackColor = SystemColors.Window;
                txtNote.BackColor = SystemColors.Window;
                txtPrintVATContent.BackColor = SystemColors.Window;
            }
            else
            {
                txtLotIMEISalesInfoName.BackColor = SystemColors.Info;
                txtDescription.BackColor = SystemColors.Info;
                txtNote.BackColor = SystemColors.Info;
                txtPrintVATContent.BackColor = SystemColors.Info;
            }
            grvProductSpec.Columns["ProductSpecStatusID"].OptionsColumn.AllowEdit = bolValue;
            grvProductSpec.Columns["Note"].OptionsColumn.AllowEdit = bolValue;
            grvIMEI.Columns["OrderIndex"].OptionsColumn.AllowEdit = bolValue;
            grvIMEI.Columns["IsSystem"].OptionsColumn.AllowEdit = bolValue;
            grvIMEI.Columns["IMEI"].OptionsColumn.AllowEdit = bolValue;
            //foreach (DevExpress.XtraGrid.Columns.GridColumn item in grvIMEI.Columns)
            //{
            //    item.OptionsColumn.AllowEdit = bolValue;
            //}
            btnAddNewImage.Enabled = bolValue;
            btnDelImage.Enabled = bolValue;
            btnEditImage.Enabled = bolValue;
            mnuIMEI.Enabled = bolValue;
            mnuIMAGE.Enabled = bolValue;
            btnMoveUp.Enabled = bolValue;
            btnMoveDown.Enabled = bolValue;
        }

        /// <summary>
        /// Hàm load dữ liệu lên lưới
        /// </summary>
        private bool LoadData()
        {
            DataTable dtbData = null;
            object[] objKeywords = new object[]
            {
                "@KeyWord", txtKeyWords.Text.Trim().ToUpper(),
                "@FromDate", this.dtpFromDate.Value,
                "@ToDate", this.dtpToDate.Value,
                "@ProductIDList", cboProductList.ProductIDList,
                "@IsDeleted", chkIsDelete.Checked
            };
            dtbData = objPLCLotIMEISalesInfo.SearchData(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }
            grdData.DataSource = dtbData;
            return FormatGrid();
        }

        /// <summary>
        /// Hàm load dữ liệu lên lưới
        /// </summary>
        private void SearchData_LotIMEISalesInfo_ProSpec_ToList()
        {
            List<LotIMEISalesInfo_ProSpec> lstLotIMEISalesInfo_ProSpec = null;
            object[] objKeywords = new object[]
            {
                "@ProductID", txtProductID.Text.Trim()
            };
            lstLotIMEISalesInfo_ProSpec = objPLCLotIMEISalesInfo.SearchData_LotIMEISalesInfo_ProSpec_ToList(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
            else
            {
                grdProduct.DataSource = lstLotIMEISalesInfo_ProSpec;
            }
        }

        ///// <summary>
        ///// Hàm load dữ liệu lên lưới
        ///// </summary>
        //private void SearchData_LotIMEISalesInfo_ProSpec_ToList()
        //{
        //    List<LotIMEISalesInfo_ProSpec> lstLotIMEISalesInfo_ProSpec = null;
        //    object[] objKeywords = new object[]
        //    {
        //        "@ProductID", txtProductID.Text.Trim()
        //    };
        //    lstLotIMEISalesInfo_ProSpec = objPLCLotIMEISalesInfo.ch(objKeywords);
        //    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
        //    {
        //        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
        //    }
        //    else
        //    {
        //        grdProduct.DataSource = lstLotIMEISalesInfo_ProSpec;
        //    }
        //}
        //

        /// <summary>
        /// Hàm lấy dữ liệu mặc định các control
        /// </summary>
        private void ClearValueControl()
        {
            tabCtrlDetail.SelectedTab = tabDetailInfo;
            txtLotIMEISalesInfoID.Text = string.Empty;
            txtLotIMEISalesInfoName.Text = string.Empty;
            txtProductID.Text = string.Empty;
            txtProductName.Text = string.Empty;
            txtDescription.Text = string.Empty;
            chkIsBrandNewWarranty.Checked = false;
            dteEndWarantyDate.Value = DateTime.Now;
            txtNote.Text = string.Empty;
            txtPrintVATContent.Text = string.Empty;
            chkIsComfirm.Checked = false;
            chkIsReviewed.Checked = false;
            chkIsActive.Checked = true;
            chkIsSystem.Checked = false;
            //chkIsReviewed_CheckedChanged(null, null);
            //chkIsSystem_CheckedChanged(null, null);
            LotIMEISalesInfo objLotIMEISalesInfo = new LotIMEISalesInfo();
            grdProduct.DataSource = new List<LotIMEISalesInfo_ProSpec>();
            grdIMEI.DataSource = new List<LotIMEISalesInfo_IMEI>();
            if (grvIMEI.Columns["IsOutput"] != null)
                grvIMEI.Columns["IsOutput"].FilterInfo = new ColumnFilterInfo();
            objProduct_ImagesList = new List<LotIMEISalesInfo_Images>();
            grdImages.DataSource = objProduct_ImagesList;
        }

        /// <summary>
        /// Upload File
        /// </summary>
        /// <param name="strFileID"></param>
        /// <param name="strUrlLocal"></param>
        /// <returns>đường dẫn trên server</returns>
        private string SaveAttachment(ref string strFileID, string strUrlLocal)
        {
            //Tạo trạng thái chờ trong khi đính kèm tập tin
            string strURL = string.Empty;
            this.Refresh();
            try
            {
                if (strUrlLocal != string.Empty)
                {
                    FileInfo objFileInfo = new FileInfo(strUrlLocal);
                    if (objFileInfo != null)
                    {
                        ERP.FMS.DUI.DUIFileManager.UploadFile(this, "FMSAplication_ProERP_ProductImage", strUrlLocal, ref strFileID, ref strURL);
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        }
                    }
                }
            }
            catch (Exception objEx)
            {
                MessageBox.Show("Lỗi đính kèm tập tin ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi đính kèm tập tin ", objEx, DUIInventory_Globals.ModuleName);
                return string.Empty;
            }
            //Bỏ trạng thái chờ sau khi đính kèm tập tin			

            return strURL;
        }
        /// <summary>
        /// Hàm cập nhật dữ liệu
        /// Thêm, chỉnh sửa
        /// </summary>
        /// <returns>true or false</returns>
        private bool UpdateData()
        {
            bool bolResult = false;
            string strMessage = string.Empty;
            try
            {
                LotIMEISalesInfo objLotIMEISalesInfo = new LotIMEISalesInfo();
                objLotIMEISalesInfo.LotIMEISalesInfoName = txtLotIMEISalesInfoName.Text.Trim();
                objLotIMEISalesInfo.ProductID = txtProductID.Text.Trim();
                objLotIMEISalesInfo.Description = txtDescription.Text.Trim();
                objLotIMEISalesInfo.Note = txtNote.Text.Trim();
                objLotIMEISalesInfo.PrintVATContent = txtPrintVATContent.Text.Trim();
                objLotIMEISalesInfo.EndWarrantyDate = dteEndWarantyDate.Value;
                objLotIMEISalesInfo.IsBrandNewWarranty = chkIsBrandNewWarranty.Checked;
                objLotIMEISalesInfo.IsConfirm = chkIsComfirm.Checked;
                objLotIMEISalesInfo.IsReviewed = chkIsReviewed.Checked;
                objLotIMEISalesInfo.IsActive = chkIsActive.Checked;
                objLotIMEISalesInfo.IsSystem = chkIsSystem.Checked;
                objLotIMEISalesInfo.ReviewedUser = strReviewedUser;
                objLotIMEISalesInfo.ReviewedDate = dteReviewedDate;
                if (!bolIsReview && objLotIMEISalesInfo.IsReviewed)
                {
                    objLotIMEISalesInfo.ReviewedUser = SystemConfig.objSessionUser.UserName;
                    objLotIMEISalesInfo.ReviewedDate = DateTime.Now;
                }
                else if (bolIsReview && !objLotIMEISalesInfo.IsReviewed)
                {
                    objLotIMEISalesInfo.ReviewedUser = string.Empty;
                    objLotIMEISalesInfo.ReviewedDate = null;
                }

                objLotIMEISalesInfo.LotIMEISalesInfo_ImagesList = ((List<LotIMEISalesInfo_Images>)grdImages.DataSource).ToArray();
                List<LotIMEISalesInfo_IMEI> lstLotIMEISalesInfo_IMEI = (List<LotIMEISalesInfo_IMEI>)grdIMEI.DataSource;
                lstLotIMEISalesInfo_IMEI = lstLotIMEISalesInfo_IMEI.FindAll(p => !string.IsNullOrEmpty(p.IMEI));
                objLotIMEISalesInfo.LotIMEISalesInfo_IMEIList = lstLotIMEISalesInfo_IMEI.ToArray();

                objLotIMEISalesInfo.LotIMEISalesInfo_ProSpecList = ((List<LotIMEISalesInfo_ProSpec>)grdProduct.DataSource).ToArray();
                objLotIMEISalesInfo.WebProductSpecContent = GetWebProductSpecContent(objLotIMEISalesInfo.LotIMEISalesInfo_ProSpecList.ToList());
                if (objProduct_ImagesList != null && objProduct_ImagesList.Count > 0)
                {
                    var NewImages = from img in objProduct_ImagesList
                                    where img.IsNew == true
                                    select img;
                    if (NewImages != null && NewImages.Count() > 0)
                    {
                        List<LotIMEISalesInfo_Images> objNewList = NewImages.ToList();
                        foreach (var item in objNewList)
                        {
                            string strLargesizeImageFileID = string.Empty;
                            item.LargeSizeImage = SaveAttachment(ref strLargesizeImageFileID, item.LargesizeImage_Local);
                            item.LargeSizeImageFileID = strLargesizeImageFileID;
                            string strMediumSizeImageFileID = string.Empty;
                            item.MediumSizeImage = SaveAttachment(ref strMediumSizeImageFileID, item.MediumSizeImage_Local);
                            item.MediumSizeImageFileID = strMediumSizeImageFileID;
                            string strSmallSizeImageFileID = string.Empty;
                            item.SmallSizeImage = SaveAttachment(ref strSmallSizeImageFileID, item.SmallSizeImage_Local);
                            item.SmallSizeImageFileID = strSmallSizeImageFileID;
                            item.IsNew = false;
                        }
                    }
                }
                if (FormState == FormStateType.ADD)
                {
                    objLotIMEISalesInfo.CreatedStoreID = SystemConfig.intDefaultStoreID;
                    bolResult = objPLCLotIMEISalesInfo.Insert(objLotIMEISalesInfo);
                    strMessage = "Thêm";
                }
                else if (FormState == FormStateType.EDIT)
                {
                    objLotIMEISalesInfo.LotIMEISalesInfoID = Convert.ToString(txtLotIMEISalesInfoID.Text);
                    bolResult = objPLCLotIMEISalesInfo.Update(objLotIMEISalesInfo);
                    strMessage = "Cập nhật";
                }
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this,SystemConfig.objSessionUser.ResultMessageApp.Message,SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                MessageBox.Show(strMessage + " thông tin bán hàng của lô IMEI thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi " + strMessage.ToLower() + " thông tin bán hàng của lô IMEI");
                SystemErrorWS.Insert("Lỗi " + strMessage.ToLower() + " thông tin bán hàng của lô IMEI", objExce.ToString(), this.Name + " -> UpdateData", DUIInventory_Globals.ModuleName);
            }
            return bolResult;
        }

        private void SelectTextAll(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            TextBox textbox = (TextBox)sender;
            if (e.Control & e.KeyCode == Keys.A)
                textbox.SelectAll();
        }

        /// <summary>
        /// Hàm kiểm tra dữ liệu
        /// </summary>
        /// <returns></returns>
        private bool ValidateData()
        {
            if (txtLotIMEISalesInfoName.Text.Trim() == string.Empty)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập tên thông tin bán hàng của lô IMEI!");
                tabCtrlDetail.SelectedTab = tabDetailInfo;
                txtLotIMEISalesInfoName.Focus();
                return false;
            }
            if (txtProductID.Text.Trim() == string.Empty)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn sản phẩm!");
                tabCtrlDetail.SelectedTab = tabDetailInfo;
                txtLotIMEISalesInfoName.Focus();
                return false;
            }
            //Hủy duyệt và không thay đổi ngày hết hạn bảo hành thì không check
            if (bolIsReview && !chkIsReviewed.Checked  && dtEndWarantyDate == dteEndWarantyDate.Value)
            {
                return true;
            }
            if (chkIsBrandNewWarranty.Checked)
            {
                if (dteEndWarantyDate.Value.CompareTo(DateTime.Now.Date) < 0)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày hết hạn bảo hành không thể nhỏ hơn ngày hiện tại !");
                    tabCtrlDetail.SelectedTab = tabDetailInfo;
                    dteEndWarantyDate.Focus();
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// Hàm xóa dữ liệu
        /// </summary>
        /// <returns>true or false</returns>
        //private bool DeleteData()
        //{
        //    bool bolResult = false;
        //    try
        //    {
        //        DataRow row = (DataRow)grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
        //        LotIMEISalesInfo objLotIMEISalesInfo = new LotIMEISalesInfo();
        //        objLotIMEISalesInfo.LotIMEISalesInfoID = Convert.ToString(row["LOTIMEISALESINFOID"]);
        //        bolResult = objPLCLotIMEISalesInfo.Delete(objLotIMEISalesInfo);
        //        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
        //        {
        //            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
        //        }
        //    }
        //    catch (Exception objExce)
        //    {
        //        MessageBoxObject.ShowWarningMessage(this, "Lỗi xóa thông tin thông tin bán hàng của lô IMEI");
        //        SystemErrorWS.Insert("Lỗi xóa thông tin thông tin bán hàng của lô IMEI", objExce.ToString(), this.Name + " -> EditData", DUIInventory_Globals.ModuleName);
        //        return false;
        //    }
        //    return bolResult;
        //}

        private bool DeleteData()
        {
            bool bolIsResult = false;
            List<LotIMEISalesInfo> lstLotIMEISalesInfo = new List<LotIMEISalesInfo>();
            foreach (DataRow row in ((DataTable)grdData.DataSource).Rows)
            {
                if (Convert.ToBoolean(row["IsSelect"]))
                {
                    LotIMEISalesInfo objIMEISalesInfo = new LotIMEISalesInfo();
                    objIMEISalesInfo.LotIMEISalesInfoID = row["LotIMEISalesInfoID"].ToString().Trim();
                    lstLotIMEISalesInfo.Add(objIMEISalesInfo);
                }
            }
            bolIsResult = objPLCLotIMEISalesInfo.Delete(lstLotIMEISalesInfo);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
            else
            {
                MessageBoxObject.ShowInfoMessage(this, "Xóa thông tin bán hàng của lô IMEI thành công !");
            }
            return bolIsResult;
        }

        /// <summary>
        /// Hàm lấy dữ liệu Chỉnh sửa
        /// </summary>
        private bool EditData()
        {
            tabCtrlDetail.SelectedTab = tabDetailInfo;
            try
            {
                DataRow dtRow = (DataRow)grdViewData.GetFocusedDataRow();
                string strLotIMEISalesInfoID = dtRow["LOTIMEISALESINFOID"].ToString().Trim();
                PLCLotIMEISalesInfo objPLCLotIMEISalesInfo = new PLCLotIMEISalesInfo();
                LotIMEISalesInfo objLotIMEISalesInfo = objPLCLotIMEISalesInfo.LoadInfo(strLotIMEISalesInfoID);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                }
                txtLotIMEISalesInfoID.Text = objLotIMEISalesInfo.LotIMEISalesInfoID;
                txtLotIMEISalesInfoName.Text = objLotIMEISalesInfo.LotIMEISalesInfoName;
                txtProductID.Text = objLotIMEISalesInfo.ProductID;
                txtProductName.Text = objLotIMEISalesInfo.ProductName;
                txtDescription.Text = objLotIMEISalesInfo.Description.Replace("\n", "\r\n");
                chkIsBrandNewWarranty.Checked = objLotIMEISalesInfo.IsBrandNewWarranty;
                chkIsBrandNewWarranty_Click(null, null);
                dteEndWarantyDate.Value = objLotIMEISalesInfo.EndWarrantyDate.Value;
                txtNote.Text = objLotIMEISalesInfo.Note.Replace("\n", "\r\n");
                txtPrintVATContent.Text = objLotIMEISalesInfo.PrintVATContent.Replace("\n", "\r\n");
                chkIsActive.Checked = objLotIMEISalesInfo.IsActive;
                chkIsComfirm.Checked = objLotIMEISalesInfo.IsConfirm;
                chkIsReviewed.Checked = objLotIMEISalesInfo.IsReviewed;
                chkIsSystem.Checked = objLotIMEISalesInfo.IsSystem;
                grdProduct.DataSource = objLotIMEISalesInfo.LotIMEISalesInfo_ProSpecList.ToList();
                grdIMEI.DataSource = objLotIMEISalesInfo.LotIMEISalesInfo_IMEIList.ToList();
                objProduct_ImagesList = objLotIMEISalesInfo.LotIMEISalesInfo_ImagesList.ToList();
                grdImages.DataSource = objProduct_ImagesList;
                LoadIMEI();
                if (grvIMEI.Columns["IsOutput"] != null)
                    grvIMEI.Columns["IsOutput"].FilterInfo = new ColumnFilterInfo("[IsOutput] = true", "Đã xuất = Cheked");
                bolIsReview = chkIsReviewed.Checked;
                dtEndWarantyDate = dteEndWarantyDate.Value;
                strReviewedUser = objLotIMEISalesInfo.ReviewedUser;
                dteReviewedDate = objLotIMEISalesInfo.ReviewedDate;
            }
            catch (Exception objExce)
            {

                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin thông tin bán hàng của lô IMEI");
                SystemErrorWS.Insert("Lỗi nạp thông tin thông tin bán hàng của lô IMEI", objExce.ToString(), this.Name + " -> EditData", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private bool FormatGrid()
        {
            try
            {
                grdViewData.Columns["ISSELECT"].Caption = "Chọn";
                grdViewData.Columns["LOTIMEISALESINFOID"].Caption = "Mã loại thông tin lô IMEI";
                grdViewData.Columns["LOTIMEISALESINFONAME"].Caption = "Tên loại thông tin lô IMEI";
                grdViewData.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
                grdViewData.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
                grdViewData.Columns["DESCRIPTION"].Caption = "Mô tả";
                grdViewData.Columns["ISBRANDNEWWARRANTY"].Caption = "Bảo hành chính hãng";
                grdViewData.Columns["ENDWARRANTYDATE"].Caption = "Ngày hết hạn bảo hành";
                grdViewData.Columns["ISCONFIRM"].Caption = "Xác nhận";
                grdViewData.Columns["ISREVIEWED"].Caption = "Duyệt";
                grdViewData.Columns["REVIEWEDUSER"].Caption = "Người duyệt";
                grdViewData.Columns["REVIEWEDDATE"].Caption = "Ngày duyệt";
                grdViewData.Columns["ISACTIVE"].Caption = "Kích hoạt";
                grdViewData.Columns["ISSYSTEM"].Caption = "Hệ thống";
                grdViewData.Columns["CREATEDUSER"].Caption = "Người tạo";
                grdViewData.Columns["CREATEDDATE"].Caption = "Ngày tạo";
                grdViewData.Columns["UPDATEDUSER"].Caption = "Người cập nhật";
                grdViewData.Columns["UPDATEDDATE"].Caption = "Ngày cập nhật";
                grdViewData.Columns["TOTALCOMMENT"].Caption = "Số bình luận";

                grdViewData.OptionsView.ColumnAutoWidth = false;
                grdViewData.Columns["ISSELECT"].Width = 60;
                grdViewData.Columns["LOTIMEISALESINFOID"].Width = 130;
                grdViewData.Columns["LOTIMEISALESINFONAME"].Width = 200;
                grdViewData.Columns["PRODUCTID"].Width = 100;
                grdViewData.Columns["PRODUCTNAME"].Width = 200;
                grdViewData.Columns["DESCRIPTION"].Width = 100;
                grdViewData.Columns["ISBRANDNEWWARRANTY"].Width = 80;
                grdViewData.Columns["ENDWARRANTYDATE"].Width = 120;
                grdViewData.Columns["ISCONFIRM"].Width = 80;
                grdViewData.Columns["ISREVIEWED"].Width = 80;
                grdViewData.Columns["REVIEWEDUSER"].Width = 220;
                grdViewData.Columns["REVIEWEDDATE"].Width = 120;
                grdViewData.Columns["ISACTIVE"].Width = 80;
                grdViewData.Columns["ISSYSTEM"].Width = 80;
                grdViewData.Columns["CREATEDUSER"].Width = 220;
                grdViewData.Columns["CREATEDDATE"].Width = 120;
                grdViewData.Columns["UPDATEDUSER"].Width = 220;
                grdViewData.Columns["UPDATEDDATE"].Width = 120;
                grdViewData.Columns["TOTALCOMMENT"].Width = 120;

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsSelect.ValueChecked = ((decimal)(1));
                chkCheckBoxIsSelect.ValueUnchecked = ((decimal)(0));
                grdViewData.Columns["ISSELECT"].ColumnEdit = chkCheckBoxIsSelect;
                chkCheckBoxIsSelect.CheckedChanged += new EventHandler(chkCheckBoxIsSelect_CheckedChanged);

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsBrandNewWarranty = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsBrandNewWarranty.ValueChecked = ((short)(1));
                chkCheckBoxIsBrandNewWarranty.ValueUnchecked = ((short)(0));
                grdViewData.Columns["ISBRANDNEWWARRANTY"].ColumnEdit = chkCheckBoxIsBrandNewWarranty;

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsConfirm = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsConfirm.ValueChecked = ((short)(1));
                chkCheckBoxIsConfirm.ValueUnchecked = ((short)(0));
                grdViewData.Columns["ISCONFIRM"].ColumnEdit = chkCheckBoxIsConfirm;

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsReview = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsReview.ValueChecked = ((short)(1));
                chkCheckBoxIsReview.ValueUnchecked = ((short)(0));
                grdViewData.Columns["ISREVIEWED"].ColumnEdit = chkCheckBoxIsReview;

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsActive = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsActive.ValueChecked = ((short)(1));
                chkCheckBoxIsActive.ValueUnchecked = ((short)(0));
                grdViewData.Columns["ISACTIVE"].ColumnEdit = chkCheckBoxIsActive;
                
                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsSystem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsSystem.ValueChecked = ((short)(1));
                chkCheckBoxIsSystem.ValueUnchecked = ((short)(0));
                grdViewData.Columns["ISSYSTEM"].ColumnEdit = chkCheckBoxIsSystem;

                grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewData.OptionsView.ShowAutoFilterRow = true;
                grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewData.OptionsView.ShowFooter = false;
                grdViewData.OptionsView.ShowGroupPanel = false;
                grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewData.ColumnPanelRowHeight = 40;

                grdViewData.Columns["ISSELECT"].OptionsColumn.AllowEdit = true;
                grdViewData.Columns["ISSELECT"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                grdViewData.Columns["LOTIMEISALESINFOID"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                grdViewData.Columns["LOTIMEISALESINFONAME"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách thông tin bán hàng của lô IMEI");
                SystemErrorWS.Insert("Định dạng lưới danh sách thông tin bán hàng của lô IMEI", objExce.ToString(), this.Name + " -> FormatGrid", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void chkCheckBoxIsSelect_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit chkEdit = (DevExpress.XtraEditors.CheckEdit)sender;
            grdViewData.SetFocusedRowCellValue("ISSELECT", Convert.ToDecimal(chkEdit.Checked));
        }

        private List<MasterData.PLC.MD.WSProductSpec_SpecStatus.ProductSpec_SpecStatus> lstProductSpecStatus = null;
        private DataTable dtbDataSpecStatus = null;
        private void LoadDataForRepCombo()
        {
            int intMainGroupID = -1;
            object[] objKeywords = new object[] { "@MainGroupID", intMainGroupID };
            ERP.Inventory.PLC.IMEISales.PLCIMEISalesInfo objPLCIMEISalesInfo = new PLC.IMEISales.PLCIMEISalesInfo();
            dtbDataSpecStatus = objPLCIMEISalesInfo.SearchDataSpecStatus(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            DataRow dtrSpecStatus = dtbDataSpecStatus.NewRow();
            dtrSpecStatus["ProductSpecID"] = -1;
            dtrSpecStatus["ProductSpecStatusID"] = -1;
            dtrSpecStatus["ProductSpecStatusName"] = "--Chọn trạng thái--";
            dtbDataSpecStatus.Rows.Add(dtrSpecStatus);
            dtbDataSpecStatus.DefaultView.Sort = "ProductSpecStatusID asc";
            dtbDataSpecStatus = dtbDataSpecStatus.DefaultView.ToTable();
            lstProductSpecStatus = new List<MasterData.PLC.MD.WSProductSpec_SpecStatus.ProductSpec_SpecStatus>();
            foreach (DataRow row in dtbDataSpecStatus.Rows)
            {
                MasterData.PLC.MD.WSProductSpec_SpecStatus.ProductSpec_SpecStatus objProductSpecStatus = new MasterData.PLC.MD.WSProductSpec_SpecStatus.ProductSpec_SpecStatus();
                objProductSpecStatus.ProductSpecID = Convert.ToInt32(row["ProductSpecID"]); 
                objProductSpecStatus.ProductSpecStatusID = Convert.ToInt32(row["ProductSpecStatusID"]); 
                objProductSpecStatus.ProductSpecStatusName= Convert.ToString(row["ProductSpecStatusName"]);
                lstProductSpecStatus.Add(objProductSpecStatus);
            }
            
            DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboProduct = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            cboProduct.NullText = "--Chọn trạng thái--";
            cboProduct.DisplayMember = "ProductSpecStatusName";
            cboProduct.ValueMember = "ProductSpecStatusID";
            cboProduct.DataSource = lstProductSpecStatus;
            cboProduct.PopupWidth = 400;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col = new DevExpress.XtraEditors.Controls.LookUpColumnInfo();
            col.Caption = "Mã trạng thái";
            col.FieldName = "ProductSpecStatusID";
            cboProduct.Columns.Add(col);
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col1 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo();
            col1.Caption = "Tên trạng thái";
            col1.FieldName = "ProductSpecStatusName";
            cboProduct.Columns.Add(col1);
            cboProduct.Enter += new EventHandler(cboProduct_Enter);
            this.grvProductSpec.Columns["ProductSpecStatusID"].ColumnEdit = cboProduct;
            this.grvProductSpec.Columns["ProductSpecStatusID"].OptionsColumn.AllowEdit = true;
        }

        private void cboProduct_Enter(object sender, EventArgs e)
        {
            try
            {
                if (grvProductSpec.FocusedColumn.FieldName == "ProductSpecStatusID")
                {
                    LotIMEISalesInfo_ProSpec objLotIMEISalesInfo_ProSpec = (LotIMEISalesInfo_ProSpec)grvProductSpec.GetFocusedRow();
                    List<MasterData.PLC.MD.WSProductSpec_SpecStatus.ProductSpec_SpecStatus> lstProductSpecStatusTemp = lstProductSpecStatus.FindAll(p => (p.ProductSpecID == objLotIMEISalesInfo_ProSpec.ProductSpecID || p.ProductSpecStatusID == -1));
                    DevExpress.XtraEditors.LookUpEdit edit = ((DevExpress.XtraEditors.LookUpEdit)(sender));
                    edit.Properties.DataSource = lstProductSpecStatusTemp;
                }
            }
            catch { }
        }

        /// <summary>
        /// Hàm load dữ liệu lên lưới
        /// </summary>
        private void LoadIMEI()
        {
            try
            {
                object[] objKeywords = new object[] 
                {                     
                    "@ProductID", txtProductID.Text.Trim()
                };
                ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
                dtbIMEI = objPLCReportDataSource.GetDataSource("PM_CRINSTOCKDETAIL_GETBYPRO", objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp danh sách IMEI!");
                SystemErrorWS.Insert("Lỗi nạp danh sách IMEI!", objExce.ToString(), this.Name + " -> LoadIMEI", DUIInventory_Globals.ModuleName);
                return;
            }
        }
        #endregion
        
        #region Form Status
        /// <summary>
        /// Các trạng thái của Form
        /// </summary>
        enum FormStateType
        {
            LIST,
            ADD,
            EDIT
        }

        FormStateType intFormState = FormStateType.LIST;
        FormStateType FormState
        {
            set
            {
                intFormState = value;
                if (intFormState == FormStateType.LIST)
                {
                    //Kiểm tra quyền:
                    btnAdd.Enabled = mnuItemAdd.Enabled = bolPermission_Add;
                    btnDelete.Enabled = mnuItemDelete.Enabled = bolPermission_Delete && grdViewData.RowCount > 0 && !chkIsDelete.Checked;
                    btnEdit.Enabled = mnuItemEdit.Enabled = (bolPermission_UnReview || bolPermission_Review || bolPermission_Edit) && grdViewData.RowCount > 0;
                    btnReview.Enabled = mnuItemReview.Enabled = bolPermission_Review  && grdViewData.RowCount > 0 && !chkIsDelete.Checked;
                    btnUndo.Enabled = false;
                    btnUpdate.Enabled = false;
                    btnExportExcel.Enabled = mnuItemExport.Enabled = bolPermission_ExportExcel && grdViewData.RowCount > 0;
                    tabControl.SelectedTab = tabList;
                    grdViewData.ClearColumnsFilter();
                    chkIsSystem.Checked = false;
                }
                else if (intFormState == FormStateType.ADD || intFormState == FormStateType.EDIT)
                {
                    btnAdd.Enabled = false;
                    btnEdit.Enabled = false;
                    btnReview.Enabled = false;
                    btnDelete.Enabled = false;
                    btnExportExcel.Enabled = false;
                    btnUndo.Enabled = true;
                    btnUpdate.Enabled = true;
                    tabControl.SelectedTab = tabDetail;
                    txtLotIMEISalesInfoName.Focus();

                    if (intFormState == FormStateType.ADD)
                    {
                        EnableControl(!chkIsReviewed.Checked && !chkIsSystem.Checked);
                        chkIsReviewed.Enabled = bolPermission_Review;
                    }
                    else
                    {
                        if (chkIsDelete.Checked)
                        {
                             EnableControl(!chkIsReviewed.Checked && !chkIsSystem.Checked);
                              btnUpdate.Enabled = false;
                              EnableControl(false);
                              chkIsSystem.Enabled = false;
                              chkIsReviewed.Enabled = false;
                        }
                        else
                        {
                            chkIsSystem.Enabled = SystemConfig.objSessionUser.UserName == "administrator";
                            chkIsReviewed.Enabled = ((!chkIsReviewed.Checked && bolPermission_Review) || (chkIsReviewed.Checked && bolPermission_UnReview));
                            EnableControl(!chkIsReviewed.Checked && !chkIsSystem.Checked && bolPermission_Edit);
                        }
                    }
                    //grvIMEI.Columns["IsSystem"].OptionsColumn.AllowEdit = chkIsSystem.Enabled;
                }
            }
            get
            {
                return intFormState;
            }
        }

        #endregion                     


        private void btnSelectProduct_Click(object sender, EventArgs e)
        {
            ERP.MasterData.DUI.Search.frmProductSearch frm = ERP.MasterData.DUI.MasterData_Globals.frmProductSearch;
            frm.IsRequestIMEIType = Library.AppCore.Constant.EnumType.IsRequestIMEIType.REQUEST_IMEI;
            frm.ShowDialog();
            if (frm.Product != null)
            {
                this.txtProductID.Text = frm.Product.ProductID.Trim();
                this.txtProductName.Text = frm.Product.ProductName.Trim();
                LoadIMEI();
                SearchData_LotIMEISalesInfo_ProSpec_ToList();
                grdIMEI.DataSource = new List<LotIMEISalesInfo_IMEI>();
                //LoadDataForRepCombo();
            }
        }

        //private void grdIMEI_Click(object sender, EventArgs e)
        //{
        //    if (grvIMEI.FocusedRowHandle < 0)
        //        return;
        //    List<LotIMEISalesInfo_IMEI> lstLotIMEISalesInfo_IMEI = (List<LotIMEISalesInfo_IMEI>)grdIMEI.DataSource;
        //    LotIMEISalesInfo_IMEI obj = (LotIMEISalesInfo_IMEI)grvIMEI.GetRow(grvIMEI.FocusedRowHandle);
        //    if (!string.IsNullOrEmpty(obj.LotIMEISalesInfoID) && obj.IsSystem)
        //    {
        //        MessageBox.Show(this, "Không được xóa hình sản phẩm có thuộc tính hệ thống?", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Question);
        //        return;
        //    }

        //    if (MessageBox.Show(this, "Bạn chắc chắn muốn xóa hình ảnh sản phẩm này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
        //        return;
        //    lstLotIMEISalesInfo_IMEI.Remove(obj);
        //    grdIMEI.RefreshDataSource();
        //}

        private void mnuItemAddIMEI_Click(object sender, EventArgs e)
        {
            if (txtProductID.Text.Trim() == string.Empty)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn sản phẩm!");
                tabCtrlDetail.SelectedTab = tabDetailInfo;
                return;
            }
            int intOrderIndex = 0;
            List<LotIMEISalesInfo_IMEI> lstLotIMEISalesInfo_IMEI = (List<LotIMEISalesInfo_IMEI>)grdIMEI.DataSource;
            if (lstLotIMEISalesInfo_IMEI != null && lstLotIMEISalesInfo_IMEI.Count > 0)
            {
                intOrderIndex = lstLotIMEISalesInfo_IMEI[lstLotIMEISalesInfo_IMEI.Count - 1].OrderIndex;
            }
            LotIMEISalesInfo_IMEI objLotIMEISalesInfo_IMEI = new LotIMEISalesInfo_IMEI();
            objLotIMEISalesInfo_IMEI.OrderIndex = intOrderIndex + 1;
            lstLotIMEISalesInfo_IMEI.Add(objLotIMEISalesInfo_IMEI);
            grdIMEI.RefreshDataSource();
        }

        private void mnuItemChooseIMEI_Click(object sender, EventArgs e)
        {
            if (txtProductID.Text.Trim() == string.Empty)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn sản phẩm!");
                tabCtrlDetail.SelectedTab = tabDetailInfo;
                return;
            }
            frmIMEISearch frm = new frmIMEISearch();
            frm.ProductID = txtProductID.Text.Trim();
            frm.IsMultiSelect = true;
            frm.ShowDialog();
            if (frm.IMEIList != null && frm.IMEIList.Count > 0)
            {
                List<LotIMEISalesInfo_IMEI> lstLotIMEISalesInfo_IMEI = (List<LotIMEISalesInfo_IMEI>)grdIMEI.DataSource;
                int intOrderIndex = 0;
                if (lstLotIMEISalesInfo_IMEI != null && lstLotIMEISalesInfo_IMEI.Count > 0)
                {
                    intOrderIndex = lstLotIMEISalesInfo_IMEI[lstLotIMEISalesInfo_IMEI.Count - 1].OrderIndex;
                }
                foreach (string item in frm.IMEIList)
                {
                    if (!CheckIMEI(item))
                    {
                        LotIMEISalesInfo_IMEI objLotIMEISalesInfo_IMEI = new LotIMEISalesInfo_IMEI();
                        objLotIMEISalesInfo_IMEI.IMEI = item;
                        objLotIMEISalesInfo_IMEI.OrderIndex = intOrderIndex + 1;
                        lstLotIMEISalesInfo_IMEI.Add(objLotIMEISalesInfo_IMEI);
                        intOrderIndex++;
                    }
                }
                grdIMEI.RefreshDataSource();
            }
        }

        private void mnuItemDelIMEI_Click(object sender, EventArgs e)
        {
            //if (grvIMEI.FocusedRowHandle < 0)
            //    return;
            grvIMEI.PostEditor();
            List<LotIMEISalesInfo_IMEI> ListLotIMEISalesInfo_IMEI = new List<LotIMEISalesInfo_IMEI>();
            List<LotIMEISalesInfo_IMEI> lstLotIMEISalesInfo_IMEI = (List<LotIMEISalesInfo_IMEI>)grdIMEI.DataSource;
            var ListIsSelect = from d in lstLotIMEISalesInfo_IMEI where d.IsSelect == true select d;
            if (ListIsSelect == null || ListIsSelect.Count() < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn IMEI cần xóa");
                return;
            }
            //LotIMEISalesInfo_IMEI obj = (LotIMEISalesInfo_IMEI)grvIMEI.GetRow(grvIMEI.FocusedRowHandle);
            //if (obj.IsExist && obj.IsSystem)
            //{
            //    MessageBox.Show(this, "Không được xóa IMEI có thuộc tính hệ thống?", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Question);
            //    return;
            //}

            if (MessageBox.Show(this, "Bạn chắc chắn muốn xóa IMEI này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            //List<LotIMEISalesInfo_IMEI> lstLotIMEISalesInfo_IMEI = (List<LotIMEISalesInfo_IMEI>)grdIMEI.DataSource;
            while (ListIsSelect.Count() > 0)
                lstLotIMEISalesInfo_IMEI.Remove(ListIsSelect.First());
            grdIMEI.RefreshDataSource();
        }   

        private void btnAddNewImage_Click(object sender, EventArgs e)
        {
            frmLotIMEItImage frm = new frmLotIMEItImage();
            frm.ProductID = txtProductID.Text.Trim();
            frm.ShowDialog();
            if (frm.Product_Images != null && !string.IsNullOrEmpty(frm.Product_Images.ImageName))
            {
                frm.Product_Images.OrderIndex = objProduct_ImagesList.Count + 1;
                if (frm.Product_Images.IsDefault)
                {
                    foreach (PLC.PM.WSLotIMEISalesInfo.LotIMEISalesInfo_Images img in objProduct_ImagesList.Where(img => img.IsDefault = true))
                    {
                        img.IsDefault = false;
                    }
                }
                frm.Product_Images.CreatedUser = SystemConfig.objSessionUser.UserName;
                objProduct_ImagesList.Add(frm.Product_Images);
            }
            grdImages.RefreshDataSource();
        }

        private void btnEditImage_Click(object sender, EventArgs e)
        {
            if (grvImages.FocusedRowHandle < 0)
                return;
            LotIMEISalesInfo_Images obj = (LotIMEISalesInfo_Images)grvImages.GetRow(grvImages.FocusedRowHandle);
            frmLotIMEItImage frm = new frmLotIMEItImage();
            frm.IsReview = bolIsReview || chkIsSystem.Checked;
            frm.Product_Images = obj;
            frm.ProductID = txtProductID.Text.Trim();
            frm.ShowDialog();
            if (frm.Product_Images.IsDefault)
            {
                foreach (LotIMEISalesInfo_Images img in objProduct_ImagesList.Where(img => img.IsDefault = true))
                {
                    img.IsDefault = false;
                }
                frm.Product_Images.IsDefault = true;
            }
            frm.Product_Images.LotIMEISalesInfoID = txtLotIMEISalesInfoID.Text.Trim();
            frm.Product_Images.UpdatedUser = SystemConfig.objSessionUser.UserName;
            grdImages.RefreshDataSource();
        }

        private void btnDelImage_Click(object sender, EventArgs e)
        {
            if (grvImages.FocusedRowHandle < 0)
                return;
            LotIMEISalesInfo_Images obj = (LotIMEISalesInfo_Images)grvImages.GetRow(grvImages.FocusedRowHandle);
            if (obj.IsSystem)
            {
                MessageBox.Show(this, "Không được xóa hình sản phẩm có thuộc tính hệ thống?", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Question);
                return;
            }

            if (MessageBox.Show(this, "Bạn chắc chắn muốn xóa hình ảnh sản phẩm này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            if (obj != null)
            {
                for (int i = obj.OrderIndex; i < objProduct_ImagesList.Count; i++)
                {
                    objProduct_ImagesList[i].OrderIndex = i;
                }
                objProduct_ImagesList.Remove(obj);

            }
            if (obj.ImageID != string.Empty)
                objProduct_ImagesList_Del.Add(obj);
            grdImages.RefreshDataSource();
        }

        private void btnMoveDown_Click(object sender, EventArgs e)
        {
            grvImages.ClearColumnsFilter();
            if (grvImages.FocusedRowHandle < 0 || grvImages.FocusedRowHandle == grvImages.RowCount - 1)
                return;
            var ItemOld = (LotIMEISalesInfo_Images)grvImages.GetRow(grvImages.FocusedRowHandle);
            var ItemNew = objProduct_ImagesList[grvImages.FocusedRowHandle + 1];
            int intCurrentIndex = grvImages.FocusedRowHandle;
            objProduct_ImagesList.RemoveAt(intCurrentIndex + 1);
            objProduct_ImagesList.RemoveAt(intCurrentIndex);

            ItemNew.OrderIndex = ItemNew.OrderIndex - 1;
            ItemOld.OrderIndex = ItemOld.OrderIndex + 1;

            objProduct_ImagesList.Insert(intCurrentIndex, ItemNew);
            objProduct_ImagesList.Insert(intCurrentIndex + 1, ItemOld);
            grvImages.FocusedRowHandle = grvImages.FocusedRowHandle + 1;
            grdImages.RefreshDataSource();
        }

        private void btnMoveUp_Click(object sender, EventArgs e)
        {
            grvImages.ClearColumnsFilter();
            if (grvImages.FocusedRowHandle < 0 || grvImages.FocusedRowHandle == 0)
                return;
            grvImages.ClearColumnsFilter();
            var ItemOld = (LotIMEISalesInfo_Images)grvImages.GetRow(grvImages.FocusedRowHandle);
            var ItemNew = objProduct_ImagesList[grvImages.FocusedRowHandle - 1];
            int intCurrentIndex = grvImages.FocusedRowHandle;
            objProduct_ImagesList.RemoveAt(intCurrentIndex);
            objProduct_ImagesList.RemoveAt(intCurrentIndex - 1);

            ItemNew.OrderIndex = ItemNew.OrderIndex + 1;
            ItemOld.OrderIndex = ItemOld.OrderIndex - 1;

            objProduct_ImagesList.Insert(intCurrentIndex - 1, ItemOld);
            objProduct_ImagesList.Insert(intCurrentIndex, ItemNew);
            grvImages.FocusedRowHandle = grvImages.FocusedRowHandle - 1;
            grdImages.RefreshDataSource();
        }


        private void btnReview_Click(object sender, EventArgs e)
        {
            grdData.RefreshDataSource();
            DataTable dtbResource = (DataTable)grdData.DataSource;
            dtbResource.AcceptChanges();
            DataRow[] rows = dtbResource.Select("ISSELECT = 1");
            if (rows == null || rows.Length == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn thông tin cần duyệt!");
                return;
            }
            for (int i = 0; i < dtbResource.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dtbResource.Rows[i]["ISSELECT"]) && !Convert.ToBoolean(dtbResource.Rows[i]["ISACTIVE"]))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Thông tin phải được kích hoạt trước khi duyệt!");
                    grdViewData.FocusedRowHandle = i;
                    return;
                }
            }
            if (!Review())
            {
                return;
            }
            btnSearch_Click(null, null);
        }

        private bool Review()
        {
            bool bolIsResult = false;
            List<LotIMEISalesInfo> lstLotIMEISalesInfo = new List<LotIMEISalesInfo>();
            foreach (DataRow row in ((DataTable)grdData.DataSource).Rows)
            {
                if (Convert.ToBoolean(row["IsSelect"]))
                {
                    LotIMEISalesInfo objIMEISalesInfo = new LotIMEISalesInfo();
                    objIMEISalesInfo.LotIMEISalesInfoID = row["LotIMEISalesInfoID"].ToString().Trim();
                    objIMEISalesInfo.ReviewedUser = SystemConfig.objSessionUser.UserName;
                    objIMEISalesInfo.ReviewedDate = DateTime.Now;
                    lstLotIMEISalesInfo.Add(objIMEISalesInfo);
                }
            }
            bolIsResult = objPLCLotIMEISalesInfo.Review(lstLotIMEISalesInfo);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
            else
            {
                MessageBoxObject.ShowInfoMessage(this, "Duyệt thông tin bán hàng của lô IMEI thành công !");
            }
            return bolIsResult;
        }


        private void mnuAddImage_Click(object sender, EventArgs e)
        {
            if (btnAddNewImage.Enabled)
            {
                btnAddNewImage_Click(null, null);
            }
        }

        private void mnuEditImage_Click(object sender, EventArgs e)
        {
            if (btnEditImage.Enabled)
            {
                btnEditImage_Click(null, null);
            }
        }

        private void mnuDelImage_Click(object sender, EventArgs e)
        {
            if (btnDelImage.Enabled)
            {
                btnDelImage_Click(null, null);
            }
        }

        private void repChkIMEI_IsSytem_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit chkEdit = (DevExpress.XtraEditors.CheckEdit)sender;
            grvIMEI.SetFocusedRowCellValue("IsSytem", chkEdit.Checked);
        }

        private void grdImages_DoubleClick(object sender, EventArgs e)
        {
            btnEditImage_Click(null, null);
        }

        private void repTxtIMEI_Click(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.TextEdit txt = (DevExpress.XtraEditors.TextEdit)sender;
            LotIMEISalesInfo_IMEI objLotIMEISalesInfo_IMEI = (LotIMEISalesInfo_IMEI)grvIMEI.GetFocusedRow();
            if (objLotIMEISalesInfo_IMEI != null)
            {
                txt.Properties.ReadOnly = objLotIMEISalesInfo_IMEI.IsSystem;
            }
        }

        private void repTxtIMEI_Validating(object sender, CancelEventArgs e)
        {
            if (grvIMEI.FocusedRowHandle < 0)
            {
                return;
            }
            DevExpress.XtraEditors.TextEdit txt = (DevExpress.XtraEditors.TextEdit)sender;
            string strIMEI = txt.Text.Trim();
            if (strIMEI == string.Empty)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập IMEI!");
                return;
            }
            //List<LotIMEISalesInfo_IMEI> lstLotIMEISalesInfo_IMEI = (List<LotIMEISalesInfo_IMEI>)grdIMEI.DataSource;
            //List<LotIMEISalesInfo_IMEI> lstLotIMEISalesInfo_IMEITemp = lstLotIMEISalesInfo_IMEI.FindAll(p => p.IMEI == strIMEI);
            if (CheckIMEI(strIMEI))
            {
                MessageBoxObject.ShowWarningMessage(this, "IMEI đã tồn tại trong danh sách!");
                txt.Text = string.Empty;
                txt.Focus();
            }
            DataRow[] rows = dtbIMEI.Select("IMEI = '" + strIMEI + "'");
            if (rows !=null && rows.Length > 0)
            {
                string strLotIMEISaleInfoID = rows[0]["LOTIMEISALESINFOID"].ToString().Trim();
                if (strLotIMEISaleInfoID != string.Empty)
                {
                    MessageBoxObject.ShowWarningMessage(this, "IMEI đã thuộc lô IMEI :  " + strLotIMEISaleInfoID + "!");
                    txt.Text = string.Empty;
                    txt.Focus();
                }
                else if (Convert.ToBoolean(rows[0]["ISIMESALEINFO"]))
                {
                    MessageBoxObject.ShowWarningMessage(this, "IMEI đã được khai báo thông tin bán hàng!");
                    txt.Text = string.Empty;
                    txt.Focus();
                }
            }
            else
            {
                MessageBoxObject.ShowWarningMessage(this, "IMEI không hợp lệ(không tồn kho hoặc là IMEI mới)!");
                txt.Text = string.Empty;
                txt.Focus();
            }
        }

        private bool CheckIMEI(string strIMEI)
        {
            List<LotIMEISalesInfo_IMEI> lstLotIMEISalesInfo_IMEI = (List<LotIMEISalesInfo_IMEI>)grdIMEI.DataSource;
            List<LotIMEISalesInfo_IMEI> lstLotIMEISalesInfo_IMEITemp = lstLotIMEISalesInfo_IMEI.FindAll(p => p.IMEI == strIMEI);
            if (lstLotIMEISalesInfo_IMEITemp != null && lstLotIMEISalesInfo_IMEITemp.Count > 0)
            {
                return true;
            }
            return false;
        }

        ///// <summary>
        ///// Hàm load dữ liệu lên lưới
        ///// </summary>
        //private void LoadIMEI()
        //{
        //    try
        //    {
        //        object[] objKeywords = new object[] 
        //        {                     
        //            "@ProductID", txtProductID.Text.Trim()
        //        };
        //        ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        //        DataTable dtbSource = objPLCReportDataSource.GetDataSource("PM_CRINSTOCKDETAIL_GETBYPRO", objKeywords);
        //        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
        //        {
        //            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
        //        }
        //    }
        //    catch (Exception objExce)
        //    {
        //        MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp danh sách IMEI!");
        //        SystemErrorWS.Insert("Lỗi nạp danh sách IMEI!", objExce.ToString(), this.Name + " -> LoadData", MasterData_Globals.ModuleName);
        //        return;
        //    }
        //}

        private string GetWebProductSpecContent(List<LotIMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpec)
        {
            string strWebProductSpecContent = string.Empty;
            List<LotIMEISalesInfo_ProSpec> lstUsed = new List<LotIMEISalesInfo_ProSpec>();
            foreach (LotIMEISalesInfo_ProSpec item in lstIMEISalesInfo_ProSpec)
            {
                LotIMEISalesInfo_ProSpec IMEISalesInfo_ProSpecTemp = lstUsed.Find(p => p.LotIMEISalesInfoID == item.LotIMEISalesInfoID &&  p.ProductSpecID == item.ProductSpecID);
                if (IMEISalesInfo_ProSpecTemp == null)
                {
                    lstUsed.Add(item);
                    List<LotIMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpecTemp = lstIMEISalesInfo_ProSpec.FindAll
                                                                (p => p.LotIMEISalesInfoID == item.LotIMEISalesInfoID && p.ProductSpecID == item.ProductSpecID);
                    if (item.ProductSpecStatusID > 0)
                    {

                        //strWebProductSpecContent += item.ProductSpecName;
                        //strWebProductSpecContent += " : ";
                        foreach (LotIMEISalesInfo_ProSpec item1 in lstIMEISalesInfo_ProSpecTemp)
                        {
                            if (item1.ProductSpecStatusID > 0)
                            {
                                DataRow[] dr = dtbDataSpecStatus.Select("ProductSpecStatusID = " + item1.ProductSpecStatusID);
                                if (dr != null || dr.Length > 0)
                                {
                                    strWebProductSpecContent += dr[0]["ProductSpecStatusName"].ToString().Trim();
                                    if (item.Note != string.Empty)
                                    {
                                        strWebProductSpecContent += "(" + item.Note + ")";
                                    }
                                    strWebProductSpecContent += ",";
                                }
                            }
                        }
                        strWebProductSpecContent = strWebProductSpecContent.TrimEnd(',');
                        strWebProductSpecContent += " ; ";
                    }
                }
            }
            strWebProductSpecContent = strWebProductSpecContent.TrimEnd(';');
            return strWebProductSpecContent;
        }

        private void mnuItemReview_Click(object sender, EventArgs e)
        {
            if (btnReview.Enabled)
            {
                btnReview_Click(null, null);
            }
        }

        private void chkIsBrandNewWarranty_CheckedChanged(object sender, EventArgs e)
        {
            dteEndWarantyDate.Enabled = chkIsBrandNewWarranty.Checked;
        }

        private void mnuItemViewComment_Click(object sender, EventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0)
                return;
            DataRow dtRow = (DataRow)grdViewData.GetFocusedDataRow();
            string strLotIMEISalesInfoID = Convert.ToString(dtRow["LOTIMEISALESINFOID"]).Trim();
            string strLotIMEISalesInfoName = Convert.ToString(dtRow["LOTIMEISALESINFONAME"]).Trim();
            frmLotIMEISaleSInfo_CommentManager frm = new frmLotIMEISaleSInfo_CommentManager();
            frm.LotIMEISalesInfoID = strLotIMEISalesInfoID;
            frm.LotIMEISalesInfoName = strLotIMEISalesInfoName;
            frm.ShowDialog();
        }

        private void grvIMEI_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void grvIMEI_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (grvIMEI.FocusedRowHandle < 0)
                return;
            if (grvIMEI.FocusedColumn.FieldName.ToUpper() == "ISSELECT")
            {
                LotIMEISalesInfo_IMEI obj = grvIMEI.GetFocusedRow() as LotIMEISalesInfo_IMEI;
                if (obj.IsSystem)
                    e.Cancel = true;
            }
            
        }

        private void grvIMEI_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (grvIMEI.FocusedRowHandle < 0)
                return;
            if (grvIMEI.FocusedColumn.FieldName.ToUpper() == "ISSYSTEM")
            {
                LotIMEISalesInfo_IMEI obj = grvIMEI.GetFocusedRow() as LotIMEISalesInfo_IMEI;
                if (obj.IsSystem)
                    obj.IsSelect = false;
            }
            grvIMEI.ShowEditor();
        }

        private void grvIMEI_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            e.ShowFilterEditor = false;
        }
    }
}
