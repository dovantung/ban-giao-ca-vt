﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using ERP.MasterData.DUI.Common;
namespace ERP.Inventory.DUI.PM
{
    /// <summary>
    /// Created by  :   Cao Hữu Vũ Lam
    /// Description :   Tìm kiếm phân tích
    /// </summary>
    public partial class frmIMEISearch : Form
    {
        #region Variable
        private bool bolIsMultiSelect = false;
        private string strProductID = string.Empty;
        private string strIMEI = string.Empty;
        private List<string> lstIMEI = new List<string>();
        #endregion

        #region Propertie
        public bool IsMultiSelect
        {
            get { return bolIsMultiSelect; }
            set { bolIsMultiSelect = value; }
        }
        public string ProductID
        {
            get { return strProductID; }
            set { strProductID = value; }
        }
        public string IMEI
        {
            get { return strIMEI; }
            set { strIMEI = value; }
        }
        public List<String> IMEIList
        {
            get { return lstIMEI; }
            set { lstIMEI = value; }
        }

        #endregion

        #region Constructor
        public frmIMEISearch()
        {
           InitializeComponent();
        }
        #endregion

        #region Các hàm sự kiện
        
        private void frmAccountAnalysis_Load(object sender, EventArgs e)
        {
            InitControl();
            LoadData();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void cmdSelect_Click(object sender, EventArgs e)
        {
            if (!bolIsMultiSelect)
            {
                if (SelectRow()) this.Close();
            }
            else
            {
                if (SelectMuilti()) this.Close();
            }
        } 
       
        #endregion

        #region Các hàm hỗ trợ
        /// <summary>
        /// Khởi tạo cho các control
        /// Xét các thuộc tính
        /// </summary>
        private void InitControl()
        {
            string[] fieldNames = new string[] { "ISSELECT", "PRODUCTID", "PRODUCTNAME", "IMEI", "PRICE", "STORENAME", "LOTIMEISALESINFOID", "ISIMESALEINFO" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            txtProductID.Text = strProductID;
            ERP.MasterData.PLC.MD.PLCProduct objPLCProduct = new ERP.MasterData.PLC.MD.PLCProduct();
            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = new ERP.MasterData.PLC.MD.WSProduct.Product();
            objPLCProduct.LoadInfo(ref objProduct, strProductID);
            if (objProduct.IsExist)
            {
                txtProductName.Text = objProduct.ProductName;
            }
        }
     
        /// <summary>
        /// Hàm load dữ liệu lên lưới
        /// </summary>
        private void LoadData()
        {
            try
            {
                object[] objKeywords = new object[] 
                {                     
                    "@ProductID", txtProductID.Text.Trim()
                };
                ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
                DataTable dtbSource = objPLCReportDataSource.GetDataSource("PM_CRINSTOCKDETAIL_GETBYPRO", objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                }
                grdData.DataSource = dtbSource;
                FormatGrid();

            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp danh sách IMEI!");
                SystemErrorWS.Insert("Lỗi nạp danh sách IMEI!", objExce.ToString(), this.Name + " -> LoadData", DUIInventory_Globals.ModuleName);
                return;
            }
        }

        private bool FormatGrid()
        {
            try
            {
                grdViewData.Columns["ISSELECT"].Caption = "Chọn";
                grdViewData.Columns["ISSELECT"].AppearanceHeader.Options.UseTextOptions = true;
                grdViewData.Columns["ISSELECT"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                grdViewData.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
                grdViewData.Columns["PRODUCTID"].AppearanceHeader.Options.UseTextOptions = true;
                grdViewData.Columns["PRODUCTID"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                grdViewData.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
                grdViewData.Columns["PRODUCTNAME"].AppearanceHeader.Options.UseTextOptions = true;
                grdViewData.Columns["PRODUCTNAME"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                grdViewData.Columns["IMEI"].Caption = "IMEI";
                grdViewData.Columns["IMEI"].AppearanceHeader.Options.UseTextOptions = true;
                grdViewData.Columns["IMEI"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                grdViewData.Columns["PRICE"].Caption = "Giá bán";
                grdViewData.Columns["PRICE"].AppearanceHeader.Options.UseTextOptions = true;
                grdViewData.Columns["PRICE"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                grdViewData.Columns["STORENAME"].Caption = "Kho";
                grdViewData.Columns["STORENAME"].AppearanceHeader.Options.UseTextOptions = true;
                grdViewData.Columns["STORENAME"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                grdViewData.Columns["LOTIMEISALESINFOID"].Caption = "Mã lô IMEI";
                grdViewData.Columns["LOTIMEISALESINFOID"].AppearanceHeader.Options.UseTextOptions = true;
                grdViewData.Columns["LOTIMEISALESINFOID"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

                grdViewData.Columns["ISIMESALEINFO"].Caption = "Đã khai báo thông tin IMEI";
                grdViewData.Columns["ISIMESALEINFO"].AppearanceHeader.Options.UseTextOptions = true;
                grdViewData.Columns["ISIMESALEINFO"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                
                grdViewData.OptionsView.ColumnAutoWidth = false;
                grdViewData.Columns["ISSELECT"].Width = 80;
                grdViewData.Columns["IMEI"].Width = 160;
                grdViewData.Columns["PRICE"].Width = 120;
                grdViewData.Columns["PRODUCTID"].Width = 160;
                grdViewData.Columns["PRODUCTNAME"].Width = 250;
                grdViewData.Columns["STORENAME"].Width = 200;
                grdViewData.Columns["LOTIMEISALESINFOID"].Width = 160;
                grdViewData.Columns["ISIMESALEINFO"].Width = 100;
                grdViewData.Columns["PRICE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                grdViewData.Columns["PRICE"].DisplayFormat.FormatString = "#,###";

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repChkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                repChkIsSelect.ValueChecked = (decimal)1;
                repChkIsSelect.ValueUnchecked = (decimal)0;
                repChkIsSelect.CheckedChanged += new EventHandler(repChkIsSelect_CheckedChanged);
                grdViewData.Columns["ISSELECT"].ColumnEdit = repChkIsSelect;
                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repChkIsIMEISaleInfo = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                repChkIsIMEISaleInfo.ValueChecked = (decimal)1;
                repChkIsIMEISaleInfo.ValueUnchecked = (decimal)0;
                repChkIsIMEISaleInfo.CheckedChanged += new EventHandler(repChkIsSelect_CheckedChanged);
                grdViewData.Columns["ISIMESALEINFO"].ColumnEdit = repChkIsIMEISaleInfo;

                grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewData.OptionsView.ShowAutoFilterRow = true;
                grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewData.OptionsView.ShowFooter = false;
                grdViewData.OptionsView.ShowGroupPanel = false;
                grdViewData.ColumnPanelRowHeight = 40;
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
                //filter
                grdViewData.Columns["IMEI"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["PRICE"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["PRODUCTID"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["PRODUCTNAME"].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewData.Columns["ISSELECT"].Visible = bolIsMultiSelect;
    
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách phân tích");
                SystemErrorWS.Insert("Định dạng lưới danh sách phân tích", objExce.ToString(), this.Name + " -> FormatGrid", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void repChkIsSelect_CheckedChanged(object sender, EventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0)
            {
                return;
            }
            DevExpress.XtraEditors.CheckEdit chkEdit = (DevExpress.XtraEditors.CheckEdit)sender;
            DataRow row = (DataRow)grdViewData.GetFocusedDataRow();
            grdViewData.Columns["ISSELECT"].OptionsColumn.AllowEdit = (row["LOTIMEISALESINFOID"].ToString().Trim() == string.Empty && !Convert.ToBoolean(row["ISIMESALEINFO"]));
            //grdViewData.SetFocusedRowCellValue("IsSystem", row["LOTIMEISALESINFOID"].ToString().Trim() == string.Empty);
        }

        private bool SelectRow()
        {
            if (grdViewData.FocusedRowHandle < 0) return false;
            DataRow row = grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
            if (row == null) return false;
            strIMEI = row["IMEI"].ToString().Trim();
            return true;
        }

        private bool SelectMuilti()
        {
            grdViewData.RefreshData();
            DataTable dt = (DataTable)grdData.DataSource;
            if (dt == null || dt.Rows.Count == 0) return false;
            DataRow[] dtRows = dt.Select("IsSelect = 1");
            if (dtRows.Length == 0)
            {
                MessageBox.Show(this, "Chưa có IMEI nào được chọn!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            lstIMEI = new List<string>();
            for (int i = 0; i < dtRows.Length; i++)
            {
                lstIMEI.Add(dtRows[i]["IMEI"].ToString().Trim());
            }

            return true;
        }
        #endregion

        private void txtKeyword_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtKeyword_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch_Click(null, null);
            }
        }

        private void grdViewData_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0)
            {
                return;
            }
            DataRow row = (DataRow)grdViewData.GetFocusedDataRow();
            grdViewData.Columns["ISSELECT"].OptionsColumn.AllowEdit = (row["LOTIMEISALESINFOID"].ToString().Trim() == string.Empty && !Convert.ToBoolean(row["ISIMESALEINFO"]));
        }


    }
}
