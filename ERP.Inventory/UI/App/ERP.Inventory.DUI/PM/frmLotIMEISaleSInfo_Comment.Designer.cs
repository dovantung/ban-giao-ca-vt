﻿namespace ERP.Inventory.DUI.PM
{
    partial class frmLotIMEISaleSInfo_Comment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLotIMEISaleSInfo_Comment));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtPhoneNumber = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtContentReply = new DevExpress.XtraEditors.MemoEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.lnkReply = new System.Windows.Forms.LinkLabel();
            this.trbRate = new DevExpress.XtraEditors.TrackBarControl();
            this.txtContent = new DevExpress.XtraEditors.MemoEdit();
            this.dtCommentDate = new System.Windows.Forms.DateTimePicker();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtCommentUser = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCommentTitleReply = new System.Windows.Forms.TextBox();
            this.txtCommentTitle = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCommentID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkIsSystem = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkIsActive = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnUndo = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContentReply.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContent.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.groupControl1.Controls.Add(this.txtPhoneNumber);
            this.groupControl1.Controls.Add(this.label10);
            this.groupControl1.Controls.Add(this.groupBox1);
            this.groupControl1.Controls.Add(this.txtContentReply);
            this.groupControl1.Controls.Add(this.label8);
            this.groupControl1.Controls.Add(this.lnkReply);
            this.groupControl1.Controls.Add(this.trbRate);
            this.groupControl1.Controls.Add(this.txtContent);
            this.groupControl1.Controls.Add(this.dtCommentDate);
            this.groupControl1.Controls.Add(this.txtEmail);
            this.groupControl1.Controls.Add(this.txtCommentUser);
            this.groupControl1.Controls.Add(this.label7);
            this.groupControl1.Controls.Add(this.label6);
            this.groupControl1.Controls.Add(this.txtCommentTitleReply);
            this.groupControl1.Controls.Add(this.txtCommentTitle);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.label9);
            this.groupControl1.Controls.Add(this.txtCommentID);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.chkIsSystem);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.chkIsActive);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.label5);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.SkinName = "Blue";
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(620, 335);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin bình luận";
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.Location = new System.Drawing.Point(368, 233);
            this.txtPhoneNumber.MaxLength = 20;
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new System.Drawing.Size(227, 22);
            this.txtPhoneNumber.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(317, 236);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 16);
            this.label10.TabIndex = 12;
            this.label10.Text = "SĐT:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Gray;
            this.groupBox1.Location = new System.Drawing.Point(0, 325);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(620, 1);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            // 
            // txtContentReply
            // 
            this.txtContentReply.Location = new System.Drawing.Point(129, 367);
            this.txtContentReply.Name = "txtContentReply";
            this.txtContentReply.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContentReply.Properties.Appearance.Options.UseFont = true;
            this.txtContentReply.Properties.MaxLength = 2000;
            this.txtContentReply.Size = new System.Drawing.Size(466, 113);
            this.txtContentReply.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 370);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 16);
            this.label8.TabIndex = 21;
            this.label8.Text = "Nội dung trả lời:";
            // 
            // lnkReply
            // 
            this.lnkReply.AutoSize = true;
            this.lnkReply.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkReply.LinkVisited = true;
            this.lnkReply.Location = new System.Drawing.Point(13, 305);
            this.lnkReply.Name = "lnkReply";
            this.lnkReply.Size = new System.Drawing.Size(73, 16);
            this.lnkReply.TabIndex = 10;
            this.lnkReply.TabStop = true;
            this.lnkReply.Text = "Trả lời >>";
            this.lnkReply.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkReply_LinkClicked);
            // 
            // trbRate
            // 
            this.trbRate.EditValue = null;
            this.trbRate.Location = new System.Drawing.Point(129, 261);
            this.trbRate.Name = "trbRate";
            this.trbRate.Properties.Maximum = 5;
            this.trbRate.Properties.ShowValueToolTip = true;
            this.trbRate.Properties.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trbRate.Size = new System.Drawing.Size(225, 45);
            this.trbRate.TabIndex = 7;
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(129, 84);
            this.txtContent.Name = "txtContent";
            this.txtContent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContent.Properties.Appearance.Options.UseFont = true;
            this.txtContent.Properties.MaxLength = 2000;
            this.txtContent.Size = new System.Drawing.Size(466, 113);
            this.txtContent.TabIndex = 3;
            // 
            // dtCommentDate
            // 
            this.dtCommentDate.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtCommentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtCommentDate.Location = new System.Drawing.Point(368, 27);
            this.dtCommentDate.Name = "dtCommentDate";
            this.dtCommentDate.Size = new System.Drawing.Size(227, 22);
            this.dtCommentDate.TabIndex = 1;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(129, 233);
            this.txtEmail.MaxLength = 200;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(182, 22);
            this.txtEmail.TabIndex = 5;
            // 
            // txtCommentUser
            // 
            this.txtCommentUser.BackColor = System.Drawing.SystemColors.Info;
            this.txtCommentUser.Location = new System.Drawing.Point(129, 204);
            this.txtCommentUser.MaxLength = 200;
            this.txtCommentUser.Name = "txtCommentUser";
            this.txtCommentUser.Size = new System.Drawing.Size(466, 22);
            this.txtCommentUser.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 272);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "Đánh giá";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 236);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "Email:";
            // 
            // txtCommentTitleReply
            // 
            this.txtCommentTitleReply.Location = new System.Drawing.Point(129, 339);
            this.txtCommentTitleReply.MaxLength = 200;
            this.txtCommentTitleReply.Name = "txtCommentTitleReply";
            this.txtCommentTitleReply.Size = new System.Drawing.Size(466, 22);
            this.txtCommentTitleReply.TabIndex = 11;
            // 
            // txtCommentTitle
            // 
            this.txtCommentTitle.Location = new System.Drawing.Point(129, 56);
            this.txtCommentTitle.MaxLength = 200;
            this.txtCommentTitle.Name = "txtCommentTitle";
            this.txtCommentTitle.Size = new System.Drawing.Size(466, 22);
            this.txtCommentTitle.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 207);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Người bình luận:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 342);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 16);
            this.label9.TabIndex = 19;
            this.label9.Text = "Tiêu đề trả lời:";
            // 
            // txtCommentID
            // 
            this.txtCommentID.BackColor = System.Drawing.SystemColors.Info;
            this.txtCommentID.Location = new System.Drawing.Point(129, 27);
            this.txtCommentID.Name = "txtCommentID";
            this.txtCommentID.ReadOnly = true;
            this.txtCommentID.Size = new System.Drawing.Size(182, 22);
            this.txtCommentID.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Tiêu đề:";
            // 
            // chkIsSystem
            // 
            this.chkIsSystem.Location = new System.Drawing.Point(395, 286);
            this.chkIsSystem.Name = "chkIsSystem";
            this.chkIsSystem.Size = new System.Drawing.Size(124, 24);
            this.chkIsSystem.TabIndex = 9;
            this.chkIsSystem.Text = "Hệ thống";
            this.chkIsSystem.UseVisualStyleBackColor = true;
            this.chkIsSystem.CheckedChanged += new System.EventHandler(this.chkIsSystem_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã bình luận:";
            // 
            // chkIsActive
            // 
            this.chkIsActive.Location = new System.Drawing.Point(395, 262);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(124, 24);
            this.chkIsActive.TabIndex = 8;
            this.chkIsActive.Text = "Kích hoạt";
            this.chkIsActive.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(317, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ngày:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Nội dung:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnUndo);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 335);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(620, 38);
            this.panel1.TabIndex = 0;
            // 
            // btnUndo
            // 
            this.btnUndo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUndo.Image = ((System.Drawing.Image)(resources.GetObject("btnUndo.Image")));
            this.btnUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUndo.Location = new System.Drawing.Point(519, 5);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(95, 25);
            this.btnUndo.TabIndex = 1;
            this.btnUndo.Text = "   Bỏ qua";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(419, 5);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(95, 25);
            this.btnUpdate.TabIndex = 0;
            this.btnUpdate.Text = "   Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // frmLotIMEISaleSInfo_Comment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 373);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(620, 372);
            this.Name = "frmLotIMEISaleSInfo_Comment";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông tin bình luận";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmModelComment_FormClosing);
            this.Load += new System.EventHandler(this.frmModelComment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContentReply.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContent.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.DateTimePicker dtCommentDate;
        private System.Windows.Forms.TextBox txtCommentID;
        private System.Windows.Forms.CheckBox chkIsSystem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkIsActive;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnUpdate;
        private DevExpress.XtraEditors.TrackBarControl trbRate;
        private DevExpress.XtraEditors.MemoEdit txtContent;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtCommentUser;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCommentTitle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.MemoEdit txtContentReply;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.LinkLabel lnkReply;
        private System.Windows.Forms.TextBox txtCommentTitleReply;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtPhoneNumber;
        private System.Windows.Forms.Label label10;
    }
}