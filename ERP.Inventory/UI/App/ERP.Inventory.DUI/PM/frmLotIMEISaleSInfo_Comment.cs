﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using ERP.Inventory.PLC.PM.WSLotIMEISalesInfo_Comment;
using System.Text.RegularExpressions;
using Library.AppCore.LoadControls;

namespace ERP.Inventory.DUI.PM
{
    public partial class frmLotIMEISaleSInfo_Comment : Form
    {
        #region Variable
        private bool bolIsReply = false;
        private LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment_Reply = null;
        private List<LotIMEISalesInfo_Comment> objLotIMEISalesInfo_CommentList = null;
        private LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment = null;
        private LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment_Old = null;
        bool bolIsClose = false;
        private bool bolIsUpdated = false;
        private PLC.PM.PLCLotIMEISalesInfo_Comment objPLCLotIMEISalesInfo_Comment = new PLC.PM.PLCLotIMEISalesInfo_Comment();
        private string strLotIMEISalesInfo = string.Empty;
        private bool bolIsPermission_Add = false;
        private bool bolIsPermission_Edit = false;
        #endregion
        #region Constructor
        public frmLotIMEISaleSInfo_Comment()
        {
            InitializeComponent();
        }
        #endregion

        #region Properties
        public bool IsPermission_Edit
        {
            //get { return bolIsPermission_Edit; }
            set { bolIsPermission_Edit = value; }
        }
        public bool IsPermission_Add
        {
            //get { return bolIsPermission_Add; }
            set { bolIsPermission_Add = value; }
        }
        public List<LotIMEISalesInfo_Comment> LotIMEISalesInfo_CommentList
        {
            get {

                if (objLotIMEISalesInfo_CommentList == null)
                    objLotIMEISalesInfo_CommentList = new List<LotIMEISalesInfo_Comment>();
                return objLotIMEISalesInfo_CommentList; }
            set { objLotIMEISalesInfo_CommentList = value; }
        }

        public bool IsReply
        {
            get { return bolIsReply;  }
            set { bolIsReply = value; }
        }

        public bool IsUpdated
        {
            get { return bolIsUpdated; }
            set { bolIsUpdated = value; }
        }
        public LotIMEISalesInfo_Comment LotIMEISalesInfo_Comment
        {
            get { return objLotIMEISalesInfo_Comment; }
            set { objLotIMEISalesInfo_Comment = value; }
        }

        public LotIMEISalesInfo_Comment LotIMEISalesInfo_Comment_Reply
        {
            get { return objLotIMEISalesInfo_Comment_Reply; }
            set { objLotIMEISalesInfo_Comment_Reply = value; }
        }

        #endregion
        #region Method
        private bool CheckInput()
        {
            if (string.IsNullOrEmpty(txtContent.Text.Trim()))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập nội dung bình luận");
                txtContent.Focus();
                return false;
            }
            if (txtCommentUser.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this, "Bạn chưa nhập người bình luận", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCommentUser.Focus();
                return false;
            }
            if (txtEmail.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this, "Bạn chưa nhập email", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmail.Focus();
                return false;
            }
            if (!Regex.IsMatch(txtEmail.Text,
                   @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$"))
            {
                MessageBox.Show(this, "Email không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmail.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtPhoneNumber.Text.Trim()))
            {
                txtPhoneNumber.Focus();
                MessageBox.Show(this, "Vui lòng nhập số điện thoại khách hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (txtPhoneNumber.Text.Trim().Length > 0 && (!Globals.CheckIsPhoneNumber(txtPhoneNumber.Text.Trim())
                || txtPhoneNumber.Text.Trim().Length < 10 || txtPhoneNumber.Text.Trim().Length > 11))
            {
                txtPhoneNumber.Focus();
                MessageBox.Show(this, "Số điện thoại khách hàng không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            
            if (txtContentReply.Enabled && string.IsNullOrEmpty(txtContentReply.Text.Trim()))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập nội dung bình luận trả lời");
                txtContentReply.Focus();
                return false;
            }
            
            return true;
        }

        /// <summary>
        /// Ẩn hiện control
        /// </summary>
        /// <param name="bolValue">true: hiện; false: ẩn</param>
        private void EnableControl(bool bolValue)
        {
            dtCommentDate.Enabled = bolValue;
            txtCommentTitle.ReadOnly = !bolValue;
            
            txtContent.Properties.ReadOnly = !bolValue;
            txtEmail.ReadOnly = !bolValue;
            chkIsActive.Enabled = bolValue;
            trbRate.Properties.ReadOnly = !bolValue;
            txtPhoneNumber.ReadOnly = !bolValue;
        }
        #endregion
        #region Event
        private void frmModelComment_Load(object sender, EventArgs e)
        {
            try
            {
                if (objLotIMEISalesInfo_Comment != null)
                {
                    strLotIMEISalesInfo = objLotIMEISalesInfo_Comment.LotIMEISalesInfoID;
                    txtCommentID.Text = Convert.ToString(objLotIMEISalesInfo_Comment.CommentID);
                    txtCommentTitle.Text = objLotIMEISalesInfo_Comment.CommentTitle;
                    txtCommentUser.Text = objLotIMEISalesInfo_Comment.CommentUser;
                    txtContent.Lines = objLotIMEISalesInfo_Comment.CommentContent.Split('\n');
                    txtEmail.Text = objLotIMEISalesInfo_Comment.CommentEmail;
                    txtPhoneNumber.Text = objLotIMEISalesInfo_Comment.PhoneNumber;
                    chkIsActive.Checked = objLotIMEISalesInfo_Comment.IsActive;
                    chkIsSystem.Checked = objLotIMEISalesInfo_Comment.IsSystem;
                    dtCommentDate.Value = objLotIMEISalesInfo_Comment.CommentDate.Value;
                    trbRate.Value = objLotIMEISalesInfo_Comment.ModelRate;
                    txtCommentUser.ReadOnly = true;
                    if (objLotIMEISalesInfo_Comment.ReplyToCommentID > 0 || !bolIsPermission_Add)
                    {
                        lnkReply.Visible = false;
                    }
                    //else
                    //{
                    //    txtCommentUser.ReadOnly = false;
                    //}

                    //objLotIMEISalesInfo_Comment_Old = new PLC.PM.WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment();
                    //objLotIMEISalesInfo_Comment_Old.CommentID = Convert.ToDecimal(txtCommentID.Text);
                    //objLotIMEISalesInfo_Comment_Old.CommentTitle = txtCommentID.Text;
                    //objLotIMEISalesInfo_Comment_Old.CommentUser = txtCommentUser.Text;
                    //objLotIMEISalesInfo_Comment_Old.CommentContent = txtContent.Text;
                    //objLotIMEISalesInfo_Comment_Old.CommentEmail = txtEmail.Text;
                    //objLotIMEISalesInfo_Comment_Old.IsActive = chkIsActive.Checked;
                    //objLotIMEISalesInfo_Comment_Old.IsSystem = chkIsSystem.Checked;
                    //objLotIMEISalesInfo_Comment_Old.CommentDate = dtCommentDate.Value;
                    //objLotIMEISalesInfo_Comment_Old.ModelRate = trbRate.Value;
                }
                else
                    this.Close();
                if (!bolIsPermission_Edit)
                {
                    txtCommentID.ReadOnly = true;
                    txtCommentTitle.ReadOnly = true;
                    txtCommentUser.ReadOnly = true;
                    txtContent.Properties.ReadOnly = true;
                    txtEmail.ReadOnly = true;
                    chkIsActive.Enabled = false;
                    chkIsSystem.Enabled = false;
                    dtCommentDate.Enabled = false;
                    trbRate.Properties.ReadOnly = true;
                    txtPhoneNumber.ReadOnly = true;
                    //this.btnUpdate.Enabled = false;
                }
                if (!bolIsPermission_Add && !bolIsPermission_Edit)
                {
                    btnUpdate.Enabled = false;
                }
                if (SystemConfig.objSessionUser.UserName != "administrator")
                {
                    chkIsSystem.Enabled = false;
                }
                txtCommentTitleReply.Enabled = false;
                txtContentReply.Enabled = false;
                txtCommentUser.ReadOnly = true;
            }
            catch (Exception objExc)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin bình luận", objExc.ToString());
                SystemErrorWS.Insert("Lỗi nạp thông tin bình luận", objExc.ToString(), this.Name + " -> frmModelComment_Load", DUIInventory_Globals.ModuleName);
            }
        }
        
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckInput())
                    return;
                objLotIMEISalesInfo_Comment.CommentID = Convert.ToUInt32(txtCommentID.Text);
                objLotIMEISalesInfo_Comment.CommentTitle = txtCommentTitle.Text;
                objLotIMEISalesInfo_Comment.CommentUser = txtCommentUser.Text;
                objLotIMEISalesInfo_Comment.CommentContent = txtContent.Text;
                objLotIMEISalesInfo_Comment.CommentEmail = txtEmail.Text;
                objLotIMEISalesInfo_Comment.PhoneNumber = txtPhoneNumber.Text.Trim();
                objLotIMEISalesInfo_Comment.IsActive = chkIsActive.Checked;
                objLotIMEISalesInfo_Comment.IsSystem = chkIsSystem.Checked;
                objLotIMEISalesInfo_Comment.CommentDate = dtCommentDate.Value;
                objLotIMEISalesInfo_Comment.ModelRate = trbRate.Value;
                objLotIMEISalesInfo_Comment.LotIMEISalesInfoID = strLotIMEISalesInfo;
                if (txtCommentTitleReply.Text.Trim() != string.Empty || txtContentReply.Text.Trim() != string.Empty)
                {
                    if (objLotIMEISalesInfo_Comment_Reply == null)
                    {
                        objLotIMEISalesInfo_Comment_Reply = new LotIMEISalesInfo_Comment();
                        objLotIMEISalesInfo_Comment_Reply.CommentID = LotIMEISalesInfo_CommentList.Count + 1;
                        /////////////objLotIMEISalesInfo_Comment_Reply.IsAddNew = true;

                    }
                    if (objLotIMEISalesInfo_Comment.ReplyToCommentID > 0)
                    {
                        var lst = from o in objLotIMEISalesInfo_CommentList
                                  where o.ReplyToCommentID == objLotIMEISalesInfo_Comment.ReplyToCommentID
                                  select o;
                        objLotIMEISalesInfo_Comment_Reply.OrderIndex = lst.Count() + 1;
                        objLotIMEISalesInfo_Comment_Reply.ReplyToCommentID = Convert.ToUInt32(objLotIMEISalesInfo_Comment.ReplyToCommentID);
                    }
                    else
                    {
                        var lst = from o in objLotIMEISalesInfo_CommentList
                                  where o.ReplyToCommentID == objLotIMEISalesInfo_Comment.CommentID
                                  select o;
                        objLotIMEISalesInfo_Comment_Reply.OrderIndex = lst.Count() + 1;
                        objLotIMEISalesInfo_Comment_Reply.ReplyToCommentID = Convert.ToUInt32(objLotIMEISalesInfo_Comment.CommentID);
                    }

                    ERP.MasterData.SYS.PLC.WSUser.User objUser = ERP.MasterData.SYS.PLC.PLCUser.LoadInfoFromCache(SystemConfig.objSessionUser.UserName);
                    objLotIMEISalesInfo_Comment_Reply.CommentTitle = txtCommentTitleReply.Text; /////////// LƯU Ý
                    objLotIMEISalesInfo_Comment_Reply.CommentUser = SystemConfig.objSessionUser.UserName;
                    objLotIMEISalesInfo_Comment_Reply.CommentContent = txtContentReply.Text;
                    if (objUser != null && !string.IsNullOrEmpty(objUser.Email))
                    {
                        objLotIMEISalesInfo_Comment_Reply.CommentEmail = objUser.Email;

                    }
                    else
                        objLotIMEISalesInfo_Comment_Reply.CommentEmail = SystemConfig.DefaultCompany.Email;

                    objLotIMEISalesInfo_Comment_Reply.IsActive = true;
                    objLotIMEISalesInfo_Comment_Reply.IsSystem = false;
                    objLotIMEISalesInfo_Comment_Reply.CommentDate = Library.AppCore.Globals.GetServerDateTime();
                    objLotIMEISalesInfo_Comment_Reply.ModelRate = 0;
                    objLotIMEISalesInfo_Comment_Reply.IsStaffComment = true;
                    objLotIMEISalesInfo_Comment_Reply.CommentFullName = SystemConfig.objSessionUser.FullName;
                    objLotIMEISalesInfo_Comment_Reply.LotIMEISalesInfoID = strLotIMEISalesInfo;
                    objLotIMEISalesInfo_Comment_Reply.IsCommentUserLogin = true;
                }
                if (!bolIsPermission_Edit)
                {
                    objLotIMEISalesInfo_Comment = null;
                }
                if (objLotIMEISalesInfo_Comment != null || objLotIMEISalesInfo_Comment_Reply != null)
                {
                    bool bolResult = objPLCLotIMEISalesInfo_Comment.UpdateData(objLotIMEISalesInfo_Comment, objLotIMEISalesInfo_Comment_Reply);
                    if (!bolResult)
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                                SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    }
                    this.bolIsUpdated = bolResult;
                }
                else
                    this.bolIsUpdated = false;
                this.Close();
            }
            catch (Exception objExc)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi cập nhật thông tin bình luận", objExc.ToString());
                SystemErrorWS.Insert("Lỗi cập nhật thông tin bình luận", objExc.ToString(), this.Name + " -> btnUpdate_Click", DUIInventory_Globals.ModuleName);
            }
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            //objLotIMEISalesInfo_Comment = objLotIMEISalesInfo_Comment_Old;
            //bolIsClose = true;
            bolIsUpdated = false;
            this.Close();
        }

        private void chkIsSystem_CheckedChanged(object sender, EventArgs e)
        {
            EnableControl(!chkIsSystem.Checked);
        }
       
        private void frmModelComment_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(!bolIsClose)
               objLotIMEISalesInfo_Comment = objLotIMEISalesInfo_Comment_Old;
        }
        
        private void lnkReply_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (ClientSize.Height <=400)
            {
                this.ClientSize = new System.Drawing.Size(620, 524);
                lnkReply.Text = "<< Trả lời";
                txtCommentTitleReply.Text = txtCommentTitle.Text;
                txtContentReply.Text = string.Empty;
                txtCommentTitleReply.Enabled = true;
                txtContentReply.Enabled = true;
            }
            else
                if (ClientSize.Height >400)
                {
                    this.ClientSize = new System.Drawing.Size(620, 372);
                    lnkReply.Text = "Trả lời >>";
                    txtCommentTitleReply.Text = string.Empty;
                    txtContentReply.Text = string.Empty;
                    txtCommentTitleReply.Enabled = false;
                    txtContentReply.Enabled = false;
                }
        }
        #endregion
    }
}
