﻿namespace ERP.Inventory.DUI.PM
{
    partial class frmLotIMEItImage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLotIMEItImage));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnUndo = new System.Windows.Forms.Button();
            this.btnUpdateImages = new System.Windows.Forms.Button();
            this.lblImageID = new System.Windows.Forms.Label();
            this.btnSmall = new System.Windows.Forms.Button();
            this.btnMediumView = new System.Windows.Forms.Button();
            this.btnLargeView = new System.Windows.Forms.Button();
            this.btnPictureBrowse = new System.Windows.Forms.Button();
            this.picIMG = new System.Windows.Forms.PictureBox();
            this.txtImageName = new System.Windows.Forms.TextBox();
            this.chkImageSystem = new System.Windows.Forms.CheckBox();
            this.chkImageIsActive = new System.Windows.Forms.CheckBox();
            this.chkIsDefaultImage = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtImageDescription = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIMG)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.panel1);
            this.groupControl1.Controls.Add(this.lblImageID);
            this.groupControl1.Controls.Add(this.btnSmall);
            this.groupControl1.Controls.Add(this.btnMediumView);
            this.groupControl1.Controls.Add(this.btnLargeView);
            this.groupControl1.Controls.Add(this.btnPictureBrowse);
            this.groupControl1.Controls.Add(this.picIMG);
            this.groupControl1.Controls.Add(this.txtImageName);
            this.groupControl1.Controls.Add(this.chkImageSystem);
            this.groupControl1.Controls.Add(this.chkImageIsActive);
            this.groupControl1.Controls.Add(this.chkIsDefaultImage);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.txtImageDescription);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.SkinName = "Blue";
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(544, 294);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin hình ảnh sản phẩm";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnUndo);
            this.panel1.Controls.Add(this.btnUpdateImages);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(2, 260);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(540, 32);
            this.panel1.TabIndex = 11;
            // 
            // btnUndo
            // 
            this.btnUndo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUndo.Image = ((System.Drawing.Image)(resources.GetObject("btnUndo.Image")));
            this.btnUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUndo.Location = new System.Drawing.Point(439, 2);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(95, 25);
            this.btnUndo.TabIndex = 15;
            this.btnUndo.Text = "   Bỏ qua";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnUpdateImages
            // 
            this.btnUpdateImages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdateImages.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateImages.Image")));
            this.btnUpdateImages.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdateImages.Location = new System.Drawing.Point(332, 2);
            this.btnUpdateImages.Name = "btnUpdateImages";
            this.btnUpdateImages.Size = new System.Drawing.Size(102, 25);
            this.btnUpdateImages.TabIndex = 0;
            this.btnUpdateImages.Text = "     Cập nhật";
            this.btnUpdateImages.UseVisualStyleBackColor = true;
            this.btnUpdateImages.Click += new System.EventHandler(this.btnUpdateImages_Click);
            // 
            // lblImageID
            // 
            this.lblImageID.AutoSize = true;
            this.lblImageID.Location = new System.Drawing.Point(557, 0);
            this.lblImageID.Name = "lblImageID";
            this.lblImageID.Size = new System.Drawing.Size(0, 16);
            this.lblImageID.TabIndex = 19;
            this.lblImageID.Visible = false;
            // 
            // btnSmall
            // 
            this.btnSmall.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSmall.Image = ((System.Drawing.Image)(resources.GetObject("btnSmall.Image")));
            this.btnSmall.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSmall.Location = new System.Drawing.Point(485, 206);
            this.btnSmall.Name = "btnSmall";
            this.btnSmall.Size = new System.Drawing.Size(55, 25);
            this.btnSmall.TabIndex = 10;
            this.btnSmall.Text = "    Nhỏ";
            this.btnSmall.UseVisualStyleBackColor = true;
            this.btnSmall.Click += new System.EventHandler(this.btnSmall_Click);
            // 
            // btnMediumView
            // 
            this.btnMediumView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMediumView.Image = ((System.Drawing.Image)(resources.GetObject("btnMediumView.Image")));
            this.btnMediumView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMediumView.Location = new System.Drawing.Point(427, 206);
            this.btnMediumView.Name = "btnMediumView";
            this.btnMediumView.Size = new System.Drawing.Size(55, 25);
            this.btnMediumView.TabIndex = 9;
            this.btnMediumView.Text = "    Vừa";
            this.btnMediumView.UseVisualStyleBackColor = true;
            this.btnMediumView.Click += new System.EventHandler(this.btnMediumView_Click);
            // 
            // btnLargeView
            // 
            this.btnLargeView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLargeView.Image = ((System.Drawing.Image)(resources.GetObject("btnLargeView.Image")));
            this.btnLargeView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLargeView.Location = new System.Drawing.Point(371, 206);
            this.btnLargeView.Name = "btnLargeView";
            this.btnLargeView.Size = new System.Drawing.Size(55, 25);
            this.btnLargeView.TabIndex = 8;
            this.btnLargeView.Text = "    Lớn";
            this.btnLargeView.UseVisualStyleBackColor = true;
            this.btnLargeView.Click += new System.EventHandler(this.btnLargeView_Click);
            // 
            // btnPictureBrowse
            // 
            this.btnPictureBrowse.Image = ((System.Drawing.Image)(resources.GetObject("btnPictureBrowse.Image")));
            this.btnPictureBrowse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPictureBrowse.Location = new System.Drawing.Point(387, 169);
            this.btnPictureBrowse.Name = "btnPictureBrowse";
            this.btnPictureBrowse.Size = new System.Drawing.Size(141, 25);
            this.btnPictureBrowse.TabIndex = 7;
            this.btnPictureBrowse.Text = "     Chọn hình ảnh";
            this.btnPictureBrowse.UseVisualStyleBackColor = true;
            this.btnPictureBrowse.Click += new System.EventHandler(this.btnPictureBrowse_Click);
            // 
            // picIMG
            // 
            this.picIMG.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picIMG.BackgroundImage")));
            this.picIMG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picIMG.Image = ((System.Drawing.Image)(resources.GetObject("picIMG.Image")));
            this.picIMG.Location = new System.Drawing.Point(387, 26);
            this.picIMG.Name = "picIMG";
            this.picIMG.Size = new System.Drawing.Size(137, 137);
            this.picIMG.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picIMG.TabIndex = 18;
            this.picIMG.TabStop = false;
            // 
            // txtImageName
            // 
            this.txtImageName.Location = new System.Drawing.Point(111, 26);
            this.txtImageName.MaxLength = 200;
            this.txtImageName.Name = "txtImageName";
            this.txtImageName.Size = new System.Drawing.Size(270, 22);
            this.txtImageName.TabIndex = 1;
            // 
            // chkImageSystem
            // 
            this.chkImageSystem.Location = new System.Drawing.Point(111, 216);
            this.chkImageSystem.Name = "chkImageSystem";
            this.chkImageSystem.Size = new System.Drawing.Size(151, 24);
            this.chkImageSystem.TabIndex = 6;
            this.chkImageSystem.Text = "Hệ thống";
            this.chkImageSystem.UseVisualStyleBackColor = true;
            this.chkImageSystem.CheckedChanged += new System.EventHandler(this.chkImageSystem_CheckedChanged);
            // 
            // chkImageIsActive
            // 
            this.chkImageIsActive.Location = new System.Drawing.Point(111, 192);
            this.chkImageIsActive.Name = "chkImageIsActive";
            this.chkImageIsActive.Size = new System.Drawing.Size(151, 24);
            this.chkImageIsActive.TabIndex = 5;
            this.chkImageIsActive.Text = "Kích hoạt";
            this.chkImageIsActive.UseVisualStyleBackColor = true;
            // 
            // chkIsDefaultImage
            // 
            this.chkIsDefaultImage.Location = new System.Drawing.Point(111, 169);
            this.chkIsDefaultImage.Name = "chkIsDefaultImage";
            this.chkIsDefaultImage.Size = new System.Drawing.Size(151, 24);
            this.chkIsDefaultImage.TabIndex = 4;
            this.chkIsDefaultImage.Text = "Hình ảnh mặc định";
            this.chkIsDefaultImage.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Tên hình ảnh:";
            // 
            // txtImageDescription
            // 
            this.txtImageDescription.Location = new System.Drawing.Point(111, 54);
            this.txtImageDescription.MaxLength = 2000;
            this.txtImageDescription.Multiline = true;
            this.txtImageDescription.Name = "txtImageDescription";
            this.txtImageDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtImageDescription.Size = new System.Drawing.Size(270, 109);
            this.txtImageDescription.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Mô tả:";
            // 
            // frmLotIMEItImage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 294);
            this.Controls.Add(this.groupControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(550, 322);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(550, 322);
            this.Name = "frmLotIMEItImage";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmLotIMEItImage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picIMG)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnUpdateImages;
        private System.Windows.Forms.Label lblImageID;
        private System.Windows.Forms.Button btnSmall;
        private System.Windows.Forms.Button btnMediumView;
        private System.Windows.Forms.Button btnLargeView;
        private System.Windows.Forms.Button btnPictureBrowse;
        private System.Windows.Forms.PictureBox picIMG;
        private System.Windows.Forms.TextBox txtImageName;
        private System.Windows.Forms.CheckBox chkImageSystem;
        private System.Windows.Forms.CheckBox chkImageIsActive;
        private System.Windows.Forms.CheckBox chkIsDefaultImage;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtImageDescription;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnUndo;
    }
}