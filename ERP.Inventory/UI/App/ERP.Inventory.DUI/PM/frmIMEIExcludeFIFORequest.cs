﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using ERP.Inventory.PLC.PM;
using ERP.Inventory.PLC.PM.WSLotIMEISalesInfo;
using Library.AppCore.LoadControls;
using System.IO;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
using ERP.Inventory.PLC.WSIMEIExcludeFIFORequest;
using Library.AppCore.Other;
namespace ERP.Inventory.DUI.PM
{
    /// <summary>
    /// Created by  :   Hồ Tấn Tài
    /// Description :   Quản lý thông tin Yêu cầu IMEI không FIFO
    /// </summary>
    public partial class frmIMEIExcludeFIFORequest : Form
    {
        #region Khai báo biến
        private string strPermission_Add = "PM_IMEIEXCLUDEFIFOREQUEST_ADD";
        private string strPermission_Delete = "PM_IMEIEXCLUDEFIFOREQUEST_DELETE";
        private string strPermission_Edit = "PM_IMEIEXCLUDEFIFOREQUEST_EDIT";
        private string strPermission_Review = "PM_IMEIEXCLUDEFIFOREQUEST_REVIEW";

        private bool bolPermission_ViewComment = false;
        private bool bolPermission_Add = false;
        private bool bolPermission_Delete = false;
        private bool bolPermission_Edit = false;
        private bool bolPermission_Review = false;
        private bool bolIsReview = false;
        private DateTime dtEndWarantyDate = DateTime.Now;
        private string strReviewedUser = string.Empty;
        private DateTime? dteReviewedDate = null;

        private DataTable dtbIMEI = null;
        private PLCIMEIExcludeFIFORequest objPLCIMEIExcludeFIFORequest = new PLCIMEIExcludeFIFORequest();
        private IMEIExcludeFIFORequest objIMEIExcludeFIFORequest = null;
        private List<LotIMEISalesInfo_Images> objProduct_ImagesList = new List<LotIMEISalesInfo_Images>();
        private List<LotIMEISalesInfo_Images> objProduct_ImagesList_Del = new List<LotIMEISalesInfo_Images>();
        List<IMEIExcludeFIFORequestDT> lstIMEIExcludeFIFORequestDT_Del = new List<IMEIExcludeFIFORequestDT>();
        private DataTable dtType = null;
        #endregion

        #region Constructor
        public frmIMEIExcludeFIFORequest()
        {
            InitializeComponent();
        }
        #endregion

        #region Các hàm sự kiện
        /// <summary>
        /// Hàm thay đổi focus khi nhấn Enter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeFocus(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                this.SelectNextControl(this.ActiveControl, true, true, true, true);
        }

        private void frmLotIMEISalesInfo_Load(object sender, EventArgs e)
        {
            KeyPreview = true;   
            InitControl();
            LoadPermission();
            FormatGrid();
            FormatGridIMEI();
            FormState = FormStateType.LIST;
        }

        private void LoadPermission()
        {
            bolPermission_Add = SystemConfig.objSessionUser.IsPermission(strPermission_Add);
            bolPermission_Delete = SystemConfig.objSessionUser.IsPermission(strPermission_Delete);
            bolPermission_Edit = SystemConfig.objSessionUser.IsPermission(strPermission_Edit);
            bolPermission_Review = SystemConfig.objSessionUser.IsPermission(strPermission_Review);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!btnAdd.Enabled)
                return;
            ClearValueControl();
            FormState = FormStateType.ADD;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (!btnEdit.Enabled)
                return;
            if (grdViewData.FocusedRowHandle < 0)
                return;
            if (!EditData())
                return;
            FormState = FormStateType.EDIT;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (!btnDelete.Enabled)
                return;

            if (!DeleteData())
                return;

            btnSearch_Click(null, null);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!btnUpdate.Enabled)
                return;
            if (!ValidateData())
                return;
            if (!UpdateData())
                return;
            LoadData();
            FormState = FormStateType.LIST;
        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            btnEdit_Click(null, null);
        }
        
        private void mnuItemAdd_Click(object sender, EventArgs e)
        {
            btnAdd_Click(null, null);
        }

        private void mnuItemDelete_Click(object sender, EventArgs e)
        {
            btnDelete_Click(null, null);
        }

        private void mnuItemEdit_Click(object sender, EventArgs e)
        {
            btnEdit_Click(null, null);
        }

        private void mnuItemExport_Click(object sender, EventArgs e)
        {
            btnExportExcel_Click(null, null);
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            FormState = FormStateType.LIST;
        }

        private void tabDetail_Enter(object sender, EventArgs e)
        {
            if (FormState == FormStateType.LIST)
            {
                tabControl.SelectedTab = tabList;
            }
        }

        private void tabList_Enter(object sender, EventArgs e)
        {
            if (FormState == FormStateType.ADD || FormState == FormStateType.EDIT)
            {
                tabControl.SelectedTab = tabDetail;
            }
        }

        private bool ValidateSearch()
        {
            if (!ERP.MasterData.DUI.Common.CommonFunction.EndDateValidation(dtpFromDate.Value, dtpToDate.Value, false))
            {
                dtpToDate.Focus();
                MessageBox.Show(this, "Đến ngày không thể nhỏ hơn từ ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!ValidateSearch())
            {
                return;
            }
            LoadData();
            FormState = FormStateType.LIST;
        }

        private void txtKeyWords_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && btnSearch.Enabled)
            {
                btnSearch_Click(null, null);
            }
        }

        #endregion

        #region Các hàm hỗ trợ
        /// <summary>
        /// Khởi tạo cho các control
        /// Xét các thuộc tính
        /// </summary>
        private void InitControl()
        {
            dtType = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("IMEIEXCLUDEFIFOREQUESTTYPESRH", new object[] { });
            DataTable dtTypeTemp = dtType.Clone();
            dtTypeTemp.Rows.Clear();
            foreach (DataRow row in dtType.Rows)
            {
                if (SystemConfig.objSessionUser.IsPermission(row["PERMISSION"].ToString().Trim()))
                    dtTypeTemp.Rows.Add(row.ItemArray);
            }

            cboRequestType.InitControl(false, dtTypeTemp, "IMEIEXCLUDEFIFOREQUESTTYPEID", "IMEIEXCLUDEFIFOREQUESTTYPENAME", "--Chọn loại yêu cầu--");
            cboSearchType.SelectedIndex = 0;

            DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
            cboStoreIDList.InitControl(true, dtbStore, "STOREID", "STORENAME", "-- Tất cả --");
            cboStoreIDList.IsReturnAllWhenNotChoose = true;

            cboStoreID.InitControl(false, dtbStore, "STOREID", "STORENAME", "-- Chọn kho --");

            cboReviewStatus.SelectedIndex = 0;

            DevExpress.XtraBars.BarButtonItem barItem = new DevExpress.XtraBars.BarButtonItem();
            barItem.ImageIndex = 1;
            barItem.Caption = "Từ chối";
            barItem.Name = "Từ chối";
            barItem.Tag = 1;
            barItem.ItemClick += barItem_ItemClick;
            pMnu.AddItem(barItem);

            DevExpress.XtraBars.BarButtonItem barItemAccept = new DevExpress.XtraBars.BarButtonItem();
            barItemAccept.ImageIndex = 2;
            barItemAccept.Caption = "Đồng ý";
            barItemAccept.Name = "Đồng ý";
            barItemAccept.Tag = 2;
            barItemAccept.ItemClick += barItemAccept_ItemClick;
            pMnu.AddItem(barItemAccept);
        }

        void barItemAccept_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Reviewed(1);
        }

        void barItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Reviewed(0);
        }

        private void Reviewed(int intReviewStatus)
        {
            if (!ValidateData())
                return;

            string strReasion = string.Empty;
            Library.AppControl.FormCommon.frmInputString frm = new Library.AppControl.FormCommon.frmInputString();
            frm.MaxLengthString = 400;
            frm.IsAllowEmpty = false;
            frm.Text = "Nhập nội dung duyệt";
            frm.ShowDialog();
            if (frm.IsAccept)
            {
                strReasion = frm.ContentString;
                if (string.IsNullOrEmpty(strReasion))
                {
                    MessageBox.Show("Vui lòng nhập nội dung duyệt.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    frm.ShowDialog();
                }
            }
            else
                return;

            objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestID = txtRequestID.Text.Trim();
            objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestTypeID = cboRequestType.ColumnID;
            objIMEIExcludeFIFORequest.RequestStoreID = cboStoreID.ColumnID;
            objIMEIExcludeFIFORequest.RequestUser = ucRequestUser.UserName;
            objIMEIExcludeFIFORequest.Note = txtNote.Text;
            objIMEIExcludeFIFORequest.ReviewedUser = SystemConfig.objSessionUser.UserName;
            objIMEIExcludeFIFORequest.ReviewedDate = Library.AppCore.Globals.GetServerDateTime();
            objIMEIExcludeFIFORequest.ReviewedStatus = intReviewStatus;
            objIMEIExcludeFIFORequest.ReviewedNote = strReasion;
            List<IMEIExcludeFIFORequestDT> lstIMEIExcludeFIFORequestDT = grdIMEI.DataSource as List<IMEIExcludeFIFORequestDT>;
            objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestDTList = lstIMEIExcludeFIFORequestDT.ToArray();
            objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestDT_Del_List = lstIMEIExcludeFIFORequestDT_Del.ToArray();

            objPLCIMEIExcludeFIFORequest.Update(objIMEIExcludeFIFORequest);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }

            MessageBox.Show("Duyệt thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            btnSearch_Click(null, null);
        }

        /// <summary>
        /// Ẩn hiện control
        /// </summary>
        /// <param name="bolValue">true: hiện; false: ẩn</param>
        private void EnableControl(bool bolValue)
        {            
            txtNote.ReadOnly = !bolValue;
            if (bolValue)
            {
                txtNote.BackColor = SystemColors.Window;
            }
            else
            {
                txtNote.BackColor = SystemColors.Info;
            }
            mnuIMEI.Enabled = bolValue;
        }

        /// <summary>
        /// Hàm load dữ liệu lên lưới
        /// </summary>
        private bool LoadData()
        {
            DataTable dtbData = null;
            object[] objKeywords = new object[]
            {
                "@FromDate", this.dtpFromDate.Value,
                "@ToDate", this.dtpToDate.Value,
                "@ProductIDList", cboProductList.ProductIDList,
                "@StoreIDList", cboStoreIDList.ColumnIDList,
                "@USERNAME", ctrlRequestUser.UserName,
                "@KeyWord", txtKeyWords.Text.Trim().ToUpper(),
                "@SEARCHTYPE", cboSearchType.SelectedIndex,
                "@IsDeleted", chkIsDelete.Checked,
                "@REVIEWEDSTATUS", cboReviewStatus.SelectedIndex
            };
            dtbData = objPLCIMEIExcludeFIFORequest.SearchData(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }
            grdData.DataSource = dtbData;
            return FormatGrid();
        }

        /// <summary>
        /// Hàm lấy dữ liệu mặc định các control
        /// </summary>
        private void ClearValueControl()
        {
            DataTable dtTypeTemp = dtType.Clone();
            dtTypeTemp.Rows.Clear();
            foreach (DataRow row in dtType.Rows)
            {
                if (SystemConfig.objSessionUser.IsPermission(row["PERMISSION"].ToString().Trim()))
                    dtTypeTemp.Rows.Add(row.ItemArray);
            }

            cboRequestType.InitControl(false, dtTypeTemp, "IMEIEXCLUDEFIFOREQUESTTYPEID", "IMEIEXCLUDEFIFOREQUESTTYPENAME", "--Chọn loại yêu cầu--");

            txtRequestID.Text = string.Empty;
            cboRequestType.ResetDefaultValue();
            cboStoreID.ResetDefaultValue();
            ucRequestUser.UserName = string.Empty;
            txtReviewedStatus.Text = string.Empty;
            ucReviewedUser.UserName = string.Empty;
            txtNote.Text = string.Empty;
            txtBarcode.Text = string.Empty;
            grdIMEI.DataSource = new List<IMEIExcludeFIFORequestDT>();
            lstIMEIExcludeFIFORequestDT_Del = new List<IMEIExcludeFIFORequestDT>();
            objIMEIExcludeFIFORequest = new IMEIExcludeFIFORequest();
        }

        /// <summary>
        /// Hàm cập nhật dữ liệu
        /// Thêm, chỉnh sửa
        /// </summary>
        /// <returns>true or false</returns>
        private bool UpdateData()
        {
            string strMessage = string.Empty;
            try
            {
                if (objIMEIExcludeFIFORequest == null)
                    objIMEIExcludeFIFORequest = new IMEIExcludeFIFORequest();

                objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestID = txtRequestID.Text.Trim();
                objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestTypeID = cboRequestType.ColumnID;
                objIMEIExcludeFIFORequest.RequestStoreID = cboStoreID.ColumnID;
                objIMEIExcludeFIFORequest.RequestUser = ucRequestUser.UserName;
                objIMEIExcludeFIFORequest.Note = txtNote.Text;

                List<IMEIExcludeFIFORequestDT> lstIMEIExcludeFIFORequestDT = grdIMEI.DataSource as List<IMEIExcludeFIFORequestDT>;
                objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestDTList = lstIMEIExcludeFIFORequestDT.ToArray();
                objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestDT_Del_List = lstIMEIExcludeFIFORequestDT_Del.ToArray();
                if (FormState == FormStateType.ADD)
                {
                    objIMEIExcludeFIFORequest.CreatedStoreID = SystemConfig.intDefaultStoreID;
                    objPLCIMEIExcludeFIFORequest.Insert(objIMEIExcludeFIFORequest);
                    strMessage = "Thêm";
                }
                else if (FormState == FormStateType.EDIT)
                {
                    objPLCIMEIExcludeFIFORequest.Update(objIMEIExcludeFIFORequest);
                    strMessage = "Cập nhật";
                }
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this,SystemConfig.objSessionUser.ResultMessageApp.Message,SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                MessageBox.Show(strMessage + " yêu cầu IMEI vi phạm FIFO thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return true;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi " + strMessage.ToLower() + " yêu cầu IMEI vi phạm FIFO");
                SystemErrorWS.Insert("Lỗi " + strMessage.ToLower() + " yêu cầu IMEI vi phạm FIFO", objExce.ToString(), this.Name + " -> UpdateData", DUIInventory_Globals.ModuleName);
                return false;
            }
            
        }

        /// <summary>
        /// Hàm kiểm tra dữ liệu
        /// </summary>
        /// <returns></returns>
        private bool ValidateData()
        {
            if (cboRequestType.ColumnID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn loại yêu cầu!");
                cboRequestType.Focus();
                return false;
            }

            if (cboStoreID.ColumnID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng kho!");
                cboStoreID.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(ucRequestUser.UserName))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn nhân viên yêu cầu!");
                ucRequestUser.Focus();
                return false;
            }

            List<IMEIExcludeFIFORequestDT> lstIMEIExcludeFIFORequestDT = grdIMEI.DataSource as List<IMEIExcludeFIFORequestDT>;
            if (lstIMEIExcludeFIFORequestDT == null || lstIMEIExcludeFIFORequestDT.Count == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không có chi tiết để lưu!");
                return false;
            }

            return true;
        }

        private bool DeleteData()
        {
            DataRow row = grdViewData.GetFocusedDataRow();
            if (row == null)
                return false;

            if(Convert.ToInt32(row["REVIEWEDSTATUS"]) == 1)
            {
                MessageBox.Show("Yêu cầu này đã được duyệt. Không thể xóa.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            DialogResult dr = MessageBox.Show("Bạn có muốn hủy yêu cầu này?", "Hủy", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.No)
                return false;

            string strReasion = string.Empty;
            Library.AppControl.FormCommon.frmInputString frm = new Library.AppControl.FormCommon.frmInputString();
            frm.MaxLengthString = 400;
            frm.IsAllowEmpty = false;
            frm.Text = "Nhập lý do hủy";
            frm.ShowDialog();
            if (frm.IsAccept)
                strReasion = frm.ContentString;

            if (string.IsNullOrEmpty(strReasion))
            {
                MessageBox.Show("Vui lòng nhập lý do hủy.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            objIMEIExcludeFIFORequest = new IMEIExcludeFIFORequest();
            objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestID = row["IMEIEXCLUDEFIFOREQUESTID"].ToString().Trim();
            objIMEIExcludeFIFORequest.DeletedReason = strReasion;
            objPLCIMEIExcludeFIFORequest.Delete(objIMEIExcludeFIFORequest);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }

            MessageBoxObject.ShowInfoMessage(this, "Xóa yêu cầu IMEI vi phạm FIFO thành công !");

            return true;
        }

        /// <summary>
        /// Hàm lấy dữ liệu Chỉnh sửa
        /// </summary>
        private bool EditData()
        {
            try
            {
                DataRow dtRow = (DataRow)grdViewData.GetFocusedDataRow();
                if (dtRow == null)
                    return false;

                if (Convert.ToInt32(dtRow["ISDELETED"]) == 1)
                    return false;

                string strIMEIExcludeFIFORequestID = dtRow["IMEIEXCLUDEFIFOREQUESTID"].ToString().Trim();

                objIMEIExcludeFIFORequest = objPLCIMEIExcludeFIFORequest.LoadInfo(strIMEIExcludeFIFORequestID);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                }

                DataTable dtTypeTemp = dtType.Copy();
                cboRequestType.InitControl(false, dtTypeTemp, "IMEIEXCLUDEFIFOREQUESTTYPEID", "IMEIEXCLUDEFIFOREQUESTTYPENAME", "--Chọn loại yêu cầu--");

                txtRequestID.Text = objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestID;
                intCurrentRequestTypeID = objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestTypeID;
                cboRequestType.SetValue(objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestTypeID);
                intCurrentStoreID = objIMEIExcludeFIFORequest.RequestStoreID;
                cboStoreID.SetValue(objIMEIExcludeFIFORequest.RequestStoreID);
                ucRequestUser.UserName = objIMEIExcludeFIFORequest.RequestUser;
                ucReviewedUser.UserName = objIMEIExcludeFIFORequest.ReviewedUser;
                txtNote.Text = objIMEIExcludeFIFORequest.Note;
                if (dtRow["REVIEWEDSTATUSNAME"] != DBNull.Value)
                    txtReviewedStatus.Text = dtRow["REVIEWEDSTATUSNAME"].ToString().Trim();

                grdIMEI.DataSource = objIMEIExcludeFIFORequest.IMEIExcludeFIFORequestDTList.ToList();
            }
            catch (Exception objExce)
            {

                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin yêu cầu IMEI không FIFO");
                SystemErrorWS.Insert("Lỗi nạp thông tin yêu cầu IMEI không FIFO", objExce.ToString(), this.Name + " -> EditData", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private bool FormatGrid()
        {
            try
            {
                string[] fieldNames = new string[] { "STT", 
                    "IMEIEXCLUDEFIFOREQUESTID", 
                    "REQUESTSTORENAME", 
                    "IMEIEXCLUDEFIFOREQUESTTYPENAME", 
                    "REQUESTUSER", 
                    "REVIEWEDSTATUSNAME", 
                    "REVIEWEDDATE", 
                    "REVIEWEDUSER", 
                    "CREATEDDATE", 
                    "CREATEDUSER", 
                    "UPDATEDDATE", 
                    "UPDATEDUSER", 
                    "DELETEDDATE", 
                    "DELETEDUSER",
                    "ISDELETED"};
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);

                grdViewData.Columns["STT"].Caption = "STT";
                grdViewData.Columns["IMEIEXCLUDEFIFOREQUESTID"].Caption = "Mã yêu cầu";
                grdViewData.Columns["REQUESTSTORENAME"].Caption = "Kho yêu cầu";
                grdViewData.Columns["IMEIEXCLUDEFIFOREQUESTTYPENAME"].Caption = "Loại yêu cầu";
                grdViewData.Columns["REQUESTUSER"].Caption = "Người yêu cầu";
                grdViewData.Columns["REVIEWEDSTATUSNAME"].Caption = "Trạng thái duyệt";
                grdViewData.Columns["REVIEWEDDATE"].Caption = "Ngày duyệt";
                grdViewData.Columns["REVIEWEDUSER"].Caption = "Người duyệt";
                grdViewData.Columns["CREATEDDATE"].Caption = "Ngày tạo";
                grdViewData.Columns["CREATEDUSER"].Caption = "Người tạo";
                grdViewData.Columns["UPDATEDDATE"].Caption = "Ngày cập nhật";
                grdViewData.Columns["UPDATEDUSER"].Caption = "Người cập nhật";
                grdViewData.Columns["DELETEDDATE"].Caption = "Ngày hủy";
                grdViewData.Columns["DELETEDUSER"].Caption = "Người hủy";
                grdViewData.Columns["ISDELETED"].Caption = "Đã hủy";

                grdViewData.OptionsView.ColumnAutoWidth = false;
                grdViewData.Columns["STT"].Width = 50;
                grdViewData.Columns["IMEIEXCLUDEFIFOREQUESTID"].Width = 120;
                grdViewData.Columns["REQUESTSTORENAME"].Width = 120;
                grdViewData.Columns["IMEIEXCLUDEFIFOREQUESTTYPENAME"].Width = 120;
                grdViewData.Columns["REQUESTUSER"].Width = 150;
                grdViewData.Columns["REVIEWEDSTATUSNAME"].Width = 120;
                grdViewData.Columns["REVIEWEDDATE"].Width = 100;
                grdViewData.Columns["REVIEWEDUSER"].Width = 150;
                grdViewData.Columns["CREATEDDATE"].Width = 100;
                grdViewData.Columns["CREATEDUSER"].Width = 150;
                grdViewData.Columns["UPDATEDDATE"].Width = 100;
                grdViewData.Columns["UPDATEDUSER"].Width = 150;
                grdViewData.Columns["DELETEDDATE"].Width = 100;
                grdViewData.Columns["DELETEDUSER"].Width = 150;
                grdViewData.Columns["ISDELETED"].Width = 50;

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsDeleted = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsDeleted.ValueChecked = ((short)(1));
                chkCheckBoxIsDeleted.ValueUnchecked = ((short)(0));
                grdViewData.Columns["ISDELETED"].ColumnEdit = chkCheckBoxIsDeleted;

                grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewData.OptionsView.ShowAutoFilterRow = true;
                grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewData.OptionsView.ShowFooter = false;
                grdViewData.OptionsView.ShowGroupPanel = false;
                grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewData.ColumnPanelRowHeight = 40;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới quản lý");
                SystemErrorWS.Insert("Lỗi định dạng lưới quản lý", objExce.ToString(), this.Name + " -> FormatGrid", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private bool FormatGridIMEI()
        {
            try
            {
                string[] fieldNames = new string[] { "ProductID", "ProductName", "IMEI", "InStockStatusID", "FirstInputDate", "VoucherConcern", "Note" };
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdIMEI, true, false, true, true, fieldNames);
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewIMEI);

                grdViewIMEI.Columns["ProductID"].Caption = "Mã sản phẩm";
                grdViewIMEI.Columns["ProductName"].Caption = "Tên sản phẩm";
                grdViewIMEI.Columns["IMEI"].Caption = "IMEI";
                grdViewIMEI.Columns["InStockStatusID"].Caption = "Trạng thái";
                grdViewIMEI.Columns["FirstInputDate"].Caption = "Ngày nhập";
                grdViewIMEI.Columns["VoucherConcern"].Caption = "Chứng từ liên quan";
                grdViewIMEI.Columns["Note"].Caption = "Ghi chú";
                
                grdViewIMEI.OptionsView.ColumnAutoWidth = false;
                grdViewIMEI.Columns["ProductID"].Width = 120;
                grdViewIMEI.Columns["ProductName"].Width = 200;
                grdViewIMEI.Columns["IMEI"].Width = 100;
                grdViewIMEI.Columns["InStockStatusID"].Width = 100;
                grdViewIMEI.Columns["FirstInputDate"].Width = 80;
                grdViewIMEI.Columns["VoucherConcern"].Width = 150;
                grdViewIMEI.Columns["Note"].Width = 300;

                grdViewIMEI.Columns["FirstInputDate"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                grdViewIMEI.Columns["FirstInputDate"].DisplayFormat.FormatString = "dd/MM/yyyy";
                
                grdViewIMEI.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewIMEI.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewIMEI.OptionsView.ShowAutoFilterRow = true;
                grdViewIMEI.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewIMEI.OptionsView.ShowFooter = false;
                grdViewIMEI.OptionsView.ShowGroupPanel = false;
                grdViewIMEI.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                //grdViewIMEI.ColumnPanelRowHeight = 40;

                DataTable dtProductStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
                DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboInstockStatus = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
                DevExpress.XtraEditors.Controls.LookUpColumnInfo col = new DevExpress.XtraEditors.Controls.LookUpColumnInfo();
                col.Caption = "Mã";
                col.FieldName = "PRODUCTSTATUSID";
                cboInstockStatus.Columns.Add(col);
                DevExpress.XtraEditors.Controls.LookUpColumnInfo col1 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo();
                col1.Caption = "Tên";
                col1.FieldName = "PRODUCTSTATUSNAME";
                cboInstockStatus.Columns.Add(col1);
                cboInstockStatus.DisplayMember = "PRODUCTSTATUSNAME";
                cboInstockStatus.ValueMember = "PRODUCTSTATUSID";
                cboInstockStatus.DataSource = dtProductStatus;
                grdViewIMEI.Columns["InStockStatusID"].ColumnEdit = cboInstockStatus;

                grdViewIMEI.Columns["Note"].OptionsColumn.AllowEdit = true;

            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới chi tiết");
                SystemErrorWS.Insert("Lỗi định dạng lưới chi tiết", objExce.ToString(), this.Name + " -> FormatGridIMEI", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void chkCheckBoxIsSelect_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit chkEdit = (DevExpress.XtraEditors.CheckEdit)sender;
            grdViewData.SetFocusedRowCellValue("ISSELECT", Convert.ToDecimal(chkEdit.Checked));
        }

        private List<MasterData.PLC.MD.WSProductSpec_SpecStatus.ProductSpec_SpecStatus> lstProductSpecStatus = null;
        private DataTable dtbDataSpecStatus = null;

        #endregion
        
        #region Form Status
        /// <summary>
        /// Các trạng thái của Form
        /// </summary>
        enum FormStateType
        {
            LIST,
            ADD,
            EDIT
        }

        FormStateType intFormState = FormStateType.LIST;
        FormStateType FormState
        {
            set
            {
                intFormState = value;
                if (intFormState == FormStateType.LIST)
                {
                    //Kiểm tra quyền:
                    btnAdd.Enabled = mnuItemAdd.Enabled = bolPermission_Add;
                    btnDelete.Enabled = mnuItemDelete.Enabled = bolPermission_Delete && grdViewData.RowCount > 0 && !chkIsDelete.Checked;
                    btnEdit.Enabled = mnuItemEdit.Enabled = (bolPermission_Review || bolPermission_Edit) && grdViewData.RowCount > 0;
                    btnReview.Enabled = false;
                    btnUndo.Enabled = false;
                    btnUpdate.Enabled = false;
                    btnExportExcel.Enabled = mnuItemExport.Enabled = grdViewData.RowCount > 0;
                    tabControl.SelectedTab = tabList;
                    grdViewData.ClearColumnsFilter();
                    grdViewData.Columns["DELETEDDATE"].Visible = chkIsDelete.Checked;
                    grdViewData.Columns["DELETEDUSER"].Visible = chkIsDelete.Checked;
                }
                else if (intFormState == FormStateType.ADD || intFormState == FormStateType.EDIT)
                {
                    btnAdd.Enabled = false;
                    btnEdit.Enabled = false;
                    btnReview.Enabled = false;
                    btnDelete.Enabled = false;
                    btnExportExcel.Enabled = false;
                    btnUndo.Enabled = true;
                    btnUpdate.Enabled = true;
                    tabControl.SelectedTab = tabDetail;

                    if (intFormState == FormStateType.ADD)
                    {
                        txtBarcode.Enabled = true;
                        txtRequestID.ReadOnly = true;
                        txtRequestID.BackColor = SystemColors.Info;
                        cboStoreID.Enabled = true;
                        cboRequestType.Enabled = true;
                    }
                    else
                    {
                        txtBarcode.Enabled = string.IsNullOrEmpty(ucReviewedUser.UserName);
                        txtRequestID.ReadOnly = true;
                        txtRequestID.BackColor = SystemColors.Info;
                        btnUpdate.Enabled = btnDelete.Enabled = string.IsNullOrEmpty(ucReviewedUser.UserName);
                        btnReview.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Review) && string.IsNullOrEmpty(ucReviewedUser.UserName);
                        lstIMEIExcludeFIFORequestDT_Del = new List<IMEIExcludeFIFORequestDT>();
                        cboStoreID.Enabled = false;
                        cboRequestType.Enabled = false;
                    }
                }
            }
            get
            {
                return intFormState;
            }
        }

        #endregion                     

        private void mnuItemDelIMEI_Click(object sender, EventArgs e)
        {
            grdViewIMEI.PostEditor();
            IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT = grdViewIMEI.GetFocusedRow() as IMEIExcludeFIFORequestDT;
            if (objIMEIExcludeFIFORequestDT == null)
                return;

            List<IMEIExcludeFIFORequestDT> lstIMEIExcludeFIFORequestDT = grdIMEI.DataSource as List<IMEIExcludeFIFORequestDT>;
            if (lstIMEIExcludeFIFORequestDT == null || lstIMEIExcludeFIFORequestDT.Count == 0)
                return;

            if (MessageBox.Show(this, "Bạn chắc chắn muốn xóa IMEI này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;

            if (!string.IsNullOrEmpty(objIMEIExcludeFIFORequestDT.IMEIExcludeFIFORequestDTID))
            {
                lstIMEIExcludeFIFORequestDT_Del.Add(objIMEIExcludeFIFORequestDT);
            }

            lstIMEIExcludeFIFORequestDT.Remove(objIMEIExcludeFIFORequestDT);
            grdIMEI.DataSource = lstIMEIExcludeFIFORequestDT;
            grdIMEI.RefreshDataSource();
        }   

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!txtBarcode.Enabled)
                return;
            if (cboStoreID.ColumnID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Bạn chưa chọn kho.");
                cboStoreID.Focus();
                return;
            }

            if (cboRequestType.ColumnID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Bạn chưa loại yêu cầu.");
                cboRequestType.Focus();
                return;
            }

            if (e.KeyChar == 13)
            {
                string strBarcode = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(txtBarcode.Text.Trim());
                if (strBarcode.Length < 1)
                    return;

                AddBarcode(strBarcode, cboStoreID.ColumnID);
                txtBarcode.Text = string.Empty;
                txtBarcode.Focus();
            }
        }

        private void AddBarcode(string strBarcode, int intStoreID)
        {
            if (!CheckObject.CheckIMEI(strBarcode))
            {
                MessageBoxObject.ShowWarningMessage(this, "Mã IMEI không đúng định dạng");
                return;
            }

            List<IMEIExcludeFIFORequestDT> lstIMEIExcludeFIFORequestDT = grdIMEI.DataSource as List<IMEIExcludeFIFORequestDT>;
            if (lstIMEIExcludeFIFORequestDT.Any(x => x.IMEI == strBarcode))
            {
                MessageBoxObject.ShowWarningMessage(this, "IMEI đã tồn tại trên lưới");
                return;
            }

            if (strBarcode.Length > 0)
            {
                ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock = new ERP.Inventory.PLC.PLCProductInStock().LoadInfo(strBarcode, intStoreID);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }

                if (objProductInStock == null || string.IsNullOrEmpty(objProductInStock.IMEI))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Không lấy được thông tin IMEI.");
                    return;
                }

                if (objProductInStock.InStockStatusID != 1)//Chỉ được bắn IMEI mới
                {
                    MessageBoxObject.ShowWarningMessage(this, "Chỉ cho phép bắn IMEI mới.");
                    return;
                }

                object[] objKetwords = new object[]{"@IMEI",objProductInStock.IMEI,
                                                    "@STOREID", cboStoreID.ColumnID,
                                                    "@IMEIEXCLUDEFIFOREQUESTTYPEID", cboRequestType.ColumnID,
                                                    "@IMEIEXCLUDEFIFOREQUESTID", txtRequestID.Text.Trim()};
                DataTable dtDataCheck = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_IMEIEXCLUDEFIFOREQUEST_CHK", objKetwords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }

                IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDT = new IMEIExcludeFIFORequestDT();
                objIMEIExcludeFIFORequestDT.ProductID = objProductInStock.ProductID;
                objIMEIExcludeFIFORequestDT.ProductName = objProductInStock.ProductName;
                objIMEIExcludeFIFORequestDT.IMEI = objProductInStock.IMEI;
                objIMEIExcludeFIFORequestDT.InStockStatusID = objProductInStock.InStockStatusID;
                objIMEIExcludeFIFORequestDT.FirstInputDate = objProductInStock.InputVoucherDate;
                lstIMEIExcludeFIFORequestDT.Add(objIMEIExcludeFIFORequestDT);

                grdIMEI.DataSource = lstIMEIExcludeFIFORequestDT;
                grdIMEI.RefreshDataSource();
            }
        }

        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            DataRow row = (DataRow)grdViewData.GetFocusedDataRow();
            if(row == null)
                return;

            mnuItemAdd.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Add);
            mnuItemDelete.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Delete) && Convert.ToInt32(row["ISDELETED"]) == 0 && Convert.ToInt32(row["REVIEWEDSTATUS"]) != 1;
            mnuItemEdit.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Edit) && Convert.ToInt32(row["ISDELETED"]) == 0;
        }

        private int intCurrentStoreID = -1;
        private void cboStoreID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if(cboStoreID.ColumnID != intCurrentStoreID && grdViewIMEI.RowCount > 0)
            {
                DialogResult dr = MessageBox.Show("Thay đổi kho sẽ xóa dữ liệu trên lưới. Bạn có muốn thay đổi?", "Thông báo",MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if(dr == DialogResult.No)
                {
                    cboStoreID.SetValue(intCurrentStoreID);
                    return;
                }
                else
                {
                    List<IMEIExcludeFIFORequestDT> lstIMEIExcludeFIFORequestDT = new List<IMEIExcludeFIFORequestDT>();
                    grdIMEI.DataSource = lstIMEIExcludeFIFORequestDT;
                    grdIMEI.RefreshDataSource();
                }
            }

            intCurrentStoreID = cboStoreID.ColumnID;
        }

        private int intCurrentRequestTypeID = -1;
        private void cboRequestType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboRequestType.ColumnID != intCurrentRequestTypeID && grdViewIMEI.RowCount > 0)
            {
                DialogResult dr = MessageBox.Show("Thay đổi loại yêu cầu sẽ xóa dữ liệu trên lưới. Bạn có muốn thay đổi?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.No)
                {
                    cboRequestType.SetValue(intCurrentRequestTypeID);
                    return;
                }
                else
                {
                    List<IMEIExcludeFIFORequestDT> lstIMEIExcludeFIFORequestDT = new List<IMEIExcludeFIFORequestDT>();
                    grdIMEI.DataSource = lstIMEIExcludeFIFORequestDT;
                    grdIMEI.RefreshDataSource();
                }
            }

            intCurrentRequestTypeID = cboRequestType.ColumnID;
        }

        private void mnuIMEI_Opening(object sender, CancelEventArgs e)
        {
            mnuItemDelIMEI.Enabled = grdViewIMEI.RowCount > 0 && btnUpdate.Enabled;
            mnuItemIMEIImportExcel.Enabled = btnUpdate.Enabled;
        }

        private void mnuItemIMEIImportExcel_Click(object sender, EventArgs e)
        {
            if (cboRequestType.ColumnID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn loại yêu cầu!");
                cboRequestType.Focus();
                return;
            }

            if (cboStoreID.ColumnID < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng kho!");
                cboStoreID.Focus();
                return;
            }
            if (grdViewIMEI.RowCount > 0)
            {
                if (MessageBox.Show(this, "Dữ liệu trên lưới sẽ bị xóa. Bạn có muốn tiếp tục không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                {
                    return;
                }
            }
            List<IMEIExcludeFIFORequestDT> lstOld = grdIMEI.DataSource as List<IMEIExcludeFIFORequestDT>;
            foreach(var obj in lstOld)
            {
                lstIMEIExcludeFIFORequestDT_Del.Add(obj);
            }
            List<IMEIExcludeFIFORequestDT> lst = new List<IMEIExcludeFIFORequestDT>();
            grdIMEI.DataSource = lst;
            grdIMEI.RefreshDataSource();

            ERP.Inventory.DUI.StoreChange.frmIMEIExcludeFIFORequestImportExcel frm = new StoreChange.frmIMEIExcludeFIFORequestImportExcel();
            frm.IMEIExcludeFIFORequestType = cboRequestType.ColumnID;
            frm.RequestStoreID = cboStoreID.ColumnID;
            frm.strRequestID = txtRequestID.Text.Trim();
            frm.ShowDialog();

            if (frm.lstIMEIExcludeFIFORequestDT == null || frm.lstIMEIExcludeFIFORequestDT.Count == 0)
                return;

            List<IMEIExcludeFIFORequestDT> lstIMEIExcludeFIFORequestDT = grdIMEI.DataSource as List<IMEIExcludeFIFORequestDT>;
            if(lstIMEIExcludeFIFORequestDT == null)
                lstIMEIExcludeFIFORequestDT = new List<IMEIExcludeFIFORequestDT>();

            foreach(IMEIExcludeFIFORequestDT objIMEIExcludeFIFORequestDTImport in frm.lstIMEIExcludeFIFORequestDT)
            {
                if (lstIMEIExcludeFIFORequestDT.Any(x => x.IMEI.ToString().Trim() == objIMEIExcludeFIFORequestDTImport.IMEI.ToString().Trim()))
                    continue;

                lstIMEIExcludeFIFORequestDT.Add(objIMEIExcludeFIFORequestDTImport);
            }

            grdIMEI.DataSource = lstIMEIExcludeFIFORequestDT;
            grdIMEI.RefreshDataSource();
        }

        private void chkIsDelete_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
