﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.LoadControls;
using System.Collections;
using ERP.MasterData.PLC.MD;
using Library.AppControl;
using Library.AppCore;
using GemBox.Spreadsheet;
using NPOI.XSSF.UserModel;
using ERP.Report.DUI;
using NPOI.SS.UserModel;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmInventoryTermStock : Form
    {
        public frmInventoryTermStock()
        {
            InitializeComponent();
        }

        #region variable

        private string strPermission_Add = "INV_INVENTORYTERM_ADD";
        private string strPermission_Delete = "INV_INVENTORYTERM_DELETE";
        private string strPermission_Edit = "INV_INVENTORYTERM_EDIT";
        private string strPermission_ExportExcel = "INV_INVENTORYTERM_EXPORTEXCEL";
        private string strPermission__DeleteSearch = "INV_INVENTORYTERM_ISDELETED_SRH";
        private ERP.Inventory.PLC.Inventory.PLCInventoryTerm objPLCInventoryTerm = new PLC.Inventory.PLCInventoryTerm();
        private ERP.Inventory.PLC.Inventory.WSInventoryTerm.InventoryTerm objInventoryTerm = new PLC.Inventory.WSInventoryTerm.InventoryTerm();
        private ERP.Inventory.PLC.Inventory.WSInventoryTerm.ResultMessage objResultMessage = null;
        private DataTable dtbStore = null;
        private DataTable dtbMainGroup = null;
        private DataTable dtbBrand = null;
        private DataTable dtbSearch = null;
        private bool bolIsShowMessWaring = false;
        private int intIsLoadEdit = -1;
        private DataTable dtbSubGroup = null;
        private DataTable dtbSubGroupAll = null;
        private bool bolIsSelect = false;
        private DataTable dtbDataCauseGroups = null;
        #endregion

        #region method

        private void LoadComboBox()
        {
            Library.AppCore.LoadControls.SetDataSource.SetInventoryType(this, cboInvTypeSearch);
            cboBranch.InitControl(false);
            cboStoreIDList.InitControl(false);
            cboReportType.SelectedIndex = 0;
            cboInvTypeSearch_SelectionChangeCommitted(null, null);

            //Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            //cboStoreIDList.InitControl(true, true);
            //string strUserName = SystemConfig.objSessionUser.UserName;
            //object[] objKeywords = new object[]
            //{
            //    "@InventoryTermID", -1
            //};

            //object[] objKeywords_GetAllStore = new object[] 
            //{
            //    "@InventoryTermID", -1,
            //    "@UserName", strUserName
            //};

            //dtbStore = objPLCInventoryTerm.GetAllStore(objKeywords_GetAllStore);
            //dtbMainGroup = objPLCInventoryTerm.GetAllMainGroup(objKeywords);
            //dtbBrand = objPLCInventoryTerm.GetAllBrand(objKeywords);

            //dtbSubGroup = new DataTable();
            //dtbSubGroup.Columns.Add("IsSelect", typeof(Boolean));
            //dtbSubGroup.Columns.Add("SubGroupID", typeof(Int32));
            //dtbSubGroup.Columns.Add("SubGroupName", typeof(String));
            //dtbSubGroup.Columns.Add("InventoryTermID", typeof(Int32));
            //dtbSubGroup.Columns.Add("InventoryDate", typeof(DateTime));
            //dtbSubGroup.Columns.Add("MainGroupID", typeof(Int32));
            //dtbSubGroup.PrimaryKey = new DataColumn[] { dtbSubGroup.Columns["SubGroupID"] };
            //dtbSubGroup.TableName = "Inv_Term_SubGroup";

            //dtbSubGroupAll = Library.AppCore.DataSource.GetDataSource.GetSubGroup().Copy();
        }

        private bool CheckSearch()
        {
            if (Globals.DateDiff(dtpToDate.Value, dtpFromDate.Value, Globals.DateDiffType.DATE) < 0)
            {
                MessageBox.Show("Từ ngày không được lớn hơn đến ngày yêu cầu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtpToDate.Focus();
                return false;
            }
            if (Globals.DateDiff(dtpToDate.Value, dtpFromDate.Value, Globals.DateDiffType.DATE) > 92)
            {
                MessageBox.Show("Vui lòng tìm kiếm trong thời gian 3 tháng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtpToDate.Focus();
                return false;
            }

            if (cboInvTypeSearch.SelectedIndex == 0)
            {
                MessageBox.Show("Vui lòng chọn loại kiểm kê.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboInvTypeSearch.Focus();
                return false;
            }

            if (cboInventoryTerm.ColumnID <= 0)
            {
                MessageBox.Show("Vui lòng chọn kỳ kiểm kê.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboInventoryTerm.Focus();
                return false;
            }

            if (cboReportType.SelectedIndex != 4 && cboReportType.SelectedIndex != 5 && cboReportType.SelectedIndex != 6 && cboReportType.SelectedIndex != 7)
            {
                if (cboStoreIDList.StoreID <= 0)
                {
                    MessageBox.Show("Vui lòng chọn kho.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboStoreIDList.Focus();
                    return false;
                }
            }

            return true;
        }

        private void SearchData()
        {
            string strUserName = SystemConfig.objSessionUser.UserName;

            string strStoreIDList = string.Empty;
            if (cboReportType.SelectedIndex == 4
                || cboReportType.SelectedIndex == 5
                || cboReportType.SelectedIndex == 6
                || cboReportType.SelectedIndex == 7)
                strStoreIDList = cboStoreIDList.StoreIDList;
            else
                strStoreIDList = "<" + cboStoreIDList.StoreID + ">";

            object[] objKeywords = new object[]
            {
                "@INVENTORYTERMID", cboInventoryTerm.ColumnID,
                "@INVENTORYTYPEID", Convert.ToInt32(cboInvTypeSearch.SelectedValue),
                "@STOREIDLIST", strStoreIDList,
                "@ReportType", cboReportType.SelectedIndex
            };
            string strStore = "INV_INVENTORYTERMSTOCK_SRH";
            if (cboReportType.SelectedIndex == 5
                || cboReportType.SelectedIndex == 6)
                strStore = "INV_INVENTORYTERMSTOCK_SRH2";
            if (cboReportType.SelectedIndex == 7)
                strStore = "INV_INVENTORYTERMSTOCK_SRH3";
            string[] strs = new string[] { "v_out1", "v_out2" };
            if (cboReportType.SelectedIndex == 5
               || cboReportType.SelectedIndex == 6)
                strs = new string[] { "v_out1", "v_out2", "v_out3" };
            if (cboReportType.SelectedIndex == 7)
                strs = new string[] { "V_PL1", "V_PL2", "V_PL3", "V_PL4", "V_PL5", "V_PL1_1" };
            // DataSet ds = new ERP.Report.PLC.PLCReportDataSource().GetDataSetByJson("INV_INVENTORYTERMSTOCK_SRH", new string[] { "v_out1", "v_out2" }, objKeywords);
            DataSet ds = new ERP.Report.PLC.PLCReportDataSource().GetDataSet(strStore, strs, objKeywords);

            //DataTable dtData = new ERP.Report.PLC.PLCReportDataSource().GetHeavyDataSource("INV_INVENTORYTERMSTOCK_SRH", objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }

            DataTable dtData1 = null;
            DataTable dtData2 = null;

            if (ds != null && ds.Tables.Count >= 2)
            {
                dtData1 = ds.Tables[0];
                dtData2 = ds.Tables[1];
            }

            if (dtData1 == null || dtData1.Rows.Count == 0)
            {
                MessageBox.Show("Không tìm thấy dữ liệu để xuất.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (cboReportType.SelectedIndex == 0)
                ExportProduct(dtData1);
            else if (cboReportType.SelectedIndex == 1)
                ExportProduct01B(dtData1);
            else if (cboReportType.SelectedIndex == 2)
                ExportProductStatus01C(dtData1);
            else if (cboReportType.SelectedIndex == 3)
                ExportProductStatus01D(dtData1);
            else if (cboReportType.SelectedIndex == 5)
                ExportPhuLuc01(ds);
            else if (cboReportType.SelectedIndex == 6)
                ExportDetail(ds);
            else if (cboReportType.SelectedIndex == 7)
                ExportBCKK(ds);
            else
            {
                if (dtData2 == null || dtData2.Rows.Count == 0)
                {
                    MessageBox.Show("Không tìm thấy dữ liệu để xuất.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                ExportProductStatusValue(dtData1, dtData2);
            }

            //CreateDataExport(dtData, dtProductStatus);
        }

        private void CreateDataExport(DataTable dtData, DataTable dtProductStatus)
        {
            DataTable dtProductStatusDistinct = DataTableClass.SelectDistinct(dtProductStatus, "PRODUCTSTATUSID");
            decimal decSumQuantity = 0;
            if (dtProductStatusDistinct != null)
            {
                foreach (DataRow row in dtProductStatusDistinct.Rows)
                {
                    dtData.Columns.Add("PRODUCTSTATUSID_" + row["PRODUCTSTATUSID"].ToString().Trim(), typeof(decimal));
                    foreach (DataRow rowData in dtData.Rows)
                    {
                        string strCondition = string.Format(@"PRODUCTID ='{0}' AND PRODUCTSTATUSID = {1}", rowData["PRODUCTID"].ToString().Trim(), row["PRODUCTSTATUSID"].ToString().Trim());
                        object decSum = dtProductStatus.Compute("Sum(QUANTITY)", strCondition);
                        if (decSum != DBNull.Value)
                            decSumQuantity = Convert.ToDecimal(decSum);
                        else
                            decSumQuantity = 0;

                        rowData["PRODUCTSTATUSID_" + row["PRODUCTSTATUSID"].ToString().Trim()] = decSumQuantity;
                    }
                }
            }

            Export(dtData, dtProductStatus);
        }

        #endregion

        #region event

        private void frmInventoryTerm_Load(object sender, EventArgs e)
        {
            LoadComboBox();
            dtbDataCauseGroups = new PLC.Inventory.PLCInventory().SearchDataCauseGroups();
        }

        private void txtKeywords_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!CheckSearch())
                return;

            Library.AppCore.Forms.frmWaitDialog.Show("Xem báo cáo", "Đang xử lý...");
            SearchData();
            Library.AppCore.Forms.frmWaitDialog.Close();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        #endregion

        private void cboBranch_SelectionChangeCommitted(object sender, EventArgs e)
        {

            if (cboReportType.SelectedIndex == 4
                || cboReportType.SelectedIndex == 5
                || cboReportType.SelectedIndex == 6
                || cboReportType.SelectedIndex == 7
                )
            {
                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                if (cboBranch.BranchID > 0)
                    objStoreFilter.OtherCondition = "BRANCHID=" + cboBranch.BranchID;

                cboStoreIDList.InitControl(true, objStoreFilter);
            }
            else
            {
                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                if (cboBranch.BranchID > 0)
                    objStoreFilter.OtherCondition = "BRANCHID=" + cboBranch.BranchID;

                cboStoreIDList.InitControl(false, objStoreFilter);
            }
        }

        #region Export 01A

        public void ExportProduct(DataTable dtbData)
        {
            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strSheetName = string.Empty;
            strSheetName = "Mau 01A-KKHH";
            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\InventoryTermStockProduct01A.xlsx";
            if (CheckFileTemplate(strFileTemplate, ref strPathFileName) == false) return;
            if (ExportExcelProduct(dtbData, strFileTemplate, strPathFileName, strSheetName))
            {
                OpenFileExport(strPathFileName);
                return;
            }
            else
            {
                return;
            }
        }

        public bool ExportExcel2(DataTable dtbData, string strFileTemplate, string strPathFileName)
        {
            try
            {
                XSSFWorkbook workbook = null;
                workbook = ExcelHelperXSSF.InitializeXSSFWorkbookFromFile(workbook, strFileTemplate);
                if (workbook != null)
                {
                    ICellStyle styleTextBoldCenter = ExcelHelperXSSF.InitializeCellStyle(workbook, 12, false, false, true, false, COLOR.BLUE, NPOI.SS.UserModel.HorizontalAlignment.Center, VerticalAlignment.Center, FORMATEXCEL.TEXT);
                    ICellStyle styleTextNormalLeft = ExcelHelperXSSF.InitializeCellStyle(workbook, 12, false, false, false, false, COLOR.BLUE, NPOI.SS.UserModel.HorizontalAlignment.Left, VerticalAlignment.Center, FORMATEXCEL.TEXT);
                    ICellStyle style = ExcelHelperXSSF.InitializeCellStyle(workbook, 12, false, false, false, true, COLOR.BLACK, NPOI.SS.UserModel.HorizontalAlignment.Left, VerticalAlignment.Center, FORMATEXCEL.TEXT);
                    ICellStyle styleCenter = ExcelHelperXSSF.InitializeCellStyle(workbook, 12, false, false, false, false, COLOR.BLACK, NPOI.SS.UserModel.HorizontalAlignment.Center, VerticalAlignment.Center, FORMATEXCEL.TEXT);
                    ICellStyle styleDate = ExcelHelperXSSF.InitializeCellStyle(workbook, 12, false, false, false, true, COLOR.BLACK, NPOI.SS.UserModel.HorizontalAlignment.Left, VerticalAlignment.Center, FORMATEXCEL.DATE);
                    ICellStyle styleNumber = ExcelHelperXSSF.InitializeCellStyle(workbook, 12, false, false, false, true, COLOR.BLACK, NPOI.SS.UserModel.HorizontalAlignment.Right, VerticalAlignment.Center, FORMATEXCEL.INT);
                    ISheet sheet = workbook.GetSheetAt(0);
                    if (sheet != null)
                    {
                        #region Insert Header
                        string strStoreName = dtbData.Rows[0]["STORENAME"].ToString().Trim();
                        DateTime? dtReviewedDate = null;
                        DataRow[] lstRowReviewed = dtbData.Select("REVIEWUNEVENTTIME IS NOT NULL");
                        if (lstRowReviewed != null && lstRowReviewed.Count() > 0)
                            dtReviewedDate = Convert.ToDateTime(lstRowReviewed[0]["REVIEWUNEVENTTIME"]);

                        string strReviewedDate = "Ngày .…  tháng …. năm ….";
                        string strReviewedTime = "…….giờ … phút";
                        string strInventoryTermName = string.Format(@"Kỳ kiểm kê: {0}", cboInventoryTerm.ColumnName.ToString().Trim());
                        if (dtReviewedDate != null)
                        {
                            strReviewedDate = string.Format(@"Ngày {0} tháng {1} năm {2}", dtReviewedDate.Value.Day, dtReviewedDate.Value.Month, dtReviewedDate.Value.Year);
                            strReviewedTime = string.Format(@"{0} giờ {1} phút", dtReviewedDate.Value.Hour, dtReviewedDate.Value.Minute);
                        }

                        string strReason = string.Format(@"Hôm nay, {0}, siêu thị {1} tiến hành kiểm kê vào lúc:{2}. Kết quả kiểm kê như sau:", strReviewedDate, strStoreName, strReviewedTime);
                        string strPrintDate = string.Format(@"………….., Ngày {0} Tháng {1} Năm {2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);

                        ExcelHelperXSSF.SetCellValue(sheet, 3, ExcelHelperXSSF.GetColumnNumberByColumnName("A"), strStoreName, styleTextBoldCenter);
                        ExcelHelperXSSF.SetCellValue(sheet, 6, ExcelHelperXSSF.GetColumnNumberByColumnName("A"), strReviewedDate, styleTextBoldCenter);
                        ExcelHelperXSSF.SetCellValue(sheet, 7, ExcelHelperXSSF.GetColumnNumberByColumnName("A"), strInventoryTermName, styleTextBoldCenter);
                        ExcelHelperXSSF.SetCellValue(sheet, 15, ExcelHelperXSSF.GetColumnNumberByColumnName("A"), strReason, styleTextNormalLeft);
                        ExcelHelperXSSF.SetCellValue(sheet, 27, ExcelHelperXSSF.GetColumnNumberByColumnName("I"), strPrintDate, styleTextNormalLeft);

                        #endregion

                        #region Insert Data


                        int iRowStart = 23;
                        int iRowCurrent = iRowStart;
                        int intSTT = 1;
                        int intMainGroupIDTemp = 0;
                        int intSubGroupIDTemp = 0;
                        int intMainGroupIndex = 0;
                        int intSubGroupIndex = 0;
                        bool bolInsertMainGroup = false;
                        bool bolInsertSubGroup = false;

                        foreach (DataRow row in dtbData.Rows)
                        {
                            if (Convert.ToInt32(row["MAINGROUPID"]) == intMainGroupIDTemp)
                                bolInsertMainGroup = false;
                            else
                            {
                                bolInsertMainGroup = true;
                                intMainGroupIDTemp = Convert.ToInt32(row["MAINGROUPID"]);
                                intMainGroupIndex++;
                            }

                            if (Convert.ToInt32(row["SUBGROUPID"]) == intSubGroupIDTemp)
                                bolInsertSubGroup = false;
                            else
                            {
                                bolInsertSubGroup = true;
                                intSubGroupIDTemp = Convert.ToInt32(row["SUBGROUPID"]);
                                intSubGroupIndex++;
                            }

                            if (bolInsertMainGroup)
                            {
                                if (iRowStart < iRowCurrent)
                                {
                                    ExcelHelperXSSF.SetCellValue(sheet, iRowCurrent, ExcelHelperXSSF.GetColumnNumberByColumnName("A"), row["MAINGROUPNAME"].ToString(), style);
                                    iRowCurrent++;
                                }
                                else
                                {
                                    ExcelHelperXSSF.SetCellValue(sheet, 21, ExcelHelperXSSF.GetColumnNumberByColumnName("A"), row["MAINGROUPNAME"].ToString(), style);
                                }
                            }

                            if (bolInsertMainGroup)
                            {
                                if (iRowStart != iRowCurrent)
                                {
                                    ExcelHelperXSSF.SetCellValue(sheet, iRowCurrent, ExcelHelperXSSF.GetColumnNumberByColumnName("A"), row["SUBGROUPNAME"].ToString(), style);
                                    iRowCurrent++;
                                }
                                else
                                {
                                    ExcelHelperXSSF.SetCellValue(sheet, 22, ExcelHelperXSSF.GetColumnNumberByColumnName("A"), row["SUBGROUPNAME"].ToString(), style);
                                }
                            }

                            ExcelHelperXSSF.SetCellValue(sheet, iRowCurrent, ExcelHelperXSSF.GetColumnNumberByColumnName("A"), intSTT.ToString(), style);
                            ExcelHelperXSSF.SetCellValue(sheet, iRowCurrent, ExcelHelperXSSF.GetColumnNumberByColumnName("B"), row["PRODUCTNAME"].ToString().ToString(), style);
                            ExcelHelperXSSF.SetCellValue(sheet, iRowCurrent, ExcelHelperXSSF.GetColumnNumberByColumnName("C"), row["PCONSIGNMENTTYPENAME"].ToString(), style);
                            ExcelHelperXSSF.SetCellValue(sheet, iRowCurrent, ExcelHelperXSSF.GetColumnNumberByColumnName("D"), row["QUANTITYUNITNAME"].ToString(), style);
                            ExcelHelperXSSF.SetCellValue(sheet, iRowCurrent, ExcelHelperXSSF.GetColumnNumberByColumnName("E"), Convert.ToDecimal(row["QUANTITY"]), styleNumber);
                            ExcelHelperXSSF.SetCellValue(sheet, iRowCurrent, ExcelHelperXSSF.GetColumnNumberByColumnName("F"), row["IMEI"].ToString(), style);
                            ExcelHelperXSSF.SetCellValue(sheet, iRowCurrent, ExcelHelperXSSF.GetColumnNumberByColumnName("G"), Convert.ToDecimal(row["CHECKQUANTITY"]), styleNumber);
                            ExcelHelperXSSF.SetCellValue(sheet, iRowCurrent, ExcelHelperXSSF.GetColumnNumberByColumnName("H"), row["INVENTORYIMEI"].ToString(), style);
                            ExcelHelperXSSF.SetCellValue(sheet, iRowCurrent, ExcelHelperXSSF.GetColumnNumberByColumnName("I"), Convert.ToDecimal(row["RESIDUALQUANTITY"]), styleNumber);
                            ExcelHelperXSSF.SetCellValue(sheet, iRowCurrent, ExcelHelperXSSF.GetColumnNumberByColumnName("J"), Convert.ToDecimal(row["MISSQUANTITY"]), styleNumber);
                            ExcelHelperXSSF.SetCellValue(sheet, iRowCurrent, ExcelHelperXSSF.GetColumnNumberByColumnName("K"), string.Format(@"=I{0}-J{0}", iRowCurrent), styleNumber);
                            ExcelHelperXSSF.SetCellValue(sheet, iRowCurrent, ExcelHelperXSSF.GetColumnNumberByColumnName("L"), row["EXPLAINNOTE"].ToString(), style);

                            iRowCurrent++;
                            intSTT++;
                        }
                        #endregion
                        ExcelHelperXSSF.WriteToFileXSSF(strPathFileName, workbook);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return true;
        }

        private bool ExportExcelProduct(DataTable dtbData, string strFileTemplate, string strPathFileName, string strSheetName)
        {
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
            }
            catch { }

            if (workbook != null)
            {
                ExcelWorksheet sheet = (ExcelWorksheet)workbook.Worksheets[0];
                if (sheet != null)
                {
                    #region Insert Header
                    string strStoreName = dtbData.Rows[0]["STORENAME"].ToString().Trim();
                    DateTime? dtReviewedDate = null;
                    DataRow[] lstRowReviewed = dtbData.Select("REVIEWUNEVENTTIME IS NOT NULL");
                    if (lstRowReviewed != null && lstRowReviewed.Count() > 0)
                        dtReviewedDate = Convert.ToDateTime(lstRowReviewed[0]["REVIEWUNEVENTTIME"]);

                    string strReviewedDate = "Ngày .…  tháng …. năm ….";
                    string strReviewedTime = "…….giờ … phút";
                    string strInventoryTermName = string.Format(@"Kỳ kiểm kê: {0}", cboInventoryTerm.ColumnName.ToString().Trim());
                    if (dtReviewedDate != null)
                    {
                        strReviewedDate = string.Format(@"Ngày {0} tháng {1} năm {2}", dtReviewedDate.Value.Day, dtReviewedDate.Value.Month, dtReviewedDate.Value.Year);
                        strReviewedTime = string.Format(@"{0} giờ {1} phút", dtReviewedDate.Value.Hour, dtReviewedDate.Value.Minute);
                    }

                    string strReason = string.Format(@"Hôm nay, {0}, siêu thị {1} tiến hành kiểm kê vào lúc:{2}. Kết quả kiểm kê như sau:", strReviewedDate, strStoreName, strReviewedTime);
                    string strPrintDate = string.Format(@"………….., Ngày {0} Tháng {1} Năm {2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);

                    InsertValueCell(ref sheet, "A", 3, strStoreName, false, true);
                    InsertValueCell(ref sheet, "A", 6, strReviewedDate, false, true);
                    InsertValueCell(ref sheet, "A", 7, strInventoryTermName, false, true);
                    InsertValueCell(ref sheet, "A", 15, strReason, false, true);
                    InsertValueCell(ref sheet, "J", 27, strPrintDate, false, false);

                    #endregion

                    #region Import Row

                    int iRowStart = 21;
                    int iRowCurrent = iRowStart;
                    int intSTT = 1;
                    int intMainGroupIDTemp = 0;
                    int intSubGroupIDTemp = 0;
                    int intMainGroupIndex = 0;
                    int intSubGroupIndex = 0;
                    bool bolInsertMainGroup = false;
                    bool bolInsertSubGroup = false;
                    for (int i = 0; i < dtbData.Rows.Count; i++)
                    {
                        //if (Convert.ToInt32(dtbData.Rows[i]["MAINGROUPID"]) == intMainGroupIDTemp)
                        //    bolInsertMainGroup = false;
                        //else
                        //{
                        //    bolInsertMainGroup = true;
                        //    intMainGroupIDTemp = Convert.ToInt32(dtbData.Rows[i]["MAINGROUPID"]);
                        //    intMainGroupIndex++;
                        //}

                        //if (Convert.ToInt32(dtbData.Rows[i]["SUBGROUPID"]) == intSubGroupIDTemp)
                        //    bolInsertSubGroup = false;
                        //else
                        //{
                        //    bolInsertSubGroup = true;
                        //    intSubGroupIDTemp = Convert.ToInt32(dtbData.Rows[i]["SUBGROUPID"]);
                        //    intSubGroupIndex++;
                        //}

                        AddRowProduct(ref sheet, dtbData.Rows[i], ref iRowCurrent, iRowStart, ref intSTT, intMainGroupIndex, intSubGroupIndex, bolInsertMainGroup, bolInsertSubGroup);
                    }

                    InsertValueCell(ref sheet, "E", iRowCurrent, string.Format(@"=SUM(E{0}:E{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "G", iRowCurrent, string.Format(@"=SUM(G{0}:G{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "I", iRowCurrent, string.Format(@"=SUM(I{0}:I{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "J", iRowCurrent, string.Format(@"=SUM(J{0}:J{1})", iRowStart, iRowCurrent - 1), false, true);

                    InsertValueCell(ref sheet, "F", iRowCurrent, string.Format(@"=SUM(F{0}:F{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "H", iRowCurrent, string.Format(@"=SUM(H{0}:H{1})", iRowStart, iRowCurrent - 1), false, true);

                    #endregion
                }
            }
            try
            {
                workbook.SaveXlsx(strPathFileName);

                #region Export pdf

                //Microsoft.Office.Interop.Excel.Application excelApplication;
                //Microsoft.Office.Interop.Excel.Workbook excelWorkbook;

                //// Create new instance of Excel
                //excelApplication = new Microsoft.Office.Interop.Excel.Application();

                //// Make the process invisible to the user
                //excelApplication.ScreenUpdating = false;

                //// Make the process silent
                //excelApplication.DisplayAlerts = false;

                //// Open the workbook that you wish to export to PDF
                //excelWorkbook = excelApplication.Workbooks.Open(strPathFileName);

                //// If the workbook failed to open, stop, clean up, and bail out
                //if (excelWorkbook == null)
                //{
                //    excelApplication.Quit();

                //    excelApplication = null;
                //    excelWorkbook = null;

                //    return false;
                //}

                //var exportSuccessful = true;
                //try
                //{
                //    // Call Excel's native export function (valid in Office 2007 and Office 2010, AFAIK)
                //    object misValue = System.Reflection.Missing.Value;
                //    excelWorkbook.ActiveSheet.PageSetup.Orientation =Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
                //    excelWorkbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, strPathFileName, misValue,true);
                //}
                //catch (System.Exception ex)
                //{
                //    // Mark the export as failed for the return value...
                //    exportSuccessful = false;

                //    // Do something with any exceptions here, if you wish...
                //    // MessageBox.Show...        
                //}
                //finally
                //{
                //    // Close the workbook, quit the Excel, and clean up regardless of the results...
                //    excelWorkbook.Close();
                //    excelApplication.Quit();

                //    excelApplication = null;
                //    excelWorkbook = null;
                //}

                //// You can use the following method to automatically open the PDF after export if you wish
                //// Make sure that the file actually exists first...
                //if (System.IO.File.Exists(strPathFileName))
                //{
                //    System.Diagnostics.Process.Start(strPathFileName);
                //}

                #endregion

                return true;
            }
            catch
            {
                return false;
            }
        }

        public void AddRowProduct(ref ExcelWorksheet sheet, DataRow row, ref int iRow, int iRowTemplate, ref int iSTT, int intMainGroupIndex, int intSubGroupIndex, bool bolInsertMainGroup, bool bolInsertSubGroup)
        {
            if (bolInsertMainGroup)
            {
                if (iRowTemplate < iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(21);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "A", iRow, row["MAINGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "A", 21, row["MAINGROUPNAME"].ToString(), false, true);
                }
            }

            if (bolInsertMainGroup)
            {
                if (iRowTemplate != iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(22);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "B", iRow, row["SUBGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "B", 22, row["SUBGROUPNAME"].ToString(), false, true);
                }
            }

            if (iRowTemplate != iRow)
            {
                ExcelRow rowTemplate = sheet.GetRow(iRowTemplate);
                rowTemplate.InsertCopy(iRow, false);
            }

            InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
            InsertValueCell(ref sheet, "B", iRow, row["PRODUCTID"].ToString(), false, false);
            InsertValueCell(ref sheet, "C", iRow, row["PRODUCTNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "D", iRow, row["PCONSIGNMENTTYPENAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "E", iRow, row["QUANTITYUNITNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "F", iRow, Convert.ToDecimal(row["QUANTITY"]), false, false);
            InsertValueCell(ref sheet, "G", iRow, row["IMEI"].ToString(), false, false);
            InsertValueCell(ref sheet, "H", iRow, Convert.ToDecimal(row["CHECKQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "I", iRow, row["INVENTORYIMEI"].ToString(), false, false);
            InsertValueCell(ref sheet, "J", iRow, Convert.ToDecimal(row["RESIDUALQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "K", iRow, Convert.ToDecimal(row["MISSQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "L", iRow, string.Format(@"=J{0}-K{0}", iRow), false, false);
            //InsertValueCell(ref sheet, "M", iRow, row["EXPLAINNOTE"].ToString(), false, false);

            iSTT++;
            iRow++;
        }

        #endregion

        #region Export 01B

        public void ExportProduct01B(DataTable dtbData)
        {
            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strSheetName = string.Empty;
            strSheetName = "Mau 01A-KKHH";
            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\InventoryTermStockProduct01B.xlsx";
            if (CheckFileTemplate(strFileTemplate, ref strPathFileName) == false) return;
            if (ExportExcelProduct01B(dtbData, strFileTemplate, strPathFileName, strSheetName))
            {
                OpenFileExport(strPathFileName);
                return;
            }
            else
            {
                return;
            }
        }

        private bool ExportExcelProduct01B(DataTable dtbData, string strFileTemplate, string strPathFileName, string strSheetName)
        {
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
            }
            catch { }

            if (workbook != null)
            {
                ExcelWorksheet sheet = (ExcelWorksheet)workbook.Worksheets[0];
                if (sheet != null)
                {
                    #region Insert Header
                    string strStoreName = dtbData.Rows[0]["STORENAME"].ToString().Trim();
                    DateTime? dtReviewedDate = null;
                    DataRow[] lstRowReviewed = dtbData.Select("REVIEWUNEVENTTIME IS NOT NULL");
                    if (lstRowReviewed != null && lstRowReviewed.Count() > 0)
                        dtReviewedDate = Convert.ToDateTime(lstRowReviewed[0]["REVIEWUNEVENTTIME"]);

                    string strReviewedDate = "Ngày .…  tháng …. năm ….";
                    string strReviewedTime = "…….giờ … phút";
                    string strInventoryTermName = string.Format(@"Kỳ kiểm kê: {0}", cboInventoryTerm.ColumnName.ToString().Trim());
                    if (dtReviewedDate != null)
                    {
                        strReviewedDate = string.Format(@"Ngày {0} tháng {1} năm {2}", dtReviewedDate.Value.Day, dtReviewedDate.Value.Month, dtReviewedDate.Value.Year);
                        strReviewedTime = string.Format(@"{0} giờ {1} phút", dtReviewedDate.Value.Hour, dtReviewedDate.Value.Minute);
                    }

                    string strReason = string.Format(@"Hôm nay, {0}, siêu thị {1} tiến hành kiểm kê vào lúc:{2}. Kết quả kiểm kê như sau:", strReviewedDate, strStoreName, strReviewedTime);
                    string strPrintDate = string.Format(@"………….., Ngày {0} Tháng {1} Năm {2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);

                    InsertValueCell(ref sheet, "A", 3, strStoreName, false, true);
                    InsertValueCell(ref sheet, "A", 7, strReviewedDate, false, true);
                    InsertValueCell(ref sheet, "A", 8, strInventoryTermName, false, true);
                    InsertValueCell(ref sheet, "A", 16, strReason, false, true);
                    InsertValueCell(ref sheet, "J", 26, strPrintDate, false, false);

                    #endregion

                    #region Import Row

                    int iRowStart = 23;
                    int iRowCurrent = iRowStart;
                    int intSTT = 1;
                    int intMainGroupIDTemp = 0;
                    int intSubGroupIDTemp = 0;
                    int intMainGroupIndex = 0;
                    int intSubGroupIndex = 0;
                    bool bolInsertMainGroup = false;
                    bool bolInsertSubGroup = false;
                    for (int i = 0; i < dtbData.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dtbData.Rows[i]["MAINGROUPID"]) == intMainGroupIDTemp)
                            bolInsertMainGroup = false;
                        else
                        {
                            bolInsertMainGroup = true;
                            intMainGroupIDTemp = Convert.ToInt32(dtbData.Rows[i]["MAINGROUPID"]);
                            intMainGroupIndex++;
                        }

                        if (Convert.ToInt32(dtbData.Rows[i]["SUBGROUPID"]) == intSubGroupIDTemp)
                            bolInsertSubGroup = false;
                        else
                        {
                            bolInsertSubGroup = true;
                            intSubGroupIDTemp = Convert.ToInt32(dtbData.Rows[i]["SUBGROUPID"]);
                            intSubGroupIndex++;
                        }

                        AddRowProduct01B(ref sheet, dtbData.Rows[i], ref iRowCurrent, iRowStart, ref intSTT, intMainGroupIndex, intSubGroupIndex, bolInsertMainGroup, bolInsertSubGroup);
                    }

                    InsertValueCell(ref sheet, "E", iRowCurrent, string.Format(@"=SUM(E{0}:E{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "F", iRowCurrent, string.Format(@"=SUM(F{0}:F{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "I", iRowCurrent, string.Format(@"=SUM(I{0}:I{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "J", iRowCurrent, string.Format(@"=SUM(J{0}:J{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "K", iRowCurrent, string.Format(@"=SUM(K{0}:K{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "L", iRowCurrent, string.Format(@"=SUM(L{0}:L{1})", iRowStart, iRowCurrent - 1), false, true);

                    #endregion
                }
            }
            try
            {
                workbook.SaveXlsx(strPathFileName);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public void AddRowProduct01B(ref ExcelWorksheet sheet, DataRow row, ref int iRow, int iRowTemplate, ref int iSTT, int intMainGroupIndex, int intSubGroupIndex, bool bolInsertMainGroup, bool bolInsertSubGroup)
        {
            if (bolInsertMainGroup)
            {
                if (iRowTemplate < iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(21);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "A", iRow, row["MAINGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "A", 21, row["MAINGROUPNAME"].ToString(), false, true);
                }
            }

            if (bolInsertMainGroup)
            {
                if (iRowTemplate != iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(22);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "B", iRow, row["SUBGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "B", 22, row["SUBGROUPNAME"].ToString(), false, true);
                }
            }

            if (iRowTemplate != iRow)
            {
                ExcelRow rowTemplate = sheet.GetRow(iRowTemplate);
                rowTemplate.InsertCopy(iRow, false);
            }

            InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
            InsertValueCell(ref sheet, "B", iRow, row["PRODUCTNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "C", iRow, row["PCONSIGNMENTTYPENAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "D", iRow, row["QUANTITYUNITNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "E", iRow, Convert.ToDecimal(row["QUANTITY"]), false, false);
            InsertValueCell(ref sheet, "F", iRow, Convert.ToDecimal(row["CHECKQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "G", iRow, string.Format(@"=E{0}-F{0}", iRow), false, false);
            InsertValueCell(ref sheet, "H", iRow, row["EXPLAINNOTE"].ToString(), false, false);
            InsertValueCell(ref sheet, "I", iRow, Convert.ToDecimal(row["QUANTITY_SALE"]), false, false);
            InsertValueCell(ref sheet, "J", iRow, Convert.ToDecimal(row["QUANTITY_STORECHANGE"]), false, false);
            InsertValueCell(ref sheet, "K", iRow, Convert.ToDecimal(row["QUANTITY_OUTPUT"]), false, false);
            InsertValueCell(ref sheet, "L", iRow, Convert.ToDecimal(row["QUANTITY_INPUT"]), false, false);
            InsertValueCell(ref sheet, "M", iRow, string.Format(@"=F{0}+I{0}+J{0}+K{0}-L{0}", iRow), false, false);
            InsertValueCell(ref sheet, "N", iRow, string.Format(@"=E{0}-M{0}", iRow), false, false);

            iSTT++;
            iRow++;
        }

        #endregion

        #region Export 01C

        public void ExportProductStatus01C(DataTable dtbData)
        {
            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strSheetName = string.Empty;
            strSheetName = "Mau 01A-KKHH";
            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\InventoryTermStockProductStatus01C.xlsx";
            if (CheckFileTemplate(strFileTemplate, ref strPathFileName) == false) return;
            if (ExportExcelProductStatus01C(dtbData, strFileTemplate, strPathFileName, strSheetName))
            {
                OpenFileExport(strPathFileName);
                return;
            }
            else
            {
                return;
            }
        }

        private bool ExportExcelProductStatus01C(DataTable dtbData, string strFileTemplate, string strPathFileName, string strSheetName)
        {
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
            }
            catch { }

            if (workbook != null)
            {
                ExcelWorksheet sheet = (ExcelWorksheet)workbook.Worksheets[0];
                if (sheet != null)
                {

                    #region Insert Header
                    string strStoreName = dtbData.Rows[0]["STORENAME"].ToString().Trim();
                    DateTime? dtReviewedDate = null;
                    DataRow[] lstRowReviewed = dtbData.Select("REVIEWUNEVENTTIME IS NOT NULL");
                    if (lstRowReviewed != null && lstRowReviewed.Count() > 0)
                        dtReviewedDate = Convert.ToDateTime(lstRowReviewed[0]["REVIEWUNEVENTTIME"]);

                    string strReviewedDate = "Ngày .…  tháng …. năm ….";
                    string strReviewedTime = "…….giờ … phút";
                    string strInventoryTermName = string.Format(@"Kỳ kiểm kê: {0}", cboInventoryTerm.ColumnName.ToString().Trim());
                    if (dtReviewedDate != null)
                    {
                        strReviewedDate = string.Format(@"Ngày {0} tháng {1} năm {2}", dtReviewedDate.Value.Day, dtReviewedDate.Value.Month, dtReviewedDate.Value.Year);
                        strReviewedTime = string.Format(@"{0} giờ {1} phút", dtReviewedDate.Value.Hour, dtReviewedDate.Value.Minute);
                    }

                    string strReason = string.Format(@"Hôm nay, {0}, siêu thị {1} tiến hành kiểm kê vào lúc:{2}. Kết quả kiểm kê như sau:", strReviewedDate, strStoreName, strReviewedTime);
                    string strPrintDate = string.Format(@"………….., Ngày {0} Tháng {1} Năm {2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);

                    InsertValueCell(ref sheet, "A", 3, strStoreName, false, true);
                    InsertValueCell(ref sheet, "A", 6, strReviewedDate, false, true);
                    InsertValueCell(ref sheet, "A", 7, strInventoryTermName, false, true);
                    InsertValueCell(ref sheet, "A", 15, strReason, false, true);
                    InsertValueCell(ref sheet, "I", 26, strPrintDate, false, false);

                    #endregion

                    #region Import Row

                    int iRowStart = 22;
                    int iRowCurrent = iRowStart;
                    int intSTT = 1;
                    int intMainGroupIDTemp = 0;
                    int intSubGroupIDTemp = 0;
                    int intMainGroupIndex = 0;
                    int intSubGroupIndex = 0;
                    bool bolInsertMainGroup = false;
                    bool bolInsertSubGroup = false;
                    for (int i = 0; i < dtbData.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dtbData.Rows[i]["MAINGROUPID"]) == intMainGroupIDTemp)
                            bolInsertMainGroup = false;
                        else
                        {
                            bolInsertMainGroup = true;
                            intMainGroupIDTemp = Convert.ToInt32(dtbData.Rows[i]["MAINGROUPID"]);
                            intMainGroupIndex++;
                        }

                        if (Convert.ToInt32(dtbData.Rows[i]["SUBGROUPID"]) == intSubGroupIDTemp)
                            bolInsertSubGroup = false;
                        else
                        {
                            bolInsertSubGroup = true;
                            intSubGroupIDTemp = Convert.ToInt32(dtbData.Rows[i]["SUBGROUPID"]);
                            intSubGroupIndex++;
                        }

                        AddRowProductStatus01C(ref sheet, dtbData.Rows[i], ref iRowCurrent, iRowStart, ref intSTT, intMainGroupIndex, intSubGroupIndex, bolInsertMainGroup, bolInsertSubGroup);
                    }

                    InsertValueCell(ref sheet, "E", iRowCurrent, string.Format(@"=SUM(E{0}:E{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "H", iRowCurrent, string.Format(@"=SUM(H{0}:H{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "K", iRowCurrent, string.Format(@"=SUM(K{0}:K{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "L", iRowCurrent, string.Format(@"=SUM(L{0}:L{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "M", iRowCurrent, string.Format(@"=SUM(M{0}:M{1})", iRowStart, iRowCurrent - 1), false, true);

                    #endregion
                }
            }
            try
            {
                workbook.SaveExcel(strPathFileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void AddRowProductStatus01C(ref ExcelWorksheet sheet, DataRow row, ref int iRow, int iRowTemplate, ref int iSTT, int intMainGroupIndex, int intSubGroupIndex, bool bolInsertMainGroup, bool bolInsertSubGroup)
        {
            if (bolInsertMainGroup)
            {
                if (iRowTemplate < iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(20);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "A", iRow, row["MAINGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "A", 20, row["MAINGROUPNAME"].ToString(), false, true);
                }
            }

            if (bolInsertMainGroup)
            {
                if (iRowTemplate != iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(21);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "B", iRow, row["SUBGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "B", 21, row["SUBGROUPNAME"].ToString(), false, true);
                }
            }

            if (iRowTemplate != iRow)
            {
                ExcelRow rowTemplate = sheet.GetRow(iRowTemplate);
                sheet.GetRow(iRowTemplate);
                rowTemplate.InsertCopy(iRow, false);
            }

            InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
            InsertValueCell(ref sheet, "B", iRow, row["PRODUCTNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "C", iRow, row["PCONSIGNMENTTYPENAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "D", iRow, row["QUANTITYUNITNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "E", iRow, Convert.ToDecimal(row["QUANTITY"]), false, false);
            InsertValueCell(ref sheet, "F", iRow, row["IMEI"].ToString(), false, false);
            InsertValueCell(ref sheet, "G", iRow, row["CUR_PRODUCTSTATUSNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "H", iRow, Convert.ToDecimal(row["CHECKQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "I", iRow, row["INVENTORYIMEI"].ToString(), false, false);
            InsertValueCell(ref sheet, "J", iRow, row["PRODUCTSTATUSNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "K", iRow, Convert.ToDecimal(row["RESIDUALQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "L", iRow, Convert.ToDecimal(row["MISSQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "M", iRow, string.Format(@"=K{0}-L{0}", iRow), false, false);
            //InsertValueCell(ref sheet, "N", iRow, row["EXPLAINNOTE"].ToString(), false, false);
            //InsertValueCell(ref sheet, "O", iRow, Convert.ToDecimal(row["QUANTITY_SALE"]), false, false);
            //InsertValueCell(ref sheet, "P", iRow, Convert.ToDecimal(row["QUANTITY_STORECHANGE"]), false, false);
            //InsertValueCell(ref sheet, "Q", iRow, Convert.ToDecimal(row["QUANTITY_OUTPUT"]), false, false);
            //InsertValueCell(ref sheet, "R", iRow, Convert.ToDecimal(row["QUANTITY_INPUT"]), false, false);
            iSTT++;

            iRow++;
        }

        #endregion

        #region Export 01D

        public void ExportProductStatus01D(DataTable dtbData)
        {
            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strFileName = string.Empty;
            string strSheetName = string.Empty;
            strSheetName = "Mau 01A-KKHH";
            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\InventoryTermStockProductStatus01D.xlsx";
            if (CheckFileTemplate(strFileTemplate, ref strPathFileName) == false) return;

            strFileName = System.IO.Path.GetFileNameWithoutExtension(strPathFileName);
            if (ExportExcelProductStatus01D(dtbData, strFileTemplate, strFileName, strPathFileName, strSheetName))
            {
                //OpenFileExport(strPathFileName);
                return;
            }
            else
            {
                return;
            }
        }

        private bool ExportExcelProductStatus01D(DataTable dtbData, string strFileTemplate, string strFileName, string strPathFileName, string strSheetName)
        {
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
            }
            catch { }

            if (workbook != null)
            {
                ExcelWorksheet sheet = (ExcelWorksheet)workbook.Worksheets[0];
                if (sheet != null)
                {
                    #region Insert Header
                    string strStoreName = dtbData.Rows[0]["STORENAME"].ToString().Trim();
                    DateTime? dtReviewedDate = null;
                    DataRow[] lstRowReviewed = dtbData.Select("REVIEWUNEVENTTIME IS NOT NULL");
                    if (lstRowReviewed != null && lstRowReviewed.Count() > 0)
                        dtReviewedDate = Convert.ToDateTime(lstRowReviewed[0]["REVIEWUNEVENTTIME"]);

                    string strReviewedDate = "Ngày .…  tháng …. năm ….";
                    string strReviewedTime = "…….giờ … phút";
                    string strInventoryTermName = string.Format(@"Kỳ kiểm kê: {0}", cboInventoryTerm.ColumnName.ToString().Trim());
                    if (dtReviewedDate != null)
                    {
                        strReviewedDate = string.Format(@"Ngày {0} tháng {1} năm {2}", dtReviewedDate.Value.Day, dtReviewedDate.Value.Month, dtReviewedDate.Value.Year);
                        strReviewedTime = string.Format(@"{0} giờ {1} phút", dtReviewedDate.Value.Hour, dtReviewedDate.Value.Minute);
                    }

                    string strReason = string.Format(@"Hôm nay, {0}, siêu thị {1} tiến hành kiểm kê vào lúc:{2}. Kết quả kiểm kê như sau:", strReviewedDate, strStoreName, strReviewedTime);
                    string strPrintDate = string.Format(@"………….., Ngày {0} Tháng {1} Năm {2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);

                    InsertValueCell(ref sheet, "A", 3, strStoreName, false, true);
                    InsertValueCell(ref sheet, "A", 6, strReviewedDate, false, true);
                    InsertValueCell(ref sheet, "A", 7, strInventoryTermName, false, true);
                    InsertValueCell(ref sheet, "A", 15, strReason, false, true);
                    InsertValueCell(ref sheet, "H", 25, strPrintDate, false, false);

                    #endregion

                    #region Import Row

                    int iRowStart = 22;
                    int iRowCurrent = iRowStart;
                    int intSTT = 1;
                    int intMainGroupIDTemp = 0;
                    int intSubGroupIDTemp = 0;
                    int intMainGroupIndex = 0;
                    int intSubGroupIndex = 0;
                    bool bolInsertMainGroup = false;
                    bool bolInsertSubGroup = false;
                    for (int i = 0; i < dtbData.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dtbData.Rows[i]["MAINGROUPID"]) == intMainGroupIDTemp)
                            bolInsertMainGroup = false;
                        else
                        {
                            bolInsertMainGroup = true;
                            intMainGroupIDTemp = Convert.ToInt32(dtbData.Rows[i]["MAINGROUPID"]);
                            intMainGroupIndex++;
                        }

                        if (Convert.ToInt32(dtbData.Rows[i]["SUBGROUPID"]) == intSubGroupIDTemp)
                            bolInsertSubGroup = false;
                        else
                        {
                            bolInsertSubGroup = true;
                            intSubGroupIDTemp = Convert.ToInt32(dtbData.Rows[i]["SUBGROUPID"]);
                            intSubGroupIndex++;
                        }

                        AddRowProductStatus01D(ref sheet, dtbData.Rows[i], ref iRowCurrent, iRowStart, ref intSTT, intMainGroupIndex, intSubGroupIndex, bolInsertMainGroup, bolInsertSubGroup);
                    }

                    InsertValueCell(ref sheet, "E", iRowCurrent, string.Format(@"=SUM(E{0}:E{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "G", iRowCurrent, string.Format(@"=SUM(G{0}:G{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "I", iRowCurrent, string.Format(@"=SUM(I{0}:I{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "J", iRowCurrent, string.Format(@"=SUM(J{0}:J{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "K", iRowCurrent, string.Format(@"=SUM(K{0}:K{1})", iRowStart, iRowCurrent - 1), false, true);

                    #endregion
                }
            }
            try
            {
                workbook.SaveExcel(strPathFileName);

                #region Export pdf

                Microsoft.Office.Interop.Excel.Application excelApplication;
                Microsoft.Office.Interop.Excel.Workbook excelWorkbook;

                // Create new instance of Excel
                excelApplication = new Microsoft.Office.Interop.Excel.Application();

                // Make the process invisible to the user
                excelApplication.ScreenUpdating = false;

                // Make the process silent
                excelApplication.DisplayAlerts = false;

                // Open the workbook that you wish to export to PDF
                excelWorkbook = excelApplication.Workbooks.Open(strPathFileName);

                // If the workbook failed to open, stop, clean up, and bail out
                if (excelWorkbook == null)
                {
                    excelApplication.Quit();

                    excelApplication = null;
                    excelWorkbook = null;

                    return false;
                }

                var exportSuccessful = true;
                string strFileNamePDF = strPathFileName.Replace(".xlsx", ".pdf").Replace(".xls", ".pdf");
                try
                {
                    // Call Excel's native export function (valid in Office 2007 and Office 2010, AFAIK)
                    object misValue = System.Reflection.Missing.Value;
                    //excelWorkbook.ActiveSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
                    //excelWorkbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, strFileNamePDF);

                    excelWorkbook.ActiveSheet.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, strFileNamePDF,
                    Microsoft.Office.Interop.Excel.XlFixedFormatQuality.xlQualityMinimum, true, false, Type.Missing, Type.Missing, false, Type.Missing);
                }
                catch (System.Exception ex)
                {
                    exportSuccessful = false;
                }
                finally
                {
                    // Close the workbook, quit the Excel, and clean up regardless of the results...
                    excelWorkbook.Close();
                    excelApplication.Quit();

                    excelApplication = null;
                    excelWorkbook = null;
                }

                //Delete excel file
                if (System.IO.File.Exists(strPathFileName))
                {
                    System.IO.FileInfo fi = new System.IO.FileInfo(strPathFileName);
                    try
                    {
                        fi.Delete();
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }

                if (System.IO.File.Exists(strFileNamePDF))
                {
                    System.Diagnostics.Process.Start(strFileNamePDF);
                }

                #endregion

                return true;
            }
            catch
            {
                return false;
            }
        }

        public void AddRowProductStatus01D(ref ExcelWorksheet sheet, DataRow row, ref int iRow, int iRowTemplate, ref int iSTT, int intMainGroupIndex, int intSubGroupIndex, bool bolInsertMainGroup, bool bolInsertSubGroup)
        {
            if (bolInsertMainGroup)
            {
                if (iRowTemplate < iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(20);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "A", iRow, row["MAINGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "A", 20, row["MAINGROUPNAME"].ToString(), false, true);
                }
            }

            if (bolInsertMainGroup)
            {
                if (iRowTemplate != iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(21);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "B", iRow, row["SUBGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "B", 21, row["SUBGROUPNAME"].ToString(), false, true);
                }
            }

            if (iRowTemplate != iRow)
            {
                ExcelRow rowTemplate = sheet.GetRow(iRowTemplate);
                sheet.GetRow(iRowTemplate);
                rowTemplate.InsertCopy(iRow, false);
            }

            InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
            InsertValueCell(ref sheet, "B", iRow, row["PRODUCTNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "C", iRow, row["PCONSIGNMENTTYPENAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "D", iRow, row["QUANTITYUNITNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "E", iRow, Convert.ToDecimal(row["QUANTITY"]), false, false);
            InsertValueCell(ref sheet, "F", iRow, row["CUR_PRODUCTSTATUSNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "G", iRow, Convert.ToDecimal(row["CHECKQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "H", iRow, row["PRODUCTSTATUSNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "I", iRow, string.Format(@"=E{0}-G{0}", iRow), false, false);
            InsertValueCell(ref sheet, "J", iRow, Convert.ToDecimal(row["EXCHANGEQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "K", iRow, string.Format(@"=E{0}-J{0}", iRow), false, false);
            iSTT++;

            iRow++;
        }

        #endregion

        #region Export 01B/KKHH

        public void ExportProductStatusValue(DataTable dtbData1, DataTable dtbData2)
        {
            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strSheetName = string.Empty;
            strSheetName = "PL.01";
            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\InventoryTermStockProductStatusValue.xlsx";
            if (CheckFileTemplate(strFileTemplate, ref strPathFileName) == false) return;
            if (ExportExcelProductStatusValue(dtbData1, dtbData2, strFileTemplate, strPathFileName, strSheetName))
            {
                OpenFileExport(strPathFileName);
                return;
            }
            else
            {
                return;
            }
        }

        private bool ExportExcelProductStatusValue(DataTable dtbData1, DataTable dtbData2, string strFileTemplate, string strPathFileName, string strSheetName)
        {
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
            }
            catch { }

            if (workbook != null)
            {
                ExcelWorksheet sheet = (ExcelWorksheet)workbook.Worksheets[0];
                if (sheet != null)
                {
                    string strStoreName = dtbData1.Rows[0]["STORENAME"].ToString().Trim();
                    DateTime? dtReviewedDate = null;
                    DataRow[] lstRowReviewed = dtbData1.Select("REVIEWUNEVENTTIME IS NOT NULL");
                    if (lstRowReviewed != null && lstRowReviewed.Count() > 0)
                        dtReviewedDate = Convert.ToDateTime(lstRowReviewed[0]["REVIEWUNEVENTTIME"]);

                    string strReviewedDate = "Ngày .…  tháng …. năm ….";
                    string strReviewedTime = "…….giờ … phút";
                    string strInventoryTermName = string.Format(@"Kỳ kiểm kê: {0}", cboInventoryTerm.ColumnName.ToString().Trim());
                    if (dtReviewedDate != null)
                    {
                        strReviewedDate = string.Format(@"Ngày {0} tháng {1} năm {2}", dtReviewedDate.Value.Day, dtReviewedDate.Value.Month, dtReviewedDate.Value.Year);
                        strReviewedTime = string.Format(@"{0} giờ {1} phút", dtReviewedDate.Value.Hour, dtReviewedDate.Value.Minute);
                    }

                    string strReason = string.Format(@"Hôm nay, {0}, tiến hành kiểm kê vào lúc:{1}. Kết quả kiểm kê như sau:", strReviewedDate, strReviewedTime);
                    string strPrintDate = string.Format(@"………….., Ngày {0} Tháng {1} Năm {2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);

                    #region Tổng hợp

                    #region Insert Header

                    //InsertValueCell(ref sheet, "A", 3, strStoreName, false, true);
                    InsertValueCell(ref sheet, "A", 7, strReviewedDate, false, true);
                    InsertValueCell(ref sheet, "A", 8, strInventoryTermName, false, true);
                    InsertValueCell(ref sheet, "A", 16, strReason, false, true);
                    InsertValueCell(ref sheet, "R", 28, strPrintDate, false, false);

                    #endregion

                    #region Import Row

                    int iRowStart = 25;
                    int iRowCurrent = iRowStart;
                    int intSTT = 1;
                    int intMainGroupIDTemp = 0;
                    int intSubGroupIDTemp = 0;
                    int intMainGroupIndex = 0;
                    int intSubGroupIndex = 0;
                    bool bolInsertMainGroup = false;
                    bool bolInsertSubGroup = false;
                    for (int i = 0; i < dtbData1.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dtbData1.Rows[i]["MAINGROUPID"]) == intMainGroupIDTemp)
                            bolInsertMainGroup = false;
                        else
                        {
                            bolInsertMainGroup = true;
                            intMainGroupIDTemp = Convert.ToInt32(dtbData1.Rows[i]["MAINGROUPID"]);
                            intMainGroupIndex++;
                        }

                        if (Convert.ToInt32(dtbData1.Rows[i]["SUBGROUPID"]) == intSubGroupIDTemp)
                            bolInsertSubGroup = false;
                        else
                        {
                            bolInsertSubGroup = true;
                            intSubGroupIDTemp = Convert.ToInt32(dtbData1.Rows[i]["SUBGROUPID"]);
                            intSubGroupIndex++;
                        }
                        AddRowProductStatusValue(ref sheet, dtbData1.Rows[i], ref iRowCurrent, iRowStart, ref intSTT, 0, 0, bolInsertMainGroup, bolInsertSubGroup);
                    }

                    InsertValueCell(ref sheet, "G", iRowCurrent, string.Format(@"=SUM(G{0}:G{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "H", iRowCurrent, string.Format(@"=SUM(H{0}:H{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "I", iRowCurrent, string.Format(@"=SUM(I{0}:I{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "J", iRowCurrent, string.Format(@"=SUM(J{0}:J{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "K", iRowCurrent, string.Format(@"=SUM(K{0}:K{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "L", iRowCurrent, string.Format(@"=SUM(L{0}:L{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "N", iRowCurrent, string.Format(@"=SUM(N{0}:N{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "O", iRowCurrent, string.Format(@"=SUM(O{0}:O{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "P", iRowCurrent, string.Format(@"=SUM(P{0}:P{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "Q", iRowCurrent, string.Format(@"=SUM(Q{0}:Q{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "R", iRowCurrent, string.Format(@"=SUM(R{0}:R{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "S", iRowCurrent, string.Format(@"=SUM(S{0}:S{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "T", iRowCurrent, string.Format(@"=SUM(T{0}:T{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "U", iRowCurrent, string.Format(@"=SUM(U{0}:U{1})", iRowStart, iRowCurrent - 1), false, true);

                    #endregion

                    #endregion

                    ExcelWorksheet sheetDetail = (ExcelWorksheet)workbook.Worksheets[1];
                    if (sheetDetail != null)
                    {
                        #region Chi tiết

                        #region Insert Header

                        //InsertValueCell(ref sheetDetail, "A", 3, strStoreName, false, true);
                        InsertValueCell(ref sheetDetail, "A", 7, strReviewedDate, false, true);
                        InsertValueCell(ref sheetDetail, "A", 8, strInventoryTermName, false, true);
                        InsertValueCell(ref sheetDetail, "A", 16, strReason, false, true);
                        InsertValueCell(ref sheetDetail, "W", 28, strPrintDate, false, false);

                        #endregion

                        #region Import Row

                        iRowStart = 25;
                        iRowCurrent = iRowStart;
                        intSTT = 1;
                        intMainGroupIDTemp = 0;
                        intSubGroupIDTemp = 0;
                        intMainGroupIndex = 0;
                        intSubGroupIndex = 0;
                        bolInsertMainGroup = false;
                        bolInsertSubGroup = false;
                        for (int i = 0; i < dtbData2.Rows.Count; i++)
                        {
                            if (Convert.ToInt32(dtbData2.Rows[i]["MAINGROUPID"]) == intMainGroupIDTemp)
                                bolInsertMainGroup = false;
                            else
                            {
                                bolInsertMainGroup = true;
                                intMainGroupIDTemp = Convert.ToInt32(dtbData2.Rows[i]["MAINGROUPID"]);
                                intMainGroupIndex++;
                            }

                            if (Convert.ToInt32(dtbData2.Rows[i]["SUBGROUPID"]) == intSubGroupIDTemp)
                                bolInsertSubGroup = false;
                            else
                            {
                                bolInsertSubGroup = true;
                                intSubGroupIDTemp = Convert.ToInt32(dtbData2.Rows[i]["SUBGROUPID"]);
                                intSubGroupIndex++;
                            }
                            AddRowProductStatusValueDetail(ref sheetDetail, dtbData2.Rows[i], ref iRowCurrent, iRowStart, ref intSTT, 0, 0, bolInsertMainGroup, bolInsertSubGroup);
                        }

                        InsertValueCell(ref sheetDetail, "G", iRowCurrent, string.Format(@"=SUM(G{0}:G{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "H", iRowCurrent, string.Format(@"=SUM(H{0}:H{1})", iRowStart, iRowCurrent - 1), false, true);

                        InsertValueCell(ref sheetDetail, "K", iRowCurrent, string.Format(@"=SUM(K{0}:K{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "L", iRowCurrent, string.Format(@"=SUM(L{0}:L{1})", iRowStart, iRowCurrent - 1), false, true);

                        InsertValueCell(ref sheetDetail, "O", iRowCurrent, string.Format(@"=SUM(O{0}:O{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "P", iRowCurrent, string.Format(@"=SUM(P{0}:P{1})", iRowStart, iRowCurrent - 1), false, true);

                        InsertValueCell(ref sheetDetail, "S", iRowCurrent, string.Format(@"=SUM(S{0}:S{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "T", iRowCurrent, string.Format(@"=SUM(T{0}:T{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "U", iRowCurrent, string.Format(@"=SUM(U{0}:U{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "V", iRowCurrent, string.Format(@"=SUM(V{0}:V{1})", iRowStart, iRowCurrent - 1), false, true);

                        InsertValueCell(ref sheetDetail, "W", iRowCurrent, string.Format(@"=SUM(W{0}:W{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "X", iRowCurrent, string.Format(@"=SUM(X{0}:X{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "Y", iRowCurrent, string.Format(@"=SUM(Y{0}:Y{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "Z", iRowCurrent, string.Format(@"=SUM(Z{0}:Z{1})", iRowStart, iRowCurrent - 1), false, true);

                        #endregion

                        #endregion
                    }
                }
            }
            try
            {
                workbook.SaveExcel(strPathFileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void AddRowProductStatusValue(ref ExcelWorksheet sheet, DataRow row, ref int iRow, int iRowTemplate, ref int iSTT, int intMainGroupIndex, int intSubGroupIndex, bool bolInsertMainGroup, bool bolInsertSubGroup)
        {
            if (bolInsertMainGroup)
            {
                if (iRowTemplate < iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(23);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "A", iRow, row["MAINGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "A", 23, row["MAINGROUPNAME"].ToString(), false, true);
                }
            }

            if (bolInsertMainGroup)
            {
                if (iRowTemplate != iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(24);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "B", iRow, row["SUBGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "B", 24, row["SUBGROUPNAME"].ToString(), false, true);
                }
            }

            if (iRowTemplate != iRow)
            {
                ExcelRow rowTemplate = sheet.GetRow(iRowTemplate);
                sheet.GetRow(iRowTemplate);
                rowTemplate.InsertCopy(iRow, false);
            }

            InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
            InsertValueCell(ref sheet, "B", iRow, row["PRODUCTNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "C", iRow, row["PCONSIGNMENTTYPENAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "D", iRow, row["EOL"].ToString(), false, false);
            InsertValueCell(ref sheet, "E", iRow, Convert.ToDecimal(row["AVGPRICE"]), false, false);
            InsertValueCell(ref sheet, "F", iRow, row["QUANTITYUNITNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "G", iRow, Convert.ToDecimal(row["QUANTITY"]), false, false);
            InsertValueCell(ref sheet, "H", iRow, Convert.ToDecimal(row["TOTALCOST"]), false, false);
            InsertValueCell(ref sheet, "I", iRow, Convert.ToDecimal(row["CHECKQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "J", iRow, Convert.ToDecimal(row["REALTOTALCOST"]), false, false);
            InsertValueCell(ref sheet, "K", iRow, string.Format(@"=G{0}-I{0}", iRow), false, false);
            InsertValueCell(ref sheet, "L", iRow, string.Format(@"=H{0}-J{0}", iRow), false, false);
            InsertValueCell(ref sheet, "M", iRow, row["EXPLAINNOTE"].ToString(), false, false);
            InsertValueCell(ref sheet, "N", iRow, Convert.ToDecimal(row["QUANTITY_SALE"]), false, false);
            InsertValueCell(ref sheet, "O", iRow, Convert.ToDecimal(row["QUANTITY_STORECHANGE"]), false, false);
            InsertValueCell(ref sheet, "P", iRow, Convert.ToDecimal(row["QUANTITY_OUTPUT"]), false, false);
            InsertValueCell(ref sheet, "Q", iRow, Convert.ToDecimal(row["QUANTITY_INPUT"]), false, false);
            InsertValueCell(ref sheet, "R", iRow, string.Format(@"=I{0}+N{0}+O{0}+P{0}-Q{0}", iRow), false, false);
            InsertValueCell(ref sheet, "S", iRow, string.Format(@"=R{0}*E{0}", iRow), false, false);
            InsertValueCell(ref sheet, "T", iRow, string.Format(@"=G{0}-R{0}", iRow), false, false);
            InsertValueCell(ref sheet, "U", iRow, string.Format(@"=H{0}-S{0}", iRow), false, false);

            iSTT++;
            iRow++;
        }

        public void AddRowProductStatusValueDetail(ref ExcelWorksheet sheet, DataRow row, ref int iRow, int iRowTemplate, ref int iSTT, int intMainGroupIndex, int intSubGroupIndex, bool bolInsertMainGroup, bool bolInsertSubGroup)
        {
            if (bolInsertMainGroup)
            {
                if (iRowTemplate < iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(23);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "A", iRow, row["MAINGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "A", 23, row["MAINGROUPNAME"].ToString(), false, true);
                }
            }

            if (bolInsertMainGroup)
            {
                if (iRowTemplate != iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(24);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "B", iRow, row["SUBGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "B", 24, row["SUBGROUPNAME"].ToString(), false, true);
                }
            }

            if (iRowTemplate != iRow)
            {
                ExcelRow rowTemplate = sheet.GetRow(iRowTemplate);
                sheet.GetRow(iRowTemplate);
                rowTemplate.InsertCopy(iRow, false);
            }

            InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
            InsertValueCell(ref sheet, "B", iRow, row["PRODUCTNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "C", iRow, row["PCONSIGNMENTTYPENAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "D", iRow, row["EOL"].ToString(), false, false);
            InsertValueCell(ref sheet, "E", iRow, Convert.ToDecimal(row["AVGPRICE"]), false, false);
            InsertValueCell(ref sheet, "F", iRow, row["QUANTITYUNITNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "G", iRow, Convert.ToDecimal(row["QUANTITY"]), false, false);
            InsertValueCell(ref sheet, "H", iRow, Convert.ToDecimal(row["TOTALCOST"]), false, false);
            InsertValueCell(ref sheet, "I", iRow, row["STOCKIMEI"].ToString().Trim(), false, false);
            InsertValueCell(ref sheet, "J", iRow, row["CUR_PRODUCTSTATUSNAME"].ToString().Trim(), false, false);
            InsertValueCell(ref sheet, "K", iRow, Convert.ToDecimal(row["CHECKQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "L", iRow, Convert.ToDecimal(row["REALTOTALCOST"]), false, false);
            InsertValueCell(ref sheet, "M", iRow, row["INVENTORYIMEI"].ToString().Trim(), false, false);
            InsertValueCell(ref sheet, "N", iRow, row["PRODUCTSTATUSNAME"].ToString().Trim(), false, false);
            InsertValueCell(ref sheet, "O", iRow, string.Format(@"=G{0}-K{0}", iRow), false, false);
            InsertValueCell(ref sheet, "P", iRow, string.Format(@"=H{0}-L{0}", iRow), false, false);
            InsertValueCell(ref sheet, "Q", iRow, row["UNEVENTPRODUCTSTATUSNAME"].ToString().Trim(), false, false);
            InsertValueCell(ref sheet, "R", iRow, row["EXPLAINNOTE"].ToString(), false, false);

            InsertValueCell(ref sheet, "S", iRow, Convert.ToDecimal(row["QUANTITY_SALE"]), false, false);
            InsertValueCell(ref sheet, "T", iRow, Convert.ToDecimal(row["QUANTITY_STORECHANGE"]), false, false);
            InsertValueCell(ref sheet, "U", iRow, Convert.ToDecimal(row["QUANTITY_OUTPUT"]), false, false);
            InsertValueCell(ref sheet, "V", iRow, Convert.ToDecimal(row["QUANTITY_INPUT"]), false, false);
            InsertValueCell(ref sheet, "W", iRow, string.Format(@"=O{0}+S{0}+T{0}+U{0}-V{0}", iRow), false, false);
            InsertValueCell(ref sheet, "X", iRow, string.Format(@"=W{0}*E{0}", iRow), false, false);
            InsertValueCell(ref sheet, "Y", iRow, string.Format(@"=G{0}-W{0}", iRow), false, false);
            InsertValueCell(ref sheet, "Z", iRow, string.Format(@"=H{0}-X{0}", iRow), false, false);

            iSTT++;
            iRow++;
        }

        #endregion

        #region Export Phụ Lục 01

        public void ExportPhuLuc01(DataSet dtbDataSet)
        {
            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strSheetName = string.Empty;

            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\InventoryTermStockPhuLuc.xlsx";
            if (CheckFileTemplate(strFileTemplate, ref strPathFileName) == false) return;
            if (ExportExcelPhuLuc(dtbDataSet, strFileTemplate, strPathFileName))
            {
                OpenFileExport(strPathFileName);
                return;
            }
            else
            {
                return;
            }
        }
        private bool ExportExcelPhuLuc(DataSet dtbDataSet, string strFileTemplate, string strPathFileName)
        {
            DataTable dtbData = dtbDataSet.Tables[0];
            DataTable dtbDanhMuc = dtbDataSet.Tables[1];
            DataTable dtbDanhMucNguyenNhan = dtbDataSet.Tables[2];
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
            }
            catch { }

            if (workbook != null)
            {

                string strSheetName = "Phụ Lục 01";
                #region Phu luc 01
                ExcelWorksheet sheet = (ExcelWorksheet)workbook.Worksheets[0];
                ExportExcelSheet01(ref sheet, dtbData, dtbDanhMuc, dtbDanhMucNguyenNhan);
                #endregion

                #region Phu luc 02
                ExcelWorksheet sheet2 = (ExcelWorksheet)workbook.Worksheets[1];
                ExportExcelSheet02(ref sheet2, dtbData, dtbDanhMuc);
                #endregion

            }
            try
            {
                workbook.SaveXlsx(strPathFileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool ExportExcelSheet01(ref ExcelWorksheet sheet, DataTable dtbData, DataTable dtbDanhMuc, DataTable dtbDanhMucNguyenNhan)
        {
            if (sheet != null)
            {
                #region Insert Header
                InsertValueCell(ref sheet, "E", 3, "                          Hà Nội, ngày     tháng        năm " + DateTime.Now.Year.ToString(), false, true);
                string strReviewedDate = "Ngày .…  tháng …. năm ….";
                InsertValueCell(ref sheet, "A", 6, strReviewedDate, false, true);

                ////string strStoreName = dtbData.Rows[0]["STORENAME"].ToString().Trim();
                ////DateTime? dtReviewedDate = null;
                ////DataRow[] lstRowReviewed = dtbData.Select("REVIEWUNEVENTTIME IS NOT NULL");
                ////if (lstRowReviewed != null && lstRowReviewed.Count() > 0)
                ////    dtReviewedDate = Convert.ToDateTime(lstRowReviewed[0]["REVIEWUNEVENTTIME"]);


                ////string strReviewedTime = "…….giờ … phút";
                ////string strInventoryTermName = string.Format(@"Kỳ kiểm kê: {0}", cboInventoryTerm.ColumnName.ToString().Trim());
                ////if (dtReviewedDate != null)
                ////{
                ////    strReviewedDate = string.Format(@"Ngày {0} tháng {1} năm {2}", dtReviewedDate.Value.Day, dtReviewedDate.Value.Month, dtReviewedDate.Value.Year);
                ////    strReviewedTime = string.Format(@"{0} giờ {1} phút", dtReviewedDate.Value.Hour, dtReviewedDate.Value.Minute);
                ////}

                ////string strReason = string.Format(@"Hôm nay, {0}, siêu thị {1} tiến hành kiểm kê vào lúc:{2}. Kết quả kiểm kê như sau:", strReviewedDate, strStoreName, strReviewedTime);
                ////string strPrintDate = string.Format(@"………….., Ngày {0} Tháng {1} Năm {2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);

                //InsertValueCell(ref sheet, "A", 3, strStoreName, false, true);
                //InsertValueCell(ref sheet, "A", 6, strReviewedDate, false, true);
                //InsertValueCell(ref sheet, "A", 7, strInventoryTermName, false, true);
                //InsertValueCell(ref sheet, "A", 15, strReason, false, true);
                //InsertValueCell(ref sheet, "I", 27, strPrintDate, false, false);

                #endregion
                List<string> lst = new List<string>();
                string strLoaiTru = Library.AppCore.AppConfig.GetStringConfigValue("INVENTORY_RPT_PL1");

                if (!string.IsNullOrEmpty(strLoaiTru))
                {
                    var result = dtbDataCauseGroups.Select("CAUSEGROUPSID IN (" + strLoaiTru + ")");
                    if (result.Any())
                    {
                        foreach (DataRow drNguyenNhan in result)
                        {
                            lst.Add((drNguyenNhan["CAUSEGROUPSNAME"] ?? "").ToString().Trim().ToUpper());
                        }
                    }
                }
                #region Import Row
                int iRowStart = 20;
                int iRowCurrent = iRowStart;
                int intSTT = 1;
                // Thừa: Kiem Ke Nhiu Hon he Thong 
                var varThua = dtbData.AsEnumerable().Where(row => Convert.ToDecimal((row.Field<object>("UNEVENTQUANTITY2") ?? "").ToString()) < 0);
                decimal decTongThua = 0;
                if (varThua.Any())
                {
                    foreach (DataRow dr in varThua)
                    {
                        bool IsNoAdd = false;
                        string strNguyenNhanLoi = "";
                        var varNguyenNhan = dtbDanhMuc.AsEnumerable()
                                .Where(row => (row.Field<object>("INVENTORYID") ?? "").ToString().Trim() == (dr["INVENTORYID"] ?? "").ToString().Trim()
                                              && (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == (dr["PRODUCTID"] ?? "").ToString().Trim()
                                              && (row.Field<object>("STOCKIMEI") ?? "").ToString().Trim() == (dr["IMEI"] ?? "").ToString().Trim()
                                              );
                        if (varNguyenNhan.Any())
                        {
                            foreach (DataRow drNguyenNhan in varNguyenNhan)
                            {
                                strNguyenNhanLoi += (drNguyenNhan["CAUSEGROUPSNAME"] ?? "").ToString().Trim() + ", ";
                                if (lst.Contains((drNguyenNhan["CAUSEGROUPSNAME"] ?? "").ToString().Trim().ToUpper()))
                                {
                                    IsNoAdd = true;
                                    break;
                                }
                            }
                            strNguyenNhanLoi = strNguyenNhanLoi.Trim();
                            strNguyenNhanLoi = strNguyenNhanLoi.Trim(',');
                        }
                        if (IsNoAdd)
                        {
                            continue;
                        }
                        decimal decThua = 0;
                        decThua = Math.Abs(Convert.ToDecimal(dr["UNEVENTQUANTITY2"] ?? ""));
                        InsertValueCell(ref sheet, "A", iRowCurrent, intSTT, false, false); // STT
                        InsertValueCell(ref sheet, "B", iRowCurrent, (dr["STOREID"] ?? "").ToString(), false, false); // Mã ST
                        InsertValueCell(ref sheet, "C", iRowCurrent, (dr["PRODUCTNAME"] ?? "").ToString().Trim(), false, false); //Ten SP
                        InsertValueCell(ref sheet, "D", iRowCurrent, (dr["IMEI"] ?? "").ToString().Trim(), false, false); // IMEI
                                                                                                                          // Nhom Nguyen Nhan Loi 

                        InsertValueCell(ref sheet, "E", iRowCurrent, strNguyenNhanLoi.ToString().Trim(), false, false);
                        InsertValueCell(ref sheet, "F", iRowCurrent, decThua, false, false);
                        InsertValueCell(ref sheet, "G", iRowCurrent, string.Empty, false, false);

                        string strNguyenNhanLoi2 = "";
                        var varNguyenNhan2 = dtbDanhMucNguyenNhan.AsEnumerable()
                                .Where(row => (row.Field<object>("INVENTORYID") ?? "").ToString().Trim() == (dr["INVENTORYID"] ?? "").ToString().Trim()
                                              && (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == (dr["PRODUCTID"] ?? "").ToString().Trim()
                                              && (row.Field<object>("STOCKIMEI") ?? "").ToString().Trim() == (dr["IMEI"] ?? "").ToString().Trim()
                                              );
                        if (varNguyenNhan2.Any())
                        {
                            List<string> lstOUTIN = new List<string>();
                            foreach (DataRow drNguyenNhan2 in varNguyenNhan2)
                            {
                                string strName = (drNguyenNhan2["UNEVENTEXPLAIN"] ?? "").ToString().Trim();
                                if (strName.Contains("IV")
                                    || strName.Contains("OV"))
                                {
                                    if (lstOUTIN.Contains(strName))
                                        continue;
                                    lstOUTIN.Add(strName);
                                    continue;
                                }
                                //                    CASE WHEN LTRIM(RTRIM(XMLAGG(XMLELEMENT(E, TMP.EXPLAINNOTE, ',').EXTRACT('//text()')).GetClobVal(), ','), ',') LIKE '%IV%'
                                //          OR LTRIM(RTRIM(XMLAGG(XMLELEMENT(E, TMP.EXPLAINNOTE,',').EXTRACT('//text()')).GetClobVal(),','),',') LIKE '%OV%'
                                //THEN 'Sản phẩm đã phát sinh nhập xuất:' || CAST(SUBSTR(LTRIM(RTRIM(XMLAGG(XMLELEMENT(E, TMP.EXPLAINNOTE, ',').


                                strNguyenNhanLoi2 += strName + ", ";
                            }
                            if (lstOUTIN.Count > 0)
                            {
                                strNguyenNhanLoi2 += "Sản phẩm đã phát sinh nhập xuất: " + String.Join(", ", lstOUTIN.ToArray());
                            }

                            strNguyenNhanLoi2 = strNguyenNhanLoi2.Trim();
                            strNguyenNhanLoi2 = strNguyenNhanLoi2.Trim(',');
                        }
                        InsertValueCell(ref sheet, "H", iRowCurrent, strNguyenNhanLoi2.ToString().Trim(), false, false);
                        decTongThua += decThua;
                        iRowCurrent++;
                        intSTT++;
                    }
                }

                // Tong thua
                InsertValueCell(ref sheet, "C", 19, "TỔNG THỪA", false, true);
                InsertValueCell(ref sheet, "F", 19, decTongThua, false, true);

                // Thieu: Kiem Ke It Hon He thong
                int iRowTongThieu = iRowCurrent;
                iRowCurrent++;
                intSTT = 1;
                var varThieu = dtbData.AsEnumerable().Where(row => Convert.ToDecimal((row.Field<object>("UNEVENTQUANTITY2") ?? "").ToString()) > 0);
                decimal decTongThieu = 0;
                if (varThieu.Any())
                {
                    foreach (DataRow dr in varThieu)
                    {
                        bool IsNoAdd = false;
                        string strNguyenNhanLoi = "";
                        var varNguyenNhan = dtbDanhMuc.AsEnumerable()
                                .Where(row => (row.Field<object>("INVENTORYID") ?? "").ToString().Trim() == (dr["INVENTORYID"] ?? "").ToString().Trim()
                                              && (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == (dr["PRODUCTID"] ?? "").ToString().Trim()
                                              && (row.Field<object>("STOCKIMEI") ?? "").ToString().Trim() == (dr["IMEI"] ?? "").ToString().Trim()
                                              );
                        if (varNguyenNhan.Any())
                        {
                            foreach (DataRow drNguyenNhan in varNguyenNhan)
                            {
                                strNguyenNhanLoi += (drNguyenNhan["CAUSEGROUPSNAME"] ?? "").ToString().Trim() + ", ";
                                if (lst.Contains((drNguyenNhan["CAUSEGROUPSNAME"] ?? "").ToString().Trim().ToUpper()))
                                {
                                    IsNoAdd = true;
                                    break;
                                }
                            }
                            strNguyenNhanLoi = strNguyenNhanLoi.Trim();
                            strNguyenNhanLoi = strNguyenNhanLoi.Trim(',');
                        }
                        if (IsNoAdd)
                        {
                            continue;
                        }
                        decimal decThieu = 0;
                        decThieu = Math.Abs(Convert.ToDecimal(dr["UNEVENTQUANTITY2"] ?? ""));
                        InsertValueCell(ref sheet, "A", iRowCurrent, intSTT, false, false); // STT
                        InsertValueCell(ref sheet, "B", iRowCurrent, (dr["STOREID"] ?? "").ToString(), false, false); // Mã ST
                        InsertValueCell(ref sheet, "C", iRowCurrent, (dr["PRODUCTNAME"] ?? "").ToString().Trim(), false, false); //Ten SP
                        InsertValueCell(ref sheet, "D", iRowCurrent, (dr["IMEI"] ?? "").ToString().Trim(), false, false); // IMEI
                                                                                                                          // Nhom Nguyen Nhan Loi 

                        InsertValueCell(ref sheet, "E", iRowCurrent, strNguyenNhanLoi.ToString().Trim(), false, false);
                        InsertValueCell(ref sheet, "F", iRowCurrent, string.Empty, false, false);
                        InsertValueCell(ref sheet, "G", iRowCurrent, decThieu, false, false);
                        string strNguyenNhanLoi2 = "";
                        var varNguyenNhan2 = dtbDanhMucNguyenNhan.AsEnumerable()
                                .Where(row => (row.Field<object>("INVENTORYID") ?? "").ToString().Trim() == (dr["INVENTORYID"] ?? "").ToString().Trim()
                                              && (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == (dr["PRODUCTID"] ?? "").ToString().Trim()
                                              && (row.Field<object>("STOCKIMEI") ?? "").ToString().Trim() == (dr["IMEI"] ?? "").ToString().Trim()
                                              );
                        if (varNguyenNhan2.Any())
                        {
                            List<string> lstOUTIN = new List<string>();
                            foreach (DataRow drNguyenNhan2 in varNguyenNhan2)
                            {
                                string strName = (drNguyenNhan2["UNEVENTEXPLAIN"] ?? "").ToString().Trim();
                                if (strName.Contains("IV")
                                    || strName.Contains("OV"))
                                {
                                    if (lstOUTIN.Contains(strName))
                                        continue;
                                    lstOUTIN.Add(strName);
                                    continue;
                                }
                                //                    CASE WHEN LTRIM(RTRIM(XMLAGG(XMLELEMENT(E, TMP.EXPLAINNOTE, ',').EXTRACT('//text()')).GetClobVal(), ','), ',') LIKE '%IV%'
                                //          OR LTRIM(RTRIM(XMLAGG(XMLELEMENT(E, TMP.EXPLAINNOTE,',').EXTRACT('//text()')).GetClobVal(),','),',') LIKE '%OV%'
                                //THEN 'Sản phẩm đã phát sinh nhập xuất:' || CAST(SUBSTR(LTRIM(RTRIM(XMLAGG(XMLELEMENT(E, TMP.EXPLAINNOTE, ',').


                                strNguyenNhanLoi2 += strName + ", ";
                            }
                            if (lstOUTIN.Count > 0)
                            {
                                strNguyenNhanLoi2 += "Sản phẩm đã phát sinh nhập xuất: " + String.Join(", ", lstOUTIN.ToArray());
                            }

                            strNguyenNhanLoi2 = strNguyenNhanLoi2.Trim();
                            strNguyenNhanLoi2 = strNguyenNhanLoi2.Trim(',');
                        }
                        InsertValueCell(ref sheet, "H", iRowCurrent, strNguyenNhanLoi2.ToString().Trim(), false, false);
                        decTongThieu += decThieu;
                        iRowCurrent++;
                        intSTT++;
                    }
                }

                // Tong Thieu
                InsertValueCell(ref sheet, "C", iRowTongThieu, "TỔNG THIẾU", false, true);
                InsertValueCell(ref sheet, "G", iRowTongThieu, decTongThieu, false, true);
                #endregion
                return true;
            }
            return false;
        }

        private bool ExportExcelSheet02(ref ExcelWorksheet sheet, DataTable dtbData, DataTable dtbDanhMuc)
        {
            if (sheet != null)
            {
                #region Insert Header
                InsertValueCell(ref sheet, "E", 3, "                 Hà nội, ngày     tháng        năm " + DateTime.Now.Year.ToString(), false, true);
                string strReviewedDate = "Thời điểm: Hết ngày .…/ …./ ….";
                InsertValueCell(ref sheet, "A", 6, strReviewedDate, false, true);

                #endregion
                List<string> lst = new List<string>();
                string strLoaiTru = Library.AppCore.AppConfig.GetStringConfigValue("INVENTORY_RPT_PL2");
                if (!string.IsNullOrEmpty(strLoaiTru))
                {
                    var result = dtbDataCauseGroups.Select("CAUSEGROUPSID IN (" + strLoaiTru + ")");
                    if (result.Any())
                    {
                        foreach (DataRow drNguyenNhan in result)
                        {
                            lst.Add((drNguyenNhan["CAUSEGROUPSNAME"] ?? "").ToString().Trim().ToUpper());
                        }
                    }
                }
                #region Import Row
                int iRowStart = 20;
                int iRowCurrent = iRowStart;
                int intSTT = 1;
                // Thừa: Kiem Ke Nhiu Hon he Thong 
                var varThua = dtbData.AsEnumerable().Where(row => Convert.ToDecimal((row.Field<object>("UNEVENTQUANTITY2") ?? "").ToString()) < 0);
                decimal decTongThua = 0;
                if (varThua.Any())
                {
                    foreach (DataRow dr in varThua)
                    {
                        bool IsNoAdd = false;
                        string strNguyenNhanLoi = "";
                        var varNguyenNhan = dtbDanhMuc.AsEnumerable()
                                .Where(row => (row.Field<object>("INVENTORYID") ?? "").ToString().Trim() == (dr["INVENTORYID"] ?? "").ToString().Trim()
                                              && (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == (dr["PRODUCTID"] ?? "").ToString().Trim()
                                              && (row.Field<object>("STOCKIMEI") ?? "").ToString().Trim() == (dr["IMEI"] ?? "").ToString().Trim()
                                              );
                        if (varNguyenNhan.Any())
                        {
                            foreach (DataRow drNguyenNhan in varNguyenNhan)
                            {
                                strNguyenNhanLoi += (drNguyenNhan["CAUSEGROUPSNAME"] ?? "").ToString().Trim() + ", ";
                                if (lst.Contains((drNguyenNhan["CAUSEGROUPSNAME"] ?? "").ToString().Trim().ToUpper()))
                                {
                                    IsNoAdd = true;
                                    break;
                                }
                            }
                            strNguyenNhanLoi = strNguyenNhanLoi.Trim();
                            strNguyenNhanLoi = strNguyenNhanLoi.Trim(',');
                        }
                        if (IsNoAdd)
                        {
                            continue;
                        }
                        decimal decThua = 0;
                        decThua = Math.Abs(Convert.ToDecimal(dr["UNEVENTQUANTITY2"] ?? ""));
                        InsertValueCell(ref sheet, "A", iRowCurrent, intSTT, false, false); // STT
                        InsertValueCell(ref sheet, "B", iRowCurrent, (dr["STOREID"] ?? "").ToString(), false, false); // Mã ST
                        InsertValueCell(ref sheet, "C", iRowCurrent, (dr["PRODUCTNAME"] ?? "").ToString().Trim(), false, false); //Ten SP
                        InsertValueCell(ref sheet, "D", iRowCurrent, decThua, false, false);
                        InsertValueCell(ref sheet, "E", iRowCurrent, Convert.ToDecimal(dr["COSTPRICE"] ?? ""), false, false);
                        InsertValueCell(ref sheet, "F", iRowCurrent, Convert.ToDecimal(dr["SALEPRICE"] ?? ""), false, false);
                        InsertValueCell(ref sheet, "J", iRowCurrent, (dr["IMEI"] ?? "").ToString().Trim(), false, false); // IMEI
                        decTongThua += decThua;
                        iRowCurrent++;
                        intSTT++;
                    }
                }
                // Thieu: Kiem Ke It Hon He thong
                intSTT = 1;
                var varThieu = dtbData.AsEnumerable().Where(row => Convert.ToDecimal((row.Field<object>("UNEVENTQUANTITY2") ?? "").ToString()) > 0);
                decimal decTongThieu = 0;
                if (varThieu.Any())
                {
                    foreach (DataRow dr in varThieu)
                    {
                        bool IsNoAdd = false;
                        string strNguyenNhanLoi = "";
                        var varNguyenNhan = dtbDanhMuc.AsEnumerable()
                                .Where(row => (row.Field<object>("INVENTORYID") ?? "").ToString().Trim() == (dr["INVENTORYID"] ?? "").ToString().Trim()
                                              && (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == (dr["PRODUCTID"] ?? "").ToString().Trim()
                                              && (row.Field<object>("STOCKIMEI") ?? "").ToString().Trim() == (dr["IMEI"] ?? "").ToString().Trim()
                                              );
                        if (varNguyenNhan.Any())
                        {
                            foreach (DataRow drNguyenNhan in varNguyenNhan)
                            {
                                strNguyenNhanLoi += (drNguyenNhan["CAUSEGROUPSNAME"] ?? "").ToString().Trim() + ", ";
                                if (lst.Contains((drNguyenNhan["CAUSEGROUPSNAME"] ?? "").ToString().Trim().ToUpper()))
                                {
                                    IsNoAdd = true;
                                    break;
                                }
                            }
                            strNguyenNhanLoi = strNguyenNhanLoi.Trim();
                            strNguyenNhanLoi = strNguyenNhanLoi.Trim(',');
                        }
                        if (IsNoAdd)
                        {
                            continue;
                        }
                        decimal decThieu = 0;
                        decThieu = Math.Abs(Convert.ToDecimal(dr["UNEVENTQUANTITY2"] ?? ""));
                        InsertValueCell(ref sheet, "A", iRowCurrent, intSTT, false, false); // STT
                        InsertValueCell(ref sheet, "B", iRowCurrent, (dr["STOREID"] ?? "").ToString(), false, false); // Mã ST
                        InsertValueCell(ref sheet, "C", iRowCurrent, (dr["PRODUCTNAME"] ?? "").ToString().Trim(), false, false); //Ten SP
                        InsertValueCell(ref sheet, "G", iRowCurrent, decThieu, false, false);
                        InsertValueCell(ref sheet, "H", iRowCurrent, Convert.ToDecimal(dr["COSTPRICE"] ?? ""), false, false);
                        InsertValueCell(ref sheet, "I", iRowCurrent, Convert.ToDecimal(dr["SALEPRICE"] ?? ""), false, false);
                        InsertValueCell(ref sheet, "J", iRowCurrent, (dr["IMEI"] ?? "").ToString().Trim(), false, false); // IMEI
                        decTongThieu += decThieu;
                        iRowCurrent++;
                        intSTT++;
                    }
                }
                InsertValueCell(ref sheet, "D", 19, string.Format(@"=SUM(D{0}:D{1})", iRowStart, iRowCurrent - 1), false, false);
                InsertValueCell(ref sheet, "E", 19, string.Format(@"=SUM(E{0}:E{1})", iRowStart, iRowCurrent - 1), false, false);
                InsertValueCell(ref sheet, "F", 19, string.Format(@"=SUM(F{0}:F{1})", iRowStart, iRowCurrent - 1), false, false);
                InsertValueCell(ref sheet, "G", 19, string.Format(@"=SUM(G{0}:G{1})", iRowStart, iRowCurrent - 1), false, false);
                InsertValueCell(ref sheet, "H", 19, string.Format(@"=SUM(H{0}:H{1})", iRowStart, iRowCurrent - 1), false, false);
                InsertValueCell(ref sheet, "I", 19, string.Format(@"=SUM(I{0}:I{1})", iRowStart, iRowCurrent - 1), false, false);
                #endregion
                return true;
            }
            return false;
        }
        #endregion

        #region Export Detail Mau Moi

        public void ExportDetail(DataSet dtbDataSet)
        {
            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strSheetName = string.Empty;

            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\InventoryTermStockDetail.xlsx";
            if (CheckFileTemplate(strFileTemplate, ref strPathFileName) == false) return;
            if (ExportExcelDetail(dtbDataSet, strFileTemplate, strPathFileName))
            {
                OpenFileExport(strPathFileName);
                return;
            }
            else
            {
                return;
            }
        }
        private bool ExportExcelDetail(DataSet dtbDataSet, string strFileTemplate, string strPathFileName)
        {
            DataTable dtbData = dtbDataSet.Tables[0];
            DataTable dtbDanhMuc = dtbDataSet.Tables[1];
            DataTable dtbDanhMucNguyenNhan = dtbDataSet.Tables[2];
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
            }
            catch { }

            if (workbook != null)
            {

                string strSheetName = "Phụ Lục 01";
                #region Phu luc 01
                ExcelWorksheet sheet = (ExcelWorksheet)workbook.Worksheets[0];
                ExportExcelSheetDetail(ref sheet, dtbData, dtbDanhMuc, dtbDanhMucNguyenNhan);
                #endregion

            }
            try
            {
                workbook.SaveXlsx(strPathFileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool ExportExcelSheetDetail(ref ExcelWorksheet sheet, DataTable dtbData, DataTable dtbDanhMuc, DataTable dtbDanhMucNguyenNhan)
        {
            if (sheet != null)
            {
                #region Insert Header
                InsertValueCell(ref sheet, "M", 3, "                          Hà Nội, ngày     tháng        năm " + DateTime.Now.Year.ToString(), false, true);
                string strReviewedDate = "Ngày .…  tháng …. năm ….";
                InsertValueCell(ref sheet, "A", 6, strReviewedDate, false, true);

                ////string strStoreName = dtbData.Rows[0]["STORENAME"].ToString().Trim();
                ////DateTime? dtReviewedDate = null;
                ////DataRow[] lstRowReviewed = dtbData.Select("REVIEWUNEVENTTIME IS NOT NULL");
                ////if (lstRowReviewed != null && lstRowReviewed.Count() > 0)
                ////    dtReviewedDate = Convert.ToDateTime(lstRowReviewed[0]["REVIEWUNEVENTTIME"]);


                ////string strReviewedTime = "…….giờ … phút";
                ////string strInventoryTermName = string.Format(@"Kỳ kiểm kê: {0}", cboInventoryTerm.ColumnName.ToString().Trim());
                ////if (dtReviewedDate != null)
                ////{
                ////    strReviewedDate = string.Format(@"Ngày {0} tháng {1} năm {2}", dtReviewedDate.Value.Day, dtReviewedDate.Value.Month, dtReviewedDate.Value.Year);
                ////    strReviewedTime = string.Format(@"{0} giờ {1} phút", dtReviewedDate.Value.Hour, dtReviewedDate.Value.Minute);
                ////}

                ////string strReason = string.Format(@"Hôm nay, {0}, siêu thị {1} tiến hành kiểm kê vào lúc:{2}. Kết quả kiểm kê như sau:", strReviewedDate, strStoreName, strReviewedTime);
                ////string strPrintDate = string.Format(@"………….., Ngày {0} Tháng {1} Năm {2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);

                //InsertValueCell(ref sheet, "A", 3, strStoreName, false, true);
                //InsertValueCell(ref sheet, "A", 6, strReviewedDate, false, true);
                //InsertValueCell(ref sheet, "A", 7, strInventoryTermName, false, true);
                //InsertValueCell(ref sheet, "A", 15, strReason, false, true);
                //InsertValueCell(ref sheet, "I", 27, strPrintDate, false, false);

                #endregion

                #region Import Row
                int iRowStart = 19;
                int iRowCurrent = iRowStart;
                int intSTT = 1;
                // Thừa: Kiem Ke Nhiu Hon he Thong 
                var varThua = dtbData.AsEnumerable().Where(row => Convert.ToDecimal((row.Field<object>("UNEVENTQUANTITY2") ?? "").ToString()) != 0);
                decimal decTongThua = 0;
                if (varThua.Any())
                {
                    foreach (DataRow dr in varThua)
                    {
                        decimal decThua = 0;
                        decimal decThieu = 0;
                        //decThua = Convert.ToDecimal(dr["UNEVENTQUANTITY2"]??"");

                        decimal decSalePrice = Convert.ToDecimal(dr["SALEPRICE"] ?? "");
                        decimal decSLTon = Convert.ToDecimal(dr["STOCKQUANTITY2"] ?? "");
                        decimal decSLKiemKe = Convert.ToDecimal(dr["INVENTORYQUANTITY"] ?? "");

                        // InsertValueCell(ref sheet, "A", iRowCurrent, intSTT, false, false); // STT
                        InsertValueCell(ref sheet, "A", iRowCurrent, (dr["STOREID"] ?? "").ToString(), false, false); // Mã ST
                        InsertValueCell(ref sheet, "B", iRowCurrent, (dr["PRODUCTNAME"] ?? "").ToString().Trim(), false, false); //Ten SP
                        InsertValueCell(ref sheet, "C", iRowCurrent, (dr["PRODUCTIDOLD"] ?? "").ToString().Trim(), false, false); // MÃ VT theo DSS
                        InsertValueCell(ref sheet, "D", iRowCurrent, (dr["QUANTITYUNITNAME"] ?? "").ToString().Trim(), false, false);
                        InsertValueCell(ref sheet, "E", iRowCurrent, decSalePrice, false, false);
                        InsertValueCell(ref sheet, "F", iRowCurrent, decSLTon, false, false);
                        InsertValueCell(ref sheet, "G", iRowCurrent, decSalePrice * decSLTon, false, false);
                        InsertValueCell(ref sheet, "H", iRowCurrent, decSLKiemKe, false, false);
                        InsertValueCell(ref sheet, "I", iRowCurrent, decSalePrice * decSLKiemKe, false, false);
                        if (Convert.ToDecimal(dr["UNEVENTQUANTITY2"] ?? "") > 0)
                            decThieu = Math.Abs(Convert.ToDecimal(dr["UNEVENTQUANTITY2"] ?? ""));
                        else decThua = Math.Abs(Convert.ToDecimal(dr["UNEVENTQUANTITY2"] ?? ""));
                        InsertValueCell(ref sheet, "J", iRowCurrent, decThua, false, false);
                        InsertValueCell(ref sheet, "K", iRowCurrent, decThua * decSalePrice, false, false);
                        InsertValueCell(ref sheet, "L", iRowCurrent, decThieu, false, false);
                        InsertValueCell(ref sheet, "M", iRowCurrent, decThieu * decSalePrice, false, false);


                        //InsertValueCell(ref sheet, "E", iRowCurrent, decSalePrice, false, false);
                        //InsertValueCell(ref sheet, "E", iRowCurrent, decSalePrice, false, false);
                        //InsertValueCell(ref sheet, "E", iRowCurrent, decSalePrice, false, false);
                        //InsertValueCell(ref sheet, "E", iRowCurrent, decSalePrice, false, false);



                        // Nhom Nguyen Nhan Loi 
                        string strNguyenNhanLoi = "";
                        var varNguyenNhan = dtbDanhMuc.AsEnumerable()
                                .Where(row => (row.Field<object>("INVENTORYID") ?? "").ToString().Trim() == (dr["INVENTORYID"] ?? "").ToString().Trim()
                                              && (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == (dr["PRODUCTID"] ?? "").ToString().Trim()
                                              //&& (row.Field<object>("STOCKIMEI") ?? "").ToString().Trim() == (dr["IMEI"] ?? "").ToString().Trim()
                                              );
                        if (varNguyenNhan.Any())
                        {
                            foreach (DataRow drNguyenNhan in varNguyenNhan)
                            {
                                strNguyenNhanLoi += (drNguyenNhan["CAUSEGROUPSNAME"] ?? "").ToString().Trim() + ", ";
                            }
                            strNguyenNhanLoi = strNguyenNhanLoi.Trim();
                            strNguyenNhanLoi = strNguyenNhanLoi.Trim(',');
                        }
                        InsertValueCell(ref sheet, "R", iRowCurrent, strNguyenNhanLoi.ToString().Trim(), false, false);

                        string strNguyenNhanLoi2 = "";
                        var varNguyenNhan2 = dtbDanhMucNguyenNhan.AsEnumerable()
                                .Where(row => (row.Field<object>("INVENTORYID") ?? "").ToString().Trim() == (dr["INVENTORYID"] ?? "").ToString().Trim()
                                              && (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == (dr["PRODUCTID"] ?? "").ToString().Trim()
                                              //&& (row.Field<object>("STOCKIMEI") ?? "").ToString().Trim() == (dr["IMEI"] ?? "").ToString().Trim()
                                              );
                        if (varNguyenNhan2.Any())
                        {
                            List<string> lstOUTIN = new List<string>();
                            foreach (DataRow drNguyenNhan2 in varNguyenNhan2)
                            {
                                string strName = (drNguyenNhan2["UNEVENTEXPLAIN"] ?? "").ToString().Trim();
                                if (strName.Contains("IV")
                                    || strName.Contains("OV"))
                                {
                                    if (lstOUTIN.Contains(strName))
                                        continue;
                                    lstOUTIN.Add(strName);
                                    continue;
                                }
                                //                    CASE WHEN LTRIM(RTRIM(XMLAGG(XMLELEMENT(E, TMP.EXPLAINNOTE, ',').EXTRACT('//text()')).GetClobVal(), ','), ',') LIKE '%IV%'
                                //          OR LTRIM(RTRIM(XMLAGG(XMLELEMENT(E, TMP.EXPLAINNOTE,',').EXTRACT('//text()')).GetClobVal(),','),',') LIKE '%OV%'
                                //THEN 'Sản phẩm đã phát sinh nhập xuất:' || CAST(SUBSTR(LTRIM(RTRIM(XMLAGG(XMLELEMENT(E, TMP.EXPLAINNOTE, ',').


                                strNguyenNhanLoi2 += strName + ", ";
                            }
                            if (lstOUTIN.Count > 0)
                            {
                                strNguyenNhanLoi2 += "Sản phẩm đã phát sinh nhập xuất: " + String.Join(", ", lstOUTIN.ToArray());
                            }

                            strNguyenNhanLoi2 = strNguyenNhanLoi2.Trim();
                            strNguyenNhanLoi2 = strNguyenNhanLoi2.Trim(',');
                        }
                        InsertValueCell(ref sheet, "S", iRowCurrent, strNguyenNhanLoi2.ToString().Trim(), false, false);


                        //InsertValueCell(ref sheet, "F", iRowCurrent, decThua, false, false);
                        //InsertValueCell(ref sheet, "G", iRowCurrent, string.Empty, false, false);
                        //InsertValueCell(ref sheet, "H", iRowCurrent, string.Empty, false, false);
                        decTongThua += decThua;
                        iRowCurrent++;
                        intSTT++;
                    }
                }

                // Tong thua
                InsertValueCell(ref sheet, "B", iRowCurrent, "TỔNG", false, true);
                InsertValueCell(ref sheet, "E", iRowCurrent, string.Format(@"=SUM(E{0}:E{1})", iRowStart, iRowCurrent - 1), false, true);
                InsertValueCell(ref sheet, "F", iRowCurrent, string.Format(@"=SUM(F{0}:F{1})", iRowStart, iRowCurrent - 1), false, true);
                InsertValueCell(ref sheet, "G", iRowCurrent, string.Format(@"=SUM(G{0}:G{1})", iRowStart, iRowCurrent - 1), false, true);
                InsertValueCell(ref sheet, "H", iRowCurrent, string.Format(@"=SUM(H{0}:H{1})", iRowStart, iRowCurrent - 1), false, true);
                InsertValueCell(ref sheet, "I", iRowCurrent, string.Format(@"=SUM(I{0}:I{1})", iRowStart, iRowCurrent - 1), false, true);
                InsertValueCell(ref sheet, "J", iRowCurrent, string.Format(@"=SUM(J{0}:J{1})", iRowStart, iRowCurrent - 1), false, true);
                InsertValueCell(ref sheet, "K", iRowCurrent, string.Format(@"=SUM(K{0}:K{1})", iRowStart, iRowCurrent - 1), false, true);
                InsertValueCell(ref sheet, "L", iRowCurrent, string.Format(@"=SUM(L{0}:L{1})", iRowStart, iRowCurrent - 1), false, true);
                InsertValueCell(ref sheet, "M", iRowCurrent, string.Format(@"=SUM(M{0}:M{1})", iRowStart, iRowCurrent - 1), false, true);
                InsertValueCell(ref sheet, "N", iRowCurrent, string.Format(@"=SUM(N{0}:N{1})", iRowStart, iRowCurrent - 1), false, true);
                InsertValueCell(ref sheet, "O", iRowCurrent, string.Format(@"=SUM(O{0}:O{1})", iRowStart, iRowCurrent - 1), false, true);
                InsertValueCell(ref sheet, "P", iRowCurrent, string.Format(@"=SUM(P{0}:P{1})", iRowStart, iRowCurrent - 1), false, true);
                InsertValueCell(ref sheet, "Q", iRowCurrent, string.Format(@"=SUM(Q{0}:Q{1})", iRowStart, iRowCurrent - 1), false, true);

                #endregion
                return true;
            }
            return false;
        }

        #endregion

        #region Export PL - BCKK
        private Boolean CheckFileTemplateBCKK(String strFileTemplate, ref string strPathFileName)
        {
            try
            {
                SaveFileDialog saveDlg = new SaveFileDialog();
                saveDlg.Filter = "Excel Worksheet file(*.xlsx)|*.xls";
                saveDlg.AddExtension = true;
                saveDlg.RestoreDirectory = true;
                saveDlg.Title = "Chọn vị trí lưu file?";
                saveDlg.FileName = "BL-BCKK_" + System.DateTime.Now.Date.ToString("yyyy");
                if (saveDlg.ShowDialog() == DialogResult.OK)
                {
                    strPathFileName = System.IO.Path.ChangeExtension(saveDlg.FileName, ".xlsx");
                }
                if (string.IsNullOrEmpty(strPathFileName)) return false;

                if (System.IO.File.Exists(strPathFileName))
                {
                    try
                    {
                        System.IO.File.Delete(strPathFileName);
                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show("File Excel đang được mở, Bạn phải đóng lại trước khi mở file tiếp", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }

                if (System.IO.File.Exists(strFileTemplate) == false)
                {
                    MessageBox.Show("Tập tin mẫu không tồn tại");
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            return true;
        }

        #region 5 phu luc

        private bool ExportExcel_PL1TongHop(ref ExcelWorksheet sheet, DataSet dtsData)
        {

            IDictionary<int, string> dict = new Dictionary<int, string>() {
                                                {31,"II_1"},
                                                {32, "II_2"},
                                                {33,"II_3"},
                                                {34,"II_4"},
                                                {35, "II_5"},
                                                {36,"II_6"},
                                                {37,"II_7"},
                                                {38, "II_8"}
            };
            if (sheet != null)
            {
                DataTable dtbSheet01 = dtsData.Tables[0].Copy();
                DataTable dtbSheet02 = dtsData.Tables[5].Copy();
                #region Import Row
                foreach (DataRow drDT in dtbSheet01.Rows)
                {
                    foreach (KeyValuePair<int, string> item in dict)
                    {
                        if (item.Value == (drDT["DANHMUC_TENKHO"] ?? "").ToString().Trim())
                        {
                            decimal valueD = Convert.ToDecimal(drDT["DON_GIA"]);
                            decimal valueE = Convert.ToDecimal(drDT["SL_SOSACH"]);
                            decimal valueF = Convert.ToDecimal(drDT["TIEN_SOSACH"]);
                            decimal valueG = Convert.ToDecimal(drDT["SL_THUCTE"]);
                            decimal valueH = Convert.ToDecimal(drDT["TIEN_THUCTE"]);
                            decimal valueI = Convert.ToDecimal(drDT["SL_THUA"]);
                            decimal valueJ = Convert.ToDecimal(drDT["TIEN_THUA"]);
                            decimal valueK = Convert.ToDecimal(drDT["SL_THIEU"]);
                            decimal valueL = Convert.ToDecimal(drDT["TIEN_THIEU"]);
                            decimal valueM = Convert.ToDecimal(drDT["SL_TINHTRANG_TOT"]);
                            decimal valueN = Convert.ToDecimal(drDT["TIEN_TINHTRANG_TOT"]);
                            decimal valueO = Convert.ToDecimal(drDT["SL_TINHTRANG_KEMPHAM"]);
                            decimal valueP = Convert.ToDecimal(drDT["TIEN_TINHTRANG_KEMPHAM"]);
                            decimal valueQ = Convert.ToDecimal(drDT["SL_TINHTRANG_PHEPHAM"]);
                            decimal valueR = Convert.ToDecimal(drDT["TIEN_TINHTRANG_PHEPHAM"]);
                            string valueS = (drDT["GHI_CHU"] ?? "").ToString().Trim();
                            //if (iRowCurrentHHNK != iRowStartHHNK)
                            //{
                            //ExcelRow rowTemplate = sheet.GetRow(item.Key);
                            //rowTemplate.InsertCopy(iRowCurrentHHNK, false);
                            //}
                            //InsertValueCell(ref sheet, "A", iRowCurrentHHNK, valueA, false, false); // STT
                            //InsertValueCell(ref sheet, "B", iRowCurrentHHNK, valueB, false, false); // Mã ST
                            //InsertValueCell(ref sheet, "C", iRowCurrentHHNK, valueC, false, false); //Ten SP
                            InsertValueCell(ref sheet, "D", item.Key, valueD, false, false); // IMEI
                            InsertValueCell(ref sheet, "E", item.Key, valueE, false, false);
                            InsertValueCell(ref sheet, "F", item.Key, valueF, false, false);
                            InsertValueCell(ref sheet, "G", item.Key, valueG, false, false);
                            InsertValueCell(ref sheet, "H", item.Key, valueH, false, false);
                            InsertValueCell(ref sheet, "I", item.Key, valueI, false, false);
                            InsertValueCell(ref sheet, "J", item.Key, valueJ, false, false);
                            InsertValueCell(ref sheet, "K", item.Key, valueK, false, false);
                            InsertValueCell(ref sheet, "L", item.Key, valueL, false, false);
                            InsertValueCell(ref sheet, "M", item.Key, valueM, false, false);
                            InsertValueCell(ref sheet, "N", item.Key, valueN, false, false);
                            InsertValueCell(ref sheet, "O", item.Key, valueO, false, false);
                            InsertValueCell(ref sheet, "P", item.Key, valueP, false, false);
                            InsertValueCell(ref sheet, "Q", item.Key, valueQ, false, false);
                            InsertValueCell(ref sheet, "R", item.Key, valueR, false, false);
                            InsertValueCell(ref sheet, "S", item.Key, valueS, false, false);
                            //iRowCurrentHHNK++;
                        }

                    }

                }
                int iRowStart = 19;
                int iRowCurrent = iRowStart;
                int intSTT = 1;
                foreach (DataRow drDT in dtbSheet02.Rows)
                {
                    int valueA = intSTT;
                    string valueB = (drDT["DANHMUC_TENKHO"] ?? "").ToString().Trim();
                    string valueC = (drDT["DVT"] ?? "").ToString().Trim();
                    decimal valueD = Convert.ToDecimal(drDT["DON_GIA"]);
                    decimal valueE = Convert.ToDecimal(drDT["SL_SOSACH"]);
                    decimal valueF = Convert.ToDecimal(drDT["TIEN_SOSACH"]);
                    decimal valueG = Convert.ToDecimal(drDT["SL_THUCTE"]);
                    decimal valueH = Convert.ToDecimal(drDT["TIEN_THUCTE"]);
                    decimal valueI = Convert.ToDecimal(drDT["SL_THUA"]);
                    decimal valueJ = Convert.ToDecimal(drDT["TIEN_THUA"]);
                    decimal valueK = Convert.ToDecimal(drDT["SL_THIEU"]);
                    decimal valueL = Convert.ToDecimal(drDT["TIEN_THIEU"]);
                    decimal valueM = Convert.ToDecimal(drDT["SL_TINHTRANG_TOT"]);
                    decimal valueN = Convert.ToDecimal(drDT["TIEN_TINHTRANG_TOT"]);
                    decimal valueO = Convert.ToDecimal(drDT["SL_TINHTRANG_KEMPHAM"]);
                    decimal valueP = Convert.ToDecimal(drDT["TIEN_TINHTRANG_KEMPHAM"]);
                    decimal valueQ = Convert.ToDecimal(drDT["SL_TINHTRANG_PHEPHAM"]);
                    decimal valueR = Convert.ToDecimal(drDT["TIEN_TINHTRANG_PHEPHAM"]);
                    string valueS = (drDT["GHI_CHU"] ?? "").ToString().Trim();
                    if (iRowCurrent != iRowStart)
                    {
                        ExcelRow rowTemplate = sheet.GetRow(iRowStart);
                        rowTemplate.InsertCopy(iRowCurrent, false);
                    }
                    InsertValueCell(ref sheet, "A", iRowCurrent, valueA, false, false); // STT
                    InsertValueCell(ref sheet, "B", iRowCurrent, valueB, false, false); // Mã ST
                    InsertValueCell(ref sheet, "C", iRowCurrent, valueC, false, false); //Ten SP
                    InsertValueCell(ref sheet, "D", iRowCurrent, valueD, false, false); // IMEI
                    InsertValueCell(ref sheet, "E", iRowCurrent, valueE, false, false);
                    InsertValueCell(ref sheet, "F", iRowCurrent, valueF, false, false);
                    InsertValueCell(ref sheet, "G", iRowCurrent, valueG, false, false);
                    InsertValueCell(ref sheet, "H", iRowCurrent, valueH, false, false);
                    InsertValueCell(ref sheet, "I", iRowCurrent, valueI, false, false);
                    InsertValueCell(ref sheet, "J", iRowCurrent, valueJ, false, false);
                    InsertValueCell(ref sheet, "K", iRowCurrent, valueK, false, false);
                    InsertValueCell(ref sheet, "L", iRowCurrent, valueL, false, false);
                    InsertValueCell(ref sheet, "M", iRowCurrent, valueM, false, false);
                    InsertValueCell(ref sheet, "N", iRowCurrent, valueN, false, false);
                    InsertValueCell(ref sheet, "O", iRowCurrent, valueO, false, false);
                    InsertValueCell(ref sheet, "P", iRowCurrent, valueP, false, false);
                    InsertValueCell(ref sheet, "Q", iRowCurrent, valueQ, false, false);
                    InsertValueCell(ref sheet, "R", iRowCurrent, valueR, false, false);
                    InsertValueCell(ref sheet, "S", iRowCurrent, valueS, false, false);
                    iRowCurrent++;
                    intSTT++;
                }

                #endregion
                return true;
            }
            return false;
        }

        private bool ExportExcel_PL2ChiTiet(ref ExcelWorksheet sheet, DataSet dtsData)
        {
            if (sheet != null)
            {
                DataTable dtbSheet02 = dtsData.Tables[1].Copy();
                #region Import Row

                int iRowStart = 18;
                int iRowCurrent = iRowStart;
                int intSTT = 1;
                foreach (DataRow drDT in dtbSheet02.Rows)
                {
                    int valueA = intSTT;
                    string valueB = (drDT["MA_DONVI"] ?? "").ToString().Trim();
                    string valueC = (drDT["MA_KHO"] ?? "").ToString().Trim();
                    string valueD = (drDT["DANHMUC_TENKHO"] ?? "").ToString().Trim();
                    decimal valueE = Convert.ToDecimal(drDT["DON_GIA"]);
                    decimal valueF = Convert.ToDecimal(drDT["SL_SOSACH"]);
                    decimal valueG = Convert.ToDecimal(drDT["TIEN_SOSACH"]);
                    decimal valueH = Convert.ToDecimal(drDT["SL_THUCTE"]);
                    decimal valueI = Convert.ToDecimal(drDT["TIEN_THUCTE"]);
                    decimal valueJ = Convert.ToDecimal(drDT["SL_THUA"]);
                    decimal valueK = Convert.ToDecimal(drDT["TIEN_THUA"]);
                    decimal valueL = Convert.ToDecimal(drDT["SL_THIEU"]);
                    decimal valueM = Convert.ToDecimal(drDT["TIEN_THIEU"]);
                    decimal valueN = Convert.ToDecimal(drDT["SL_TINHTRANG_TOT"]);
                    decimal valueO = Convert.ToDecimal(drDT["TIEN_TINHTRANG_TOT"]);
                    decimal valueP = Convert.ToDecimal(drDT["SL_TINHTRANG_KEMPHAM"]);
                    decimal valueQ = Convert.ToDecimal(drDT["TIEN_TINHTRANG_KEMPHAM"]);
                    decimal valueR = Convert.ToDecimal(drDT["SL_TINHTRANG_PHEPHAM"]);
                    decimal valueS = Convert.ToDecimal(drDT["TIEN_TINHTRANG_PHEPHAM"]);
                    string valueT = (drDT["GHI_CHU"] ?? "").ToString().Trim();
                    if (iRowCurrent != iRowStart)
                    {
                        ExcelRow rowTemplate = sheet.GetRow(iRowStart);
                        rowTemplate.InsertCopy(iRowCurrent, false);
                    }
                    InsertValueCell(ref sheet, "A", iRowCurrent, valueA, false, false); // STT
                    InsertValueCell(ref sheet, "B", iRowCurrent, valueB, false, false); // Mã ST
                    InsertValueCell(ref sheet, "C", iRowCurrent, valueC, false, false); //Ten SP
                    InsertValueCell(ref sheet, "D", iRowCurrent, valueD, false, false); // IMEI
                    InsertValueCell(ref sheet, "E", iRowCurrent, valueE, false, false);
                    InsertValueCell(ref sheet, "F", iRowCurrent, valueF, false, false);
                    InsertValueCell(ref sheet, "G", iRowCurrent, valueG, false, false);
                    InsertValueCell(ref sheet, "H", iRowCurrent, valueH, false, false);
                    InsertValueCell(ref sheet, "I", iRowCurrent, valueI, false, false);
                    InsertValueCell(ref sheet, "J", iRowCurrent, valueJ, false, false);
                    InsertValueCell(ref sheet, "K", iRowCurrent, valueK, false, false);
                    InsertValueCell(ref sheet, "L", iRowCurrent, valueL, false, false);
                    InsertValueCell(ref sheet, "M", iRowCurrent, valueM, false, false);
                    InsertValueCell(ref sheet, "N", iRowCurrent, valueN, false, false);
                    InsertValueCell(ref sheet, "O", iRowCurrent, valueO, false, false);
                    InsertValueCell(ref sheet, "P", iRowCurrent, valueP, false, false);
                    InsertValueCell(ref sheet, "Q", iRowCurrent, valueQ, false, false);
                    InsertValueCell(ref sheet, "R", iRowCurrent, valueR, false, false);
                    InsertValueCell(ref sheet, "S", iRowCurrent, valueS, false, false);
                    InsertValueCell(ref sheet, "T", iRowCurrent, valueT, false, false);
                    iRowCurrent++;
                    intSTT++;
                }

                #endregion
                return true;
            }
            return false;
        }

        private bool ExportExcel_PL3(ref ExcelWorksheet sheet, DataSet dtsData)
        {
            IDictionary<int, string> dict = new Dictionary<int, string>() {
                                                //{31,"II_1"},
                                                //{32, "II_2"},
                                                //{33,"II_3"},
                                                //{34,"II_4"},
                                                //{35, "II_5"},
                                                //{36,"II_6"},
                                                {26,"6_b"},
                                                {18, "2"}
            };
            if (sheet != null)
            {
                DataTable dtbSheet03 = dtsData.Tables[2].Copy();
                #region Import Row
                foreach (KeyValuePair<int, string> item in dict)
                {
                    int iRowStart = item.Key;
                    int iRowCurrent = iRowStart;
                    int intSTT = 1;
                    foreach (DataRow drDT in dtbSheet03.Rows)
                    {
                        if (item.Value == (drDT["NHOM_VTTB_HH"] ?? "").ToString().Trim())
                        {
                            iRowCurrent = item.Key + 1;
                            string valueA = "";
                            string valueB = "";
                            if (item.Value == "6_b")
                                valueB = "";
                            string valueC = (drDT["TEN_SP"] ?? "").ToString().Trim();
                            string valueD = (drDT["MA_SP"] ?? "").ToString().Trim();
                            string valueE = (drDT["DVT"] ?? "").ToString().Trim();
                            decimal valueF = Convert.ToDecimal(drDT["DON_GIA"]);
                            decimal valueG = Convert.ToDecimal(drDT["SL_SOSACH"]);
                            decimal valueH = Convert.ToDecimal(drDT["TIEN_SOSACH"]);
                            decimal valueI = Convert.ToDecimal(drDT["SL_THUCTE"]);
                            decimal valueJ = Convert.ToDecimal(drDT["TIEN_THUCTE"]);
                            decimal valueK = Convert.ToDecimal(drDT["SL_THUA"]);
                            decimal valueL = Convert.ToDecimal(drDT["TIEN_THUA"]);
                            decimal valueM = Convert.ToDecimal(drDT["SL_THIEU"]);
                            decimal valueN = Convert.ToDecimal(drDT["TIEN_THIEU"]);
                            decimal valueO = Convert.ToDecimal(drDT["SL_TINHTRANG_TOT"]);
                            decimal valueP = Convert.ToDecimal(drDT["TIEN_TINHTRANG_TOT"]);
                            decimal valueQ = Convert.ToDecimal(drDT["SL_TINHTRANG_KEMPHAM"]);
                            decimal valueR = Convert.ToDecimal(drDT["TIEN_TINHTRANG_KEMPHAM"]);
                            decimal valueS = Convert.ToDecimal(drDT["SL_TINHTRANG_PHEPHAM"]);
                            decimal valueT = Convert.ToDecimal(drDT["TIEN_TINHTRANG_PHEPHAM"]);
                            //string valueU = "";// (drDT["GHI_CHU"] ?? "").ToString().Trim();
                            //if (iRowCurrent != iRowStart)
                            //{
                            ExcelRow rowTemplate = sheet.GetRow(iRowStart);
                            rowTemplate.InsertCopy(iRowCurrent, false);
                            //}
                            InsertValueCell(ref sheet, "A", iRowCurrent, valueA, false, false); // STT
                            InsertValueCell(ref sheet, "B", iRowCurrent, valueB, false, false); // Mã ST
                            InsertValueCell(ref sheet, "C", iRowCurrent, valueC, false, false); //Ten SP
                            InsertValueCell(ref sheet, "D", iRowCurrent, valueD, false, false); // IMEI
                            InsertValueCell(ref sheet, "E", iRowCurrent, valueE, false, false);
                            InsertValueCell(ref sheet, "F", iRowCurrent, valueF, false, false);
                            InsertValueCell(ref sheet, "G", iRowCurrent, valueG, false, false);
                            InsertValueCell(ref sheet, "H", iRowCurrent, valueH, false, false);
                            InsertValueCell(ref sheet, "I", iRowCurrent, valueI, false, false);
                            InsertValueCell(ref sheet, "J", iRowCurrent, valueJ, false, false);
                            InsertValueCell(ref sheet, "K", iRowCurrent, valueK, false, false);
                            InsertValueCell(ref sheet, "L", iRowCurrent, valueL, false, false);
                            InsertValueCell(ref sheet, "M", iRowCurrent, valueM, false, false);
                            InsertValueCell(ref sheet, "N", iRowCurrent, valueN, false, false);
                            InsertValueCell(ref sheet, "O", iRowCurrent, valueO, false, false);
                            InsertValueCell(ref sheet, "P", iRowCurrent, valueP, false, false);
                            InsertValueCell(ref sheet, "Q", iRowCurrent, valueQ, false, false);
                            InsertValueCell(ref sheet, "R", iRowCurrent, valueR, false, false);
                            InsertValueCell(ref sheet, "S", iRowCurrent, valueS, false, false);
                            //InsertValueCell(ref sheet, "U", iRowCurrent, valueU, false, false);
                            iRowCurrent++;
                            intSTT++;
                        }
                    }

                }

                #endregion
                return true;
            }
            return false;
        }


        
        private bool ExportExcel_PL4(ref ExcelWorksheet sheet, DataSet dtsData)
        {
            if (sheet != null)
            {
                DataTable dtbSheet04 = dtsData.Tables[3].Copy();

                DataTable dtbSheet4Thua = null;
                DataTable dtbSheet4Thieu = null;
                if (dtbSheet04.Select("ISTHUA = 1").Any()) dtbSheet4Thua = dtbSheet04.Select("ISTHUA = 1").CopyToDataTable();
                if (dtbSheet04.Select("ISTHUA = 0").Any()) dtbSheet4Thieu = dtbSheet04.Select("ISTHUA = 0").CopyToDataTable();
                #region Import Row

                int iRowStart = 17;
                int iRowCurrent = iRowStart;
                int intSTT = 1;
                if (dtbSheet4Thieu != null)
                {
                    int iRowCopy = 17;
                    DataTable dtbGroup = dtbSheet4Thieu.AsDataView().ToTable(true, "ID", "NHOM_NGUYEN_NHAN");
                    dtbGroup.Columns.Add("ID_COLUMN", typeof(int));
                    DataTable dtbGroup2 = dtbSheet4Thieu.AsDataView().ToTable(true, "ID", "NHOM_NGUYEN_NHAN", "MA_NHOM_NGUYEN_NHAN", "TEN_NHOM_NGUYEN_NHAN");
                    dtbGroup2.Columns.Add("ID_COLUMN", typeof(int));
                    foreach (DataRow drDT in dtbGroup.Rows)
                    {
                        drDT["ID_COLUMN"] = iRowCurrent;
                        iRowCurrent++;
                        intSTT++;
                    }
                    dtbGroup.AcceptChanges();
                    DataView dtv = dtbGroup.AsDataView();
                    dtv.Sort = "ID_COLUMN";
                    int intGroup = 1;
                    foreach (DataRowView drDTV in dtv)
                    {
                        string strKey = (drDTV["ID"] ?? "").ToString().Trim();
                        var varResult = dtbGroup2.Select("ID = '" + strKey + "'");//.Where(row => row.Field<string>("ID") == strKey);
                        if (varResult.Any())
                        {
                            
                            iRowStart = intGroup + Convert.ToInt32(drDTV["ID_COLUMN"]);
                            intGroup += varResult.Count() - 1;
                            iRowCurrent = iRowStart;
                            foreach (DataRow dr2 in varResult)
                            {
                                dr2["ID_COLUMN"] = iRowCurrent;
                                iRowCurrent++;

                            }
                            drDTV["ID_COLUMN"] = iRowStart - 1;
                        }
                        intGroup++;
                    }
                    dtbGroup2.AcceptChanges();
                    intSTT = 1;
                    foreach (DataRowView drGoup1 in dtv)
                    {
                        int valueA = intSTT;
                        iRowCurrent = Convert.ToInt32(drGoup1["ID_COLUMN"]);
                        string valueB = (drGoup1["NHOM_NGUYEN_NHAN"] ?? "").ToString().Trim();

                        if (iRowCurrent != iRowCopy)
                        {
                            ExcelRow rowTemplate = sheet.GetRow(iRowCopy);
                            rowTemplate.InsertCopy(iRowCurrent, false);
                        }
                        InsertValueCell(ref sheet, "A", iRowCurrent, valueA, false, false); // STT
                        InsertValueCell(ref sheet, "B", iRowCurrent, valueB, false, false); // Mã ST
                        string strKey = (drGoup1["ID"] ?? "").ToString().Trim();
                        iRowCopy = iRowCurrent;
                        var varResult = dtbGroup2.Select("ID = '" + strKey + "'");//.Where(row => row.Field<string>("ID") == strKey);
                        if(varResult.Any())
                        {
                            DataTable dtbTmp = varResult.CopyToDataTable();
                            DataView dtvGroup2 = dtbTmp.AsDataView();
                            dtvGroup2.Sort = "ID_COLUMN";
                            foreach (DataRowView drGroup2 in dtvGroup2)
                            {
                                iRowCurrent = Convert.ToInt32(drGroup2["ID_COLUMN"]);
                                valueB = (drGroup2["TEN_NHOM_NGUYEN_NHAN"] ?? "").ToString().Trim();
                                if (iRowCurrent != iRowCopy)
                                {
                                    ExcelRow rowTemplate = sheet.GetRow(iRowCopy);
                                    rowTemplate.InsertCopy(iRowCurrent, false);
                                }
                                InsertValueCell(ref sheet, "B", iRowCurrent, valueB, false, false); // Mã ST
                            }
                        }
                        intSTT++;
                    }

                    DataView dtvG = dtbGroup2.AsDataView();
                    dtvG.Sort = "ID_COLUMN DESC";
                    foreach (DataRowView drDTV in dtvG)
                    {
                        iRowStart = Convert.ToInt32(drDTV["ID_COLUMN"]);
                        iRowCopy = iRowStart;
                        iRowCurrent = iRowStart;
                        var varResult = dtbSheet4Thieu.AsEnumerable()
                           .Where(row => row.Field<string>("ID") == (drDTV["ID"] ?? "").ToString()
                                     && (row.Field<object>("MA_NHOM_NGUYEN_NHAN") ?? "").ToString() == (drDTV["MA_NHOM_NGUYEN_NHAN"] ?? "").ToString());
                        DataTable dtbData = varResult.CopyToDataTable();
                        string strRowName = string.Empty;
                        foreach (DataRow drDT in dtbData.Rows)
                        {
                            int valueA = intSTT;
                            string valueB = (drDT["TEN_NHOM_NGUYEN_NHAN"] ?? "").ToString().Trim();
                            string valueC = (drDT["TEN_SP"] ?? "").ToString().Trim();
                            string valueD = (drDT["DVT"] ?? "").ToString().Trim();
                            decimal valueE = Convert.ToDecimal(drDT["SL_SOSACH"]);
                            decimal valueF = Convert.ToDecimal(drDT["TIEN_SOSACH"]);
                            decimal valueG = Convert.ToDecimal(drDT["SL_THUCTE"]);
                            decimal valueH = Convert.ToDecimal(drDT["TIEN_THUCTE"]);
                            decimal valueI = Convert.ToDecimal(drDT["SL_THUA"]);
                            decimal valueJ = Convert.ToDecimal(drDT["TIEN_THUA"]);
                            decimal valueK = Convert.ToDecimal(drDT["SL_THIEU"]);
                            decimal valueL = Convert.ToDecimal(drDT["TIEN_THIEU"]);
                            string valueM = Convert.ToString(drDT["DANGKY_TG_XULY"]);
                            if (iRowCurrent != iRowCopy)
                            {
                                ExcelRow rowTemplate = sheet.GetRow(iRowCopy);
                                rowTemplate.InsertCopy(iRowCurrent, false);
                                InsertValueCell(ref sheet, "B", iRowCurrent, string.Empty, false, false); //Ten SP
                            }
                            InsertValueCell(ref sheet, "C", iRowCurrent, valueC, false, false); //Ten SP
                            InsertValueCell(ref sheet, "D", iRowCurrent, valueD, false, false); // IMEI
                            InsertValueCell(ref sheet, "E", iRowCurrent, valueE, false, false);
                            InsertValueCell(ref sheet, "F", iRowCurrent, valueF, false, false);
                            InsertValueCell(ref sheet, "G", iRowCurrent, valueG, false, false);
                            InsertValueCell(ref sheet, "H", iRowCurrent, valueH, false, false);
                            InsertValueCell(ref sheet, "I", iRowCurrent, valueI, false, false);
                            InsertValueCell(ref sheet, "J", iRowCurrent, valueJ, false, false);
                            InsertValueCell(ref sheet, "K", iRowCurrent, valueK, false, false);
                            InsertValueCell(ref sheet, "L", iRowCurrent, valueL, false, false);
                            InsertValueCell(ref sheet, "M", iRowCurrent, valueM, false, false);
                            iRowCurrent++;
                            intSTT++;
                        }
                    }
                }

                if (dtbSheet4Thua != null)
                {
                    int iRowCopy = 15;
                    iRowStart = 15;
                    iRowCurrent = iRowStart;
                    intSTT = 1;
                    DataTable dtbGroup = dtbSheet4Thua.AsDataView().ToTable(true, "ID", "NHOM_NGUYEN_NHAN");
                    dtbGroup.Columns.Add("ID_COLUMN", typeof(int));
                    DataTable dtbGroup2 = dtbSheet4Thua.AsDataView().ToTable(true, "ID", "NHOM_NGUYEN_NHAN", "MA_NHOM_NGUYEN_NHAN", "TEN_NHOM_NGUYEN_NHAN");
                    dtbGroup2.Columns.Add("ID_COLUMN", typeof(int));
                    foreach (DataRow drDT in dtbGroup.Rows)
                    {
                        drDT["ID_COLUMN"] = iRowCurrent;
                        iRowCurrent++;
                        intSTT++;
                    }
                    dtbGroup.AcceptChanges();
                    DataView dtv = dtbGroup.AsDataView();
                    dtv.Sort = "ID_COLUMN";
                    int intGroup = 1;
                    foreach (DataRowView drDTV in dtv)
                    {
                        string strKey = (drDTV["ID"] ?? "").ToString().Trim();
                        var varResult = dtbGroup2.Select("ID = '" + strKey + "'");//.Where(row => row.Field<string>("ID") == strKey);
                        if (varResult.Any())
                        {

                            iRowStart = intGroup + Convert.ToInt32(drDTV["ID_COLUMN"]);
                            intGroup += varResult.Count() - 1;
                            iRowCurrent = iRowStart;
                            foreach (DataRow dr2 in varResult)
                            {
                                dr2["ID_COLUMN"] = iRowCurrent;
                                iRowCurrent++;

                            }
                            drDTV["ID_COLUMN"] = iRowStart - 1;
                        }
                        intGroup++;
                    }
                    dtbGroup2.AcceptChanges();
                    intSTT = 1;
                    foreach (DataRowView drGoup1 in dtv)
                    {
                        int valueA = intSTT;
                        iRowCurrent = Convert.ToInt32(drGoup1["ID_COLUMN"]);
                        string valueB = (drGoup1["NHOM_NGUYEN_NHAN"] ?? "").ToString().Trim();

                        if (iRowCurrent != iRowCopy)
                        {
                            ExcelRow rowTemplate = sheet.GetRow(iRowCopy);
                            rowTemplate.InsertCopy(iRowCurrent, false);
                        }
                        InsertValueCell(ref sheet, "A", iRowCurrent, valueA, false, false); // STT
                        InsertValueCell(ref sheet, "B", iRowCurrent, valueB, false, false); // Mã ST
                        string strKey = (drGoup1["ID"] ?? "").ToString().Trim();
                        iRowCopy = iRowCurrent;
                        var varResult = dtbGroup2.Select("ID = '" + strKey + "'");//.Where(row => row.Field<string>("ID") == strKey);
                        if (varResult.Any())
                        {
                            DataTable dtbTmp = varResult.CopyToDataTable();
                            DataView dtvGroup2 = dtbTmp.AsDataView();
                            dtvGroup2.Sort = "ID_COLUMN";
                            foreach (DataRowView drGroup2 in dtvGroup2)
                            {
                                iRowCurrent = Convert.ToInt32(drGroup2["ID_COLUMN"]);
                                valueB = (drGroup2["TEN_NHOM_NGUYEN_NHAN"] ?? "").ToString().Trim();
                                if (iRowCurrent != iRowCopy)
                                {
                                    ExcelRow rowTemplate = sheet.GetRow(iRowCopy);
                                    rowTemplate.InsertCopy(iRowCurrent, false);
                                }
                                InsertValueCell(ref sheet, "B", iRowCurrent, valueB, false, false); // Mã ST
                            }
                        }
                        intSTT++;
                    }

                    DataView dtvG = dtbGroup2.AsDataView();
                    dtvG.Sort = "ID_COLUMN DESC";
                    foreach (DataRowView drDTV in dtvG)
                    {
                        iRowStart = Convert.ToInt32(drDTV["ID_COLUMN"]);
                        iRowCopy = iRowStart;
                        iRowCurrent = iRowStart;
                        var varResult = dtbSheet4Thua.AsEnumerable()
                           .Where(row => row.Field<string>("ID") == (drDTV["ID"] ?? "").ToString()
                                     && (row.Field<object>("MA_NHOM_NGUYEN_NHAN") ?? "").ToString() == (drDTV["MA_NHOM_NGUYEN_NHAN"] ?? "").ToString());
                        DataTable dtbData = varResult.CopyToDataTable();
                        string strRowName = string.Empty;
                        foreach (DataRow drDT in dtbData.Rows)
                        {
                            int valueA = intSTT;
                            string valueB = (drDT["TEN_NHOM_NGUYEN_NHAN"] ?? "").ToString().Trim();
                            string valueC = (drDT["TEN_SP"] ?? "").ToString().Trim();
                            string valueD = (drDT["DVT"] ?? "").ToString().Trim();
                            decimal valueE = Convert.ToDecimal(drDT["SL_SOSACH"]);
                            decimal valueF = Convert.ToDecimal(drDT["TIEN_SOSACH"]);
                            decimal valueG = Convert.ToDecimal(drDT["SL_THUCTE"]);
                            decimal valueH = Convert.ToDecimal(drDT["TIEN_THUCTE"]);
                            decimal valueI = Convert.ToDecimal(drDT["SL_THUA"]);
                            decimal valueJ = Convert.ToDecimal(drDT["TIEN_THUA"]);
                            decimal valueK = Convert.ToDecimal(drDT["SL_THIEU"]);
                            decimal valueL = Convert.ToDecimal(drDT["TIEN_THIEU"]);
                            string valueM = Convert.ToString(drDT["DANGKY_TG_XULY"]);
                            if (iRowCurrent != iRowCopy)
                            {
                                ExcelRow rowTemplate = sheet.GetRow(iRowCopy);
                                rowTemplate.InsertCopy(iRowCurrent, false);
                                InsertValueCell(ref sheet, "B", iRowCurrent, string.Empty, false, false); //Ten SP
                            }
                            InsertValueCell(ref sheet, "C", iRowCurrent, valueC, false, false); //Ten SP
                            InsertValueCell(ref sheet, "D", iRowCurrent, valueD, false, false); // IMEI
                            InsertValueCell(ref sheet, "E", iRowCurrent, valueE, false, false);
                            InsertValueCell(ref sheet, "F", iRowCurrent, valueF, false, false);
                            InsertValueCell(ref sheet, "G", iRowCurrent, valueG, false, false);
                            InsertValueCell(ref sheet, "H", iRowCurrent, valueH, false, false);
                            InsertValueCell(ref sheet, "I", iRowCurrent, valueI, false, false);
                            InsertValueCell(ref sheet, "J", iRowCurrent, valueJ, false, false);
                            InsertValueCell(ref sheet, "K", iRowCurrent, valueK, false, false);
                            InsertValueCell(ref sheet, "L", iRowCurrent, valueL, false, false);
                            InsertValueCell(ref sheet, "M", iRowCurrent, valueM, false, false);
                            iRowCurrent++;
                            intSTT++;
                        }
                    }
                }




                    #endregion
                    return true;
            }
            return false;
        }

        private bool ExportExcel_PL5(ref ExcelWorksheet sheet, DataSet dtsData)
        {
            if (sheet != null)
            {
                DataTable dtbSheet05 = dtsData.Tables[4].Copy();
                #region Import Row

                int iRowStart = 14;
                int iRowCurrent = iRowStart;
                int intSTT = 1;
                foreach (DataRow drDT in dtbSheet05.Rows)
                {
                    int valueA = intSTT;
                    string valueB = (drDT["STORENAME"] ?? "").ToString().Trim();
                    string valueC = (drDT["STOREID"] ?? "").ToString().Trim();
                    string valueD = (drDT["DV_QUANLY"] ?? "").ToString().Trim();
                    string valueE = (drDT["MADV_QUANLY"] ?? "").ToString().Trim();
                    string valueF = (drDT["DV_GIAMSAT"] ?? "").ToString().Trim();
                    string valueG = (drDT["MA_TINH"] ?? "").ToString().Trim();
                    string valueH = (drDT["SL_VITRI"] ?? "").ToString().Trim();
                    string valueI = (drDT["DT_TONG_CONG"] ?? "").ToString().Trim();
                    string valueJ = (drDT["DT_NHAKHO"] ?? "").ToString().Trim();
                    string valueK = (drDT["DT_BAI_KHO"] ?? "").ToString().Trim();
                    string valueL = (drDT["TINHTRANG_SOHUU"] ?? "").ToString().Trim();
                    string valueM = (drDT["KIEUKHO"] ?? "").ToString().Trim();
                    string valueN = (drDT["LOAI_KHO"] ?? "").ToString().Trim();
                    string valueO = (drDT["DOI_TAC"] ?? "").ToString().Trim();
                    string valueP = (drDT["CAP_KHO"] ?? "").ToString().Trim();
                    string valueQ = (drDT["IS_CON_TON"] ?? "").ToString().Trim();
                    string valueR = (drDT["IS_KHOA_KHO"] ?? "").ToString().Trim();
                    string valueS = (drDT["IS_KIEM_KE"] ?? "").ToString().Trim();
                    if (iRowCurrent != iRowStart)
                    {
                        ExcelRow rowTemplate = sheet.GetRow(iRowStart);
                        rowTemplate.InsertCopy(iRowCurrent, false);
                    }
                    InsertValueCell(ref sheet, "A", iRowCurrent, valueA, false, false); // STT
                    InsertValueCell(ref sheet, "B", iRowCurrent, valueB, false, false); // Mã ST
                    InsertValueCell(ref sheet, "C", iRowCurrent, valueC, false, false); //Ten SP
                    InsertValueCell(ref sheet, "D", iRowCurrent, valueD, false, false); // IMEI
                    InsertValueCell(ref sheet, "E", iRowCurrent, valueE, false, false);
                    InsertValueCell(ref sheet, "F", iRowCurrent, valueF, false, false);
                    InsertValueCell(ref sheet, "G", iRowCurrent, valueG, false, false);
                    InsertValueCell(ref sheet, "H", iRowCurrent, valueH, false, false);
                    InsertValueCell(ref sheet, "I", iRowCurrent, valueI, false, false);
                    InsertValueCell(ref sheet, "J", iRowCurrent, valueJ, false, false);
                    InsertValueCell(ref sheet, "K", iRowCurrent, valueK, false, false);
                    InsertValueCell(ref sheet, "L", iRowCurrent, valueL, false, false);
                    InsertValueCell(ref sheet, "M", iRowCurrent, valueM, false, false);
                    InsertValueCell(ref sheet, "N", iRowCurrent, valueN, false, false);
                    InsertValueCell(ref sheet, "O", iRowCurrent, valueO, false, false);
                    InsertValueCell(ref sheet, "P", iRowCurrent, valueP, false, false);
                    InsertValueCell(ref sheet, "Q", iRowCurrent, valueQ, false, false);
                    InsertValueCell(ref sheet, "R", iRowCurrent, valueR, false, false);
                    InsertValueCell(ref sheet, "S", iRowCurrent, valueS, false, false);
                    iRowCurrent++;
                    intSTT++;
                }

                #endregion
                return true;
            }
            return false;
        }

        #endregion


        private bool ExportExcelBCKK(DataSet dtbDataSet, string strFileTemplate, string strPathFileName)
        {
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
            }
            catch { }

            if (workbook != null)
            {
                string strSheetName = "Phụ Lục 01";
                #region Phu luc 01
                ExcelWorksheet sheet1 = (ExcelWorksheet)workbook.Worksheets[0];
                ExportExcel_PL1TongHop(ref sheet1, dtbDataSet);
                ExcelWorksheet sheet2 = (ExcelWorksheet)workbook.Worksheets[1];
                ExportExcel_PL2ChiTiet(ref sheet2, dtbDataSet);
                ExcelWorksheet sheet3 = (ExcelWorksheet)workbook.Worksheets[2];
                ExportExcel_PL3(ref sheet3, dtbDataSet);
                ExcelWorksheet sheet4 = (ExcelWorksheet)workbook.Worksheets[3];
                ExportExcel_PL4(ref sheet4, dtbDataSet);
                ExcelWorksheet sheet5 = (ExcelWorksheet)workbook.Worksheets[4];
                ExportExcel_PL5(ref sheet5, dtbDataSet);
                //ExportExcelSheetDetail(ref sheet, dtbData, dtbDanhMuc, dtbDanhMucNguyenNhan);
                #endregion

            }
            try
            {
                workbook.SaveXlsx(strPathFileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void ExportBCKK(DataSet dtbDataSet)
        {
            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strSheetName = string.Empty;

            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\InventoryTermStockBCKK.xlsx";
            if (CheckFileTemplateBCKK(strFileTemplate, ref strPathFileName) == false) return;
            if (ExportExcelBCKK(dtbDataSet, strFileTemplate, strPathFileName))
            {
                OpenFileExport(strPathFileName);
                return;
            }
            else
            {
                return;
            }
        }

        #endregion

        public void Export(DataTable dtbData, DataTable dtProductStatus)
        {
            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strSheetName = string.Empty;
            strSheetName = "Mau 01A- KKHH";
            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\InventoryTermStock.xlsx";
            if (CheckFileTemplate(strFileTemplate, ref strPathFileName) == false) return;
            if (ExportExcel(dtbData, dtProductStatus, strFileTemplate, strPathFileName, strSheetName))
            {
                OpenFileExport(strPathFileName);
                return;
            }
            else
            {
                return;
            }
        }
        private bool ExportExcel(DataTable dtbData, DataTable dtProductStatus, string strFileTemplate, string strPathFileName, string strSheetName)
        {
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
            }
            catch { }

            if (workbook != null)
            {
                ExcelWorksheet sheet = (ExcelWorksheet)workbook.Worksheets[strSheetName];
                if (sheet != null)
                {
                    #region Import Row

                    int iRowStart = 19;
                    int iRowCurrent = iRowStart;
                    int intSTT = 1;
                    int iColumnStart = GemBox.Spreadsheet.ExcelColumnCollection.GetColumnIndex("K");
                    int iColumnEnd = CreateColumnExcel(ref sheet, "K", dtProductStatus);
                    DataTable dtColumnProductStatus = DataTableClass.SelectDistinct(dtProductStatus, "PRODUCTSTATUSID");
                    int intMainGroupIDTemp = 0;
                    int intSubGroupIDTemp = 0;
                    int intMainGroupIndex = 0;
                    int intSubGroupIndex = 0;
                    bool bolInsertMainGroup = false;
                    bool bolInsertSubGroup = false;
                    for (int i = 0; i < dtbData.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dtbData.Rows[i]["MAINGROUPID"]) == intMainGroupIDTemp)
                            bolInsertMainGroup = false;
                        else
                        {
                            bolInsertMainGroup = true;
                            intMainGroupIDTemp = Convert.ToInt32(dtbData.Rows[i]["MAINGROUPID"]);
                            intMainGroupIndex++;
                        }

                        if (Convert.ToInt32(dtbData.Rows[i]["SUBGROUPID"]) == intSubGroupIDTemp)
                            bolInsertSubGroup = false;
                        else
                        {
                            bolInsertSubGroup = true;
                            intSubGroupIDTemp = Convert.ToInt32(dtbData.Rows[i]["SUBGROUPID"]);
                            intSubGroupIndex++;
                        }


                        AddRow(ref sheet, dtbData.Rows[i], ref iRowCurrent, iRowStart, ref intSTT, intMainGroupIndex, intSubGroupIndex, bolInsertMainGroup, bolInsertSubGroup, iColumnStart, iColumnEnd, dtColumnProductStatus);
                    }

                    InsertValueCell(ref sheet, "D", iRowCurrent, string.Format(@"=SUM(D{0}:D{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "E", iRowCurrent, string.Format(@"=SUM(E{0}:E{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "F", iRowCurrent, string.Format(@"=SUM(F{0}:F{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "G", iRowCurrent, string.Format(@"=SUM(G{0}:G{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "H", iRowCurrent, string.Format(@"=SUM(H{0}:H{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "I", iRowCurrent, string.Format(@"=SUM(I{0}:I{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "J", iRowCurrent, string.Format(@"=SUM(J{0}:J{1})", iRowStart, iRowCurrent - 1), false, true);

                    int iTemp = 0;
                    for (int i = iColumnStart; i <= iColumnEnd; i++)
                    {
                        InsertValueCell(ref sheet, ExcelColumnCollection.GetColumnName(i), iRowCurrent, string.Format(@"=SUM({0}{1}:{0}{2})", ExcelColumnCollection.GetColumnName(i), iRowStart, iRowCurrent - 1), false, true);
                        iTemp++;
                    }

                    InsertValueCell(ref sheet, ExcelColumnCollection.GetColumnName(iColumnEnd + 1), iRowCurrent, string.Format(@"=SUM({0}{1}:{0}{2})", ExcelColumnCollection.GetColumnName(iColumnEnd + 1), iRowStart, iRowCurrent - 1), false, true);

                    #endregion
                }
            }
            try
            {
                workbook.SaveExcel(strPathFileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public int CreateColumnExcel(ref ExcelWorksheet sheet, string strColumnStart, DataTable dtProductStatus)
        {
            CellRange cellRange = null;
            int iRowTemplate = 17;
            int iColumnStar = ExcelColumnCollection.GetColumnIndex(strColumnStart);
            ExcelCell cellRangeTemplate = sheet.Cells[ExcelColumnCollection.GetColumnName(iColumnStar) + iRowTemplate];
            ExcelColumn columnTemplete = sheet.Columns["K"];
            DataTable dtColumnProductStatus = DataTableClass.SelectDistinct(dtProductStatus, "PRODUCTSTATUSNAME");
            int iCount = 0;
            for (int i = 0; i < dtColumnProductStatus.Rows.Count; i++)
            {
                if (i > 0)
                {
                    ExcelColumn columnInsert = sheet.Columns[iColumnStar + i - 1];
                    if (columnTemplete != null)
                    {
                        columnInsert.InsertCopy(1, columnTemplete);
                        cellRange = sheet.get_Range(ExcelColumnCollection.GetColumnName(iColumnStar + i) + iRowTemplate, ExcelColumnCollection.GetColumnName(iColumnStar + i) + iRowTemplate);
                        if (cellRange != null)
                        {
                            cellRange.CopyFormat(cellRangeTemplate);
                            cellRange.Value = dtColumnProductStatus.Rows[i]["PRODUCTSTATUSNAME"].ToString().Trim();
                        }

                        cellRange = sheet.get_Range(ExcelColumnCollection.GetColumnName(iColumnStar + i) + 18, ExcelColumnCollection.GetColumnName(iColumnStar + i) + 18);
                        if (cellRange != null)
                        {
                            cellRange.Value = 8 + i;
                            cellRange.CopyFormat(cellRangeTemplate);
                        }
                    }
                }
                else
                {
                    cellRange = sheet.get_Range(ExcelColumnCollection.GetColumnName(iColumnStar + i) + iRowTemplate, ExcelColumnCollection.GetColumnName(iColumnStar + i) + iRowTemplate);
                    if (cellRange != null)
                    {
                        cellRange.CopyFormat(cellRangeTemplate);
                        cellRange.Value = dtColumnProductStatus.Rows[i]["PRODUCTSTATUSNAME"].ToString().Trim();
                    }
                }
                iCount = i;
            }

            cellRange = sheet.get_Range(ExcelColumnCollection.GetColumnName(iColumnStar + iCount + 1) + 18, ExcelColumnCollection.GetColumnName(iColumnStar + iCount + 1) + 18);
            if (cellRange != null)
            {
                cellRange.Value = 9 + iCount;
                cellRange.CopyFormat(cellRangeTemplate);
            }

            cellRange = sheet.get_Range("K" + 16, ExcelColumnCollection.GetColumnName(iColumnStar + iCount + 1) + 16);
            if (cellRange != null)
            {
                cellRange.Merged = true;
                cellRange.SetHorizontalAlignment(HorizontalAlignmentStyle.Center);
                cellRange.CopyFormat(cellRangeTemplate);
            }

            return iColumnStar + iCount;
        }

        public void AddRow(ref ExcelWorksheet sheet, DataRow row, ref int iRow, int iRowTemplate, ref int iSTT, int intMainGroupIndex, int intSubGroupIndex, bool bolInsertMainGroup, bool bolInsertSubGroup, int iColumnStart, int iColumnEnd, DataTable dtColumnProductStatus)
        {
            if (bolInsertMainGroup)
            {
                if (iRowTemplate != iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(iRowTemplate);
                    sheet.GetRow(iRowTemplate);
                    rowTemplate.InsertCopy(iRow, false);
                }

                InsertValueCell(ref sheet, "B", iRow, row["MAINGROUPNAME"].ToString(), false, true);
                iRow++;
            }

            if (bolInsertMainGroup)
            {
                if (iRowTemplate != iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(iRowTemplate);
                    sheet.GetRow(iRowTemplate);
                    rowTemplate.InsertCopy(iRow, false);
                }

                InsertValueCell(ref sheet, "B", iRow, row["SUBGROUPNAME"].ToString(), false, true);
                iRow++;
            }

            if (iRowTemplate != iRow)
            {
                ExcelRow rowTemplate = sheet.GetRow(iRowTemplate);
                sheet.GetRow(iRowTemplate);
                rowTemplate.InsertCopy(iRow, false);
            }

            InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
            InsertValueCell(ref sheet, "B", iRow, row["PRODUCTNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "C", iRow, row["QUANTITYUNITNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "D", iRow, Convert.ToDecimal(row["QUANTITY"]), false, false);
            InsertValueCell(ref sheet, "E", iRow, Convert.ToDecimal(row["CHECKQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "F", iRow, Convert.ToDecimal(row["RESIDUALQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "G", iRow, Convert.ToDecimal(row["MISSQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "H", iRow, Convert.ToDecimal(row["RESIDUALQUANTITY"]) - Convert.ToDecimal(row["MISSQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "I", iRow, Convert.ToDecimal(row["EXPLAINQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "J", iRow, Convert.ToDecimal(row["UNEXPLAINQUANTITY"]), false, false);
            iSTT++;

            int iTemp = 0;
            for (int i = iColumnStart; i <= iColumnEnd; i++)
            {
                InsertValueCell(ref sheet, ExcelColumnCollection.GetColumnName(i), iRow, row["PRODUCTSTATUSID_" + dtColumnProductStatus.Rows[iTemp]["PRODUCTSTATUSID"].ToString().Trim()], false, false);
                iTemp++;
            }

            InsertValueCell(ref sheet, ExcelColumnCollection.GetColumnName(iColumnEnd + 1), iRow, string.Format(@"=SUM({0}{1}:{2}{1})", ExcelColumnCollection.GetColumnName(iColumnStart), iRow, ExcelColumnCollection.GetColumnName(iColumnEnd)), false, false);

            iRow++;
        }

        private Boolean CheckFileTemplate(String strFileTemplate, ref string strPathFileName)
        {
            try
            {
                SaveFileDialog saveDlg = new SaveFileDialog();
                saveDlg.Filter = "Excel Worksheet file(*.xlsx)|*.xls";
                saveDlg.AddExtension = true;
                saveDlg.RestoreDirectory = true;
                saveDlg.Title = "Chọn vị trí lưu file?";
                saveDlg.FileName = "Báo cáo chốt kiểm kê theo kỳ " + System.DateTime.Now.Date.ToString("dd-MM-yyyy");
                if (saveDlg.ShowDialog() == DialogResult.OK)
                {
                    strPathFileName = System.IO.Path.ChangeExtension(saveDlg.FileName, ".xlsx");
                }
                if (string.IsNullOrEmpty(strPathFileName)) return false;

                if (System.IO.File.Exists(strPathFileName))
                {
                    try
                    {
                        System.IO.File.Delete(strPathFileName);
                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show("File Excel đang được mở, Bạn phải đóng lại trước khi mở file tiếp", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }

                if (System.IO.File.Exists(strFileTemplate) == false)
                {
                    MessageBox.Show("Tập tin mẫu không tồn tại");
                    return false;
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
        private void InsertValueCell(ref ExcelWorksheet worksheet, String strColumnName, int iRow, Object value, Boolean isAdd = false, Boolean isBold = false)
        {
            try
            {
                ExcelCell cell = worksheet.Cells[strColumnName + iRow];
                SetValueRange(ref cell, value, isAdd, isBold);
            }
            catch { }
        }
        private void SetValueRange(ref ExcelCell cell, Object value, Boolean isAdd, Boolean isBold)
        {
            try
            {
                if (isBold)
                    cell.Style.Font.Weight = ExcelFont.BoldWeight;
            }
            catch { }

            if (cell != null)
            {
                try
                {
                    if (isAdd)
                        cell.Value = cell.Value.ToString() + " " + value;
                    else
                        cell.Value = value;

                    if (value.ToString().StartsWith("="))
                        cell.Formula = value.ToString();
                }
                catch { }
            }
        }
        private void OpenFileExport(String strPathFileName)
        {
            try
            {
                if (MessageBox.Show("Bạn có muốn mở file vừa lưu?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    System.Diagnostics.Process.Start(strPathFileName);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void cboInvTypeSearch_SelectionChangeCommitted(object sender, EventArgs e)
        {
            DataTable dtData = objPLCInventoryTerm.SearchData(new object[] { "@InventoryTypeID", Convert.ToInt32(cboInvTypeSearch.SelectedValue), "@FromDate", dtpFromDate.Value, "@ToDate", dtpToDate.Value });
            cboInventoryTerm.InitControl(false, dtData, "INVENTORYTERMID", "INVENTORYTERMNAME", "-- Chọn kỳ kiểm kê --");
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            cboInvTypeSearch_SelectionChangeCommitted(null, null);
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            cboInvTypeSearch_SelectionChangeCommitted(null, null);
        }

        private int intReportType = -1;
        private void cboReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboReportType.SelectedIndex == intReportType)
                return;

            if (cboReportType.SelectedIndex == 4
                || cboReportType.SelectedIndex == 5
                || cboReportType.SelectedIndex == 6
                || cboReportType.SelectedIndex == 7)
            {
                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                if (cboBranch.BranchID > 0)
                    objStoreFilter.OtherCondition = "BRANCHID=" + cboBranch.BranchID;

                cboStoreIDList.InitControl(true, objStoreFilter);
            }
            else if (cboReportType.SelectedIndex != 4)
            {
                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                if (cboBranch.BranchID > 0)
                    objStoreFilter.OtherCondition = "BRANCHID=" + cboBranch.BranchID;

                cboStoreIDList.InitControl(false, objStoreFilter);
            }

            intReportType = cboReportType.SelectedIndex;
        }
    }
}
