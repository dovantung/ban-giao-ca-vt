﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.LoadControls;
using System.Collections;
using ERP.MasterData.PLC.MD;
using Library.AppControl;
using Library.AppCore;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmInventoryTerm : Form
    {
        public frmInventoryTerm()
        {
            InitializeComponent();
        }

        #region variable

        private string strPermission_Add = "INV_INVENTORYTERM_ADD";
        private string strPermission_Delete = "INV_INVENTORYTERM_DELETE";
        private string strPermission_Edit = "INV_INVENTORYTERM_EDIT";
        private string strPermission_ExportExcel = "INV_INVENTORYTERM_EXPORTEXCEL";
        private string strPermission__DeleteSearch = "INV_INVENTORYTERM_ISDELETED_SRH";
        private string strPermission_CalcInStock = "INV_INVENTORYTERM_CALCINSTOCK";
        private ERP.Inventory.PLC.Inventory.PLCInventoryTerm objPLCInventoryTerm = new PLC.Inventory.PLCInventoryTerm();
        private ERP.Inventory.PLC.Inventory.WSInventoryTerm.InventoryTerm objInventoryTerm = new PLC.Inventory.WSInventoryTerm.InventoryTerm();
        private ERP.Inventory.PLC.Inventory.WSInventoryTerm.ResultMessage objResultMessage = null;
        private DataTable dtbStore = null;
        private DataTable dtbMainGroup = null;
        private DataTable dtbBrand = null;
        private DataTable dtbSearch = null;
        private bool bolIsShowMessWaring = false;
        private int intIsLoadEdit = -1;
        private DataTable dtbSubGroup = null;
        private DataTable dtbSubGroupAll = null;
        private bool bolIsSelect = false;

        #endregion

        #region method

        private void LoadComboBox()
        {
            DataTable dtbProductStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
            //cboProductStateID.InitControl(false, dtbProductStatus, "PRODUCTSTATUSID", "PRODUCTSTATUSNAME", "--- Tất cả ---");
            DataTable dtbDataStatus = dtbProductStatus.Copy();
            DataRow drALL = dtbDataStatus.NewRow();
            drALL["PRODUCTSTATUSID"] = -1;
            drALL["PRODUCTSTATUSNAME"] = "--- Tất cả ---";
            dtbDataStatus.Rows.InsertAt(drALL, 0);
            cboProductStateID.DataSource = dtbDataStatus;
            cboProductStateID.DisplayMember = "PRODUCTSTATUSNAME";
            cboProductStateID.ValueMember = "PRODUCTSTATUSID";
            cboProductStateIDSearch.InitControl(false, dtbProductStatus, "PRODUCTSTATUSID", "PRODUCTSTATUSNAME", "--- Tất cả ---");
            cboProductStateIDSearch.SetValue(-1);
            Library.AppCore.LoadControls.SetDataSource.SetInventoryType(this, cboInventoryType);
            Library.AppCore.LoadControls.SetDataSource.SetInventoryType(this, cboInvTypeSearch);
            Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            cboStoreIDList.InitControl(true, true);
            string strUserName = SystemConfig.objSessionUser.UserName;
            object[] objKeywords = new object[]
            {
                "@InventoryTermID", -1
            };

            object[] objKeywords_GetAllStore = new object[] 
            {
                "@InventoryTermID", -1,
                "@UserName", strUserName
            };

            dtbStore = objPLCInventoryTerm.GetAllStore(objKeywords_GetAllStore);
            dtbMainGroup = objPLCInventoryTerm.GetAllMainGroup(objKeywords);
            dtbBrand = objPLCInventoryTerm.GetAllBrand(objKeywords);

            dtbSubGroup = new DataTable();
            dtbSubGroup.Columns.Add("IsSelect", typeof(Boolean));
            dtbSubGroup.Columns.Add("SubGroupID", typeof(Int32));
            dtbSubGroup.Columns.Add("SubGroupName", typeof(String));
            dtbSubGroup.Columns.Add("InventoryTermID", typeof(Int32));
            dtbSubGroup.Columns.Add("InventoryDate", typeof(DateTime));
            dtbSubGroup.Columns.Add("MainGroupID", typeof(Int32));
            dtbSubGroup.PrimaryKey = new DataColumn[] { dtbSubGroup.Columns["SubGroupID"] };
            dtbSubGroup.TableName = "Inv_Term_SubGroup";

            dtbSubGroupAll = Library.AppCore.DataSource.GetDataSource.GetSubGroup().Copy();
        }


        private void EditData(int intInventoryTermID)
        {
            intIsLoadEdit = 1;
            bolIsShowMessWaring = false;
            btnCalInStock.Enabled = false;
            LoadInfo(intInventoryTermID);
            objInventoryTerm.dtbTerm_Store.AcceptChanges();
            DataRow[] rSelect = objInventoryTerm.dtbTerm_Store.Select("IsSelect = 1");
            btnCalInStock.Enabled = Convert.ToBoolean(rSelect.Length) && SystemConfig.objSessionUser.IsPermission(strPermission_CalcInStock);
            intIsLoadEdit = 0;
        }

        private void LoadInfo(int intInventoryTermID)
        {
            txtInventoryTermID.Text = string.Empty;
            objPLCInventoryTerm.LoadInfo(ref objInventoryTerm, intInventoryTermID);
            if (objInventoryTerm != null)
            {

                txtInventoryTermID.Text = intInventoryTermID.ToString();
                txtInventoryTermName.Text = objInventoryTerm.InventoryTermName;
                txtDescription.Text = objInventoryTerm.Description;
                dtpInventoryDate.Value = objInventoryTerm.InventoryDate.Value;
                dtpBeginInventoryTime.Value = objInventoryTerm.BeginInventoryTime.Value;
                dtpEndInventoryTime.Value = objInventoryTerm.EndInventoryTime.Value;
                dtpLockDataTime.Value = objInventoryTerm.LockDataTime.Value;
                chkAllowBarcode.Checked = objInventoryTerm.Inv_AllowBarcode;
                cboInventoryType.SelectedValue = objInventoryTerm.InventoryTypeID;
                if (cboInventoryType.SelectedIndex < 0)
                    cboInventoryType.SelectedIndex = 0;
                cboProductStateID.SelectedValue = objInventoryTerm.ProductStatusID;// +1;
                radiSchedule.Checked = objInventoryTerm.IsCallStockBySchedule;
                flexStore.DataSource = objInventoryTerm.dtbTerm_Store;
                FormatFlexStore();
                flexMainGroup.DataSource = objInventoryTerm.dtbTerm_MainGroup;
                FormatFlexMainGroup();
                flexBrand.DataSource = objInventoryTerm.dtbTerm_Brand;
                FormatFlexBrand();
                objInventoryTerm.dtbTerm_SubGroup.PrimaryKey = new DataColumn[] { objInventoryTerm.dtbTerm_SubGroup.Columns["SubGroupID"] };
                flexSubGroup.DataSource = objInventoryTerm.dtbTerm_SubGroup;
                FormatFlexSubGroup();
            }
        }

        /// <summary>
        /// Kiểm tra phân quyền trên kho - Permission
        /// </summary>
        private void CheckIsHasPermission()
        {
            for (int i = flexStore.Rows.Fixed; i < flexStore.Rows.Count; i++)
            {
                if (Convert.ToInt32(flexStore.Rows[i]["ISHASPERMISSION"]) == 0)
                {
                    for (int j = 1; j < flexStore.Cols["ISHASPERMISSION"].Index; j++)
                    {
                        C1FlexGridObject.SetRowStyle(flexStore, i, Color.White, Color.Red, FontStyle.Italic);
                        flexStore.Rows[i].AllowEditing = false;
                    }
                }
            }
        }

        /// <summary>
        /// Thong bao thoi gian tin ton kho ra label
        /// </summary>
        private void GetCalInStockStatus()
        {
            for (int i = flexStore.Rows.Fixed; i < flexStore.Rows.Count; i++)
            {
                if (flexStore.Rows[i]["UpdateStockDataTime"] != null)
                {
                    try
                    {
                        lblStatus.Text = "Phương thức chốt tồn: ( Đã tính tồn kho vào lúc: " + Convert.ToDateTime(flexStore.Rows[i]["UpdateStockDataTime"]).ToString("dd/MM/yyyy HH:mm") + ")";
                        break;
                    }
                    catch
                    {
                        lblStatus.Text = "Phương thức chốt tồn:";
                    }
                }
            }
        }

        private void InitFlex()
        {
            flexStore.DataSource = dtbStore.Copy();
            FormatFlexStore();
            flexMainGroup.DataSource = dtbMainGroup.Copy();
            FormatFlexMainGroup();
            flexBrand.DataSource = dtbBrand.Copy();
            FormatFlexBrand();
            flexSubGroup.DataSource = dtbSubGroup.Copy();
            FormatFlexSubGroup();
        }

        private void ClearValueControl()
        {
            objInventoryTerm = null;
            txtInventoryTermID.Text = string.Empty;
            DateTime dtmNow = DateTime.Now;
            txtInventoryTermName.Text = string.Empty;
            txtDescription.Text = string.Empty;
            dtpInventoryDate.Value = dtmNow;
            dtpBeginInventoryTime.Value = dtmNow;
            dtpLockDataTime.Value = dtmNow;
            cboInventoryType.SelectedIndex = 0;
            cboProductStateID.SelectedValue = -1;
            bolIsShowMessWaring = false;
            intIsLoadEdit = 0;
            btnCalInStock.Enabled = false;
        }

        private void FormatFlex()
        {
            //if (flexData.DataSource == null)
            //{
            //    return;
            //}
            //for (int i = 0; i < flexData.Cols.Count; i++)
            //{
            //    flexData.Cols[i].Visible = false;
            //    flexData.Cols[i].AllowEditing = false;
            //    flexData.Cols[i].AllowDragging = false;
            //}

            string[] fieldNames = new string[] { "INVENTORYTERMID","INVENTORYTERMNAME","DESCRIPTION","INVENTORYDATE","INVENTORYTYPENAME","BEGININVENTORYTIME","ENDINVENTORYTIME","LOCKDATATIME","PRODUCTSTATUSNAME","CREATEFULLNAME","CREATEDDATE","UPDATEFULLNAME","UPDATEDDATE","DELETEFULLNAME","DELETEDDATE" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(gridDataDetail, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(gridViewDataDetail);



            gridViewDataDetail.Columns["INVENTORYTERMID"    ].Caption = "Mã kỳ kiểm kê";
            gridViewDataDetail.Columns["INVENTORYTERMNAME"  ].Caption = "Tên kỳ kiểm kê";
            gridViewDataDetail.Columns["DESCRIPTION"        ].Caption = "Mô tả";
            gridViewDataDetail.Columns["INVENTORYDATE"      ].Caption = "Ngày kiểm kê";
            gridViewDataDetail.Columns["INVENTORYTYPENAME"  ].Caption = "Loại kiểm kê";
            gridViewDataDetail.Columns["BEGININVENTORYTIME" ].Caption = "Thời gian bắt đầu";
            gridViewDataDetail.Columns["ENDINVENTORYTIME"].Caption = "Thời gian kết thúc";
            gridViewDataDetail.Columns["LOCKDATATIME"       ].Caption = "Thời gian chốt tồn kho";
            gridViewDataDetail.Columns["PRODUCTSTATUSNAME"  ].Caption = "Loại hàng";
            gridViewDataDetail.Columns["CREATEFULLNAME"     ].Caption = "Nhân viên tạo";
            gridViewDataDetail.Columns["CREATEDDATE"        ].Caption = "Ngày tạo";
            gridViewDataDetail.Columns["UPDATEFULLNAME"     ].Caption = "Nhân viên cập nhật";
            gridViewDataDetail.Columns["UPDATEDDATE"        ].Caption = "Ngày cập nhật";
            gridViewDataDetail.Columns["DELETEFULLNAME"     ].Caption = "Nhân viên hủy";
            gridViewDataDetail.Columns["DELETEDDATE"        ].Caption = "Ngày hủy";


            gridViewDataDetail.OptionsView.ColumnAutoWidth = false;
            gridViewDataDetail.Columns["INVENTORYTERMID"    ].Width = 60;
            gridViewDataDetail.Columns["INVENTORYTERMNAME"  ].Width = 280;
            gridViewDataDetail.Columns["DESCRIPTION"        ].Width = 180;
            gridViewDataDetail.Columns["INVENTORYDATE"      ].Width = 90;
            gridViewDataDetail.Columns["INVENTORYTYPENAME"  ].Width = 200;
            gridViewDataDetail.Columns["BEGININVENTORYTIME" ].Width = 150;
            gridViewDataDetail.Columns["ENDINVENTORYTIME"].Width = 150;
            gridViewDataDetail.Columns["LOCKDATATIME"       ].Width = 150;
            gridViewDataDetail.Columns["PRODUCTSTATUSNAME"  ].Width = 120;
            gridViewDataDetail.Columns["CREATEFULLNAME"     ].Width = 150;
            gridViewDataDetail.Columns["CREATEDDATE"        ].Width = 120;
            gridViewDataDetail.Columns["UPDATEFULLNAME"     ].Width = 150;
            gridViewDataDetail.Columns["UPDATEDDATE"        ].Width = 120;
            gridViewDataDetail.Columns["DELETEFULLNAME"     ].Width = 150;
            gridViewDataDetail.Columns["DELETEDDATE"        ].Width = 120;


            gridViewDataDetail.Columns["INVENTORYDATE"].DisplayFormat.FormatString = "dd/MM/yyyy";
            gridViewDataDetail.Columns["BEGININVENTORYTIME"].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            gridViewDataDetail.Columns["ENDINVENTORYTIME"].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            gridViewDataDetail.Columns["LOCKDATATIME"].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            gridViewDataDetail.Columns["CREATEDDATE"].DisplayFormat.FormatString = "dd/MM/yyyy";
            gridViewDataDetail.Columns["UPDATEDDATE"].DisplayFormat.FormatString = "dd/MM/yyyy";
            gridViewDataDetail.Columns["DELETEDDATE"].DisplayFormat.FormatString = "dd/MM/yyyy";

            gridViewDataDetail.Columns["DELETEFULLNAME"].Visible = chkDeleted.Checked;
            gridViewDataDetail.Columns["DELETEDDATE"].Visible = chkDeleted.Checked;

            gridViewDataDetail.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            gridViewDataDetail.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            gridViewDataDetail.OptionsView.ShowAutoFilterRow = true;
            gridViewDataDetail.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            gridViewDataDetail.OptionsView.ShowFooter = false;
            gridViewDataDetail.OptionsView.ShowGroupPanel = false;
            gridViewDataDetail.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            gridViewDataDetail.ColumnPanelRowHeight = 40;


            //C1.Win.C1FlexGrid.CellStyle style = flexData.Styles.Add("flexStyle");
            //style.WordWrap = true;
            //style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Bold);
            //style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            //C1.Win.C1FlexGrid.CellRange range = flexData.GetCellRange(0, 0, 0, flexData.Cols.Count - 1);
            //range.Style = style;
            //flexData.Rows[0].Height = 40;
            //flexData.Rows[0].AllowDragging = false;
        }

        private void FormatFlexStore()
        {
            if (flexStore.DataSource == null)
            {
                return;
            }
            for (int i = 0; i < flexStore.Cols.Count; i++)
            {
                flexStore.Cols[i].Visible = false;
                flexStore.Cols[i].AllowEditing = false;
                flexStore.Cols[i].AllowDragging = false;
            }
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexStore, true, "IsSelect,StoreID,StoreName,FullName");
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnWidth(flexStore,
                "IsSelect", 18,
                "StoreID", 30,
                "StoreName", 170,
                "FullName", 185);
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnCaption(flexStore,
                "IsSelect", " ",
                "StoreID", "Mã kho",
                "StoreName", "Tên kho",
                "FullName", "Nhân viên kiểm kê");
            flexStore.Cols[0].Width = 10;
            flexStore.Cols["IsSelect"].DataType = typeof(bool);
            flexStore.Cols["IsUpdateStockData"].DataType = typeof(bool);
            flexStore.Cols["UpdateStockDataTime"].Format = "dd/MM/yyyy HH:mm";

            flexStore.Cols["IsSelect"].AllowEditing = true;
            flexStore.Cols["FullName"].AllowEditing = true;
            flexStore.Cols["FullName"].ComboList = "...";

            C1.Win.C1FlexGrid.CellStyle style = flexStore.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexStore.GetCellRange(0, 0, 0, flexStore.Cols.Count - 1);
            range.Style = style;
            flexStore.Rows[0].Height = 62;
            flexStore.Rows[0].AllowDragging = false;
            flexStore.AllowFiltering = false;
            flexStore.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            CheckIsHasPermission();
            GetCalInStockStatus();
            Library.AppCore.LoadControls.C1FlexGridObject.AddColumnHeaderCheckBox(flexStore, "IsSelect", "IsSelect", "ISHASPERMISSION", "0");
        }

        private void FormatFlexMainGroup()
        {
            if (flexMainGroup.DataSource == null)
            {
                return;
            }
            for (int i = 0; i < flexMainGroup.Cols.Count; i++)
            {
                flexMainGroup.Cols[i].Visible = false;
                flexMainGroup.Cols[i].AllowEditing = false;
                flexMainGroup.Cols[i].AllowDragging = false;
            }
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexMainGroup, true, "IsSelect,MainGroupName");
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnWidth(flexMainGroup,
                "IsSelect", 20,
                "MainGroupName", 250);
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnCaption(flexMainGroup,
                "IsSelect", "Chọn",
                "MainGroupName", "Ngành hàng");
            flexMainGroup.Cols[0].Width = 10;
            flexMainGroup.Cols["IsSelect"].DataType = typeof(bool);
            flexMainGroup.Cols["IsSelect"].AllowSorting = false;
            flexMainGroup.Cols["IsSelect"].AllowResizing = false;
            flexMainGroup.Cols["IsSelect"].AllowEditing = true;

            C1.Win.C1FlexGrid.CellStyle style = flexMainGroup.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexMainGroup.GetCellRange(0, 0, 0, flexMainGroup.Cols.Count - 1);
            range.Style = style;
            flexMainGroup.Rows[0].Height = 40;
            flexMainGroup.Rows[0].AllowDragging = false;

            flexMainGroup.SetCellCheck(0, flexMainGroup.Cols["IsSelect"].Index, C1.Win.C1FlexGrid.CheckEnum.Unchecked);
            flexMainGroup.SetData(0, flexMainGroup.Cols["IsSelect"].Index, " ");
            //for (int i = flexMainGroup.Rows.Fixed; i < flexMainGroup.Rows.Count; i++)
            //{
            //    flexMainGroup.SetCellCheck(i, flexMainGroup.Cols["IsSelect"].Index, C1.Win.C1FlexGrid.CheckEnum.Unchecked);
            //}

        }

        private void FormatFlexBrand()
        {
            if (flexBrand.DataSource == null)
            {
                return;
            }
            for (int i = 0; i < flexBrand.Cols.Count; i++)
            {
                flexBrand.Cols[i].Visible = false;
                flexBrand.Cols[i].AllowEditing = false;
                flexBrand.Cols[i].AllowDragging = false;
            }
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexBrand, true, "IsSelect,BrandName");
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnWidth(flexBrand,
                "IsSelect", 18,
                "BrandName", 350);
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnCaption(flexBrand,
                "IsSelect", "Chọn",
                "BrandName", "Nhà sản xuất");
            flexBrand.Cols[0].Width = 10;
            flexBrand.Cols["IsSelect"].DataType = typeof(bool);
            flexBrand.Cols["IsSelect"].AllowEditing = true;

            C1.Win.C1FlexGrid.CellStyle style = flexBrand.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexBrand.GetCellRange(0, 0, 0, flexBrand.Cols.Count - 1);
            range.Style = style;
            flexBrand.Rows[0].Height = 40;
            flexBrand.Rows[0].AllowDragging = false;

            Library.AppCore.LoadControls.C1FlexGridObject.AddColumnHeaderCheckBox(flexBrand, "IsSelect");
        }

        private void FormatFlexSubGroup()
        {
            if (flexSubGroup.DataSource == null)
            {
                return;
            }
            for (int i = 0; i < flexSubGroup.Cols.Count; i++)
            {
                flexSubGroup.Cols[i].Visible = false;
                flexSubGroup.Cols[i].AllowEditing = false;
                flexSubGroup.Cols[i].AllowDragging = false;
            }
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexSubGroup, true, "IsSelect,SubGroupName");
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnWidth(flexSubGroup,
                "IsSelect", 18,
                "SubGroupName", 250);
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnCaption(flexSubGroup,
                "IsSelect", "Chọn",
                "SubGroupName", "Nhóm hàng");
            flexSubGroup.Cols[0].Width = 10;
            flexSubGroup.Cols["IsSelect"].DataType = typeof(bool);
            flexSubGroup.Cols["IsSelect"].AllowEditing = true;

            C1.Win.C1FlexGrid.CellStyle style = flexSubGroup.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexSubGroup.GetCellRange(0, 0, 0, flexSubGroup.Cols.Count - 1);
            range.Style = style;
            flexSubGroup.Rows[0].Height = 40;
            flexSubGroup.Rows[0].AllowDragging = false;

            Library.AppCore.LoadControls.C1FlexGridObject.AddColumnHeaderCheckBox(flexSubGroup, "IsSelect");
        }

        private bool CheckSearch()
        {
            if (Globals.DateDiff(dtpToDate.Value, dtpFromDate.Value, Globals.DateDiffType.DATE) < 0)
            {
                MessageBox.Show("Từ ngày không được lớn hơn đến ngày yêu cầu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtpToDate.Focus();
                return false;
            }
            if (Globals.DateDiff(dtpToDate.Value, dtpFromDate.Value, Globals.DateDiffType.DATE) > 92)
            {
                MessageBox.Show("Vui lòng tìm kiếm trong thời gian 3 tháng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtpToDate.Focus();
                return false;
            }

            return true;
        }

        private void SearchData()
        {
            string strUserName = SystemConfig.objSessionUser.UserName;
            object[] objKeywords = new object[]
            {
                "@KEYWORDS", Convert.ToString(txtKeywords.Text).Trim(),
                "@FROMDATE", dtpFromDate.Value,
                "@TODATE", dtpToDate.Value,
                "@INVENTORYTYPEID", Convert.ToInt32(cboInvTypeSearch.SelectedValue),
                "@STOREIDLIST", cboStoreIDList.StoreIDList,
                "@PRODUCTSTATUSID", cboProductStateIDSearch.ColumnID,
                //"@ISDELETED", checkDelelte, //Bỏ không tìm kiếm đã hủy (Ngày 25/06/2016)
                "@USERNAME", strUserName
            };
            dtbSearch = objPLCInventoryTerm.SearchData(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            gridDataDetail.DataSource = dtbSearch;
           

            if (gridViewDataDetail.RowCount <= 0)
            {
                btnExportExcel.Enabled = mnuAction_ExportExcel.Enabled = false;
                btnEdit.Enabled = mnuAction_Edit.Enabled = false;
                btnDelete.Enabled = mnuAction_Delete.Enabled = false;
            }
            else
            {
                btnExportExcel.Enabled = mnuAction_ExportExcel.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_ExportExcel);
                btnEdit.Enabled = mnuAction_Edit.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Edit);
                btnDelete.Enabled = mnuAction_Delete.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Delete);
            }
        }

        private bool CheckUpdate()
        {
            if (Convert.ToString(txtInventoryTermName.Text).Trim() == string.Empty)
            {
                MessageBox.Show(this, "Vui lòng nhập tên kỳ kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tabControl1.SelectedTab = tabPage1;
                txtInventoryTermName.Focus();
                return false;
            }
            if (cboInventoryType.SelectedIndex < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn loại kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tabControl1.SelectedTab = tabPage1;
                cboInventoryType.Focus();
                return false;
            }

            if (dtpBeginInventoryTime.Value > dtpEndInventoryTime.Value)
            {
                MessageBox.Show(this, "Thời gian kết thúc kiểm không được nhỏ hơn thời gian bắt đầu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tabControl1.SelectedTab = tabPage1;
                dtpEndInventoryTime.Focus();
                return false;
            }

            if (dtpLockDataTime.Value > dtpBeginInventoryTime.Value)
            {
                MessageBox.Show(this, "Thời gian chốt tồn kho không được lớn hơn thời gian bắt đầu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tabControl1.SelectedTab = tabPage1;
                dtpLockDataTime.Focus();
                return false;
            }
            if (dtpLockDataTime.Value.Date > dtpInventoryDate.Value.Date)
            {
                MessageBox.Show(this, "Ngày kiểm kê không được nhỏ hơn thời gian chốt tồn kho. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tabControl1.SelectedTab = tabPage1;
                dtpInventoryDate.Focus();
                return false;
            }
            bool bolSelectStore = false;
            for (int i = flexStore.Rows.Fixed; i < flexStore.Rows.Count; i++)
            {
                if (Convert.ToBoolean(flexStore[i, "IsSelect"]))
                {
                    if (Convert.ToString(flexStore[i, "FullName"]).Trim() == string.Empty)
                    {
                        MessageBox.Show(this, "Vui lòng chọn nhân viên kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        tabControl1.SelectedTab = tabCustomer;
                        flexStore.Select(i, flexStore.Cols["FullName"].Index);
                        return false;
                    }
                    bolSelectStore = true;
                }
            }
            if (!bolSelectStore)
            {
                MessageBox.Show(this, "Vui lòng chọn kho kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tabControl1.SelectedTab = tabCustomer;
                return false;
            }
            bool bolSelectMainGroup = false;
            for (int i = flexMainGroup.Rows.Fixed; i < flexMainGroup.Rows.Count; i++)
            {
                if (Convert.ToBoolean(flexMainGroup[i, "IsSelect"]))
                {
                    bolSelectMainGroup = true;
                    break;
                }
            }
            if (!bolSelectMainGroup)
            {
                MessageBox.Show(this, "Vui lòng chọn ngành hàng kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tabControl1.SelectedTab = tabCustomer;
                return false;
            }
            bool bolSelectBrand = false;
            for (int i = flexBrand.Rows.Fixed; i < flexBrand.Rows.Count; i++)
            {
                if (Convert.ToBoolean(flexBrand[i, "IsSelect"]))
                {
                    bolSelectBrand = true;
                    break;
                }
            }
            if (!bolSelectBrand)
            {
                MessageBox.Show(this, "Vui lòng chọn nhà sản xuất kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tabControl1.SelectedTab = tabCustomer;
                return false;
            }
            return true;
        }

        private bool UpdateData()
        {
            string strMessage = string.Empty;
            try
            {
                objInventoryTerm = new PLC.Inventory.WSInventoryTerm.InventoryTerm();
                if (Convert.ToString(txtInventoryTermID.Text).Trim() != string.Empty)
                {
                    objInventoryTerm.InventoryTermID = Convert.ToInt32(txtInventoryTermID.Text);
                }
                objInventoryTerm.CreatedStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                objInventoryTerm.InventoryTermName = txtInventoryTermName.Text;
                objInventoryTerm.Description = txtDescription.Text;
                objInventoryTerm.InventoryDate = dtpInventoryDate.Value;
                objInventoryTerm.BeginInventoryTime = dtpBeginInventoryTime.Value;
                objInventoryTerm.EndInventoryTime = dtpEndInventoryTime.Value;
                objInventoryTerm.LockDataTime = dtpLockDataTime.Value;
                objInventoryTerm.IsCallStockBySchedule = radiSchedule.Checked;
                objInventoryTerm.Inv_AllowBarcode = chkAllowBarcode.Checked;
                objInventoryTerm.InventoryTypeID = Convert.ToInt32(cboInventoryType.SelectedValue);
                objInventoryTerm.ProductStatusID = Convert.ToInt32(cboProductStateID.SelectedValue);// cboIsNew.SelectedIndex - 1;
                DataTable dtbFlexStore = (flexStore.DataSource as DataTable).Copy();
                dtbFlexStore.DefaultView.RowFilter = "IsSelect > 0";
                objInventoryTerm.dtbTerm_Store = dtbFlexStore.DefaultView.ToTable();

                DataTable dtbFlexMainGroup = (flexMainGroup.DataSource as DataTable).Copy();
                dtbFlexMainGroup.DefaultView.RowFilter = "IsSelect > 0";
                objInventoryTerm.dtbTerm_MainGroup = dtbFlexMainGroup.DefaultView.ToTable();

                DataTable dtbFlexBrand = (flexBrand.DataSource as DataTable).Copy();
                dtbFlexBrand.DefaultView.RowFilter = "IsSelect > 0";
                objInventoryTerm.dtbTerm_Brand = dtbFlexBrand.DefaultView.ToTable();

                DataTable dtbFlexSubGroup = (flexSubGroup.DataSource as DataTable).Copy();
                dtbFlexSubGroup.DefaultView.RowFilter = "IsSelect > 0";
                objInventoryTerm.dtbTerm_SubGroup = dtbFlexSubGroup.DefaultView.ToTable();

                if (FormState == FormStateType.ADD)
                {
                    strMessage = "Thêm mới";
                    objPLCInventoryTerm.Insert(objInventoryTerm);
                }
                else if (FormState == FormStateType.EDIT)
                {
                    strMessage = "Cập nhật";
                    objPLCInventoryTerm.Update(objInventoryTerm);
                }
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                bolIsShowMessWaring = false;
                MessageBox.Show(this, strMessage + " kỳ kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception objEx)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Lỗi " + strMessage.ToLower() + " kỳ kiểm kê");
                SystemErrorWS.Insert("Lỗi " + strMessage.ToLower() + " kỳ kiểm kê", objEx.ToString(), this.Name + " -> UpdateData", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private bool DeleteData()
        {
            if (gridViewDataDetail.RowCount <= 0)
                return false;
            DataRow drFocus = gridViewDataDetail.GetFocusedDataRow();
            if (drFocus == null)
                return false;
            try
            {
                if (MessageBox.Show("Bạn có chắc muốn xóa kỳ kiểm kê [" + Convert.ToString((drFocus["InventoryTermName"]??"").ToString()) + "] không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    return false;
                }
                objPLCInventoryTerm.Delete(Convert.ToInt32((drFocus["InventoryTermID"] ?? "").ToString()));
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                bolIsShowMessWaring = false;
                btnCalInStock.Enabled = false;
                MessageBox.Show(this, "Xóa kỳ kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception objEx)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Lỗi xóa kỳ kiểm kê");
                SystemErrorWS.Insert("Lỗi xóa kỳ kiểm kê", objEx.ToString(), this.Name + " -> DeleteData", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        #endregion

        #region event

        private void frmInventoryTerm_Load(object sender, EventArgs e)
        {
            LoadComboBox();
            FormatFlex();
            btnSearch_Click(null, null);

            //if (SystemConfig.objSessionUser.UserName != "administrator")
            //    chkDeleted.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission__DeleteSearch);
            FormState = FormStateType.LIST;
        }

        private void txtKeywords_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
                btnSearch_Click(sender, null);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!btnSearch.Enabled)
                return;
            if (!CheckSearch())
                return;
            SearchData();
        }

        private void flexData_DoubleClick(object sender, EventArgs e)
        {
            if (gridViewDataDetail.RowCount <= 0)
                return ;
            DataRow drFocus = gridViewDataDetail.GetFocusedDataRow();
            if (drFocus == null)
                return ;
            int intInventoryTermID = Convert.ToInt32((drFocus["InventoryTermID"] ?? "").ToString());
            EditData(intInventoryTermID);
            FormState = FormStateType.EDIT;
        }

        private void flexStore_CellButtonClick(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            ERP.MasterData.DUI.Search.frmUserSearch frmUserSearch1 = new MasterData.DUI.Search.frmUserSearch();
            frmUserSearch1.IsRealStaff = 1;
            frmUserSearch1.ShowDialog();
            if (frmUserSearch1.User != null)
            {
                flexStore[flexStore.Row, "InventoryUser"] = frmUserSearch1.User.UserName;
                flexStore[flexStore.Row, "FullName"] = frmUserSearch1.User.UserName + (frmUserSearch1.User.FullName != string.Empty ? ("-" + frmUserSearch1.User.FullName) : string.Empty);
                if (!Convert.ToBoolean(flexStore[flexStore.Row, "IsSelect"]) && Convert.ToBoolean(flexStore[flexStore.Row, "IsHasPermission"]))
                {
                    flexStore[flexStore.Row, "IsSelect"] = true;
                }

            }
        }

        private void flexStore_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Delete && flexStore.Col == flexStore.Cols["FullName"].Index)
            {
                flexStore[flexStore.Row, "InventoryUser"] = string.Empty;
                flexStore[flexStore.Row, "FullName"] = string.Empty;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!btnAdd.Enabled)
                return;
            ClearValueControl();
            InitFlex();
           // lbUpdateStockDataTime.Text = ""; // neu la them moi thi se ko hien thi thoi gian tinh ton kho
            FormState = FormStateType.ADD;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (!btnEdit.Enabled)
                return;
            flexData_DoubleClick(null, null);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (!btnDelete.Enabled)
                return;

            btnDelete.Enabled = false;
            txtInventoryTermID.Text = string.Empty;
            if (DeleteData())
            {
                SearchData();
            }
            btnDelete.Enabled = true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!btnUpdate.Enabled)
                return;
            if (!CheckUpdate())
                return;
            btnUpdate.Enabled = false;
            if (UpdateData())
            {
                SearchData();
            }
            btnUpdate.Enabled = true;
            FormState = FormStateType.LIST;
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            if (!btnUndo.Enabled)
                return;
            objInventoryTerm = null;
            FormState = FormStateType.LIST;
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(gridDataDetail);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void mnuAction_Add_Click(object sender, EventArgs e)
        {
            btnAdd_Click(null, null);
        }

        private void mnuAction_Edit_Click(object sender, EventArgs e)
        {
            btnEdit_Click(null, null);
        }

        private void mnuAction_Delete_Click(object sender, EventArgs e)
        {
            btnDelete_Click(null, null);
        }

        private void mnuAction_ExportExcel_Click(object sender, EventArgs e)
        {
            btnExportExcel_Click(null, null);
        }

        private void tabPageDetail_Enter(object sender, EventArgs e)
        {
            if (FormState == FormStateType.LIST)
            {
                tabControl.SelectedTab = tabPageList;
            }
        }

        private void tabPageList_Enter(object sender, EventArgs e)
        {
            if (FormState == FormStateType.ADD || FormState == FormStateType.EDIT)
            {
                tabControl.SelectedTab = tabPageDetail;
            }
        }

        #endregion

        #region Form Status
        /// <summary>
        /// Các trạng thái của Form
        /// </summary>
        enum FormStateType
        {
            LIST,
            ADD,
            EDIT
        }

        FormStateType intFormState = FormStateType.LIST;
        FormStateType FormState
        {
            set
            {
                intFormState = value;
                if (intFormState == FormStateType.LIST)
                {
                    tabControl.SelectedTab = tabPageList;
                    btnAdd.Enabled = mnuAction_Add.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Add);
                    btnUpdate.Enabled = false;
                    btnUndo.Enabled = false;
                    if (dtbSearch != null && dtbSearch.Rows.Count > 0)
                    {
                        btnEdit.Enabled = mnuAction_Edit.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Edit);
                        btnDelete.Enabled = mnuAction_Delete.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Delete);
                        btnExportExcel.Enabled = mnuAction_ExportExcel.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_ExportExcel);
                    }
                    else
                    {
                        btnEdit.Enabled = mnuAction_Edit.Enabled = false;
                        btnDelete.Enabled = mnuAction_Delete.Enabled = false;
                        btnExportExcel.Enabled = mnuAction_ExportExcel.Enabled = false;
                    }
                }
                else if (intFormState == FormStateType.ADD || intFormState == FormStateType.EDIT)
                {
                    tabControl.SelectedTab = tabPageDetail;
                    btnAdd.Enabled = mnuAction_Add.Enabled = false;
                    btnEdit.Enabled = mnuAction_Edit.Enabled = false;
                    btnDelete.Enabled = mnuAction_Delete.Enabled = false;
                    btnUpdate.Enabled = true;
                    btnUndo.Enabled = true;
                }
            }
            get
            {
                return intFormState;
            }
        }

        #endregion

        private void flexStore_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexStore.Rows.Count <= flexStore.Rows.Fixed)
            {
                return;
            }
            if (flexStore.RowSel < flexStore.Rows.Fixed)
            {
                return;
            }

            if (e.Col == flexStore.Cols["IsSelect"].Index)
            {
                if (objInventoryTerm != null && objInventoryTerm.dtbTerm_Store != null)
                {
                    objInventoryTerm.dtbTerm_Store.AcceptChanges();
                    bolIsShowMessWaring = true;
                    DataRow[] rSelect = objInventoryTerm.dtbTerm_Store.Select("IsSelect = 1");
                    btnCalInStock.Enabled = Convert.ToBoolean(rSelect.Length) && SystemConfig.objSessionUser.IsPermission(strPermission_CalcInStock); 
                }
            }
            if (e.Col == flexStore.Cols["IsSelect"].Index)
            {
                if (e.Row >= flexStore.Rows.Fixed)
                {
                    for (int k = flexStore.Rows.Fixed; k < flexStore.Rows.Count; k++)
                    {
                        if (Convert.ToInt32(flexStore[k, "IsSelect"]) == 0)
                        {
                            flexStore.SetCellCheck(0, flexStore.Cols["IsSelect"].Index, C1.Win.C1FlexGrid.CheckEnum.Unchecked);
                            flexStore.SetData(0, flexStore.Cols["IsSelect"].Index, " ");
                            break;
                        }
                    }
                }
            }
        }

        private void flexMainGroup_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexMainGroup.Rows.Count <= flexMainGroup.Rows.Fixed)
            {
                return;
            }
            if (flexMainGroup.RowSel < flexMainGroup.Rows.Fixed)
            {
                return;
            }
            if (e.Col == flexMainGroup.Cols["IsSelect"].Index)
            {
                if (e.Row >= flexMainGroup.Rows.Fixed)
                {
                    for (int k = flexMainGroup.Rows.Fixed; k < flexMainGroup.Rows.Count; k++)
                    {
                        if (Convert.ToInt32(flexMainGroup[k, "IsSelect"]) == 0)
                        {
                            flexMainGroup.SetCellCheck(0, flexMainGroup.Cols["IsSelect"].Index, C1.Win.C1FlexGrid.CheckEnum.Unchecked);
                            flexMainGroup.SetData(0, flexMainGroup.Cols["IsSelect"].Index, " ");
                            break;
                        }
                    }
                    bolIsShowMessWaring = true;

                    int intMainGroupID = Convert.ToInt32(flexMainGroup[flexMainGroup.RowSel, "MainGroupID"]);
                    ComboBox cbTMP = new ComboBox();
                    Library.AppCore.LoadControls.SetDataSource.SetSubGroup(this, cbTMP, intMainGroupID);
                    DataTable dtbSubGroup = (DataTable)cbTMP.DataSource;
                    if (dtbSubGroup.Rows.Count > 0 && Convert.ToInt32(dtbSubGroup.Rows[0]["SubGroupID"]) < 1)
                        dtbSubGroup.Rows.RemoveAt(0);
                    DataTable dtbFlexSubGroup = (DataTable)flexSubGroup.DataSource;
                    if (Convert.ToBoolean(flexMainGroup[flexMainGroup.RowSel, "IsSelect"]))
                    {
                        for (int i = 0; i < dtbSubGroup.Rows.Count; i++)
                        {
                            DataRow rExist = dtbFlexSubGroup.Rows.Find(Convert.ToInt32(dtbSubGroup.Rows[i]["SubGroupID"]));
                            if (rExist == null)
                            {
                                DataRow rSubGroup = dtbFlexSubGroup.NewRow();
                                rSubGroup["IsSelect"] = false;
                                rSubGroup["InventoryTermID"] = 0;
                                rSubGroup["MainGroupID"] = intMainGroupID;
                                rSubGroup["SubGroupID"] = Convert.ToInt32(dtbSubGroup.Rows[i]["SubGroupID"]);
                                rSubGroup["SubGroupName"] = dtbSubGroup.Rows[i]["SubGroupName"].ToString().Trim();
                                if (objInventoryTerm != null && objInventoryTerm.InventoryTermID > 0)
                                {
                                    rSubGroup["InventoryDate"] = objInventoryTerm.InventoryDate;
                                    rSubGroup["InventoryTermID"] = objInventoryTerm.InventoryTermID;
                                }
                                else
                                    rSubGroup["InventoryDate"] = dtpInventoryDate.Value;
                                dtbFlexSubGroup.Rows.Add(rSubGroup);
                            }
                        }
                    }
                    else
                    {
                        DataRow[] drMainGroup = dtbFlexSubGroup.Select("MainGroupID = " + intMainGroupID);
                        if (drMainGroup.Length > 0)
                        {
                            for (int i = 0; i < drMainGroup.Length; i++)
                            {
                                dtbFlexSubGroup.Rows.Remove(drMainGroup[i]);
                            }
                        }
                    }
                }
                else
                {
                    if (flexMainGroup.Rows.Count > 0)
                    {
                        if (flexMainGroup[e.Row, "IsSelect"].ToString() == " ")
                        {
                            flexMainGroup.SetData(0, flexMainGroup.Cols["IsSelect"].Index, "  ");
                            string strMainGroupIDList = string.Empty;
                            for (int i = flexMainGroup.Rows.Fixed; i < flexMainGroup.Rows.Count; i++)
                            {
                                strMainGroupIDList += flexMainGroup[i, "MainGroupID"].ToString() + ",";
                            }
                            strMainGroupIDList = strMainGroupIDList.Trim(',');
                            dtbSubGroupAll.DefaultView.RowFilter = "MainGroupID IN(" + strMainGroupIDList + ")";
                            DataTable dtbSubGroupFilter = dtbSubGroupAll.DefaultView.ToTable();

                            DataTable dtbFlexSubGroup = (DataTable)flexSubGroup.DataSource;
                            dtbFlexSubGroup.Rows.Clear();
                            for (int i = 0; i < dtbSubGroupFilter.Rows.Count; i++)
                            {
                                DataRow rSubGroupFilter = dtbSubGroupFilter.Rows[i];
                                DataRow rSubGroup = dtbFlexSubGroup.NewRow();
                                rSubGroup["IsSelect"] = false;
                                rSubGroup["InventoryTermID"] = 0;
                                rSubGroup["MainGroupID"] = Convert.ToInt32(rSubGroupFilter["MainGroupID"]);
                                rSubGroup["SubGroupID"] = Convert.ToInt32(rSubGroupFilter["SubGroupID"]);
                                rSubGroup["SubGroupName"] = rSubGroupFilter["SubGroupName"].ToString().Trim();
                                if (objInventoryTerm != null && objInventoryTerm.InventoryTermID > 0)
                                {
                                    rSubGroup["InventoryDate"] = objInventoryTerm.InventoryDate;
                                    rSubGroup["InventoryTermID"] = objInventoryTerm.InventoryTermID;
                                }
                                else
                                    rSubGroup["InventoryDate"] = dtpInventoryDate.Value;
                                dtbFlexSubGroup.Rows.Add(rSubGroup);
                            }

                            for (int i = flexMainGroup.Rows.Fixed; i < flexMainGroup.Rows.Count; i++)
                            {
                                flexMainGroup.SetCellCheck(i, flexMainGroup.Cols["IsSelect"].Index, C1.Win.C1FlexGrid.CheckEnum.Checked);
                            }
                        }
                        else
                        {
                            flexMainGroup.SetData(0, flexMainGroup.Cols["IsSelect"].Index, " ");
                            DataTable dtbFlexSubGroup = (DataTable)flexSubGroup.DataSource;
                            dtbFlexSubGroup.Rows.Clear();

                            for (int i = flexMainGroup.Rows.Fixed; i < flexMainGroup.Rows.Count; i++)
                            {
                                flexMainGroup.SetCellCheck(i, flexMainGroup.Cols["IsSelect"].Index, C1.Win.C1FlexGrid.CheckEnum.Unchecked);
                            }
                        }
                    }
                }
            }
        }

        private void flexBrand_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexBrand.Rows.Count <= flexBrand.Rows.Fixed)
            {
                return;
            }
            if (flexBrand.RowSel < flexBrand.Rows.Fixed)
            {
                return;
            }
            if (e.Col == flexBrand.Cols["IsSelect"].Index)
            {
                bolIsShowMessWaring = true;
            }
            if (e.Col == flexBrand.Cols["IsSelect"].Index)
            {
                if (e.Row >= flexBrand.Rows.Fixed)
                {
                    for (int k = flexBrand.Rows.Fixed; k < flexBrand.Rows.Count; k++)
                    {
                        if (Convert.ToInt32(flexBrand[k, "IsSelect"]) == 0)
                        {
                            flexBrand.SetCellCheck(0, flexBrand.Cols["IsSelect"].Index, C1.Win.C1FlexGrid.CheckEnum.Unchecked);
                            flexBrand.SetData(0, flexBrand.Cols["IsSelect"].Index, " ");
                            break;
                        }
                    }
                }
            }

        }

        private void flexSubGroup_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexSubGroup.Rows.Count <= flexSubGroup.Rows.Fixed)
            {
                return;
            }
            if (flexSubGroup.RowSel < flexSubGroup.Rows.Fixed)
            {
                return;
            }
            if (e.Col == flexSubGroup.Cols["IsSelect"].Index)
            {
                bolIsShowMessWaring = true;
            }
            if (e.Col == flexSubGroup.Cols["IsSelect"].Index)
            {
                if (e.Row >= flexSubGroup.Rows.Fixed)
                {
                    for (int k = flexSubGroup.Rows.Fixed; k < flexSubGroup.Rows.Count; k++)
                    {
                        if (Convert.ToInt32(flexSubGroup[k, "IsSelect"]) == 0)
                        {
                            flexSubGroup.SetCellCheck(0, flexSubGroup.Cols["IsSelect"].Index, C1.Win.C1FlexGrid.CheckEnum.Unchecked);
                            flexSubGroup.SetData(0, flexSubGroup.Cols["IsSelect"].Index, " ");
                            break;
                        }
                    }
                }
            }

        }

        private void cboIsNew_SelectionChangeCommitted(object sender, EventArgs e)
        {
            bolIsShowMessWaring = true;
        }

        private void dtpLockDataTime_ValueChanged(object sender, EventArgs e)
        {
            if (intIsLoadEdit == 0)
            {
                bolIsShowMessWaring = true;
            }
        }

        private void btnCalInStock_Click(object sender, EventArgs e)
        {
            if (!btnUpdate.Enabled)
                return;
            if (!CheckUpdate())
                return;
            if (objInventoryTerm == null || (objInventoryTerm.dtbTerm_Store == null || objInventoryTerm.dtbTerm_Store.Rows.Count < 1))
            {
                MessageBox.Show(this, "Vui lòng cập nhật kỳ kiểm kê trước khi tính tồn kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (bolIsShowMessWaring)
            {
                if (MessageBox.Show(this, "Khi có thay đổi, vui lòng cập nhật kỳ kiểm kê trước khi tính tồn kho. Bạn có chắc tính tồn kho?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    return;
                }
            }
            try
            {
                DataTable dtbCalStoreInStock = objInventoryTerm.dtbTerm_Store.Clone();
                for (int i = 0; i < objInventoryTerm.dtbTerm_Store.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(objInventoryTerm.dtbTerm_Store.Rows[i]["IsSelect"]))
                    {
                        DataRow r = dtbCalStoreInStock.NewRow();
                        r.ItemArray = objInventoryTerm.dtbTerm_Store.Rows[i].ItemArray;
                        dtbCalStoreInStock.Rows.Add(r);
                    }
                }
                objPLCInventoryTerm.CalStockData(objInventoryTerm.InventoryTermID, dtbCalStoreInStock);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                MessageBox.Show(this, "Tính tồn kho kỳ kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FormState = FormStateType.LIST;
            }
            catch (Exception objEx)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Lỗi tính tồn kho kỳ kiểm kê");
                SystemErrorWS.Insert("Lỗi tính tồn kho kỳ kiểm kê", objEx.ToString(), this.Name + " -> mnuItemInvStoreCal_Click", DUIInventory_Globals.ModuleName);
                return;
            }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void mnuItemExportExcelTemp_Click(object sender, EventArgs e)
        {
            Library.AppCore.CustomControls.C1FlexGrid.CustomizeC1FlexGrid.ExportExcel(flexStore);
        }

        private void mnuItemImportExcel_Click(object sender, EventArgs e)
        {
            DataTable dtbUser = Library.AppCore.DataSource.GetDataSource.GetUser().Copy();
            DataTable dtbStoreImport = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable();
            //DataTable dtbGrid = grdData.DataSource as DataTable;
            if (!ValidTemplatesExcel(dtbStoreImport))
                return;

            var varReulst = dtbStoreImport.AsEnumerable()
                .Where(row => !string.IsNullOrEmpty((row.Field<object>("MA_KHO") ?? "").ToString().Trim())
                           && !string.IsNullOrEmpty((row.Field<object>("MA_NV") ?? "").ToString().Trim())
                                );

            if(varReulst.Any())
            {
                Dictionary<int, List<string>> dic = new Dictionary<int, List<string>>();
                foreach (DataRow dr in varReulst)
                {
                    int storeID = -1;
                    if (!int.TryParse((dr["MA_KHO"] ?? "").ToString().Trim(), out storeID))
                        continue;
                    string strUser = (dr["MA_NV"] ?? "").ToString().Trim();
                    var varUserResult = dtbUser.AsEnumerable().Where(row => (row.Field<object>("USERNAME") ?? "").ToString().Trim() == strUser);
                    if (!varUserResult.Any())
                        continue;
                    List<string> lstUser = new List<string>();
                    lstUser.Add((varUserResult.FirstOrDefault()["USERNAME"] ?? "").ToString().Trim());
                    lstUser.Add((varUserResult.FirstOrDefault()["FULLNAME"] ?? "").ToString().Trim());

                    if (dic.ContainsKey(storeID))
                        dic[storeID] = lstUser;
                    else
                        dic.Add(storeID, lstUser);
                }
                DataTable dtbFlexStore = (flexStore.DataSource as DataTable).Copy();
                foreach (DataRow dr in dtbFlexStore.Rows)
                {
                    int storeID = -1;
                    if (!int.TryParse((dr["StoreID"] ?? "").ToString().Trim(), out storeID))
                        continue;
                    if (!dic.ContainsKey(storeID))
                        continue;
                    dr["InventoryUser"] = dic[storeID][0];
                    dr["FullName"] = dic[storeID][0]+"-" +dic[storeID][1];
                    dr["IsSelect"] = 1;
                }
                flexStore.DataSource = dtbFlexStore;
                FormatFlexStore();
            }
            else
            {
                MessageBox.Show(this, "File excel không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }

        private bool ValidTemplatesExcel(DataTable dtbImeiImport)
        {
            if (dtbImeiImport == null)
            {
                MessageBox.Show(this, "File excel không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (dtbImeiImport.Columns.Count == 0)
            {
                MessageBox.Show(this, "File excel không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            dtbImeiImport.Rows.RemoveAt(0);
            if (dtbImeiImport.Columns.Count == 0)
            {
                MessageBox.Show(this, "File excel không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            dtbImeiImport.Columns[0].ColumnName = "ISSELECT";
            dtbImeiImport.Columns[1].ColumnName = "MA_KHO";
            dtbImeiImport.Columns[2].ColumnName = "TEN_KHO";
            dtbImeiImport.Columns[3].ColumnName = "MA_NV";
            return true;
        }
    }
}
