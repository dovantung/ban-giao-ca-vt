﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmInventoryJoin : Form
    {
        public frmInventoryJoin()
        {
            InitializeComponent();
        }

        #region variable
        private ERP.Inventory.PLC.Inventory.PLCInventoryTerm objPLCInventoryTerm = new PLC.Inventory.PLCInventoryTerm();
        private ERP.Inventory.PLC.Inventory.PLCInventory objPLCInventory = new PLC.Inventory.PLCInventory();
        private ERP.Inventory.PLC.Inventory.WSInventoryTerm.InventoryTerm objInventoryTerm = null;
        private int intSelectedIndex_InventoryType = 0;
        private int intSelectedIndex_InventoryTerm = 0;
        private int intSelectedIndex_Store = 0;
        private int intSelectedIndex_MainGroup = 0;
        private DataTable dtbINVTerm_Brand = null;
        #endregion

        #region method
        private void LoadComboBox()
        {
            Library.AppCore.LoadControls.SetDataSource.SetInventoryType(this, cboInventoryType);
            
            cboInventoryTerm.Text = "-- Chọn kỳ kiểm kê --";
            cboStore.Text = "-- Chọn chi nhánh --";
            cboMainGroup.Text = "-- Chọn ngành hàng --";
            cboSubGroup.Text = "-- Chọn nhóm hàng --";

            DataTable dtbProductStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
            cboProductStateID.DataSource = dtbProductStatus;
            cboProductStateID.DisplayMember = "PRODUCTSTATUSNAME";
            cboProductStateID.ValueMember = "PRODUCTSTATUSID";
            cboProductStateID.SelectedValue = 1;
        }

        private void Reset()
        {
            intSelectedIndex_InventoryType = 0;
            cboInventoryType.SelectedIndex = 0;

            cboInventoryTerm.DataSource = null;
            cboInventoryTerm.Items.Clear();
            cboInventoryTerm.Items.Add("-- Chọn kỳ kiểm kê --");
            cboInventoryTerm.Text = "-- Chọn kỳ kiểm kê --";
            cboInventoryTerm.Enabled = false;

            ClearInfoTerm();
        }

        private void ClearInfoTerm()
        {
            if (cboMainGroup.DataSource != null && (cboMainGroup.DataSource as DataTable).Rows.Count > 0)
            {
                DataTable dtbTerm_MainGroup = (cboMainGroup.DataSource as DataTable).Clone();
                DataRow rNewMainGroup = dtbTerm_MainGroup.NewRow();
                rNewMainGroup.ItemArray = (cboMainGroup.DataSource as DataTable).Rows[0].ItemArray;
                dtbTerm_MainGroup.Rows.Add(rNewMainGroup);
                cboMainGroup.DataSource = dtbTerm_MainGroup;
                cboMainGroup.ValueMember = dtbTerm_MainGroup.Columns[0].ColumnName;
                cboMainGroup.DisplayMember = dtbTerm_MainGroup.Columns[1].ColumnName;
                cboMainGroup.Enabled = false;
                if (cboMainGroup.Items.Count > 0)
                    cboMainGroup.SelectedIndex = 0;
            }

            if (cboStore.DataSource != null && (cboStore.DataSource as DataTable).Rows.Count > 0)
            {
                DataTable dtbTerm_Store = (cboStore.DataSource as DataTable).Clone();
                DataRow rNewStore = dtbTerm_Store.NewRow();
                rNewStore.ItemArray = (cboStore.DataSource as DataTable).Rows[0].ItemArray;
                dtbTerm_Store.Rows.Add(rNewStore);
                cboStore.DataSource = dtbTerm_Store;
                cboStore.ValueMember = dtbTerm_Store.Columns[0].ColumnName;
                cboStore.DisplayMember = dtbTerm_Store.Columns[1].ColumnName;
                cboStore.Enabled = false;
                if (cboStore.Items.Count > 0)
                    cboStore.SelectedIndex = 0;
            }
            cboSubGroup.Visible = false;
            cboSubGroup.Enabled = false;
            if (cboSubGroup.Items.Count > 0)
                cboSubGroup.SelectedIndex = 0;
            txtSubGroup.Visible = true;
        }

        private bool ValidateInput()
        {
            if (cboInventoryType.SelectedIndex < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn loại kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboInventoryType.Focus();
                return false;
            }
            if (cboInventoryTerm.SelectedIndex < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn kỳ kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboInventoryTerm.Focus();
                return false;
            }
            if (cboStore.SelectedIndex < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn chi nhánh", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboStore.Focus();
                return false;
            }
            if (cboMainGroup.SelectedIndex < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn ngành hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMainGroup.Focus();
                return false;
            }
            //if ((!radIsNew.Checked) && (!radIsOld.Checked))
            //{
            //    MessageBox.Show(this, "Vui lòng chọn loại hàng hoặc mới hoặc cũ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return false;
            //}
            if (objInventoryTerm != null)
            {
                //if (objInventoryTerm.IsNew == 1 && radIsOld.Checked)
                //{
                //    MessageBox.Show(this, "Kỳ kiểm kê [" + Convert.ToString(cboInventoryTerm.Text).Trim() + "] chỉ được khai báo loại hàng mới. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    return false;
                //}
                //else if (objInventoryTerm.IsNew == 0 && radIsNew.Checked)
                //{
                //    MessageBox.Show(this, "Kỳ kiểm kê [" + Convert.ToString(cboInventoryTerm.Text).Trim() + "] chỉ được khai báo loại hàng cũ. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    return false;
                //}
                if (objInventoryTerm.ProductStatusID != -1 && objInventoryTerm.ProductStatusID != Convert.ToInt32(cboProductStateID.SelectedValue))
                {
                    DataTable dtb = (DataTable)cboProductStateID.DataSource;
                    DataRow[] drs = dtb.Select("PRODUCTSTATUSID = " + objInventoryTerm.ProductStatusID);
                    string strName = (drs[0]["PRODUCTSTATUSNAME"] ?? "").ToString().Trim();
                    MessageBox.Show(this, "Kỳ kiểm kê [" + Convert.ToString(cboInventoryTerm.SelectedText).Trim() + "] chỉ được khai báo loại hàng " + strName + ". Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            int intSubGroupID = -1;
            if (cboSubGroup.Visible && cboSubGroup.SelectedIndex >= 0)
                intSubGroupID = Convert.ToInt32(cboSubGroup.SelectedValue);
            int intInventoryCount = objPLCInventory.CountSimilarInventory(Convert.ToInt32(cboInventoryTerm.SelectedValue), Convert.ToInt32(cboStore.SelectedValue), Convert.ToInt32(cboMainGroup.SelectedValue), Convert.ToInt32(cboProductStateID.SelectedValue), intSubGroupID);
            if (intInventoryCount < 2)
            {
                MessageBox.Show(this, "Phải tồn tại ít nhất 2 phiếu kiểm kê chưa duyệt mới được nối dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        #endregion

        #region event
        private void frmInventoryJoin_Load(object sender, EventArgs e)
        {
            LoadComboBox();
        }

        private void cboInventoryType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboInventoryType.SelectedIndex < 1)
            {
                cboInventoryType.SelectedIndex = intSelectedIndex_InventoryType;
                return;
            }
            DateTime dtmServerDate = Library.AppCore.Globals.GetServerDateTime();
            DataTable dtbINVTerm = objPLCInventoryTerm.SearchData(new object[] { "@Keywords", string.Empty, "@InventoryTypeID", Convert.ToInt32(cboInventoryType.SelectedValue), "@FromDate", dtmServerDate.AddDays(-100), "@ToDate", dtmServerDate.AddDays(1) });
            //string filterDate = string.Format(System.Globalization.CultureInfo.InvariantCulture,
            //                                "InventoryDate >= #{0:MM/dd/yyyy}# AND InventoryDate < #{1:MM/dd/yyyy}#",
            //                                dtmServerDate.AddDays(-100),
            //                                dtmServerDate.AddDays(1));
            //dtbINVTerm.DefaultView.RowFilter = filterDate;
            dtbINVTerm.DefaultView.Sort = "InventoryDate DESC";
            dtbINVTerm = dtbINVTerm.DefaultView.ToTable();
            Library.AppCore.LoadControls.ComboBoxObject.LoadDataFromDataTable(dtbINVTerm, cboInventoryTerm, "-- Chọn kỳ kiểm kê --");
            cboInventoryTerm.Enabled = true;

            ClearInfoTerm();
        }

        private void cboInventoryTerm_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboInventoryTerm.SelectedIndex < 1)
            {
                cboInventoryTerm.SelectedIndex = intSelectedIndex_InventoryTerm;
                return;
            }
            objPLCInventoryTerm.LoadInfo(ref objInventoryTerm, Convert.ToInt32(cboInventoryTerm.SelectedValue));
            objInventoryTerm.dtbTerm_MainGroup.DefaultView.RowFilter = "IsSelect = 1";
            DataTable dtbTerm_MainGroup = objInventoryTerm.dtbTerm_MainGroup.DefaultView.ToTable();
            if (dtbTerm_MainGroup.Columns.Contains("IsSelect"))
            {
                dtbTerm_MainGroup.Columns.Remove("IsSelect");
            }
            Library.AppCore.LoadControls.ComboBoxObject.LoadDataFromDataTable(dtbTerm_MainGroup, cboMainGroup, "-- Chọn ngành hàng --");
            cboMainGroup.Enabled = true;
            objInventoryTerm.dtbTerm_Store.DefaultView.RowFilter = "IsSelect = 1";
            DataTable dtbTerm_Store = objInventoryTerm.dtbTerm_Store.DefaultView.ToTable();
            if (dtbTerm_Store.Columns.Contains("IsSelect"))
            {
                dtbTerm_Store.Columns.Remove("IsSelect");
            }
            Library.AppCore.LoadControls.ComboBoxObject.LoadDataFromDataTable(dtbTerm_Store, cboStore, "-- Chọn chi nhánh --");
            cboStore.Enabled = true;

            dtbINVTerm_Brand = objInventoryTerm.dtbTerm_Brand.Copy();

            cboSubGroup.Visible = false;
            cboSubGroup.Enabled = false;
            if (cboSubGroup.Items.Count > 0)
                cboSubGroup.SelectedIndex = 0;
            txtSubGroup.Visible = true;
            if (objInventoryTerm.dtbTerm_SubGroup.Rows.Count > 0)
            {
                objInventoryTerm.dtbTerm_SubGroup.DefaultView.RowFilter = "IsSelect = 1";
                DataTable dtbTerm_SubGroup = objInventoryTerm.dtbTerm_SubGroup.DefaultView.ToTable();
                if (dtbTerm_SubGroup.Columns.Contains("IsSelect"))
                    dtbTerm_SubGroup.Columns.Remove("IsSelect");
                if (dtbTerm_SubGroup.Rows.Count > 0)
                {
                    Library.AppCore.LoadControls.ComboBoxObject.LoadDataFromDataTable(dtbTerm_SubGroup, cboSubGroup, "-- Chọn nhóm hàng --");
                    cboSubGroup.Visible = true;
                    cboSubGroup.Enabled = true;
                    txtSubGroup.Visible = false;
                }
            }
        }

        private void cboInventoryType_DropDown(object sender, EventArgs e)
        {
            intSelectedIndex_InventoryType = cboInventoryType.SelectedIndex;
        }

        private void cboInventoryTerm_DropDown(object sender, EventArgs e)
        {
            intSelectedIndex_InventoryTerm = cboInventoryTerm.SelectedIndex;
        }

        private void cboStore_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboStore.SelectedIndex < 1)
            {
                cboStore.SelectedIndex = intSelectedIndex_Store;
            }
        }

        private void cboMainGroup_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboMainGroup.SelectedIndex < 1)
            {
                cboMainGroup.SelectedIndex = intSelectedIndex_MainGroup;
            }
            cboSubGroup.Visible = false;
            cboSubGroup.Enabled = false;
            if (cboSubGroup.Items.Count > 0)
                cboSubGroup.SelectedIndex = 0;
            txtSubGroup.Visible = true;
            if (objInventoryTerm.dtbTerm_SubGroup.Rows.Count > 0)
            {
                objInventoryTerm.dtbTerm_SubGroup.DefaultView.RowFilter = "IsSelect = 1 AND MainGroupID = " + Convert.ToInt32(cboMainGroup.SelectedValue);
                DataTable dtbTerm_SubGroup = objInventoryTerm.dtbTerm_SubGroup.DefaultView.ToTable();
                if (dtbTerm_SubGroup.Columns.Contains("IsSelect"))
                    dtbTerm_SubGroup.Columns.Remove("IsSelect");
                if (dtbTerm_SubGroup.Rows.Count > 0)
                {
                    Library.AppCore.LoadControls.ComboBoxObject.LoadDataFromDataTable(dtbTerm_SubGroup, cboSubGroup);//, "-- Chọn nhóm hàng --");
                    cboSubGroup.Visible = true;
                    cboSubGroup.Enabled = true;
                    txtSubGroup.Visible = false;
                }
            }
        }

        private void cboStore_DropDown(object sender, EventArgs e)
        {
            intSelectedIndex_Store = cboStore.SelectedIndex;
        }

        private void cboMainGroup_DropDown(object sender, EventArgs e)
        {
            intSelectedIndex_MainGroup = cboMainGroup.SelectedIndex;
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdJoin_Click(object sender, EventArgs e)
        {
            cmdJoin.Enabled = false;
            if (!ValidateInput())
            {
                cmdJoin.Enabled = true;
                return;
            }
            if (MessageBox.Show(this, "Bạn có chắc muốn nối dữ liệu kiểm kê không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                cmdJoin.Enabled = true;
                return;
            }
            int intSubGroupID = -1;
            if (cboSubGroup.Visible && cboSubGroup.SelectedIndex >= 0)
                intSubGroupID = Convert.ToInt32(cboSubGroup.SelectedValue);
            bool bolResult = false;
            if (intSubGroupID > 0)
                bolResult = objPLCInventory.JoinInventorySubGroup(Convert.ToInt32(cboInventoryTerm.SelectedValue), Convert.ToInt32(cboStore.SelectedValue), Convert.ToInt32(cboMainGroup.SelectedValue), Convert.ToInt32(cboProductStateID.SelectedValue), intSubGroupID);
            else
                bolResult = objPLCInventory.JoinInventory(Convert.ToInt32(cboInventoryTerm.SelectedValue), Convert.ToInt32(cboStore.SelectedValue), Convert.ToInt32(cboMainGroup.SelectedValue), Convert.ToInt32(cboProductStateID.SelectedValue));
            if (!bolResult)
            {
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    cmdJoin.Enabled = true;
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
            }

            cmdJoin.Enabled = true;
            MessageBox.Show(this, "Nối dữ liệu kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        #endregion

    }
}
