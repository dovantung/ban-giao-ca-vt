﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmCheckIMEI_InOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCheckIMEI_InOut));
            this.flexImportResult = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.mnuExportExcel = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.flexImportResult)).BeginInit();
            this.mnuExportExcel.SuspendLayout();
            this.SuspendLayout();
            // 
            // flexImportResult
            // 
            this.flexImportResult.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexImportResult.AllowEditing = false;
            this.flexImportResult.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexImportResult.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexImportResult.AutoClipboard = true;
            this.flexImportResult.ColumnInfo = "10,1,0,0,0,105,Columns:0{Width:20;}\t";
            this.flexImportResult.ContextMenuStrip = this.mnuExportExcel;
            this.flexImportResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexImportResult.Location = new System.Drawing.Point(0, 0);
            this.flexImportResult.Name = "flexImportResult";
            this.flexImportResult.Rows.Count = 1;
            this.flexImportResult.Rows.DefaultSize = 21;
            this.flexImportResult.Size = new System.Drawing.Size(866, 458);
            this.flexImportResult.StyleInfo = resources.GetString("flexImportResult.StyleInfo");
            this.flexImportResult.TabIndex = 56;
            // 
            // mnuExportExcel
            // 
            this.mnuExportExcel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemExportExcel});
            this.mnuExportExcel.Name = "mnuImportExcel";
            this.mnuExportExcel.Size = new System.Drawing.Size(141, 26);
            // 
            // mnuItemExportExcel
            // 
            this.mnuItemExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemExportExcel.Image")));
            this.mnuItemExportExcel.Name = "mnuItemExportExcel";
            this.mnuItemExportExcel.Size = new System.Drawing.Size(140, 22);
            this.mnuItemExportExcel.Text = "Xuất ra Excel";
            this.mnuItemExportExcel.Click += new System.EventHandler(this.mnuItemExportExcel_Click);
            // 
            // frmCheckIMEI_InOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 458);
            this.Controls.Add(this.flexImportResult);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(874, 485);
            this.Name = "frmCheckIMEI_InOut";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kiểm tra IMEI xuất-nhập kho theo ngày chốt tồn kho kỳ kiểm kê";
            this.Load += new System.EventHandler(this.frmCheckIMEI_InOut_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flexImportResult)).EndInit();
            this.mnuExportExcel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private C1.Win.C1FlexGrid.C1FlexGrid flexImportResult;
        private System.Windows.Forms.ContextMenuStrip mnuExportExcel;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcel;
    }
}