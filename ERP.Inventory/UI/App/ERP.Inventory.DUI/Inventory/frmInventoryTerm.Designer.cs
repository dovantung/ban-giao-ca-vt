﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmInventoryTerm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInventoryTerm));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageList = new System.Windows.Forms.TabPage();
            this.gridDataDetail = new DevExpress.XtraGrid.GridControl();
            this.gridViewDataDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.cboProductStateIDSearch = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label10 = new System.Windows.Forms.Label();
            this.chkDeleted = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cboStoreIDList = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.lblInvTerm = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.cboInvTypeSearch = new System.Windows.Forms.ComboBox();
            this.tabPageDetail = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.flexStore = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.mnuStore = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemExportExcelTemp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemImportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chkAllowBarcode = new System.Windows.Forms.CheckBox();
            this.dtpLockDataTime = new System.Windows.Forms.DateTimePicker();
            this.radiButton = new System.Windows.Forms.RadioButton();
            this.radiSchedule = new System.Windows.Forms.RadioButton();
            this.btnCalInStock = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.cboProductStateID = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtInventoryTermID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpEndInventoryTime = new System.Windows.Forms.DateTimePicker();
            this.lblStatus = new System.Windows.Forms.Label();
            this.cboInventoryType = new System.Windows.Forms.ComboBox();
            this.dtpInventoryDate = new System.Windows.Forms.DateTimePicker();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.dtpBeginInventoryTime = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtInventoryTermName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabCustomer = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.flexBrand = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.flexSubGroup = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.flexMainGroup = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuAction_Add = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAction_Edit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAction_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuAction_ExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnUndo = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl.SuspendLayout();
            this.tabPageList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDataDetail)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabPageDetail.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexStore)).BeginInit();
            this.mnuStore.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabCustomer.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexBrand)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexSubGroup)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexMainGroup)).BeginInit();
            this.mnuAction.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPageList);
            this.tabControl.Controls.Add(this.tabPageDetail);
            this.tabControl.Location = new System.Drawing.Point(2, 2);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1127, 520);
            this.tabControl.TabIndex = 0;
            // 
            // tabPageList
            // 
            this.tabPageList.Controls.Add(this.gridDataDetail);
            this.tabPageList.Controls.Add(this.panel1);
            this.tabPageList.Location = new System.Drawing.Point(4, 25);
            this.tabPageList.Name = "tabPageList";
            this.tabPageList.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageList.Size = new System.Drawing.Size(1119, 491);
            this.tabPageList.TabIndex = 0;
            this.tabPageList.Text = "Danh sách";
            this.tabPageList.UseVisualStyleBackColor = true;
            this.tabPageList.Enter += new System.EventHandler(this.tabPageList_Enter);
            // 
            // gridDataDetail
            // 
            this.gridDataDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDataDetail.Location = new System.Drawing.Point(3, 74);
            this.gridDataDetail.MainView = this.gridViewDataDetail;
            this.gridDataDetail.Name = "gridDataDetail";
            this.gridDataDetail.Size = new System.Drawing.Size(1113, 414);
            this.gridDataDetail.TabIndex = 83;
            this.gridDataDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDataDetail});
            this.gridDataDetail.DoubleClick += new System.EventHandler(this.flexData_DoubleClick);
            // 
            // gridViewDataDetail
            // 
            this.gridViewDataDetail.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridViewDataDetail.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewDataDetail.GridControl = this.gridDataDetail;
            this.gridViewDataDetail.Name = "gridViewDataDetail";
            this.gridViewDataDetail.OptionsSelection.MultiSelect = true;
            this.gridViewDataDetail.OptionsView.ShowGroupPanel = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.cboProductStateIDSearch);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.chkDeleted);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.cboStoreIDList);
            this.panel1.Controls.Add(this.lblInvTerm);
            this.panel1.Controls.Add(this.dtpToDate);
            this.panel1.Controls.Add(this.txtKeywords);
            this.panel1.Controls.Add(this.dtpFromDate);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.btnExportExcel);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.cboInvTypeSearch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1113, 71);
            this.panel1.TabIndex = 56;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 16);
            this.label12.TabIndex = 55;
            this.label12.Text = "Ngày KK:";
            // 
            // cboProductStateIDSearch
            // 
            this.cboProductStateIDSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductStateIDSearch.Location = new System.Drawing.Point(476, 37);
            this.cboProductStateIDSearch.Margin = new System.Windows.Forms.Padding(0);
            this.cboProductStateIDSearch.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboProductStateIDSearch.Name = "cboProductStateIDSearch";
            this.cboProductStateIDSearch.Size = new System.Drawing.Size(234, 24);
            this.cboProductStateIDSearch.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(729, 12);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 16);
            this.label10.TabIndex = 44;
            this.label10.Text = "Kho kiểm kê:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.label10, "Nhập mã kỳ kiểm kê hoặc tên kỳ kiểm kê");
            // 
            // chkDeleted
            // 
            this.chkDeleted.AutoSize = true;
            this.chkDeleted.Location = new System.Drawing.Point(732, 37);
            this.chkDeleted.Name = "chkDeleted";
            this.chkDeleted.Size = new System.Drawing.Size(68, 20);
            this.chkDeleted.TabIndex = 13;
            this.chkDeleted.Text = "Đã hủy";
            this.chkDeleted.UseVisualStyleBackColor = true;
            this.chkDeleted.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 40);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 16);
            this.label9.TabIndex = 43;
            this.label9.Text = "Loại kiểm kê:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.label9, "Nhập mã kỳ kiểm kê hoặc tên kỳ kiểm kê");
            // 
            // cboStoreIDList
            // 
            this.cboStoreIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreIDList.Location = new System.Drawing.Point(813, 7);
            this.cboStoreIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreIDList.Name = "cboStoreIDList";
            this.cboStoreIDList.Size = new System.Drawing.Size(233, 24);
            this.cboStoreIDList.TabIndex = 7;
            // 
            // lblInvTerm
            // 
            this.lblInvTerm.AutoSize = true;
            this.lblInvTerm.Location = new System.Drawing.Point(359, 12);
            this.lblInvTerm.Name = "lblInvTerm";
            this.lblInvTerm.Size = new System.Drawing.Size(76, 16);
            this.lblInvTerm.TabIndex = 39;
            this.lblInvTerm.Text = "Kỳ kiểm kê:";
            this.lblInvTerm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.lblInvTerm, "Nhập mã kỳ kiểm kê hoặc tên kỳ kiểm kê");
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(224, 9);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(119, 22);
            this.dtpToDate.TabIndex = 3;
            // 
            // txtKeywords
            // 
            this.txtKeywords.Location = new System.Drawing.Point(476, 9);
            this.txtKeywords.MaxLength = 200;
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.Size = new System.Drawing.Size(234, 22);
            this.txtKeywords.TabIndex = 5;
            this.toolTip1.SetToolTip(this.txtKeywords, "Nhập mã kỳ kiểm kê hoặc tên kỳ kiểm kê");
            this.txtKeywords.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeywords_KeyPress);
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(104, 9);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(119, 22);
            this.dtpFromDate.TabIndex = 1;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(813, 35);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(114, 25);
            this.btnSearch.TabIndex = 15;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Enabled = false;
            this.btnExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportExcel.Location = new System.Drawing.Point(933, 35);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(114, 25);
            this.btnExportExcel.TabIndex = 17;
            this.btnExportExcel.Text = "     Xuất Excel";
            this.btnExportExcel.UseVisualStyleBackColor = true;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(359, 40);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 16);
            this.label11.TabIndex = 52;
            this.label11.Text = "Loại hàng:";
            // 
            // cboInvTypeSearch
            // 
            this.cboInvTypeSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInvTypeSearch.FormattingEnabled = true;
            this.cboInvTypeSearch.Location = new System.Drawing.Point(104, 36);
            this.cboInvTypeSearch.Name = "cboInvTypeSearch";
            this.cboInvTypeSearch.Size = new System.Drawing.Size(239, 24);
            this.cboInvTypeSearch.TabIndex = 9;
            // 
            // tabPageDetail
            // 
            this.tabPageDetail.Controls.Add(this.tabControl1);
            this.tabPageDetail.Location = new System.Drawing.Point(4, 25);
            this.tabPageDetail.Name = "tabPageDetail";
            this.tabPageDetail.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDetail.Size = new System.Drawing.Size(1119, 491);
            this.tabPageDetail.TabIndex = 1;
            this.tabPageDetail.Text = "Thông tin chi tiết";
            this.tabPageDetail.UseVisualStyleBackColor = true;
            this.tabPageDetail.Enter += new System.EventHandler(this.tabPageDetail_Enter);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabCustomer);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1113, 485);
            this.tabControl1.TabIndex = 68;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1105, 456);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thông tin kiểm kê";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.flexStore);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(610, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(492, 450);
            this.groupBox1.TabIndex = 55;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kho kiểm kê:";
            // 
            // flexStore
            // 
            this.flexStore.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexStore.AutoClipboard = true;
            this.flexStore.ColumnInfo = "10,1,0,0,0,105,Columns:";
            this.flexStore.ContextMenuStrip = this.mnuStore;
            this.flexStore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexStore.Location = new System.Drawing.Point(3, 18);
            this.flexStore.Name = "flexStore";
            this.flexStore.Rows.DefaultSize = 21;
            this.flexStore.Size = new System.Drawing.Size(486, 429);
            this.flexStore.StyleInfo = resources.GetString("flexStore.StyleInfo");
            this.flexStore.TabIndex = 0;
            this.flexStore.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            this.flexStore.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexStore_AfterEdit);
            this.flexStore.CellButtonClick += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexStore_CellButtonClick);
            this.flexStore.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.flexStore_KeyPress);
            // 
            // mnuStore
            // 
            this.mnuStore.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mnuStore.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemExportExcelTemp,
            this.mnuItemImportExcel});
            this.mnuStore.Name = "mnuStore";
            this.mnuStore.Size = new System.Drawing.Size(175, 56);
            // 
            // mnuItemExportExcelTemp
            // 
            this.mnuItemExportExcelTemp.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportExcelTemp.Name = "mnuItemExportExcelTemp";
            this.mnuItemExportExcelTemp.Size = new System.Drawing.Size(174, 26);
            this.mnuItemExportExcelTemp.Text = "Xuất excel mẫu";
            this.mnuItemExportExcelTemp.Click += new System.EventHandler(this.mnuItemExportExcelTemp_Click);
            // 
            // mnuItemImportExcel
            // 
            this.mnuItemImportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemImportExcel.Name = "mnuItemImportExcel";
            this.mnuItemImportExcel.Size = new System.Drawing.Size(174, 26);
            this.mnuItemImportExcel.Text = "Nhập Kho từ excel";
            this.mnuItemImportExcel.Click += new System.EventHandler(this.mnuItemImportExcel_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chkAllowBarcode);
            this.groupBox5.Controls.Add(this.dtpLockDataTime);
            this.groupBox5.Controls.Add(this.radiButton);
            this.groupBox5.Controls.Add(this.radiSchedule);
            this.groupBox5.Controls.Add(this.btnCalInStock);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.cboProductStateID);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.txtInventoryTermID);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.dtpEndInventoryTime);
            this.groupBox5.Controls.Add(this.lblStatus);
            this.groupBox5.Controls.Add(this.cboInventoryType);
            this.groupBox5.Controls.Add(this.dtpInventoryDate);
            this.groupBox5.Controls.Add(this.txtDescription);
            this.groupBox5.Controls.Add(this.dtpBeginInventoryTime);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.txtInventoryTermName);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(607, 450);
            this.groupBox5.TabIndex = 67;
            this.groupBox5.TabStop = false;
            // 
            // chkAllowBarcode
            // 
            this.chkAllowBarcode.AutoSize = true;
            this.chkAllowBarcode.Location = new System.Drawing.Point(23, 250);
            this.chkAllowBarcode.Name = "chkAllowBarcode";
            this.chkAllowBarcode.Size = new System.Drawing.Size(271, 20);
            this.chkAllowBarcode.TabIndex = 85;
            this.chkAllowBarcode.Text = "Cho phép sử dụng tính năng bắn barcode";
            this.chkAllowBarcode.UseVisualStyleBackColor = true;
            // 
            // dtpLockDataTime
            // 
            this.dtpLockDataTime.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpLockDataTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLockDataTime.Location = new System.Drawing.Point(158, 195);
            this.dtpLockDataTime.Name = "dtpLockDataTime";
            this.dtpLockDataTime.Size = new System.Drawing.Size(142, 22);
            this.dtpLockDataTime.TabIndex = 5;
            this.dtpLockDataTime.ValueChanged += new System.EventHandler(this.dtpLockDataTime_ValueChanged);
            // 
            // radiButton
            // 
            this.radiButton.AutoSize = true;
            this.radiButton.Location = new System.Drawing.Point(23, 224);
            this.radiButton.Name = "radiButton";
            this.radiButton.Size = new System.Drawing.Size(129, 20);
            this.radiButton.TabIndex = 68;
            this.radiButton.Text = "Chốt tồn bằng tay";
            this.radiButton.UseVisualStyleBackColor = true;
            // 
            // radiSchedule
            // 
            this.radiSchedule.AutoSize = true;
            this.radiSchedule.Checked = true;
            this.radiSchedule.Location = new System.Drawing.Point(23, 198);
            this.radiSchedule.Name = "radiSchedule";
            this.radiSchedule.Size = new System.Drawing.Size(138, 20);
            this.radiSchedule.TabIndex = 67;
            this.radiSchedule.TabStop = true;
            this.radiSchedule.Text = "Đặt lịch chốt tồn lúc";
            this.radiSchedule.UseVisualStyleBackColor = true;
            // 
            // btnCalInStock
            // 
            this.btnCalInStock.Enabled = false;
            this.btnCalInStock.Image = global::ERP.Inventory.DUI.Properties.Resources.cal;
            this.btnCalInStock.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCalInStock.Location = new System.Drawing.Point(158, 222);
            this.btnCalInStock.Name = "btnCalInStock";
            this.btnCalInStock.Size = new System.Drawing.Size(109, 25);
            this.btnCalInStock.TabIndex = 60;
            this.btnCalInStock.Text = "Tính tồn kho";
            this.btnCalInStock.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCalInStock.UseVisualStyleBackColor = true;
            this.btnCalInStock.Click += new System.EventHandler(this.btnCalInStock_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 16);
            this.label7.TabIndex = 59;
            this.label7.Text = "Mã kỳ kiểm kê:";
            // 
            // cboProductStateID
            // 
            this.cboProductStateID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProductStateID.FormattingEnabled = true;
            this.cboProductStateID.Location = new System.Drawing.Point(390, 105);
            this.cboProductStateID.Name = "cboProductStateID";
            this.cboProductStateID.Size = new System.Drawing.Size(199, 24);
            this.cboProductStateID.TabIndex = 64;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(435, 140);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(12, 16);
            this.label13.TabIndex = 66;
            this.label13.Text = "-";
            // 
            // txtInventoryTermID
            // 
            this.txtInventoryTermID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInventoryTermID.Location = new System.Drawing.Point(114, 23);
            this.txtInventoryTermID.MaxLength = 200;
            this.txtInventoryTermID.Name = "txtInventoryTermID";
            this.txtInventoryTermID.ReadOnly = true;
            this.txtInventoryTermID.Size = new System.Drawing.Size(194, 22);
            this.txtInventoryTermID.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(314, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 16);
            this.label8.TabIndex = 50;
            this.label8.Text = "Loại hàng:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 43;
            this.label2.Text = "Mô tả:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 16);
            this.label5.TabIndex = 51;
            this.label5.Text = "Loai kiểm kê:";
            // 
            // dtpEndInventoryTime
            // 
            this.dtpEndInventoryTime.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpEndInventoryTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndInventoryTime.Location = new System.Drawing.Point(453, 135);
            this.dtpEndInventoryTime.Name = "dtpEndInventoryTime";
            this.dtpEndInventoryTime.Size = new System.Drawing.Size(136, 22);
            this.dtpEndInventoryTime.TabIndex = 65;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(6, 169);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(133, 16);
            this.lblStatus.TabIndex = 46;
            this.lblStatus.Text = "Phương thức chốt tồn:";
            // 
            // cboInventoryType
            // 
            this.cboInventoryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInventoryType.DropDownWidth = 250;
            this.cboInventoryType.Location = new System.Drawing.Point(114, 105);
            this.cboInventoryType.Name = "cboInventoryType";
            this.cboInventoryType.Size = new System.Drawing.Size(194, 24);
            this.cboInventoryType.TabIndex = 3;
            // 
            // dtpInventoryDate
            // 
            this.dtpInventoryDate.CustomFormat = "dd/MM/yyyy";
            this.dtpInventoryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInventoryDate.Location = new System.Drawing.Point(114, 135);
            this.dtpInventoryDate.Name = "dtpInventoryDate";
            this.dtpInventoryDate.Size = new System.Drawing.Size(107, 22);
            this.dtpInventoryDate.TabIndex = 2;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(114, 77);
            this.txtDescription.MaxLength = 200;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(475, 22);
            this.txtDescription.TabIndex = 1;
            // 
            // dtpBeginInventoryTime
            // 
            this.dtpBeginInventoryTime.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpBeginInventoryTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBeginInventoryTime.Location = new System.Drawing.Point(293, 135);
            this.dtpBeginInventoryTime.Name = "dtpBeginInventoryTime";
            this.dtpBeginInventoryTime.Size = new System.Drawing.Size(136, 22);
            this.dtpBeginInventoryTime.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 16);
            this.label3.TabIndex = 42;
            this.label3.Text = "Tên kỳ kiểm kê:";
            // 
            // txtInventoryTermName
            // 
            this.txtInventoryTermName.Location = new System.Drawing.Point(114, 51);
            this.txtInventoryTermName.MaxLength = 200;
            this.txtInventoryTermName.Name = "txtInventoryTermName";
            this.txtInventoryTermName.Size = new System.Drawing.Size(475, 22);
            this.txtInventoryTermName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 16);
            this.label1.TabIndex = 44;
            this.label1.Text = "Ngày kiểm kê:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(227, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 16);
            this.label4.TabIndex = 45;
            this.label4.Text = "Từ ngày:";
            // 
            // tabCustomer
            // 
            this.tabCustomer.BackColor = System.Drawing.Color.Transparent;
            this.tabCustomer.Controls.Add(this.groupBox3);
            this.tabCustomer.Controls.Add(this.groupBox4);
            this.tabCustomer.Controls.Add(this.groupBox2);
            this.tabCustomer.Location = new System.Drawing.Point(4, 25);
            this.tabCustomer.Name = "tabCustomer";
            this.tabCustomer.Padding = new System.Windows.Forms.Padding(3);
            this.tabCustomer.Size = new System.Drawing.Size(1105, 456);
            this.tabCustomer.TabIndex = 3;
            this.tabCustomer.Text = "Ngành Hàng, Nhóm Hàng, NSX";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.flexBrand);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(691, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(411, 450);
            this.groupBox3.TabIndex = 57;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Nhà sản xuất:";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // flexBrand
            // 
            this.flexBrand.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexBrand.AutoClipboard = true;
            this.flexBrand.ClipboardCopyMode = C1.Win.C1FlexGrid.ClipboardCopyModeEnum.DataAndAllHeaders;
            this.flexBrand.ColumnInfo = "10,1,0,0,0,105,Columns:";
            this.flexBrand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexBrand.Location = new System.Drawing.Point(3, 18);
            this.flexBrand.Name = "flexBrand";
            this.flexBrand.Rows.DefaultSize = 21;
            this.flexBrand.Size = new System.Drawing.Size(405, 429);
            this.flexBrand.StyleInfo = resources.GetString("flexBrand.StyleInfo");
            this.flexBrand.TabIndex = 0;
            this.flexBrand.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            this.flexBrand.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexBrand_AfterEdit);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.flexSubGroup);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox4.Location = new System.Drawing.Point(320, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(371, 450);
            this.groupBox4.TabIndex = 61;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Nhóm hàng:";
            // 
            // flexSubGroup
            // 
            this.flexSubGroup.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexSubGroup.AutoClipboard = true;
            this.flexSubGroup.ClipboardCopyMode = C1.Win.C1FlexGrid.ClipboardCopyModeEnum.DataAndAllHeaders;
            this.flexSubGroup.ColumnInfo = "10,1,0,0,0,105,Columns:";
            this.flexSubGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexSubGroup.Location = new System.Drawing.Point(3, 18);
            this.flexSubGroup.Name = "flexSubGroup";
            this.flexSubGroup.Rows.DefaultSize = 21;
            this.flexSubGroup.Size = new System.Drawing.Size(365, 429);
            this.flexSubGroup.StyleInfo = resources.GetString("flexSubGroup.StyleInfo");
            this.flexSubGroup.TabIndex = 0;
            this.flexSubGroup.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            this.flexSubGroup.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexSubGroup_AfterEdit);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.flexMainGroup);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(317, 450);
            this.groupBox2.TabIndex = 56;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ngành hàng:";
            // 
            // flexMainGroup
            // 
            this.flexMainGroup.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexMainGroup.AutoClipboard = true;
            this.flexMainGroup.ClipboardCopyMode = C1.Win.C1FlexGrid.ClipboardCopyModeEnum.DataAndAllHeaders;
            this.flexMainGroup.ColumnInfo = "10,1,0,0,0,105,Columns:";
            this.flexMainGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexMainGroup.Location = new System.Drawing.Point(3, 18);
            this.flexMainGroup.Name = "flexMainGroup";
            this.flexMainGroup.Rows.DefaultSize = 21;
            this.flexMainGroup.Size = new System.Drawing.Size(311, 429);
            this.flexMainGroup.StyleInfo = resources.GetString("flexMainGroup.StyleInfo");
            this.flexMainGroup.TabIndex = 0;
            this.flexMainGroup.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            this.flexMainGroup.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexMainGroup_AfterEdit);
            // 
            // mnuAction
            // 
            this.mnuAction.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAction_Add,
            this.mnuAction_Edit,
            this.mnuAction_Delete,
            this.toolStripSeparator1,
            this.mnuAction_ExportExcel});
            this.mnuAction.Name = "mnuAction";
            this.mnuAction.Size = new System.Drawing.Size(134, 114);
            // 
            // mnuAction_Add
            // 
            this.mnuAction_Add.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.mnuAction_Add.Name = "mnuAction_Add";
            this.mnuAction_Add.Size = new System.Drawing.Size(133, 26);
            this.mnuAction_Add.Text = "Thêm mới";
            this.mnuAction_Add.Click += new System.EventHandler(this.mnuAction_Add_Click);
            // 
            // mnuAction_Edit
            // 
            this.mnuAction_Edit.Image = global::ERP.Inventory.DUI.Properties.Resources.edit;
            this.mnuAction_Edit.Name = "mnuAction_Edit";
            this.mnuAction_Edit.Size = new System.Drawing.Size(133, 26);
            this.mnuAction_Edit.Text = "Chỉnh sửa";
            this.mnuAction_Edit.Click += new System.EventHandler(this.mnuAction_Edit_Click);
            // 
            // mnuAction_Delete
            // 
            this.mnuAction_Delete.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.mnuAction_Delete.Name = "mnuAction_Delete";
            this.mnuAction_Delete.Size = new System.Drawing.Size(133, 26);
            this.mnuAction_Delete.Text = "Xóa";
            this.mnuAction_Delete.Click += new System.EventHandler(this.mnuAction_Delete_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(130, 6);
            // 
            // mnuAction_ExportExcel
            // 
            this.mnuAction_ExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuAction_ExportExcel.Name = "mnuAction_ExportExcel";
            this.mnuAction_ExportExcel.Size = new System.Drawing.Size(133, 26);
            this.mnuAction_ExportExcel.Text = "Xuất Excel";
            this.mnuAction_ExportExcel.Click += new System.EventHandler(this.mnuAction_ExportExcel_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEdit.Image = global::ERP.Inventory.DUI.Properties.Resources.edit;
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(710, 528);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(100, 25);
            this.btnEdit.TabIndex = 34;
            this.btnEdit.Text = "  Chỉnh sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUndo.Image = global::ERP.Inventory.DUI.Properties.Resources.undo;
            this.btnUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUndo.Location = new System.Drawing.Point(1028, 528);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(100, 25);
            this.btnUndo.TabIndex = 37;
            this.btnUndo.Text = "  Bỏ qua";
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(816, 528);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 25);
            this.btnDelete.TabIndex = 35;
            this.btnDelete.Text = "   Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(604, 528);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(100, 25);
            this.btnAdd.TabIndex = 33;
            this.btnAdd.Text = "  Thêm";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(922, 528);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(100, 25);
            this.btnUpdate.TabIndex = 36;
            this.btnUpdate.Text = "   Cập nhật";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // frmInventoryTerm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1131, 561);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnUndo);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.tabControl);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(969, 549);
            this.Name = "frmInventoryTerm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý khai báo kỳ kiểm kê";
            this.Load += new System.EventHandler(this.frmInventoryTerm_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPageList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDataDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDataDetail)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPageDetail.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexStore)).EndInit();
            this.mnuStore.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabCustomer.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexBrand)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexSubGroup)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexMainGroup)).EndInit();
            this.mnuAction.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageList;
        private System.Windows.Forms.TabPage tabPageDetail;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtKeywords;
        private System.Windows.Forms.Label lblInvTerm;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtInventoryTermName;
        private System.Windows.Forms.DateTimePicker dtpLockDataTime;
        private System.Windows.Forms.DateTimePicker dtpBeginInventoryTime;
        private System.Windows.Forms.DateTimePicker dtpInventoryDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboInventoryType;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private C1.Win.C1FlexGrid.C1FlexGrid flexBrand;
        private C1.Win.C1FlexGrid.C1FlexGrid flexMainGroup;
        private C1.Win.C1FlexGrid.C1FlexGrid flexStore;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuAction_Add;
        private System.Windows.Forms.ToolStripMenuItem mnuAction_Edit;
        private System.Windows.Forms.ToolStripMenuItem mnuAction_Delete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuAction_ExportExcel;
        private System.Windows.Forms.Button btnExportExcel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtInventoryTermID;
        private System.Windows.Forms.Button btnCalInStock;
        private System.Windows.Forms.GroupBox groupBox4;
        private C1.Win.C1FlexGrid.C1FlexGrid flexSubGroup;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboInvTypeSearch;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStoreIDList;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox chkDeleted;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboProductStateIDSearch;
        private System.Windows.Forms.ComboBox cboProductStateID;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraGrid.GridControl gridDataDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDataDetail;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker dtpEndInventoryTime;
        private System.Windows.Forms.RadioButton radiButton;
        private System.Windows.Forms.RadioButton radiSchedule;
        private System.Windows.Forms.ContextMenuStrip mnuStore;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcelTemp;
        private System.Windows.Forms.ToolStripMenuItem mnuItemImportExcel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabCustomer;
        private System.Windows.Forms.CheckBox chkAllowBarcode;
    }
}