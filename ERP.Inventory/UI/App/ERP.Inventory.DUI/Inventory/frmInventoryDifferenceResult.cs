﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmInventoryDifferenceResult : Form
    {
        public frmInventoryDifferenceResult()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        public List<ERP.Inventory.PLC.Inventory.WSInventory.Inventory> lstInventory = null;
        private ERP.Inventory.PLC.Inventory.PLCInventory objPLCInventory = new PLC.Inventory.PLCInventory();
        private ERP.MasterData.PLC.MD.PLCInventoryType objPLCInventoryType = new MasterData.PLC.MD.PLCInventoryType();
        private ERP.MasterData.PLC.MD.WSInventoryType.InventoryType objInventoryType = new MasterData.PLC.MD.WSInventoryType.InventoryType();
        ERP.Inventory.PLC.Inventory.WSInventory.Inventory objInventory = new PLC.Inventory.WSInventory.Inventory();
        public ERP.Inventory.PLC.Inventory.WSInventoryTerm.InventoryTerm objInventoryTerm = null;
        private ERP.Inventory.PLC.Inventory.PLCInventoryTerm objPLCInventoryTerm = new PLC.Inventory.PLCInventoryTerm();
        private DataTable dtbInventoryDetail = new DataTable();
        private DataTable dtbInventoryUnEvent = new DataTable();
        private DataTable dtbData = new DataTable();
        private int intKeyIMEI = -1;
        private DataTable dtbInStockProduct = null;
        private DataTable dtbInStockProductIMEI = null;
        private string strIMEICheck = string.Empty;
        public  int intSubGroupID = 0;
        public bool bolIsClone = false;
        public bool bolIsAddSumRow = false;
        public bool bolIsFirstTime = false;
        private int intQuantityOld = 0;
        private int intQuantity_InventoryOld = 0;
        private int intDifferentOld = 0;

        private int intQuantityNew = 0;
        private int intQuantity_InventoryNew = 0;
        private int intDifferentNew = 0;

        private int intRowSum = 0;
        private int intSumProd = 0;
        private string[] arrReviewUserList;
        private bool bolIsUpdateReviewStatus = false;

        public bool IsUpdateReviewStatus
        {
            get { return bolIsUpdateReviewStatus; }
            set { bolIsUpdateReviewStatus = value; }
        }

        public string[] ReviewUserList
        {
            get { return arrReviewUserList; }
            set { arrReviewUserList = value; }
        }
        private void frmInventoryDifferenceResult_Load(object sender, EventArgs e)
        {
            LoadInfoInventory();
            dtbData.Columns["InventoryID"].SetOrdinal(0);
            flexData.DataSource = dtbData;
            intSumProd = dtbData.Rows.Count - intRowSum;
            lblSum.Text = intSumProd.ToString("#,###");
            CustomFlex();
        }

        private void LoadInfoInventory()
        {
            intRowSum = 0;
            intSumProd = 0;
            foreach (ERP.Inventory.PLC.Inventory.WSInventory.Inventory objInventory in lstInventory)
            {
                dtbInventoryDetail = null;
                dtbInStockProduct = null;
                dtbInStockProductIMEI = null;
                ERP.Inventory.PLC.Inventory.WSInventory.Inventory obj = new PLC.Inventory.WSInventory.Inventory();
                objPLCInventory.LoadInventoryInfo(ref obj, objInventory.InventoryID, ref dtbInventoryDetail);
                dtbInventoryUnEvent = objPLCInventory.GetInventoryUnEvent(objInventory.InventoryID);

                if (dtbInStockProduct == null || dtbInStockProduct.Rows.Count < 1)
                {
                    dtbInStockProduct = objPLCInventory.GetListTermStock(objInventory.InventoryTermID, objInventory.InventoryStoreID, objInventory.MainGroupID, objInventory.SubGroupID);
                    #region
                    dtbInStockProduct.Columns.Add("InventoryID", typeof(string));
                    dtbInStockProduct.Columns.Add("CabinetNumber", typeof(int));
                    dtbInStockProduct.Columns["CabinetNumber"].SetOrdinal(0);
                    dtbInStockProduct.Columns.Add("IMEI", typeof(string));
                    dtbInStockProduct.Columns["IMEI"].SetOrdinal(3);
                    dtbInStockProduct.Columns.Add("Col_IMEI", typeof(string));
                    dtbInStockProduct.Columns["Col_IMEI"].SetOrdinal(4);
                    dtbInStockProduct.Columns.Add("IMEI_Inventory", typeof(string));
                    dtbInStockProduct.Columns.Add("Quantity_Inventory", typeof(decimal));
                    dtbInStockProduct.Columns.Add("Different", typeof(decimal));
                    #endregion
                    dtbInStockProduct.PrimaryKey = new DataColumn[] { dtbInStockProduct.Columns["ProductID"] };
                }

                if (dtbInStockProductIMEI == null || dtbInStockProductIMEI.Rows.Count < 1)
                {
                    dtbInStockProductIMEI = objPLCInventory.GetListTermStockIMEI(objInventory.InventoryTermID, objInventory.InventoryStoreID, objInventory.MainGroupID, objInventory.SubGroupID);
                    #region
                    dtbInStockProductIMEI.Columns.Add("InventoryID", typeof(string));
                    dtbInStockProductIMEI.Columns.Add("CabinetNumber", typeof(int));
                    dtbInStockProductIMEI.Columns["CabinetNumber"].SetOrdinal(0);
                    dtbInStockProductIMEI.Columns.Add("Col_IMEI", typeof(string));
                    dtbInStockProductIMEI.Columns["Col_IMEI"].SetOrdinal(4);
                    dtbInStockProductIMEI.Columns.Add("IMEI_Inventory", typeof(string));
                    dtbInStockProductIMEI.Columns.Add("Quantity_Inventory", typeof(decimal));
                    dtbInStockProductIMEI.Columns.Add("Different", typeof(decimal));
                    dtbInStockProductIMEI.Columns.Add("IsRequestIMEI", typeof(bool));
                    dtbInStockProductIMEI.Columns["IsRequestIMEI"].DefaultValue = false;
                    #endregion
                    for (int i = dtbInStockProductIMEI.Rows.Count - 1; i > -1; i--)
                    {
                        if (dtbInStockProductIMEI.Rows[i]["IsNew"] != null && (!Convert.IsDBNull(dtbInStockProductIMEI.Rows[i]["IsNew"])) && Convert.ToBoolean(dtbInStockProductIMEI.Rows[i]["IsNew"]) != objInventory.IsNew)
                        {
                            dtbInStockProductIMEI.Rows.RemoveAt(i);
                        }
                    }
                    dtbInStockProductIMEI.PrimaryKey = new DataColumn[] { dtbInStockProductIMEI.Columns["ProductID"], dtbInStockProductIMEI.Columns["IMEI"] };
                }

                #region
                if (dtbInventoryDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtbInventoryDetail.Rows.Count; i++)
                    {
                        DataRow drDetail = dtbInventoryDetail.Rows[i];
                        //xem chenh lech: xet IsRequestIMEI = 0 truoc, tiep theo IsRequestIMEI = 1 ( xet IsInStock = 1 truoc, tiep theo IsInStock = 0 )
                        string strProductID = Convert.ToString(drDetail["ProductID"]).Trim();
                        string strProductName = Convert.ToString(drDetail["ProductName"]).Trim();
                        int intCabinetNumber = Convert.ToInt32(drDetail["CabinetNumber"]);
                        decimal decQuantity = Convert.ToDecimal(drDetail["Quantity"]);
                        decimal decQuantityInStock = Convert.ToDecimal(drDetail["InStockQuantity"]);
                        if (!Convert.ToBoolean(drDetail["IsRequestIMEI"]))
                        {
                            DataRow rInStockProduct = dtbInStockProduct.Rows.Find(strProductID);
                            if (rInStockProduct != null)
                            {
                                rInStockProduct["CabinetNumber"] = intCabinetNumber;
                                rInStockProduct["Quantity_Inventory"] = decQuantity;
                                rInStockProduct["Different"] = decQuantityInStock - decQuantity;
                            }
                        }
                        else
                        {
                            string strIMEI = Convert.ToString(drDetail["IMEI"]).Trim();
                            if (Convert.ToBoolean(drDetail["IsInStock"]))
                            {
                                DataRow rInStockIMEI = dtbInStockProductIMEI.Rows.Find(new object[] { strProductID, strIMEI });
                                if (rInStockIMEI != null)
                                {
                                    rInStockIMEI["CabinetNumber"] = intCabinetNumber;
                                    rInStockIMEI["Col_IMEI"] = strIMEI;
                                    rInStockIMEI["IMEI_Inventory"] = strIMEI;
                                    rInStockIMEI["Quantity_Inventory"] = 1;
                                    rInStockIMEI["Different"] = decQuantityInStock - 1;
                                }
                            }
                            else
                            {
                                DataRow[] rExistIMEINotInStock = dtbInStockProductIMEI.Select("TRIM(ProductID) = '" + strProductID + "' AND (Col_IMEI IS NULL OR TRIM(Col_IMEI)='') AND TRIM(IMEI_Inventory) = '" + strIMEI + "'");
                                if (rExistIMEINotInStock.Length < 1)
                                {
                                    DataRow rNew = dtbInStockProductIMEI.NewRow();
                                    rNew["CabinetNumber"] = intCabinetNumber;
                                    rNew["ProductID"] = strProductID;
                                    rNew["ProductName"] = strProductName;
                                    rNew["IMEI"] = (--intKeyIMEI);
                                    rNew["Col_IMEI"] = string.Empty;
                                    rNew["IMEI_Inventory"] = strIMEI;
                                    rNew["Quantity_Inventory"] = 1;
                                    decimal decQuantity_InStock = 0;
                                    rNew["Different"] = decQuantity_InStock - 1;
                                    dtbInStockProductIMEI.Rows.Add(rNew);
                                }
                            }
                        }
                    }
                }
                #endregion

                if (!bolIsClone)
                {
                    dtbData = dtbInStockProductIMEI.Clone();
                    bolIsClone = true;
                }
                if (objInventory != null && Convert.ToString(objInventory.InventoryID) != string.Empty)
                {
                    if (dtbInventoryUnEvent != null && dtbInventoryUnEvent.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtbInventoryUnEvent.Rows.Count; i++)
                        {
                            DataRow r = dtbInventoryUnEvent.Rows[i];
                            DataRow dr = dtbData.NewRow();
                            dr["InventoryID"] = objInventory.InventoryID;
                            dr["CabinetNumber"] = r["CabinetNumber"];
                            dr["ProductID"] = r["ProductID"];
                            dr["ProductName"] = r["ProductName"];
                            dr["IMEI"] = r["StockIMEI"];
                            dr["Col_IMEI"] = r["StockIMEI"];
                            if (Convert.ToDecimal(r["StockQuantity"]) != 0)
                            {
                                dr["Quantity"] = r["StockQuantity"];
                            }
                            dr["IMEI_Inventory"] = r["InventoryIMEI"];
                            if (Convert.ToDecimal(r["InventoryQuantity"]) != 0)
                            {
                                dr["Quantity_Inventory"] = r["InventoryQuantity"];
                            }
                            dr["Different"] = r["UnEventQuantity"];
                            if (dr["IMEI"] == null || Convert.IsDBNull(dr["IMEI"]))
                            {
                                dr["IMEI"] = (--intKeyIMEI);
                            }
                            if (dr["IMEI"].ToString() == Convert.ToString(--intKeyIMEI))
                            {
                                intKeyIMEI--;
                            }
                            dtbData.Rows.Add(dr);
                        }
                    }
                    else
                    {
                        DataTable dtbCopy = dtbInventoryDetail.Copy();
                        if (dtbCopy != null && dtbCopy.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtbCopy.Rows.Count; i++)
                            {
                                DataRow row = dtbCopy.Rows[i];
                                if (!Convert.ToBoolean(row["IsRequestIMEI"]))
                                {
                                    DataRow rProduct = dtbInStockProduct.Rows.Find(row["ProductID"].ToString());
                                    if (rProduct == null)
                                    {
                                        rProduct = dtbInStockProduct.NewRow();
                                        rProduct["IsRequestIMEI"] = 0;
                                        rProduct["CabinetNumber"] = row["CabinetNumber"];
                                        rProduct["ProductID"] = row["ProductID"];
                                        rProduct["ProductName"] = row["ProductName"];
                                        rProduct["IMEI"] = string.Empty;
                                        rProduct["Col_IMEI"] = string.Empty;
                                        rProduct["IMEI_Inventory"] = string.Empty;
                                        rProduct["Quantity_Inventory"] = row["Quantity"];
                                        rProduct["Quantity"] = 0;
                                        if (Convert.ToBoolean(row["IsInStock"]))
                                            rProduct["Quantity"] = row["InStockQuantity"];
                                        rProduct["Different"] = Convert.ToDecimal(rProduct["Quantity"]) - Convert.ToDecimal(rProduct["Quantity_Inventory"]);
                                        dtbInStockProduct.Rows.Add(rProduct);
                                    }
                                }
                                else
                                {
                                    DataRow rIMEI = dtbInStockProductIMEI.Rows.Find(new object[] { row["ProductID"].ToString(), row["IMEI"].ToString() });
                                    if (rIMEI == null && row["IMEI"].ToString() != strIMEICheck)
                                    {
                                        strIMEICheck = row["IMEI"].ToString();
                                        rIMEI = dtbInStockProductIMEI.NewRow();
                                        rIMEI["IsRequestIMEI"] = 1;
                                        rIMEI["CabinetNumber"] = row["CabinetNumber"];
                                        rIMEI["ProductID"] = row["ProductID"];
                                        rIMEI["ProductName"] = row["ProductName"];
                                        rIMEI["IMEI"] = (--intKeyIMEI);
                                        rIMEI["Col_IMEI"] = string.Empty;
                                        rIMEI["IMEI_Inventory"] = row["IMEI"];
                                        rIMEI["Quantity_Inventory"] = row["Quantity"];
                                        rIMEI["Quantity"] = 0;
                                        if (Convert.ToBoolean(row["IsInStock"]))
                                            rIMEI["Quantity"] = row["InStockQuantity"];
                                        rIMEI["Different"] = Convert.ToDecimal(rIMEI["Quantity"]) - Convert.ToDecimal(rIMEI["Quantity_Inventory"]);
                                        DataRow[] dr = dtbInStockProductIMEI.Select("IMEI_Inventory = '" + row["IMEI"] + "'");
                                        if (dr.Length == 0)
                                            dtbInStockProductIMEI.Rows.Add(rIMEI);
                                    }
                                }
                            }
                        }
                        if (dtbInStockProduct.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtbInStockProduct.Rows.Count; i++)
                            {
                                DataRow drProduct = dtbInStockProduct.Rows[i];
                                if (intSubGroupID > 0)
                                {
                                    if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(drProduct["PRODUCTID"].ToString().Trim()) == null)
                                        continue;
                                    else
                                    {
                                        if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(drProduct["PRODUCTID"].ToString().Trim()).SubGroupID != intSubGroupID)
                                            continue;
                                    }
                                }
                                if (!Convert.ToBoolean(drProduct["IsRequestIMEI"]))
                                {
                                    DataRow drData = dtbData.NewRow();
                                    drData.ItemArray = drProduct.ItemArray;
                                    drData["InventoryID"] = objInventory.InventoryID;
                                    if (drData["CabinetNumber"] == null || (Convert.IsDBNull(drData["CabinetNumber"])) || Convert.ToString(drData["CabinetNumber"]) == string.Empty)
                                    {
                                        drData["CabinetNumber"] = 1;
                                    }
                                    if (!Convert.IsDBNull(drData["Quantity"]))
                                    {
                                        drData["Col_IMEI"] = drData["IMEI"];
                                    }
                                    if (Convert.IsDBNull(drData["Different"]))
                                    {
                                        decimal decDifferent = Convert.ToDecimal(drData["Quantity"]);
                                        if (!Convert.IsDBNull(drData["Quantity_Inventory"]))
                                        {
                                            decDifferent = Convert.ToDecimal(drData["Quantity"]) - Convert.ToDecimal(drData["Quantity_Inventory"]);
                                        }
                                        drData["Different"] = decDifferent;
                                    }
                                    if (Convert.IsDBNull(drData["IMEI"]) || Convert.ToString(drData["IMEI"]).Trim() == string.Empty)
                                    {
                                        drData["IMEI"] = (--intKeyIMEI);
                                    }
                                    dtbData.Rows.Add(drData);
                                }
                            }
                        }
                        if (dtbInStockProductIMEI.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtbInStockProductIMEI.Rows.Count; i++)
                            {
                                DataRow drProductImei = dtbInStockProductIMEI.Rows[i];
                                intSubGroupID = objInventory.SubGroupID;
                                if (intSubGroupID > 0)
                                {
                                    if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(drProductImei["PRODUCTID"].ToString().Trim()) == null)
                                        continue;
                                    else
                                    {
                                        if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(drProductImei["PRODUCTID"].ToString().Trim()).SubGroupID != intSubGroupID)
                                            continue;
                                    }
                                }
                                DataRow drData = dtbData.NewRow();
                                drData.ItemArray = drProductImei.ItemArray;
                                drData["InventoryID"] = objInventory.InventoryID;
                                if (drData["CabinetNumber"] == null || Convert.IsDBNull(drData["CabinetNumber"]) || Convert.ToString(drData["CabinetNumber"]) == string.Empty)
                                {
                                    drData["CabinetNumber"] = 1;
                                }
                                if (!Convert.IsDBNull(drData["Quantity"]))
                                {
                                    drData["Col_IMEI"] = drData["IMEI"];
                                }
                                if (Convert.IsDBNull(drData["Different"]))
                                {
                                    decimal decDifferent = Convert.ToDecimal(drData["Quantity"]);
                                    if (!Convert.IsDBNull(drData["Quantity_Inventory"]))
                                    {
                                        decDifferent = Convert.ToDecimal(drData["Quantity"]) - Convert.ToDecimal(drData["Quantity_Inventory"]);
                                    }
                                    drData["Different"] = decDifferent;
                                }
                                dtbData.Rows.Add(drData);
                            }
                        }
                    }
                }
                else
                {
                    if (dtbInStockProduct.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtbInStockProduct.Rows.Count; i++)
                        {
                            DataRow drProduct = dtbInStockProduct.Rows[i];
                            if (intSubGroupID > 0)
                            {
                                if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(drProduct["PRODUCTID"].ToString().Trim()) == null)
                                    continue;
                                else
                                {
                                    if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(drProduct["PRODUCTID"].ToString().Trim()).SubGroupID != intSubGroupID)
                                        continue;
                                }
                            }
                            if (!Convert.ToBoolean(drProduct["IsRequestIMEI"]))
                            {
                                DataRow drData = dtbData.NewRow();
                                drData.ItemArray = drProduct.ItemArray;
                                drData["InventoryID"] = objInventory.InventoryID;
                                if (drData["CabinetNumber"] == null || (Convert.IsDBNull(drData["CabinetNumber"])) || Convert.ToString(drData["CabinetNumber"]) == string.Empty)
                                {
                                    drData["CabinetNumber"] = 1;
                                }
                                if (!Convert.IsDBNull(drData["Quantity"]))
                                {
                                    drData["Col_IMEI"] = drData["IMEI"];
                                }
                                if (Convert.IsDBNull(drData["Different"]))
                                {
                                    decimal decDifferent = Convert.ToDecimal(drData["Different"]);
                                    if (!Convert.IsDBNull(drData["Quantity_Inventory"]))
                                    {
                                        decDifferent = Convert.ToDecimal(drData["Quantity"]) - Convert.ToDecimal(drData["Quantity_Inventory"]);
                                    }
                                    drData["Different"] = decDifferent;
                                }
                                if (Convert.IsDBNull(drData["IMEI"]) || Convert.ToString(drData["IMEI"]).Trim() == string.Empty)
                                {
                                    drData["IMEI"] = (--intKeyIMEI);
                                }
                                dtbData.Rows.Add(drData);
                            }
                        }
                    }
                    if (dtbInStockProductIMEI.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtbInStockProductIMEI.Rows.Count; i++)
                        {
                            DataRow drProductImei = dtbInStockProductIMEI.Rows[i];
                            if (intSubGroupID > 0)
                            {
                                if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(drProductImei["PRODUCTID"].ToString().Trim()) == null)
                                    continue;
                                else
                                {
                                    if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(drProductImei["PRODUCTID"].ToString().Trim()).SubGroupID != intSubGroupID)
                                        continue;
                                }
                            }
                            DataRow drData = dtbData.NewRow();
                            drData.ItemArray = drProductImei.ItemArray;
                            drData["InventoryID"] = objInventory.InventoryID;
                            if (drData["CabinetNumber"] == null || Convert.IsDBNull(drData["CabinetNumber"]) || Convert.ToString(drData["CabinetNumber"]) == string.Empty)
                            {
                                drData["CabinetNumber"] = 1;
                            }
                            if (!Convert.IsDBNull(drData["Quantity"]))
                            {
                                drData["Col_IMEI"] = drData["IMEI"];
                            }
                            if (Convert.IsDBNull(drData["Different"]))
                            {
                                decimal decDifferent = Convert.ToDecimal(drData["Quantity"]);
                                if (!Convert.IsDBNull(drData["Quantity_Inventory"]))
                                {
                                    decDifferent = Convert.ToDecimal(drData["Quantity"]) - Convert.ToDecimal(drData["Quantity_Inventory"]);
                                }
                                drData["Different"] = decDifferent;
                            }
                            dtbData.Rows.Add(drData);
                        }
                    }
                }
                bolIsAddSumRow = true;
                if (bolIsAddSumRow)
                {
                    dtbData.PrimaryKey = null;
                    if (!bolIsFirstTime)
                    {
                        AddRowSumInFlexData.AddSumRowCustom(dtbData, "ProductName,Tổng cộng,ProductID,.,IMEI,.", "Quantity,Quantity_Inventory,Different", "");
                        intRowSum++;
                        foreach (DataRow row in dtbData.Rows)
                        {
                            if (row["PRODUCTID"].ToString() == "." && row["PRODUCTNAME"].ToString() == "Tổng cộng")
                            {
                                intQuantityOld += Convert.ToInt32(row["Quantity"]) * 2;
                                intQuantity_InventoryOld += Convert.ToInt32(row["Quantity_Inventory"]) * 2;
                                intDifferentOld += Convert.ToInt32(row["Different"]) * 2;
                            }
                        }
                        bolIsFirstTime = true;
                    }
                    else
                    {
                        if (intRowSum > 1)
                        {
                            intQuantityOld = 0;
                            intQuantity_InventoryOld = 0;
                            intDifferentOld = 0;
                            foreach (DataRow row in dtbData.Rows)
                            {
                                if (row["PRODUCTID"].ToString() == "." && row["PRODUCTNAME"].ToString() == "Tổng cộng")
                                {
                                    intQuantityOld += Convert.ToInt32(row["Quantity"]) * 2;
                                    intQuantity_InventoryOld += Convert.ToInt32(row["Quantity_Inventory"]) * 2;
                                    intDifferentOld += Convert.ToInt32(row["Different"]) * 2;
                                }
                            }
                        }
                        AddRowSumInFlexData.AddSumRowCustom(dtbData, "ProductName,Tổng cộng,ProductID,.,IMEI,.", "Quantity,Quantity_Inventory,Different", "");
                        intRowSum++;
                        for (int i = dtbData.Rows.Count - 1; i < dtbData.Rows.Count;i++ )
                        {
                            DataRow row = dtbData.Rows[i];
                            if (Convert.ToString(row["PRODUCTID"]) == "." && Convert.ToString(row["PRODUCTNAME"]) == "Tổng cộng")
                            {
                                intQuantityNew = Convert.ToInt32(row["Quantity"]);
                                intQuantity_InventoryNew = Convert.ToInt32(row["Quantity_Inventory"]);
                                intDifferentNew = Convert.ToInt32(row["Different"]);

                                row["Quantity"] = intQuantityNew - intQuantityOld;
                                row["Quantity_Inventory"] = intQuantity_InventoryNew - intQuantity_InventoryOld;
                                row["Different"] = intDifferentNew - intDifferentOld;
                            }
                        }
                    }
                }
            }
        }

        private void CustomFlex()
        {
            flexData.Rows.Fixed = 2;
            for (int i = 0; i < flexData.Cols.Count; i++)
            {
                flexData.Cols[i].Visible = false;
                flexData.Cols[i].AllowEditing = false;
            }
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexData, true, "InventoryID,CabinetNumber,ProductID,ProductName,Col_IMEI,Quantity,IMEI_Inventory,Quantity_Inventory,Different");
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowMerging(flexData, "InventoryID,CabinetNumber,ProductID,ProductName,Different", true);

            flexData[0, "InventoryID"] = "Mã phiếu kiểm kê";
            flexData[1, "InventoryID"] = "Mã phiếu kiểm kê";

            flexData[0, "CabinetNumber"] = "Tủ";
            flexData[1, "CabinetNumber"] = "Tủ";

            flexData[0, "ProductID"] = "Mã sản phẩm";
            flexData[1, "ProductID"] = "Mã sản phẩm";

            flexData[0, "ProductName"] = "Tên sản phẩm";
            flexData[1, "ProductName"] = "Tên sản phẩm";

            flexData[0, "Col_IMEI"] = "ERP";
            flexData[1, "Col_IMEI"] = "IMEI";

            flexData[0, "Quantity"] = "ERP";
            flexData[1, "Quantity"] = "Số lượng";

            flexData[0, "IMEI_Inventory"] = "Kiểm kê";
            flexData[1, "IMEI_Inventory"] = "IMEI";

            flexData[0, "Quantity_Inventory"] = "Kiểm kê";
            flexData[1, "Quantity_Inventory"] = "Số lượng";

            flexData[0, "Different"] = "Chênh lệch";
            flexData[1, "Different"] = "Chênh lệch";

            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnWidth(flexData,
                "InventoryID", 150,
                "CabinetNumber", 30,
                "ProductID", 120,
                "ProductName", 300,
                "Col_IMEI", 130,
                "Quantity", 70,
                "IMEI_Inventory", 130,
                "Quantity_Inventory", 70,
                "Different", 70);

            C1.Win.C1FlexGrid.CellStyle style = flexData.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexData.GetCellRange(0, 0, flexData.Rows.Fixed - 1, flexData.Cols.Count - 1);
            range.Style = style;
            flexData.Rows[0].AllowMerging = true;
            flexData.AllowEditing = false;
        }

        private void Review(string strReviewStatus, int intReviewStatus)
        {
            try
            {
                List<ERP.Inventory.PLC.Inventory.WSInventory.Inventory_ReviewList> lstReviewInventory = new List<ERP.Inventory.PLC.Inventory.WSInventory.Inventory_ReviewList>();
                List<bool> lstIsReviewedInventory = new List<bool>();
                string strReviewUser = string.Empty;
                string strFullName = string.Empty;
                string strTitle = intReviewStatus == 2 ? "XÁC NHẬN DUYỆT ĐỒNG Ý CHÊNH LỆCH KIỂM KÊ" : "XÁC NHẬN DUYỆT TỪ CHỐI CHÊNH LỆCH KIỂM KÊ";
                Library.Login.frmCertifyLogin frmLog = new Library.Login.frmCertifyLogin(strTitle, "XÁC NHẬN", null, null, true, false);
                frmLog.ShowDialog();
                if (!frmLog.IsCorrect)
                    return;
                strReviewUser = frmLog.UserName;
                if (strReviewUser != string.Empty)
                    strFullName = ERP.MasterData.SYS.PLC.PLCUser.LoadInfoFromCache(strReviewUser).FullName;
                List<string> lstCancel = new List<string>();
                int intCount = lstInventory.Count;
                foreach (ERP.Inventory.PLC.Inventory.WSInventory.Inventory objInventory in lstInventory)
                {
                    //ERP.Inventory.PLC.Inventory.WSInventory.Inventory obj = null;
                    int intReviewLevelID = -1;
                    int intReviewType = 0;
                    bool bolIsCheckStorePermission = false;
                    string strReviewFunctionID = string.Empty;
                    int intIndexRvl = -1;
                    //objPLCInventory.LoadInventoryInfo(ref obj, objInventory.InventoryID, ref dtbInventoryDetail);
                    objPLCInventoryTerm.LoadInfo(ref objInventoryTerm, objInventory.InventoryTermID);
                    objPLCInventoryType.LoadInfo(ref objInventoryType, objInventoryTerm.InventoryTypeID);
                    DataTable dtbInventoryRvwLevel = objPLCInventory.GetInventoryReviewLevel(objInventory.InventoryID);
                    DataTable dtbReviewLevel = null;
                    objPLCInventoryType.SearchDataReviewLevel(ref dtbReviewLevel, new object[] { "@InventoryTypeID", objInventoryTerm.InventoryTypeID });

                    int intInventoryCount = objPLCInventory.CountSimilarInventory(objInventoryTerm.InventoryTermID, objInventory.InventoryStoreID, objInventory.MainGroupID, objInventory.IsNew? 1 : 0, objInventory.SubGroupID);
                    if (intInventoryCount > 1)
                    {
                        MessageBox.Show(this, "Tồn tại " + intInventoryCount.ToString() + " phiếu kiểm kê cùng kho [" + Convert.ToString(objInventory.InventoryStoreID).Trim() + "].\nVui lòng nối các phiếu này lại với nhau trước khi duyệt", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    if (arrReviewUserList != null)
                    {
                        if (!arrReviewUserList.Contains(strReviewUser))
                        {
                            MessageBox.Show(this, "Người dùng [" + strReviewUser + "] không nằm trong danh sách duyệt. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    else
                    {
                        DataRow[] rUserReviewLevel = dtbInventoryRvwLevel.Select("TRIM(UserName) = '" + strReviewUser + "' AND ISREVIEWED = 0 AND ReviewLevelID = " + objInventory.CurrentReviewLevelID);

                        if (rUserReviewLevel.Length < 1 && strReviewUser != string.Empty)
                        {
                            //if (intCount > 1)
                            //{
                            //    if (MessageBox.Show(this, "Người dùng " + strReviewUser + " không nằm trong danh sách duyệt mức duyệt hiện tại của phiếu [" + objInventory.InventoryID + "]. Bạn có muốn bỏ qua phiếu này?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                            //    {
                            //        lstCancel.Add(objInventory.InventoryID);
                            //    }
                            //    else
                            //        return;
                            //}
                            //else
                            //{
                            MessageBox.Show(this, "Người dùng [" + strReviewUser + "] không nằm trong danh sách duyệt mức duyệt hiện tại của phiếu [" + objInventory.InventoryID + "].", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                            //}
                        }
                    }
                    int intTotalRun = 0;
                    bool bolIsHasDeny = false;
                    for (int i = 0; i < dtbInventoryRvwLevel.Rows.Count; i++)
                    {
                        if (Convert.ToString(dtbInventoryRvwLevel.Rows[i]["UserName"]).Trim() == strReviewUser && Convert.ToInt32(dtbInventoryRvwLevel.Rows[i]["ReviewLevelID"]) == objInventory.CurrentReviewLevelID && Convert.ToInt32(dtbInventoryRvwLevel.Rows[i]["ReviewStatus"]) == 3)
                        {
                            bolIsHasDeny = true;
                            break;
                        }
                        if (Convert.ToString(dtbInventoryRvwLevel.Rows[i]["UserName"]).Trim() == strReviewUser && Convert.ToInt32(dtbInventoryRvwLevel.Rows[i]["ReviewLevelID"]) == objInventory.CurrentReviewLevelID)
                        {
                            if (Convert.ToBoolean(dtbInventoryRvwLevel.Rows[i]["IsReviewed"]))
                            {
                                continue;
                            }
                            intReviewLevelID = Convert.ToInt32(dtbInventoryRvwLevel.Rows[i]["ReviewLevelID"]);
                            intIndexRvl = i;
                            break;
                        }
                        intTotalRun = i;
                    }
                    if(bolIsHasDeny)
                    {
                        MessageBox.Show(this, "Người dùng [" + strReviewUser + "] đã từ chối duyệt phiếu [" + objInventory.InventoryID + "]. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (intReviewLevelID == -1)
                    {
                        MessageBox.Show(this, "Người dùng [" + strReviewUser + "] đã duyệt phiếu [" +objInventory.InventoryID +"]. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    for (int i = 0; i < dtbReviewLevel.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]) == intReviewLevelID)
                        {
                            if (i > 0)
                            {
                                DataRow[] rIsDenyUserRvwLvl = dtbInventoryRvwLevel.Select("ReviewStatus = 3");
                                if (rIsDenyUserRvwLvl.Length > 0)
                                {
                                    MessageBox.Show(this, "Phiếu kiểm kê [" + objInventory.InventoryID + "] đã bị duyệt từ chối. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }

                                DataRow[] rIsReviewedUserRvwLvl = dtbInventoryRvwLevel.Select("IsReviewed = 1 and ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i - 1]["ReviewLevelID"]));
                                if (rIsReviewedUserRvwLvl.Length < 1)
                                {
                                    //if (intCount > 1)
                                    //{
                                    //    if (MessageBox.Show(this, "Mức duyệt " + Convert.ToString(dtbReviewLevel.Rows[i - 1]["ReviewLevelName"]).Trim() + " của phiếu [" + objInventory.InventoryID + "] chưa được duyệt. Bạn có muốn bỏ qua phiếu này?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                                    //    {
                                    //        lstCancel.Add(objInventory.InventoryID);
                                    //    }

                                    //    else
                                    //        return;
                                    //}
                                    //else
                                    //{
                                    MessageBox.Show(this, "Mức duyệt [" + Convert.ToString(dtbReviewLevel.Rows[i - 1]["ReviewLevelName"]).Trim() + "] của phiếu [" + objInventory.InventoryID + "] chưa được duyệt.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                    //}
                                }
                                else
                                {
                                    if (Convert.ToInt32(dtbReviewLevel.Rows[i - 1]["ReviewType"]) == 0)
                                    {
                                        if (rIsReviewedUserRvwLvl.Length < (dtbInventoryRvwLevel.Select("ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i - 1]["ReviewLevelID"])).Length))
                                        {
                                            //if (intCount > 1)
                                            //{
                                            //    if (MessageBox.Show(this, "Mức duyệt " + Convert.ToString(dtbReviewLevel.Rows[i - 1]["ReviewLevelName"]).Trim() + " của phiếu [" + objInventory.InventoryID + "]  phải được tất cả duyệt. Bạn có muốn bỏ qua phiếu này?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                                            //    {
                                            //        lstCancel.Add(objInventory.InventoryID);
                                            //    }
                                            //    else
                                            //        return;
                                            //}
                                            //else
                                            //{
                                            MessageBox.Show(this, "Mức duyệt " + Convert.ToString(dtbReviewLevel.Rows[i - 1]["ReviewLevelName"]).Trim() + " của phiếu [" + objInventory.InventoryID + "]  phải được tất cả duyệt.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return;
                                            //}
                                        }
                                    }
                                }
                            }

                            intReviewType = Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewType"]);
                            if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["ReviewFunctionID"]))
                            {
                                strReviewFunctionID = Convert.ToString(dtbReviewLevel.Rows[i]["ReviewFunctionID"]).Trim();
                            }
                            if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCheckStorePermission"]))
                            {
                                bolIsCheckStorePermission = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCheckStorePermission"]);
                            }
                            break;
                        }
                    }

                    ERP.MasterData.SYS.PLC.PLCUser objPLCUser = new MasterData.SYS.PLC.PLCUser();
                    if (bolIsCheckStorePermission)
                    {
                        DataTable dtbStorePermission = objPLCUser.GetDataUser_Store(strReviewUser);
                        DataRow[] rStorePermission = dtbStorePermission.Select("IsSelect > 0 AND StoreID = " + Convert.ToInt32(objInventory.InventoryStoreID));
                        if (rStorePermission.Length < 1)
                        {
                            //if (intCount > 1)
                            //{
                            //    if (MessageBox.Show(this, "Mã nhân viên " + strReviewUser + " không có quyền trên kho " + Convert.ToString(objInventory.InventoryStoreID).Trim() + " phiếu [" + objInventory.InventoryID + "]. Bạn có muốn bỏ qua phiếu này?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                            //    {
                            //        lstCancel.Add(objInventory.InventoryID);
                            //    }
                            //    else
                            //        return;
                            //}
                            //else
                            //{
                            string strStoreName = string.Empty;
                            MasterData.PLC.MD.WSStore.Store objStore = new MasterData.PLC.MD.WSStore.Store();
                            new MasterData.PLC.MD.PLCStore().LoadInfo(ref objStore, objInventory.InventoryStoreID);
                            if (objStore != null)
                                strStoreName = objStore.StoreName;
                            MessageBox.Show(this, "Mã nhân viên " + strReviewUser + " không có quyền trên kho [" + Convert.ToString(objInventory.InventoryStoreID).Trim() + "-" + strStoreName + "], phiếu kiểm kê [" + objInventory.InventoryID + "].", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                            //}
                        }
                    }
                    //}
                    //List<string> lstFinalReview = new List<string>();
                    //if (lstCancel.Count > 0)
                    //{
                    //    lstFinalReview = lstInventory.Except(lstCancel).ToList();
                    //}
                    //else
                    //    lstFinalReview = lstInventory;
                    //if (lstFinalReview.Count == 0)
                    //    return;
                    //foreach (string objInventory.InventoryIDFinal in lstFinalReview)
                    //{
                    //    objInventory = null;
                    //    int intReviewLevelIDFinal = 0;
                    //    int intReviewTypeFinal = 0;
                    //    string strReviewFunctionIDFinal = string.Empty;
                    //    int intIndexRvlFinal = -1;
                    //    objPLCInventory.LoadInventoryInfo(ref objInventory, objInventory.InventoryIDFinal, ref dtbInventoryDetail);
                    //    objPLCInventoryTerm.LoadInfo(ref objInventoryTerm, objInventory.InventoryTermID);
                    //    objPLCInventoryType.LoadInfo(ref objInventoryType, objInventoryTerm.InventoryTypeID);
                    //    DataTable dtbInventoryRvwLevelFinal = objPLCInventory.GetInventoryReviewLevel(objInventory.InventoryIDFinal);
                    //    DataTable dtbReviewLevelFinal = null;
                    //    objPLCInventoryType.SearchDataReviewLevel(ref dtbReviewLevelFinal, new object[] { "@InventoryTypeID", objInventory.InventoryStoreID });

                    //for (int i = 0; i < dtbInventoryRvwLevel.Rows.Count; i++)
                    //{
                    //    if (Convert.ToString(dtbInventoryRvwLevel.Rows[i]["UserName"]).Trim() == strReviewUser)
                    //    {
                    //        if (Convert.ToBoolean(dtbInventoryRvwLevel.Rows[i]["IsReviewed"]))
                    //        {
                    //            continue;
                    //        }
                    //        intReviewLevelID = Convert.ToInt32(dtbInventoryRvwLevel.Rows[i]["ReviewLevelID"]);
                    //        intIndexRvl = i;
                    //        break;
                    //    }
                    //}

                    DataTable dtbMainGroupSource = null;
                    ERP.MasterData.PLC.MD.PLCMainGroup objPLCMainGroup = new MasterData.PLC.MD.PLCMainGroup();
                    objPLCMainGroup.GetMainGroup(ref dtbMainGroupSource, strReviewUser);
                    DataRow[] rMainGroupPermission = dtbMainGroupSource.Select("HASRIGHT = 1 AND MAINGROUPID = " + objInventory.MainGroupID);
                    if (rMainGroupPermission.Length < 1)
                    {
                        MessageBox.Show(this, "Mã nhân viên kiểm kê [" + strReviewUser + "] không có quyền trên ngành hàng [" + objInventory.MainGroupID + "]. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    ERP.Inventory.PLC.Inventory.WSInventory.Inventory_ReviewList objInventoryReview = new PLC.Inventory.WSInventory.Inventory_ReviewList();
                    objInventoryReview.InventoryID = objInventory.InventoryID;
                    objInventoryReview.ReviewLevelID = intReviewLevelID;
                    objInventoryReview.UserName = strReviewUser;
                    objInventoryReview.ReviewStatus = intReviewStatus;
                    objInventoryReview.InventoryDate = objInventory.InventoryDate;
                    objInventoryReview.IsReviewed = true;
                    objInventoryReview.ReviewedDate = Library.AppCore.Globals.GetServerDateTime();

                    bool bolIsReviewedInventory = false;
                    if (intReviewLevelID == Convert.ToInt32(dtbInventoryRvwLevel.Rows[dtbInventoryRvwLevel.Rows.Count - 1]["ReviewLevelID"]))
                    {
                        if (intReviewType == 0)
                        {
                            DataRow[] rUserRvwLvl = dtbInventoryRvwLevel.Select("ReviewLevelID = " + intReviewLevelID);
                            DataRow[] rReviewed = dtbInventoryRvwLevel.Select("IsReviewed = 1 AND ReviewLevelID = " + intReviewLevelID);
                            if (objInventoryReview.ReviewStatus == 2)
                            {
                                if (rReviewed.Length == (rUserRvwLvl.Length - 1))
                                {
                                    bolIsReviewedInventory = true;
                                }
                            }
                        }
                        else
                        {
                            //DataRow[] rReviewed = dtbInventoryRvwLevel.Select("IsReviewed = 1 AND ReviewLevelID = " + intReviewLevelID);
                            //if (rReviewed.Length > 0)
                            if (objInventoryReview.ReviewStatus == 2)
                            {
                                bolIsReviewedInventory = true;
                            }
                            else
                            {
                                if (dtbInventoryRvwLevel.Select("ReviewLevelID = " + intReviewLevelID).Length == 1)
                                {
                                    bolIsReviewedInventory = true;
                                }
                            }
                        }
                    }
                    lstReviewInventory.Add(objInventoryReview);
                    lstIsReviewedInventory.Add(bolIsReviewedInventory);
                }

                //    objPLCInventory.UpdateInventoryReviewStatus(objInventoryReview, bolIsReviewedInventory);
                //    lstReviewInventory.Add(objInventoryReview);
                //}
                //foreach(var objInventoryReview in lstInventory)
                //    if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                //    {
                //        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                //        return;
                //    }
                //    if (intIndexRvl > -1 && intIndexRvl < dtbInventoryRvwLevel.Rows.Count)
                //    {
                //        dtbInventoryRvwLevel.Rows[intIndexRvl]["ReviewStatus"] = objInventoryReview.ReviewStatus;
                //        dtbInventoryRvwLevel.Rows[intIndexRvl]["StatusName"] = strReviewStatus;
                //        if (objInventoryReview.ReviewStatus == 2)
                //        {
                //            dtbInventoryRvwLevel.Rows[intIndexRvl]["IsReviewed"] = true;
                //            dtbInventoryRvwLevel.Rows[intIndexRvl]["ReviewedDate"] = objInventoryReview.ReviewedDate;
                //        }
                //        else
                //        {
                //            dtbInventoryRvwLevel.Rows[intIndexRvl]["IsReviewed"] = false;
                //            dtbInventoryRvwLevel.Rows[intIndexRvl]["ReviewedDate"] = DBNull.Value;
                //        }
                //        dtbInventoryRvwLevel.Rows[intIndexRvl]["UserName"] = strReviewUser;
                //        dtbInventoryRvwLevel.Rows[intIndexRvl]["FullName"] = strFullName;
                //    }
                //}

                //Duyệt danh sách phiếu
                objPLCInventory.UpdateInventoryReviewList(lstReviewInventory, lstIsReviewedInventory);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    MessageBox.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    SystemErrorWS.Insert("Lỗi cập nhật trạng thái duyệt!", SystemConfig.objSessionUser.ResultMessageApp.Message);
                    return;
                }
                MessageBox.Show(this, "Cập nhật trạng thái duyệt phiếu kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                bolIsUpdateReviewStatus = true;
                this.Close();
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi cập nhật trạng thái duyệt!", objExce, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi cập nhật trạng thái duyệt", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void mnuItemReview_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Review("Đồng ý", 2);
        }

        private void mnuItemDenied_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Review("Từ chối", 3);
        }

        private void chkOnlyUnEvent_CheckedChanged(object sender, EventArgs e)
        {
            lblSum.Text = "0";
            DataTable tblTemp = null;
            if (chkOnlyUnEvent.Checked)
            {
                tblTemp = Library.AppCore.DataTableClass.Select(dtbData, "Different <> 0");
                tblTemp = Library.AppCore.Globals.DataTableSelect(tblTemp, string.Empty);
            }
            else
            {
                tblTemp = dtbData.Copy();
                tblTemp.PrimaryKey = null;
            }
            flexData.DataSource = tblTemp;
            intSumProd = tblTemp.Rows.Count - intRowSum;
            lblSum.Text = intSumProd.ToString("#,###");
            CustomFlex();
        }

        private void mnuItemExcelExport_Click(object sender, EventArgs e)
        {
            Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexData);
        }
    }

    public static class AddRowSumInFlexData
    {
        public static void AddSumRowCustom(DataTable dtbSourceTable, String strValueColumnList, String strSumColumnList, String strFilterExp)
        {
            DataRow objRow = dtbSourceTable.NewRow();
            String[] arrValueColumnList = strValueColumnList.Split(',');
            for (int i = 0; i < arrValueColumnList.Length / 2; i++)
                objRow[arrValueColumnList[i * 2].Trim()] = arrValueColumnList[i * 2 + 1].Trim();

            String[] arrSumColumnList = strSumColumnList.Split(',');
            for (int i = 0; i < arrSumColumnList.Length; i++)
                objRow[arrSumColumnList[i].Trim()] = Library.AppCore.DataTableClass.GetComputeValue(dtbSourceTable, "sum(" + arrSumColumnList[i].Trim() + ")", strFilterExp, 0);

            dtbSourceTable.Rows.Add(objRow);
        }
    }
}
