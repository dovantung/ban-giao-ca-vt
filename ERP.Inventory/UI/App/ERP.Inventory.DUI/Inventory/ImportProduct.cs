﻿using ERP.MasterData.DUI.CustomControl.ImportExcel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace ERP.Inventory.DUI.Inventory
{
    class ImportProduct : IExcelImport
    {
        private ClsImportExcel objClsImportExcel;
        public DataTable dtbProduct = null;
        private DataTable dtbProductCache = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
        private IWin32Window hwdOwner = null;
        public IWin32Window HwdOwner { get; set; }
        public ImportProduct()
        {
            objClsImportExcel = new ClsImportExcel(this,true);
            CreateColumnImport();
        }
        public void CreateColumnImport()
        {
            objClsImportExcel.InitializeColumnGridView(
             new DataColumnInfor("PRODUCTID", "Mã sản phẩm", typeof(string), 160, true, 1, DataColumnInfor.TypeShow.ALL),
             new DataColumnInfor("PRODUCTNAME", "Tên sản phẩm", typeof(string), 250, true, 2, DataColumnInfor.TypeShow.ALL),
             new DataColumnInfor("QUANTITY", "Số lượng thực tế", typeof(decimal), 125, true, 3, DataColumnInfor.TypeShow.ALL),
             new DataColumnInfor("NOTE1", "Ghi chú", typeof(string), 250, true, 4, DataColumnInfor.TypeShow.ALL)
             );
        }
        public string IExcelImport_FormTitle
        {
            get
            {
                return "Import phiếu kiểm kê nhanh";
            }
        }

        public List<object> IExcelImport_ReturnDataImport(DataTable dtbData, DataRow drExcelFile, out bool IsError, out string strMessenger)
        {
            IsError = false;
            strMessenger = string.Empty;
            List<object> objReturn;

            string strProductId = string.Empty;
            string strProductName = string.Empty;
            string strQUANTITY = string.Empty;
            string strNOTE = string.Empty;
            decimal decQuantity = 0;
            try
            {
                strProductId = (drExcelFile[0] ?? "").ToString().Trim();
                strProductName = (drExcelFile[1] ?? "").ToString().Trim();
                strQUANTITY = (drExcelFile[2] ?? "0").ToString().Trim();
                strNOTE = (drExcelFile[3] ?? "").ToString().Trim();
                if (dtbProduct != null && dtbProduct.Rows.Count > 0)
                {
                    var objProduct = dtbProduct.Select("PRODUCTID='" + strProductId + "'");
                    if (objProduct == null || objProduct.Count() == 0)
                    {
                        throw (new Exception("Mã sản phẩm không tồn tại trong danh sách!"));
                    }
                    else
                    {
                        var objProductCache = dtbProductCache.Select("PRODUCTID='" + strProductId + "'");
                        if (objProductCache != null && objProductCache.Count() > 0)
                        {
                            strProductName = objProductCache[0]["PRODUCTNAME"].ToString().Trim();
                        }
                        decimal.TryParse(strQUANTITY, out decQuantity);
                        if (decQuantity < 0)
                        {
                            throw (new Exception("Số lượng thực tế phải lớn hơn hoặc bằng 0!"));
                        }
                        if(objProduct[0]["ISALLOWDECIMAL"].ToString() == "0")
                        {
                            decQuantity = Math.Round(decQuantity);
                        }
                        //decimal decQuantityInStock = 0;
                        //decimal.TryParse(objProduct[0]["INSTOCKQUANTITY"].ToString(), out decQuantityInStock);
                        //if (decQuantityInStock < decQuantity)
                        //{
                        //    throw (new Exception("Số lượng thực tế phải nhỏ hơn hoặc bằng số lượng tồn kho!"));
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                IsError = true;
                if (string.IsNullOrEmpty(strMessenger))
                    strMessenger = ex.Message;
                else strMessenger += ", " + ex.Message;
            }
            finally
            {
                objReturn = new List<object>() { strProductId, strProductName, decQuantity, strNOTE };
            }
            return objReturn;
        }

        public void Import(ref DataTable dtbSimIssue)
        {
            if (objClsImportExcel.DoImportExcel())
            {
                if (objClsImportExcel.dtbReturn != null)
                {
                    dtbSimIssue = objClsImportExcel.dtbReturn;
                    MessageBox.Show(hwdOwner, "Import kiểm kê thực tế thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

    }
}
