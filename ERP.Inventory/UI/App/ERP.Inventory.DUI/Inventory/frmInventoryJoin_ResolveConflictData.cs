﻿using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmInventoryJoin_ResolveConflictData : Form
    {
        public int data_inventoryTermID = 0;
        public int data_storeID = 0;
        public DataTable dtbConflictData { get; set; }
        DataTable dtbProductStatus { get; set; }
        public frmInventoryJoin_ResolveConflictData(DataTable _dtbConflictData, int _inventoryTermID, int _storeID)
        {
            InitializeComponent();
            dtbConflictData = _dtbConflictData;
            data_inventoryTermID = _inventoryTermID;
            data_storeID = _storeID;
            this.DialogResult = DialogResult.Cancel;
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(viewData);
        }
        private void frmInventoryJoin_ResolveConflictData_Load(object sender, EventArgs e)
        {
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Location = new Point((Screen.PrimaryScreen.WorkingArea.Width / 2) - (this.Width / 2), 0);
            if (dtbConflictData == null || dtbConflictData.Rows.Count == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không có dữ liệu trùng để xử lý!");
                this.Close();
            }
            else
            {
                if (!dtbConflictData.Columns.Contains("SELECTED"))
                {
                    dtbConflictData.Columns.Add(new DataColumn() { ColumnName = "SELECTED", DataType = typeof(bool), DefaultValue = false });
                }
                grdData.DataSource = dtbConflictData;
            }
            dtbProductStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView();
        }
        private void viewData_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

        }
        private void viewData_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.RowHandle >= 0 && e.Column.FieldName == "SELECTED")
            {
                DataRow r = viewData.GetDataRow(e.RowHandle);
                string productID = r["PRODUCTID"].ToString();
                bool selected = Convert.ToBoolean(e.Value);
                r["SELECTED"] = selected;
                bool isRequestImei = Convert.ToBoolean(r["ISREQUESTIMEI"]);
                string imei = r["IMEI"].ToString();
                string inventoryID = r["INVENTORYID"].ToString();
                int productStatusID = Convert.ToInt32(r["PRODUCTSTATUSID"]);
                string inventoryDetailID = r["INVENTORYDETAILID"].ToString();
                if (selected)
                {
                    if (isRequestImei)
                    {
                        ((DataTable)grdData.DataSource).AsEnumerable().Where(row =>
                        Convert.ToString(row["IMEI"]).Trim() == imei.Trim()
                        && Convert.ToString(row["INVENTORYDETAILID"]).Trim() != inventoryDetailID.Trim())
                        .ToList().ForEach(row => row["SELECTED"] = false);

                    }
                    //else
                    //{
                    //    ((DataTable)grdData.DataSource).AsEnumerable().Where(row =>
                    //    Convert.ToString(row["PRODUCTID"]).Trim() == productID.Trim()
                    //    && Convert.ToInt32(row["PRODUCTSTATUSID"]) == productStatusID
                    //    && Convert.ToString(row["INVENTORYDETAILID"]).Trim() != inventoryDetailID.Trim()
                    //     )
                    //     .ToList().ForEach(row => row["SELECTED"] = false);
                    //}

                }
            }
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                btnUpdate.Enabled = false;
                if (IsValidForUpdate())
                {
                    if (MessageBox.Show("Xác nhận những dữ liệu đã chọn?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (!dtbConflictData.AsEnumerable().Where(d => !Convert.ToBoolean(d["SELECTED"])).ToList().Any())
                        {
                            this.DialogResult = DialogResult.OK;
                            this.Close();
                            return;
                        }
                        else
                        {
                            DataTable dtbResolve = dtbConflictData.AsEnumerable().Where(d => !Convert.ToBoolean(d["SELECTED"])).ToList().CopyToDataTable();
                            if (dtbResolve != null && dtbResolve.Rows.Count > 0)
                            {
                                if (new PLC.Inventory.PLCInventory().ResolveInventoryConflict(dtbResolve))
                                {
                                    this.DialogResult = DialogResult.OK;
                                    this.Close();
                                }
                                else
                                {
                                    MessageBoxObject.ShowWarningMessage(this, "Cập nhật dữ liệu trùng thất bại!");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Library.AppCore.SystemErrorWS.Insert("Lỗi xử lý dữ liệu conflict - Kiểm kê", exception, Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Lỗi cập nhật xử lý dữ liệu trùng!");
            }
            finally
            {
                btnUpdate.Enabled = true;
            }
        }
        private bool IsValidForUpdate()
        {
            List<string> imeiList = ((DataTable)grdData.DataSource)
                .AsEnumerable().Where(row => Convert.ToBoolean(row["ISREQUESTIMEI"]))
                .Select(row => Convert.ToString(row["IMEI"]).Trim()).Distinct().ToList();
            foreach (string imei in imeiList)
            {
                if (((DataTable)grdData.DataSource).AsEnumerable()
                .Where(d => Convert.ToString(d["IMEI"]).Trim() == imei)
                .All(d => Convert.ToBoolean(d["SELECTED"]) == false))
                {
                    MessageBoxObject.ShowWarningMessage(this, "IMEI " + imei.Trim() + " chưa được chọn xử lý!");
                    return false;
                }
            }
            List<string> productList = ((DataTable)grdData.DataSource)
               .AsEnumerable().Where(row => !Convert.ToBoolean(row["ISREQUESTIMEI"]))
               .Select(row => Convert.ToString(row["PRODUCTID"]).Trim()).Distinct().ToList();

            List<int> productStatusIDList = ((DataTable)grdData.DataSource).AsEnumerable().Where(d => productList.Contains(Convert.ToString(d["PRODUCTID"]).Trim()))
                .Select(d => Convert.ToInt32(d["PRODUCTSTATUSID"])).Distinct().ToList();

            foreach (int productStatusID in productStatusIDList)
            {
                string productStatusName = dtbProductStatus.AsEnumerable().Where(d => Convert.ToInt32(d["PRODUCTSTATUSID"]) == productStatusID)
                    .Select(d => Convert.ToString(d["PRODUCTSTATUSNAME"])).FirstOrDefault();
                foreach (string productId in productList)
                {
                    List<DataRow> rows = ((DataTable)grdData.DataSource).AsEnumerable()
                   .Where(d => Convert.ToString(d["PRODUCTID"]).Trim() == productId.Trim()
                   && Convert.ToInt32(d["PRODUCTSTATUSID"]) == productStatusID).ToList();

                    if (rows.Any())
                    {
                        if (!rows.Where(r=> Convert.ToBoolean(r["SELECTED"])).Any())
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Mã sản phẩm " + productId + " [" + productStatusName + "] chưa được chọn xử lý!");
                            return false;
                        }
                        //if (rows.All(d => Convert.ToBoolean(d["SELECTED"])) == false)
                        //{
                        //    MessageBoxObject.ShowWarningMessage(this, "Mã sản phẩm " + productId + " [" + productStatusName + "] chưa được chọn xử lý!");
                        //    return false;
                        //}
                    }
                }
            }

            DataTable dtbIsParent = new Report.PLC.PLCReportDataSource().GetDataSource("INV_INVENTORY_IS_PARENT", new object[] { "@INVENTORYTERMID", data_inventoryTermID, "@STOREID", data_storeID });
            if (dtbIsParent != null && dtbIsParent.Rows.Count > 0)
            {
                MessageBox.Show(this, "Các phiếu kiểm kê trong kỳ của kho này đã được nối hết!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }
    }
}
