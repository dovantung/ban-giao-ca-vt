﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmInventoryOnWayReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label10 = new System.Windows.Forms.Label();
            this.cboStoreIDList = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cboReportType = new System.Windows.Forms.ComboBox();
            this.cboBranch = new Library.AppControl.ComboBoxControl.ComboBoxBranch();
            this.chkIsInput = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(323, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 16);
            this.label10.TabIndex = 67;
            this.label10.Text = "Kho kiểm kê:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboStoreIDList
            // 
            this.cboStoreIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreIDList.Location = new System.Drawing.Point(406, 40);
            this.cboStoreIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreIDList.Name = "cboStoreIDList";
            this.cboStoreIDList.Size = new System.Drawing.Size(510, 24);
            this.cboStoreIDList.TabIndex = 59;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(68, 12);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(119, 22);
            this.dtpFromDate.TabIndex = 56;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 15);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 16);
            this.label12.TabIndex = 69;
            this.label12.Text = "Ngày:";
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportExcel.Location = new System.Drawing.Point(802, 67);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(114, 25);
            this.btnExportExcel.TabIndex = 64;
            this.btnExportExcel.Text = "     Xuất Excel";
            this.btnExportExcel.UseVisualStyleBackColor = true;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(188, 12);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(119, 22);
            this.dtpToDate.TabIndex = 57;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(323, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 16);
            this.label1.TabIndex = 71;
            this.label1.Text = "Chi nhánh:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 16);
            this.label2.TabIndex = 66;
            this.label2.Text = "Mẫu in:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboReportType
            // 
            this.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReportType.FormattingEnabled = true;
            this.cboReportType.Items.AddRange(new object[] {
            "Mẫu 01D/KKHH",
            "Mẫu tổng hợp 01A/KKHH"});
            this.cboReportType.Location = new System.Drawing.Point(68, 40);
            this.cboReportType.Name = "cboReportType";
            this.cboReportType.Size = new System.Drawing.Size(239, 24);
            this.cboReportType.TabIndex = 60;
            this.cboReportType.SelectedIndexChanged += new System.EventHandler(this.cboReportType_SelectedIndexChanged);
            // 
            // cboBranch
            // 
            this.cboBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBranch.Location = new System.Drawing.Point(406, 12);
            this.cboBranch.Margin = new System.Windows.Forms.Padding(0);
            this.cboBranch.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.Size = new System.Drawing.Size(510, 24);
            this.cboBranch.TabIndex = 72;
            this.cboBranch.SelectionChangeCommitted += new System.EventHandler(this.cboBranch_SelectionChangeCommitted);
            // 
            // chkIsInput
            // 
            this.chkIsInput.AutoSize = true;
            this.chkIsInput.Location = new System.Drawing.Point(624, 70);
            this.chkIsInput.Name = "chkIsInput";
            this.chkIsInput.Size = new System.Drawing.Size(172, 20);
            this.chkIsInput.TabIndex = 73;
            this.chkIsInput.Text = "Kho kiểm kê là kho nhập";
            this.chkIsInput.UseVisualStyleBackColor = true;
            // 
            // frmInventoryOnWayReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(953, 510);
            this.Controls.Add(this.chkIsInput);
            this.Controls.Add(this.cboBranch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboStoreIDList);
            this.Controls.Add(this.dtpToDate);
            this.Controls.Add(this.dtpFromDate);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cboReportType);
            this.Controls.Add(this.btnExportExcel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label10);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(969, 549);
            this.Name = "frmInventoryOnWayReport";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý chốt tồn kiểm kê theo kỳ";
            this.Load += new System.EventHandler(this.frmInventoryTerm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStoreIDList;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnExportExcel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboReportType;
        private Library.AppControl.ComboBoxControl.ComboBoxBranch cboBranch;
        private System.Windows.Forms.CheckBox chkIsInput;
    }
}