﻿using DevExpress.XtraGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmShowUnEventVTS : Form
    {
        public frmShowUnEventVTS()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        private void frmShowUnEventVTS_Load(object sender, EventArgs e)
        {
          
            DataTable tblTemp = tblUnEventTable.Copy();
            tblTemp.PrimaryKey = null;
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewProducts);
            DataTable dtbData = objPLCInventory.SearchDataCauseGroups();
            repCauseGroups.DataSource = dtbData;
            //Library.AppCore.DataTableClass.AddSumRow(tblTemp, "PRODUCTNAME,Tổng cộng,PRODUCTID,.,ERP_IMEI,.", "ERP_QUANTITY,INV_QUANTITY,DIFFERENT");

            grdCtlProducts.DataSource = tblTemp;
           // CustomFlex();

            btnSave.Enabled = false;

            if (intIsPermitUpdateUnEvent == 0)
            {
                btnSave.Enabled = false;
                btnViewReport.Enabled = false;
                return;
            }

            if (bolIsReviewed && (!bolIsUpdateUnEvent))
            {
                btnSave.Enabled = true;
            }
            if (bolIsUpdateUnEvent)
            {
                btnViewReport.Enabled = true;
            }

            if (bolIsDeleted)
            {
                btnSave.Enabled = false;
            }
        }

        public string strInventoryID = string.Empty;
        public DateTime dtmInventoryDate;
        public string strStoreName = string.Empty;
        public string strMainGroupName = string.Empty;
        public int intProductInstockID = 0;
        public int intCreatedStoreID;
        public bool bolIsReviewed = false;
        public bool bolIsUpdateUnEvent = false;
        public bool bolIsDeleted = false;
        public bool bolIsSaveInventory = false;
        public DataTable tblUnEventTable = null;
        private ERP.Inventory.PLC.Inventory.PLCInventory objPLCInventory = new PLC.Inventory.PLCInventory();
        private bool bolIsSaveChanged_IsUpdateUnEvent = false;
        public bool bolIsSave = false;
        public int intIsPermitUpdateUnEvent = -1;

       

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!bolIsSaveInventory)
            {
                MessageBox.Show(this, "Vui lòng cập nhật phiếu kiểm kê trước, sau đó cập nhật chênh lệch kiểm kê!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            bolIsSave = true;
            btnSave.Enabled = false;
            grdViewProducts.CloseEditor();
            grdViewProducts.UpdateCurrentRow();
            ERP.Inventory.PLC.Inventory.WSInventory.Inventory objInventory = new PLC.Inventory.WSInventory.Inventory();
            objInventory.InventoryID = strInventoryID;
            objInventory.InventoryDate = dtmInventoryDate;
            objInventory.CreatedStoreID = intCreatedStoreID;
            tblUnEventTable.TableName = "UNEVENTTABLE";
            objPLCInventory.AddInventoryUnEvent(objInventory, Library.AppCore.DataTableClass.Select(tblUnEventTable, "Different <> 0 OR ERP_PRODUCTSTATUSID <> INV_PRODUCTSTATUSID"));
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                btnSave.Enabled = true;
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }

            bolIsUpdateUnEvent = true;
            btnViewReport.Enabled = true;

            MessageBox.Show(this, "Cập nhật chênh lệch kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            object[] objKeywords;
            objKeywords = new object[] { "@InventoryID", strInventoryID };

            //DataTable dtbPrint = new PLC.Inventory.PLCInventoryProcess().GetDataSourceByStoreName(objKeywords, "INV_INVENTORY_UNEVENT_ListT");

            // LE VĂN ĐÔNG: 12/10/2017 -> Lấy dữ liệu trực tiếp từ lưới

            DataTable dtbSource = (DataTable)grdCtlProducts.DataSource;
            DataTable dtbPrint = null;
            if (dtbSource != null)
            {
                dtbPrint = dtbSource.Copy();
                if (!dtbPrint.Columns.Contains("ISSUMROW"))
                {
                    dtbPrint.Columns.Add("ISSUMROW", typeof(Int32));
                    dtbPrint.AsEnumerable().ToList<DataRow>().ForEach(r => { r["ISSUMROW"] = 0; });
                    //foreach (DataRow row in dtbPrint.Rows)
                    //    row["ISSUMROW"] = 0;
                }
                dtbPrint.Rows.RemoveAt(dtbPrint.Rows.Count - 1);
            }
            // >.

            ERP.MasterData.PLC.MD.WSReport.Report objReport = new MasterData.PLC.MD.WSReport.Report();
            objReport.DataTableSource = dtbPrint;
            objReport.PaperOrientation = true;
            objReport.ReportPath = "Reports\\Inventory\\Inventory\\rptViewUnEventReport.rpt";
            System.Collections.Hashtable htParams = new System.Collections.Hashtable();
            htParams.Add("@InventoryDate", dtmInventoryDate);
            htParams.Add("@StoreName", strStoreName);
            htParams.Add("@MainGroupName", strMainGroupName);
            htParams.Add("@ProductInstockID", intProductInstockID);
            htParams.Add("@CompanyName", Library.AppCore.SystemConfig.DefaultCompany.CompanyName);
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htParams);
        }

        private void chkOnlyUnEvent_CheckedChanged(object sender, EventArgs e)
        {
            DataTable tblTemp = null;
            if (chkOnlyUnEvent.Checked)
            {
                tblTemp = Library.AppCore.DataTableClass.Select(tblUnEventTable, "Different <> 0 OR ERP_PRODUCTSTATUSID <> INV_PRODUCTSTATUSID");
                tblTemp = Library.AppCore.Globals.DataTableSelect(tblTemp, string.Empty, "ProductName");
                //Library.AppCore.DataTableClass.AddSumRow(tblTemp, "PRODUCTNAME,Tổng cộng,PRODUCTID,.,ERP_IMEI,.", "ERP_QUANTITY,INV_QUANTITY,DIFFERENT");
            }
            else
            {
                tblTemp = tblUnEventTable.Copy();
                tblTemp.PrimaryKey = null;
                //Library.AppCore.DataTableClass.AddSumRow(tblTemp, "PRODUCTNAME,Tổng cộng,PRODUCTID,.,ERP_IMEI,.", "ERP_QUANTITY,INV_QUANTITY,DIFFERENT");
            }
            grdCtlProducts.DataSource = tblTemp;
            //CustomFlex();
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            //Library.AppCore.CustomControls.GridControl.v.ExportExcel(grdCtlProducts);
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdCtlProducts);
        }

        private void grdViewProducts_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle == GridControl.AutoFilterRowHandle)
                return;
            if(grdViewProducts.RowCount == 0)
                return;
            string strValue = (grdViewProducts.GetRowCellValue(e.RowHandle, "DIFFERENT") ?? "").ToString().Trim();
            if (string.IsNullOrEmpty(strValue))
                return;
            decimal dec = Convert.ToDecimal(strValue);
            if(dec > 0) // thieu
            {
                e.Appearance.ForeColor = Color.Blue;
                e.Appearance.Options.UseForeColor = true;
            }
            if (dec < 0) // thua
            {
                e.Appearance.ForeColor = Color.Red;
                e.Appearance.Options.UseForeColor = true;
            }
        }
    }
}
