﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmInventoryDifferenceResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInventoryDifferenceResult));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnReview = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenuReview = new DevExpress.XtraBars.PopupMenu();
            this.mnuItemReview = new DevExpress.XtraBars.BarButtonItem();
            this.mnuItemDenied = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.lblSum = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.chkOnlyUnEvent = new System.Windows.Forms.CheckBox();
            this.mnReview = new DevExpress.XtraBars.BarListItem();
            this.mnDenied = new DevExpress.XtraBars.BarListItem();
            this.flexData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip();
            this.mnuItemExcelExport = new System.Windows.Forms.ToolStripMenuItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barMdiChildrenListItem1 = new DevExpress.XtraBars.BarMdiChildrenListItem();
            this.barLinkContainerItem1 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.mnuReview = new DevExpress.XtraBars.BarStaticItem();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuReview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btnReview);
            this.panel1.Controls.Add(this.lblSum);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.chkOnlyUnEvent);
            this.panel1.Location = new System.Drawing.Point(1, 440);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1103, 44);
            this.panel1.TabIndex = 1;
            // 
            // btnReview
            // 
            this.btnReview.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnReview.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReview.Appearance.Options.UseFont = true;
            this.btnReview.DropDownControl = this.popupMenuReview;
            this.btnReview.Image = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.btnReview.Location = new System.Drawing.Point(989, 11);
            this.btnReview.Name = "btnReview";
            this.btnReview.Size = new System.Drawing.Size(94, 23);
            this.btnReview.TabIndex = 85;
            this.btnReview.Text = "Duyệt";
            // 
            // popupMenuReview
            // 
            this.popupMenuReview.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Caption, this.mnuItemReview, "Đồng ý"),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Caption, this.mnuItemDenied, "Từ chối")});
            this.popupMenuReview.Manager = this.barManager1;
            this.popupMenuReview.Name = "popupMenuReview";
            // 
            // mnuItemReview
            // 
            this.mnuItemReview.Caption = "mnuItemReview";
            this.mnuItemReview.Id = 2;
            this.mnuItemReview.Name = "mnuItemReview";
            this.mnuItemReview.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuItemReview_ItemClick);
            // 
            // mnuItemDenied
            // 
            this.mnuItemDenied.Caption = "mnuItemDenied";
            this.mnuItemDenied.Id = 3;
            this.mnuItemDenied.Name = "mnuItemDenied";
            this.mnuItemDenied.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuItemDenied_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.mnuItemReview,
            this.mnuItemDenied});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 4;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 1;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.Text = "Tools";
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1107, 51);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 461);
            this.barDockControlBottom.Size = new System.Drawing.Size(1107, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 51);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 410);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1107, 51);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 410);
            // 
            // lblSum
            // 
            this.lblSum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSum.AutoSize = true;
            this.lblSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSum.ForeColor = System.Drawing.Color.Blue;
            this.lblSum.Location = new System.Drawing.Point(185, 14);
            this.lblSum.Name = "lblSum";
            this.lblSum.Size = new System.Drawing.Size(16, 16);
            this.lblSum.TabIndex = 84;
            this.lblSum.Text = "0";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(3, 14);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(172, 16);
            this.label16.TabIndex = 83;
            this.label16.Text = "Tổng sản phẩm hiển thị:";
            // 
            // chkOnlyUnEvent
            // 
            this.chkOnlyUnEvent.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkOnlyUnEvent.AutoSize = true;
            this.chkOnlyUnEvent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOnlyUnEvent.Location = new System.Drawing.Point(755, 12);
            this.chkOnlyUnEvent.Name = "chkOnlyUnEvent";
            this.chkOnlyUnEvent.Size = new System.Drawing.Size(219, 20);
            this.chkOnlyUnEvent.TabIndex = 82;
            this.chkOnlyUnEvent.Text = "Chỉ hiển thị sản phẩm chênh lệch";
            this.chkOnlyUnEvent.UseVisualStyleBackColor = true;
            this.chkOnlyUnEvent.CheckedChanged += new System.EventHandler(this.chkOnlyUnEvent_CheckedChanged);
            // 
            // mnReview
            // 
            this.mnReview.Caption = "Duyệt";
            this.mnReview.Id = 5;
            this.mnReview.Name = "mnReview";
            // 
            // mnDenied
            // 
            this.mnDenied.Caption = "Từ chối";
            this.mnDenied.Id = 6;
            this.mnDenied.Name = "mnDenied";
            // 
            // flexData
            // 
            this.flexData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexData.AllowEditing = false;
            this.flexData.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexData.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flexData.AutoClipboard = true;
            this.flexData.ColumnInfo = "10,1,0,0,0,105,Columns:";
            this.flexData.ContextMenuStrip = this.contextMenuStrip1;
            this.flexData.Location = new System.Drawing.Point(0, 0);
            this.flexData.Name = "flexData";
            this.flexData.Rows.DefaultSize = 21;
            this.flexData.Size = new System.Drawing.Size(1105, 435);
            this.flexData.StyleInfo = resources.GetString("flexData.StyleInfo");
            this.flexData.TabIndex = 4;
            this.flexData.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemExcelExport});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(128, 26);
            // 
            // mnuItemExcelExport
            // 
            this.mnuItemExcelExport.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExcelExport.Name = "mnuItemExcelExport";
            this.mnuItemExcelExport.Size = new System.Drawing.Size(127, 22);
            this.mnuItemExcelExport.Text = "Xuất Excel";
            this.mnuItemExcelExport.Click += new System.EventHandler(this.mnuItemExcelExport_Click);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 8;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barMdiChildrenListItem1
            // 
            this.barMdiChildrenListItem1.Caption = "barMdiChildrenListItem1";
            this.barMdiChildrenListItem1.Id = 9;
            this.barMdiChildrenListItem1.Name = "barMdiChildrenListItem1";
            // 
            // barLinkContainerItem1
            // 
            this.barLinkContainerItem1.Caption = "barLinkContainerItem1";
            this.barLinkContainerItem1.Id = 10;
            this.barLinkContainerItem1.Name = "barLinkContainerItem1";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "barStaticItem2";
            this.barStaticItem2.Id = 12;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // mnuReview
            // 
            this.mnuReview.Caption = "Duyệt";
            this.mnuReview.Id = 13;
            this.mnuReview.Name = "mnuReview";
            this.mnuReview.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // frmInventoryDifferenceResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1107, 484);
            this.Controls.Add(this.flexData);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "frmInventoryDifferenceResult";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kết quả chênh lệch kiểm kê";
            this.Load += new System.EventHandler(this.frmInventoryDifferenceResult_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuReview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private C1.Win.C1FlexGrid.C1FlexGrid flexData;
        private System.Windows.Forms.Label lblSum;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chkOnlyUnEvent;
        private DevExpress.XtraEditors.DropDownButton btnReview;
        private DevExpress.XtraBars.BarListItem mnReview;
        private DevExpress.XtraBars.BarListItem mnDenied;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarMdiChildrenListItem barMdiChildrenListItem1;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem mnuReview;
        private DevExpress.XtraBars.PopupMenu popupMenuReview;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem mnuItemReview;
        private DevExpress.XtraBars.BarButtonItem mnuItemDenied;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExcelExport;
    }
}