﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using ERP.MasterData.PLC.MD.WSProduct;
using Library.AppCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmInventoryVTS_AFJoin : Form
    {
        #region Create PLC
        private ERP.Inventory.PLC.ProductChange.PLCProductChange objPLCProductChange = new PLC.ProductChange.PLCProductChange();

        private ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();

        private ERP.MasterData.PLC.MD.WSInventoryType.InventoryType objInventoryType = new MasterData.PLC.MD.WSInventoryType.InventoryType();
        private ERP.MasterData.PLC.MD.PLCInventoryType objPLCInventoryType = new MasterData.PLC.MD.PLCInventoryType();
        private ERP.Inventory.PLC.Inventory.PLCInventoryTerm objPLCInventoryTerm = new PLC.Inventory.PLCInventoryTerm();
        private ERP.Inventory.PLC.Inventory.PLCInventory objPLCInventory = new PLC.Inventory.PLCInventory();
        private PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();
        #endregion

        #region FormType
        enum eFormType
        {
            ADD,
            VIEW
        }

        eFormType objFormType = eFormType.VIEW;
        private eFormType FormType
        {
            get { return objFormType; }
            set
            {
                objFormType = value;
                // Ẩn các nut
                txtInventoryID.Text = InventoryID;
                btnSave.Enabled = IsCanUpdateReview;
                btnDelete.Enabled = objFormType == eFormType.VIEW
                        && !chkIsReviewed.Checked
                        && !chkIsDeleted.Checked
                    //    && !chkIsConnected.Checked
                    // && !chkIsParent.Checked
                        ;
                if (!btnDelete.Enabled)
                    btnDelete.Enabled = isPermissionCancel;
                txtContentDelete.Visible = chkIsDeleted.Checked;
                dropDownBtnApprove.Enabled = !chkIsReviewed.Checked && chkIsParent.Checked && (objInventory != null && objInventory.ReviewStatusID != 3);
                // cboItemUnEvent.Enabled = chkIsReviewed.Checked && chkIsParent.Checked;
                if (objFormType == eFormType.VIEW)
                    txtContent.Size = new Size(540, 68);
                if (objFormType == eFormType.ADD)
                {
                    txtInventoryID.Text = objPLCInventory.GetInventoryNewID(Library.AppCore.SystemConfig.intDefaultStoreID);
                    btnSave.Enabled = true;
                }
            }
        }

        #endregion

        #region variable
        public ERP.Inventory.PLC.Inventory.WSInventoryTerm.InventoryTerm objInventoryTerm = null;
        private ERP.Inventory.PLC.Inventory.WSInventory.Inventory objInventory = null;
        private string PERMISSION_INVENTORY_VTS_CANCEL = "PERMISSION_INVENTORY_VTS_CANCEL";
        bool isPermissionCancel = false;

        private int intInventoryTermID = -1;
        public int InventoryTermID
        {
            get { return intInventoryTermID; }
            set { intInventoryTermID = value; }
        }

        private string strInventoryID = string.Empty;
        public string InventoryID
        {
            get { return strInventoryID; }
            set { strInventoryID = value; }
        }

        private string strMaingroupIDList = string.Empty;
        public string MaingroupIDList
        {
            get { return strMaingroupIDList; }
            set
            {
                string strValue = value;
                strValue = strValue.Replace("><", ",");
                strValue = strValue.Replace("<", string.Empty);
                strValue = strValue.Replace(">", string.Empty);
                strMaingroupIDList = strValue;
            }
        }

        private string strSubgroupIDList = string.Empty;
        public string SubgroupIDList
        {
            get { return strSubgroupIDList; }
            set
            {
                string strValue = value;
                strValue = strValue.Replace("><", ",");
                strValue = strValue.Replace("<", string.Empty);
                strValue = strValue.Replace(">", string.Empty);
                strSubgroupIDList = strValue;
            }
        }

        private int intStoreID = -1;
        public int StoreID
        {
            get { return intStoreID; }
            set { intStoreID = value; }
        }

        private int intProductStateID = -1;
        public int ProductStateID
        {
            get { return intProductStateID; }
            set { intProductStateID = value; }
        }
        private string strInventoryUser = string.Empty;
        public string InventoryUser
        {
            get { return strInventoryUser; }
            set { strInventoryUser = value; }
        }
        int intInventoryTypeID = 0;//objInventoryTerm.InventoryTypeID;

        private bool bolIsCheckDuplicateIMEIAllProduct = false;
        private bool bolIsCheckDuplicateIMEIAllStore = false;
        private DataTable dtbDataProducts = null;
        private DataTable dtbReviewLevel = null;
        private DataTable dtbUnEvent = null;
        private bool bolUpdate_IsAddReviewList = false;
        #endregion


        public frmInventoryVTS_AFJoin()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            isPermissionCancel = SystemConfig.objSessionUser.IsPermission(PERMISSION_INVENTORY_VTS_CANCEL);
        }
        #region Load Form

        private void frmInventoryVTS_Load(object sender, EventArgs e)
        {
            InitiallizeDataProducts();

            if (string.IsNullOrEmpty(strInventoryID))
                FormType = eFormType.ADD;
            else
                InitiallizeInventoryInfo();
            InitiallizeAppconfig();
            InitiallizeCombobox();

            InitiallizeGridProduct();
            InitiallizeGridUserReviewLevel();
            InitiallizeInventoryUser();
            InitiallizeInventoryTerm(InventoryTermID);
            InitiallizeUserReviewLevel();
            InitiallizeInStock();
            if (FormType == eFormType.VIEW)
                btnSave.Enabled = IsCanUpdateReview;
        }

        #region Initiallize
        DataTable dtbInStockProduct = null;
        DataTable dtbInStockProductIMEI = null;
        DataSet dtsInOutProduct = null;
        private void InitiallizeInStock()
        {
            if (dtbInStockProduct == null || dtbInStockProduct.Rows.Count < 1)
            {
                dtbInStockProduct = objPLCInventory.GetListTermStock_Byte(objInventoryTerm.InventoryTermID, intStoreID, cboMainGroupIDList.MainGroupIDList, cboSubGroupIDList.SubGroupIDList, -1);//Convert.ToInt32(cboProductStateID.SelectedValue));
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    this.Close();
                }
                dtbInStockProduct.PrimaryKey = new DataColumn[] { dtbInStockProduct.Columns["PRODUCTID"], dtbInStockProduct.Columns["PRODUCTSTATUSID"] };
            }

            if (dtbInStockProductIMEI == null || dtbInStockProductIMEI.Rows.Count < 1)
            {
                dtbInStockProductIMEI = objPLCInventory.GetListTermStockIMEI_Byte(objInventoryTerm.InventoryTermID, intStoreID, cboMainGroupIDList.MainGroupIDList, cboSubGroupIDList.SubGroupIDList, -1);// Convert.ToInt32(cboProductStateID.SelectedValue));
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    this.Close();
                }
                dtbInStockProductIMEI.PrimaryKey = new DataColumn[] { dtbInStockProductIMEI.Columns["PRODUCTID"], dtbInStockProductIMEI.Columns["IMEI"], dtbInStockProductIMEI.Columns["PRODUCTSTATUSID"] };
            }
            
        }
        private void InitiallizeAppconfig()
        {
            string strISCHECKDUPLICATEIMEIALLPRODUCT = (Library.AppCore.AppConfig.GetConfigValue("ISCHECKDUPLICATEIMEIALLPRODUCT") ?? "").ToString().Trim();
            string strISCHECKDUPLICATEIMEIALLSTORE = (Library.AppCore.AppConfig.GetConfigValue("ISCHECKDUPLICATEIMEIALLSTORE") ?? "").ToString().Trim();
            if (!string.IsNullOrEmpty(strISCHECKDUPLICATEIMEIALLPRODUCT))
                bolIsCheckDuplicateIMEIAllProduct = Convert.ToBoolean(Convert.ToInt32(strISCHECKDUPLICATEIMEIALLPRODUCT));
            if (!string.IsNullOrEmpty(strISCHECKDUPLICATEIMEIALLSTORE))
                bolIsCheckDuplicateIMEIAllStore = Convert.ToBoolean(Convert.ToInt32(strISCHECKDUPLICATEIMEIALLSTORE));
        }

        private void InitiallizeCombobox()
        {
            cboMainGroupIDList.InitControl(true, false);
            cboMainGroupIDList.IsReturnAllWhenNotChoose = true;
            cboSubGroupIDList.InitControl(true, cboMainGroupIDList.MainGroupIDList);
            cboSubGroupIDList.IsReturnAllWhenNotChoose = true;
            cboStoreID.InitControl(false, false);
            DataTable dtbProductStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
            if (objFormType == eFormType.VIEW)
            {
                DataRow drALL = dtbProductStatus.NewRow();
                drALL["PRODUCTSTATUSID"] = -1;
                drALL["PRODUCTSTATUSNAME"] = "--- Tất cả ---";
                dtbProductStatus.Rows.InsertAt(drALL, 0);
            }

            cboProductStateID.DataSource = dtbProductStatus;
            cboProductStateID.DisplayMember = "PRODUCTSTATUSNAME";
            cboProductStateID.ValueMember = "PRODUCTSTATUSID";
            cboProductStateID.SelectedValue = -1;
            if (ProductStateID > -1)
                cboProductStateID.SelectedValue = ProductStateID;

            if (StoreID > -1)
                cboStoreID.SetValue(StoreID);
        }

        private void InitiallizeInventoryTerm(int InventoryTermID)
        {
            if (InventoryTermID < 1 && objInventoryTerm == null)
            {
                MessageBox.Show(this, "Không tìm thấy thông tin kỳ kiểm kê!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return; // Trả lỗi dong form
            }
            if (InventoryTermID < 1 && objInventoryTerm != null)
            {
                InventoryTermID = objInventoryTerm.InventoryTermID;
                return;
            }
            if (InventoryTermID > 0 && objInventoryTerm == null)
                objPLCInventoryTerm.LoadInfo_ByUser(ref objInventoryTerm, InventoryTermID, InventoryUser);
            intInventoryTypeID = objInventoryTerm.InventoryTypeID;
            // Load DataCombobox
            DataTable tblINVTerm = new DataTable();
            tblINVTerm.Columns.Add("INVENTORYTERMID", typeof(int));
            tblINVTerm.Columns.Add("INVENTORYTERMNAME", typeof(string));
            DataRow rINVTerm = tblINVTerm.NewRow();
            rINVTerm["INVENTORYTERMID"] = objInventoryTerm.InventoryTermID;
            rINVTerm["INVENTORYTERMNAME"] = objInventoryTerm.InventoryTermName;
            tblINVTerm.Rows.Add(rINVTerm);
            cboInventoryTermID.InitControl(false, tblINVTerm, "INVENTORYTERMID", "INVENTORYTERMNAME", "--- Kiểm kê ---");
            cboInventoryTermID.SetValue(InventoryTermID);

            // Load Kho 
            objInventoryTerm.dtbTerm_MainGroup.DefaultView.RowFilter = "IsSelect = 1";
            DataTable dtbTerm_MainGroup = objInventoryTerm.dtbTerm_MainGroup.DefaultView.ToTable();

            if (dtbTerm_MainGroup.Columns.Contains("IsSelect"))
            {
                dtbTerm_MainGroup.Columns.Remove("IsSelect");
            }
            if (dtbTerm_MainGroup.Rows.Count > 0)
            {
                cboMainGroupIDList.InitControl(true, dtbTerm_MainGroup);
                cboMainGroupIDList.SetValue(strMaingroupIDList);
            }

            string strMainGroupList = cboMainGroupIDList.MainGroupIDList;
            strMainGroupList = strMainGroupList.Replace("><", ",");
            strMainGroupList = strMainGroupList.Replace("<", string.Empty);
            strMainGroupList = strMainGroupList.Replace(">", string.Empty);
            objInventoryTerm.dtbTerm_SubGroup.DefaultView.RowFilter = "IsSelect = 1 AND MainGroupID IN (" + strMainGroupList + ")";
            DataTable dtbTerm_SubGroup = objInventoryTerm.dtbTerm_SubGroup.DefaultView.ToTable();
            if (dtbTerm_SubGroup.Columns.Contains("IsSelect"))
                dtbTerm_SubGroup.Columns.Remove("IsSelect");
            cboSubGroupIDList.InitControl(true, dtbTerm_SubGroup);
            cboSubGroupIDList.SetValue(strSubgroupIDList);
        }

        private void InitiallizeInventoryUser()
        {
            ERP.MasterData.SYS.PLC.WSUser.User objUser = new MasterData.SYS.PLC.WSUser.User();
            ERP.MasterData.SYS.PLC.PLCUser objPLCUser = new MasterData.SYS.PLC.PLCUser();
            objUser = objPLCUser.LoadInfo(strInventoryUser);
            string strUserFullName = objUser.UserName;
            if (Convert.ToString(objUser.FullName).Trim() != string.Empty)
            {
                strUserFullName = objUser.UserName + "-" + Convert.ToString(objUser.FullName).Trim();
            }
            txtInventoryUser.Text = strUserFullName;
        }

        private void InitiallizeDataUnEvent()
        {
            dtbUnEvent = new DataTable();
            dtbUnEvent.Columns.Add("PRODUCTID", typeof(string));
            dtbUnEvent.Columns.Add("PRODUCTNAME", typeof(string));
            dtbUnEvent.Columns.Add("ERP_IMEI", typeof(string));
            dtbUnEvent.Columns.Add("ERP_QUANTITY", typeof(decimal));
            dtbUnEvent.Columns["ERP_QUANTITY"].DefaultValue = 0;
            dtbUnEvent.Columns.Add("ERP_PRODUCTSTATUSNAME", typeof(string));
            dtbUnEvent.Columns.Add("ERP_PRODUCTSTATUSID", typeof(int));
            dtbUnEvent.Columns["ERP_PRODUCTSTATUSID"].DefaultValue = -1;

            dtbUnEvent.Columns.Add("INV_IMEI", typeof(string));
            dtbUnEvent.Columns.Add("INV_QUANTITY", typeof(decimal));
            dtbUnEvent.Columns["INV_QUANTITY"].DefaultValue = 0;
            dtbUnEvent.Columns.Add("INV_PRODUCTSTATUSNAME", typeof(string));
            dtbUnEvent.Columns.Add("INV_PRODUCTSTATUSID", typeof(int));
            dtbUnEvent.Columns["INV_PRODUCTSTATUSID"].DefaultValue = -1;

            dtbUnEvent.Columns.Add("DIFFERENT", typeof(decimal));
            dtbUnEvent.Columns["DIFFERENT"].DefaultValue = 0;
        }

        private void InitiallizeDataProducts()
        {
            dtbDataProducts = new DataTable();
            DataColumn column = new DataColumn();
            column.ColumnName = "STT";
            column.DataType = System.Type.GetType("System.Int32");
            column.AutoIncrement = true;
            column.AutoIncrementSeed = 1;
            column.AutoIncrementStep = 1;
            dtbDataProducts.Columns.Add(column);
            dtbDataProducts.Columns.Add("PRODUCTID", typeof(string));
            dtbDataProducts.Columns.Add("PRODUCTNAME", typeof(string));
            dtbDataProducts.Columns.Add("IMEI", typeof(string));
            dtbDataProducts.Columns.Add("QUANTITY", typeof(decimal));
            dtbDataProducts.Columns["QUANTITY"].DefaultValue = 0;
            dtbDataProducts.Columns.Add("CABINETNUMBER", typeof(int));
            dtbDataProducts.Columns["CABINETNUMBER"].DefaultValue = 1;
            dtbDataProducts.Columns.Add("PRODUCTSTATUSID", typeof(int));
            dtbDataProducts.Columns.Add("ISREQUESTIMEI", typeof(int));
            dtbDataProducts.Columns["ISREQUESTIMEI"].DefaultValue = 0;
            dtbDataProducts.Columns.Add("NOTE", typeof(string));
            dtbDataProducts.Columns.Add("INVENTORYDETAILID", typeof(string));
            dtbDataProducts.Columns.Add("PRODUCTSTATUSNAME", typeof(string));
            grdCtlProducts.DataSource = dtbDataProducts;
        }

        private void InitiallizeGridUserReviewLevel()
        {
            string[] fieldNames = new[] { "REVIEWSEQUENCE", "REVIEWLEVELNAME", "ISREVIEWED", "REVIEWEDDATE", "FULLNAME", "STATUSNAME", "ISSELECT" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdUserReviewLevel, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewUserReviewLevel);
            FormatGridUserReviewLevel(FormType == eFormType.ADD);

        }

        private void InitiallizeGridProduct()
        {
            string[] fieldNames = new[] { "PRODUCTID", "PRODUCTNAME", "IMEI", "QUANTITY", "PRODUCTSTATUSNAME", "NOTE" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdCtlProducts, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewProducts);
            FormatGridProduct(FormType == eFormType.ADD);

        }

        private void InitiallizeUserReviewLevel()
        {
            objPLCInventoryType.LoadInfo(ref objInventoryType, intInventoryTypeID);
            if (objInventoryType == null || objInventoryType.InventoryTypeID < 1)
            {
                if ((grdUserReviewLevel.DataSource as DataTable) == null)
                {
                    grdUserReviewLevel.DataSource = objPLCInventory.GetInventoryUserReviewed("-1", Convert.ToString(txtInventoryID.Text).Trim(), 0, cboStoreID.StoreID, false);
                }
                if (!(grdUserReviewLevel.DataSource as DataTable).Columns.Contains("ISSELECT"))
                {
                    (grdUserReviewLevel.DataSource as DataTable).Columns.Add("ISSELECT", typeof(Decimal));
                    (grdUserReviewLevel.DataSource as DataTable).Columns["ISSELECT"].DefaultValue = Convert.ToDecimal(1);
                }
                //CustomFlexUserRvwLvl();
                //flexUserReviewLevel.Cols["IsSelect"].Visible = false;
                FormatGridUserReviewLevel(false);
                return;
            }
            dtbReviewLevel = null;
            objPLCInventoryType.SearchDataReviewLevel(ref dtbReviewLevel, new object[] { "@InventoryTypeID", intInventoryTypeID });
            if (dtbReviewLevel.Rows.Count < 1)
            {
                if ((grdUserReviewLevel.DataSource as DataTable) == null)
                {
                    grdUserReviewLevel.DataSource = objPLCInventory.GetInventoryUserReviewed("-1", Convert.ToString(txtInventoryID.Text).Trim(), 0, cboStoreID.StoreID, false);
                }
                if (!(grdUserReviewLevel.DataSource as DataTable).Columns.Contains("ISSELECT"))
                {
                    (grdUserReviewLevel.DataSource as DataTable).Columns.Add("ISSELECT", typeof(Decimal));
                    (grdUserReviewLevel.DataSource as DataTable).Columns["ISSELECT"].DefaultValue = Convert.ToDecimal(1);
                }
                //CustomFlexUserRvwLvl();
                //flexUserReviewLevel.Cols["IsSelect"].Visible = false;
                FormatGridUserReviewLevel(false);
                return;
            }

            dtbReviewLevel.DefaultView.Sort = "ReviewSequence ASC";
            dtbReviewLevel = dtbReviewLevel.DefaultView.ToTable();
            dtbReviewLevel.PrimaryKey = new DataColumn[] { dtbReviewLevel.Columns["ReviewLevelID"] };

            if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
            {
                DataTable dtbInventoryRvwLevel = objPLCInventory.GetInventoryReviewLevel(objInventory.InventoryID);
                if (!dtbInventoryRvwLevel.Columns.Contains("ISSELECT"))
                {
                    dtbInventoryRvwLevel.Columns.Add("ISSELECT", typeof(Decimal));
                    dtbInventoryRvwLevel.Columns["ISSELECT"].DefaultValue = Convert.ToDecimal(1);
                }
                if (dtbInventoryRvwLevel.Rows.Count > 0)
                {
                    grdUserReviewLevel.DataSource = dtbInventoryRvwLevel;
                    //CustomFlexUserRvwLvl();
                    //flexUserReviewLevel.Cols["IsSelect"].Visible = false;
                    FormatGridUserReviewLevel(false);
                    return;
                }
                else
                {
                    grdUserReviewLevel.DataSource = dtbInventoryRvwLevel;
                    FormatGridUserReviewLevel(true);
                    //CustomFlexUserRvwLvl();
                    bolUpdate_IsAddReviewList = true;
                    //txtUserName.Enabled = false;
                    //txtPassword.Enabled = false;
                    dropDownBtnApprove.Enabled = false;
                    //btnReviewStatus.Enabled = false;
                }
            }

            string strReviewFunctionID = string.Empty;
            DataTable dtbUserReviewLevel = null;
            for (int i = 0; i < dtbReviewLevel.Rows.Count; i++)
            {
                if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["ReviewFunctionID"]))
                {
                    strReviewFunctionID = Convert.ToString(dtbReviewLevel.Rows[i]["ReviewFunctionID"]).Trim();
                }
                bool bolIsCheckStorePermission = false;
                if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCheckStorePermission"]))
                {
                    bolIsCheckStorePermission = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCheckStorePermission"]);
                }
                bool bolIsCheckInventoryUser = false;
                if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCheckInventoryUser"]))
                {
                    bolIsCheckInventoryUser = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCheckInventoryUser"]);
                }
                DataTable dtbTEMP = objPLCInventory.GetInventoryUserReviewed(strReviewFunctionID, Convert.ToString(txtInventoryID.Text).Trim(), Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]), cboStoreID.StoreID, bolIsCheckStorePermission);
                if (dtbUserReviewLevel == null)
                {
                    dtbUserReviewLevel = dtbTEMP.Clone();
                    if (!dtbUserReviewLevel.Columns.Contains("ISSELECT"))
                    {
                        dtbUserReviewLevel.Columns.Add("ISSELECT", typeof(Decimal));
                        dtbUserReviewLevel.Columns["ISSELECT"].DefaultValue = Convert.ToDecimal(0);
                    }
                }
                int intReviewType = Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewType"]);
                if (bolIsCheckInventoryUser)
                {
                    DataRow[] rUserRvwFunct = dtbTEMP.Select("ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]) + " AND UserName = '" + strInventoryUser + "'");
                    if (rUserRvwFunct.Length > 0)
                    {
                        DataRow rInventoryUser = dtbUserReviewLevel.NewRow();
                        rInventoryUser.ItemArray = rUserRvwFunct[0].ItemArray;
                        rInventoryUser["ISSELECT"] = Convert.ToDecimal(1);
                        dtbUserReviewLevel.Rows.Add(rInventoryUser);
                    }
                }
                else
                {
                    for (int j = 0; j < dtbTEMP.Rows.Count; j++)
                    {
                        DataRow rInventoryUser = dtbUserReviewLevel.NewRow();
                        rInventoryUser.ItemArray = dtbTEMP.Rows[j].ItemArray;
                        if (intReviewType == 0)
                        {
                            //rInventoryUser["IsSelect"] = true;
                        }
                        dtbUserReviewLevel.Rows.Add(rInventoryUser);
                    }
                }
            }
            grdUserReviewLevel.DataSource = dtbUserReviewLevel;
            FormatGridUserReviewLevel(true);
            //CustomFlexUserRvwLvl();
        }

        private void InitiallizeInventoryInfo()
        {
            if (strInventoryID.Trim() == string.Empty)
            {
                return;
            }
            if (objInventory == null)
            {
                objInventory = new PLC.Inventory.WSInventory.Inventory();
            }
            InitiallizeDataProducts();
            DataTable dtbInventoryDetail = null;
            objPLCInventory.LoadInventoryInfo(ref objInventory, strInventoryID.Trim(), ref  dtbInventoryDetail);
            InventoryTermID = objInventory.InventoryTermID;
            strInventoryUser = objInventory.InventoryUser;
            intStoreID = objInventory.InventoryStoreID;
            strMaingroupIDList = objInventory.MainGroupIDList;
            strSubgroupIDList = objInventory.SubGroupIDList;
            intProductStateID = objInventory.ProductStatusID;
            txtContent.Text = Convert.ToString(objInventory.Content).Trim();
            txtContentDelete.Text = Convert.ToString(objInventory.DeletedNote).Trim();
            chkIsReviewed.Checked = objInventory.IsReviewed;
            chkIsDeleted.Checked = objInventory.IsDeleted;
            chkIsConnected.Checked = objInventory.IsConnected;
            chkIsParent.Checked = objInventory.IsParent;
            dtpInventoryDate.Value = DateTime.Now;// (DateTime)objInventory.InventoryDate;
            dtpBeginInventoryTime.Value = (DateTime)objInventory.BeginInventoryTime;
            if (objInventory.IsReviewed)
            {
                txtContent.ReadOnly = true;
            }
            if (objInventory.IsDeleted)
            {
                txtContent.ReadOnly = true;
                txtContentDelete.ReadOnly = true;
            }
            if (objInventory.IsUpdateUnEvent)
            {
                mnuItemExplainUnEvent.Enabled = true;
            }
            if (objInventory.IsExplainUnEvent && objInventory.IsExplainAccess)
            {
                mnuEventHandlingUnEvent.Enabled = true;
            }
            if (dtbInventoryDetail.Rows.Count > 0)
            {
                for (int i = 0; i < dtbInventoryDetail.Rows.Count; i++)
                {
                    DataRow rDetail = dtbInventoryDetail.Rows[i];
                    DataRow rData = dtbDataProducts.NewRow();
                    rData["PRODUCTID"] = rDetail["ProductID"];
                    rData["PRODUCTNAME"] = rDetail["ProductName"];
                    rData["IMEI"] = rDetail["IMEI"];
                    rData["QUANTITY"] = rDetail["Quantity"];
                    rData["CABINETNUMBER"] = rDetail["CabinetNumber"];
                    rData["ISREQUESTIMEI"] = rDetail["IsRequestIMEI"];
                    rData["PRODUCTSTATUSID"] = rDetail["PRODUCTSTATUSID"];
                    rData["PRODUCTSTATUSNAME"] = rDetail["PRODUCTSTATUSNAME"];
                    rData["NOTE"] = rDetail["NOTE"];
                    rData["INVENTORYDETAILID"] = rDetail["INVENTORYDETAILID"];
                    //#region
                    ////xem chenh lech: xet IsRequestIMEI = 0 truoc, tiep theo IsRequestIMEI = 1 ( xet IsInStock = 1 truoc, tiep theo IsInStock = 0 )
                    //string strProductID = Convert.ToString(rData["ProductID"]).Trim();
                    //string strProductName = Convert.ToString(rData["ProductName"]).Trim();
                    //int intCabinetNumber = Convert.ToInt32(rData["CabinetNumber"]);
                    //decimal decQuantity = Convert.ToDecimal(rData["Quantity"]);
                    //decimal decQuantityInStock = Convert.ToDecimal(rData["InStockQuantity"]);
                    //if (!Convert.ToBoolean(rData["IsRequestIMEI"]))
                    //{
                    //    DataRow rInStockProduct = dtbInStockProduct.Rows.Find(strProductID);
                    //    if (rInStockProduct != null)
                    //    {
                    //        rInStockProduct["CabinetNumber"] = intCabinetNumber;
                    //        rInStockProduct["Quantity_Inventory"] = decQuantity;
                    //        rInStockProduct["Different"] = decQuantityInStock - decQuantity;
                    //    }
                    //}
                    //else
                    //{
                    //    string strIMEI = Convert.ToString(rData["IMEI"]).Trim();
                    //    if (Convert.ToBoolean(rData["IsInStock"]))
                    //    {
                    //        DataRow rInStockIMEI = dtbInStockProductIMEI.Rows.Find(new object[] { strProductID, strIMEI });
                    //        if (rInStockIMEI != null)
                    //        {
                    //            rInStockIMEI["CabinetNumber"] = intCabinetNumber;
                    //            rInStockIMEI["Col_IMEI"] = strIMEI;
                    //            rInStockIMEI["IMEI_Inventory"] = strIMEI;
                    //            rInStockIMEI["Quantity_Inventory"] = 1;
                    //            rInStockIMEI["Different"] = decQuantityInStock - 1;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        DataRow[] rExistIMEINotInStock = dtbInStockProductIMEI.Select("TRIM(ProductID) = '" + strProductID + "' AND (Col_IMEI IS NULL OR TRIM(Col_IMEI)='') AND TRIM(IMEI_Inventory) = '" + strIMEI + "'");
                    //        if (rExistIMEINotInStock.Length < 1)
                    //        {
                    //            DataRow rNew = dtbInStockProductIMEI.NewRow();
                    //            rNew["CabinetNumber"] = intCabinetNumber;
                    //            rNew["ProductID"] = strProductID;
                    //            rNew["ProductName"] = strProductName;
                    //            rNew["IMEI"] = (--intKeyIMEI);
                    //            rNew["Col_IMEI"] = string.Empty;
                    //            rNew["IMEI_Inventory"] = strIMEI;
                    //            rNew["Quantity_Inventory"] = 1;
                    //            decimal decQuantity_InStock = 0;
                    //            rNew["Different"] = decQuantity_InStock - 1;
                    //            rNew["SubGroupID"] = intSubGroupID;
                    //            dtbInStockProductIMEI.Rows.Add(rNew);
                    //        }
                    //    }
                    //}

                    //#endregion

                    dtbDataProducts.Rows.Add(rData);
                }

            }

            //txtUserName.Enabled = true;
            //txtPassword.Enabled = true;
            dropDownBtnApprove.Enabled = true;
            //btnReviewStatus.Enabled = true;
            FormType = eFormType.VIEW;
        }


        #endregion

        #region Format Grid
        bool IsCanUpdateReview = false;
        private void FormatGridUserReviewLevel(bool bolIsCanEdit)
        {
            IsCanUpdateReview = bolIsCanEdit;
            //REVIEWSEQUENCE","REVIEWLEVELNAME","ISREVIEWED","REVIEWEDDATE","USERNAME","FULLNAME","STATUSNAME","ISSELECT
            grdViewUserReviewLevel.Columns["REVIEWSEQUENCE"].Caption = "Thứ tự duyệt";
            grdViewUserReviewLevel.Columns["REVIEWLEVELNAME"].Caption = "Mức duyệt";
            grdViewUserReviewLevel.Columns["ISREVIEWED"].Caption = "Đã duyệt";
            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].Caption = "Ngày duyệt";
            //grdViewUserReviewLevel.Columns["USERNAME"].Caption = "Mã Người duyệt";
            grdViewUserReviewLevel.Columns["FULLNAME"].Caption = "Người duyệt";
            grdViewUserReviewLevel.Columns["STATUSNAME"].Caption = "Trạng thái duyệt";
            grdViewUserReviewLevel.Columns["ISSELECT"].Caption = "Chọn";

            grdViewUserReviewLevel.Columns["REVIEWSEQUENCE"].Width = 60;
            grdViewUserReviewLevel.Columns["REVIEWSEQUENCE"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.Columns["REVIEWSEQUENCE"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewUserReviewLevel.Columns["REVIEWSEQUENCE"].OptionsColumn.AllowEdit = false;

            grdViewUserReviewLevel.Columns["REVIEWLEVELNAME"].Width = 200;
            grdViewUserReviewLevel.Columns["REVIEWLEVELNAME"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.Columns["REVIEWLEVELNAME"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewUserReviewLevel.Columns["REVIEWLEVELNAME"].OptionsColumn.AllowEdit = false;

            grdViewUserReviewLevel.Columns["ISREVIEWED"].Width = 70;
            grdViewUserReviewLevel.Columns["ISREVIEWED"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.Columns["ISREVIEWED"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewUserReviewLevel.Columns["ISREVIEWED"].OptionsColumn.AllowEdit = false;

            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].Width = 110;
            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].OptionsColumn.AllowEdit = false;

            grdViewUserReviewLevel.Columns["FULLNAME"].Width = 220;
            grdViewUserReviewLevel.Columns["FULLNAME"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.Columns["FULLNAME"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewUserReviewLevel.Columns["FULLNAME"].OptionsColumn.AllowEdit = false;

            grdViewUserReviewLevel.Columns["STATUSNAME"].Width = 80;
            grdViewUserReviewLevel.Columns["STATUSNAME"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.Columns["STATUSNAME"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewUserReviewLevel.Columns["STATUSNAME"].OptionsColumn.AllowEdit = false;

            grdViewUserReviewLevel.Columns["ISSELECT"].Width = 40;
            grdViewUserReviewLevel.Columns["ISSELECT"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.Columns["ISSELECT"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewUserReviewLevel.Columns["ISSELECT"].OptionsColumn.AllowEdit = true;
            grdViewUserReviewLevel.Columns["ISSELECT"].Visible = bolIsCanEdit;


            grdViewUserReviewLevel.OptionsView.AllowCellMerge = true;
            grdViewUserReviewLevel.Columns["FULLNAME"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            grdViewUserReviewLevel.Columns["ISREVIEWED"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            grdViewUserReviewLevel.Columns["STATUSNAME"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            grdViewUserReviewLevel.Columns["ISSELECT"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;

            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";


            DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            chkCheckBoxIsSelect.ValueChecked = ((Decimal)(1));
            chkCheckBoxIsSelect.ValueUnchecked = ((Decimal)(0));
            grdViewUserReviewLevel.Columns["ISSELECT"].ColumnEdit = chkCheckBoxIsSelect;


            DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsReviewed = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            chkCheckBoxIsReviewed.ValueChecked = ((Decimal)(1));
            chkCheckBoxIsReviewed.ValueUnchecked = ((Decimal)(0));
            grdViewUserReviewLevel.Columns["ISREVIEWED"].ColumnEdit = chkCheckBoxIsReviewed;

            DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboLookUpEditStorePermissionType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();


            grdViewUserReviewLevel.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            grdViewUserReviewLevel.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            grdViewUserReviewLevel.OptionsView.ShowAutoFilterRow = true;
            grdViewUserReviewLevel.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            grdViewUserReviewLevel.OptionsView.ShowFooter = false;
            grdViewUserReviewLevel.OptionsView.ShowGroupPanel = false;
            grdViewUserReviewLevel.ColumnPanelRowHeight = 50;
            //grdViewReviewLevel.OptionsFilter.Reset();
            for (int i = 0; i < this.grdViewUserReviewLevel.Columns.Count; i++)
            {
                this.grdViewUserReviewLevel.Columns[i].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            }
        }
        private void FormatGridProduct(bool bolIsCanEdit)
        {
            grdViewProducts.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
            grdViewProducts.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
            grdViewProducts.Columns["IMEI"].Caption = "IMEI";
            grdViewProducts.Columns["QUANTITY"].Caption = "Số lượng";
            grdViewProducts.Columns["PRODUCTSTATUSNAME"].Caption = "Trạng thái";
            grdViewProducts.Columns["NOTE"].Caption = "Ghi chú";

            grdViewProducts.Columns["PRODUCTID"].Width = 120;
            grdViewProducts.Columns["PRODUCTID"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewProducts.Columns["PRODUCTID"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewProducts.Columns["PRODUCTID"].OptionsColumn.AllowEdit = false;
            grdViewProducts.Columns["PRODUCTID"].Summary.Add(DevExpress.Data.SummaryItemType.Custom, "PRODUCTID", "Tổng cộng:");

            grdViewProducts.Columns["PRODUCTNAME"].Width = 200;
            grdViewProducts.Columns["PRODUCTNAME"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewProducts.Columns["PRODUCTNAME"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewProducts.Columns["PRODUCTNAME"].OptionsColumn.AllowEdit = false;
            grdViewProducts.Columns["PRODUCTNAME"].Summary.Add(DevExpress.Data.SummaryItemType.Count, "PRODUCTNAME", "{0:N0}");
            grdViewProducts.Columns["IMEI"].Width = 120;
            grdViewProducts.Columns["IMEI"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewProducts.Columns["IMEI"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewProducts.Columns["IMEI"].OptionsColumn.AllowEdit = bolIsCanEdit;
            grdViewProducts.Columns["IMEI"].ColumnEdit = repTextImei;


            grdViewProducts.Columns["QUANTITY"].Width = 110;
            grdViewProducts.Columns["QUANTITY"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewProducts.Columns["QUANTITY"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewProducts.Columns["QUANTITY"].OptionsColumn.AllowEdit = bolIsCanEdit;
            grdViewProducts.Columns["QUANTITY"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "QUANTITY", "{0:N0}");
            grdViewProducts.Columns["QUANTITY"].ColumnEdit = repQuantity;

            grdViewProducts.Columns["PRODUCTSTATUSNAME"].Width = 120;
            grdViewProducts.Columns["PRODUCTSTATUSNAME"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewProducts.Columns["PRODUCTSTATUSNAME"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewProducts.Columns["PRODUCTSTATUSNAME"].OptionsColumn.AllowEdit = false;

            grdViewProducts.Columns["NOTE"].Width = 220;
            grdViewProducts.Columns["NOTE"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewProducts.Columns["NOTE"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewProducts.Columns["NOTE"].OptionsColumn.AllowEdit = bolIsCanEdit;
            grdViewProducts.Columns["NOTE"].ColumnEdit = repNote;

            grdViewProducts.OptionsView.AllowCellMerge = false;
            grdViewProducts.Columns["PRODUCTID"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            grdViewProducts.Columns["PRODUCTNAME"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            grdViewProducts.Columns["IMEI"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            grdViewProducts.Columns["QUANTITY"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            //grdViewProducts.Columns["CABINETNUMBER"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            grdViewProducts.Columns["NOTE"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;


            grdViewProducts.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewProducts.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            grdViewProducts.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            grdViewProducts.OptionsView.ShowAutoFilterRow = true;
            grdViewProducts.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            grdViewProducts.OptionsView.ShowFooter = true;
            grdViewProducts.OptionsView.ShowGroupPanel = false;
            grdViewProducts.ColumnPanelRowHeight = 50;
            //grdViewReviewLevel.OptionsFilter.Reset();
            for (int i = 0; i < this.grdViewUserReviewLevel.Columns.Count; i++)
            {
                this.grdViewUserReviewLevel.Columns[i].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            }
        }
        #endregion
        #endregion




        private void grdViewProducts_ShowingEditor(object sender, CancelEventArgs e)
        {
            int intRowFocus = grdViewProducts.FocusedRowHandle;
            if (intRowFocus == GridControl.AutoFilterRowHandle)
                return;
            DataRow dr = grdViewProducts.GetFocusedDataRow();
            if (dr == null)
                return;
            if (grdViewProducts.FocusedColumn.FieldName == "QUANTITY"
                && grdViewProducts.FocusedColumn.OptionsColumn.AllowEdit
                && Convert.ToInt32(dr["ISREQUESTIMEI"]) == 1
                )
            {
                e.Cancel = true;
            }
            if (grdViewProducts.FocusedColumn.FieldName == "IMEI"
                && grdViewProducts.FocusedColumn.OptionsColumn.AllowEdit
                && Convert.ToInt32(dr["ISREQUESTIMEI"]) == 0
                )
            {
                e.Cancel = true;
            }
        }
        #region Update Data
        private void CheckReviewLevel()
        {
            DataTable dtbUserReviewLevel = grdUserReviewLevel.DataSource as DataTable;
            DataRow[] rUserReviewLevel = dtbUserReviewLevel.Select("TRIM(UserName) = '" + Library.AppCore.SystemConfig.objSessionUser.UserName + "'");
            if (rUserReviewLevel.Length < 1)
            {
                #region
                txtContent.ReadOnly = true;
                grdViewProducts.OptionsBehavior.Editable = false;
                btnSave.Enabled = false;
                // btnDelete.Enabled = (intIsDelete == 1);
                if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
                {
                    chkIsReviewed.Checked = objInventory.IsReviewed;
                    if (objInventory.IsDeleted || objInventory.IsUpdateUnEvent)
                    {
                        dropDownBtnApprove.Enabled = false;

                        if (objInventory.IsDeleted)
                        {
                            btnDelete.Enabled = false;
                            chkIsDeleted.Checked = objInventory.IsDeleted;
                        }
                        if (objInventory.IsUpdateUnEvent)
                        {
                            mnuItemExplainUnEvent.Enabled = true;
                            if (objInventory.IsExplainUnEvent)
                            {
                                mnuEventHandlingUnEvent.Enabled = true;
                            }
                        }
                    }
                }
                #endregion

                DataRow[] drReview = dtbUserReviewLevel.Select("IsReviewed = 1");
                if (drReview.Length == dtbUserReviewLevel.Rows.Count)
                {
                    //txtUserName.Enabled = false;
                    //txtPassword.Enabled = false;
                    dropDownBtnApprove.Enabled = false;
                    //btnReviewStatus.Enabled = false;
                }

                return;
            }

            int intReviewLevelID = 0;
            bool bolIsCanAddNewProduct = false;
            bool bolIsCanEditProduct = false;
            bool bolIsCanRemoveProduct = false;
            bool bolIsCanDeleteInventory = false;

            bool bolIsReviewed = false;
            for (int i = 0; i < dtbUserReviewLevel.Rows.Count; i++)
            {
                if (Convert.ToString(dtbUserReviewLevel.Rows[i]["UserName"]).Trim() == Library.AppCore.SystemConfig.objSessionUser.UserName)
                {
                    if (Convert.ToBoolean(dtbUserReviewLevel.Rows[i]["IsReviewed"]))
                    {
                        bolIsReviewed = true;
                        continue;
                    }
                    intReviewLevelID = Convert.ToInt32(dtbUserReviewLevel.Rows[i]["ReviewLevelID"]);
                    break;
                }
            }

            if (intReviewLevelID < 1)
            {
                if (bolIsReviewed)
                {
                    for (int i = dtbUserReviewLevel.Rows.Count - 1; i > -1; i--)
                    {
                        if (Convert.ToString(dtbUserReviewLevel.Rows[i]["UserName"]).Trim() == Library.AppCore.SystemConfig.objSessionUser.UserName)
                        {
                            if (Convert.ToBoolean(dtbUserReviewLevel.Rows[i]["IsReviewed"]))
                            {
                                intReviewLevelID = Convert.ToInt32(dtbUserReviewLevel.Rows[i]["ReviewLevelID"]);
                                break;
                            }
                        }
                    }
                }
                if (intReviewLevelID < 1)
                {
                    #region
                    txtContent.ReadOnly = true;
                    grdViewProducts.OptionsBehavior.Editable = false;
                    btnSave.Enabled = false;
                    //btnDelete.Enabled = (intIsDelete == 1);
                    if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
                    {
                        if (objInventory.IsDeleted || objInventory.IsUpdateUnEvent)
                        {
                            dropDownBtnApprove.Enabled = false;

                            if (objInventory.IsDeleted)
                            {
                                btnDelete.Enabled = false;
                                chkIsDeleted.Checked = objInventory.IsDeleted;
                            }
                            if (objInventory.IsUpdateUnEvent)
                            {
                                mnuItemExplainUnEvent.Enabled = true;
                                if (objInventory.IsExplainUnEvent)
                                {
                                    mnuEventHandlingUnEvent.Enabled = true;
                                }
                            }
                        }
                    }
                    #endregion

                    DataRow[] drReview = dtbUserReviewLevel.Select("IsReviewed = 1");
                    if (drReview.Length == dtbUserReviewLevel.Rows.Count)
                    {
                        //txtUserName.Enabled = false;
                        //txtPassword.Enabled = false;
                        dropDownBtnApprove.Enabled = false;
                        // btnReviewStatus.Enabled = false;
                    }

                    return;
                }
            }

            for (int i = 0; i < dtbReviewLevel.Rows.Count; i++)
            {
                if (Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]) == intReviewLevelID)
                {
                    if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCanAddNewProduct"]))
                    {
                        bolIsCanAddNewProduct = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCanAddNewProduct"]);
                    }
                    if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCanEditProduct"]))
                    {
                        bolIsCanEditProduct = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCanEditProduct"]);
                    }
                    if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCanRemoveProduct"]))
                    {
                        bolIsCanRemoveProduct = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCanRemoveProduct"]);
                    }
                    if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCanDeleteInventory"]))
                    {
                        bolIsCanDeleteInventory = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCanDeleteInventory"]);
                    }
                    break;
                }
            }

            //intIsAddProduct_ReviewLevel = Convert.ToInt32(bolIsCanAddNewProduct);
            //intIsEditProduct_ReviewLevel = Convert.ToInt32(bolIsCanEditProduct);
            //intIsRemoveProduct_ReviewLevel = Convert.ToInt32(bolIsCanRemoveProduct);
            //btnDelete.Enabled = (bolIsCanDeleteInventory && (intIsDelete == 1));

            if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
            {
                txtContent.ReadOnly = true;
                grdViewProducts.OptionsBehavior.Editable = false;
                btnSave.Enabled = false;

                if (objInventory.IsDeleted || objInventory.IsUpdateUnEvent)
                {
                    dropDownBtnApprove.Enabled = false;

                    if (objInventory.IsDeleted)
                    {
                        btnDelete.Enabled = false;
                        chkIsDeleted.Checked = objInventory.IsDeleted;
                    }
                    if (objInventory.IsUpdateUnEvent)
                    {
                        mnuItemExplainUnEvent.Enabled = true;
                        if (objInventory.IsExplainUnEvent)
                        {
                            mnuEventHandlingUnEvent.Enabled = true;
                        }
                    }
                }
                //else
                //{
                //    if (intIsEdit == 1)
                //    {
                //        txtContent.ReadOnly = false;
                //        cboCabinet.Enabled = true;
                //        flexDetail.AllowEditing = true;
                //        if (intIsAddProduct_ReviewLevel == 1)
                //        {
                //            txtBarcode.Enabled = true;
                //            btnSearch.Enabled = true;
                //        }
                //        btnSave.Enabled = true;
                //    }
                //}
                DataRow[] drReview = dtbUserReviewLevel.Select("IsReviewed = 1");
                if (drReview.Length == dtbUserReviewLevel.Rows.Count)
                {
                    //txtUserName.Enabled = false;
                    //txtPassword.Enabled = false;
                    dropDownBtnApprove.Enabled = false;
                    //btnReviewStatus.Enabled = false;
                }

            }

        }

        private bool CheckUpdate()
        {
            //if (dtbDataProducts.Rows.Count < 1)
            //{
            //    MessageBox.Show(this, "Vui lòng nhập dữ liệu kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return false;
            //}
            //for (int i = 0; i < grdViewProducts.RowCount; i++)
            //{
            //    DataRow rDetail = dtbDataProducts.Rows[i];
            //    string strProductID = (rDetail["PRODUCTID"] ?? "").ToString().Trim();
            //    string strProductName = (rDetail["PRODUCTNAME"] ?? "").ToString().Trim();
            //    string strIMEI = (rDetail["IMEI"] ?? "").ToString().Trim();
            //    decimal decQuantity = Convert.ToDecimal((rDetail["QUANTITY"] ?? "").ToString().Trim());
            //    int intProductStatusID = Convert.ToInt32((rDetail["PRODUCTSTATUSID"] ?? "").ToString().Trim());
            //    int intCabinet = Convert.ToInt32(rDetail["CABINETNUMBER"]);
            //    int intIsRequestIMEI = Convert.ToInt32((rDetail["ISREQUESTIMEI"] ?? "").ToString().Trim());
            //    string strINVENTORYDETAILID = (rDetail["INVENTORYDETAILID"] ?? "").ToString().Trim();
            //    int intSTT = Convert.ToInt32((rDetail["STT"] ?? "").ToString().Trim());
            //    if (intIsRequestIMEI == 1 && !string.IsNullOrEmpty(strIMEI))
            //    {
            //        DataTable dtbProductInfor = null;
            //        PLC.Inventory.WSInventory.ResultMessage objResultMessage // = new PLC.Inventory.WSInventory.ResultMessage();
            //                = objPLCInventory.INVProduct_SelInStock(ref dtbProductInfor, strIMEI, -1);// cboStoreID.StoreID);
            //        if (!objResultMessage.IsError
            //            && dtbProductInfor != null
            //            && dtbProductInfor.Rows.Count > 0)
            //        {
            //            intStoreID = Convert.ToInt32((dtbProductInfor.Rows[0]["STOREID"] ?? "").ToString().Trim());
            //            string strProductID_Check = (dtbProductInfor.Rows[0]["PRODUCTID"] ?? "").ToString().Trim();
            //            // Check san pham co thuoc nganh hang 
            //            if (intIsRequestIMEI == 1 && !string.IsNullOrEmpty((dtbProductInfor.Rows[0]["IMEI"] ?? "").ToString().Trim())
            //                && strProductID_Check == strProductID
            //                && intStoreID != cboStoreID.StoreID)
            //            {
            //                MessageBox.Show(this, "IMEI " + strIMEI + " không thuộc kho đang được kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //                grdViewProducts.FocusedRowHandle = i;
            //                grdViewProducts.FocusedColumn = grdViewProducts.Columns["IMEI"];
            //                grdViewProducts.GridControl.BeginInvoke((MethodInvoker)delegate { grdViewProducts.ShowEditor(); });
            //                return false;
            //            }
            //        }
            //    }



            //    if (intIsRequestIMEI == 1 && !string.IsNullOrEmpty(strIMEI))
            //    {
            //        var lstData = dtbDataProducts.AsEnumerable().Where(
            //                        row => (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == strProductID
            //                            && (row.Field<object>("IMEI") ?? "").ToString().Trim() == strIMEI
            //                            && (row.Field<object>("INVENTORYDETAILID") ?? "").ToString().Trim() != strINVENTORYDETAILID
            //                            && Convert.ToInt32((row.Field<object>("STT") ?? "").ToString().Trim()) < intSTT
            //                        );
            //        if (lstData.Any())
            //        {
            //            MessageBox.Show(this, "IMEI " + strIMEI + " đã tồn tại trong danh sách kiểm kê. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //            grdViewProducts.FocusedRowHandle = i;
            //            grdViewProducts.FocusedColumn = grdViewProducts.Columns["IMEI"];
            //            grdViewProducts.GridControl.BeginInvoke((MethodInvoker)delegate { grdViewProducts.ShowEditor(); });
            //            return false;
            //        }
            //    }
            //    if (string.IsNullOrEmpty(strIMEI) && intIsRequestIMEI == 1)
            //    {
            //        MessageBox.Show(this, "Vui lòng nhập IMEI cho sản phẩm này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        grdViewProducts.FocusedRowHandle = i;
            //        grdViewProducts.FocusedColumn = grdViewProducts.Columns["IMEI"];
            //        grdViewProducts.GridControl.BeginInvoke((MethodInvoker)delegate { grdViewProducts.ShowEditor(); });
            //        return false;
            //    }
            //    if (decQuantity < 1)
            //    {
            //        MessageBox.Show(this, "Số lượng sản phẩm không được phép âm hoặc bằng không!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        grdViewProducts.FocusedRowHandle = i;
            //        grdViewProducts.FocusedColumn = grdViewProducts.Columns["QUANTITY"];
            //        grdViewProducts.GridControl.BeginInvoke((MethodInvoker)delegate { grdViewProducts.ShowEditor(); });
            //        return false;
            //    }
            //}

            if (strInventoryID.Trim() == string.Empty || bolUpdate_IsAddReviewList && chkIsParent.Checked)
            {
                DataTable dtbUserReviewLevel = grdUserReviewLevel.DataSource as DataTable;
                if (dtbUserReviewLevel != null && dtbUserReviewLevel.Rows.Count > 0)
                {
                    if (dtbUserReviewLevel.Columns.Contains("ISSELECT"))
                    {
                        dtbUserReviewLevel.AcceptChanges();
                        DataRow[] rIsSelect = dtbUserReviewLevel.Select("ISSELECT = 1");
                        if (rIsSelect.Length < 1)
                        {
                            MessageBox.Show(this, "Vui lòng chọn người duyệt", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            tabControl1.SelectedTab = tabPage2;
                            return false;
                        }
                        for (int i = 0; i < dtbReviewLevel.Rows.Count; i++)
                        {
                            DataRow[] rIsSelectRvwLvl = dtbUserReviewLevel.Select("IsSelect = 1 AND ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]));
                            if (rIsSelectRvwLvl.Length < 1)
                            {
                                MessageBox.Show(this, "Vui lòng chọn người duyệt cho mức duyệt " + Convert.ToString(dtbReviewLevel.Rows[i]["ReviewLevelName"]).Trim(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                tabControl1.SelectedTab = tabPage2;
                                return false;
                            }
                            if (Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewType"]) == 0)
                            {
                                DataRow[] rCountReviewLevel = dtbUserReviewLevel.Select("ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]));
                                DataRow[] rCountSelect = dtbUserReviewLevel.Select("IsSelect = 1 AND ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]));
                                //if (rCountSelect.Length < rCountReviewLevel.Length)
                                //{
                                //    MessageBox.Show(this, "Mức duyệt " + Convert.ToString(dtbReviewLevel.Rows[i]["ReviewLevelName"]).Trim() + " có hình thức duyệt là tất cả phải duyệt. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                //    tabControl1.SelectedTab = tabPage2;
                                //    return false;
                                //}
                            }
                        }
                    }
                }
            }

            return true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if ((!btnSave.Enabled) || (!btnSave.Visible))
            {
                return;
            }
            btnSave.Enabled = false;

            grdViewProducts.CloseEditor();
            grdViewProducts.UpdateCurrentRow();
            try
            {
                dtbDataProducts.AcceptChanges();

                if (!CheckUpdate())
                {
                    btnSave.Enabled = true;
                    return;
                }

                if (objInventory == null || Convert.ToString(objInventory.InventoryID).Trim() == string.Empty)
                    objInventory = new PLC.Inventory.WSInventory.Inventory();
                objInventory.InventoryID = txtInventoryID.Text;
                objInventory.InventoryTermID = objInventoryTerm.InventoryTermID;
                objInventory.InventoryStoreID = intStoreID;
                objInventory.CreatedStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                objInventory.MainGroupIDList = cboMainGroupIDList.MainGroupIDList;
                objInventory.SubGroupIDList = cboSubGroupIDList.SubGroupIDList;
                objInventory.InventoryDate = (DateTime)objInventoryTerm.InventoryDate;
                objInventory.BeginInventoryTime = (DateTime)objInventoryTerm.BeginInventoryTime;
                objInventory.EndInventoryTime = (DateTime)objInventoryTerm.LockDataTime;
                objInventory.Content = Convert.ToString(txtContent.Text).Trim();
                objInventory.ProductStatusID = Convert.ToInt32(cboProductStateID.SelectedValue);
                objInventory.InventoryUser = strInventoryUser;
                objInventory.CreatedUser = Library.AppCore.SystemConfig.objSessionUser.UserName;
                List<ERP.Inventory.PLC.Inventory.WSInventory.InventoryDetail> objInventoryDetailList = new List<PLC.Inventory.WSInventory.InventoryDetail>();
                if (objInventoryType == null || objInventoryType.InventoryTypeID < 1)
                {
                    objPLCInventoryType.LoadInfo(ref objInventoryType, intInventoryTypeID);
                }
                objInventory.InventoryTypeID = objInventoryType.InventoryTypeID;
                objInventory.IsAutoReview = objInventoryType.IsAutoReview;
                string strRef_InventoryID = string.Empty;
                if (!objPLCInventory.AddInventoryAFJoin(objInventory, (DataTable)grdUserReviewLevel.DataSource, ref strRef_InventoryID))
                {
                    btnSave.Enabled = true;
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                //if (objInventory.IsAutoReview && (!objInventory.IsReviewed))
                //{
                //    objInventory.IsReviewed = true;
                //    chkIsReviewed.Checked = objInventory.IsReviewed;
                //}

                ////txtUserName.Enabled = true;
                ////txtPassword.Enabled = true;
                ////dropDownBtnApprove.Enabled = true;
                //// btnReviewStatus.Enabled = true;
                ////if (bolIsCreateNew)
                ////{
                //DataTable dtbInventoryRvwLevel = objPLCInventory.GetInventoryReviewLevel(objInventory.InventoryID);
                //if (!dtbInventoryRvwLevel.Columns.Contains("ISSELECT"))
                //{
                //    dtbInventoryRvwLevel.Columns.Add("ISSELECT", typeof(Decimal));
                //    dtbInventoryRvwLevel.Columns["ISSELECT"].DefaultValue = Convert.ToDecimal(0);
                //}
                //grdUserReviewLevel.DataSource = dtbInventoryRvwLevel;
                ////CustomFlexUserRvwLvl();
                ////flexUserReviewLevel.Cols["IsSelect"].Visible = false;
                ////FormatGridUserReviewLevel(false);
                ////}

                //CheckReviewLevel();

                //FormType = eFormType.VIEW;
                MessageBox.Show(this, "Cập nhật dữ liệu kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (!InOutProductExce())
                {
                    btnSave.Enabled = true;
                    return;
                }
                frmInventoryVTS_Load(null, null);
            }
            catch
            {
                MessageBox.Show(this, "Lỗi lấy thông tin dữ liệu kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnSave.Enabled = true;
            }
        }
        #endregion

        private void mnuItemDeleteProduct_Click(object sender, EventArgs e)
        {
            int intRowFocus = grdViewProducts.FocusedRowHandle;
            if (intRowFocus == GridControl.AutoFilterRowHandle)
                return;
            DataRow dr = grdViewProducts.GetFocusedDataRow();
            dtbDataProducts.Rows.Remove(dr);
            dtbDataProducts.AcceptChanges();
        }

        private void mnuFlexDetail_Opening(object sender, CancelEventArgs e)
        {
            if (FormType == eFormType.VIEW)
                e.Cancel = true;
        }


        private void btnApprove_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnReviewStatus_Click(2, "Đồng ý");
        }

        private void btnUnApprove_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnReviewStatus_Click(3, "Từ chối");
        }

        bool InOutProductExce()
        {
            var objKeywords = new object[] {
                "@INVENTORYTERMID", objInventory.InventoryTermID,
                "@STOREID", cboStoreID.StoreID
                };
            Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
            DataTable dtbConflict = objPLCReportDataSource.GetDataSource("INV_TERMCALINOUTDATA", objKeywords);

            return true;
        }
        private void btnReviewStatus_Click(int intStatusID, string strStatusName)
        {
            //int intInventoryCount = objPLCInventory.CountSimilarInventory(objInventoryTerm.InventoryTermID, intStoreID, intMainGroupID, Convert.ToInt32(cboProductStateID.SelectedValue), intSubGroupID);
            //if (intInventoryCount > 1)
            //{
            //    MessageBox.Show(this, "Tồn tại " + intInventoryCount.ToString() + " phiếu kiểm kê cùng kho " + Convert.ToString(cboStore.Text).Trim() + "\nVui lòng nối các phiếu này lại với nhau trước khi duyệt", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}

            #region Duyet

            dropDownBtnApprove.Enabled = false;
            List<string> ListUserName = new List<string>();
            DataTable dtbUserReviewLevel = grdUserReviewLevel.DataSource as DataTable;
            foreach (DataRow dr in dtbUserReviewLevel.Rows)
            {
                ListUserName.Add((dr["UserName"] ?? "").ToString().Trim());
            }
            Library.Login.frmCertifyLogin frmCertifyLogin = new Library.Login.frmCertifyLogin("Xác nhận duyệt kiểm kê", "!\nChỉ nhân viên trong danh sách duyệt mới được xác nhận!", ListUserName, null, true, true);
            frmCertifyLogin.ShowDialog(this);
            if (!frmCertifyLogin.IsDisposed && !frmCertifyLogin.IsCorrect)
            {
                dropDownBtnApprove.Enabled = true;
                return;
            }
            string strUserName = frmCertifyLogin.UserName;
            string strFullName = frmCertifyLogin.FullName;
            //if (Convert.ToString(txtUserName.Text).Trim() != string.Empty)
            //{
            //    if (Convert.ToString(txtPassword.Text).Trim() == string.Empty)
            //    {
            //        txtPassword.Focus();
            //        MessageBox.Show(this, "Vui lòng nhập mật khẩu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        btnReviewStatus.Enabled = true;
            //        return;
            //    }
            //    Library.AppCore.SessionUser objSessionUser = new Library.AppCore.SessionUser(Convert.ToString(txtUserName.Text).Trim(), Convert.ToString(txtPassword.Text).Trim(), true);
            //    objSessionUser.Login();
            //    if (!objSessionUser.IsLogin)
            //    {
            //        txtUserName.Focus();
            //        txtUserName.SelectAll();
            //        txtPassword.Text = string.Empty;
            //        MessageBox.Show(this, "Tên đăng nhập hoặc mật khẩu không đúng. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        btnReviewStatus.Enabled = true;
            //        return;
            //    }
            //    strUserName = Convert.ToString(txtUserName.Text).Trim();
            //    strFullName = Convert.ToString(objSessionUser.FullName).Trim();
            //}
            //if (strUserName == string.Empty)
            //{
            //    strUserName = Library.AppCore.SystemConfig.objSessionUser.UserName;
            //    strFullName = Convert.ToString(Library.AppCore.SystemConfig.objSessionUser.FullName).Trim();
            //}


            //if (rUserReviewLevel.Length < 1)
            //{
            //    MessageBox.Show(this, "Người dùng [" + strUserName + "] không nằm trong danh sách duyệt. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    btnReviewStatus.Enabled = true;
            //    return;
            //}
            int intReviewLevelID = 0;
            bool bolIsCheckStorePermission = false;
            int intReviewType = 0;
            string strRvwFunctionID = string.Empty;
            int intIndexRvl = -1;
            for (int i = 0; i < dtbUserReviewLevel.Rows.Count; i++)
            {
                if (Convert.ToString(dtbUserReviewLevel.Rows[i]["UserName"]).Trim() == strUserName)
                {
                    if (Convert.ToBoolean(dtbUserReviewLevel.Rows[i]["IsReviewed"]))
                    {
                        continue;
                    }
                    intReviewLevelID = Convert.ToInt32(dtbUserReviewLevel.Rows[i]["ReviewLevelID"]);
                    intIndexRvl = i;
                    break;
                }
            }

            if (intReviewLevelID < 1)
            {
                MessageBox.Show(this, "Người dùng [" + strUserName + "] đã duyệt rồi. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dropDownBtnApprove.Enabled = true;
                return;
            }

            for (int i = 0; i < dtbReviewLevel.Rows.Count; i++)
            {
                if (Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]) == intReviewLevelID)
                {
                    if (i + 1 < dtbReviewLevel.Rows.Count)
                    {
                        DataRow[] rIsHasUserRvwNextLvl = dtbUserReviewLevel.Select("ReviewStatus >0 AND ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i + 1]["ReviewLevelID"]));
                        if (rIsHasUserRvwNextLvl.Length > 0)
                        {
                            MessageBox.Show(this, "Mức duyệt [" + Convert.ToString(dtbReviewLevel.Rows[i + 1]["ReviewLevelName"]).Trim() + "] đã có người duyệt. Không thể cập nhật trạng thái duyệt mức hiện tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            dropDownBtnApprove.Enabled = true;
                            return;
                        }
                    }
                    if (i > 0)
                    {
                        DataRow[] rIsDeniedUserRvwLvl = dtbUserReviewLevel.Select("ReviewStatus = 3");
                        if (rIsDeniedUserRvwLvl.Length > 0)
                        {
                            MessageBox.Show(this, "Phiếu kiểm kê [" + objInventory.InventoryID + "] đã bị duyệt chối. Không thể cập nhật trạng thái duyệt!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            dropDownBtnApprove.Enabled = true;
                            return;
                        }
                        DataRow[] rIsReviewedUserRvwLvl = dtbUserReviewLevel.Select("IsReviewed = 1 AND ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i - 1]["ReviewLevelID"]));

                        if (rIsReviewedUserRvwLvl.Length < 1)
                        {
                            MessageBox.Show(this, "Mức duyệt [" + Convert.ToString(dtbReviewLevel.Rows[i - 1]["ReviewLevelName"]).Trim() + "] chưa được duyệt. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            dropDownBtnApprove.Enabled = true;
                            return;
                        }
                        else
                        {
                            if (Convert.ToInt32(dtbReviewLevel.Rows[i - 1]["ReviewType"]) == 0)
                            {
                                if (rIsReviewedUserRvwLvl.Length < (dtbUserReviewLevel.Select("ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i - 1]["ReviewLevelID"])).Length))
                                {
                                    MessageBox.Show(this, "Mức duyệt [" + Convert.ToString(dtbReviewLevel.Rows[i - 1]["ReviewLevelName"]).Trim() + "] phải được tất cả duyệt. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    dropDownBtnApprove.Enabled = true;
                                    return;
                                }
                            }
                        }
                    }

                    intReviewType = Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewType"]);
                    if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["ReviewFunctionID"]))
                    {
                        strRvwFunctionID = Convert.ToString(dtbReviewLevel.Rows[i]["ReviewFunctionID"]).Trim();
                    }
                    if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCheckStorePermission"]))
                    {
                        bolIsCheckStorePermission = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCheckStorePermission"]);
                    }
                    break;
                }
            }

            ERP.MasterData.SYS.PLC.PLCUser objPLCUser = new MasterData.SYS.PLC.PLCUser();
            if (bolIsCheckStorePermission)
            {
                DataTable dtbStorePermission = objPLCUser.GetDataUser_Store(strUserName);
                DataRow[] rStorePermission = dtbStorePermission.Select("IsSelect > 0 AND StoreID = " + Convert.ToInt32(cboStoreID.StoreID));
                if (rStorePermission.Length < 1)
                {
                    MessageBox.Show(this, "Mã nhân viên kiểm kê [" + strUserName + "] không có quyền trên kho [" + Convert.ToString(cboStoreID.StoreName).Trim() + "]. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            DataTable dtbMainGroupSource = null;
            ERP.MasterData.PLC.MD.PLCMainGroup objPLCMainGroup = new MasterData.PLC.MD.PLCMainGroup();
            objPLCMainGroup.GetMainGroup(ref dtbMainGroupSource, strUserName);//dtbMainGroupSource = objPLCUser.GetDataUserMainGroup(Convert.ToString(txtUserName.Text).Trim());
            //DataRow[] rMainGroupPermission = dtbMainGroupSource.Select("HASRIGHT = 1 AND MAINGROUPID = " + Convert.ToInt32(cboMainGroup.SelectedValue));//dtbMainGroupSource.Select("IsSelect > 0 AND MainGroupID = " + Convert.ToInt32(cboMainGroup.SelectedValue));
            //if (rMainGroupPermission.Length < 1)
            //{
            //    MessageBox.Show(this, "Mã nhân viên kiểm kê [" + Convert.ToString(txtUserName.Text).Trim() + "] không có quyền trên ngành hàng [" + Convert.ToString(cboMainGroup.Text).Trim() + "]. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}

            ERP.Inventory.PLC.Inventory.WSInventory.Inventory_ReviewList objInventoryReview = new PLC.Inventory.WSInventory.Inventory_ReviewList();
            objInventoryReview.InventoryID = objInventory.InventoryID;
            objInventoryReview.ReviewLevelID = intReviewLevelID;
            objInventoryReview.UserName = strUserName;
            objInventoryReview.ReviewStatus = intStatusID;
            objInventoryReview.InventoryDate = objInventory.InventoryDate;
            objInventoryReview.ReviewedDate = Library.AppCore.Globals.GetServerDateTime();

            bool bolIsReviewedInventory = false;
            if (intReviewLevelID == Convert.ToInt32(dtbReviewLevel.Rows[dtbReviewLevel.Rows.Count - 1]["ReviewLevelID"]))
            {
                DataTable dtbInventoryRvwLevel = null;
                if (strRvwFunctionID != string.Empty)
                {
                    dtbInventoryRvwLevel = objPLCInventory.GetInventoryReviewLevel(objInventory.InventoryID);
                }
                if (intReviewType == 0)
                {
                    DataRow[] rUserRvwLvl = dtbInventoryRvwLevel.Select("ReviewLevelID = " + intReviewLevelID);
                    DataRow[] rReviewed = dtbInventoryRvwLevel.Select("IsReviewed = 1 AND ReviewLevelID = " + intReviewLevelID);
                    if (objInventoryReview.ReviewStatus == 2)
                    {
                        if (rReviewed.Length == (rUserRvwLvl.Length - 1))
                        {
                            bolIsReviewedInventory = true;
                        }
                    }
                }
                else
                {
                    //DataRow[] rReviewed = dtbInventoryRvwLevel.Select("IsReviewed = 1 AND ReviewLevelID = " + intReviewLevelID);
                    //if (rReviewed.Length > 0)
                    if (objInventoryReview.ReviewStatus == 2)
                    {
                        bolIsReviewedInventory = true;
                    }
                    else
                    {
                        if (dtbUserReviewLevel.Select("ReviewLevelID = " + intReviewLevelID).Length == 1)
                        {
                            bolIsReviewedInventory = true;
                        }
                    }
                }
            }

            objPLCInventory.UpdateInventoryReviewStatus(objInventoryReview, bolIsReviewedInventory);

            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            else
            {
                if ((bolIsReviewedInventory) && objInventoryReview.ReviewStatus == 2)
                {
                    objInventory.IsReviewed = true;
                    chkIsReviewed.Checked = objInventory.IsReviewed;
                }
            }

            if (intIndexRvl > -1 && intIndexRvl < dtbUserReviewLevel.Rows.Count)
            {
                dtbUserReviewLevel.Rows[intIndexRvl]["ReviewStatus"] = objInventoryReview.ReviewStatus;
                dtbUserReviewLevel.Rows[intIndexRvl]["StatusName"] = strStatusName;
                if (objInventoryReview.ReviewStatus == 2)
                {
                    dtbUserReviewLevel.Rows[intIndexRvl]["IsReviewed"] = true;
                    dtbUserReviewLevel.Rows[intIndexRvl]["ReviewedDate"] = objInventoryReview.ReviewedDate;
                }
                else
                {
                    dtbUserReviewLevel.Rows[intIndexRvl]["IsReviewed"] = false;
                    dtbUserReviewLevel.Rows[intIndexRvl]["ReviewedDate"] = DBNull.Value;
                }
                dtbUserReviewLevel.Rows[intIndexRvl]["UserName"] = strUserName;
                dtbUserReviewLevel.Rows[intIndexRvl]["FullName"] = strFullName;
            }

            //CheckReviewLevel();
            #endregion
            MessageBox.Show(this, "Cập nhật trạng thái duyệt phiếu kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            frmInventoryVTS_Load(null, null);
            if (objInventory.IsReviewed)
            {

                // Chot nhap xuat

                if (!InOutProductExce())
                {
                    btnSave.Enabled = true;
                    return;
                }
                var objKeywords = new object[] {
                "@INVENTORYTERMID", objInventory.InventoryTermID,
                "@STOREID", cboStoreID.StoreID
                };
                dtsInOutProduct = objPLCReportDataSource.GetDataSet("INV_INOUTDATA_SRH", new string[] { "V_OUT", "V_OUT2" }, objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    this.Close();
                }
                GetDataUnEvent();
                ERP.Inventory.PLC.Inventory.WSInventory.Inventory objInventory2 = new PLC.Inventory.WSInventory.Inventory();
                objInventory2.InventoryID = objInventory.InventoryID;
                objInventory2.InventoryDate = objInventory.InventoryDate;
                objInventory2.CreatedStoreID = objInventory.CreatedStoreID;
                dtbUnEvent.TableName = "UNEVENTTABLE";
                objPLCInventory.AddInventoryUnEvent(objInventory2, Library.AppCore.DataTableClass.Select(dtbUnEvent.Copy(), "Different <> 0 OR ERP_PRODUCTSTATUSID <> INV_PRODUCTSTATUSID"));
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {

                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnSave.Enabled = true;
                    return;
                }
                InitiallizeInStock();
                if (objInventory != null)
                {
                    objInventory.IsUpdateUnEvent = true;
                    if (objInventory.IsUpdateUnEvent)
                    {
                        mnuItemExplainUnEvent.Enabled = true;
                    }
                    if (objInventory.IsExplainUnEvent && objInventory.IsExplainAccess)
                    {
                        mnuEventHandlingUnEvent.Enabled = true;
                    }
                }
            }

            //txtPassword.Text = string.Empty;
            // FormType = eFormType.VIEW;
            //dropDownBtnApprove.Enabled = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(txtInventoryID.Text).Trim() == string.Empty)
            {
                return;
            }

            string strInventoryProcessID = string.Empty;
            ERP.Inventory.PLC.Inventory.PLCInventoryProcess objPLCInventoryProcess = new PLC.Inventory.PLCInventoryProcess();
            strInventoryProcessID = objPLCInventoryProcess.CheckExistInventoryProcess(objInventory.InventoryID).Trim();
            if (strInventoryProcessID != string.Empty)
            {
                MessageBox.Show(this, "Phiếu kiểm kê này đã được xử lý kiểm kê. Vui lòng xóa phiếu yêu cầu xử lý kiểm kê " + strInventoryProcessID + " trước", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string strContentDelete = ShowReason("Nội dung hủy", 500);
            if (strContentDelete == "-1" || string.IsNullOrWhiteSpace(strContentDelete))
                return;
            if (!objPLCInventory.DeleteInventory(Convert.ToString(txtInventoryID.Text).Trim(), strContentDelete))
            {
                btnDelete.Enabled = true;
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
            }

            MessageBox.Show(this, "Hủy phiếu kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            btnDelete.Enabled = false;
            this.Close();
        }

        private string ShowReason(string strTitle, int MaxLengthString)
        {
            string strReasion = "-1";
            Library.AppControl.FormCommon.frmInputString frm = new Library.AppControl.FormCommon.frmInputString();
            frm.MaxLengthString = MaxLengthString;
            frm.IsAllowEmpty = false;
            frm.Text = strTitle;
            frm.ShowDialog();
            if (frm.IsAccept)
            {
                strReasion = frm.ContentString;
            }

            return strReasion;
        }



        #region Lay thong tin ton kho
        void GetDataUnEvent()
        {
            grdViewProducts.CloseEditor();
            grdViewProducts.UpdateCurrentRow();
            dtbDataProducts.AcceptChanges();
            InitiallizeDataUnEvent();

            if (!dtbUnEvent.Columns.Contains("UNEVENTEXPLAIN"))
                dtbUnEvent.Columns.Add("UNEVENTEXPLAIN", typeof(string));
            if (!dtbUnEvent.Columns.Contains("CAUSEGROUPSLIST"))
                dtbUnEvent.Columns.Add("CAUSEGROUPSLIST", typeof(string));
            if (!dtbUnEvent.Columns.Contains("EXPLAINNOTE"))
                dtbUnEvent.Columns.Add("EXPLAINNOTE", typeof(string));
            if (!dtbUnEvent.Columns.Contains("UNEVENTID"))
                dtbUnEvent.Columns.Add("UNEVENTID", typeof(string));
            if (!dtbUnEvent.Columns.Contains("ISSYSTEMERROR"))
            {
                dtbUnEvent.Columns.Add("ISSYSTEMERROR", typeof(int));
                dtbUnEvent.Columns["ISSYSTEMERROR"].DefaultValue = 0;
            }
            if (!dtbUnEvent.Columns.Contains("EXPLAINSYSTEMNOTE"))
                dtbUnEvent.Columns.Add("EXPLAINSYSTEMNOTE", typeof(string));
            if (!dtbUnEvent.Columns.Contains("ISREQUESTIMEI"))
                dtbUnEvent.Columns.Add("ISREQUESTIMEI", typeof(int));

            if (!dtbUnEvent.Columns.Contains("QUANTITY_SALE"))
            {
                dtbUnEvent.Columns.Add("QUANTITY_SALE", typeof(decimal));
                dtbUnEvent.Columns["QUANTITY_SALE"].DefaultValue = 0;
            }
            if (!dtbUnEvent.Columns.Contains("QUANTITY_INPUT"))
            {
                dtbUnEvent.Columns.Add("QUANTITY_INPUT", typeof(decimal));
                dtbUnEvent.Columns["QUANTITY_INPUT"].DefaultValue = 0;
            }
            if (!dtbUnEvent.Columns.Contains("QUANTITY_STORECHANGE"))
            {
                dtbUnEvent.Columns.Add("QUANTITY_STORECHANGE", typeof(decimal));
                dtbUnEvent.Columns["QUANTITY_STORECHANGE"].DefaultValue = 0;
            }
            if (!dtbUnEvent.Columns.Contains("QUANTITY_OUTPUT"))
            {
                dtbUnEvent.Columns.Add("QUANTITY_OUTPUT", typeof(decimal));
                dtbUnEvent.Columns["QUANTITY_OUTPUT"].DefaultValue = 0;
            }

            if (!dtbUnEvent.Columns.Contains("QUANTITY_DIFFERENT"))
            {
                dtbUnEvent.Columns.Add("QUANTITY_DIFFERENT", typeof(decimal));
                dtbUnEvent.Columns["QUANTITY_DIFFERENT"].DefaultValue = 0;
            }

            #region nap du lieu ton kho
            if (dtbInStockProduct != null)
            {
                foreach (DataRow dr in dtbInStockProduct.Rows)
                {
                    string strPRODUCTID = (dr["PRODUCTID"] ?? "").ToString().Trim();
                    string strPRODUCTNAME = (dr["PRODUCTNAME"] ?? "").ToString().Trim();
                    // string strERP_IMEI = (dr["ERP_IMEI"] ?? "").ToString().Trim();
                    int intERP_QUANTITY = Convert.ToInt32((dr["QUANTITY"] ?? "").ToString().Trim());
                    int intERP_PRODUCTSTATUSID = Convert.ToInt32((dr["PRODUCTSTATUSID"] ?? "").ToString().Trim());
                    string strERP_PRODUCTSTATUSNAME = (dr["PRODUCTSTATUSNAME"] ?? "").ToString().Trim();

                    // Nap du lieu vao bang 
                    DataRow drNew = dtbUnEvent.NewRow();
                    drNew["PRODUCTID"] = strPRODUCTID;
                    drNew["PRODUCTNAME"] = strPRODUCTNAME;
                    drNew["ERP_IMEI"] = string.Empty;
                    drNew["ERP_QUANTITY"] = intERP_QUANTITY;
                    drNew["ERP_PRODUCTSTATUSID"] = intERP_PRODUCTSTATUSID;
                    drNew["ERP_PRODUCTSTATUSNAME"] = strERP_PRODUCTSTATUSNAME;
                    dtbUnEvent.Rows.Add(drNew);
                }
            }

            if (dtbInStockProductIMEI != null)
            {
                foreach (DataRow dr in dtbInStockProductIMEI.Rows)
                {
                    string strPRODUCTID = (dr["PRODUCTID"] ?? "").ToString().Trim();
                    string strPRODUCTNAME = (dr["PRODUCTNAME"] ?? "").ToString().Trim();
                    string strERP_IMEI = (dr["IMEI"] ?? "").ToString().Trim();
                    int intERP_QUANTITY = Convert.ToInt32((dr["QUANTITY"] ?? "").ToString().Trim());
                    int intERP_PRODUCTSTATUSID = Convert.ToInt32((dr["PRODUCTSTATUSID"] ?? "").ToString().Trim());
                    string strERP_PRODUCTSTATUSNAME = (dr["PRODUCTSTATUSNAME"] ?? "").ToString().Trim();

                    // Nap du lieu vao bang 
                    DataRow drNew = dtbUnEvent.NewRow();
                    drNew["PRODUCTID"] = strPRODUCTID;
                    drNew["PRODUCTNAME"] = strPRODUCTNAME;
                    drNew["ERP_IMEI"] = strERP_IMEI;
                    drNew["ERP_QUANTITY"] = intERP_QUANTITY;
                    drNew["ERP_PRODUCTSTATUSID"] = intERP_PRODUCTSTATUSID;
                    drNew["ERP_PRODUCTSTATUSNAME"] = strERP_PRODUCTSTATUSNAME;
                    dtbUnEvent.Rows.Add(drNew);
                }
            }
            #endregion

            #region Nap du lieu kiem ke
            foreach (DataRow dr in dtbDataProducts.Rows)
            {
                string strPRODUCTID = (dr["PRODUCTID"] ?? "").ToString().Trim();
                string strPRODUCTNAME = (dr["PRODUCTNAME"] ?? "").ToString().Trim();
                string strINV_IMEI = (dr["IMEI"] ?? "").ToString().Trim();
                int intINV_QUANTITY = Convert.ToInt32((dr["QUANTITY"] ?? "").ToString().Trim());
                int intINV_IsRequestIMEI = Convert.ToInt32((dr["ISREQUESTIMEI"] ?? "").ToString().Trim());
                int intINV_PRODUCTSTATUSID = Convert.ToInt32((dr["PRODUCTSTATUSID"] ?? "").ToString().Trim());
                string strINV_PRODUCTSTATUSNAME = (dr["PRODUCTSTATUSNAME"] ?? "").ToString().Trim();
                if (FormType == eFormType.ADD)
                    strINV_PRODUCTSTATUSNAME = cboProductStateID.SelectedText;
                var varResult = dtbUnEvent.AsEnumerable()
                    .Where(row => (row.Field<string>("PRODUCTID") ?? "").ToString().Trim() == strPRODUCTID
                             && ((row.Field<string>("ERP_IMEI") ?? "").ToString().Trim() == strINV_IMEI || intINV_IsRequestIMEI == 0)
                             && (row.Field<int>("ERP_PRODUCTSTATUSID") == intINV_PRODUCTSTATUSID || intINV_IsRequestIMEI == 1)

                             );
                if (varResult.Any())
                {
                    DataRow drSearch = varResult.FirstOrDefault();
                    drSearch["INV_IMEI"] = strINV_IMEI;
                    drSearch["INV_QUANTITY"] = intINV_QUANTITY;
                    drSearch["INV_PRODUCTSTATUSID"] = intINV_PRODUCTSTATUSID;
                    drSearch["INV_PRODUCTSTATUSNAME"] = strINV_PRODUCTSTATUSNAME;
                }
                else
                {
                    DataRow drNew = dtbUnEvent.NewRow();
                    drNew["PRODUCTID"] = strPRODUCTID;
                    drNew["PRODUCTNAME"] = strPRODUCTNAME;
                    drNew["INV_IMEI"] = strINV_IMEI;
                    drNew["INV_QUANTITY"] = intINV_QUANTITY;
                    drNew["INV_PRODUCTSTATUSID"] = intINV_PRODUCTSTATUSID;
                    drNew["INV_PRODUCTSTATUSNAME"] = strINV_PRODUCTSTATUSNAME;
                    dtbUnEvent.Rows.Add(drNew);
                }
            }
            #endregion
            foreach (DataRow rUnEvent in dtbUnEvent.Rows)
            {
                int intERP_QUANTITY = Convert.ToInt32((rUnEvent["ERP_QUANTITY"] ?? "").ToString().Trim());
                int intINV_QUANTITY = Convert.ToInt32((rUnEvent["INV_QUANTITY"] ?? "").ToString().Trim());
                rUnEvent["DIFFERENT"] = intERP_QUANTITY - intINV_QUANTITY;


                // giai trinh chech lenh tu he thong 
                DataTable dtbInOut_NONEIMEI = dtsInOutProduct.Tables[0].Copy();
                DataTable dtbInOut_IMEI = dtsInOutProduct.Tables[1].Copy();
                string strProductID = (rUnEvent["PRODUCTID"] ?? "").ToString().Trim();
                string strERP_IMEI = (rUnEvent["ERP_IMEI"] ?? "").ToString().Trim();
                string strINV_IMEI = (rUnEvent["INV_IMEI"] ?? "").ToString().Trim();
                int intERP_ProductStatusID = Convert.ToInt32((rUnEvent["ERP_PRODUCTSTATUSID"] ?? "").ToString().Trim());
                int intINV_ProductStatusID = Convert.ToInt32((rUnEvent["INV_PRODUCTSTATUSID"] ?? "").ToString().Trim());

                bool bolHaveIMEI = false;
                if (!string.IsNullOrEmpty(strERP_IMEI)
                    || !string.IsNullOrEmpty(strINV_IMEI))
                    bolHaveIMEI = true;
                if (bolHaveIMEI)
                {
                    string strIMEI = strERP_IMEI;
                    if (string.IsNullOrEmpty(strERP_IMEI)
                        && !string.IsNullOrEmpty(strINV_IMEI))
                        strIMEI = strINV_IMEI;
                    var varResult = dtbInOut_IMEI.AsEnumerable()
                       .Where(row => (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == strProductID
                                  && (row.Field<object>("IMEI") ?? "").ToString().Trim() == strIMEI);
                    if (varResult.Any()) // => Có tồn tại giải trình
                    {
                        DataRow drOUT = varResult.FirstOrDefault();
                        rUnEvent["QUANTITY_INPUT"] = drOUT["QUANTITY_INPUT"];
                        rUnEvent["QUANTITY_SALE"] = drOUT["QUANTITY_SALE"];
                        rUnEvent["QUANTITY_STORECHANGE"] = drOUT["QUANTITY_STORECHANGE"];
                        rUnEvent["QUANTITY_OUTPUT"] = drOUT["QUANTITY_OUTPUT"];
                        rUnEvent["ISSYSTEMERROR"] = 1;
                        rUnEvent["UNEVENTEXPLAIN"] = (drOUT["NOTE"] ?? "").ToString().Trim();
                        rUnEvent["CAUSEGROUPSLIST"] = (drOUT["CAUSEGROUPSLIST"] ?? "").ToString().Trim();
                        rUnEvent["EXPLAINSYSTEMNOTE"] = (drOUT["NOTE"] ?? "").ToString().Trim();
                    }
                }
                else
                {
                    int intProductStatusID = intERP_ProductStatusID;
                    if (intERP_ProductStatusID < 0)
                        intProductStatusID = intINV_ProductStatusID;

                    var varResult = dtbInOut_NONEIMEI.AsEnumerable()
                       .Where(row => (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == strProductID
                                  && Convert.ToInt32((row.Field<object>("PRODUCTSTATUSID") ?? "").ToString().Trim()) == intProductStatusID
                       );
                    if (varResult.Any()) // => Có tồn tại giải trình
                    {
                        DataRow drOUT = varResult.FirstOrDefault();
                        rUnEvent["QUANTITY_INPUT"] = drOUT["QUANTITY_INPUT"];
                        rUnEvent["QUANTITY_SALE"] = drOUT["QUANTITY_SALE"];
                        rUnEvent["QUANTITY_STORECHANGE"] = drOUT["QUANTITY_STORECHANGE"];
                        rUnEvent["QUANTITY_OUTPUT"] = drOUT["QUANTITY_OUTPUT"];
                        rUnEvent["ISSYSTEMERROR"] = 1;
                        rUnEvent["UNEVENTEXPLAIN"] = (drOUT["NOTE"] ?? "").ToString().Trim();
                        rUnEvent["CAUSEGROUPSLIST"] = (drOUT["CAUSEGROUPSLIST"] ?? "").ToString().Trim();
                        rUnEvent["EXPLAINSYSTEMNOTE"] = (drOUT["NOTE"] ?? "").ToString().Trim();
                    }
                }
                rUnEvent["QUANTITY_DIFFERENT"] =
                    Convert.ToDecimal(rUnEvent["ERP_QUANTITY"]) + Convert.ToDecimal(rUnEvent["QUANTITY_INPUT"]) - (Convert.ToDecimal(rUnEvent["INV_QUANTITY"]) + Convert.ToDecimal(rUnEvent["QUANTITY_SALE"]) + Convert.ToDecimal(rUnEvent["QUANTITY_OUTPUT"]) + Convert.ToDecimal(rUnEvent["QUANTITY_STORECHANGE"]))
                   ;
            }

        }
        private void mnuItemViewUnEvent_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
            // Chot du lieu giai trinh
            var objKeywords = new object[] {
                "@INVENTORYTERMID", objInventory.InventoryTermID,
                "@STOREID", cboStoreID.StoreID
                };
            if (!objInventory.IsExplainUnEvent)
            {
                DataTable dtbTemp = objPLCReportDataSource.GetDataSource("INV_TERMCALINOUTDATA_SRH", objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    this.Close();
                }
            }
         

            // Lay Danh Sach Giai Trinh

            dtsInOutProduct = objPLCReportDataSource.GetDataSet("INV_INOUTDATA_SRH", new string[] { "V_OUT", "V_OUT2" }, objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                this.Close();
            }

            // kiem tra ton kho theo chot ton 
            frmShowUnEventVTS frmShowUnEvent1 = new frmShowUnEventVTS();
            GetDataUnEvent();
            if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
            {
                frmShowUnEvent1.strInventoryID = objInventory.InventoryID;
                frmShowUnEvent1.dtmInventoryDate = (DateTime)objInventory.InventoryDate;

                frmShowUnEvent1.intProductInstockID = Convert.ToInt32(cboProductStateID.SelectedValue);
                frmShowUnEvent1.intCreatedStoreID = objInventory.CreatedStoreID;
                frmShowUnEvent1.bolIsReviewed = objInventory.IsReviewed;
                frmShowUnEvent1.bolIsUpdateUnEvent = objInventory.IsUpdateUnEvent;
                frmShowUnEvent1.bolIsDeleted = objInventory.IsDeleted;
            }
            else
            {
                frmShowUnEvent1.strInventoryID = txtInventoryID.Text;
                frmShowUnEvent1.dtmInventoryDate = (DateTime)objInventoryTerm.InventoryDate;
                frmShowUnEvent1.strStoreName = cboStoreID.StoreName;
                frmShowUnEvent1.strMainGroupName = cboMainGroupIDList.MainGroupName;
                frmShowUnEvent1.intProductInstockID = Convert.ToInt32(cboProductStateID.SelectedValue);
                frmShowUnEvent1.intCreatedStoreID = intStoreID;
                frmShowUnEvent1.bolIsReviewed = false;
                frmShowUnEvent1.bolIsUpdateUnEvent = false;
                frmShowUnEvent1.bolIsDeleted = false;
            }
            if (Convert.ToString(objInventoryType.UpdateUneventFunctionID).Trim() != string.Empty)
            {
                try
                {
                    if (Library.AppCore.SystemConfig.objSessionUser.IsPermission(Convert.ToString(objInventoryType.UpdateUneventFunctionID).Trim()))
                    {
                        frmShowUnEvent1.intIsPermitUpdateUnEvent = 1;
                    }
                    else
                    {
                        frmShowUnEvent1.intIsPermitUpdateUnEvent = 0;
                    }
                }
                catch { }
            }
            else
            {
                frmShowUnEvent1.intIsPermitUpdateUnEvent = 1;
            }
            frmShowUnEvent1.tblUnEventTable = dtbUnEvent;
            if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
            {
                frmShowUnEvent1.bolIsSaveInventory = true;
            }
            frmShowUnEvent1.ShowDialog();
            if (frmShowUnEvent1.bolIsSave)
            {
                if (objInventory != null)
                {
                    objInventory.IsUpdateUnEvent = frmShowUnEvent1.bolIsUpdateUnEvent;
                }
            }

            if (objInventory != null)
            {
                if (objInventory.IsUpdateUnEvent)
                {
                    mnuItemExplainUnEvent.Enabled = true;
                }
                if (objInventory.IsExplainUnEvent)
                {
                    mnuEventHandlingUnEvent.Enabled = true;
                }
            }
        }

        private void mnuItemExplainUnEvent_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
            // Chot du lieu giai trinh
            var objKeywords = new object[] {
                "@INVENTORYTERMID", objInventory.InventoryTermID,
                "@STOREID", cboStoreID.StoreID
                };
            DataTable dtbTemp = objPLCReportDataSource.GetDataSource("INV_TERMCALINOUTDATA_SRH", objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                this.Close();
            }
            
            // Lay Danh Sach Giai Trinh
           
            dtsInOutProduct = objPLCReportDataSource.GetDataSet("INV_INOUTDATA_SRH", new string[] { "V_OUT", "V_OUT2" }, objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                this.Close();
            }


            frmExplainUnEvent frmExplainUnEvent1 = new frmExplainUnEvent();
            InitiallizeDataUnEvent();
            DataTable tblUnEventTable = dtbUnEvent.Clone();
            if (!tblUnEventTable.Columns.Contains("UNEVENTEXPLAIN"))
                tblUnEventTable.Columns.Add("UNEVENTEXPLAIN", typeof(string));
            if (!tblUnEventTable.Columns.Contains("CAUSEGROUPSLIST"))
                tblUnEventTable.Columns.Add("CAUSEGROUPSLIST", typeof(string));
            if (!tblUnEventTable.Columns.Contains("EXPLAINNOTE"))
                tblUnEventTable.Columns.Add("EXPLAINNOTE", typeof(string));
            if (!tblUnEventTable.Columns.Contains("UNEVENTID"))
                tblUnEventTable.Columns.Add("UNEVENTID", typeof(string));
            if (!tblUnEventTable.Columns.Contains("ISSYSTEMERROR"))
            {
                tblUnEventTable.Columns.Add("ISSYSTEMERROR", typeof(int));
                tblUnEventTable.Columns["ISSYSTEMERROR"].DefaultValue = 0;
            }
            if (!tblUnEventTable.Columns.Contains("EXPLAINSYSTEMNOTE"))
                tblUnEventTable.Columns.Add("EXPLAINSYSTEMNOTE", typeof(string));
            if (!tblUnEventTable.Columns.Contains("ISREQUESTIMEI"))
                tblUnEventTable.Columns.Add("ISREQUESTIMEI", typeof(int));

            if (!tblUnEventTable.Columns.Contains("QUANTITY_SALE"))
            {
                tblUnEventTable.Columns.Add("QUANTITY_SALE", typeof(decimal));
                tblUnEventTable.Columns["QUANTITY_SALE"].DefaultValue = 0;
            }
            if (!tblUnEventTable.Columns.Contains("QUANTITY_INPUT"))
            {
                tblUnEventTable.Columns.Add("QUANTITY_INPUT", typeof(decimal));
                tblUnEventTable.Columns["QUANTITY_INPUT"].DefaultValue = 0;
            }
            if (!tblUnEventTable.Columns.Contains("QUANTITY_STORECHANGE"))
            {
                tblUnEventTable.Columns.Add("QUANTITY_STORECHANGE", typeof(decimal));
                tblUnEventTable.Columns["QUANTITY_STORECHANGE"].DefaultValue = 0;
            }
            if (!tblUnEventTable.Columns.Contains("QUANTITY_OUTPUT"))
            {
                tblUnEventTable.Columns.Add("QUANTITY_OUTPUT", typeof(decimal));
                tblUnEventTable.Columns["QUANTITY_OUTPUT"].DefaultValue = 0;
            }

            if (!tblUnEventTable.Columns.Contains("QUANTITY_DIFFERENT"))
            {
                tblUnEventTable.Columns.Add("QUANTITY_DIFFERENT", typeof(decimal));
                tblUnEventTable.Columns["QUANTITY_DIFFERENT"].DefaultValue = 0;
            }
            tblUnEventTable.Columns["ISREQUESTIMEI"].SetOrdinal(tblUnEventTable.Columns["UNEVENTID"].Ordinal);

            DataTable dtbInventoryUnEvent = objPLCInventory.GetInventoryUnEvent(objInventory.InventoryID);
            if (dtbInventoryUnEvent != null && dtbInventoryUnEvent.Rows.Count > 0)
            {
                for (int i = 0; i < dtbInventoryUnEvent.Rows.Count; i++)
                {
                    DataRow r = dtbInventoryUnEvent.Rows[i];
                    DataRow rUnEvent = tblUnEventTable.NewRow();
                    rUnEvent["UNEVENTID"] = r["UNEVENTID"];
                    //rUnEvent["CABINETNUMBER"] = r["CABINETNUMBER"];
                    rUnEvent["PRODUCTID"] = r["PRODUCTID"];
                    rUnEvent["PRODUCTNAME"] = r["PRODUCTNAME"];
                    rUnEvent["ERP_IMEI"] = r["STOCKIMEI"];
                    rUnEvent["ERP_PRODUCTSTATUSID"] = r["ERP_PRODUCTSTATUSID"];
                    rUnEvent["ERP_PRODUCTSTATUSNAME"] = r["ERP_PRODUCTSTATUSNAME"];

                    if (Convert.ToDecimal(r["STOCKQUANTITY"]) != 0)
                    {
                        rUnEvent["ERP_QUANTITY"] = r["STOCKQUANTITY"];
                    }
                    rUnEvent["INV_IMEI"] = r["InventoryIMEI"];
                    if (Convert.ToDecimal(r["INVENTORYQUANTITY"]) != 0)
                    {
                        rUnEvent["INV_QUANTITY"] = r["INVENTORYQUANTITY"];
                    }
                    rUnEvent["INV_PRODUCTSTATUSID"] = r["INV_PRODUCTSTATUSID"];
                    rUnEvent["INV_PRODUCTSTATUSNAME"] = r["INV_PRODUCTSTATUSNAME"];
                    rUnEvent["DIFFERENT"] = r["UNEVENTQUANTITY"];
                    rUnEvent["ISSYSTEMERROR"] = r["ISSYSTEMERROR"];
                    rUnEvent["UNEVENTEXPLAIN"] = r["UNEVENTEXPLAIN"];
                    rUnEvent["CAUSEGROUPSLIST"] = r["CAUSEGROUPSLIST"]; 
                    rUnEvent["EXPLAINSYSTEMNOTE"] = r["EXPLAINSYSTEMNOTE"];
                    rUnEvent["EXPLAINNOTE"] = r["EXPLAINNOTE"];

                    rUnEvent["QUANTITY_SALE"] = r["QUANTITY_SALE"];
                    rUnEvent["QUANTITY_INPUT"] = r["QUANTITY_INPUT"];
                    rUnEvent["QUANTITY_STORECHANGE"] = r["QUANTITY_STORECHANGE"];
                    rUnEvent["QUANTITY_OUTPUT"] = r["QUANTITY_OUTPUT"];

                    rUnEvent["QUANTITY_DIFFERENT"] =
                        Convert.ToDecimal(rUnEvent["ERP_QUANTITY"]) + Convert.ToDecimal(rUnEvent["QUANTITY_INPUT"]) - (Convert.ToDecimal(rUnEvent["INV_QUANTITY"]) + Convert.ToDecimal(rUnEvent["QUANTITY_SALE"]) + Convert.ToDecimal(rUnEvent["QUANTITY_OUTPUT"]) + Convert.ToDecimal(rUnEvent["QUANTITY_STORECHANGE"]))
                       ;

                    if (objInventory.IsExplainUnEvent)
                    {
                        tblUnEventTable.Rows.Add(rUnEvent);
                        continue;
                    }

                    // giai trinh chech lenh tu he thong 
                    DataTable dtbInOut_NONEIMEI = dtsInOutProduct.Tables[0].Copy();
                    DataTable dtbInOut_IMEI = dtsInOutProduct.Tables[1].Copy();
                    string strProductID = (rUnEvent["PRODUCTID"] ?? "").ToString().Trim();
                    string strERP_IMEI = (rUnEvent["ERP_IMEI"] ?? "").ToString().Trim();
                    string strINV_IMEI = (rUnEvent["INV_IMEI"] ?? "").ToString().Trim();
                    int intERP_ProductStatusID = Convert.ToInt32((rUnEvent["ERP_PRODUCTSTATUSID"] ?? "").ToString().Trim());
                    int intINV_ProductStatusID = Convert.ToInt32((rUnEvent["INV_PRODUCTSTATUSID"] ?? "").ToString().Trim());

                    bool bolHaveIMEI = false;
                    if (!string.IsNullOrEmpty(strERP_IMEI)
                        || !string.IsNullOrEmpty(strINV_IMEI))
                        bolHaveIMEI = true;
                    if (bolHaveIMEI)
                    {
                        string strIMEI = strERP_IMEI;
                        if (string.IsNullOrEmpty(strERP_IMEI)
                            && !string.IsNullOrEmpty(strINV_IMEI))
                            strIMEI = strINV_IMEI;
                        var varResult = dtbInOut_IMEI.AsEnumerable()
                           .Where(row => (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == strProductID
                                      && (row.Field<object>("IMEI") ?? "").ToString().Trim() == strIMEI);
                        if (varResult.Any()) // => Có tồn tại giải trình
                        {
                            DataRow drOUT = varResult.FirstOrDefault();
                            rUnEvent["QUANTITY_INPUT"] = drOUT["QUANTITY_INPUT"];
                            rUnEvent["QUANTITY_SALE"] = drOUT["QUANTITY_SALE"];
                            rUnEvent["QUANTITY_STORECHANGE"] = drOUT["QUANTITY_STORECHANGE"];
                            rUnEvent["QUANTITY_OUTPUT"] = drOUT["QUANTITY_OUTPUT"];
                            rUnEvent["ISSYSTEMERROR"] = 1;
                            rUnEvent["UNEVENTEXPLAIN"] = (drOUT["NOTE"] ?? "").ToString().Trim();
                            rUnEvent["EXPLAINSYSTEMNOTE"] = (drOUT["NOTE"] ?? "").ToString().Trim();
                        }
                    }
                    else
                    {
                        int intProductStatusID = intERP_ProductStatusID;
                        if (intERP_ProductStatusID < 0)
                            intProductStatusID = intINV_ProductStatusID;

                        var varResult = dtbInOut_NONEIMEI.AsEnumerable()
                           .Where(row => (row.Field<object>("PRODUCTID") ?? "").ToString().Trim() == strProductID
                                      && Convert.ToInt32((row.Field<object>("PRODUCTSTATUSID") ?? "").ToString().Trim()) == intProductStatusID
                           );
                        if (varResult.Any()) // => Có tồn tại giải trình
                        {
                            DataRow drOUT = varResult.FirstOrDefault();
                            rUnEvent["QUANTITY_INPUT"] = drOUT["QUANTITY_INPUT"];
                            rUnEvent["QUANTITY_SALE"] = drOUT["QUANTITY_SALE"];
                            rUnEvent["QUANTITY_STORECHANGE"] = drOUT["QUANTITY_STORECHANGE"];
                            rUnEvent["QUANTITY_OUTPUT"] = drOUT["QUANTITY_OUTPUT"];
                            rUnEvent["ISSYSTEMERROR"] = 1;
                            rUnEvent["UNEVENTEXPLAIN"] = (drOUT["NOTE"] ?? "").ToString().Trim();
                            rUnEvent["EXPLAINSYSTEMNOTE"] = (drOUT["NOTE"] ?? "").ToString().Trim();
                        }
                    }
                    rUnEvent["QUANTITY_DIFFERENT"] =
                        Convert.ToDecimal(rUnEvent["ERP_QUANTITY"]) + Convert.ToDecimal(rUnEvent["QUANTITY_INPUT"]) - (Convert.ToDecimal(rUnEvent["INV_QUANTITY"]) + Convert.ToDecimal(rUnEvent["QUANTITY_SALE"]) + Convert.ToDecimal(rUnEvent["QUANTITY_OUTPUT"]) + Convert.ToDecimal(rUnEvent["QUANTITY_STORECHANGE"]))
                       ;
                    if (Convert.ToDecimal(rUnEvent["QUANTITY_DIFFERENT"]) == 0)
                        continue;
                    tblUnEventTable.Rows.Add(rUnEvent);
                }
            }
            frmExplainUnEvent1.strInventoryID = objInventory.InventoryID;
            frmExplainUnEvent1.dtmInventoryDate = (DateTime)objInventory.InventoryDate;
            frmExplainUnEvent1.intCreatedStoreID = objInventory.CreatedStoreID;
            frmExplainUnEvent1.bolIsReviewed = objInventory.IsReviewed;
            frmExplainUnEvent1.bolIsUpdateUnEvent = objInventory.IsUpdateUnEvent;
            frmExplainUnEvent1.bolIsExplainUnEvent = objInventory.IsExplainUnEvent;
            frmExplainUnEvent1.bolIsAccessExplainUnEvent = objInventory.IsExplainAccess;
            frmExplainUnEvent1.bolIsDeleted = objInventory.IsDeleted;
            frmExplainUnEvent1.tblUnEventTable = tblUnEventTable;
            if (Convert.ToString(objInventoryType.ExplainUneventFunctionID).Trim() != string.Empty)
            {
                try
                {
                    if (Library.AppCore.SystemConfig.objSessionUser.IsPermission(Convert.ToString(objInventoryType.ExplainUneventFunctionID).Trim()))
                    {
                        frmExplainUnEvent1.intIsPermitExplainUnEvent = 1;
                    }
                    else
                    {
                        frmExplainUnEvent1.intIsPermitExplainUnEvent = 0;
                    }
                }
                catch { }
            }
            else
            {
                frmExplainUnEvent1.intIsPermitExplainUnEvent = 1;
            }
            frmExplainUnEvent1.strUsername = objInventory.ReviewUnEventUser;
            frmExplainUnEvent1.ShowDialog();
            if (frmExplainUnEvent1.bolIsSave)
            {
                if (objInventory != null)
                {
                    objInventory.IsExplainUnEvent = frmExplainUnEvent1.bolIsExplainUnEvent;
                    objInventory.IsExplainAccess = frmExplainUnEvent1.bolIsAccessExplainUnEvent;
                }
            }

            if (objInventory != null)
            {
                if (objInventory.IsUpdateUnEvent)
                {
                    mnuItemExplainUnEvent.Enabled = true;
                }
                if (objInventory.IsExplainUnEvent && objInventory.IsExplainAccess)
                {
                    mnuEventHandlingUnEvent.Enabled = true;
                }
            }
        }

        private void mnuEventHandlingUnEvent_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            MessageBox.Show(this, "Hệ thống tạp khóa để nâng cấp!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
            string strInventoryProcessID = string.Empty;
            ERP.Inventory.PLC.Inventory.PLCInventoryProcess objPLCInventoryProcess = new PLC.Inventory.PLCInventoryProcess();
            strInventoryProcessID = objPLCInventoryProcess.CheckExistInventoryProcess(objInventory.InventoryID).Trim();
            DataTable dtb = objPLCInventory.GetInventoryHandleUnEvent(objInventory.InventoryID);
            if (dtb.Rows.Count == 0)
            {
                //frmResolveQuantity frmResolveQuantity1 = new frmResolveQuantity();
                //frmResolveQuantity1.InventoryProcessID = strInventoryProcessID;
                //frmResolveQuantity1.ShowDialog();
                MessageBox.Show(this, "Phiếu kiểm kê đang trong quá trình xử lý dữ liệu chênh lệch!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                int intIsPermitProcessInventory = -1;
                if (Convert.ToString(objInventoryType.ProcessFunctionID).Trim() != string.Empty)
                {
                    try
                    {
                        if (Library.AppCore.SystemConfig.objSessionUser.IsPermission(Convert.ToString(objInventoryType.ProcessFunctionID).Trim()))
                        {
                            intIsPermitProcessInventory = 1;
                        }
                        else
                        {
                            intIsPermitProcessInventory = 0;
                        }
                    }
                    catch { }
                }
                else
                {
                    intIsPermitProcessInventory = 1;
                }
                if (intIsPermitProcessInventory == 0)
                {
                    MessageBox.Show(this, "Bạn không có quyền xử lý chênh lệch kiểm kê. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                frmHandlingUnEventVTS frmHandlingUnEvent1 = new frmHandlingUnEventVTS();
                // frmHandlingUnEvent frmHandlingUnEvent1 = new frmHandlingUnEvent();
                frmHandlingUnEvent1.UnEventTable = objPLCInventory.GetInventoryHandleUnEvent(objInventory.InventoryID);
                frmHandlingUnEvent1.intInventoryTermID = objInventory.InventoryTermID;
                frmHandlingUnEvent1.strInventoryID = objInventory.InventoryID;
                frmHandlingUnEvent1.intStoreID = objInventory.InventoryStoreID;
                frmHandlingUnEvent1.dtmInventoryDate = (DateTime)objInventory.InventoryDate;
                frmHandlingUnEvent1.intMainGroupID = objInventory.MainGroupID;
                frmHandlingUnEvent1.intIsNew = Convert.ToInt32(objInventory.ProductStatusID);
                frmHandlingUnEvent1.bolIsDeleted = objInventory.IsDeleted;
                frmHandlingUnEvent1.ShowDialog();
            }
        }

        #endregion

        private void tabPage2_Enter(object sender, EventArgs e)
        {
            if (!chkIsParent.Checked)
            {
                tabControl1.SelectedTab = tabPage1;
            }
        }

        private void mnuItemExportTemplateExcel_Click(object sender, EventArgs e)
        {


        }

        private void btnCalc_Click(object sender, EventArgs e)
        {

        }
    }
}
