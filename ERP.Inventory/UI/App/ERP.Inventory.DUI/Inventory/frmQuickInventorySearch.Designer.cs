﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmQuickInventorySearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQuickInventorySearch));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboMainGroup = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.ucReviewedUser = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.ucInventoryUser = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.cmdExportExcel = new System.Windows.Forms.Button();
            this.cboStoreIDList = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.label4 = new System.Windows.Forms.Label();
            this.cboSearchType = new System.Windows.Forms.ComboBox();
            this.cmdSearch = new System.Windows.Forms.Button();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpLockStockTimeTo = new System.Windows.Forms.DateTimePicker();
            this.dtpInventoryDateTo = new System.Windows.Forms.DateTimePicker();
            this.dtpLockStockTimeFrom = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpInventoryDateFrom = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chkDeleted = new System.Windows.Forms.CheckBox();
            this.grdInventory = new DevExpress.XtraGrid.GridControl();
            this.grvInventory = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colQUICKINVENTORYID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSTORENAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colISNEWTYPE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCONTENT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINVENTORYDATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInventoryFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCREATEDFULLNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCREATEDDATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colISDELETED = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDELETEDFULLNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDELETEDDATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboMainGroup);
            this.groupBox1.Controls.Add(this.ucReviewedUser);
            this.groupBox1.Controls.Add(this.ucInventoryUser);
            this.groupBox1.Controls.Add(this.cmdExportExcel);
            this.groupBox1.Controls.Add(this.cboStoreIDList);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cboSearchType);
            this.groupBox1.Controls.Add(this.cmdSearch);
            this.groupBox1.Controls.Add(this.txtKeywords);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.dtpLockStockTimeTo);
            this.groupBox1.Controls.Add(this.dtpInventoryDateTo);
            this.groupBox1.Controls.Add(this.dtpLockStockTimeFrom);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dtpInventoryDateFrom);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.chkDeleted);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(941, 99);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // cboMainGroup
            // 
            this.cboMainGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroup.Location = new System.Drawing.Point(409, 11);
            this.cboMainGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroup.Name = "cboMainGroup";
            this.cboMainGroup.Size = new System.Drawing.Size(201, 24);
            this.cboMainGroup.TabIndex = 1;
            // 
            // ucReviewedUser
            // 
            this.ucReviewedUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucReviewedUser.IsOnlyShowRealStaff = false;
            this.ucReviewedUser.IsValidate = true;
            this.ucReviewedUser.Location = new System.Drawing.Point(728, 39);
            this.ucReviewedUser.Margin = new System.Windows.Forms.Padding(0);
            this.ucReviewedUser.MaximumSize = new System.Drawing.Size(400, 22);
            this.ucReviewedUser.MinimumSize = new System.Drawing.Size(0, 22);
            this.ucReviewedUser.Name = "ucReviewedUser";
            this.ucReviewedUser.Size = new System.Drawing.Size(193, 22);
            this.ucReviewedUser.TabIndex = 7;
            this.ucReviewedUser.UserName = "";
            this.ucReviewedUser.WorkingPositionID = 0;
            // 
            // ucInventoryUser
            // 
            this.ucInventoryUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucInventoryUser.IsOnlyShowRealStaff = false;
            this.ucInventoryUser.IsValidate = true;
            this.ucInventoryUser.Location = new System.Drawing.Point(728, 11);
            this.ucInventoryUser.Margin = new System.Windows.Forms.Padding(0);
            this.ucInventoryUser.MaximumSize = new System.Drawing.Size(400, 22);
            this.ucInventoryUser.MinimumSize = new System.Drawing.Size(0, 22);
            this.ucInventoryUser.Name = "ucInventoryUser";
            this.ucInventoryUser.Size = new System.Drawing.Size(193, 22);
            this.ucInventoryUser.TabIndex = 2;
            this.ucInventoryUser.UserName = "";
            this.ucInventoryUser.WorkingPositionID = 0;
            // 
            // cmdExportExcel
            // 
            this.cmdExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdExportExcel.Image")));
            this.cmdExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdExportExcel.Location = new System.Drawing.Point(821, 65);
            this.cmdExportExcel.Name = "cmdExportExcel";
            this.cmdExportExcel.Size = new System.Drawing.Size(100, 25);
            this.cmdExportExcel.TabIndex = 13;
            this.cmdExportExcel.Text = "       Xuất Excel";
            this.cmdExportExcel.UseVisualStyleBackColor = true;
            this.cmdExportExcel.Click += new System.EventHandler(this.cmdExportExcel_Click);
            // 
            // cboStoreIDList
            // 
            this.cboStoreIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreIDList.Location = new System.Drawing.Point(110, 11);
            this.cboStoreIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreIDList.Name = "cboStoreIDList";
            this.cboStoreIDList.Size = new System.Drawing.Size(201, 24);
            this.cboStoreIDList.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(317, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 16);
            this.label4.TabIndex = 55;
            this.label4.Text = "Tìm theo:";
            // 
            // cboSearchType
            // 
            this.cboSearchType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchType.FormattingEnabled = true;
            this.cboSearchType.Items.AddRange(new object[] {
            "-- Chọn tìm theo --",
            "Mã phiếu",
            "Nhân viên kiểm kê",
            "Nhân viên tạo",
            "Nhân viên duyệt"});
            this.cboSearchType.Location = new System.Drawing.Point(409, 65);
            this.cboSearchType.Name = "cboSearchType";
            this.cboSearchType.Size = new System.Drawing.Size(201, 24);
            this.cboSearchType.TabIndex = 10;
            // 
            // cmdSearch
            // 
            this.cmdSearch.Image = ((System.Drawing.Image)(resources.GetObject("cmdSearch.Image")));
            this.cmdSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSearch.Location = new System.Drawing.Point(728, 65);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(94, 25);
            this.cmdSearch.TabIndex = 12;
            this.cmdSearch.Text = "Tìm kiếm";
            this.cmdSearch.UseVisualStyleBackColor = true;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // txtKeywords
            // 
            this.txtKeywords.Location = new System.Drawing.Point(110, 66);
            this.txtKeywords.MaxLength = 200;
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.Size = new System.Drawing.Size(200, 22);
            this.txtKeywords.TabIndex = 9;
            this.txtKeywords.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInventoryID_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 16);
            this.label8.TabIndex = 37;
            this.label8.Text = "Chuỗi tìm:";
            // 
            // dtpLockStockTimeTo
            // 
            this.dtpLockStockTimeTo.CustomFormat = "dd/MM/yyyy";
            this.dtpLockStockTimeTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLockStockTimeTo.Location = new System.Drawing.Point(510, 39);
            this.dtpLockStockTimeTo.Name = "dtpLockStockTimeTo";
            this.dtpLockStockTimeTo.Size = new System.Drawing.Size(100, 22);
            this.dtpLockStockTimeTo.TabIndex = 6;
            // 
            // dtpInventoryDateTo
            // 
            this.dtpInventoryDateTo.CustomFormat = "dd/MM/yyyy";
            this.dtpInventoryDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInventoryDateTo.Location = new System.Drawing.Point(211, 39);
            this.dtpInventoryDateTo.Name = "dtpInventoryDateTo";
            this.dtpInventoryDateTo.Size = new System.Drawing.Size(100, 22);
            this.dtpInventoryDateTo.TabIndex = 4;
            // 
            // dtpLockStockTimeFrom
            // 
            this.dtpLockStockTimeFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpLockStockTimeFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLockStockTimeFrom.Location = new System.Drawing.Point(409, 39);
            this.dtpLockStockTimeFrom.Name = "dtpLockStockTimeFrom";
            this.dtpLockStockTimeFrom.Size = new System.Drawing.Size(100, 22);
            this.dtpLockStockTimeFrom.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(317, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 16);
            this.label6.TabIndex = 30;
            this.label6.Text = "Ngày chốt tồn:";
            // 
            // dtpInventoryDateFrom
            // 
            this.dtpInventoryDateFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpInventoryDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInventoryDateFrom.Location = new System.Drawing.Point(110, 39);
            this.dtpInventoryDateFrom.Name = "dtpInventoryDateFrom";
            this.dtpInventoryDateFrom.Size = new System.Drawing.Size(95, 22);
            this.dtpInventoryDateFrom.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 16);
            this.label9.TabIndex = 30;
            this.label9.Text = "Ngày KK:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(619, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 16);
            this.label7.TabIndex = 26;
            this.label7.Text = "Nhân viên duyệt:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(619, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 16);
            this.label3.TabIndex = 26;
            this.label3.Text = "Nhân viên KK:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 16);
            this.label5.TabIndex = 25;
            this.label5.Text = "Kho kiểm kê:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(317, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 16);
            this.label2.TabIndex = 22;
            this.label2.Text = "Ngành hàng:";
            // 
            // chkDeleted
            // 
            this.chkDeleted.Location = new System.Drawing.Point(622, 67);
            this.chkDeleted.Name = "chkDeleted";
            this.chkDeleted.Size = new System.Drawing.Size(83, 23);
            this.chkDeleted.TabIndex = 11;
            this.chkDeleted.Text = "Đã hủy";
            this.chkDeleted.UseVisualStyleBackColor = true;
            // 
            // grdInventory
            // 
            this.grdInventory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdInventory.Location = new System.Drawing.Point(0, 99);
            this.grdInventory.MainView = this.grvInventory;
            this.grdInventory.Name = "grdInventory";
            this.grdInventory.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.grdInventory.Size = new System.Drawing.Size(941, 446);
            this.grdInventory.TabIndex = 2;
            this.grdInventory.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvInventory});
            // 
            // grvInventory
            // 
            this.grvInventory.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvInventory.Appearance.FocusedRow.Options.UseFont = true;
            this.grvInventory.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvInventory.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvInventory.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvInventory.Appearance.Preview.Options.UseFont = true;
            this.grvInventory.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvInventory.Appearance.Row.Options.UseFont = true;
            this.grvInventory.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colQUICKINVENTORYID,
            this.colSTORENAME,
            this.colISNEWTYPE,
            this.colCONTENT,
            this.colINVENTORYDATE,
            this.colInventoryFullName,
            this.colCREATEDFULLNAME,
            this.colCREATEDDATE,
            this.colISDELETED,
            this.colDELETEDFULLNAME,
            this.colDELETEDDATE});
            this.grvInventory.GridControl = this.grdInventory;
            this.grvInventory.Name = "grvInventory";
            this.grvInventory.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grvInventory.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grvInventory.OptionsNavigation.UseTabKey = false;
            this.grvInventory.OptionsSelection.MultiSelect = true;
            this.grvInventory.OptionsView.ColumnAutoWidth = false;
            this.grvInventory.OptionsView.ShowAutoFilterRow = true;
            this.grvInventory.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvInventory.OptionsView.ShowGroupPanel = false;
            this.grvInventory.DoubleClick += new System.EventHandler(this.grvInventory_DoubleClick);
            // 
            // colQUICKINVENTORYID
            // 
            this.colQUICKINVENTORYID.AppearanceHeader.Options.UseTextOptions = true;
            this.colQUICKINVENTORYID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQUICKINVENTORYID.Caption = "Mã phiếu";
            this.colQUICKINVENTORYID.FieldName = "QUICKINVENTORYID";
            this.colQUICKINVENTORYID.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colQUICKINVENTORYID.Name = "colQUICKINVENTORYID";
            this.colQUICKINVENTORYID.OptionsColumn.AllowEdit = false;
            this.colQUICKINVENTORYID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQUICKINVENTORYID.Visible = true;
            this.colQUICKINVENTORYID.VisibleIndex = 0;
            this.colQUICKINVENTORYID.Width = 120;
            // 
            // colSTORENAME
            // 
            this.colSTORENAME.AppearanceHeader.Options.UseTextOptions = true;
            this.colSTORENAME.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSTORENAME.Caption = "Kho kiểm kê";
            this.colSTORENAME.FieldName = "STORENAME";
            this.colSTORENAME.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colSTORENAME.Name = "colSTORENAME";
            this.colSTORENAME.OptionsColumn.AllowEdit = false;
            this.colSTORENAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colSTORENAME.Visible = true;
            this.colSTORENAME.VisibleIndex = 1;
            this.colSTORENAME.Width = 300;
            // 
            // colISNEWTYPE
            // 
            this.colISNEWTYPE.AppearanceHeader.Options.UseTextOptions = true;
            this.colISNEWTYPE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colISNEWTYPE.Caption = "Trạng thái";
            this.colISNEWTYPE.FieldName = "ISNEWTYPENAME";
            this.colISNEWTYPE.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colISNEWTYPE.Name = "colISNEWTYPE";
            this.colISNEWTYPE.OptionsColumn.AllowEdit = false;
            this.colISNEWTYPE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colISNEWTYPE.Visible = true;
            this.colISNEWTYPE.VisibleIndex = 3;
            this.colISNEWTYPE.Width = 120;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit1.ReadOnly = true;
            this.repositoryItemCheckEdit1.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemCheckEdit1.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // colCONTENT
            // 
            this.colCONTENT.AppearanceHeader.Options.UseTextOptions = true;
            this.colCONTENT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCONTENT.Caption = "Nội dung";
            this.colCONTENT.FieldName = "CONTENT";
            this.colCONTENT.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colCONTENT.Name = "colCONTENT";
            this.colCONTENT.OptionsColumn.AllowEdit = false;
            this.colCONTENT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCONTENT.Visible = true;
            this.colCONTENT.VisibleIndex = 4;
            this.colCONTENT.Width = 300;
            // 
            // colINVENTORYDATE
            // 
            this.colINVENTORYDATE.AppearanceHeader.Options.UseTextOptions = true;
            this.colINVENTORYDATE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colINVENTORYDATE.Caption = "Ngày kiểm kê";
            this.colINVENTORYDATE.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colINVENTORYDATE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colINVENTORYDATE.FieldName = "INVENTORYDATE";
            this.colINVENTORYDATE.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colINVENTORYDATE.Name = "colINVENTORYDATE";
            this.colINVENTORYDATE.OptionsColumn.AllowEdit = false;
            this.colINVENTORYDATE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colINVENTORYDATE.Visible = true;
            this.colINVENTORYDATE.VisibleIndex = 2;
            this.colINVENTORYDATE.Width = 150;
            // 
            // colInventoryFullName
            // 
            this.colInventoryFullName.AppearanceHeader.Options.UseTextOptions = true;
            this.colInventoryFullName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInventoryFullName.Caption = "Nhân viên kiểm kê";
            this.colInventoryFullName.FieldName = "INVENTORYFULLNAME";
            this.colInventoryFullName.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colInventoryFullName.Name = "colInventoryFullName";
            this.colInventoryFullName.OptionsColumn.AllowEdit = false;
            this.colInventoryFullName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colInventoryFullName.Visible = true;
            this.colInventoryFullName.VisibleIndex = 5;
            this.colInventoryFullName.Width = 220;
            // 
            // colCREATEDFULLNAME
            // 
            this.colCREATEDFULLNAME.AppearanceHeader.Options.UseTextOptions = true;
            this.colCREATEDFULLNAME.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCREATEDFULLNAME.Caption = "Nhân viên tạo";
            this.colCREATEDFULLNAME.FieldName = "CREATEDFULLNAME";
            this.colCREATEDFULLNAME.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colCREATEDFULLNAME.Name = "colCREATEDFULLNAME";
            this.colCREATEDFULLNAME.OptionsColumn.AllowEdit = false;
            this.colCREATEDFULLNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCREATEDFULLNAME.Visible = true;
            this.colCREATEDFULLNAME.VisibleIndex = 6;
            this.colCREATEDFULLNAME.Width = 220;
            // 
            // colCREATEDDATE
            // 
            this.colCREATEDDATE.AppearanceHeader.Options.UseTextOptions = true;
            this.colCREATEDDATE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCREATEDDATE.Caption = "Ngày tạo";
            this.colCREATEDDATE.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colCREATEDDATE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colCREATEDDATE.FieldName = "CREATEDDATE";
            this.colCREATEDDATE.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colCREATEDDATE.Name = "colCREATEDDATE";
            this.colCREATEDDATE.OptionsColumn.AllowEdit = false;
            this.colCREATEDDATE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCREATEDDATE.Visible = true;
            this.colCREATEDDATE.VisibleIndex = 7;
            this.colCREATEDDATE.Width = 150;
            // 
            // colISDELETED
            // 
            this.colISDELETED.AppearanceHeader.Options.UseTextOptions = true;
            this.colISDELETED.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colISDELETED.Caption = "Xóa";
            this.colISDELETED.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colISDELETED.FieldName = "ISDELETED";
            this.colISDELETED.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colISDELETED.Name = "colISDELETED";
            this.colISDELETED.OptionsColumn.AllowEdit = false;
            this.colISDELETED.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colISDELETED.Visible = true;
            this.colISDELETED.VisibleIndex = 8;
            this.colISDELETED.Width = 45;
            // 
            // colDELETEDFULLNAME
            // 
            this.colDELETEDFULLNAME.AppearanceHeader.Options.UseTextOptions = true;
            this.colDELETEDFULLNAME.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDELETEDFULLNAME.Caption = "Nhân viên xóa";
            this.colDELETEDFULLNAME.FieldName = "DELETEDFULLNAME";
            this.colDELETEDFULLNAME.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colDELETEDFULLNAME.Name = "colDELETEDFULLNAME";
            this.colDELETEDFULLNAME.OptionsColumn.AllowEdit = false;
            this.colDELETEDFULLNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDELETEDFULLNAME.Visible = true;
            this.colDELETEDFULLNAME.VisibleIndex = 9;
            this.colDELETEDFULLNAME.Width = 220;
            // 
            // colDELETEDDATE
            // 
            this.colDELETEDDATE.AppearanceHeader.Options.UseTextOptions = true;
            this.colDELETEDDATE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDELETEDDATE.Caption = "Ngày xóa";
            this.colDELETEDDATE.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colDELETEDDATE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDELETEDDATE.FieldName = "DELETEDDATE";
            this.colDELETEDDATE.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colDELETEDDATE.Name = "colDELETEDDATE";
            this.colDELETEDDATE.OptionsColumn.AllowEdit = false;
            this.colDELETEDDATE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDELETEDDATE.Visible = true;
            this.colDELETEDDATE.VisibleIndex = 10;
            this.colDELETEDDATE.Width = 150;
            // 
            // frmQuickInventorySearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 545);
            this.Controls.Add(this.grdInventory);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(951, 583);
            this.Name = "frmQuickInventorySearch";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý phiếu kiểm kê nhanh";
            this.Load += new System.EventHandler(this.frmQuickInventorySearch_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button cmdSearch;
        private System.Windows.Forms.TextBox txtKeywords;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpInventoryDateTo;
        private System.Windows.Forms.DateTimePicker dtpInventoryDateFrom;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkDeleted;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboSearchType;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStoreIDList;
        private System.Windows.Forms.Button cmdExportExcel;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ucInventoryUser;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroup;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ucReviewedUser;
        private System.Windows.Forms.DateTimePicker dtpLockStockTimeTo;
        private System.Windows.Forms.DateTimePicker dtpLockStockTimeFrom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraGrid.GridControl grdInventory;
        private DevExpress.XtraGrid.Views.Grid.GridView grvInventory;
        private DevExpress.XtraGrid.Columns.GridColumn colQUICKINVENTORYID;
        private DevExpress.XtraGrid.Columns.GridColumn colINVENTORYDATE;
        private DevExpress.XtraGrid.Columns.GridColumn colSTORENAME;
        private DevExpress.XtraGrid.Columns.GridColumn colISNEWTYPE;
        private DevExpress.XtraGrid.Columns.GridColumn colInventoryFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colCREATEDFULLNAME;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colCREATEDDATE;
        private DevExpress.XtraGrid.Columns.GridColumn colISDELETED;
        private DevExpress.XtraGrid.Columns.GridColumn colCONTENT;
        private DevExpress.XtraGrid.Columns.GridColumn colDELETEDFULLNAME;
        private DevExpress.XtraGrid.Columns.GridColumn colDELETEDDATE;
    }
}