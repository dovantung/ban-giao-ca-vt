﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmCreateQuickInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCreateQuickInventory));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.txtQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cboIsCheckRealInput = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboBrandID = new Library.AppControl.ComboBoxControl.ComboBoxBrand();
            this.cboSubGroup = new Library.AppControl.ComboBoxControl.ComboBoxSubGroup();
            this.label17 = new System.Windows.Forms.Label();
            this.cboMainGroup = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.cboStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.btnInventory = new System.Windows.Forms.Button();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpLockStockTime = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.grdStoreChangeOrderDetail = new DevExpress.XtraGrid.GridControl();
            this.mnuDetail = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemImportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemExportTemplateExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.btnExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.grvStoreChangeOrderDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColSTT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINSTOCKQUANTITY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQUANTITY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQUANTITYDIFFERENCE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grdTemp = new DevExpress.XtraGrid.GridControl();
            this.grvTemp = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cboProductStateID = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.colPRODUCTSTATUSNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdStoreChangeOrderDetail)).BeginInit();
            this.mnuDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvStoreChangeOrderDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            this.SuspendLayout();
            // 
            // txtQuantity
            // 
            this.txtQuantity.AutoHeight = false;
            this.txtQuantity.DisplayFormat.FormatString = "#,##0.####;";
            this.txtQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtQuantity.Mask.EditMask = "#,###,###,##0.####;";
            this.txtQuantity.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.NullText = "0";
            this.txtQuantity.NullValuePrompt = "0";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cboProductStateID);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtNote);
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cboIsCheckRealInput);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cboBrandID);
            this.groupBox1.Controls.Add(this.cboSubGroup);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.cboMainGroup);
            this.groupBox1.Controls.Add(this.cboStoreID);
            this.groupBox1.Controls.Add(this.btnInventory);
            this.groupBox1.Controls.Add(this.txtContent);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.dtpLockStockTime);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(911, 125);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin kiểm kê tồn kho";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(607, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 16);
            this.label4.TabIndex = 20;
            this.label4.Text = "Trạng thái:";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(108, 94);
            this.txtNote.MaxLength = 200;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(491, 22);
            this.txtNote.TabIndex = 17;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(811, 94);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(95, 25);
            this.btnUpdate.TabIndex = 19;
            this.btnUpdate.Text = "   Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 16);
            this.label1.TabIndex = 16;
            this.label1.Text = "Ghi chú:";
            // 
            // cboIsCheckRealInput
            // 
            this.cboIsCheckRealInput.DropDownHeight = 250;
            this.cboIsCheckRealInput.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIsCheckRealInput.DropDownWidth = 150;
            this.cboIsCheckRealInput.FormattingEnabled = true;
            this.cboIsCheckRealInput.IntegralHeight = false;
            this.cboIsCheckRealInput.Items.AddRange(new object[] {
            "Bao gồm cả hàng đi đường",
            "Chỉ hiển thị hàng đi đường",
            "Không hiển thị hàng đi đường"});
            this.cboIsCheckRealInput.Location = new System.Drawing.Point(712, 42);
            this.cboIsCheckRealInput.Name = "cboIsCheckRealInput";
            this.cboIsCheckRealInput.Size = new System.Drawing.Size(194, 24);
            this.cboIsCheckRealInput.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(607, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 16);
            this.label10.TabIndex = 10;
            this.label10.Text = "Hàng đi đường:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Nhà sản xuất:";
            // 
            // cboBrandID
            // 
            this.cboBrandID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBrandID.Location = new System.Drawing.Point(108, 42);
            this.cboBrandID.Margin = new System.Windows.Forms.Padding(0);
            this.cboBrandID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBrandID.Name = "cboBrandID";
            this.cboBrandID.Size = new System.Drawing.Size(194, 24);
            this.cboBrandID.TabIndex = 7;
            // 
            // cboSubGroup
            // 
            this.cboSubGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSubGroup.Location = new System.Drawing.Point(712, 15);
            this.cboSubGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboSubGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboSubGroup.Name = "cboSubGroup";
            this.cboSubGroup.Size = new System.Drawing.Size(194, 24);
            this.cboSubGroup.TabIndex = 5;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(607, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 16);
            this.label17.TabIndex = 4;
            this.label17.Text = "Nhóm hàng:";
            // 
            // cboMainGroup
            // 
            this.cboMainGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroup.Location = new System.Drawing.Point(406, 16);
            this.cboMainGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroup.Name = "cboMainGroup";
            this.cboMainGroup.Size = new System.Drawing.Size(194, 24);
            this.cboMainGroup.TabIndex = 3;
            this.cboMainGroup.SelectionChangeCommitted += new System.EventHandler(this.cboMainGroup_SelectionChangeCommitted);
            // 
            // cboStoreID
            // 
            this.cboStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreID.Location = new System.Drawing.Point(108, 16);
            this.cboStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreID.Name = "cboStoreID";
            this.cboStoreID.Size = new System.Drawing.Size(194, 24);
            this.cboStoreID.TabIndex = 1;
            this.cboStoreID.SelectionChangeCommitted += new System.EventHandler(this.cboStoreID_SelectionChangeCommitted);
            // 
            // btnInventory
            // 
            this.btnInventory.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.btnInventory.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInventory.Location = new System.Drawing.Point(712, 94);
            this.btnInventory.Name = "btnInventory";
            this.btnInventory.Size = new System.Drawing.Size(88, 25);
            this.btnInventory.TabIndex = 18;
            this.btnInventory.Text = "Kiểm kê";
            this.btnInventory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInventory.UseVisualStyleBackColor = true;
            this.btnInventory.Click += new System.EventHandler(this.btnInventory_Click);
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(108, 69);
            this.txtContent.MaxLength = 200;
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(491, 22);
            this.txtContent.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 16);
            this.label8.TabIndex = 12;
            this.label8.Text = "Nội dung:";
            // 
            // dtpLockStockTime
            // 
            this.dtpLockStockTime.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpLockStockTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLockStockTime.Location = new System.Drawing.Point(406, 44);
            this.dtpLockStockTime.Name = "dtpLockStockTime";
            this.dtpLockStockTime.Size = new System.Drawing.Size(193, 22);
            this.dtpLockStockTime.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(314, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 16);
            this.label6.TabIndex = 8;
            this.label6.Text = "Ngày chốt tồn:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Kho kiểm kê:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(314, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ngành hàng:";
            // 
            // grdStoreChangeOrderDetail
            // 
            this.grdStoreChangeOrderDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdStoreChangeOrderDetail.ContextMenuStrip = this.mnuDetail;
            this.grdStoreChangeOrderDetail.Location = new System.Drawing.Point(0, 125);
            this.grdStoreChangeOrderDetail.MainView = this.grvStoreChangeOrderDetail;
            this.grdStoreChangeOrderDetail.Name = "grdStoreChangeOrderDetail";
            this.grdStoreChangeOrderDetail.Size = new System.Drawing.Size(911, 410);
            this.grdStoreChangeOrderDetail.TabIndex = 2;
            this.grdStoreChangeOrderDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvStoreChangeOrderDetail,
            this.gridView2,
            this.gridView1});
            // 
            // mnuDetail
            // 
            this.mnuDetail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemImportExcel,
            this.mnuItemExportTemplateExcel,
            this.btnExportExcel});
            this.mnuDetail.Name = "mnuFlexDetail";
            this.mnuDetail.Size = new System.Drawing.Size(187, 70);
            this.mnuDetail.Opening += new System.ComponentModel.CancelEventHandler(this.mnuDetail_Opening);
            // 
            // mnuItemImportExcel
            // 
            this.mnuItemImportExcel.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemImportExcel.Image")));
            this.mnuItemImportExcel.Name = "mnuItemImportExcel";
            this.mnuItemImportExcel.Size = new System.Drawing.Size(186, 22);
            this.mnuItemImportExcel.Text = "Nhập từ Excel";
            this.mnuItemImportExcel.Click += new System.EventHandler(this.mnuItemImportExcel_Click);
            // 
            // mnuItemExportTemplateExcel
            // 
            this.mnuItemExportTemplateExcel.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemExportTemplateExcel.Image")));
            this.mnuItemExportTemplateExcel.Name = "mnuItemExportTemplateExcel";
            this.mnuItemExportTemplateExcel.Size = new System.Drawing.Size(186, 22);
            this.mnuItemExportTemplateExcel.Text = "Xuất ra file Excel mẫu";
            this.mnuItemExportTemplateExcel.Click += new System.EventHandler(this.mnuItemExportTemplateExcel_Click);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(186, 22);
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // grvStoreChangeOrderDetail
            // 
            this.grvStoreChangeOrderDetail.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvStoreChangeOrderDetail.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Red;
            this.grvStoreChangeOrderDetail.Appearance.FooterPanel.Options.UseFont = true;
            this.grvStoreChangeOrderDetail.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grvStoreChangeOrderDetail.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvStoreChangeOrderDetail.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvStoreChangeOrderDetail.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvStoreChangeOrderDetail.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvStoreChangeOrderDetail.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvStoreChangeOrderDetail.Appearance.Row.Options.UseFont = true;
            this.grvStoreChangeOrderDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColSTT,
            this.colProductID,
            this.colProductName,
            this.colPRODUCTSTATUSNAME,
            this.colINSTOCKQUANTITY,
            this.colQUANTITY,
            this.colQUANTITYDIFFERENCE,
            this.colNOTE});
            this.grvStoreChangeOrderDetail.FooterPanelHeight = 50;
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition1.Expression = "[Status]=true";
            styleFormatCondition1.Value1 = true;
            this.grvStoreChangeOrderDetail.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.grvStoreChangeOrderDetail.GridControl = this.grdStoreChangeOrderDetail;
            this.grvStoreChangeOrderDetail.Name = "grvStoreChangeOrderDetail";
            this.grvStoreChangeOrderDetail.OptionsBehavior.CopyToClipboardWithColumnHeaders = false;
            this.grvStoreChangeOrderDetail.OptionsView.ColumnAutoWidth = false;
            this.grvStoreChangeOrderDetail.OptionsView.ShowAutoFilterRow = true;
            this.grvStoreChangeOrderDetail.OptionsView.ShowFooter = true;
            this.grvStoreChangeOrderDetail.OptionsView.ShowGroupPanel = false;
            this.grvStoreChangeOrderDetail.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.grvStoreChangeOrderDetail_CustomDrawFooterCell);
            this.grvStoreChangeOrderDetail.CustomDrawRowFooter += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.grvStoreChangeOrderDetail_CustomDrawRowFooter);
            this.grvStoreChangeOrderDetail.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.grvStoreChangeOrderDetail_RowCellStyle);
            this.grvStoreChangeOrderDetail.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grvStoreChangeOrderDetail_ShowingEditor);
            this.grvStoreChangeOrderDetail.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grvStoreChangeOrderDetail_CellValueChanged);
            // 
            // ColSTT
            // 
            this.ColSTT.Caption = "STT";
            this.ColSTT.FieldName = "ORDERINDEX";
            this.ColSTT.Name = "ColSTT";
            this.ColSTT.OptionsColumn.AllowEdit = false;
            this.ColSTT.Visible = true;
            this.ColSTT.VisibleIndex = 0;
            this.ColSTT.Width = 50;
            // 
            // colProductID
            // 
            this.colProductID.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductID.Caption = "Mã sản phẩm";
            this.colProductID.FieldName = "PRODUCTID";
            this.colProductID.Name = "colProductID";
            this.colProductID.OptionsColumn.AllowEdit = false;
            this.colProductID.OptionsColumn.ReadOnly = true;
            this.colProductID.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "PRODUCTID", "Tổng cộng:"),
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "PRODUCTID", "Số chênh lệch:")});
            this.colProductID.Visible = true;
            this.colProductID.VisibleIndex = 1;
            this.colProductID.Width = 150;
            // 
            // colProductName
            // 
            this.colProductName.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductName.Caption = "Tên sản phẩm";
            this.colProductName.FieldName = "PRODUCTNAME";
            this.colProductName.Name = "colProductName";
            this.colProductName.OptionsColumn.AllowEdit = false;
            this.colProductName.OptionsColumn.ReadOnly = true;
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 2;
            this.colProductName.Width = 220;
            // 
            // colINSTOCKQUANTITY
            // 
            this.colINSTOCKQUANTITY.AppearanceCell.Options.UseTextOptions = true;
            this.colINSTOCKQUANTITY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colINSTOCKQUANTITY.AppearanceHeader.Options.UseTextOptions = true;
            this.colINSTOCKQUANTITY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colINSTOCKQUANTITY.Caption = "Số lượng tồn";
            this.colINSTOCKQUANTITY.DisplayFormat.FormatString = "#,##0.####;";
            this.colINSTOCKQUANTITY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colINSTOCKQUANTITY.FieldName = "INSTOCKQUANTITY";
            this.colINSTOCKQUANTITY.Name = "colINSTOCKQUANTITY";
            this.colINSTOCKQUANTITY.OptionsColumn.AllowEdit = false;
            this.colINSTOCKQUANTITY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "INSTOCKQUANTITY", "{0:#,##0;}"),
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "INSTOCKQUANTITY", "Thừa: 0")});
            this.colINSTOCKQUANTITY.Visible = true;
            this.colINSTOCKQUANTITY.VisibleIndex = 4;
            this.colINSTOCKQUANTITY.Width = 125;
            // 
            // colQUANTITY
            // 
            this.colQUANTITY.AppearanceCell.Options.UseTextOptions = true;
            this.colQUANTITY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colQUANTITY.AppearanceHeader.Options.UseTextOptions = true;
            this.colQUANTITY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQUANTITY.Caption = "Số lượng thực tế";
            this.colQUANTITY.ColumnEdit = this.txtQuantity;
            this.colQUANTITY.FieldName = "QUANTITY";
            this.colQUANTITY.Name = "colQUANTITY";
            this.colQUANTITY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY", "{0:#,##0;}"),
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "QUANTITY", "Thiếu: 0")});
            this.colQUANTITY.Visible = true;
            this.colQUANTITY.VisibleIndex = 5;
            this.colQUANTITY.Width = 125;
            // 
            // colQUANTITYDIFFERENCE
            // 
            this.colQUANTITYDIFFERENCE.AppearanceCell.Options.UseTextOptions = true;
            this.colQUANTITYDIFFERENCE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colQUANTITYDIFFERENCE.AppearanceHeader.Options.UseTextOptions = true;
            this.colQUANTITYDIFFERENCE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQUANTITYDIFFERENCE.Caption = "Chênh lệch";
            this.colQUANTITYDIFFERENCE.DisplayFormat.FormatString = "#,##0.####;";
            this.colQUANTITYDIFFERENCE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colQUANTITYDIFFERENCE.FieldName = "QUANTITYDIFFERENCE";
            this.colQUANTITYDIFFERENCE.Name = "colQUANTITYDIFFERENCE";
            this.colQUANTITYDIFFERENCE.OptionsColumn.AllowEdit = false;
            this.colQUANTITYDIFFERENCE.Visible = true;
            this.colQUANTITYDIFFERENCE.VisibleIndex = 6;
            this.colQUANTITYDIFFERENCE.Width = 125;
            // 
            // colNOTE
            // 
            this.colNOTE.Caption = "Ghi chú";
            this.colNOTE.FieldName = "NOTE";
            this.colNOTE.Name = "colNOTE";
            this.colNOTE.Visible = true;
            this.colNOTE.VisibleIndex = 7;
            this.colNOTE.Width = 250;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.grdStoreChangeOrderDetail;
            this.gridView2.Name = "gridView2";
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.grdStoreChangeOrderDetail;
            this.gridView1.Name = "gridView1";
            // 
            // grdTemp
            // 
            this.grdTemp.ContextMenuStrip = this.mnuDetail;
            this.grdTemp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdTemp.Location = new System.Drawing.Point(0, 0);
            this.grdTemp.MainView = this.grvTemp;
            this.grdTemp.Name = "grdTemp";
            this.grdTemp.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.grdTemp.Size = new System.Drawing.Size(911, 535);
            this.grdTemp.TabIndex = 15;
            this.grdTemp.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvTemp,
            this.gridView4,
            this.gridView5});
            this.grdTemp.Visible = false;
            // 
            // grvTemp
            // 
            this.grvTemp.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvTemp.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvTemp.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvTemp.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvTemp.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvTemp.Appearance.Row.Options.UseFont = true;
            this.grvTemp.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7});
            styleFormatCondition2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            styleFormatCondition2.Appearance.Options.UseBackColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition2.Expression = "[Status]=true";
            styleFormatCondition2.Value1 = true;
            this.grvTemp.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.grvTemp.GridControl = this.grdTemp;
            this.grvTemp.Name = "grvTemp";
            this.grvTemp.OptionsBehavior.CopyToClipboardWithColumnHeaders = false;
            this.grvTemp.OptionsView.ColumnAutoWidth = false;
            this.grvTemp.OptionsView.ShowAutoFilterRow = true;
            this.grvTemp.OptionsView.ShowFooter = true;
            this.grvTemp.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "STT";
            this.gridColumn1.FieldName = "ORDERINDEX";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 50;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "Mã sản phẩm";
            this.gridColumn2.FieldName = "PRODUCTID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 150;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.Caption = "Tên sản phẩm";
            this.gridColumn3.FieldName = "PRODUCTNAME";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 220;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.Caption = "Số lượng tồn";
            this.gridColumn4.FieldName = "INSTOCKQUANTITY";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "INSTOCKQUANTITY", "{0:#,##0;}")});
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 125;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "Số lượng thực tế";
            this.gridColumn5.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn5.FieldName = "QUANTITY";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY", "{0:#,##0;}")});
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 125;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.DisplayFormat.FormatString = "#,##0;";
            this.repositoryItemTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemTextEdit1.Mask.EditMask = "#,###,###,##0;";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.NullText = "0";
            this.repositoryItemTextEdit1.NullValuePrompt = "0";
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "Chênh lệch";
            this.gridColumn6.FieldName = "QUANTITYDIFFERENCE";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 125;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Ghi chú";
            this.gridColumn7.FieldName = "NOTE";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            this.gridColumn7.Width = 250;
            // 
            // gridView4
            // 
            this.gridView4.GridControl = this.grdTemp;
            this.gridView4.Name = "gridView4";
            // 
            // gridView5
            // 
            this.gridView5.GridControl = this.grdTemp;
            this.gridView5.Name = "gridView5";
            // 
            // cboProductStateID
            // 
            this.cboProductStateID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductStateID.Location = new System.Drawing.Point(712, 69);
            this.cboProductStateID.Margin = new System.Windows.Forms.Padding(0);
            this.cboProductStateID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboProductStateID.Name = "cboProductStateID";
            this.cboProductStateID.Size = new System.Drawing.Size(194, 24);
            this.cboProductStateID.TabIndex = 64;
            // 
            // colPRODUCTSTATUSNAME
            // 
            this.colPRODUCTSTATUSNAME.AppearanceHeader.Options.UseTextOptions = true;
            this.colPRODUCTSTATUSNAME.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPRODUCTSTATUSNAME.Caption = "Trạng thái";
            this.colPRODUCTSTATUSNAME.FieldName = "PRODUCTSTATUSNAME";
            this.colPRODUCTSTATUSNAME.Name = "colPRODUCTSTATUSNAME";
            this.colPRODUCTSTATUSNAME.OptionsColumn.AllowEdit = false;
            this.colPRODUCTSTATUSNAME.Visible = true;
            this.colPRODUCTSTATUSNAME.VisibleIndex = 3;
            this.colPRODUCTSTATUSNAME.Width = 120;
            // 
            // frmCreateQuickInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 535);
            this.Controls.Add(this.grdStoreChangeOrderDetail);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grdTemp);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmCreateQuickInventory";
            this.ShowIcon = false;
            this.Text = "Tạo phiếu kiểm kê nhanh";
            this.Load += new System.EventHandler(this.frmCreateQuickInventory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdStoreChangeOrderDetail)).EndInit();
            this.mnuDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvStoreChangeOrderDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroup;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStoreID;
        private System.Windows.Forms.Button btnInventory;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpLockStockTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Library.AppControl.ComboBoxControl.ComboBoxBrand cboBrandID;
        private Library.AppControl.ComboBoxControl.ComboBoxSubGroup cboSubGroup;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cboIsCheckRealInput;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.GridControl grdStoreChangeOrderDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView grvStoreChangeOrderDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colProductID;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
        private DevExpress.XtraGrid.Columns.GridColumn colINSTOCKQUANTITY;
        private DevExpress.XtraGrid.Columns.GridColumn colQUANTITY;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn ColSTT;
        private DevExpress.XtraGrid.Columns.GridColumn colQUANTITYDIFFERENCE;
        private DevExpress.XtraGrid.Columns.GridColumn colNOTE;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ContextMenuStrip mnuDetail;
        private System.Windows.Forms.ToolStripMenuItem mnuItemImportExcel;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportTemplateExcel;
        private DevExpress.XtraGrid.GridControl grdTemp;
        private DevExpress.XtraGrid.Views.Grid.GridView grvTemp;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtQuantity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripMenuItem btnExportExcel;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboProductStateID;
        private DevExpress.XtraGrid.Columns.GridColumn colPRODUCTSTATUSNAME;
    }
}