﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmQuickInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQuickInventory));
            this.flexData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtReviewedFullName = new System.Windows.Forms.TextBox();
            this.txtBrandName = new System.Windows.Forms.TextBox();
            this.txtMainGroupName = new System.Windows.Forms.TextBox();
            this.txtInventoryDate = new System.Windows.Forms.TextBox();
            this.txtCreatedFullName = new System.Windows.Forms.TextBox();
            this.txtInventoryFullName = new System.Windows.Forms.TextBox();
            this.txtStoreName = new System.Windows.Forms.TextBox();
            this.txtQuickInventoryID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flexData
            // 
            this.flexData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexData.AllowEditing = false;
            this.flexData.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexData.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flexData.AutoClipboard = true;
            this.flexData.ColumnInfo = "10,1,0,0,0,105,Columns:";
            this.flexData.Location = new System.Drawing.Point(0, 141);
            this.flexData.Name = "flexData";
            this.flexData.Rows.DefaultSize = 21;
            this.flexData.Size = new System.Drawing.Size(962, 383);
            this.flexData.StyleInfo = resources.GetString("flexData.StyleInfo");
            this.flexData.TabIndex = 3;
            this.flexData.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.btnDelete);
            this.groupControl1.Controls.Add(this.txtReviewedFullName);
            this.groupControl1.Controls.Add(this.txtBrandName);
            this.groupControl1.Controls.Add(this.txtMainGroupName);
            this.groupControl1.Controls.Add(this.txtInventoryDate);
            this.groupControl1.Controls.Add(this.txtCreatedFullName);
            this.groupControl1.Controls.Add(this.txtInventoryFullName);
            this.groupControl1.Controls.Add(this.txtStoreName);
            this.groupControl1.Controls.Add(this.txtQuickInventoryID);
            this.groupControl1.Controls.Add(this.label5);
            this.groupControl1.Controls.Add(this.label6);
            this.groupControl1.Controls.Add(this.label7);
            this.groupControl1.Controls.Add(this.label8);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Location = new System.Drawing.Point(0, -2);
            this.groupControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.groupControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(961, 144);
            this.groupControl1.TabIndex = 73;
            this.groupControl1.Text = "Thông tin phiếu kiểm kê";
            // 
            // txtReviewedFullName
            // 
            this.txtReviewedFullName.BackColor = System.Drawing.SystemColors.Info;
            this.txtReviewedFullName.Location = new System.Drawing.Point(540, 111);
            this.txtReviewedFullName.Name = "txtReviewedFullName";
            this.txtReviewedFullName.ReadOnly = true;
            this.txtReviewedFullName.Size = new System.Drawing.Size(217, 22);
            this.txtReviewedFullName.TabIndex = 7;
            // 
            // txtBrandName
            // 
            this.txtBrandName.BackColor = System.Drawing.SystemColors.Info;
            this.txtBrandName.Location = new System.Drawing.Point(540, 83);
            this.txtBrandName.Name = "txtBrandName";
            this.txtBrandName.ReadOnly = true;
            this.txtBrandName.Size = new System.Drawing.Size(217, 22);
            this.txtBrandName.TabIndex = 5;
            // 
            // txtMainGroupName
            // 
            this.txtMainGroupName.BackColor = System.Drawing.SystemColors.Info;
            this.txtMainGroupName.Location = new System.Drawing.Point(540, 55);
            this.txtMainGroupName.Name = "txtMainGroupName";
            this.txtMainGroupName.ReadOnly = true;
            this.txtMainGroupName.Size = new System.Drawing.Size(217, 22);
            this.txtMainGroupName.TabIndex = 3;
            // 
            // txtInventoryDate
            // 
            this.txtInventoryDate.BackColor = System.Drawing.SystemColors.Info;
            this.txtInventoryDate.Location = new System.Drawing.Point(540, 27);
            this.txtInventoryDate.Name = "txtInventoryDate";
            this.txtInventoryDate.ReadOnly = true;
            this.txtInventoryDate.Size = new System.Drawing.Size(217, 22);
            this.txtInventoryDate.TabIndex = 1;
            // 
            // txtCreatedFullName
            // 
            this.txtCreatedFullName.BackColor = System.Drawing.SystemColors.Info;
            this.txtCreatedFullName.Location = new System.Drawing.Point(141, 111);
            this.txtCreatedFullName.Name = "txtCreatedFullName";
            this.txtCreatedFullName.ReadOnly = true;
            this.txtCreatedFullName.Size = new System.Drawing.Size(217, 22);
            this.txtCreatedFullName.TabIndex = 6;
            // 
            // txtInventoryFullName
            // 
            this.txtInventoryFullName.BackColor = System.Drawing.SystemColors.Info;
            this.txtInventoryFullName.Location = new System.Drawing.Point(141, 83);
            this.txtInventoryFullName.Name = "txtInventoryFullName";
            this.txtInventoryFullName.ReadOnly = true;
            this.txtInventoryFullName.Size = new System.Drawing.Size(217, 22);
            this.txtInventoryFullName.TabIndex = 4;
            // 
            // txtStoreName
            // 
            this.txtStoreName.BackColor = System.Drawing.SystemColors.Info;
            this.txtStoreName.Location = new System.Drawing.Point(141, 55);
            this.txtStoreName.Name = "txtStoreName";
            this.txtStoreName.ReadOnly = true;
            this.txtStoreName.Size = new System.Drawing.Size(217, 22);
            this.txtStoreName.TabIndex = 2;
            // 
            // txtQuickInventoryID
            // 
            this.txtQuickInventoryID.BackColor = System.Drawing.SystemColors.Info;
            this.txtQuickInventoryID.Location = new System.Drawing.Point(141, 27);
            this.txtQuickInventoryID.Name = "txtQuickInventoryID";
            this.txtQuickInventoryID.ReadOnly = true;
            this.txtQuickInventoryID.Size = new System.Drawing.Size(217, 22);
            this.txtQuickInventoryID.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(413, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 16);
            this.label5.TabIndex = 54;
            this.label5.Text = "Ngày kiểm kê:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(413, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 16);
            this.label6.TabIndex = 54;
            this.label6.Text = "Ngành hàng:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(413, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 16);
            this.label7.TabIndex = 54;
            this.label7.Text = "Nhà sản xuất:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(413, 114);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 16);
            this.label8.TabIndex = 54;
            this.label8.Text = "Nhân viên xác nhận:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 16);
            this.label1.TabIndex = 54;
            this.label1.Text = "Mã phiếu:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 16);
            this.label2.TabIndex = 54;
            this.label2.Text = "Kho kiểm kê:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 16);
            this.label3.TabIndex = 54;
            this.label3.Text = "Nhân viên kiểm kê:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 16);
            this.label4.TabIndex = 54;
            this.label4.Text = "Nhân viên tạo phiếu:";
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnDelete.Enabled = false;
            this.btnDelete.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(763, 109);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(59, 25);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "      Hủy";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // frmQuickInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(961, 522);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.flexData);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(969, 549);
            this.Name = "frmQuickInventory";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chi tiết phiếu kiểm kê nhanh";
            this.Load += new System.EventHandler(this.frmShowUnEvent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private C1.Win.C1FlexGrid.C1FlexGrid flexData;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCreatedFullName;
        private System.Windows.Forms.TextBox txtInventoryFullName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStoreName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtReviewedFullName;
        private System.Windows.Forms.TextBox txtBrandName;
        private System.Windows.Forms.TextBox txtMainGroupName;
        private System.Windows.Forms.TextBox txtInventoryDate;
        private System.Windows.Forms.TextBox txtQuickInventoryID;
        private System.Windows.Forms.Button btnDelete;
    }
}