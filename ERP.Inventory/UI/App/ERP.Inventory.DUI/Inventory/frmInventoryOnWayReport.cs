﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.LoadControls;
using System.Collections;
using ERP.MasterData.PLC.MD;
using Library.AppControl;
using Library.AppCore;
using GemBox.Spreadsheet;
using NPOI.XSSF.UserModel;
using ERP.Report.DUI;
using NPOI.SS.UserModel;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmInventoryOnWayReport : Form
    {
        #region Variables

        private int intReportType = -1;
        private int isInputStore;


        #endregion

        #region Constructor

        public frmInventoryOnWayReport()
        {
            InitializeComponent();
            
        }

        #endregion

        #region Methods

        private void LoadComboBox()
        {
            cboBranch.InitControl(false);
            cboStoreIDList.InitControl(true);
            cboReportType.SelectedIndex = 0;
            if (this.Tag != null && this.Tag.ToString().Trim() != string.Empty)
            {
                try
                {
                    Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                    isInputStore = Convert.ToInt32(hstbParam["ISINPUTSTORE"]);
                }
                catch { }
            }
        }

        private bool CheckSearch()
        {
            if (Globals.DateDiff(dtpToDate.Value, dtpFromDate.Value, Globals.DateDiffType.DATE) < 0)
            {
                MessageBox.Show("Từ ngày không được lớn hơn đến ngày yêu cầu. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtpToDate.Focus();
                return false;
            }
            if (Globals.DateDiff(dtpToDate.Value, dtpFromDate.Value, Globals.DateDiffType.DATE) > 92)
            {
                MessageBox.Show("Vui lòng tìm kiếm trong thời gian 3 tháng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtpToDate.Focus();
                return false;
            }

            if (cboReportType.SelectedIndex == 0)
            {
                if(cboStoreIDList.StoreID <= 0)
                {
                    MessageBox.Show("Vui lòng chọn kho.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboStoreIDList.Focus();
                    return false;
                }
            }

            return true;
        }

        private void SearchData()
        {
            string strUserName = SystemConfig.objSessionUser.UserName;

            string strStoreIDList = string.Empty;
            //if (cboReportType.SelectedIndex == 1)
              
            //else
            //    strStoreIDList = "<"+ cboStoreIDList.StoreID + ">";
            strStoreIDList = cboStoreIDList.StoreIDList;

            object[] objKeywords = new object[]
            {
                "@FROMDATE", dtpFromDate.Value,
                "@TODATE", dtpToDate.Value,
                "@STOREIDLIST", strStoreIDList,
                "@ReportType", cboReportType.SelectedIndex,
                "@ISINPUTSTORE", chkIsInput.Checked
            };

            DataSet ds = new ERP.Report.PLC.PLCReportDataSource().GetDataSet("RPT_INVENTORYONWAY_SRH", new string[] { "v_out1", "v_out2" }, objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }

            DataTable dtData1 = null;
            DataTable dtData2 = null;

            if (ds != null && ds.Tables.Count == 2)
            {
                dtData1 = ds.Tables[0];
                dtData2 = ds.Tables[1];
            }

            if (dtData1 == null || dtData1.Rows.Count == 0)
            {
                MessageBox.Show("Không tìm thấy dữ liệu để xuất.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (cboReportType.SelectedIndex == 0)
                ExportProductStatus01D(dtData1);
            else
            {
                if (dtData2 == null || dtData2.Rows.Count == 0)
                {
                    MessageBox.Show("Không tìm thấy dữ liệu để xuất.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                ExportProductStatusValue(dtData1, dtData2);
            }
                
            //CreateDataExport(dtData, dtProductStatus);
        }

        private Boolean CheckFileTemplate(String strFileTemplate, ref string strPathFileName)
        {
            try
            {
                SaveFileDialog saveDlg = new SaveFileDialog();
                saveDlg.Filter = "Excel Worksheet file(*.xlsx)|*.xls";
                saveDlg.AddExtension = true;
                saveDlg.RestoreDirectory = true;
                saveDlg.Title = "Chọn vị trí lưu file?";
                saveDlg.FileName = "Báo cáo chốt kiểm kê theo kỳ " + System.DateTime.Now.Date.ToString("dd-MM-yyyy");
                if (saveDlg.ShowDialog() == DialogResult.OK)
                {
                    strPathFileName = System.IO.Path.ChangeExtension(saveDlg.FileName, ".xlsx");
                }
                if (string.IsNullOrEmpty(strPathFileName)) return false;

                if (System.IO.File.Exists(strPathFileName))
                {
                    try
                    {
                        System.IO.File.Delete(strPathFileName);
                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show("File Excel đang được mở, Bạn phải đóng lại trước khi mở file tiếp", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }

                if (System.IO.File.Exists(strFileTemplate) == false)
                {
                    MessageBox.Show("Tập tin mẫu không tồn tại");
                    return false;
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
        private void InsertValueCell(ref ExcelWorksheet worksheet, String strColumnName, int iRow, Object value, Boolean isAdd = false, Boolean isBold = false)
        {
            try
            {
                ExcelCell cell = worksheet.Cells[strColumnName + iRow];
                SetValueRange(ref cell, value, isAdd, isBold);
            }
            catch { }
        }
        private void SetValueRange(ref ExcelCell cell, Object value, Boolean isAdd, Boolean isBold)
        {
            try
            {
                if (isBold)
                    cell.Style.Font.Weight = ExcelFont.BoldWeight;
            }
            catch { }

            if (cell != null)
            {
                try
                {
                    if (isAdd)
                        cell.Value = cell.Value.ToString() + " " + value;
                    else
                        cell.Value = value;

                    if (value.ToString().StartsWith("="))
                        cell.Formula = value.ToString();
                }
                catch { }
            }
        }
        private void OpenFileExport(String strPathFileName)
        {
            try
            {
                if (MessageBox.Show("Bạn có muốn mở file vừa lưu?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    System.Diagnostics.Process.Start(strPathFileName);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Export 01D

        public void ExportProductStatus01D(DataTable dtbData)
        {
            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strFileName = string.Empty;
            string strSheetName = string.Empty;
            strSheetName = "Mau 01A-KKHH";
            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\InventoryOnWayProductStatus01D.xlsx";
            if (CheckFileTemplate(strFileTemplate, ref strPathFileName) == false) return;

            strFileName = System.IO.Path.GetFileNameWithoutExtension(strPathFileName);
            if (ExportExcelProductStatus01D(dtbData, strFileTemplate, strFileName, strPathFileName, strSheetName))
            {
                OpenFileExport(strPathFileName);
                return;
            }
            else
            {
                return;
            }
        }

        private bool ExportExcelProductStatus01D(DataTable dtbData, string strFileTemplate, string strFileName, string strPathFileName, string strSheetName)
        {
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
            }
            catch { }

            if (workbook != null)
            {
                ExcelWorksheet sheet = (ExcelWorksheet)workbook.Worksheets[0];
                if (sheet != null)
                {
                    #region Insert Header
                    string strStoreName = dtbData.Rows[0]["STORENAME"].ToString().Trim();
                    DateTime? dtReviewedDate = null;
                    DataRow[] lstRowReviewed = dtbData.Select("REVIEWUNEVENTTIME IS NOT NULL");
                    if (lstRowReviewed != null && lstRowReviewed.Count() > 0)
                        dtReviewedDate = Convert.ToDateTime(lstRowReviewed[0]["REVIEWUNEVENTTIME"]);

                    string strReviewedDate = "Ngày .…  tháng …. năm ….";
                    string strReviewedTime = "…….giờ … phút";
                    if (dtReviewedDate != null)
                    {
                        strReviewedDate = string.Format(@"Ngày {0} tháng {1} năm {2}", dtReviewedDate.Value.Day, dtReviewedDate.Value.Month, dtReviewedDate.Value.Year);
                        strReviewedTime = string.Format(@"{0} giờ {1} phút", dtReviewedDate.Value.Hour, dtReviewedDate.Value.Minute);
                    }

                    string strReason = string.Format(@"Hôm nay, {0}, siêu thị {1} tiến hành kiểm kê vào lúc:{2}. Kết quả kiểm kê như sau:", strReviewedDate, strStoreName, strReviewedTime);
                    string strPrintDate = string.Format(@"………….., Ngày {0} Tháng {1} Năm {2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);

                    InsertValueCell(ref sheet, "A", 3, strStoreName, false, true);
                    InsertValueCell(ref sheet, "A", 6, strReviewedDate, false, true);
                    InsertValueCell(ref sheet, "A", 15, strReason, false, true);
                    InsertValueCell(ref sheet, "H", 25, strPrintDate, false, false);

                    #endregion

                    #region Import Row

                    int iRowStart = 22;
                    int iRowCurrent = iRowStart;
                    int intSTT = 1;
                    int intMainGroupIDTemp = 0;
                    int intSubGroupIDTemp = 0;
                    int intMainGroupIndex = 0;
                    int intSubGroupIndex = 0;
                    bool bolInsertMainGroup = false;
                    bool bolInsertSubGroup = false;
                    for (int i = 0; i < dtbData.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dtbData.Rows[i]["MAINGROUPID"]) == intMainGroupIDTemp)
                            bolInsertMainGroup = false;
                        else
                        {
                            bolInsertMainGroup = true;
                            intMainGroupIDTemp = Convert.ToInt32(dtbData.Rows[i]["MAINGROUPID"]);
                            intMainGroupIndex++;
                        }

                        if (Convert.ToInt32(dtbData.Rows[i]["SUBGROUPID"]) == intSubGroupIDTemp)
                            bolInsertSubGroup = false;
                        else
                        {
                            bolInsertSubGroup = true;
                            intSubGroupIDTemp = Convert.ToInt32(dtbData.Rows[i]["SUBGROUPID"]);
                            intSubGroupIndex++;
                        }

                        AddRowProductStatus01D(ref sheet, dtbData.Rows[i], ref iRowCurrent, iRowStart, ref intSTT, intMainGroupIndex, intSubGroupIndex, bolInsertMainGroup, bolInsertSubGroup);
                    }

                    InsertValueCell(ref sheet, "E", iRowCurrent, string.Format(@"=SUM(E{0}:E{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "H", iRowCurrent, string.Format(@"=SUM(H{0}:H{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "K", iRowCurrent, string.Format(@"=SUM(K{0}:K{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "L", iRowCurrent, string.Format(@"=SUM(L{0}:L{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "M", iRowCurrent, string.Format(@"=SUM(M{0}:M{1})", iRowStart, iRowCurrent - 1), false, true);

                    #endregion
                }
            }
            try
            {
                workbook.SaveExcel(strPathFileName);

                #region Export pdf

                //Microsoft.Office.Interop.Excel.Application excelApplication;
                //Microsoft.Office.Interop.Excel.Workbook excelWorkbook;

                //// Create new instance of Excel
                //excelApplication = new Microsoft.Office.Interop.Excel.Application();

                //// Make the process invisible to the user
                //excelApplication.ScreenUpdating = false;

                //// Make the process silent
                //excelApplication.DisplayAlerts = false;

                //// Open the workbook that you wish to export to PDF
                //excelWorkbook = excelApplication.Workbooks.Open(strPathFileName);

                //// If the workbook failed to open, stop, clean up, and bail out
                //if (excelWorkbook == null)
                //{
                //    excelApplication.Quit();

                //    excelApplication = null;
                //    excelWorkbook = null;

                //    return false;
                //}

                //var exportSuccessful = true;
                //string strFileNamePDF = strPathFileName.Replace(".xlsx", ".pdf").Replace(".xls", ".pdf");
                //try
                //{
                //    // Call Excel's native export function (valid in Office 2007 and Office 2010, AFAIK)
                //    object misValue = System.Reflection.Missing.Value;
                //    //excelWorkbook.ActiveSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
                //    //excelWorkbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, strFileNamePDF);

                //    excelWorkbook.ActiveSheet.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, strFileNamePDF,
                //    Microsoft.Office.Interop.Excel.XlFixedFormatQuality.xlQualityStandard, true, false, Type.Missing, Type.Missing, false, Type.Missing);
                //}
                //catch (System.Exception ex)
                //{
                //    exportSuccessful = false;
                //}
                //finally
                //{
                //    // Close the workbook, quit the Excel, and clean up regardless of the results...
                //    excelWorkbook.Close();
                //    excelApplication.Quit();

                //    excelApplication = null;
                //    excelWorkbook = null;
                //}

                ////Delete excel file
                //if (System.IO.File.Exists(strPathFileName))
                //{
                //    System.IO.FileInfo fi = new System.IO.FileInfo(strPathFileName);
                //    try
                //    {
                //        fi.Delete();
                //    }
                //    catch (System.IO.IOException e)
                //    {
                //        Console.WriteLine(e.Message);
                //    }
                //}

                //if (System.IO.File.Exists(strFileNamePDF))
                //{
                //    System.Diagnostics.Process.Start(strFileNamePDF);
                //}

                #endregion

                return true;
            }
            catch
            {
                return false;
            }
        }

        public void AddRowProductStatus01D(ref ExcelWorksheet sheet, DataRow row, ref int iRow, int iRowTemplate, ref int iSTT, int intMainGroupIndex, int intSubGroupIndex, bool bolInsertMainGroup, bool bolInsertSubGroup)
        {
            if (bolInsertMainGroup)
            {
                if (iRowTemplate < iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(20);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "A", iRow, row["MAINGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "A", 20, row["MAINGROUPNAME"].ToString(), false, true);
                }
            }

            if (bolInsertMainGroup)
            {
                if (iRowTemplate != iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(21);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "B", iRow, row["SUBGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "B", 21, row["SUBGROUPNAME"].ToString(), false, true);
                }
            }

            if (iRowTemplate != iRow)
            {
                ExcelRow rowTemplate = sheet.GetRow(iRowTemplate);
                sheet.GetRow(iRowTemplate);
                rowTemplate.InsertCopy(iRow, false);
            }

            InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
            InsertValueCell(ref sheet, "B", iRow, row["PRODUCTNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "C", iRow, row["PCONSIGNMENTTYPENAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "D", iRow, row["QUANTITYUNITNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "E", iRow, Convert.ToDecimal(row["QUANTITY"]), false, false);
            InsertValueCell(ref sheet, "F", iRow, row["CUR_PRODUCTSTATUSNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "G", iRow, Convert.ToDecimal(row["CHECKQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "H", iRow, row["PRODUCTSTATUSNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "I", iRow, string.Format(@"=E{0}-G{0}", iRow), false, false);
            InsertValueCell(ref sheet, "J", iRow, Convert.ToDecimal(row["EXCHANGEQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "K", iRow, string.Format(@"=E{0}-J{0}", iRow), false, false);
            iSTT++;

            iRow++;
        }

        #endregion

        #region Export 01A/KKHH

        public void ExportProductStatusValue(DataTable dtbData1, DataTable dtbData2)
        {
            string strFileTemplate = string.Empty;
            string strPathFileName = string.Empty;
            string strSheetName = string.Empty;
            strSheetName = "PL.01";
            strFileTemplate = System.Windows.Forms.Application.StartupPath + "\\Templates\\Reports\\InventoryOnWayProductStatusValue.xlsx";
            if (CheckFileTemplate(strFileTemplate, ref strPathFileName) == false) return;
            if (ExportExcelProductStatusValue(dtbData1, dtbData2, strFileTemplate, strPathFileName, strSheetName))
            {
                OpenFileExport(strPathFileName);
                return;
            }
            else
            {
                return;
            }
        }

        private bool ExportExcelProductStatusValue(DataTable dtbData1, DataTable dtbData2, string strFileTemplate, string strPathFileName, string strSheetName)
        {
            ExcelFile workbook = null;
            try
            {
                workbook = new ExcelFile(strFileTemplate);
            }
            catch { }

            if (workbook != null)
            {
                ExcelWorksheet sheet = (ExcelWorksheet)workbook.Worksheets[0];
                if (sheet != null)
                {
                    string strStoreName = dtbData1.Rows[0]["STORENAME"].ToString().Trim();
                    DateTime? dtReviewedDate = null;
                    DataRow[] lstRowReviewed = dtbData1.Select("REVIEWUNEVENTTIME IS NOT NULL");
                    if (lstRowReviewed != null && lstRowReviewed.Count() > 0)
                        dtReviewedDate = Convert.ToDateTime(lstRowReviewed[0]["REVIEWUNEVENTTIME"]);

                    string strReviewedDate = "Ngày .…  tháng …. năm ….";
                    string strReviewedTime = "…….giờ … phút";
                    if (dtReviewedDate != null)
                    {
                        strReviewedDate = string.Format(@"Ngày {0} tháng {1} năm {2}", dtReviewedDate.Value.Day, dtReviewedDate.Value.Month, dtReviewedDate.Value.Year);
                        strReviewedTime = string.Format(@"{0} giờ {1} phút", dtReviewedDate.Value.Hour, dtReviewedDate.Value.Minute);
                    }

                    string strReason = string.Format(@"Hôm nay, {0}, tiến hành kiểm kê vào lúc:{1}. Kết quả kiểm kê như sau:", strReviewedDate, strReviewedTime);
                    string strPrintDate = string.Format(@"………….., Ngày {0} Tháng {1} Năm {2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);

                    #region Tổng hợp

                    #region Insert Header

                    //InsertValueCell(ref sheet, "A", 3, strStoreName, false, true);
                    InsertValueCell(ref sheet, "A", 7, strReviewedDate, false, true);
                    InsertValueCell(ref sheet, "A", 16, strReason, false, true);
                    InsertValueCell(ref sheet, "R", 28, strPrintDate, false, false);

                    #endregion

                    #region Import Row

                    int iRowStart = 25;
                    int iRowCurrent = iRowStart;
                    int intSTT = 1;
                    int intMainGroupIDTemp = 0;
                    int intSubGroupIDTemp = 0;
                    int intMainGroupIndex = 0;
                    int intSubGroupIndex = 0;
                    bool bolInsertMainGroup = false;
                    bool bolInsertSubGroup = false;
                    for (int i = 0; i < dtbData1.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dtbData1.Rows[i]["MAINGROUPID"]) == intMainGroupIDTemp)
                            bolInsertMainGroup = false;
                        else
                        {
                            bolInsertMainGroup = true;
                            intMainGroupIDTemp = Convert.ToInt32(dtbData1.Rows[i]["MAINGROUPID"]);
                            intMainGroupIndex++;
                        }

                        if (Convert.ToInt32(dtbData1.Rows[i]["SUBGROUPID"]) == intSubGroupIDTemp)
                            bolInsertSubGroup = false;
                        else
                        {
                            bolInsertSubGroup = true;
                            intSubGroupIDTemp = Convert.ToInt32(dtbData1.Rows[i]["SUBGROUPID"]);
                            intSubGroupIndex++;
                        }
                        AddRowProductStatusValue(ref sheet, dtbData1.Rows[i], ref iRowCurrent, iRowStart, ref intSTT, 0, 0, bolInsertMainGroup, bolInsertSubGroup);
                    }

                    InsertValueCell(ref sheet, "G", iRowCurrent, string.Format(@"=SUM(G{0}:G{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "H", iRowCurrent, string.Format(@"=SUM(H{0}:H{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "I", iRowCurrent, string.Format(@"=SUM(I{0}:I{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "J", iRowCurrent, string.Format(@"=SUM(J{0}:J{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "K", iRowCurrent, string.Format(@"=SUM(K{0}:K{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "L", iRowCurrent, string.Format(@"=SUM(L{0}:L{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "N", iRowCurrent, string.Format(@"=SUM(N{0}:N{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "O", iRowCurrent, string.Format(@"=SUM(O{0}:O{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "P", iRowCurrent, string.Format(@"=SUM(P{0}:P{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "Q", iRowCurrent, string.Format(@"=SUM(Q{0}:Q{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "R", iRowCurrent, string.Format(@"=SUM(R{0}:R{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "S", iRowCurrent, string.Format(@"=SUM(S{0}:S{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "T", iRowCurrent, string.Format(@"=SUM(T{0}:T{1})", iRowStart, iRowCurrent - 1), false, true);
                    InsertValueCell(ref sheet, "U", iRowCurrent, string.Format(@"=SUM(U{0}:U{1})", iRowStart, iRowCurrent - 1), false, true);

                    #endregion

                    #endregion

                    ExcelWorksheet sheetDetail = (ExcelWorksheet)workbook.Worksheets[1];
                    if (sheetDetail != null)
                    {
                        #region Chi tiết

                        #region Insert Header

                        //InsertValueCell(ref sheetDetail, "A", 3, strStoreName, false, true);
                        InsertValueCell(ref sheetDetail, "A", 7, strReviewedDate, false, true);
                        InsertValueCell(ref sheetDetail, "A", 16, strReason, false, true);
                        InsertValueCell(ref sheetDetail, "W", 28, strPrintDate, false, false);

                        #endregion

                        #region Import Row

                        iRowStart = 25;
                        iRowCurrent = iRowStart;
                        intSTT = 1;
                        intMainGroupIDTemp = 0;
                        intSubGroupIDTemp = 0;
                        intMainGroupIndex = 0;
                        intSubGroupIndex = 0;
                        bolInsertMainGroup = false;
                        bolInsertSubGroup = false;
                        for (int i = 0; i < dtbData2.Rows.Count; i++)
                        {
                            if (Convert.ToInt32(dtbData2.Rows[i]["MAINGROUPID"]) == intMainGroupIDTemp)
                                bolInsertMainGroup = false;
                            else
                            {
                                bolInsertMainGroup = true;
                                intMainGroupIDTemp = Convert.ToInt32(dtbData2.Rows[i]["MAINGROUPID"]);
                                intMainGroupIndex++;
                            }

                            if (Convert.ToInt32(dtbData2.Rows[i]["SUBGROUPID"]) == intSubGroupIDTemp)
                                bolInsertSubGroup = false;
                            else
                            {
                                bolInsertSubGroup = true;
                                intSubGroupIDTemp = Convert.ToInt32(dtbData2.Rows[i]["SUBGROUPID"]);
                                intSubGroupIndex++;
                            }
                            AddRowProductStatusValueDetail(ref sheetDetail, dtbData2.Rows[i], ref iRowCurrent, iRowStart, ref intSTT, 0, 0, bolInsertMainGroup, bolInsertSubGroup);
                        }

                        InsertValueCell(ref sheetDetail, "G", iRowCurrent, string.Format(@"=SUM(G{0}:G{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "H", iRowCurrent, string.Format(@"=SUM(H{0}:H{1})", iRowStart, iRowCurrent - 1), false, true);

                        InsertValueCell(ref sheetDetail, "K", iRowCurrent, string.Format(@"=SUM(K{0}:K{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "L", iRowCurrent, string.Format(@"=SUM(L{0}:L{1})", iRowStart, iRowCurrent - 1), false, true);

                        InsertValueCell(ref sheetDetail, "O", iRowCurrent, string.Format(@"=SUM(O{0}:O{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "P", iRowCurrent, string.Format(@"=SUM(P{0}:P{1})", iRowStart, iRowCurrent - 1), false, true);

                        InsertValueCell(ref sheetDetail, "S", iRowCurrent, string.Format(@"=SUM(S{0}:S{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "T", iRowCurrent, string.Format(@"=SUM(T{0}:T{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "U", iRowCurrent, string.Format(@"=SUM(U{0}:U{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "V", iRowCurrent, string.Format(@"=SUM(V{0}:V{1})", iRowStart, iRowCurrent - 1), false, true);

                        InsertValueCell(ref sheetDetail, "W", iRowCurrent, string.Format(@"=SUM(W{0}:W{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "X", iRowCurrent, string.Format(@"=SUM(X{0}:X{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "Y", iRowCurrent, string.Format(@"=SUM(Y{0}:Y{1})", iRowStart, iRowCurrent - 1), false, true);
                        InsertValueCell(ref sheetDetail, "Z", iRowCurrent, string.Format(@"=SUM(Z{0}:Z{1})", iRowStart, iRowCurrent - 1), false, true);

                        #endregion

                        #endregion
                    }
                }
            }
            try
            {
                workbook.SaveExcel(strPathFileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void AddRowProductStatusValue(ref ExcelWorksheet sheet, DataRow row, ref int iRow, int iRowTemplate, ref int iSTT, int intMainGroupIndex, int intSubGroupIndex, bool bolInsertMainGroup, bool bolInsertSubGroup)
        {
            if (bolInsertMainGroup)
            {
                if (iRowTemplate < iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(23);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "A", iRow, row["MAINGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "A", 23, row["MAINGROUPNAME"].ToString(), false, true);
                }
            }

            if (bolInsertMainGroup)
            {
                if (iRowTemplate != iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(24);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "B", iRow, row["SUBGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "B", 24, row["SUBGROUPNAME"].ToString(), false, true);
                }
            }

            if (iRowTemplate != iRow)
            {
                ExcelRow rowTemplate = sheet.GetRow(iRowTemplate);
                sheet.GetRow(iRowTemplate);
                rowTemplate.InsertCopy(iRow, false);
            }

            InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
            InsertValueCell(ref sheet, "B", iRow, row["PRODUCTNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "C", iRow, row["PCONSIGNMENTTYPENAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "D", iRow, row["EOL"].ToString(), false, false);
            InsertValueCell(ref sheet, "E", iRow, Convert.ToDecimal(row["AVGPRICE"]), false, false);
            InsertValueCell(ref sheet, "F", iRow, row["QUANTITYUNITNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "G", iRow, Convert.ToDecimal(row["QUANTITY"]), false, false);
            InsertValueCell(ref sheet, "H", iRow, Convert.ToDecimal(row["TOTALCOST"]), false, false);
            InsertValueCell(ref sheet, "I", iRow, Convert.ToDecimal(row["CHECKQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "J", iRow, Convert.ToDecimal(row["REALTOTALCOST"]), false, false);
            InsertValueCell(ref sheet, "K", iRow, string.Format(@"=G{0}-I{0}", iRow), false, false);
            InsertValueCell(ref sheet, "L", iRow, string.Format(@"=H{0}-J{0}", iRow), false, false);
            InsertValueCell(ref sheet, "M", iRow, row["EXPLAINNOTE"].ToString(), false, false);
            InsertValueCell(ref sheet, "N", iRow, Convert.ToDecimal(row["QUANTITY_SALE"]), false, false);
            InsertValueCell(ref sheet, "O", iRow, Convert.ToDecimal(row["QUANTITY_STORECHANGE"]), false, false);
            InsertValueCell(ref sheet, "P", iRow, Convert.ToDecimal(row["QUANTITY_OUTPUT"]), false, false);
            InsertValueCell(ref sheet, "Q", iRow, Convert.ToDecimal(row["QUANTITY_INPUT"]), false, false);
            InsertValueCell(ref sheet, "R", iRow, string.Format(@"=I{0}+N{0}+O{0}+P{0}-Q{0}", iRow), false, false);
            InsertValueCell(ref sheet, "S", iRow, string.Format(@"=R{0}*E{0}", iRow), false, false);
            InsertValueCell(ref sheet, "T", iRow, string.Format(@"=G{0}-R{0}", iRow), false, false);
            InsertValueCell(ref sheet, "U", iRow, string.Format(@"=H{0}-S{0}", iRow), false, false);

            iSTT++;
            iRow++;
        }

        public void AddRowProductStatusValueDetail(ref ExcelWorksheet sheet, DataRow row, ref int iRow, int iRowTemplate, ref int iSTT, int intMainGroupIndex, int intSubGroupIndex, bool bolInsertMainGroup, bool bolInsertSubGroup)
        {
            if (bolInsertMainGroup)
            {
                if (iRowTemplate < iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(23);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "A", iRow, row["MAINGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "A", 23, row["MAINGROUPNAME"].ToString(), false, true);
                }
            }

            if (bolInsertMainGroup)
            {
                if (iRowTemplate != iRow)
                {
                    ExcelRow rowTemplate = sheet.GetRow(24);
                    rowTemplate.InsertCopy(iRow, false);

                    InsertValueCell(ref sheet, "B", iRow, row["SUBGROUPNAME"].ToString(), false, true);
                    iRow++;
                }
                else
                {
                    InsertValueCell(ref sheet, "B", 24, row["SUBGROUPNAME"].ToString(), false, true);
                }
            }

            if (iRowTemplate != iRow)
            {
                ExcelRow rowTemplate = sheet.GetRow(iRowTemplate);
                sheet.GetRow(iRowTemplate);
                rowTemplate.InsertCopy(iRow, false);
            }

            InsertValueCell(ref sheet, "A", iRow, iSTT.ToString(), false, false);
            InsertValueCell(ref sheet, "B", iRow, row["PRODUCTNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "C", iRow, row["PCONSIGNMENTTYPENAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "D", iRow, row["EOL"].ToString(), false, false);
            InsertValueCell(ref sheet, "E", iRow, Convert.ToDecimal(row["AVGPRICE"]), false, false);
            InsertValueCell(ref sheet, "F", iRow, row["QUANTITYUNITNAME"].ToString(), false, false);
            InsertValueCell(ref sheet, "G", iRow, Convert.ToDecimal(row["QUANTITY"]), false, false);
            InsertValueCell(ref sheet, "H", iRow, Convert.ToDecimal(row["TOTALCOST"]), false, false);
            InsertValueCell(ref sheet, "I", iRow, row["STOCKIMEI"].ToString().Trim(), false, false);
            InsertValueCell(ref sheet, "J", iRow, row["CUR_PRODUCTSTATUSNAME"].ToString().Trim(), false, false);
            InsertValueCell(ref sheet, "K", iRow, Convert.ToDecimal(row["CHECKQUANTITY"]), false, false);
            InsertValueCell(ref sheet, "L", iRow, Convert.ToDecimal(row["REALTOTALCOST"]), false, false);
            InsertValueCell(ref sheet, "M", iRow, row["INVENTORYIMEI"].ToString().Trim(), false, false);
            InsertValueCell(ref sheet, "N", iRow, row["PRODUCTSTATUSNAME"].ToString().Trim(), false, false);
            InsertValueCell(ref sheet, "O", iRow, string.Format(@"=G{0}-K{0}", iRow), false, false);
            InsertValueCell(ref sheet, "P", iRow, string.Format(@"=H{0}-L{0}", iRow), false, false);
            InsertValueCell(ref sheet, "Q", iRow, row["UNEVENTPRODUCTSTATUSNAME"].ToString().Trim(), false, false);
            InsertValueCell(ref sheet, "R", iRow, row["EXPLAINNOTE"].ToString(), false, false);

            InsertValueCell(ref sheet, "S", iRow, Convert.ToDecimal(row["QUANTITY_SALE"]), false, false);
            InsertValueCell(ref sheet, "T", iRow, Convert.ToDecimal(row["QUANTITY_STORECHANGE"]), false, false);
            InsertValueCell(ref sheet, "U", iRow, Convert.ToDecimal(row["QUANTITY_OUTPUT"]), false, false);
            InsertValueCell(ref sheet, "V", iRow, Convert.ToDecimal(row["QUANTITY_INPUT"]), false, false);
            InsertValueCell(ref sheet, "W", iRow, string.Format(@"=O{0}+S{0}+T{0}+U{0}-V{0}", iRow), false, false);
            InsertValueCell(ref sheet, "X", iRow, string.Format(@"=W{0}*E{0}", iRow), false, false);
            InsertValueCell(ref sheet, "Y", iRow, string.Format(@"=G{0}-W{0}", iRow), false, false);
            InsertValueCell(ref sheet, "Z", iRow, string.Format(@"=H{0}-X{0}", iRow), false, false);

            iSTT++;
            iRow++;
        }

        #endregion

        #endregion

        #region Events

        private void frmInventoryTerm_Load(object sender, EventArgs e)
        {
            LoadComboBox();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!CheckSearch())
                return;

            Library.AppCore.Forms.frmWaitDialog.Show("Xem báo cáo", "Đang xử lý...");
            SearchData();
            Library.AppCore.Forms.frmWaitDialog.Close();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        private void cboBranch_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            if (cboBranch.BranchID > 0)
                objStoreFilter.OtherCondition = "BRANCHID=" + cboBranch.BranchID;
            cboStoreIDList.InitControl(true, objStoreFilter);
        }

        private void cboReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboReportType.SelectedIndex == intReportType)
                return;
            Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            if (cboBranch.BranchID > 0)
                objStoreFilter.OtherCondition = "BRANCHID=" + cboBranch.BranchID;
            cboStoreIDList.InitControl(true, objStoreFilter);
            intReportType = cboReportType.SelectedIndex;
        }

        #endregion

    }
}
