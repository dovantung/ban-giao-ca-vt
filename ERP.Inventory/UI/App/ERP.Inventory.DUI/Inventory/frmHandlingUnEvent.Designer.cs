﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmHandlingUnEvent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHandlingUnEvent));
            this.flexUnEvent = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.mnuHandlingUnEvent = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuOutputChange = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuInOutHandling = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.flexUnEvent)).BeginInit();
            this.mnuHandlingUnEvent.SuspendLayout();
            this.SuspendLayout();
            // 
            // flexUnEvent
            // 
            this.flexUnEvent.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexUnEvent.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexUnEvent.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexUnEvent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flexUnEvent.AutoClipboard = true;
            this.flexUnEvent.ColumnInfo = "10,1,0,0,0,105,Columns:";
            this.flexUnEvent.ContextMenuStrip = this.mnuHandlingUnEvent;
            this.flexUnEvent.Location = new System.Drawing.Point(0, 0);
            this.flexUnEvent.Name = "flexUnEvent";
            this.flexUnEvent.Rows.DefaultSize = 21;
            this.flexUnEvent.Size = new System.Drawing.Size(1066, 612);
            this.flexUnEvent.StyleInfo = resources.GetString("flexUnEvent.StyleInfo");
            this.flexUnEvent.TabIndex = 5;
            this.flexUnEvent.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            this.flexUnEvent.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexUnEvent_BeforeEdit);
            this.flexUnEvent.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexUnEvent_AfterEdit);
            this.flexUnEvent.SetupEditor += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexUnEvent_SetupEditor);
            // 
            // mnuHandlingUnEvent
            // 
            this.mnuHandlingUnEvent.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuOutputChange,
            this.mnuInOutHandling});
            this.mnuHandlingUnEvent.Name = "mnuHandlingUnEvent";
            this.mnuHandlingUnEvent.Size = new System.Drawing.Size(215, 70);
            this.mnuHandlingUnEvent.Opening += new System.ComponentModel.CancelEventHandler(this.mnuHandlingUnEvent_Opening);
            // 
            // mnuOutputChange
            // 
            this.mnuOutputChange.Image = global::ERP.Inventory.DUI.Properties.Resources._switch;
            this.mnuOutputChange.Name = "mnuOutputChange";
            this.mnuOutputChange.Size = new System.Drawing.Size(214, 22);
            this.mnuOutputChange.Text = "Xuất đổi hàng";
            this.mnuOutputChange.Click += new System.EventHandler(this.mnuOutputChange_Click);
            // 
            // mnuInOutHandling
            // 
            this.mnuInOutHandling.Image = global::ERP.Inventory.DUI.Properties.Resources._out;
            this.mnuInOutHandling.Name = "mnuInOutHandling";
            this.mnuInOutHandling.Size = new System.Drawing.Size(214, 22);
            this.mnuInOutHandling.Text = "Nhập thừa hoặc xuất thiếu";
            this.mnuInOutHandling.Click += new System.EventHandler(this.mnuInOutHandling_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Enabled = false;
            this.btnSave.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(964, 621);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(91, 25);
            this.btnSave.TabIndex = 20;
            this.btnSave.Text = "      Cập nhật";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmHandlingUnEvent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 653);
            this.Controls.Add(this.flexUnEvent);
            this.Controls.Add(this.btnSave);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmHandlingUnEvent";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xử lý chênh lệch kiểm kê";
            this.Load += new System.EventHandler(this.frmHandlingUnEvent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flexUnEvent)).EndInit();
            this.mnuHandlingUnEvent.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private C1.Win.C1FlexGrid.C1FlexGrid flexUnEvent;
        private System.Windows.Forms.ContextMenuStrip mnuHandlingUnEvent;
        private System.Windows.Forms.ToolStripMenuItem mnuOutputChange;
        private System.Windows.Forms.ToolStripMenuItem mnuInOutHandling;
        private System.Windows.Forms.Button btnSave;
    }
}