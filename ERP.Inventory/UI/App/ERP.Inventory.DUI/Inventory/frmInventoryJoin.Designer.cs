﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmInventoryJoin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInventoryJoin));
            this.cboInventoryType = new System.Windows.Forms.ComboBox();
            this.cboInventoryTerm = new System.Windows.Forms.ComboBox();
            this.cboStore = new System.Windows.Forms.ComboBox();
            this.cboMainGroup = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmdClose = new System.Windows.Forms.Button();
            this.cmdJoin = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cboSubGroup = new System.Windows.Forms.ComboBox();
            this.txtSubGroup = new System.Windows.Forms.TextBox();
            this.cboProductStateID = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cboInventoryType
            // 
            this.cboInventoryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInventoryType.DropDownWidth = 250;
            this.cboInventoryType.Location = new System.Drawing.Point(137, 15);
            this.cboInventoryType.Name = "cboInventoryType";
            this.cboInventoryType.Size = new System.Drawing.Size(285, 24);
            this.cboInventoryType.TabIndex = 0;
            this.cboInventoryType.DropDown += new System.EventHandler(this.cboInventoryType_DropDown);
            this.cboInventoryType.SelectionChangeCommitted += new System.EventHandler(this.cboInventoryType_SelectionChangeCommitted);
            // 
            // cboInventoryTerm
            // 
            this.cboInventoryTerm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInventoryTerm.DropDownWidth = 250;
            this.cboInventoryTerm.Enabled = false;
            this.cboInventoryTerm.Items.AddRange(new object[] {
            "-- Chọn kỳ kiểm kê --"});
            this.cboInventoryTerm.Location = new System.Drawing.Point(137, 45);
            this.cboInventoryTerm.Name = "cboInventoryTerm";
            this.cboInventoryTerm.Size = new System.Drawing.Size(285, 24);
            this.cboInventoryTerm.TabIndex = 1;
            this.cboInventoryTerm.DropDown += new System.EventHandler(this.cboInventoryTerm_DropDown);
            this.cboInventoryTerm.SelectionChangeCommitted += new System.EventHandler(this.cboInventoryTerm_SelectionChangeCommitted);
            // 
            // cboStore
            // 
            this.cboStore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStore.DropDownWidth = 250;
            this.cboStore.Enabled = false;
            this.cboStore.Items.AddRange(new object[] {
            "-- Chọn chi nhánh --"});
            this.cboStore.Location = new System.Drawing.Point(137, 75);
            this.cboStore.Name = "cboStore";
            this.cboStore.Size = new System.Drawing.Size(285, 24);
            this.cboStore.TabIndex = 2;
            this.cboStore.DropDown += new System.EventHandler(this.cboStore_DropDown);
            this.cboStore.SelectionChangeCommitted += new System.EventHandler(this.cboStore_SelectionChangeCommitted);
            // 
            // cboMainGroup
            // 
            this.cboMainGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMainGroup.DropDownWidth = 250;
            this.cboMainGroup.Enabled = false;
            this.cboMainGroup.Items.AddRange(new object[] {
            "-- Chọn ngành hàng --"});
            this.cboMainGroup.Location = new System.Drawing.Point(137, 105);
            this.cboMainGroup.Name = "cboMainGroup";
            this.cboMainGroup.Size = new System.Drawing.Size(285, 24);
            this.cboMainGroup.TabIndex = 3;
            this.cboMainGroup.DropDown += new System.EventHandler(this.cboMainGroup_DropDown);
            this.cboMainGroup.SelectionChangeCommitted += new System.EventHandler(this.cboMainGroup_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 16);
            this.label1.TabIndex = 50;
            this.label1.Text = "Loại hàng:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 79);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 16);
            this.label5.TabIndex = 46;
            this.label5.Text = "Kho kiểm kê:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 109);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 16);
            this.label6.TabIndex = 47;
            this.label6.Text = "Ngành hàng:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 49);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 16);
            this.label3.TabIndex = 45;
            this.label3.Text = "Kỳ kiểm kê:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 16);
            this.label2.TabIndex = 44;
            this.label2.Text = "Loại kiểm kê:";
            // 
            // cmdClose
            // 
            this.cmdClose.Image = ((System.Drawing.Image)(resources.GetObject("cmdClose.Image")));
            this.cmdClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdClose.Location = new System.Drawing.Point(322, 192);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Size = new System.Drawing.Size(100, 25);
            this.cmdClose.TabIndex = 10;
            this.cmdClose.Text = "     Đóng lại";
            this.cmdClose.UseVisualStyleBackColor = true;
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // cmdJoin
            // 
            this.cmdJoin.Image = ((System.Drawing.Image)(resources.GetObject("cmdJoin.Image")));
            this.cmdJoin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdJoin.Location = new System.Drawing.Point(216, 192);
            this.cmdJoin.Name = "cmdJoin";
            this.cmdJoin.Size = new System.Drawing.Size(100, 25);
            this.cmdJoin.TabIndex = 9;
            this.cmdJoin.Text = "    Nối dữ liệu";
            this.cmdJoin.Click += new System.EventHandler(this.cmdJoin_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 139);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 16);
            this.label4.TabIndex = 62;
            this.label4.Text = "Nhóm hàng:";
            // 
            // cboSubGroup
            // 
            this.cboSubGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSubGroup.DropDownWidth = 250;
            this.cboSubGroup.Enabled = false;
            this.cboSubGroup.Items.AddRange(new object[] {
            "-- Chọn nhóm hàng --"});
            this.cboSubGroup.Location = new System.Drawing.Point(137, 135);
            this.cboSubGroup.Name = "cboSubGroup";
            this.cboSubGroup.Size = new System.Drawing.Size(285, 24);
            this.cboSubGroup.TabIndex = 4;
            this.cboSubGroup.Visible = false;
            // 
            // txtSubGroup
            // 
            this.txtSubGroup.BackColor = System.Drawing.SystemColors.Info;
            this.txtSubGroup.Location = new System.Drawing.Point(137, 136);
            this.txtSubGroup.MaxLength = 200;
            this.txtSubGroup.Name = "txtSubGroup";
            this.txtSubGroup.ReadOnly = true;
            this.txtSubGroup.Size = new System.Drawing.Size(285, 22);
            this.txtSubGroup.TabIndex = 63;
            this.txtSubGroup.Text = "Tất cả";
            // 
            // cboProductStateID
            // 
            this.cboProductStateID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProductStateID.DropDownWidth = 250;
            this.cboProductStateID.Enabled = false;
            this.cboProductStateID.Location = new System.Drawing.Point(137, 162);
            this.cboProductStateID.Name = "cboProductStateID";
            this.cboProductStateID.Size = new System.Drawing.Size(285, 24);
            this.cboProductStateID.TabIndex = 81;
            // 
            // frmInventoryJoin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 224);
            this.Controls.Add(this.cboProductStateID);
            this.Controls.Add(this.cboSubGroup);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmdClose);
            this.Controls.Add(this.cmdJoin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboMainGroup);
            this.Controls.Add(this.cboStore);
            this.Controls.Add(this.cboInventoryTerm);
            this.Controls.Add(this.cboInventoryType);
            this.Controls.Add(this.txtSubGroup);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(438, 220);
            this.Name = "frmInventoryJoin";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nối dữ liệu kiểm kê";
            this.Load += new System.EventHandler(this.frmInventoryJoin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboInventoryType;
        private System.Windows.Forms.ComboBox cboInventoryTerm;
        private System.Windows.Forms.ComboBox cboStore;
        private System.Windows.Forms.ComboBox cboMainGroup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cmdClose;
        private System.Windows.Forms.Button cmdJoin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboSubGroup;
        private System.Windows.Forms.TextBox txtSubGroup;
        private System.Windows.Forms.ComboBox cboProductStateID;
    }
}