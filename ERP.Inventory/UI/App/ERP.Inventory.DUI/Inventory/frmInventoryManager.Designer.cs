﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmInventoryManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInventoryManager));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnReview = new System.Windows.Forms.Button();
            this.cboMainGroupID = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.cboInventoryTermID = new ERP.MasterData.DUI.CustomControl.ucComboSearch();
            this.ucInventoryUser = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.cmdExportExcel = new System.Windows.Forms.Button();
            this.cboStoreIDList = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.label4 = new System.Windows.Forms.Label();
            this.cboSearchType = new System.Windows.Forms.ComboBox();
            this.cboIsReviewed = new System.Windows.Forms.ComboBox();
            this.cboInventoryType = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmdSearch = new System.Windows.Forms.Button();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dtToDate = new System.Windows.Forms.DateTimePicker();
            this.dtFromDate = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkDeleted = new System.Windows.Forms.CheckBox();
            this.flexListInventory = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexListInventory)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.btnReview);
            this.groupBox1.Controls.Add(this.cboMainGroupID);
            this.groupBox1.Controls.Add(this.cboInventoryTermID);
            this.groupBox1.Controls.Add(this.ucInventoryUser);
            this.groupBox1.Controls.Add(this.cmdExportExcel);
            this.groupBox1.Controls.Add(this.cboStoreIDList);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cboSearchType);
            this.groupBox1.Controls.Add(this.cboIsReviewed);
            this.groupBox1.Controls.Add(this.cboInventoryType);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.cmdSearch);
            this.groupBox1.Controls.Add(this.txtKeywords);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.dtToDate);
            this.groupBox1.Controls.Add(this.dtFromDate);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.chkDeleted);
            this.groupBox1.Location = new System.Drawing.Point(8, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1128, 122);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(945, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(12, 16);
            this.label7.TabIndex = 56;
            this.label7.Text = "-";
            // 
            // btnReview
            // 
            this.btnReview.Enabled = false;
            this.btnReview.Image = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.btnReview.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReview.Location = new System.Drawing.Point(193, 91);
            this.btnReview.Name = "btnReview";
            this.btnReview.Size = new System.Drawing.Size(93, 25);
            this.btnReview.TabIndex = 15;
            this.btnReview.Text = "Duyệt";
            this.btnReview.UseVisualStyleBackColor = true;
            this.btnReview.Visible = false;
            this.btnReview.Click += new System.EventHandler(this.btnReview_Click);
            // 
            // cboMainGroupID
            // 
            this.cboMainGroupID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroupID.Location = new System.Drawing.Point(455, 11);
            this.cboMainGroupID.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroupID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroupID.Name = "cboMainGroupID";
            this.cboMainGroupID.Size = new System.Drawing.Size(233, 24);
            this.cboMainGroupID.TabIndex = 1;
            // 
            // cboInventoryTermID
            // 
            this.cboInventoryTermID.Caption = "";
            this.cboInventoryTermID.CharChanged = " -> ";
            this.cboInventoryTermID.FirstID = -1;
            this.cboInventoryTermID.FirstName = "";
            this.cboInventoryTermID.FocusIsShowPopup = false;
            this.cboInventoryTermID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInventoryTermID.intID = -1;
            this.cboInventoryTermID.IsShowButtonSearch = false;
            this.cboInventoryTermID.Location = new System.Drawing.Point(455, 37);
            this.cboInventoryTermID.Name = "cboInventoryTermID";
            this.cboInventoryTermID.OldID = -1;
            this.cboInventoryTermID.OldName = "";
            this.cboInventoryTermID.RowSelected = null;
            this.cboInventoryTermID.Size = new System.Drawing.Size(233, 25);
            this.cboInventoryTermID.strName = "";
            this.cboInventoryTermID.TabIndex = 4;
            this.cboInventoryTermID.ValueChanged = "";
            // 
            // ucInventoryUser
            // 
            this.ucInventoryUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucInventoryUser.IsOnlyShowRealStaff = false;
            this.ucInventoryUser.IsValidate = true;
            this.ucInventoryUser.Location = new System.Drawing.Point(805, 11);
            this.ucInventoryUser.Margin = new System.Windows.Forms.Padding(4);
            this.ucInventoryUser.MaximumSize = new System.Drawing.Size(400, 22);
            this.ucInventoryUser.MinimumSize = new System.Drawing.Size(0, 22);
            this.ucInventoryUser.Name = "ucInventoryUser";
            this.ucInventoryUser.Size = new System.Drawing.Size(289, 22);
            this.ucInventoryUser.TabIndex = 2;
            this.ucInventoryUser.UserName = "";
            this.ucInventoryUser.WorkingPositionID = 0;
            // 
            // cmdExportExcel
            // 
            this.cmdExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdExportExcel.Image")));
            this.cmdExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdExportExcel.Location = new System.Drawing.Point(1001, 91);
            this.cmdExportExcel.Name = "cmdExportExcel";
            this.cmdExportExcel.Size = new System.Drawing.Size(93, 25);
            this.cmdExportExcel.TabIndex = 13;
            this.cmdExportExcel.Text = "Xuất Excel";
            this.cmdExportExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdExportExcel.UseVisualStyleBackColor = true;
            this.cmdExportExcel.Click += new System.EventHandler(this.cmdExportExcel_Click);
            // 
            // cboStoreIDList
            // 
            this.cboStoreIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreIDList.Location = new System.Drawing.Point(110, 11);
            this.cboStoreIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreIDList.Name = "cboStoreIDList";
            this.cboStoreIDList.Size = new System.Drawing.Size(233, 24);
            this.cboStoreIDList.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(713, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 16);
            this.label4.TabIndex = 55;
            this.label4.Text = "Tìm theo:";
            // 
            // cboSearchType
            // 
            this.cboSearchType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchType.FormattingEnabled = true;
            this.cboSearchType.Items.AddRange(new object[] {
            "-- Chọn tìm theo --",
            "Mã phiếu",
            "Loại kiểm kê",
            "Kỳ kiểm kê",
            "Kho kiểm kê",
            "Ngành hàng",
            "Nhân viên kiểm kê",
            "Nhân viên tạo",
            "Nhân viên đã duyệt",
            "Nhân viên cần duyệt"});
            this.cboSearchType.Location = new System.Drawing.Point(805, 63);
            this.cboSearchType.Name = "cboSearchType";
            this.cboSearchType.Size = new System.Drawing.Size(289, 24);
            this.cboSearchType.TabIndex = 9;
            // 
            // cboIsReviewed
            // 
            this.cboIsReviewed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIsReviewed.FormattingEnabled = true;
            this.cboIsReviewed.Items.AddRange(new object[] {
            "--Tất cả--",
            "Phiếu chưa nối",
            "Phiếu đã nối",
            "Phiếu xác nhận kiểm kê - chưa duyệt",
            "Phiếu xác nhận kiểm kê - đã duyệt"});
            this.cboIsReviewed.Location = new System.Drawing.Point(110, 66);
            this.cboIsReviewed.Name = "cboIsReviewed";
            this.cboIsReviewed.Size = new System.Drawing.Size(233, 24);
            this.cboIsReviewed.TabIndex = 7;
            // 
            // cboInventoryType
            // 
            this.cboInventoryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInventoryType.FormattingEnabled = true;
            this.cboInventoryType.Location = new System.Drawing.Point(110, 38);
            this.cboInventoryType.Name = "cboInventoryType";
            this.cboInventoryType.Size = new System.Drawing.Size(233, 24);
            this.cboInventoryType.TabIndex = 3;
            this.cboInventoryType.SelectionChangeCommitted += new System.EventHandler(this.cboInventoryType_SelectionChangeCommitted);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(372, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 16);
            this.label11.TabIndex = 46;
            this.label11.Text = "Kỳ kiểm kê:";
            // 
            // cmdSearch
            // 
            this.cmdSearch.Image = ((System.Drawing.Image)(resources.GetObject("cmdSearch.Image")));
            this.cmdSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSearch.Location = new System.Drawing.Point(904, 91);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(93, 25);
            this.cmdSearch.TabIndex = 11;
            this.cmdSearch.Text = "Tìm kiếm";
            this.cmdSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdSearch.UseVisualStyleBackColor = true;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // txtKeywords
            // 
            this.txtKeywords.Location = new System.Drawing.Point(455, 67);
            this.txtKeywords.MaxLength = 200;
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.Size = new System.Drawing.Size(233, 22);
            this.txtKeywords.TabIndex = 8;
            this.txtKeywords.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInventoryID_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(372, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 16);
            this.label8.TabIndex = 37;
            this.label8.Text = "Chuỗi tìm:";
            // 
            // dtToDate
            // 
            this.dtToDate.CustomFormat = "dd/MM/yyyy";
            this.dtToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtToDate.Location = new System.Drawing.Point(978, 38);
            this.dtToDate.Name = "dtToDate";
            this.dtToDate.Size = new System.Drawing.Size(116, 22);
            this.dtToDate.TabIndex = 6;
            this.dtToDate.ValueChanged += new System.EventHandler(this.dtToDate_ValueChanged);
            // 
            // dtFromDate
            // 
            this.dtFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFromDate.Location = new System.Drawing.Point(805, 38);
            this.dtFromDate.Name = "dtFromDate";
            this.dtFromDate.Size = new System.Drawing.Size(117, 22);
            this.dtFromDate.TabIndex = 5;
            this.dtFromDate.ValueChanged += new System.EventHandler(this.dtFromDate_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(713, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 16);
            this.label9.TabIndex = 30;
            this.label9.Text = "Ngày KK:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 16);
            this.label6.TabIndex = 28;
            this.label6.Text = "Loại kiểm kê:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(713, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 16);
            this.label3.TabIndex = 26;
            this.label3.Text = "Nhân viên KK:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 16);
            this.label5.TabIndex = 25;
            this.label5.Text = "Kho kiểm kê:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(372, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 16);
            this.label2.TabIndex = 22;
            this.label2.Text = "Ngành hàng:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 16);
            this.label1.TabIndex = 52;
            this.label1.Text = "Trạng thái duyệt:";
            // 
            // chkDeleted
            // 
            this.chkDeleted.Location = new System.Drawing.Point(719, 94);
            this.chkDeleted.Name = "chkDeleted";
            this.chkDeleted.Size = new System.Drawing.Size(69, 23);
            this.chkDeleted.TabIndex = 10;
            this.chkDeleted.Text = "Đã hủy";
            this.chkDeleted.UseVisualStyleBackColor = true;
            // 
            // flexListInventory
            // 
            this.flexListInventory.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexListInventory.AllowEditing = false;
            this.flexListInventory.AllowMerging = C1.Win.C1FlexGrid.AllowMergingEnum.FixedOnly;
            this.flexListInventory.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexListInventory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flexListInventory.AutoClipboard = true;
            this.flexListInventory.ColumnInfo = "10,1,0,0,0,105,Columns:";
            this.flexListInventory.Location = new System.Drawing.Point(7, 127);
            this.flexListInventory.Name = "flexListInventory";
            this.flexListInventory.Rows.Count = 1;
            this.flexListInventory.Rows.DefaultSize = 21;
            this.flexListInventory.Size = new System.Drawing.Size(1129, 417);
            this.flexListInventory.StyleInfo = resources.GetString("flexListInventory.StyleInfo");
            this.flexListInventory.TabIndex = 4;
            this.flexListInventory.DoubleClick += new System.EventHandler(this.flexListInventory_DoubleClick);
            // 
            // frmInventoryManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1142, 545);
            this.Controls.Add(this.flexListInventory);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(951, 583);
            this.Name = "frmInventoryManager";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý phiếu kiểm kê";
            this.Load += new System.EventHandler(this.frmInventoryManager_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexListInventory)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button cmdSearch;
        private System.Windows.Forms.TextBox txtKeywords;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtToDate;
        private System.Windows.Forms.DateTimePicker dtFromDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private C1.Win.C1FlexGrid.C1FlexGrid flexListInventory;
        private System.Windows.Forms.ComboBox cboInventoryType;
        private System.Windows.Forms.CheckBox chkDeleted;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboIsReviewed;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboSearchType;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStoreIDList;
        private System.Windows.Forms.Button cmdExportExcel;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ucInventoryUser;
        private MasterData.DUI.CustomControl.ucComboSearch cboInventoryTermID;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroupID;
        private System.Windows.Forms.Button btnReview;
        private System.Windows.Forms.Label label7;
    }
}