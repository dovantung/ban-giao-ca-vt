﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmHandlingUnEventVTS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mnuHandlingUnEvent = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuOutputChange = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuInOutHandling = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSave = new System.Windows.Forms.Button();
            this.grdCtlProducts = new DevExpress.XtraGrid.GridControl();
            this.grdViewProducts = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.colISLOCK = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.chkIsLock = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colISSELECT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.chkIsSelected = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colPRODUCTID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPRODUCTNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colERP_IMEI = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colERP_PRODUCTSTATUSNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colERP_QUANTITY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.reQuantityShow = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colINV_IMEI = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colINV_PRODUCTSTATUSNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colINV_QUANTITY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSALEPRICE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colISCHANGESTATUS = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.chkISChangeStatusID = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colQUANTITY_INPUT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colQUANTITY_SALE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colQUANTITY_STORECHANGE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colQUANTITY_OUTPUT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colQUANTITY_DIFFERENT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colQUANTITY_INPUTTING = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDELTAINPUTTINGQUANTITY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colADJUSTUNEVENTQUANTITY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCHG_PRODUCTSTATUSID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repItemProductStatusIDList = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colDELTAADJUSTUNEVENTQUANTITY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colUNEVENTEXPLAIN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colNOTE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colQUANTITY_DIFINPUTTING = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repTextImei = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.colRESOLVENOTE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand24 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand23 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand22 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand31 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand25 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand27 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand26 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand28 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand29 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.chkOnlyUnEvent = new System.Windows.Forms.CheckBox();
            this.mnuHandlingUnEvent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCtlProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsLock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reQuantityShow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkISChangeStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemProductStatusIDList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextImei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuHandlingUnEvent
            // 
            this.mnuHandlingUnEvent.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuOutputChange,
            this.mnuInOutHandling});
            this.mnuHandlingUnEvent.Name = "mnuHandlingUnEvent";
            this.mnuHandlingUnEvent.Size = new System.Drawing.Size(215, 48);
            this.mnuHandlingUnEvent.Opening += new System.ComponentModel.CancelEventHandler(this.mnuHandlingUnEvent_Opening);
            // 
            // mnuOutputChange
            // 
            this.mnuOutputChange.Image = global::ERP.Inventory.DUI.Properties.Resources._switch;
            this.mnuOutputChange.Name = "mnuOutputChange";
            this.mnuOutputChange.Size = new System.Drawing.Size(214, 22);
            this.mnuOutputChange.Text = "Xuất đổi hàng";
            this.mnuOutputChange.Click += new System.EventHandler(this.mnuOutputChange_Click);
            // 
            // mnuInOutHandling
            // 
            this.mnuInOutHandling.Image = global::ERP.Inventory.DUI.Properties.Resources._out;
            this.mnuInOutHandling.Name = "mnuInOutHandling";
            this.mnuInOutHandling.Size = new System.Drawing.Size(214, 22);
            this.mnuInOutHandling.Text = "Nhập thừa hoặc xuất thiếu";
            this.mnuInOutHandling.Click += new System.EventHandler(this.mnuInOutHandling_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Enabled = false;
            this.btnSave.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(971, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(91, 25);
            this.btnSave.TabIndex = 20;
            this.btnSave.Text = "      Cập nhật";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // grdCtlProducts
            // 
            this.grdCtlProducts.ContextMenuStrip = this.mnuHandlingUnEvent;
            this.grdCtlProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCtlProducts.Location = new System.Drawing.Point(0, 0);
            this.grdCtlProducts.MainView = this.grdViewProducts;
            this.grdCtlProducts.Name = "grdCtlProducts";
            this.grdCtlProducts.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repTextImei,
            this.repQuantity,
            this.repNote,
            this.reQuantityShow,
            this.repItemProductStatusIDList,
            this.chkISChangeStatusID,
            this.chkIsSelected,
            this.chkIsLock});
            this.grdCtlProducts.Size = new System.Drawing.Size(1067, 619);
            this.grdCtlProducts.TabIndex = 84;
            this.grdCtlProducts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewProducts});
            // 
            // grdViewProducts
            // 
            this.grdViewProducts.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.BandPanel.Options.UseFont = true;
            this.grdViewProducts.Appearance.BandPanel.Options.UseTextOptions = true;
            this.grdViewProducts.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewProducts.Appearance.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewProducts.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProducts.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewProducts.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewProducts.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewProducts.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewProducts.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProducts.Appearance.Preview.Options.UseFont = true;
            this.grdViewProducts.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProducts.Appearance.Row.Options.UseFont = true;
            this.grdViewProducts.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand24,
            this.gridBand23,
            this.gridBand1,
            this.gridBand4,
            this.gridBand8,
            this.gridBand18,
            this.gridBand16,
            this.gridBand11,
            this.gridBand20,
            this.gridBand22,
            this.gridBand31,
            this.gridBand25,
            this.gridBand29,
            this.gridBand17,
            this.gridBand19});
            this.grdViewProducts.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colPRODUCTID,
            this.colPRODUCTNAME,
            this.colERP_IMEI,
            this.colERP_QUANTITY,
            this.colERP_PRODUCTSTATUSNAME,
            this.colINV_IMEI,
            this.colINV_QUANTITY,
            this.colINV_PRODUCTSTATUSNAME,
            this.colISCHANGESTATUS,
            this.colQUANTITY_INPUTTING,
            this.colQUANTITY_INPUT,
            this.colQUANTITY_SALE,
            this.colQUANTITY_STORECHANGE,
            this.colQUANTITY_OUTPUT,
            this.colQUANTITY_DIFINPUTTING,
            this.colQUANTITY_DIFFERENT,
            this.colUNEVENTEXPLAIN,
            this.colNOTE,
            this.colISSELECT,
            this.colISLOCK,
            this.colSALEPRICE,
            this.colCHG_PRODUCTSTATUSID,
            this.colADJUSTUNEVENTQUANTITY,
            this.colDELTAADJUSTUNEVENTQUANTITY,
            this.colDELTAINPUTTINGQUANTITY,
            this.colRESOLVENOTE});
            this.grdViewProducts.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewProducts.GridControl = this.grdCtlProducts;
            this.grdViewProducts.MinBandPanelRowCount = 2;
            this.grdViewProducts.Name = "grdViewProducts";
            this.grdViewProducts.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewProducts.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewProducts.OptionsNavigation.UseTabKey = false;
            this.grdViewProducts.OptionsView.ColumnAutoWidth = false;
            this.grdViewProducts.OptionsView.ShowAutoFilterRow = true;
            this.grdViewProducts.OptionsView.ShowColumnHeaders = false;
            this.grdViewProducts.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewProducts.OptionsView.ShowFooter = true;
            this.grdViewProducts.OptionsView.ShowGroupPanel = false;
            this.grdViewProducts.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grdViewProducts_ShowingEditor);
            this.grdViewProducts.HiddenEditor += new System.EventHandler(this.grdViewProducts_HiddenEditor);
            // 
            // colISLOCK
            // 
            this.colISLOCK.Caption = "ISLOCK";
            this.colISLOCK.ColumnEdit = this.chkIsLock;
            this.colISLOCK.FieldName = "ISLOCK";
            this.colISLOCK.Name = "colISLOCK";
            this.colISLOCK.Visible = true;
            this.colISLOCK.Width = 45;
            // 
            // chkIsLock
            // 
            this.chkIsLock.AutoHeight = false;
            this.chkIsLock.Name = "chkIsLock";
            this.chkIsLock.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // colISSELECT
            // 
            this.colISSELECT.Caption = "ISSELECT";
            this.colISSELECT.ColumnEdit = this.chkIsSelected;
            this.colISSELECT.FieldName = "ISSELECT";
            this.colISSELECT.Name = "colISSELECT";
            this.colISSELECT.Visible = true;
            this.colISSELECT.Width = 45;
            // 
            // chkIsSelected
            // 
            this.chkIsSelected.AutoHeight = false;
            this.chkIsSelected.Name = "chkIsSelected";
            this.chkIsSelected.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // colPRODUCTID
            // 
            this.colPRODUCTID.Caption = "MÃ SẢN PHẨM";
            this.colPRODUCTID.FieldName = "PRODUCTID";
            this.colPRODUCTID.Name = "colPRODUCTID";
            this.colPRODUCTID.OptionsColumn.AllowEdit = false;
            this.colPRODUCTID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPRODUCTID.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "PRODUCTID", "Tổng cổng")});
            this.colPRODUCTID.Visible = true;
            this.colPRODUCTID.Width = 120;
            // 
            // colPRODUCTNAME
            // 
            this.colPRODUCTNAME.Caption = "TÊN SẢN PHẨM";
            this.colPRODUCTNAME.FieldName = "PRODUCTNAME";
            this.colPRODUCTNAME.Name = "colPRODUCTNAME";
            this.colPRODUCTNAME.OptionsColumn.AllowEdit = false;
            this.colPRODUCTNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPRODUCTNAME.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "PRODUCTNAME", "{0:N0}")});
            this.colPRODUCTNAME.Visible = true;
            this.colPRODUCTNAME.Width = 220;
            // 
            // colERP_IMEI
            // 
            this.colERP_IMEI.Caption = "ERP_IMEI";
            this.colERP_IMEI.FieldName = "STOCKIMEI";
            this.colERP_IMEI.Name = "colERP_IMEI";
            this.colERP_IMEI.OptionsColumn.AllowEdit = false;
            this.colERP_IMEI.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colERP_IMEI.Visible = true;
            this.colERP_IMEI.Width = 130;
            // 
            // colERP_PRODUCTSTATUSNAME
            // 
            this.colERP_PRODUCTSTATUSNAME.Caption = "ERP_TRẠNG THÁI";
            this.colERP_PRODUCTSTATUSNAME.FieldName = "ERP_PRODUCTSTATUSNAME";
            this.colERP_PRODUCTSTATUSNAME.Name = "colERP_PRODUCTSTATUSNAME";
            this.colERP_PRODUCTSTATUSNAME.OptionsColumn.AllowEdit = false;
            this.colERP_PRODUCTSTATUSNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colERP_PRODUCTSTATUSNAME.Visible = true;
            this.colERP_PRODUCTSTATUSNAME.Width = 130;
            // 
            // colERP_QUANTITY
            // 
            this.colERP_QUANTITY.Caption = "ERP_SỐ LƯỢNG";
            this.colERP_QUANTITY.ColumnEdit = this.reQuantityShow;
            this.colERP_QUANTITY.FieldName = "STOCKQUANTITY";
            this.colERP_QUANTITY.Name = "colERP_QUANTITY";
            this.colERP_QUANTITY.OptionsColumn.AllowEdit = false;
            this.colERP_QUANTITY.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colERP_QUANTITY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "STOCKQUANTITY", "{0:N0}")});
            this.colERP_QUANTITY.Visible = true;
            this.colERP_QUANTITY.Width = 70;
            // 
            // reQuantityShow
            // 
            this.reQuantityShow.AutoHeight = false;
            this.reQuantityShow.Mask.EditMask = "N0";
            this.reQuantityShow.Mask.UseMaskAsDisplayFormat = true;
            this.reQuantityShow.Name = "reQuantityShow";
            this.reQuantityShow.NullText = "0";
            this.reQuantityShow.NullValuePrompt = "0";
            // 
            // colINV_IMEI
            // 
            this.colINV_IMEI.Caption = "INV_IMEI";
            this.colINV_IMEI.FieldName = "INVENTORYIMEI";
            this.colINV_IMEI.Name = "colINV_IMEI";
            this.colINV_IMEI.OptionsColumn.AllowEdit = false;
            this.colINV_IMEI.Visible = true;
            this.colINV_IMEI.Width = 120;
            // 
            // colINV_PRODUCTSTATUSNAME
            // 
            this.colINV_PRODUCTSTATUSNAME.Caption = "INV_TRẠNG THÁI";
            this.colINV_PRODUCTSTATUSNAME.FieldName = "INV_PRODUCTSTATUSNAME";
            this.colINV_PRODUCTSTATUSNAME.Name = "colINV_PRODUCTSTATUSNAME";
            this.colINV_PRODUCTSTATUSNAME.OptionsColumn.AllowEdit = false;
            this.colINV_PRODUCTSTATUSNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colINV_PRODUCTSTATUSNAME.Visible = true;
            this.colINV_PRODUCTSTATUSNAME.Width = 130;
            // 
            // colINV_QUANTITY
            // 
            this.colINV_QUANTITY.Caption = "INV_SỐ LƯỢNG";
            this.colINV_QUANTITY.ColumnEdit = this.reQuantityShow;
            this.colINV_QUANTITY.FieldName = "INVENTORYQUANTITY";
            this.colINV_QUANTITY.Name = "colINV_QUANTITY";
            this.colINV_QUANTITY.OptionsColumn.AllowEdit = false;
            this.colINV_QUANTITY.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colINV_QUANTITY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "INVENTORYQUANTITY", "{0:N0}")});
            this.colINV_QUANTITY.Visible = true;
            this.colINV_QUANTITY.Width = 70;
            // 
            // colSALEPRICE
            // 
            this.colSALEPRICE.Caption = "SALEPRICE";
            this.colSALEPRICE.ColumnEdit = this.reQuantityShow;
            this.colSALEPRICE.FieldName = "SALEPRICE";
            this.colSALEPRICE.Name = "colSALEPRICE";
            this.colSALEPRICE.OptionsColumn.AllowEdit = false;
            this.colSALEPRICE.Visible = true;
            this.colSALEPRICE.Width = 109;
            // 
            // colISCHANGESTATUS
            // 
            this.colISCHANGESTATUS.AppearanceCell.ForeColor = System.Drawing.Color.Red;
            this.colISCHANGESTATUS.AppearanceCell.Options.UseForeColor = true;
            this.colISCHANGESTATUS.Caption = "LÀ CHUYỂN TRẠNG THÁI";
            this.colISCHANGESTATUS.ColumnEdit = this.chkISChangeStatusID;
            this.colISCHANGESTATUS.FieldName = "ISCHANGESTATUS";
            this.colISCHANGESTATUS.Name = "colISCHANGESTATUS";
            this.colISCHANGESTATUS.OptionsColumn.AllowEdit = false;
            this.colISCHANGESTATUS.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colISCHANGESTATUS.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DIFFERENT", "{0:N0}")});
            this.colISCHANGESTATUS.Visible = true;
            this.colISCHANGESTATUS.Width = 70;
            // 
            // chkISChangeStatusID
            // 
            this.chkISChangeStatusID.AutoHeight = false;
            this.chkISChangeStatusID.Name = "chkISChangeStatusID";
            this.chkISChangeStatusID.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // colQUANTITY_INPUT
            // 
            this.colQUANTITY_INPUT.AppearanceCell.ForeColor = System.Drawing.Color.Blue;
            this.colQUANTITY_INPUT.AppearanceCell.Options.UseForeColor = true;
            this.colQUANTITY_INPUT.Caption = "SL NHẬP";
            this.colQUANTITY_INPUT.ColumnEdit = this.repQuantity;
            this.colQUANTITY_INPUT.FieldName = "QUANTITY_INPUT";
            this.colQUANTITY_INPUT.Name = "colQUANTITY_INPUT";
            this.colQUANTITY_INPUT.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_INPUT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQUANTITY_INPUT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_INPUT", "{0:N0}")});
            this.colQUANTITY_INPUT.Visible = true;
            this.colQUANTITY_INPUT.Width = 70;
            // 
            // repQuantity
            // 
            this.repQuantity.AutoHeight = false;
            this.repQuantity.DisplayFormat.FormatString = "N0";
            this.repQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repQuantity.Mask.EditMask = "N0";
            this.repQuantity.Mask.UseMaskAsDisplayFormat = true;
            this.repQuantity.MaxValue = new decimal(new int[] {
            1215752191,
            23,
            0,
            0});
            this.repQuantity.Name = "repQuantity";
            this.repQuantity.NullText = "0";
            this.repQuantity.NullValuePrompt = "0";
            // 
            // colQUANTITY_SALE
            // 
            this.colQUANTITY_SALE.AppearanceCell.ForeColor = System.Drawing.Color.Orange;
            this.colQUANTITY_SALE.AppearanceCell.Options.UseForeColor = true;
            this.colQUANTITY_SALE.Caption = "SL XUẤT BÁN";
            this.colQUANTITY_SALE.ColumnEdit = this.repQuantity;
            this.colQUANTITY_SALE.FieldName = "QUANTITY_SALE";
            this.colQUANTITY_SALE.Name = "colQUANTITY_SALE";
            this.colQUANTITY_SALE.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_SALE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQUANTITY_SALE.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_SALE", "{0:N0}")});
            this.colQUANTITY_SALE.Visible = true;
            this.colQUANTITY_SALE.Width = 70;
            // 
            // colQUANTITY_STORECHANGE
            // 
            this.colQUANTITY_STORECHANGE.AppearanceCell.ForeColor = System.Drawing.Color.Orange;
            this.colQUANTITY_STORECHANGE.AppearanceCell.Options.UseForeColor = true;
            this.colQUANTITY_STORECHANGE.Caption = "SL CHUYỂN KHO";
            this.colQUANTITY_STORECHANGE.ColumnEdit = this.repQuantity;
            this.colQUANTITY_STORECHANGE.FieldName = "QUANTITY_STORECHANGE";
            this.colQUANTITY_STORECHANGE.Name = "colQUANTITY_STORECHANGE";
            this.colQUANTITY_STORECHANGE.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_STORECHANGE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQUANTITY_STORECHANGE.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_STORECHANGE", "{0:N0}")});
            this.colQUANTITY_STORECHANGE.Visible = true;
            this.colQUANTITY_STORECHANGE.Width = 85;
            // 
            // colQUANTITY_OUTPUT
            // 
            this.colQUANTITY_OUTPUT.AppearanceCell.ForeColor = System.Drawing.Color.Orange;
            this.colQUANTITY_OUTPUT.AppearanceCell.Options.UseForeColor = true;
            this.colQUANTITY_OUTPUT.Caption = "SL XUẤT KHÁC";
            this.colQUANTITY_OUTPUT.ColumnEdit = this.repQuantity;
            this.colQUANTITY_OUTPUT.FieldName = "QUANTITY_OUTPUT";
            this.colQUANTITY_OUTPUT.Name = "colQUANTITY_OUTPUT";
            this.colQUANTITY_OUTPUT.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_OUTPUT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQUANTITY_OUTPUT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_OUTPUT", "{0:N0}")});
            this.colQUANTITY_OUTPUT.Visible = true;
            this.colQUANTITY_OUTPUT.Width = 70;
            // 
            // colQUANTITY_DIFFERENT
            // 
            this.colQUANTITY_DIFFERENT.AppearanceCell.ForeColor = System.Drawing.Color.Red;
            this.colQUANTITY_DIFFERENT.AppearanceCell.Options.UseForeColor = true;
            this.colQUANTITY_DIFFERENT.Caption = "SL CHÊNH LỆCH SAU GIẢI TRÌNH";
            this.colQUANTITY_DIFFERENT.ColumnEdit = this.reQuantityShow;
            this.colQUANTITY_DIFFERENT.FieldName = "QUANTITY_DIFFERENT";
            this.colQUANTITY_DIFFERENT.Name = "colQUANTITY_DIFFERENT";
            this.colQUANTITY_DIFFERENT.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_DIFFERENT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQUANTITY_DIFFERENT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_DIFFERENT", "{0:N0}")});
            this.colQUANTITY_DIFFERENT.Visible = true;
            this.colQUANTITY_DIFFERENT.Width = 85;
            // 
            // colQUANTITY_INPUTTING
            // 
            this.colQUANTITY_INPUTTING.Caption = "SL HÀNG ĐANG ĐI ĐƯỜNG";
            this.colQUANTITY_INPUTTING.ColumnEdit = this.repQuantity;
            this.colQUANTITY_INPUTTING.FieldName = "INPUTTINGQUANTITY";
            this.colQUANTITY_INPUTTING.Name = "colQUANTITY_INPUTTING";
            this.colQUANTITY_INPUTTING.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_INPUTTING", "{0:N0}")});
            this.colQUANTITY_INPUTTING.Visible = true;
            this.colQUANTITY_INPUTTING.Width = 70;
            // 
            // colDELTAINPUTTINGQUANTITY
            // 
            this.colDELTAINPUTTINGQUANTITY.Caption = "CHÊNH LỆCH SAU KHI TRỪ HÀNG ĐANG ĐI ĐƯỜNG";
            this.colDELTAINPUTTINGQUANTITY.ColumnEdit = this.reQuantityShow;
            this.colDELTAINPUTTINGQUANTITY.FieldName = "DELTAINPUTTINGQUANTITY";
            this.colDELTAINPUTTINGQUANTITY.Name = "colDELTAINPUTTINGQUANTITY";
            this.colDELTAINPUTTINGQUANTITY.OptionsColumn.AllowEdit = false;
            this.colDELTAINPUTTINGQUANTITY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DELTAINPUTTINGQUANTITY", "{0:N0}")});
            this.colDELTAINPUTTINGQUANTITY.Visible = true;
            this.colDELTAINPUTTINGQUANTITY.Width = 180;
            // 
            // colADJUSTUNEVENTQUANTITY
            // 
            this.colADJUSTUNEVENTQUANTITY.Caption = "ĐIỀU CHỈNH";
            this.colADJUSTUNEVENTQUANTITY.ColumnEdit = this.repQuantity;
            this.colADJUSTUNEVENTQUANTITY.FieldName = "ADJUSTUNEVENTQUANTITY";
            this.colADJUSTUNEVENTQUANTITY.Name = "colADJUSTUNEVENTQUANTITY";
            this.colADJUSTUNEVENTQUANTITY.OptionsColumn.AllowEdit = false;
            this.colADJUSTUNEVENTQUANTITY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ADJUSTUNEVENTQUANTITY", "{0:N0}")});
            this.colADJUSTUNEVENTQUANTITY.Visible = true;
            this.colADJUSTUNEVENTQUANTITY.Width = 91;
            // 
            // colCHG_PRODUCTSTATUSID
            // 
            this.colCHG_PRODUCTSTATUSID.Caption = "CHG_PRODUCTSTATUSID";
            this.colCHG_PRODUCTSTATUSID.ColumnEdit = this.repItemProductStatusIDList;
            this.colCHG_PRODUCTSTATUSID.FieldName = "CHG_PRODUCTSTATUSID";
            this.colCHG_PRODUCTSTATUSID.Name = "colCHG_PRODUCTSTATUSID";
            this.colCHG_PRODUCTSTATUSID.Visible = true;
            this.colCHG_PRODUCTSTATUSID.Width = 248;
            // 
            // repItemProductStatusIDList
            // 
            this.repItemProductStatusIDList.AutoHeight = false;
            this.repItemProductStatusIDList.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemProductStatusIDList.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PRODUCTSTATUSNAME", 120, "Trạng Thái")});
            this.repItemProductStatusIDList.DisplayMember = "PRODUCTSTATUSNAME";
            this.repItemProductStatusIDList.Name = "repItemProductStatusIDList";
            this.repItemProductStatusIDList.ValueMember = "PRODUCTSTATUSID";
            // 
            // colDELTAADJUSTUNEVENTQUANTITY
            // 
            this.colDELTAADJUSTUNEVENTQUANTITY.Caption = "CHÊNH LỆCH SAU KHI ĐIỀU CHỈNH";
            this.colDELTAADJUSTUNEVENTQUANTITY.ColumnEdit = this.reQuantityShow;
            this.colDELTAADJUSTUNEVENTQUANTITY.FieldName = "DELTAADJUSTUNEVENTQUANTITY";
            this.colDELTAADJUSTUNEVENTQUANTITY.Name = "colDELTAADJUSTUNEVENTQUANTITY";
            this.colDELTAADJUSTUNEVENTQUANTITY.OptionsColumn.AllowEdit = false;
            this.colDELTAADJUSTUNEVENTQUANTITY.Visible = true;
            // 
            // colUNEVENTEXPLAIN
            // 
            this.colUNEVENTEXPLAIN.Caption = "GIẢI TRÌNH CHÊCH LỆCH";
            this.colUNEVENTEXPLAIN.ColumnEdit = this.repNote;
            this.colUNEVENTEXPLAIN.FieldName = "UNEVENTEXPLAIN";
            this.colUNEVENTEXPLAIN.Name = "colUNEVENTEXPLAIN";
            this.colUNEVENTEXPLAIN.OptionsColumn.AllowEdit = false;
            this.colUNEVENTEXPLAIN.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colUNEVENTEXPLAIN.Visible = true;
            this.colUNEVENTEXPLAIN.Width = 220;
            // 
            // repNote
            // 
            this.repNote.AutoHeight = false;
            this.repNote.MaxLength = 2000;
            this.repNote.Name = "repNote";
            // 
            // colNOTE
            // 
            this.colNOTE.Caption = "GHI CHÚ";
            this.colNOTE.ColumnEdit = this.repNote;
            this.colNOTE.FieldName = "NOTE";
            this.colNOTE.Name = "colNOTE";
            this.colNOTE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colNOTE.Visible = true;
            this.colNOTE.Width = 220;
            // 
            // colQUANTITY_DIFINPUTTING
            // 
            this.colQUANTITY_DIFINPUTTING.Caption = "QUANTITY_DIFINPUTTING";
            this.colQUANTITY_DIFINPUTTING.FieldName = "QUANTITY_DIFINPUTTING";
            this.colQUANTITY_DIFINPUTTING.Name = "colQUANTITY_DIFINPUTTING";
            // 
            // repTextImei
            // 
            this.repTextImei.AutoHeight = false;
            this.repTextImei.MaxLength = 50;
            this.repTextImei.Name = "repTextImei";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.chkOnlyUnEvent);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 619);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1067, 34);
            this.panelControl1.TabIndex = 85;
            // 
            // colRESOLVENOTE
            // 
            this.colRESOLVENOTE.Caption = "Ghi chú điều chỉnh";
            this.colRESOLVENOTE.FieldName = "RESOLVENOTE";
            this.colRESOLVENOTE.Name = "colRESOLVENOTE";
            this.colRESOLVENOTE.OptionsColumn.AllowEdit = false;
            this.colRESOLVENOTE.Visible = true;
            this.colRESOLVENOTE.Width = 150;
            // 
            // gridBand24
            // 
            this.gridBand24.Caption = "Khóa";
            this.gridBand24.Columns.Add(this.colISLOCK);
            this.gridBand24.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand24.Name = "gridBand24";
            this.gridBand24.Width = 45;
            // 
            // gridBand23
            // 
            this.gridBand23.Caption = "Chọn";
            this.gridBand23.Columns.Add(this.colISSELECT);
            this.gridBand23.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand23.Name = "gridBand23";
            this.gridBand23.Width = 45;
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "Thông tin sản phẩm";
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand3});
            this.gridBand1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 340;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "Mã sản phẩm";
            this.gridBand2.Columns.Add(this.colPRODUCTID);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.OptionsBand.AllowMove = false;
            this.gridBand2.OptionsBand.FixedWidth = true;
            this.gridBand2.Width = 120;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "Tên sản phẩm";
            this.gridBand3.Columns.Add(this.colPRODUCTNAME);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 220;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "ERP";
            this.gridBand4.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand5,
            this.gridBand6,
            this.gridBand7});
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Width = 330;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "IMEI";
            this.gridBand5.Columns.Add(this.colERP_IMEI);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.Width = 130;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "Trạng thái";
            this.gridBand6.Columns.Add(this.colERP_PRODUCTSTATUSNAME);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.Width = 130;
            // 
            // gridBand7
            // 
            this.gridBand7.Caption = "Tồn kho";
            this.gridBand7.Columns.Add(this.colERP_QUANTITY);
            this.gridBand7.Name = "gridBand7";
            // 
            // gridBand8
            // 
            this.gridBand8.Caption = "Kiểm kê";
            this.gridBand8.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand21,
            this.gridBand9,
            this.gridBand10});
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.Width = 320;
            // 
            // gridBand21
            // 
            this.gridBand21.Caption = "IMEI";
            this.gridBand21.Columns.Add(this.colINV_IMEI);
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.Width = 120;
            // 
            // gridBand9
            // 
            this.gridBand9.Caption = "Trạng thái";
            this.gridBand9.Columns.Add(this.colINV_PRODUCTSTATUSNAME);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.Width = 130;
            // 
            // gridBand10
            // 
            this.gridBand10.Caption = "Tồn kho";
            this.gridBand10.Columns.Add(this.colINV_QUANTITY);
            this.gridBand10.Name = "gridBand10";
            // 
            // gridBand18
            // 
            this.gridBand18.Caption = "Đơn giá    (Chưa có VAT)";
            this.gridBand18.Columns.Add(this.colSALEPRICE);
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.Width = 109;
            // 
            // gridBand16
            // 
            this.gridBand16.Caption = "Sai trạng thái";
            this.gridBand16.Columns.Add(this.colISCHANGESTATUS);
            this.gridBand16.Name = "gridBand16";
            // 
            // gridBand11
            // 
            this.gridBand11.Caption = "Giả trình chênh lệch";
            this.gridBand11.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand12,
            this.gridBand13,
            this.gridBand14,
            this.gridBand15});
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.Width = 295;
            // 
            // gridBand12
            // 
            this.gridBand12.Caption = "Nhập";
            this.gridBand12.Columns.Add(this.colQUANTITY_INPUT);
            this.gridBand12.Name = "gridBand12";
            // 
            // gridBand13
            // 
            this.gridBand13.Caption = "xuất bán";
            this.gridBand13.Columns.Add(this.colQUANTITY_SALE);
            this.gridBand13.Name = "gridBand13";
            // 
            // gridBand14
            // 
            this.gridBand14.Caption = "xuất chuyển kho";
            this.gridBand14.Columns.Add(this.colQUANTITY_STORECHANGE);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.RowCount = 2;
            this.gridBand14.Width = 85;
            // 
            // gridBand15
            // 
            this.gridBand15.Caption = "Xuất khác";
            this.gridBand15.Columns.Add(this.colQUANTITY_OUTPUT);
            this.gridBand15.Name = "gridBand15";
            // 
            // gridBand20
            // 
            this.gridBand20.Caption = "SL Chênh lệch sau giải trình";
            this.gridBand20.Columns.Add(this.colQUANTITY_DIFFERENT);
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.Width = 85;
            // 
            // gridBand22
            // 
            this.gridBand22.Caption = "SL Hàng đi đường";
            this.gridBand22.Columns.Add(this.colQUANTITY_INPUTTING);
            this.gridBand22.Name = "gridBand22";
            // 
            // gridBand31
            // 
            this.gridBand31.Caption = "Chênh lệch sau khi giải trình hàng đi đường";
            this.gridBand31.Columns.Add(this.colDELTAINPUTTINGQUANTITY);
            this.gridBand31.Name = "gridBand31";
            this.gridBand31.Width = 180;
            // 
            // gridBand25
            // 
            this.gridBand25.Caption = "Xử lý chênh lệch";
            this.gridBand25.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand27,
            this.gridBand26,
            this.gridBand28});
            this.gridBand25.Name = "gridBand25";
            this.gridBand25.Width = 489;
            // 
            // gridBand27
            // 
            this.gridBand27.Caption = "SL Điều chỉnh";
            this.gridBand27.Columns.Add(this.colADJUSTUNEVENTQUANTITY);
            this.gridBand27.Name = "gridBand27";
            this.gridBand27.Width = 91;
            // 
            // gridBand26
            // 
            this.gridBand26.Caption = "Trạng thái";
            this.gridBand26.Columns.Add(this.colCHG_PRODUCTSTATUSID);
            this.gridBand26.Name = "gridBand26";
            this.gridBand26.Width = 248;
            // 
            // gridBand28
            // 
            this.gridBand28.Caption = "Ghi chú điều chỉnh";
            this.gridBand28.Columns.Add(this.colRESOLVENOTE);
            this.gridBand28.Name = "gridBand28";
            this.gridBand28.Width = 150;
            // 
            // gridBand29
            // 
            this.gridBand29.Caption = "SL chênh lệch sau khi xử lý";
            this.gridBand29.Columns.Add(this.colDELTAADJUSTUNEVENTQUANTITY);
            this.gridBand29.Name = "gridBand29";
            this.gridBand29.Width = 75;
            // 
            // gridBand17
            // 
            this.gridBand17.Caption = "Giải trình chênh lệch";
            this.gridBand17.Columns.Add(this.colUNEVENTEXPLAIN);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.Width = 220;
            // 
            // gridBand19
            // 
            this.gridBand19.Caption = "Ghi chú";
            this.gridBand19.Columns.Add(this.colNOTE);
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.Width = 220;
            // 
            // chkOnlyUnEvent
            // 
            this.chkOnlyUnEvent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkOnlyUnEvent.AutoSize = true;
            this.chkOnlyUnEvent.Checked = true;
            this.chkOnlyUnEvent.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOnlyUnEvent.Location = new System.Drawing.Point(759, 8);
            this.chkOnlyUnEvent.Name = "chkOnlyUnEvent";
            this.chkOnlyUnEvent.Size = new System.Drawing.Size(206, 20);
            this.chkOnlyUnEvent.TabIndex = 83;
            this.chkOnlyUnEvent.Text = "Chỉ hiển thị sản phẩm cần xử lý";
            this.chkOnlyUnEvent.UseVisualStyleBackColor = true;
            this.chkOnlyUnEvent.CheckedChanged += new System.EventHandler(this.chkOnlyUnEvent_CheckedChanged);
            // 
            // frmHandlingUnEventVTS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 653);
            this.Controls.Add(this.grdCtlProducts);
            this.Controls.Add(this.panelControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmHandlingUnEventVTS";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xử lý chênh lệch kiểm kê";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmHandlingUnEvent_Load);
            this.mnuHandlingUnEvent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdCtlProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsLock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reQuantityShow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkISChangeStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemProductStatusIDList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextImei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip mnuHandlingUnEvent;
        private System.Windows.Forms.ToolStripMenuItem mnuOutputChange;
        private System.Windows.Forms.ToolStripMenuItem mnuInOutHandling;
        private System.Windows.Forms.Button btnSave;
        private DevExpress.XtraGrid.GridControl grdCtlProducts;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView grdViewProducts;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPRODUCTID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPRODUCTNAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colERP_IMEI;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colERP_PRODUCTSTATUSNAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colERP_QUANTITY;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit reQuantityShow;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colINV_IMEI;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colINV_PRODUCTSTATUSNAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colINV_QUANTITY;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colISCHANGESTATUS;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_INPUT;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repQuantity;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_SALE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_STORECHANGE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_OUTPUT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_DIFFERENT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colUNEVENTEXPLAIN;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repNote;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colNOTE;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTextImei;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_INPUTTING;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_DIFINPUTTING;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colISSELECT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colISLOCK;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSALEPRICE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCHG_PRODUCTSTATUSID;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repItemProductStatusIDList;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colADJUSTUNEVENTQUANTITY;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelected;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsLock;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkISChangeStatusID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDELTAADJUSTUNEVENTQUANTITY;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDELTAINPUTTINGQUANTITY;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand24;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand23;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand22;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand31;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand25;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand27;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand26;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRESOLVENOTE;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand29;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private System.Windows.Forms.CheckBox chkOnlyUnEvent;
    }
}