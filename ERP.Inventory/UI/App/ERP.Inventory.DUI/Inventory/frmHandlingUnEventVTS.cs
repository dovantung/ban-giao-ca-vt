﻿using DevExpress.XtraGrid;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmHandlingUnEventVTS : Form
    {
        public frmHandlingUnEventVTS()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        private DataTable tblUnEventTable = null;
        private DataTable tblDataTable = null;
        private DataTable tblDataFull = null;
        private DataTable dtbProductChange = null;
        //private DataTable dtbStoreInStock = null;
        private ERP.Inventory.PLC.ProductChange.PLCProductChange objPLCProductChange = new PLC.ProductChange.PLCProductChange();
        private ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        private ERP.Inventory.PLC.Inventory.PLCInventoryTerm objPLCInventoryTerm = new PLC.Inventory.PLCInventoryTerm();
        private ERP.Inventory.PLC.Inventory.PLCInventory objPLCInventory = new PLC.Inventory.PLCInventory();
        private ERP.Inventory.PLC.Inventory.PLCInventoryProcess objPLCInventoryProcess = new PLC.Inventory.PLCInventoryProcess();
        public int intInventoryTermID = 0;
        public string strInventoryID = string.Empty;
        public int intStoreID = 0;
        public DateTime dtmInventoryDate;
        public int intMainGroupID = 0;
        public int intIsNew = 0;
        public bool bolIsDeleted = false;

        public DataTable UnEventTable
        {
            get { return tblUnEventTable; }
            set { tblUnEventTable = value; }
        }

        public DataTable dtbDataTable
        {
            get { return tblDataTable; }
            set { tblDataTable = value; }
        }

        private void frmHandlingUnEvent_Load(object sender, EventArgs e)
        {
            if (tblUnEventTable == null)
            {
                tblUnEventTable = InitFlex();
            }
            DataTable dtbProductStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
            repItemProductStatusIDList.DataSource = dtbProductStatus;

            dtbDataTable = InitFlex();

            if (!dtbDataTable.Columns.Contains("ResolveQuantity2"))
            {
                dtbDataTable.Columns.Add("ResolveQuantity2", typeof(int));
                dtbDataTable.Columns["ResolveQuantity2"].SetOrdinal(15);
            }
            if (!dtbDataTable.Columns.Contains("CostPrice"))
            {
                dtbDataTable.Columns.Add("CostPrice", typeof(decimal));
            }

            if (tblUnEventTable != null && tblUnEventTable.Rows.Count > 0)
            {
                //DataTable dtbUnEventInputtingQuantity = null;
                //try
                //{
                //    dtbUnEventInputtingQuantity = objInventoryObject.LoadUnEventInputtingQuantity(objInventoryObject.InventoryID);
                //}
                //catch { }
                foreach (DataRow rUnEvent in tblUnEventTable.Rows)
                {
                    string strProductID = Convert.ToString(rUnEvent["ProductID"]).Trim();
                    string strIMEI = string.Empty;
                    bool bolStockIMEI = false;
                    if (!Convert.IsDBNull(rUnEvent["StockIMEI"]))
                    {
                        strIMEI = Convert.ToString(rUnEvent["StockIMEI"]).Trim();
                        bolStockIMEI = true;
                    }
                    if (strIMEI == string.Empty)
                    {
                        if (!Convert.IsDBNull(rUnEvent["InventoryIMEI"]))
                        {
                            strIMEI = Convert.ToString(rUnEvent["InventoryIMEI"]).Trim();
                        }
                    }
                    if (strIMEI != string.Empty)
                    {
                        DataRow[] drStockIMEI = dtbDataTable.Select("StockIMEI IS NOT NULL AND TRIM(StockIMEI) <> '' AND TRIM(StockIMEI) = '" + strIMEI + "'");
                        if (bolStockIMEI)
                        {
                            if (drStockIMEI.Length > 0)
                            {
                                continue;
                            }
                        }
                        else
                        {
                            DataRow[] drInventoryIMEI = dtbDataTable.Select("InventoryIMEI IS NOT NULL AND TRIM(InventoryIMEI) <> '' AND TRIM(InventoryIMEI) = '" + strIMEI + "'");
                            if (drInventoryIMEI.Length > 0)
                            {
                                continue;
                            }
                        }
                    }
                    else
                    {
                        DataRow[] drProductID = dtbDataTable.Select("TRIM(ProductID) = '" + strProductID + "'");
                        if (drProductID.Length > 0)
                        {
                            continue;
                        }
                    }
                    DataRow rData = dtbDataTable.NewRow();
                    rData["IsSelect"] = false;
                    rData["IsLock"] = false;
                    if (!Convert.IsDBNull(rUnEvent["CabinetNumber"])) rData["CabinetNumber"] = Convert.ToInt32(rUnEvent["CabinetNumber"]);
                    if (!Convert.IsDBNull(rUnEvent["ProductID"])) rData["ProductID"] = Convert.ToString(rUnEvent["ProductID"]);
                    if (!Convert.IsDBNull(rUnEvent["ProductName"])) rData["ProductName"] = Convert.ToString(rUnEvent["ProductName"]);
                    if (!Convert.IsDBNull(rUnEvent["StockIMEI"])) rData["StockIMEI"] = Convert.ToString(rUnEvent["StockIMEI"]);
                    if (!Convert.IsDBNull(rUnEvent["StockQuantity"])) rData["StockQuantity"] = Convert.ToInt32(rUnEvent["StockQuantity"]);
                    if (!Convert.IsDBNull(rUnEvent["InventoryIMEI"])) rData["InventoryIMEI"] = Convert.ToString(rUnEvent["InventoryIMEI"]);
                    if (!Convert.IsDBNull(rUnEvent["InventoryQuantity"])) rData["InventoryQuantity"] = Convert.ToInt32(rUnEvent["InventoryQuantity"]);
                    rData["ERP_PRODUCTSTATUSID"] = -1;
                    rData["INV_PRODUCTSTATUSID"] = -1;
                    if (!Convert.IsDBNull(rUnEvent["ERP_PRODUCTSTATUSID"])) rData["ERP_PRODUCTSTATUSID"] = Convert.ToInt32(rUnEvent["ERP_PRODUCTSTATUSID"]);
                    if (!Convert.IsDBNull(rUnEvent["INV_PRODUCTSTATUSID"])) rData["INV_PRODUCTSTATUSID"] = Convert.ToInt32(rUnEvent["INV_PRODUCTSTATUSID"]);
                    if (!Convert.IsDBNull(rUnEvent["ERP_PRODUCTSTATUSNAME"])) rData["ERP_PRODUCTSTATUSNAME"] = (rUnEvent["ERP_PRODUCTSTATUSNAME"] ?? "").ToString().Trim();
                    if (!Convert.IsDBNull(rUnEvent["INV_PRODUCTSTATUSNAME"])) rData["INV_PRODUCTSTATUSNAME"] = (rUnEvent["INV_PRODUCTSTATUSNAME"] ?? "").ToString().Trim();

                    if (!Convert.IsDBNull(rUnEvent["ISCHANGESTATUS"])) rData["ISCHANGESTATUS"] = Convert.ToInt32(rUnEvent["ISCHANGESTATUS"]);
                    if (!Convert.IsDBNull(rUnEvent["CHG_PRODUCTSTATUSID"])) rData["CHG_PRODUCTSTATUSID"] = Convert.ToInt32(rUnEvent["CHG_PRODUCTSTATUSID"]);


                    if (!Convert.IsDBNull(rUnEvent["QUANTITY_SALE"])) rData["QUANTITY_SALE"] = Convert.ToDecimal(rUnEvent["QUANTITY_SALE"]);
                    if (!Convert.IsDBNull(rUnEvent["QUANTITY_INPUT"])) rData["QUANTITY_INPUT"] = Convert.ToDecimal(rUnEvent["QUANTITY_INPUT"]);
                    if (!Convert.IsDBNull(rUnEvent["QUANTITY_STORECHANGE"])) rData["QUANTITY_STORECHANGE"] = Convert.ToDecimal(rUnEvent["QUANTITY_STORECHANGE"]);
                    if (!Convert.IsDBNull(rUnEvent["QUANTITY_OUTPUT"])) rData["QUANTITY_OUTPUT"] = Convert.ToDecimal(rUnEvent["QUANTITY_OUTPUT"]);
                    if (!Convert.IsDBNull(rUnEvent["QUANTITY_DIFFERENT"])) rData["QUANTITY_DIFFERENT"] = Convert.ToDecimal(rUnEvent["QUANTITY_DIFFERENT"]);

                    int intDeltaInventoryQuantity = Convert.ToInt32(rUnEvent["DeltaInventoryQuantity"]);
                    rData["DeltaInventoryQuantity"] = intDeltaInventoryQuantity;
                    int intInputtingQuantity = 0;
                    //if (dtbUnEventInputtingQuantity != null && dtbUnEventInputtingQuantity.Rows.Count > 0)
                    //{
                    //    DataRow[] drUnEventInputtingQuantity;
                    //    if (strIMEI != string.Empty)
                    //    {
                    //        drUnEventInputtingQuantity = dtbUnEventInputtingQuantity.Select("TRIM(ProductID) = '" + strProductID + "' AND (IMEI IS NOT NULL AND TRIM(IMEI) <> '' AND TRIM(IMEI) = '" + strIMEI + "')");
                    //        if (drUnEventInputtingQuantity.Length > 0)
                    //        {
                    //            if (!Convert.IsDBNull(drUnEventInputtingQuantity[0]["Quantity"]))
                    //            {
                    //                intInputtingQuantity = Convert.ToInt32(drUnEventInputtingQuantity[0]["Quantity"]);
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        drUnEventInputtingQuantity = dtbUnEventInputtingQuantity.Select("TRIM(ProductID) = '" + strProductID + "'");
                    //        if (drUnEventInputtingQuantity.Length > 0)
                    //        {
                    //            if (!Convert.IsDBNull(drUnEventInputtingQuantity[0]["Quantity"]))
                    //            {
                    //                intInputtingQuantity = Convert.ToInt32(drUnEventInputtingQuantity[0]["Quantity"]);
                    //            }
                    //        }
                    //    }
                    //}
                    int intQUANTITY_DIFFERENT = Convert.ToInt32(rUnEvent["QUANTITY_DIFFERENT"]);
                    rData["InputtingQuantity"] = intInputtingQuantity;
                    int intDeltaInputtingQuantity = intDeltaInventoryQuantity - intInputtingQuantity - intQUANTITY_DIFFERENT;
                    rData["DeltaInputtingQuantity"] = intDeltaInputtingQuantity;
                    rData["AdjustUnEventQuantity"] = 0;
                    rData["DeltaAdjustUnEventQuantity"] = intDeltaInputtingQuantity;
                    if (!Convert.IsDBNull(rUnEvent["CostPrice"])) rData["CostPrice"] = Convert.ToDecimal(rUnEvent["CostPrice"]);
                    if (!Convert.IsDBNull(rUnEvent["SalePrice"])) rData["SalePrice"] = Convert.ToDecimal(rUnEvent["SalePrice"]);
                    if (!Convert.IsDBNull(rUnEvent["UnEventExplain"])) rData["UnEventExplain"] = Convert.ToString(rUnEvent["UnEventExplain"]);
                    rData["ResolveQuantity"] = 0;
                    rData["ResolveQuantity2"] = 0;
                    rData["DeltaRemainQuantity"] = intDeltaInputtingQuantity;
                    if (!Convert.IsDBNull(rUnEvent["ResolveNote"])) rData["ResolveNote"] = Convert.ToString(rUnEvent["ResolveNote"]);
                    if (!Convert.IsDBNull(rUnEvent["Note"])) rData["Note"] = Convert.ToString(rUnEvent["Note"]);
                    if (!Convert.IsDBNull(rUnEvent["IsRequestIMEI"])) rData["IsRequestIMEI"] = Convert.ToBoolean(rUnEvent["IsRequestIMEI"]);
                    if (!Convert.IsDBNull(rUnEvent["UnEventID"])) rData["UnEventID"] = Convert.ToString(rUnEvent["UnEventID"]);
                    if (!Convert.IsDBNull(rUnEvent["ChangeUnEventID"])) rData["ChangeUnEventID"] = Convert.ToString(rUnEvent["ChangeUnEventID"]);


                    if (!Convert.IsDBNull(rUnEvent["ResolveType"])) rData["ResolveType"] = Convert.ToInt32(rUnEvent["ResolveType"]);
                    else rData["ResolveType"] = 0;

                    // Chenh lech sau khi giai trinh
                    decimal decQUANTITY_DIFFERENT = 0;
                    if (!Convert.IsDBNull(rData["QUANTITY_DIFFERENT"]))
                        decQUANTITY_DIFFERENT = Convert.ToDecimal(rData["QUANTITY_DIFFERENT"]);

                    // So luong hang di duong 
                    decimal decInputing = 0;
                    if (!Convert.IsDBNull(rData["INPUTTINGQUANTITY"]))
                        decInputing = Convert.ToDecimal(rData["INPUTTINGQUANTITY"]);

                    //check lech hang di duong 
                    decimal decDELTAINPUTTINGQUANTITY = decQUANTITY_DIFFERENT - decInputing;
                    rData["DELTAINPUTTINGQUANTITY"] = decDELTAINPUTTINGQUANTITY;

                    // lay du lieu đã xử lý
                    decimal decADJUSTUNEVENTQUANTITY = 0;
                    if (!Convert.IsDBNull(grdViewProducts.GetFocusedRowCellValue("ADJUSTUNEVENTQUANTITY")))
                        decADJUSTUNEVENTQUANTITY = Convert.ToDecimal(rData["ADJUSTUNEVENTQUANTITY"]);

                    //
                    decimal decDELTAADJUSTUNEVENTQUANTITY = decDELTAINPUTTINGQUANTITY - decADJUSTUNEVENTQUANTITY;
                    rData["DELTAADJUSTUNEVENTQUANTITY"] = decDELTAADJUSTUNEVENTQUANTITY;
                    rData["RESOLVEQUANTITY"] = decDELTAADJUSTUNEVENTQUANTITY;
                    rData["DELTAADJUSTUNEVENTQUANTITY"] = decDELTAADJUSTUNEVENTQUANTITY;
                    rData["DELTAREMAINQUANTITY"] = decDELTAADJUSTUNEVENTQUANTITY;
                    if (decDELTAADJUSTUNEVENTQUANTITY == 0
                        || (rData["ISCHANGESTATUS"] ?? "").ToString().Trim() == "1")
                    {
                        rData["ISLOCK"] = true;
                    }
                    dtbDataTable.Rows.Add(rData);
                }
            }
            //foreach (DataRow dr in tblUnEventTable.Rows)
            //{
            //    string strPRODUCTID = (dr["PRODUCTID"] ?? "").ToString().Trim();
            //    string strISREQUESTIMEI = (dr["ISREQUESTIMEI"] ?? "").ToString().Trim();
            //    string strERP_IMEI = (dr["ERP_IMEI"] ?? "").ToString().Trim();
            //    string strINV_IMEI = (dr["INV_IMEI"] ?? "").ToString().Trim();
            //    string strISCHANGESTATUS = (dr["ISCHANGESTATUS"] ?? "").ToString().Trim();

            //    string strERP_PRODUCTSTATUSID = (dr["ERP_PRODUCTSTATUSID"] ?? "").ToString().Trim();
            //    string strINV_PRODUCTSTATUSID = (dr["INV_PRODUCTSTATUSID"] ?? "").ToString().Trim();

            //    decimal decERP_Quantity = Convert.ToDecimal(dr["ERP_QUANTITY"]);
            //    decimal decINV_Quantity = Convert.ToDecimal(dr["INV_QUANTITY"]);

            //    decimal decQUANTITY_SALE = Convert.ToDecimal(dr["QUANTITY_SALE"]);
            //    decimal decQUANTITY_INPUT = Convert.ToDecimal(dr["QUANTITY_INPUT"]);
            //    decimal decQUANTITY_STORECHANGE = Convert.ToDecimal(dr["QUANTITY_STORECHANGE"]);
            //    decimal decQUANTITY_OUTPUT = Convert.ToDecimal(dr["QUANTITY_OUTPUT"]);


            //    decimal decQUANTITY_INPUTTING = Convert.ToDecimal(dr["QUANTITY_INPUTTING"]);
            //    decimal decCHG_INQUANTITY = Convert.ToDecimal(dr["CHG_INQUANTITY"]);
            //    decimal decCHG_OUTQUANTITY = Convert.ToDecimal(dr["CHG_OUTQUANTITY"]);
            //    decimal decCHG_DIFQUANTITY = Convert.ToDecimal(dr["CHG_DIFQUANTITY"]);

            //    decimal decQUANTITY_DIFFERENT = Convert.ToDecimal(dr["QUANTITY_DIFFERENT"]);



            //    decQUANTITY_DIFFERENT = (decERP_Quantity - decINV_Quantity) + decQUANTITY_INPUT - (decQUANTITY_SALE + decQUANTITY_STORECHANGE + decQUANTITY_OUTPUT);
            //    dr["QUANTITY_DIFFERENT"] = decQUANTITY_DIFFERENT;
            //    if (decQUANTITY_DIFFERENT > 0)
            //        decCHG_OUTQUANTITY = decQUANTITY_DIFFERENT;
            //    if (decQUANTITY_DIFFERENT < 0)
            //        decCHG_INQUANTITY = -1 * decQUANTITY_DIFFERENT;
            //    decCHG_DIFQUANTITY = decQUANTITY_DIFFERENT - decCHG_INQUANTITY + decCHG_OUTQUANTITY;
            //    dr["CHG_DIFQUANTITY"] = decCHG_DIFQUANTITY;
            //    dr["CHG_INQUANTITY"] = decCHG_INQUANTITY;
            //    dr["CHG_OUTQUANTITY"] = decCHG_OUTQUANTITY;
            //    if (decCHG_DIFQUANTITY == 0 && (decCHG_OUTQUANTITY != 0 || decCHG_INQUANTITY != 0))
            //        dr["ISSELECT"] = 1;
            //    if (strISCHANGESTATUS == "1")
            //        dr["ISSELECT"] = 1;
            //}
            tblDataFull = dtbDataTable.Copy();
            chkOnlyUnEvent_CheckedChanged(null, null);
            grdCtlProducts.DataSource = dtbDataTable;

            //CustomFlex();

            if (bolIsDeleted)
            {
                btnSave.Enabled = false;
                mnuHandlingUnEvent.Enabled = false;
                //for (int i = 0; i < flexUnEvent.Cols.Count; i++)
                //{
                //    flexUnEvent.Cols[i].AllowEditing = false;
                //}
            }
            else
            {
                btnSave.Enabled = true;
            }

            //dtbStoreInStock = objPLCReportDataSource.GetDataSource("PM_PrdChange_GetStoreInStock", new object[] { "@StoreID", 0 });
        }

        private DataTable InitFlex()
        {
            DataTable dtbInit = new DataTable();
            dtbInit.Columns.Add("ISSELECT", typeof(bool));
            dtbInit.Columns.Add("CABINETNUMBER", typeof(int));
            dtbInit.Columns.Add("PRODUCTID", typeof(string));
            dtbInit.Columns.Add("PRODUCTNAME", typeof(string));
            dtbInit.Columns.Add("STOCKIMEI", typeof(string));
            dtbInit.Columns.Add("STOCKQUANTITY", typeof(int));
            dtbInit.Columns.Add("INVENTORYIMEI", typeof(string));
            dtbInit.Columns.Add("INVENTORYQUANTITY", typeof(int));
            dtbInit.Columns.Add("DELTAINVENTORYQUANTITY", typeof(int));
            dtbInit.Columns.Add("INPUTTINGQUANTITY", typeof(int));
            dtbInit.Columns.Add("DELTAINPUTTINGQUANTITY", typeof(int));
            dtbInit.Columns.Add("ADJUSTUNEVENTQUANTITY", typeof(int));
            dtbInit.Columns.Add("DELTAADJUSTUNEVENTQUANTITY", typeof(int));
            dtbInit.Columns.Add("SALEPRICE", typeof(decimal));
            dtbInit.Columns.Add("UNEVENTEXPLAIN", typeof(string));
            dtbInit.Columns.Add("RESOLVEQUANTITY", typeof(int));
            dtbInit.Columns.Add("DELTAREMAINQUANTITY", typeof(int));
            dtbInit.Columns.Add("RESOLVENOTE", typeof(string));
            dtbInit.Columns.Add("NOTE", typeof(string));
            dtbInit.Columns.Add("RESOLVETYPE", typeof(int));
            dtbInit.Columns.Add("ISREQUESTIMEI", typeof(bool));
            dtbInit.Columns.Add("UNEVENTID", typeof(string));
            dtbInit.Columns.Add("CHANGEUNEVENTID", typeof(string));
            dtbInit.Columns.Add("ISLOCK", typeof(bool));
            dtbInit.Columns.Add("COSTPRICE", typeof(decimal));

            dtbInit.Columns.Add("ISCHANGESTATUS", typeof(bool));
            dtbInit.Columns.Add("CHG_PRODUCTSTATUSID", typeof(Int32));
            dtbInit.Columns.Add("QUANTITY_SALE", typeof(decimal));
            dtbInit.Columns.Add("QUANTITY_INPUT", typeof(decimal));
            dtbInit.Columns.Add("QUANTITY_STORECHANGE", typeof(decimal));
            dtbInit.Columns.Add("QUANTITY_OUTPUT", typeof(decimal));
            dtbInit.Columns.Add("QUANTITY_DIFFERENT", typeof(decimal));


            dtbInit.Columns.Add("ERP_PRODUCTSTATUSID", typeof(int));
            dtbInit.Columns.Add("ERP_PRODUCTSTATUSNAME", typeof(string));

            dtbInit.Columns.Add("INV_PRODUCTSTATUSID", typeof(int));
            dtbInit.Columns.Add("INV_PRODUCTSTATUSNAME", typeof(string));
            return dtbInit;
        }

        //private void CustomFlex()
        //{
        //    flexUnEvent.Rows.Fixed = 2;
        //    flexUnEvent.Rows[0].AllowMerging = true;

        //    Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowMerging(flexUnEvent, "IsSelect,IsLock,CabinetNumber,ProductID,ProductName,StockIMEI,InventoryIMEI,DeltaInventoryQuantity,InputtingQuantity,DeltaInputtingQuantity,AdjustUnEventQuantity,DeltaAdjustUnEventQuantity,SalePrice,UnEventExplain,ResolveQuantity2,DeltaRemainQuantity,ResolveNote,Note", true);
        //    Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexUnEvent, false, "ResolveType,IsRequestIMEI,UnEventID,ChangeUnEventID,ResolveQuantity,CostPrice");

        //    flexUnEvent.Cols["CabinetNumber"].Caption = "Tủ";
        //    flexUnEvent.Cols["ProductID"].Caption = "Mã sản phẩm";
        //    flexUnEvent.Cols["ProductName"].Caption = "Tên sản phẩm";
        //    flexUnEvent.Cols["DeltaInventoryQuantity"].Caption = "Chênh lệch kiểm kê";
        //    flexUnEvent.Cols["InputtingQuantity"].Caption = "SL hàng đang đi đường";
        //    flexUnEvent.Cols["DeltaInputtingQuantity"].Caption = "Chênh lệch sau khi trừ hàng đang đi đường";
        //    flexUnEvent.Cols["AdjustUnEventQuantity"].Caption = "Điều chỉnh";
        //    flexUnEvent.Cols["DeltaAdjustUnEventQuantity"].Caption = "Chênh lệch sau khi điều chỉnh";
        //    flexUnEvent.Cols["CostPrice"].Caption = "Giá vốn";
        //    flexUnEvent.Cols["SalePrice"].Caption = "Đơn giá(chưa có VAT)";
        //    flexUnEvent.Cols["UnEventExplain"].Caption = "Giải trình chêch lệch";
        //    flexUnEvent.Cols["ResolveQuantity2"].Caption = "SL đã xử lý";
        //    flexUnEvent.Cols["DeltaRemainQuantity"].Caption = "Chênh lệch còn lại";
        //    flexUnEvent.Cols["ResolveNote"].Caption = "Nội dung xử lý";
        //    flexUnEvent.Cols["Note"].Caption = "Ghi chú";
        //    flexUnEvent.Cols["IsLock"].Caption = "Khóa";

        //    flexUnEvent.Cols["IsSelect"].DataType = typeof(bool);
        //    flexUnEvent.Cols["IsLock"].DataType = typeof(bool);
        //    flexUnEvent.Cols["StockQuantity"].Format = "###";
        //    flexUnEvent.Cols["InventoryQuantity"].Format = "###";
        //    flexUnEvent.Cols["CostPrice"].Format = "#,##0";
        //    flexUnEvent.Cols["SalePrice"].Format = "#,##0";
        //    flexUnEvent.Cols["InputtingQuantity"].Format = "##0";
        //    flexUnEvent.Cols["AdjustUnEventQuantity"].Format = "##0";

        //    flexUnEvent.Cols[0].AllowMerging = true;
        //    flexUnEvent.Cols[0].AllowResizing = false;
        //    flexUnEvent.Cols[0].Width = 10;
        //    flexUnEvent[0, 0] = " ";
        //    flexUnEvent[1, 0] = " ";

        //    flexUnEvent.Cols["IsSelect"].AllowResizing = false;
        //    flexUnEvent.Cols["IsSelect"].Width = 16;
        //    flexUnEvent[0, "IsSelect"] = " ";
        //    flexUnEvent[1, "IsSelect"] = " ";

        //    flexUnEvent.Cols["CabinetNumber"].Width = 30;
        //    flexUnEvent[0, "CabinetNumber"] = "Tủ";
        //    flexUnEvent[1, "CabinetNumber"] = "Tủ";

        //    flexUnEvent.Cols["ProductID"].Width = 80;
        //    flexUnEvent[0, "ProductID"] = "Mã sản phẩm";
        //    flexUnEvent[1, "ProductID"] = "Mã sản phẩm";

        //    flexUnEvent.Cols["ProductName"].Width = 120;
        //    flexUnEvent[0, "ProductName"] = "Tên sản phẩm";
        //    flexUnEvent[1, "ProductName"] = "Tên sản phẩm";

        //    flexUnEvent.Cols["StockIMEI"].Width = 60;
        //    flexUnEvent[0, "StockIMEI"] = "ERP";
        //    flexUnEvent[1, "StockIMEI"] = "IMEI";

        //    flexUnEvent.Cols["StockQuantity"].Width = 50;
        //    flexUnEvent[0, "StockQuantity"] = "ERP";
        //    flexUnEvent[1, "StockQuantity"] = "Số lượng";

        //    flexUnEvent.Cols["InventoryIMEI"].Width = 60;
        //    flexUnEvent[0, "InventoryIMEI"] = "Kiểm kê";
        //    flexUnEvent[1, "InventoryIMEI"] = "IMEI";

        //    flexUnEvent.Cols["InventoryQuantity"].Width = 50;
        //    flexUnEvent[0, "InventoryQuantity"] = "Kiểm kê";
        //    flexUnEvent[1, "InventoryQuantity"] = "Số lượng";

        //    flexUnEvent.Cols["DeltaInventoryQuantity"].Width = 50;
        //    flexUnEvent[0, "DeltaInventoryQuantity"] = "Chênh lệch kiểm kê";
        //    flexUnEvent[1, "DeltaInventoryQuantity"] = "Chênh lệch kiểm kê";

        //    flexUnEvent.Cols["InputtingQuantity"].Width = 60;
        //    flexUnEvent[0, "InputtingQuantity"] = "SL hàng đang đi đường";
        //    flexUnEvent[1, "InputtingQuantity"] = "SL hàng đang đi đường";

        //    flexUnEvent.Cols["DeltaInputtingQuantity"].Width = 90;
        //    flexUnEvent[0, "DeltaInputtingQuantity"] = "Chênh lệch sau khi trừ hàng đang đi đường";
        //    flexUnEvent[1, "DeltaInputtingQuantity"] = "Chênh lệch sau khi trừ hàng đang đi đường";

        //    flexUnEvent.Cols["AdjustUnEventQuantity"].Width = 40;
        //    flexUnEvent[0, "AdjustUnEventQuantity"] = "Điều chỉnh";
        //    flexUnEvent[1, "AdjustUnEventQuantity"] = "Điều chỉnh";

        //    flexUnEvent.Cols["DeltaAdjustUnEventQuantity"].Width = 60;
        //    flexUnEvent[0, "DeltaAdjustUnEventQuantity"] = "Chênh lệch sau khi điều chỉnh";
        //    flexUnEvent[1, "DeltaAdjustUnEventQuantity"] = "Chênh lệch sau khi điều chỉnh";

        //    flexUnEvent.Cols["SalePrice"].Width = 70;
        //    flexUnEvent[0, "SalePrice"] = "Đơn giá(chưa có VAT)";
        //    flexUnEvent[1, "SalePrice"] = "Đơn giá(chưa có VAT)";

        //    flexUnEvent.Cols["UnEventExplain"].Width = 90;
        //    flexUnEvent[0, "UnEventExplain"] = "Giải trình chêch lệch";
        //    flexUnEvent[1, "UnEventExplain"] = "Giải trình chêch lệch";

        //    flexUnEvent.Cols["ResolveQuantity2"].Width = 50;
        //    flexUnEvent[0, "ResolveQuantity2"] = "SL đã xử lý";
        //    flexUnEvent[1, "ResolveQuantity2"] = "SL đã xử lý";

        //    flexUnEvent.Cols["DeltaRemainQuantity"].Width = 50;
        //    flexUnEvent[0, "DeltaRemainQuantity"] = "Chênh lệch còn lại";
        //    flexUnEvent[1, "DeltaRemainQuantity"] = "Chênh lệch còn lại";

        //    flexUnEvent.Cols["ResolveNote"].Width = 70;
        //    flexUnEvent[0, "ResolveNote"] = "Nội dung xử lý";
        //    flexUnEvent[1, "ResolveNote"] = "Nội dung xử lý";

        //    flexUnEvent.Cols["Note"].Width = 90;
        //    flexUnEvent[0, "Note"] = "Ghi chú";
        //    flexUnEvent[1, "Note"] = "Ghi chú";

        //    flexUnEvent.Cols["IsLock"].Width = 40;
        //    flexUnEvent[0, "IsLock"] = "Khóa";
        //    flexUnEvent[1, "IsLock"] = "Khóa";

        //    for (int i = 1; i < flexUnEvent.Cols.Count; i++)
        //    {
        //        flexUnEvent.Cols[i].AllowEditing = false;
        //    }

        //    flexUnEvent.Cols["IsSelect"].AllowEditing = true;
        //    flexUnEvent.Cols["InputtingQuantity"].AllowEditing = true;
        //    flexUnEvent.Cols["AdjustUnEventQuantity"].AllowEditing = true;
        //    flexUnEvent.Cols["Note"].AllowEditing = true;
        //    flexUnEvent.Cols["IsLock"].AllowEditing = true;

        //    C1.Win.C1FlexGrid.CellStyle style = flexUnEvent.Styles.Add("flexStyle");
        //    style.WordWrap = true;
        //    style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
        //    C1.Win.C1FlexGrid.CellRange range = flexUnEvent.GetCellRange(0, 0, 1, flexUnEvent.Cols.Count - 1);
        //    range.Style = style;
        //    flexUnEvent.Rows[1].Height = 50;

        //    Library.AppCore.LoadControls.C1FlexGridObject.AddFormSearchProduct(flexUnEvent, flexUnEvent.Cols["ProductID"].Index, flexUnEvent.Cols["ProductName"].Index, flexUnEvent.Cols["StockIMEI"].Index);

        //    flexUnEvent.SetCellCheck(0, flexUnEvent.Cols["IsSelect"].Index, C1.Win.C1FlexGrid.CheckEnum.Unchecked);
        //    flexUnEvent.SetCellCheck(1, flexUnEvent.Cols["IsSelect"].Index, C1.Win.C1FlexGrid.CheckEnum.Unchecked);
        //}

        private void BindColor()
        {
            //try
            //{
            //    if (flexUnEvent.Rows.Count <= flexUnEvent.Rows.Fixed)
            //    {
            //        return;
            //    }
            //    DataTable dtbData = flexUnEvent.DataSource as DataTable;
            //    if (dtbData != null && dtbData.Rows.Count > 0)
            //    {
            //        for (int i = 0; i < dtbData.Rows.Count; i++)
            //        {
            //            DataRow rData = dtbData.Rows[i];
            //            int intResolveType = Convert.ToInt32(rData["ResolveType"]);
            //            if (intResolveType == 1)
            //            {
            //                Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.LightBlue);
            //            }
            //            else if (intResolveType == 2)
            //            {
            //                Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.Yellow);
            //            }
            //            else if (intResolveType == 3)
            //            {
            //                Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.Orange);
            //            }
            //        }
            //    }
            //}
            //catch
            //{
            //}
        }

        private void mnuHandlingUnEvent_Opening(object sender, CancelEventArgs e)
        {
            grdViewProducts.CloseEditor();
            grdViewProducts.UpdateCurrentRow();
            try
            {
                mnuInOutHandling.Enabled = false;
                mnuOutputChange.Enabled = false;
                //mnuChangeStatus.Enabled = false;
                if (grdViewProducts.RowCount == 0)
                    return;
                bool bolIsOutputChange = false;
                DataTable dtbData = grdCtlProducts.DataSource as DataTable;
                dtbData.AcceptChanges();
                if (dtbData != null && dtbData.Rows.Count > 0)
                {
                    dtbData.AcceptChanges();
                    DataRow[] drData = dtbData.Select("IsSelect = 1");
                    if (drData != null && drData.Length > 0)
                    {
                        mnuInOutHandling.Enabled = true;
                        if (drData.Length == 2 && ((Convert.ToInt32(drData[0]["DELTAREMAINQUANTITY"]) > 0 && Convert.ToInt32(drData[1]["DELTAREMAINQUANTITY"]) < 0) || (Convert.ToInt32(drData[0]["DELTAREMAINQUANTITY"]) < 0 && Convert.ToInt32(drData[1]["DELTAREMAINQUANTITY"]) > 0)))
                            bolIsOutputChange = true;
                    }
                }

                mnuOutputChange.Enabled = bolIsOutputChange;
            }
            catch (Exception ex)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi mở menu", ex.Message);
            }
        }

        private void mnuInOutHandling_Click(object sender, System.EventArgs e)
        {
            grdViewProducts.CloseEditor();
            grdViewProducts.UpdateCurrentRow();
            try
            {
                if (grdViewProducts.RowCount == 0)
                    return;
                DataTable dtbData = grdCtlProducts.DataSource as DataTable;
                dtbData.AcceptChanges();
                if (dtbData != null && dtbData.Rows.Count > 0)
                {
                    dtbData.AcceptChanges();
                    DataRow[] drIsHasIMEI = dtbData.Select("IsSelect = 1 AND ((StockIMEI IS NOT NULL AND TRIM(StockIMEI) <> '') OR (InventoryIMEI IS NOT NULL AND TRIM(InventoryIMEI) <> ''))");
                    DataRow[] drNotIMEI = dtbData.Select("IsSelect = 1 AND ((StockIMEI IS NULL AND InventoryIMEI IS NULL) OR (TRIM(StockIMEI) = '' AND TRIM(InventoryIMEI) = ''))");
                    if (drIsHasIMEI.Length > 0 && drNotIMEI.Length > 0)
                    {
                        MessageBox.Show(this, "Chỉ có thể xử lý chênh lệch kiểm kê cho các sản phẩm hoặc có IMEI hoặc không có IMEI", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    if (drNotIMEI.Length > 0)
                    {
                        frmInputResolveQuantity frmInputResolveQuantity1 = new frmInputResolveQuantity();
                        if (frmInputResolveQuantity1.dtbProduct == null)
                        {
                            frmInputResolveQuantity1.dtbProduct = new DataTable();
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("PRODUCTID", typeof(string));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("PRODUCTNAME", typeof(string));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("DELTAADJUSTUNEVENTQUANTITY", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("DELTAREMAINQUANTITY", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("RESOLVEQUANTITY", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("RESOLVEQUANTITY2", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("RESOLVENOTE", typeof(string));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("RESOLVETYPE", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("UNEVENTID", typeof(string));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("CHANGEUNEVENTID", typeof(string));

                            frmInputResolveQuantity1.dtbProduct.Columns.Add("ERP_PRODUCTSTATUSID", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("ERP_PRODUCTSTATUSNAME", typeof(string));

                            frmInputResolveQuantity1.dtbProduct.Columns.Add("INV_PRODUCTSTATUSID", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("INV_PRODUCTSTATUSNAME", typeof(string));

                            frmInputResolveQuantity1.dtbProduct.Columns.Add("CHG_PRODUCTSTATUSID", typeof(int));
                        }
                        foreach (DataRow rNotIMEI in drNotIMEI)
                        {
                            DataRow rProduct = frmInputResolveQuantity1.dtbProduct.NewRow();
                            rProduct["UNEVENTID"] = Convert.ToString(rNotIMEI["UNEVENTID"]);
                            rProduct["PRODUCTID"] = Convert.ToString(rNotIMEI["PRODUCTID"]);
                            rProduct["PRODUCTNAME"] = Convert.ToString(rNotIMEI["PRODUCTNAME"]);
                            rProduct["DELTAADJUSTUNEVENTQUANTITY"] = Convert.ToInt32(rNotIMEI["DELTAADJUSTUNEVENTQUANTITY"]);
                            rProduct["DELTAREMAINQUANTITY"] = Convert.ToInt32(rNotIMEI["DELTAREMAINQUANTITY"]);
                            rProduct["RESOLVEQUANTITY"] = 0;
                            rProduct["RESOLVEQUANTITY2"] = Convert.ToInt32(rNotIMEI["RESOLVEQUANTITY2"]);
                            rProduct["RESOLVENOTE"] = string.Empty;

                            rProduct["ERP_PRODUCTSTATUSID"] = Convert.ToInt32(rNotIMEI["ERP_PRODUCTSTATUSID"]);
                            rProduct["ERP_PRODUCTSTATUSNAME"] = Convert.ToString(rNotIMEI["ERP_PRODUCTSTATUSNAME"]);

                            rProduct["INV_PRODUCTSTATUSID"] = Convert.ToInt32(rNotIMEI["INV_PRODUCTSTATUSID"]);
                            rProduct["INV_PRODUCTSTATUSNAME"] = Convert.ToString(rNotIMEI["INV_PRODUCTSTATUSNAME"]);

                            rProduct["CHG_PRODUCTSTATUSID"] = Convert.ToInt32(rNotIMEI["CHG_PRODUCTSTATUSID"]);
                            frmInputResolveQuantity1.dtbProduct.Rows.Add(rProduct);
                        }
                        frmInputResolveQuantity1.InOutHandling = true;
                        frmInputResolveQuantity1.ShowDialog();
                        if (frmInputResolveQuantity1.AnswerYes)
                        {
                            foreach (DataRow rProduct in frmInputResolveQuantity1.dtbProduct.Rows)
                            {
                                for (int i = 0; i < dtbData.Rows.Count; i++)
                                {
                                    DataRow rData = dtbData.Rows[i];
                                    if (Convert.ToString(rData["UNEVENTID"]) == Convert.ToString(rProduct["UNEVENTID"]))
                                    {
                                        rData["RESOLVEQUANTITY"] = Convert.ToInt32(rProduct["RESOLVEQUANTITY"]);
                                        rData["RESOLVEQUANTITY2"] = Convert.ToInt32(rProduct["RESOLVEQUANTITY2"]) + Convert.ToInt32(rProduct["RESOLVEQUANTITY"]);
                                        int intDeltaRemainQuantity = Convert.ToInt32(rProduct["DELTAREMAINQUANTITY"]);
                                        rData["DELTAREMAINQUANTITY"] = intDeltaRemainQuantity;
                                        rData["CHG_PRODUCTSTATUSID"] = Convert.ToInt32(rProduct["CHG_PRODUCTSTATUSID"]);
                                        rData["ADJUSTUNEVENTQUANTITY"] = Convert.ToInt32(rProduct["RESOLVEQUANTITY"]);
                                        rData["DELTAADJUSTUNEVENTQUANTITY"] = Convert.ToInt32(rProduct["DELTAADJUSTUNEVENTQUANTITY"]) - Convert.ToInt32(rProduct["RESOLVEQUANTITY"]);

                                        string strResolveNoteProduct = string.Empty;
                                        try
                                        {
                                            strResolveNoteProduct = Convert.ToString(rProduct["RESOLVENOTE"]).Trim();
                                        }
                                        catch { }
                                        string strResolveNote = string.Empty;
                                        try
                                        {
                                            strResolveNote = Convert.ToString(rData["RESOLVENOTE"]).Trim();
                                        }
                                        catch { }
                                        if (strResolveNote != string.Empty)
                                        {
                                            string[] strSpl = strResolveNote.Split(';');
                                            //flexUnEvent.Rows[flexUnEvent.Rows.Fixed + i].Height = 20 * (strSpl.Length + 1);
                                            strResolveNote = strResolveNote + ";\r\n" + strResolveNoteProduct;
                                        }
                                        else
                                        {
                                            strResolveNote = strResolveNoteProduct;
                                        }
                                        rData["RESOLVENOTE"] = strResolveNote;

                                        int intResolveType = Convert.ToInt32(rProduct["RESOLVETYPE"]);
                                        rData["RESOLVETYPE"] = intResolveType;
                                        if (intResolveType == 1)
                                        {
                                            //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.LightBlue);
                                            rData["ISSELECT"] = false;
                                            if (intDeltaRemainQuantity == 0)
                                            {
                                                rData["ISLOCK"] = true;
                                            }
                                        }
                                        else if (intResolveType == 2)
                                        {
                                            //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.Yellow);
                                            rData["ISSELECT"] = false;
                                            if (intDeltaRemainQuantity == 0)
                                            {
                                                rData["ISLOCK"] = true;
                                            }
                                        }
                                        else if (intResolveType == 3)
                                        {
                                            //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.Orange);
                                            rData["ISSELECT"] = false;
                                            if (intDeltaRemainQuantity == 0)
                                            {
                                                rData["ISLOCK"] = true;
                                            }
                                        }
                                        else
                                        {
                                            // Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.White);
                                            rData["RESOLVETYPE"] = 0;
                                            rData["ISSELECT"] = true;
                                            rData["ISLOCK"] = false;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (drIsHasIMEI.Length > 0)
                        {
                            if (MessageBox.Show(this, "Bạn có chắc nhập thừa hoặc xuất thiếu?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                            {
                                return;
                            }
                            foreach (DataRow rIsHasIMEI in drIsHasIMEI)
                            {
                                for (int i = 0; i < dtbData.Rows.Count; i++)
                                {
                                    DataRow rData = dtbData.Rows[i];
                                    if (Convert.ToString(rIsHasIMEI["UNEVENTID"]) == Convert.ToString(rData["UNEVENTID"]))
                                    {
                                        int intDeltaRemainQuantity = Convert.ToInt32(rData["DELTAREMAINQUANTITY"]);
                                        rData["RESOLVEQUANTITY"] = intDeltaRemainQuantity;
                                        rData["RESOLVEQUANTITY2"] = intDeltaRemainQuantity;
                                        rData["DELTAREMAINQUANTITY"] = 0;
                                        rData["RESOLVETYPE"] = 0;
                                        rData["ISSELECT"] = false;
                                        rData["ISLOCK"] = true;
                                        rData["ADJUSTUNEVENTQUANTITY"] = Convert.ToInt32(rData["RESOLVEQUANTITY"]);
                                        rData["DELTAADJUSTUNEVENTQUANTITY"] = Convert.ToInt32(rData["DELTAADJUSTUNEVENTQUANTITY"]) - Convert.ToInt32(rData["RESOLVEQUANTITY"]);
                                        string strResolveNote = string.Empty;
                                        try
                                        {
                                            strResolveNote = Convert.ToString(rData["RESOLVENOTE"]);
                                        }
                                        catch { }

                                        if (strResolveNote != string.Empty)
                                        {
                                            string[] strSpl = strResolveNote.Split(';');
                                            //flexUnEvent.Rows[flexUnEvent.Rows.Fixed + i].Height = 20 * (strSpl.Length + 1);

                                            if (intDeltaRemainQuantity < 0)
                                            {
                                                strResolveNote = strResolveNote + ";\r\n" + intDeltaRemainQuantity.ToString().TrimStart('-') + ": nhập thừa";
                                                rData["RESOLVENOTE"] = strResolveNote;
                                                rData["RESOLVETYPE"] = 2;
                                                // Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.Yellow);
                                            }
                                            else
                                            {
                                                strResolveNote = strResolveNote + ";\r\n" + intDeltaRemainQuantity.ToString().TrimStart('-') + ": xuất thiếu";
                                                rData["RESOLVENOTE"] = strResolveNote;
                                                rData["RESOLVETYPE"] = 3;
                                                //  Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.Orange);
                                            }
                                        }
                                        else
                                        {
                                            if (intDeltaRemainQuantity < 0)
                                            {
                                                strResolveNote = intDeltaRemainQuantity.ToString().TrimStart('-') + ": nhập thừa";
                                                rData["RESOLVENOTE"] = strResolveNote;
                                                rData["RESOLVETYPE"] = 2;
                                                // Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.Yellow);
                                            }
                                            else
                                            {
                                                strResolveNote = intDeltaRemainQuantity.ToString().TrimStart('-') + ": xuất thiếu";
                                                rData["RESOLVENOTE"] = strResolveNote;
                                                rData["RESOLVETYPE"] = 3;
                                                //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.Orange);
                                            }
                                        }

                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi mở menu", ex.Message);
            }
        }

        private void mnuOutputChange_Click(object sender, System.EventArgs e)
        {
            grdViewProducts.CloseEditor();
            grdViewProducts.UpdateCurrentRow();
            try
            {
                if (grdViewProducts.RowCount == 0)
                    return;

                DataTable dtbData = grdCtlProducts.DataSource as DataTable;
                dtbData.AcceptChanges();
                if (dtbData != null && dtbData.Rows.Count > 0)
                {
                    dtbData.AcceptChanges();
                    DataRow[] drIsHasIMEI = dtbData.Select("IsSelect = 1 AND ((StockIMEI IS NOT NULL AND TRIM(StockIMEI) <> '') OR (InventoryIMEI IS NOT NULL AND TRIM(InventoryIMEI) <> ''))");
                    DataRow[] drNotIMEI = dtbData.Select("IsSelect = 1 AND ((StockIMEI IS NULL AND InventoryIMEI IS NULL) OR (TRIM(StockIMEI) = '' AND TRIM(InventoryIMEI) = ''))");
                    if (drIsHasIMEI.Length > 0 && drNotIMEI.Length > 0)
                    {
                        MessageBox.Show(this, "Chỉ có thể xử lý chênh lệch kiểm kê cho các sản phẩm hoặc có IMEI hoặc không có IMEI", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    DataRow[] drProductChange;

                    if (drNotIMEI.Length > 0)
                    {
                        frmInputResolveQuantity frmInputResolveQuantity1 = new frmInputResolveQuantity();
                        if (frmInputResolveQuantity1.dtbProduct == null)
                        {
                            frmInputResolveQuantity1.dtbProduct = new DataTable();
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("PRODUCTID", typeof(string));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("PRODUCTNAME", typeof(string));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("DELTAADJUSTUNEVENTQUANTITY", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("DELTAREMAINQUANTITY", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("RESOLVEQUANTITY", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("RESOLVEQUANTITY2", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("RESOLVENOTE", typeof(string));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("RESOLVETYPE", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("UNEVENTID", typeof(string));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("CHANGEUNEVENTID", typeof(string));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("ERP_PRODUCTSTATUSID", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("ERP_PRODUCTSTATUSNAME", typeof(string));

                            frmInputResolveQuantity1.dtbProduct.Columns.Add("INV_PRODUCTSTATUSID", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("INV_PRODUCTSTATUSNAME", typeof(string));

                            frmInputResolveQuantity1.dtbProduct.Columns.Add("CHG_PRODUCTSTATUSID", typeof(int));
                        }
                        foreach (DataRow rNotIMEI in drNotIMEI)
                        {
                            DataRow rProduct = frmInputResolveQuantity1.dtbProduct.NewRow();
                            rProduct["UNEVENTID"] = Convert.ToString(rNotIMEI["UNEVENTID"]);
                            rProduct["PRODUCTID"] = Convert.ToString(rNotIMEI["PRODUCTID"]);
                            rProduct["PRODUCTNAME"] = Convert.ToString(rNotIMEI["PRODUCTNAME"]);
                            rProduct["DELTAADJUSTUNEVENTQUANTITY"] = Convert.ToInt32(rNotIMEI["DELTAADJUSTUNEVENTQUANTITY"]);
                            rProduct["DELTAREMAINQUANTITY"] = Convert.ToInt32(rNotIMEI["DELTAREMAINQUANTITY"]);
                            rProduct["RESOLVEQUANTITY"] = 0;
                            rProduct["RESOLVEQUANTITY2"] = Convert.ToInt32(rNotIMEI["RESOLVEQUANTITY2"]);
                            rProduct["RESOLVENOTE"] = string.Empty;
                            try
                            {
                                rProduct["CHANGEUNEVENTID"] = Convert.ToInt32(rNotIMEI["CHANGEUNEVENTID"]);
                            }
                            catch { }
                            rProduct["ERP_PRODUCTSTATUSID"] = Convert.ToInt32(rNotIMEI["ERP_PRODUCTSTATUSID"]);
                            rProduct["ERP_PRODUCTSTATUSNAME"] = Convert.ToString(rNotIMEI["ERP_PRODUCTSTATUSNAME"]);

                            rProduct["INV_PRODUCTSTATUSID"] = Convert.ToInt32(rNotIMEI["INV_PRODUCTSTATUSID"]);
                            rProduct["INV_PRODUCTSTATUSNAME"] = Convert.ToString(rNotIMEI["INV_PRODUCTSTATUSNAME"]);

                            rProduct["CHG_PRODUCTSTATUSID"] = Convert.ToInt32(rNotIMEI["CHG_PRODUCTSTATUSID"]);
                            frmInputResolveQuantity1.dtbProduct.Rows.Add(rProduct);
                        }
                        frmInputResolveQuantity1.OutputChange = true;
                        frmInputResolveQuantity1.ShowDialog();
                        if (frmInputResolveQuantity1.AnswerYes)
                        {
                            if (dtbProductChange == null || dtbProductChange.Rows.Count < 1)
                            {
                                dtbProductChange = dtbData.Clone();
                            }

                            foreach (DataRow rProduct in frmInputResolveQuantity1.dtbProduct.Rows)
                            {
                                for (int i = 0; i < dtbData.Rows.Count; i++)
                                {
                                    DataRow rData = dtbData.Rows[i];
                                    if (Convert.ToString(rData["UnEventID"]) == Convert.ToString(rProduct["UnEventID"]))
                                    {
                                        try
                                        {
                                            rData["ChangeUnEventID"] = Convert.ToString(rProduct["ChangeUnEventID"]);
                                        }
                                        catch { }

                                        rData["ResolveQuantity"] = Convert.ToInt32(rProduct["ResolveQuantity"]);
                                        rData["ResolveQuantity2"] = Convert.ToInt32(rProduct["ResolveQuantity2"]) + Convert.ToInt32(rProduct["ResolveQuantity"]);
                                        int intDeltaRemainQuantity = Convert.ToInt32(rProduct["DeltaRemainQuantity"]);
                                        rData["DeltaRemainQuantity"] = intDeltaRemainQuantity;
                                        rData["ADJUSTUNEVENTQUANTITY"] = Convert.ToInt32(rProduct["RESOLVEQUANTITY"]);
                                        rData["DELTAADJUSTUNEVENTQUANTITY"] = Convert.ToInt32(rProduct["DELTAADJUSTUNEVENTQUANTITY"]) - Convert.ToInt32(rProduct["RESOLVEQUANTITY"]);

                                        string strResolveNoteProduct = string.Empty;
                                        try
                                        {
                                            strResolveNoteProduct = Convert.ToString(rProduct["ResolveNote"]).Trim();
                                        }
                                        catch { }
                                        string strResolveNote = string.Empty;
                                        try
                                        {
                                            strResolveNote = Convert.ToString(rData["ResolveNote"]).Trim();
                                        }
                                        catch { }
                                        if (strResolveNote != string.Empty)
                                        {
                                            string[] strSpl = strResolveNote.Split(';');
                                            // flexUnEvent.Rows[flexUnEvent.Rows.Fixed + i].Height = 20 * (strSpl.Length + 1);
                                            strResolveNote = strResolveNote + ";\r\n" + strResolveNoteProduct;
                                        }
                                        else
                                        {
                                            strResolveNote = strResolveNoteProduct;
                                        }
                                        rData["ResolveNote"] = strResolveNote;

                                        int intResolveType = Convert.ToInt32(rProduct["ResolveType"]);
                                        rData["ResolveType"] = intResolveType;
                                        if (intResolveType == 1)
                                        {
                                            // Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.LightBlue);
                                            rData["IsSelect"] = false;
                                            if (intDeltaRemainQuantity == 0)
                                            {
                                                rData["IsLock"] = true;
                                            }
                                        }
                                        else if (intResolveType == 2)
                                        {
                                            //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.Yellow);
                                            rData["IsSelect"] = false;
                                            if (intDeltaRemainQuantity == 0)
                                            {
                                                rData["IsLock"] = true;
                                            }
                                        }
                                        else if (intResolveType == 3)
                                        {
                                            //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.Orange);
                                            rData["IsSelect"] = false;
                                            if (intDeltaRemainQuantity == 0)
                                            {
                                                rData["IsLock"] = true;
                                            }
                                        }
                                        else
                                        {
                                            //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.White);
                                            rData["ResolveType"] = 0;
                                            rData["IsSelect"] = true;
                                            rData["IsLock"] = false;
                                        }

                                        drProductChange = dtbData.Select("UnEventID = '" + Convert.ToString(rData["UnEventID"]) + "'");
                                        if (drProductChange.Length > 0)
                                        {
                                            dtbProductChange.ImportRow(drProductChange[0]);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (drIsHasIMEI.Length == 2)
                        {
                            if (MessageBox.Show(this, "Bạn có chắc xuất đổi hàng?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                            {
                                return;
                            }

                            if (dtbProductChange == null || dtbProductChange.Rows.Count < 1)
                            {
                                dtbProductChange = dtbData.Clone();
                            }

                            DataRow rIsHasIMEI = drIsHasIMEI[0];
                            DataRow rIsHasIMEIOther = drIsHasIMEI[1];
                            DataRow rData = null;
                            DataRow rDataOther = null;
                            int intIndex = 0;
                            int intIndexOther = 0;
                            for (intIndex = 0; intIndex < dtbData.Rows.Count; intIndex++)
                            {
                                if (Convert.ToString(rIsHasIMEI["UnEventID"]) == Convert.ToString(dtbData.Rows[intIndex]["UnEventID"]))
                                {
                                    rData = dtbData.Rows[intIndex];
                                    break;
                                }
                            }
                            for (intIndexOther = 0; intIndexOther < dtbData.Rows.Count; intIndexOther++)
                            {
                                if (Convert.ToString(rIsHasIMEIOther["UnEventID"]) == Convert.ToString(dtbData.Rows[intIndexOther]["UnEventID"]))
                                {
                                    rDataOther = dtbData.Rows[intIndexOther];
                                    break;
                                }
                            }
                            if (rData != null && rDataOther != null)
                            {
                                try
                                {
                                    rData["ChangeUnEventID"] = Convert.ToString(rDataOther["UnEventID"]);
                                }
                                catch { }

                                try
                                {
                                    rDataOther["ChangeUnEventID"] = Convert.ToString(rData["UnEventID"]);
                                }
                                catch { }

                                #region rData
                                int intDeltaRemainQuantity = Convert.ToInt32(rData["DeltaRemainQuantity"]);
                                rData["ResolveQuantity"] = intDeltaRemainQuantity;
                                rData["ResolveQuantity2"] = intDeltaRemainQuantity;
                                rData["DeltaRemainQuantity"] = 0;
                                rData["ResolveType"] = 0;
                                rData["IsSelect"] = false;
                                rData["IsLock"] = true;
                                string strResolveNote = string.Empty;
                                rData["ADJUSTUNEVENTQUANTITY"] = Convert.ToInt32(rData["RESOLVEQUANTITY"]);
                                rData["DELTAADJUSTUNEVENTQUANTITY"] = Convert.ToInt32(rData["DELTAADJUSTUNEVENTQUANTITY"]) - Convert.ToInt32(rData["RESOLVEQUANTITY"]);

                                try
                                {
                                    strResolveNote = Convert.ToString(rData["ResolveNote"]);
                                }
                                catch { }

                                if (strResolveNote != string.Empty)
                                {
                                    string[] strSpl = strResolveNote.Split(';');
                                    //flexUnEvent.Rows[flexUnEvent.Rows.Fixed + intIndex].Height = 20 * (strSpl.Length + 1);

                                    if (intDeltaRemainQuantity < 0)
                                    {
                                        strResolveNote = strResolveNote + ";\r\n" + intDeltaRemainQuantity.ToString().TrimStart('-') + ": nhập đổi " + (Convert.ToBoolean(rDataOther["IsRequestIMEI"]) ? ("[" + Convert.ToString(rDataOther["StockIMEI"]) + "] ") : string.Empty) + Convert.ToString(rDataOther["ProductName"]);
                                        rData["ResolveNote"] = strResolveNote;
                                        rData["ResolveType"] = 1;
                                        //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndex, Color.LightBlue);
                                    }
                                    else
                                    {
                                        strResolveNote = strResolveNote + ";\r\n" + intDeltaRemainQuantity.ToString().TrimStart('-') + ": xuất đổi " + (Convert.ToBoolean(rDataOther["IsRequestIMEI"]) ? ("[" + Convert.ToString(rDataOther["InventoryIMEI"]) + "] ") : string.Empty) + Convert.ToString(rDataOther["ProductName"]);
                                        rData["ResolveNote"] = strResolveNote;
                                        rData["ResolveType"] = 1;
                                        //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndex, Color.LightBlue);
                                    }
                                }
                                else
                                {
                                    if (intDeltaRemainQuantity < 0)
                                    {
                                        strResolveNote = intDeltaRemainQuantity.ToString().TrimStart('-') + ": nhập đổi " + (Convert.ToBoolean(rDataOther["IsRequestIMEI"]) ? ("[" + Convert.ToString(rDataOther["StockIMEI"]) + "] ") : string.Empty) + Convert.ToString(rDataOther["ProductName"]);
                                        rData["ResolveNote"] = strResolveNote;
                                        rData["ResolveType"] = 1;
                                        // Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndex, Color.LightBlue);
                                    }
                                    else
                                    {
                                        strResolveNote = intDeltaRemainQuantity.ToString().TrimStart('-') + ": xuất đổi " + (Convert.ToBoolean(rDataOther["IsRequestIMEI"]) ? ("[" + Convert.ToString(rDataOther["InventoryIMEI"]) + "] ") : string.Empty) + Convert.ToString(rDataOther["ProductName"]);
                                        rData["ResolveNote"] = strResolveNote;
                                        rData["ResolveType"] = 1;
                                        //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndex, Color.LightBlue);
                                    }
                                }
                                #endregion
                                #region rDataOther
                                int intDeltaRemainQuantityOther = Convert.ToInt32(rDataOther["DeltaRemainQuantity"]);
                                rDataOther["ResolveQuantity"] = intDeltaRemainQuantityOther;
                                rDataOther["ResolveQuantity2"] = intDeltaRemainQuantityOther;
                                rDataOther["DeltaRemainQuantity"] = 0;
                                rDataOther["ResolveType"] = 0;
                                rDataOther["IsSelect"] = false;
                                rDataOther["IsLock"] = true;
                                rDataOther["ADJUSTUNEVENTQUANTITY"] = Convert.ToInt32(rDataOther["RESOLVEQUANTITY"]);
                                rDataOther["DELTAADJUSTUNEVENTQUANTITY"] = Convert.ToInt32(rDataOther["DELTAADJUSTUNEVENTQUANTITY"]) - Convert.ToInt32(rDataOther["RESOLVEQUANTITY"]);

                                string strResolveNoteOther = string.Empty;
                                try
                                {
                                    strResolveNoteOther = Convert.ToString(rDataOther["ResolveNote"]);
                                }
                                catch { }

                                if (strResolveNoteOther != string.Empty)
                                {
                                    string[] strSplOther = strResolveNoteOther.Split(';');
                                    //flexUnEvent.Rows[flexUnEvent.Rows.Fixed + intIndexOther].Height = 20 * (strSplOther.Length + 1);

                                    if (intDeltaRemainQuantityOther < 0)
                                    {
                                        strResolveNoteOther = strResolveNoteOther + ";\r\n" + intDeltaRemainQuantityOther.ToString().TrimStart('-') + ": nhập đổi " + (Convert.ToBoolean(rData["IsRequestIMEI"]) ? ("[" + Convert.ToString(rData["StockIMEI"]) + "] ") : string.Empty) + Convert.ToString(rData["ProductName"]);
                                        rDataOther["ResolveNote"] = strResolveNoteOther;
                                        rDataOther["ResolveType"] = 1;
                                        // Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndexOther, Color.LightBlue);
                                    }
                                    else
                                    {
                                        strResolveNoteOther = strResolveNoteOther + ";\r\n" + intDeltaRemainQuantityOther.ToString().TrimStart('-') + ": xuất đổi " + (Convert.ToBoolean(rData["IsRequestIMEI"]) ? ("[" + Convert.ToString(rData["InventoryIMEI"]) + "] ") : string.Empty) + Convert.ToString(rData["ProductName"]);
                                        rDataOther["ResolveNote"] = strResolveNoteOther;
                                        rDataOther["ResolveType"] = 1;
                                        //   Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndexOther, Color.LightBlue);
                                    }
                                }
                                else
                                {
                                    if (intDeltaRemainQuantityOther < 0)
                                    {
                                        strResolveNoteOther = intDeltaRemainQuantityOther.ToString().TrimStart('-') + ": nhập đổi " + (Convert.ToBoolean(rData["IsRequestIMEI"]) ? ("[" + Convert.ToString(rData["StockIMEI"]) + "] ") : string.Empty) + Convert.ToString(rData["ProductName"]);
                                        rDataOther["ResolveNote"] = strResolveNoteOther;
                                        rDataOther["ResolveType"] = 1;
                                        // Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndexOther, Color.LightBlue);
                                    }
                                    else
                                    {
                                        strResolveNoteOther = intDeltaRemainQuantityOther.ToString().TrimStart('-') + ": xuất đổi " + (Convert.ToBoolean(rData["IsRequestIMEI"]) ? ("[" + Convert.ToString(rData["InventoryIMEI"]) + "] ") : string.Empty) + Convert.ToString(rData["ProductName"]);
                                        rDataOther["ResolveNote"] = strResolveNoteOther;
                                        rDataOther["ResolveType"] = 1;
                                        //   Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndexOther, Color.LightBlue);
                                    }
                                }
                                #endregion

                                drProductChange = dtbData.Select("UnEventID = '" + Convert.ToString(rData["UnEventID"]) + "'");
                                if (drProductChange.Length > 0)
                                {
                                    dtbProductChange.ImportRow(drProductChange[0]);
                                }

                                drProductChange = dtbData.Select("UnEventID = '" + Convert.ToString(rDataOther["UnEventID"]) + "'");
                                if (drProductChange.Length > 0)
                                {
                                    dtbProductChange.ImportRow(drProductChange[0]);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi mở menu", ex.Message);
            }
        }

        private void flexUnEvent_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            //try
            //{
            //    if (flexUnEvent.Rows.Count <= flexUnEvent.Rows.Fixed)
            //    {
            //        return;
            //    }
            //    if (e.Row < flexUnEvent.Rows.Fixed)
            //    {
            //        if (e.Col == flexUnEvent.Cols["IsSelect"].Index)
            //        {
            //            if (Convert.ToString(flexUnEvent[e.Row, "IsSelect"]) == "  ")
            //            {
            //                for (int i = flexUnEvent.Rows.Fixed; i < flexUnEvent.Rows.Count; i++)
            //                {
            //                    flexUnEvent[i, "IsSelect"] = false;
            //                }
            //                flexUnEvent[0, "IsSelect"] = " ";
            //                flexUnEvent[1, "IsSelect"] = " ";
            //                flexUnEvent.SetCellCheck(0, flexUnEvent.Cols["IsSelect"].Index, C1.Win.C1FlexGrid.CheckEnum.Unchecked);
            //                flexUnEvent.SetCellCheck(1, flexUnEvent.Cols["IsSelect"].Index, C1.Win.C1FlexGrid.CheckEnum.Unchecked);
            //            }
            //            else
            //            {
            //                for (int i = flexUnEvent.Rows.Fixed; i < flexUnEvent.Rows.Count; i++)
            //                {
            //                    flexUnEvent[i, "IsSelect"] = true;
            //                }
            //                flexUnEvent[0, "IsSelect"] = "  ";
            //                flexUnEvent[1, "IsSelect"] = "  ";
            //                flexUnEvent.SetCellCheck(0, flexUnEvent.Cols["IsSelect"].Index, C1.Win.C1FlexGrid.CheckEnum.Checked);
            //                flexUnEvent.SetCellCheck(1, flexUnEvent.Cols["IsSelect"].Index, C1.Win.C1FlexGrid.CheckEnum.Checked);
            //            }
            //        }
            //    }
            //    if (e.Col == 10)
            //    {
            //        flexUnEvent[e.Row, "InputtingQuantity"] = Math.Abs(Convert.ToInt32(flexUnEvent[e.Row, "InputtingQuantity"]));
            //        if (Convert.ToString(flexUnEvent[e.Row, "StockIMEI"]).Trim() != string.Empty || Convert.ToString(flexUnEvent[e.Row, "InventoryIMEI"]).Trim() != string.Empty)
            //        {
            //            if (Convert.ToInt32(flexUnEvent[e.Row, "InputtingQuantity"]) > 1)
            //            {
            //                flexUnEvent[e.Row, "InputtingQuantity"] = 1;
            //            }
            //        }
            //        else
            //        {
            //            if (Convert.ToInt32(flexUnEvent[e.Row, "InputtingQuantity"]) > Math.Abs(Convert.ToInt32(flexUnEvent[e.Row, "DeltaInventoryQuantity"])))
            //            {
            //                flexUnEvent[e.Row, "InputtingQuantity"] = Math.Abs(Convert.ToInt32(flexUnEvent[e.Row, "DeltaInventoryQuantity"]));
            //            }
            //        }
            //    }
            //    if (e.Col == 12)
            //    {
            //        if (Convert.ToString(flexUnEvent[e.Row, "StockIMEI"]).Trim() != string.Empty || Convert.ToString(flexUnEvent[e.Row, "InventoryIMEI"]).Trim() != string.Empty)
            //        {
            //            if (Convert.ToInt32(flexUnEvent[e.Row, "DeltaInputtingQuantity"]) >= 1)
            //            {
            //                if (Math.Abs(Convert.ToInt32(flexUnEvent[e.Row, "AdjustUnEventQuantity"])) > 1)
            //                {
            //                    flexUnEvent[e.Row, "AdjustUnEventQuantity"] = 0;
            //                }
            //            }
            //            else if (Convert.ToInt32(flexUnEvent[e.Row, "DeltaInputtingQuantity"]) <= -1)
            //            {
            //                if (Math.Abs(Convert.ToInt32(flexUnEvent[e.Row, "AdjustUnEventQuantity"])) > 1)
            //                {
            //                    flexUnEvent[e.Row, "AdjustUnEventQuantity"] = 0;
            //                }
            //            }
            //        }
            //        else
            //        {
            //            if (Convert.ToInt32(flexUnEvent[e.Row, "DeltaInputtingQuantity"]) > 0)
            //            {
            //                if (Convert.ToInt32(flexUnEvent[e.Row, "AdjustUnEventQuantity"]) > 0)
            //                {
            //                    flexUnEvent[e.Row, "AdjustUnEventQuantity"] = Convert.ToInt32(flexUnEvent[e.Row, "AdjustUnEventQuantity"]) * Convert.ToInt32(-1);
            //                }
            //            }
            //            else
            //            {
            //                if (Convert.ToInt32(flexUnEvent[e.Row, "DeltaInputtingQuantity"]) < 0)
            //                {
            //                    if (Convert.ToInt32(flexUnEvent[e.Row, "AdjustUnEventQuantity"]) < 0)
            //                    {
            //                        flexUnEvent[e.Row, "AdjustUnEventQuantity"] = Convert.ToInt32(flexUnEvent[e.Row, "AdjustUnEventQuantity"]) * Convert.ToInt32(-1);
            //                    }
            //                }
            //                else
            //                {
            //                    if (Math.Abs(Convert.ToInt32(flexUnEvent[e.Row, "AdjustUnEventQuantity"])) > Math.Abs(Convert.ToInt32(flexUnEvent[e.Row, "DeltaInputtingQuantity"])))
            //                    {
            //                        flexUnEvent[e.Row, "AdjustUnEventQuantity"] = Convert.ToInt32(flexUnEvent[e.Row, "DeltaInputtingQuantity"]) * Convert.ToInt32(-1);
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    int intDeltaRemainQuantity = 0;
            //    switch (e.Col)
            //    {
            //        case 10:
            //        case 12:
            //            int intInputtingQuantity = 0;
            //            int intDeltaInventoryQuantity = 0;
            //            int intAdjustUnEventQuantity = 0;
            //            int intResolveQuantity = 0;
            //            try
            //            {
            //                intInputtingQuantity = Convert.ToInt32(flexUnEvent[e.Row, "InputtingQuantity"]);
            //            }
            //            catch { }
            //            if (e.Col == 10 && intInputtingQuantity < 0)
            //            {
            //                intInputtingQuantity = 0;
            //                flexUnEvent[e.Row, "InputtingQuantity"] = 0;
            //            }
            //            try
            //            {
            //                intDeltaInventoryQuantity = Convert.ToInt32(flexUnEvent[e.Row, "DeltaInventoryQuantity"]);
            //            }
            //            catch { }
            //            try
            //            {
            //                intAdjustUnEventQuantity = Convert.ToInt32(flexUnEvent[e.Row, "AdjustUnEventQuantity"]);
            //            }
            //            catch { }
            //            try
            //            {
            //                intResolveQuantity = Convert.ToInt32(flexUnEvent[e.Row, "ResolveQuantity2"]);
            //            }
            //            catch { }

            //            flexUnEvent[e.Row, "DeltaInputtingQuantity"] = intDeltaInventoryQuantity - intInputtingQuantity;
            //            flexUnEvent[e.Row, "DeltaAdjustUnEventQuantity"] = (intDeltaInventoryQuantity - intInputtingQuantity) + intAdjustUnEventQuantity;
            //            intDeltaRemainQuantity = (intDeltaInventoryQuantity - intInputtingQuantity) + intAdjustUnEventQuantity - intResolveQuantity;
            //            flexUnEvent[e.Row, "DeltaRemainQuantity"] = intDeltaRemainQuantity;
            //            if (intDeltaRemainQuantity == 0)
            //            {
            //                flexUnEvent[e.Row, "IsLock"] = true;
            //                flexUnEvent[e.Row, "IsSelect"] = false;
            //            }
            //            else
            //            {
            //                flexUnEvent[e.Row, "IsLock"] = false;
            //            }
            //            break;
            //        case 1:
            //            intDeltaRemainQuantity = Convert.ToInt32(flexUnEvent[e.Row, "DeltaRemainQuantity"]);
            //            if (intDeltaRemainQuantity == 0)
            //            {
            //                flexUnEvent[e.Row, "IsSelect"] = false;
            //            }
            //            else
            //            {
            //                flexUnEvent[e.Row, "IsLock"] = false;
            //            }
            //            break;
            //        case 25:
            //            intDeltaRemainQuantity = Convert.ToInt32(flexUnEvent[e.Row, "DeltaRemainQuantity"]);
            //            bool bolIsSelect = false;
            //            try
            //            {
            //                bolIsSelect = Convert.ToBoolean(flexUnEvent[e.Row, "IsSelect"]);
            //            }
            //            catch { }
            //            if (intDeltaRemainQuantity == 0 && Convert.ToInt32(flexUnEvent[e.Row, "ResolveType"]) > 0)
            //            {
            //                flexUnEvent[e.Row, "IsLock"] = true;
            //            }
            //            else if (intDeltaRemainQuantity != 0 && bolIsSelect)
            //            {
            //                flexUnEvent[e.Row, "IsLock"] = false;
            //            }
            //            break;
            //        default:
            //            break;
            //    }
            //}
            //catch
            //{
            //}
        }

        private void flexUnEvent_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            //try
            //{
            //    if (flexUnEvent.Rows.Count <= flexUnEvent.Rows.Fixed)
            //    {
            //        return;
            //    }
            //    int intDeltaRemainQuantity = 0;
            //    if (e.Col == 10 || e.Col == 12)
            //    {
            //        if (e.Col == 10)
            //        {
            //            if (Convert.ToInt32(flexUnEvent[e.Row, "DeltaInventoryQuantity"]) < 0)
            //            {
            //                e.Cancel = true;
            //            }
            //        }
            //        intDeltaRemainQuantity = Convert.ToInt32(flexUnEvent[e.Row, "DeltaRemainQuantity"]);
            //        bool bolIsLock = false;
            //        try
            //        {
            //            bolIsLock = Convert.ToBoolean(flexUnEvent[e.Row, "IsLock"]);
            //        }
            //        catch { }
            //        if (Convert.ToInt32(flexUnEvent[e.Row, "ResolveType"]) > 0)
            //        {
            //            e.Cancel = true;
            //        }
            //        else
            //        {
            //            if (bolIsLock)
            //            {
            //                e.Cancel = true;
            //            }
            //        }
            //    }
            //}
            //catch
            //{
            //}
        }

        private bool CheckInput()
        {
            DataTable dtbData = grdCtlProducts.DataSource as DataTable;
            DataRow[] drStChange = dtbData.Select("ISLOCK = 1");
            if (drStChange.Length > 0)
            {
                DialogResult drResult = MessageBox.Show(this, "Tồn tại các sản phẩm không xử lý chênh lệch. Bạn có muốn tiếp tục?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (drResult != System.Windows.Forms.DialogResult.OK)
                    return false;
            }
            for (int i = 0; i < grdViewProducts.RowCount; i++)
            {
                DataRow dr = grdViewProducts.GetDataRow(i);
                if (dr == null)
                    continue;
                bool bolIsLock = Convert.ToBoolean(dr["ISLOCK"]);
                bool bolSelect = Convert.ToBoolean(dr["ISSELECT"]);
                decimal decChenhLech = Convert.ToDecimal(dr["DELTAADJUSTUNEVENTQUANTITY"]);
                decimal decSalePrice = Convert.ToDecimal(dr["SALEPRICE"]);

                //if (!bolIsLock && !bolSelect)
                //{
                //    MessageBox.Show(this, "Có ít nhất 1 sản phẩm có chênh lệch còn lại bằng 0 nhưng chưa khóa lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    // flexUnEvent.Select(i, flexUnEvent.Cols["DeltaRemainQuantity"].Index);
                //    return false;
                //}
                if (bolIsLock && decChenhLech > 0)
                {
                    MessageBox.Show(this, "Có ít nhất 1 sản phẩm có chênh lệch còn lại khác 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //flexUnEvent.Select(i, flexUnEvent.Cols["DeltaRemainQuantity"].Index);
                    return false;
                }
                //if (decSalePrice == 0)
                //{
                //    MessageBox.Show(this, "Sản phẩm " + (dr["PRODUCTNAME"] ?? "").ToString().Trim() + " có đơn giá(chưa có VAT) bằng 0?", "Thông báo");
                //    return false;
                //}
            }
            return true;
        }


        #region Nap du lieu
        ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_StChange AddStatusChange(
            string strProductID, string strIMEI, decimal decQuantity, decimal decSalePrice, int intOut_ProductStatusID, int intInt_ProductStatusID, string strNote)
        {
            ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_StChange obj = new PLC.Inventory.WSInventoryProcess.InventoryProcess_StChange();
            obj.ProductID_In = strProductID;
            obj.ProductID_Out = strProductID;

            obj.IMEI_In = strIMEI;
            obj.IMEI_Out = strIMEI;

            obj.In_ProductStatusID = intInt_ProductStatusID;
            obj.Out_ProductStatusID = intOut_ProductStatusID;

            obj.Quantity = decQuantity;
            obj.RefSalePrice_In = decSalePrice;
            obj.RefSalePrice_Out = decSalePrice;
            obj.ChangeUnEventNote = strNote;
            obj.UnEventAmount = Math.Round((obj.RefSalePrice_In - obj.RefSalePrice_Out) * obj.Quantity);
            if (obj.UnEventAmount < 0)
                obj.CollectArrearPrice = 0;
            else
                obj.CollectArrearPrice = obj.UnEventAmount;
            return obj;
        }

        ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input AddInput(
           string strProductID, string strIMEI, decimal decQuantity, decimal decSalePrice, int intProductStatusID, string strNote)
        {
            ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input obj = new PLC.Inventory.WSInventoryProcess.InventoryProcess_Input();
            obj.ProductID = strProductID;

            obj.IMEI = strIMEI;

            obj.ProductStatusID = intProductStatusID;

            obj.Quantity = decQuantity;
            obj.InputPrice = decSalePrice;
            obj.UnEventNote = strNote;
            return obj;
        }


        ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output AddOutput(
           string strProductID, string strIMEI, decimal decQuantity, decimal decSalePrice, int intProductStatusID, string strNote)
        {
            ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output obj = new PLC.Inventory.WSInventoryProcess.InventoryProcess_Output();
            obj.ProductID = strProductID;

            obj.IMEI = strIMEI;

            obj.ProductStatusID = intProductStatusID;

            obj.Quantity = decQuantity;
            obj.OutputPrice = decSalePrice;
            obj.UnEventNote = strNote;
            return obj;
        }
        #endregion

        DataTable CreateProcessHandlerTable()
        {
            DataTable result = new DataTable("PROCESSHANDLER");
            result.Columns.AddRange(new DataColumn[] {
                new DataColumn("INVENTORYID", typeof(string)),
                new DataColumn("INVENTORYPROCESSID", typeof(string)),
                new DataColumn("UNEVENTID", typeof(string)),
            });
            return result;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            grdViewProducts.CloseEditor();
            grdViewProducts.UpdateCurrentRow();
            if (!CheckInput()) return;
            if (MessageBox.Show(this, "Bạn có chắc tạo phiếu yêu cầu xử lý kiểm kê?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                return;
            }
            btnSave.Enabled = false;
            DataTable dtbProcessHandler = CreateProcessHandlerTable();
            DataTable dtbData = grdCtlProducts.DataSource as DataTable;
            dtbData.AcceptChanges();
            try
            {
                ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess objInventoryProcess = new PLC.Inventory.WSInventoryProcess.InventoryProcess();
                objInventoryProcess.InventoryID = strInventoryID;
                objInventoryProcess.InventoryStoreID = intStoreID;
                objInventoryProcess.MainGroupID = intMainGroupID;
                objInventoryProcess.IsNew = (intIsNew == 1 ? true : false);
                objInventoryProcess.InventoryTermID = intInventoryTermID;
                objInventoryProcess.InventoryDate = dtmInventoryDate;
                objInventoryProcess.CreatedUser = Library.AppCore.SystemConfig.objSessionUser.UserName;
                objInventoryProcess.CreatedStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                objInventoryProcess.IsProcess = false;
                objInventoryProcess.CreatedDate = DateTime.MinValue;
                objInventoryProcess.ReviewedDate = DateTime.MinValue;
                objInventoryProcess.ProcessDate = DateTime.MinValue;
                objInventoryProcess.DeletedDate = DateTime.MinValue;

                if (dtbProductChange != null && dtbProductChange.Rows.Count > 0)
                {
                    DataRow[] drProductChange = dtbProductChange.Select("ResolveType = 1 AND (ChangeUnEventID IS NOT NULL AND TRIM(ChangeUnEventID) <> '')", "ResolveQuantity DESC, UnEventID ASC, ChangeUnEventID ASC");
                    if (drProductChange.Length > 0)
                    {
                        List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange> objInventoryProcessPrChangeList = new List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange>();
                        for (int i = 0; i < drProductChange.Length; i++)
                        {
                            DataRow rProductChange = drProductChange[i];
                            ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange objInventoryProcessPrChange = new ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange();
                            int intResolveQuantity = Convert.ToInt32(rProductChange["ResolveQuantity"]);
                            string strStockIMEI = string.Empty;
                            string strInventoryIMEI = string.Empty;
                            if (intResolveQuantity > 0)
                            {
                                objInventoryProcessPrChange.ProductID_Out = Convert.ToString(rProductChange["ProductID"]);
                                try
                                {
                                    strStockIMEI = Convert.ToString(rProductChange["StockIMEI"]);
                                    if (strStockIMEI == string.Empty)
                                    {
                                        strStockIMEI = Convert.ToString(rProductChange["InventoryIMEI"]);
                                    }
                                }
                                catch { }

                                objInventoryProcessPrChange.In_ProductStatusID = Convert.ToInt32(rProductChange["ERP_PRODUCTSTATUSID"]);
                                objInventoryProcessPrChange.Out_ProductStatusID = Convert.ToInt32(rProductChange["CHG_PRODUCTSTATUSID"]);
                                objInventoryProcessPrChange.IMEI_Out = strStockIMEI;
                                objInventoryProcessPrChange.RefSalePrice_Out = Convert.ToDecimal(rProductChange["SalePrice"]);
                                DataRow[] rIn = dtbData.Select("TRIM(UnEventID) = '" + Convert.ToString(rProductChange["ChangeUnEventID"]) + "'");
                                if (rIn.Length > 0)
                                {
                                    objInventoryProcessPrChange.ProductID_In = Convert.ToString(rIn[0]["ProductID"]);
                                    try
                                    {
                                        strInventoryIMEI = Convert.ToString(rIn[0]["InventoryIMEI"]);
                                        if (strInventoryIMEI == string.Empty)
                                        {
                                            strInventoryIMEI = Convert.ToString(rIn[0]["StockIMEI"]);
                                        }
                                    }
                                    catch { }
                                    objInventoryProcessPrChange.IMEI_In = strInventoryIMEI;
                                    objInventoryProcessPrChange.RefSalePrice_In = Convert.ToDecimal(rIn[0]["SalePrice"]);

                                    objInventoryProcessPrChange.ChangeUnEventID = Convert.ToString(rProductChange["ChangeUnEventID"]);
                                    if (!Convert.IsDBNull(rIn[0]["Note"])) objInventoryProcessPrChange.ChangeUnEventNote = Convert.ToString(rIn[0]["Note"]);
                                }
                                objInventoryProcessPrChange.Quantity = Math.Abs(intResolveQuantity);
                                objInventoryProcessPrChange.ProductChangeDetailID = string.Empty;
                                objInventoryProcessPrChange.UnEventAmount = Math.Round((objInventoryProcessPrChange.RefSalePrice_Out - objInventoryProcessPrChange.RefSalePrice_In) * objInventoryProcessPrChange.Quantity);
                                if (objInventoryProcessPrChange.UnEventAmount < 0)
                                {
                                    objInventoryProcessPrChange.CollectArrearPrice = 0;
                                }
                                else
                                {
                                    objInventoryProcessPrChange.CollectArrearPrice = objInventoryProcessPrChange.UnEventAmount;
                                }
                                if (!Convert.IsDBNull(rProductChange["UnEventID"])) objInventoryProcessPrChange.UnEventID = Convert.ToString(rProductChange["UnEventID"]);
                                if (!Convert.IsDBNull(rProductChange["Note"])) objInventoryProcessPrChange.UnEventNote = Convert.ToString(rProductChange["Note"]);
                            }
                            else
                            {
                                objInventoryProcessPrChange.ProductID_In = Convert.ToString(rProductChange["ProductID"]);
                                try
                                {
                                    strInventoryIMEI = Convert.ToString(rProductChange["InventoryIMEI"]);
                                    if (strInventoryIMEI == string.Empty)
                                    {
                                        strInventoryIMEI = Convert.ToString(rProductChange["StockIMEI"]);
                                    }
                                }
                                catch { }
                                objInventoryProcessPrChange.IMEI_In = strInventoryIMEI;
                                objInventoryProcessPrChange.RefSalePrice_In = Convert.ToDecimal(rProductChange["SalePrice"]);
                                DataRow[] rOut = dtbData.Select("TRIM(UnEventID) = '" + Convert.ToString(rProductChange["ChangeUnEventID"]) + "'");
                                if (rOut.Length > 0)
                                {
                                    objInventoryProcessPrChange.ProductID_Out = Convert.ToString(rOut[0]["ProductID"]);
                                    try
                                    {
                                        strStockIMEI = Convert.ToString(rOut[0]["StockIMEI"]);
                                        if (strStockIMEI == string.Empty)
                                        {
                                            strStockIMEI = Convert.ToString(rOut[0]["InventoryIMEI"]);
                                        }
                                    }
                                    catch { }
                                    objInventoryProcessPrChange.IMEI_Out = strStockIMEI;
                                    objInventoryProcessPrChange.RefSalePrice_Out = Convert.ToDecimal(rOut[0]["SalePrice"]);

                                    objInventoryProcessPrChange.ChangeUnEventID = Convert.ToString(rProductChange["ChangeUnEventID"]);
                                    if (!Convert.IsDBNull(rOut[0]["Note"])) objInventoryProcessPrChange.ChangeUnEventNote = Convert.ToString(rOut[0]["Note"]);
                                }
                                objInventoryProcessPrChange.Quantity = Math.Abs(intResolveQuantity);
                                objInventoryProcessPrChange.ProductChangeDetailID = string.Empty;
                                objInventoryProcessPrChange.UnEventAmount = Math.Round((objInventoryProcessPrChange.RefSalePrice_In - objInventoryProcessPrChange.RefSalePrice_Out) * objInventoryProcessPrChange.Quantity);
                                if (objInventoryProcessPrChange.UnEventAmount < 0)
                                {
                                    objInventoryProcessPrChange.CollectArrearPrice = 0;
                                }
                                else
                                {
                                    objInventoryProcessPrChange.CollectArrearPrice = objInventoryProcessPrChange.UnEventAmount;
                                }
                                if (!Convert.IsDBNull(rProductChange["UnEventID"])) objInventoryProcessPrChange.UnEventID = Convert.ToString(rProductChange["UnEventID"]);
                                if (!Convert.IsDBNull(rProductChange["Note"])) objInventoryProcessPrChange.UnEventNote = Convert.ToString(rProductChange["Note"]);
                            }
                            bool bolIsExist = false;
                            if (objInventoryProcessPrChange.IMEI_Out.Trim() != string.Empty)
                            {
                                if (objInventoryProcessPrChangeList.Count > 0)
                                {
                                    foreach (ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange itemPrChange in objInventoryProcessPrChangeList)
                                    {
                                        if (itemPrChange.ProductID_Out == objInventoryProcessPrChange.ProductID_Out
                                            && itemPrChange.IMEI_Out == objInventoryProcessPrChange.IMEI_Out
                                            && itemPrChange.ProductID_In == objInventoryProcessPrChange.ProductID_In
                                            && itemPrChange.IMEI_In == objInventoryProcessPrChange.IMEI_In)
                                        {
                                            bolIsExist = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (objInventoryProcessPrChangeList.Count > 0)
                                {
                                    foreach (ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange itemPrChange in objInventoryProcessPrChangeList)
                                    {
                                        if (itemPrChange.ProductID_Out == objInventoryProcessPrChange.ProductID_Out
                                            && itemPrChange.ProductID_In == objInventoryProcessPrChange.ProductID_In)
                                        {
                                            if (itemPrChange.UnEventID == objInventoryProcessPrChange.UnEventID)
                                            {
                                                itemPrChange.Quantity = itemPrChange.Quantity + objInventoryProcessPrChange.Quantity;
                                            }
                                            bolIsExist = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            dtbProcessHandler.Rows.Add(objInventoryProcess.InventoryID, string.Empty, objInventoryProcessPrChange.UnEventID);
                            if (!bolIsExist)
                            {
                                objInventoryProcessPrChangeList.Add(objInventoryProcessPrChange);
                            }
                        }
                        objInventoryProcess.InventoryProcessPrChangeList = objInventoryProcessPrChangeList.ToArray();
                    }
                }

                DataRow[] drOutput = dtbData.Select("IsLock = 1 AND ResolveType = 3 AND DeltaRemainQuantity = 0");
                if (drOutput.Length > 0)
                {
                    List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output> objInventoryProcessOutputList = new List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output>();
                    foreach (DataRow rOutput in drOutput)
                    {
                        ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output objInventoryProcessOutput = new ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output();
                        objInventoryProcessOutput.ProductID = Convert.ToString(rOutput["ProductID"]);
                        string strStockIMEI = string.Empty;
                        try
                        {
                            strStockIMEI = Convert.ToString(rOutput["StockIMEI"]);
                            if (strStockIMEI == string.Empty)
                            {
                                strStockIMEI = Convert.ToString(rOutput["InventoryIMEI"]);
                            }
                        }
                        catch { }
                        objInventoryProcessOutput.IMEI = strStockIMEI;
                        objInventoryProcessOutput.Quantity = Math.Abs(Convert.ToInt32(rOutput["ResolveQuantity"]));
                        objInventoryProcessOutput.OutputVoucherID = string.Empty;
                        objInventoryProcessOutput.OutputVoucherDetailID = string.Empty;
                        objInventoryProcessOutput.OutputPrice = Convert.ToDecimal(rOutput["SalePrice"]);
                        objInventoryProcessOutput.ProductStatusID = Convert.ToInt32(rOutput["ERP_PRODUCTSTATUSID"]);
                        objInventoryProcessOutput.CollectArrearPrice = Math.Round(objInventoryProcessOutput.OutputPrice * objInventoryProcessOutput.Quantity);
                        if (!Convert.IsDBNull(rOutput["UnEventID"])) objInventoryProcessOutput.UnEventID = Convert.ToString(rOutput["UnEventID"]);
                        if (!Convert.IsDBNull(rOutput["Note"])) objInventoryProcessOutput.UnEventNote = Convert.ToString(rOutput["Note"]);
                        dtbProcessHandler.Rows.Add(objInventoryProcess.InventoryID, string.Empty, objInventoryProcessOutput.UnEventID);
                        objInventoryProcessOutputList.Add(objInventoryProcessOutput);
                    }
                    objInventoryProcess.InventoryProcessOutputList = objInventoryProcessOutputList.ToArray();
                }

                DataRow[] drInput = dtbData.Select("IsLock = 1 AND ResolveType = 2 AND DeltaRemainQuantity = 0");
                if (drInput.Length > 0)
                {
                    List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input> objInventoryProcessInputList = new List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input>();
                    foreach (DataRow rInput in drInput)
                    {
                        ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input objInventoryProcessInput = new ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input();
                        objInventoryProcessInput.ProductID = Convert.ToString(rInput["ProductID"]);
                        string strInventoryIMEI = string.Empty;
                        try
                        {
                            strInventoryIMEI = Convert.ToString(rInput["InventoryIMEI"]);
                            if (strInventoryIMEI == string.Empty)
                            {
                                strInventoryIMEI = Convert.ToString(rInput["StockIMEI"]);
                            }
                        }
                        catch { }
                        objInventoryProcessInput.IMEI = strInventoryIMEI;
                        objInventoryProcessInput.Quantity = Math.Abs(Convert.ToInt32(rInput["ResolveQuantity"]));
                        objInventoryProcessInput.InputVoucherID = string.Empty;
                        objInventoryProcessInput.InputVoucherDetailID = string.Empty;
                        objInventoryProcessInput.InputPrice = Convert.ToDecimal(rInput["CostPrice"]); // Thông 13/07/2012 Sửa lại nhập thừa thì lấy giá vốn
                        objInventoryProcessInput.ProductStatusID = Convert.ToInt32(rInput["CHG_PRODUCTSTATUSID"]);
                        if (!Convert.IsDBNull(rInput["UnEventID"])) objInventoryProcessInput.UnEventID = Convert.ToString(rInput["UnEventID"]);
                        if (!Convert.IsDBNull(rInput["Note"])) objInventoryProcessInput.UnEventNote = Convert.ToString(rInput["Note"]);
                        dtbProcessHandler.Rows.Add(objInventoryProcess.InventoryID, string.Empty, objInventoryProcessInput.UnEventID);
                        objInventoryProcessInputList.Add(objInventoryProcessInput);
                    }
                    objInventoryProcess.InventoryProcessInputList = objInventoryProcessInputList.ToArray();
                }

                List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_StChange> objInventoryProcessStChangeList = new List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_StChange>();
                DataRow[] drStChange = dtbData.Select("ISLOCK = 1 AND ISCHANGESTATUS = 1");
                if (drStChange.Length > 0)
                {
                    foreach (DataRow dr in drStChange)
                    {
                        ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_StChange obj =
                                AddStatusChange((dr["PRODUCTID"] ?? "").ToString().Trim(), (dr["STOCKIMEI"] ?? "").ToString().Trim(),
                                                 Convert.ToDecimal(dr["STOCKQUANTITY"]), Convert.ToDecimal(dr["SALEPRICE"]),
                                                  Convert.ToInt32(dr["ERP_PRODUCTSTATUSID"]), Convert.ToInt32(dr["CHG_PRODUCTSTATUSID"]), (dr["NOTE"] ?? "").ToString().Trim()
                                                 );
                        obj.UnEventID = Convert.ToString(dr["UnEventID"]);
                        dtbProcessHandler.Rows.Add(objInventoryProcess.InventoryID, string.Empty, obj.UnEventID);
                        objInventoryProcessStChangeList.Add(obj);

                    }
                    objInventoryProcess.InventoryProcessStChangeList = objInventoryProcessStChangeList.ToArray();
                }


                PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();

                string strOutIMEIList = string.Empty;
                if (objInventoryProcess.InventoryProcessOutputList != null && objInventoryProcess.InventoryProcessOutputList.Length > 0)
                {
                    foreach (ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output objInventoryProcessOutput in objInventoryProcess.InventoryProcessOutputList)
                    {
                        if (objInventoryProcessOutput.IMEI != null && Convert.ToString(objInventoryProcessOutput.IMEI).Trim() != string.Empty)
                        {
                            if (!objPLCProductInStock.CheckIsExistIMEI(string.Empty, Convert.ToString(objInventoryProcessOutput.IMEI).Trim(), objInventoryProcess.InventoryStoreID, 0, -1, true, true))
                            //DataRow[] rImeiExistOut = dtbStoreInStock.Select("StoreID = " + objInventoryProcess.InventoryStoreID + " AND TRIM(IMEI)='" + Convert.ToString(objInventoryProcessOutput.IMEI).Trim() + "'");
                            //if (rImeiExistOut.Length < 1)
                            {
                                strOutIMEIList += "<" + Convert.ToString(objInventoryProcessOutput.IMEI).Trim() + ">";
                            }
                        }
                    }
                }

                string strInIMEIList = string.Empty;
                if (objInventoryProcess.InventoryProcessInputList != null && objInventoryProcess.InventoryProcessInputList.Length > 0)
                {
                    foreach (ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input objInventoryProcessInput in objInventoryProcess.InventoryProcessInputList)
                    {
                        if (objInventoryProcessInput.IMEI != null && Convert.ToString(objInventoryProcessInput.IMEI).Trim() != string.Empty)
                        {
                            if (objPLCProductInStock.CheckIsExistIMEI(string.Empty, Convert.ToString(objInventoryProcessInput.IMEI).Trim(), objInventoryProcess.InventoryStoreID, 0, -1, true, true))
                            //DataRow[] rImeiExistIn = dtbStoreInStock.Select("StoreID = " + objInventoryProcess.InventoryStoreID + " AND TRIM(IMEI)='" + Convert.ToString(objInventoryProcessInput.IMEI).Trim() + "'");
                            //if (rImeiExistIn.Length > 0)
                            {
                                strInIMEIList += "<" + Convert.ToString(objInventoryProcessInput.IMEI).Trim() + ">";
                            }
                        }
                    }
                }

                if (objInventoryProcess.InventoryProcessPrChangeList != null && objInventoryProcess.InventoryProcessPrChangeList.Length > 0)
                {
                    foreach (ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange objInventoryProcess_PrChange in objInventoryProcess.InventoryProcessPrChangeList)
                    {
                        if (objInventoryProcess_PrChange.IMEI_Out != null && Convert.ToString(objInventoryProcess_PrChange.IMEI_Out).Trim() != string.Empty)
                        {
                            if (!objPLCProductInStock.CheckIsExistIMEI(string.Empty, Convert.ToString(objInventoryProcess_PrChange.IMEI_Out).Trim(), objInventoryProcess.InventoryStoreID, 0, -1, true, true))
                            //DataRow[] rImeiExistOut = dtbStoreInStock.Select("StoreID = " + objInventoryProcess.InventoryStoreID + " AND TRIM(IMEI)='" + Convert.ToString(objInventoryProcess_PrChange.IMEI_Out).Trim() + "'");
                            //if (rImeiExistOut.Length < 1)
                            {
                                strOutIMEIList += "<" + Convert.ToString(objInventoryProcess_PrChange.IMEI_Out).Trim() + ">";
                            }
                        }
                        if (objInventoryProcess_PrChange.IMEI_In != null && Convert.ToString(objInventoryProcess_PrChange.IMEI_In).Trim() != string.Empty)
                        {
                            if (objPLCProductInStock.CheckIsExistIMEI(string.Empty, Convert.ToString(objInventoryProcess_PrChange.IMEI_In).Trim(), objInventoryProcess.InventoryStoreID, 0, -1, true, true))
                            //DataRow[] rImeiExistIn = dtbStoreInStock.Select("StoreID = " + objInventoryProcess.InventoryStoreID + " AND TRIM(IMEI)='" + Convert.ToString(objInventoryProcess_PrChange.IMEI_In).Trim() + "'");
                            //if (rImeiExistIn.Length > 0)
                            {
                                strInIMEIList += "<" + Convert.ToString(objInventoryProcess_PrChange.IMEI_In).Trim() + ">";
                            }
                        }
                    }
                }

                frmCheckIMEI_InOut frmCheckIMEI_InOut1 = new frmCheckIMEI_InOut();
                if (strOutIMEIList != string.Empty || strInIMEIList != string.Empty)
                {
                    if (strOutIMEIList != string.Empty)
                    {
                        frmCheckIMEI_InOut1.dtbCheckIMEI_OutputStore = objPLCInventory.GetCheckIMEI_OutputStore(strOutIMEIList);
                    }
                    if (strInIMEIList != string.Empty)
                    {
                        frmCheckIMEI_InOut1.dtbCheckIMEI_InputStore = objPLCInventory.GetCheckIMEI_InputStore(strInIMEIList);
                    }
                }
                if ((frmCheckIMEI_InOut1.dtbCheckIMEI_OutputStore != null && frmCheckIMEI_InOut1.dtbCheckIMEI_OutputStore.Rows.Count > 0) || (frmCheckIMEI_InOut1.dtbCheckIMEI_InputStore != null && frmCheckIMEI_InOut1.dtbCheckIMEI_InputStore.Rows.Count > 0))
                {
                    MessageBox.Show(this, "Có một số IMEI sau không xử lý được!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ERP.Inventory.PLC.Inventory.WSInventoryTerm.InventoryTerm objInventoryTerm = null;
                    objPLCInventoryTerm.LoadInfo(ref objInventoryTerm, intInventoryTermID);
                    frmCheckIMEI_InOut1.Text = "Kiểm tra IMEI xuất-nhập kho - Ngày chốt tồn kho kỳ kiểm kê: " + ((DateTime)objInventoryTerm.LockDataTime).ToString("dd/MM/yyyy HH:mm");
                    frmCheckIMEI_InOut1.ShowDialog();
                    btnSave.Enabled = true;
                    return;
                }

                objPLCInventoryProcess.AddInventoryProcess(objInventoryProcess, dtbProcessHandler);

                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnSave.Enabled = true;
                }
                else
                {
                    MessageBox.Show(this, "Cập nhật xử lý chênh lệch kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception objEx)
            {
                MessageBox.Show(this, "Lỗi cập nhật xử lý chênh lệch kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnSave.Enabled = true;
            }
        }



        private void btnSave_Click2(object sender, EventArgs e)
        {
            grdViewProducts.CloseEditor();
            grdViewProducts.UpdateCurrentRow();
            if (!CheckInput()) return;
            if (MessageBox.Show(this, "Bạn có chắc tạo phiếu yêu cầu xử lý kiểm kê?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                return;
            }
            btnSave.Enabled = false;
            DataTable dtbData = grdCtlProducts.DataSource as DataTable;
            dtbData.AcceptChanges();
            try
            {
                ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess objInventoryProcess = new PLC.Inventory.WSInventoryProcess.InventoryProcess();
                objInventoryProcess.InventoryID = strInventoryID;
                objInventoryProcess.InventoryStoreID = intStoreID;
                objInventoryProcess.MainGroupID = intMainGroupID;
                objInventoryProcess.IsNew = (intIsNew == 1 ? true : false);
                objInventoryProcess.InventoryTermID = intInventoryTermID;
                objInventoryProcess.InventoryDate = dtmInventoryDate;
                objInventoryProcess.CreatedUser = Library.AppCore.SystemConfig.objSessionUser.UserName;
                objInventoryProcess.CreatedStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                objInventoryProcess.IsProcess = false;
                objInventoryProcess.CreatedDate = DateTime.MinValue;
                objInventoryProcess.ReviewedDate = DateTime.MinValue;
                objInventoryProcess.ProcessDate = DateTime.MinValue;
                objInventoryProcess.DeletedDate = DateTime.MinValue;
                // Lay danh sach nhung phan quyen da chon 
                DataRow[] drsSelected = tblUnEventTable.Select("ISSELECT = 1");
                if (drsSelected != null && drsSelected.Length > 0)
                {


                    DataTable dtbSelected = drsSelected.CopyToDataTable();
                    DataTable dtbOut = dtbSelected.Clone();
                    DataTable dtbIn = dtbSelected.Clone();
                    DataTable dtbChangeStatus = dtbSelected.Clone();
                    DataTable dtbChangeProduct = dtbSelected.Clone();
                    List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange> objInventoryProcessPrChangeList = new List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange>();
                    List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_StChange> objInventoryProcessStChangeList = new List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_StChange>();
                    List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input> objInventoryProcessInputList = new List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input>();
                    List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output> objInventoryProcessOutputList = new List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output>();
                    foreach (DataRow dr in dtbSelected.Rows)
                    {
                        string strISCHANGESTATUS = (dr["ISCHANGESTATUS"] ?? "").ToString().Trim();
                        if (strISCHANGESTATUS == "1")
                        {
                            ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_StChange obj =
                                    AddStatusChange((dr["PRODUCTID"] ?? "").ToString().Trim(), (dr["ERP_IMEI"] ?? "").ToString().Trim(),
                                                     Convert.ToDecimal(dr["INV_QUANTITY"]), Convert.ToDecimal(dr["SALEPRICE"]),
                                                      Convert.ToInt32(dr["ERP_PRODUCTSTATUSID"]), Convert.ToInt32(dr["CHG_PRODUCTSTATUSID"]), (dr["NOTE"] ?? "").ToString().Trim()
                                                     );
                            objInventoryProcessStChangeList.Add(obj);
                            continue;
                        }

                        decimal decCHG_INQUANTITY = Convert.ToDecimal(dr["CHG_INQUANTITY"]);
                        decimal decCHG_OUTQUANTITY = Convert.ToDecimal(dr["CHG_OUTQUANTITY"]);
                        decimal decQUANTITY_INPUTTING = Convert.ToDecimal(dr["QUANTITY_INPUTTING"]);
                        if (decCHG_INQUANTITY > 0)
                        {
                            ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input obj =
                                    AddInput((dr["PRODUCTID"] ?? "").ToString().Trim(), (dr["INV_IMEI"] ?? "").ToString().Trim(),
                                                     decCHG_INQUANTITY, Convert.ToDecimal(dr["SALEPRICE"]),
                                                     Convert.ToInt32(dr["CHG_PRODUCTSTATUSID"]), (dr["NOTE"] ?? "").ToString().Trim()
                                                     );
                            objInventoryProcessInputList.Add(obj);
                            continue;
                        }

                        if (decCHG_OUTQUANTITY > 0)
                        {
                            ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output obj =
                                    AddOutput((dr["PRODUCTID"] ?? "").ToString().Trim(), (dr["ERP_IMEI"] ?? "").ToString().Trim(),
                                                     decCHG_OUTQUANTITY, Convert.ToDecimal(dr["SALEPRICE"]),
                                                     Convert.ToInt32(dr["ERP_PRODUCTSTATUSID"]), (dr["NOTE"] ?? "").ToString().Trim()
                                                     );
                            objInventoryProcessOutputList.Add(obj);
                            continue;
                        }
                        string strERP_PRODUCTSTATUSID = (dr["ERP_PRODUCTSTATUSID"] ?? "").ToString().Trim();
                        string strCHG_PRODUCTSTATUSID = (dr["CHG_PRODUCTSTATUSID"] ?? "").ToString().Trim();
                        // Check chuyen trang thai 
                        if (strISCHANGESTATUS != "1"
                            && decCHG_INQUANTITY == 0
                            && decCHG_OUTQUANTITY == 0
                            && decQUANTITY_INPUTTING == 0
                            && strERP_PRODUCTSTATUSID != strCHG_PRODUCTSTATUSID
                            && !string.IsNullOrEmpty((dr["ERP_IMEI"] ?? "").ToString().Trim())
                            )
                        {
                            ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_StChange obj =
                                    AddStatusChange((dr["PRODUCTID"] ?? "").ToString().Trim(), (dr["ERP_IMEI"] ?? "").ToString().Trim(),
                                                     Convert.ToDecimal(dr["INV_QUANTITY"]), Convert.ToDecimal(dr["SALEPRICE"]),
                                                      Convert.ToInt32(dr["ERP_PRODUCTSTATUSID"]), Convert.ToInt32(dr["CHG_PRODUCTSTATUSID"]), (dr["NOTE"] ?? "").ToString().Trim()
                                                     );
                            objInventoryProcessStChangeList.Add(obj);
                            continue;
                        }

                    }
                    objInventoryProcess.InventoryProcessInputList = objInventoryProcessInputList.ToArray();
                    objInventoryProcess.InventoryProcessOutputList = objInventoryProcessOutputList.ToArray();
                    objInventoryProcess.InventoryProcessStChangeList = objInventoryProcessStChangeList.ToArray();
                    objInventoryProcess.InventoryProcessPrChangeList = objInventoryProcessPrChangeList.ToArray();

                }





                ////tblUnEventTable
                //if (dtbProductChange != null && dtbProductChange.Rows.Count > 0)
                //{
                //    DataRow[] drProductChange = dtbProductChange.Select("ResolveType = 1 AND (ChangeUnEventID IS NOT NULL AND TRIM(ChangeUnEventID) <> '')", "ResolveQuantity DESC, UnEventID ASC, ChangeUnEventID ASC");
                //    if (drProductChange.Length > 0)
                //    {
                //        for (int i = 0; i < drProductChange.Length; i++)
                //        {
                //            if (Convert.ToInt32(drProductChange[i]["ResolveQuantity"]) > 0)
                //            {
                //                if (Convert.ToDecimal(drProductChange[i]["SalePrice"]) == 0)
                //                {
                //                    if (MessageBox.Show(this, "Sản phẩm " + Convert.ToString(drProductChange[i]["ProductName"]).Trim() + " có đơn giá(chưa có VAT) bằng 0. Bạn có tiếp tục xử lý kiểm kê không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                //                    {
                //                        btnSave.Enabled = true;
                //                        int intIndex = dtbData.Rows.IndexOf(drProductChange[i]);
                //                        if (intIndex >= 0 && intIndex < dtbData.Rows.Count)
                //                        {
                //                            //flexUnEvent.Select(flexUnEvent.Rows.Fixed + intIndex, flexUnEvent.Cols["ProductID"].Index);
                //                        }
                //                        return;
                //                    }
                //                    break;
                //                }
                //            }
                //        }
                //        List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange> objInventoryProcessPrChangeList = new List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange>();
                //        for (int i = 0; i < drProductChange.Length; i++)
                //        {
                //            DataRow rProductChange = drProductChange[i];
                //            ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange objInventoryProcessPrChange = new ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange();
                //            int intResolveQuantity = Convert.ToInt32(rProductChange["ResolveQuantity"]);
                //            string strStockIMEI = string.Empty;
                //            string strInventoryIMEI = string.Empty;
                //            if (intResolveQuantity > 0)
                //            {
                //                objInventoryProcessPrChange.ProductID_Out = Convert.ToString(rProductChange["ProductID"]);
                //                try
                //                {
                //                    strStockIMEI = Convert.ToString(rProductChange["StockIMEI"]);
                //                    if (strStockIMEI == string.Empty)
                //                    {
                //                        strStockIMEI = Convert.ToString(rProductChange["InventoryIMEI"]);
                //                    }
                //                }
                //                catch { }
                //                objInventoryProcessPrChange.IMEI_Out = strStockIMEI;
                //                objInventoryProcessPrChange.RefSalePrice_Out = Convert.ToDecimal(rProductChange["SalePrice"]);
                //                DataRow[] rIn = dtbData.Select("TRIM(UnEventID) = '" + Convert.ToString(rProductChange["ChangeUnEventID"]) + "'");
                //                if (rIn.Length > 0)
                //                {
                //                    objInventoryProcessPrChange.ProductID_In = Convert.ToString(rIn[0]["ProductID"]);
                //                    try
                //                    {
                //                        strInventoryIMEI = Convert.ToString(rIn[0]["InventoryIMEI"]);
                //                        if (strInventoryIMEI == string.Empty)
                //                        {
                //                            strInventoryIMEI = Convert.ToString(rIn[0]["StockIMEI"]);
                //                        }
                //                    }
                //                    catch { }
                //                    objInventoryProcessPrChange.IMEI_In = strInventoryIMEI;
                //                    objInventoryProcessPrChange.RefSalePrice_In = Convert.ToDecimal(rIn[0]["SalePrice"]);

                //                    objInventoryProcessPrChange.ChangeUnEventID = Convert.ToString(rProductChange["ChangeUnEventID"]);
                //                    if (!Convert.IsDBNull(rIn[0]["Note"])) objInventoryProcessPrChange.ChangeUnEventNote = Convert.ToString(rIn[0]["Note"]);
                //                }
                //                objInventoryProcessPrChange.Quantity = Math.Abs(intResolveQuantity);
                //                objInventoryProcessPrChange.ProductChangeDetailID = string.Empty;
                //                objInventoryProcessPrChange.UnEventAmount = Math.Round((objInventoryProcessPrChange.RefSalePrice_Out - objInventoryProcessPrChange.RefSalePrice_In) * objInventoryProcessPrChange.Quantity);
                //                if (objInventoryProcessPrChange.UnEventAmount < 0)
                //                {
                //                    objInventoryProcessPrChange.CollectArrearPrice = 0;
                //                }
                //                else
                //                {
                //                    objInventoryProcessPrChange.CollectArrearPrice = objInventoryProcessPrChange.UnEventAmount;
                //                }
                //                if (!Convert.IsDBNull(rProductChange["UnEventID"])) objInventoryProcessPrChange.UnEventID = Convert.ToString(rProductChange["UnEventID"]);
                //                if (!Convert.IsDBNull(rProductChange["Note"])) objInventoryProcessPrChange.UnEventNote = Convert.ToString(rProductChange["Note"]);
                //            }
                //            else
                //            {
                //                objInventoryProcessPrChange.ProductID_In = Convert.ToString(rProductChange["ProductID"]);
                //                try
                //                {
                //                    strInventoryIMEI = Convert.ToString(rProductChange["InventoryIMEI"]);
                //                    if (strInventoryIMEI == string.Empty)
                //                    {
                //                        strInventoryIMEI = Convert.ToString(rProductChange["StockIMEI"]);
                //                    }
                //                }
                //                catch { }
                //                objInventoryProcessPrChange.IMEI_In = strInventoryIMEI;
                //                objInventoryProcessPrChange.RefSalePrice_In = Convert.ToDecimal(rProductChange["SalePrice"]);
                //                DataRow[] rOut = dtbData.Select("TRIM(UnEventID) = '" + Convert.ToString(rProductChange["ChangeUnEventID"]) + "'");
                //                if (rOut.Length > 0)
                //                {
                //                    objInventoryProcessPrChange.ProductID_Out = Convert.ToString(rOut[0]["ProductID"]);
                //                    try
                //                    {
                //                        strStockIMEI = Convert.ToString(rOut[0]["StockIMEI"]);
                //                        if (strStockIMEI == string.Empty)
                //                        {
                //                            strStockIMEI = Convert.ToString(rOut[0]["InventoryIMEI"]);
                //                        }
                //                    }
                //                    catch { }
                //                    objInventoryProcessPrChange.IMEI_Out = strStockIMEI;
                //                    objInventoryProcessPrChange.RefSalePrice_Out = Convert.ToDecimal(rOut[0]["SalePrice"]);

                //                    objInventoryProcessPrChange.ChangeUnEventID = Convert.ToString(rProductChange["ChangeUnEventID"]);
                //                    if (!Convert.IsDBNull(rOut[0]["Note"])) objInventoryProcessPrChange.ChangeUnEventNote = Convert.ToString(rOut[0]["Note"]);
                //                }
                //                objInventoryProcessPrChange.Quantity = Math.Abs(intResolveQuantity);
                //                objInventoryProcessPrChange.ProductChangeDetailID = string.Empty;
                //                objInventoryProcessPrChange.UnEventAmount = Math.Round((objInventoryProcessPrChange.RefSalePrice_In - objInventoryProcessPrChange.RefSalePrice_Out) * objInventoryProcessPrChange.Quantity);
                //                if (objInventoryProcessPrChange.UnEventAmount < 0)
                //                {
                //                    objInventoryProcessPrChange.CollectArrearPrice = 0;
                //                }
                //                else
                //                {
                //                    objInventoryProcessPrChange.CollectArrearPrice = objInventoryProcessPrChange.UnEventAmount;
                //                }
                //                if (!Convert.IsDBNull(rProductChange["UnEventID"])) objInventoryProcessPrChange.UnEventID = Convert.ToString(rProductChange["UnEventID"]);
                //                if (!Convert.IsDBNull(rProductChange["Note"])) objInventoryProcessPrChange.UnEventNote = Convert.ToString(rProductChange["Note"]);
                //            }
                //            bool bolIsExist = false;
                //            if (objInventoryProcessPrChange.IMEI_Out.Trim() != string.Empty)
                //            {
                //                if (objInventoryProcessPrChangeList.Count > 0)
                //                {
                //                    foreach (ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange itemPrChange in objInventoryProcessPrChangeList)
                //                    {
                //                        if (itemPrChange.ProductID_Out == objInventoryProcessPrChange.ProductID_Out
                //                            && itemPrChange.IMEI_Out == objInventoryProcessPrChange.IMEI_Out
                //                            && itemPrChange.ProductID_In == objInventoryProcessPrChange.ProductID_In
                //                            && itemPrChange.IMEI_In == objInventoryProcessPrChange.IMEI_In)
                //                        {
                //                            bolIsExist = true;
                //                            break;
                //                        }
                //                    }
                //                }
                //            }
                //            else
                //            {
                //                if (objInventoryProcessPrChangeList.Count > 0)
                //                {
                //                    foreach (ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange itemPrChange in objInventoryProcessPrChangeList)
                //                    {
                //                        if (itemPrChange.ProductID_Out == objInventoryProcessPrChange.ProductID_Out
                //                            && itemPrChange.ProductID_In == objInventoryProcessPrChange.ProductID_In)
                //                        {
                //                            if (itemPrChange.UnEventID == objInventoryProcessPrChange.UnEventID)
                //                            {
                //                                itemPrChange.Quantity = itemPrChange.Quantity + objInventoryProcessPrChange.Quantity;
                //                            }
                //                            bolIsExist = true;
                //                            break;
                //                        }
                //                    }
                //                }
                //            }
                //            if (!bolIsExist)
                //            {
                //                objInventoryProcessPrChangeList.Add(objInventoryProcessPrChange);
                //            }
                //        }
                //        objInventoryProcess.InventoryProcessPrChangeList = objInventoryProcessPrChangeList.ToArray();
                //    }
                //}

                //DataRow[] drOutput = dtbData.Select("IsLock = 1 AND ResolveType = 3 AND DeltaRemainQuantity = 0");
                //if (drOutput.Length > 0)
                //{
                //    List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output> objInventoryProcessOutputList = new List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output>();
                //    foreach (DataRow rOutput in drOutput)
                //    {
                //        ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output objInventoryProcessOutput = new ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output();
                //        objInventoryProcessOutput.ProductID = Convert.ToString(rOutput["ProductID"]);
                //        string strStockIMEI = string.Empty;
                //        try
                //        {
                //            strStockIMEI = Convert.ToString(rOutput["StockIMEI"]);
                //            if (strStockIMEI == string.Empty)
                //            {
                //                strStockIMEI = Convert.ToString(rOutput["InventoryIMEI"]);
                //            }
                //        }
                //        catch { }
                //        objInventoryProcessOutput.IMEI = strStockIMEI;
                //        objInventoryProcessOutput.Quantity = Math.Abs(Convert.ToInt32(rOutput["ResolveQuantity"]));
                //        objInventoryProcessOutput.OutputVoucherID = string.Empty;
                //        objInventoryProcessOutput.OutputVoucherDetailID = string.Empty;
                //        objInventoryProcessOutput.OutputPrice = Convert.ToDecimal(rOutput["SalePrice"]);
                //        objInventoryProcessOutput.CollectArrearPrice = Math.Round(objInventoryProcessOutput.OutputPrice * objInventoryProcessOutput.Quantity);
                //        if (!Convert.IsDBNull(rOutput["UnEventID"])) objInventoryProcessOutput.UnEventID = Convert.ToString(rOutput["UnEventID"]);
                //        if (!Convert.IsDBNull(rOutput["Note"])) objInventoryProcessOutput.UnEventNote = Convert.ToString(rOutput["Note"]);
                //        objInventoryProcessOutputList.Add(objInventoryProcessOutput);
                //    }
                //    objInventoryProcess.InventoryProcessOutputList = objInventoryProcessOutputList.ToArray();
                //}

                //DataRow[] drInput = dtbData.Select("IsLock = 1 AND ResolveType = 2 AND DeltaRemainQuantity = 0");
                //if (drInput.Length > 0)
                //{
                //    List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input> objInventoryProcessInputList = new List<ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input>();
                //    foreach (DataRow rInput in drInput)
                //    {
                //        ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input objInventoryProcessInput = new ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input();
                //        objInventoryProcessInput.ProductID = Convert.ToString(rInput["ProductID"]);
                //        string strInventoryIMEI = string.Empty;
                //        try
                //        {
                //            strInventoryIMEI = Convert.ToString(rInput["InventoryIMEI"]);
                //            if (strInventoryIMEI == string.Empty)
                //            {
                //                strInventoryIMEI = Convert.ToString(rInput["StockIMEI"]);
                //            }
                //        }
                //        catch { }
                //        objInventoryProcessInput.IMEI = strInventoryIMEI;
                //        objInventoryProcessInput.Quantity = Math.Abs(Convert.ToInt32(rInput["ResolveQuantity"]));
                //        objInventoryProcessInput.InputVoucherID = string.Empty;
                //        objInventoryProcessInput.InputVoucherDetailID = string.Empty;
                //        objInventoryProcessInput.InputPrice = Convert.ToDecimal(rInput["CostPrice"]); // Thông 13/07/2012 Sửa lại nhập thừa thì lấy giá vốn
                //        if (!Convert.IsDBNull(rInput["UnEventID"])) objInventoryProcessInput.UnEventID = Convert.ToString(rInput["UnEventID"]);
                //        if (!Convert.IsDBNull(rInput["Note"])) objInventoryProcessInput.UnEventNote = Convert.ToString(rInput["Note"]);
                //        objInventoryProcessInputList.Add(objInventoryProcessInput);
                //    }
                //    objInventoryProcess.InventoryProcessInputList = objInventoryProcessInputList.ToArray();
                //}

                //PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();

                //string strOutIMEIList = string.Empty;
                //if (objInventoryProcess.InventoryProcessOutputList != null && objInventoryProcess.InventoryProcessOutputList.Length > 0)
                //{
                //    foreach (ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Output objInventoryProcessOutput in objInventoryProcess.InventoryProcessOutputList)
                //    {
                //        if (objInventoryProcessOutput.IMEI != null && Convert.ToString(objInventoryProcessOutput.IMEI).Trim() != string.Empty)
                //        {
                //            if (!objPLCProductInStock.CheckIsExistIMEI(string.Empty, Convert.ToString(objInventoryProcessOutput.IMEI).Trim(), objInventoryProcess.InventoryStoreID, 0, -1, true, true))
                //            //DataRow[] rImeiExistOut = dtbStoreInStock.Select("StoreID = " + objInventoryProcess.InventoryStoreID + " AND TRIM(IMEI)='" + Convert.ToString(objInventoryProcessOutput.IMEI).Trim() + "'");
                //            //if (rImeiExistOut.Length < 1)
                //            {
                //                strOutIMEIList += "<" + Convert.ToString(objInventoryProcessOutput.IMEI).Trim() + ">";
                //            }
                //        }
                //    }
                //}

                //string strInIMEIList = string.Empty;
                //if (objInventoryProcess.InventoryProcessInputList != null && objInventoryProcess.InventoryProcessInputList.Length > 0)
                //{
                //    foreach (ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_Input objInventoryProcessInput in objInventoryProcess.InventoryProcessInputList)
                //    {
                //        if (objInventoryProcessInput.IMEI != null && Convert.ToString(objInventoryProcessInput.IMEI).Trim() != string.Empty)
                //        {
                //            if (objPLCProductInStock.CheckIsExistIMEI(string.Empty, Convert.ToString(objInventoryProcessInput.IMEI).Trim(), objInventoryProcess.InventoryStoreID, 0, -1, true, true))
                //            //DataRow[] rImeiExistIn = dtbStoreInStock.Select("StoreID = " + objInventoryProcess.InventoryStoreID + " AND TRIM(IMEI)='" + Convert.ToString(objInventoryProcessInput.IMEI).Trim() + "'");
                //            //if (rImeiExistIn.Length > 0)
                //            {
                //                strInIMEIList += "<" + Convert.ToString(objInventoryProcessInput.IMEI).Trim() + ">";
                //            }
                //        }
                //    }
                //}

                //if (objInventoryProcess.InventoryProcessPrChangeList != null && objInventoryProcess.InventoryProcessPrChangeList.Length > 0)
                //{
                //    foreach (ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess_PrChange objInventoryProcess_PrChange in objInventoryProcess.InventoryProcessPrChangeList)
                //    {
                //        if (objInventoryProcess_PrChange.IMEI_Out != null && Convert.ToString(objInventoryProcess_PrChange.IMEI_Out).Trim() != string.Empty)
                //        {
                //            if (!objPLCProductInStock.CheckIsExistIMEI(string.Empty, Convert.ToString(objInventoryProcess_PrChange.IMEI_Out).Trim(), objInventoryProcess.InventoryStoreID, 0, -1, true, true))
                //            //DataRow[] rImeiExistOut = dtbStoreInStock.Select("StoreID = " + objInventoryProcess.InventoryStoreID + " AND TRIM(IMEI)='" + Convert.ToString(objInventoryProcess_PrChange.IMEI_Out).Trim() + "'");
                //            //if (rImeiExistOut.Length < 1)
                //            {
                //                strOutIMEIList += "<" + Convert.ToString(objInventoryProcess_PrChange.IMEI_Out).Trim() + ">";
                //            }
                //        }
                //        if (objInventoryProcess_PrChange.IMEI_In != null && Convert.ToString(objInventoryProcess_PrChange.IMEI_In).Trim() != string.Empty)
                //        {
                //            if (objPLCProductInStock.CheckIsExistIMEI(string.Empty, Convert.ToString(objInventoryProcess_PrChange.IMEI_In).Trim(), objInventoryProcess.InventoryStoreID, 0, -1, true, true))
                //            //DataRow[] rImeiExistIn = dtbStoreInStock.Select("StoreID = " + objInventoryProcess.InventoryStoreID + " AND TRIM(IMEI)='" + Convert.ToString(objInventoryProcess_PrChange.IMEI_In).Trim() + "'");
                //            //if (rImeiExistIn.Length > 0)
                //            {
                //                strInIMEIList += "<" + Convert.ToString(objInventoryProcess_PrChange.IMEI_In).Trim() + ">";
                //            }
                //        }
                //    }
                //}

                //frmCheckIMEI_InOut frmCheckIMEI_InOut1 = new frmCheckIMEI_InOut();
                //if (strOutIMEIList != string.Empty || strInIMEIList != string.Empty)
                //{
                //    if (strOutIMEIList != string.Empty)
                //    {
                //        frmCheckIMEI_InOut1.dtbCheckIMEI_OutputStore = objPLCInventory.GetCheckIMEI_OutputStore(strOutIMEIList);
                //    }
                //    if (strInIMEIList != string.Empty)
                //    {
                //        frmCheckIMEI_InOut1.dtbCheckIMEI_InputStore = objPLCInventory.GetCheckIMEI_InputStore(strInIMEIList);
                //    }
                //}
                //if ((frmCheckIMEI_InOut1.dtbCheckIMEI_OutputStore != null && frmCheckIMEI_InOut1.dtbCheckIMEI_OutputStore.Rows.Count > 0) || (frmCheckIMEI_InOut1.dtbCheckIMEI_InputStore != null && frmCheckIMEI_InOut1.dtbCheckIMEI_InputStore.Rows.Count > 0))
                //{
                //    MessageBox.Show(this, "Có một số IMEI sau không xử lý được!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    ERP.Inventory.PLC.Inventory.WSInventoryTerm.InventoryTerm objInventoryTerm = null;
                //    objPLCInventoryTerm.LoadInfo(ref objInventoryTerm, intInventoryTermID);
                //    frmCheckIMEI_InOut1.Text = "Kiểm tra IMEI xuất-nhập kho - Ngày chốt tồn kho kỳ kiểm kê: " + ((DateTime)objInventoryTerm.LockDataTime).ToString("dd/MM/yyyy HH:mm");
                //    frmCheckIMEI_InOut1.ShowDialog();
                //    btnSave.Enabled = true;
                //    return;
                //}

                objPLCInventoryProcess.AddInventoryProcess(objInventoryProcess, new DataTable("NULL"));

                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnSave.Enabled = true;
                }
                else
                {
                    MessageBox.Show(this, "Cập nhật xử lý chênh lệch kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception objEx)
            {
                MessageBox.Show(this, "Lỗi cập nhật xử lý chênh lệch kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnSave.Enabled = true;
            }
        }

        private void flexUnEvent_SetupEditor(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            //if (flexUnEvent.RowSel >= flexUnEvent.Rows.Fixed)
            //{
            //    if (e.Col == flexUnEvent.Cols["Note"].Index)
            //    {
            //        TextBox txtEditor1 = flexUnEvent.Editor as TextBox;
            //        if (txtEditor1 != null)
            //        {
            //            txtEditor1.MaxLength = 2000;
            //        }
            //    }
            //}
        }

        private void grdViewProducts_ShowingEditor(object sender, CancelEventArgs e)
        {
            int intRowFocus = grdViewProducts.FocusedRowHandle;
            if (intRowFocus == GridControl.AutoFilterRowHandle)
                return;
            DataRow dr = grdViewProducts.GetFocusedDataRow();
            if (dr == null)
                return;
            string strIsLock = (dr["ISLOCK"] ?? "").ToString().Trim();
            if (strIsLock == "1"
                && grdViewProducts.FocusedColumn.FieldName != "ISLOCK")
                e.Cancel = true;
        }

        private void grdViewProducts_HiddenEditor(object sender, EventArgs e)
        {
            int intRowFocus = grdViewProducts.FocusedRowHandle;
            if (intRowFocus == GridControl.AutoFilterRowHandle)
                return;
            string strFieldName = grdViewProducts.FocusedColumn.FieldName;
            DataRow dr = grdViewProducts.GetDataRow(intRowFocus);
            if (dr == null)
                return;
            int intIsRequestImei = Convert.ToInt32(dr["ISREQUESTIMEI"]);

            // Chenh lech sau khi giai trinh
            decimal decQUANTITY_DIFFERENT = 0;
            if (!Convert.IsDBNull(grdViewProducts.GetFocusedRowCellValue("QUANTITY_DIFFERENT")))
                decQUANTITY_DIFFERENT = Convert.ToDecimal(grdViewProducts.GetFocusedRowCellValue("QUANTITY_DIFFERENT"));

            // So luong hang di duong 
            decimal decInputing = 0;
            if (!Convert.IsDBNull(grdViewProducts.GetFocusedRowCellValue("INPUTTINGQUANTITY")))
                decInputing = Convert.ToDecimal(grdViewProducts.GetFocusedRowCellValue("INPUTTINGQUANTITY"));
            if (intIsRequestImei == 1)
            {
                if (decInputing < 0 && strFieldName == "INPUTTINGQUANTITY")
                {
                    grdViewProducts.SetFocusedRowCellValue("INPUTTINGQUANTITY", 0);
                    decInputing = 0;
                }
                if (decInputing >= 1 && strFieldName == "INPUTTINGQUANTITY")
                {
                    grdViewProducts.SetFocusedRowCellValue("INPUTTINGQUANTITY", 1);
                    decInputing = 1;
                }
            }
            //check lech hang di duong 
            decimal decDELTAINPUTTINGQUANTITY = decQUANTITY_DIFFERENT - decInputing;
            grdViewProducts.SetFocusedRowCellValue("DELTAINPUTTINGQUANTITY", decDELTAINPUTTINGQUANTITY);

            // lay du lieu đã xử lý
            decimal decADJUSTUNEVENTQUANTITY = 0;
            if (!Convert.IsDBNull(grdViewProducts.GetFocusedRowCellValue("ADJUSTUNEVENTQUANTITY")))
                decADJUSTUNEVENTQUANTITY = Convert.ToDecimal(grdViewProducts.GetFocusedRowCellValue("ADJUSTUNEVENTQUANTITY"));

            //
            decimal decDELTAADJUSTUNEVENTQUANTITY = decDELTAINPUTTINGQUANTITY - decADJUSTUNEVENTQUANTITY;
            grdViewProducts.SetFocusedRowCellValue("DELTAADJUSTUNEVENTQUANTITY", decDELTAADJUSTUNEVENTQUANTITY);
            dr["RESOLVEQUANTITY"] = decDELTAADJUSTUNEVENTQUANTITY;
            dr["DELTAADJUSTUNEVENTQUANTITY"] = decDELTAADJUSTUNEVENTQUANTITY;
            dr["DELTAREMAINQUANTITY"] = decDELTAADJUSTUNEVENTQUANTITY;




            //decInputing = 0;
            //if (!Convert.IsDBNull(grdViewProducts.GetFocusedRowCellValue("QUANTITY_INPUTTING")))
            //    decInputing = Convert.ToDecimal(grdViewProducts.GetFocusedRowCellValue("QUANTITY_INPUTTING"));
            //decInQuantity = 0;
            //if (!Convert.IsDBNull(grdViewProducts.GetFocusedRowCellValue("CHG_INQUANTITY")))
            //    decInQuantity = Convert.ToDecimal(grdViewProducts.GetFocusedRowCellValue("CHG_INQUANTITY"));
            //decOutQuantity = 0;
            //if (!Convert.IsDBNull(grdViewProducts.GetFocusedRowCellValue("CHG_OUTQUANTITY")))
            //    decOutQuantity = Convert.ToDecimal(grdViewProducts.GetFocusedRowCellValue("CHG_OUTQUANTITY"));
            //// Tính chenh lech
            //grdViewProducts.SetFocusedRowCellValue("CHG_DIFQUANTITY",
            //    Convert.ToDecimal(grdViewProducts.GetFocusedRowCellValue("QUANTITY_DIFFERENT")) - decInputing + decInQuantity - decOutQuantity);

        }

        private void mnuChangeStatus_Click(object sender, EventArgs e)
        {
            grdViewProducts.CloseEditor();
            grdViewProducts.UpdateCurrentRow();
            try
            {
                if (grdViewProducts.RowCount == 0)
                    return;

                DataTable dtbData = grdCtlProducts.DataSource as DataTable;
                if (dtbData != null && dtbData.Rows.Count > 0)
                {
                    dtbData.AcceptChanges();
                    DataRow[] drIsHasIMEI = dtbData.Select("IsSelect = 1 AND ((StockIMEI IS NOT NULL AND TRIM(StockIMEI) <> '') OR (InventoryIMEI IS NOT NULL AND TRIM(InventoryIMEI) <> ''))");
                    DataRow[] drNotIMEI = dtbData.Select("IsSelect = 1 AND ((StockIMEI IS NULL AND InventoryIMEI IS NULL) OR (TRIM(StockIMEI) = '' AND TRIM(InventoryIMEI) = ''))");
                    if (drIsHasIMEI.Length > 0 && drNotIMEI.Length > 0)
                    {
                        MessageBox.Show(this, "Chỉ có thể xử lý chênh lệch kiểm kê cho các sản phẩm hoặc có IMEI hoặc không có IMEI", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    DataRow[] drProductChange;

                    if (drNotIMEI.Length > 0)
                    {
                        frmInputResolveQuantity frmInputResolveQuantity1 = new frmInputResolveQuantity();
                        if (frmInputResolveQuantity1.dtbProduct == null)
                        {
                            frmInputResolveQuantity1.dtbProduct = new DataTable();
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("ProductID", typeof(string));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("ProductName", typeof(string));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("DeltaAdjustUnEventQuantity", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("DeltaRemainQuantity", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("ResolveQuantity", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("ResolveQuantity2", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("ResolveNote", typeof(string));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("ResolveType", typeof(int));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("UnEventID", typeof(string));
                            frmInputResolveQuantity1.dtbProduct.Columns.Add("ChangeUnEventID", typeof(string));
                        }
                        foreach (DataRow rNotIMEI in drNotIMEI)
                        {
                            DataRow rProduct = frmInputResolveQuantity1.dtbProduct.NewRow();
                            rProduct["UnEventID"] = Convert.ToString(rNotIMEI["UnEventID"]);
                            rProduct["ProductID"] = Convert.ToString(rNotIMEI["ProductID"]);
                            rProduct["ProductName"] = Convert.ToString(rNotIMEI["ProductName"]);
                            rProduct["DeltaAdjustUnEventQuantity"] = Convert.ToInt32(rNotIMEI["DeltaAdjustUnEventQuantity"]);
                            rProduct["DeltaRemainQuantity"] = Convert.ToInt32(rNotIMEI["DeltaRemainQuantity"]);
                            rProduct["ResolveQuantity"] = 0;
                            rProduct["ResolveQuantity2"] = Convert.ToInt32(rNotIMEI["ResolveQuantity2"]);
                            rProduct["ResolveNote"] = string.Empty;
                            try
                            {
                                rProduct["ChangeUnEventID"] = Convert.ToInt32(rNotIMEI["ChangeUnEventID"]);
                            }
                            catch { }

                            frmInputResolveQuantity1.dtbProduct.Rows.Add(rProduct);
                        }
                        frmInputResolveQuantity1.OutputChange = true;
                        frmInputResolveQuantity1.ShowDialog();
                        if (frmInputResolveQuantity1.AnswerYes)
                        {
                            if (dtbProductChange == null || dtbProductChange.Rows.Count < 1)
                            {
                                dtbProductChange = dtbData.Clone();
                            }

                            foreach (DataRow rProduct in frmInputResolveQuantity1.dtbProduct.Rows)
                            {
                                for (int i = 0; i < dtbData.Rows.Count; i++)
                                {
                                    DataRow rData = dtbData.Rows[i];
                                    if (Convert.ToString(rData["UnEventID"]) == Convert.ToString(rProduct["UnEventID"]))
                                    {
                                        try
                                        {
                                            rData["ChangeUnEventID"] = Convert.ToString(rProduct["ChangeUnEventID"]);
                                        }
                                        catch { }

                                        rData["ResolveQuantity"] = Convert.ToInt32(rProduct["ResolveQuantity"]);
                                        rData["ResolveQuantity2"] = Convert.ToInt32(rProduct["ResolveQuantity2"]) + Convert.ToInt32(rProduct["ResolveQuantity"]);
                                        int intDeltaRemainQuantity = Convert.ToInt32(rProduct["DeltaRemainQuantity"]);
                                        rData["DeltaRemainQuantity"] = intDeltaRemainQuantity;

                                        string strResolveNoteProduct = string.Empty;
                                        try
                                        {
                                            strResolveNoteProduct = Convert.ToString(rProduct["ResolveNote"]).Trim();
                                        }
                                        catch { }
                                        string strResolveNote = string.Empty;
                                        try
                                        {
                                            strResolveNote = Convert.ToString(rData["ResolveNote"]).Trim();
                                        }
                                        catch { }
                                        if (strResolveNote != string.Empty)
                                        {
                                            string[] strSpl = strResolveNote.Split(';');
                                            // flexUnEvent.Rows[flexUnEvent.Rows.Fixed + i].Height = 20 * (strSpl.Length + 1);
                                            strResolveNote = strResolveNote + ";\r\n" + strResolveNoteProduct;
                                        }
                                        else
                                        {
                                            strResolveNote = strResolveNoteProduct;
                                        }
                                        rData["ResolveNote"] = strResolveNote;

                                        int intResolveType = Convert.ToInt32(rProduct["ResolveType"]);
                                        rData["ResolveType"] = intResolveType;
                                        if (intResolveType == 1)
                                        {
                                            // Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.LightBlue);
                                            rData["IsSelect"] = false;
                                            if (intDeltaRemainQuantity == 0)
                                            {
                                                rData["IsLock"] = true;
                                            }
                                        }
                                        else if (intResolveType == 2)
                                        {
                                            //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.Yellow);
                                            rData["IsSelect"] = false;
                                            if (intDeltaRemainQuantity == 0)
                                            {
                                                rData["IsLock"] = true;
                                            }
                                        }
                                        else if (intResolveType == 3)
                                        {
                                            //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.Orange);
                                            rData["IsSelect"] = false;
                                            if (intDeltaRemainQuantity == 0)
                                            {
                                                rData["IsLock"] = true;
                                            }
                                        }
                                        else
                                        {
                                            //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + i, Color.White);
                                            rData["ResolveType"] = 0;
                                            rData["IsSelect"] = true;
                                            rData["IsLock"] = false;
                                        }

                                        drProductChange = dtbData.Select("UnEventID = '" + Convert.ToString(rData["UnEventID"]) + "'");
                                        if (drProductChange.Length > 0)
                                        {
                                            dtbProductChange.ImportRow(drProductChange[0]);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (drIsHasIMEI.Length == 2)
                        {
                            if (MessageBox.Show(this, "Bạn có chắc xuất đổi hàng?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                            {
                                return;
                            }

                            if (dtbProductChange == null || dtbProductChange.Rows.Count < 1)
                            {
                                dtbProductChange = dtbData.Clone();
                            }

                            DataRow rIsHasIMEI = drIsHasIMEI[0];
                            DataRow rIsHasIMEIOther = drIsHasIMEI[1];
                            DataRow rData = null;
                            DataRow rDataOther = null;
                            int intIndex = 0;
                            int intIndexOther = 0;
                            for (intIndex = 0; intIndex < dtbData.Rows.Count; intIndex++)
                            {
                                if (Convert.ToString(rIsHasIMEI["UnEventID"]) == Convert.ToString(dtbData.Rows[intIndex]["UnEventID"]))
                                {
                                    rData = dtbData.Rows[intIndex];
                                    break;
                                }
                            }
                            for (intIndexOther = 0; intIndexOther < dtbData.Rows.Count; intIndexOther++)
                            {
                                if (Convert.ToString(rIsHasIMEIOther["UnEventID"]) == Convert.ToString(dtbData.Rows[intIndexOther]["UnEventID"]))
                                {
                                    rDataOther = dtbData.Rows[intIndexOther];
                                    break;
                                }
                            }
                            if (rData != null && rDataOther != null)
                            {
                                try
                                {
                                    rData["ChangeUnEventID"] = Convert.ToString(rDataOther["UnEventID"]);
                                }
                                catch { }

                                try
                                {
                                    rDataOther["ChangeUnEventID"] = Convert.ToString(rData["UnEventID"]);
                                }
                                catch { }

                                #region rData
                                int intDeltaRemainQuantity = Convert.ToInt32(rData["DeltaRemainQuantity"]);
                                rData["ResolveQuantity"] = intDeltaRemainQuantity;
                                rData["ResolveQuantity2"] = intDeltaRemainQuantity;
                                rData["DeltaRemainQuantity"] = 0;
                                rData["ResolveType"] = 0;
                                rData["IsSelect"] = false;
                                rData["IsLock"] = true;
                                string strResolveNote = string.Empty;
                                try
                                {
                                    strResolveNote = Convert.ToString(rData["ResolveNote"]);
                                }
                                catch { }

                                if (strResolveNote != string.Empty)
                                {
                                    string[] strSpl = strResolveNote.Split(';');
                                    //flexUnEvent.Rows[flexUnEvent.Rows.Fixed + intIndex].Height = 20 * (strSpl.Length + 1);

                                    if (intDeltaRemainQuantity < 0)
                                    {
                                        strResolveNote = strResolveNote + ";\r\n" + intDeltaRemainQuantity.ToString().TrimStart('-') + ": nhập đổi " + (Convert.ToBoolean(rDataOther["IsRequestIMEI"]) ? ("[" + Convert.ToString(rDataOther["StockIMEI"]) + "] ") : string.Empty) + Convert.ToString(rDataOther["ProductName"]);
                                        rData["ResolveNote"] = strResolveNote;
                                        rData["ResolveType"] = 1;
                                        //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndex, Color.LightBlue);
                                    }
                                    else
                                    {
                                        strResolveNote = strResolveNote + ";\r\n" + intDeltaRemainQuantity.ToString().TrimStart('-') + ": xuất đổi " + (Convert.ToBoolean(rDataOther["IsRequestIMEI"]) ? ("[" + Convert.ToString(rDataOther["InventoryIMEI"]) + "] ") : string.Empty) + Convert.ToString(rDataOther["ProductName"]);
                                        rData["ResolveNote"] = strResolveNote;
                                        rData["ResolveType"] = 1;
                                        //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndex, Color.LightBlue);
                                    }
                                }
                                else
                                {
                                    if (intDeltaRemainQuantity < 0)
                                    {
                                        strResolveNote = intDeltaRemainQuantity.ToString().TrimStart('-') + ": nhập đổi " + (Convert.ToBoolean(rDataOther["IsRequestIMEI"]) ? ("[" + Convert.ToString(rDataOther["StockIMEI"]) + "] ") : string.Empty) + Convert.ToString(rDataOther["ProductName"]);
                                        rData["ResolveNote"] = strResolveNote;
                                        rData["ResolveType"] = 1;
                                        // Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndex, Color.LightBlue);
                                    }
                                    else
                                    {
                                        strResolveNote = intDeltaRemainQuantity.ToString().TrimStart('-') + ": xuất đổi " + (Convert.ToBoolean(rDataOther["IsRequestIMEI"]) ? ("[" + Convert.ToString(rDataOther["InventoryIMEI"]) + "] ") : string.Empty) + Convert.ToString(rDataOther["ProductName"]);
                                        rData["ResolveNote"] = strResolveNote;
                                        rData["ResolveType"] = 1;
                                        //Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndex, Color.LightBlue);
                                    }
                                }
                                #endregion
                                #region rDataOther
                                int intDeltaRemainQuantityOther = Convert.ToInt32(rDataOther["DeltaRemainQuantity"]);
                                rDataOther["ResolveQuantity"] = intDeltaRemainQuantityOther;
                                rDataOther["ResolveQuantity2"] = intDeltaRemainQuantityOther;
                                rDataOther["DeltaRemainQuantity"] = 0;
                                rDataOther["ResolveType"] = 0;
                                rDataOther["IsSelect"] = false;
                                rDataOther["IsLock"] = true;
                                string strResolveNoteOther = string.Empty;
                                try
                                {
                                    strResolveNoteOther = Convert.ToString(rDataOther["ResolveNote"]);
                                }
                                catch { }

                                if (strResolveNoteOther != string.Empty)
                                {
                                    string[] strSplOther = strResolveNoteOther.Split(';');
                                    //flexUnEvent.Rows[flexUnEvent.Rows.Fixed + intIndexOther].Height = 20 * (strSplOther.Length + 1);

                                    if (intDeltaRemainQuantityOther < 0)
                                    {
                                        strResolveNoteOther = strResolveNoteOther + ";\r\n" + intDeltaRemainQuantityOther.ToString().TrimStart('-') + ": nhập đổi " + (Convert.ToBoolean(rData["IsRequestIMEI"]) ? ("[" + Convert.ToString(rData["StockIMEI"]) + "] ") : string.Empty) + Convert.ToString(rData["ProductName"]);
                                        rDataOther["ResolveNote"] = strResolveNoteOther;
                                        rDataOther["ResolveType"] = 1;
                                        // Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndexOther, Color.LightBlue);
                                    }
                                    else
                                    {
                                        strResolveNoteOther = strResolveNoteOther + ";\r\n" + intDeltaRemainQuantityOther.ToString().TrimStart('-') + ": xuất đổi " + (Convert.ToBoolean(rData["IsRequestIMEI"]) ? ("[" + Convert.ToString(rData["InventoryIMEI"]) + "] ") : string.Empty) + Convert.ToString(rData["ProductName"]);
                                        rDataOther["ResolveNote"] = strResolveNoteOther;
                                        rDataOther["ResolveType"] = 1;
                                        //   Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndexOther, Color.LightBlue);
                                    }
                                }
                                else
                                {
                                    if (intDeltaRemainQuantityOther < 0)
                                    {
                                        strResolveNoteOther = intDeltaRemainQuantityOther.ToString().TrimStart('-') + ": nhập đổi " + (Convert.ToBoolean(rData["IsRequestIMEI"]) ? ("[" + Convert.ToString(rData["StockIMEI"]) + "] ") : string.Empty) + Convert.ToString(rData["ProductName"]);
                                        rDataOther["ResolveNote"] = strResolveNoteOther;
                                        rDataOther["ResolveType"] = 1;
                                        // Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndexOther, Color.LightBlue);
                                    }
                                    else
                                    {
                                        strResolveNoteOther = intDeltaRemainQuantityOther.ToString().TrimStart('-') + ": xuất đổi " + (Convert.ToBoolean(rData["IsRequestIMEI"]) ? ("[" + Convert.ToString(rData["InventoryIMEI"]) + "] ") : string.Empty) + Convert.ToString(rData["ProductName"]);
                                        rDataOther["ResolveNote"] = strResolveNoteOther;
                                        rDataOther["ResolveType"] = 1;
                                        //   Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexUnEvent, flexUnEvent.Rows.Fixed + intIndexOther, Color.LightBlue);
                                    }
                                }
                                #endregion

                                drProductChange = dtbData.Select("UnEventID = '" + Convert.ToString(rData["UnEventID"]) + "'");
                                if (drProductChange.Length > 0)
                                {
                                    dtbProductChange.ImportRow(drProductChange[0]);
                                }

                                drProductChange = dtbData.Select("UnEventID = '" + Convert.ToString(rDataOther["UnEventID"]) + "'");
                                if (drProductChange.Length > 0)
                                {
                                    dtbProductChange.ImportRow(drProductChange[0]);
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void chkOnlyUnEvent_CheckedChanged(object sender, EventArgs e)
        {
            grdViewProducts.CloseEditor();
            grdViewProducts.UpdateCurrentRow();
            dtbDataTable = tblDataFull.Clone();
            if (chkOnlyUnEvent.Checked)
            {

                DataRow[] drs = tblDataFull.Select("(ISCHANGESTATUS = 1) OR (ISCHANGESTATUS = 0 AND INPUTTINGQUANTITY = 0 AND ADJUSTUNEVENTQUANTITY = 0 AND DELTAADJUSTUNEVENTQUANTITY <> 0)");
                if (drs.Length > 0)
                    dtbDataTable = drs.CopyToDataTable();
            }
            else dtbDataTable = tblDataFull.Copy();
            grdCtlProducts.DataSource = dtbDataTable;
        }

    }
}
