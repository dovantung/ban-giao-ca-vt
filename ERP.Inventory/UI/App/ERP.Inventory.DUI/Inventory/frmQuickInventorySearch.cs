﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using Library.AppCore;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmQuickInventorySearch : Form
    {
        public frmQuickInventorySearch()
        {
            InitializeComponent();
        }

        private ERP.Inventory.PLC.Inventory.PLCQuickInventory objPLCQuickInventory = new PLC.Inventory.PLCQuickInventory();
        private DataTable dtbBrandAll = null;

        private void frmQuickInventorySearch_Load(object sender, EventArgs e)
        {
            LoadComboBox();
            //cboIsReviewed.SelectedIndex = 0;
            cboSearchType.SelectedIndex = 0;
            cmdSearch_Click(null, null);
        }

        private void LoadComboBox()
        {
            try
            {
                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                objStoreFilter.IsCheckPermission = true;
                objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.VIEWREPORT;
                cboStoreIDList.InitControl(false, objStoreFilter);
                cboMainGroup.InitControl(true, true);
                cboMainGroup.IsReturnAllWhenNotChoose = true;
                dtbBrandAll = Library.AppCore.DataSource.GetDataSource.GetBrand().Copy();
            }
            catch (Exception objEx)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp ComboBox");
                SystemErrorWS.Insert("Lỗi nạp ComboBox", objEx.ToString(), this.Name + " -> LoadComboBox", DUIInventory_Globals.ModuleName);
            }
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            cmdSearch.Enabled = false;
            if (dtpInventoryDateFrom.Value.Date > dtpInventoryDateTo.Value.Date)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Từ ngày kiểm kê phải nhỏ hơn hoặc bằng đến ngày kiểm kê");
                dtpInventoryDateFrom.Focus();
                cmdSearch.Enabled = true;
                return;
            }
            if (dtpLockStockTimeFrom.Value.Date > dtpLockStockTimeTo.Value.Date)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Từ ngày chốt tồn phải nhỏ hơn hoặc bằng đến ngày chốt tồn");
                dtpLockStockTimeFrom.Focus();
                cmdSearch.Enabled = true;
                return;
            }
            try
            {
                int intIsReviewed = -1;
                DataTable dtbDataInventory = objPLCQuickInventory.SearchData(cboStoreIDList.StoreID, cboMainGroup.MainGroupIDList, ucInventoryUser.UserName, ucReviewedUser.UserName, dtpInventoryDateFrom.Value, dtpInventoryDateTo.Value, dtpLockStockTimeFrom.Value, dtpLockStockTimeTo.Value, (chkDeleted.Checked ? 1 : 0), intIsReviewed, txtKeywords.Text.Trim(), cboSearchType.SelectedIndex);
                grdInventory.DataSource = dtbDataInventory;
                if (chkDeleted.Checked)
                {
                    colISDELETED.Visible = true;
                    colISDELETED.VisibleIndex = 98;
                    colDELETEDFULLNAME.Visible = true;
                    colDELETEDFULLNAME.VisibleIndex = 99;
                    colDELETEDDATE.Visible = true;
                    colDELETEDDATE.VisibleIndex = 100;

                }
                else
                {
                    colISDELETED.Visible = false;
                    colDELETEDFULLNAME.Visible = false;
                    colDELETEDDATE.Visible = false;
                }
            }
            catch (Exception objEx)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Lỗi tìm kiếm phiếu kiểm kê");
                SystemErrorWS.Insert("Lỗi tìm kiếm phiếu kiểm kê", objEx.ToString(), this.Name + " -> LoadComboBox", DUIInventory_Globals.ModuleName);
            }
            cmdSearch.Enabled = true;
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtInventoryID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                cmdSearch_Click(null, null);
            }
        }

        private void cmdExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdInventory);
            //Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexListInventory);
        }

        private void grvInventory_DoubleClick(object sender, EventArgs e)
        {
            if (grvInventory.FocusedRowHandle < 0)
                return;
            DataRowView drv = (DataRowView)grvInventory.GetFocusedRow();
            frmQuickInventory frm1 = new frmQuickInventory();
            frm1.strQuickInventoryID = Convert.ToString(drv.Row["QUICKINVENTORYID"]).Trim();
            frm1.dtmInventoryDate = Convert.ToDateTime(drv.Row["InventoryDate"]);
            frm1.strStoreName = drv.Row["STORENAME"].ToString();
            if (drv.Row["REVIEWEDFULLNAME"] != DBNull.Value)
                frm1.strReviewedFullName = Convert.ToString(drv.Row["REVIEWEDFULLNAME"]).Trim();
            if (drv.Row["INVENTORYFULLNAME"] != DBNull.Value)
                frm1.strInventoryFullName = Convert.ToString(drv.Row["INVENTORYFULLNAME"]).Trim();
            if (drv.Row["CREATEDFULLNAME"] != DBNull.Value)
                frm1.strCreatedFullName = Convert.ToString(drv.Row["CREATEDFULLNAME"]).Trim();
            frm1.dtbBrandAll = dtbBrandAll;
            frm1.ShowDialog();
        }
    }
}