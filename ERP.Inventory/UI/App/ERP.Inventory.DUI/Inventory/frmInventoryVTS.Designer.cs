﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmInventoryVTS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInventoryVTS));
            this.grdCtlProducts = new DevExpress.XtraGrid.GridControl();
            this.mnuFlexDetail = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemImportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemExportTemplateExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemDeleteProduct = new System.Windows.Forms.ToolStripMenuItem();
            this.grdViewProducts = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repTextImei = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtContentDelete = new System.Windows.Forms.TextBox();
            this.chkIsParent = new System.Windows.Forms.CheckBox();
            this.chkIsConnected = new System.Windows.Forms.CheckBox();
            this.cboInventoryTermID = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.cboSubGroupIDList = new Library.AppControl.ComboBoxControl.ComboBoxSubGroup();
            this.cboStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.cboMainGroupIDList = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.label1 = new System.Windows.Forms.Label();
            this.cboProductStateID = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtInventoryUser = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.chkIsDeleted = new System.Windows.Forms.CheckBox();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.chkIsReviewed = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpBeginInventoryTime = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpInventoryDate = new System.Windows.Forms.DateTimePicker();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtInventoryID = new System.Windows.Forms.TextBox();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnViewUpEvent = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdCtlProducts)).BeginInit();
            this.mnuFlexDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextImei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdCtlProducts
            // 
            this.grdCtlProducts.ContextMenuStrip = this.mnuFlexDetail;
            this.grdCtlProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCtlProducts.Location = new System.Drawing.Point(0, 169);
            this.grdCtlProducts.MainView = this.grdViewProducts;
            this.grdCtlProducts.Name = "grdCtlProducts";
            this.grdCtlProducts.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repTextImei,
            this.repQuantity,
            this.repNote});
            this.grdCtlProducts.Size = new System.Drawing.Size(1189, 377);
            this.grdCtlProducts.TabIndex = 81;
            this.grdCtlProducts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewProducts});
            // 
            // mnuFlexDetail
            // 
            this.mnuFlexDetail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemImportExcel,
            this.mnuItemExportTemplateExcel,
            this.mnuItemDeleteProduct});
            this.mnuFlexDetail.Name = "mnuFlexDetail";
            this.mnuFlexDetail.Size = new System.Drawing.Size(194, 70);
            this.mnuFlexDetail.Opening += new System.ComponentModel.CancelEventHandler(this.mnuFlexDetail_Opening);
            this.mnuFlexDetail.Opened += new System.EventHandler(this.mnuFlexDetail_Opened);
            // 
            // mnuItemImportExcel
            // 
            this.mnuItemImportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.mnuItemImportExcel.Name = "mnuItemImportExcel";
            this.mnuItemImportExcel.Size = new System.Drawing.Size(193, 22);
            this.mnuItemImportExcel.Text = "Nhập từ Excel";
            this.mnuItemImportExcel.Click += new System.EventHandler(this.mnuItemImportExcel_Click);
            // 
            // mnuItemExportTemplateExcel
            // 
            this.mnuItemExportTemplateExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportTemplateExcel.Name = "mnuItemExportTemplateExcel";
            this.mnuItemExportTemplateExcel.Size = new System.Drawing.Size(193, 22);
            this.mnuItemExportTemplateExcel.Text = "Xuất ra file mẫu";
            this.mnuItemExportTemplateExcel.Visible = false;
            this.mnuItemExportTemplateExcel.Click += new System.EventHandler(this.mnuItemExportTemplateExcel_Click);
            // 
            // mnuItemDeleteProduct
            // 
            this.mnuItemDeleteProduct.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.mnuItemDeleteProduct.Name = "mnuItemDeleteProduct";
            this.mnuItemDeleteProduct.ShortcutKeys = ((System.Windows.Forms.Keys)((((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.D)));
            this.mnuItemDeleteProduct.Size = new System.Drawing.Size(193, 22);
            this.mnuItemDeleteProduct.Text = "Hủy";
            this.mnuItemDeleteProduct.Click += new System.EventHandler(this.mnuItemDeleteProduct_Click);
            // 
            // grdViewProducts
            // 
            this.grdViewProducts.Appearance.FocusedRow.BorderColor = System.Drawing.Color.Red;
            this.grdViewProducts.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.grdViewProducts.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewProducts.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewProducts.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.Preview.Options.UseFont = true;
            this.grdViewProducts.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.Row.Options.UseFont = true;
            this.grdViewProducts.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewProducts.GridControl = this.grdCtlProducts;
            this.grdViewProducts.Name = "grdViewProducts";
            this.grdViewProducts.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewProducts.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewProducts.OptionsNavigation.UseTabKey = false;
            this.grdViewProducts.OptionsView.ShowAutoFilterRow = true;
            this.grdViewProducts.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewProducts.OptionsView.ShowFooter = true;
            this.grdViewProducts.OptionsView.ShowGroupPanel = false;
            this.grdViewProducts.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grdViewProducts_ShowingEditor);
            this.grdViewProducts.HiddenEditor += new System.EventHandler(this.grdViewProducts_HiddenEditor);
            // 
            // repTextImei
            // 
            this.repTextImei.AutoHeight = false;
            this.repTextImei.MaxLength = 50;
            this.repTextImei.Name = "repTextImei";
            // 
            // repQuantity
            // 
            this.repQuantity.AutoHeight = false;
            this.repQuantity.DisplayFormat.FormatString = "N0";
            this.repQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repQuantity.Mask.EditMask = "N0";
            this.repQuantity.Mask.UseMaskAsDisplayFormat = true;
            this.repQuantity.MaxValue = new decimal(new int[] {
            1215752191,
            23,
            0,
            0});
            this.repQuantity.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repQuantity.Name = "repQuantity";
            // 
            // repNote
            // 
            this.repNote.AutoHeight = false;
            this.repNote.MaxLength = 2000;
            this.repNote.Name = "repNote";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtContentDelete);
            this.panel1.Controls.Add(this.chkIsParent);
            this.panel1.Controls.Add(this.chkIsConnected);
            this.panel1.Controls.Add(this.cboInventoryTermID);
            this.panel1.Controls.Add(this.cboSubGroupIDList);
            this.panel1.Controls.Add(this.cboStoreID);
            this.panel1.Controls.Add(this.cboMainGroupIDList);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cboProductStateID);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtInventoryUser);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.chkIsDeleted);
            this.panel1.Controls.Add(this.lblBarcode);
            this.panel1.Controls.Add(this.chkIsReviewed);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.dtpBeginInventoryTime);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.dtpInventoryDate);
            this.panel1.Controls.Add(this.txtContent);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.txtInventoryID);
            this.panel1.Controls.Add(this.txtBarcode);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1189, 169);
            this.panel1.TabIndex = 80;
            // 
            // txtContentDelete
            // 
            this.txtContentDelete.Location = new System.Drawing.Point(817, 90);
            this.txtContentDelete.MaxLength = 2000;
            this.txtContentDelete.Multiline = true;
            this.txtContentDelete.Name = "txtContentDelete";
            this.txtContentDelete.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContentDelete.Size = new System.Drawing.Size(233, 69);
            this.txtContentDelete.TabIndex = 86;
            // 
            // chkIsParent
            // 
            this.chkIsParent.AutoSize = true;
            this.chkIsParent.Enabled = false;
            this.chkIsParent.Location = new System.Drawing.Point(659, 109);
            this.chkIsParent.Name = "chkIsParent";
            this.chkIsParent.Size = new System.Drawing.Size(152, 20);
            this.chkIsParent.TabIndex = 85;
            this.chkIsParent.Text = "Là phiếu KK tổng hợp";
            this.chkIsParent.UseVisualStyleBackColor = true;
            this.chkIsParent.Visible = false;
            // 
            // chkIsConnected
            // 
            this.chkIsConnected.AutoSize = true;
            this.chkIsConnected.Enabled = false;
            this.chkIsConnected.Location = new System.Drawing.Point(731, 64);
            this.chkIsConnected.Name = "chkIsConnected";
            this.chkIsConnected.Size = new System.Drawing.Size(101, 20);
            this.chkIsConnected.TabIndex = 84;
            this.chkIsConnected.Text = "Đã nối phiếu";
            this.chkIsConnected.UseVisualStyleBackColor = true;
            // 
            // cboInventoryTermID
            // 
            this.cboInventoryTermID.Enabled = false;
            this.cboInventoryTermID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInventoryTermID.Location = new System.Drawing.Point(409, 9);
            this.cboInventoryTermID.Margin = new System.Windows.Forms.Padding(0);
            this.cboInventoryTermID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInventoryTermID.Name = "cboInventoryTermID";
            this.cboInventoryTermID.Size = new System.Drawing.Size(223, 24);
            this.cboInventoryTermID.TabIndex = 81;
            // 
            // cboSubGroupIDList
            // 
            this.cboSubGroupIDList.Enabled = false;
            this.cboSubGroupIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSubGroupIDList.Location = new System.Drawing.Point(731, 36);
            this.cboSubGroupIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboSubGroupIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboSubGroupIDList.Name = "cboSubGroupIDList";
            this.cboSubGroupIDList.Size = new System.Drawing.Size(319, 24);
            this.cboSubGroupIDList.TabIndex = 83;
            // 
            // cboStoreID
            // 
            this.cboStoreID.Enabled = false;
            this.cboStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreID.Location = new System.Drawing.Point(92, 36);
            this.cboStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreID.Name = "cboStoreID";
            this.cboStoreID.Size = new System.Drawing.Size(223, 24);
            this.cboStoreID.TabIndex = 81;
            // 
            // cboMainGroupIDList
            // 
            this.cboMainGroupIDList.Enabled = false;
            this.cboMainGroupIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroupIDList.Location = new System.Drawing.Point(409, 36);
            this.cboMainGroupIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroupIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroupIDList.Name = "cboMainGroupIDList";
            this.cboMainGroupIDList.Size = new System.Drawing.Size(223, 24);
            this.cboMainGroupIDList.TabIndex = 82;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 16);
            this.label1.TabIndex = 52;
            this.label1.Text = "Mã phiếu:";
            // 
            // cboProductStateID
            // 
            this.cboProductStateID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProductStateID.DropDownWidth = 250;
            this.cboProductStateID.Enabled = false;
            this.cboProductStateID.Location = new System.Drawing.Point(92, 63);
            this.cboProductStateID.Name = "cboProductStateID";
            this.cboProductStateID.Size = new System.Drawing.Size(223, 24);
            this.cboProductStateID.TabIndex = 79;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(648, 40);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 16);
            this.label17.TabIndex = 74;
            this.label17.Text = "Nhóm hàng:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(323, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 16);
            this.label9.TabIndex = 61;
            this.label9.Text = "NV kiểm kê:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 16);
            this.label7.TabIndex = 59;
            this.label7.Text = "Loại hàng:";
            // 
            // txtInventoryUser
            // 
            this.txtInventoryUser.BackColor = System.Drawing.SystemColors.Info;
            this.txtInventoryUser.Location = new System.Drawing.Point(409, 63);
            this.txtInventoryUser.Name = "txtInventoryUser";
            this.txtInventoryUser.ReadOnly = true;
            this.txtInventoryUser.Size = new System.Drawing.Size(223, 22);
            this.txtInventoryUser.TabIndex = 75;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 16);
            this.label10.TabIndex = 62;
            this.label10.Text = "Nội dung:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(832, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 16);
            this.label6.TabIndex = 58;
            this.label6.Text = "TG bắt đầu:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(648, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 16);
            this.label5.TabIndex = 57;
            this.label5.Text = "Ngày KK:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(648, 65);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 16);
            this.label15.TabIndex = 57;
            this.label15.Text = "Trạng thái:";
            // 
            // chkIsDeleted
            // 
            this.chkIsDeleted.AutoSize = true;
            this.chkIsDeleted.Enabled = false;
            this.chkIsDeleted.Location = new System.Drawing.Point(731, 90);
            this.chkIsDeleted.Name = "chkIsDeleted";
            this.chkIsDeleted.Size = new System.Drawing.Size(68, 20);
            this.chkIsDeleted.TabIndex = 70;
            this.chkIsDeleted.Text = "Đã hủy";
            this.chkIsDeleted.UseVisualStyleBackColor = true;
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.Location = new System.Drawing.Point(5, 138);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(63, 16);
            this.lblBarcode.TabIndex = 54;
            this.lblBarcode.Text = "Barcode:";
            // 
            // chkIsReviewed
            // 
            this.chkIsReviewed.AutoSize = true;
            this.chkIsReviewed.Enabled = false;
            this.chkIsReviewed.Location = new System.Drawing.Point(681, 134);
            this.chkIsReviewed.Name = "chkIsReviewed";
            this.chkIsReviewed.Size = new System.Drawing.Size(80, 20);
            this.chkIsReviewed.TabIndex = 70;
            this.chkIsReviewed.Text = "Đã duyệt";
            this.chkIsReviewed.UseVisualStyleBackColor = true;
            this.chkIsReviewed.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(323, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 16);
            this.label4.TabIndex = 56;
            this.label4.Text = "Kỳ kiểm kê:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(323, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 16);
            this.label3.TabIndex = 55;
            this.label3.Text = "Ngành hàng:";
            // 
            // dtpBeginInventoryTime
            // 
            this.dtpBeginInventoryTime.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpBeginInventoryTime.Enabled = false;
            this.dtpBeginInventoryTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBeginInventoryTime.Location = new System.Drawing.Point(915, 11);
            this.dtpBeginInventoryTime.Name = "dtpBeginInventoryTime";
            this.dtpBeginInventoryTime.Size = new System.Drawing.Size(135, 22);
            this.dtpBeginInventoryTime.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 16);
            this.label2.TabIndex = 53;
            this.label2.Text = "Kho kiểm kê:";
            // 
            // dtpInventoryDate
            // 
            this.dtpInventoryDate.CustomFormat = "dd/MM/yyyy";
            this.dtpInventoryDate.Enabled = false;
            this.dtpInventoryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInventoryDate.Location = new System.Drawing.Point(731, 11);
            this.dtpInventoryDate.Name = "dtpInventoryDate";
            this.dtpInventoryDate.Size = new System.Drawing.Size(101, 22);
            this.dtpInventoryDate.TabIndex = 4;
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(92, 91);
            this.txtContent.MaxLength = 2000;
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContent.Size = new System.Drawing.Size(540, 38);
            this.txtContent.TabIndex = 10;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(349, 135);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(36, 24);
            this.btnSearch.TabIndex = 14;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtInventoryID
            // 
            this.txtInventoryID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInventoryID.Location = new System.Drawing.Point(92, 11);
            this.txtInventoryID.Name = "txtInventoryID";
            this.txtInventoryID.ReadOnly = true;
            this.txtInventoryID.Size = new System.Drawing.Size(223, 22);
            this.txtInventoryID.TabIndex = 0;
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(92, 136);
            this.txtBarcode.MaxLength = 50;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(256, 22);
            this.txtBarcode.TabIndex = 13;
            this.txtBarcode.Enter += new System.EventHandler(this.txtBarcode_Enter);
            this.txtBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarcode_KeyPress);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnViewUpEvent);
            this.panelControl1.Controls.Add(this.btnDelete);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 546);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1189, 34);
            this.panelControl1.TabIndex = 82;
            // 
            // btnViewUpEvent
            // 
            this.btnViewUpEvent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewUpEvent.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.btnViewUpEvent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnViewUpEvent.Location = new System.Drawing.Point(844, 4);
            this.btnViewUpEvent.Name = "btnViewUpEvent";
            this.btnViewUpEvent.Size = new System.Drawing.Size(132, 25);
            this.btnViewUpEvent.TabIndex = 70;
            this.btnViewUpEvent.Text = "       Xem chênh lệch";
            this.btnViewUpEvent.Click += new System.EventHandler(this.btnViewUpEvent_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Enabled = false;
            this.btnDelete.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(1084, 4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(94, 25);
            this.btnDelete.TabIndex = 16;
            this.btnDelete.Text = "      Hủy";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(982, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(96, 25);
            this.btnSave.TabIndex = 19;
            this.btnSave.Text = "       Cập nhật";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmInventoryVTS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1189, 580);
            this.Controls.Add(this.grdCtlProducts);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmInventoryVTS";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kiểm kê";
            this.Load += new System.EventHandler(this.frmInventoryVTS_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdCtlProducts)).EndInit();
            this.mnuFlexDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextImei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cboProductStateID;
        private System.Windows.Forms.TextBox txtInventoryUser;
        private System.Windows.Forms.CheckBox chkIsDeleted;
        private System.Windows.Forms.CheckBox chkIsReviewed;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DateTimePicker dtpInventoryDate;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.TextBox txtInventoryID;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpBeginInventoryTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblBarcode;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel1;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStoreID;
        private Library.AppControl.ComboBoxControl.ComboBoxSubGroup cboSubGroupIDList;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroupIDList;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboInventoryTermID;
        private DevExpress.XtraGrid.GridControl grdCtlProducts;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewProducts;
        private System.Windows.Forms.ContextMenuStrip mnuFlexDetail;
        private System.Windows.Forms.ToolStripMenuItem mnuItemImportExcel;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportTemplateExcel;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDeleteProduct;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.CheckBox chkIsParent;
        private System.Windows.Forms.CheckBox chkIsConnected;
        private System.Windows.Forms.TextBox txtContentDelete;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTextImei;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repQuantity;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repNote;
        private System.Windows.Forms.Button btnViewUpEvent;
    }
}