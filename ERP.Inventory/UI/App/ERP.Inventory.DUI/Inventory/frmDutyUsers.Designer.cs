﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmDutyUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDutyUsers));
            this.flexDutyUsers = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.mnuUsers = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemAddUser = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemDeleteUser = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdYes = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.flexDutyUsers)).BeginInit();
            this.mnuUsers.SuspendLayout();
            this.SuspendLayout();
            // 
            // flexDutyUsers
            // 
            this.flexDutyUsers.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexDutyUsers.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexDutyUsers.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexDutyUsers.AutoClipboard = true;
            this.flexDutyUsers.ColumnInfo = "10,1,0,0,0,105,Columns:0{Width:20;}\t";
            this.flexDutyUsers.ContextMenuStrip = this.mnuUsers;
            this.flexDutyUsers.Location = new System.Drawing.Point(3, 45);
            this.flexDutyUsers.Name = "flexDutyUsers";
            this.flexDutyUsers.Rows.Count = 1;
            this.flexDutyUsers.Rows.DefaultSize = 21;
            this.flexDutyUsers.Size = new System.Drawing.Size(629, 221);
            this.flexDutyUsers.StyleInfo = resources.GetString("flexDutyUsers.StyleInfo");
            this.flexDutyUsers.TabIndex = 56;
            this.flexDutyUsers.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexDutyUsers_AfterEdit);
            // 
            // mnuUsers
            // 
            this.mnuUsers.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemAddUser,
            this.mnuItemDeleteUser});
            this.mnuUsers.Name = "mnuUsers";
            this.mnuUsers.Size = new System.Drawing.Size(161, 48);
            this.mnuUsers.Opening += new System.ComponentModel.CancelEventHandler(this.mnuUsers_Opening);
            // 
            // mnuItemAddUser
            // 
            this.mnuItemAddUser.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.mnuItemAddUser.Name = "mnuItemAddUser";
            this.mnuItemAddUser.Size = new System.Drawing.Size(160, 22);
            this.mnuItemAddUser.Text = "Thêm nhân viên";
            this.mnuItemAddUser.Click += new System.EventHandler(this.mnuItemAddUser_Click);
            // 
            // mnuItemDeleteUser
            // 
            this.mnuItemDeleteUser.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.mnuItemDeleteUser.Name = "mnuItemDeleteUser";
            this.mnuItemDeleteUser.Size = new System.Drawing.Size(160, 22);
            this.mnuItemDeleteUser.Text = "Xóa nhân viên";
            this.mnuItemDeleteUser.Click += new System.EventHandler(this.mnuItemDeleteUser_Click);
            // 
            // cmdYes
            // 
            this.cmdYes.Image = ((System.Drawing.Image)(resources.GetObject("cmdYes.Image")));
            this.cmdYes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdYes.Location = new System.Drawing.Point(547, 13);
            this.cmdYes.Margin = new System.Windows.Forms.Padding(4);
            this.cmdYes.Name = "cmdYes";
            this.cmdYes.Size = new System.Drawing.Size(84, 25);
            this.cmdYes.TabIndex = 59;
            this.cmdYes.Text = "   Chọn";
            this.cmdYes.Click += new System.EventHandler(this.cmdYes_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(179, 16);
            this.label11.TabIndex = 62;
            this.label11.Text = "Danh sách nhân viên truy thu:";
            // 
            // frmDutyUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 280);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.flexDutyUsers);
            this.Controls.Add(this.cmdYes);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(660, 318);
            this.Name = "frmDutyUsers";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Danh sách nhân viên truy thu";
            this.Load += new System.EventHandler(this.frmDutyUsers_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flexDutyUsers)).EndInit();
            this.mnuUsers.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private C1.Win.C1FlexGrid.C1FlexGrid flexDutyUsers;
        private System.Windows.Forms.Button cmdYes;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ContextMenuStrip mnuUsers;
        private System.Windows.Forms.ToolStripMenuItem mnuItemAddUser;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDeleteUser;
    }
}