﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.LoadControls;
using System.Collections;
using Library.AppCore;
using DevExpress.XtraEditors;
using System.IO;
using DevExpress.XtraEditors.Repository;
using ERP.Inventory.PLC.WSPriceProtect;

namespace ERP.Inventory.DUI.Inventory
{
    /// <summary>
    /// Created by : Nguyễn Văn Tài
    /// Created date : 03/07/2017
    /// Description : Màn hình hóa đơn hỗ trợ giá hàng tồn.
    /// 
    /// LÊ VĂN ĐÔNG - 18/08/2017 : Đập đi xây lại
    /// </summary>
    public partial class frmInventoryPrice : Form
    {
        #region Enum
        public enum StateForm
        {
            Insert = 1,
            Update = 2,
            Delete = 3,
            View = 0
        }

        #endregion

        #region Permission
        private string strPermission_Add = "VAT_PINVOICE_ADD";
        private string strPermission_Edit = "VAT_PINVOICE_EDIT";
        private string strPermission_Delete = "VAT_PINVOICE_DELETE";
        private string strPermission_View = "VAT_PINVOICE_VIEW";
        private string strPermission_DeleteSearch = "VAT_PINVOICE_ISDELETED_SRH";
        private string strPermission_EditMoney = "ACC_VATACCOUNTANTMANAGEMET_EDIT";
        private string strPermission_DeleteInvoice = "ACC_VATACCOUNTANTMANAGEMET_DELETE";
        #endregion

        #region Variables
        StateForm objStateForm = StateForm.Insert;
        public bool bolIsLoadForm = false;
        public bool bolRefresh = false;
        string strVATPInvoiceID = string.Empty;
        bool IsLoadDataFormLoad = false;


        private PLC.PM.PLCPriceProtect objPLCPriceProtect = new PLC.PM.PLCPriceProtect();
        private string strProductID = string.Empty;

        object[] objKeywords = null;
        private DialogResult drResult = DialogResult.Cancel;

        DataTable dtbDataProductService = null;
        DataTable dtbDataAttachment = null;
        DataTable dtbDataAttachment_Del = null;

        private DataTable dtbGroupService = null;
        private DataTable dtbInvoiceTransactionType = null;
        private DataTable dtbVatType = null;

        string strPriceProtectIdList = string.Empty;
        public string PriceProtectIdList
        {
            get { return strPriceProtectIdList; }
            set { strPriceProtectIdList = value; }
        }

        string strPriceProtectNo = string.Empty;
        public string PriceProtectNo
        {
            get { return strPriceProtectNo; }
            set { strPriceProtectNo = value; }
        }

        //Kho tạo mặc định
        int intStoreIDSelect = -1;
        public int StoreIDSelect
        {
            get { return intStoreIDSelect; }
            set { intStoreIDSelect = value; }
        }

        // Chi nhanh mặc định tạo hóa đơn
        int intBranchIDSelect = -1;
        public int BranchIDSelect
        {
            get { return intBranchIDSelect; }
            set { intBranchIDSelect = value; }
        }

        // loại nghiệp hóa đơn mặc định
        int intInvoiceTransactionTypeIDSelect = -1;
        public int InvoiceTransactionTypeIDSelect
        {
            get { return intInvoiceTransactionTypeIDSelect; }
            set { intInvoiceTransactionTypeIDSelect = value; }
        }

        // Hình thức thanh toán mặc định
        int intPayableTypeIDSelect = -1;
        public int PayableTypeIDSelect
        {
            get { return intPayableTypeIDSelect; }
            set { intPayableTypeIDSelect = value; }
        }

        private int intFormTypeID = -1;
        public int FormTypeID
        {
            get { return intFormTypeID; }
            set { intFormTypeID = value; }
        }


        private int intTypeID = -1;
        public int TypeID
        {
            get { return intTypeID; }
            set { intTypeID = value; }
        }

        //Hóa đơn giá trị gia tăng khách hàng tổ chức/ công ty
        bool bolIsForCompany = false;
        public bool IsForCompany
        {
            get { return bolIsForCompany; }
            set { bolIsForCompany = value; }
        }



        #region Thông tin khách hàng
        ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = null;
        ERP.MasterData.PLC.MD.PLCCustomer objPLCCustomer = new ERP.MasterData.PLC.MD.PLCCustomer();
        int intCustomerID = -1;
        public int CustomerID
        {
            get { return intCustomerID; }
            set { intCustomerID = value; }
        }

        string strTaxCustomerName = string.Empty;
        public string TaxCustomerName
        {
            get { return strTaxCustomerName; }
            set { strTaxCustomerName = value; }
        }

        string strTaxCustomerAddress = string.Empty;
        public string TaxCustomerAddress
        {
            get { return TaxCustomerAddress; }
            set { TaxCustomerAddress = value; }
        }

        string strTaxCustomerTaxCode = string.Empty;
        public string TaxCustomerTaxCode
        {
            get { return strTaxCustomerTaxCode; }
            set { strTaxCustomerTaxCode = value; }
        }

        string strTaxCustomerSalerName = string.Empty;
        public string TaxCustomerSalerName
        {
            get { return strTaxCustomerSalerName; }
            set { strTaxCustomerSalerName = value; }
        }

        string strTaxCustomerDescription = string.Empty;
        public string TaxCustomerDescription
        {
            get { return strTaxCustomerDescription; }
            set { strTaxCustomerDescription = value; }
        }

        string strTaxCustomerPhone = string.Empty;
        public string TaxCustomerPhone
        {
            get { return strTaxCustomerPhone; }
            set { strTaxCustomerPhone = value; }
        }
        #endregion

        private int intTransactionGroupId = -1;
        public int TransactionGroupId
        {
            get { return intTransactionGroupId; }
            set { intTransactionGroupId = value; }
        }

        private bool bolIsManager = false;
        public bool IsManager
        {
            get { return bolIsManager; }
            set { bolIsManager = value; }
        }
        #endregion

        #region Constructors
        public frmInventoryPrice(StateForm objStateForm) : base()
        {
            this.objStateForm = objStateForm;
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;

            CreateDataProductService();
            CreateDataAttachment();
        }

        public frmInventoryPrice()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;

            CreateDataProductService();
            CreateDataAttachment();
        }
        #endregion

        #region Functions
        private void frmInventoryPrice_Load(object sender, EventArgs e)
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewDataAttachment);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewDataProductService);
            if (!GetParameterForm())
            {
                this.Close();
            }

            LoadCombobox();
            InitializeGridDataProductService();
            InitializeGridDataAttachment();
            InitializeDataControl();
            InitializeStateForm();

            grdDataProductService.DataSource = dtbDataProductService;
            grdDataAttachment.DataSource = dtbDataAttachment;

            if (IsLoadDataFormLoad)
                LoadVATInformation(strVATPInvoiceID);

            bolIsLoadForm = true;
        }

        void InitializeDataControl()
        {
            DateTime dteServer = Globals.GetServerDateTime();
            dtmCreateDate.DateTime = dteServer;
            dtmDueDate.DateTime = dteServer;
            dtmInvoiceDate.DateTime = dteServer;

            if (intStoreIDSelect == -1)
                intStoreIDSelect = SystemConfig.intDefaultStoreID;
            cboStore.SetValue(intStoreIDSelect);

            cboBranch.SetValue(intBranchIDSelect);
            cboBranch_SelectionChangeCommitted(null, null);

            cboInvoiceTransType.SetValue(intInvoiceTransactionTypeIDSelect);
            cboPayableType.SetValue(intPayableTypeIDSelect);
            if (intBranchIDSelect == -1)
                cboBranch.Focus();
            else if (intInvoiceTransactionTypeIDSelect == -1)
                cboInvoiceTransType.Focus();
            else if (intStoreIDSelect == -1)
                cboStore.Focus();
            else
                txtDenominator.Focus();
            SetCustomerInforToControl();
        }

        private void LoadCombobox()
        {
            // Chi Nhanh
            DataTable dtbBranch = Library.AppCore.DataSource.GetDataSource.GetBranch();
            cboBranch.InitControl(false, dtbBranch, "BRANCHID", "BRANCHNAME", "--Chọn chi nhánh--");

            // Danh Sach Kho
            DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore();
            cboStore.InitControl(false, dtbStore, "STOREID", "STORENAME", "--Chọn kho--");
            cboStore.SetValue(SystemConfig.intDefaultStoreID);

            //hình thức thanh toán
            DataTable dtbPayableType = Library.AppCore.DataSource.GetDataSource.GetPayableType();
            cboPayableType.InitControl(false, dtbPayableType, "PAYABLETYPEID", "PRINTVATINVOICENAME", "--Chọn hình thức thanh toán--");

            if (objStateForm == StateForm.Insert)
                LoadInvoiceTranstype();

            // Thue suất
            dtbVatType = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_INVOICEVAT_VATTYPE");
            if (dtbVatType != null)
            {
                cboVAT.DataSource = dtbVatType.DefaultView.ToTable();
            }

            // Tỷ giá
            Library.AppCore.LoadControls.SetDataSource.SetCurrencyUnit(this, cboCurrencyUnit, true);
            cboCurrencyUnit_SelectionChangeCommitted(null, null);

            dtbGroupService = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_GROUPSERVICE_CACHE");
            if (dtbGroupService != null)
            {
                DataTable dtGroupServiceTemp = dtbGroupService.DefaultView.ToTable();
                dtGroupServiceTemp.Columns["GROUPSERVICEID"].Caption = "Mã nhóm";
                dtGroupServiceTemp.Columns["GROUPSERVICENAME"].Caption = "Tên nhóm";
                repLockUpServiceGroup.DataSource = dtGroupServiceTemp.DefaultView.ToTable();
                repLockUpServiceGroup.ValueMember = "GROUPSERVICEID";
                repLockUpServiceGroup.DisplayMember = "GROUPSERVICENAME";
            }

            //Số biên bản
            DataTable dtbInvoiceNo = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_PRICEPROTECT_GETINVOICENO", objKeywords);
            if (dtbInvoiceNo != null && dtbInvoiceNo.Rows.Count > 0)
            {
                foreach (DataRow item in dtbInvoiceNo.Rows)
                {
                    if (strPriceProtectNo == string.Empty)
                        strPriceProtectNo = item[0].ToString();
                    else
                        strPriceProtectNo = strPriceProtectNo + ";" + item[0].ToString();
                }
            }
            txtPriceProtectNo.Text = strPriceProtectNo;
        }

        private void LoadInvoiceTranstype()
        {
            dtbInvoiceTransactionType = Library.AppCore.DataSource.GetDataSource.GetInvoiceTransactionType();
            if (dtbInvoiceTransactionType != null)
            {
                DataRow[] row = dtbInvoiceTransactionType.Select("TRANSACTIONGROUPID=" + intTransactionGroupId);
                if (row.Count() > 0)
                    cboInvoiceTransType.InitControl(false, row.CopyToDataTable(), "INVOICETRANSTYPEID", "INVOICETRANSTYPENAME", "--Chọn loại nghiệp vụ--");
            }
        }

        void CreateDataProductService()
        {
            dtbDataProductService = new DataTable();
            dtbDataProductService.Columns.Add("VATPINVOICEID", typeof(string));
            dtbDataProductService.Columns.Add("VATPINVOICEDTID", typeof(string));
            dtbDataProductService.Columns.Add("PRICEPROTECTNO", typeof(string));
            dtbDataProductService.Columns.Add("PRICEPROTECTID", typeof(string));
            dtbDataProductService.Columns.Add("PRICEPROTECTDETAILIDLIST", typeof(string));
            dtbDataProductService.Columns.Add("INPUTVOUCHERDETAILID", typeof(string));
            dtbDataProductService.Columns.Add("ORDERDETAILID", typeof(string));
            dtbDataProductService.Columns.Add("CREATEDDATE", typeof(object));
            dtbDataProductService.Columns.Add("CUSTOMERID", typeof(Int32));
            dtbDataProductService.Columns["CUSTOMERID"].DefaultValue = -1;

            dtbDataProductService.Columns.Add("CUSTOMERNAME", typeof(string));
            dtbDataProductService.Columns.Add("CUSTOMERADDRESS", typeof(string));
            dtbDataProductService.Columns.Add("CUSTOMERPHONE", typeof(string));
            dtbDataProductService.Columns.Add("CUSTOMERTAXID", typeof(string));

            dtbDataProductService.Columns.Add("PRODUCTID", typeof(string));
            dtbDataProductService.Columns.Add("PRODUCTNAME", typeof(string));

            dtbDataProductService.Columns.Add("APPLYMISAPRODUCTID", typeof(string));
            dtbDataProductService.Columns.Add("MISAPRODUCTMISANAME", typeof(string));
            dtbDataProductService.Columns.Add("MISAPRODUCTNAMECODE", typeof(string));

            dtbDataProductService.Columns.Add("QUANTITYUNITID", typeof(Int32));
            dtbDataProductService.Columns["QUANTITYUNITID"].DefaultValue = -1;

            dtbDataProductService.Columns.Add("QUANTITY", typeof(decimal));
            dtbDataProductService.Columns["QUANTITY"].DefaultValue = 0;

            dtbDataProductService.Columns.Add("QUANTITYUNITNAME", typeof(string));

            dtbDataProductService.Columns.Add("INPUTPRICE", typeof(decimal));
            dtbDataProductService.Columns["INPUTPRICE"].DefaultValue = 0;

            dtbDataProductService.Columns.Add("VAT", typeof(decimal));
            dtbDataProductService.Columns["VAT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("VATPERCENT", typeof(decimal));
            dtbDataProductService.Columns["VATPERCENT"].DefaultValue = 0;

            dtbDataProductService.Columns.Add("VATAMOUNT", typeof(decimal));
            dtbDataProductService.Columns["VATAMOUNT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("AMOUNTWITHVAT", typeof(decimal));
            dtbDataProductService.Columns["AMOUNTWITHVAT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("AMOUNTINVOICE", typeof(decimal));
            dtbDataProductService.Columns["AMOUNTINVOICE"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("TOTALAMOUNTWITHOUTVAT", typeof(decimal));
            dtbDataProductService.Columns["TOTALAMOUNTWITHOUTVAT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("TOTALAMOUNT", typeof(decimal));
            dtbDataProductService.Columns["TOTALAMOUNT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("TOTALAMOUNTINVOICEWITHOUTVAT", typeof(decimal));
            dtbDataProductService.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("INVOICEVAT", typeof(decimal));
            dtbDataProductService.Columns["INVOICEVAT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("INVOICEAMOUNT", typeof(decimal));
            dtbDataProductService.Columns["INVOICEAMOUNT"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("TOTALAMOUNTINVOICE", typeof(decimal));
            dtbDataProductService.Columns["TOTALAMOUNTINVOICE"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("UNEVEN", typeof(decimal));
            dtbDataProductService.Columns["UNEVEN"].DefaultValue = 0;

            dtbDataProductService.Columns.Add("CREATEUSER", typeof(string));
            dtbDataProductService.Columns.Add("CREATEDATE", typeof(object));
            dtbDataProductService.Columns.Add("UPDATEUSER", typeof(string));
            dtbDataProductService.Columns.Add("UPDATEDATE", typeof(object));

            dtbDataProductService.Columns.Add("ISDELETE", typeof(Int32));
            dtbDataProductService.Columns["ISDELETE"].DefaultValue = 0;

            dtbDataProductService.Columns.Add("DELETEUSER", typeof(string));
            dtbDataProductService.Columns.Add("DELETEDATE", typeof(object));
            dtbDataProductService.Columns.Add("DELETEREASON", typeof(string));

            dtbDataProductService.Columns.Add("ISNOTAX", typeof(Int32));
            dtbDataProductService.Columns["ISNOTAX"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("ISCHKCHANGE", typeof(Int32));
            dtbDataProductService.Columns["ISCHKCHANGE"].DefaultValue = 0;
            dtbDataProductService.Columns.Add("ISPROMOTIONPRODUCTMISA", typeof(Int32));
            dtbDataProductService.Columns["ISPROMOTIONPRODUCTMISA"].DefaultValue = 0;

            dtbDataProductService.Columns.Add("INVOICENO", typeof(string));
            dtbDataProductService.Columns.Add("INPUTVOUCHERINFO", typeof(string));
            dtbDataProductService.Columns.Add("PROMOTIONDETAILID", typeof(string));
            dtbDataProductService.Columns.Add("PROMOTIONNAME", typeof(string));

            dtbDataProductService.Columns.Add("VATINVOICENOMAIN", typeof(string));
            dtbDataProductService.Columns.Add("VATINVOICEIDMAIN", typeof(string));
            dtbDataProductService.Columns.Add("GROUPSERVICEID", typeof(Int32));
        }

        void InitializeGridDataProductService()
        {
            List<string> colList = new List<string>();
            foreach (DataColumn cl in dtbDataProductService.Columns)
            {
                string strColumnName = (cl.ColumnName ?? "").ToString().Trim();
                if (!colList.Contains(strColumnName))
                    colList.Add(strColumnName);
            }

            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdDataProductService, colList.ToArray());
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewDataProductService);

            grdViewDataProductService.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewDataProductService.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            grdViewDataProductService.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            grdViewDataProductService.OptionsView.ShowAutoFilterRow = true;
            grdViewDataProductService.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            grdViewDataProductService.OptionsView.ShowFooter = true;
            grdViewDataProductService.OptionsView.ShowGroupPanel = false;
            for (int i = 0; i < grdViewDataProductService.Columns.Count; i++)
            {
                grdViewDataProductService.Columns[i].Visible = false;
                grdViewDataProductService.Columns[i].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewDataProductService.Columns[i].AppearanceHeader.Options.UseTextOptions = true;
                grdViewDataProductService.Columns[i].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewDataProductService.Columns[i].AppearanceCell.BackColor = SystemColors.Info;
                grdViewDataProductService.Columns[i].AppearanceCell.Options.UseTextOptions = true;
                grdViewDataProductService.Columns[i].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewDataProductService.Columns[i].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            }

            RepositoryItemDateEdit repDate = new RepositoryItemDateEdit();
            repDate.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            repDate.Mask.UseMaskAsDisplayFormat = true;

            RepositoryItemSpinEdit repQuantity = new RepositoryItemSpinEdit();
            repQuantity.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            repQuantity.MinValue = 0;
            repQuantity.MaxValue = 9999999;
            repQuantity.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Custom;
            repQuantity.Mask.EditMask = "#,##0.####;";
            repQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            repQuantity.DisplayFormat.FormatString = "#,##0.####;";
            repQuantity.Mask.UseMaskAsDisplayFormat = true;
            repQuantity.Buttons.Clear();

            RepositoryItemSpinEdit repQuantityCus = new RepositoryItemSpinEdit();
            repQuantityCus.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            repQuantityCus.MinValue = 0;
            repQuantityCus.MaxValue = 9999999;
            repQuantityCus.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            repQuantityCus.Mask.EditMask = "#,##0.####;";
            repQuantityCus.Mask.UseMaskAsDisplayFormat = true;
            repQuantityCus.Buttons.Clear();

            RepositoryItemSpinEdit repAmount = new RepositoryItemSpinEdit();
            repAmount.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            repAmount.MinValue = -9999999999;
            repAmount.MaxValue = 9999999999;
            repAmount.Mask.EditMask = "N0";
            repAmount.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            repAmount.Mask.UseMaskAsDisplayFormat = true;
            repAmount.Buttons.Clear();

            grdViewDataProductService.Columns["PRICEPROTECTNO"].Caption = "Số biên bản";
            grdViewDataProductService.Columns["PRICEPROTECTNO"].Visible = true;
            grdViewDataProductService.Columns["PRICEPROTECTNO"].VisibleIndex = 1;
            grdViewDataProductService.Columns["PRICEPROTECTNO"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["PRICEPROTECTNO"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            grdViewDataProductService.Columns["PRICEPROTECTNO"].Width = 120;

            grdViewDataProductService.Columns["PRICEPROTECTID"].Caption = "Mã phiếu hỗ trợ";
            grdViewDataProductService.Columns["PRICEPROTECTID"].VisibleIndex = 2;
            grdViewDataProductService.Columns["PRICEPROTECTID"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["PRICEPROTECTID"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            grdViewDataProductService.Columns["PRICEPROTECTID"].Width = 120;

            grdViewDataProductService.Columns["MISAPRODUCTNAMECODE"].Caption = "Tên hàng hóa, dịch vụ";
            grdViewDataProductService.Columns["MISAPRODUCTNAMECODE"].Visible = true;
            grdViewDataProductService.Columns["MISAPRODUCTNAMECODE"].VisibleIndex = 3;
            grdViewDataProductService.Columns["MISAPRODUCTNAMECODE"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["MISAPRODUCTNAMECODE"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            grdViewDataProductService.Columns["MISAPRODUCTNAMECODE"].Width = 200;

            grdViewDataProductService.Columns["QUANTITY"].Caption = "SL";
            grdViewDataProductService.Columns["QUANTITY"].Visible = true;
            grdViewDataProductService.Columns["QUANTITY"].VisibleIndex = 4;
            grdViewDataProductService.Columns["QUANTITY"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["QUANTITY"].ColumnEdit = repQuantity;
            grdViewDataProductService.Columns["QUANTITY"].Width = 60;

            grdViewDataProductService.Columns["QUANTITYUNITNAME"].Caption = "ĐVT";
            grdViewDataProductService.Columns["QUANTITYUNITNAME"].Visible = true;
            grdViewDataProductService.Columns["QUANTITYUNITNAME"].VisibleIndex = 5;
            grdViewDataProductService.Columns["QUANTITYUNITNAME"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["QUANTITYUNITNAME"].Width = 50;

            grdViewDataProductService.Columns["AMOUNTINVOICE"].Caption = "ĐG hóa đơn chưa VAT";
            grdViewDataProductService.Columns["AMOUNTINVOICE"].Visible = true;
            grdViewDataProductService.Columns["AMOUNTINVOICE"].VisibleIndex = 6;
            grdViewDataProductService.Columns["AMOUNTINVOICE"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["AMOUNTINVOICE"].ColumnEdit = repAmount;
            grdViewDataProductService.Columns["AMOUNTINVOICE"].Width = 110;

            grdViewDataProductService.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].Caption = "Thành tiền HĐ chưa VAT";
            grdViewDataProductService.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].Visible = true;
            grdViewDataProductService.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].VisibleIndex = 7;
            grdViewDataProductService.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].ColumnEdit = repAmount;
            grdViewDataProductService.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].Width = 120;

            grdViewDataProductService.Columns["INVOICEVAT"].Caption = "Thuế suất HĐ";
            grdViewDataProductService.Columns["INVOICEVAT"].Visible = true;
            grdViewDataProductService.Columns["INVOICEVAT"].VisibleIndex = 8;
            grdViewDataProductService.Columns["INVOICEVAT"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["INVOICEVAT"].ColumnEdit = cboVAT;
            grdViewDataProductService.Columns["INVOICEVAT"].Width = 70;

            grdViewDataProductService.Columns["INVOICEAMOUNT"].Caption = "Tiền thuế HĐ";
            grdViewDataProductService.Columns["INVOICEAMOUNT"].Visible = true;
            grdViewDataProductService.Columns["INVOICEAMOUNT"].VisibleIndex = 9;
            grdViewDataProductService.Columns["INVOICEAMOUNT"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["INVOICEAMOUNT"].ColumnEdit = repAmount;
            grdViewDataProductService.Columns["INVOICEAMOUNT"].Width = 110;

            grdViewDataProductService.Columns["TOTALAMOUNTINVOICE"].Caption = "Thành tiền HĐ có VAT";
            grdViewDataProductService.Columns["TOTALAMOUNTINVOICE"].Visible = true;
            grdViewDataProductService.Columns["TOTALAMOUNTINVOICE"].VisibleIndex = 10;
            grdViewDataProductService.Columns["TOTALAMOUNTINVOICE"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["TOTALAMOUNTINVOICE"].ColumnEdit = repAmount;
            grdViewDataProductService.Columns["TOTALAMOUNTINVOICE"].Width = 120;

            grdViewDataProductService.Columns["INPUTPRICE"].Caption = "ĐG chưa VAT";
            grdViewDataProductService.Columns["INPUTPRICE"].Visible = true;
            grdViewDataProductService.Columns["INPUTPRICE"].VisibleIndex = 11;
            grdViewDataProductService.Columns["INPUTPRICE"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["INPUTPRICE"].ColumnEdit = repAmount;
            grdViewDataProductService.Columns["INPUTPRICE"].Width = 110;

            grdViewDataProductService.Columns["AMOUNTWITHVAT"].Caption = "ĐG có VAT";
            grdViewDataProductService.Columns["AMOUNTWITHVAT"].Visible = true;
            grdViewDataProductService.Columns["AMOUNTWITHVAT"].VisibleIndex = 12;
            grdViewDataProductService.Columns["AMOUNTWITHVAT"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["AMOUNTWITHVAT"].ColumnEdit = repAmount;
            grdViewDataProductService.Columns["AMOUNTWITHVAT"].Width = 110;

            grdViewDataProductService.Columns["TOTALAMOUNTWITHOUTVAT"].Caption = "Thành tiền chưa VAT";
            grdViewDataProductService.Columns["TOTALAMOUNTWITHOUTVAT"].Visible = true;
            grdViewDataProductService.Columns["TOTALAMOUNTWITHOUTVAT"].VisibleIndex = 13;
            grdViewDataProductService.Columns["TOTALAMOUNTWITHOUTVAT"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["TOTALAMOUNTWITHOUTVAT"].ColumnEdit = repAmount;
            grdViewDataProductService.Columns["TOTALAMOUNTWITHOUTVAT"].Width = 120;

            grdViewDataProductService.Columns["VAT"].Caption = "Thuế suất VAT";
            grdViewDataProductService.Columns["VAT"].Visible = true;
            grdViewDataProductService.Columns["VAT"].VisibleIndex = 14;
            grdViewDataProductService.Columns["VAT"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["VAT"].ColumnEdit = repAmount;
            grdViewDataProductService.Columns["VAT"].Width = 70;

            grdViewDataProductService.Columns["VATAMOUNT"].Caption = "Tiền thuế VAT";
            grdViewDataProductService.Columns["VATAMOUNT"].Visible = true;
            grdViewDataProductService.Columns["VATAMOUNT"].VisibleIndex = 15;
            grdViewDataProductService.Columns["VATAMOUNT"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["VATAMOUNT"].ColumnEdit = repAmount;
            grdViewDataProductService.Columns["VATAMOUNT"].Width = 110;

            grdViewDataProductService.Columns["TOTALAMOUNT"].Caption = "Thành tiền có VAT";
            grdViewDataProductService.Columns["TOTALAMOUNT"].Visible = true;
            grdViewDataProductService.Columns["TOTALAMOUNT"].VisibleIndex = 16;
            grdViewDataProductService.Columns["TOTALAMOUNT"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["TOTALAMOUNT"].ColumnEdit = repAmount;
            grdViewDataProductService.Columns["TOTALAMOUNT"].Width = 120;

            grdViewDataProductService.Columns["UNEVEN"].Caption = "Tiền chênh lệch";
            grdViewDataProductService.Columns["UNEVEN"].Visible = true;
            grdViewDataProductService.Columns["UNEVEN"].VisibleIndex = 17;
            grdViewDataProductService.Columns["UNEVEN"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["UNEVEN"].ColumnEdit = repAmount;
            grdViewDataProductService.Columns["UNEVEN"].Width = 110;

            grdViewDataProductService.Columns["CREATEUSER"].Caption = "Người tạo";
            grdViewDataProductService.Columns["CREATEUSER"].Visible = true;
            grdViewDataProductService.Columns["CREATEUSER"].VisibleIndex = 18;
            grdViewDataProductService.Columns["CREATEUSER"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["CREATEUSER"].Width = 178;

            grdViewDataProductService.Columns["CREATEDATE"].Caption = "Ngày tạo";
            grdViewDataProductService.Columns["CREATEDATE"].Visible = true;
            grdViewDataProductService.Columns["CREATEDATE"].VisibleIndex = 19;
            grdViewDataProductService.Columns["CREATEDATE"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["CREATEDATE"].ColumnEdit = repDate;
            grdViewDataProductService.Columns["CREATEDATE"].Width = 110;

            grdViewDataProductService.Columns["UPDATEUSER"].Caption = "Người cập nhật";
            grdViewDataProductService.Columns["UPDATEUSER"].Visible = true;
            grdViewDataProductService.Columns["UPDATEUSER"].VisibleIndex = 20;
            grdViewDataProductService.Columns["UPDATEUSER"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["UPDATEUSER"].Width = 178;

            grdViewDataProductService.Columns["UPDATEDATE"].Caption = "Ngày cập nhật";
            grdViewDataProductService.Columns["UPDATEDATE"].Visible = true;
            grdViewDataProductService.Columns["UPDATEDATE"].VisibleIndex = 21;
            grdViewDataProductService.Columns["UPDATEDATE"].OptionsColumn.AllowEdit = false;
            grdViewDataProductService.Columns["UPDATEDATE"].ColumnEdit = repDate;
            grdViewDataProductService.Columns["UPDATEDATE"].Width = 110;

            grdViewDataProductService.Columns["GROUPSERVICEID"].Caption = "Nhóm hàng hóa dịch vụ";
            grdViewDataProductService.Columns["GROUPSERVICEID"].VisibleIndex = 22;
            grdViewDataProductService.Columns["GROUPSERVICEID"].OptionsColumn.AllowEdit = true;
            grdViewDataProductService.Columns["GROUPSERVICEID"].ColumnEdit = repLockUpServiceGroup;
            grdViewDataProductService.Columns["GROUPSERVICEID"].Width = 200;


            grdViewDataProductService.Columns["PRICEPROTECTNO"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "PRICEPROTECTNO", "Tổng cộng: {0:N0}")});

            grdViewDataProductService.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTINVOICEWITHOUTVAT", "{0:N0}")});

            grdViewDataProductService.Columns["INVOICEAMOUNT"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "INVOICEAMOUNT", "{0:N0}")});

            grdViewDataProductService.Columns["TOTALAMOUNTINVOICE"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTINVOICE", "{0:N0}")});

            grdViewDataProductService.Columns["TOTALAMOUNTWITHOUTVAT"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNTWITHOUTVAT", "{0:N0}")});

            grdViewDataProductService.Columns["VATAMOUNT"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VATAMOUNT", "{0:N0}")});

            grdViewDataProductService.Columns["TOTALAMOUNT"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TOTALAMOUNT", "{0:N0}")});

            grdViewDataProductService.Columns["UNEVEN"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UNEVEN", "{0:N0}")});

            grdViewDataProductService.Columns["GROUPSERVICEID"].Visible = false;
        }

        public void InitializeDataProductService(DataTable dtbData, string _strProductID, bool CalulatorInvoice = false)
        {

            CreateDataProductService();
            if (dtbData == null)
                return;

            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = null;
            new ERP.MasterData.PLC.MD.PLCProduct().LoadInfo(ref objProduct, _strProductID);
            foreach (DataRow drData in dtbData.Rows)
            {

                DataRow drInsert = dtbDataProductService.NewRow();
                #region Add Value To Column
                if (dtbData.Columns.Contains("VATPINVOICEID"))
                    drInsert["VATPINVOICEID"] = drData["VATPINVOICEID"];
                if (dtbData.Columns.Contains("VATPINVOICEDTID"))
                    drInsert["VATPINVOICEDTID"] = drData["VATPINVOICEDTID"];
                if (dtbData.Columns.Contains("PRICEPROTECTNO"))
                    drInsert["PRICEPROTECTNO"] = drData["PRICEPROTECTNO"];
                if (dtbData.Columns.Contains("PRICEPROTECTID"))
                    drInsert["PRICEPROTECTID"] = drData["PRICEPROTECTID"];
                if (dtbData.Columns.Contains("PRICEPROTECTDETAILIDLIST"))
                    drInsert["PRICEPROTECTDETAILIDLIST"] = drData["PRICEPROTECTDETAILIDLIST"];
                if (dtbData.Columns.Contains("INPUTVOUCHERDETAILID"))
                    drInsert["INPUTVOUCHERDETAILID"] = drData["INPUTVOUCHERDETAILID"];
                if (dtbData.Columns.Contains("CREATEDDATE"))
                    drInsert["CREATEDDATE"] = drData["CREATEDDATE"];
                if (dtbData.Columns.Contains("CUSTOMERID"))
                    drInsert["CUSTOMERID"] = drData["CUSTOMERID"];

                if (dtbData.Columns.Contains("CUSTOMERNAME"))
                    drInsert["CUSTOMERNAME"] = drData["CUSTOMERNAME"];
                if (dtbData.Columns.Contains("CUSTOMERADDRESS"))
                    drInsert["CUSTOMERADDRESS"] = drData["CUSTOMERADDRESS"];
                if (dtbData.Columns.Contains("CUSTOMERPHONE"))
                    drInsert["CUSTOMERPHONE"] = drData["CUSTOMERPHONE"];
                if (dtbData.Columns.Contains("CUSTOMERTAXID"))
                    drInsert["CUSTOMERTAXID"] = drData["CUSTOMERTAXID"];

                if (dtbData.Columns.Contains("PRODUCTID"))
                    drInsert["PRODUCTID"] = strProductID.Trim();
                if (dtbData.Columns.Contains("PRODUCTNAME"))
                {
                    if (objProduct != null)
                        drInsert["PRODUCTNAME"] = objProduct.ProductName;
                    else
                        drInsert["PRODUCTNAME"] = drData["PRODUCTNAME"];
                }
                if (dtbData.Columns.Contains("APPLYMISAPRODUCTID"))
                    drInsert["APPLYMISAPRODUCTID"] = drData["APPLYMISAPRODUCTID"];
                if (dtbData.Columns.Contains("MISAPRODUCTMISANAME"))
                    drInsert["MISAPRODUCTMISANAME"] = drData["MISAPRODUCTMISANAME"];
                if (dtbData.Columns.Contains("MISAPRODUCTNAMECODE"))
                    drInsert["MISAPRODUCTNAMECODE"] = drData["MISAPRODUCTNAMECODE"];

                if (dtbData.Columns.Contains("QUANTITYUNITID"))
                    drInsert["QUANTITYUNITID"] = drData["QUANTITYUNITID"];
                if (dtbData.Columns.Contains("QUANTITYUNITNAME"))
                    drInsert["QUANTITYUNITNAME"] = drData["QUANTITYUNITNAME"];

                if (dtbData.Columns.Contains("QUANTITY"))
                    drInsert["QUANTITY"] = drData["QUANTITY"];
                if (dtbData.Columns.Contains("INPUTPRICE"))
                    drInsert["INPUTPRICE"] = drData["INPUTPRICE"];
                if (dtbData.Columns.Contains("VAT"))
                    drInsert["VAT"] = drData["VAT"];
                if (dtbData.Columns.Contains("VATPERCENT"))
                    drInsert["VATPERCENT"] = drData["VATPERCENT"];

                if (dtbData.Columns.Contains("VATAMOUNT"))
                    drInsert["VATAMOUNT"] = drData["VATAMOUNT"];
                if (dtbData.Columns.Contains("AMOUNTWITHVAT"))
                    drInsert["AMOUNTWITHVAT"] = drData["AMOUNTWITHVAT"];
                if (dtbData.Columns.Contains("AMOUNTINVOICE"))
                    drInsert["AMOUNTINVOICE"] = drData["AMOUNTINVOICE"];
                if (dtbData.Columns.Contains("TOTALAMOUNTWITHOUTVAT"))
                    drInsert["TOTALAMOUNTWITHOUTVAT"] = drData["TOTALAMOUNTWITHOUTVAT"];
                if (dtbData.Columns.Contains("TOTALAMOUNT"))
                    drInsert["TOTALAMOUNT"] = drData["TOTALAMOUNT"];
                if (dtbData.Columns.Contains("TOTALAMOUNTINVOICEWITHOUTVAT"))
                    drInsert["TOTALAMOUNTINVOICEWITHOUTVAT"] = drData["TOTALAMOUNTINVOICEWITHOUTVAT"];
                if (dtbData.Columns.Contains("INVOICEVAT"))
                    drInsert["INVOICEVAT"] = drData["INVOICEVAT"];
                if (dtbData.Columns.Contains("INVOICEAMOUNT"))
                    drInsert["INVOICEAMOUNT"] = drData["INVOICEAMOUNT"];
                if (dtbData.Columns.Contains("TOTALAMOUNTINVOICE"))
                    drInsert["TOTALAMOUNTINVOICE"] = drData["TOTALAMOUNTINVOICE"];
                if (dtbData.Columns.Contains("UNEVEN"))
                    drInsert["UNEVEN"] = drData["UNEVEN"];

                if (dtbData.Columns.Contains("CREATEUSER"))
                    drInsert["CREATEUSER"] = drData["CREATEUSER"];
                if (dtbData.Columns.Contains("CREATEDATE"))
                    drInsert["CREATEDATE"] = drData["CREATEDATE"];
                if (dtbData.Columns.Contains("UPDATEUSER"))
                    drInsert["UPDATEUSER"] = drData["UPDATEUSER"];
                if (dtbData.Columns.Contains("UPDATEDATE"))
                    drInsert["UPDATEDATE"] = drData["UPDATEDATE"];

                if (dtbData.Columns.Contains("ISDELETE"))
                    drInsert["ISDELETE"] = drData["ISDELETE"];

                if (dtbData.Columns.Contains("DELETEUSER"))
                    drInsert["DELETEUSER"] = drData["DELETEUSER"];
                if (dtbData.Columns.Contains("DELETEDATE"))
                    drInsert["DELETEDATE"] = drData["DELETEDATE"];
                if (dtbData.Columns.Contains("DELETEREASON"))
                    drInsert["DELETEREASON"] = drData["DELETEREASON"];

                if (dtbData.Columns.Contains("ISNOTAX"))
                    drInsert["ISNOTAX"] = drData["ISNOTAX"];
                if (dtbData.Columns.Contains("ISCHKCHANGE"))
                    drInsert["ISCHKCHANGE"] = drData["ISCHKCHANGE"];
                if (dtbData.Columns.Contains("ISPROMOTIONPRODUCTMISA"))
                    drInsert["ISPROMOTIONPRODUCTMISA"] = drData["ISPROMOTIONPRODUCTMISA"];

                if (dtbData.Columns.Contains("INVOICENO"))
                    drInsert["INVOICENO"] = drData["INVOICENO"];
                if (dtbData.Columns.Contains("INPUTVOUCHERINFO"))
                    drInsert["INPUTVOUCHERINFO"] = drData["INPUTVOUCHERINFO"];
                if (dtbData.Columns.Contains("PROMOTIONDETAILID"))
                    drInsert["PROMOTIONDETAILID"] = drData["PROMOTIONDETAILID"];
                if (dtbData.Columns.Contains("PROMOTIONNAME"))
                    drInsert["PROMOTIONNAME"] = drData["PROMOTIONNAME"];

                if (dtbData.Columns.Contains("VATINVOICENOMAIN"))
                    drInsert["VATINVOICENOMAIN"] = drData["VATINVOICENOMAIN"];
                if (dtbData.Columns.Contains("VATINVOICEIDMAIN"))
                    drInsert["VATINVOICEIDMAIN"] = drData["VATINVOICEIDMAIN"];

                if (dtbData.Columns.Contains("GROUPSERVICEID"))
                    drInsert["GROUPSERVICEID"] = drData["GROUPSERVICEID"];
                #endregion
                if (CalulatorInvoice)
                {
                    // Tinh lai tien 

                    decimal decQuantity = Convert.ToDecimal(drData["QUANTITY"]);
                    decimal decVATPERCENT = Convert.ToDecimal(drData["VATPERCENT"]);


                    // TIỀN HÓA ĐƠN
                    decimal decAmountInvoicePrice = Convert.ToDecimal(drData["AMOUNTINVOICE"]);
                    decimal decInvoiceVAT = Convert.ToDecimal(drData["INVOICEVAT"]);

                    decimal decAMOUNTWITHVAT = decAmountInvoicePrice + Math.Round(decAmountInvoicePrice * (decInvoiceVAT * decVATPERCENT) / 10000, 4);// ĐƠN GIÁ CÓ VAT

                    decimal decTOTALAMOUNTINVOICEWITHOUTVAT = Math.Round(decQuantity * decAmountInvoicePrice, 4);// THÀNH TIỀN HÓA ĐƠN CHƯA VAT
                    decimal decINVOICEAMOUNT = Math.Round(decQuantity * (decAmountInvoicePrice * (decInvoiceVAT * decVATPERCENT) / 10000), 4);//TIỀN THUẾ HÓA ĐƠN
                    decimal decTOTALAMOUNTINVOICE = Math.Round(decQuantity * decAmountInvoicePrice) + Math.Round(decQuantity * (decAmountInvoicePrice) * (decInvoiceVAT * decVATPERCENT) / 10000, 4);//THÀNH TIỀN HÓA ĐƠN CÓ VAT

                    // TIỀN PHIẾU
                    decimal decSalePrice = Convert.ToDecimal(drData["INPUTPRICE"]);
                    decimal decVAT = Convert.ToDecimal(drData["VAT"]);

                    decimal decTOTALAMOUNTWITHOUTVAT = (decQuantity * decSalePrice); // THÀNH TIỀN PHIẾU NHẬP CHƯA VAT
                    decimal decVATAMOUNT = Math.Round(decQuantity * (decSalePrice * (decVAT * decVATPERCENT) / 10000), 4);// TIỀN THUẾ VAT PHIEU NHẬP
                    decimal decTOTALAMOUNT = (decQuantity * decSalePrice) + Math.Round(decQuantity * (decSalePrice * (decVAT * decVATPERCENT) / 10000), 4);//THÀNH TIỀN PHIEU NHẬP CÓ VAT

                    drInsert["VATAMOUNT"] = decVATAMOUNT;
                    drInsert["TOTALAMOUNTWITHOUTVAT"] = decTOTALAMOUNTWITHOUTVAT;
                    drInsert["TOTALAMOUNT"] = decTOTALAMOUNT;

                    drInsert["AMOUNTWITHVAT"] = decAMOUNTWITHVAT;
                    drInsert["TOTALAMOUNTINVOICEWITHOUTVAT"] = decTOTALAMOUNTINVOICEWITHOUTVAT;
                    drInsert["INVOICEAMOUNT"] = decINVOICEAMOUNT;
                    drInsert["TOTALAMOUNTINVOICE"] = decTOTALAMOUNTINVOICE;
                }
                decimal decTOTALAMOUNTINVOICEWITHOUTVATC = Convert.ToDecimal(drInsert["TOTALAMOUNTINVOICEWITHOUTVAT"]);
                decimal decTOTALAMOUNTWITHOUTVATC = Convert.ToDecimal(drInsert["TOTALAMOUNTWITHOUTVAT"]);
                decimal decUNEVEN = decTOTALAMOUNTINVOICEWITHOUTVATC - decTOTALAMOUNTWITHOUTVATC;

                if (bolIsLoadForm)
                    drInsert["UNEVEN"] = decUNEVEN;

                dtbDataProductService.Rows.Add(drInsert);
            }
        }
        private bool GetParameterForm()
        {
            try
            {
                if (objStateForm == StateForm.Insert)
                {
                    if (this.Tag == null || this.Tag.ToString() == string.Empty)
                    {
                        MessageBoxObject.ShowWarningMessage(this, @"Chưa khai báo tham số cấu hình!"
                                                                    + "PRODUCTID = ?");
                        return false;
                    }
                    Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                    if (hstbParam.ContainsKey("PRODUCTID"))
                        strProductID = (hstbParam["PRODUCTID"] ?? "").ToString().Trim();
                    if (string.IsNullOrEmpty(strProductID))
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Khai báo tham số cấu hình không hợp lệ!"
                                                                   + "Không bao gồm PRODUCTID");
                        return false;
                    }

                    ERP.MasterData.PLC.MD.WSProduct.Product objProduct = new MasterData.PLC.MD.WSProduct.Product();
                    objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID.Trim());
                    if (objProduct != null)
                    {
                        foreach (DataRow row in dtbDataProductService.Rows)
                        {
                            row["QUANTITYUNITID"] = objProduct.QuantityUnitID;
                            row["QUANTITYUNITNAME"] = objProduct.QuantityUnitName;
                        }
                        grdDataProductService.RefreshDataSource();
                    }
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi lấy tham số cấu hình form " + this.Text, objExce, "frmInventoryPrice ->GetParameterForm");
                MessageBoxObject.ShowWarningMessage(this, "Lỗi khi lấy tham số cấu hình form " + this.Text);
                return false;
            }
            return true;
        }

        void InitializeStateForm()
        {
            btnUpdate.Enabled = true;
            btnDelete.Enabled = false;
            if (objStateForm != StateForm.Insert)
            {
                btnUpdate.Enabled = objStateForm == StateForm.Update;
                btnDelete.Enabled = SystemConfig.objSessionUser.UserName == "administrator"
                                     || SystemConfig.objSessionUser.IsPermission(strPermission_DeleteInvoice);

                // Khoa Toan Bo Control
                cboBranch.Enabled = false;
                cboInvoiceTransType.Enabled = false;
                cboPayableType.Enabled = false;
                cboCurrencyUnit.Enabled = false;
                cboSaleman.Enabled = false;
                dtmInvoiceDate.Properties.ReadOnly = true;
                dtmDueDate.Properties.ReadOnly = true;
                txtDenominator.ReadOnly = true;
                txtInvoiceSymbol.ReadOnly = true;
                txtInvoiceNo.ReadOnly = true;
                txtCurrencyExchange.ReadOnly = true;
                txtTaxCustomerName.ReadOnly = true;
                txtTaxCustomerAddress.ReadOnly = true;
                txtTaxCustomerTaxCode.ReadOnly = true;
                txtTaxCustomerSalerName.ReadOnly = true;
                txtTaxCustomerDescription.ReadOnly = true;
                if (objStateForm == StateForm.Update)
                {
                    cboInvoiceTransType.Enabled = true;
                    cboPayableType.Enabled = true;
                    cboCurrencyUnit.Enabled = true;

                    cboSaleman.Enabled = true;
                    txtCurrencyExchange.ReadOnly = false;

                    txtTaxCustomerAddress.ReadOnly = false;
                    txtTaxCustomerTaxCode.ReadOnly = false;
                    txtTaxCustomerSalerName.ReadOnly = false;
                    txtTaxCustomerDescription.ReadOnly = false;
                }
            }
            if (dtmInvoiceDate.Properties.ReadOnly)
            {
                dtmInvoiceDate.BackColor = SystemColors.Info;
                dtmInvoiceDate.Properties.Buttons[0].Visible = false;
                dtmInvoiceDate.TabStop = false;
            }
            if (dtmDueDate.Properties.ReadOnly)
            {
                dtmDueDate.BackColor = SystemColors.Info;
                dtmDueDate.Properties.Buttons[0].Visible = false;
                dtmDueDate.TabStop = false;
            }
            if (txtDenominator.ReadOnly)
            {
                txtDenominator.BackColor = SystemColors.Info;
                txtDenominator.TabStop = false;
            }
            if (txtInvoiceSymbol.ReadOnly)
            {
                txtInvoiceSymbol.BackColor = SystemColors.Info;
                txtInvoiceSymbol.TabStop = false;
            }
            if (txtInvoiceNo.ReadOnly)
            {
                txtInvoiceNo.BackColor = SystemColors.Info;
                txtInvoiceNo.TabStop = false;
            }
            if (txtCurrencyExchange.ReadOnly)
            {
                txtCurrencyExchange.BackColor = SystemColors.Info;
                txtCurrencyExchange.TabStop = false;
            }
            if (txtTaxCustomerName.ReadOnly)
            {
                txtTaxCustomerName.BackColor = SystemColors.Info;
                txtTaxCustomerName.TabStop = false;
            }
            if (txtTaxCustomerAddress.ReadOnly)
            {
                txtTaxCustomerAddress.BackColor = SystemColors.Info;
                txtTaxCustomerAddress.TabStop = false;
            }
            if (txtTaxCustomerTaxCode.ReadOnly)
            {
                txtTaxCustomerTaxCode.BackColor = SystemColors.Info;
                txtTaxCustomerTaxCode.TabStop = false;
            }
            if (txtTaxCustomerSalerName.ReadOnly)
            {
                txtTaxCustomerSalerName.BackColor = SystemColors.Info;
                txtTaxCustomerSalerName.TabStop = false;
            }
            if (txtTaxCustomerDescription.ReadOnly)
            {
                txtTaxCustomerDescription.BackColor = SystemColors.Info;
                txtTaxCustomerDescription.TabStop = false;
            }

            #region Enable Grid Control
            if (objStateForm == StateForm.Insert)
            {
                grdViewDataProductService.Columns["CREATEUSER"].Visible = false;
                grdViewDataProductService.Columns["CREATEDATE"].Visible = false;
                grdViewDataProductService.Columns["UPDATEUSER"].Visible = false;
                grdViewDataProductService.Columns["UPDATEDATE"].Visible = false;
            }
            if (objStateForm == StateForm.Insert
                || objStateForm == StateForm.Update)
            {
                if (SystemConfig.objSessionUser.UserName == "administrator"
                    || SystemConfig.objSessionUser.IsPermission(strPermission_EditMoney))
                {
                    grdViewDataProductService.Columns["AMOUNTINVOICE"].OptionsColumn.AllowEdit = true;
                    // Hồ Bình yêu cầu khóa cột này 26/07/2017 grdViewDataProductService.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].OptionsColumn.AllowEdit = true;
                    grdViewDataProductService.Columns["INVOICEVAT"].OptionsColumn.AllowEdit = true;
                    // Hồ Bình yêu cầu khóa cột này 26/07/2017 grdViewDataProductService.Columns["INVOICEAMOUNT"].OptionsColumn.AllowEdit = true;
                    // Hồ Bình yêu cầu khóa cột này 26/07/2017 grdViewDataProductService.Columns["TOTALAMOUNTINVOICE"].OptionsColumn.AllowEdit = true;
                    grdViewDataProductService.Columns["UNEVEN"].OptionsColumn.AllowEdit = true;
                }
                else
                {
                    if (SystemConfig.objSessionUser.IsPermission("VAT_INVOICE_EDIT_VATINVOICE")
                        || SystemConfig.objSessionUser.UserName == "administrator")
                        grdViewDataProductService.Columns["INVOICEVAT"].OptionsColumn.AllowEdit = true;
                    if (SystemConfig.objSessionUser.IsPermission("VAT_INVOICE_EDIT_TOTALAMOUNT")
                        || SystemConfig.objSessionUser.UserName == "administrator")
                        grdViewDataProductService.Columns["TOTALAMOUNTINVOICE"].OptionsColumn.AllowEdit = true;
                    if (SystemConfig.objSessionUser.IsPermission("VAT_INVOICE_EDIT_DIFFERENCEAMOUNT")
                        || SystemConfig.objSessionUser.UserName == "administrator")
                        grdViewDataProductService.Columns["UNEVEN"].OptionsColumn.AllowEdit = true;
                }
            }
            if (grdViewDataProductService.Columns["AMOUNTINVOICE"].OptionsColumn.AllowEdit)
                grdViewDataProductService.Columns["AMOUNTINVOICE"].AppearanceCell.BackColor = Color.White;
            if (grdViewDataProductService.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].OptionsColumn.AllowEdit)
                grdViewDataProductService.Columns["TOTALAMOUNTINVOICEWITHOUTVAT"].AppearanceCell.BackColor = Color.White;
            if (grdViewDataProductService.Columns["INVOICEVAT"].OptionsColumn.AllowEdit)
                grdViewDataProductService.Columns["INVOICEVAT"].AppearanceCell.BackColor = Color.White;
            if (grdViewDataProductService.Columns["INVOICEAMOUNT"].OptionsColumn.AllowEdit)
                grdViewDataProductService.Columns["INVOICEAMOUNT"].AppearanceCell.BackColor = Color.White;
            if (grdViewDataProductService.Columns["TOTALAMOUNTINVOICE"].OptionsColumn.AllowEdit)
                grdViewDataProductService.Columns["TOTALAMOUNTINVOICE"].AppearanceCell.BackColor = Color.White;
            if (grdViewDataProductService.Columns["UNEVEN"].OptionsColumn.AllowEdit)
                grdViewDataProductService.Columns["UNEVEN"].AppearanceCell.BackColor = Color.White;
            if (grdViewDataProductService.Columns["GROUPSERVICEID"].OptionsColumn.AllowEdit)
                grdViewDataProductService.Columns["GROUPSERVICEID"].AppearanceCell.BackColor = Color.White;
            #endregion
        }
        private bool ValidateData()
        {
            if (cboBranch.ColumnID == -1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn chi nhánh!");
                cboBranch.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtTaxNumber.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Mã số thuế chi nhánh không tồn tại. Vui lòng kiểm tra lại!");
                cboBranch.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtAddress.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Địa chỉ chi nhánh không tồn tại. Vui lòng kiểm tra lại!");
                cboBranch.Focus();
                return false;
            }

            if (cboInvoiceTransType.ColumnID == -1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn loại nghiệp vụ!");
                cboInvoiceTransType.Focus();
                return false;
            }

            //if (cboStore.ColumnID == -1)
            //{
            //    MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho tạo!");
            //    cboStore.Focus();
            //    return false;
            //}

            #region Check Thong Tin Khach Hang
            if (string.IsNullOrEmpty(txtTaxCustomerName.Text))
            {
                txtTaxCustomerName.Focus();
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập tên nhà cung cấp!");
                return false;
            }
            else if (txtTaxCustomerName.Text.Length > 200)
            {
                MessageBoxObject.ShowWarningMessage(this, "Tên nhà cung cấp trên hóa đơn dài quá mức quy định!");
                txtTaxCustomerName.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtTaxCustomerAddress.Text.Trim()))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập địa chỉ nhà cung cấp!");
                txtTaxCustomerAddress.Focus();
                return false;
            }

            if (txtTaxCustomerAddress.Text.Length > 300)
            {
                MessageBoxObject.ShowWarningMessage(this, "Địa chỉ dài quá mức quy định!");
                txtTaxCustomerAddress.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtTaxCustomerTaxCode.Text.Trim()))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập mã số thuế nhà cung cấp!");
                txtTaxCustomerTaxCode.Focus();
                return false;
            }

            if (!string.IsNullOrEmpty(txtTaxCustomerTaxCode.Text))
            {
                if (txtTaxCustomerTaxCode.Text.Length > 100)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập mã số thuế nhỏ hơn 100 ký tự!");
                    txtTaxCustomerTaxCode.Focus();
                    return false;
                }
            }
            #endregion
            if (string.IsNullOrEmpty(txtDenominator.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập mẫu số!");
                txtDenominator.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtInvoiceSymbol.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập kí hiệu!");
                txtInvoiceSymbol.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtInvoiceNo.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập số hóa đơn!");
                txtInvoiceNo.Focus();
                return false;
            }
            if (dtmInvoiceDate.EditValue == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn ngày hóa đơn!");
                dtmInvoiceDate.Focus();
                return false;
            }

            if (dtbDataProductService == null || dtbDataProductService.Rows.Count == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không có sản phẩm để tạo hóa đơn!");
                return false;
            }


            // LE VAN DONG - BO SUNG
            // Nếu chọn loại nghiệp vụ có Lên bảng kê thuế -> Thì chi tiết phải chọn nhóm hàng hóa dịch vụ
            DataRow[] rowGroup = dtbInvoiceTransactionType.Select("INVOICETRANSTYPEID=" + cboInvoiceTransType.ColumnID);
            if (rowGroup.Length > 0 && Convert.ToBoolean(rowGroup[0]["ISTAXDECLARATION"]))
            {
                if (this.dtbDataProductService.AsEnumerable().Count(x => x.Field<int?>("GROUPSERVICEID") == null) > 0)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn nhóm hàng hóa dịch vụ cho sản phẩm!");
                    grdViewDataProductService.FocusedColumn = grdViewDataProductService.Columns["GROUPSERVICEID"];
                    return false;
                }
            }

            var lst = this.dtbDataProductService.AsEnumerable().GroupBy(x => x.Field<string>("PRODUCTID"));
            Int32 decProduct = lst.Count();
            if (decProduct > 15)
            {
                MessageBoxObject.ShowWarningMessage(this, "Tổng sản phẩm của 1 hóa đơn không vượt quá 15 sản phẩm!");
                return false;
            }
            return true;
        }
        #endregion

        #region Events
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            grdViewDataProductService.CloseEditor();
            grdViewDataProductService.UpdateCurrentRow();

            grdViewDataAttachment.CloseEditor();
            grdViewDataAttachment.UpdateCurrentRow();

            if (!ValidateData())
                return;
            
            // Insert Data Value
            //tạo object insert
            ERP.Inventory.PLC.WSPriceProtect.VAT_PInvoice objVATInvoice = new PLC.WSPriceProtect.VAT_PInvoice();

            // Thong tin khach hang 
            objVATInvoice.CustomerID = CustomerID;
            objVATInvoice.VATCustomerID = CustomerID;
            objVATInvoice.CustomerName = txtTaxCustomerName.Text.Trim();
            objVATInvoice.TaxCustomerAddress = txtTaxCustomerAddress.Text.Trim();
            objVATInvoice.CustomerTaxID = txtTaxCustomerTaxCode.Text.Trim();
            objVATInvoice.SELLER = txtTaxCustomerSalerName.Text.Trim();
            objVATInvoice.Description = txtTaxCustomerDescription.Text.Trim();
            objVATInvoice.OutputVoucherInfo = txtLstCustomerName.Text;

            // Thong tin hoa don 
            // Thong Tin Chi nhanh
            objVATInvoice.BranchID = cboBranch.ColumnID;
            objVATInvoice.CreatedStoreID = SystemConfig.intDefaultStoreID;
            objVATInvoice.InvoiceTRANSTypeID = cboInvoiceTransType.ColumnID;
            DataRow[] rowGroup = dtbInvoiceTransactionType.Select("INVOICETRANSTYPEID=" + cboInvoiceTransType.ColumnID);
            bool bolIsTaxDeclaration = Convert.ToBoolean(rowGroup[0]["ISTAXDECLARATION"]);

            // Thông tin Hoa Don
            objVATInvoice.Denominator = txtDenominator.Text.Trim(); //Mẫu số
            objVATInvoice.InvoiceSymbol = txtInvoiceSymbol.Text.Trim(); // ký hiệu
            objVATInvoice.InvoiceNO = int.Parse(txtInvoiceNo.Text); // Số hoa don
            objVATInvoice.InvoiceDate = ((DateTime)dtmInvoiceDate.EditValue);
            objVATInvoice.PurchaseMANID = cboSaleman.UserName;
            objVATInvoice.PurchaseMAN = cboSaleman.FullName;
            objVATInvoice.PayableTypeID = cboPayableType.ColumnID;
            objVATInvoice.InvoiceDUEDate = dtmDueDate.DateTime;

            objVATInvoice.CurrencyUnit = Convert.ToInt32(cboCurrencyUnit.SelectedValue);
            objVATInvoice.CurrencyExchange = Convert.ToDecimal(txtCurrencyExchange.Value);

            objVATInvoice.FormTypeID = FormTypeID;

            objVATInvoice.TypeID = intTypeID;
            objVATInvoice.InvoiceType = intTransactionGroupId;
            objVATInvoice.VATInvoiceMainID = "";
            objVATInvoice.PInvoiceVoucherNO = txtVATPInvoiceVoucherNo.Text.Trim();

            var lst = dtbDataProductService.AsEnumerable();
            //Cộng tiền chưa VAT
            objVATInvoice.AmountBF = lst.Sum(x => (x.Field<decimal?>("TOTALAMOUNTWITHOUTVAT") ?? 0));
            //Cộng tiền thuế VAT
            objVATInvoice.VATAmount = lst.Sum(x => (x.Field<decimal?>("VATAMOUNT") ?? 0));
            //Cộng tiền có  VAT
            objVATInvoice.TotalAmount = lst.Sum(x => (x.Field<decimal?>("TOTALAMOUNT") ?? 0));
            //Cộng tiền hóa đơn trước thuế
            objVATInvoice.AmountInvoiceBF = lst.Sum(x => (x.Field<decimal?>("TOTALAMOUNTINVOICEWITHOUTVAT") ?? 0));
            //Cộng tiền thuế VAT hóa đơn
            objVATInvoice.VATInvoice = lst.Sum(x => (x.Field<decimal?>("INVOICEAMOUNT") ?? 0));
            //Cộng tiền hóa đơn có VAT
            objVATInvoice.TotalAmountInvoice = lst.Sum(x => (x.Field<decimal?>("TOTALAMOUNTINVOICE") ?? 0));
            //Thuế suất hóa đơn GTGT
            objVATInvoice.InvoiceVAT = lst.Max(x => x.Field<decimal>("INVOICEVAT"));

            // nap du lieu danh sach san pham 
            List<VAT_PInvoice_Product> lstProduct = new List<VAT_PInvoice_Product>();
            foreach (DataRow item in dtbDataProductService.Rows)
            {
                VAT_PInvoice_Product obj = new VAT_PInvoice_Product();
                obj.VATPInvoiceDTID = (item["VATPINVOICEDTID"] ?? "").ToString();
                obj.VATPInvoiceID = (item["VATPINVOICEID"] ?? "").ToString();

                obj.InputVoucherDetailID = string.IsNullOrEmpty(item["INPUTVOUCHERDETAILID"].ToString()) ? "-1" : item["INPUTVOUCHERDETAILID"].ToString();
                obj.OrderDetailId = string.IsNullOrEmpty(item["ORDERDETAILID"].ToString()) ? "-1" : item["ORDERDETAILID"].ToString();
                obj.PriceProtectDetailIDList = string.IsNullOrEmpty(item["PRICEPROTECTDETAILIDLIST"].ToString()) ? "" : item["PRICEPROTECTDETAILIDLIST"].ToString();

                if (objStateForm == StateForm.Insert)
                    obj.ProductID = strProductID.Trim();
                else
                    obj.ProductID = item["PRODUCTID"].ToString().Trim();

                obj.Quantity = Convert.ToDecimal(item["QUANTITY"]);
                obj.PriceInvoice = Convert.ToDecimal(item["AMOUNTINVOICE"]);
                obj.VATInvoice = Convert.ToDecimal(item["INVOICEVAT"]);
                obj.PriceInvoice = Convert.ToDecimal(item["AMOUNTINVOICE"]);
                obj.VATInvoice = Convert.ToDecimal(item["INVOICEVAT"]);

                obj.QuantityUnitID = Convert.ToInt32(item["QUANTITYUNITID"]);
                obj.DIFFERENCEAmount = Convert.ToDecimal(item["UNEVEN"]);
                if (bolIsTaxDeclaration)
                    obj.GroupServiceId = Convert.ToInt32(item["GROUPSERVICEID"]);
                else
                    obj.GroupServiceId = null;

                lstProduct.Add(obj);
            }
            //lstProduct.AddRange(lstProductDel.ToArray());

            objVATInvoice.ProductList = lstProduct.ToArray();
            objVATInvoice.AttachmentList = UpdateAttachment().ToArray();
            objVATInvoice.AttachmentDelList = UpdateAttachmentDel().ToArray();
            // Thực thi qua trình xuong da ta
            ERP.Inventory.PLC.WSPriceProtect.ResultMessage objResultMessage = new ResultMessage();
            string strVATInvoiceID = string.Empty;
            string strStatus = string.Empty;

            frmConfirmInventoryPrice frm = new frmConfirmInventoryPrice();
            frm.TotalAmount = Math.Round(objVATInvoice.TotalAmountInvoice, 0, MidpointRounding.AwayFromZero);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (objStateForm == StateForm.Insert) // Insert 
                {
                    objVATInvoice.PriceProtectIDList = strPriceProtectIdList.Replace("<", "").Split('>');
                    objResultMessage = objPLCPriceProtect.Insert_VAT_Invoice(objVATInvoice, ref strVATInvoiceID);
                    strStatus = "Thêm mới";
                }
                else if (objStateForm == StateForm.Update) // Update
                {
                    objVATInvoice.VATPInvoiceID = strVATPInvoiceID.Trim();
                    objResultMessage = objPLCPriceProtect.Update_VAT_Invoice(objVATInvoice);
                    strVATInvoiceID = (objVATInvoice.VATPInvoiceID ?? "").ToString().Trim();
                    strStatus = "Cập nhật";
                }
                if (objResultMessage.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, objResultMessage.Message, objResultMessage.MessageDetail);
                    return;
                }
                bolRefresh = true;
                MessageBoxObject.ShowInfoMessage(this, strStatus + " hóa đơn thành công!");
                // Nap Lai Thong tin moi 
                LoadVATInformation(strVATInvoiceID);
                drResult = DialogResult.OK;
            }
            else
            {
                drResult = DialogResult.Cancel;
            }
        }
        
        private void btnUndo_Click(object sender, EventArgs e)
        {
            this.DialogResult = drResult;
            this.Close();
        }

        #endregion


        private void frmInventoryPrice_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.DialogResult = drResult;
        }



        #region TẬP TIN ĐÍNH KÈM
        void InitializeGridDataAttachment()
        {
            string[] colList = new string[] {
                "ISSELECT", "ATTACHMENTNAME", "ATTACHMENTPATH", "DESCRIPTION", "CREATEDUSER", "CREATEDDATE", "COLDOWLOAD"
            };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdDataAttachment, colList);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewDataAttachment);

            grdViewDataAttachment.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewDataAttachment.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            grdViewDataAttachment.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            grdViewDataAttachment.OptionsView.ShowAutoFilterRow = false;
            grdViewDataAttachment.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            grdViewDataAttachment.OptionsView.ShowFooter = false;
            grdViewDataAttachment.OptionsView.ShowGroupPanel = false;
            for (int i = 0; i < grdViewDataAttachment.Columns.Count; i++)
            {
                grdViewDataAttachment.Columns[i].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                grdViewDataAttachment.Columns[i].AppearanceHeader.Options.UseTextOptions = true;
                grdViewDataAttachment.Columns[i].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewDataAttachment.Columns[i].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                grdViewDataAttachment.Columns[i].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                grdViewDataAttachment.Columns[i].OptionsColumn.AllowEdit = false;

            }

            grdViewDataAttachment.Columns["ISSELECT"].Caption = "Chọn";
            grdViewDataAttachment.Columns["ATTACHMENTNAME"].Caption = "Tên tập tin";
            grdViewDataAttachment.Columns["ATTACHMENTPATH"].Caption = "Đường dẫn";
            grdViewDataAttachment.Columns["DESCRIPTION"].Caption = "Mô tả";
            grdViewDataAttachment.Columns["CREATEDUSER"].Caption = "Nhân viên tạo";
            grdViewDataAttachment.Columns["CREATEDDATE"].Caption = "Thời gian tạo";
            grdViewDataAttachment.Columns["COLDOWLOAD"].Caption = "Tải";

            grdViewDataAttachment.Columns["COLDOWLOAD"].ColumnEdit = btnViewImages;
            grdViewDataAttachment.Columns["COLDOWLOAD"].OptionsColumn.ReadOnly = true;
            grdViewDataAttachment.Columns["COLDOWLOAD"].OptionsColumn.AllowMove = false;
            grdViewDataAttachment.Columns["COLDOWLOAD"].OptionsColumn.AllowSize = false;
            grdViewDataAttachment.Columns["COLDOWLOAD"].OptionsColumn.FixedWidth = true;
            grdViewDataAttachment.Columns["COLDOWLOAD"].OptionsColumn.AllowEdit = true;
            grdViewDataAttachment.Columns["COLDOWLOAD"].ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;

            grdViewDataAttachment.Columns["DESCRIPTION"].OptionsColumn.AllowEdit = true;
            grdViewDataAttachment.Columns["DESCRIPTION"].ColumnEdit = repDescriptionAttachment;

            grdViewDataAttachment.Columns["ISSELECT"].ColumnEdit = repAttachment;
            grdViewDataAttachment.Columns["ISSELECT"].OptionsColumn.AllowEdit = true;

            grdViewDataAttachment.Columns["ISSELECT"].Width = 60;
            grdViewDataAttachment.Columns["ATTACHMENTNAME"].Width = 250;
            grdViewDataAttachment.Columns["ATTACHMENTPATH"].Width = 100;
            grdViewDataAttachment.Columns["DESCRIPTION"].Width = 250;
            grdViewDataAttachment.Columns["CREATEDUSER"].Width = 180;
            grdViewDataAttachment.Columns["CREATEDDATE"].Width = 140;
            grdViewDataAttachment.Columns["COLDOWLOAD"].Width = 30;
            grdViewDataAttachment.Columns["ATTACHMENTPATH"].Visible = false;

            grdViewDataAttachment.Columns["CREATEDDATE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            grdViewDataAttachment.Columns["CREATEDDATE"].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss";
        }

        void CreateDataAttachment()
        {
            dtbDataAttachment = new DataTable();
            dtbDataAttachment.Columns.Add("ISSELECT", typeof(decimal));
            dtbDataAttachment.Columns["ISSELECT"].DefaultValue = 0;
            dtbDataAttachment.Columns.Add("ATTACHMENTID", typeof(string));
            dtbDataAttachment.Columns.Add("VATPINVOICEID", typeof(string));
            dtbDataAttachment.Columns.Add("INVOICEDATE", typeof(string));
            dtbDataAttachment.Columns.Add("ATTACHMENTNAME", typeof(string));
            dtbDataAttachment.Columns.Add("ATTACHMENTPATH", typeof(string));
            dtbDataAttachment.Columns.Add("FILEID", typeof(string));
            dtbDataAttachment.Columns.Add("DESCRIPTION", typeof(string));
            dtbDataAttachment.Columns.Add("CREATEDUSER", typeof(string));
            dtbDataAttachment.Columns.Add("CREATEDDATE", typeof(DateTime));
            dtbDataAttachment.Columns.Add("ISNEW", typeof(bool));
            dtbDataAttachment_Del = dtbDataAttachment.Clone();
        }

        public void InitializeDataAttachment(DataTable dtbData)
        {
            CreateDataAttachment();
            if (dtbData == null)
                return;
            foreach (DataRow drData in dtbData.Rows)
            {
                DataRow drInsert = dtbDataAttachment.NewRow();
                #region Add Value To Column
                if (dtbData.Columns.Contains("ATTACHMENTID"))
                    drInsert["ATTACHMENTID"] = drData["ATTACHMENTID"];
                if (dtbData.Columns.Contains("VATPINVOICEID"))
                    drInsert["VATPINVOICEID"] = drData["VATPINVOICEID"];
                if (dtbData.Columns.Contains("INVOICEDATE"))
                    drInsert["INVOICEDATE"] = drData["INVOICEDATE"];
                if (dtbData.Columns.Contains("ATTACHMENTNAME"))
                    drInsert["ATTACHMENTNAME"] = drData["ATTACHMENTNAME"];
                if (dtbData.Columns.Contains("ATTACHMENTPATH"))
                    drInsert["ATTACHMENTPATH"] = drData["ATTACHMENTPATH"];
                if (dtbData.Columns.Contains("FILEID"))
                    drInsert["FILEID"] = drData["FILEID"];
                if (dtbData.Columns.Contains("DESCRIPTION"))
                    drInsert["DESCRIPTION"] = drData["DESCRIPTION"];
                if (dtbData.Columns.Contains("CREATEDUSER"))
                    drInsert["CREATEDUSER"] = drData["CREATEDUSER"];
                if (dtbData.Columns.Contains("CREATEDDATE"))
                    drInsert["CREATEDDATE"] = drData["CREATEDDATE"];
                drInsert["ISNEW"] = false;
                #endregion

                dtbDataAttachment.Rows.Add(drInsert);
            }
        }

        private void mnuItemAddAtt_Click(object sender, EventArgs e)
        {
            AddAttachment();
        }

        private void mnuItemDelAtt_Click(object sender, EventArgs e)
        {
            DelAttachment();
        }

        private void AddAttachment()
        {
            try
            {
                OpenFileDialog dlg = new OpenFileDialog();
                if (dlg.ShowDialog(this) == DialogResult.OK)
                {
                    if (!File.Exists(dlg.FileName))
                    {
                        MessageBox.Show(this, "File không tồn tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else
                    {
                        FileInfo objFileInfo = new FileInfo(dlg.FileName);
                        decimal FileSize = Math.Round(objFileInfo.Length / Convert.ToDecimal((1024)), 2); //dung lượng tính theo kb
                        if (FileSize > 8000)
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Dung lượng file lớn hơn giới hạn cho phép (8 MB)!");
                            return;
                        }

                        DataTable dt = (DataTable)grdDataAttachment.DataSource;
                        grdDataAttachment.DataSource = null;
                        grdDataAttachment.DataSource = dt;
                        DataRow[] DF = dt.Select("ATTACHMENTNAME='" + dlg.SafeFileName.ToString().Replace("'", "''") + "'");
                        if (DF.Length > 0) return;

                        DataRow row = dt.NewRow();
                        row["ISSELECT"] = 0;
                        row["ISNEW"] = true;
                        row["ATTACHMENTNAME"] = dlg.SafeFileName;
                        row["ATTACHMENTPATH"] = dlg.FileName;
                        row["CREATEDDATE"] = Globals.GetServerDateTime();
                        row["CREATEDUSER"] = SystemConfig.objSessionUser.UserName + "-" + SystemConfig.objSessionUser.FullName;
                        dt.Rows.Add(row);

                        grdViewDataAttachment.RefreshData();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
        private void DelAttachment()
        {
            grdViewDataAttachment.FocusedColumn = grdViewDataAttachment.Columns["ATTACHMENTNAME"];
            DataTable dt = (DataTable)grdDataAttachment.DataSource;
            grdDataAttachment.DataSource = null;
            grdDataAttachment.DataSource = dt;
            DataRow[] DF = dt.Select("ISSELECT=1");
            if (DF.Length == 0) return;
            foreach (DataRow row in DF)
            {
                DataRow drInsert = dtbDataAttachment_Del.NewRow();
                drInsert["ISSELECT"] = row["ISSELECT"];
                drInsert["ATTACHMENTID"] = row["ATTACHMENTID"];
                drInsert["VATPINVOICEID"] = row["VATPINVOICEID"];
                drInsert["INVOICEDATE"] = row["INVOICEDATE"];
                drInsert["ATTACHMENTNAME"] = row["ATTACHMENTNAME"];
                drInsert["ATTACHMENTPATH"] = row["ATTACHMENTPATH"];
                drInsert["FILEID"] = row["FILEID"];
                drInsert["DESCRIPTION"] = row["DESCRIPTION"];
                dtbDataAttachment_Del.Rows.Add(drInsert);
                row.Delete();
            }
            grdViewDataAttachment.RefreshData();
        }

        private List<VAT_PInvoice_Attachment> UpdateAttachment()
        {
            List<VAT_PInvoice_Attachment> objVAT_PInvoice_AttachmentLst = new List<VAT_PInvoice_Attachment>();
            DataTable dt = (DataTable)grdDataAttachment.DataSource;
            if (dt == null)
                return objVAT_PInvoice_AttachmentLst;
            foreach (DataRow row in dt.Rows)
            {
                if (row.RowState == DataRowState.Deleted)
                    continue;
                VAT_PInvoice_Attachment objVAT_PInvoice_Attachment = new VAT_PInvoice_Attachment();
                objVAT_PInvoice_Attachment.AttachMENTName = row["ATTACHMENTNAME"].ToString().Trim();

                string strFileID = (row["FILEID"] ?? "").ToString().Trim();
                if (string.IsNullOrEmpty(strFileID))
                {
                    strFileID = string.Empty;
                    string strURL = string.Empty;
                    if (UploadAttachment(ref strFileID, ref strURL, row["ATTACHMENTPATH"].ToString().Trim()))
                    {
                        objVAT_PInvoice_Attachment.AttachMENTPath = strURL;
                        objVAT_PInvoice_Attachment.FileID = strFileID;
                    }
                }

                objVAT_PInvoice_Attachment.AttachMENTID = (row["ATTACHMENTID"] ?? "").ToString().Trim();
                objVAT_PInvoice_Attachment.Description = row["DESCRIPTION"].ToString().Trim();
                objVAT_PInvoice_AttachmentLst.Add(objVAT_PInvoice_Attachment);
            }

            return objVAT_PInvoice_AttachmentLst;
        }

        private List<VAT_PInvoice_Attachment> UpdateAttachmentDel()
        {
            List<VAT_PInvoice_Attachment> objVAT_PInvoice_AttachmentLst = new List<VAT_PInvoice_Attachment>();
            foreach (DataRow row in dtbDataAttachment_Del.Rows)
            {
                if (row.RowState == DataRowState.Deleted)
                    continue;
                string strATTACHMENTID = (row["ATTACHMENTID"] ?? "").ToString().Trim();
                if (string.IsNullOrEmpty(strATTACHMENTID))
                    continue;
                VAT_PInvoice_Attachment objVAT_PInvoice_Attachment = new VAT_PInvoice_Attachment();
                objVAT_PInvoice_Attachment.AttachMENTID = strATTACHMENTID;
                objVAT_PInvoice_AttachmentLst.Add(objVAT_PInvoice_Attachment);
            }
            return objVAT_PInvoice_AttachmentLst;
        }

        private void mnuAttachment_Opening(object sender, CancelEventArgs e)
        {
            grdViewDataAttachment.FocusedColumn = grdViewDataAttachment.Columns["ATTACHMENTNAME"];
            DataTable dt = (DataTable)grdDataAttachment.DataSource;
            grdDataAttachment.DataSource = null;
            grdDataAttachment.DataSource = dt;
            DataRow[] DF = dt.Select("ISSELECT=1");
            mnuItemDelAtt.Enabled = DF != null & DF.Length > 0;
        }


        private bool UploadAttachment(ref string strFileID, ref string strURL, string strUrlLocal)
        {
            this.Refresh();
            try
            {
                if (strUrlLocal != string.Empty)
                {
                    string strMess = string.Empty;
                    ERP.FMS.DUI.DUIFileManager.UploadFile(this, "FMSAplication_ProERP_PurchaseOrderAttachment", strUrlLocal.Trim(), ref strFileID, ref strURL);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return false;
                    }
                }
            }
            catch (Exception objEx)
            {
                MessageBox.Show("Lỗi đính kèm tập tin ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi đính kèm tập tin ", objEx, Globals.ModuleName);
                this.Cursor = Cursors.Default;
                btnUpdate.Enabled = true;
                return false;
            }
            return true;
        }

        private void btnViewImages_Click(object sender, EventArgs e)
        {
            if (grdViewDataAttachment.FocusedRowHandle < 0)
                return;
            DataRowView row = (DataRowView)grdViewDataAttachment.GetRow(grdViewDataAttachment.FocusedRowHandle);
            if (Convert.ToBoolean(row["ISNEW"]) == true)
            {
                return;
            }
            DownloadFile(row["ATTACHMENTNAME"].ToString(), row["FILEID"].ToString());
        }

        /// <summary>
        /// Tải file đính kèm
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="strLocation"></param>
        /// <returns></returns>
        public bool DownloadFile(string strFileName, string strFileID)
        {
            try
            {
                SaveFileDialog dlgSaveFile = new SaveFileDialog();
                dlgSaveFile.Title = "Lưu file tải về";
                dlgSaveFile.FileName = strFileName;
                string strExtension = "";
                string[] str = strFileName.Split('.');
                if (str.Length > 1)
                    strExtension = "." + str[1];
                dlgSaveFile.Filter = "All files (*.*)|*.*";
                if (dlgSaveFile.ShowDialog() == DialogResult.OK)
                {
                    if (dlgSaveFile.FileName != string.Empty)
                    {
                        ERP.FMS.DUI.DUIFileManager.DownloadFile(this, "FMSAplication_ProERP_PurchaseOrderAttachment", strFileID, string.Empty, dlgSaveFile.FileName + strExtension);
                    }
                }
                return true;
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi tải file đính kèm", objExce.ToString(), "frmInventoryPrice -> DownloadFile", DUIInventory_Globals.ModuleName);
                MessageBox.Show("Lỗi tải file đính kèm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }
        #endregion

        #region Nhà cung cấp
        public void SetCustomerInforToControl()
        {
            if (objStateForm == StateForm.Insert)
            {
                ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(intCustomerID);
                if (objCustomer == null) // Nếu không tìm thấy thông tin khách hàng (đã bị xóa hoặc bỏ kích hoạt) thì lấy khách hàng mặc định
                {
                    MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy nhà cung cấp có ID = [" + intCustomerID.ToString() + "]");
                    return;
                }

                strTaxCustomerName = objCustomer.TaxCustomerName;
                if (string.IsNullOrEmpty(strTaxCustomerName))
                    strTaxCustomerName = objCustomer.CustomerName;
                strTaxCustomerAddress = objCustomer.TaxCustomerAddress;
                strTaxCustomerTaxCode = objCustomer.CustomerTaxID;
                strTaxCustomerPhone = objCustomer.CustomerPhone;
            }
            else
            {

            }
            txtTaxCustomerName.Text = strTaxCustomerName;
            txtTaxCustomerAddress.Text = strTaxCustomerAddress;
            txtTaxCustomerTaxCode.Text = strTaxCustomerTaxCode;
            //txtPhoneCustomer.Text = strTaxCustomerPhone;
        }


        #endregion
        private void cboCurrencyUnit_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                txtCurrencyExchange.Value = ERP.MasterData.PLC.MD.PLCCurrencyUnit.GetCurrencyExchange(Convert.ToInt32(cboCurrencyUnit.SelectedValue));
            }
            catch
            {
                txtCurrencyExchange.Value = ERP.MasterData.PLC.MD.PLCCurrencyUnit.GetCurrencyExchange(1);
            }
        }

        private void cboBranch_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboBranch.ColumnID != -1)
            {
                DataTable dtbBranch = (DataTable)cboBranch.DataSource;
                var dr = dtbBranch.Select("BRANCHID=" + cboBranch.ColumnID);
                if (dr.Any())
                {
                    txtAddress.Text = dr[0]["TAXADDRESS"].ToString();
                    txtTaxNumber.Text = dr[0]["TAXCODE"].ToString();
                }
            }
            else
            {
                txtAddress.Text = string.Empty;
                txtTaxNumber.Text = string.Empty;
            }
        }

        private void txtInvoiceNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void grdViewDataProductService_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                if (grdViewDataProductService.FocusedRowHandle < 0) return;
                DataRowView item = (DataRowView)grdViewDataProductService.GetFocusedRow();
                if (e.Column.FieldName == "AMOUNTINVOICE" ||
                    e.Column.FieldName == "TOTALAMOUNTINVOICEWITHOUTVAT" ||
                    e.Column.FieldName == "INVOICEVAT" ||
                    e.Column.FieldName == "INVOICEAMOUNT" ||
                    e.Column.FieldName == "TOTALAMOUNTINVOICE")
                {
                    Calculate(e.Column.FieldName, item["PRICEPROTECTID"].ToString());
                    decimal decTOTALAMOUNTINVOICE = Convert.ToDecimal(grdViewDataProductService.GetRowCellValue(e.RowHandle, "TOTALAMOUNTINVOICEWITHOUTVAT"));
                    decimal decTOTALAMOUNT = Convert.ToDecimal(grdViewDataProductService.GetRowCellValue(e.RowHandle, "TOTALAMOUNTWITHOUTVAT"));
                    grdViewDataProductService.SetRowCellValue(e.RowHandle, "UNEVEN", decTOTALAMOUNTINVOICE - decTOTALAMOUNT);
                    grdViewDataProductService.RefreshData();
                }

            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi tạo dữ liệu", objExce.ToString());
                SystemErrorWS.Insert("Lỗi tạo dữ liệu", objExce.ToString(), this.Name + " -> grdViewDataProductService_CellValueChanged", DUIInventory_Globals.ModuleName);
            }
        }

        private void Calculate(string strFieldName, string strPriceProtectId)
        {
            if (dtbDataProductService != null && dtbDataProductService.Rows.Count > 0)
            {
                DataRow[] obj = dtbDataProductService.Select("PRICEPROTECTID = '" + strPriceProtectId.Trim() + "' AND ISPROMOTIONPRODUCTMISA = 0");
                if (obj.Count() > 0)
                {
                    DataRow dr = obj[0];
                    var lst = dtbDataProductService.Select("PRODUCTID = '" + dr["PRODUCTID"] + "' AND ISPROMOTIONPRODUCTMISA = 0");//dtbDataGridProductResult.AsEnumerable();
                    var MaxINVOICEVAT = lst.Max(x => x.Field<decimal>("INVOICEVAT"));
                    decimal MaxInvoiceVATCaculate = MaxINVOICEVAT; //VAT để tính toán (Trường hợp không chịu thuế sẽ = 0)
                    if (MaxINVOICEVAT == -1)
                        MaxInvoiceVATCaculate = 0;
                    switch (strFieldName)
                    {
                        case "AMOUNTINVOICE":
                            {
                                decimal decAMOUNTINVOICE = ConvertDecimal(dr["AMOUNTINVOICE"].ToString());

                                // Cập nhật lại toàn bộ giá
                                //var lstDataAMOUNTINVOICE = dtbDataProductService.Select("PRODUCTID = '" + dr["PRODUCTID"] + "' AND ISPROMOTIONPRODUCTMISA = 0", "PRODUCTID");
                                //if (lstDataAMOUNTINVOICE.Any())
                                //{
                                //    for (int i = 0; i < lstDataAMOUNTINVOICE.Count(); i++)
                                //    {
                                //        lstDataAMOUNTINVOICE[i]["AMOUNTINVOICE"] = decAMOUNTINVOICE;
                                //    }
                                //}
                                var lstData = dtbDataProductService.Select("PRODUCTID = '" + dr["PRODUCTID"] + "' AND ISPROMOTIONPRODUCTMISA = 0", "PRODUCTID");
                                if (lstData.Any())
                                {
                                    var SumAMOUNTINVOICE = lstData.Sum(x => x.Field<decimal>("AMOUNTINVOICE") * x.Field<decimal>("QUANTITY"));
                                    var SumQUANTITY = lstData.Sum(x => x.Field<decimal>("QUANTITY"));
                                    for (int i = 0; i < lstData.Count(); i++)
                                    {
                                        lstData[i]["INVOICEVAT"] = MaxINVOICEVAT;
                                        lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"] = ConvertDecimal(lstData[i]["AMOUNTINVOICE"].ToString()) * ConvertDecimal(lstData[i]["QUANTITY"].ToString());
                                        lstData[i]["INVOICEAMOUNT"] = ConvertDecimal(lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString()) * (Convert.ToDecimal(MaxInvoiceVATCaculate) / 100);
                                        lstData[i]["TOTALAMOUNTINVOICE"] = ConvertDecimal(lstData[i]["INVOICEAMOUNT"].ToString()) + ConvertDecimal(lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString());

                                        lstData[i]["UNEVEN"] = ConvertDecimal(lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString()) - ConvertDecimal(lstData[i]["TOTALAMOUNTWITHOUTVAT"].ToString());
                                    }
                                }
                            }
                            break;

                        case "TOTALAMOUNTINVOICEWITHOUTVAT":
                            {
                                decimal decTOTALAMOUNTINVOICEWITHOUTVAT = Math.Round(ConvertDecimal(dr["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString()), 4, MidpointRounding.AwayFromZero);
                                decimal decQuantityRowChange = ConvertDecimal(dr["QUANTITY"].ToString());
                                decimal decMain = 0;
                                if (decQuantityRowChange == 1)
                                {
                                    decMain = decTOTALAMOUNTINVOICEWITHOUTVAT;
                                }
                                else
                                {
                                    decMain = decTOTALAMOUNTINVOICEWITHOUTVAT / decQuantityRowChange;
                                }

                                var lstDataTOTALAMOUNTINVOICEWITHOUTVAT = dtbDataProductService.Select("PRODUCTID = '" + dr["PRODUCTID"] + "' AND ISPROMOTIONPRODUCTMISA = 0", "PRODUCTID");
                                if (lstDataTOTALAMOUNTINVOICEWITHOUTVAT.Any())
                                {
                                    for (int i = 0; i < lstDataTOTALAMOUNTINVOICEWITHOUTVAT.Count(); i++)
                                    {
                                        lstDataTOTALAMOUNTINVOICEWITHOUTVAT[i]["TOTALAMOUNTINVOICEWITHOUTVAT"] = decMain * ConvertDecimal(lstDataTOTALAMOUNTINVOICEWITHOUTVAT[i]["QUANTITY"].ToString());
                                    }
                                }
                                var lstData = dtbDataProductService.Select("PRODUCTID = '" + dr["PRODUCTID"] + "' AND ISPROMOTIONPRODUCTMISA = 0", "PRODUCTID");
                                if (lstData.Any())
                                {
                                    var SumAMOUNTINVOICE = lstData.Sum(x => x.Field<decimal>("TOTALAMOUNTINVOICEWITHOUTVAT"));
                                    var SumQUANTITY = lstData.Sum(x => x.Field<decimal>("QUANTITY"));
                                    for (int i = 0; i < lstData.Count(); i++)
                                    {
                                        lstData[i]["AMOUNTINVOICE"] = SumAMOUNTINVOICE / SumQUANTITY;
                                        lstData[i]["INVOICEVAT"] = MaxINVOICEVAT;
                                        lstData[i]["INVOICEAMOUNT"] = ConvertDecimal(lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString()) * (Convert.ToDecimal(MaxInvoiceVATCaculate) / 100);
                                        lstData[i]["TOTALAMOUNTINVOICE"] = ConvertDecimal(lstData[i]["INVOICEAMOUNT"].ToString()) + ConvertDecimal(lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString());

                                        lstData[i]["UNEVEN"] = ConvertDecimal(lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString()) - ConvertDecimal(lstData[i]["TOTALAMOUNTWITHOUTVAT"].ToString());
                                    }
                                }
                            }
                            break;

                        case "INVOICEVAT":
                            {
                                decimal decINVOICEVAT = ConvertDecimal(dr["INVOICEVAT"].ToString());
                                decimal decVAT = decINVOICEVAT; //VAT mang đi tính toán (Trường hợp không chịu thuế sẽ = 0)
                                if (decINVOICEVAT == -1)
                                    decVAT = 0;
                                var lstDataINVOICEVAT = dtbDataProductService.Select("PRODUCTID = '" + dr["PRODUCTID"] + "' AND ISPROMOTIONPRODUCTMISA = 0", "PRODUCTID");
                                if (lstDataINVOICEVAT.Any())
                                {
                                    for (int i = 0; i < lstDataINVOICEVAT.Count(); i++)
                                    {
                                        lstDataINVOICEVAT[i]["INVOICEVAT"] = decINVOICEVAT;
                                    }
                                }
                                var lstData = dtbDataProductService.Select("PRODUCTID = '" + dr["PRODUCTID"] + "' AND ISPROMOTIONPRODUCTMISA = 0", "PRODUCTID");
                                if (lstData.Any())
                                {
                                    for (int i = 0; i < lstData.Count(); i++)
                                    {
                                        lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"] = ConvertDecimal(lstData[i]["AMOUNTINVOICE"].ToString()) * ConvertDecimal(lstData[i]["QUANTITY"].ToString());
                                        lstData[i]["INVOICEAMOUNT"] = ConvertDecimal(lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString()) * (decVAT / 100);
                                        lstData[i]["TOTALAMOUNTINVOICE"] = ConvertDecimal(lstData[i]["INVOICEAMOUNT"].ToString()) + ConvertDecimal(lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString());

                                        lstData[i]["UNEVEN"] = ConvertDecimal(lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString()) - ConvertDecimal(lstData[i]["TOTALAMOUNTWITHOUTVAT"].ToString());
                                    }
                                }
                            }
                            break;

                        case "INVOICEAMOUNT":
                            {
                                decimal decINVOICEAMOUNT = Math.Round(ConvertDecimal(dr["INVOICEAMOUNT"].ToString()), 4, MidpointRounding.AwayFromZero);
                                decimal decQuantityRowChange = Math.Round(ConvertDecimal(dr["QUANTITY"].ToString()), 4, MidpointRounding.AwayFromZero);
                                decimal decMain = 0;
                                if (decQuantityRowChange == 1)
                                {
                                    decMain = decINVOICEAMOUNT;
                                }
                                else
                                {
                                    decMain = decINVOICEAMOUNT / decQuantityRowChange;
                                }
                                var lstDataINVOICEAMOUNT = dtbDataProductService.Select("PRODUCTID = '" + dr["PRODUCTID"] + "' AND ISPROMOTIONPRODUCTMISA = 0", "PRODUCTID");
                                if (lstDataINVOICEAMOUNT.Any())
                                {
                                    for (int i = 0; i < lstDataINVOICEAMOUNT.Count(); i++)
                                    {
                                        lstDataINVOICEAMOUNT[i]["INVOICEAMOUNT"] = decMain * ConvertDecimal(lstDataINVOICEAMOUNT[i]["QUANTITY"].ToString());
                                        if (MaxInvoiceVATCaculate == 0)
                                        {
                                            lstDataINVOICEAMOUNT[i]["INVOICEAMOUNT"] = 0; // Trường hợp VAT = 0 hoặc không chịu thuế thì tiền thuế luôn = 0
                                            //break; // Dừng luôn không cập nhật những dòng khác
                                        }
                                    }
                                }
                                if (MaxInvoiceVATCaculate > 0) // Chỉ tính lại khi VAT >0
                                {
                                    var lstData = dtbDataProductService.Select("PRODUCTID = '" + dr["PRODUCTID"] + "' AND ISPROMOTIONPRODUCTMISA = 0", "PRODUCTID");
                                    if (lstData.Any())
                                    {
                                        var SumAMOUNTINVOICE = lstData.Sum(x => x.Field<decimal>("AMOUNTINVOICE") * x.Field<decimal>("QUANTITY"));
                                        var SumQUANTITY = lstData.Sum(x => x.Field<decimal>("QUANTITY"));
                                        for (int i = 0; i < lstData.Count(); i++)
                                        {
                                            lstData[i]["AMOUNTINVOICE"] = ConvertDecimal(lstData[i]["INVOICEAMOUNT"].ToString()) / ((Convert.ToDecimal(MaxInvoiceVATCaculate) / 100) * ConvertDecimal(lstData[i]["QUANTITY"].ToString()));
                                            lstData[i]["INVOICEVAT"] = MaxINVOICEVAT;
                                            lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"] = ConvertDecimal(lstData[i]["AMOUNTINVOICE"].ToString()) * ConvertDecimal(lstData[i]["QUANTITY"].ToString());
                                            lstData[i]["TOTALAMOUNTINVOICE"] = ConvertDecimal(lstData[i]["INVOICEAMOUNT"].ToString()) + ConvertDecimal(lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString());

                                            //lstData[i]["UNEVEN"] = ConvertDecimal(lstData[i]["VAT"].ToString()) != 0 ? 0 : Math.Round(((ConvertDecimal(lstData[i]["INVOICEVAT"].ToString()) - ConvertDecimal(lstData[i]["VAT"].ToString())) / ConvertDecimal(lstData[i]["VATPERCENT"].ToString())) * ConvertDecimal(lstData[i]["INPUTPRICE"].ToString()) * ConvertDecimal(lstData[i]["QUANTITY"].ToString()), 4, MidpointRounding.AwayFromZero);
                                            lstData[i]["UNEVEN"] = ConvertDecimal(lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString()) - ConvertDecimal(lstData[i]["TOTALAMOUNTWITHOUTVAT"].ToString());
                                        }
                                    }
                                }
                            }
                            break;

                        case "TOTALAMOUNTINVOICE":
                            {
                                decimal decTOTALAMOUNTINVOICE = Math.Round(ConvertDecimal(dr["TOTALAMOUNTINVOICE"].ToString()), 4, MidpointRounding.AwayFromZero);
                                decimal decQuantityRowChange = Math.Round(ConvertDecimal(dr["QUANTITY"].ToString()), 4, MidpointRounding.AwayFromZero);
                                decimal decMain = 0;
                                if (decQuantityRowChange == 1)
                                {
                                    decMain = decTOTALAMOUNTINVOICE;
                                }
                                else
                                {
                                    decMain = decTOTALAMOUNTINVOICE / decQuantityRowChange;
                                }
                                var lstDataTOTALAMOUNTINVOICE = dtbDataProductService.Select("PRODUCTID = '" + dr["PRODUCTID"] + "' AND ISPROMOTIONPRODUCTMISA = 0", "PRODUCTID");
                                if (lstDataTOTALAMOUNTINVOICE.Any())
                                {
                                    for (int i = 0; i < lstDataTOTALAMOUNTINVOICE.Count(); i++)
                                    {
                                        lstDataTOTALAMOUNTINVOICE[i]["TOTALAMOUNTINVOICE"] = decMain * ConvertDecimal(lstDataTOTALAMOUNTINVOICE[i]["QUANTITY"].ToString());
                                    }
                                }
                                var lstData = dtbDataProductService.Select("PRODUCTID = '" + dr["PRODUCTID"] + "' AND ISPROMOTIONPRODUCTMISA = 0", "PRODUCTID");
                                if (lstData.Any())
                                {
                                    var SumQUANTITY = lstData.Sum(x => x.Field<decimal>("QUANTITY"));
                                    var SumTOTALAMOUNTINVOICE = lstData.Sum(x => x.Field<decimal>("TOTALAMOUNTINVOICE"));

                                    var SumAMOUNTINVOICE = (SumTOTALAMOUNTINVOICE / ((decimal)(MaxInvoiceVATCaculate + 100) / 100));
                                    for (int i = 0; i < lstData.Count(); i++)
                                    {
                                        lstData[i]["AMOUNTINVOICE"] = SumAMOUNTINVOICE / SumQUANTITY;
                                        lstData[i]["INVOICEVAT"] = MaxINVOICEVAT;
                                        lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"] = Convert.ToDecimal(lstData[i]["AMOUNTINVOICE"]) * ConvertDecimal(lstData[i]["QUANTITY"].ToString());
                                        lstData[i]["INVOICEAMOUNT"] = Convert.ToDecimal(lstData[i]["TOTALAMOUNTINVOICE"]) - (Convert.ToDecimal(lstData[i]["AMOUNTINVOICE"]) * Convert.ToDecimal(lstData[i]["QUANTITY"].ToString()));

                                        lstData[i]["UNEVEN"] = ConvertDecimal(lstData[i]["TOTALAMOUNTINVOICEWITHOUTVAT"].ToString()) - ConvertDecimal(lstData[i]["TOTALAMOUNTWITHOUTVAT"].ToString());
                                    }
                                }
                            }
                            break;
                    }
                }
            }
        }
        private decimal ConvertDecimal(string value)
        {
            decimal ret = -1;
            decimal.TryParse(value, out ret);
            return ret;
        }

        private void cboInvoiceTransType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboInvoiceTransType.ColumnID > 0)
            {
                DataRow[] rowGroup = dtbInvoiceTransactionType.Select("INVOICETRANSTYPEID=" + cboInvoiceTransType.ColumnID);
                grdViewDataProductService.Columns["GROUPSERVICEID"].Visible = Convert.ToBoolean(rowGroup[0]["ISTAXDECLARATION"]);
            }
            else
                grdViewDataProductService.Columns["GROUPSERVICEID"].Visible = false;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa hóa đơn?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                string DeletedReason = ShowReason("Nhập lý do xóa hóa đơn", 1000);
                if (DeletedReason == "-1" || string.IsNullOrWhiteSpace(DeletedReason))
                    return;
                VAT_PInvoice objVATInvoice = new VAT_PInvoice();
                objVATInvoice.VATPInvoiceID = strVATPInvoiceID.Trim();
                objVATInvoice.DeleteReason = DeletedReason;
                var objResultMessage = objPLCPriceProtect.Delete_VAT_Invoice(objVATInvoice);
                if (objResultMessage.IsError)
                {
                    if (objResultMessage.Message == "Thông báo")
                        MessageBoxObject.ShowWarningMessage(this, objResultMessage.MessageDetail);
                    else
                        MessageBoxObject.ShowWarningMessage(this, "Lỗi xóa dữ liệu", objResultMessage.MessageDetail);
                }
                else
                {
                    bolRefresh = true;
                    MessageBox.Show("Xóa hóa đơn giá trị gia tăng thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
        }

        private string ShowReason(string strTitle, int MaxLengthString)
        {
            string strReasion = "-1";
            Library.AppControl.FormCommon.frmInputString frm = new Library.AppControl.FormCommon.frmInputString();
            frm.MaxLengthString = MaxLengthString;
            //frm.IsAllowEmpty = false;
            frm.Text = strTitle;
            frm.ShowDialog();
            if (frm.IsAccept)
            {
                strReasion = frm.ContentString;
            }

            return strReasion;
        }

        public void LoadVATInformationWhenLoading(string strVATInvoiceID)
        {
            strVATPInvoiceID = strVATInvoiceID;
            IsLoadDataFormLoad = true;
        }

        public bool LoadVATInformation(string strVATInvoiceID)
        {
            VAT_PInvoice objVATInvoice = new VAT_PInvoice();
            DataTable dtbProduct = null;
            DataTable dtbAttachment = null;
            ResultMessage objResultMessage = objPLCPriceProtect.LoadInfo_VATPInvoice(ref objVATInvoice, ref dtbProduct, ref dtbAttachment, strVATInvoiceID);
            if (objResultMessage.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, objResultMessage.Message, objResultMessage.MessageDetail);
                return false;
            }

            objStateForm = StateForm.Update;
            if (objVATInvoice.IsDelete)
                objStateForm = StateForm.View;
            InitializeStateForm();

            strVATPInvoiceID = objVATInvoice.VATPInvoiceID.Trim();

            // KHACH HANG
            CustomerID = objVATInvoice.VATCustomerID;
            txtTaxCustomerName.Text = objVATInvoice.CustomerName;
            txtTaxCustomerTaxCode.Text = objVATInvoice.CustomerTaxID;
            txtTaxCustomerAddress.Text = objVATInvoice.TaxCustomerAddress;
            txtTaxCustomerSalerName.Text = objVATInvoice.SELLER;
            txtTaxCustomerDescription.Text = objVATInvoice.Description;

            FormTypeID = objVATInvoice.FormTypeID;

            TypeID = objVATInvoice.TypeID;
            intTransactionGroupId = objVATInvoice.InvoiceType;
            LoadInvoiceTranstype();

            //strInvoiceNOcheck = intTransactionGroupId == 0 ? "" : objVATInvoice.VATInvoiceMainID;

            cboBranch.SetValue(objVATInvoice.BranchID);
            cboBranch_SelectionChangeCommitted(null, null);
            cboStore.SetValue(objVATInvoice.CreatedStoreID);
            cboInvoiceTransType.SetValue(objVATInvoice.InvoiceTRANSTypeID);
            DataRow[] rowGroup = dtbInvoiceTransactionType.Select("INVOICETRANSTYPEID=" + objVATInvoice.InvoiceTRANSTypeID);
            grdViewDataProductService.Columns["GROUPSERVICEID"].Visible = Convert.ToBoolean(rowGroup[0]["ISTAXDECLARATION"]);
            txtLstCustomerName.Text = objVATInvoice.OutputVoucherInfo;
           
            dtmCreateDate.EditValue = objVATInvoice.CreateDate;
            txtPriceProtectNo.Text = objVATInvoice.PriceProtectNo;
            txtVATPInvoiceVoucherNo.Text = objVATInvoice.PInvoiceVoucherNO;
            txtDenominator.Text = objVATInvoice.Denominator.Trim();
            txtInvoiceSymbol.Text = objVATInvoice.InvoiceSymbol.Trim();
            txtInvoiceNo.Text = objVATInvoice.InvoiceNO.ToString();
            dtmInvoiceDate.EditValue = objVATInvoice.InvoiceDate;
            cboSaleman.UserName = objVATInvoice.PurchaseMANID;
            cboPayableType.SetValue(objVATInvoice.PayableTypeID);
            dtmDueDate.EditValue = objVATInvoice.InvoiceDUEDate;
            // Thông tin Hoa Don
            cboCurrencyUnit.SelectedValue = objVATInvoice.CurrencyUnit;
            txtCurrencyExchange.Value = objVATInvoice.CurrencyExchange;

            if (dtbProduct != null)
            {
                InitializeDataProductService(dtbProduct, strProductID);
                grdDataProductService.DataSource = dtbDataProductService;
            }
            if (dtbAttachment != null)
            {
                InitializeDataAttachment(dtbAttachment);
                grdDataAttachment.DataSource = dtbDataAttachment;
            }

            if (objVATInvoice.IsDelete)
            {
                btnDelete.Enabled = false;
                btnUpdate.Enabled = false;
                btnDelete.Visible = false;
                btnUpdate.Visible = false;
                grdViewDataProductService.OptionsBehavior.Editable = false;
                grdViewDataHistory.OptionsBehavior.Editable = false;
                grdViewDataAttachment.OptionsBehavior.Editable = false;
                mnuAttachment.Enabled = false;
                mnuProductService.Enabled = false;
            }
            return true;
        }

        private void mnuItemExportExcelProduct_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdViewDataProductService.RowCount > 0)
                {
                    Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdDataProductService);
                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi xuất excel sản phẩm hóa đơn giá trị gia tăng");
                SystemErrorWS.Insert("Lỗi xuất excel sản phẩm hóa đơn giá trị gia tăng", objExce.ToString(), this.Name + " -> mnuItemExportExcelProduct_Click", DUIInventory_Globals.ModuleName);
            }
        }
    }
}