﻿using DevExpress.XtraGrid;
using Library.AppCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmExplainUnEvent : Form
    {
        public frmExplainUnEvent()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }
        private string strPermission_AccessExplainUnEvent = "INV_INVENTORY_ACCESSEVENT";
        public string strInventoryID = string.Empty;
        public string strUsername = string.Empty;
        public DateTime dtmInventoryDate;
        public int intCreatedStoreID;
        public bool bolIsReviewed = false;
        public bool bolIsUpdateUnEvent = false;
        public bool bolIsExplainUnEvent = false;
        public bool bolIsAccessExplainUnEvent = false;
        public bool bolIsDeleted = false;
        public DataTable tblUnEventTable = null;
        private ERP.Inventory.PLC.Inventory.PLCInventory objPLCInventory = new PLC.Inventory.PLCInventory();
        private bool bolIsSaveChanged_IsExplainUnEvent = false;
        public bool bolIsSave = false;
        public int intIsPermitExplainUnEvent = -1;

        private void frmExplainUnEvent_Load(object sender, EventArgs e)
        {
            btnAccess.Enabled = !bolIsAccessExplainUnEvent&& bolIsExplainUnEvent && SystemConfig.objSessionUser.IsPermission(strPermission_AccessExplainUnEvent);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewProducts);


            DataTable dtbData = objPLCInventory.SearchDataCauseGroups();
            repCauseGroups.DataSource = dtbData;

            DataView dtv = tblUnEventTable.AsDataView();
            dtv.Sort = "PRODUCTID, ERP_PRODUCTSTATUSID";
            tblUnEventTable = dtv.ToTable();
            grdCtlProducts.DataSource = tblUnEventTable ;
            //CustomFlex();

            btnSave.Enabled = false;

            if (intIsPermitExplainUnEvent == 0)
            {
                btnSave.Enabled = false;
                return;
            }


            if (bolIsReviewed && bolIsUpdateUnEvent && (!bolIsExplainUnEvent))
            {
                btnSave.Enabled = true;
            }
            if (bolIsUpdateUnEvent)
            {
                if ((!bolIsReviewed) && (!bolIsExplainUnEvent) && (!btnSave.Enabled))
                {
                    btnSave.Enabled = true;
                }
            }
            if (bolIsReviewed && intIsPermitExplainUnEvent == 1)
            {
                btnSave.Enabled = true;
            }

            if (bolIsDeleted)
            {
                btnSave.Enabled = false;
            }
        }

        //private void CustomFlex()
        //{
        //    flexData.Rows.Fixed = 2;
        //    for (int i = 0; i < flexData.Cols.Count; i++)
        //    {
        //        flexData.Cols[i].Visible = false;
        //        flexData.Cols[i].AllowEditing = false;
        //    }


        //    Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexData, false, "ERP_PRODUCTSTATUSID", "INV_PRODUCTSTATUSID", "ISSYSTEMERROR");
        //    Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexData, true,
        //        "PRODUCTID,PRODUCTNAME,ERP_IMEI,ERP_QUANTITY,ERP_PRODUCTSTATUSNAME,INV_IMEI,INV_QUANTITY,INV_PRODUCTSTATUSNAME,DIFFERENT,UNEVENTEXPLAIN,QUANTITY_INPUT,QUANTITY_SALE,QUANTITY_STORECHANGE,QUANTITY_OUTPUT,QUANTITY_DIFFERENT,EXPLAINSYSTEMNOTE,EXPLAINNOTE");
        //    Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowMerging(flexData, "PRODUCTID,PRODUCTNAME,DIFFERENT,UNEVENTEXPLAIN,EXPLAINSYSTEMNOTE,EXPLAINNOTE", true);

        //    flexData[0, "PRODUCTID"] = "Mã sản phẩm";
        //    flexData[1, "PRODUCTID"] = "Mã sản phẩm";

        //    flexData[0, "PRODUCTNAME"] = "Tên sản phẩm";
        //    flexData[1, "PRODUCTNAME"] = "Tên sản phẩm";

        //    flexData[0, "ERP_IMEI"] = "ERP";
        //    flexData[1, "ERP_IMEI"] = "IMEI";

        //    flexData[0, "ERP_QUANTITY"] = "ERP";
        //    flexData[1, "ERP_QUANTITY"] = "Số lượng";

        //    flexData[0, "ERP_PRODUCTSTATUSNAME"] = "ERP";
        //    flexData[1, "ERP_PRODUCTSTATUSNAME"] = "Trạng thái";

        //    flexData[0, "INV_IMEI"] = "Kiểm kê";
        //    flexData[1, "INV_IMEI"] = "IMEI";

        //    flexData[0, "INV_QUANTITY"] = "Kiểm kê";
        //    flexData[1, "INV_QUANTITY"] = "Số lượng";

        //    flexData[0, "INV_PRODUCTSTATUSNAME"] = "Kiểm kê";
        //    flexData[1, "INV_PRODUCTSTATUSNAME"] = "Trạng thái";

        //    flexData[0, "DIFFERENT"] = "Chênh lệch";
        //    flexData[1, "DIFFERENT"] = "Chênh lệch";

        //    flexData[0, "QUANTITY_INPUT"] = "Giải trình chênh lệch";
        //    flexData[1, "QUANTITY_INPUT"] = "Nhập";

        //    flexData[0, "QUANTITY_SALE"] = "Giải trình chênh lệch";
        //    flexData[1, "QUANTITY_SALE"] = "Xuất bán";

        //    flexData[0, "QUANTITY_STORECHANGE"] = "Giải trình chênh lệch";
        //    flexData[1, "QUANTITY_STORECHANGE"] = "Chuyển kho";

        //    flexData[0, "QUANTITY_OUTPUT"] = "Giải trình chênh lệch";
        //    flexData[1, "QUANTITY_OUTPUT"] = "Xuất khác";

        //    flexData[0, "QUANTITY_DIFFERENT"] = "Giải trình chênh lệch";
        //    flexData[1, "QUANTITY_DIFFERENT"] = "Chênh lệch";

        //    flexData[0, "UNEVENTEXPLAIN"] = "Giải trình chênh lệch";
        //    flexData[1, "UNEVENTEXPLAIN"] = "Giải trình chênh lệch";

        //    flexData[0, "EXPLAINSYSTEMNOTE"] = "Giải trình chênh lệch hệ thống";
        //    flexData[1, "EXPLAINSYSTEMNOTE"] = "Giải trình chênh lệch hệ thống";

        //    flexData[0, "EXPLAINNOTE"] = "Ghi chú";
        //    flexData[1, "EXPLAINNOTE"] = "Ghi chú";

        //    Library.AppCore.LoadControls.C1FlexGridObject.SetColumnWidth(flexData,
        //        "PRODUCTID", 120,
        //        "PRODUCTNAME", 220,
        //        "ERP_IMEI", 130,
        //        "ERP_QUANTITY", 70,
        //        "ERP_PRODUCTSTATUSNAME", 130,
        //        "INV_IMEI", 130,
        //        "INV_QUANTITY", 70,
        //        "INV_PRODUCTSTATUSNAME", 130,
        //        "DIFFERENT", 70
        //        , "UNEVENTEXPLAIN", 220
        //        , "EXPLAINNOTE", 220
        //        , "QUANTITY_INPUT", 70
        //        , "QUANTITY_SALE", 70
        //        , "QUANTITY_STORECHANGE", 70
        //        , "QUANTITY_OUTPUT", 70
        //        , "QUANTITY_DIFFERENT", 70
        //        , "EXPLAINSYSTEMNOTE", 220
        //        );

        //    flexData.Cols["UNEVENTEXPLAIN"].AllowEditing = true;
        //    flexData.Cols["EXPLAINNOTE"].AllowEditing = true;

        //    flexData.Cols["QUANTITY_INPUT"].AllowEditing = true;
        //    flexData.Cols["QUANTITY_SALE"].AllowEditing = true;
        //    flexData.Cols["QUANTITY_STORECHANGE"].AllowEditing = true;
        //    flexData.Cols["QUANTITY_OUTPUT"].AllowEditing = true;
        //    // flexData.Cols["QUANTITY_DIFFERENT"].AllowEditing = true;

        //    C1.Win.C1FlexGrid.CellStyle style = flexData.Styles.Add("flexStyle");
        //    style.WordWrap = true;
        //    style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Bold);
        //    style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
        //    C1.Win.C1FlexGrid.CellRange range = flexData.GetCellRange(0, 0, flexData.Rows.Fixed - 1, flexData.Cols.Count - 1);
        //    range.Style = style;
        //    flexData.Rows[0].AllowMerging = true;

        //    Library.AppCore.LoadControls.C1FlexGridObject.AddFormSearchProduct(flexData, flexData.Cols["PRODUCTID"].Index, flexData.Cols["PRODUCTNAME"].Index, flexData.Cols["ERP_IMEI"].Index);
        //}

        private void btnSave_Click(object sender, EventArgs e)
        {
            bolIsSave = true;
            btnSave.Enabled = false;
            grdViewProducts.CloseEditor();
            grdViewProducts.UpdateCurrentRow();
            DataTable dtbInventoryUnEvent = grdCtlProducts.DataSource as DataTable;
            var varResult = dtbInventoryUnEvent.AsEnumerable()
                .Where(row => string.IsNullOrEmpty((row.Field<object>("CAUSEGROUPSLIST") ?? "").ToString().Trim())
                              || string.IsNullOrEmpty((row.Field<object>("UNEVENTEXPLAIN") ?? "").ToString().Trim()));
            if (varResult.Any())
            {
                MessageBox.Show(this, "Có chênh lệch chưa được giải trình! Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnSave.Enabled = true;
                return;
            }
            ERP.Inventory.PLC.Inventory.WSInventory.Inventory objInventory = new PLC.Inventory.WSInventory.Inventory();
            objInventory.InventoryID = strInventoryID;
            objInventory.InventoryDate = dtmInventoryDate;
            objInventory.CreatedStoreID = intCreatedStoreID;
            dtbInventoryUnEvent.TableName = "UNEVENTTABLE";
            objPLCInventory.UpdateExplainUnEvent(strInventoryID, dtbInventoryUnEvent);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                btnSave.Enabled = true;
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            bolIsExplainUnEvent = true;
            btnSave.Enabled = true;
            btnAccess.Enabled = !bolIsAccessExplainUnEvent &&  bolIsExplainUnEvent && SystemConfig.objSessionUser.IsPermission(strPermission_AccessExplainUnEvent);
            MessageBox.Show(this, "Cập nhật giải trình chênh lệch kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void grdViewProducts_HiddenEditor(object sender, EventArgs e)
        {
            int rowHanlde = grdViewProducts.FocusedRowHandle;
            if (rowHanlde == GridControl.AutoFilterRowHandle)
                return;
            if (grdViewProducts.FocusedColumn.FieldName == "QUANTITY_INPUT"
                || grdViewProducts.FocusedColumn.FieldName == "QUANTITY_SALE"
                || grdViewProducts.FocusedColumn.FieldName == "QUANTITY_STORECHANGE"
                || grdViewProducts.FocusedColumn.FieldName == "QUANTITY_OUTPUT"
                )
            {
                grdViewProducts.CloseEditor();
                grdViewProducts.UpdateCurrentRow();
                DataRow rUnEvent = grdViewProducts.GetFocusedDataRow();
                string strValue = (rUnEvent[grdViewProducts.FocusedColumn.FieldName] ?? "").ToString().Trim();
                if (string.IsNullOrEmpty(strValue))
                    rUnEvent[grdViewProducts.FocusedColumn.FieldName] = 0;
                rUnEvent["QUANTITY_DIFFERENT"] =
                      Convert.ToDecimal(rUnEvent["ERP_QUANTITY"]) + Convert.ToDecimal(rUnEvent["QUANTITY_INPUT"]) - (Convert.ToDecimal(rUnEvent["INV_QUANTITY"]) + Convert.ToDecimal(rUnEvent["QUANTITY_SALE"]) + Convert.ToDecimal(rUnEvent["QUANTITY_OUTPUT"]) + Convert.ToDecimal(rUnEvent["QUANTITY_STORECHANGE"]))
                     ;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult drsError = MessageBox.Show(this, "Khi xác nhận giải trình bạn sẽ không thể cập nhập lại dữ liệu giải trình. Bạn có muốn xác nhận?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (drsError != System.Windows.Forms.DialogResult.Yes)
                return;
            bolIsSave = true;
            btnAccess.Enabled = false;
            List<string> ListUserName = new List<string>();
                ListUserName.Add(strUsername);
            Library.Login.frmCertifyLogin frmCertifyLogin = new Library.Login.frmCertifyLogin("Xác nhận giải trình", "!\nChỉ nhân viên trong danh sách duyệt mới được xác nhận!", ListUserName, null, true, true);
            frmCertifyLogin.ShowDialog(this);
            if (!frmCertifyLogin.IsDisposed && !frmCertifyLogin.IsCorrect)
            {
                btnAccess.Enabled = true;
                return;
            }
            grdViewProducts.CloseEditor();
            grdViewProducts.UpdateCurrentRow();
            DataTable dtbInventoryUnEvent = grdCtlProducts.DataSource as DataTable;
            ERP.Inventory.PLC.Inventory.WSInventory.Inventory objInventory = new PLC.Inventory.WSInventory.Inventory();
            objInventory.InventoryID = strInventoryID;
            objInventory.InventoryDate = dtmInventoryDate;
            objInventory.CreatedStoreID = intCreatedStoreID;
            dtbInventoryUnEvent.TableName = "UNEVENTTABLE";
            objPLCInventory.AccessExplainUnEvent(strInventoryID, strUsername);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                btnAccess.Enabled = true;
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            bolIsExplainUnEvent = true;
            bolIsAccessExplainUnEvent = true;
            btnSave.Enabled = false;
        }

        private void grdViewProducts_ShownEditor(object sender, EventArgs e)
        {
        }

        private void grdViewProducts_ShowingEditor(object sender, CancelEventArgs e)
        {
            int rowHanlde = grdViewProducts.FocusedRowHandle;
            if (rowHanlde == GridControl.AutoFilterRowHandle)
                return;
            if (grdViewProducts.FocusedColumn.FieldName == "QUANTITY_INPUT"
                || grdViewProducts.FocusedColumn.FieldName == "QUANTITY_SALE"
                || grdViewProducts.FocusedColumn.FieldName == "QUANTITY_STORECHANGE"
                || grdViewProducts.FocusedColumn.FieldName == "QUANTITY_OUTPUT"
                )
            {
                grdViewProducts.CloseEditor();
                grdViewProducts.UpdateCurrentRow();
                DataRow rUnEvent = grdViewProducts.GetFocusedDataRow();
                string strValue = (rUnEvent["ISSYSTEMERROR"] ?? "").ToString().Trim();
                if (strValue == "1")
                    e.Cancel = true;
            }
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdCtlProducts);
        }
    }
}
