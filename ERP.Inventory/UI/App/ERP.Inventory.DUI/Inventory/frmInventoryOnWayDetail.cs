﻿using ERP.Inventory.PLC.WSInventoryOnWay;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmInventoryOnWayDetail : Form
    {
        private string strPermission_Update = "INVENTORYONWAY_UPDATE";
        private INV_InventoryOnWay objINV_InventoryOnWay = null;

        public INV_InventoryOnWay INV_InventoryOnWay
        {
            get { return objINV_InventoryOnWay; }
            set { objINV_InventoryOnWay = value; }
        }

        public frmInventoryOnWayDetail()
        {
            InitializeComponent();
        }

        private void frmInventoryOnWayDetail_Load(object sender, EventArgs e)
        {
            if(objINV_InventoryOnWay != null)
            {
                if (objINV_InventoryOnWay.InventoryOnWayDetailList != null)
                    gridDataDetail.DataSource = objINV_InventoryOnWay.InventoryOnWayDetailList.ToList();

                if (objINV_InventoryOnWay.InventoryOnWay_AttachMentList != null)
                    grdAttachment.DataSource = objINV_InventoryOnWay.InventoryOnWay_AttachMentList.ToList();
            }

            FormatGridDetail();
        }

        private bool FormatGridDetail()
        {
            try
            {
                string[] fieldNames = new string[] { "ProductID", "ProductName", "Quantity", "IMEI", "CheckQuantity", "Note"};
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(gridDataDetail, true, false, true, true, fieldNames);
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(gridViewDataDetail);
                gridViewDataDetail.Columns["ProductID"].Caption = "Mã sản phẩm";
                gridViewDataDetail.Columns["ProductName"].Caption = "Tên sản phẩm";
                gridViewDataDetail.Columns["Quantity"].Caption = "Số lượng";
                gridViewDataDetail.Columns["IMEI"].Caption = "IMEI";
                gridViewDataDetail.Columns["CheckQuantity"].Caption = "Số lượng kiểm kê";
                gridViewDataDetail.Columns["Note"].Caption = "Ghi chú";
                
                gridViewDataDetail.OptionsView.ColumnAutoWidth = false;
                gridViewDataDetail.Columns["ProductID"].Width = 100;
                gridViewDataDetail.Columns["ProductName"].Width = 150;
                gridViewDataDetail.Columns["Quantity"].Width = 120;
                gridViewDataDetail.Columns["IMEI"].Width = 120;
                gridViewDataDetail.Columns["CheckQuantity"].Width = 120;
                gridViewDataDetail.Columns["Note"].Width = 200;

                gridViewDataDetail.Columns["Quantity"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                gridViewDataDetail.Columns["Quantity"].DisplayFormat.FormatString = "#,###.##";
                gridViewDataDetail.Columns["CheckQuantity"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                gridViewDataDetail.Columns["CheckQuantity"].DisplayFormat.FormatString = "#,###.##";

                gridViewDataDetail.Columns["CheckQuantity"].OptionsColumn.AllowEdit = SystemConfig.objSessionUser.IsPermission(strPermission_Update);
                gridViewDataDetail.Columns["Note"].OptionsColumn.AllowEdit = SystemConfig.objSessionUser.IsPermission(strPermission_Update);
                
                gridViewDataDetail.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                gridViewDataDetail.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                gridViewDataDetail.OptionsView.ShowAutoFilterRow = true;
                gridViewDataDetail.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                gridViewDataDetail.OptionsView.ShowFooter = false;
                gridViewDataDetail.OptionsView.ShowGroupPanel = false;
                gridViewDataDetail.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                gridViewDataDetail.ColumnPanelRowHeight = 40;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới danh sách chi tiết phiếu chuyển kho chưa được nhận.");
                SystemErrorWS.Insert("Lỗi định dạng lưới danh sách chi tiết phiếu chuyển kho chưa được nhận.", objExce.ToString(), this.Name + " -> FormatGridDetail", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void mnuAddAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog objOpenFileDialog = new OpenFileDialog();
                objOpenFileDialog.Multiselect = true;
                objOpenFileDialog.Filter = "Doc Files (*.doc, *.docx)|*.doc; *docx|Excel Files (*.xls, *.xlsx)|*.xls; *.xlsx|PDF Files(*.pdf)|*.pdf|Compressed Files(*.zip, *.rar)|*.zip;*.rar|Image Files(*.jpg, *.jpe, *.jpeg, *.gif, *.png, *.bmp, *.tif, *.tiff)|*.jpg; *.jpe; *.jpeg; *.gif; *.png; *.bmp; *.tif; *.tiff";
                if (objOpenFileDialog.ShowDialog() == DialogResult.OK)
                {
                    String[] strLocalFilePaths = objOpenFileDialog.FileNames;
                    List<INV_InventoryOnWay_AttachMent> lstINV_InventoryOnWay_AttachMent = null;
                    foreach (string strLocalFilePath in strLocalFilePaths)
                    {
                        INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent = new INV_InventoryOnWay_AttachMent();
                        FileInfo objFileInfo = new FileInfo(strLocalFilePath);
                        if(objINV_InventoryOnWay.InventoryOnWay_AttachMentList != null)
                        {
                            lstINV_InventoryOnWay_AttachMent = objINV_InventoryOnWay.InventoryOnWay_AttachMentList.ToList();
                            var ListCheck = from o in objINV_InventoryOnWay.InventoryOnWay_AttachMentList
                                            where o.AttachMENTName == objFileInfo.Name
                                            select o;
                            if (ListCheck.Count() > 0)
                                continue;
                        }
                        else
                        {
                            lstINV_InventoryOnWay_AttachMent = new List<INV_InventoryOnWay_AttachMent>();
                        }
                        
                        decimal FileSize = Math.Round(objFileInfo.Length / Convert.ToDecimal((1024 * 1024)), 2); //dung lượng tính theo MB
                        decimal FileSizeLimit = 2;//MB
                        if (FileSize > FileSizeLimit)
                        {
                            MessageBox.Show(this, "Vui lòng chọn tập tin đính kèm dung lượng không vượt quá 2 MB", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        objINV_InventoryOnWay_AttachMent.AttachMENTName = objFileInfo.Name;
                        objINV_InventoryOnWay_AttachMent.StoreChangeOrderID = objINV_InventoryOnWay.StoreChangeOrderID;
                        objINV_InventoryOnWay_AttachMent.AttachMENTPath = strLocalFilePath;
                        objINV_InventoryOnWay_AttachMent.CreatedUser = SystemConfig.objSessionUser.UserName;
                        lstINV_InventoryOnWay_AttachMent.Add(objINV_InventoryOnWay_AttachMent);

                        grdAttachment.DataSource = lstINV_InventoryOnWay_AttachMent;
                        objINV_InventoryOnWay.InventoryOnWay_AttachMentList = lstINV_InventoryOnWay_AttachMent.ToArray();

                    }
                    grdAttachment.RefreshDataSource();
                }
            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, "Lỗi thêm tập tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi thêm tập tin", objExce.ToString(), this.Name + " -> mnuAddAttachment_Click", DUIInventory_Globals.ModuleName);
            }
        }

        private void mnuItemView_Click(object sender, EventArgs e)
        {
            if (grvAttachment.FocusedRowHandle < 0)
                return;
            INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent = grvAttachment.GetFocusedRow() as INV_InventoryOnWay_AttachMent;
            string strFileName = string.Empty;
            if (string.IsNullOrEmpty(objINV_InventoryOnWay_AttachMent.INVENToRYONWAYAttachMENTID))
            {
                strFileName = Path.GetFullPath(objINV_InventoryOnWay_AttachMent.AttachMENTPath);
            }
            else
            {
                strFileName = LoadProcess(objINV_InventoryOnWay_AttachMent.FileID, objINV_InventoryOnWay_AttachMent.AttachMENTName);
            }
            if (!string.IsNullOrWhiteSpace(strFileName))
            {
                Process.Start(strFileName);
            }
        }

        private void mnuItemDownload_Click(object sender, EventArgs e)
        {
            if (grvAttachment.FocusedRowHandle < 0)
                return;
            INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent = grvAttachment.GetFocusedRow() as INV_InventoryOnWay_AttachMent;
            if (objINV_InventoryOnWay_AttachMent == null)
                return;

            if (string.IsNullOrEmpty(objINV_InventoryOnWay_AttachMent.INVENToRYONWAYAttachMENTID))
            {
                MessageBox.Show(this, "File chưa cập nhật trên server!");
                return;
            }
            DownloadFile(objINV_InventoryOnWay_AttachMent.FileID, objINV_InventoryOnWay_AttachMent.AttachMENTName);
        }

        private void mnuDelAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                if (grvAttachment.FocusedRowHandle < 0)
                    return;
                INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent = grvAttachment.GetFocusedRow() as INV_InventoryOnWay_AttachMent;
                if (objINV_InventoryOnWay_AttachMent != null && objINV_InventoryOnWay_AttachMent != null)
                {
                    if (MessageBox.Show(this, "Bạn muốn xóa file đính kèm này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        List<INV_InventoryOnWay_AttachMent> lstINV_InventoryOnWay_AttachMent_Del = null;
                        if (objINV_InventoryOnWay.InventoryOnWay_AttachMent_DelList == null)
                            lstINV_InventoryOnWay_AttachMent_Del = new List<INV_InventoryOnWay_AttachMent>();
                        else
                            lstINV_InventoryOnWay_AttachMent_Del = objINV_InventoryOnWay.InventoryOnWay_AttachMent_DelList.ToList();

                        lstINV_InventoryOnWay_AttachMent_Del.Add(objINV_InventoryOnWay_AttachMent);
                        objINV_InventoryOnWay.InventoryOnWay_AttachMent_DelList = lstINV_InventoryOnWay_AttachMent_Del.ToArray();
                        if (objINV_InventoryOnWay.InventoryOnWay_AttachMentList != null)
                        {
                            List<INV_InventoryOnWay_AttachMent> lstINV_InventoryOnWay_AttachMent = objINV_InventoryOnWay.InventoryOnWay_AttachMentList.ToList();
                            lstINV_InventoryOnWay_AttachMent.Remove(objINV_InventoryOnWay_AttachMent);
                            objINV_InventoryOnWay.InventoryOnWay_AttachMentList = lstINV_InventoryOnWay_AttachMent.ToArray();
                            grdAttachment.DataSource = lstINV_InventoryOnWay_AttachMent;
                        }

                        grdAttachment.RefreshDataSource();
                    }
                }
            }
            catch (Exception objExce)
            {
                MessageBox.Show(this, "Lỗi xóa tập tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi xóa tập tin", objExce.ToString(), this.Name + " -> mnuDelAttachment_Click", DUIInventory_Globals.ModuleName);
            }
        }
        public bool DownloadFile(string strFileID, string strFileName)
        {
            try
            {
                SaveFileDialog dlgSaveFile = new SaveFileDialog();
                dlgSaveFile.Title = "Lưu file tải về";
                dlgSaveFile.FileName = strFileName;
                string strExtension = Path.GetExtension(strFileName);
                switch (strExtension)
                {
                    case ".xls":
                    case ".xlsx":
                        dlgSaveFile.Filter = "Excel Worksheets(*.xls, *.xlsx)|*.xls; *.xlsx";
                        break;
                    case ".doc":
                    case ".docx":
                        dlgSaveFile.Filter = "Word Documents(*.doc, *.docx)|*.doc; *.docx";
                        break;
                    case ".pdf":
                        dlgSaveFile.Filter = "PDF Files(*.pdf)|*.pdf";
                        break;
                    case ".zip":
                    case ".rar":
                        dlgSaveFile.Filter = "Compressed Files(*.zip, *.rar)|*.zip;*.rar";
                        break;
                    case ".jpg":
                    case ".jpe":
                    case ".jpeg":
                    case ".gif":
                    case ".png":
                    case ".bmp":
                    case ".tif":
                    case ".tiff":
                        dlgSaveFile.Filter = "Image Files(*.jpg, *.jpe, *.jpeg, *.gif, *.png, *.bmp, *.tif, *.tiff)|*.jpg; *.jpe; *.jpeg; *.gif; *.png; *.bmp; *.tif; *.tiff";
                        break;
                    default:
                        dlgSaveFile.Filter = "All Files (*)|*";
                        break;
                }
                if (dlgSaveFile.ShowDialog() == DialogResult.OK)
                {
                    if (dlgSaveFile.FileName != string.Empty)
                    {
                        ERP.FMS.DUI.DUIFileManager.DownloadFile(this, "FMSAplication_ProERP_InputVoucherAttachment", strFileID, string.Empty, dlgSaveFile.FileName);
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception objExce)
            {
                MessageBox.Show("Lỗi tải file đính kèm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi tải file đính kèm", objExce.ToString(), " frmInputVoucher-> DownloadFile", DUIInventory_Globals.ModuleName);
                return false;
            }
        }

        private string LoadProcess(string strFileID, string strFileName)
        {
            string strURL = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(strFileID))
                {
                    strURL = Path.GetTempPath() + @"NC_Attachment";
                    if (!Directory.Exists(strURL))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(strURL);
                    }
                    strURL = Path.GetTempPath() + @"NC_Attachment\" + strFileName;
                    ERP.FMS.DUI.DUIFileManager.DownloadFile(this, "FMSAplication_ProERP_InputVoucherAttachment", strFileID, string.Empty, strURL);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    }
                }
            }
            catch
            {
                MessageBox.Show(this, "Không thể tải hình lên được", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return strURL;
        }

        private void gridViewDataDetail_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (gridViewDataDetail.FocusedRowHandle < 0)
                return;

            INV_InventoryOnWayDetail objINV_InventoryOnWayDetail = gridViewDataDetail.GetFocusedRow() as INV_InventoryOnWayDetail;
            if (objINV_InventoryOnWayDetail == null)
                return;

            if (objINV_InventoryOnWayDetail.CheckQuantity == null)
                objINV_InventoryOnWayDetail.Quantity = 0;
        }

    }
}
