﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmInventoryPrice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInventoryPrice));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboStore = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtTaxCustomerTaxCode = new System.Windows.Forms.TextBox();
            this.txtCurrencyExchange = new C1.Win.C1Input.C1NumericEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.cboCurrencyUnit = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboBranch = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.dtmCreateDate = new DevExpress.XtraEditors.DateEdit();
            this.txtVATPInvoiceVoucherNo = new System.Windows.Forms.TextBox();
            this.dtmDueDate = new DevExpress.XtraEditors.DateEdit();
            this.label33 = new DevExpress.XtraEditors.LabelControl();
            this.cboInvoiceTransType = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label32 = new DevExpress.XtraEditors.LabelControl();
            this.dtmInvoiceDate = new DevExpress.XtraEditors.DateEdit();
            this.txtLstCustomerName = new System.Windows.Forms.TextBox();
            this.label29 = new DevExpress.XtraEditors.LabelControl();
            this.cboSaleman = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.label28 = new DevExpress.XtraEditors.LabelControl();
            this.label14 = new DevExpress.XtraEditors.LabelControl();
            this.cboPayableType = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.txtTaxCustomerDescription = new System.Windows.Forms.TextBox();
            this.label22 = new DevExpress.XtraEditors.LabelControl();
            this.label21 = new DevExpress.XtraEditors.LabelControl();
            this.label17 = new DevExpress.XtraEditors.LabelControl();
            this.txtTaxCustomerAddress = new System.Windows.Forms.TextBox();
            this.label18 = new DevExpress.XtraEditors.LabelControl();
            this.txtTaxCustomerSalerName = new System.Windows.Forms.TextBox();
            this.label15 = new DevExpress.XtraEditors.LabelControl();
            this.txtTaxCustomerName = new System.Windows.Forms.TextBox();
            this.label12 = new DevExpress.XtraEditors.LabelControl();
            this.txtInvoiceSymbol = new System.Windows.Forms.TextBox();
            this.label11 = new DevExpress.XtraEditors.LabelControl();
            this.txtTaxNumber = new System.Windows.Forms.TextBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtDenominator = new System.Windows.Forms.TextBox();
            this.label8 = new DevExpress.XtraEditors.LabelControl();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label4 = new DevExpress.XtraEditors.LabelControl();
            this.label5 = new DevExpress.XtraEditors.LabelControl();
            this.label3 = new DevExpress.XtraEditors.LabelControl();
            this.label1 = new DevExpress.XtraEditors.LabelControl();
            this.txtInvoiceNo = new System.Windows.Forms.TextBox();
            this.txtPriceProtectNo = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnUndo = new System.Windows.Forms.Button();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabProduct = new DevExpress.XtraTab.XtraTabPage();
            this.grdDataProductService = new DevExpress.XtraGrid.GridControl();
            this.mnuProductService = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemExportExcelProduct = new System.Windows.Forms.ToolStripMenuItem();
            this.grdViewDataProductService = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.cboVAT = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.chkPROMOTIONPRODUCTMISA = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repLockUpServiceGroup = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tabHistory = new DevExpress.XtraTab.XtraTabPage();
            this.grdDataHistory = new DevExpress.XtraGrid.GridControl();
            this.grdViewDataHistory = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repCheckBoxIsCopy = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.grdDataAttachment = new DevExpress.XtraGrid.GridControl();
            this.mnuAttachment = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemAddAtt = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemDelAtt = new System.Windows.Forms.ToolStripMenuItem();
            this.grdViewDataAttachment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnViewImages = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repAttachment = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repDescriptionAttachment = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrencyExchange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmCreateDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmCreateDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmDueDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmDueDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmInvoiceDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmInvoiceDate.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.tabProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDataProductService)).BeginInit();
            this.mnuProductService.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewDataProductService)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPROMOTIONPRODUCTMISA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLockUpServiceGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            this.tabHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDataHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewDataHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckBoxIsCopy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDataAttachment)).BeginInit();
            this.mnuAttachment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewDataAttachment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnViewImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAttachment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDescriptionAttachment)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cboStore);
            this.panel1.Controls.Add(this.labelControl2);
            this.panel1.Controls.Add(this.txtTaxCustomerTaxCode);
            this.panel1.Controls.Add(this.txtCurrencyExchange);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cboCurrencyUnit);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.cboBranch);
            this.panel1.Controls.Add(this.dtmCreateDate);
            this.panel1.Controls.Add(this.txtVATPInvoiceVoucherNo);
            this.panel1.Controls.Add(this.dtmDueDate);
            this.panel1.Controls.Add(this.label33);
            this.panel1.Controls.Add(this.cboInvoiceTransType);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.dtmInvoiceDate);
            this.panel1.Controls.Add(this.txtLstCustomerName);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.cboSaleman);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.cboPayableType);
            this.panel1.Controls.Add(this.txtTaxCustomerDescription);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.txtTaxCustomerAddress);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.txtTaxCustomerSalerName);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.txtTaxCustomerName);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtInvoiceSymbol);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txtTaxNumber);
            this.panel1.Controls.Add(this.labelControl1);
            this.panel1.Controls.Add(this.txtDenominator);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtAddress);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtInvoiceNo);
            this.panel1.Controls.Add(this.txtPriceProtectNo);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(961, 289);
            this.panel1.TabIndex = 0;
            // 
            // cboStore
            // 
            this.cboStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStore.Location = new System.Drawing.Point(675, 300);
            this.cboStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStore.Name = "cboStore";
            this.cboStore.Size = new System.Drawing.Size(184, 24);
            this.cboStore.TabIndex = 1001;
            this.cboStore.TabStop = false;
            this.cboStore.Visible = false;
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(10, 118);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(86, 16);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "Nhà cung cấp:";
            // 
            // txtTaxCustomerTaxCode
            // 
            this.txtTaxCustomerTaxCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtTaxCustomerTaxCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxCustomerTaxCode.Location = new System.Drawing.Point(107, 174);
            this.txtTaxCustomerTaxCode.Margin = new System.Windows.Forms.Padding(4);
            this.txtTaxCustomerTaxCode.MaxLength = 20;
            this.txtTaxCustomerTaxCode.Name = "txtTaxCustomerTaxCode";
            this.txtTaxCustomerTaxCode.Size = new System.Drawing.Size(159, 22);
            this.txtTaxCustomerTaxCode.TabIndex = 17;
            this.txtTaxCustomerTaxCode.TabStop = false;
            // 
            // txtCurrencyExchange
            // 
            this.txtCurrencyExchange.CustomFormat = "#,###,###,##0";
            this.txtCurrencyExchange.DataType = typeof(double);
            this.txtCurrencyExchange.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtCurrencyExchange.DisplayFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)(((((C1.Win.C1Input.FormatInfoInheritFlags.CustomFormat | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd)));
            this.txtCurrencyExchange.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtCurrencyExchange.Location = new System.Drawing.Point(764, 253);
            this.txtCurrencyExchange.MaxLength = 11;
            this.txtCurrencyExchange.Name = "txtCurrencyExchange";
            this.txtCurrencyExchange.NullText = "0";
            this.txtCurrencyExchange.NumericInputKeys = C1.Win.C1Input.NumericInputKeyFlags.None;
            this.txtCurrencyExchange.ReadOnly = true;
            this.txtCurrencyExchange.Size = new System.Drawing.Size(184, 22);
            this.txtCurrencyExchange.TabIndex = 43;
            this.txtCurrencyExchange.Tag = null;
            this.txtCurrencyExchange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCurrencyExchange.Value = 0D;
            this.txtCurrencyExchange.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(671, 256);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 16);
            this.label2.TabIndex = 42;
            this.label2.Text = "Tỷ giá:";
            // 
            // cboCurrencyUnit
            // 
            this.cboCurrencyUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrencyUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCurrencyUnit.FormattingEnabled = true;
            this.cboCurrencyUnit.Location = new System.Drawing.Point(764, 227);
            this.cboCurrencyUnit.Name = "cboCurrencyUnit";
            this.cboCurrencyUnit.Size = new System.Drawing.Size(184, 23);
            this.cboCurrencyUnit.TabIndex = 41;
            this.cboCurrencyUnit.SelectionChangeCommitted += new System.EventHandler(this.cboCurrencyUnit_SelectionChangeCommitted);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(672, 231);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 16);
            this.label7.TabIndex = 40;
            this.label7.Text = "Loại tiền:";
            // 
            // cboBranch
            // 
            this.cboBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBranch.Location = new System.Drawing.Point(107, 8);
            this.cboBranch.Margin = new System.Windows.Forms.Padding(0);
            this.cboBranch.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.Size = new System.Drawing.Size(224, 24);
            this.cboBranch.TabIndex = 1;
            this.cboBranch.SelectionChangeCommitted += new System.EventHandler(this.cboBranch_SelectionChangeCommitted);
            // 
            // dtmCreateDate
            // 
            this.dtmCreateDate.EditValue = null;
            this.dtmCreateDate.Location = new System.Drawing.Point(437, 35);
            this.dtmCreateDate.Name = "dtmCreateDate";
            this.dtmCreateDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dtmCreateDate.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.dtmCreateDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtmCreateDate.Properties.Appearance.Options.UseBackColor = true;
            this.dtmCreateDate.Properties.Appearance.Options.UseFont = true;
            this.dtmCreateDate.Properties.AutoHeight = false;
            this.dtmCreateDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.dtmCreateDate.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtmCreateDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtmCreateDate.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtmCreateDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtmCreateDate.Size = new System.Drawing.Size(231, 23);
            this.dtmCreateDate.TabIndex = 7;
            // 
            // txtVATPInvoiceVoucherNo
            // 
            this.txtVATPInvoiceVoucherNo.BackColor = System.Drawing.SystemColors.Info;
            this.txtVATPInvoiceVoucherNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVATPInvoiceVoucherNo.Location = new System.Drawing.Point(764, 9);
            this.txtVATPInvoiceVoucherNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtVATPInvoiceVoucherNo.MaxLength = 36;
            this.txtVATPInvoiceVoucherNo.Name = "txtVATPInvoiceVoucherNo";
            this.txtVATPInvoiceVoucherNo.ReadOnly = true;
            this.txtVATPInvoiceVoucherNo.Size = new System.Drawing.Size(184, 22);
            this.txtVATPInvoiceVoucherNo.TabIndex = 25;
            this.txtVATPInvoiceVoucherNo.TabStop = false;
            // 
            // dtmDueDate
            // 
            this.dtmDueDate.EditValue = null;
            this.dtmDueDate.Location = new System.Drawing.Point(764, 201);
            this.dtmDueDate.Name = "dtmDueDate";
            this.dtmDueDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dtmDueDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtmDueDate.Properties.Appearance.Options.UseFont = true;
            this.dtmDueDate.Properties.AutoHeight = false;
            this.dtmDueDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtmDueDate.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtmDueDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtmDueDate.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtmDueDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtmDueDate.Size = new System.Drawing.Size(184, 23);
            this.dtmDueDate.TabIndex = 39;
            // 
            // label33
            // 
            this.label33.AllowHtmlString = true;
            this.label33.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(674, 204);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(50, 16);
            this.label33.TabIndex = 38;
            this.label33.Text = "Hạn TH:";
            // 
            // cboInvoiceTransType
            // 
            this.cboInvoiceTransType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInvoiceTransType.Location = new System.Drawing.Point(437, 8);
            this.cboInvoiceTransType.Margin = new System.Windows.Forms.Padding(0);
            this.cboInvoiceTransType.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInvoiceTransType.Name = "cboInvoiceTransType";
            this.cboInvoiceTransType.Size = new System.Drawing.Size(231, 24);
            this.cboInvoiceTransType.TabIndex = 3;
            this.cboInvoiceTransType.SelectionChangeCommitted += new System.EventHandler(this.cboInvoiceTransType_SelectionChangeCommitted);
            // 
            // label32
            // 
            this.label32.AllowHtmlString = true;
            this.label32.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(335, 11);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(98, 16);
            this.label32.TabIndex = 2;
            this.label32.Text = "Loại nghiệp vụ <color=\"red\">*</color>:";
            // 
            // dtmInvoiceDate
            // 
            this.dtmInvoiceDate.EditValue = null;
            this.dtmInvoiceDate.Location = new System.Drawing.Point(764, 118);
            this.dtmInvoiceDate.Name = "dtmInvoiceDate";
            this.dtmInvoiceDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dtmInvoiceDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtmInvoiceDate.Properties.Appearance.Options.UseFont = true;
            this.dtmInvoiceDate.Properties.AutoHeight = false;
            this.dtmInvoiceDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtmInvoiceDate.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtmInvoiceDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dtmInvoiceDate.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtmInvoiceDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtmInvoiceDate.Size = new System.Drawing.Size(184, 23);
            this.dtmInvoiceDate.TabIndex = 33;
            // 
            // txtLstCustomerName
            // 
            this.txtLstCustomerName.AcceptsReturn = true;
            this.txtLstCustomerName.BackColor = System.Drawing.SystemColors.Info;
            this.txtLstCustomerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLstCustomerName.Location = new System.Drawing.Point(437, 61);
            this.txtLstCustomerName.Margin = new System.Windows.Forms.Padding(4);
            this.txtLstCustomerName.MaxLength = 500;
            this.txtLstCustomerName.Multiline = true;
            this.txtLstCustomerName.Name = "txtLstCustomerName";
            this.txtLstCustomerName.ReadOnly = true;
            this.txtLstCustomerName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLstCustomerName.Size = new System.Drawing.Size(231, 51);
            this.txtLstCustomerName.TabIndex = 11;
            this.txtLstCustomerName.TabStop = false;
            // 
            // label29
            // 
            this.label29.AllowHtmlString = true;
            this.label29.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(335, 66);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(78, 16);
            this.label29.TabIndex = 10;
            this.label29.Text = "NCC trên ĐH:";
            // 
            // cboSaleman
            // 
            this.cboSaleman.BackColor = System.Drawing.SystemColors.Info;
            this.cboSaleman.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSaleman.IsOnlyShowRealStaff = false;
            this.cboSaleman.IsValidate = true;
            this.cboSaleman.Location = new System.Drawing.Point(764, 147);
            this.cboSaleman.Margin = new System.Windows.Forms.Padding(4);
            this.cboSaleman.MaximumSize = new System.Drawing.Size(400, 22);
            this.cboSaleman.MinimumSize = new System.Drawing.Size(0, 22);
            this.cboSaleman.Name = "cboSaleman";
            this.cboSaleman.Size = new System.Drawing.Size(184, 22);
            this.cboSaleman.TabIndex = 35;
            this.cboSaleman.UserName = "";
            this.cboSaleman.WorkingPositionID = 0;
            // 
            // label28
            // 
            this.label28.AllowHtmlString = true;
            this.label28.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(674, 151);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(81, 16);
            this.label28.TabIndex = 34;
            this.label28.Text = "NV bán hàng:";
            // 
            // label14
            // 
            this.label14.AllowHtmlString = true;
            this.label14.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(674, 122);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 16);
            this.label14.TabIndex = 32;
            this.label14.Text = "Ngày HĐ:";
            // 
            // cboPayableType
            // 
            this.cboPayableType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPayableType.Location = new System.Drawing.Point(764, 174);
            this.cboPayableType.Margin = new System.Windows.Forms.Padding(0);
            this.cboPayableType.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboPayableType.Name = "cboPayableType";
            this.cboPayableType.Size = new System.Drawing.Size(184, 24);
            this.cboPayableType.TabIndex = 37;
            // 
            // txtTaxCustomerDescription
            // 
            this.txtTaxCustomerDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxCustomerDescription.Location = new System.Drawing.Point(107, 201);
            this.txtTaxCustomerDescription.Margin = new System.Windows.Forms.Padding(4);
            this.txtTaxCustomerDescription.MaxLength = 1000;
            this.txtTaxCustomerDescription.Multiline = true;
            this.txtTaxCustomerDescription.Name = "txtTaxCustomerDescription";
            this.txtTaxCustomerDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTaxCustomerDescription.Size = new System.Drawing.Size(561, 45);
            this.txtTaxCustomerDescription.TabIndex = 21;
            // 
            // label22
            // 
            this.label22.AllowHtmlString = true;
            this.label22.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(10, 202);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(57, 16);
            this.label22.TabIndex = 20;
            this.label22.Text = "Nội dung:";
            // 
            // label21
            // 
            this.label21.AllowHtmlString = true;
            this.label21.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(674, 178);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(86, 16);
            this.label21.TabIndex = 36;
            this.label21.Text = "HT thanh toán:";
            // 
            // label17
            // 
            this.label17.AllowHtmlString = true;
            this.label17.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(10, 177);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 16);
            this.label17.TabIndex = 16;
            this.label17.Text = "Mã số thuế:";
            // 
            // txtTaxCustomerAddress
            // 
            this.txtTaxCustomerAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtTaxCustomerAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxCustomerAddress.Location = new System.Drawing.Point(107, 146);
            this.txtTaxCustomerAddress.Margin = new System.Windows.Forms.Padding(4);
            this.txtTaxCustomerAddress.MaxLength = 300;
            this.txtTaxCustomerAddress.Name = "txtTaxCustomerAddress";
            this.txtTaxCustomerAddress.Size = new System.Drawing.Size(561, 22);
            this.txtTaxCustomerAddress.TabIndex = 15;
            // 
            // label18
            // 
            this.label18.AllowHtmlString = true;
            this.label18.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(10, 149);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 16);
            this.label18.TabIndex = 14;
            this.label18.Text = "Địa chỉ NCC:";
            // 
            // txtTaxCustomerSalerName
            // 
            this.txtTaxCustomerSalerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxCustomerSalerName.Location = new System.Drawing.Point(380, 174);
            this.txtTaxCustomerSalerName.Margin = new System.Windows.Forms.Padding(4);
            this.txtTaxCustomerSalerName.MaxLength = 200;
            this.txtTaxCustomerSalerName.Name = "txtTaxCustomerSalerName";
            this.txtTaxCustomerSalerName.Size = new System.Drawing.Size(288, 22);
            this.txtTaxCustomerSalerName.TabIndex = 19;
            // 
            // label15
            // 
            this.label15.AllowHtmlString = true;
            this.label15.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(274, 177);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(98, 16);
            this.label15.TabIndex = 18;
            this.label15.Text = "Người bán hàng:";
            // 
            // txtTaxCustomerName
            // 
            this.txtTaxCustomerName.BackColor = System.Drawing.SystemColors.Window;
            this.txtTaxCustomerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxCustomerName.Location = new System.Drawing.Point(107, 118);
            this.txtTaxCustomerName.Margin = new System.Windows.Forms.Padding(4);
            this.txtTaxCustomerName.MaxLength = 200;
            this.txtTaxCustomerName.Name = "txtTaxCustomerName";
            this.txtTaxCustomerName.Size = new System.Drawing.Size(561, 22);
            this.txtTaxCustomerName.TabIndex = 13;
            // 
            // label12
            // 
            this.label12.AllowHtmlString = true;
            this.label12.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(674, 94);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 16);
            this.label12.TabIndex = 30;
            this.label12.Text = "Số hóa đơn:";
            // 
            // txtInvoiceSymbol
            // 
            this.txtInvoiceSymbol.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvoiceSymbol.Location = new System.Drawing.Point(764, 63);
            this.txtInvoiceSymbol.Margin = new System.Windows.Forms.Padding(4);
            this.txtInvoiceSymbol.MaxLength = 100;
            this.txtInvoiceSymbol.Name = "txtInvoiceSymbol";
            this.txtInvoiceSymbol.Size = new System.Drawing.Size(184, 22);
            this.txtInvoiceSymbol.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AllowHtmlString = true;
            this.label11.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(674, 66);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 16);
            this.label11.TabIndex = 28;
            this.label11.Text = "Ký hiệu:";
            // 
            // txtTaxNumber
            // 
            this.txtTaxNumber.BackColor = System.Drawing.SystemColors.Info;
            this.txtTaxNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxNumber.Location = new System.Drawing.Point(107, 36);
            this.txtTaxNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtTaxNumber.Name = "txtTaxNumber";
            this.txtTaxNumber.ReadOnly = true;
            this.txtTaxNumber.Size = new System.Drawing.Size(224, 22);
            this.txtTaxNumber.TabIndex = 5;
            this.txtTaxNumber.TabStop = false;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(10, 39);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 16);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Mã số thuế CN:";
            // 
            // txtDenominator
            // 
            this.txtDenominator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDenominator.Location = new System.Drawing.Point(764, 36);
            this.txtDenominator.Margin = new System.Windows.Forms.Padding(4);
            this.txtDenominator.MaxLength = 100;
            this.txtDenominator.Name = "txtDenominator";
            this.txtDenominator.Size = new System.Drawing.Size(184, 22);
            this.txtDenominator.TabIndex = 27;
            // 
            // label8
            // 
            this.label8.AllowHtmlString = true;
            this.label8.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(674, 39);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 16);
            this.label8.TabIndex = 26;
            this.label8.Text = "Mẫu số:";
            // 
            // txtAddress
            // 
            this.txtAddress.AcceptsReturn = true;
            this.txtAddress.BackColor = System.Drawing.SystemColors.Info;
            this.txtAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddress.Location = new System.Drawing.Point(107, 63);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(4);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.ReadOnly = true;
            this.txtAddress.Size = new System.Drawing.Size(224, 49);
            this.txtAddress.TabIndex = 9;
            this.txtAddress.TabStop = false;
            // 
            // label4
            // 
            this.label4.AllowHtmlString = true;
            this.label4.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 66);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Địa chỉ CN:";
            // 
            // label5
            // 
            this.label5.AllowHtmlString = true;
            this.label5.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(335, 39);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Ngày tạo:";
            // 
            // label3
            // 
            this.label3.AllowHtmlString = true;
            this.label3.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(675, 12);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 16);
            this.label3.TabIndex = 24;
            this.label3.Text = "Mã hóa đơn:";
            // 
            // label1
            // 
            this.label1.AllowHtmlString = true;
            this.label1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Chi nhánh:";
            // 
            // txtInvoiceNo
            // 
            this.txtInvoiceNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvoiceNo.Location = new System.Drawing.Point(764, 91);
            this.txtInvoiceNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtInvoiceNo.MaxLength = 7;
            this.txtInvoiceNo.Name = "txtInvoiceNo";
            this.txtInvoiceNo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtInvoiceNo.Size = new System.Drawing.Size(184, 22);
            this.txtInvoiceNo.TabIndex = 31;
            this.txtInvoiceNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInvoiceNo_KeyPress);
            // 
            // txtPriceProtectNo
            // 
            this.txtPriceProtectNo.BackColor = System.Drawing.SystemColors.Info;
            this.txtPriceProtectNo.Location = new System.Drawing.Point(107, 252);
            this.txtPriceProtectNo.Multiline = true;
            this.txtPriceProtectNo.Name = "txtPriceProtectNo";
            this.txtPriceProtectNo.ReadOnly = true;
            this.txtPriceProtectNo.Size = new System.Drawing.Size(561, 25);
            this.txtPriceProtectNo.TabIndex = 23;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 256);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 16);
            this.label9.TabIndex = 22;
            this.label9.Text = "Số biên bản:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnDelete);
            this.panel2.Controls.Add(this.btnUpdate);
            this.panel2.Controls.Add(this.btnUndo);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 524);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(961, 39);
            this.panel2.TabIndex = 2;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(765, 6);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(88, 25);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Xóa HĐ";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(660, 6);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(98, 25);
            this.btnUpdate.TabIndex = 0;
            this.btnUpdate.Text = "    Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUndo.Image = ((System.Drawing.Image)(resources.GetObject("btnUndo.Image")));
            this.btnUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUndo.Location = new System.Drawing.Point(860, 6);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(98, 25);
            this.btnUndo.TabIndex = 2;
            this.btnUndo.Text = "    Bỏ qua";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 289);
            this.xtraTabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tabProduct;
            this.xtraTabControl1.Size = new System.Drawing.Size(961, 235);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabProduct,
            this.tabHistory,
            this.xtraTabPage1});
            // 
            // tabProduct
            // 
            this.tabProduct.Controls.Add(this.grdDataProductService);
            this.tabProduct.Margin = new System.Windows.Forms.Padding(4);
            this.tabProduct.Name = "tabProduct";
            this.tabProduct.Size = new System.Drawing.Size(955, 209);
            this.tabProduct.Text = "Sản phẩm, dịch vụ";
            // 
            // grdDataProductService
            // 
            this.grdDataProductService.ContextMenuStrip = this.mnuProductService;
            this.grdDataProductService.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDataProductService.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.grdDataProductService.Location = new System.Drawing.Point(0, 0);
            this.grdDataProductService.MainView = this.grdViewDataProductService;
            this.grdDataProductService.Margin = new System.Windows.Forms.Padding(4);
            this.grdDataProductService.Name = "grdDataProductService";
            this.grdDataProductService.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEdit4,
            this.repositoryItemTextEdit5,
            this.repositoryItemCheckEdit1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemTextEdit6,
            this.cboVAT,
            this.chkPROMOTIONPRODUCTMISA,
            this.repositoryItemTextEdit7,
            this.repLockUpServiceGroup});
            this.grdDataProductService.Size = new System.Drawing.Size(955, 209);
            this.grdDataProductService.TabIndex = 0;
            this.grdDataProductService.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewDataProductService,
            this.gridView6,
            this.gridView7});
            // 
            // mnuProductService
            // 
            this.mnuProductService.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemExportExcelProduct});
            this.mnuProductService.Name = "mnuProductService";
            this.mnuProductService.Size = new System.Drawing.Size(168, 26);
            // 
            // mnuItemExportExcelProduct
            // 
            this.mnuItemExportExcelProduct.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemExportExcelProduct.Image")));
            this.mnuItemExportExcelProduct.Name = "mnuItemExportExcelProduct";
            this.mnuItemExportExcelProduct.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.mnuItemExportExcelProduct.Size = new System.Drawing.Size(167, 22);
            this.mnuItemExportExcelProduct.Text = "Xuất Excel";
            this.mnuItemExportExcelProduct.Click += new System.EventHandler(this.mnuItemExportExcelProduct_Click);
            // 
            // grdViewDataProductService
            // 
            this.grdViewDataProductService.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDataProductService.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewDataProductService.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grdViewDataProductService.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.grdViewDataProductService.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewDataProductService.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewDataProductService.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDataProductService.Appearance.Preview.Options.UseFont = true;
            this.grdViewDataProductService.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDataProductService.Appearance.Row.Options.UseFont = true;
            this.grdViewDataProductService.ColumnPanelRowHeight = 50;
            this.grdViewDataProductService.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewDataProductService.GridControl = this.grdDataProductService;
            this.grdViewDataProductService.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.grdViewDataProductService.Name = "grdViewDataProductService";
            this.grdViewDataProductService.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewDataProductService.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewDataProductService.OptionsMenu.EnableColumnMenu = false;
            this.grdViewDataProductService.OptionsMenu.EnableFooterMenu = false;
            this.grdViewDataProductService.OptionsMenu.EnableGroupPanelMenu = false;
            this.grdViewDataProductService.OptionsNavigation.UseTabKey = false;
            this.grdViewDataProductService.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grdViewDataProductService.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.grdViewDataProductService.OptionsView.ColumnAutoWidth = false;
            this.grdViewDataProductService.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewDataProductService.OptionsView.ShowFooter = true;
            this.grdViewDataProductService.OptionsView.ShowGroupPanel = false;
            this.grdViewDataProductService.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grdViewDataProductService_CellValueChanged);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.DisplayFormat.FormatString = "#,##0";
            this.repositoryItemTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemTextEdit1.EditFormat.FormatString = "#,##0.####";
            this.repositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.Mask.EditMask = "#,###,###,###,###,##0.####;";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.NullText = "0";
            this.repositoryItemTextEdit1.NullValuePrompt = "0";
            this.repositoryItemTextEdit1.NullValuePromptShowForEmptyValue = true;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.DisplayFormat.FormatString = "#,##0";
            this.repositoryItemTextEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemTextEdit2.EditFormat.FormatString = "#,##0.####";
            this.repositoryItemTextEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit2.Mask.EditMask = "#,###,###,###,###,##0.####;";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            this.repositoryItemTextEdit2.NullText = "0";
            this.repositoryItemTextEdit2.NullValuePrompt = "0";
            this.repositoryItemTextEdit2.NullValuePromptShowForEmptyValue = true;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "^(?:100(?:\\.0{1,2})?|[0-9]{1,2}(?:\\.[0-9]{1,2})?)$";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemTextEdit3.MaxLength = 100;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            this.repositoryItemTextEdit3.NullText = "0";
            this.repositoryItemTextEdit3.NullValuePrompt = "0";
            this.repositoryItemTextEdit3.NullValuePromptShowForEmptyValue = true;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.DisplayFormat.FormatString = "#,##0";
            this.repositoryItemTextEdit4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemTextEdit4.EditFormat.FormatString = "#,##0.####";
            this.repositoryItemTextEdit4.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit4.Mask.EditMask = "#,###,###,###,###,##0.####;";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            this.repositoryItemTextEdit4.NullText = "0";
            this.repositoryItemTextEdit4.NullValuePrompt = "0";
            this.repositoryItemTextEdit4.NullValuePromptShowForEmptyValue = true;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.DisplayFormat.FormatString = "#,##0";
            this.repositoryItemTextEdit5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemTextEdit5.EditFormat.FormatString = "#,##0.####";
            this.repositoryItemTextEdit5.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit5.Mask.EditMask = "#,###,###,###,###,##0.####;";
            this.repositoryItemTextEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            this.repositoryItemTextEdit5.NullText = "0";
            this.repositoryItemTextEdit5.NullValuePrompt = "0";
            this.repositoryItemTextEdit5.NullValuePromptShowForEmptyValue = true;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = "#0";
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            this.repositoryItemSpinEdit1.NullText = "0";
            this.repositoryItemSpinEdit1.NullValuePrompt = "0";
            this.repositoryItemSpinEdit1.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemSpinEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.AutoHeight = false;
            this.repositoryItemTextEdit6.DisplayFormat.FormatString = "#,##0";
            this.repositoryItemTextEdit6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemTextEdit6.EditFormat.FormatString = "#,##0.####";
            this.repositoryItemTextEdit6.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit6.Mask.EditMask = "#,###,###,###,###,##0.####;";
            this.repositoryItemTextEdit6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            this.repositoryItemTextEdit6.NullText = "0";
            this.repositoryItemTextEdit6.NullValuePrompt = "0";
            this.repositoryItemTextEdit6.NullValuePromptShowForEmptyValue = true;
            // 
            // cboVAT
            // 
            this.cboVAT.AutoHeight = false;
            this.cboVAT.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboVAT.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VATTYPEID", "Loại thuế"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VATTYPENAME", "Mức thuế")});
            this.cboVAT.DisplayMember = "VATTYPENAME";
            this.cboVAT.Name = "cboVAT";
            this.cboVAT.NullText = "--Chọn thuế suất HĐ--";
            this.cboVAT.ValueMember = "VATTYPEID";
            // 
            // chkPROMOTIONPRODUCTMISA
            // 
            this.chkPROMOTIONPRODUCTMISA.AutoHeight = false;
            this.chkPROMOTIONPRODUCTMISA.Name = "chkPROMOTIONPRODUCTMISA";
            this.chkPROMOTIONPRODUCTMISA.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.chkPROMOTIONPRODUCTMISA.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chkPROMOTIONPRODUCTMISA.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // repositoryItemTextEdit7
            // 
            this.repositoryItemTextEdit7.AutoHeight = false;
            this.repositoryItemTextEdit7.DisplayFormat.FormatString = "#,##0.##;";
            this.repositoryItemTextEdit7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemTextEdit7.EditFormat.FormatString = "#,##0.##;";
            this.repositoryItemTextEdit7.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit7.Mask.EditMask = "#,##0.##;";
            this.repositoryItemTextEdit7.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit7.Name = "repositoryItemTextEdit7";
            this.repositoryItemTextEdit7.NullText = "0";
            this.repositoryItemTextEdit7.NullValuePrompt = "0";
            // 
            // repLockUpServiceGroup
            // 
            this.repLockUpServiceGroup.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repLockUpServiceGroup.Appearance.Options.UseFont = true;
            this.repLockUpServiceGroup.Appearance.Options.UseTextOptions = true;
            this.repLockUpServiceGroup.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.repLockUpServiceGroup.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repLockUpServiceGroup.AppearanceDropDown.Options.UseFont = true;
            this.repLockUpServiceGroup.AppearanceDropDown.Options.UseTextOptions = true;
            this.repLockUpServiceGroup.AppearanceDropDown.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.repLockUpServiceGroup.AutoHeight = false;
            this.repLockUpServiceGroup.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repLockUpServiceGroup.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GROUPSERVICEID", 70, "Mã nhóm"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GROUPSERVICENAME", 330, "Tên nhóm")});
            this.repLockUpServiceGroup.Name = "repLockUpServiceGroup";
            this.repLockUpServiceGroup.NullText = "--Chọn nhóm HHDV--";
            // 
            // gridView6
            // 
            this.gridView6.GridControl = this.grdDataProductService;
            this.gridView6.Name = "gridView6";
            // 
            // gridView7
            // 
            this.gridView7.GridControl = this.grdDataProductService;
            this.gridView7.Name = "gridView7";
            // 
            // tabHistory
            // 
            this.tabHistory.Controls.Add(this.grdDataHistory);
            this.tabHistory.Margin = new System.Windows.Forms.Padding(4);
            this.tabHistory.Name = "tabHistory";
            this.tabHistory.PageVisible = false;
            this.tabHistory.Size = new System.Drawing.Size(955, 209);
            this.tabHistory.Text = "Lịch sử in hóa đơn";
            // 
            // grdDataHistory
            // 
            this.grdDataHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDataHistory.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.grdDataHistory.Location = new System.Drawing.Point(0, 0);
            this.grdDataHistory.MainView = this.grdViewDataHistory;
            this.grdDataHistory.Margin = new System.Windows.Forms.Padding(4);
            this.grdDataHistory.Name = "grdDataHistory";
            this.grdDataHistory.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repCheckBoxIsCopy});
            this.grdDataHistory.Size = new System.Drawing.Size(955, 209);
            this.grdDataHistory.TabIndex = 1;
            this.grdDataHistory.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewDataHistory,
            this.gridView3,
            this.gridView4});
            // 
            // grdViewDataHistory
            // 
            this.grdViewDataHistory.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDataHistory.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewDataHistory.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grdViewDataHistory.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.grdViewDataHistory.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewDataHistory.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewDataHistory.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDataHistory.Appearance.Preview.Options.UseFont = true;
            this.grdViewDataHistory.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDataHistory.Appearance.Row.Options.UseFont = true;
            this.grdViewDataHistory.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewDataHistory.GridControl = this.grdDataHistory;
            this.grdViewDataHistory.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.grdViewDataHistory.Name = "grdViewDataHistory";
            this.grdViewDataHistory.OptionsBehavior.Editable = false;
            this.grdViewDataHistory.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewDataHistory.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewDataHistory.OptionsMenu.EnableColumnMenu = false;
            this.grdViewDataHistory.OptionsMenu.EnableFooterMenu = false;
            this.grdViewDataHistory.OptionsMenu.EnableGroupPanelMenu = false;
            this.grdViewDataHistory.OptionsNavigation.UseTabKey = false;
            this.grdViewDataHistory.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewDataHistory.OptionsView.ShowGroupPanel = false;
            // 
            // repCheckBoxIsCopy
            // 
            this.repCheckBoxIsCopy.AutoHeight = false;
            this.repCheckBoxIsCopy.Name = "repCheckBoxIsCopy";
            this.repCheckBoxIsCopy.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.grdDataHistory;
            this.gridView3.Name = "gridView3";
            // 
            // gridView4
            // 
            this.gridView4.GridControl = this.grdDataHistory;
            this.gridView4.Name = "gridView4";
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.grdDataAttachment);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(955, 209);
            this.xtraTabPage1.Text = "Tập tin đính kèm";
            // 
            // grdDataAttachment
            // 
            this.grdDataAttachment.ContextMenuStrip = this.mnuAttachment;
            this.grdDataAttachment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDataAttachment.Location = new System.Drawing.Point(0, 0);
            this.grdDataAttachment.MainView = this.grdViewDataAttachment;
            this.grdDataAttachment.Name = "grdDataAttachment";
            this.grdDataAttachment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnViewImages,
            this.repAttachment,
            this.repDescriptionAttachment});
            this.grdDataAttachment.Size = new System.Drawing.Size(955, 209);
            this.grdDataAttachment.TabIndex = 2;
            this.grdDataAttachment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewDataAttachment});
            // 
            // mnuAttachment
            // 
            this.mnuAttachment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemAddAtt,
            this.mnuItemDelAtt});
            this.mnuAttachment.Name = "mnuAttachment";
            this.mnuAttachment.Size = new System.Drawing.Size(185, 48);
            // 
            // mnuItemAddAtt
            // 
            this.mnuItemAddAtt.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemAddAtt.Image")));
            this.mnuItemAddAtt.Name = "mnuItemAddAtt";
            this.mnuItemAddAtt.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.mnuItemAddAtt.Size = new System.Drawing.Size(184, 22);
            this.mnuItemAddAtt.Text = "Thêm tập tin";
            this.mnuItemAddAtt.Click += new System.EventHandler(this.mnuItemAddAtt_Click);
            // 
            // mnuItemDelAtt
            // 
            this.mnuItemDelAtt.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemDelAtt.Image")));
            this.mnuItemDelAtt.Name = "mnuItemDelAtt";
            this.mnuItemDelAtt.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.mnuItemDelAtt.Size = new System.Drawing.Size(184, 22);
            this.mnuItemDelAtt.Text = "Hủy tập tin";
            this.mnuItemDelAtt.Click += new System.EventHandler(this.mnuItemDelAtt_Click);
            // 
            // grdViewDataAttachment
            // 
            this.grdViewDataAttachment.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDataAttachment.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewDataAttachment.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewDataAttachment.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewDataAttachment.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDataAttachment.Appearance.Preview.Options.UseFont = true;
            this.grdViewDataAttachment.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDataAttachment.Appearance.Row.Options.UseFont = true;
            this.grdViewDataAttachment.GridControl = this.grdDataAttachment;
            this.grdViewDataAttachment.Name = "grdViewDataAttachment";
            this.grdViewDataAttachment.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewDataAttachment.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewDataAttachment.OptionsNavigation.UseTabKey = false;
            this.grdViewDataAttachment.OptionsView.ColumnAutoWidth = false;
            this.grdViewDataAttachment.OptionsView.ShowAutoFilterRow = true;
            this.grdViewDataAttachment.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewDataAttachment.OptionsView.ShowGroupPanel = false;
            // 
            // btnViewImages
            // 
            this.btnViewImages.AutoHeight = false;
            this.btnViewImages.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnViewImages.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.btnViewImages.Name = "btnViewImages";
            this.btnViewImages.Click += new System.EventHandler(this.btnViewImages_Click);
            // 
            // repAttachment
            // 
            this.repAttachment.AutoHeight = false;
            this.repAttachment.Name = "repAttachment";
            this.repAttachment.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repAttachment.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // repDescriptionAttachment
            // 
            this.repDescriptionAttachment.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repDescriptionAttachment.Appearance.Options.UseFont = true;
            this.repDescriptionAttachment.AutoHeight = false;
            this.repDescriptionAttachment.MaxLength = 500;
            this.repDescriptionAttachment.Name = "repDescriptionAttachment";
            // 
            // frmInventoryPrice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(961, 563);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmInventoryPrice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hóa đơn hỗ trợ giá hàng tồn";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmInventoryPrice_FormClosing);
            this.Load += new System.EventHandler(this.frmInventoryPrice_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrencyExchange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmCreateDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmCreateDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmDueDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmDueDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmInvoiceDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtmInvoiceDate.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.tabProduct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDataProductService)).EndInit();
            this.mnuProductService.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdViewDataProductService)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPROMOTIONPRODUCTMISA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repLockUpServiceGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            this.tabHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDataHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewDataHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckBoxIsCopy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDataAttachment)).EndInit();
            this.mnuAttachment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdViewDataAttachment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnViewImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repAttachment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDescriptionAttachment)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtPriceProtectNo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnUndo;
        private C1.Win.C1Input.C1NumericEdit txtCurrencyExchange;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboCurrencyUnit;
        private System.Windows.Forms.Label label7;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboBranch;
        private DevExpress.XtraEditors.DateEdit dtmCreateDate;
        private System.Windows.Forms.TextBox txtVATPInvoiceVoucherNo;
        private DevExpress.XtraEditors.DateEdit dtmDueDate;
        private DevExpress.XtraEditors.LabelControl label33;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboInvoiceTransType;
        private DevExpress.XtraEditors.LabelControl label32;
        private DevExpress.XtraEditors.DateEdit dtmInvoiceDate;
        private System.Windows.Forms.TextBox txtLstCustomerName;
        private DevExpress.XtraEditors.LabelControl label29;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch cboSaleman;
        private DevExpress.XtraEditors.LabelControl label28;
        private DevExpress.XtraEditors.LabelControl label14;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboPayableType;
        private System.Windows.Forms.TextBox txtTaxCustomerDescription;
        private DevExpress.XtraEditors.LabelControl label22;
        private DevExpress.XtraEditors.LabelControl label21;
        private DevExpress.XtraEditors.LabelControl label17;
        private System.Windows.Forms.TextBox txtTaxCustomerAddress;
        private DevExpress.XtraEditors.LabelControl label18;
        private System.Windows.Forms.TextBox txtTaxCustomerSalerName;
        private DevExpress.XtraEditors.LabelControl label15;
        private System.Windows.Forms.TextBox txtTaxCustomerName;
        private DevExpress.XtraEditors.LabelControl label12;
        private System.Windows.Forms.TextBox txtInvoiceSymbol;
        private DevExpress.XtraEditors.LabelControl label11;
        private System.Windows.Forms.TextBox txtTaxNumber;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.TextBox txtDenominator;
        private DevExpress.XtraEditors.LabelControl label8;
        private System.Windows.Forms.TextBox txtAddress;
        private DevExpress.XtraEditors.LabelControl label4;
        private DevExpress.XtraEditors.LabelControl label5;
        private DevExpress.XtraEditors.LabelControl label3;
        private DevExpress.XtraEditors.LabelControl label1;
        private System.Windows.Forms.TextBox txtInvoiceNo;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage tabProduct;
        private DevExpress.XtraGrid.GridControl grdDataProductService;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewDataProductService;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboVAT;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkPROMOTIONPRODUCTMISA;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repLockUpServiceGroup;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraTab.XtraTabPage tabHistory;
        private DevExpress.XtraGrid.GridControl grdDataHistory;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewDataHistory;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repCheckBoxIsCopy;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraGrid.GridControl grdDataAttachment;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewDataAttachment;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnViewImages;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repAttachment;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repDescriptionAttachment;
        private System.Windows.Forms.TextBox txtTaxCustomerTaxCode;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.ContextMenuStrip mnuAttachment;
        private System.Windows.Forms.ToolStripMenuItem mnuItemAddAtt;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDelAtt;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboStore;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ContextMenuStrip mnuProductService;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcelProduct;
    }
}