﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmQuickInventory : Form
    {
        public frmQuickInventory()
        {
            InitializeComponent();
        }

        public string strQuickInventoryID = string.Empty;
        public DateTime dtmInventoryDate = DateTime.MinValue;
        public string strStoreName = string.Empty;
        public string strMainGroupName = string.Empty;
        public string strInventoryFullName = string.Empty;
        public string strReviewedFullName = string.Empty;
        public string strCreatedFullName = string.Empty;
        public DataTable dtbBrandAll = null;
        private DataTable dtbMainGroupAll = Library.AppCore.DataSource.GetDataSource.GetMainGroup().Copy();
        private ERP.Inventory.PLC.Inventory.PLCQuickInventory objPLCQuickInventory = new PLC.Inventory.PLCQuickInventory();

        private void frmShowUnEvent_Load(object sender, EventArgs e)
        {
            DataTable dtbDetail = objPLCQuickInventory.GetDetail(strQuickInventoryID);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            dtbDetail.Columns.Add("Different", typeof(decimal));
            for (int i = 0; i < dtbDetail.Rows.Count; i++)
                dtbDetail.Rows[i]["Different"] = Convert.ToDecimal(dtbDetail.Rows[i]["QuantityERP"]) - Convert.ToDecimal(dtbDetail.Rows[i]["QuantityReal"]);
            flexData.DataSource = dtbDetail;
            CustomFlex();
            string strBrandName = string.Empty;
            DataTable dtbBrandID = objPLCQuickInventory.GetListBrandID(strQuickInventoryID);
            if (dtbBrandID != null && dtbBrandID.Rows.Count > 0)
            {
                for (int i = 0; i < dtbBrandID.Rows.Count; i++)
                {
                    DataRow[] drBrandID = dtbBrandAll.Select("BrandID = " + Convert.ToInt32(dtbBrandID.Rows[i]["BrandID"]));
                    if (drBrandID.Length > 0)
                    {
                        strBrandName += " " + Convert.ToString(drBrandID[0]["BrandName"]).Trim() + ",";
                    }
                }
                strBrandName = strBrandName.Trim().Trim(',');
            }

            DataTable dtbMainGroupID = objPLCQuickInventory.GetListMaingroupID(strQuickInventoryID);
            if (dtbMainGroupID != null && dtbMainGroupID.Rows.Count > 0)
            {
                for (int i = 0; i < dtbMainGroupID.Rows.Count; i++)
                {
                    DataRow[] drMainGroupID = dtbMainGroupAll.Select("MAINGROUPID = " + Convert.ToInt32(dtbMainGroupID.Rows[i]["MAINGROUPID"]));
                    if (drMainGroupID.Length > 0)
                    {
                        strMainGroupName += " " + Convert.ToString(drMainGroupID[0]["MAINGROUPNAME"]).Trim() + ",";
                    }
                }
                strMainGroupName = strMainGroupName.Trim().Trim(',');
            }
            txtBrandName.Text = strBrandName;
            txtQuickInventoryID.Text = strQuickInventoryID;
            txtInventoryDate.Text = dtmInventoryDate.ToString("dd/MM/yyyy HH:mm");
            txtStoreName.Text = strStoreName;
            txtMainGroupName.Text = strMainGroupName;
            txtInventoryFullName.Text = strInventoryFullName;
            txtReviewedFullName.Text = strReviewedFullName;
            txtCreatedFullName.Text = strCreatedFullName;
            btnDelete.Enabled = Library.AppCore.SystemConfig.objSessionUser.IsPermission("INV_QUICKINVENTORY_DELETE");
        }

        private void CustomFlex()
        {
            for (int i = 0; i < flexData.Cols.Count; i++)
            {
                flexData.Cols[i].Visible = false;
                flexData.Cols[i].AllowEditing = false;
            }
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexData, true, "ProductID,ProductName,PRODUCTSTATUSNAME,QuantityERP,QuantityReal,Different");
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnCaption(flexData, "ProductID,Mã sản phẩm,ProductName,Tên sản phẩm,PRODUCTSTATUSNAME, Trạng thái,QuantityERP,ERP,QuantityReal,Thực tế,Different,Chênh lệch");
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnWidth(flexData,
                "ProductID", 120,
                "ProductName", 300,
                "PRODUCTSTATUSNAME", 120,
                "QuantityERP", 70,
                "QuantityReal", 70,
                "Different", 90);
            C1.Win.C1FlexGrid.CellStyle style = flexData.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexData.GetCellRange(0, 0, flexData.Rows.Fixed - 1, flexData.Cols.Count - 1);
            range.Style = style;
            flexData.Rows[0].AllowMerging = true;
            flexData.AllowEditing = false;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (!btnDelete.Enabled)
                return;
            Library.AppControl.FormCommon.frmInputString frmInputString1 = new Library.AppControl.FormCommon.frmInputString();
            frmInputString1.MaxLengthString = 500;
            frmInputString1.Text = "Nhập lý do hủy phiếu kiểm kê nhanh";
            frmInputString1.ShowDialog(this);
            string strDeletedNote = Convert.ToString(frmInputString1.ContentString).Trim();
            if ((!frmInputString1.IsAccept) || strDeletedNote == string.Empty)
                return;
            objPLCQuickInventory.Delete(strQuickInventoryID, strDeletedNote);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            MessageBox.Show(this, "Hủy phiếu kiểm kê nhanh thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }
    }
}
