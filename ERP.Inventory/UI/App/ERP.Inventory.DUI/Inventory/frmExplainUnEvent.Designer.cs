﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmExplainUnEvent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSave = new System.Windows.Forms.Button();
            this.grdCtlProducts = new DevExpress.XtraGrid.GridControl();
            this.grdViewProducts = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colPRODUCTID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colPRODUCTNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colERP_IMEI = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colERP_PRODUCTSTATUSNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colERP_QUANTITY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.reQuantityShow = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colINV_IMEI = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colINV_PRODUCTSTATUSNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colINV_QUANTITY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colDIFFERENT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colQUANTITY_INPUT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colQUANTITY_SALE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colQUANTITY_STORECHANGE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colQUANTITY_OUTPUT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colQUANTITY_DIFFERENT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand22 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colCAUSEGROUPSLIST = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repCauseGroups = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridBand23 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colUNEVENTEXPLAIN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colEXPLAINSYSTEMNOTE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colEXPLAINNOTE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repTextImei = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnAccess = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.grdCtlProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reQuantityShow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCauseGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextImei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Enabled = false;
            this.btnSave.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(1111, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(91, 25);
            this.btnSave.TabIndex = 19;
            this.btnSave.Text = "      Cập nhật";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // grdCtlProducts
            // 
            this.grdCtlProducts.ContextMenuStrip = this.contextMenuStrip1;
            this.grdCtlProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCtlProducts.Location = new System.Drawing.Point(0, 0);
            this.grdCtlProducts.MainView = this.grdViewProducts;
            this.grdCtlProducts.Name = "grdCtlProducts";
            this.grdCtlProducts.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repTextImei,
            this.repQuantity,
            this.repNote,
            this.reQuantityShow,
            this.repCauseGroups});
            this.grdCtlProducts.Size = new System.Drawing.Size(1207, 619);
            this.grdCtlProducts.TabIndex = 82;
            this.grdCtlProducts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewProducts});
            // 
            // grdViewProducts
            // 
            this.grdViewProducts.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.BandPanel.Options.UseFont = true;
            this.grdViewProducts.Appearance.BandPanel.Options.UseTextOptions = true;
            this.grdViewProducts.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewProducts.Appearance.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewProducts.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProducts.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewProducts.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewProducts.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewProducts.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewProducts.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProducts.Appearance.Preview.Options.UseFont = true;
            this.grdViewProducts.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProducts.Appearance.Row.Options.UseFont = true;
            this.grdViewProducts.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand4,
            this.gridBand8,
            this.gridBand16,
            this.gridBand11,
            this.gridBand20,
            this.gridBand17,
            this.gridBand18,
            this.gridBand19});
            this.grdViewProducts.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colPRODUCTID,
            this.colPRODUCTNAME,
            this.colERP_IMEI,
            this.colERP_QUANTITY,
            this.colERP_PRODUCTSTATUSNAME,
            this.colINV_IMEI,
            this.colINV_QUANTITY,
            this.colINV_PRODUCTSTATUSNAME,
            this.colDIFFERENT,
            this.colQUANTITY_INPUT,
            this.colQUANTITY_SALE,
            this.colQUANTITY_STORECHANGE,
            this.colQUANTITY_OUTPUT,
            this.colQUANTITY_DIFFERENT,
            this.colUNEVENTEXPLAIN,
            this.colEXPLAINSYSTEMNOTE,
            this.colEXPLAINNOTE,
            this.colCAUSEGROUPSLIST});
            this.grdViewProducts.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewProducts.GridControl = this.grdCtlProducts;
            this.grdViewProducts.MinBandPanelRowCount = 2;
            this.grdViewProducts.Name = "grdViewProducts";
            this.grdViewProducts.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewProducts.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewProducts.OptionsNavigation.UseTabKey = false;
            this.grdViewProducts.OptionsView.ColumnAutoWidth = false;
            this.grdViewProducts.OptionsView.ShowAutoFilterRow = true;
            this.grdViewProducts.OptionsView.ShowColumnHeaders = false;
            this.grdViewProducts.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewProducts.OptionsView.ShowFooter = true;
            this.grdViewProducts.OptionsView.ShowGroupPanel = false;
            this.grdViewProducts.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grdViewProducts_ShowingEditor);
            this.grdViewProducts.HiddenEditor += new System.EventHandler(this.grdViewProducts_HiddenEditor);
            this.grdViewProducts.ShownEditor += new System.EventHandler(this.grdViewProducts_ShownEditor);
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "Thông tin sản phẩm";
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand3});
            this.gridBand1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 340;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "Mã sản phẩm";
            this.gridBand2.Columns.Add(this.colPRODUCTID);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.OptionsBand.AllowMove = false;
            this.gridBand2.OptionsBand.FixedWidth = true;
            this.gridBand2.Width = 120;
            // 
            // colPRODUCTID
            // 
            this.colPRODUCTID.Caption = "PRODUCTID";
            this.colPRODUCTID.FieldName = "PRODUCTID";
            this.colPRODUCTID.Name = "colPRODUCTID";
            this.colPRODUCTID.OptionsColumn.AllowEdit = false;
            this.colPRODUCTID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPRODUCTID.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "PRODUCTID", "Tổng cổng")});
            this.colPRODUCTID.Visible = true;
            this.colPRODUCTID.Width = 120;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "Tên sản phẩm";
            this.gridBand3.Columns.Add(this.colPRODUCTNAME);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 220;
            // 
            // colPRODUCTNAME
            // 
            this.colPRODUCTNAME.Caption = "PRODUCTNAME";
            this.colPRODUCTNAME.FieldName = "PRODUCTNAME";
            this.colPRODUCTNAME.Name = "colPRODUCTNAME";
            this.colPRODUCTNAME.OptionsColumn.AllowEdit = false;
            this.colPRODUCTNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPRODUCTNAME.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "PRODUCTNAME", "{0:N0}")});
            this.colPRODUCTNAME.Visible = true;
            this.colPRODUCTNAME.Width = 220;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "ERP";
            this.gridBand4.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand5,
            this.gridBand6,
            this.gridBand7});
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Width = 330;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "IMEI";
            this.gridBand5.Columns.Add(this.colERP_IMEI);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.Width = 130;
            // 
            // colERP_IMEI
            // 
            this.colERP_IMEI.Caption = "ERP_IMEI";
            this.colERP_IMEI.FieldName = "ERP_IMEI";
            this.colERP_IMEI.Name = "colERP_IMEI";
            this.colERP_IMEI.OptionsColumn.AllowEdit = false;
            this.colERP_IMEI.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colERP_IMEI.Visible = true;
            this.colERP_IMEI.Width = 130;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "Trạng thái";
            this.gridBand6.Columns.Add(this.colERP_PRODUCTSTATUSNAME);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.Width = 130;
            // 
            // colERP_PRODUCTSTATUSNAME
            // 
            this.colERP_PRODUCTSTATUSNAME.Caption = "ERP_PRODUCTSTATUSNAME";
            this.colERP_PRODUCTSTATUSNAME.FieldName = "ERP_PRODUCTSTATUSNAME";
            this.colERP_PRODUCTSTATUSNAME.Name = "colERP_PRODUCTSTATUSNAME";
            this.colERP_PRODUCTSTATUSNAME.OptionsColumn.AllowEdit = false;
            this.colERP_PRODUCTSTATUSNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colERP_PRODUCTSTATUSNAME.Visible = true;
            this.colERP_PRODUCTSTATUSNAME.Width = 130;
            // 
            // gridBand7
            // 
            this.gridBand7.Caption = "Tồn kho";
            this.gridBand7.Columns.Add(this.colERP_QUANTITY);
            this.gridBand7.Name = "gridBand7";
            // 
            // colERP_QUANTITY
            // 
            this.colERP_QUANTITY.Caption = "ERP_QUANTITY";
            this.colERP_QUANTITY.ColumnEdit = this.reQuantityShow;
            this.colERP_QUANTITY.FieldName = "ERP_QUANTITY";
            this.colERP_QUANTITY.Name = "colERP_QUANTITY";
            this.colERP_QUANTITY.OptionsColumn.AllowEdit = false;
            this.colERP_QUANTITY.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colERP_QUANTITY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ERP_QUANTITY", "{0:N0}")});
            this.colERP_QUANTITY.Visible = true;
            this.colERP_QUANTITY.Width = 70;
            // 
            // reQuantityShow
            // 
            this.reQuantityShow.AutoHeight = false;
            this.reQuantityShow.Mask.EditMask = "N0";
            this.reQuantityShow.Mask.UseMaskAsDisplayFormat = true;
            this.reQuantityShow.Name = "reQuantityShow";
            // 
            // gridBand8
            // 
            this.gridBand8.Caption = "Kiểm kê";
            this.gridBand8.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand21,
            this.gridBand9,
            this.gridBand10});
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.Width = 320;
            // 
            // gridBand21
            // 
            this.gridBand21.Caption = "IMEI";
            this.gridBand21.Columns.Add(this.colINV_IMEI);
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.Width = 120;
            // 
            // colINV_IMEI
            // 
            this.colINV_IMEI.Caption = "INV_IMEI";
            this.colINV_IMEI.FieldName = "INV_IMEI";
            this.colINV_IMEI.Name = "colINV_IMEI";
            this.colINV_IMEI.OptionsColumn.AllowEdit = false;
            this.colINV_IMEI.Visible = true;
            this.colINV_IMEI.Width = 120;
            // 
            // gridBand9
            // 
            this.gridBand9.Caption = "Trạng thái";
            this.gridBand9.Columns.Add(this.colINV_PRODUCTSTATUSNAME);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.Width = 130;
            // 
            // colINV_PRODUCTSTATUSNAME
            // 
            this.colINV_PRODUCTSTATUSNAME.Caption = "INV_PRODUCTSTATUSNAME";
            this.colINV_PRODUCTSTATUSNAME.FieldName = "INV_PRODUCTSTATUSNAME";
            this.colINV_PRODUCTSTATUSNAME.Name = "colINV_PRODUCTSTATUSNAME";
            this.colINV_PRODUCTSTATUSNAME.OptionsColumn.AllowEdit = false;
            this.colINV_PRODUCTSTATUSNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colINV_PRODUCTSTATUSNAME.Visible = true;
            this.colINV_PRODUCTSTATUSNAME.Width = 130;
            // 
            // gridBand10
            // 
            this.gridBand10.Caption = "Tồn kho";
            this.gridBand10.Columns.Add(this.colINV_QUANTITY);
            this.gridBand10.Name = "gridBand10";
            // 
            // colINV_QUANTITY
            // 
            this.colINV_QUANTITY.Caption = "INV_QUANTITY";
            this.colINV_QUANTITY.ColumnEdit = this.reQuantityShow;
            this.colINV_QUANTITY.FieldName = "INV_QUANTITY";
            this.colINV_QUANTITY.Name = "colINV_QUANTITY";
            this.colINV_QUANTITY.OptionsColumn.AllowEdit = false;
            this.colINV_QUANTITY.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colINV_QUANTITY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "INV_QUANTITY", "{0:N0}")});
            this.colINV_QUANTITY.Visible = true;
            this.colINV_QUANTITY.Width = 70;
            // 
            // gridBand16
            // 
            this.gridBand16.Caption = "SL Chênh lệch";
            this.gridBand16.Columns.Add(this.colDIFFERENT);
            this.gridBand16.Name = "gridBand16";
            // 
            // colDIFFERENT
            // 
            this.colDIFFERENT.AppearanceCell.ForeColor = System.Drawing.Color.Red;
            this.colDIFFERENT.AppearanceCell.Options.UseForeColor = true;
            this.colDIFFERENT.Caption = "DIFFERENT";
            this.colDIFFERENT.ColumnEdit = this.reQuantityShow;
            this.colDIFFERENT.FieldName = "DIFFERENT";
            this.colDIFFERENT.Name = "colDIFFERENT";
            this.colDIFFERENT.OptionsColumn.AllowEdit = false;
            this.colDIFFERENT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDIFFERENT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DIFFERENT", "{0:N0}")});
            this.colDIFFERENT.Visible = true;
            this.colDIFFERENT.Width = 70;
            // 
            // gridBand11
            // 
            this.gridBand11.Caption = "Giả trình chênh lệch (Người dùng nhập)";
            this.gridBand11.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand12,
            this.gridBand13,
            this.gridBand14,
            this.gridBand15});
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.Width = 295;
            // 
            // gridBand12
            // 
            this.gridBand12.Caption = "Nhập";
            this.gridBand12.Columns.Add(this.colQUANTITY_INPUT);
            this.gridBand12.Name = "gridBand12";
            // 
            // colQUANTITY_INPUT
            // 
            this.colQUANTITY_INPUT.AppearanceCell.ForeColor = System.Drawing.Color.Blue;
            this.colQUANTITY_INPUT.AppearanceCell.Options.UseForeColor = true;
            this.colQUANTITY_INPUT.Caption = "QUANTITY_INPUT";
            this.colQUANTITY_INPUT.ColumnEdit = this.repQuantity;
            this.colQUANTITY_INPUT.FieldName = "QUANTITY_INPUT";
            this.colQUANTITY_INPUT.Name = "colQUANTITY_INPUT";
            this.colQUANTITY_INPUT.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_INPUT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQUANTITY_INPUT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_INPUT", "{0:N0}")});
            this.colQUANTITY_INPUT.Visible = true;
            this.colQUANTITY_INPUT.Width = 70;
            // 
            // repQuantity
            // 
            this.repQuantity.AutoHeight = false;
            this.repQuantity.DisplayFormat.FormatString = "N0";
            this.repQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repQuantity.Mask.EditMask = "N0";
            this.repQuantity.Mask.UseMaskAsDisplayFormat = true;
            this.repQuantity.MaxValue = new decimal(new int[] {
            1215752191,
            23,
            0,
            0});
            this.repQuantity.Name = "repQuantity";
            // 
            // gridBand13
            // 
            this.gridBand13.Caption = "xuất bán";
            this.gridBand13.Columns.Add(this.colQUANTITY_SALE);
            this.gridBand13.Name = "gridBand13";
            // 
            // colQUANTITY_SALE
            // 
            this.colQUANTITY_SALE.AppearanceCell.ForeColor = System.Drawing.Color.Orange;
            this.colQUANTITY_SALE.AppearanceCell.Options.UseForeColor = true;
            this.colQUANTITY_SALE.Caption = "QUANTITY_SALE";
            this.colQUANTITY_SALE.ColumnEdit = this.repQuantity;
            this.colQUANTITY_SALE.FieldName = "QUANTITY_SALE";
            this.colQUANTITY_SALE.Name = "colQUANTITY_SALE";
            this.colQUANTITY_SALE.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_SALE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQUANTITY_SALE.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_SALE", "{0:N0}")});
            this.colQUANTITY_SALE.Visible = true;
            this.colQUANTITY_SALE.Width = 70;
            // 
            // gridBand14
            // 
            this.gridBand14.Caption = "xuất chuyển kho";
            this.gridBand14.Columns.Add(this.colQUANTITY_STORECHANGE);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.RowCount = 2;
            this.gridBand14.Width = 85;
            // 
            // colQUANTITY_STORECHANGE
            // 
            this.colQUANTITY_STORECHANGE.AppearanceCell.ForeColor = System.Drawing.Color.Orange;
            this.colQUANTITY_STORECHANGE.AppearanceCell.Options.UseForeColor = true;
            this.colQUANTITY_STORECHANGE.Caption = "QUANTITY_STORECHANGE";
            this.colQUANTITY_STORECHANGE.ColumnEdit = this.repQuantity;
            this.colQUANTITY_STORECHANGE.FieldName = "QUANTITY_STORECHANGE";
            this.colQUANTITY_STORECHANGE.Name = "colQUANTITY_STORECHANGE";
            this.colQUANTITY_STORECHANGE.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_STORECHANGE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQUANTITY_STORECHANGE.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_STORECHANGE", "{0:N0}")});
            this.colQUANTITY_STORECHANGE.Visible = true;
            this.colQUANTITY_STORECHANGE.Width = 85;
            // 
            // gridBand15
            // 
            this.gridBand15.Caption = "Xuất khác";
            this.gridBand15.Columns.Add(this.colQUANTITY_OUTPUT);
            this.gridBand15.Name = "gridBand15";
            // 
            // colQUANTITY_OUTPUT
            // 
            this.colQUANTITY_OUTPUT.AppearanceCell.ForeColor = System.Drawing.Color.Orange;
            this.colQUANTITY_OUTPUT.AppearanceCell.Options.UseForeColor = true;
            this.colQUANTITY_OUTPUT.Caption = "colQUANTITY_OUTPUT";
            this.colQUANTITY_OUTPUT.ColumnEdit = this.repQuantity;
            this.colQUANTITY_OUTPUT.FieldName = "QUANTITY_OUTPUT";
            this.colQUANTITY_OUTPUT.Name = "colQUANTITY_OUTPUT";
            this.colQUANTITY_OUTPUT.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_OUTPUT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQUANTITY_OUTPUT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_OUTPUT", "{0:N0}")});
            this.colQUANTITY_OUTPUT.Visible = true;
            this.colQUANTITY_OUTPUT.Width = 70;
            // 
            // gridBand20
            // 
            this.gridBand20.Caption = "SL Chênh lệch sau giải trình";
            this.gridBand20.Columns.Add(this.colQUANTITY_DIFFERENT);
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.Width = 85;
            // 
            // colQUANTITY_DIFFERENT
            // 
            this.colQUANTITY_DIFFERENT.AppearanceCell.ForeColor = System.Drawing.Color.Red;
            this.colQUANTITY_DIFFERENT.AppearanceCell.Options.UseForeColor = true;
            this.colQUANTITY_DIFFERENT.Caption = "QUANTITY_DIFFERENT";
            this.colQUANTITY_DIFFERENT.ColumnEdit = this.reQuantityShow;
            this.colQUANTITY_DIFFERENT.FieldName = "QUANTITY_DIFFERENT";
            this.colQUANTITY_DIFFERENT.Name = "colQUANTITY_DIFFERENT";
            this.colQUANTITY_DIFFERENT.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_DIFFERENT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQUANTITY_DIFFERENT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_DIFFERENT", "{0:N0}")});
            this.colQUANTITY_DIFFERENT.Visible = true;
            this.colQUANTITY_DIFFERENT.Width = 85;
            // 
            // gridBand17
            // 
            this.gridBand17.Caption = "Giải trình chênh lệch";
            this.gridBand17.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand22,
            this.gridBand23});
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.Width = 463;
            // 
            // gridBand22
            // 
            this.gridBand22.Caption = "Nhóm nguyên nhân lỗi";
            this.gridBand22.Columns.Add(this.colCAUSEGROUPSLIST);
            this.gridBand22.Name = "gridBand22";
            this.gridBand22.Width = 267;
            // 
            // colCAUSEGROUPSLIST
            // 
            this.colCAUSEGROUPSLIST.Caption = "CAUSEGROUPSLIST";
            this.colCAUSEGROUPSLIST.ColumnEdit = this.repCauseGroups;
            this.colCAUSEGROUPSLIST.FieldName = "CAUSEGROUPSLIST";
            this.colCAUSEGROUPSLIST.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colCAUSEGROUPSLIST.Name = "colCAUSEGROUPSLIST";
            this.colCAUSEGROUPSLIST.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCAUSEGROUPSLIST.Visible = true;
            this.colCAUSEGROUPSLIST.Width = 267;
            // 
            // repCauseGroups
            // 
            this.repCauseGroups.AutoHeight = false;
            this.repCauseGroups.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repCauseGroups.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CAUSEGROUPSNAME", "Tên nhóm lỗi")});
            this.repCauseGroups.DisplayMember = "CAUSEGROUPSNAME";
            this.repCauseGroups.Name = "repCauseGroups";
            this.repCauseGroups.NullText = "-- Chọn nhóm nguyên nhân lỗi --";
            this.repCauseGroups.ValueMember = "CAUSEGROUPSID";
            // 
            // gridBand23
            // 
            this.gridBand23.Caption = "Giải trình";
            this.gridBand23.Columns.Add(this.colUNEVENTEXPLAIN);
            this.gridBand23.Name = "gridBand23";
            this.gridBand23.Width = 196;
            // 
            // colUNEVENTEXPLAIN
            // 
            this.colUNEVENTEXPLAIN.Caption = "UNEVENTEXPLAIN";
            this.colUNEVENTEXPLAIN.ColumnEdit = this.repNote;
            this.colUNEVENTEXPLAIN.FieldName = "UNEVENTEXPLAIN";
            this.colUNEVENTEXPLAIN.Name = "colUNEVENTEXPLAIN";
            this.colUNEVENTEXPLAIN.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colUNEVENTEXPLAIN.Visible = true;
            this.colUNEVENTEXPLAIN.Width = 196;
            // 
            // repNote
            // 
            this.repNote.AutoHeight = false;
            this.repNote.MaxLength = 2000;
            this.repNote.Name = "repNote";
            // 
            // gridBand18
            // 
            this.gridBand18.Caption = "Giải trình chênh lệch hệ thống";
            this.gridBand18.Columns.Add(this.colEXPLAINSYSTEMNOTE);
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.Width = 220;
            // 
            // colEXPLAINSYSTEMNOTE
            // 
            this.colEXPLAINSYSTEMNOTE.Caption = "EXPLAINSYSTEMNOTE";
            this.colEXPLAINSYSTEMNOTE.ColumnEdit = this.repNote;
            this.colEXPLAINSYSTEMNOTE.FieldName = "EXPLAINSYSTEMNOTE";
            this.colEXPLAINSYSTEMNOTE.Name = "colEXPLAINSYSTEMNOTE";
            this.colEXPLAINSYSTEMNOTE.OptionsColumn.AllowEdit = false;
            this.colEXPLAINSYSTEMNOTE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colEXPLAINSYSTEMNOTE.Visible = true;
            this.colEXPLAINSYSTEMNOTE.Width = 220;
            // 
            // gridBand19
            // 
            this.gridBand19.Caption = "Ghi chú";
            this.gridBand19.Columns.Add(this.colEXPLAINNOTE);
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.Width = 220;
            // 
            // colEXPLAINNOTE
            // 
            this.colEXPLAINNOTE.Caption = "EXPLAINNOTE";
            this.colEXPLAINNOTE.ColumnEdit = this.repNote;
            this.colEXPLAINNOTE.FieldName = "EXPLAINNOTE";
            this.colEXPLAINNOTE.Name = "colEXPLAINNOTE";
            this.colEXPLAINNOTE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colEXPLAINNOTE.Visible = true;
            this.colEXPLAINNOTE.Width = 220;
            // 
            // repTextImei
            // 
            this.repTextImei.AutoHeight = false;
            this.repTextImei.MaxLength = 50;
            this.repTextImei.Name = "repTextImei";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnAccess);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 619);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1207, 34);
            this.panelControl1.TabIndex = 83;
            // 
            // btnAccess
            // 
            this.btnAccess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAccess.Enabled = false;
            this.btnAccess.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.btnAccess.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAccess.Location = new System.Drawing.Point(932, 5);
            this.btnAccess.Name = "btnAccess";
            this.btnAccess.Size = new System.Drawing.Size(173, 25);
            this.btnAccess.TabIndex = 20;
            this.btnAccess.Text = "      Xác nhận giải trình";
            this.btnAccess.Click += new System.EventHandler(this.button1_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemExportExcel});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(133, 30);
            // 
            // mnuItemExportExcel
            // 
            this.mnuItemExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportExcel.Name = "mnuItemExportExcel";
            this.mnuItemExportExcel.Size = new System.Drawing.Size(132, 26);
            this.mnuItemExportExcel.Text = "Xuất Excel";
            this.mnuItemExportExcel.Click += new System.EventHandler(this.mnuItemExportExcel_Click);
            // 
            // frmExplainUnEvent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1207, 653);
            this.Controls.Add(this.grdCtlProducts);
            this.Controls.Add(this.panelControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmExplainUnEvent";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Giải trình chênh lệch kiểm kê";
            this.Load += new System.EventHandler(this.frmExplainUnEvent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdCtlProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reQuantityShow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCauseGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextImei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private DevExpress.XtraGrid.GridControl grdCtlProducts;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTextImei;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repQuantity;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repNote;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView grdViewProducts;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPRODUCTID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPRODUCTNAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colERP_IMEI;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colERP_QUANTITY;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colERP_PRODUCTSTATUSNAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colINV_IMEI;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colINV_QUANTITY;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colINV_PRODUCTSTATUSNAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDIFFERENT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_INPUT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_SALE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_STORECHANGE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_OUTPUT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_DIFFERENT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colUNEVENTEXPLAIN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colEXPLAINSYSTEMNOTE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colEXPLAINNOTE;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit reQuantityShow;
        private System.Windows.Forms.Button btnAccess;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCAUSEGROUPSLIST;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repCauseGroups;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand22;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand23;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcel;
    }
}