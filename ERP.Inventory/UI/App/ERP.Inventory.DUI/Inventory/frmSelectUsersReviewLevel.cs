﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmSelectUsersReviewLevel : Form
    {
        public frmSelectUsersReviewLevel()
        {
            InitializeComponent();
        }

        private ERP.Inventory.PLC.Inventory.PLCInventory objPLCInventory = new PLC.Inventory.PLCInventory();
        public string strUserName = string.Empty;
        public int intInventoryTypeID = 0;
        public DataTable dtbReviewLevel = null;
        public int intReviewLevel = 0;
        public int intSelectIndexReviewLevel = 0;
        public string strFunctionReview = string.Empty;
        public string strInventoryID = string.Empty;
        public string strInventoryUser = string.Empty;
        public int intStoreID = 0;
        public string strStoreName = string.Empty;
        public DateTime dtmInventoryDate;
        public int intIsReviewedInventory = 0;
        public bool bolIsUpdate = false;

        private void frmSelectUsersReviewLevel_Load(object sender, EventArgs e)
        {
            intIsReviewedInventory = 0;
            bolIsUpdate = false;
            txtUserName.Text = strUserName;
            txtPassword.Text = string.Empty;
            cboReviewStatus.SelectedIndex = 0;
            txtUserName.ReadOnly = true;
        }

        private bool CheckInput()
        {
            if (Convert.ToString(txtUserName.Text).Trim() == string.Empty)
            {
                MessageBox.Show(this, "Vui lòng nhập tên đăng nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtUserName.Focus();
                return false;
            }
            if (Convert.ToString(txtPassword.Text).Trim() == string.Empty)
            {
                MessageBox.Show(this, "Vui lòng nhập mật khẩu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPassword.Focus();
                return false;
            }
            Library.AppCore.SessionUser objSessionUser = new Library.AppCore.SessionUser(Convert.ToString(txtUserName.Text).Trim(), Convert.ToString(txtPassword.Text).Trim(), true);
            objSessionUser.Login();
            if (!objSessionUser.IsLogin)
            {
                txtUserName.Focus();
                txtUserName.SelectAll();
                txtPassword.Text = string.Empty;
                MessageBox.Show(this, "Tên đăng nhập hoặc mật khẩu không đúng. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private void btnReviewStatus_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }

            int intIsPermit = -1;
            bool bolIsCheckStorePermission = false;
            int intIsCheckInventoryUser = -1;
            int intReviewType = 0;
            for (int i = 0; i < dtbReviewLevel.Rows.Count; i++)
            {
                if (Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]) == intReviewLevel)
                {
                    intReviewType = Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewType"]);

                    if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCheckStorePermission"]))
                    {
                        bolIsCheckStorePermission = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCheckStorePermission"]);
                    }

                    if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCheckInventoryUser"]))
                    {
                        if (Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCheckInventoryUser"]))
                        {
                            intIsCheckInventoryUser = 1;
                        }
                        else
                        {
                            intIsCheckInventoryUser = 0;
                        }
                    }
                    else
                    {
                        intIsCheckInventoryUser = 1;
                    }

                    break;
                }
            }

            if (bolIsCheckStorePermission)
            {
                if (!Library.AppCore.SystemConfig.objSessionUser.IsStorePermission(intStoreID))
                {
                    intIsPermit = 0;
                }
            }
            if (strInventoryUser == Convert.ToString(txtUserName.Text).Trim() && intIsCheckInventoryUser == 0)
            {
                intIsPermit = 1;
            }
            if (intIsPermit == 0)
            {
                MessageBox.Show(this, "Bạn không có quyền trên kho " + strStoreName + ". Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            ERP.Inventory.PLC.Inventory.WSInventory.Inventory_ReviewList objInventoryReview = new PLC.Inventory.WSInventory.Inventory_ReviewList();
            objInventoryReview.InventoryID = strInventoryID;
            objInventoryReview.ReviewLevelID = intReviewLevel;
            objInventoryReview.UserName = Convert.ToString(txtUserName.Text).Trim();
            objInventoryReview.ReviewStatus = cboReviewStatus.SelectedIndex;
            objInventoryReview.InventoryDate = dtmInventoryDate;

            bool bolIsReviewedInventory = false;
            if (intSelectIndexReviewLevel == (dtbReviewLevel.Rows.Count - 1))
            {
                DataTable dtbUserRvwFunction = null;
                DataTable dtbInventoryRvwLevel = null;
                if (strFunctionReview != string.Empty)
                {
                    dtbUserRvwFunction = objPLCInventory.GetUserReviewFunction(strFunctionReview);
                    dtbInventoryRvwLevel = objPLCInventory.GetInventoryReviewLevel(strInventoryID);
                }
                if (intReviewType == 0)
                {
                    if (dtbUserRvwFunction.Rows.Count == dtbInventoryRvwLevel.Rows.Count)
                    {
                        bolIsReviewedInventory = true;
                    }
                }
                else
                {
                    if (dtbInventoryRvwLevel.Rows.Count > 0)
                    {
                        bolIsReviewedInventory = true;
                    }
                }
            }

            objPLCInventory.UpdateInventoryReviewStatus(objInventoryReview, bolIsReviewedInventory);

            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            else
            {
                if (bolIsReviewedInventory)
                {
                    intIsReviewedInventory = 1;
                }
            }

            bolIsUpdate = true;
            MessageBox.Show(this, "Cập nhật trạng thái duyệt phiếu kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

    }
}
