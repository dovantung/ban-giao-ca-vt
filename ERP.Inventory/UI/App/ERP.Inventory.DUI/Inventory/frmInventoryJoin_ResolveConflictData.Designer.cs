﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmInventoryJoin_ResolveConflictData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.viewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINVENTORYID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPRODUCTID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPRODUCTNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPRODUCTSTATUSNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNOTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1100, 48);
            this.label2.TabIndex = 45;
            this.label2.Text = "Xử lý các phiếu kiểm kê có dữ liệu trùng";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 514);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1100, 43);
            this.groupBox1.TabIndex = 46;
            this.groupBox1.TabStop = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(974, 9);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(0);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(117, 30);
            this.btnUpdate.TabIndex = 19;
            this.btnUpdate.Text = "Xác nhận";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // grdData
            // 
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(0, 48);
            this.grdData.MainView = this.viewData;
            this.grdData.Name = "grdData";
            this.grdData.Size = new System.Drawing.Size(1100, 466);
            this.grdData.TabIndex = 47;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewData,
            this.gridView2,
            this.gridView1});
            // 
            // viewData
            // 
            this.viewData.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.viewData.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Red;
            this.viewData.Appearance.FooterPanel.Options.UseFont = true;
            this.viewData.Appearance.FooterPanel.Options.UseForeColor = true;
            this.viewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.viewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.viewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.viewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.viewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.viewData.Appearance.Row.Options.UseFont = true;
            this.viewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSelected,
            this.colINVENTORYID,
            this.colPRODUCTID,
            this.colPRODUCTNAME,
            this.colIMEI,
            this.colPRODUCTSTATUSNAME,
            this.colQuantity,
            this.colNOTE});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition1.Expression = "[Status]=true";
            styleFormatCondition1.Value1 = true;
            this.viewData.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.viewData.GridControl = this.grdData;
            this.viewData.Name = "viewData";
            this.viewData.OptionsView.AllowCellMerge = true;
            this.viewData.OptionsView.ShowAutoFilterRow = true;
            this.viewData.OptionsView.ShowGroupPanel = false;
            this.viewData.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.viewData_CellValueChanged);
            this.viewData.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.viewData_CellValueChanging);
            // 
            // colSelected
            // 
            this.colSelected.AppearanceCell.Options.UseTextOptions = true;
            this.colSelected.AppearanceCell.TextOptions.Trimming = DevExpress.Utils.Trimming.Word;
            this.colSelected.Caption = "Chọn";
            this.colSelected.FieldName = "SELECTED";
            this.colSelected.Name = "colSelected";
            this.colSelected.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colSelected.OptionsColumn.AllowSize = false;
            this.colSelected.OptionsColumn.FixedWidth = true;
            this.colSelected.Visible = true;
            this.colSelected.VisibleIndex = 0;
            this.colSelected.Width = 40;
            // 
            // colINVENTORYID
            // 
            this.colINVENTORYID.Caption = "Mã phiếu";
            this.colINVENTORYID.FieldName = "INVENTORYID";
            this.colINVENTORYID.Name = "colINVENTORYID";
            this.colINVENTORYID.OptionsColumn.AllowEdit = false;
            this.colINVENTORYID.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colINVENTORYID.OptionsColumn.FixedWidth = true;
            this.colINVENTORYID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colINVENTORYID.Visible = true;
            this.colINVENTORYID.VisibleIndex = 1;
            this.colINVENTORYID.Width = 130;
            // 
            // colPRODUCTID
            // 
            this.colPRODUCTID.Caption = "Mã sản phẩm";
            this.colPRODUCTID.FieldName = "PRODUCTID";
            this.colPRODUCTID.Name = "colPRODUCTID";
            this.colPRODUCTID.OptionsColumn.AllowEdit = false;
            this.colPRODUCTID.OptionsColumn.FixedWidth = true;
            this.colPRODUCTID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPRODUCTID.Visible = true;
            this.colPRODUCTID.VisibleIndex = 2;
            this.colPRODUCTID.Width = 130;
            // 
            // colPRODUCTNAME
            // 
            this.colPRODUCTNAME.Caption = "Tên sản phẩm";
            this.colPRODUCTNAME.FieldName = "PRODUCTNAME";
            this.colPRODUCTNAME.Name = "colPRODUCTNAME";
            this.colPRODUCTNAME.OptionsColumn.AllowEdit = false;
            this.colPRODUCTNAME.OptionsColumn.FixedWidth = true;
            this.colPRODUCTNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPRODUCTNAME.Visible = true;
            this.colPRODUCTNAME.VisibleIndex = 3;
            this.colPRODUCTNAME.Width = 180;
            // 
            // colIMEI
            // 
            this.colIMEI.Caption = "IMEI";
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsColumn.AllowEdit = false;
            this.colIMEI.OptionsColumn.FixedWidth = true;
            this.colIMEI.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 4;
            this.colIMEI.Width = 150;
            // 
            // colQuantity
            // 
            this.colQuantity.Caption = "Số lượng";
            this.colQuantity.DisplayFormat.FormatString = "N0";
            this.colQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQuantity.FieldName = "QUANTITY";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.OptionsColumn.AllowEdit = false;
            this.colQuantity.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colQuantity.OptionsColumn.FixedWidth = true;
            this.colQuantity.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 5;
            this.colQuantity.Width = 70;
            // 
            // colPRODUCTSTATUSNAME
            // 
            this.colPRODUCTSTATUSNAME.Caption = "Trạng thái";
            this.colPRODUCTSTATUSNAME.FieldName = "PRODUCTSTATUSNAME";
            this.colPRODUCTSTATUSNAME.Name = "colPRODUCTSTATUSNAME";
            this.colPRODUCTSTATUSNAME.OptionsColumn.AllowEdit = false;
            this.colPRODUCTSTATUSNAME.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colPRODUCTSTATUSNAME.OptionsColumn.FixedWidth = true;
            this.colPRODUCTSTATUSNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPRODUCTSTATUSNAME.Visible = true;
            this.colPRODUCTSTATUSNAME.VisibleIndex = 6;
            this.colPRODUCTSTATUSNAME.Width = 120;
            // 
            // colNOTE
            // 
            this.colNOTE.Caption = "Ghi chú";
            this.colNOTE.FieldName = "NOTE";
            this.colNOTE.Name = "colNOTE";
            this.colNOTE.OptionsColumn.AllowEdit = false;
            this.colNOTE.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colNOTE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colNOTE.Visible = true;
            this.colNOTE.VisibleIndex = 7;
            this.colNOTE.Width = 262;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.grdData;
            this.gridView2.Name = "gridView2";
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.grdData;
            this.gridView1.Name = "gridView1";
            // 
            // frmInventoryJoin_ResolveConflictData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1100, 557);
            this.Controls.Add(this.grdData);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Name = "frmInventoryJoin_ResolveConflictData";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Xác nhận kiểm kê";
            this.Load += new System.EventHandler(this.frmInventoryJoin_ResolveConflictData_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnUpdate;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView viewData;
        private DevExpress.XtraGrid.Columns.GridColumn colINVENTORYID;
        private DevExpress.XtraGrid.Columns.GridColumn colPRODUCTID;
        private DevExpress.XtraGrid.Columns.GridColumn colPRODUCTNAME;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colPRODUCTSTATUSNAME;
        private DevExpress.XtraGrid.Columns.GridColumn colNOTE;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
    }
}