﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Library.AppCore;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmDutyUsers : Form
    {
        private bool bolAnswerYes = false;
        private bool bolAnswerNo = false;
        public DataTable dtbDutyUsers = null;
        private string strInventoryProcessID = string.Empty;
        private string strRelateVoucherID = string.Empty;
        private decimal decCollectArrearMoney = 0;
        private string strDutyUsers = string.Empty;
        private int intCollectArrearType = 0;

        public bool AnswerYes
        {
            get
            {
                return bolAnswerYes;
            }
            set
            {
                bolAnswerYes = value;
            }
        }
        public bool AnswerNo
        {
            get
            {
                return bolAnswerNo;
            }
            set
            {
                bolAnswerNo = value;
            }
        }
        public string InventoryProcessID
        {
            get
            {
                return strInventoryProcessID;
            }
            set
            {
                strInventoryProcessID = value;
            }
        }
        public string RelateVoucherID
        {
            get
            {
                return strRelateVoucherID;
            }
            set
            {
                strRelateVoucherID = value;
            }
        }
        public int CollectArrearType
        {
            get
            {
                return intCollectArrearType;
            }
            set
            {
                intCollectArrearType = value;
            }
        }
        public decimal CollectArrearMoney
        {
            get
            {
                return decCollectArrearMoney;
            }
            set
            {
                decCollectArrearMoney = value;
            }
        }
        public string DutyUsers
        {
            get
            {
                return strDutyUsers;
            }
            set
            {
                strDutyUsers = value;
            }
        }

        public frmDutyUsers()
        {
            InitializeComponent();
        }

        private void frmDutyUsers_Load(object sender, EventArgs e)
        {
            if (dtbDutyUsers == null || dtbDutyUsers.Rows.Count == 0)
            {
                dtbDutyUsers = new DataTable();
                dtbDutyUsers.Columns.Add("IsSelect", typeof(bool));
                dtbDutyUsers.Columns.Add("CollectArrearID", typeof(string));
                dtbDutyUsers.Columns.Add("InventoryProcessID", typeof(string));
                dtbDutyUsers.Columns.Add("CollectArrearType", typeof(int));
                dtbDutyUsers.Columns.Add("RelateVoucherID", typeof(string));
                dtbDutyUsers.Columns.Add("UserName", typeof(string));
                dtbDutyUsers.Columns.Add("FullName", typeof(string));
                dtbDutyUsers.Columns.Add("CollectArrearMoney", typeof(decimal));
                dtbDutyUsers.Columns.Add("Note", typeof(string));
                dtbDutyUsers.Columns.Add("IsDeleted", typeof(bool));
                dtbDutyUsers.Columns.Add("DeletedUser", typeof(string));
            }
            
            flexDutyUsers.DataSource = dtbDutyUsers;
            FormatFlex();
        }

        private void FormatFlex()
        {
            if (flexDutyUsers.DataSource == null) return;
            
            flexDutyUsers.Cols[0].Visible = false;

            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnCaption(flexDutyUsers,
                "IsSelect", "Chọn",
                "UserName", "Mã nhân viên",
                "FullName", "Tên nhân viên",
                "CollectArrearMoney", "Số tiền truy thu",
                "Note", "Ghi chú");

            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexDutyUsers,
                false, "CollectArrearID,InventoryProcessID,CollectArrearType,RelateVoucherID,IsDeleted,DeletedUser");

            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnWidth(flexDutyUsers,
                "IsSelect", 40,
                "UserName", 100,
                "FullName", 180,
                "CollectArrearMoney", 100,
                "Note", 200);

            flexDutyUsers.Cols["IsSelect"].DataType = typeof(bool);

            flexDutyUsers.Cols["UserName"].ComboList = "...";
            
            flexDutyUsers.Cols["CollectArrearMoney"].Format = "#,##0";

            for (int i = 1; i < flexDutyUsers.Cols.Count; i++)
            {
                flexDutyUsers.Cols[i].AllowEditing = false;
            }

            flexDutyUsers.Cols["CollectArrearMoney"].AllowEditing = true;
            flexDutyUsers.Cols["Note"].AllowEditing = true;
            flexDutyUsers.Cols["IsSelect"].AllowEditing = true;

            C1.Win.C1FlexGrid.CellStyle style = flexDutyUsers.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = flexDutyUsers.GetCellRange(0, 0, 0, flexDutyUsers.Cols.Count - 1);
            range.Style = style;
            flexDutyUsers.Rows[0].Height = 34;
        }

        private void cmdYes_Click(object sender, EventArgs e)
        {
            DutyUsers = string.Empty;

            if (dtbDutyUsers == null || dtbDutyUsers.Rows.Count == 0)
                return;

            if (dtbDutyUsers != null && dtbDutyUsers.Rows.Count > 0)
            {
                dtbDutyUsers.AcceptChanges();
                decimal decTotal = Convert.ToDecimal(dtbDutyUsers.Compute("Sum(CollectArrearMoney)", string.Empty));
                if (decTotal != CollectArrearMoney && CollectArrearMoney != 0)
                {
                    MessageBox.Show(this, "Vui lòng nhập số tiền truy thu bằng " + CollectArrearMoney.ToString("#,###") + " VNĐ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                foreach (DataRow rDutyUser in dtbDutyUsers.Rows)
                {
                    if (Convert.IsDBNull(rDutyUser["UserName"]) || Convert.ToString(rDutyUser["UserName"]).Trim() == string.Empty)
                    {
                        MessageBox.Show(this, "Chưa nhập nhân viên truy thu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (Convert.ToDecimal(rDutyUser["CollectArrearMoney"]) == 0 && CollectArrearMoney != 0)
                    {
                        MessageBox.Show(this, "Chưa nhập tiền truy thu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    DutyUsers += Convert.ToString(rDutyUser["UserName"]) + " - " + Convert.ToString(rDutyUser["FullName"]) + ";";
                }
                DutyUsers = DutyUsers.Trim(';');
            }
            AnswerNo = false;
            AnswerYes = true;
            this.Close();
        }

        private void cmdNo_Click(object sender, EventArgs e)
        {
            AnswerYes = false;
            AnswerNo = true;
            this.Close();
        }

        private void flexDutyUsers_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            try
            {
                if (flexDutyUsers.Rows.Count <= flexDutyUsers.Rows.Fixed)
                {
                    return;
                }
                switch (e.Col)
                {
                    case 3:
                        if(!Convert.IsDBNull(flexDutyUsers[e.Row, "CollectArrearMoney"]))
                        {
                            if (Convert.ToDecimal(flexDutyUsers[e.Row, "CollectArrearMoney"]) < 0)
                            {
                                flexDutyUsers[e.Row, "CollectArrearMoney"] = Math.Abs(Convert.ToDecimal(flexDutyUsers[e.Row, "CollectArrearMoney"]));
                            }
                        }
                        break;
                }
            }
            catch
            {
            }
        }

        private void mnuItemAddUser_Click(object sender, EventArgs e)
        {
            ERP.MasterData.DUI.Search.frmUserSearch frmUserSearch1 = new MasterData.DUI.Search.frmUserSearch();
            frmUserSearch1.IsActivatedType = Library.AppCore.Constant.EnumType.IsActivatedType.ALL;
            frmUserSearch1.User = null;
            frmUserSearch1.UserList = new List<MasterData.SYS.PLC.WSUser.User>();
            frmUserSearch1.IsMultiSelect = true;
            frmUserSearch1.ShowDialog();
            if (frmUserSearch1.UserList != null && frmUserSearch1.UserList.Count > 0)
            {
                foreach (MasterData.SYS.PLC.WSUser.User objUser in frmUserSearch1.UserList)
                {
                    if (objUser != null && objUser.UserName != string.Empty)
                    {
                        DataRow[] drUsers = dtbDutyUsers.Select("UserName = '" + objUser.UserName + "'");
                        if (drUsers.Length < 1)
                        {
                            DataRow rNew = dtbDutyUsers.NewRow();
                            rNew["UserName"] = objUser.UserName;
                            rNew["FullName"] = objUser.FullName;
                            rNew["InventoryProcessID"] = strInventoryProcessID;
                            rNew["CollectArrearType"] = intCollectArrearType;
                            rNew["RelateVoucherID"] = strRelateVoucherID;
                            rNew["CollectArrearMoney"] = 0;
                            rNew["IsSelect"] = false;
                            rNew["IsDeleted"] = false;
                            rNew["DeletedUser"] = string.Empty;
                            dtbDutyUsers.Rows.Add(rNew);
                        }
                        else
                        {
                            for (int i = flexDutyUsers.Rows.Fixed; i < flexDutyUsers.Rows.Count; i++)
                            {
                                if (Convert.ToString(flexDutyUsers[i, "UserName"]) == objUser.UserName)
                                {
                                    flexDutyUsers.Select(i, flexDutyUsers.Cols["UserName"].Index);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }

        private bool CheckSelectUsers()
        {
            if (dtbDutyUsers.Rows.Count < 1)
            {
                return false;
            }
            if (flexDutyUsers.Rows.Count <= flexDutyUsers.Rows.Fixed)
            {
                return false;
            }

            dtbDutyUsers.AcceptChanges();

            DataRow[] drUsers = dtbDutyUsers.Select("IsSelect = 1");
            if (drUsers.Length < 1)
            {
                MessageBox.Show(this, "Chưa chọn nhân viên truy thu cần xóa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        private void mnuItemDeleteUser_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Bạn có chắc xóa nhân viên truy thu?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                return;
            }
            if (!CheckSelectUsers())
                return;

            for (int i = dtbDutyUsers.Rows.Count - 1; i > -1; i--)
            {
                if (Convert.ToBoolean(dtbDutyUsers.Rows[i]["IsSelect"]))
                {
                    dtbDutyUsers.Rows.RemoveAt(i);
                }
            }

        }

        private void mnuUsers_Opening(object sender, CancelEventArgs e)
        {
            if (dtbDutyUsers.Rows.Count < 1)
            {
                mnuItemDeleteUser.Enabled = false;
                return;
            }
            if (flexDutyUsers.Rows.Count <= flexDutyUsers.Rows.Fixed)
            {
                mnuItemDeleteUser.Enabled = false;
                return;
            }

            dtbDutyUsers.AcceptChanges();

            DataRow[] drUsers = dtbDutyUsers.Select("IsSelect = 1");
            if (drUsers.Length < 1)
            {
                mnuItemDeleteUser.Enabled = false;
            }
            else
            {
                mnuItemDeleteUser.Enabled = true;
            }
        }

    }
}