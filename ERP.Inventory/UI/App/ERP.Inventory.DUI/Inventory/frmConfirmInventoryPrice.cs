﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.LoadControls;
using ERP.Inventory.PLC.WSInventoryPrice;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmConfirmInventoryPrice : Form
    {
        public decimal TotalAmount { get; set; }
        private decimal decAmount = -1;
        public frmConfirmInventoryPrice()
        {
            InitializeComponent();
            txtAmount.Focus();
        }

        private void txtAmount_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            decimal.TryParse(txtAmount.Text, out decAmount);
            if (decAmount != TotalAmount)
            {
                MessageBoxObject.ShowWarningMessage(this, "Số tiền xác nhận khác với tổng tiền hóa đơn");
                txtAmount.Focus();
                return;
            }
            this.DialogResult = DialogResult.OK;

        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
