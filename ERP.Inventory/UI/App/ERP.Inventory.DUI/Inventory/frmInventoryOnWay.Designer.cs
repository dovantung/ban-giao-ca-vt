﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmInventoryOnWay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cboBranch = new Library.AppControl.ComboBoxControl.ComboBoxBranch();
            this.label1 = new System.Windows.Forms.Label();
            this.cboStoreIDList = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.gridData = new DevExpress.XtraGrid.GridControl();
            this.mnuItem = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemViewDetail = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.gridViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.cboIsReviewed = new System.Windows.Forms.ComboBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.submnuSelectALl = new System.Windows.Forms.ToolStripMenuItem();
            this.submnuUnSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAutoReview = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            this.mnuItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cboBranch
            // 
            this.cboBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBranch.Location = new System.Drawing.Point(104, 11);
            this.cboBranch.Margin = new System.Windows.Forms.Padding(0);
            this.cboBranch.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.Size = new System.Drawing.Size(483, 24);
            this.cboBranch.TabIndex = 76;
            this.cboBranch.SelectionChangeCommitted += new System.EventHandler(this.cboBranch_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 16);
            this.label1.TabIndex = 75;
            this.label1.Text = "Chi nhánh:";
            // 
            // cboStoreIDList
            // 
            this.cboStoreIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreIDList.Location = new System.Drawing.Point(104, 36);
            this.cboStoreIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreIDList.Name = "cboStoreIDList";
            this.cboStoreIDList.Size = new System.Drawing.Size(483, 24);
            this.cboStoreIDList.TabIndex = 73;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 39);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 16);
            this.label10.TabIndex = 74;
            this.label10.Text = "Kho kiểm kê:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(592, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 16);
            this.label12.TabIndex = 78;
            this.label12.Text = "Ngày kiểm tra:";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.CustomFormat = "dd/MM/yyyy";
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Location = new System.Drawing.Point(686, 11);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(163, 22);
            this.dtpEndDate.TabIndex = 77;
            // 
            // btnConfirm
            // 
            this.btnConfirm.Image = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.btnConfirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirm.Location = new System.Drawing.Point(855, 64);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(137, 25);
            this.btnConfirm.TabIndex = 79;
            this.btnConfirm.Text = "     Xác nhận dữ liệu";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(855, 10);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(137, 25);
            this.btnSearch.TabIndex = 80;
            this.btnSearch.Text = "     Tìm kiếm";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // gridData
            // 
            this.gridData.ContextMenuStrip = this.mnuItem;
            this.gridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridData.Location = new System.Drawing.Point(3, 3);
            this.gridData.MainView = this.gridViewData;
            this.gridData.Name = "gridData";
            this.gridData.Size = new System.Drawing.Size(996, 431);
            this.gridData.TabIndex = 81;
            this.gridData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewData});
            this.gridData.DoubleClick += new System.EventHandler(this.gridData_DoubleClick);
            // 
            // mnuItem
            // 
            this.mnuItem.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemViewDetail,
            this.mnuItemExportExcel,
            this.submnuSelectALl,
            this.submnuUnSelectAll});
            this.mnuItem.Name = "mnuItem";
            this.mnuItem.Size = new System.Drawing.Size(138, 92);
            // 
            // mnuItemViewDetail
            // 
            this.mnuItemViewDetail.Image = global::ERP.Inventory.DUI.Properties.Resources.view;
            this.mnuItemViewDetail.Name = "mnuItemViewDetail";
            this.mnuItemViewDetail.Size = new System.Drawing.Size(137, 22);
            this.mnuItemViewDetail.Text = "Xem chi tiết";
            this.mnuItemViewDetail.Click += new System.EventHandler(this.mnuItemViewDetail_Click);
            // 
            // mnuItemExportExcel
            // 
            this.mnuItemExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportExcel.Name = "mnuItemExportExcel";
            this.mnuItemExportExcel.Size = new System.Drawing.Size(137, 22);
            this.mnuItemExportExcel.Text = "Xuất Excel";
            this.mnuItemExportExcel.Click += new System.EventHandler(this.mnuItemExportExcel_Click);
            // 
            // gridViewData
            // 
            this.gridViewData.GridControl = this.gridData;
            this.gridViewData.Name = "gridViewData";
            this.gridViewData.OptionsSelection.MultiSelect = true;
            this.gridViewData.OptionsView.ShowGroupPanel = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 104);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1010, 466);
            this.tabControl1.TabIndex = 82;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gridData);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1002, 437);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Danh sách phiếu chuyển kho chưa được nhận";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAutoReview);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cboIsReviewed);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.cboBranch);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.cboStoreIDList);
            this.panel1.Controls.Add(this.btnConfirm);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.dtpEndDate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1010, 104);
            this.panel1.TabIndex = 83;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(592, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 16);
            this.label2.TabIndex = 83;
            this.label2.Text = "Trạng thái:";
            // 
            // cboIsReviewed
            // 
            this.cboIsReviewed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIsReviewed.FormattingEnabled = true;
            this.cboIsReviewed.Items.AddRange(new object[] {
            "Tất cả",
            "Chưa xác nhận dữ liệu",
            "Đã xác nhận dữ liệu"});
            this.cboIsReviewed.Location = new System.Drawing.Point(686, 38);
            this.cboIsReviewed.Name = "cboIsReviewed";
            this.cboIsReviewed.Size = new System.Drawing.Size(163, 24);
            this.cboIsReviewed.TabIndex = 82;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(855, 38);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(137, 25);
            this.btnUpdate.TabIndex = 81;
            this.btnUpdate.Text = "     Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // submnuSelectALl
            // 
            this.submnuSelectALl.Image = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.submnuSelectALl.Name = "submnuSelectALl";
            this.submnuSelectALl.Size = new System.Drawing.Size(137, 22);
            this.submnuSelectALl.Text = "Chọn tất cả";
            this.submnuSelectALl.Click += new System.EventHandler(this.submnuSelectALl_Click);
            // 
            // submnuUnSelectAll
            // 
            this.submnuUnSelectAll.Image = global::ERP.Inventory.DUI.Properties.Resources.undo;
            this.submnuUnSelectAll.Name = "submnuUnSelectAll";
            this.submnuUnSelectAll.Size = new System.Drawing.Size(137, 22);
            this.submnuUnSelectAll.Text = "Bỏ tất cả";
            this.submnuUnSelectAll.Click += new System.EventHandler(this.submnuUnSelectAll_Click);
            // 
            // btnAutoReview
            // 
            this.btnAutoReview.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAutoReview.Appearance.Options.UseFont = true;
            this.btnAutoReview.Enabled = false;
            this.btnAutoReview.Image = global::ERP.Inventory.DUI.Properties.Resources.arrow_split_090;
            this.btnAutoReview.Location = new System.Drawing.Point(686, 66);
            this.btnAutoReview.Name = "btnAutoReview";
            this.btnAutoReview.Size = new System.Drawing.Size(163, 23);
            this.btnAutoReview.TabIndex = 84;
            this.btnAutoReview.Text = "Tự động giải trình HT";
            this.btnAutoReview.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // frmInventoryOnWay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1010, 570);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmInventoryOnWay";
            this.ShowIcon = false;
            this.Text = "Kiểm kê hàng đi đường";
            this.Load += new System.EventHandler(this.frmInventoryOnWay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            this.mnuItem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewData)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Library.AppControl.ComboBoxControl.ComboBoxBranch cboBranch;
        private System.Windows.Forms.Label label1;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStoreIDList;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Button btnSearch;
        private DevExpress.XtraGrid.GridControl gridData;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewData;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ContextMenuStrip mnuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuItemViewDetail;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboIsReviewed;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcel;
        private System.Windows.Forms.ToolStripMenuItem submnuSelectALl;
        private System.Windows.Forms.ToolStripMenuItem submnuUnSelectAll;
        private DevExpress.XtraEditors.SimpleButton btnAutoReview;
    }
}