﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmCreateQuickInventory : Form
    {
        public frmCreateQuickInventory()
        {
            InitializeComponent();
        }

        #region variable
        private int intStoreCurrent = 0;
        private bool bolIsEdit = false;
        private bool bolIsInventory = false;
        private PLC.Inventory.PLCQuickInventory objPLCQuickInventory = new PLC.Inventory.PLCQuickInventory();
        private DataTable dtbSubGroup = Library.AppCore.DataSource.GetDataSource.GetSubGroup().Copy();
        private DataTable dtbBrand = Library.AppCore.DataSource.GetDataSource.GetBrand().Copy();
        #endregion

        #region method

        private void LoadComboBox()
        {
            try
            {
                Library.AppCore.LoadControls.SetDataSource.SetStore(this, cboStoreID, true);
                cboStoreID.InitControl(false, true);
                cboStoreID.SetValue(SystemConfig.intDefaultStoreID);
                intStoreCurrent = cboStoreID.StoreID;
                Library.AppCore.LoadControls.SetDataSource.SetMainGroup(this, cboMainGroup, false);
                cboMainGroup.InitControl(true);
                cboMainGroup.IsReturnAllWhenNotChoose = true;
                cboSubGroup.InitControl(true, dtbSubGroup);
                cboSubGroup.IsReturnAllWhenNotChoose = true;
                cboBrandID.InitControl(true, dtbBrand);
                cboBrandID.IsReturnAllWhenNotChoose = true;
                DataTable dtbProductStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
                cboProductStateID.InitControl(false, dtbProductStatus, "PRODUCTSTATUSID", "PRODUCTSTATUSNAME", "--- Tất cả ---");
                cboIsCheckRealInput.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void InitControl()
        {
            try
            {
                btnExportExcel.Enabled = grvStoreChangeOrderDetail.RowCount > 0;
                if (!bolIsEdit)
                {
                    // cboStoreID.Enabled = !bolIsInventory;
                    cboMainGroup.Enabled = !bolIsInventory;
                    cboSubGroup.Enabled = !bolIsInventory;
                    cboBrandID.Enabled = !bolIsInventory;
                    cboIsCheckRealInput.Enabled = !bolIsInventory;
                    //radIsNew.Enabled = !bolIsInventory;
                    //radOld.Enabled = !bolIsInventory;
                    cboProductStateID.Enabled = !bolIsInventory;
                    dtpLockStockTime.Enabled = !bolIsInventory;
                    btnInventory.Enabled = !bolIsInventory;
                    btnUpdate.Enabled = bolIsInventory;
                }
                else
                {
                    //cboStoreID.Enabled = false;
                    cboMainGroup.Enabled = false;
                    cboSubGroup.Enabled = false;
                    cboBrandID.Enabled = false;
                    cboIsCheckRealInput.Enabled = false;
                    // radIsNew.Enabled = false;
                    // radOld.Enabled = false;
                    cboProductStateID.Enabled = false;
                    dtpLockStockTime.Enabled = false;
                    btnInventory.Enabled = false;
                    btnUpdate.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void ClearControl()
        {
            bolIsEdit = false;
            bolIsInventory = false;
            InitControl();
            LoadComboBox();
            txtContent.Text = string.Empty;
            txtNote.Text = string.Empty;
            grdStoreChangeOrderDetail.DataSource = null;
            grvStoreChangeOrderDetail.RefreshData();
            grdStoreChangeOrderDetail.Refresh();
        }
        #endregion

        #region event

        private void frmCreateQuickInventory_Load(object sender, EventArgs e)
        {
            LoadComboBox();
            btnExportExcel.Enabled = grvStoreChangeOrderDetail.RowCount > 0;
            dtpLockStockTime.Value = Library.AppCore.Globals.GetServerDateTime();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvStoreChangeOrderDetail);
        }

        private void cboMainGroup_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                DataTable dtbTemp = dtbSubGroup.Clone();
                if (!string.IsNullOrWhiteSpace(cboMainGroup.MainGroupIDList))
                {
                    string strMainGroupIDList = cboMainGroup.MainGroupIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
                    string[] arrMainGroupID = strMainGroupIDList.Split(',');
                    foreach (var item in arrMainGroupID)
                    {
                        DataRow[] drTmp = dtbSubGroup.Select("MAINGROUPID=" + item.Trim());
                        if (drTmp.Any() && drTmp.Count() > 0)
                        {
                            foreach (DataRow objTmp in drTmp)
                            {
                                dtbTemp.ImportRow(objTmp);
                            }
                        }
                    }
                    if (dtbTemp != null)
                    {
                        cboSubGroup.InitControl(true, dtbTemp);
                        cboSubGroup.IsReturnAllWhenNotChoose = true;
                    }

                    //Library.AppCore.LoadControls.SetDataSource.SetBrand(this, cboBrandID, cboMainGroup.MainGroupIDList);
                    //cboBrandID.InitControl(true, cboBrandID.DataSource);
                }
                else
                {
                    cboSubGroup.InitControl(true, dtbSubGroup);
                    cboSubGroup.IsReturnAllWhenNotChoose = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnInventory_Click(object sender, EventArgs e)
        {
            bolIsInventory = true;
            try
            {
                if (cboStoreID.StoreID <= 0)
                {
                    MessageBox.Show(this, "Chưa chọn kho kiểm kê!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboStoreID.Focus();
                    bolIsInventory = false;
                    return;
                }
                if (dtpLockStockTime.Value.Date > Library.AppCore.Globals.GetServerDateTime().Date)
                {
                    MessageBox.Show(this, "Ngày chốt tồn không dược lớn hơn ngày hiện tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    dtpLockStockTime.Focus();
                    bolIsInventory = false;
                    return;
                }

                DataTable dtbInStockData = null;
                DataTable dtbInStockIMEI = null;
                //objPLCQuickInventory.GetInstockDataViettel(ref dtbInStockData, ref dtbInStockIMEI, dtpLockStockTime.Value, cboStoreID.StoreID, cboMainGroup.MainGroupIDList, cboSubGroup.SubGroupIDList, cboBrandID.BrandIDList, radIsNew.Checked ? 1 : 0, cboIsCheckRealInput.SelectedIndex - 1);
                int intStatus = cboProductStateID.ColumnID;
                 objPLCQuickInventory.GetInstockDataViettel(ref dtbInStockData, ref dtbInStockIMEI, dtpLockStockTime.Value, cboStoreID.StoreID, cboMainGroup.MainGroupIDList, cboSubGroup.SubGroupIDList, cboBrandID.BrandIDList, intStatus, cboIsCheckRealInput.SelectedIndex - 1);
                 grdStoreChangeOrderDetail.DataSource = null;
                 if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                 {
                     MessageBox.Show(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                     bolIsInventory = false;
                 }
                 else
                 {
                     if (dtbInStockData != null && dtbInStockData.Rows.Count > 0)
                     {
                         grdStoreChangeOrderDetail.DataSource = dtbInStockData;

                         decimal decQuantity1 = dtbInStockData.AsEnumerable().Sum(x => x.Field<decimal>("QUANTITYDIFFERENCE"));
                         if (decQuantity1 > 0)
                         {
                             grvStoreChangeOrderDetail.Columns["QUANTITY"].Summary[1].DisplayFormat = "Thiếu: " + Math.Abs(decQuantity1).ToString("#,##0");
                         }
                         else
                         {
                             grvStoreChangeOrderDetail.Columns["QUANTITY"].Summary[1].DisplayFormat = "Thiếu: 0";
                         }
                         if (decQuantity1 < 0)
                         {
                             grvStoreChangeOrderDetail.Columns["INSTOCKQUANTITY"].Summary[1].DisplayFormat = "Thừa: " + Math.Abs(decQuantity1).ToString("#,##0");
                         }
                         else
                         {
                             grvStoreChangeOrderDetail.Columns["INSTOCKQUANTITY"].Summary[1].DisplayFormat = "Thừa: 0";
                         }
                     }
                     else
                     {
                         MessageBox.Show(this, "Không tìm thấy thông tin sản phẩm kiểm kê!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                         bolIsInventory = false;
                     }
                 }
                
                InitControl();
                btnExportExcel.Enabled = grvStoreChangeOrderDetail.RowCount > 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void mnuItemExportTemplateExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdStoreChangeOrderDetail.DataSource != null)
                {
                    DataTable dtbData = ((DataTable)grdStoreChangeOrderDetail.DataSource).Copy();
                    foreach (DataRow dr in dtbData.Rows)
                    {
                        dr["QUANTITY"] = 0;
                    }
                    List<string> lstFields = new List<string>();
                    List<string> lstFieldsValue = new List<string>();
                    List<int> lstWidths = new List<int>();
                    lstFields.Add("Mã sản phẩm"); lstWidths.Add(150); lstFieldsValue.Add("PRODUCTID");
                    lstFields.Add("Tên sản phẩm"); lstWidths.Add(250); lstFieldsValue.Add("PRODUCTNAME");
                    lstFields.Add("Số lượng thực tế"); lstWidths.Add(150); lstFieldsValue.Add("QUANTITY");
                    lstFields.Add("Ghi chú"); lstWidths.Add(250); lstFieldsValue.Add("NOTE");
                    grdTemp.DataSource = dtbData;
                    MasterData.DUI.Common.GridCustom.FormatGrid(grvTemp, lstFieldsValue.ToArray(), lstFields.ToArray(), lstWidths.ToArray(), false);
                    Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdTemp);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void mnuItemImportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtbData = ((DataTable)grdStoreChangeOrderDetail.DataSource).Copy();
                decimal decQuantity1 = dtbData.AsEnumerable().Sum(x => x.Field<decimal?>("QUANTITY") ?? 0);
                if (decQuantity1 > 0)
                {
                    if (MessageBox.Show(this, "Dữ liệu trên lưới sẽ bị thay đổi. Bạn có muốn thực hiện không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        return;
                    }
                }
                ImportProduct objImportProduct = new ImportProduct();
                objImportProduct.HwdOwner = this;
                objImportProduct.dtbProduct = dtbData;
                DataTable dtbDataResult = null;
                objImportProduct.Import(ref dtbDataResult);
                if (dtbDataResult != null)
                {
                    foreach (DataRow row in dtbDataResult.Rows)
                    {
                        DataRow[] drData = dtbData.Select("PRODUCTID='" + row["PRODUCTID"].ToString() + "'");
                        if (drData != null && drData.Count() > 0)
                        {
                            decimal decQuantity = 0;
                            decimal decInStockQuantity = 0;
                            decimal.TryParse(row["QUANTITY"].ToString(), out decQuantity);
                            decimal.TryParse(drData[0]["INSTOCKQUANTITY"].ToString(), out decInStockQuantity);
                            drData[0]["QUANTITY"] = row["QUANTITY"].ToString();
                            drData[0]["QUANTITYDIFFERENCE"] = decInStockQuantity - decQuantity;
                            drData[0]["NOTE"] = row["NOTE1"].ToString();
                        }
                    }
                }
                grdStoreChangeOrderDetail.DataSource = dtbData;
                grvStoreChangeOrderDetail.RefreshData();
                DataTable dtbDataTemp = ((DataTable)grdStoreChangeOrderDetail.DataSource);
                decimal decQuantityCheck = dtbDataTemp.AsEnumerable().Sum(x => x.Field<decimal>("QUANTITYDIFFERENCE"));
                if (decQuantityCheck > 0)
                {
                    grvStoreChangeOrderDetail.Columns["QUANTITY"].Summary[1].DisplayFormat = "Thiếu: " + Math.Abs(decQuantityCheck).ToString("#,##0");
                }
                else
                {
                    grvStoreChangeOrderDetail.Columns["QUANTITY"].Summary[1].DisplayFormat = "Thiếu: 0";
                }
                if (decQuantityCheck < 0)
                {
                    grvStoreChangeOrderDetail.Columns["INSTOCKQUANTITY"].Summary[1].DisplayFormat = "Thừa: " + Math.Abs(decQuantityCheck).ToString("#,##0");
                }
                else
                {
                    grvStoreChangeOrderDetail.Columns["INSTOCKQUANTITY"].Summary[1].DisplayFormat = "Thừa: 0";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void mnuDetail_Opening(object sender, CancelEventArgs e)
        {
            DataTable dtbData = ((DataTable)grdStoreChangeOrderDetail.DataSource);
            if (dtbData != null && dtbData.Rows.Count > 0)
            {
                mnuItemImportExcel.Enabled = true && SystemConfig.objSessionUser.IsPermission("INV_QUICKINVENTORY_IMPORT");
                mnuItemExportTemplateExcel.Enabled = true && SystemConfig.objSessionUser.IsPermission("INV_QUICKINVENTORY_EXPORT");
            }
            else
            {
                mnuItemImportExcel.Enabled = false;
                mnuItemExportTemplateExcel.Enabled = false;
            }
        }

        private void txtQuantity_Leave(object sender, EventArgs e)
        {
            try
            {
                if (grvStoreChangeOrderDetail.FocusedRowHandle < 0)
                    return;
                DataRowView drvData = (DataRowView)grvStoreChangeOrderDetail.GetFocusedRow();
                if (drvData != null)
                {
                    decimal decQuantity = 0;
                    decimal decInStockQuantity = 0;
                    decimal.TryParse(drvData.Row["QUANTITY"].ToString(), out decQuantity);
                    decimal.TryParse(drvData.Row["INSTOCKQUANTITY"].ToString(), out decInStockQuantity);
                    drvData.Row["QUANTITYDIFFERENCE"] = decInStockQuantity - decQuantity;
                }
                grvStoreChangeOrderDetail.RefreshData();
                DataTable dtbData = ((DataTable)grdStoreChangeOrderDetail.DataSource);
                if (dtbData == null || dtbData.Rows.Count == 0)
                {
                    return;
                }
                decimal decQuantity1 = dtbData.AsEnumerable().Sum(x => x.Field<decimal>("QUANTITYDIFFERENCE"));
                if (decQuantity1 > 0)
                {
                    grvStoreChangeOrderDetail.Columns["QUANTITY"].Summary[1].DisplayFormat = "Thiếu: " + Math.Abs(decQuantity1).ToString("#,##0");
                }
                else
                {
                    grvStoreChangeOrderDetail.Columns["QUANTITY"].Summary[1].DisplayFormat = "Thiếu: 0";
                }
                if (decQuantity1 < 0)
                {
                    grvStoreChangeOrderDetail.Columns["INSTOCKQUANTITY"].Summary[1].DisplayFormat = "Thừa: " + Math.Abs(decQuantity1).ToString("#,##0");
                }
                else
                {
                    grvStoreChangeOrderDetail.Columns["INSTOCKQUANTITY"].Summary[1].DisplayFormat = "Thừa: 0";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void grvStoreChangeOrderDetail_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                if (grvStoreChangeOrderDetail.FocusedRowHandle < 0) return;
                DataRowView item = (DataRowView)grvStoreChangeOrderDetail.GetFocusedRow();
                if (e.Column.FieldName == "QUANTITY")
                {
                    DataRowView drvData = (DataRowView)grvStoreChangeOrderDetail.GetFocusedRow();
                    if (drvData != null)
                    {
                        decimal decQuantity = 0;
                        decimal decInStockQuantity = 0;
                        decimal.TryParse(drvData.Row["QUANTITY"].ToString(), out decQuantity);
                        decimal.TryParse(drvData.Row["INSTOCKQUANTITY"].ToString(), out decInStockQuantity);
                        drvData.Row["QUANTITYDIFFERENCE"] = decInStockQuantity - decQuantity;
                    }
                    grvStoreChangeOrderDetail.RefreshData();
                    DataTable dtbData = ((DataTable)grdStoreChangeOrderDetail.DataSource);
                    if (dtbData == null || dtbData.Rows.Count == 0)
                    {
                        return;
                    }
                    decimal decQuantity1 = dtbData.AsEnumerable().Sum(x => x.Field<decimal>("QUANTITYDIFFERENCE"));
                    if (decQuantity1 > 0)
                    {
                        grvStoreChangeOrderDetail.Columns["QUANTITY"].Summary[1].DisplayFormat = "Thiếu: " + Math.Abs(decQuantity1).ToString("#,##0");
                    }
                    else
                    {
                        grvStoreChangeOrderDetail.Columns["QUANTITY"].Summary[1].DisplayFormat = "Thiếu: 0";
                    }
                    if (decQuantity1 < 0)
                    {
                        grvStoreChangeOrderDetail.Columns["INSTOCKQUANTITY"].Summary[1].DisplayFormat = "Thừa: " + Math.Abs(decQuantity1).ToString("#,##0");
                    }
                    else
                    {
                        grvStoreChangeOrderDetail.Columns["INSTOCKQUANTITY"].Summary[1].DisplayFormat = "Thừa: 0";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string strMainGroupIDList = cboMainGroup.MainGroupIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
                string[] arrMainGroupID = strMainGroupIDList.Split(',');
                string strSubGroupIDList = cboSubGroup.SubGroupIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
                string[] arrSubGroupID = strSubGroupIDList.Split(',');
                string strBrandIDList = cboBrandID.BrandIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
                string[] arrBrandIDList = strBrandIDList.Split(',');
                List<int> lstBrandIDList = new List<int>();
                foreach (string strBrandID in arrBrandIDList)
                {
                    lstBrandIDList.Add(Convert.ToInt32(strBrandID));
                }
                PLC.Inventory.WSQuickInventory.QuickInventory objQuickInventory = new PLC.Inventory.WSQuickInventory.QuickInventory();
                objQuickInventory.BrandIDList = lstBrandIDList.ToArray();
                objQuickInventory.Content = txtContent.Text;
                objQuickInventory.InventoryStoreID = cboStoreID.StoreID;
                //objQuickInventory.IsNewType = radIsNew.Checked ? 1 : 0;
                objQuickInventory.IsNewType = cboProductStateID.ColumnID;
                objQuickInventory.LockStockTime = dtpLockStockTime.Value;
                objQuickInventory.MainGroupID = -1;
                objQuickInventory.SubGroupID = -1;
                objQuickInventory.BrandID = -1;
                objQuickInventory.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objQuickInventory.InventoryStoreID = SystemConfig.intDefaultStoreID;
                objQuickInventory.MainGroupIDList = arrMainGroupID;
                objQuickInventory.SubGroupIDList = arrSubGroupID;
                DataTable dtbData = ((DataTable)grdStoreChangeOrderDetail.DataSource).Copy();
                List<PLC.Inventory.WSQuickInventory.QuickInventoryDetail> lstQuickInventoryDetail = new List<PLC.Inventory.WSQuickInventory.QuickInventoryDetail>();
                foreach (DataRow drData in dtbData.Rows)
                {
                    PLC.Inventory.WSQuickInventory.QuickInventoryDetail objQuickInventoryDetail = new PLC.Inventory.WSQuickInventory.QuickInventoryDetail();
                    objQuickInventoryDetail.InstockQuantity = Convert.ToDecimal(drData["INSTOCKQUANTITY"].ToString());
                    objQuickInventoryDetail.Quantity = Convert.ToDecimal(drData["QUANTITY"].ToString());
                    objQuickInventoryDetail.ProductID = drData["PRODUCTID"].ToString().Trim();
                    objQuickInventoryDetail.ProductStatusID = Convert.ToInt32(drData["PRODUCTSTATUSID"].ToString());
                    objQuickInventoryDetail.Note = drData["NOTE"].ToString().Trim();
                    objQuickInventoryDetail.CabinetNumber = 0;
                    lstQuickInventoryDetail.Add(objQuickInventoryDetail);
                }
                objQuickInventory.QuickInventoryDetailList = lstQuickInventoryDetail.ToArray();
                string strQuickInventoryID = string.Empty;
                if (!objPLCQuickInventory.InsertViettel(objQuickInventory, ref strQuickInventoryID))
                {
                    MessageBox.Show(this, "Tạo phiếu kiểm kê nhanh không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                MessageBox.Show(this, "Tạo phiếu kiểm kê nhanh thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            ClearControl();
        }
        #endregion

        private void grvStoreChangeOrderDetail_CustomDrawRowFooter(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
        {
            //this.grvStoreChangeOrderDetail.master
        }

        private void grvStoreChangeOrderDetail_CustomDrawFooterCell(object sender, DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs e)
        {

            if (e.Column.FieldName == "PRODUCTID")
            {
                e.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            }
            //if (e.Column.FieldName == "INSTOCKQUANTITY")
            //{
            //    // grvStoreChangeOrderDetail.RefreshData();
            //    DataTable dtbData = ((DataTable)grdStoreChangeOrderDetail.DataSource);
            //    if (dtbData == null || dtbData.Rows.Count == 0)
            //    {
            //        return;
            //    }
            //    decimal decQuantity1 = dtbData.Copy().AsEnumerable().Sum(x => x.Field<decimal>("QUANTITYDIFFERENCE"));
            //    if (decQuantity1 < 0)
            //    {
            //        e.Column.Summary[1].DisplayFormat = "Thừa: " + Math.Abs(decQuantity1).ToString("#,##0");
            //        e.Info.Column.Summary[1].DisplayFormat = "Thừa: " + decQuantity1.ToString("#,##0");

            //    }
            //    else
            //    {
            //        e.Column.Summary[1].DisplayFormat = "Thừa: 0";
            //        e.Info.Column.Summary[1].DisplayFormat = "Thừa: 0";
            //    }
            //}
            //if (e.Column.FieldName == "QUANTITY")
            //{
            //    // grvStoreChangeOrderDetail.RefreshData();
            //    DataTable dtbData = ((DataTable)grdStoreChangeOrderDetail.DataSource);
            //    if (dtbData == null || dtbData.Rows.Count == 0)
            //    {
            //        return;
            //    }
            //    decimal decQuantity1 = dtbData.Copy().AsEnumerable().Sum(x => x.Field<decimal>("QUANTITYDIFFERENCE"));
            //    if (decQuantity1 > 0)
            //    {
            //        e.Column.Summary[1].DisplayFormat = "Thiếu: " + decQuantity1.ToString("#,##0");
            //        e.Info.Column.Summary[1].DisplayFormat = "Thiếu: " + decQuantity1.ToString("#,##0");
            //    }
            //    else
            //    {
            //        e.Column.Summary[1].DisplayFormat = "Thiếu: 0";
            //        e.Info.Column.Summary[1].DisplayFormat = "Thiếu: 0";
            //    }
            //}
            //grdStoreChangeOrderDetail.Refresh();
        }

        private void grvStoreChangeOrderDetail_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
        }

        private void grvStoreChangeOrderDetail_ShowingEditor(object sender, CancelEventArgs e)
        {
            try
            {
                if (grvStoreChangeOrderDetail.FocusedRowHandle < 0)
                    return;
                if (grvStoreChangeOrderDetail.FocusedColumn.FieldName.ToUpper() == "QUANTITY")
                {
                    DataRowView drvData = (DataRowView)grvStoreChangeOrderDetail.GetFocusedRow();
                    if (drvData != null)
                    {
                        if (drvData.Row["ISALLOWDECIMAL"].ToString() != "1")
                        {
                            txtQuantity.Mask.EditMask = "###,###,###,##0;";
                        }
                        else
                        {
                            txtQuantity.Mask.EditMask = "###,###,###,##0.####;";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void cboStoreID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (grvStoreChangeOrderDetail.RowCount > 0 && !btnInventory.Enabled)
            {
                DialogResult dialogResult = MessageBox.Show(this, "Thay đổi kho kiểm kê sẽ xóa các thông tin bên dưới lưới!\nBạn có chắc muốn thay đổi không?"
                    , "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    (grdStoreChangeOrderDetail.DataSource as DataTable).Clear();
                    grdStoreChangeOrderDetail.RefreshDataSource();
                    grvStoreChangeOrderDetail.Columns["QUANTITY"].Summary[1].DisplayFormat = "Thiếu: 0";
                    grvStoreChangeOrderDetail.Columns["INSTOCKQUANTITY"].Summary[1].DisplayFormat = "Thừa: 0";
                    bolIsInventory = false;
                    InitControl();
                    intStoreCurrent = cboStoreID.StoreID;
                }
                else
                {
                    bolIsInventory = true;
                    InitControl();
                    cboStoreID.SetValue(intStoreCurrent);

                }
            }
            else
            {
                intStoreCurrent = cboStoreID.StoreID;
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdStoreChangeOrderDetail);
        }
    }
}
