﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmShowUnEventVTS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkOnlyUnEvent = new System.Windows.Forms.CheckBox();
            this.btnViewReport = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.grdCtlProducts = new DevExpress.XtraGrid.GridControl();
            this.grdViewProducts = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.colPRODUCTID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPRODUCTNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colERP_IMEI = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colERP_PRODUCTSTATUSNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colERP_QUANTITY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.reQuantityShow = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colINV_IMEI = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colINV_PRODUCTSTATUSNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colINV_QUANTITY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDIFFERENT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colQUANTITY_INPUT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colQUANTITY_SALE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colQUANTITY_STORECHANGE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colQUANTITY_OUTPUT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colQUANTITY_DIFFERENT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCAUSEGROUPSLIST = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repCauseGroups = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.colUNEVENTEXPLAIN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repTextImei = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.panel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCtlProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reQuantityShow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCauseGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextImei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkOnlyUnEvent);
            this.panel1.Controls.Add(this.btnViewReport);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 603);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1207, 50);
            this.panel1.TabIndex = 0;
            // 
            // chkOnlyUnEvent
            // 
            this.chkOnlyUnEvent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkOnlyUnEvent.AutoSize = true;
            this.chkOnlyUnEvent.Location = new System.Drawing.Point(691, 12);
            this.chkOnlyUnEvent.Name = "chkOnlyUnEvent";
            this.chkOnlyUnEvent.Size = new System.Drawing.Size(279, 24);
            this.chkOnlyUnEvent.TabIndex = 82;
            this.chkOnlyUnEvent.Text = "Chỉ hiển thị sản phẩm chênh lệch";
            this.chkOnlyUnEvent.UseVisualStyleBackColor = true;
            this.chkOnlyUnEvent.CheckedChanged += new System.EventHandler(this.chkOnlyUnEvent_CheckedChanged);
            // 
            // btnViewReport
            // 
            this.btnViewReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewReport.Enabled = false;
            this.btnViewReport.Image = global::ERP.Inventory.DUI.Properties.Resources.Printer;
            this.btnViewReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnViewReport.Location = new System.Drawing.Point(976, 13);
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.Size = new System.Drawing.Size(122, 25);
            this.btnViewReport.TabIndex = 81;
            this.btnViewReport.Text = "      In chênh lệch";
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Enabled = false;
            this.btnSave.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(1104, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(91, 25);
            this.btnSave.TabIndex = 80;
            this.btnSave.Text = "      Cập nhật";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemExportExcel});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(151, 30);
            // 
            // mnuItemExportExcel
            // 
            this.mnuItemExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportExcel.Name = "mnuItemExportExcel";
            this.mnuItemExportExcel.Size = new System.Drawing.Size(150, 26);
            this.mnuItemExportExcel.Text = "Xuất Excel";
            this.mnuItemExportExcel.Click += new System.EventHandler(this.mnuItemExportExcel_Click);
            // 
            // grdCtlProducts
            // 
            this.grdCtlProducts.ContextMenuStrip = this.contextMenuStrip1;
            this.grdCtlProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCtlProducts.Location = new System.Drawing.Point(0, 0);
            this.grdCtlProducts.MainView = this.grdViewProducts;
            this.grdCtlProducts.Name = "grdCtlProducts";
            this.grdCtlProducts.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repTextImei,
            this.repQuantity,
            this.repNote,
            this.reQuantityShow,
            this.repCauseGroups});
            this.grdCtlProducts.Size = new System.Drawing.Size(1207, 603);
            this.grdCtlProducts.TabIndex = 83;
            this.grdCtlProducts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewProducts});
            // 
            // grdViewProducts
            // 
            this.grdViewProducts.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.BandPanel.Options.UseFont = true;
            this.grdViewProducts.Appearance.BandPanel.Options.UseTextOptions = true;
            this.grdViewProducts.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewProducts.Appearance.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewProducts.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProducts.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewProducts.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewProducts.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewProducts.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewProducts.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProducts.Appearance.Preview.Options.UseFont = true;
            this.grdViewProducts.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProducts.Appearance.Row.Options.UseFont = true;
            this.grdViewProducts.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand4,
            this.gridBand8,
            this.gridBand16,
            this.gridBand12,
            this.gridBand18,
            this.gridBand20,
            this.gridBand19});
            this.grdViewProducts.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colPRODUCTID,
            this.colPRODUCTNAME,
            this.colERP_IMEI,
            this.colERP_QUANTITY,
            this.colERP_PRODUCTSTATUSNAME,
            this.colINV_IMEI,
            this.colINV_QUANTITY,
            this.colINV_PRODUCTSTATUSNAME,
            this.colDIFFERENT,
            this.colQUANTITY_INPUT,
            this.colQUANTITY_SALE,
            this.colQUANTITY_STORECHANGE,
            this.colQUANTITY_OUTPUT,
            this.colQUANTITY_DIFFERENT,
            this.colUNEVENTEXPLAIN,
            this.colCAUSEGROUPSLIST});
            this.grdViewProducts.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewProducts.GridControl = this.grdCtlProducts;
            this.grdViewProducts.MinBandPanelRowCount = 2;
            this.grdViewProducts.Name = "grdViewProducts";
            this.grdViewProducts.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewProducts.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewProducts.OptionsNavigation.UseTabKey = false;
            this.grdViewProducts.OptionsView.ColumnAutoWidth = false;
            this.grdViewProducts.OptionsView.ShowAutoFilterRow = true;
            this.grdViewProducts.OptionsView.ShowColumnHeaders = false;
            this.grdViewProducts.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewProducts.OptionsView.ShowFooter = true;
            this.grdViewProducts.OptionsView.ShowGroupPanel = false;
            this.grdViewProducts.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.grdViewProducts_RowStyle);
            // 
            // colPRODUCTID
            // 
            this.colPRODUCTID.Caption = "PRODUCTID";
            this.colPRODUCTID.FieldName = "PRODUCTID";
            this.colPRODUCTID.Name = "colPRODUCTID";
            this.colPRODUCTID.OptionsColumn.AllowEdit = false;
            this.colPRODUCTID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPRODUCTID.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "PRODUCTID", "Tổng cổng")});
            this.colPRODUCTID.Visible = true;
            this.colPRODUCTID.Width = 120;
            // 
            // colPRODUCTNAME
            // 
            this.colPRODUCTNAME.Caption = "PRODUCTNAME";
            this.colPRODUCTNAME.FieldName = "PRODUCTNAME";
            this.colPRODUCTNAME.Name = "colPRODUCTNAME";
            this.colPRODUCTNAME.OptionsColumn.AllowEdit = false;
            this.colPRODUCTNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPRODUCTNAME.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "PRODUCTNAME", "{0:N0}")});
            this.colPRODUCTNAME.Visible = true;
            this.colPRODUCTNAME.Width = 220;
            // 
            // colERP_IMEI
            // 
            this.colERP_IMEI.Caption = "ERP_IMEI";
            this.colERP_IMEI.FieldName = "ERP_IMEI";
            this.colERP_IMEI.Name = "colERP_IMEI";
            this.colERP_IMEI.OptionsColumn.AllowEdit = false;
            this.colERP_IMEI.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colERP_IMEI.Visible = true;
            this.colERP_IMEI.Width = 130;
            // 
            // colERP_PRODUCTSTATUSNAME
            // 
            this.colERP_PRODUCTSTATUSNAME.Caption = "ERP_PRODUCTSTATUSNAME";
            this.colERP_PRODUCTSTATUSNAME.FieldName = "ERP_PRODUCTSTATUSNAME";
            this.colERP_PRODUCTSTATUSNAME.Name = "colERP_PRODUCTSTATUSNAME";
            this.colERP_PRODUCTSTATUSNAME.OptionsColumn.AllowEdit = false;
            this.colERP_PRODUCTSTATUSNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colERP_PRODUCTSTATUSNAME.Visible = true;
            this.colERP_PRODUCTSTATUSNAME.Width = 130;
            // 
            // colERP_QUANTITY
            // 
            this.colERP_QUANTITY.Caption = "ERP_QUANTITY";
            this.colERP_QUANTITY.ColumnEdit = this.reQuantityShow;
            this.colERP_QUANTITY.FieldName = "ERP_QUANTITY";
            this.colERP_QUANTITY.Name = "colERP_QUANTITY";
            this.colERP_QUANTITY.OptionsColumn.AllowEdit = false;
            this.colERP_QUANTITY.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colERP_QUANTITY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ERP_QUANTITY", "{0:N0}")});
            this.colERP_QUANTITY.Visible = true;
            this.colERP_QUANTITY.Width = 70;
            // 
            // reQuantityShow
            // 
            this.reQuantityShow.AutoHeight = false;
            this.reQuantityShow.Mask.EditMask = "N0";
            this.reQuantityShow.Mask.UseMaskAsDisplayFormat = true;
            this.reQuantityShow.Name = "reQuantityShow";
            // 
            // colINV_IMEI
            // 
            this.colINV_IMEI.Caption = "INV_IMEI";
            this.colINV_IMEI.FieldName = "INV_IMEI";
            this.colINV_IMEI.Name = "colINV_IMEI";
            this.colINV_IMEI.OptionsColumn.AllowEdit = false;
            this.colINV_IMEI.Visible = true;
            this.colINV_IMEI.Width = 130;
            // 
            // colINV_PRODUCTSTATUSNAME
            // 
            this.colINV_PRODUCTSTATUSNAME.Caption = "INV_PRODUCTSTATUSNAME";
            this.colINV_PRODUCTSTATUSNAME.FieldName = "INV_PRODUCTSTATUSNAME";
            this.colINV_PRODUCTSTATUSNAME.Name = "colINV_PRODUCTSTATUSNAME";
            this.colINV_PRODUCTSTATUSNAME.OptionsColumn.AllowEdit = false;
            this.colINV_PRODUCTSTATUSNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colINV_PRODUCTSTATUSNAME.Visible = true;
            this.colINV_PRODUCTSTATUSNAME.Width = 130;
            // 
            // colINV_QUANTITY
            // 
            this.colINV_QUANTITY.Caption = "INV_QUANTITY";
            this.colINV_QUANTITY.ColumnEdit = this.reQuantityShow;
            this.colINV_QUANTITY.FieldName = "INV_QUANTITY";
            this.colINV_QUANTITY.Name = "colINV_QUANTITY";
            this.colINV_QUANTITY.OptionsColumn.AllowEdit = false;
            this.colINV_QUANTITY.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colINV_QUANTITY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "INV_QUANTITY", "{0:N0}")});
            this.colINV_QUANTITY.Visible = true;
            this.colINV_QUANTITY.Width = 70;
            // 
            // colDIFFERENT
            // 
            this.colDIFFERENT.Caption = "DIFFERENT";
            this.colDIFFERENT.ColumnEdit = this.reQuantityShow;
            this.colDIFFERENT.FieldName = "DIFFERENT";
            this.colDIFFERENT.Name = "colDIFFERENT";
            this.colDIFFERENT.OptionsColumn.AllowEdit = false;
            this.colDIFFERENT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDIFFERENT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DIFFERENT", "{0:N0}")});
            this.colDIFFERENT.Visible = true;
            this.colDIFFERENT.Width = 80;
            // 
            // colQUANTITY_INPUT
            // 
            this.colQUANTITY_INPUT.Caption = "QUANTITY_INPUT";
            this.colQUANTITY_INPUT.ColumnEdit = this.reQuantityShow;
            this.colQUANTITY_INPUT.FieldName = "QUANTITY_INPUT";
            this.colQUANTITY_INPUT.Name = "colQUANTITY_INPUT";
            this.colQUANTITY_INPUT.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_INPUT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_INPUT", "{0:N0}")});
            this.colQUANTITY_INPUT.Visible = true;
            // 
            // colQUANTITY_SALE
            // 
            this.colQUANTITY_SALE.Caption = "QUANTITY_SALE";
            this.colQUANTITY_SALE.ColumnEdit = this.reQuantityShow;
            this.colQUANTITY_SALE.FieldName = "QUANTITY_SALE";
            this.colQUANTITY_SALE.Name = "colQUANTITY_SALE";
            this.colQUANTITY_SALE.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_SALE.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_SALE", "{0:N0}")});
            this.colQUANTITY_SALE.Visible = true;
            // 
            // colQUANTITY_STORECHANGE
            // 
            this.colQUANTITY_STORECHANGE.Caption = "QUANTITY_STORECHANGE";
            this.colQUANTITY_STORECHANGE.ColumnEdit = this.reQuantityShow;
            this.colQUANTITY_STORECHANGE.FieldName = "QUANTITY_STORECHANGE";
            this.colQUANTITY_STORECHANGE.Name = "colQUANTITY_STORECHANGE";
            this.colQUANTITY_STORECHANGE.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_STORECHANGE.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_STORECHANGE", "{0:N0}")});
            this.colQUANTITY_STORECHANGE.Visible = true;
            this.colQUANTITY_STORECHANGE.Width = 120;
            // 
            // colQUANTITY_OUTPUT
            // 
            this.colQUANTITY_OUTPUT.Caption = "QUANTITY_OUTPUT";
            this.colQUANTITY_OUTPUT.ColumnEdit = this.reQuantityShow;
            this.colQUANTITY_OUTPUT.FieldName = "QUANTITY_OUTPUT";
            this.colQUANTITY_OUTPUT.Name = "colQUANTITY_OUTPUT";
            this.colQUANTITY_OUTPUT.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_OUTPUT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_OUTPUT", "{0:N0}")});
            this.colQUANTITY_OUTPUT.Visible = true;
            // 
            // colQUANTITY_DIFFERENT
            // 
            this.colQUANTITY_DIFFERENT.Caption = "QUANTITY_DIFFERENT";
            this.colQUANTITY_DIFFERENT.ColumnEdit = this.reQuantityShow;
            this.colQUANTITY_DIFFERENT.FieldName = "QUANTITY_DIFFERENT";
            this.colQUANTITY_DIFFERENT.Name = "colQUANTITY_DIFFERENT";
            this.colQUANTITY_DIFFERENT.OptionsColumn.AllowEdit = false;
            this.colQUANTITY_DIFFERENT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY_DIFFERENT", "{0:N0}")});
            this.colQUANTITY_DIFFERENT.Visible = true;
            this.colQUANTITY_DIFFERENT.Width = 80;
            // 
            // colCAUSEGROUPSLIST
            // 
            this.colCAUSEGROUPSLIST.Caption = "CAUSEGROUPSLIST";
            this.colCAUSEGROUPSLIST.ColumnEdit = this.repCauseGroups;
            this.colCAUSEGROUPSLIST.FieldName = "CAUSEGROUPSLIST";
            this.colCAUSEGROUPSLIST.Name = "colCAUSEGROUPSLIST";
            this.colCAUSEGROUPSLIST.Visible = true;
            this.colCAUSEGROUPSLIST.Width = 220;
            // 
            // repCauseGroups
            // 
            this.repCauseGroups.AutoHeight = false;
            this.repCauseGroups.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repCauseGroups.DisplayMember = "CAUSEGROUPSNAME";
            this.repCauseGroups.Name = "repCauseGroups";
            this.repCauseGroups.ValueMember = "CAUSEGROUPSID";
            // 
            // colUNEVENTEXPLAIN
            // 
            this.colUNEVENTEXPLAIN.Caption = "UNEVENTEXPLAIN";
            this.colUNEVENTEXPLAIN.FieldName = "UNEVENTEXPLAIN";
            this.colUNEVENTEXPLAIN.Name = "colUNEVENTEXPLAIN";
            this.colUNEVENTEXPLAIN.OptionsColumn.AllowEdit = false;
            this.colUNEVENTEXPLAIN.Visible = true;
            this.colUNEVENTEXPLAIN.Width = 220;
            // 
            // repTextImei
            // 
            this.repTextImei.AutoHeight = false;
            this.repTextImei.MaxLength = 50;
            this.repTextImei.Name = "repTextImei";
            // 
            // repQuantity
            // 
            this.repQuantity.AutoHeight = false;
            this.repQuantity.DisplayFormat.FormatString = "N0";
            this.repQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repQuantity.Mask.EditMask = "N0";
            this.repQuantity.Mask.UseMaskAsDisplayFormat = true;
            this.repQuantity.MaxValue = new decimal(new int[] {
            1215752191,
            23,
            0,
            0});
            this.repQuantity.Name = "repQuantity";
            // 
            // repNote
            // 
            this.repNote.AutoHeight = false;
            this.repNote.MaxLength = 2000;
            this.repNote.Name = "repNote";
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "Thông tin sản phẩm";
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand3});
            this.gridBand1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 340;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "Mã sản phẩm";
            this.gridBand2.Columns.Add(this.colPRODUCTID);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.OptionsBand.AllowMove = false;
            this.gridBand2.OptionsBand.FixedWidth = true;
            this.gridBand2.Width = 120;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "Tên sản phẩm";
            this.gridBand3.Columns.Add(this.colPRODUCTNAME);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 220;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "ERP";
            this.gridBand4.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand5,
            this.gridBand6,
            this.gridBand7});
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Width = 330;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "IMEI";
            this.gridBand5.Columns.Add(this.colERP_IMEI);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.Width = 130;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "Trạng thái";
            this.gridBand6.Columns.Add(this.colERP_PRODUCTSTATUSNAME);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.Width = 130;
            // 
            // gridBand7
            // 
            this.gridBand7.Caption = "Tồn kho";
            this.gridBand7.Columns.Add(this.colERP_QUANTITY);
            this.gridBand7.Name = "gridBand7";
            // 
            // gridBand8
            // 
            this.gridBand8.Caption = "Kiểm kê";
            this.gridBand8.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand11,
            this.gridBand9,
            this.gridBand10});
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.Width = 330;
            // 
            // gridBand11
            // 
            this.gridBand11.Caption = "IMEI";
            this.gridBand11.Columns.Add(this.colINV_IMEI);
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.Width = 130;
            // 
            // gridBand9
            // 
            this.gridBand9.Caption = "Trạng thái";
            this.gridBand9.Columns.Add(this.colINV_PRODUCTSTATUSNAME);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.Width = 130;
            // 
            // gridBand10
            // 
            this.gridBand10.Caption = "Tồn kho";
            this.gridBand10.Columns.Add(this.colINV_QUANTITY);
            this.gridBand10.Name = "gridBand10";
            // 
            // gridBand16
            // 
            this.gridBand16.Caption = "Số lượng chênh lệch";
            this.gridBand16.Columns.Add(this.colDIFFERENT);
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.Width = 80;
            // 
            // gridBand12
            // 
            this.gridBand12.Caption = "Giải trình chênh lệch hệ thống";
            this.gridBand12.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand13,
            this.gridBand14,
            this.gridBand17,
            this.gridBand15});
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.Width = 345;
            // 
            // gridBand13
            // 
            this.gridBand13.Caption = "Nhập";
            this.gridBand13.Columns.Add(this.colQUANTITY_INPUT);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.Width = 75;
            // 
            // gridBand14
            // 
            this.gridBand14.Caption = "Xuất bán";
            this.gridBand14.Columns.Add(this.colQUANTITY_SALE);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.Width = 75;
            // 
            // gridBand17
            // 
            this.gridBand17.Caption = "Xuất chuyển kho";
            this.gridBand17.Columns.Add(this.colQUANTITY_STORECHANGE);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.Width = 120;
            // 
            // gridBand15
            // 
            this.gridBand15.Caption = "Xuất khác";
            this.gridBand15.Columns.Add(this.colQUANTITY_OUTPUT);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.Width = 75;
            // 
            // gridBand18
            // 
            this.gridBand18.Caption = "Chênh lệch giải trình";
            this.gridBand18.Columns.Add(this.colQUANTITY_DIFFERENT);
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.Width = 80;
            // 
            // gridBand20
            // 
            this.gridBand20.Caption = "Nhóm nguyên nhân lỗi";
            this.gridBand20.Columns.Add(this.colCAUSEGROUPSLIST);
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.Visible = false;
            this.gridBand20.Width = 220;
            // 
            // gridBand19
            // 
            this.gridBand19.Caption = "Giải trình ";
            this.gridBand19.Columns.Add(this.colUNEVENTEXPLAIN);
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.Width = 220;
            // 
            // frmShowUnEventVTS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1207, 653);
            this.Controls.Add(this.grdCtlProducts);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmShowUnEventVTS";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kết quả chênh lệch kiểm kê";
            this.Load += new System.EventHandler(this.frmShowUnEventVTS_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdCtlProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reQuantityShow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCauseGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextImei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkOnlyUnEvent;
        private System.Windows.Forms.Button btnViewReport;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcel;
        private DevExpress.XtraGrid.GridControl grdCtlProducts;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView grdViewProducts;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPRODUCTID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPRODUCTNAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colERP_IMEI;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colERP_PRODUCTSTATUSNAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colERP_QUANTITY;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit reQuantityShow;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colINV_PRODUCTSTATUSNAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colINV_QUANTITY;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDIFFERENT;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repQuantity;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repNote;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colINV_IMEI;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTextImei;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_INPUT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_SALE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_STORECHANGE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_OUTPUT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colQUANTITY_DIFFERENT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colUNEVENTEXPLAIN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCAUSEGROUPSLIST;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repCauseGroups;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
    }
}