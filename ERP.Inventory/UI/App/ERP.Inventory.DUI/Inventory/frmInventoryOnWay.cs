﻿using ERP.Inventory.PLC.Inventory;
using ERP.Inventory.PLC.WSInventoryOnWay;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmInventoryOnWay : Form
    {
        private string strPermission_Update = "INVENTORYONWAY_UPDATE";
        private List<INV_InventoryOnWay> lstINV_InventoryOnWay = null;
        private int isInputStore ;
        public frmInventoryOnWay()
        {
            InitializeComponent();
            
        }

        private void frmInventoryOnWay_Load(object sender, EventArgs e)
        {
            btnAutoReview.ToolTip = "Giải trình hàng đi đường đã thực nhập với các phiếu chuyển đi " + Environment.NewLine +
                " trước ngày kiểm kê và thực nhập tới ngày kiểm kê hàng đi đường";

            if (this.Tag != null && this.Tag.ToString().Trim() != string.Empty)
            {
                try
                {
                    Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                    isInputStore = Convert.ToInt32(hstbParam["ISINPUTSTORE"]);
                }
                catch { }
            }
            LoadCombo();
            FormatGrid();
            btnConfirm.Enabled = btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Update);
        }

        private void LoadCombo()
        {
            cboBranch.InitControl(false);
            cboStoreIDList.InitControl(true);
            cboIsReviewed.SelectedIndex = 0;
        }

        private bool FormatGrid()
        {
            try
            {
                string[] fieldNames = new string[] { "IsSelected", "StoreChangeOrderID", "OutputVoucherID", "FromStoreName", "InputVoucherID", "ToStoreName", "OutputDate", "Quantity", "TotalAmount", "OutputUserFullName", "IsReviewed" };
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(gridData, true, false, true, true, fieldNames);
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(gridViewData);
                gridViewData.Columns["IsSelected"].Caption = "Chọn";
                gridViewData.Columns["StoreChangeOrderID"].Caption = "Mã yêu cầu";
                gridViewData.Columns["OutputVoucherID"].Caption = "Mã phiếu xuất";
                gridViewData.Columns["FromStoreName"].Caption = "Kho xuất";
                gridViewData.Columns["InputVoucherID"].Caption = "Mã phiếu nhập";
                gridViewData.Columns["ToStoreName"].Caption = "Kho nhập";
                gridViewData.Columns["OutputDate"].Caption = "Ngày xuất";
                gridViewData.Columns["Quantity"].Caption = "Tổng số lượng";
                gridViewData.Columns["TotalAmount"].Caption = "Tổng giá trị";
                gridViewData.Columns["OutputUserFullName"].Caption = "Nhân viên xuất";
                gridViewData.Columns["IsReviewed"].Caption = "Xác nhận dữ liệu";

                gridViewData.OptionsView.ColumnAutoWidth = false;
                gridViewData.Columns["IsSelected"].Width = 80;
                gridViewData.Columns["StoreChangeOrderID"].Width = 120;
                gridViewData.Columns["OutputVoucherID"].Width = 120;
                gridViewData.Columns["FromStoreName"].Width = 200;
                gridViewData.Columns["InputVoucherID"].Width = 120;
                gridViewData.Columns["ToStoreName"].Width = 200;
                gridViewData.Columns["OutputDate"].Width = 100;
                gridViewData.Columns["Quantity"].Width = 120;
                gridViewData.Columns["TotalAmount"].Width = 120;
                gridViewData.Columns["OutputUserFullName"].Width = 120;
                gridViewData.Columns["IsReviewed"].Width = 120;

                gridViewData.Columns["Quantity"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                gridViewData.Columns["Quantity"].DisplayFormat.FormatString = "#,###.##";
                gridViewData.Columns["TotalAmount"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                gridViewData.Columns["TotalAmount"].DisplayFormat.FormatString = "#,###.##";
                gridViewData.Columns["OutputDate"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                gridViewData.Columns["OutputDate"].DisplayFormat.FormatString = "dd/MM/yyyy";

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsSelect.ValueChecked = ((bool)(true));
                chkCheckBoxIsSelect.ValueUnchecked = ((bool)(false));
                gridViewData.Columns["IsSelected"].ColumnEdit = chkCheckBoxIsSelect;
                gridViewData.Columns["IsSelected"].OptionsColumn.AllowEdit = true;

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsReviewed = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsReviewed.ValueChecked = ((bool)(true));
                chkCheckBoxIsReviewed.ValueUnchecked = ((bool)(false));
                gridViewData.Columns["IsReviewed"].ColumnEdit = chkCheckBoxIsReviewed;
                gridViewData.Columns["IsReviewed"].OptionsColumn.AllowEdit = false;

                gridViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                gridViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                gridViewData.OptionsView.ShowAutoFilterRow = true;
                gridViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                gridViewData.OptionsView.ShowFooter = false;
                gridViewData.OptionsView.ShowGroupPanel = false;
                gridViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                gridViewData.ColumnPanelRowHeight = 40;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới danh sách phiếu chuyển kho chưa được nhận.");
                SystemErrorWS.Insert("Lỗi định dạng lưới danh sách phiếu chuyển kho chưa được nhận.", objExce.ToString(), this.Name + " -> FormatGrid", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void cboBranch_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            if (cboBranch.BranchID > 0)
                objStoreFilter.OtherCondition = "BRANCHID=" + cboBranch.BranchID;

            cboStoreIDList.InitControl(true, objStoreFilter);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            object[] objKeyWords = new object[]{
                                                "@ENDDATE", dtpEndDate.Value,
                                                "@BRANCHID", cboBranch.BranchID,
                                                "@StoreIDList", cboStoreIDList.StoreIDList,
                                                "@IsReviewed", cboIsReviewed.SelectedIndex - 1,
                                                "@ISINPUTSTORE",isInputStore
                                               };

            lstINV_InventoryOnWay = new List<INV_InventoryOnWay>();
            ResultMessage objResultMessage = new PLCInventoryOnWay().InventoryOnWaySearch(ref lstINV_InventoryOnWay, objKeyWords);
            if(objResultMessage.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                return;
            }
            if (lstINV_InventoryOnWay != null && lstINV_InventoryOnWay.Count > 0) btnAutoReview.Enabled = true;
            else btnAutoReview.Enabled = false;
            gridData.DataSource = lstINV_InventoryOnWay;
            gridData.RefreshDataSource();
        }

        private void LoadDataDetail()
        {
            if (gridViewData.FocusedRowHandle < 0)
                return;

            INV_InventoryOnWay objINV_InventoryOnWay = gridViewData.GetFocusedRow() as INV_InventoryOnWay;
            if (objINV_InventoryOnWay == null)
                return;

            if(objINV_InventoryOnWay.InventoryOnWayDetailList == null)
            {
                object[] objKeyWords = new object[]{
                                                "@StoreChangeOrderID", objINV_InventoryOnWay.StoreChangeOrderID,
                                                "@OUTPUTVOUCHERID", objINV_InventoryOnWay.OutputVoucherID,
                                                "@INPUTVOUCHERID", objINV_InventoryOnWay.InputVoucherID,
                                                "@ISINPUTSTORE", isInputStore
                                               };

                List<INV_InventoryOnWayDetail> lstINV_InventoryOnWayDetail = new List<INV_InventoryOnWayDetail>();
                List<INV_InventoryOnWay_AttachMent> lstINV_InventoryOnWay_AttachMent = new List<INV_InventoryOnWay_AttachMent>();
                ResultMessage objResultMessage = new PLCInventoryOnWay().SearchDataDetail(ref lstINV_InventoryOnWayDetail, ref lstINV_InventoryOnWay_AttachMent, objKeyWords);
                if (objResultMessage.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                    return;
                }

                objINV_InventoryOnWay.InventoryOnWayDetailList = lstINV_InventoryOnWayDetail.ToArray();
                objINV_InventoryOnWay.InventoryOnWay_AttachMentList = lstINV_InventoryOnWay_AttachMent.ToArray();
            }

            frmInventoryOnWayDetail frm = new frmInventoryOnWayDetail();
            frm.INV_InventoryOnWay = objINV_InventoryOnWay;
            frm.ShowDialog();
        }

        private void mnuItemViewDetail_Click(object sender, EventArgs e)
        {
            LoadDataDetail();
        }

        private void gridData_DoubleClick(object sender, EventArgs e)
        {
            LoadDataDetail();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateData(false);
        }        

        private bool UpdateData(bool bolIsConfirm)
        {
            List<INV_InventoryOnWay> lstINV_InventoryOnWaySelected = new List<INV_InventoryOnWay>();
            List<INV_InventoryOnWay> lstINV_InventoryOnWay = gridData.DataSource as List<INV_InventoryOnWay>;
            if(lstINV_InventoryOnWay != null)
            {
                foreach(INV_InventoryOnWay objINV_InventoryOnWay in lstINV_InventoryOnWay)
                {
                    objINV_InventoryOnWay.IsInputStore = isInputStore ==1;
                    if (objINV_InventoryOnWay.IsSelected)
                        lstINV_InventoryOnWaySelected.Add(objINV_InventoryOnWay);
                }
            }

            if(lstINV_InventoryOnWaySelected.Count == 0)
            {
                MessageBox.Show("Vui lòng chọn ít nhất 1 dòng để cập nhật.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (!UploadAttachment(lstINV_InventoryOnWaySelected))
            {
                if (MessageBox.Show(this, "Upload tập tin thất bại\r\nBạn có muốn tiếp tục lưu thông tin không?", "Thông báo",
                     MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                {
                    return false;
                }
            }

            ResultMessage objResultMessage = new PLCInventoryOnWay().InsertAndUpdate(lstINV_InventoryOnWaySelected, bolIsConfirm);
            if (objResultMessage.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                return false;
            }

            string strMessage = string.Empty;
            if (bolIsConfirm)
                strMessage = "Xác nhận dữ liệu";
            else
                strMessage = "Cập nhật";

            MessageBox.Show(strMessage + " thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

            btnSearch_Click(null, null);

            return true;
        }

        private bool UploadAttachment(List<INV_InventoryOnWay> lstINV_InventoryOnWaySelected)
        {
            try
            {
                foreach (INV_InventoryOnWay objINV_InventoryOnWay in lstINV_InventoryOnWaySelected)
                {
                    if (objINV_InventoryOnWay.InventoryOnWay_AttachMentList != null)
                    {
                        foreach (INV_InventoryOnWay_AttachMent objINV_InventoryOnWay_AttachMent in objINV_InventoryOnWay.InventoryOnWay_AttachMentList)
                        {
                            if (!string.IsNullOrEmpty(objINV_InventoryOnWay_AttachMent.INVENToRYONWAYAttachMENTID))
                            {
                                string strFilePath = string.Empty;
                                string strFileID = string.Empty;
                                string strResult = ERP.FMS.DUI.DUIFileManager.UploadFile(this, "FMSAplication_ProERP_InputVoucherAttachment", objINV_InventoryOnWay_AttachMent.AttachMENTPath, ref strFileID, ref strFilePath);
                                objINV_InventoryOnWay_AttachMent.AttachMENTPath = strFilePath;
                                objINV_InventoryOnWay_AttachMent.FileID = strFileID;
                                if (!string.IsNullOrEmpty(strResult))
                                {
                                    MessageBoxObject.ShowWarningMessage(this, strResult);
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi đính kèm tập tin ", objEx, DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            UpdateData(true);
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(gridData);
        }

        private void SelectAll(bool IsSelectAll, DevExpress.XtraGrid.GridControl grid)
        {
            List<INV_InventoryOnWay> tem = (List<INV_InventoryOnWay>)grid.DataSource;
            if (tem == null || tem.Count == 0) return;
            
            foreach (INV_InventoryOnWay row in tem)
            {
                //if (row. == DataRowState.Deleted) continue;
                row.IsSelected = IsSelectAll;
            }
            gridData.DataSource = tem;
            gridData.RefreshDataSource();
            gridViewData.RefreshData();
        }

        private void submnuSelectALl_Click(object sender, EventArgs e)
        {
            SelectAll(true, gridData);
        }

        private void submnuUnSelectAll_Click(object sender, EventArgs e)
        {
            SelectAll(false, gridData);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            List<INV_InventoryOnWay> tem = (List<INV_InventoryOnWay>)gridData.DataSource;
            if (tem == null || tem.Count < 1) return;

            string lstInputVoucherIDs = "";
            foreach(INV_InventoryOnWay it in tem)
            {
                if (it.IsReviewed) continue;
                lstInputVoucherIDs += it.InputVoucherID.ToString().Trim()+",";
            }
            lstInputVoucherIDs = lstInputVoucherIDs.TrimEnd(',');
            string user = SystemConfig.objSessionUser.UserName;
            DateTime dt = dtpEndDate.Value;

            ResultMessage objResultMessage = new PLCInventoryOnWay().AutoListInputVoucher(lstInputVoucherIDs, isInputStore, user, dt);
            if (objResultMessage.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                return;
            }
            btnSearch_Click(null, null);
            MessageBox.Show("Tự động giải trình thực nhập thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
