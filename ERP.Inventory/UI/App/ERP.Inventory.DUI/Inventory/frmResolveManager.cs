﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmResolveManager : Form
    {
        private ERP.Inventory.PLC.Inventory.PLCInventoryTerm objPLCInventoryTerm = new PLC.Inventory.PLCInventoryTerm();
        private ERP.Inventory.PLC.Inventory.PLCInventoryProcess objPLCInventoryProcess = new PLC.Inventory.PLCInventoryProcess();
        private DataTable dtbData = null;
        private bool bolIsLockByClosingDataMonth = false;

        public frmResolveManager()
        {
            InitializeComponent();
        }

        private void frmResolveManager_Load(object sender, EventArgs e)
        {
            LoadComboBox();
            InitFlexResolveProcess();
            flexResolveProcess.DataSource = dtbData;
            CustomFlex();
        }

        private void LoadComboBox()
        {
            DateTime dtmServerDate = Library.AppCore.Globals.GetServerDateTime();
            dtFromDate.Value = dtmServerDate.AddDays(-10);
            dtToDate.Value = dtmServerDate.AddDays(1);
            try
            {
                Library.AppCore.LoadControls.SetDataSource.SetInventoryType(this, cboInventoryType);
                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                objStoreFilter.IsCheckPermission = true;
                objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.VIEWREPORT;
                cboStoreIDList.InitControl(true, objStoreFilter);
                cboMainGroupID.InitControl(true, true);
                ucManCreate.UserName = SystemConfig.objSessionUser.UserName;
                cboInventoryTermID.Caption = "-- Chọn kỳ kiểm kê --";
                cboInventoryType_SelectionChangeCommitted(null, null);
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi nạp ComboBox", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi nạp ComboBox", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            cboInventoryType.SelectedIndex = 0;
            cboReview.SelectedIndex = cboReview.Items.Count - 1;
            cboProcess.SelectedIndex = cboProcess.Items.Count - 1;
        }

        private void InitFlexResolveProcess()
        {
            dtbData = new DataTable();
            dtbData.Columns.Add("InventoryProcessID", typeof(string));
            dtbData.Columns.Add("InventoryID", typeof(string));
            dtbData.Columns.Add("StoreID", typeof(int));
            dtbData.Columns.Add("StoreName", typeof(string));
            dtbData.Columns.Add("InventoryTermID", typeof(int));
            dtbData.Columns.Add("InventoryTermName", typeof(string));
            dtbData.Columns.Add("InventoryDate", typeof(DateTime));
            dtbData.Columns.Add("CreatedDate", typeof(DateTime));
            dtbData.Columns.Add("MainGroupID", typeof(int));
            dtbData.Columns.Add("MainGroupName", typeof(string));
            dtbData.Columns.Add("IsNew", typeof(bool));
            dtbData.Columns.Add("CreatedUser", typeof(string));
            dtbData.Columns.Add("ReviewedUser", typeof(string));
            dtbData.Columns.Add("ReviewedDate", typeof(DateTime));
            dtbData.Columns.Add("ProcessUser", typeof(string));
            dtbData.Columns.Add("ProcessDate", typeof(DateTime));
            dtbData.Columns.Add("DeleteUser", typeof(string));
            dtbData.Columns.Add("DeletedDate", typeof(DateTime));
            dtbData.Columns.Add("DeletedReason", typeof(string));
            dtbData.Columns.Add("IsProcess", typeof(bool));
        }

        private void CustomFlex()
        {
            flexResolveProcess.Rows.Fixed = 1;
            flexResolveProcess.Cols[0].Visible = false;
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexResolveProcess, false, "StoreID,InventoryTermID,MainGroupID");

            flexResolveProcess.Cols["InventoryProcessID"].Caption = "Mã yêu cầu xử lý kiểm kê";
            flexResolveProcess.Cols["InventoryID"].Caption = "Mã phiếu kiểm kê";
            flexResolveProcess.Cols["StoreName"].Caption = "Kho kiểm kê";
            flexResolveProcess.Cols["InventoryTermName"].Caption = "Kỳ kiểm kê";
            flexResolveProcess.Cols["InventoryDate"].Caption = "Ngày kiểm kê";
            flexResolveProcess.Cols["CreatedDate"].Caption = "Ngày tạo";
            flexResolveProcess.Cols["MainGroupName"].Caption = "Ngành hàng kiểm kê";
            flexResolveProcess.Cols["IsNew"].Caption = "Mới";
            flexResolveProcess.Cols["CreatedUser"].Caption = "Nhân viên tạo";
            flexResolveProcess.Cols["ReviewedUser"].Caption = "Nhân viên duyệt";
            flexResolveProcess.Cols["ReviewedDate"].Caption = "Ngày duyệt";
            flexResolveProcess.Cols["ProcessUser"].Caption = "Nhân viên xử lý";
            flexResolveProcess.Cols["ProcessDate"].Caption = "Ngày xử lý";
            flexResolveProcess.Cols["DeleteUser"].Caption = "Nhân viên hủy";
            flexResolveProcess.Cols["DeletedDate"].Caption = "Ngày hủy";
            flexResolveProcess.Cols["DeletedReason"].Caption = "Lý do hủy";

            flexResolveProcess.Cols["IsNew"].DataType = typeof(bool);
            flexResolveProcess.Cols["IsProcess"].DataType = typeof(bool);

            flexResolveProcess.Cols["IsProcess"].Visible = false;

            flexResolveProcess.Cols["InventoryDate"].Format = "dd/MM/yyyy HH:mm";
            flexResolveProcess.Cols["CreatedDate"].Format = "dd/MM/yyyy HH:mm";
            flexResolveProcess.Cols["ReviewedDate"].Format = "dd/MM/yyyy HH:mm";
            flexResolveProcess.Cols["ProcessDate"].Format = "dd/MM/yyyy HH:mm";
            flexResolveProcess.Cols["DeletedDate"].Format = "dd/MM/yyyy HH:mm";

            flexResolveProcess.Cols["InventoryProcessID"].Width = 130;
            flexResolveProcess.Cols["InventoryID"].Width = 130;
            flexResolveProcess.Cols["StoreName"].Width = 100;
            flexResolveProcess.Cols["InventoryTermName"].Width = 100;
            flexResolveProcess.Cols["InventoryDate"].Width = 110;
            flexResolveProcess.Cols["CreatedDate"].Width = 110;
            flexResolveProcess.Cols["MainGroupName"].Width = 80;
            flexResolveProcess.Cols["IsNew"].Width = 40;
            flexResolveProcess.Cols["CreatedUser"].Width = 100;
            flexResolveProcess.Cols["ReviewedUser"].Width = 100;
            flexResolveProcess.Cols["ReviewedDate"].Width = 100;
            flexResolveProcess.Cols["ProcessUser"].Width = 100;
            flexResolveProcess.Cols["ProcessDate"].Width = 100;
            flexResolveProcess.Cols["DeleteUser"].Width = 100;
            flexResolveProcess.Cols["DeletedDate"].Width = 100;
            flexResolveProcess.Cols["DeletedReason"].Width = 100;

            for (int i = 1; i < flexResolveProcess.Cols.Count; i++)
            {
                flexResolveProcess.Cols[i].AllowEditing = false;
            }

            C1.Win.C1FlexGrid.CellStyle style = flexResolveProcess.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = flexResolveProcess.GetCellRange(0, 0, 0, flexResolveProcess.Cols.Count - 1);
            range.Style = style;
            flexResolveProcess.Rows[0].Height = 50;

            if (chkDeleted.Checked)
            {
                flexResolveProcess.Cols["DeleteUser"].Visible = true;
                flexResolveProcess.Cols["DeletedDate"].Visible = true;
                flexResolveProcess.Cols["DeletedReason"].Visible = true;
            }
            else
            {
                flexResolveProcess.Cols["DeleteUser"].Visible = false;
                flexResolveProcess.Cols["DeletedDate"].Visible = false;
                flexResolveProcess.Cols["DeletedReason"].Visible = false;
            }
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            if (dtFromDate.Value.Date.AddDays(31).Date < dtToDate.Value.Date)
            {
                MessageBox.Show(this, "Khoảng thời gian tìm kiếm cho phép trong vòng 31 ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (dtFromDate.Value.Date > dtToDate.Value.Date)
            {
                MessageBox.Show(this, "Vui lòng kiểm tra lại từ ngày đến ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int intInventorytype = -1;
            if (cboInventoryType.SelectedIndex > 0)
            {
                if (cboInventoryType.SelectedIndex == 1)
                {
                    intInventorytype = 0;
                }
                else
                {
                    intInventorytype = 1;
                }
            }
            int intIsReviewed = -1;
            if (cboReview.SelectedIndex > 0)
            {
                if (cboReview.SelectedIndex == 1)
                {
                    intIsReviewed = 1;
                }
                else
                {
                    intIsReviewed = 0;
                }
            }
            int intIsProcess = -1;
            if (cboProcess.SelectedIndex > 0)
            {
                if (cboProcess.SelectedIndex == 1)
                {
                    intIsProcess = 1;
                }
                else
                {
                    intIsProcess = 0;
                }
            }
            int intCheck = -1;
            if (chkInventoryProcessID.Checked && chkInventoryID.Checked)
            {
                intCheck = -1;
            }
            else if ((!chkInventoryProcessID.Checked) && (!chkInventoryID.Checked))
            {
                intCheck = -1;
            }
            else
            {
                if (chkInventoryProcessID.Checked)
                {
                    intCheck = 0;
                }
                else
                {
                    if (chkInventoryID.Checked)
                    {
                        intCheck = 1;
                    }
                }
            }
            object[] objKeywords = new object[]{
                "@StoreIDList", Convert.ToString(cboStoreIDList.StoreIDList).Trim().Replace("><", ",").Replace("<", string.Empty).Replace(">", string.Empty),
                "@MainGroupIDList", cboMainGroupID.MainGroupIDList.Replace("><", ",").Replace("<", string.Empty).Replace(">", string.Empty),
                "@Inventorytype", intInventorytype,
                "@InventoryTermID", cboInventoryTermID.intID,
                "@FromDate", dtFromDate.Value,
                "@ToDate", dtToDate.Value,
                "@IsReviewed", intIsReviewed,
                "@IsProcess", intIsProcess,
                "@IsDeleted", chkDeleted.Checked,
                "@CreatedUser", ucManCreate.UserName,
                "@Keyword", txtKeywords.Text.Trim(),
                "@check", intCheck
            };
            dtbData = objPLCInventoryProcess.SearchData(objKeywords);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            flexResolveProcess.DataSource = dtbData;
            CustomFlex();
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboInventoryType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboInventoryType.SelectedIndex > 0)
                cboInventoryTermID.SetCombo(objPLCInventoryTerm.SearchData(new object[] { "@Keywords", string.Empty, "@InventoryTypeID", Convert.ToInt32(cboInventoryType.SelectedValue), "@FromDate", dtFromDate.Value, "@ToDate", dtToDate.Value }));
            else
                cboInventoryTermID.SetCombo(objPLCInventoryTerm.SearchData(new object[] { "@Keywords", string.Empty, "@FromDate", dtFromDate.Value, "@ToDate", dtToDate.Value }));
        }

        private void flexResolveProcess_DoubleClick(object sender, EventArgs e)
        {
            if (flexResolveProcess.Rows.Count <= flexResolveProcess.Rows.Fixed)
            {
                return;
            }

            string strInventoryProcessID = Convert.ToString(flexResolveProcess[flexResolveProcess.RowSel, "InventoryProcessID"]);

            frmResolveQuantity frmResolveQuantity1 = new frmResolveQuantity();
            frmResolveQuantity1.InventoryProcessID = strInventoryProcessID;

            bolIsLockByClosingDataMonth = false;
            try
            {
                bolIsLockByClosingDataMonth = ERP.MasterData.SYS.PLC.PLCClosingDataMonth.CheckLockClosingDataMonth(ERP.MasterData.SYS.PLC.PLCClosingDataMonth.ClosingDataType.INVENTORY,
                    Convert.ToDateTime(flexResolveProcess[flexResolveProcess.RowSel, "CreatedDate"]), Convert.ToInt32(flexResolveProcess[flexResolveProcess.RowSel, "StoreID"]));
            }
            catch { }
            if (bolIsLockByClosingDataMonth)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Tháng " + Convert.ToDateTime(flexResolveProcess[flexResolveProcess.RowSel, "CreatedDate"]).Month.ToString("00") + " đã khóa sổ. Bạn không thể chỉnh sửa");
            }

            frmResolveQuantity1.ShowDialog();
        }

        private void cmdExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexResolveProcess);
        }

        private void mnuPrintReport_Click(object sender, EventArgs e)
        {
            string strInventoryProcessID = Convert.ToString(flexResolveProcess[flexResolveProcess.RowSel, "InventoryProcessID"]);
            object[] objKeywords;
            objKeywords = new object[] { "@InventoryProcessID", strInventoryProcessID.Trim() };

            DataTable dtbGetArrearMoney = objPLCInventoryProcess.GetDataSourceByStoreName(objKeywords, "Inv_Rpt_DutyUsers_GetArrear");
            decimal decSumToTalCollectArrearMoney = 0;
            if (dtbGetArrearMoney != null && dtbGetArrearMoney.Rows.Count > 0)
            {
                decSumToTalCollectArrearMoney = Convert.ToDecimal(dtbGetArrearMoney.Compute("Sum(CollectArrearMoney)", "RelateVoucherID IS NOT NULL"));
            }
            DataTable dtbHandlingReport = objPLCInventoryProcess.GetDataSourceByStoreName(objKeywords, "Inv_Rpt_InventoryProcess");
            if (dtbHandlingReport != null && dtbHandlingReport.Rows.Count > 0)
            {
                for (int i = 0; i < dtbHandlingReport.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        dtbHandlingReport.Rows[i]["STT"] = 1;
                    }
                    else
                    {
                        DataRow rHandlingReport = dtbHandlingReport.Rows[i];
                        if (Convert.ToInt32(dtbHandlingReport.Rows[i - 1]["IsPrChange"]) == 1)
                        {
                            dtbHandlingReport.Rows[i]["STT"] = Convert.ToInt32(dtbHandlingReport.Rows[i - 1]["STT"]) + 2;
                        }
                        else
                        {
                            dtbHandlingReport.Rows[i]["STT"] = Convert.ToInt32(dtbHandlingReport.Rows[i - 1]["STT"]) + 1;
                        }
                    }

                    dtbHandlingReport.Rows[i]["SumToTalCollectArrearMoney"] = decSumToTalCollectArrearMoney;
                }
            }

            DataTable dtbDutyUsersReport = objPLCInventoryProcess.GetDataSourceByStoreName(objKeywords, "Inv_Rpt_DutyUsers");

            dtbHandlingReport.TableName = "Inv_Rpt_InventoryProcess";
            dtbDutyUsersReport.TableName = "Inv_Rpt_DutyUsers";
            DataSet dsReport = new DataSet();
            dsReport.Tables.Add(dtbHandlingReport);
            dsReport.Tables.Add(dtbDutyUsersReport);

            ERP.MasterData.PLC.MD.WSReport.Report objReport = new MasterData.PLC.MD.WSReport.Report();
            objReport.DataSetSource = dsReport;
            objReport.PaperOrientation = true;
            objReport.ReportPath = "Reports\\Inventory\\Inventory\\InventoryHandlingReport.rpt";
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport);
        }

        private void txtKeywords_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                cmdSearch_Click(null, null);
            }
        }

        private void dtFromDate_ValueChanged(object sender, EventArgs e)
        {
            cboInventoryType_SelectionChangeCommitted(null, null);
        }

        private void dtToDate_ValueChanged(object sender, EventArgs e)
        {
            cboInventoryType_SelectionChangeCommitted(null, null);
        }

    }
}