﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInventory));
            this.cboReviewStatus = new System.Windows.Forms.ComboBox();
            this.btnReviewStatus = new System.Windows.Forms.Button();
            this.lnkUsersReview = new System.Windows.Forms.LinkLabel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.cboItemUnEvent = new DevExpress.XtraEditors.DropDownButton();
            this.popupItemUnEvent = new DevExpress.XtraBars.PopupMenu(this.components);
            this.mnuItemViewUnEvent = new DevExpress.XtraBars.BarButtonItem();
            this.mnuItemExplainUnEvent = new DevExpress.XtraBars.BarButtonItem();
            this.mnuEventHandlingUnEvent = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.txtInventoryID = new System.Windows.Forms.TextBox();
            this.cboCabinet = new System.Windows.Forms.ComboBox();
            this.cboInventoryTerm = new System.Windows.Forms.ComboBox();
            this.cboMainGroup = new System.Windows.Forms.ComboBox();
            this.dtpBeginInventoryTime = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblReview = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.dtpInventoryDate = new System.Windows.Forms.DateTimePicker();
            this.cboStore = new System.Windows.Forms.ComboBox();
            this.flexDetail = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.mnuFlexDetail = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemImportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemExportTemplateExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemDeleteProduct = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cboProductStateID = new System.Windows.Forms.ComboBox();
            this.lblSum = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtInventoryUser = new System.Windows.Forms.TextBox();
            this.txtSubGroupName = new System.Windows.Forms.TextBox();
            this.lblSumQuantity = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.chkIsDeleted = new System.Windows.Forms.CheckBox();
            this.chkIsReviewed = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtContentDelete = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.flexUserReviewLevel = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grdUserReviewLevel = new DevExpress.XtraGrid.GridControl();
            this.grdViewUserReviewLevel = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.tmr = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.popupItemUnEvent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flexDetail)).BeginInit();
            this.mnuFlexDetail.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexUserReviewLevel)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdUserReviewLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewUserReviewLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // cboReviewStatus
            // 
            this.cboReviewStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cboReviewStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReviewStatus.Enabled = false;
            this.cboReviewStatus.FormattingEnabled = true;
            this.cboReviewStatus.Items.AddRange(new object[] {
            "Chưa duyệt",
            "Đang xử lý",
            "Đồng ý",
            "Từ chối"});
            this.cboReviewStatus.Location = new System.Drawing.Point(391, 588);
            this.cboReviewStatus.Name = "cboReviewStatus";
            this.cboReviewStatus.Size = new System.Drawing.Size(129, 24);
            this.cboReviewStatus.TabIndex = 66;
            // 
            // btnReviewStatus
            // 
            this.btnReviewStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnReviewStatus.Enabled = false;
            this.btnReviewStatus.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.btnReviewStatus.Location = new System.Drawing.Point(519, 588);
            this.btnReviewStatus.Name = "btnReviewStatus";
            this.btnReviewStatus.Size = new System.Drawing.Size(33, 24);
            this.btnReviewStatus.TabIndex = 65;
            this.btnReviewStatus.UseVisualStyleBackColor = true;
            this.btnReviewStatus.Click += new System.EventHandler(this.btnReviewStatus_Click);
            // 
            // lnkUsersReview
            // 
            this.lnkUsersReview.AutoSize = true;
            this.lnkUsersReview.Enabled = false;
            this.lnkUsersReview.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkUsersReview.Location = new System.Drawing.Point(300, 274);
            this.lnkUsersReview.Name = "lnkUsersReview";
            this.lnkUsersReview.Size = new System.Drawing.Size(144, 16);
            this.lnkUsersReview.TabIndex = 12;
            this.lnkUsersReview.TabStop = true;
            this.lnkUsersReview.Text = "Danh sách người duyệt";
            this.lnkUsersReview.Visible = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(959, 145);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(94, 25);
            this.btnDelete.TabIndex = 16;
            this.btnDelete.Text = "      Hủy";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // cboItemUnEvent
            // 
            this.cboItemUnEvent.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.cboItemUnEvent.Appearance.Options.UseFont = true;
            this.cboItemUnEvent.DropDownControl = this.popupItemUnEvent;
            this.cboItemUnEvent.Location = new System.Drawing.Point(557, 145);
            this.cboItemUnEvent.LookAndFeel.SkinName = "Caramel";
            this.cboItemUnEvent.Name = "cboItemUnEvent";
            this.cboItemUnEvent.Size = new System.Drawing.Size(87, 25);
            this.cboItemUnEvent.TabIndex = 17;
            this.cboItemUnEvent.Text = "Chênh lệch";
            // 
            // popupItemUnEvent
            // 
            this.popupItemUnEvent.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuItemViewUnEvent),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuItemExplainUnEvent),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuEventHandlingUnEvent)});
            this.popupItemUnEvent.Manager = this.barManager1;
            this.popupItemUnEvent.Name = "popupItemUnEvent";
            // 
            // mnuItemViewUnEvent
            // 
            this.mnuItemViewUnEvent.Caption = "Xem chênh lệch";
            this.mnuItemViewUnEvent.Id = 0;
            this.mnuItemViewUnEvent.Name = "mnuItemViewUnEvent";
            this.mnuItemViewUnEvent.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuItemViewUnEvent_ItemClick);
            // 
            // mnuItemExplainUnEvent
            // 
            this.mnuItemExplainUnEvent.Caption = "Giải trình chênh lệch";
            this.mnuItemExplainUnEvent.Enabled = false;
            this.mnuItemExplainUnEvent.Id = 1;
            this.mnuItemExplainUnEvent.Name = "mnuItemExplainUnEvent";
            this.mnuItemExplainUnEvent.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuItemExplainUnEvent_ItemClick);
            // 
            // mnuEventHandlingUnEvent
            // 
            this.mnuEventHandlingUnEvent.Caption = "Xử lý chênh lệch";
            this.mnuEventHandlingUnEvent.Enabled = false;
            this.mnuEventHandlingUnEvent.Id = 2;
            this.mnuEventHandlingUnEvent.Name = "mnuEventHandlingUnEvent";
            this.mnuEventHandlingUnEvent.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuEventHandlingUnEvent_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.mnuItemViewUnEvent,
            this.mnuItemExplainUnEvent,
            this.mnuEventHandlingUnEvent});
            this.barManager1.MaxItemId = 3;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1067, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 653);
            this.barDockControlBottom.Size = new System.Drawing.Size(1067, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 653);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1067, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 653);
            // 
            // txtInventoryID
            // 
            this.txtInventoryID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInventoryID.Location = new System.Drawing.Point(108, 21);
            this.txtInventoryID.Name = "txtInventoryID";
            this.txtInventoryID.ReadOnly = true;
            this.txtInventoryID.Size = new System.Drawing.Size(223, 22);
            this.txtInventoryID.TabIndex = 0;
            // 
            // cboCabinet
            // 
            this.cboCabinet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCabinet.DropDownWidth = 250;
            this.cboCabinet.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.cboCabinet.Location = new System.Drawing.Point(502, 146);
            this.cboCabinet.Name = "cboCabinet";
            this.cboCabinet.Size = new System.Drawing.Size(49, 24);
            this.cboCabinet.TabIndex = 15;
            // 
            // cboInventoryTerm
            // 
            this.cboInventoryTerm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInventoryTerm.DropDownWidth = 250;
            this.cboInventoryTerm.Enabled = false;
            this.cboInventoryTerm.Location = new System.Drawing.Point(420, 20);
            this.cboInventoryTerm.Name = "cboInventoryTerm";
            this.cboInventoryTerm.Size = new System.Drawing.Size(223, 24);
            this.cboInventoryTerm.TabIndex = 3;
            // 
            // cboMainGroup
            // 
            this.cboMainGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMainGroup.DropDownWidth = 250;
            this.cboMainGroup.Enabled = false;
            this.cboMainGroup.Location = new System.Drawing.Point(420, 46);
            this.cboMainGroup.Name = "cboMainGroup";
            this.cboMainGroup.Size = new System.Drawing.Size(223, 24);
            this.cboMainGroup.TabIndex = 2;
            // 
            // dtpBeginInventoryTime
            // 
            this.dtpBeginInventoryTime.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpBeginInventoryTime.Enabled = false;
            this.dtpBeginInventoryTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBeginInventoryTime.Location = new System.Drawing.Point(918, 21);
            this.dtpBeginInventoryTime.Name = "dtpBeginInventoryTime";
            this.dtpBeginInventoryTime.Size = new System.Drawing.Size(135, 22);
            this.dtpBeginInventoryTime.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(469, 149);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 16);
            this.label13.TabIndex = 64;
            this.label13.Text = "Tủ:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(28, 149);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 16);
            this.label12.TabIndex = 54;
            this.label12.Text = "Barcode:";
            // 
            // lblReview
            // 
            this.lblReview.AutoSize = true;
            this.lblReview.Enabled = false;
            this.lblReview.Location = new System.Drawing.Point(215, 250);
            this.lblReview.Name = "lblReview";
            this.lblReview.Size = new System.Drawing.Size(72, 16);
            this.lblReview.TabIndex = 63;
            this.lblReview.Text = "Mức duyệt:";
            this.lblReview.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(28, 104);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 16);
            this.label10.TabIndex = 62;
            this.label10.Text = "Nội dung:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(664, 76);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 16);
            this.label9.TabIndex = 61;
            this.label9.Text = "NV kiểm kê:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 16);
            this.label7.TabIndex = 59;
            this.label7.Text = "Loại hàng:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(844, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 16);
            this.label6.TabIndex = 58;
            this.label6.Text = "TG bắt đầu:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(664, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 16);
            this.label5.TabIndex = 57;
            this.label5.Text = "Ngày KK:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(339, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 16);
            this.label4.TabIndex = 56;
            this.label4.Text = "Kỳ kiểm kê:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(339, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 16);
            this.label3.TabIndex = 55;
            this.label3.Text = "Ngành hàng:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 16);
            this.label2.TabIndex = 53;
            this.label2.Text = "Kho kiểm kê:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 16);
            this.label1.TabIndex = 52;
            this.label1.Text = "Mã phiếu:";
            // 
            // btnSave
            // 
            this.btnSave.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(857, 145);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(96, 25);
            this.btnSave.TabIndex = 19;
            this.btnSave.Text = "       Cập nhật";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(365, 146);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(36, 22);
            this.btnSearch.TabIndex = 14;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(108, 146);
            this.txtBarcode.MaxLength = 50;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(256, 22);
            this.txtBarcode.TabIndex = 13;
            this.txtBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarcode_KeyPress);
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(108, 101);
            this.txtContent.MaxLength = 2000;
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContent.Size = new System.Drawing.Size(535, 38);
            this.txtContent.TabIndex = 10;
            // 
            // dtpInventoryDate
            // 
            this.dtpInventoryDate.CustomFormat = "dd/MM/yyyy";
            this.dtpInventoryDate.Enabled = false;
            this.dtpInventoryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInventoryDate.Location = new System.Drawing.Point(743, 21);
            this.dtpInventoryDate.Name = "dtpInventoryDate";
            this.dtpInventoryDate.Size = new System.Drawing.Size(101, 22);
            this.dtpInventoryDate.TabIndex = 4;
            // 
            // cboStore
            // 
            this.cboStore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStore.DropDownWidth = 250;
            this.cboStore.Enabled = false;
            this.cboStore.Location = new System.Drawing.Point(108, 46);
            this.cboStore.Name = "cboStore";
            this.cboStore.Size = new System.Drawing.Size(223, 24);
            this.cboStore.TabIndex = 1;
            // 
            // flexDetail
            // 
            this.flexDetail.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexDetail.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flexDetail.AutoClipboard = true;
            this.flexDetail.ColumnInfo = "10,1,0,0,0,105,Columns:";
            this.flexDetail.ContextMenuStrip = this.mnuFlexDetail;
            this.flexDetail.Location = new System.Drawing.Point(6, 174);
            this.flexDetail.Name = "flexDetail";
            this.flexDetail.Rows.DefaultSize = 21;
            this.flexDetail.Size = new System.Drawing.Size(1047, 425);
            this.flexDetail.StyleInfo = resources.GetString("flexDetail.StyleInfo");
            this.flexDetail.TabIndex = 3;
            this.flexDetail.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2010Blue;
            this.flexDetail.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexDetail_BeforeEdit);
            this.flexDetail.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexDetail_AfterEdit);
            this.flexDetail.SetupEditor += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexDetail_SetupEditor);
            this.flexDetail.DoubleClick += new System.EventHandler(this.flexDetail_DoubleClick);
            // 
            // mnuFlexDetail
            // 
            this.mnuFlexDetail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemImportExcel,
            this.mnuItemExportTemplateExcel,
            this.mnuItemDeleteProduct});
            this.mnuFlexDetail.Name = "mnuFlexDetail";
            this.mnuFlexDetail.Size = new System.Drawing.Size(192, 70);
            this.mnuFlexDetail.Opening += new System.ComponentModel.CancelEventHandler(this.mnuFlexDetail_Opening);
            // 
            // mnuItemImportExcel
            // 
            this.mnuItemImportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.mnuItemImportExcel.Name = "mnuItemImportExcel";
            this.mnuItemImportExcel.Size = new System.Drawing.Size(191, 22);
            this.mnuItemImportExcel.Text = "Nhập từ Excel";
            this.mnuItemImportExcel.Click += new System.EventHandler(this.mnuItemImportExcel_Click);
            // 
            // mnuItemExportTemplateExcel
            // 
            this.mnuItemExportTemplateExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportTemplateExcel.Name = "mnuItemExportTemplateExcel";
            this.mnuItemExportTemplateExcel.Size = new System.Drawing.Size(191, 22);
            this.mnuItemExportTemplateExcel.Text = "Xuất ra file mẫu";
            this.mnuItemExportTemplateExcel.Click += new System.EventHandler(this.mnuItemExportTemplateExcel_Click);
            // 
            // mnuItemDeleteProduct
            // 
            this.mnuItemDeleteProduct.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.mnuItemDeleteProduct.Name = "mnuItemDeleteProduct";
            this.mnuItemDeleteProduct.ShortcutKeys = ((System.Windows.Forms.Keys)((((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.D)));
            this.mnuItemDeleteProduct.Size = new System.Drawing.Size(191, 22);
            this.mnuItemDeleteProduct.Text = "Xóa";
            this.mnuItemDeleteProduct.Click += new System.EventHandler(this.mnuItemDeleteProduct_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1067, 653);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cboProductStateID);
            this.tabPage1.Controls.Add(this.lblSum);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.txtInventoryUser);
            this.tabPage1.Controls.Add(this.txtSubGroupName);
            this.tabPage1.Controls.Add(this.lblSumQuantity);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.chkIsDeleted);
            this.tabPage1.Controls.Add(this.chkIsReviewed);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.txtContentDelete);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.flexDetail);
            this.tabPage1.Controls.Add(this.cboStore);
            this.tabPage1.Controls.Add(this.btnDelete);
            this.tabPage1.Controls.Add(this.dtpInventoryDate);
            this.tabPage1.Controls.Add(this.cboItemUnEvent);
            this.tabPage1.Controls.Add(this.txtContent);
            this.tabPage1.Controls.Add(this.txtInventoryID);
            this.tabPage1.Controls.Add(this.txtBarcode);
            this.tabPage1.Controls.Add(this.cboCabinet);
            this.tabPage1.Controls.Add(this.btnSearch);
            this.tabPage1.Controls.Add(this.cboInventoryTerm);
            this.tabPage1.Controls.Add(this.btnSave);
            this.tabPage1.Controls.Add(this.cboMainGroup);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.dtpBeginInventoryTime);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1059, 624);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thông tin kiểm kê";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // cboProductStateID
            // 
            this.cboProductStateID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProductStateID.DropDownWidth = 250;
            this.cboProductStateID.Enabled = false;
            this.cboProductStateID.Location = new System.Drawing.Point(108, 73);
            this.cboProductStateID.Name = "cboProductStateID";
            this.cboProductStateID.Size = new System.Drawing.Size(223, 24);
            this.cboProductStateID.TabIndex = 79;
            // 
            // lblSum
            // 
            this.lblSum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSum.AutoSize = true;
            this.lblSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSum.ForeColor = System.Drawing.Color.Blue;
            this.lblSum.Location = new System.Drawing.Point(132, 602);
            this.lblSum.Name = "lblSum";
            this.lblSum.Size = new System.Drawing.Size(16, 16);
            this.lblSum.TabIndex = 77;
            this.lblSum.Text = "0";
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(183, 602);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(70, 16);
            this.label19.TabIndex = 76;
            this.label19.Text = "Tổng SL:";
            // 
            // txtInventoryUser
            // 
            this.txtInventoryUser.BackColor = System.Drawing.SystemColors.Info;
            this.txtInventoryUser.Location = new System.Drawing.Point(743, 73);
            this.txtInventoryUser.Name = "txtInventoryUser";
            this.txtInventoryUser.ReadOnly = true;
            this.txtInventoryUser.Size = new System.Drawing.Size(310, 22);
            this.txtInventoryUser.TabIndex = 75;
            // 
            // txtSubGroupName
            // 
            this.txtSubGroupName.BackColor = System.Drawing.SystemColors.Info;
            this.txtSubGroupName.Location = new System.Drawing.Point(743, 47);
            this.txtSubGroupName.Name = "txtSubGroupName";
            this.txtSubGroupName.ReadOnly = true;
            this.txtSubGroupName.Size = new System.Drawing.Size(310, 22);
            this.txtSubGroupName.TabIndex = 73;
            // 
            // lblSumQuantity
            // 
            this.lblSumQuantity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSumQuantity.AutoSize = true;
            this.lblSumQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSumQuantity.ForeColor = System.Drawing.Color.Blue;
            this.lblSumQuantity.Location = new System.Drawing.Point(260, 624);
            this.lblSumQuantity.Name = "lblSumQuantity";
            this.lblSumQuantity.Size = new System.Drawing.Size(16, 16);
            this.lblSumQuantity.TabIndex = 72;
            this.lblSumQuantity.Text = "0";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 624);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(119, 16);
            this.label16.TabIndex = 71;
            this.label16.Text = "Tổng sản phẩm:";
            // 
            // chkIsDeleted
            // 
            this.chkIsDeleted.AutoSize = true;
            this.chkIsDeleted.Enabled = false;
            this.chkIsDeleted.Location = new System.Drawing.Point(506, 74);
            this.chkIsDeleted.Name = "chkIsDeleted";
            this.chkIsDeleted.Size = new System.Drawing.Size(68, 20);
            this.chkIsDeleted.TabIndex = 70;
            this.chkIsDeleted.Text = "Đã hủy";
            this.chkIsDeleted.UseVisualStyleBackColor = true;
            // 
            // chkIsReviewed
            // 
            this.chkIsReviewed.AutoSize = true;
            this.chkIsReviewed.Enabled = false;
            this.chkIsReviewed.Location = new System.Drawing.Point(420, 74);
            this.chkIsReviewed.Name = "chkIsReviewed";
            this.chkIsReviewed.Size = new System.Drawing.Size(80, 20);
            this.chkIsReviewed.TabIndex = 70;
            this.chkIsReviewed.Text = "Đã duyệt";
            this.chkIsReviewed.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(664, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 16);
            this.label8.TabIndex = 69;
            this.label8.Text = "Lý do hủy:";
            // 
            // txtContentDelete
            // 
            this.txtContentDelete.Location = new System.Drawing.Point(743, 101);
            this.txtContentDelete.MaxLength = 2000;
            this.txtContentDelete.Multiline = true;
            this.txtContentDelete.Name = "txtContentDelete";
            this.txtContentDelete.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContentDelete.Size = new System.Drawing.Size(310, 38);
            this.txtContentDelete.TabIndex = 68;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(339, 76);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 16);
            this.label15.TabIndex = 57;
            this.label15.Text = "Trạng thái:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(664, 50);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 16);
            this.label17.TabIndex = 74;
            this.label17.Text = "Nhóm hàng:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.flexUserReviewLevel);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.txtPassword);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.cboReviewStatus);
            this.tabPage2.Controls.Add(this.txtUserName);
            this.tabPage2.Controls.Add(this.btnReviewStatus);
            this.tabPage2.Controls.Add(this.lnkUsersReview);
            this.tabPage2.Controls.Add(this.lblReview);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1059, 624);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Duyệt";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // flexUserReviewLevel
            // 
            this.flexUserReviewLevel.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexUserReviewLevel.AllowFreezing = C1.Win.C1FlexGrid.AllowFreezingEnum.Both;
            this.flexUserReviewLevel.AllowMerging = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexUserReviewLevel.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexUserReviewLevel.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexUserReviewLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flexUserReviewLevel.AutoClipboard = true;
            this.flexUserReviewLevel.ColumnInfo = "10,1,0,0,0,105,Columns:0{Width:20;}\t";
            this.flexUserReviewLevel.Location = new System.Drawing.Point(893, 549);
            this.flexUserReviewLevel.Name = "flexUserReviewLevel";
            this.flexUserReviewLevel.Rows.Count = 1;
            this.flexUserReviewLevel.Rows.DefaultSize = 21;
            this.flexUserReviewLevel.Size = new System.Drawing.Size(158, 52);
            this.flexUserReviewLevel.StyleInfo = resources.GetString("flexUserReviewLevel.StyleInfo");
            this.flexUserReviewLevel.TabIndex = 57;
            this.flexUserReviewLevel.Visible = false;
            this.flexUserReviewLevel.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexUserReviewLevel_AfterEdit);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.grdUserReviewLevel);
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1051, 520);
            this.groupBox1.TabIndex = 71;
            this.groupBox1.TabStop = false;
            // 
            // grdUserReviewLevel
            // 
            this.grdUserReviewLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdUserReviewLevel.Location = new System.Drawing.Point(3, 18);
            this.grdUserReviewLevel.MainView = this.grdViewUserReviewLevel;
            this.grdUserReviewLevel.Name = "grdUserReviewLevel";
            this.grdUserReviewLevel.Size = new System.Drawing.Size(1045, 499);
            this.grdUserReviewLevel.TabIndex = 34;
            this.grdUserReviewLevel.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewUserReviewLevel});
            // 
            // grdViewUserReviewLevel
            // 
            this.grdViewUserReviewLevel.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewUserReviewLevel.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewUserReviewLevel.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewUserReviewLevel.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewUserReviewLevel.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewUserReviewLevel.Appearance.Preview.Options.UseFont = true;
            this.grdViewUserReviewLevel.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewUserReviewLevel.Appearance.Row.Options.UseFont = true;
            this.grdViewUserReviewLevel.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewUserReviewLevel.GridControl = this.grdUserReviewLevel;
            this.grdViewUserReviewLevel.Name = "grdViewUserReviewLevel";
            this.grdViewUserReviewLevel.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewUserReviewLevel.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewUserReviewLevel.OptionsNavigation.UseTabKey = false;
            this.grdViewUserReviewLevel.OptionsView.ShowAutoFilterRow = true;
            this.grdViewUserReviewLevel.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewUserReviewLevel.OptionsView.ShowFooter = true;
            this.grdViewUserReviewLevel.OptionsView.ShowGroupPanel = false;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(285, 535);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 16);
            this.label11.TabIndex = 69;
            this.label11.Text = "Tên đăng nhập:";
            // 
            // txtPassword
            // 
            this.txtPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtPassword.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtPassword.Enabled = false;
            this.txtPassword.Location = new System.Drawing.Point(391, 560);
            this.txtPassword.MaxLength = 200;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(160, 22);
            this.txtPassword.TabIndex = 68;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(285, 563);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 16);
            this.label14.TabIndex = 70;
            this.label14.Text = "Mật khẩu:";
            // 
            // txtUserName
            // 
            this.txtUserName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtUserName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtUserName.Enabled = false;
            this.txtUserName.Location = new System.Drawing.Point(391, 532);
            this.txtUserName.MaxLength = 200;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(160, 22);
            this.txtUserName.TabIndex = 67;
            this.txtUserName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUserName_KeyPress);
            // 
            // tmr
            // 
            this.tmr.Interval = 300000;
            this.tmr.Tick += new System.EventHandler(this.tmr_Tick);
            // 
            // frmInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 653);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmInventory";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kiểm kê";
            this.Load += new System.EventHandler(this.frmInventory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.popupItemUnEvent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flexDetail)).EndInit();
            this.mnuFlexDetail.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexUserReviewLevel)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdUserReviewLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewUserReviewLevel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtInventoryID;
        private System.Windows.Forms.ComboBox cboCabinet;
        private System.Windows.Forms.ComboBox cboInventoryTerm;
        private System.Windows.Forms.ComboBox cboMainGroup;
        private System.Windows.Forms.DateTimePicker dtpBeginInventoryTime;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblReview;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.DateTimePicker dtpInventoryDate;
        private System.Windows.Forms.ComboBox cboStore;
        private DevExpress.XtraEditors.DropDownButton cboItemUnEvent;
        private System.Windows.Forms.Button btnDelete;
        private C1.Win.C1FlexGrid.C1FlexGrid flexDetail;
        private System.Windows.Forms.ContextMenuStrip mnuFlexDetail;
        private System.Windows.Forms.ToolStripMenuItem mnuItemImportExcel;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportTemplateExcel;
        private DevExpress.XtraBars.PopupMenu popupItemUnEvent;
        private DevExpress.XtraBars.BarButtonItem mnuItemViewUnEvent;
        private DevExpress.XtraBars.BarButtonItem mnuItemExplainUnEvent;
        private DevExpress.XtraBars.BarButtonItem mnuEventHandlingUnEvent;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private System.Windows.Forms.LinkLabel lnkUsersReview;
        private System.Windows.Forms.Button btnReviewStatus;
        private System.Windows.Forms.ComboBox cboReviewStatus;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private C1.Win.C1FlexGrid.C1FlexGrid flexUserReviewLevel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtContentDelete;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDeleteProduct;
        private System.Windows.Forms.CheckBox chkIsDeleted;
        private System.Windows.Forms.CheckBox chkIsReviewed;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblSumQuantity;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Timer tmr;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtSubGroupName;
        private System.Windows.Forms.TextBox txtInventoryUser;
        private System.Windows.Forms.Label lblSum;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraGrid.GridControl grdUserReviewLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewUserReviewLevel;
        private System.Windows.Forms.ComboBox cboProductStateID;
    }
}