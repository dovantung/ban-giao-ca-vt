﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmResolveQuantity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmResolveQuantity));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageInfo = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.txtContentDelete = new System.Windows.Forms.TextBox();
            this.txtCreatedUser = new System.Windows.Forms.TextBox();
            this.txtIsNew = new System.Windows.Forms.TextBox();
            this.txtMainGroupName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtInventoryDate = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtInventoryTermName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCreatedDate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtStoreName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtInventoryID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtInventoryProcessID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPageDetailOutputChange = new System.Windows.Forms.TabPage();
            this.grdProductChange = new DevExpress.XtraGrid.GridControl();
            this.grdViewDataProductChange = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colProductID_Out = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName_Out = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI_Out = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOut_ProductStatusName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefSalePrice_Out = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductID_In = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName_In = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI_In = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIn_ProductStatusName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefSalePrice_In = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnEventAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCollectArrearPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDutyUsers = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnPrChangeDuytUsers = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colInputVoucherID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutputVoucherID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductChangeProcessID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInventoryProcessID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOut_ProductStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIn_ProductStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductChangeDetailID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPRProcessMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkActiveItem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkSystemItem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.calTotalLookup = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtPrChangeContent = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.flexProductChange = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.tabPageDetailInPlus = new System.Windows.Forms.TabPage();
            this.grdDataInPlus = new DevExpress.XtraGrid.GridControl();
            this.gridViewInPlus = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInputProcessID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInventoryProcessID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductStatusName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVATPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalMoney = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInputVoucherID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInputVoucherDetailID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.flexInPlus = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.txtInputContent = new System.Windows.Forms.RichTextBox();
            this.tabPageOutMinus = new System.Windows.Forms.TabPage();
            this.grdDataOutMinus = new DevExpress.XtraGrid.GridControl();
            this.gridViewOutMinus = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutMinusCollectArrearPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutMinusDUTYUSERS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOutDuytUsers = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutMinProcessMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCalcEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.flexOutMinus = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.txtOutPutContent = new System.Windows.Forms.RichTextBox();
            this.tabStChange = new System.Windows.Forms.TabPage();
            this.grdDataSTChange = new DevExpress.XtraGrid.GridControl();
            this.gridViewSTChange = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStChangeQuantityOut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSTChangeDuytUsers = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnSTDuytUser = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSTProcessMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCalcEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSTChangeContent = new System.Windows.Forms.RichTextBox();
            this.cmdClose = new System.Windows.Forms.Button();
            this.cmdUpdate = new System.Windows.Forms.Button();
            this.cmdDelete = new System.Windows.Forms.Button();
            this.cmdReview = new System.Windows.Forms.Button();
            this.cmdProcess = new System.Windows.Forms.Button();
            this.tabControl.SuspendLayout();
            this.tabPageInfo.SuspendLayout();
            this.tabPageDetailOutputChange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProductChange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewDataProductChange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrChangeDuytUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActiveItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSystemItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calTotalLookup)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexProductChange)).BeginInit();
            this.tabPageDetailInPlus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDataInPlus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInPlus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexInPlus)).BeginInit();
            this.tabPageOutMinus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDataOutMinus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOutMinus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOutDuytUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit2)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexOutMinus)).BeginInit();
            this.tabStChange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDataSTChange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSTChange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSTDuytUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit3)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPageInfo);
            this.tabControl.Controls.Add(this.tabPageDetailOutputChange);
            this.tabControl.Controls.Add(this.tabPageDetailInPlus);
            this.tabControl.Controls.Add(this.tabPageOutMinus);
            this.tabControl.Controls.Add(this.tabStChange);
            this.tabControl.Location = new System.Drawing.Point(6, 6);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1036, 529);
            this.tabControl.TabIndex = 0;
            this.tabControl.TabStop = false;
            // 
            // tabPageInfo
            // 
            this.tabPageInfo.Controls.Add(this.label13);
            this.tabPageInfo.Controls.Add(this.txtContentDelete);
            this.tabPageInfo.Controls.Add(this.txtCreatedUser);
            this.tabPageInfo.Controls.Add(this.txtIsNew);
            this.tabPageInfo.Controls.Add(this.txtMainGroupName);
            this.tabPageInfo.Controls.Add(this.label1);
            this.tabPageInfo.Controls.Add(this.txtInventoryDate);
            this.tabPageInfo.Controls.Add(this.label11);
            this.tabPageInfo.Controls.Add(this.txtInventoryTermName);
            this.tabPageInfo.Controls.Add(this.label10);
            this.tabPageInfo.Controls.Add(this.txtCreatedDate);
            this.tabPageInfo.Controls.Add(this.label9);
            this.tabPageInfo.Controls.Add(this.txtStoreName);
            this.tabPageInfo.Controls.Add(this.label8);
            this.tabPageInfo.Controls.Add(this.txtInventoryID);
            this.tabPageInfo.Controls.Add(this.label7);
            this.tabPageInfo.Controls.Add(this.txtInventoryProcessID);
            this.tabPageInfo.Controls.Add(this.label6);
            this.tabPageInfo.Controls.Add(this.label4);
            this.tabPageInfo.Controls.Add(this.label2);
            this.tabPageInfo.Location = new System.Drawing.Point(4, 25);
            this.tabPageInfo.Name = "tabPageInfo";
            this.tabPageInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageInfo.Size = new System.Drawing.Size(1028, 500);
            this.tabPageInfo.TabIndex = 0;
            this.tabPageInfo.Text = "Thông tin phiếu yêu cầu xử lý kiểm kê";
            this.tabPageInfo.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 212);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 16);
            this.label13.TabIndex = 71;
            this.label13.Text = "Lý do hủy:";
            // 
            // txtContentDelete
            // 
            this.txtContentDelete.Location = new System.Drawing.Point(254, 213);
            this.txtContentDelete.MaxLength = 2000;
            this.txtContentDelete.Multiline = true;
            this.txtContentDelete.Name = "txtContentDelete";
            this.txtContentDelete.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContentDelete.Size = new System.Drawing.Size(250, 57);
            this.txtContentDelete.TabIndex = 70;
            // 
            // txtCreatedUser
            // 
            this.txtCreatedUser.BackColor = System.Drawing.SystemColors.Info;
            this.txtCreatedUser.Location = new System.Drawing.Point(254, 185);
            this.txtCreatedUser.MaxLength = 30;
            this.txtCreatedUser.Name = "txtCreatedUser";
            this.txtCreatedUser.ReadOnly = true;
            this.txtCreatedUser.Size = new System.Drawing.Size(250, 22);
            this.txtCreatedUser.TabIndex = 19;
            // 
            // txtIsNew
            // 
            this.txtIsNew.BackColor = System.Drawing.SystemColors.Info;
            this.txtIsNew.Location = new System.Drawing.Point(254, 390);
            this.txtIsNew.MaxLength = 30;
            this.txtIsNew.Name = "txtIsNew";
            this.txtIsNew.ReadOnly = true;
            this.txtIsNew.Size = new System.Drawing.Size(250, 22);
            this.txtIsNew.TabIndex = 17;
            this.txtIsNew.Visible = false;
            // 
            // txtMainGroupName
            // 
            this.txtMainGroupName.BackColor = System.Drawing.SystemColors.Info;
            this.txtMainGroupName.Location = new System.Drawing.Point(254, 362);
            this.txtMainGroupName.MaxLength = 30;
            this.txtMainGroupName.Name = "txtMainGroupName";
            this.txtMainGroupName.ReadOnly = true;
            this.txtMainGroupName.Size = new System.Drawing.Size(250, 22);
            this.txtMainGroupName.TabIndex = 15;
            this.txtMainGroupName.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 188);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nhân viên tạo phiếu kiểm kê:";
            // 
            // txtInventoryDate
            // 
            this.txtInventoryDate.BackColor = System.Drawing.SystemColors.Info;
            this.txtInventoryDate.Location = new System.Drawing.Point(254, 157);
            this.txtInventoryDate.MaxLength = 30;
            this.txtInventoryDate.Name = "txtInventoryDate";
            this.txtInventoryDate.ReadOnly = true;
            this.txtInventoryDate.Size = new System.Drawing.Size(250, 22);
            this.txtInventoryDate.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 393);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 16);
            this.label11.TabIndex = 0;
            this.label11.Text = "Loại hàng:";
            this.label11.Visible = false;
            // 
            // txtInventoryTermName
            // 
            this.txtInventoryTermName.BackColor = System.Drawing.SystemColors.Info;
            this.txtInventoryTermName.Location = new System.Drawing.Point(254, 129);
            this.txtInventoryTermName.MaxLength = 30;
            this.txtInventoryTermName.Name = "txtInventoryTermName";
            this.txtInventoryTermName.ReadOnly = true;
            this.txtInventoryTermName.Size = new System.Drawing.Size(250, 22);
            this.txtInventoryTermName.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 365);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 16);
            this.label10.TabIndex = 0;
            this.label10.Text = "Ngành hàng:";
            this.label10.Visible = false;
            // 
            // txtCreatedDate
            // 
            this.txtCreatedDate.BackColor = System.Drawing.SystemColors.Info;
            this.txtCreatedDate.Location = new System.Drawing.Point(254, 101);
            this.txtCreatedDate.MaxLength = 30;
            this.txtCreatedDate.Name = "txtCreatedDate";
            this.txtCreatedDate.ReadOnly = true;
            this.txtCreatedDate.Size = new System.Drawing.Size(250, 22);
            this.txtCreatedDate.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 160);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 16);
            this.label9.TabIndex = 0;
            this.label9.Text = "Ngày kiểm kê:";
            // 
            // txtStoreName
            // 
            this.txtStoreName.BackColor = System.Drawing.SystemColors.Info;
            this.txtStoreName.Location = new System.Drawing.Point(254, 73);
            this.txtStoreName.MaxLength = 30;
            this.txtStoreName.Name = "txtStoreName";
            this.txtStoreName.ReadOnly = true;
            this.txtStoreName.Size = new System.Drawing.Size(250, 22);
            this.txtStoreName.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 132);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 16);
            this.label8.TabIndex = 0;
            this.label8.Text = "Kỳ kiểm kê:";
            // 
            // txtInventoryID
            // 
            this.txtInventoryID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInventoryID.Location = new System.Drawing.Point(254, 45);
            this.txtInventoryID.MaxLength = 30;
            this.txtInventoryID.Name = "txtInventoryID";
            this.txtInventoryID.ReadOnly = true;
            this.txtInventoryID.Size = new System.Drawing.Size(250, 22);
            this.txtInventoryID.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(231, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "Ngày tạo phiếu yêu cầu xử lý kiểm kê:";
            // 
            // txtInventoryProcessID
            // 
            this.txtInventoryProcessID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInventoryProcessID.Location = new System.Drawing.Point(254, 17);
            this.txtInventoryProcessID.MaxLength = 30;
            this.txtInventoryProcessID.Name = "txtInventoryProcessID";
            this.txtInventoryProcessID.ReadOnly = true;
            this.txtInventoryProcessID.Size = new System.Drawing.Size(250, 22);
            this.txtInventoryProcessID.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "Kho kiểm kê:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Mã phiếu kiểm kê:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(195, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã phiếu yêu cầu xử lý kiểm kê:";
            // 
            // tabPageDetailOutputChange
            // 
            this.tabPageDetailOutputChange.Controls.Add(this.grdProductChange);
            this.tabPageDetailOutputChange.Controls.Add(this.panel1);
            this.tabPageDetailOutputChange.Location = new System.Drawing.Point(4, 25);
            this.tabPageDetailOutputChange.Name = "tabPageDetailOutputChange";
            this.tabPageDetailOutputChange.Size = new System.Drawing.Size(1028, 500);
            this.tabPageDetailOutputChange.TabIndex = 1;
            this.tabPageDetailOutputChange.Text = "Xuất đổi hàng";
            this.tabPageDetailOutputChange.UseVisualStyleBackColor = true;
            // 
            // grdProductChange
            // 
            this.grdProductChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdProductChange.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.grdProductChange.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.grdProductChange.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.grdProductChange.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.grdProductChange.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.grdProductChange.EmbeddedNavigator.TextStringFormat = "Dòng {0} của {1}";
            this.grdProductChange.Location = new System.Drawing.Point(0, 84);
            this.grdProductChange.MainView = this.grdViewDataProductChange;
            this.grdProductChange.Name = "grdProductChange";
            this.grdProductChange.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chkIsSelect,
            this.chkActiveItem,
            this.chkSystemItem,
            this.calTotalLookup,
            this.btnPrChangeDuytUsers});
            this.grdProductChange.Size = new System.Drawing.Size(1028, 416);
            this.grdProductChange.TabIndex = 66;
            this.grdProductChange.UseEmbeddedNavigator = true;
            this.grdProductChange.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewDataProductChange});
            // 
            // grdViewDataProductChange
            // 
            this.grdViewDataProductChange.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDataProductChange.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewDataProductChange.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Red;
            this.grdViewDataProductChange.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grdViewDataProductChange.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewDataProductChange.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewDataProductChange.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDataProductChange.Appearance.Preview.Options.UseFont = true;
            this.grdViewDataProductChange.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewDataProductChange.Appearance.Row.Options.UseFont = true;
            this.grdViewDataProductChange.ColumnPanelRowHeight = 50;
            this.grdViewDataProductChange.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colProductID_Out,
            this.colProductName_Out,
            this.colIMEI_Out,
            this.colOut_ProductStatusName,
            this.colRefSalePrice_Out,
            this.colProductID_In,
            this.colProductName_In,
            this.colIMEI_In,
            this.colIn_ProductStatusName,
            this.colRefSalePrice_In,
            this.colQuantity,
            this.colUnEventAmount,
            this.colCollectArrearPrice,
            this.colDutyUsers,
            this.colInputVoucherID,
            this.colOutputVoucherID,
            this.colProductChangeProcessID,
            this.colInventoryProcessID,
            this.colOut_ProductStatusID,
            this.colIn_ProductStatusID,
            this.colCreatedDate,
            this.colProductChangeDetailID,
            this.colPRProcessMessage});
            this.grdViewDataProductChange.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewDataProductChange.GridControl = this.grdProductChange;
            this.grdViewDataProductChange.Name = "grdViewDataProductChange";
            this.grdViewDataProductChange.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewDataProductChange.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewDataProductChange.OptionsNavigation.UseTabKey = false;
            this.grdViewDataProductChange.OptionsView.ColumnAutoWidth = false;
            this.grdViewDataProductChange.OptionsView.ShowAutoFilterRow = true;
            this.grdViewDataProductChange.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewDataProductChange.OptionsView.ShowFooter = true;
            this.grdViewDataProductChange.OptionsView.ShowGroupPanel = false;
            this.grdViewDataProductChange.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grdViewDataProductChange_CellValueChanged);
            this.grdViewDataProductChange.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grdViewDataProductChange_CellValueChanging);
            // 
            // colProductID_Out
            // 
            this.colProductID_Out.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductID_Out.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductID_Out.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colProductID_Out.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProductID_Out.Caption = "Mã sản phẩm";
            this.colProductID_Out.FieldName = "PRODUCTID_OUT";
            this.colProductID_Out.Name = "colProductID_Out";
            this.colProductID_Out.OptionsColumn.AllowEdit = false;
            this.colProductID_Out.OptionsColumn.FixedWidth = true;
            this.colProductID_Out.Visible = true;
            this.colProductID_Out.VisibleIndex = 0;
            this.colProductID_Out.Width = 130;
            // 
            // colProductName_Out
            // 
            this.colProductName_Out.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductName_Out.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductName_Out.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colProductName_Out.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProductName_Out.Caption = "Tên sản phẩm";
            this.colProductName_Out.FieldName = "PRODUCTNAME_OUT";
            this.colProductName_Out.Name = "colProductName_Out";
            this.colProductName_Out.OptionsColumn.AllowEdit = false;
            this.colProductName_Out.OptionsColumn.FixedWidth = true;
            this.colProductName_Out.Visible = true;
            this.colProductName_Out.VisibleIndex = 1;
            this.colProductName_Out.Width = 180;
            // 
            // colIMEI_Out
            // 
            this.colIMEI_Out.AppearanceHeader.Options.UseTextOptions = true;
            this.colIMEI_Out.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIMEI_Out.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIMEI_Out.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIMEI_Out.Caption = "IMEI";
            this.colIMEI_Out.FieldName = "IMEI_OUT";
            this.colIMEI_Out.Name = "colIMEI_Out";
            this.colIMEI_Out.OptionsColumn.AllowEdit = false;
            this.colIMEI_Out.OptionsColumn.FixedWidth = true;
            this.colIMEI_Out.Visible = true;
            this.colIMEI_Out.VisibleIndex = 2;
            this.colIMEI_Out.Width = 150;
            // 
            // colOut_ProductStatusName
            // 
            this.colOut_ProductStatusName.AppearanceHeader.Options.UseTextOptions = true;
            this.colOut_ProductStatusName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOut_ProductStatusName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOut_ProductStatusName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOut_ProductStatusName.Caption = "Trạng thái xuất";
            this.colOut_ProductStatusName.FieldName = "OUT_PRODUCTSTATUSNAME";
            this.colOut_ProductStatusName.Name = "colOut_ProductStatusName";
            this.colOut_ProductStatusName.OptionsColumn.AllowEdit = false;
            this.colOut_ProductStatusName.OptionsColumn.FixedWidth = true;
            this.colOut_ProductStatusName.Visible = true;
            this.colOut_ProductStatusName.VisibleIndex = 3;
            this.colOut_ProductStatusName.Width = 120;
            // 
            // colRefSalePrice_Out
            // 
            this.colRefSalePrice_Out.AppearanceHeader.Options.UseTextOptions = true;
            this.colRefSalePrice_Out.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRefSalePrice_Out.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colRefSalePrice_Out.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRefSalePrice_Out.Caption = "Đơn giá bán (chưa có VAT)";
            this.colRefSalePrice_Out.DisplayFormat.FormatString = "#,##0";
            this.colRefSalePrice_Out.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRefSalePrice_Out.FieldName = "REFSALEPRICE_OUT";
            this.colRefSalePrice_Out.Name = "colRefSalePrice_Out";
            this.colRefSalePrice_Out.OptionsColumn.FixedWidth = true;
            this.colRefSalePrice_Out.Visible = true;
            this.colRefSalePrice_Out.VisibleIndex = 4;
            this.colRefSalePrice_Out.Width = 120;
            // 
            // colProductID_In
            // 
            this.colProductID_In.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductID_In.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductID_In.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colProductID_In.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProductID_In.Caption = "Mã sản phẩm";
            this.colProductID_In.FieldName = "PRODUCTID_IN";
            this.colProductID_In.Name = "colProductID_In";
            this.colProductID_In.OptionsColumn.AllowEdit = false;
            this.colProductID_In.OptionsColumn.FixedWidth = true;
            this.colProductID_In.Visible = true;
            this.colProductID_In.VisibleIndex = 5;
            this.colProductID_In.Width = 130;
            // 
            // colProductName_In
            // 
            this.colProductName_In.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductName_In.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductName_In.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colProductName_In.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProductName_In.Caption = "Tên sản phẩm";
            this.colProductName_In.FieldName = "PRODUCTNAME_IN";
            this.colProductName_In.Name = "colProductName_In";
            this.colProductName_In.OptionsColumn.AllowEdit = false;
            this.colProductName_In.OptionsColumn.FixedWidth = true;
            this.colProductName_In.Visible = true;
            this.colProductName_In.VisibleIndex = 6;
            this.colProductName_In.Width = 180;
            // 
            // colIMEI_In
            // 
            this.colIMEI_In.AppearanceHeader.Options.UseTextOptions = true;
            this.colIMEI_In.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIMEI_In.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIMEI_In.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIMEI_In.Caption = "IMEI";
            this.colIMEI_In.FieldName = "IMEI_IN";
            this.colIMEI_In.Name = "colIMEI_In";
            this.colIMEI_In.OptionsColumn.AllowEdit = false;
            this.colIMEI_In.OptionsColumn.FixedWidth = true;
            this.colIMEI_In.Visible = true;
            this.colIMEI_In.VisibleIndex = 7;
            this.colIMEI_In.Width = 150;
            // 
            // colIn_ProductStatusName
            // 
            this.colIn_ProductStatusName.AppearanceHeader.Options.UseTextOptions = true;
            this.colIn_ProductStatusName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIn_ProductStatusName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIn_ProductStatusName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIn_ProductStatusName.Caption = "Trạng thái nhập";
            this.colIn_ProductStatusName.FieldName = "IN_PRODUCTSTATUSNAME";
            this.colIn_ProductStatusName.Name = "colIn_ProductStatusName";
            this.colIn_ProductStatusName.OptionsColumn.AllowEdit = false;
            this.colIn_ProductStatusName.OptionsColumn.FixedWidth = true;
            this.colIn_ProductStatusName.Visible = true;
            this.colIn_ProductStatusName.VisibleIndex = 8;
            this.colIn_ProductStatusName.Width = 120;
            // 
            // colRefSalePrice_In
            // 
            this.colRefSalePrice_In.AppearanceHeader.Options.UseTextOptions = true;
            this.colRefSalePrice_In.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRefSalePrice_In.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colRefSalePrice_In.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRefSalePrice_In.Caption = "Đơn giá mua (chưa có VAT)";
            this.colRefSalePrice_In.DisplayFormat.FormatString = "#,##0";
            this.colRefSalePrice_In.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRefSalePrice_In.FieldName = "REFSALEPRICE_IN";
            this.colRefSalePrice_In.Name = "colRefSalePrice_In";
            this.colRefSalePrice_In.OptionsColumn.FixedWidth = true;
            this.colRefSalePrice_In.Visible = true;
            this.colRefSalePrice_In.VisibleIndex = 9;
            this.colRefSalePrice_In.Width = 120;
            // 
            // colQuantity
            // 
            this.colQuantity.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantity.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colQuantity.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantity.Caption = "Số lượng";
            this.colQuantity.DisplayFormat.FormatString = "#,##0";
            this.colQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQuantity.FieldName = "QUANTITY";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.OptionsColumn.AllowEdit = false;
            this.colQuantity.OptionsColumn.FixedWidth = true;
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 10;
            this.colQuantity.Width = 80;
            // 
            // colUnEventAmount
            // 
            this.colUnEventAmount.AppearanceHeader.Options.UseTextOptions = true;
            this.colUnEventAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUnEventAmount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colUnEventAmount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUnEventAmount.Caption = "Số tiền chênh lệch";
            this.colUnEventAmount.DisplayFormat.FormatString = "#,##0";
            this.colUnEventAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colUnEventAmount.FieldName = "UNEVENTAMOUNT";
            this.colUnEventAmount.Name = "colUnEventAmount";
            this.colUnEventAmount.OptionsColumn.AllowEdit = false;
            this.colUnEventAmount.OptionsColumn.FixedWidth = true;
            this.colUnEventAmount.Visible = true;
            this.colUnEventAmount.VisibleIndex = 11;
            // 
            // colCollectArrearPrice
            // 
            this.colCollectArrearPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colCollectArrearPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCollectArrearPrice.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCollectArrearPrice.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCollectArrearPrice.Caption = "Số tiền truy thu";
            this.colCollectArrearPrice.DisplayFormat.FormatString = "#,##0";
            this.colCollectArrearPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCollectArrearPrice.FieldName = "COLLECTARREARPRICE";
            this.colCollectArrearPrice.Name = "colCollectArrearPrice";
            this.colCollectArrearPrice.OptionsColumn.FixedWidth = true;
            this.colCollectArrearPrice.Visible = true;
            this.colCollectArrearPrice.VisibleIndex = 12;
            // 
            // colDutyUsers
            // 
            this.colDutyUsers.AppearanceHeader.Options.UseTextOptions = true;
            this.colDutyUsers.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDutyUsers.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDutyUsers.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDutyUsers.Caption = "DS nhân viên truy thu";
            this.colDutyUsers.ColumnEdit = this.btnPrChangeDuytUsers;
            this.colDutyUsers.FieldName = "DUTYUSERS";
            this.colDutyUsers.Name = "colDutyUsers";
            this.colDutyUsers.OptionsColumn.FixedWidth = true;
            this.colDutyUsers.Visible = true;
            this.colDutyUsers.VisibleIndex = 13;
            this.colDutyUsers.Width = 180;
            // 
            // btnPrChangeDuytUsers
            // 
            this.btnPrChangeDuytUsers.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "...", 35, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "", null, null, true)});
            this.btnPrChangeDuytUsers.Name = "btnPrChangeDuytUsers";
            this.btnPrChangeDuytUsers.NullText = "...";
            this.btnPrChangeDuytUsers.NullValuePrompt = "...";
            this.btnPrChangeDuytUsers.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnPrChangeDuytUsers.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnPrChangeDuytUsers_ButtonClick);
            // 
            // colInputVoucherID
            // 
            this.colInputVoucherID.AppearanceHeader.Options.UseTextOptions = true;
            this.colInputVoucherID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInputVoucherID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInputVoucherID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colInputVoucherID.Caption = "Mã phiếu nhập";
            this.colInputVoucherID.FieldName = "INPUTVOUCHERID";
            this.colInputVoucherID.Name = "colInputVoucherID";
            this.colInputVoucherID.OptionsColumn.AllowEdit = false;
            this.colInputVoucherID.OptionsColumn.FixedWidth = true;
            this.colInputVoucherID.Visible = true;
            this.colInputVoucherID.VisibleIndex = 14;
            this.colInputVoucherID.Width = 140;
            // 
            // colOutputVoucherID
            // 
            this.colOutputVoucherID.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutputVoucherID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutputVoucherID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOutputVoucherID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOutputVoucherID.Caption = "Mã phiếu xuất";
            this.colOutputVoucherID.FieldName = "OUTPUTVOUCHERID";
            this.colOutputVoucherID.Name = "colOutputVoucherID";
            this.colOutputVoucherID.OptionsColumn.AllowEdit = false;
            this.colOutputVoucherID.OptionsColumn.FixedWidth = true;
            this.colOutputVoucherID.Visible = true;
            this.colOutputVoucherID.VisibleIndex = 15;
            this.colOutputVoucherID.Width = 140;
            // 
            // colProductChangeProcessID
            // 
            this.colProductChangeProcessID.Caption = "ProductChangeProcessID";
            this.colProductChangeProcessID.FieldName = "PRODUCTCHANGEPROCESSID";
            this.colProductChangeProcessID.Name = "colProductChangeProcessID";
            // 
            // colInventoryProcessID
            // 
            this.colInventoryProcessID.Caption = "InventoryProcessID";
            this.colInventoryProcessID.FieldName = "InventoryProcessID";
            this.colInventoryProcessID.Name = "colInventoryProcessID";
            // 
            // colOut_ProductStatusID
            // 
            this.colOut_ProductStatusID.Caption = "Out_ProductStatusID";
            this.colOut_ProductStatusID.FieldName = "Out_ProductStatusID";
            this.colOut_ProductStatusID.Name = "colOut_ProductStatusID";
            // 
            // colIn_ProductStatusID
            // 
            this.colIn_ProductStatusID.Caption = "In_ProductStatusID";
            this.colIn_ProductStatusID.FieldName = "In_ProductStatusID";
            this.colIn_ProductStatusID.Name = "colIn_ProductStatusID";
            // 
            // colCreatedDate
            // 
            this.colCreatedDate.Caption = "CreatedDate";
            this.colCreatedDate.FieldName = "CreatedDate";
            this.colCreatedDate.Name = "colCreatedDate";
            // 
            // colProductChangeDetailID
            // 
            this.colProductChangeDetailID.Caption = "ProductChangeDetailID";
            this.colProductChangeDetailID.FieldName = "ProductChangeDetailID";
            this.colProductChangeDetailID.Name = "colProductChangeDetailID";
            // 
            // colPRProcessMessage
            // 
            this.colPRProcessMessage.AppearanceHeader.Options.UseTextOptions = true;
            this.colPRProcessMessage.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPRProcessMessage.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPRProcessMessage.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPRProcessMessage.Caption = "Nội dung xử lý";
            this.colPRProcessMessage.FieldName = "PROCESSMESSAGE";
            this.colPRProcessMessage.Name = "colPRProcessMessage";
            this.colPRProcessMessage.OptionsColumn.AllowEdit = false;
            this.colPRProcessMessage.OptionsColumn.FixedWidth = true;
            this.colPRProcessMessage.Visible = true;
            this.colPRProcessMessage.VisibleIndex = 16;
            this.colPRProcessMessage.Width = 200;
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            // 
            // chkActiveItem
            // 
            this.chkActiveItem.AutoHeight = false;
            this.chkActiveItem.Name = "chkActiveItem";
            this.chkActiveItem.ValueChecked = ((short)(1));
            this.chkActiveItem.ValueUnchecked = ((short)(0));
            // 
            // chkSystemItem
            // 
            this.chkSystemItem.AutoHeight = false;
            this.chkSystemItem.Name = "chkSystemItem";
            this.chkSystemItem.ValueChecked = ((short)(1));
            this.chkSystemItem.ValueUnchecked = ((short)(0));
            // 
            // calTotalLookup
            // 
            this.calTotalLookup.AutoHeight = false;
            this.calTotalLookup.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calTotalLookup.DisplayFormat.FormatString = "#,##0";
            this.calTotalLookup.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.calTotalLookup.EditFormat.FormatString = "#,##0";
            this.calTotalLookup.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.calTotalLookup.Name = "calTotalLookup";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtPrChangeContent);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.flexProductChange);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1028, 84);
            this.panel1.TabIndex = 67;
            // 
            // txtPrChangeContent
            // 
            this.txtPrChangeContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPrChangeContent.Location = new System.Drawing.Point(162, 13);
            this.txtPrChangeContent.Name = "txtPrChangeContent";
            this.txtPrChangeContent.Size = new System.Drawing.Size(846, 57);
            this.txtPrChangeContent.TabIndex = 65;
            this.txtPrChangeContent.Text = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 13);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(147, 16);
            this.label12.TabIndex = 63;
            this.label12.Text = "Nội dung xuất đổi hàng:";
            // 
            // flexProductChange
            // 
            this.flexProductChange.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexProductChange.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexProductChange.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexProductChange.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.flexProductChange.AutoClipboard = true;
            this.flexProductChange.ColumnInfo = "10,1,0,0,0,105,Columns:0{Width:1;}\t";
            this.flexProductChange.Location = new System.Drawing.Point(19, 32);
            this.flexProductChange.Name = "flexProductChange";
            this.flexProductChange.Rows.Count = 1;
            this.flexProductChange.Rows.DefaultSize = 21;
            this.flexProductChange.Size = new System.Drawing.Size(62, 40);
            this.flexProductChange.StyleInfo = resources.GetString("flexProductChange.StyleInfo");
            this.flexProductChange.TabIndex = 1;
            this.flexProductChange.Visible = false;
            this.flexProductChange.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexProductChange_BeforeEdit);
            this.flexProductChange.CellButtonClick += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexProductChange_CellButtonClick);
            // 
            // tabPageDetailInPlus
            // 
            this.tabPageDetailInPlus.Controls.Add(this.grdDataInPlus);
            this.tabPageDetailInPlus.Controls.Add(this.panel2);
            this.tabPageDetailInPlus.Location = new System.Drawing.Point(4, 25);
            this.tabPageDetailInPlus.Name = "tabPageDetailInPlus";
            this.tabPageDetailInPlus.Size = new System.Drawing.Size(1028, 500);
            this.tabPageDetailInPlus.TabIndex = 2;
            this.tabPageDetailInPlus.Text = "Nhập thừa";
            this.tabPageDetailInPlus.UseVisualStyleBackColor = true;
            // 
            // grdDataInPlus
            // 
            this.grdDataInPlus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDataInPlus.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.grdDataInPlus.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.grdDataInPlus.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.grdDataInPlus.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.grdDataInPlus.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.grdDataInPlus.EmbeddedNavigator.TextStringFormat = "Dòng {0} của {1}";
            this.grdDataInPlus.Location = new System.Drawing.Point(0, 80);
            this.grdDataInPlus.MainView = this.gridViewInPlus;
            this.grdDataInPlus.Name = "grdDataInPlus";
            this.grdDataInPlus.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3,
            this.repositoryItemCalcEdit1});
            this.grdDataInPlus.Size = new System.Drawing.Size(1028, 420);
            this.grdDataInPlus.TabIndex = 67;
            this.grdDataInPlus.UseEmbeddedNavigator = true;
            this.grdDataInPlus.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewInPlus});
            // 
            // gridViewInPlus
            // 
            this.gridViewInPlus.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewInPlus.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewInPlus.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Red;
            this.gridViewInPlus.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridViewInPlus.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridViewInPlus.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewInPlus.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewInPlus.Appearance.Preview.Options.UseFont = true;
            this.gridViewInPlus.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewInPlus.Appearance.Row.Options.UseFont = true;
            this.gridViewInPlus.ColumnPanelRowHeight = 50;
            this.gridViewInPlus.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInputProcessID,
            this.colInventoryProcessID1,
            this.colProductID,
            this.colProductName,
            this.colIMEI,
            this.colProductStatusID,
            this.colProductStatusName,
            this.colPrice,
            this.colQuantity1,
            this.colVAT,
            this.colVATPercent,
            this.colTotalMoney,
            this.colCreatedDate1,
            this.colInputVoucherID1,
            this.colInputVoucherDetailID1});
            this.gridViewInPlus.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridViewInPlus.GridControl = this.grdDataInPlus;
            this.gridViewInPlus.Name = "gridViewInPlus";
            this.gridViewInPlus.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridViewInPlus.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridViewInPlus.OptionsNavigation.UseTabKey = false;
            this.gridViewInPlus.OptionsView.ColumnAutoWidth = false;
            this.gridViewInPlus.OptionsView.ShowAutoFilterRow = true;
            this.gridViewInPlus.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewInPlus.OptionsView.ShowFooter = true;
            this.gridViewInPlus.OptionsView.ShowGroupPanel = false;
            this.gridViewInPlus.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewInPlus_CellValueChanged);
            // 
            // colInputProcessID
            // 
            this.colInputProcessID.AppearanceHeader.Options.UseTextOptions = true;
            this.colInputProcessID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInputProcessID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInputProcessID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colInputProcessID.Caption = "InputProcessID";
            this.colInputProcessID.FieldName = "INPUTPROCESSID";
            this.colInputProcessID.Name = "colInputProcessID";
            this.colInputProcessID.OptionsColumn.AllowEdit = false;
            this.colInputProcessID.OptionsColumn.FixedWidth = true;
            // 
            // colInventoryProcessID1
            // 
            this.colInventoryProcessID1.AppearanceHeader.Options.UseTextOptions = true;
            this.colInventoryProcessID1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInventoryProcessID1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInventoryProcessID1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colInventoryProcessID1.Caption = "InventoryProcessID";
            this.colInventoryProcessID1.FieldName = "INVENTORYPROCESSID";
            this.colInventoryProcessID1.Name = "colInventoryProcessID1";
            this.colInventoryProcessID1.OptionsColumn.AllowEdit = false;
            this.colInventoryProcessID1.OptionsColumn.FixedWidth = true;
            // 
            // colProductID
            // 
            this.colProductID.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colProductID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProductID.Caption = "Mã sản phẩm";
            this.colProductID.FieldName = "PRODUCTID";
            this.colProductID.Name = "colProductID";
            this.colProductID.OptionsColumn.AllowEdit = false;
            this.colProductID.OptionsColumn.FixedWidth = true;
            this.colProductID.Visible = true;
            this.colProductID.VisibleIndex = 0;
            this.colProductID.Width = 130;
            // 
            // colProductName
            // 
            this.colProductName.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colProductName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProductName.Caption = "Tên sản phẩm";
            this.colProductName.FieldName = "PRODUCTNAME";
            this.colProductName.Name = "colProductName";
            this.colProductName.OptionsColumn.AllowEdit = false;
            this.colProductName.OptionsColumn.FixedWidth = true;
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 1;
            this.colProductName.Width = 180;
            // 
            // colIMEI
            // 
            this.colIMEI.AppearanceHeader.Options.UseTextOptions = true;
            this.colIMEI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIMEI.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIMEI.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIMEI.Caption = "IMEI";
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsColumn.AllowEdit = false;
            this.colIMEI.OptionsColumn.FixedWidth = true;
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 2;
            this.colIMEI.Width = 150;
            // 
            // colProductStatusID
            // 
            this.colProductStatusID.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductStatusID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductStatusID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colProductStatusID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProductStatusID.Caption = "Mã trạng thái";
            this.colProductStatusID.FieldName = "PRODUCTSTATUSID";
            this.colProductStatusID.Name = "colProductStatusID";
            this.colProductStatusID.OptionsColumn.AllowEdit = false;
            this.colProductStatusID.OptionsColumn.FixedWidth = true;
            // 
            // colProductStatusName
            // 
            this.colProductStatusName.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductStatusName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductStatusName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colProductStatusName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProductStatusName.Caption = "Tên trạng thái";
            this.colProductStatusName.FieldName = "PRODUCTSTATUSNAME";
            this.colProductStatusName.Name = "colProductStatusName";
            this.colProductStatusName.OptionsColumn.AllowEdit = false;
            this.colProductStatusName.OptionsColumn.FixedWidth = true;
            this.colProductStatusName.Visible = true;
            this.colProductStatusName.VisibleIndex = 3;
            this.colProductStatusName.Width = 150;
            // 
            // colPrice
            // 
            this.colPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrice.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colPrice.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colPrice.Caption = "Đơn giá (chưa VAT)";
            this.colPrice.DisplayFormat.FormatString = "#,##0";
            this.colPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrice.FieldName = "PRICE";
            this.colPrice.Name = "colPrice";
            this.colPrice.OptionsColumn.FixedWidth = true;
            this.colPrice.Visible = true;
            this.colPrice.VisibleIndex = 4;
            this.colPrice.Width = 110;
            // 
            // colQuantity1
            // 
            this.colQuantity1.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantity1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantity1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colQuantity1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantity1.Caption = "Số lượng";
            this.colQuantity1.DisplayFormat.FormatString = "#,##0";
            this.colQuantity1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQuantity1.FieldName = "QUANTITY";
            this.colQuantity1.Name = "colQuantity1";
            this.colQuantity1.OptionsColumn.AllowEdit = false;
            this.colQuantity1.OptionsColumn.FixedWidth = true;
            this.colQuantity1.Visible = true;
            this.colQuantity1.VisibleIndex = 5;
            this.colQuantity1.Width = 80;
            // 
            // colVAT
            // 
            this.colVAT.AppearanceHeader.Options.UseTextOptions = true;
            this.colVAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVAT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colVAT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVAT.Caption = "VAT";
            this.colVAT.FieldName = "VAT";
            this.colVAT.Name = "colVAT";
            this.colVAT.OptionsColumn.AllowEdit = false;
            this.colVAT.OptionsColumn.FixedWidth = true;
            this.colVAT.Visible = true;
            this.colVAT.VisibleIndex = 6;
            this.colVAT.Width = 80;
            // 
            // colVATPercent
            // 
            this.colVATPercent.AppearanceHeader.Options.UseTextOptions = true;
            this.colVATPercent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVATPercent.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colVATPercent.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVATPercent.Caption = "% VAT nộp";
            this.colVATPercent.DisplayFormat.FormatString = "#,##0";
            this.colVATPercent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colVATPercent.FieldName = "VATPERCENT";
            this.colVATPercent.Name = "colVATPercent";
            this.colVATPercent.OptionsColumn.AllowEdit = false;
            this.colVATPercent.OptionsColumn.FixedWidth = true;
            this.colVATPercent.Visible = true;
            this.colVATPercent.VisibleIndex = 7;
            this.colVATPercent.Width = 80;
            // 
            // colTotalMoney
            // 
            this.colTotalMoney.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalMoney.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalMoney.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalMoney.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalMoney.Caption = "Tổng tiền";
            this.colTotalMoney.DisplayFormat.FormatString = "#,##0";
            this.colTotalMoney.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalMoney.FieldName = "TOTALMONEY";
            this.colTotalMoney.Name = "colTotalMoney";
            this.colTotalMoney.OptionsColumn.AllowEdit = false;
            this.colTotalMoney.OptionsColumn.FixedWidth = true;
            this.colTotalMoney.Visible = true;
            this.colTotalMoney.VisibleIndex = 8;
            this.colTotalMoney.Width = 100;
            // 
            // colCreatedDate1
            // 
            this.colCreatedDate1.AppearanceHeader.Options.UseTextOptions = true;
            this.colCreatedDate1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCreatedDate1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCreatedDate1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCreatedDate1.Caption = "CreatedDate";
            this.colCreatedDate1.FieldName = "CREATEDDATE";
            this.colCreatedDate1.Name = "colCreatedDate1";
            this.colCreatedDate1.OptionsColumn.AllowEdit = false;
            this.colCreatedDate1.OptionsColumn.FixedWidth = true;
            // 
            // colInputVoucherID1
            // 
            this.colInputVoucherID1.AppearanceHeader.Options.UseTextOptions = true;
            this.colInputVoucherID1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInputVoucherID1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInputVoucherID1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colInputVoucherID1.Caption = "Mã phiếu nhập";
            this.colInputVoucherID1.FieldName = "INPUTVOUCHERID";
            this.colInputVoucherID1.Name = "colInputVoucherID1";
            this.colInputVoucherID1.OptionsColumn.AllowEdit = false;
            this.colInputVoucherID1.OptionsColumn.FixedWidth = true;
            this.colInputVoucherID1.Visible = true;
            this.colInputVoucherID1.VisibleIndex = 9;
            this.colInputVoucherID1.Width = 150;
            // 
            // colInputVoucherDetailID1
            // 
            this.colInputVoucherDetailID1.AppearanceHeader.Options.UseTextOptions = true;
            this.colInputVoucherDetailID1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInputVoucherDetailID1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInputVoucherDetailID1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colInputVoucherDetailID1.Caption = "InputVoucherDetailID";
            this.colInputVoucherDetailID1.FieldName = "INPUTVOUCHERDETAILID";
            this.colInputVoucherDetailID1.Name = "colInputVoucherDetailID1";
            this.colInputVoucherDetailID1.OptionsColumn.AllowEdit = false;
            this.colInputVoucherDetailID1.OptionsColumn.FixedWidth = true;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit2.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit3.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.DisplayFormat.FormatString = "#,##0";
            this.repositoryItemCalcEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemCalcEdit1.EditFormat.FormatString = "#,##0";
            this.repositoryItemCalcEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.flexInPlus);
            this.panel2.Controls.Add(this.txtInputContent);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1028, 80);
            this.panel2.TabIndex = 65;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 16);
            this.label5.TabIndex = 62;
            this.label5.Text = "Nội dung nhập thừa:";
            // 
            // flexInPlus
            // 
            this.flexInPlus.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexInPlus.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexInPlus.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexInPlus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flexInPlus.AutoClipboard = true;
            this.flexInPlus.ColumnInfo = "10,1,0,0,0,105,Columns:0{Width:1;}\t";
            this.flexInPlus.Location = new System.Drawing.Point(25, 36);
            this.flexInPlus.Name = "flexInPlus";
            this.flexInPlus.Rows.Count = 1;
            this.flexInPlus.Rows.DefaultSize = 21;
            this.flexInPlus.Size = new System.Drawing.Size(47, 31);
            this.flexInPlus.StyleInfo = resources.GetString("flexInPlus.StyleInfo");
            this.flexInPlus.TabIndex = 2;
            this.flexInPlus.Visible = false;
            this.flexInPlus.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexInPlus_BeforeEdit);
            this.flexInPlus.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexInPlus_AfterEdit);
            // 
            // txtInputContent
            // 
            this.txtInputContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInputContent.Location = new System.Drawing.Point(143, 12);
            this.txtInputContent.Name = "txtInputContent";
            this.txtInputContent.Size = new System.Drawing.Size(873, 57);
            this.txtInputContent.TabIndex = 64;
            this.txtInputContent.Text = "";
            // 
            // tabPageOutMinus
            // 
            this.tabPageOutMinus.Controls.Add(this.grdDataOutMinus);
            this.tabPageOutMinus.Controls.Add(this.panel3);
            this.tabPageOutMinus.Location = new System.Drawing.Point(4, 25);
            this.tabPageOutMinus.Name = "tabPageOutMinus";
            this.tabPageOutMinus.Size = new System.Drawing.Size(1028, 500);
            this.tabPageOutMinus.TabIndex = 3;
            this.tabPageOutMinus.Text = "Xuất thiếu";
            this.tabPageOutMinus.UseVisualStyleBackColor = true;
            // 
            // grdDataOutMinus
            // 
            this.grdDataOutMinus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDataOutMinus.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.grdDataOutMinus.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.grdDataOutMinus.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.grdDataOutMinus.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.grdDataOutMinus.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.grdDataOutMinus.EmbeddedNavigator.TextStringFormat = "Dòng {0} của {1}";
            this.grdDataOutMinus.Location = new System.Drawing.Point(0, 80);
            this.grdDataOutMinus.MainView = this.gridViewOutMinus;
            this.grdDataOutMinus.Name = "grdDataOutMinus";
            this.grdDataOutMinus.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4,
            this.repositoryItemCheckEdit5,
            this.repositoryItemCheckEdit6,
            this.repositoryItemCalcEdit2,
            this.btnOutDuytUsers});
            this.grdDataOutMinus.Size = new System.Drawing.Size(1028, 420);
            this.grdDataOutMinus.TabIndex = 68;
            this.grdDataOutMinus.UseEmbeddedNavigator = true;
            this.grdDataOutMinus.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewOutMinus});
            // 
            // gridViewOutMinus
            // 
            this.gridViewOutMinus.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewOutMinus.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewOutMinus.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Red;
            this.gridViewOutMinus.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridViewOutMinus.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridViewOutMinus.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewOutMinus.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewOutMinus.Appearance.Preview.Options.UseFont = true;
            this.gridViewOutMinus.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewOutMinus.Appearance.Row.Options.UseFont = true;
            this.gridViewOutMinus.ColumnPanelRowHeight = 50;
            this.gridViewOutMinus.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.colOutMinusCollectArrearPrice,
            this.colOutMinusDUTYUSERS,
            this.gridColumn14,
            this.gridColumn15,
            this.colOutMinProcessMessage});
            this.gridViewOutMinus.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridViewOutMinus.GridControl = this.grdDataOutMinus;
            this.gridViewOutMinus.Name = "gridViewOutMinus";
            this.gridViewOutMinus.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridViewOutMinus.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridViewOutMinus.OptionsNavigation.UseTabKey = false;
            this.gridViewOutMinus.OptionsView.ColumnAutoWidth = false;
            this.gridViewOutMinus.OptionsView.ShowAutoFilterRow = true;
            this.gridViewOutMinus.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewOutMinus.OptionsView.ShowFooter = true;
            this.gridViewOutMinus.OptionsView.ShowGroupPanel = false;
            this.gridViewOutMinus.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewOutMinus_CellValueChanged);
            this.gridViewOutMinus.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridViewOutMinus_ValidatingEditor);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn1.Caption = "InputProcessID";
            this.gridColumn1.FieldName = "INPUTPROCESSID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "InventoryProcessID";
            this.gridColumn2.FieldName = "INVENTORYPROCESSID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.FixedWidth = true;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "Mã sản phẩm";
            this.gridColumn3.FieldName = "PRODUCTID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.FixedWidth = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 130;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn4.Caption = "Tên sản phẩm";
            this.gridColumn4.FieldName = "PRODUCTNAME";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.FixedWidth = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            this.gridColumn4.Width = 180;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn5.Caption = "IMEI";
            this.gridColumn5.FieldName = "IMEI";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.FixedWidth = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            this.gridColumn5.Width = 150;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn6.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn6.Caption = "Mã trạng thái";
            this.gridColumn6.FieldName = "PRODUCTSTATUSID";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.FixedWidth = true;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn7.Caption = "Tên trạng thái";
            this.gridColumn7.FieldName = "PRODUCTSTATUSNAME";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.FixedWidth = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            this.gridColumn7.Width = 150;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn8.Caption = "Đơn giá (chưa VAT)";
            this.gridColumn8.DisplayFormat.FormatString = "#,##0";
            this.gridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn8.FieldName = "PRICE";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.FixedWidth = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 4;
            this.gridColumn8.Width = 110;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn9.Caption = "Số lượng";
            this.gridColumn9.DisplayFormat.FormatString = "#,##0";
            this.gridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn9.FieldName = "QUANTITY";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.FixedWidth = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 5;
            this.gridColumn9.Width = 80;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn10.Caption = "VAT";
            this.gridColumn10.FieldName = "VAT";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.FixedWidth = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 6;
            this.gridColumn10.Width = 80;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn11.Caption = "% VAT nộp";
            this.gridColumn11.DisplayFormat.FormatString = "#,##0";
            this.gridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn11.FieldName = "VATPERCENT";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.FixedWidth = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 7;
            this.gridColumn11.Width = 80;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn12.Caption = "Tổng tiền";
            this.gridColumn12.DisplayFormat.FormatString = "#,##0";
            this.gridColumn12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn12.FieldName = "TOTALMONEY";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.FixedWidth = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 8;
            this.gridColumn12.Width = 100;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn13.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn13.Caption = "CreatedDate";
            this.gridColumn13.FieldName = "CREATEDDATE";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.FixedWidth = true;
            // 
            // colOutMinusCollectArrearPrice
            // 
            this.colOutMinusCollectArrearPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutMinusCollectArrearPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutMinusCollectArrearPrice.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOutMinusCollectArrearPrice.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOutMinusCollectArrearPrice.Caption = "Số tiền truy thu";
            this.colOutMinusCollectArrearPrice.DisplayFormat.FormatString = "#,##0";
            this.colOutMinusCollectArrearPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOutMinusCollectArrearPrice.FieldName = "COLLECTARREARPRICE";
            this.colOutMinusCollectArrearPrice.Name = "colOutMinusCollectArrearPrice";
            this.colOutMinusCollectArrearPrice.OptionsColumn.AllowEdit = false;
            this.colOutMinusCollectArrearPrice.OptionsColumn.FixedWidth = true;
            this.colOutMinusCollectArrearPrice.Visible = true;
            this.colOutMinusCollectArrearPrice.VisibleIndex = 9;
            this.colOutMinusCollectArrearPrice.Width = 100;
            // 
            // colOutMinusDUTYUSERS
            // 
            this.colOutMinusDUTYUSERS.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutMinusDUTYUSERS.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutMinusDUTYUSERS.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOutMinusDUTYUSERS.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOutMinusDUTYUSERS.Caption = "DS nhân viên truy thu";
            this.colOutMinusDUTYUSERS.ColumnEdit = this.btnOutDuytUsers;
            this.colOutMinusDUTYUSERS.FieldName = "DUTYUSERS";
            this.colOutMinusDUTYUSERS.Name = "colOutMinusDUTYUSERS";
            this.colOutMinusDUTYUSERS.OptionsColumn.FixedWidth = true;
            this.colOutMinusDUTYUSERS.Visible = true;
            this.colOutMinusDUTYUSERS.VisibleIndex = 10;
            this.colOutMinusDUTYUSERS.Width = 140;
            // 
            // btnOutDuytUsers
            // 
            this.btnOutDuytUsers.AutoHeight = false;
            this.btnOutDuytUsers.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "...", 35, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", null, null, true)});
            this.btnOutDuytUsers.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btnOutDuytUsers.Name = "btnOutDuytUsers";
            this.btnOutDuytUsers.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnOutDuytUsers.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnOutDuytUsers_ButtonClick);
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn14.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn14.Caption = "Mã phiếu xuất";
            this.gridColumn14.FieldName = "OUTPUTVOUCHERID";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.FixedWidth = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 11;
            this.gridColumn14.Width = 150;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn15.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn15.Caption = "OUTPUTVOUCHERDETAILID";
            this.gridColumn15.FieldName = "OUTPUTVOUCHERDETAILID";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.FixedWidth = true;
            // 
            // colOutMinProcessMessage
            // 
            this.colOutMinProcessMessage.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutMinProcessMessage.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutMinProcessMessage.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOutMinProcessMessage.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOutMinProcessMessage.Caption = "Nội dung xử lý";
            this.colOutMinProcessMessage.FieldName = "PROCESSMESSAGE";
            this.colOutMinProcessMessage.Name = "colOutMinProcessMessage";
            this.colOutMinProcessMessage.OptionsColumn.AllowEdit = false;
            this.colOutMinProcessMessage.OptionsColumn.FixedWidth = true;
            this.colOutMinProcessMessage.Visible = true;
            this.colOutMinProcessMessage.VisibleIndex = 12;
            this.colOutMinProcessMessage.Width = 200;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit5.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit6.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCalcEdit2
            // 
            this.repositoryItemCalcEdit2.AutoHeight = false;
            this.repositoryItemCalcEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit2.DisplayFormat.FormatString = "#,##0";
            this.repositoryItemCalcEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemCalcEdit2.EditFormat.FormatString = "#,##0";
            this.repositoryItemCalcEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemCalcEdit2.Name = "repositoryItemCalcEdit2";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.flexOutMinus);
            this.panel3.Controls.Add(this.txtOutPutContent);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1028, 80);
            this.panel3.TabIndex = 64;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 16);
            this.label3.TabIndex = 61;
            this.label3.Text = "Nội dung xuất thiếu:";
            // 
            // flexOutMinus
            // 
            this.flexOutMinus.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexOutMinus.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexOutMinus.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexOutMinus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flexOutMinus.AutoClipboard = true;
            this.flexOutMinus.ColumnInfo = "10,1,0,0,0,105,Columns:0{Width:1;}\t";
            this.flexOutMinus.Location = new System.Drawing.Point(50, 31);
            this.flexOutMinus.Name = "flexOutMinus";
            this.flexOutMinus.Rows.Count = 1;
            this.flexOutMinus.Rows.DefaultSize = 21;
            this.flexOutMinus.Size = new System.Drawing.Size(48, 34);
            this.flexOutMinus.StyleInfo = resources.GetString("flexOutMinus.StyleInfo");
            this.flexOutMinus.TabIndex = 2;
            this.flexOutMinus.Visible = false;
            this.flexOutMinus.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexOutMinus_BeforeEdit);
            this.flexOutMinus.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexOutMinus_AfterEdit);
            this.flexOutMinus.CellButtonClick += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexOutMinus_CellButtonClick);
            // 
            // txtOutPutContent
            // 
            this.txtOutPutContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutPutContent.Location = new System.Drawing.Point(143, 12);
            this.txtOutPutContent.Name = "txtOutPutContent";
            this.txtOutPutContent.Size = new System.Drawing.Size(871, 57);
            this.txtOutPutContent.TabIndex = 63;
            this.txtOutPutContent.Text = "";
            // 
            // tabStChange
            // 
            this.tabStChange.Controls.Add(this.grdDataSTChange);
            this.tabStChange.Controls.Add(this.panel4);
            this.tabStChange.Location = new System.Drawing.Point(4, 25);
            this.tabStChange.Name = "tabStChange";
            this.tabStChange.Padding = new System.Windows.Forms.Padding(3);
            this.tabStChange.Size = new System.Drawing.Size(1028, 500);
            this.tabStChange.TabIndex = 4;
            this.tabStChange.Text = "Xuất đổi trạng thái";
            this.tabStChange.UseVisualStyleBackColor = true;
            // 
            // grdDataSTChange
            // 
            this.grdDataSTChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDataSTChange.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.grdDataSTChange.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.grdDataSTChange.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.grdDataSTChange.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.grdDataSTChange.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.grdDataSTChange.EmbeddedNavigator.TextStringFormat = "Dòng {0} của {1}";
            this.grdDataSTChange.Location = new System.Drawing.Point(3, 83);
            this.grdDataSTChange.MainView = this.gridViewSTChange;
            this.grdDataSTChange.Name = "grdDataSTChange";
            this.grdDataSTChange.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit7,
            this.repositoryItemCheckEdit8,
            this.repositoryItemCheckEdit9,
            this.repositoryItemCalcEdit3,
            this.btnSTDuytUser});
            this.grdDataSTChange.Size = new System.Drawing.Size(1022, 414);
            this.grdDataSTChange.TabIndex = 69;
            this.grdDataSTChange.UseEmbeddedNavigator = true;
            this.grdDataSTChange.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSTChange});
            // 
            // gridViewSTChange
            // 
            this.gridViewSTChange.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewSTChange.Appearance.FocusedRow.Options.UseFont = true;
            this.gridViewSTChange.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Red;
            this.gridViewSTChange.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridViewSTChange.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridViewSTChange.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewSTChange.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewSTChange.Appearance.Preview.Options.UseFont = true;
            this.gridViewSTChange.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridViewSTChange.Appearance.Row.Options.UseFont = true;
            this.gridViewSTChange.ColumnPanelRowHeight = 50;
            this.gridViewSTChange.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.colStChangeQuantityOut,
            this.gridColumn31,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn34,
            this.gridColumn29,
            this.gridColumn30,
            this.colSTChangeDuytUsers,
            this.gridColumn32,
            this.gridColumn33,
            this.colSTProcessMessage});
            this.gridViewSTChange.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridViewSTChange.GridControl = this.grdDataSTChange;
            this.gridViewSTChange.Name = "gridViewSTChange";
            this.gridViewSTChange.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridViewSTChange.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridViewSTChange.OptionsNavigation.UseTabKey = false;
            this.gridViewSTChange.OptionsView.ColumnAutoWidth = false;
            this.gridViewSTChange.OptionsView.ShowAutoFilterRow = true;
            this.gridViewSTChange.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewSTChange.OptionsView.ShowFooter = true;
            this.gridViewSTChange.OptionsView.ShowGroupPanel = false;
            this.gridViewSTChange.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewSTChange_CellValueChanged);
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn18.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn18.Caption = "InputProcessID";
            this.gridColumn18.FieldName = "INPUTPROCESSID";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.FixedWidth = true;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn19.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn19.Caption = "InventoryProcessID";
            this.gridColumn19.FieldName = "INVENTORYPROCESSID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.FixedWidth = true;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn20.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn20.Caption = "Mã sản phẩm xuất";
            this.gridColumn20.FieldName = "PRODUCTID_OUT";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.FixedWidth = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            this.gridColumn20.Width = 130;
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn21.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn21.Caption = "Tên sản phẩm xuất";
            this.gridColumn21.FieldName = "PRODUCTNAME_OUT";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.FixedWidth = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            this.gridColumn21.Width = 180;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn22.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn22.Caption = "IMEI xuất";
            this.gridColumn22.FieldName = "IMEI_OUT";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.FixedWidth = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            this.gridColumn22.Width = 130;
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn23.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn23.Caption = "Giá xuất (chưa VAT)";
            this.gridColumn23.DisplayFormat.FormatString = "#,##0";
            this.gridColumn23.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn23.FieldName = "REFSALEPRICE_OUT";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.FixedWidth = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            this.gridColumn23.Width = 110;
            // 
            // colStChangeQuantityOut
            // 
            this.colStChangeQuantityOut.AppearanceHeader.Options.UseTextOptions = true;
            this.colStChangeQuantityOut.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStChangeQuantityOut.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colStChangeQuantityOut.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStChangeQuantityOut.Caption = "Số lượng xuất";
            this.colStChangeQuantityOut.FieldName = "QUANTITY";
            this.colStChangeQuantityOut.Name = "colStChangeQuantityOut";
            this.colStChangeQuantityOut.OptionsColumn.AllowEdit = false;
            this.colStChangeQuantityOut.OptionsColumn.FixedWidth = true;
            this.colStChangeQuantityOut.Visible = true;
            this.colStChangeQuantityOut.VisibleIndex = 4;
            this.colStChangeQuantityOut.Width = 80;
            // 
            // gridColumn31
            // 
            this.gridColumn31.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn31.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn31.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn31.Caption = "Trạng thái xuất";
            this.gridColumn31.FieldName = "OUT_PRODUCTSTATUSNAME";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.FixedWidth = true;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 5;
            this.gridColumn31.Width = 120;
            // 
            // gridColumn24
            // 
            this.gridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn24.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn24.Caption = "Mã sản phẩm nhập";
            this.gridColumn24.FieldName = "PRODUCTID_IN";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.FixedWidth = true;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 6;
            this.gridColumn24.Width = 130;
            // 
            // gridColumn25
            // 
            this.gridColumn25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn25.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn25.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn25.Caption = "Tên sản phẩm nhập";
            this.gridColumn25.FieldName = "PRODUCTNAME_IN";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.FixedWidth = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 7;
            this.gridColumn25.Width = 180;
            // 
            // gridColumn26
            // 
            this.gridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn26.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn26.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn26.Caption = "IMEI nhập";
            this.gridColumn26.FieldName = "IMEI_IN";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.FixedWidth = true;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 8;
            this.gridColumn26.Width = 130;
            // 
            // gridColumn27
            // 
            this.gridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn27.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn27.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn27.Caption = "Giá nhập (chưa VAT)";
            this.gridColumn27.DisplayFormat.FormatString = "#,##0";
            this.gridColumn27.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn27.FieldName = "REFSALEPRICE_IN";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.FixedWidth = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 9;
            this.gridColumn27.Width = 110;
            // 
            // gridColumn28
            // 
            this.gridColumn28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn28.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn28.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn28.Caption = "Số lượng nhập";
            this.gridColumn28.DisplayFormat.FormatString = "#,##0";
            this.gridColumn28.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn28.FieldName = "QUANTITY";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.FixedWidth = true;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 10;
            this.gridColumn28.Width = 80;
            // 
            // gridColumn34
            // 
            this.gridColumn34.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn34.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn34.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn34.Caption = "Trạng thái nhập";
            this.gridColumn34.FieldName = "IN_PRODUCTSTATUSNAME";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.FixedWidth = true;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 11;
            this.gridColumn34.Width = 120;
            // 
            // gridColumn29
            // 
            this.gridColumn29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn29.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn29.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn29.Caption = "Số tiền chênh lệch";
            this.gridColumn29.DisplayFormat.FormatString = "#,##0";
            this.gridColumn29.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn29.FieldName = "UNEVENTAMOUNT";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.FixedWidth = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 12;
            this.gridColumn29.Width = 110;
            // 
            // gridColumn30
            // 
            this.gridColumn30.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn30.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn30.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn30.Caption = "Số tiền truy thu";
            this.gridColumn30.DisplayFormat.FormatString = "#,##0";
            this.gridColumn30.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn30.FieldName = "COLLECTARREARPRICE";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.FixedWidth = true;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 13;
            this.gridColumn30.Width = 110;
            // 
            // colSTChangeDuytUsers
            // 
            this.colSTChangeDuytUsers.AppearanceHeader.Options.UseTextOptions = true;
            this.colSTChangeDuytUsers.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSTChangeDuytUsers.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSTChangeDuytUsers.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSTChangeDuytUsers.Caption = "DS nhân viên truy thu";
            this.colSTChangeDuytUsers.ColumnEdit = this.btnSTDuytUser;
            this.colSTChangeDuytUsers.FieldName = "DUTYUSERS";
            this.colSTChangeDuytUsers.Name = "colSTChangeDuytUsers";
            this.colSTChangeDuytUsers.OptionsColumn.FixedWidth = true;
            this.colSTChangeDuytUsers.Visible = true;
            this.colSTChangeDuytUsers.VisibleIndex = 14;
            this.colSTChangeDuytUsers.Width = 130;
            // 
            // btnSTDuytUser
            // 
            this.btnSTDuytUser.AutoHeight = false;
            serializableAppearanceObject6.BackColor = System.Drawing.Color.Silver;
            serializableAppearanceObject6.Options.UseBackColor = true;
            this.btnSTDuytUser.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "...", 35, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, "", null, null, true)});
            this.btnSTDuytUser.Name = "btnSTDuytUser";
            this.btnSTDuytUser.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnSTDuytUser.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnSTDuytUser_ButtonClick);
            // 
            // gridColumn32
            // 
            this.gridColumn32.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn32.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn32.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn32.Caption = "Mã phiếu nhập";
            this.gridColumn32.FieldName = "INPUTVOUCHERID";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.FixedWidth = true;
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 15;
            this.gridColumn32.Width = 130;
            // 
            // gridColumn33
            // 
            this.gridColumn33.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn33.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn33.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn33.Caption = "Mã phiếu xuất";
            this.gridColumn33.FieldName = "OUTPUTVOUCHERID";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.FixedWidth = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 16;
            this.gridColumn33.Width = 130;
            // 
            // colSTProcessMessage
            // 
            this.colSTProcessMessage.AppearanceHeader.Options.UseTextOptions = true;
            this.colSTProcessMessage.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSTProcessMessage.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSTProcessMessage.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSTProcessMessage.Caption = "Nội dung xử lý";
            this.colSTProcessMessage.FieldName = "PROCESSMESSAGE";
            this.colSTProcessMessage.Name = "colSTProcessMessage";
            this.colSTProcessMessage.OptionsColumn.AllowEdit = false;
            this.colSTProcessMessage.OptionsColumn.FixedWidth = true;
            this.colSTProcessMessage.Visible = true;
            this.colSTProcessMessage.VisibleIndex = 17;
            this.colSTProcessMessage.Width = 200;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            this.repositoryItemCheckEdit8.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit8.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit9
            // 
            this.repositoryItemCheckEdit9.AutoHeight = false;
            this.repositoryItemCheckEdit9.Name = "repositoryItemCheckEdit9";
            this.repositoryItemCheckEdit9.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit9.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCalcEdit3
            // 
            this.repositoryItemCalcEdit3.AutoHeight = false;
            this.repositoryItemCalcEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit3.DisplayFormat.FormatString = "#,##0";
            this.repositoryItemCalcEdit3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemCalcEdit3.EditFormat.FormatString = "#,##0";
            this.repositoryItemCalcEdit3.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemCalcEdit3.Name = "repositoryItemCalcEdit3";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.txtSTChangeContent);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1022, 80);
            this.panel4.TabIndex = 65;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(14, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(171, 16);
            this.label14.TabIndex = 61;
            this.label14.Text = "Nội dung xuất đổi trạng thái:";
            // 
            // txtSTChangeContent
            // 
            this.txtSTChangeContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSTChangeContent.Location = new System.Drawing.Point(191, 12);
            this.txtSTChangeContent.Name = "txtSTChangeContent";
            this.txtSTChangeContent.Size = new System.Drawing.Size(817, 57);
            this.txtSTChangeContent.TabIndex = 63;
            this.txtSTChangeContent.Text = "";
            // 
            // cmdClose
            // 
            this.cmdClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdClose.Image = ((System.Drawing.Image)(resources.GetObject("cmdClose.Image")));
            this.cmdClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdClose.Location = new System.Drawing.Point(950, 541);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Size = new System.Drawing.Size(92, 25);
            this.cmdClose.TabIndex = 6;
            this.cmdClose.TabStop = false;
            this.cmdClose.Text = "    Đóng lại";
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // cmdUpdate
            // 
            this.cmdUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdUpdate.Enabled = false;
            this.cmdUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.cmdUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdUpdate.Location = new System.Drawing.Point(656, 541);
            this.cmdUpdate.Name = "cmdUpdate";
            this.cmdUpdate.Size = new System.Drawing.Size(92, 25);
            this.cmdUpdate.TabIndex = 4;
            this.cmdUpdate.Text = "     Cập nhật";
            this.cmdUpdate.Click += new System.EventHandler(this.cmdUpdate_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdDelete.Enabled = false;
            this.cmdDelete.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdDelete.Location = new System.Drawing.Point(558, 541);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(92, 25);
            this.cmdDelete.TabIndex = 20;
            this.cmdDelete.TabStop = false;
            this.cmdDelete.Text = "  Hủy";
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // cmdReview
            // 
            this.cmdReview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdReview.Enabled = false;
            this.cmdReview.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.cmdReview.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdReview.Location = new System.Drawing.Point(754, 541);
            this.cmdReview.Name = "cmdReview";
            this.cmdReview.Size = new System.Drawing.Size(92, 25);
            this.cmdReview.TabIndex = 20;
            this.cmdReview.Text = "     Duyệt";
            this.cmdReview.Click += new System.EventHandler(this.cmdReview_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdProcess.Enabled = false;
            this.cmdProcess.Image = global::ERP.Inventory.DUI.Properties.Resources.calculator;
            this.cmdProcess.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdProcess.Location = new System.Drawing.Point(852, 541);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Size = new System.Drawing.Size(92, 25);
            this.cmdProcess.TabIndex = 21;
            this.cmdProcess.Text = "     Xử lý";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // frmResolveQuantity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 574);
            this.Controls.Add(this.cmdProcess);
            this.Controls.Add(this.cmdDelete);
            this.Controls.Add(this.cmdReview);
            this.Controls.Add(this.cmdClose);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.cmdUpdate);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmResolveQuantity";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Yêu cầu xử lý kiểm kê";
            this.Load += new System.EventHandler(this.frmResolveQuantity_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPageInfo.ResumeLayout(false);
            this.tabPageInfo.PerformLayout();
            this.tabPageDetailOutputChange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProductChange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewDataProductChange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrChangeDuytUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActiveItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSystemItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calTotalLookup)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexProductChange)).EndInit();
            this.tabPageDetailInPlus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDataInPlus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInPlus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexInPlus)).EndInit();
            this.tabPageOutMinus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDataOutMinus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOutMinus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOutDuytUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexOutMinus)).EndInit();
            this.tabStChange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDataSTChange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSTChange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSTDuytUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit3)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageInfo;
        private System.Windows.Forms.TabPage tabPageDetailOutputChange;
        private System.Windows.Forms.Button cmdClose;
        private System.Windows.Forms.Button cmdUpdate;
        private System.Windows.Forms.TabPage tabPageDetailInPlus;
        private System.Windows.Forms.TabPage tabPageOutMinus;
        private System.Windows.Forms.TextBox txtInventoryProcessID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCreatedUser;
        private System.Windows.Forms.TextBox txtIsNew;
        private System.Windows.Forms.TextBox txtMainGroupName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtInventoryDate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtInventoryTermName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCreatedDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtStoreName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtInventoryID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private C1.Win.C1FlexGrid.C1FlexGrid flexProductChange;
        private C1.Win.C1FlexGrid.C1FlexGrid flexInPlus;
        private C1.Win.C1FlexGrid.C1FlexGrid flexOutMinus;
        private System.Windows.Forms.Button cmdProcess;
        private System.Windows.Forms.Button cmdReview;
        private System.Windows.Forms.Button cmdDelete;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox txtOutPutContent;
        private System.Windows.Forms.RichTextBox txtInputContent;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox txtPrChangeContent;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtContentDelete;
        private DevExpress.XtraGrid.GridControl grdProductChange;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewDataProductChange;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkActiveItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkSystemItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit calTotalLookup;
        private DevExpress.XtraGrid.Columns.GridColumn colProductChangeProcessID;
        private DevExpress.XtraGrid.Columns.GridColumn colInventoryProcessID;
        private DevExpress.XtraGrid.Columns.GridColumn colProductID_Out;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName_Out;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI_Out;
        private DevExpress.XtraGrid.Columns.GridColumn colOut_ProductStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colOut_ProductStatusName;
        private DevExpress.XtraGrid.Columns.GridColumn colRefSalePrice_Out;
        private DevExpress.XtraGrid.Columns.GridColumn colProductID_In;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName_In;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI_In;
        private DevExpress.XtraGrid.Columns.GridColumn colIn_ProductStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colIn_ProductStatusName;
        private DevExpress.XtraGrid.Columns.GridColumn colRefSalePrice_In;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colProductChangeDetailID;
        private DevExpress.XtraGrid.Columns.GridColumn colCollectArrearPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colDutyUsers;
        private DevExpress.XtraGrid.Columns.GridColumn colUnEventAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colInputVoucherID;
        private DevExpress.XtraGrid.Columns.GridColumn colOutputVoucherID;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraGrid.GridControl grdDataInPlus;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInPlus;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colInputProcessID;
        private DevExpress.XtraGrid.Columns.GridColumn colInventoryProcessID1;
        private DevExpress.XtraGrid.Columns.GridColumn colProductID;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colProductStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colProductStatusName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity1;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colVATPercent;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalMoney;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colInputVoucherID1;
        private DevExpress.XtraGrid.Columns.GridColumn colInputVoucherDetailID1;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraGrid.GridControl grdDataOutMinus;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewOutMinus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colOutMinusCollectArrearPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colOutMinusDUTYUSERS;
        private System.Windows.Forms.TabPage tabStChange;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RichTextBox txtSTChangeContent;
        private DevExpress.XtraGrid.GridControl grdDataSTChange;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSTChange;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn colSTChangeDuytUsers;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnPrChangeDuytUsers;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnSTDuytUser;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnOutDuytUsers;
        private DevExpress.XtraGrid.Columns.GridColumn colStChangeQuantityOut;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn colSTProcessMessage;
        private DevExpress.XtraGrid.Columns.GridColumn colPRProcessMessage;
        private DevExpress.XtraGrid.Columns.GridColumn colOutMinProcessMessage;
    }
}