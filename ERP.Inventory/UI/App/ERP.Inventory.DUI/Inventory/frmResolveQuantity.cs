﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using System.Linq;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmResolveQuantity : Form
    {
        #region Variables

        private string strInventoryProcessID = string.Empty;
        ERP.Inventory.PLC.Inventory.WSInventoryProcess.InventoryProcess objInventoryProcess = new PLC.Inventory.WSInventoryProcess.InventoryProcess();
        private ERP.Inventory.PLC.Inventory.PLCInventoryProcess objPLCInventoryProcess = new PLC.Inventory.PLCInventoryProcess();
        private ERP.Inventory.PLC.Inventory.PLCInventory objPLCInventory = new PLC.Inventory.PLCInventory();
        private ERP.Inventory.PLC.Inventory.PLCInventoryTerm objPLCInventoryTerm = new PLC.Inventory.PLCInventoryTerm();
        private DataTable dtbProductChange = null;
        private DataTable dtbStatusChange = null;
        private DataTable dtbInput = null;
        private DataTable dtbOutput = null;
        private DataTable dtbCollectArrearPrChange = null;
        private DataTable dtbCollectArrearStChange = null;
        private DataTable dtbCollectArrearOutMinus = null;
        private DataTable dtbUsers = null;
        private bool bolIsRight_InventoryProcess_Review = false;
        private bool bolIsRight_InventoryProcess_Process = false;
        private bool bolIsRight_InventoryProcess_Delete = false;
        public bool bolIsLockByClosingDataMonth = false;

        public string InventoryProcessID
        {
            get { return strInventoryProcessID; }
            set { strInventoryProcessID = value; }
        }
        #endregion

        #region Constuctors

        public frmResolveQuantity()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        #endregion


        #region Control Events

        private void frmResolveQuantity_Load(object sender, EventArgs e)
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewDataProductChange);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(gridViewInPlus);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(gridViewOutMinus);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(gridViewSTChange);

            objPLCInventoryProcess.LoadInfo(ref objInventoryProcess, InventoryProcessID);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            txtInventoryProcessID.Text = objInventoryProcess.InventoryProcessID;
            txtInventoryID.Text = objInventoryProcess.InventoryID;
            txtStoreName.Text = Convert.ToString(ERP.MasterData.PLC.MD.PLCStore.GetStoreName(objInventoryProcess.InventoryStoreID)).Trim();
            DateTime dtmServerDate = Library.AppCore.Globals.GetServerDateTime();
            if (objInventoryProcess.CreatedDate != DateTime.MinValue)
            {
                txtCreatedDate.Text = ((DateTime)objInventoryProcess.CreatedDate).ToString("dd/MM/yyyy HH:mm");
                dtmServerDate = (DateTime)objInventoryProcess.CreatedDate;
            }
            ERP.Inventory.PLC.Inventory.PLCInventoryTerm objPLCInventoryTerm = new PLC.Inventory.PLCInventoryTerm();
            DataTable dtbINVTerm = objPLCInventoryTerm.SearchData(new object[] { "@Keywords", string.Empty, "@FromDate", dtmServerDate.AddDays(-100), "@ToDate", dtmServerDate.AddDays(1) });
            DataRow[] rTerm = dtbINVTerm.Select("InventoryTermID = " + objInventoryProcess.InventoryTermID);
            if (rTerm.Length > 0)
            {
                txtInventoryTermName.Text = Convert.ToString(rTerm[0]["InventoryTermName"]).Trim();
            }
            if (objInventoryProcess.InventoryDate != DateTime.MinValue)
            {
                txtInventoryDate.Text = ((DateTime)objInventoryProcess.InventoryDate).ToString("dd/MM/yyyy HH:mm");
            }
            txtMainGroupName.Text = Convert.ToString(ERP.MasterData.PLC.MD.PLCMainGroup.GetMainGroupName(objInventoryProcess.MainGroupID)).Trim();

            ERP.MasterData.SYS.PLC.PLCUser objPLCUser = new MasterData.SYS.PLC.PLCUser();
            ERP.MasterData.SYS.PLC.WSUser.User objUser = objPLCUser.LoadInfo(objInventoryProcess.CreatedUser);
            txtCreatedUser.Text = objInventoryProcess.CreatedUser + " - " + objUser.FullName;

            txtIsNew.Text = (objInventoryProcess.IsNew ? "Mới" : "Cũ");
            if (objInventoryProcess.DeletedReason != null)
            {
                txtContentDelete.Text = Convert.ToString(objInventoryProcess.DeletedReason).Trim();
            }

            txtInputContent.Text = objInventoryProcess.InputContent;
            txtOutPutContent.Text = objInventoryProcess.OutputContent;
            txtPrChangeContent.Text = objInventoryProcess.PrChangeContent;
            txtSTChangeContent.Text = objInventoryProcess.StatusChangeContent;

            DataTable dtbCollectArrear = objPLCInventoryProcess.LoadInventProcessArrear(objInventoryProcess.InventoryProcessID, string.Empty);
            string strDutyUsers = string.Empty;
            //InitFlexProductChange();
            dtbProductChange = objInventoryProcess.tblInventoryProcessPrChange;
            if (dtbCollectArrear.Rows.Count > 0 && dtbProductChange.Rows.Count > 0)
            {
                if (dtbCollectArrearPrChange == null || dtbCollectArrearPrChange.Rows.Count == 0)
                {
                    dtbCollectArrearPrChange = new DataTable();
                    dtbCollectArrearPrChange.Columns.Add("IsSelect", typeof(bool));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("InventoryProcessID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearType", typeof(int));
                    dtbCollectArrearPrChange.Columns.Add("RelateVoucherID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("UserName", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("FullName", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearMoney", typeof(decimal));
                    dtbCollectArrearPrChange.Columns.Add("Note", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("IsDeleted", typeof(bool));
                    dtbCollectArrearPrChange.Columns.Add("DeletedUser", typeof(string));
                }
                foreach (DataRow rProductChange in dtbProductChange.Rows)
                {
                    strDutyUsers = string.Empty;
                    string strProductChangeProcessID = Convert.ToString(rProductChange["ProductChangeProcessID"]);
                    DataRow[] drCollectArrear = dtbCollectArrear.Select("RelateVoucherID = '" + strProductChangeProcessID + "'");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            strDutyUsers += rCollectArrear["UserName"];
                            try
                            {
                                strDutyUsers += "-" + rCollectArrear["FullName"] + "!";
                            }
                            catch { }

                            if (rCollectArrear["InventoryProcessID"] != null && (!Convert.IsDBNull(rCollectArrear["InventoryProcessID"])) && Convert.ToString(rCollectArrear["InventoryProcessID"]) != string.Empty)
                            {
                                DataRow rPrChangeArrear = dtbCollectArrearPrChange.NewRow();
                                rPrChangeArrear.ItemArray = rCollectArrear.ItemArray;
                                dtbCollectArrearPrChange.Rows.Add(rPrChangeArrear);
                            }
                        }
                        strDutyUsers = strDutyUsers.Trim('!');
                    }
                    rProductChange["DutyUsers"] = strDutyUsers;
                }
            }
            //flexProductChange.DataSource = dtbProductChange;
            grdProductChange.DataSource = dtbProductChange;
            //CustomFlexProductChange();

            //InitFlexStatusChange();
            dtbStatusChange = objInventoryProcess.tblInventoryProcessStChange;
            if (dtbCollectArrear.Rows.Count > 0 && dtbStatusChange.Rows.Count > 0)
            {
                if (dtbCollectArrearStChange == null || dtbCollectArrearStChange.Rows.Count == 0)
                {
                    dtbCollectArrearStChange = new DataTable();
                    dtbCollectArrearStChange.Columns.Add("IsSelect", typeof(bool));
                    dtbCollectArrearStChange.Columns.Add("CollectArrearID", typeof(string));
                    dtbCollectArrearStChange.Columns.Add("InventoryProcessID", typeof(string));
                    dtbCollectArrearStChange.Columns.Add("CollectArrearType", typeof(int));
                    dtbCollectArrearStChange.Columns.Add("RelateVoucherID", typeof(string));
                    dtbCollectArrearStChange.Columns.Add("UserName", typeof(string));
                    dtbCollectArrearStChange.Columns.Add("FullName", typeof(string));
                    dtbCollectArrearStChange.Columns.Add("CollectArrearMoney", typeof(decimal));
                    dtbCollectArrearStChange.Columns.Add("Note", typeof(string));
                    dtbCollectArrearStChange.Columns.Add("IsDeleted", typeof(bool));
                    dtbCollectArrearStChange.Columns.Add("DeletedUser", typeof(string));
                }
                foreach (DataRow rProductChange in dtbStatusChange.Rows)
                {
                    strDutyUsers = string.Empty;
                    string strProductChangeProcessID = Convert.ToString(rProductChange["ProductChangeProcessID"]);
                    DataRow[] drCollectArrear = dtbCollectArrear.Select("RelateVoucherID = '" + strProductChangeProcessID + "'");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            strDutyUsers += rCollectArrear["UserName"];
                            try
                            {
                                strDutyUsers += "-" + rCollectArrear["FullName"] + "!";
                            }
                            catch { }

                            if (rCollectArrear["InventoryProcessID"] != null && (!Convert.IsDBNull(rCollectArrear["InventoryProcessID"])) && Convert.ToString(rCollectArrear["InventoryProcessID"]) != string.Empty)
                            {
                                DataRow rPrChangeArrear = dtbCollectArrearStChange.NewRow();
                                rPrChangeArrear.ItemArray = rCollectArrear.ItemArray;
                                dtbCollectArrearStChange.Rows.Add(rPrChangeArrear);
                            }
                        }
                        strDutyUsers = strDutyUsers.Trim('!');
                    }
                    rProductChange["DutyUsers"] = strDutyUsers;
                }
            }
            //flexProductChange.DataSource = dtbProductChange;
            //CustomFlexProductChange();

            //InitFlexInPlus();
            dtbInput = objInventoryProcess.tblInventoryProcessInput;
            if (dtbInput != null && dtbInput.Rows.Count > 0)
            {
                foreach (DataRow rInput in dtbInput.Rows)
                {
                    decimal decTotalMoney = Convert.ToDecimal(rInput["Price"]) *
                            Convert.ToDecimal(Convert.ToDecimal(1) + Convert.ToDecimal(rInput["VAT"]) * Convert.ToDecimal(rInput["VATPercent"]) * Convert.ToDecimal(0.0001)) *
                            Convert.ToDecimal(rInput["Quantity"]);
                    rInput["TotalMoney"] = Math.Round(decTotalMoney);
                }
            }
            //flexInPlus.DataSource = dtbInput;
            grdDataInPlus.DataSource = dtbInput;
            //CustomFlexInPlus();

            //InitFlexOutMinus();
            dtbOutput = objInventoryProcess.tblInventoryProcessOutput;
            if (dtbCollectArrear.Rows.Count > 0 && dtbOutput.Rows.Count > 0)
            {
                if (dtbCollectArrearOutMinus == null || dtbCollectArrearOutMinus.Rows.Count == 0)
                {
                    dtbCollectArrearOutMinus = new DataTable();
                    dtbCollectArrearOutMinus.Columns.Add("IsSelect", typeof(bool));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("InventoryProcessID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearType", typeof(int));
                    dtbCollectArrearOutMinus.Columns.Add("RelateVoucherID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("UserName", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("FullName", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearMoney", typeof(decimal));
                    dtbCollectArrearOutMinus.Columns.Add("Note", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("IsDeleted", typeof(bool));
                    dtbCollectArrearOutMinus.Columns.Add("DeletedUser", typeof(string));
                }
                foreach (DataRow rOutput in dtbOutput.Rows)
                {
                    strDutyUsers = string.Empty;
                    string strOutputProcessID = Convert.ToString(rOutput["OutputProcessID"]);
                    DataRow[] drCollectArrear = dtbCollectArrear.Select("RelateVoucherID = '" + strOutputProcessID + "'");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            strDutyUsers += rCollectArrear["UserName"];
                            try
                            {
                                strDutyUsers += "-" + rCollectArrear["FullName"] + "!";
                            }
                            catch { }

                            if (rCollectArrear["InventoryProcessID"] != null && (!Convert.IsDBNull(rCollectArrear["InventoryProcessID"])) && Convert.ToString(rCollectArrear["InventoryProcessID"]) != string.Empty)
                            {
                                DataRow rOutMinusArrear = dtbCollectArrearOutMinus.NewRow();
                                rOutMinusArrear.ItemArray = rCollectArrear.ItemArray;
                                dtbCollectArrearOutMinus.Rows.Add(rOutMinusArrear);
                            }
                        }
                        strDutyUsers = strDutyUsers.Trim('!');
                    }
                    rOutput["DutyUsers"] = strDutyUsers;
                    decimal decTotalMoneyOutput = Convert.ToDecimal(rOutput["Price"]) *
                            Convert.ToDecimal(Convert.ToDecimal(1) + Convert.ToDecimal(rOutput["VAT"]) * Convert.ToDecimal(rOutput["VATPercent"]) * Convert.ToDecimal(0.0001)) *
                            Convert.ToDecimal(rOutput["Quantity"]);
                    rOutput["TotalMoney"] = Math.Round(decTotalMoneyOutput);
                }
            }
            else
            {
                foreach (DataRow rOutput in dtbOutput.Rows)
                {
                    decimal decTotalMoneyOutput = Convert.ToDecimal(rOutput["Price"]) *
                            Convert.ToDecimal(Convert.ToDecimal(1) + Convert.ToDecimal(rOutput["VAT"]) * Convert.ToDecimal(rOutput["VATPercent"]) * Convert.ToDecimal(0.0001)) *
                            Convert.ToDecimal(rOutput["Quantity"]);
                    rOutput["TotalMoney"] = Math.Round(decTotalMoneyOutput);
                }
            }
            //flexOutMinus.DataSource = dtbOutput;
            grdDataOutMinus.DataSource = dtbOutput;
            //CustomFlexOutMinus();



            dtbStatusChange = objInventoryProcess.tblInventoryProcessStChange;
            grdDataSTChange.DataSource = dtbStatusChange;

            dtbUsers = Library.AppCore.DataSource.GetDataSource.GetUser().Copy();
            dtbUsers.Columns["FULLNAME"].SetOrdinal(1);

            try
            {
                bolIsRight_InventoryProcess_Review = Library.AppCore.SystemConfig.objSessionUser.IsPermission("INV_InventoryProcess_Review");
                bolIsRight_InventoryProcess_Process = Library.AppCore.SystemConfig.objSessionUser.IsPermission("INV_InventoryProcess_Process");
                bolIsRight_InventoryProcess_Delete = Library.AppCore.SystemConfig.objSessionUser.IsPermission("INV_InventoryProcess_Delete");
            }
            catch { }

            if (objInventoryProcess.IsProcess || objInventoryProcess.IsReviewed)
            {
                colOutMinProcessMessage.Visible = colPRProcessMessage.Visible = colSTProcessMessage.Visible = true;

                if (objInventoryProcess.IsProcess)
                {
                    cmdUpdate.Enabled = false;
                    cmdReview.Enabled = false;
                    cmdProcess.Enabled = false;
                    cmdDelete.Enabled = false;
                }
                else
                {
                    cmdUpdate.Enabled = false;
                    cmdReview.Enabled = false;
                    cmdProcess.Enabled = bolIsRight_InventoryProcess_Process;
                    cmdDelete.Enabled = bolIsRight_InventoryProcess_Delete;
                }
            }
            else
            {
                colOutMinProcessMessage.Visible =  colPRProcessMessage.Visible = colSTProcessMessage.Visible = false;
                cmdUpdate.Enabled = true;
                cmdProcess.Enabled = false;
                cmdDelete.Enabled = bolIsRight_InventoryProcess_Delete;

                if ((dtbProductChange.Rows.Count > 0 && dtbProductChange.Select("CollectArrearPrice > 0 AND (DutyUsers IS NULL OR TRIM(DutyUsers)='')").Length > 0) || (dtbOutput.Rows.Count > 0 && dtbOutput.Select("CollectArrearPrice > 0 AND (DutyUsers IS NULL OR TRIM(DutyUsers)='')").Length > 0))
                {
                    cmdReview.Enabled = false;
                }
                else
                {
                    cmdReview.Enabled = bolIsRight_InventoryProcess_Review;
                }
            }
            if (bolIsLockByClosingDataMonth || objInventoryProcess.IsDeleted)
            {
                cmdUpdate.Enabled = false;
                cmdReview.Enabled = false;
                cmdProcess.Enabled = false;
                cmdDelete.Enabled = false;
                txtContentDelete.ReadOnly = true;
            }
        }

        private void InitFlexProductChange()
        {
            dtbProductChange = new DataTable();
            dtbProductChange.Columns.Add("ProductChangeProcessID", typeof(string));
            dtbProductChange.Columns.Add("InventoryProcessID", typeof(string));
            dtbProductChange.Columns.Add("ProductID_Out", typeof(string));
            dtbProductChange.Columns.Add("ProductName_Out", typeof(string));
            dtbProductChange.Columns.Add("IMEI_Out", typeof(string));
            dtbProductChange.Columns.Add("Out_ProductStatusID", typeof(int));
            dtbProductChange.Columns.Add("Out_ProductStatusName", typeof(string));
            dtbProductChange.Columns.Add("RefSalePrice_Out", typeof(decimal));
            dtbProductChange.Columns.Add("ProductID_In", typeof(string));
            dtbProductChange.Columns.Add("ProductName_In", typeof(string));
            dtbProductChange.Columns.Add("IMEI_In", typeof(string));
            dtbProductChange.Columns.Add("In_ProductStatusID", typeof(int));
            dtbProductChange.Columns.Add("In_ProductStatusName", typeof(string));
            dtbProductChange.Columns.Add("RefSalePrice_In", typeof(decimal));
            dtbProductChange.Columns.Add("Quantity", typeof(int));
            dtbProductChange.Columns.Add("CreatedDate", typeof(DateTime));
            dtbProductChange.Columns.Add("ProductChangeDetailID", typeof(string));
            dtbProductChange.Columns.Add("CollectArrearPrice", typeof(decimal));
            dtbProductChange.Columns.Add("DutyUsers", typeof(string));
        }

        private void InitFlexInPlus()
        {
            dtbInput = new DataTable();
            dtbInput.Columns.Add("InputProcessID", typeof(string));
            dtbInput.Columns.Add("InventoryProcessID", typeof(string));
            dtbInput.Columns.Add("ProductID", typeof(string));
            dtbInput.Columns.Add("ProductName", typeof(string));
            dtbInput.Columns.Add("IMEI", typeof(string));
            dtbInput.Columns.Add("ProductStatusID", typeof(int));
            dtbInput.Columns.Add("ProductStatusName", typeof(string));
            dtbInput.Columns.Add("Price", typeof(decimal));
            dtbInput.Columns.Add("Quantity", typeof(int));
            dtbInput.Columns.Add("VAT", typeof(int));
            dtbInput.Columns.Add("VATPercent", typeof(int));
            dtbInput.Columns.Add("TotalMoney", typeof(decimal));
            dtbInput.Columns.Add("CreatedDate", typeof(DateTime));
            dtbInput.Columns.Add("InputVoucherID", typeof(string));
            dtbInput.Columns.Add("InputVoucherDetailID", typeof(string));
        }

        private void InitFlexOutMinus()
        {
            dtbOutput = new DataTable();
            dtbOutput.Columns.Add("OutputProcessID", typeof(string));
            dtbOutput.Columns.Add("InventoryProcessID", typeof(string));
            dtbOutput.Columns.Add("ProductID", typeof(string));
            dtbOutput.Columns.Add("ProductName", typeof(string));
            dtbOutput.Columns.Add("IMEI", typeof(string));
            dtbOutput.Columns.Add("ProductStatusID", typeof(int));
            dtbOutput.Columns.Add("ProductStatusName", typeof(string));
            dtbOutput.Columns.Add("Price", typeof(decimal));
            dtbOutput.Columns.Add("Quantity", typeof(int));
            dtbOutput.Columns.Add("VAT", typeof(int));
            dtbOutput.Columns.Add("VATPercent", typeof(int));
            dtbOutput.Columns.Add("TotalMoney", typeof(decimal));
            dtbOutput.Columns.Add("CreatedDate", typeof(DateTime));
            dtbOutput.Columns.Add("OutputVoucherID", typeof(string));
            dtbOutput.Columns.Add("OutputVoucherDetailID", typeof(string));
            dtbOutput.Columns.Add("CollectArrearPrice", typeof(decimal));
            dtbOutput.Columns.Add("DutyUsers", typeof(string));
        }

        private void InitFlexStatusChange()
        {
            dtbStatusChange = new DataTable();
            dtbStatusChange.Columns.Add("ProductChangeProcessID", typeof(string));
            dtbStatusChange.Columns.Add("InventoryProcessID", typeof(string));
            dtbStatusChange.Columns.Add("ProductID_Out", typeof(string));
            dtbStatusChange.Columns.Add("ProductName_Out", typeof(string));
            dtbStatusChange.Columns.Add("IMEI_Out", typeof(string));
            dtbStatusChange.Columns.Add("Out_ProductStatusID", typeof(int));
            dtbStatusChange.Columns.Add("Out_ProductStatusName", typeof(string));
            dtbStatusChange.Columns.Add("RefSalePrice_Out", typeof(decimal));
            dtbStatusChange.Columns.Add("ProductID_In", typeof(string));
            dtbStatusChange.Columns.Add("ProductName_In", typeof(string));
            dtbStatusChange.Columns.Add("IMEI_In", typeof(string));
            dtbStatusChange.Columns.Add("In_ProductStatusID", typeof(int));
            dtbStatusChange.Columns.Add("In_ProductStatusName", typeof(string));
            dtbStatusChange.Columns.Add("RefSalePrice_In", typeof(decimal));
            dtbStatusChange.Columns.Add("Quantity", typeof(int));
            dtbStatusChange.Columns.Add("CreatedDate", typeof(DateTime));
            dtbStatusChange.Columns.Add("ProductChangeDetailID", typeof(string));
            dtbStatusChange.Columns.Add("CollectArrearPrice", typeof(decimal));
            dtbStatusChange.Columns.Add("DutyUsers", typeof(string));
        }

        private void CustomFlexProductChange()
        {
            flexProductChange.Rows.Fixed = 1;
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexProductChange, false, "ProductChangeProcessID,InventoryProcessID,CreatedDate,ProductChangeDetailID");

            flexProductChange.Cols["ProductID_Out"].Caption = "Mã sản phẩm";
            flexProductChange.Cols["ProductName_Out"].Caption = "Tên sản phẩm";
            flexProductChange.Cols["IMEI_Out"].Caption = "IMEI";
            flexProductChange.Cols["RefSalePrice_Out"].Caption = "Đơn giá bán(chưa có VAT)";
            flexProductChange.Cols["ProductID_In"].Caption = "Mã sản phẩm";
            flexProductChange.Cols["ProductName_In"].Caption = "Tên sản phẩm";
            flexProductChange.Cols["IMEI_In"].Caption = "IMEI";
            flexProductChange.Cols["RefSalePrice_In"].Caption = "Đơn giá bán(chưa có VAT)";
            flexProductChange.Cols["Quantity"].Caption = "Số lượng";
            flexProductChange.Cols["UnEventAmount"].Caption = "Số tiền chênh lệch";
            flexProductChange.Cols["CollectArrearPrice"].Caption = "Số tiền truy thu";
            flexProductChange.Cols["DutyUsers"].Caption = "DS nhân viên truy thu";
            flexProductChange.Cols["DutyUsers"].ComboList = "...";
            flexProductChange.Cols["InputVoucherID"].Caption = "Mã phiếu nhập";
            flexProductChange.Cols["OutputVoucherID"].Caption = "Mã phiếu xuất";

            flexProductChange.Cols["Quantity"].Format = "##0";
            flexProductChange.Cols["RefSalePrice_Out"].Format = "#,##0";
            flexProductChange.Cols["RefSalePrice_In"].Format = "#,##0";
            flexProductChange.Cols["UnEventAmount"].Format = "#,##0";
            flexProductChange.Cols["CollectArrearPrice"].Format = "#,##0";

            flexProductChange.Cols["ProductID_Out"].Width = 130;
            flexProductChange.Cols["ProductName_Out"].Width = 110;
            flexProductChange.Cols["IMEI_Out"].Width = 130;
            flexProductChange.Cols["RefSalePrice_Out"].Width = 80;
            flexProductChange.Cols["ProductID_In"].Width = 130;
            flexProductChange.Cols["ProductName_In"].Width = 110;
            flexProductChange.Cols["IMEI_In"].Width = 130;
            flexProductChange.Cols["RefSalePrice_In"].Width = 80;
            flexProductChange.Cols["Quantity"].Width = 80;
            flexProductChange.Cols["CollectArrearPrice"].Width = 100;
            flexProductChange.Cols["DutyUsers"].Width = 180;
            flexProductChange.Cols["InputVoucherID"].Width = 140;
            flexProductChange.Cols["OutputVoucherID"].Width = 140;

            for (int i = 1; i < flexProductChange.Cols.Count; i++)
            {
                flexProductChange.Cols[i].AllowEditing = false;
            }

            flexProductChange.Cols["CollectArrearPrice"].AllowEditing = true;
            flexProductChange.Cols["DutyUsers"].AllowEditing = true;

            C1.Win.C1FlexGrid.CellStyle style = flexProductChange.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = flexProductChange.GetCellRange(0, 0, 0, flexProductChange.Cols.Count - 1);
            range.Style = style;
            flexProductChange.Rows[0].Height = 50;

            Library.AppCore.LoadControls.C1FlexGridObject.AddFormSearchProduct(flexProductChange, flexProductChange.Cols["ProductID_Out"].Index, flexProductChange.Cols["ProductName_Out"].Index, flexProductChange.Cols["IMEI_Out"].Index);
        }

        private void CustomFlexInPlus()
        {
            flexInPlus.Rows.Fixed = 1;
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexInPlus, false, "InputProcessID,InventoryProcessID,CreatedDate,InputVoucherDetailID");

            flexInPlus.Cols["ProductID"].Caption = "Mã sản phẩm";
            flexInPlus.Cols["ProductName"].Caption = "Tên sản phẩm";
            flexInPlus.Cols["IMEI"].Caption = "IMEI";
            flexInPlus.Cols["Price"].Caption = "Đơn giá(chưa có VAT)";
            flexInPlus.Cols["Quantity"].Caption = "Số lượng";
            flexInPlus.Cols["VAT"].Caption = "VAT";
            flexInPlus.Cols["VATPercent"].Caption = "%VAT nộp";
            flexInPlus.Cols["TotalMoney"].Caption = "Tổng tiền";
            flexInPlus.Cols["InputVoucherID"].Caption = "Mã phiếu nhập";

            flexInPlus.Cols["Quantity"].Format = "##0";
            flexInPlus.Cols["VAT"].Format = "##0";
            flexInPlus.Cols["VATPercent"].Format = "##0";
            flexInPlus.Cols["Price"].Format = "#,##0";
            flexInPlus.Cols["TotalMoney"].Format = "#,##0";

            flexInPlus.Cols["ProductID"].Width = 130;
            flexInPlus.Cols["ProductName"].Width = 110;
            flexInPlus.Cols["IMEI"].Width = 130;
            flexInPlus.Cols["Price"].Width = 100;
            flexInPlus.Cols["Quantity"].Width = 80;
            flexInPlus.Cols["VAT"].Width = 80;
            flexInPlus.Cols["VATPercent"].Width = 80;
            flexInPlus.Cols["TotalMoney"].Width = 100;
            flexInPlus.Cols["InputVoucherID"].Width = 140;

            for (int i = 1; i < flexInPlus.Cols.Count; i++)
            {
                flexInPlus.Cols[i].AllowEditing = false;
            }

            flexInPlus.Cols["Price"].AllowEditing = true;
            flexInPlus.Cols["VAT"].AllowEditing = true;
            flexInPlus.Cols["VATPercent"].AllowEditing = true;

            C1.Win.C1FlexGrid.CellStyle style = flexInPlus.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = flexInPlus.GetCellRange(0, 0, 0, flexInPlus.Cols.Count - 1);
            range.Style = style;
            flexInPlus.Rows[0].Height = 50;

            Library.AppCore.LoadControls.C1FlexGridObject.AddFormSearchProduct(flexInPlus, flexInPlus.Cols["ProductID"].Index, flexInPlus.Cols["ProductName"].Index, flexInPlus.Cols["IMEI"].Index);
        }

        private void CustomFlexOutMinus()
        {
            flexOutMinus.Rows.Fixed = 1;
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexOutMinus, false, "OutputProcessID,InventoryProcessID,CreatedDate,OutputVoucherDetailID");

            flexOutMinus.Cols["ProductID"].Caption = "Mã sản phẩm";
            flexOutMinus.Cols["ProductName"].Caption = "Tên sản phẩm";
            flexOutMinus.Cols["IMEI"].Caption = "IMEI";
            flexOutMinus.Cols["Price"].Caption = "Đơn giá(chưa có VAT)";
            flexOutMinus.Cols["Quantity"].Caption = "Số lượng";
            flexOutMinus.Cols["VAT"].Caption = "VAT";
            flexOutMinus.Cols["VATPercent"].Caption = "%VAT nộp";
            flexOutMinus.Cols["TotalMoney"].Caption = "Tổng tiền";
            flexOutMinus.Cols["CollectArrearPrice"].Caption = "Số tiền truy thu";
            flexOutMinus.Cols["DutyUsers"].Caption = "DS nhân viên truy thu";
            flexOutMinus.Cols["DutyUsers"].ComboList = "...";
            flexOutMinus.Cols["OutputVoucherID"].Caption = "Mã phiếu xuất";

            flexOutMinus.Cols["Quantity"].Format = "##0";
            flexOutMinus.Cols["VAT"].Format = "##0";
            flexOutMinus.Cols["VATPercent"].Format = "##0";
            flexOutMinus.Cols["Price"].Format = "#,##0";
            flexOutMinus.Cols["TotalMoney"].Format = "#,##0";
            flexOutMinus.Cols["CollectArrearPrice"].Format = "#,##0";

            flexOutMinus.Cols["ProductID"].Width = 130;
            flexOutMinus.Cols["ProductName"].Width = 110;
            flexOutMinus.Cols["IMEI"].Width = 130;
            flexOutMinus.Cols["Price"].Width = 100;
            flexOutMinus.Cols["Quantity"].Width = 80;
            flexOutMinus.Cols["VAT"].Width = 80;
            flexOutMinus.Cols["VATPercent"].Width = 80;
            flexOutMinus.Cols["TotalMoney"].Width = 100;
            flexOutMinus.Cols["CollectArrearPrice"].Width = 100;
            flexOutMinus.Cols["DutyUsers"].Width = 180;
            flexOutMinus.Cols["OutputVoucherID"].Width = 140;

            for (int i = 1; i < flexOutMinus.Cols.Count; i++)
            {
                flexOutMinus.Cols[i].AllowEditing = false;
            }

            flexOutMinus.Cols["Price"].AllowEditing = true;
            flexOutMinus.Cols["VAT"].AllowEditing = true;
            flexOutMinus.Cols["VATPercent"].AllowEditing = true;
            flexOutMinus.Cols["CollectArrearPrice"].AllowEditing = true;
            flexOutMinus.Cols["DutyUsers"].AllowEditing = true;

            C1.Win.C1FlexGrid.CellStyle style = flexOutMinus.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = flexOutMinus.GetCellRange(0, 0, 0, flexOutMinus.Cols.Count - 1);
            range.Style = style;
            flexOutMinus.Rows[0].Height = 50;

            Library.AppCore.LoadControls.C1FlexGridObject.AddFormSearchProduct(flexOutMinus, flexOutMinus.Cols["ProductID"].Index, flexOutMinus.Cols["ProductName"].Index, flexOutMinus.Cols["IMEI"].Index);
        }

        #endregion

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool CheckPrChange()
        {
            if (grdProductChange.DataSource != null)
            {
                foreach (DataRow row in ((DataTable)grdProductChange.DataSource).Rows)
                {
                    decimal decCollectArrearPrice = Convert.ToDecimal(row["CollectArrearPrice"]);
                    if (decCollectArrearPrice > 0)
                    {
                        string strDutyUsers = string.Empty;
                        try
                        {
                            strDutyUsers = Convert.ToString(row["DutyUsers"]);
                        }
                        catch { }

                        if (strDutyUsers == string.Empty)
                        {
                            MessageBox.Show(this, "Có ít nhất 1 trạng thái trong xuất đổi sản phẩm có truy thu tiền nhưng chưa nhập nhân viên truy thu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            grdViewDataProductChange.FocusedColumn = colDutyUsers;
                            grdViewDataProductChange.SetFocusedRowCellValue("DutyUsers", row["DutyUsers"]);
                            return false;
                        }
                        if (dtbCollectArrearPrChange.Rows.Count > 0)
                        {
                            string strRelateVoucherID = Convert.ToString(row["ProductChangeProcessID"]);
                            decimal decTotal = Convert.ToDecimal(dtbCollectArrearPrChange.Compute("Sum(CollectArrearMoney)", "RelateVoucherID = '" + strRelateVoucherID + "' AND IsDeleted = 0 "));
                            if (Math.Round(decTotal) != Math.Round(decCollectArrearPrice))
                            {
                                MessageBox.Show(this, "Vui lòng nhập lại nhân viên truy thu vì số tiền truy thu không bằng với số tiền của nhân viên truy thu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                grdViewDataProductChange.FocusedColumn = colDutyUsers;
                                grdViewDataProductChange.SetFocusedRowCellValue("CollectArrearPrice", row["CollectArrearPrice"]);
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }
        private bool CheckStChange()
        {
            if (grdDataSTChange.DataSource != null)
            {
                foreach (DataRow row in ((DataTable)grdDataSTChange.DataSource).Rows)
                {
                    decimal decCollectArrearPrice = Convert.ToDecimal(row["CollectArrearPrice"]);
                    if (decCollectArrearPrice > 0)
                    {
                        string strDutyUsers = string.Empty;
                        try
                        {
                            strDutyUsers = Convert.ToString(row["DutyUsers"]);
                        }
                        catch { }

                        if (strDutyUsers == string.Empty)
                        {
                            MessageBox.Show(this, "Có ít nhất 1 trạng thái trong xuất đổi trạng thái có truy thu tiền nhưng chưa nhập nhân viên truy thu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            gridViewSTChange.FocusedColumn = colSTChangeDuytUsers;
                            gridViewSTChange.SetFocusedRowCellValue("DutyUsers", row["DutyUsers"]);
                            return false;
                        }
                        if (dtbCollectArrearStChange.Rows.Count > 0)
                        {
                            string strRelateVoucherID = Convert.ToString(row["ProductChangeProcessID"]);
                            decimal decTotal = Convert.ToDecimal(dtbCollectArrearStChange.Compute("Sum(CollectArrearMoney)", "RelateVoucherID = '" + strRelateVoucherID + "' AND IsDeleted = 0 "));
                            if (Math.Round(decTotal) != Math.Round(decCollectArrearPrice))
                            {
                                MessageBox.Show(this, "Vui lòng nhập lại nhân viên truy thu vì số tiền truy thu không bằng với số tiền của nhân viên truy thu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                gridViewSTChange.FocusedColumn = colSTChangeDuytUsers;
                                gridViewSTChange.SetFocusedRowCellValue("CollectArrearPrice", row["CollectArrearPrice"]);
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }
        private bool CheckOutMinus()
        {
            DataTable dtbOutMinus = (DataTable)grdDataOutMinus.DataSource;
            if (dtbOutMinus != null)
            {
                for (int i = 0; i < dtbOutMinus.Rows.Count; i++)
                {
                    DataRow row = dtbOutMinus.Rows[i];
                    decimal decCollectArrearPrice = 0;
                    decimal.TryParse((row["CollectArrearPrice"]).ToString(), out decCollectArrearPrice);
                    if (decCollectArrearPrice > 0)
                    {
                        string strDutyUsers = string.Empty;
                        try
                        {
                            strDutyUsers = Convert.ToString(row["DutyUsers"]);
                        }
                        catch { }
                        if (strDutyUsers == string.Empty)
                        {
                            MessageBox.Show(this, "Có ít nhất 1 sản phẩm trong xuất thiếu có truy thu tiền nhưng chưa nhập nhân viên truy thu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            gridViewOutMinus.FocusedColumn = colOutMinusDUTYUSERS;
                            gridViewOutMinus.SetFocusedRowCellValue("DUTYUSERS", row["DutyUsers"]);
                            return false;
                        }
                        if (dtbCollectArrearOutMinus.Rows.Count > 0)
                        {
                            string strRelateVoucherID = Convert.ToString(row["OutputProcessID"]);
                            decimal decTotal = 0;
                            decimal.TryParse(dtbCollectArrearOutMinus.Compute("Sum(CollectArrearMoney)", "RelateVoucherID = '" + strRelateVoucherID + "' AND IsDeleted = 0 ").ToString(), out decTotal);
                            if (Math.Round(decTotal) != Math.Round(decCollectArrearPrice))
                            {
                                MessageBox.Show(this, "Vui lòng nhập lại nhân viên truy thu vì số tiền truy thu không bằng với số tiền của nhân viên truy thu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                gridViewOutMinus.FocusedColumn = colOutMinusCollectArrearPrice;
                                gridViewOutMinus.SetFocusedRowCellValue("COLLECTARREARPRICE", row["CollectArrearPrice"]);
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }
        private bool CheckContent()
        {
            if (txtPrChangeContent.Text.Length > 2000)
            {
                MessageBox.Show(this, "Nội dung xuất đổi hàng chỉ cho phép được 2000 ký tự!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (txtInputContent.Text.Length > 2000)
            {
                MessageBox.Show(this, "Nội dung nhập thừa chỉ cho phép được 2000 ký tự!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (txtOutPutContent.Text.Length > 2000)
            {
                MessageBox.Show(this, "Nội dung xuất thiếu chỉ cho phép được 2000 ký tự!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (txtSTChangeContent.Text.Length > 2000)
            {
                MessageBox.Show(this, "Nội dung xuất đổi trạng thái chỉ cho phép được 2000 ký tự!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }
        private void cmdUpdate_Click(object sender, EventArgs e)
        {
            if (!CheckPrChange())
                return;
            if (!CheckStChange())
                return;
            if (!CheckOutMinus())
                return;
            if (!CheckContent())
                return;
            #region Update
            cmdUpdate.Enabled = false;
            try
            {
                DataTable dtbCollectArrear = null;
                if (dtbCollectArrearPrChange != null && dtbCollectArrearPrChange.Rows.Count > 0)
                {
                    dtbCollectArrear = dtbCollectArrearPrChange.Clone();
                }
                if (dtbCollectArrear == null)
                {
                    if (dtbCollectArrearOutMinus != null && dtbCollectArrearOutMinus.Rows.Count > 0)
                    {
                        dtbCollectArrear = dtbCollectArrearOutMinus.Clone();
                    }
                    if (dtbCollectArrear == null)
                    {
                        dtbCollectArrear = new DataTable();
                        dtbCollectArrear.Columns.Add("IsSelect", typeof(bool));
                        dtbCollectArrear.Columns.Add("CollectArrearID", typeof(string));
                        dtbCollectArrear.Columns.Add("InventoryProcessID", typeof(string));
                        dtbCollectArrear.Columns.Add("CollectArrearType", typeof(int));
                        dtbCollectArrear.Columns.Add("RelateVoucherID", typeof(string));
                        dtbCollectArrear.Columns.Add("UserName", typeof(string));
                        dtbCollectArrear.Columns.Add("FullName", typeof(string));
                        dtbCollectArrear.Columns.Add("CollectArrearMoney", typeof(decimal));
                        dtbCollectArrear.Columns.Add("Note", typeof(string));
                        dtbCollectArrear.Columns.Add("IsDeleted", typeof(bool));
                        dtbCollectArrear.Columns.Add("DeletedUser", typeof(string));
                    }
                }
                DataRow[] drCollectArrear;
                if (dtbCollectArrearPrChange != null && dtbCollectArrearPrChange.Rows.Count > 0)
                {
                    drCollectArrear = dtbCollectArrearPrChange.Select("IsDeleted = 1");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            if (!Convert.IsDBNull(rCollectArrear["CollectArrearID"]) && Convert.ToString(rCollectArrear["CollectArrearID"]) != string.Empty)
                            {
                                DataRow rNewArrear = dtbCollectArrear.NewRow();
                                rNewArrear.ItemArray = rCollectArrear.ItemArray;
                                dtbCollectArrear.Rows.Add(rNewArrear);
                            }
                        }
                    }
                    drCollectArrear = dtbCollectArrearPrChange.Select("IsDeleted = 0");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            DataRow rNewArrear = dtbCollectArrear.NewRow();
                            rNewArrear.ItemArray = rCollectArrear.ItemArray;
                            dtbCollectArrear.Rows.Add(rNewArrear);
                        }
                    }
                }

                if (dtbCollectArrearStChange != null && dtbCollectArrearStChange.Rows.Count > 0)
                {
                    drCollectArrear = dtbCollectArrearStChange.Select("IsDeleted = 1");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            if (!Convert.IsDBNull(rCollectArrear["CollectArrearID"]) && Convert.ToString(rCollectArrear["CollectArrearID"]) != string.Empty)
                            {
                                DataRow rNewArrear = dtbCollectArrear.NewRow();
                                rNewArrear.ItemArray = rCollectArrear.ItemArray;
                                dtbCollectArrear.Rows.Add(rNewArrear);
                            }
                        }
                    }
                    drCollectArrear = dtbCollectArrearStChange.Select("IsDeleted = 0");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            DataRow rNewArrear = dtbCollectArrear.NewRow();
                            rNewArrear.ItemArray = rCollectArrear.ItemArray;
                            dtbCollectArrear.Rows.Add(rNewArrear);
                        }
                    }
                }
                if (dtbCollectArrearOutMinus != null && dtbCollectArrearOutMinus.Rows.Count > 0)
                {
                    drCollectArrear = dtbCollectArrearOutMinus.Select("IsDeleted = 1");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            if (!Convert.IsDBNull(rCollectArrear["CollectArrearID"]) && Convert.ToString(rCollectArrear["CollectArrearID"]) != string.Empty)
                            {
                                DataRow rNewArrear = dtbCollectArrear.NewRow();
                                rNewArrear.ItemArray = rCollectArrear.ItemArray;
                                dtbCollectArrear.Rows.Add(rNewArrear);
                            }
                        }
                    }
                    drCollectArrear = dtbCollectArrearOutMinus.Select("IsDeleted = 0");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            DataRow rNewArrear = dtbCollectArrear.NewRow();
                            rNewArrear.ItemArray = rCollectArrear.ItemArray;
                            dtbCollectArrear.Rows.Add(rNewArrear);
                        }
                    }
                }
                if (dtbProductChange.Rows.Count == 0)
                {
                    //DataRow rNewProductChange = dtbProductChange.NewRow();
                    //rNewProductChange["ProductChangeProcessID"] = string.Empty;
                    //rNewProductChange["InventoryProcessID"] = string.Empty;
                    //rNewProductChange["ProductID_Out"] = string.Empty;
                    //rNewProductChange["ProductName_Out"] = string.Empty;
                    //rNewProductChange["IMEI_Out"] = string.Empty;
                    //rNewProductChange["RefSalePrice_Out"] = 0;
                    //rNewProductChange["ProductID_In"] = string.Empty;
                    //rNewProductChange["ProductName_In"] = string.Empty;
                    //rNewProductChange["IMEI_In"] = string.Empty;
                    //rNewProductChange["RefSalePrice_In"] = 0;
                    //rNewProductChange["Quantity"] = 0;
                    //rNewProductChange["CreatedDate"] = DateTime.MinValue;
                    //rNewProductChange["ProductChangeDetailID"] = string.Empty;
                    //rNewProductChange["CollectArrearPrice"] = 0;
                    //rNewProductChange["DutyUsers"] = string.Empty;
                    //dtbProductChange.Rows.Add(rNewProductChange);
                }

                if (dtbStatusChange.Rows.Count == 0)
                {
                    //DataRow rNewStatusChange = dtbStatusChange.NewRow();
                    //rNewStatusChange["PRODUCTCHANGEPROCESSID"] = string.Empty;
                    //rNewStatusChange["INVENTORYPROCESSID"] = string.Empty;
                    //rNewStatusChange["PRODUCTID_OUT"] = string.Empty;
                    //rNewStatusChange["PRODUCTNAME_OUT"] = string.Empty;
                    //rNewStatusChange["IMEI_OUT"] = string.Empty;
                    //rNewStatusChange["REFSALEPRICE_OUT"] = 0;
                    //rNewStatusChange["PRODUCTID_IN"] = string.Empty;
                    //rNewStatusChange["PRODUCTNAME_IN"] = string.Empty;
                    //rNewStatusChange["IMEI_IN"] = string.Empty;
                    //rNewStatusChange["REFSALEPRICE_IN"] = 0;
                    //rNewStatusChange["QUANTITY"] = 0;
                    //rNewStatusChange["CREATEDDATE"] = DateTime.MinValue;
                    //rNewStatusChange["PRODUCTCHANGEDETAILID"] = string.Empty;
                    //rNewStatusChange["COLLECTARREARPRICE"] = 0;
                    //rNewStatusChange["DUTYUSERS"] = string.Empty;
                    //dtbStatusChange.Rows.Add(rNewStatusChange);
                }
                DataTable dtbResultInput = new DataTable();
                if (dtbInput != null && dtbInput.Rows.Count > 0)
                {
                    dtbResultInput = dtbInput.Clone();
                    dtbResultInput.TableName = "ResultInput";
                    DataRow[] drInput = dtbInput.Select("ProductID <> ''");
                    if (drInput.Length > 0)
                    {
                        foreach (DataRow rInput in drInput)
                        {
                            DataRow rNewInput = dtbResultInput.NewRow();
                            rNewInput.ItemArray = rInput.ItemArray;
                            dtbResultInput.Rows.Add(rNewInput);
                        }
                    }
                }
                if (dtbResultInput.Rows.Count == 0)
                {
                    dtbResultInput = dtbInput.Clone();
                    dtbResultInput.TableName = "ResultInput";
                    DataRow rNewResultInput = dtbResultInput.NewRow();
                    rNewResultInput["InputProcessID"] = string.Empty;
                    rNewResultInput["InventoryProcessID"] = string.Empty;
                    rNewResultInput["ProductID"] = string.Empty;
                    rNewResultInput["ProductName"] = string.Empty;
                    rNewResultInput["IMEI"] = string.Empty;
                    rNewResultInput["Price"] = 0;
                    rNewResultInput["Quantity"] = 0;
                    rNewResultInput["VAT"] = 0;
                    rNewResultInput["VATPercent"] = 0;
                    rNewResultInput["TotalMoney"] = 0;
                    rNewResultInput["CreatedDate"] = DateTime.MinValue;
                    rNewResultInput["InputVoucherID"] = string.Empty;
                    rNewResultInput["InputVoucherDetailID"] = string.Empty;
                    dtbResultInput.Rows.Add(rNewResultInput);
                }
                DataTable dtbResultOutput = new DataTable();
                if (dtbOutput != null && dtbOutput.Rows.Count > 0)
                {
                    dtbResultOutput = dtbOutput.Clone();
                    dtbResultOutput.TableName = "ResultOutput";
                    DataRow[] drOutput = dtbOutput.Select("ProductID <> ''");
                    if (drOutput.Length > 0)
                    {
                        foreach (DataRow rOutput in drOutput)
                        {
                            DataRow rNewOutput = dtbResultOutput.NewRow();
                            rNewOutput.ItemArray = rOutput.ItemArray;
                            decimal decCollectArrearPrice = Math.Round(Convert.ToDecimal(rOutput["CollectArrearPrice"]) / (1 + Convert.ToInt32(rOutput["VAT"]) * Convert.ToInt32(rOutput["VATPercent"]) * Convert.ToDecimal(0.0001)), 4);
                            rNewOutput["CollectArrearPrice"] = decCollectArrearPrice;
                            dtbResultOutput.Rows.Add(rNewOutput);
                        }
                    }
                }
                if (dtbResultOutput.Rows.Count == 0)
                {
                    dtbResultOutput = dtbOutput.Clone();
                    dtbResultOutput.TableName = "ResultOutput";
                    DataRow rNewResultOutput = dtbResultOutput.NewRow();
                    rNewResultOutput["OutputProcessID"] = string.Empty;
                    rNewResultOutput["InventoryProcessID"] = string.Empty;
                    rNewResultOutput["ProductID"] = string.Empty;
                    rNewResultOutput["ProductName"] = string.Empty;
                    rNewResultOutput["IMEI"] = string.Empty;
                    rNewResultOutput["Price"] = 0;
                    rNewResultOutput["Quantity"] = 0;
                    rNewResultOutput["VAT"] = 0;
                    rNewResultOutput["VATPercent"] = 0;
                    rNewResultOutput["TotalMoney"] = 0;
                    rNewResultOutput["CreatedDate"] = DateTime.MinValue;
                    rNewResultOutput["OutputVoucherID"] = string.Empty;
                    rNewResultOutput["OutputVoucherDetailID"] = string.Empty;
                    rNewResultOutput["CollectArrearPrice"] = 0;
                    rNewResultOutput["DutyUsers"] = string.Empty;
                    dtbResultOutput.Rows.Add(rNewResultOutput);
                }
                bool bolSave = objPLCInventoryProcess.UpdateInventoryProcess(objInventoryProcess.InventoryProcessID,
                    txtInputContent.Text.Trim(),
                    txtOutPutContent.Text.Trim(),
                    txtPrChangeContent.Text.Trim(),
                    txtSTChangeContent.Text.Trim(),
                    SystemConfig.intDefaultStoreID,
                    dtbResultInput, dtbResultOutput, dtbProductChange, dtbStatusChange,
                    ref dtbCollectArrear);
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    cmdUpdate.Enabled = true;
                }
                else
                {
                    if (dtbCollectArrear != null && dtbCollectArrear.Rows.Count > 0)
                    {
                        if (dtbCollectArrearOutMinus != null && dtbCollectArrearOutMinus.Rows.Count > 0)
                        {
                            foreach (DataRow rArrearOut in dtbCollectArrearOutMinus.Rows)
                            {
                                if (Convert.IsDBNull(rArrearOut["CollectArrearID"]) || Convert.ToString(rArrearOut["CollectArrearID"]) == string.Empty)
                                {
                                    DataRow[] drArrearOut = dtbCollectArrear.Select("RelateVoucherID = '" + rArrearOut["RelateVoucherID"] + "' AND UserName = '" + rArrearOut["UserName"] + "' AND IsDeleted = 0");
                                    if (drArrearOut.Length > 0)
                                    {
                                        rArrearOut["CollectArrearID"] = Convert.ToString(drArrearOut[0]["CollectArrearID"]);
                                    }
                                }
                            }
                        }
                        if (dtbCollectArrearPrChange != null && dtbCollectArrearPrChange.Rows.Count > 0)
                        {
                            foreach (DataRow rPrChange in dtbCollectArrearPrChange.Rows)
                            {
                                if (Convert.IsDBNull(rPrChange["CollectArrearID"]) || Convert.ToString(rPrChange["CollectArrearID"]) == string.Empty)
                                {
                                    DataRow[] drArrearPrChange = dtbCollectArrear.Select("RelateVoucherID = '" + rPrChange["RelateVoucherID"] + "' AND UserName = '" + rPrChange["UserName"] + "' AND IsDeleted = 0");
                                    if (drArrearPrChange.Length > 0)
                                    {
                                        rPrChange["CollectArrearID"] = Convert.ToString(drArrearPrChange[0]["CollectArrearID"]);
                                    }
                                }
                            }
                        }

                        if (dtbCollectArrearStChange != null && dtbCollectArrearStChange.Rows.Count > 0)
                        {
                            foreach (DataRow rStChange in dtbCollectArrearStChange.Rows)
                            {
                                if (Convert.IsDBNull(rStChange["CollectArrearID"]) || Convert.ToString(rStChange["CollectArrearID"]) == string.Empty)
                                {
                                    DataRow[] drArrearStChange = dtbCollectArrear.Select("RelateVoucherID = '" + rStChange["RelateVoucherID"] + "' AND UserName = '" + rStChange["UserName"] + "' AND IsDeleted = 0");
                                    if (drArrearStChange.Length > 0)
                                    {
                                        rStChange["CollectArrearID"] = Convert.ToString(drArrearStChange[0]["CollectArrearID"]);
                                    }
                                }
                            }
                        }
                    }
                    MessageBox.Show(this, "Đã cập nhật phiếu yêu cầu xử lý kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmdUpdate.Enabled = true;
                    cmdReview.Enabled = bolIsRight_InventoryProcess_Review;
                }
            }
            catch (Exception objEx)
            {
                MessageBox.Show(this, "Lỗi cập nhật phiếu yêu cầu xử lý kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cmdUpdate.Enabled = true;
            }
            #endregion
        }
        private void flexProductChange_CellButtonClick(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (objInventoryProcess.IsProcess || objInventoryProcess.IsReviewed)
            {
                return;
            }

            decimal decCollectArrearMoney = 0;
            if (!Convert.IsDBNull(flexProductChange[e.Row, "CollectArrearPrice"])) decCollectArrearMoney = Math.Round(Convert.ToDecimal(flexProductChange[e.Row, "CollectArrearPrice"]));
            if (decCollectArrearMoney <= 0)
            {
                return;
            }

            frmDutyUsers frmDutyUsers1 = new frmDutyUsers();
            frmDutyUsers1.InventoryProcessID = objInventoryProcess.InventoryProcessID;
            frmDutyUsers1.CollectArrearMoney = decCollectArrearMoney;
            if (!Convert.IsDBNull(flexProductChange[e.Row, "ProductChangeProcessID"])) frmDutyUsers1.RelateVoucherID = Convert.ToString(flexProductChange[e.Row, "ProductChangeProcessID"]);
            frmDutyUsers1.CollectArrearType = 2;
            frmDutyUsers1.dtbDutyUsers = objPLCInventoryProcess.LoadInventProcessArrear(objInventoryProcess.InventoryProcessID, frmDutyUsers1.RelateVoucherID);
            if (frmDutyUsers1.dtbDutyUsers == null || frmDutyUsers1.dtbDutyUsers.Rows.Count == 0)
            {
                if (dtbCollectArrearPrChange == null || dtbCollectArrearPrChange.Rows.Count == 0)
                {
                    dtbCollectArrearPrChange = new DataTable();
                    dtbCollectArrearPrChange.Columns.Add("IsSelect", typeof(bool));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("InventoryProcessID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearType", typeof(int));
                    dtbCollectArrearPrChange.Columns.Add("RelateVoucherID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("UserName", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("FullName", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearMoney", typeof(decimal));
                    dtbCollectArrearPrChange.Columns.Add("Note", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("IsDeleted", typeof(bool));
                    dtbCollectArrearPrChange.Columns.Add("DeletedUser", typeof(string));
                }
                else
                {
                    if (dtbCollectArrearPrChange.Rows.Count > 0)
                    {
                        frmDutyUsers1.dtbDutyUsers = dtbCollectArrearPrChange.Clone();
                        DataRow[] drProductChange = dtbCollectArrearPrChange.Select("IsDeleted = 0 AND RelateVoucherID = '" + Convert.ToString(flexProductChange[flexProductChange.RowSel, "ProductChangeProcessID"]) + "'");
                        if (drProductChange.Length > 0)
                        {
                            foreach (DataRow rProductChange in drProductChange)
                            {
                                try
                                {
                                    DataRow[] drUser = dtbUsers.Select("UserName = '" + Convert.ToString(rProductChange["UserName"]) + "'");
                                    if (drUser.Length > 0)
                                    {
                                        rProductChange["FullName"] = Convert.ToString(drUser[0]["FullName"]).Trim();
                                    }
                                }
                                catch { }
                                DataRow rNewProductChange = frmDutyUsers1.dtbDutyUsers.NewRow();
                                rNewProductChange.ItemArray = rProductChange.ItemArray;
                                frmDutyUsers1.dtbDutyUsers.Rows.Add(rNewProductChange);
                            }
                        }
                    }
                }
            }
            else
            {
                if (dtbCollectArrearPrChange == null || dtbCollectArrearPrChange.Rows.Count == 0)
                {
                    dtbCollectArrearPrChange = new DataTable();
                    dtbCollectArrearPrChange.Columns.Add("IsSelect", typeof(bool));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("InventoryProcessID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearType", typeof(int));
                    dtbCollectArrearPrChange.Columns.Add("RelateVoucherID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("UserName", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("FullName", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearMoney", typeof(decimal));
                    dtbCollectArrearPrChange.Columns.Add("Note", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("IsDeleted", typeof(bool));
                    dtbCollectArrearPrChange.Columns.Add("DeletedUser", typeof(string));
                }
                DataRow[] drArrearUsers = frmDutyUsers1.dtbDutyUsers.Select("InventoryProcessID <> ''");
                foreach (DataRow rArrear in drArrearUsers)
                {
                    DataRow[] drPrChange = dtbCollectArrearPrChange.Select("RelateVoucherID = '" + Convert.ToString(flexProductChange[flexProductChange.RowSel, "ProductChangeProcessID"]) + "' AND UserName = '" + Convert.ToString(rArrear["UserName"]) + "'");
                    if (drPrChange.Length == 0)
                    {
                        DataRow rNewArrear = dtbCollectArrearPrChange.NewRow();
                        rNewArrear.ItemArray = rArrear.ItemArray;
                        dtbCollectArrearPrChange.Rows.Add(rNewArrear);
                    }
                }
                DataRow[] drArrear = dtbCollectArrearPrChange.Select("IsDeleted = 0 AND RelateVoucherID = '" + Convert.ToString(flexProductChange[flexProductChange.RowSel, "ProductChangeProcessID"]) + "'");
                if (drArrear.Length > 0)
                {
                    frmDutyUsers1.dtbDutyUsers = new DataTable();
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("IsSelect", typeof(bool));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("CollectArrearID", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("InventoryProcessID", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("CollectArrearType", typeof(int));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("RelateVoucherID", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("UserName", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("FullName", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("CollectArrearMoney", typeof(decimal));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("Note", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("IsDeleted", typeof(bool));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("DeletedUser", typeof(string));

                    foreach (DataRow rArrear in drArrear)
                    {
                        try
                        {
                            DataRow[] drUser = dtbUsers.Select("UserName = '" + Convert.ToString(rArrear["UserName"]) + "'");
                            if (drUser.Length > 0)
                            {
                                rArrear["FullName"] = Convert.ToString(drUser[0]["FullName"]).Trim();
                            }
                        }
                        catch { }
                        DataRow rNewArrear = frmDutyUsers1.dtbDutyUsers.NewRow();
                        rNewArrear.ItemArray = rArrear.ItemArray;
                        frmDutyUsers1.dtbDutyUsers.Rows.Add(rNewArrear);
                    }
                }
            }
            if (frmDutyUsers1.CollectArrearMoney > 0)
            {
                frmDutyUsers1.Text = "Số tiền truy thu là " + frmDutyUsers1.CollectArrearMoney.ToString("#,##0") + " VNĐ";
            }
            frmDutyUsers1.ShowDialog();
            if (frmDutyUsers1.DutyUsers != string.Empty)
            {
                flexProductChange[e.Row, "DutyUsers"] = frmDutyUsers1.DutyUsers;

                if (frmDutyUsers1.dtbDutyUsers != null && frmDutyUsers1.dtbDutyUsers.Rows.Count > 0)
                {
                    if (dtbCollectArrearPrChange.Rows.Count > 0)
                    {
                        for (int i = dtbCollectArrearPrChange.Rows.Count - 1; i > -1; i--)
                        {
                            if (!Convert.IsDBNull(dtbCollectArrearPrChange.Rows[i]["RelateVoucherID"]) && Convert.ToString(dtbCollectArrearPrChange.Rows[i]["RelateVoucherID"]) == frmDutyUsers1.RelateVoucherID)
                            {
                                DataRow[] drPrChange = frmDutyUsers1.dtbDutyUsers.Select("RelateVoucherID = '" + frmDutyUsers1.RelateVoucherID + "' AND UserName = '" + Convert.ToString(dtbCollectArrearPrChange.Rows[i]["UserName"]) + "'");
                                if (drPrChange.Length == 0)
                                {
                                    dtbCollectArrearPrChange.Rows[i]["IsDeleted"] = true;
                                    dtbCollectArrearPrChange.Rows[i]["DeletedUser"] = SystemConfig.objSessionUser.UserName;
                                }
                            }
                        }
                    }
                    for (int i = 0; i < frmDutyUsers1.dtbDutyUsers.Rows.Count; i++)
                    {
                        DataRow rDutyUser = frmDutyUsers1.dtbDutyUsers.Rows[i];
                        if (!Convert.IsDBNull(rDutyUser["CollectArrearID"]) && Convert.ToString(rDutyUser["CollectArrearID"]) != string.Empty)
                        {
                            DataRow[] drCollectArrear = dtbCollectArrearPrChange.Select("CollectArrearID = '" + Convert.ToString(rDutyUser["CollectArrearID"]) + "'");
                            if (drCollectArrear.Length > 0)
                            {
                                foreach (DataRow rCollectArrear in dtbCollectArrearPrChange.Rows)
                                {
                                    if (Convert.ToString(rCollectArrear["CollectArrearID"]) == Convert.ToString(rDutyUser["CollectArrearID"]))
                                    {
                                        rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                        rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                        rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                        rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                        rCollectArrear["IsDeleted"] = false;
                                        rCollectArrear["DeletedUser"] = string.Empty;
                                        rCollectArrear["IsSelect"] = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                DataRow rCollectArrear = dtbCollectArrearPrChange.NewRow();
                                rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                rCollectArrear["CollectArrearType"] = 2;
                                rCollectArrear["RelateVoucherID"] = frmDutyUsers1.RelateVoucherID;
                                rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                rCollectArrear["IsDeleted"] = false;
                                rCollectArrear["DeletedUser"] = string.Empty;
                                rCollectArrear["IsSelect"] = false;
                                dtbCollectArrearPrChange.Rows.Add(rCollectArrear);
                            }
                        }
                        else
                        {
                            if (dtbCollectArrearPrChange.Rows.Count > 0)
                            {
                                DataRow[] drCollectArrear = dtbCollectArrearPrChange.Select("RelateVoucherID = '" + frmDutyUsers1.RelateVoucherID + "' AND UserName = '" + Convert.ToString(rDutyUser["UserName"]) + "'");
                                if (drCollectArrear.Length > 0)
                                {
                                    foreach (DataRow rCollectArrear in dtbCollectArrearPrChange.Rows)
                                    {
                                        if (Convert.ToString(rCollectArrear["UserName"]) == Convert.ToString(rDutyUser["UserName"]))
                                        {
                                            rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                            rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                            rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                            rCollectArrear["IsDeleted"] = false;
                                            rCollectArrear["DeletedUser"] = string.Empty;
                                            rCollectArrear["IsSelect"] = false;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    DataRow rCollectArrear = dtbCollectArrearPrChange.NewRow();
                                    rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                    rCollectArrear["CollectArrearType"] = 2;
                                    rCollectArrear["RelateVoucherID"] = frmDutyUsers1.RelateVoucherID;
                                    rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                    rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                    rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                    rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                    rCollectArrear["IsDeleted"] = false;
                                    rCollectArrear["DeletedUser"] = string.Empty;
                                    rCollectArrear["IsSelect"] = false;
                                    dtbCollectArrearPrChange.Rows.Add(rCollectArrear);
                                }
                            }
                            else
                            {
                                DataRow rCollectArrear = dtbCollectArrearPrChange.NewRow();
                                rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                rCollectArrear["CollectArrearType"] = 2;
                                rCollectArrear["RelateVoucherID"] = frmDutyUsers1.RelateVoucherID;
                                rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                rCollectArrear["IsDeleted"] = false;
                                rCollectArrear["DeletedUser"] = string.Empty;
                                rCollectArrear["IsSelect"] = false;
                                dtbCollectArrearPrChange.Rows.Add(rCollectArrear);
                            }
                        }
                    }
                }
            }

        }

        private void flexInPlus_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            try
            {
                if (flexInPlus.Rows.Count <= flexInPlus.Rows.Fixed)
                {
                    return;
                }
                if (e.Col == 6 || e.Col == 8 || e.Col == 9)
                {
                    if (e.Col == 6)
                    {
                        flexInPlus[e.Row, "Price"] = Math.Abs(Convert.ToDecimal(flexInPlus[e.Row, "Price"]));
                    }
                    if (e.Col == 8)
                    {
                        flexInPlus[e.Row, "VAT"] = Math.Abs(Convert.ToDecimal(flexInPlus[e.Row, "VAT"]));
                        if (Convert.ToDecimal(flexInPlus[e.Row, "VAT"]) > 100)
                        {
                            flexInPlus[e.Row, "VAT"] = 0;
                        }
                    }
                    if (e.Col == 9)
                    {
                        flexInPlus[e.Row, "VATPercent"] = Math.Abs(Convert.ToDecimal(flexInPlus[e.Row, "VATPercent"]));
                    }

                    decimal decTotalMoney = Convert.ToDecimal(flexInPlus[e.Row, "Price"]) *
                            Convert.ToDecimal(Convert.ToDecimal(1) + Convert.ToDecimal(flexInPlus[e.Row, "VAT"]) * Convert.ToDecimal(flexInPlus[e.Row, "VATPercent"]) * Convert.ToDecimal(0.0001)) *
                            Convert.ToDecimal(flexInPlus[e.Row, "Quantity"]);
                    flexInPlus[e.Row, "TotalMoney"] = decTotalMoney;
                }
            }
            catch
            {
            }
        }

        private void flexOutMinus_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            try
            {
                if (flexOutMinus.Rows.Count <= flexOutMinus.Rows.Fixed)
                {
                    return;
                }
                if (e.Col == 6 || e.Col == 8 || e.Col == 9)
                {
                    if (e.Col == 6)
                    {
                        flexOutMinus[e.Row, "Price"] = Math.Abs(Convert.ToDecimal(flexOutMinus[e.Row, "Price"]));
                    }
                    if (e.Col == 8)
                    {
                        flexOutMinus[e.Row, "VAT"] = Math.Abs(Convert.ToDecimal(flexOutMinus[e.Row, "VAT"]));
                        if (Convert.ToDecimal(flexOutMinus[e.Row, "VAT"]) > 100)
                        {
                            flexOutMinus[e.Row, "VAT"] = 0;
                        }
                    }
                    if (e.Col == 9)
                    {
                        flexOutMinus[e.Row, "VATPercent"] = Math.Abs(Convert.ToDecimal(flexOutMinus[e.Row, "VATPercent"]));
                    }

                    decimal decTotalMoney = Convert.ToDecimal(flexOutMinus[e.Row, "Price"]) *
                            Convert.ToDecimal(Convert.ToDecimal(1) + Convert.ToDecimal(flexOutMinus[e.Row, "VAT"]) * Convert.ToDecimal(flexOutMinus[e.Row, "VATPercent"]) * Convert.ToDecimal(0.0001)) *
                            Convert.ToDecimal(flexOutMinus[e.Row, "Quantity"]);
                    flexOutMinus[e.Row, "TotalMoney"] = decTotalMoney;
                    flexOutMinus[e.Row, "CollectArrearPrice"] = decTotalMoney;
                }
            }
            catch
            {
            }
        }

        private void flexOutMinus_CellButtonClick(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (objInventoryProcess.IsProcess || objInventoryProcess.IsReviewed)
            {
                return;
            }

            decimal decCollectArrearMoney = 0;
            if (!Convert.IsDBNull(flexOutMinus[e.Row, "CollectArrearPrice"])) decCollectArrearMoney = Math.Round(Convert.ToDecimal(flexOutMinus[e.Row, "CollectArrearPrice"]));
            if (decCollectArrearMoney <= 0)
            {
                return;
            }

            frmDutyUsers frmDutyUsers1 = new frmDutyUsers();
            frmDutyUsers1.InventoryProcessID = objInventoryProcess.InventoryProcessID;
            frmDutyUsers1.CollectArrearMoney = decCollectArrearMoney;
            if (!Convert.IsDBNull(flexOutMinus[e.Row, "OutputProcessID"])) frmDutyUsers1.RelateVoucherID = Convert.ToString(flexOutMinus[e.Row, "OutputProcessID"]);
            frmDutyUsers1.CollectArrearType = 1;
            frmDutyUsers1.dtbDutyUsers = objPLCInventoryProcess.LoadInventProcessArrear(objInventoryProcess.InventoryProcessID, frmDutyUsers1.RelateVoucherID);
            if (frmDutyUsers1.dtbDutyUsers == null || frmDutyUsers1.dtbDutyUsers.Rows.Count == 0)
            {
                if (dtbCollectArrearOutMinus == null || dtbCollectArrearOutMinus.Rows.Count == 0)
                {
                    dtbCollectArrearOutMinus = new DataTable();
                    dtbCollectArrearOutMinus.Columns.Add("IsSelect", typeof(bool));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("InventoryProcessID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearType", typeof(int));
                    dtbCollectArrearOutMinus.Columns.Add("RelateVoucherID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("UserName", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("FullName", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearMoney", typeof(decimal));
                    dtbCollectArrearOutMinus.Columns.Add("Note", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("IsDeleted", typeof(bool));
                    dtbCollectArrearOutMinus.Columns.Add("DeletedUser", typeof(string));
                }
                else
                {
                    if (dtbCollectArrearOutMinus.Rows.Count > 0)
                    {
                        frmDutyUsers1.dtbDutyUsers = dtbCollectArrearOutMinus.Clone();
                        DataRow[] drOutMinus = dtbCollectArrearOutMinus.Select("IsDeleted = 0 AND RelateVoucherID = '" + Convert.ToString(flexOutMinus[flexOutMinus.RowSel, "OutputProcessID"]) + "'");
                        if (drOutMinus.Length > 0)
                        {
                            foreach (DataRow rOutMinus in drOutMinus)
                            {
                                try
                                {
                                    DataRow[] drUser = dtbUsers.Select("UserName = '" + Convert.ToString(rOutMinus["UserName"]) + "'");
                                    if (drUser.Length > 0)
                                    {
                                        rOutMinus["FullName"] = Convert.ToString(drUser[0]["FullName"]).Trim();
                                    }
                                }
                                catch { }
                                DataRow rNewOutMinus = frmDutyUsers1.dtbDutyUsers.NewRow();
                                rNewOutMinus.ItemArray = rOutMinus.ItemArray;
                                frmDutyUsers1.dtbDutyUsers.Rows.Add(rNewOutMinus);
                            }
                        }
                    }
                }
            }
            else
            {
                if (dtbCollectArrearOutMinus == null || dtbCollectArrearOutMinus.Rows.Count == 0)
                {
                    dtbCollectArrearOutMinus = new DataTable();
                    dtbCollectArrearOutMinus.Columns.Add("IsSelect", typeof(bool));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("InventoryProcessID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearType", typeof(int));
                    dtbCollectArrearOutMinus.Columns.Add("RelateVoucherID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("UserName", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("FullName", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearMoney", typeof(decimal));
                    dtbCollectArrearOutMinus.Columns.Add("Note", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("IsDeleted", typeof(bool));
                    dtbCollectArrearOutMinus.Columns.Add("DeletedUser", typeof(string));
                }
                DataRow[] drArrearUsers = frmDutyUsers1.dtbDutyUsers.Select("InventoryProcessID <> ''");
                foreach (DataRow rArrear in drArrearUsers)
                {
                    DataRow[] drOutMinus = dtbCollectArrearOutMinus.Select("RelateVoucherID = '" + Convert.ToString(flexOutMinus[flexOutMinus.RowSel, "OutputProcessID"]) + "' AND UserName = '" + Convert.ToString(rArrear["UserName"]) + "'");
                    if (drOutMinus.Length == 0)
                    {
                        DataRow rNewArrear = dtbCollectArrearOutMinus.NewRow();
                        rNewArrear.ItemArray = rArrear.ItemArray;
                        dtbCollectArrearOutMinus.Rows.Add(rNewArrear);
                    }
                }
                DataRow[] drArrear = dtbCollectArrearOutMinus.Select("IsDeleted = 0 AND RelateVoucherID = '" + Convert.ToString(flexOutMinus[flexOutMinus.RowSel, "OutputProcessID"]) + "'");
                if (drArrear.Length > 0)
                {
                    frmDutyUsers1.dtbDutyUsers = new DataTable();
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("IsSelect", typeof(bool));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("CollectArrearID", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("InventoryProcessID", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("CollectArrearType", typeof(int));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("RelateVoucherID", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("UserName", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("FullName", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("CollectArrearMoney", typeof(decimal));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("Note", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("IsDeleted", typeof(bool));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("DeletedUser", typeof(string));

                    foreach (DataRow rArrear in drArrear)
                    {
                        try
                        {
                            DataRow[] drUser = dtbUsers.Select("UserName = '" + Convert.ToString(rArrear["UserName"]) + "'");
                            if (drUser.Length > 0)
                            {
                                rArrear["FullName"] = Convert.ToString(drUser[0]["FullName"]).Trim();
                            }
                        }
                        catch { }
                        DataRow rNewArrear = frmDutyUsers1.dtbDutyUsers.NewRow();
                        rNewArrear.ItemArray = rArrear.ItemArray;
                        frmDutyUsers1.dtbDutyUsers.Rows.Add(rNewArrear);
                    }
                }
            }
            if (frmDutyUsers1.CollectArrearMoney > 0)
            {
                frmDutyUsers1.Text = "Số tiền truy thu là " + frmDutyUsers1.CollectArrearMoney.ToString("#,##0") + " VNĐ";
            }
            frmDutyUsers1.ShowDialog();
            if (frmDutyUsers1.DutyUsers != string.Empty)
            {
                flexOutMinus[e.Row, "DutyUsers"] = frmDutyUsers1.DutyUsers;

                if (frmDutyUsers1.dtbDutyUsers != null && frmDutyUsers1.dtbDutyUsers.Rows.Count > 0)
                {
                    if (dtbCollectArrearOutMinus.Rows.Count > 0)
                    {
                        for (int i = dtbCollectArrearOutMinus.Rows.Count - 1; i > -1; i--)
                        {
                            if (!Convert.IsDBNull(dtbCollectArrearOutMinus.Rows[i]["RelateVoucherID"]) && Convert.ToString(dtbCollectArrearOutMinus.Rows[i]["RelateVoucherID"]) == frmDutyUsers1.RelateVoucherID)
                            {
                                DataRow[] drOutMinus = frmDutyUsers1.dtbDutyUsers.Select("RelateVoucherID = '" + frmDutyUsers1.RelateVoucherID + "' AND UserName = '" + Convert.ToString(dtbCollectArrearOutMinus.Rows[i]["UserName"]) + "'");
                                if (drOutMinus.Length == 0)
                                {
                                    dtbCollectArrearOutMinus.Rows[i]["IsDeleted"] = true;
                                    dtbCollectArrearOutMinus.Rows[i]["DeletedUser"] = SystemConfig.objSessionUser.UserName;
                                }
                            }
                        }
                    }
                    for (int i = 0; i < frmDutyUsers1.dtbDutyUsers.Rows.Count; i++)
                    {
                        DataRow rDutyUser = frmDutyUsers1.dtbDutyUsers.Rows[i];
                        if (!Convert.IsDBNull(rDutyUser["CollectArrearID"]) && Convert.ToString(rDutyUser["CollectArrearID"]) != string.Empty)
                        {
                            DataRow[] drCollectArrear = dtbCollectArrearOutMinus.Select("CollectArrearID = '" + Convert.ToString(rDutyUser["CollectArrearID"]) + "'");
                            if (drCollectArrear.Length > 0)
                            {
                                foreach (DataRow rCollectArrear in dtbCollectArrearOutMinus.Rows)
                                {
                                    if (Convert.ToString(rCollectArrear["CollectArrearID"]) == Convert.ToString(rDutyUser["CollectArrearID"]))
                                    {
                                        rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                        rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                        rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                        rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                        rCollectArrear["IsDeleted"] = false;
                                        rCollectArrear["DeletedUser"] = string.Empty;
                                        rCollectArrear["IsSelect"] = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                DataRow rCollectArrear = dtbCollectArrearOutMinus.NewRow();
                                rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                rCollectArrear["CollectArrearType"] = 1;
                                rCollectArrear["RelateVoucherID"] = frmDutyUsers1.RelateVoucherID;
                                rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                rCollectArrear["IsDeleted"] = false;
                                rCollectArrear["DeletedUser"] = string.Empty;
                                rCollectArrear["IsSelect"] = false;
                                dtbCollectArrearOutMinus.Rows.Add(rCollectArrear);
                            }
                        }
                        else
                        {
                            if (dtbCollectArrearOutMinus.Rows.Count > 0)
                            {
                                DataRow[] drCollectArrear = dtbCollectArrearOutMinus.Select("RelateVoucherID = '" + frmDutyUsers1.RelateVoucherID + "' AND UserName = '" + Convert.ToString(rDutyUser["UserName"]) + "'");
                                if (drCollectArrear.Length > 0)
                                {
                                    foreach (DataRow rCollectArrear in dtbCollectArrearOutMinus.Rows)
                                    {
                                        if (Convert.ToString(rCollectArrear["UserName"]) == Convert.ToString(rDutyUser["UserName"]))
                                        {
                                            rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                            rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                            rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                            rCollectArrear["IsDeleted"] = false;
                                            rCollectArrear["DeletedUser"] = string.Empty;
                                            rCollectArrear["IsSelect"] = false;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    DataRow rCollectArrear = dtbCollectArrearOutMinus.NewRow();
                                    rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                    rCollectArrear["CollectArrearType"] = 1;
                                    rCollectArrear["RelateVoucherID"] = frmDutyUsers1.RelateVoucherID;
                                    rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                    rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                    rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                    rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                    rCollectArrear["IsDeleted"] = false;
                                    rCollectArrear["DeletedUser"] = string.Empty;
                                    rCollectArrear["IsSelect"] = false;
                                    dtbCollectArrearOutMinus.Rows.Add(rCollectArrear);
                                }
                            }
                            else
                            {
                                DataRow rCollectArrear = dtbCollectArrearOutMinus.NewRow();
                                rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                rCollectArrear["CollectArrearType"] = 2;
                                rCollectArrear["RelateVoucherID"] = frmDutyUsers1.RelateVoucherID;
                                rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                rCollectArrear["IsDeleted"] = false;
                                rCollectArrear["DeletedUser"] = string.Empty;
                                rCollectArrear["IsSelect"] = false;
                                dtbCollectArrearOutMinus.Rows.Add(rCollectArrear);
                            }
                        }
                    }
                }
            }

        }

        private void cmdProcess_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Bạn có chắc xử lý phiếu yêu cầu xử lý kiểm kê?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                return;
            }

            if (!CheckPrChange())
                return;
            if (!CheckStChange())
                return;
            if (!CheckOutMinus())
                return;

            cmdProcess.Enabled = false;
            try
            {
                bool bolSave = objPLCInventoryProcess.ProcessedInventProcess(objInventoryProcess.InventoryProcessID, true, SystemConfig.intDefaultStoreID);
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    cmdProcess.Enabled = true;
                }
                else
                {
                    MessageBox.Show(this, "Đã xử lý phiếu yêu cầu xử lý kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    frmResolveQuantity_Load(null, null);
                }
            }
            catch (Exception objEx)
            {
                MessageBox.Show(this, "Lỗi xử lý phiếu yêu cầu xử lý kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cmdProcess.Enabled = true;
            }
        }

        private void cmdReview_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Bạn có chắc duyệt phiếu yêu cầu xử lý kiểm kê?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                return;
            }

            if (!CheckPrChange())
                return;
            if (!CheckStChange())
                return;
            if (!CheckOutMinus())
                return;
            if (!CheckContent())
                return;

            bool isUpdateSucceed = false;
            #region Update
            cmdUpdate.Enabled = false;
            try
            {
                DataTable dtbCollectArrear = null;
                if (dtbCollectArrearPrChange != null && dtbCollectArrearPrChange.Rows.Count > 0)
                {
                    dtbCollectArrear = dtbCollectArrearPrChange.Clone();
                }
                if (dtbCollectArrear == null)
                {
                    if (dtbCollectArrearOutMinus != null && dtbCollectArrearOutMinus.Rows.Count > 0)
                    {
                        dtbCollectArrear = dtbCollectArrearOutMinus.Clone();
                    }
                    if (dtbCollectArrear == null)
                    {
                        dtbCollectArrear = new DataTable();
                        dtbCollectArrear.Columns.Add("IsSelect", typeof(bool));
                        dtbCollectArrear.Columns.Add("CollectArrearID", typeof(string));
                        dtbCollectArrear.Columns.Add("InventoryProcessID", typeof(string));
                        dtbCollectArrear.Columns.Add("CollectArrearType", typeof(int));
                        dtbCollectArrear.Columns.Add("RelateVoucherID", typeof(string));
                        dtbCollectArrear.Columns.Add("UserName", typeof(string));
                        dtbCollectArrear.Columns.Add("FullName", typeof(string));
                        dtbCollectArrear.Columns.Add("CollectArrearMoney", typeof(decimal));
                        dtbCollectArrear.Columns.Add("Note", typeof(string));
                        dtbCollectArrear.Columns.Add("IsDeleted", typeof(bool));
                        dtbCollectArrear.Columns.Add("DeletedUser", typeof(string));
                    }
                }
                DataRow[] drCollectArrear;
                if (dtbCollectArrearPrChange != null && dtbCollectArrearPrChange.Rows.Count > 0)
                {
                    drCollectArrear = dtbCollectArrearPrChange.Select("IsDeleted = 1");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            if (!Convert.IsDBNull(rCollectArrear["CollectArrearID"]) && Convert.ToString(rCollectArrear["CollectArrearID"]) != string.Empty)
                            {
                                DataRow rNewArrear = dtbCollectArrear.NewRow();
                                rNewArrear.ItemArray = rCollectArrear.ItemArray;
                                dtbCollectArrear.Rows.Add(rNewArrear);
                            }
                        }
                    }
                    drCollectArrear = dtbCollectArrearPrChange.Select("IsDeleted = 0");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            DataRow rNewArrear = dtbCollectArrear.NewRow();
                            rNewArrear.ItemArray = rCollectArrear.ItemArray;
                            dtbCollectArrear.Rows.Add(rNewArrear);
                        }
                    }
                }

                if (dtbCollectArrearStChange != null && dtbCollectArrearStChange.Rows.Count > 0)
                {
                    drCollectArrear = dtbCollectArrearStChange.Select("IsDeleted = 1");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            if (!Convert.IsDBNull(rCollectArrear["CollectArrearID"]) && Convert.ToString(rCollectArrear["CollectArrearID"]) != string.Empty)
                            {
                                DataRow rNewArrear = dtbCollectArrear.NewRow();
                                rNewArrear.ItemArray = rCollectArrear.ItemArray;
                                dtbCollectArrear.Rows.Add(rNewArrear);
                            }
                        }
                    }
                    drCollectArrear = dtbCollectArrearStChange.Select("IsDeleted = 0");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            DataRow rNewArrear = dtbCollectArrear.NewRow();
                            rNewArrear.ItemArray = rCollectArrear.ItemArray;
                            dtbCollectArrear.Rows.Add(rNewArrear);
                        }
                    }
                }
                if (dtbCollectArrearOutMinus != null && dtbCollectArrearOutMinus.Rows.Count > 0)
                {
                    drCollectArrear = dtbCollectArrearOutMinus.Select("IsDeleted = 1");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            if (!Convert.IsDBNull(rCollectArrear["CollectArrearID"]) && Convert.ToString(rCollectArrear["CollectArrearID"]) != string.Empty)
                            {
                                DataRow rNewArrear = dtbCollectArrear.NewRow();
                                rNewArrear.ItemArray = rCollectArrear.ItemArray;
                                dtbCollectArrear.Rows.Add(rNewArrear);
                            }
                        }
                    }
                    drCollectArrear = dtbCollectArrearOutMinus.Select("IsDeleted = 0");
                    if (drCollectArrear.Length > 0)
                    {
                        foreach (DataRow rCollectArrear in drCollectArrear)
                        {
                            DataRow rNewArrear = dtbCollectArrear.NewRow();
                            rNewArrear.ItemArray = rCollectArrear.ItemArray;
                            dtbCollectArrear.Rows.Add(rNewArrear);
                        }
                    }
                }
                if (dtbProductChange.Rows.Count == 0)
                {
                    //DataRow rNewProductChange = dtbProductChange.NewRow();
                    //rNewProductChange["ProductChangeProcessID"] = string.Empty;
                    //rNewProductChange["InventoryProcessID"] = string.Empty;
                    //rNewProductChange["ProductID_Out"] = string.Empty;
                    //rNewProductChange["ProductName_Out"] = string.Empty;
                    //rNewProductChange["IMEI_Out"] = string.Empty;
                    //rNewProductChange["RefSalePrice_Out"] = 0;
                    //rNewProductChange["ProductID_In"] = string.Empty;
                    //rNewProductChange["ProductName_In"] = string.Empty;
                    //rNewProductChange["IMEI_In"] = string.Empty;
                    //rNewProductChange["RefSalePrice_In"] = 0;
                    //rNewProductChange["Quantity"] = 0;
                    //rNewProductChange["CreatedDate"] = DateTime.MinValue;
                    //rNewProductChange["ProductChangeDetailID"] = string.Empty;
                    //rNewProductChange["CollectArrearPrice"] = 0;
                    //rNewProductChange["DutyUsers"] = string.Empty;
                    //dtbProductChange.Rows.Add(rNewProductChange);
                }

                if (dtbStatusChange.Rows.Count == 0)
                {
                    //DataRow rNewStatusChange = dtbStatusChange.NewRow();
                    //rNewStatusChange["PRODUCTCHANGEPROCESSID"] = string.Empty;
                    //rNewStatusChange["INVENTORYPROCESSID"] = string.Empty;
                    //rNewStatusChange["PRODUCTID_OUT"] = string.Empty;
                    //rNewStatusChange["PRODUCTNAME_OUT"] = string.Empty;
                    //rNewStatusChange["IMEI_OUT"] = string.Empty;
                    //rNewStatusChange["REFSALEPRICE_OUT"] = 0;
                    //rNewStatusChange["PRODUCTID_IN"] = string.Empty;
                    //rNewStatusChange["PRODUCTNAME_IN"] = string.Empty;
                    //rNewStatusChange["IMEI_IN"] = string.Empty;
                    //rNewStatusChange["REFSALEPRICE_IN"] = 0;
                    //rNewStatusChange["QUANTITY"] = 0;
                    //rNewStatusChange["CREATEDDATE"] = DateTime.MinValue;
                    //rNewStatusChange["PRODUCTCHANGEDETAILID"] = string.Empty;
                    //rNewStatusChange["COLLECTARREARPRICE"] = 0;
                    //rNewStatusChange["DUTYUSERS"] = string.Empty;
                    //dtbStatusChange.Rows.Add(rNewStatusChange);
                }
                DataTable dtbResultInput = new DataTable();
                if (dtbInput != null && dtbInput.Rows.Count > 0)
                {
                    dtbResultInput = dtbInput.Clone();
                    dtbResultInput.TableName = "ResultInput";
                    DataRow[] drInput = dtbInput.Select("ProductID <> ''");
                    if (drInput.Length > 0)
                    {
                        foreach (DataRow rInput in drInput)
                        {
                            DataRow rNewInput = dtbResultInput.NewRow();
                            rNewInput.ItemArray = rInput.ItemArray;
                            dtbResultInput.Rows.Add(rNewInput);
                        }
                    }
                }
                if (dtbResultInput.Rows.Count == 0)
                {
                    dtbResultInput = dtbInput.Clone();
                    dtbResultInput.TableName = "ResultInput";
                    DataRow rNewResultInput = dtbResultInput.NewRow();
                    rNewResultInput["InputProcessID"] = string.Empty;
                    rNewResultInput["InventoryProcessID"] = string.Empty;
                    rNewResultInput["ProductID"] = string.Empty;
                    rNewResultInput["ProductName"] = string.Empty;
                    rNewResultInput["IMEI"] = string.Empty;
                    rNewResultInput["Price"] = 0;
                    rNewResultInput["Quantity"] = 0;
                    rNewResultInput["VAT"] = 0;
                    rNewResultInput["VATPercent"] = 0;
                    rNewResultInput["TotalMoney"] = 0;
                    rNewResultInput["CreatedDate"] = DateTime.MinValue;
                    rNewResultInput["InputVoucherID"] = string.Empty;
                    rNewResultInput["InputVoucherDetailID"] = string.Empty;
                    dtbResultInput.Rows.Add(rNewResultInput);
                }
                DataTable dtbResultOutput = new DataTable();
                if (dtbOutput != null && dtbOutput.Rows.Count > 0)
                {
                    dtbResultOutput = dtbOutput.Clone();
                    dtbResultOutput.TableName = "ResultOutput";
                    DataRow[] drOutput = dtbOutput.Select("ProductID <> ''");
                    if (drOutput.Length > 0)
                    {
                        foreach (DataRow rOutput in drOutput)
                        {
                            DataRow rNewOutput = dtbResultOutput.NewRow();
                            rNewOutput.ItemArray = rOutput.ItemArray;
                            decimal decCollectArrearPrice = Math.Round(Convert.ToDecimal(rOutput["CollectArrearPrice"]) / (1 + Convert.ToInt32(rOutput["VAT"]) * Convert.ToInt32(rOutput["VATPercent"]) * Convert.ToDecimal(0.0001)), 4);
                            rNewOutput["CollectArrearPrice"] = decCollectArrearPrice;
                            dtbResultOutput.Rows.Add(rNewOutput);
                        }
                    }
                }
                if (dtbResultOutput.Rows.Count == 0)
                {
                    dtbResultOutput = dtbOutput.Clone();
                    dtbResultOutput.TableName = "ResultOutput";
                    DataRow rNewResultOutput = dtbResultOutput.NewRow();
                    rNewResultOutput["OutputProcessID"] = string.Empty;
                    rNewResultOutput["InventoryProcessID"] = string.Empty;
                    rNewResultOutput["ProductID"] = string.Empty;
                    rNewResultOutput["ProductName"] = string.Empty;
                    rNewResultOutput["IMEI"] = string.Empty;
                    rNewResultOutput["Price"] = 0;
                    rNewResultOutput["Quantity"] = 0;
                    rNewResultOutput["VAT"] = 0;
                    rNewResultOutput["VATPercent"] = 0;
                    rNewResultOutput["TotalMoney"] = 0;
                    rNewResultOutput["CreatedDate"] = DateTime.MinValue;
                    rNewResultOutput["OutputVoucherID"] = string.Empty;
                    rNewResultOutput["OutputVoucherDetailID"] = string.Empty;
                    rNewResultOutput["CollectArrearPrice"] = 0;
                    rNewResultOutput["DutyUsers"] = string.Empty;
                    dtbResultOutput.Rows.Add(rNewResultOutput);
                }
                bool bolSave = objPLCInventoryProcess.UpdateInventoryProcess(objInventoryProcess.InventoryProcessID,
                    txtInputContent.Text.Trim(),
                    txtOutPutContent.Text.Trim(),
                    txtPrChangeContent.Text.Trim(),
                    txtSTChangeContent.Text.Trim(),
                    SystemConfig.intDefaultStoreID,
                    dtbResultInput, dtbResultOutput, dtbProductChange, dtbStatusChange,
                    ref dtbCollectArrear);
                isUpdateSucceed = bolSave;
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    cmdUpdate.Enabled = true;
                    return;
                }
                else
                {
                    if (dtbCollectArrear != null && dtbCollectArrear.Rows.Count > 0)
                    {
                        if (dtbCollectArrearOutMinus != null && dtbCollectArrearOutMinus.Rows.Count > 0)
                        {
                            foreach (DataRow rArrearOut in dtbCollectArrearOutMinus.Rows)
                            {
                                if (Convert.IsDBNull(rArrearOut["CollectArrearID"]) || Convert.ToString(rArrearOut["CollectArrearID"]) == string.Empty)
                                {
                                    DataRow[] drArrearOut = dtbCollectArrear.Select("RelateVoucherID = '" + rArrearOut["RelateVoucherID"] + "' AND UserName = '" + rArrearOut["UserName"] + "' AND IsDeleted = 0");
                                    if (drArrearOut.Length > 0)
                                    {
                                        rArrearOut["CollectArrearID"] = Convert.ToString(drArrearOut[0]["CollectArrearID"]);
                                    }
                                }
                            }
                        }
                        if (dtbCollectArrearPrChange != null && dtbCollectArrearPrChange.Rows.Count > 0)
                        {
                            foreach (DataRow rPrChange in dtbCollectArrearPrChange.Rows)
                            {
                                if (Convert.IsDBNull(rPrChange["CollectArrearID"]) || Convert.ToString(rPrChange["CollectArrearID"]) == string.Empty)
                                {
                                    DataRow[] drArrearPrChange = dtbCollectArrear.Select("RelateVoucherID = '" + rPrChange["RelateVoucherID"] + "' AND UserName = '" + rPrChange["UserName"] + "' AND IsDeleted = 0");
                                    if (drArrearPrChange.Length > 0)
                                    {
                                        rPrChange["CollectArrearID"] = Convert.ToString(drArrearPrChange[0]["CollectArrearID"]);
                                    }
                                }
                            }
                        }

                        if (dtbCollectArrearStChange != null && dtbCollectArrearStChange.Rows.Count > 0)
                        {
                            foreach (DataRow rStChange in dtbCollectArrearStChange.Rows)
                            {
                                if (Convert.IsDBNull(rStChange["CollectArrearID"]) || Convert.ToString(rStChange["CollectArrearID"]) == string.Empty)
                                {
                                    DataRow[] drArrearStChange = dtbCollectArrear.Select("RelateVoucherID = '" + rStChange["RelateVoucherID"] + "' AND UserName = '" + rStChange["UserName"] + "' AND IsDeleted = 0");
                                    if (drArrearStChange.Length > 0)
                                    {
                                        rStChange["CollectArrearID"] = Convert.ToString(drArrearStChange[0]["CollectArrearID"]);
                                    }
                                }
                            }
                        }
                    }
                    cmdUpdate.Enabled = true;
                    cmdReview.Enabled = bolIsRight_InventoryProcess_Review;
                }
            }
            catch (Exception objEx)
            {
                MessageBox.Show(this, "Lỗi cập nhật phiếu yêu cầu xử lý kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cmdUpdate.Enabled = true;
            }
            #endregion

            if (isUpdateSucceed)
            {
                #region Approve
                cmdReview.Enabled = false;
                try
                {
                    bool bolSave = objPLCInventoryProcess.ReviewedInventProcess(objInventoryProcess.InventoryProcessID, true);
                    if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        cmdReview.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show(this, "Đã duyệt phiếu yêu cầu xử lý kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmdUpdate.Enabled = false;
                        cmdReview.Enabled = false;
                        cmdProcess.Enabled = bolIsRight_InventoryProcess_Process;
                    }
                }
                catch (Exception objEx)
                {
                    MessageBox.Show(this, "Lỗi duyệt phiếu yêu cầu xử lý kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cmdReview.Enabled = true;
                }
                #endregion
            }
        }

        private void flexProductChange_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexProductChange.Rows.Count <= flexProductChange.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexProductChange.Rows.Fixed)
            {
                return;
            }
            if (objInventoryProcess.IsProcess || objInventoryProcess.IsReviewed)
            {
                e.Cancel = true;
            }
            if (Convert.ToDecimal(flexProductChange[e.Row, "UnEventAmount"]) == 0)
            {
                e.Cancel = true;
            }
            if (e.Col == flexProductChange.Cols["DutyUsers"].Index)
            {
                if (Convert.ToDecimal(flexProductChange[e.Row, "CollectArrearPrice"]) == 0)
                {
                    e.Cancel = true;
                }
            }
        }

        private void flexInPlus_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexInPlus.Rows.Count <= flexInPlus.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexInPlus.Rows.Fixed)
            {
                return;
            }
            if (objInventoryProcess.IsProcess || objInventoryProcess.IsReviewed)
            {
                e.Cancel = true;
            }
        }
        private void flexOutMinus_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexOutMinus.Rows.Count <= flexOutMinus.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexOutMinus.Rows.Fixed)
            {
                return;
            }
            if (objInventoryProcess.IsProcess || objInventoryProcess.IsReviewed)
            {
                e.Cancel = true;
            }
            if (Convert.ToDecimal(flexOutMinus[e.Row, "TotalMoney"]) == 0)
            {
                if (e.Col == flexOutMinus.Cols["CollectArrearPrice"].Index || e.Col == flexOutMinus.Cols["DutyUsers"].Index)
                {
                    e.Cancel = true;
                }
            }
            if (e.Col == flexOutMinus.Cols["DutyUsers"].Index)
            {
                if (Convert.ToDecimal(flexOutMinus[e.Row, "CollectArrearPrice"]) == 0)
                {
                    e.Cancel = true;
                }
            }
        }
        private void cmdDelete_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(txtInventoryProcessID.Text).Trim() == string.Empty)
            {
                return;
            }
            string strDeletedReason = Convert.ToString(txtContentDelete.Text).Trim();
            if (strDeletedReason == string.Empty)
            {
                txtContentDelete.Focus();
                MessageBox.Show(this, "Vui lòng nhập lý do hủy.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (!objPLCInventoryProcess.DeleteInventoryProcess(Convert.ToString(txtInventoryProcessID.Text).Trim(), strDeletedReason))
            {
                cmdDelete.Enabled = bolIsRight_InventoryProcess_Delete;
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
            }
            MessageBox.Show(this, "Hủy thông tin phiếu xử lý kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }
        private void grdViewDataProductChange_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

        }
        private void grdViewDataProductChange_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "REFSALEPRICE_OUT" || e.Column.FieldName == "REFSALEPRICE_IN")
            {
                DataRow r = grdViewDataProductChange.GetDataRow(e.RowHandle);
                decimal quantity = Convert.ToDecimal(r["QUANTITY"]);
                decimal outPrice = Convert.ToDecimal(r["REFSALEPRICE_OUT"]);
                decimal inPrice = Convert.ToDecimal(r["REFSALEPRICE_IN"]);
                decimal totalMoenyOut = outPrice * quantity;
                decimal totalMoenyIn = inPrice * quantity;
                r["UNEVENTAMOUNT"] = totalMoenyOut - totalMoenyIn;
                r["COLLECTARREARPRICE"] = totalMoenyOut - totalMoenyIn;
            }
        }
        private void btnSTDuytUser_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (objInventoryProcess.IsProcess || objInventoryProcess.IsReviewed)
                return;

            DataRow row = gridViewSTChange.GetFocusedDataRow();
            if (row != null)
            {
                decimal decCollectArrearMoney = 0;
                if (!Convert.IsDBNull(row["CollectArrearPrice"]))
                    decCollectArrearMoney = Math.Round(Convert.ToDecimal(row["CollectArrearPrice"]));

                if (decCollectArrearMoney <= 0)
                    return;

                frmDutyUsers frmSTDutyUsers = new frmDutyUsers();
                frmSTDutyUsers.InventoryProcessID = objInventoryProcess.InventoryProcessID;
                frmSTDutyUsers.CollectArrearMoney = decCollectArrearMoney;
                if (!Convert.IsDBNull(row["ProductChangeProcessID"]))
                    frmSTDutyUsers.RelateVoucherID = Convert.ToString(row["ProductChangeProcessID"]);
                frmSTDutyUsers.CollectArrearType = 3;
                frmSTDutyUsers.dtbDutyUsers = objPLCInventoryProcess.LoadInventProcessArrear(objInventoryProcess.InventoryProcessID, frmSTDutyUsers.RelateVoucherID);

                if (frmSTDutyUsers.dtbDutyUsers == null || frmSTDutyUsers.dtbDutyUsers.Rows.Count == 0)
                {
                    #region 1
                    if (dtbCollectArrearStChange == null || dtbCollectArrearStChange.Rows.Count == 0)
                    {
                        dtbCollectArrearStChange = new DataTable();
                        dtbCollectArrearStChange.Columns.Add("IsSelect", typeof(bool));
                        dtbCollectArrearStChange.Columns.Add("CollectArrearID", typeof(string));
                        dtbCollectArrearStChange.Columns.Add("InventoryProcessID", typeof(string));
                        dtbCollectArrearStChange.Columns.Add("CollectArrearType", typeof(int));
                        dtbCollectArrearStChange.Columns.Add("RelateVoucherID", typeof(string));
                        dtbCollectArrearStChange.Columns.Add("UserName", typeof(string));
                        dtbCollectArrearStChange.Columns.Add("FullName", typeof(string));
                        dtbCollectArrearStChange.Columns.Add("CollectArrearMoney", typeof(decimal));
                        dtbCollectArrearStChange.Columns.Add("Note", typeof(string));
                        dtbCollectArrearStChange.Columns.Add("IsDeleted", typeof(bool));
                        dtbCollectArrearStChange.Columns.Add("DeletedUser", typeof(string));
                    }
                    else
                    {
                        if (dtbCollectArrearStChange.Rows.Count > 0)
                        {
                            frmSTDutyUsers.dtbDutyUsers = dtbCollectArrearStChange.Clone();
                            DataRow[] drProductChange = dtbCollectArrearStChange.Select("IsDeleted = 0 AND RelateVoucherID = '" + Convert.ToString(row["ProductChangeProcessID"]) + "'");
                            if (drProductChange.Length > 0)
                            {
                                foreach (DataRow rProductChange in drProductChange)
                                {
                                    try
                                    {
                                        DataRow[] drUser = dtbUsers.Select("UserName = '" + Convert.ToString(rProductChange["UserName"]) + "'");
                                        if (drUser.Length > 0)
                                            rProductChange["FullName"] = Convert.ToString(drUser[0]["FullName"]).Trim();
                                    }
                                    catch { }
                                    DataRow rNewProductChange = frmSTDutyUsers.dtbDutyUsers.NewRow();
                                    rNewProductChange.ItemArray = rProductChange.ItemArray;
                                    frmSTDutyUsers.dtbDutyUsers.Rows.Add(rNewProductChange);
                                }
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region 2
                    if (dtbCollectArrearStChange == null || dtbCollectArrearStChange.Rows.Count == 0)
                    {
                        dtbCollectArrearStChange = new DataTable();
                        dtbCollectArrearStChange.Columns.Add("IsSelect", typeof(bool));
                        dtbCollectArrearStChange.Columns.Add("CollectArrearID", typeof(string));
                        dtbCollectArrearStChange.Columns.Add("InventoryProcessID", typeof(string));
                        dtbCollectArrearStChange.Columns.Add("CollectArrearType", typeof(int));
                        dtbCollectArrearStChange.Columns.Add("RelateVoucherID", typeof(string));
                        dtbCollectArrearStChange.Columns.Add("UserName", typeof(string));
                        dtbCollectArrearStChange.Columns.Add("FullName", typeof(string));
                        dtbCollectArrearStChange.Columns.Add("CollectArrearMoney", typeof(decimal));
                        dtbCollectArrearStChange.Columns.Add("Note", typeof(string));
                        dtbCollectArrearStChange.Columns.Add("IsDeleted", typeof(bool));
                        dtbCollectArrearStChange.Columns.Add("DeletedUser", typeof(string));
                    }
                    DataRow[] drArrearUsers = frmSTDutyUsers.dtbDutyUsers.Select("InventoryProcessID <> ''");
                    foreach (DataRow rArrear in drArrearUsers)
                    {
                        DataRow[] drPrChange = dtbCollectArrearStChange.Select("RelateVoucherID = '" + Convert.ToString(row["ProductChangeProcessID"]) + "' AND UserName = '" + Convert.ToString(rArrear["UserName"]) + "'");
                        if (drPrChange.Length == 0)
                        {
                            DataRow rNewArrear = dtbCollectArrearStChange.NewRow();
                            rNewArrear.ItemArray = rArrear.ItemArray;
                            dtbCollectArrearStChange.Rows.Add(rNewArrear);
                        }
                    }
                    DataRow[] drArrear = dtbCollectArrearStChange.Select("IsDeleted = 0 AND RelateVoucherID = '" + Convert.ToString(row["ProductChangeProcessID"]) + "'");
                    if (drArrear.Length > 0)
                    {
                        frmSTDutyUsers.dtbDutyUsers = new DataTable();
                        frmSTDutyUsers.dtbDutyUsers.Columns.Add("IsSelect", typeof(bool));
                        frmSTDutyUsers.dtbDutyUsers.Columns.Add("CollectArrearID", typeof(string));
                        frmSTDutyUsers.dtbDutyUsers.Columns.Add("InventoryProcessID", typeof(string));
                        frmSTDutyUsers.dtbDutyUsers.Columns.Add("CollectArrearType", typeof(int));
                        frmSTDutyUsers.dtbDutyUsers.Columns.Add("RelateVoucherID", typeof(string));
                        frmSTDutyUsers.dtbDutyUsers.Columns.Add("UserName", typeof(string));
                        frmSTDutyUsers.dtbDutyUsers.Columns.Add("FullName", typeof(string));
                        frmSTDutyUsers.dtbDutyUsers.Columns.Add("CollectArrearMoney", typeof(decimal));
                        frmSTDutyUsers.dtbDutyUsers.Columns.Add("Note", typeof(string));
                        frmSTDutyUsers.dtbDutyUsers.Columns.Add("IsDeleted", typeof(bool));
                        frmSTDutyUsers.dtbDutyUsers.Columns.Add("DeletedUser", typeof(string));

                        foreach (DataRow rArrear in drArrear)
                        {
                            try
                            {
                                DataRow[] drUser = dtbUsers.Select("UserName = '" + Convert.ToString(rArrear["UserName"]) + "'");
                                if (drUser.Length > 0)
                                    rArrear["FullName"] = Convert.ToString(drUser[0]["FullName"]).Trim();
                            }
                            catch { }
                            DataRow rNewArrear = frmSTDutyUsers.dtbDutyUsers.NewRow();
                            rNewArrear.ItemArray = rArrear.ItemArray;
                            frmSTDutyUsers.dtbDutyUsers.Rows.Add(rNewArrear);
                        }
                    }
                    #endregion
                }

                if (frmSTDutyUsers.CollectArrearMoney > 0)
                    frmSTDutyUsers.Text = "Số tiền truy thu là " + frmSTDutyUsers.CollectArrearMoney.ToString("#,##0") + " VNĐ";

                frmSTDutyUsers.ShowDialog();
                if (frmSTDutyUsers.DutyUsers != string.Empty)
                {
                    row["DutyUsers"] = frmSTDutyUsers.DutyUsers;

                    if (frmSTDutyUsers.dtbDutyUsers != null && frmSTDutyUsers.dtbDutyUsers.Rows.Count > 0)
                    {
                        if (dtbCollectArrearStChange.Rows.Count > 0)
                        {
                            for (int i = dtbCollectArrearStChange.Rows.Count - 1; i > -1; i--)
                            {
                                if (!Convert.IsDBNull(dtbCollectArrearStChange.Rows[i]["RelateVoucherID"]) && Convert.ToString(dtbCollectArrearStChange.Rows[i]["RelateVoucherID"]) == frmSTDutyUsers.RelateVoucherID)
                                {
                                    DataRow[] drPrChange = frmSTDutyUsers.dtbDutyUsers.Select("RelateVoucherID = '" + frmSTDutyUsers.RelateVoucherID + "' AND UserName = '" + Convert.ToString(dtbCollectArrearStChange.Rows[i]["UserName"]) + "'");
                                    if (drPrChange.Length == 0)
                                    {
                                        dtbCollectArrearStChange.Rows[i]["IsDeleted"] = true;
                                        dtbCollectArrearStChange.Rows[i]["DeletedUser"] = SystemConfig.objSessionUser.UserName;
                                    }
                                }
                            }
                        }
                        for (int i = 0; i < frmSTDutyUsers.dtbDutyUsers.Rows.Count; i++)
                        {
                            DataRow rDutyUser = frmSTDutyUsers.dtbDutyUsers.Rows[i];
                            if (!Convert.IsDBNull(rDutyUser["CollectArrearID"]) && Convert.ToString(rDutyUser["CollectArrearID"]) != string.Empty)
                            {
                                DataRow[] drCollectArrear = dtbCollectArrearStChange.Select("CollectArrearID = '" + Convert.ToString(rDutyUser["CollectArrearID"]) + "'");
                                if (drCollectArrear.Length > 0)
                                {
                                    foreach (DataRow rCollectArrear in dtbCollectArrearStChange.Rows)
                                    {
                                        if (Convert.ToString(rCollectArrear["CollectArrearID"]) == Convert.ToString(rDutyUser["CollectArrearID"]))
                                        {
                                            rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                            rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                            rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                            rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                            rCollectArrear["IsDeleted"] = false;
                                            rCollectArrear["DeletedUser"] = string.Empty;
                                            rCollectArrear["IsSelect"] = false;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    DataRow rCollectArrear = dtbCollectArrearStChange.NewRow();
                                    rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                    rCollectArrear["CollectArrearType"] = 3;
                                    rCollectArrear["RelateVoucherID"] = frmSTDutyUsers.RelateVoucherID;
                                    rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                    rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                    rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                    rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                    rCollectArrear["IsDeleted"] = false;
                                    rCollectArrear["DeletedUser"] = string.Empty;
                                    rCollectArrear["IsSelect"] = false;
                                    dtbCollectArrearStChange.Rows.Add(rCollectArrear);
                                }
                            }
                            else
                            {
                                if (dtbCollectArrearStChange.Rows.Count > 0)
                                {
                                    DataRow[] drCollectArrear = dtbCollectArrearStChange.Select("RelateVoucherID = '" + frmSTDutyUsers.RelateVoucherID + "' AND UserName = '" + Convert.ToString(rDutyUser["UserName"]) + "'");
                                    if (drCollectArrear.Length > 0)
                                    {
                                        foreach (DataRow rCollectArrear in dtbCollectArrearStChange.Rows)
                                        {
                                            if (Convert.ToString(rCollectArrear["UserName"]) == Convert.ToString(rDutyUser["UserName"]))
                                            {
                                                rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                                rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                                rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                                rCollectArrear["IsDeleted"] = false;
                                                rCollectArrear["DeletedUser"] = string.Empty;
                                                rCollectArrear["IsSelect"] = false;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DataRow rCollectArrear = dtbCollectArrearStChange.NewRow();
                                        rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                        rCollectArrear["CollectArrearType"] = 3;
                                        rCollectArrear["RelateVoucherID"] = frmSTDutyUsers.RelateVoucherID;
                                        rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                        rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                        rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                        rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                        rCollectArrear["IsDeleted"] = false;
                                        rCollectArrear["DeletedUser"] = string.Empty;
                                        rCollectArrear["IsSelect"] = false;
                                        dtbCollectArrearStChange.Rows.Add(rCollectArrear);
                                    }
                                }
                                else
                                {
                                    DataRow rCollectArrear = dtbCollectArrearStChange.NewRow();
                                    rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                    rCollectArrear["CollectArrearType"] = 3;
                                    rCollectArrear["RelateVoucherID"] = frmSTDutyUsers.RelateVoucherID;
                                    rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                    rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                    rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                    rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                    rCollectArrear["IsDeleted"] = false;
                                    rCollectArrear["DeletedUser"] = string.Empty;
                                    rCollectArrear["IsSelect"] = false;
                                    dtbCollectArrearStChange.Rows.Add(rCollectArrear);
                                }
                            }
                        }
                    }
                }
            }
        }
        private void btnOutDuytUsers_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (objInventoryProcess.IsProcess || objInventoryProcess.IsReviewed)
                return;

            DataRow row = gridViewOutMinus.GetFocusedDataRow();
            if (row == null) return;

            decimal decCollectArrearMoney = 0;
            if (!Convert.IsDBNull(row["CollectArrearPrice"])) decCollectArrearMoney = Math.Round(Convert.ToDecimal(row["CollectArrearPrice"]));
            if (decCollectArrearMoney <= 0)
                return;

            frmDutyUsers frmDutyUsers1 = new frmDutyUsers();
            frmDutyUsers1.InventoryProcessID = objInventoryProcess.InventoryProcessID;
            frmDutyUsers1.CollectArrearMoney = decCollectArrearMoney;
            if (!Convert.IsDBNull(row["OutputProcessID"])) frmDutyUsers1.RelateVoucherID = Convert.ToString(row["OutputProcessID"]);
            frmDutyUsers1.CollectArrearType = 1;
            frmDutyUsers1.dtbDutyUsers = objPLCInventoryProcess.LoadInventProcessArrear(objInventoryProcess.InventoryProcessID, frmDutyUsers1.RelateVoucherID);
            if (frmDutyUsers1.dtbDutyUsers == null || frmDutyUsers1.dtbDutyUsers.Rows.Count == 0)
            {
                if (dtbCollectArrearOutMinus == null || dtbCollectArrearOutMinus.Rows.Count == 0)
                {
                    dtbCollectArrearOutMinus = new DataTable();
                    dtbCollectArrearOutMinus.Columns.Add("IsSelect", typeof(bool));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("InventoryProcessID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearType", typeof(int));
                    dtbCollectArrearOutMinus.Columns.Add("RelateVoucherID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("UserName", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("FullName", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearMoney", typeof(decimal));
                    dtbCollectArrearOutMinus.Columns.Add("Note", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("IsDeleted", typeof(bool));
                    dtbCollectArrearOutMinus.Columns.Add("DeletedUser", typeof(string));
                }
                else
                {
                    if (dtbCollectArrearOutMinus.Rows.Count > 0)
                    {
                        frmDutyUsers1.dtbDutyUsers = dtbCollectArrearOutMinus.Clone();
                        DataRow[] drOutMinus = dtbCollectArrearOutMinus.Select("IsDeleted = 0 AND RelateVoucherID = '" + Convert.ToString(row["OutputProcessID"]) + "'");
                        if (drOutMinus.Length > 0)
                        {
                            foreach (DataRow rOutMinus in drOutMinus)
                            {
                                try
                                {
                                    DataRow[] drUser = dtbUsers.Select("UserName = '" + Convert.ToString(rOutMinus["UserName"]) + "'");
                                    if (drUser.Length > 0)
                                        rOutMinus["FullName"] = Convert.ToString(drUser[0]["FullName"]).Trim();
                                }
                                catch { }
                                DataRow rNewOutMinus = frmDutyUsers1.dtbDutyUsers.NewRow();
                                rNewOutMinus.ItemArray = rOutMinus.ItemArray;
                                frmDutyUsers1.dtbDutyUsers.Rows.Add(rNewOutMinus);
                            }
                        }
                    }
                }
            }
            else
            {
                if (dtbCollectArrearOutMinus == null || dtbCollectArrearOutMinus.Rows.Count == 0)
                {
                    dtbCollectArrearOutMinus = new DataTable();
                    dtbCollectArrearOutMinus.Columns.Add("IsSelect", typeof(bool));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("InventoryProcessID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearType", typeof(int));
                    dtbCollectArrearOutMinus.Columns.Add("RelateVoucherID", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("UserName", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("FullName", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("CollectArrearMoney", typeof(decimal));
                    dtbCollectArrearOutMinus.Columns.Add("Note", typeof(string));
                    dtbCollectArrearOutMinus.Columns.Add("IsDeleted", typeof(bool));
                    dtbCollectArrearOutMinus.Columns.Add("DeletedUser", typeof(string));
                }
                DataRow[] drArrearUsers = frmDutyUsers1.dtbDutyUsers.Select("InventoryProcessID <> ''");
                foreach (DataRow rArrear in drArrearUsers)
                {
                    DataRow[] drOutMinus = dtbCollectArrearOutMinus.Select("RelateVoucherID = '" + Convert.ToString(row["OutputProcessID"]) + "' AND UserName = '" + Convert.ToString(rArrear["UserName"]) + "'");
                    if (drOutMinus.Length == 0)
                    {
                        DataRow rNewArrear = dtbCollectArrearOutMinus.NewRow();
                        rNewArrear.ItemArray = rArrear.ItemArray;
                        dtbCollectArrearOutMinus.Rows.Add(rNewArrear);
                    }
                }
                DataRow[] drArrear = dtbCollectArrearOutMinus.Select("IsDeleted = 0 AND RelateVoucherID = '" + Convert.ToString(row["OutputProcessID"]) + "'");
                if (drArrear.Length > 0)
                {
                    frmDutyUsers1.dtbDutyUsers = new DataTable();
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("IsSelect", typeof(bool));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("CollectArrearID", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("InventoryProcessID", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("CollectArrearType", typeof(int));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("RelateVoucherID", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("UserName", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("FullName", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("CollectArrearMoney", typeof(decimal));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("Note", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("IsDeleted", typeof(bool));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("DeletedUser", typeof(string));

                    foreach (DataRow rArrear in drArrear)
                    {
                        try
                        {
                            DataRow[] drUser = dtbUsers.Select("UserName = '" + Convert.ToString(rArrear["UserName"]) + "'");
                            if (drUser.Length > 0)
                            {
                                rArrear["FullName"] = Convert.ToString(drUser[0]["FullName"]).Trim();
                            }
                        }
                        catch { }
                        DataRow rNewArrear = frmDutyUsers1.dtbDutyUsers.NewRow();
                        rNewArrear.ItemArray = rArrear.ItemArray;
                        frmDutyUsers1.dtbDutyUsers.Rows.Add(rNewArrear);
                    }
                }
            }
            if (frmDutyUsers1.CollectArrearMoney > 0)
            {
                frmDutyUsers1.Text = "Số tiền truy thu là " + frmDutyUsers1.CollectArrearMoney.ToString("#,##0") + " VNĐ";
            }
            frmDutyUsers1.ShowDialog();
            if (frmDutyUsers1.DutyUsers != string.Empty)
            {
                row["DutyUsers"] = frmDutyUsers1.DutyUsers;

                if (frmDutyUsers1.dtbDutyUsers != null && frmDutyUsers1.dtbDutyUsers.Rows.Count > 0)
                {
                    if (dtbCollectArrearOutMinus.Rows.Count > 0)
                    {
                        for (int i = dtbCollectArrearOutMinus.Rows.Count - 1; i > -1; i--)
                        {
                            if (!Convert.IsDBNull(dtbCollectArrearOutMinus.Rows[i]["RelateVoucherID"]) && Convert.ToString(dtbCollectArrearOutMinus.Rows[i]["RelateVoucherID"]) == frmDutyUsers1.RelateVoucherID)
                            {
                                DataRow[] drOutMinus = frmDutyUsers1.dtbDutyUsers.Select("RelateVoucherID = '" + frmDutyUsers1.RelateVoucherID + "' AND UserName = '" + Convert.ToString(dtbCollectArrearOutMinus.Rows[i]["UserName"]) + "'");
                                if (drOutMinus.Length == 0)
                                {
                                    dtbCollectArrearOutMinus.Rows[i]["IsDeleted"] = true;
                                    dtbCollectArrearOutMinus.Rows[i]["DeletedUser"] = SystemConfig.objSessionUser.UserName;
                                }
                            }
                        }
                    }
                    for (int i = 0; i < frmDutyUsers1.dtbDutyUsers.Rows.Count; i++)
                    {
                        DataRow rDutyUser = frmDutyUsers1.dtbDutyUsers.Rows[i];
                        if (!Convert.IsDBNull(rDutyUser["CollectArrearID"]) && Convert.ToString(rDutyUser["CollectArrearID"]) != string.Empty)
                        {
                            DataRow[] drCollectArrear = dtbCollectArrearOutMinus.Select("CollectArrearID = '" + Convert.ToString(rDutyUser["CollectArrearID"]) + "'");
                            if (drCollectArrear.Length > 0)
                            {
                                foreach (DataRow rCollectArrear in dtbCollectArrearOutMinus.Rows)
                                {
                                    if (Convert.ToString(rCollectArrear["CollectArrearID"]) == Convert.ToString(rDutyUser["CollectArrearID"]))
                                    {
                                        rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                        rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                        rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                        rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                        rCollectArrear["IsDeleted"] = false;
                                        rCollectArrear["DeletedUser"] = string.Empty;
                                        rCollectArrear["IsSelect"] = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                DataRow rCollectArrear = dtbCollectArrearOutMinus.NewRow();
                                rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                rCollectArrear["CollectArrearType"] = 1;
                                rCollectArrear["RelateVoucherID"] = frmDutyUsers1.RelateVoucherID;
                                rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                rCollectArrear["IsDeleted"] = false;
                                rCollectArrear["DeletedUser"] = string.Empty;
                                rCollectArrear["IsSelect"] = false;
                                dtbCollectArrearOutMinus.Rows.Add(rCollectArrear);
                            }
                        }
                        else
                        {
                            if (dtbCollectArrearOutMinus.Rows.Count > 0)
                            {
                                DataRow[] drCollectArrear = dtbCollectArrearOutMinus.Select("RelateVoucherID = '" + frmDutyUsers1.RelateVoucherID + "' AND UserName = '" + Convert.ToString(rDutyUser["UserName"]) + "'");
                                if (drCollectArrear.Length > 0)
                                {
                                    foreach (DataRow rCollectArrear in dtbCollectArrearOutMinus.Rows)
                                    {
                                        if (Convert.ToString(rCollectArrear["UserName"]) == Convert.ToString(rDutyUser["UserName"]))
                                        {
                                            rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                            rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                            rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                            rCollectArrear["IsDeleted"] = false;
                                            rCollectArrear["DeletedUser"] = string.Empty;
                                            rCollectArrear["IsSelect"] = false;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    DataRow rCollectArrear = dtbCollectArrearOutMinus.NewRow();
                                    rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                    rCollectArrear["CollectArrearType"] = 1;
                                    rCollectArrear["RelateVoucherID"] = frmDutyUsers1.RelateVoucherID;
                                    rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                    rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                    rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                    rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                    rCollectArrear["IsDeleted"] = false;
                                    rCollectArrear["DeletedUser"] = string.Empty;
                                    rCollectArrear["IsSelect"] = false;
                                    dtbCollectArrearOutMinus.Rows.Add(rCollectArrear);
                                }
                            }
                            else
                            {
                                DataRow rCollectArrear = dtbCollectArrearOutMinus.NewRow();
                                rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                rCollectArrear["CollectArrearType"] = 2;
                                rCollectArrear["RelateVoucherID"] = frmDutyUsers1.RelateVoucherID;
                                rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                rCollectArrear["IsDeleted"] = false;
                                rCollectArrear["DeletedUser"] = string.Empty;
                                rCollectArrear["IsSelect"] = false;
                                dtbCollectArrearOutMinus.Rows.Add(rCollectArrear);
                            }
                        }
                    }
                }
            }
        }
        private void gridViewOutMinus_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
        }
        private void gridViewOutMinus_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "PRICE")
            {
                DataRow r = gridViewOutMinus.GetDataRow(e.RowHandle);
                decimal quantity = Convert.ToDecimal(r["QUANTITY"]);
                decimal price = Convert.ToDecimal(e.Value);
                decimal vat = Convert.ToDecimal(r["VAT"]);
                decimal vatPercent = Convert.ToDecimal(r["VATPERCENT"]);
                decimal totalMoney = price * (1 + vat * vatPercent * Convert.ToDecimal(0.0001)) * quantity;
                r["TotalMoney"] = totalMoney;
                r["CollectArrearPrice"] = totalMoney;
            }
        }
        private void gridViewInPlus_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "PRICE")
            {
                DataRow r = gridViewInPlus.GetDataRow(e.RowHandle);
                decimal quantity = Convert.ToDecimal(r["QUANTITY"]);
                decimal price = Convert.ToDecimal(e.Value);
                decimal vat = Convert.ToDecimal(r["VAT"]);
                decimal vatPercent = Convert.ToDecimal(r["VATPERCENT"]);
                decimal totalMoney = price * (1 + vat * vatPercent * Convert.ToDecimal(0.0001)) * quantity;
                r["TotalMoney"] = totalMoney;
            }
        }
        private void btnPrChangeDuytUsers_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (objInventoryProcess.IsProcess || objInventoryProcess.IsReviewed)
                return;

            DataRow row = grdViewDataProductChange.GetFocusedDataRow();
            if (row == null) return;

            decimal decCollectArrearMoney = 0;
            if (!Convert.IsDBNull(row["CollectArrearPrice"])) decCollectArrearMoney = Math.Round(Convert.ToDecimal(row["CollectArrearPrice"]));
            if (decCollectArrearMoney <= 0)
                return;

            frmDutyUsers frmDutyUsers1 = new frmDutyUsers();
            frmDutyUsers1.InventoryProcessID = objInventoryProcess.InventoryProcessID;
            frmDutyUsers1.CollectArrearMoney = decCollectArrearMoney;
            if (!Convert.IsDBNull(row["ProductChangeProcessID"])) frmDutyUsers1.RelateVoucherID = Convert.ToString(row["ProductChangeProcessID"]);
            frmDutyUsers1.CollectArrearType = 2;
            frmDutyUsers1.dtbDutyUsers = objPLCInventoryProcess.LoadInventProcessArrear(objInventoryProcess.InventoryProcessID, frmDutyUsers1.RelateVoucherID);
            if (frmDutyUsers1.dtbDutyUsers == null || frmDutyUsers1.dtbDutyUsers.Rows.Count == 0)
            {
                if (dtbCollectArrearPrChange == null || dtbCollectArrearPrChange.Rows.Count == 0)
                {
                    dtbCollectArrearPrChange = new DataTable();
                    dtbCollectArrearPrChange.Columns.Add("IsSelect", typeof(bool));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("InventoryProcessID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearType", typeof(int));
                    dtbCollectArrearPrChange.Columns.Add("RelateVoucherID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("UserName", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("FullName", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearMoney", typeof(decimal));
                    dtbCollectArrearPrChange.Columns.Add("Note", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("IsDeleted", typeof(bool));
                    dtbCollectArrearPrChange.Columns.Add("DeletedUser", typeof(string));
                }
                else
                {
                    if (dtbCollectArrearPrChange.Rows.Count > 0)
                    {
                        frmDutyUsers1.dtbDutyUsers = dtbCollectArrearPrChange.Clone();
                        DataRow[] drProductChange = dtbCollectArrearPrChange.Select("IsDeleted = 0 AND RelateVoucherID = '" + Convert.ToString(row["ProductChangeProcessID"]) + "'");
                        if (drProductChange.Length > 0)
                        {
                            foreach (DataRow rProductChange in drProductChange)
                            {
                                try
                                {
                                    DataRow[] drUser = dtbUsers.Select("UserName = '" + Convert.ToString(rProductChange["UserName"]) + "'");
                                    if (drUser.Length > 0)
                                    {
                                        rProductChange["FullName"] = Convert.ToString(drUser[0]["FullName"]).Trim();
                                    }
                                }
                                catch { }
                                DataRow rNewProductChange = frmDutyUsers1.dtbDutyUsers.NewRow();
                                rNewProductChange.ItemArray = rProductChange.ItemArray;
                                frmDutyUsers1.dtbDutyUsers.Rows.Add(rNewProductChange);
                            }
                        }
                    }
                }
            }
            else
            {
                if (dtbCollectArrearPrChange == null || dtbCollectArrearPrChange.Rows.Count == 0)
                {
                    dtbCollectArrearPrChange = new DataTable();
                    dtbCollectArrearPrChange.Columns.Add("IsSelect", typeof(bool));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("InventoryProcessID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearType", typeof(int));
                    dtbCollectArrearPrChange.Columns.Add("RelateVoucherID", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("UserName", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("FullName", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("CollectArrearMoney", typeof(decimal));
                    dtbCollectArrearPrChange.Columns.Add("Note", typeof(string));
                    dtbCollectArrearPrChange.Columns.Add("IsDeleted", typeof(bool));
                    dtbCollectArrearPrChange.Columns.Add("DeletedUser", typeof(string));
                }
                DataRow[] drArrearUsers = frmDutyUsers1.dtbDutyUsers.Select("InventoryProcessID <> ''");
                foreach (DataRow rArrear in drArrearUsers)
                {
                    DataRow[] drPrChange = dtbCollectArrearPrChange.Select("RelateVoucherID = '" + Convert.ToString(row["ProductChangeProcessID"]) + "' AND UserName = '" + Convert.ToString(rArrear["UserName"]) + "'");
                    if (drPrChange.Length == 0)
                    {
                        DataRow rNewArrear = dtbCollectArrearPrChange.NewRow();
                        rNewArrear.ItemArray = rArrear.ItemArray;
                        dtbCollectArrearPrChange.Rows.Add(rNewArrear);
                    }
                }
                DataRow[] drArrear = dtbCollectArrearPrChange.Select("IsDeleted = 0 AND RelateVoucherID = '" + Convert.ToString(row["ProductChangeProcessID"]) + "'");
                if (drArrear.Length > 0)
                {
                    frmDutyUsers1.dtbDutyUsers = new DataTable();
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("IsSelect", typeof(bool));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("CollectArrearID", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("InventoryProcessID", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("CollectArrearType", typeof(int));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("RelateVoucherID", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("UserName", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("FullName", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("CollectArrearMoney", typeof(decimal));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("Note", typeof(string));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("IsDeleted", typeof(bool));
                    frmDutyUsers1.dtbDutyUsers.Columns.Add("DeletedUser", typeof(string));

                    foreach (DataRow rArrear in drArrear)
                    {
                        try
                        {
                            DataRow[] drUser = dtbUsers.Select("UserName = '" + Convert.ToString(rArrear["UserName"]) + "'");
                            if (drUser.Length > 0)
                            {
                                rArrear["FullName"] = Convert.ToString(drUser[0]["FullName"]).Trim();
                            }
                        }
                        catch { }
                        DataRow rNewArrear = frmDutyUsers1.dtbDutyUsers.NewRow();
                        rNewArrear.ItemArray = rArrear.ItemArray;
                        frmDutyUsers1.dtbDutyUsers.Rows.Add(rNewArrear);
                    }
                }
            }
            if (frmDutyUsers1.CollectArrearMoney > 0)
            {
                frmDutyUsers1.Text = "Số tiền truy thu là " + frmDutyUsers1.CollectArrearMoney.ToString("#,##0") + " VNĐ";
            }
            frmDutyUsers1.ShowDialog();
            if (frmDutyUsers1.DutyUsers != string.Empty)
            {
                row["DutyUsers"] = frmDutyUsers1.DutyUsers;

                if (frmDutyUsers1.dtbDutyUsers != null && frmDutyUsers1.dtbDutyUsers.Rows.Count > 0)
                {
                    if (dtbCollectArrearPrChange.Rows.Count > 0)
                    {
                        for (int i = dtbCollectArrearPrChange.Rows.Count - 1; i > -1; i--)
                        {
                            if (!Convert.IsDBNull(dtbCollectArrearPrChange.Rows[i]["RelateVoucherID"]) && Convert.ToString(dtbCollectArrearPrChange.Rows[i]["RelateVoucherID"]) == frmDutyUsers1.RelateVoucherID)
                            {
                                DataRow[] drPrChange = frmDutyUsers1.dtbDutyUsers.Select("RelateVoucherID = '" + frmDutyUsers1.RelateVoucherID + "' AND UserName = '" + Convert.ToString(dtbCollectArrearPrChange.Rows[i]["UserName"]) + "'");
                                if (drPrChange.Length == 0)
                                {
                                    dtbCollectArrearPrChange.Rows[i]["IsDeleted"] = true;
                                    dtbCollectArrearPrChange.Rows[i]["DeletedUser"] = SystemConfig.objSessionUser.UserName;
                                }
                            }
                        }
                    }
                    for (int i = 0; i < frmDutyUsers1.dtbDutyUsers.Rows.Count; i++)
                    {
                        DataRow rDutyUser = frmDutyUsers1.dtbDutyUsers.Rows[i];
                        if (!Convert.IsDBNull(rDutyUser["CollectArrearID"]) && Convert.ToString(rDutyUser["CollectArrearID"]) != string.Empty)
                        {
                            DataRow[] drCollectArrear = dtbCollectArrearPrChange.Select("CollectArrearID = '" + Convert.ToString(rDutyUser["CollectArrearID"]) + "'");
                            if (drCollectArrear.Length > 0)
                            {
                                foreach (DataRow rCollectArrear in dtbCollectArrearPrChange.Rows)
                                {
                                    if (Convert.ToString(rCollectArrear["CollectArrearID"]) == Convert.ToString(rDutyUser["CollectArrearID"]))
                                    {
                                        rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                        rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                        rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                        rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                        rCollectArrear["IsDeleted"] = false;
                                        rCollectArrear["DeletedUser"] = string.Empty;
                                        rCollectArrear["IsSelect"] = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                DataRow rCollectArrear = dtbCollectArrearPrChange.NewRow();
                                rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                rCollectArrear["CollectArrearType"] = 2;
                                rCollectArrear["RelateVoucherID"] = frmDutyUsers1.RelateVoucherID;
                                rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                rCollectArrear["IsDeleted"] = false;
                                rCollectArrear["DeletedUser"] = string.Empty;
                                rCollectArrear["IsSelect"] = false;
                                dtbCollectArrearPrChange.Rows.Add(rCollectArrear);
                            }
                        }
                        else
                        {
                            if (dtbCollectArrearPrChange.Rows.Count > 0)
                            {
                                DataRow[] drCollectArrear = dtbCollectArrearPrChange.Select("RelateVoucherID = '" + frmDutyUsers1.RelateVoucherID + "' AND UserName = '" + Convert.ToString(rDutyUser["UserName"]) + "'");
                                if (drCollectArrear.Length > 0)
                                {
                                    foreach (DataRow rCollectArrear in dtbCollectArrearPrChange.Rows)
                                    {
                                        if (Convert.ToString(rCollectArrear["UserName"]) == Convert.ToString(rDutyUser["UserName"]))
                                        {
                                            rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                            rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                            rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                            rCollectArrear["IsDeleted"] = false;
                                            rCollectArrear["DeletedUser"] = string.Empty;
                                            rCollectArrear["IsSelect"] = false;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    DataRow rCollectArrear = dtbCollectArrearPrChange.NewRow();
                                    rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                    rCollectArrear["CollectArrearType"] = 2;
                                    rCollectArrear["RelateVoucherID"] = frmDutyUsers1.RelateVoucherID;
                                    rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                    rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                    rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                    rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                    rCollectArrear["IsDeleted"] = false;
                                    rCollectArrear["DeletedUser"] = string.Empty;
                                    rCollectArrear["IsSelect"] = false;
                                    dtbCollectArrearPrChange.Rows.Add(rCollectArrear);
                                }
                            }
                            else
                            {
                                DataRow rCollectArrear = dtbCollectArrearPrChange.NewRow();
                                rCollectArrear["InventoryProcessID"] = objInventoryProcess.InventoryProcessID;
                                rCollectArrear["CollectArrearType"] = 2;
                                rCollectArrear["RelateVoucherID"] = frmDutyUsers1.RelateVoucherID;
                                rCollectArrear["CollectArrearMoney"] = Convert.ToDecimal(rDutyUser["CollectArrearMoney"]);
                                rCollectArrear["UserName"] = Convert.ToString(rDutyUser["UserName"]);
                                rCollectArrear["FullName"] = Convert.ToString(rDutyUser["FullName"]);
                                rCollectArrear["Note"] = Convert.ToString(rDutyUser["Note"]);
                                rCollectArrear["IsDeleted"] = false;
                                rCollectArrear["DeletedUser"] = string.Empty;
                                rCollectArrear["IsSelect"] = false;
                                dtbCollectArrearPrChange.Rows.Add(rCollectArrear);
                            }
                        }
                    }
                }
            }
        }

        private void gridViewSTChange_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "REFSALEPRICE_OUT" || e.Column.FieldName == "REFSALEPRICE_IN")
            {
                DataRow r = gridViewSTChange.GetDataRow(e.RowHandle);
                decimal quantity = Convert.ToDecimal(r["QUANTITY"]);
                decimal outPrice = Convert.ToDecimal(r["REFSALEPRICE_OUT"]);
                decimal inPrice = Convert.ToDecimal(r["REFSALEPRICE_IN"]);
                //decimal vat = Convert.ToDecimal(r["VAT"]);
                //decimal vatPercent = Convert.ToDecimal(r["VATPERCENT"]);
                //decimal totalMoney = price * (1 + vat * vatPercent * Convert.ToDecimal(0.0001)) * quantity;
                decimal totalMoenyOut = outPrice * quantity;
                decimal totalMoenyIn = inPrice * quantity;
                r["UNEVENTAMOUNT"] = totalMoenyOut - totalMoenyIn;
                r["COLLECTARREARPRICE"] = totalMoenyOut - totalMoenyIn;
            }
        }
    }
}