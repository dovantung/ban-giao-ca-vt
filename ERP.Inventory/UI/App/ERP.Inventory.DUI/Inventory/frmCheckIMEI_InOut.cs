﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmCheckIMEI_InOut : Form
    {
        public DataTable dtbCheckIMEI_InputStore = null;
        public DataTable dtbCheckIMEI_OutputStore = null;
        
        public frmCheckIMEI_InOut()
        {
            InitializeComponent();
        }

        private void frmCheckIMEI_InOut_Load(object sender, EventArgs e)
        {
            DataTable dtbCheckIMEI_InOut = new DataTable();
            dtbCheckIMEI_InOut.Columns.Add("IMEI_Out", typeof(string));
            dtbCheckIMEI_InOut.Columns.Add("OutputDate", typeof(DateTime));
            dtbCheckIMEI_InOut.Columns.Add("IMEI_In", typeof(string));
            dtbCheckIMEI_InOut.Columns.Add("InputDate", typeof(DateTime));
            if (dtbCheckIMEI_InputStore != null || dtbCheckIMEI_OutputStore != null)
            {
                if (dtbCheckIMEI_OutputStore != null)
                {
                    for (int i = 0; i < dtbCheckIMEI_OutputStore.Rows.Count; i++)
                    {
                        DataRow rOut = dtbCheckIMEI_InOut.NewRow();
                        rOut["IMEI_Out"] = dtbCheckIMEI_OutputStore.Rows[i]["IMEI"];
                        rOut["OutputDate"] = dtbCheckIMEI_OutputStore.Rows[i]["OutputDate"];
                        dtbCheckIMEI_InOut.Rows.Add(rOut);
                    }
                }
                if (dtbCheckIMEI_InputStore != null)
                {
                    for (int i = 0; i < dtbCheckIMEI_InputStore.Rows.Count; i++)
                    {
                        DataRow rIn = dtbCheckIMEI_InOut.NewRow();
                        rIn["IMEI_In"] = dtbCheckIMEI_InputStore.Rows[i]["IMEI"];
                        rIn["InputDate"] = dtbCheckIMEI_InputStore.Rows[i]["InputDate"];
                        dtbCheckIMEI_InOut.Rows.Add(rIn);
                    }
                }
            }
            flexImportResult.DataSource = dtbCheckIMEI_InOut;
            CustomFlex();
        }
        private void CustomFlex()
        {
            for (int i = 0; i < flexImportResult.Cols.Count; i++)
            {
                flexImportResult.Cols[i].AllowEditing = false;
                flexImportResult.Cols[i].AllowDragging = false;
            }
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnCaption(flexImportResult, "IMEI_Out,IMEI xuất,OutputDate,Ngày xuất,IMEI_In,IMEI nhập,InputDate,Ngày nhập");
            flexImportResult.Cols["OutputDate"].Format = "dd/MM/yyyy HH:mm";
            flexImportResult.Cols["InputDate"].Format = "dd/MM/yyyy HH:mm";
            flexImportResult.AutoSizeCols();
            flexImportResult.Cols[0].Width = 10;
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexImportResult);
        }
    }
}