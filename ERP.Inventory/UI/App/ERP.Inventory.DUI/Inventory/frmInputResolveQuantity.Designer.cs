﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmInputResolveQuantity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInputResolveQuantity));
            this.cmdYes = new System.Windows.Forms.Button();
            this.cmdNo = new System.Windows.Forms.Button();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.grdCtlProducts = new DevExpress.XtraGrid.GridControl();
            this.grdViewProducts = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.colPRODUCTID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPRODUCTNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colERP_PRODUCTSTATUSNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colINV_PRODUCTSTATUSNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colDELTAREMAINQUANTITY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.reQuantityShow = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colCHG_INQUANTITY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colCHG_PRODUCTSTATUSID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repItemProductStatusIDList = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colRESOLVENOTE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repTextImei = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.chkISChangeStatusID = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkIsSelected = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkIsLock = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand25 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand27 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand26 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCtlProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reQuantityShow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemProductStatusIDList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextImei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkISChangeStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsLock)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdYes
            // 
            this.cmdYes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdYes.Image = ((System.Drawing.Image)(resources.GetObject("cmdYes.Image")));
            this.cmdYes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdYes.Location = new System.Drawing.Point(1020, 3);
            this.cmdYes.Margin = new System.Windows.Forms.Padding(4);
            this.cmdYes.Name = "cmdYes";
            this.cmdYes.Size = new System.Drawing.Size(92, 25);
            this.cmdYes.TabIndex = 59;
            this.cmdYes.Text = "   Cập nhật";
            this.cmdYes.Click += new System.EventHandler(this.cmdYes_Click);
            // 
            // cmdNo
            // 
            this.cmdNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdNo.Image = ((System.Drawing.Image)(resources.GetObject("cmdNo.Image")));
            this.cmdNo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdNo.Location = new System.Drawing.Point(1120, 3);
            this.cmdNo.Margin = new System.Windows.Forms.Padding(4);
            this.cmdNo.Name = "cmdNo";
            this.cmdNo.Size = new System.Drawing.Size(92, 25);
            this.cmdNo.TabIndex = 61;
            this.cmdNo.Text = "   Bỏ qua";
            this.cmdNo.Click += new System.EventHandler(this.cmdNo_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.cmdNo);
            this.panelControl1.Controls.Add(this.cmdYes);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 375);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1218, 34);
            this.panelControl1.TabIndex = 86;
            // 
            // grdCtlProducts
            // 
            this.grdCtlProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCtlProducts.Location = new System.Drawing.Point(0, 0);
            this.grdCtlProducts.MainView = this.grdViewProducts;
            this.grdCtlProducts.Name = "grdCtlProducts";
            this.grdCtlProducts.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repTextImei,
            this.repQuantity,
            this.repNote,
            this.reQuantityShow,
            this.repItemProductStatusIDList,
            this.chkISChangeStatusID,
            this.chkIsSelected,
            this.chkIsLock});
            this.grdCtlProducts.Size = new System.Drawing.Size(1218, 375);
            this.grdCtlProducts.TabIndex = 87;
            this.grdCtlProducts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewProducts});
            // 
            // grdViewProducts
            // 
            this.grdViewProducts.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.BandPanel.Options.UseFont = true;
            this.grdViewProducts.Appearance.BandPanel.Options.UseTextOptions = true;
            this.grdViewProducts.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewProducts.Appearance.BandPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewProducts.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProducts.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewProducts.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewProducts.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewProducts.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewProducts.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProducts.Appearance.Preview.Options.UseFont = true;
            this.grdViewProducts.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProducts.Appearance.Row.Options.UseFont = true;
            this.grdViewProducts.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand4,
            this.gridBand8,
            this.gridBand17,
            this.gridBand25,
            this.gridBand19});
            this.grdViewProducts.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colPRODUCTID,
            this.colPRODUCTNAME,
            this.colERP_PRODUCTSTATUSNAME,
            this.colINV_PRODUCTSTATUSNAME,
            this.colCHG_PRODUCTSTATUSID,
            this.colCHG_INQUANTITY,
            this.colDELTAREMAINQUANTITY,
            this.colRESOLVENOTE});
            this.grdViewProducts.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewProducts.GridControl = this.grdCtlProducts;
            this.grdViewProducts.MinBandPanelRowCount = 2;
            this.grdViewProducts.Name = "grdViewProducts";
            this.grdViewProducts.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewProducts.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewProducts.OptionsNavigation.UseTabKey = false;
            this.grdViewProducts.OptionsView.ColumnAutoWidth = false;
            this.grdViewProducts.OptionsView.ShowAutoFilterRow = true;
            this.grdViewProducts.OptionsView.ShowColumnHeaders = false;
            this.grdViewProducts.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewProducts.OptionsView.ShowFooter = true;
            this.grdViewProducts.OptionsView.ShowGroupPanel = false;
            this.grdViewProducts.HiddenEditor += new System.EventHandler(this.grdViewProducts_HiddenEditor);
            // 
            // colPRODUCTID
            // 
            this.colPRODUCTID.Caption = "MÃ SẢN PHẨM";
            this.colPRODUCTID.FieldName = "PRODUCTID";
            this.colPRODUCTID.Name = "colPRODUCTID";
            this.colPRODUCTID.OptionsColumn.AllowEdit = false;
            this.colPRODUCTID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPRODUCTID.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "PRODUCTID", "Tổng cổng")});
            this.colPRODUCTID.Visible = true;
            this.colPRODUCTID.Width = 120;
            // 
            // colPRODUCTNAME
            // 
            this.colPRODUCTNAME.Caption = "TÊN SẢN PHẨM";
            this.colPRODUCTNAME.FieldName = "PRODUCTNAME";
            this.colPRODUCTNAME.Name = "colPRODUCTNAME";
            this.colPRODUCTNAME.OptionsColumn.AllowEdit = false;
            this.colPRODUCTNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colPRODUCTNAME.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "PRODUCTNAME", "{0:N0}")});
            this.colPRODUCTNAME.Visible = true;
            this.colPRODUCTNAME.Width = 220;
            // 
            // colERP_PRODUCTSTATUSNAME
            // 
            this.colERP_PRODUCTSTATUSNAME.Caption = "ERP_TRẠNG THÁI";
            this.colERP_PRODUCTSTATUSNAME.FieldName = "ERP_PRODUCTSTATUSNAME";
            this.colERP_PRODUCTSTATUSNAME.Name = "colERP_PRODUCTSTATUSNAME";
            this.colERP_PRODUCTSTATUSNAME.OptionsColumn.AllowEdit = false;
            this.colERP_PRODUCTSTATUSNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colERP_PRODUCTSTATUSNAME.Visible = true;
            this.colERP_PRODUCTSTATUSNAME.Width = 130;
            // 
            // colINV_PRODUCTSTATUSNAME
            // 
            this.colINV_PRODUCTSTATUSNAME.Caption = "INV_TRẠNG THÁI";
            this.colINV_PRODUCTSTATUSNAME.FieldName = "INV_PRODUCTSTATUSNAME";
            this.colINV_PRODUCTSTATUSNAME.Name = "colINV_PRODUCTSTATUSNAME";
            this.colINV_PRODUCTSTATUSNAME.OptionsColumn.AllowEdit = false;
            this.colINV_PRODUCTSTATUSNAME.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colINV_PRODUCTSTATUSNAME.Visible = true;
            this.colINV_PRODUCTSTATUSNAME.Width = 130;
            // 
            // colDELTAREMAINQUANTITY
            // 
            this.colDELTAREMAINQUANTITY.Caption = "Chênh lệch còn lại";
            this.colDELTAREMAINQUANTITY.ColumnEdit = this.reQuantityShow;
            this.colDELTAREMAINQUANTITY.FieldName = "DELTAREMAINQUANTITY";
            this.colDELTAREMAINQUANTITY.Name = "colDELTAREMAINQUANTITY";
            this.colDELTAREMAINQUANTITY.OptionsColumn.AllowEdit = false;
            this.colDELTAREMAINQUANTITY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DELTAINPUTTINGQUANTITY", "{0:N0}")});
            this.colDELTAREMAINQUANTITY.Visible = true;
            this.colDELTAREMAINQUANTITY.Width = 80;
            // 
            // reQuantityShow
            // 
            this.reQuantityShow.AutoHeight = false;
            this.reQuantityShow.Mask.EditMask = "N0";
            this.reQuantityShow.Mask.UseMaskAsDisplayFormat = true;
            this.reQuantityShow.Name = "reQuantityShow";
            this.reQuantityShow.NullText = "0";
            this.reQuantityShow.NullValuePrompt = "0";
            // 
            // colCHG_INQUANTITY
            // 
            this.colCHG_INQUANTITY.Caption = "Số lượng cần xử lý";
            this.colCHG_INQUANTITY.ColumnEdit = this.repQuantity;
            this.colCHG_INQUANTITY.FieldName = "RESOLVEQUANTITY";
            this.colCHG_INQUANTITY.Name = "colCHG_INQUANTITY";
            this.colCHG_INQUANTITY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DELTAADJUSTUNEVENTQUANTITY", "{0:N0}")});
            this.colCHG_INQUANTITY.Visible = true;
            this.colCHG_INQUANTITY.Width = 100;
            // 
            // repQuantity
            // 
            this.repQuantity.AutoHeight = false;
            this.repQuantity.DisplayFormat.FormatString = "N0";
            this.repQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repQuantity.Mask.EditMask = "N0";
            this.repQuantity.Mask.UseMaskAsDisplayFormat = true;
            this.repQuantity.MaxValue = new decimal(new int[] {
            1215752191,
            23,
            0,
            0});
            this.repQuantity.Name = "repQuantity";
            this.repQuantity.NullText = "0";
            this.repQuantity.NullValuePrompt = "0";
            // 
            // colCHG_PRODUCTSTATUSID
            // 
            this.colCHG_PRODUCTSTATUSID.Caption = "CHG_PRODUCTSTATUSID";
            this.colCHG_PRODUCTSTATUSID.ColumnEdit = this.repItemProductStatusIDList;
            this.colCHG_PRODUCTSTATUSID.FieldName = "CHG_PRODUCTSTATUSID";
            this.colCHG_PRODUCTSTATUSID.Name = "colCHG_PRODUCTSTATUSID";
            this.colCHG_PRODUCTSTATUSID.Visible = true;
            this.colCHG_PRODUCTSTATUSID.Width = 219;
            // 
            // repItemProductStatusIDList
            // 
            this.repItemProductStatusIDList.AutoHeight = false;
            this.repItemProductStatusIDList.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repItemProductStatusIDList.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PRODUCTSTATUSNAME", 120, "Trạng Thái")});
            this.repItemProductStatusIDList.DisplayMember = "PRODUCTSTATUSNAME";
            this.repItemProductStatusIDList.Name = "repItemProductStatusIDList";
            this.repItemProductStatusIDList.ValueMember = "PRODUCTSTATUSID";
            // 
            // colRESOLVENOTE
            // 
            this.colRESOLVENOTE.Caption = "GHI CHÚ";
            this.colRESOLVENOTE.ColumnEdit = this.repNote;
            this.colRESOLVENOTE.FieldName = "RESOLVENOTE";
            this.colRESOLVENOTE.Name = "colRESOLVENOTE";
            this.colRESOLVENOTE.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colRESOLVENOTE.Visible = true;
            this.colRESOLVENOTE.Width = 220;
            // 
            // repNote
            // 
            this.repNote.AutoHeight = false;
            this.repNote.MaxLength = 2000;
            this.repNote.Name = "repNote";
            // 
            // repTextImei
            // 
            this.repTextImei.AutoHeight = false;
            this.repTextImei.MaxLength = 50;
            this.repTextImei.Name = "repTextImei";
            // 
            // chkISChangeStatusID
            // 
            this.chkISChangeStatusID.AutoHeight = false;
            this.chkISChangeStatusID.Name = "chkISChangeStatusID";
            this.chkISChangeStatusID.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // chkIsSelected
            // 
            this.chkIsSelected.AutoHeight = false;
            this.chkIsSelected.Name = "chkIsSelected";
            this.chkIsSelected.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // chkIsLock
            // 
            this.chkIsLock.AutoHeight = false;
            this.chkIsLock.Name = "chkIsLock";
            this.chkIsLock.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "Thông tin sản phẩm";
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand3});
            this.gridBand1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 340;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "Mã sản phẩm";
            this.gridBand2.Columns.Add(this.colPRODUCTID);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.OptionsBand.AllowMove = false;
            this.gridBand2.OptionsBand.FixedWidth = true;
            this.gridBand2.Width = 120;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "Tên sản phẩm";
            this.gridBand3.Columns.Add(this.colPRODUCTNAME);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 220;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "ERP";
            this.gridBand4.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand6});
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Width = 130;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "Trạng thái";
            this.gridBand6.Columns.Add(this.colERP_PRODUCTSTATUSNAME);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.Width = 130;
            // 
            // gridBand8
            // 
            this.gridBand8.Caption = "Kiểm kê";
            this.gridBand8.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand9});
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.Width = 130;
            // 
            // gridBand9
            // 
            this.gridBand9.Caption = "Trạng thái";
            this.gridBand9.Columns.Add(this.colINV_PRODUCTSTATUSNAME);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.Width = 130;
            // 
            // gridBand17
            // 
            this.gridBand17.Caption = "Chênh lệch còn lại";
            this.gridBand17.Columns.Add(this.colDELTAREMAINQUANTITY);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.Width = 80;
            // 
            // gridBand25
            // 
            this.gridBand25.Caption = "Xử lý chênh lệch";
            this.gridBand25.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand27,
            this.gridBand26});
            this.gridBand25.Name = "gridBand25";
            this.gridBand25.Width = 319;
            // 
            // gridBand27
            // 
            this.gridBand27.Caption = "SL Điều chỉnh";
            this.gridBand27.Columns.Add(this.colCHG_INQUANTITY);
            this.gridBand27.Name = "gridBand27";
            this.gridBand27.Width = 100;
            // 
            // gridBand26
            // 
            this.gridBand26.Caption = "Trạng thái";
            this.gridBand26.Columns.Add(this.colCHG_PRODUCTSTATUSID);
            this.gridBand26.Name = "gridBand26";
            this.gridBand26.Width = 219;
            // 
            // gridBand19
            // 
            this.gridBand19.Caption = "Ghi chú";
            this.gridBand19.Columns.Add(this.colRESOLVENOTE);
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.Width = 220;
            // 
            // frmInputResolveQuantity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1218, 409);
            this.Controls.Add(this.grdCtlProducts);
            this.Controls.Add(this.panelControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "frmInputResolveQuantity";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Danh sách sản phẩm";
            this.Load += new System.EventHandler(this.frmInputResolveQuantity_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdCtlProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reQuantityShow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemProductStatusIDList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextImei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkISChangeStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsLock)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cmdYes;
        private System.Windows.Forms.Button cmdNo;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl grdCtlProducts;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView grdViewProducts;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsLock;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelected;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPRODUCTID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPRODUCTNAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colERP_PRODUCTSTATUSNAME;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit reQuantityShow;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colINV_PRODUCTSTATUSNAME;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkISChangeStatusID;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repQuantity;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colDELTAREMAINQUANTITY;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCHG_INQUANTITY;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCHG_PRODUCTSTATUSID;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repItemProductStatusIDList;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repNote;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRESOLVENOTE;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTextImei;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand25;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand27;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand26;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
    }
}