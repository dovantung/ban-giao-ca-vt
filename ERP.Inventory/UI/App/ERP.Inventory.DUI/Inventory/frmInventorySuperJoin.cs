﻿using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmInventorySuperJoin : Form
    {
        public frmInventorySuperJoin()
        {
            InitializeComponent();
        }

        #region variable
        private ERP.Inventory.PLC.Inventory.PLCInventoryTerm objPLCInventoryTerm = new PLC.Inventory.PLCInventoryTerm();
        private ERP.Inventory.PLC.Inventory.PLCInventory objPLCInventory = new PLC.Inventory.PLCInventory();
        private ERP.Inventory.PLC.Inventory.WSInventoryTerm.InventoryTerm objInventoryTerm = null;
        private int intSelectedIndex_InventoryType = 0;
        private int intSelectedIndex_InventoryTerm = 0;
        private int intSelectedIndex_Store = 0;
        private int intSelectedIndex_MainGroup = 0;
        private DataTable dtbINVTerm_Brand = null;
        #endregion

        #region method
        private void LoadComboBox()
        {
            cboInventoryType1.InitControl(false, Library.AppCore.DataSource.GetDataSource.GetInventoryType().Copy(), "INVENTORYTYPEID", "INVENTORYTYPENAME", "--Chọn loại kiểm kê--");
            // Library.AppCore.LoadControls.SetDataSource.SetInventoryType(this, cboInventoryType);

            //cboInventoryTerm.Text = "-- Chọn kỳ kiểm kê --";
            cboStore1.InitControl(false, null, "", "", "--Chọn chi nhánh--");
        }

        private void Reset()
        {
            intSelectedIndex_InventoryType = 0;
            //cboInventoryType.SelectedIndex = 0;
            cboInventoryType1.ResetDefaultValue();

            //cboInventoryTerm1.DataSource = null;
            //cboInventoryTerm1.Items.Clear();
            //cboInventoryTerm.Items.Add("-- Chọn kỳ kiểm kê --");
            //cboInventoryTerm.Text = "-- Chọn kỳ kiểm kê --";
            cboInventoryTerm1.Enabled = false;

            ClearInfoTerm();
        }

        private void ClearInfoTerm()
        {
            if (cboStore1.DataSource != null && (cboStore1.DataSource as DataTable).Rows.Count > 0)
            {
                DataTable dtbTerm_Store = (cboStore1.DataSource as DataTable).Clone();
                DataRow rNewStore = dtbTerm_Store.NewRow();
                rNewStore.ItemArray = (cboStore1.DataSource as DataTable).Rows[0].ItemArray;
                dtbTerm_Store.Rows.Add(rNewStore);
                cboStore1.InitControl(false, dtbTerm_Store, "STOREID", "STORENAME", "--Chọn chi nhánh--");
                cboStore1.Enabled = false;
            }
        }

        private bool ValidateInput()
        {
            if (cboInventoryType1.ColumnID < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn loại kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboInventoryType1.Focus();
                return false;
            }
            if (cboInventoryTerm1.ColumnID < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn kỳ kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboInventoryTerm1.Focus();
                return false;
            }
            if (cboStore1.ColumnID < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn chi nhánh", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboStore1.Focus();
                return false;
            }

            DataTable dtbIsParent = new Report.PLC.PLCReportDataSource().GetDataSource("INV_INVENTORY_IS_PARENT", new object[] { "@INVENTORYTERMID", Convert.ToInt32(cboInventoryTerm1.ColumnID), "@STOREID", Convert.ToInt32(cboStore1.ColumnID) });
            if (dtbIsParent != null && dtbIsParent.Rows.Count > 0)
            {
                MessageBox.Show(this, "Các phiếu kiểm kê trong kỳ của kho này đã được nối hết!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        #endregion

        #region event
        private void frmInventoryJoin_Load(object sender, EventArgs e)
        {
            LoadComboBox();
        }

        private void cboInventoryType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //if (cboInventoryType1.ColumnID < 1)
            //{
            //    cboInventoryType1.ResetDefaultValue();
            //    return;
            //}
            //DateTime dtmServerDate = Library.AppCore.Globals.GetServerDateTime();
            //DataTable dtbINVTerm = objPLCInventoryTerm.SearchData(new object[] { "@Keywords", string.Empty, "@InventoryTypeID", Convert.ToInt32(cboInventoryType1.ColumnID), "@FromDate", dtmServerDate.AddDays(-100), "@ToDate", dtmServerDate.AddDays(1) });
            ////string filterDate = string.Format(System.Globalization.CultureInfo.InvariantCulture,
            ////                                "InventoryDate >= #{0:MM/dd/yyyy}# AND InventoryDate < #{1:MM/dd/yyyy}#",
            ////                                dtmServerDate.AddDays(-100),
            ////                                dtmServerDate.AddDays(1));
            ////dtbINVTerm.DefaultView.RowFilter = filterDate;
            //dtbINVTerm.DefaultView.Sort = "InventoryDate DESC";
            //dtbINVTerm = dtbINVTerm.DefaultView.ToTable();
            //Library.AppCore.LoadControls.ComboBoxObject.LoadDataFromDataTable(dtbINVTerm, cboInventoryTerm, "-- Chọn kỳ kiểm kê --");
            //cboInventoryTerm.Enabled = true;

            //ClearInfoTerm();
        }

        private void cboInventoryTerm_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboInventoryTerm1.ColumnID < 1)
            {
                cboInventoryTerm1.ResetDefaultValue();
                return;
            }
            objPLCInventoryTerm.LoadInfo(ref objInventoryTerm, Convert.ToInt32(cboInventoryTerm1.ColumnID));
            objInventoryTerm.dtbTerm_MainGroup.DefaultView.RowFilter = "IsSelect = 1";
            DataTable dtbTerm_MainGroup = objInventoryTerm.dtbTerm_MainGroup.DefaultView.ToTable();
            if (dtbTerm_MainGroup.Columns.Contains("IsSelect"))
            {
                dtbTerm_MainGroup.Columns.Remove("IsSelect");
            }
            objInventoryTerm.dtbTerm_Store.DefaultView.RowFilter = "IsSelect = 1";
            DataTable dtbTerm_Store = objInventoryTerm.dtbTerm_Store.DefaultView.ToTable();
            if (dtbTerm_Store.Columns.Contains("IsSelect"))
            {
                dtbTerm_Store.Columns.Remove("IsSelect");
            }
            //Library.AppCore.LoadControls.ComboBoxObject.LoadDataFromDataTable(dtbTerm_Store, cboStore1, "-- Chọn chi nhánh --");
            cboStore1.Enabled = true;

            dtbINVTerm_Brand = objInventoryTerm.dtbTerm_Brand.Copy();

        }

        //private void cboInventoryType_DropDown(object sender, EventArgs e)
        //{
        //    intSelectedIndex_InventoryType = cboInventoryType.SelectedIndex;
        //}

        private void cboInventoryTerm_DropDown(object sender, EventArgs e)
        {
            intSelectedIndex_InventoryTerm = cboInventoryTerm1.ColumnID;
        }

        private void cboStore_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboStore1.ColumnID < 1)
            {
                //   cboStore1.ColumnID = intSelectedIndex_Store;
            }
        }

        private void cboMainGroup_SelectionChangeCommitted(object sender, EventArgs e)
        {
        }

        private void cboStore_DropDown(object sender, EventArgs e)
        {
            intSelectedIndex_Store = cboStore1.ColumnID;
        }

        private void cboMainGroup_DropDown(object sender, EventArgs e)
        {
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdJoin_Click(object sender, EventArgs e)
        {
            cmdJoin.Enabled = false;
            try
            {
                if (!ValidateInput())
                {
                    return;
                }


                int inventoryTermID = Convert.ToInt32(cboInventoryTerm1.ColumnID);
                int storeID = Convert.ToInt32(cboStore1.ColumnID);
                var objKeywords = new object[] {
                    "@INVENTORYTERMID", inventoryTermID,
                    "@STOREID", storeID };

                Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
                DataTable dtbConflict = objPLCReportDataSource.GetDataSource("INV_INVENTORY_GETCONFLICT", objKeywords);
                bool isResolveConflict = false;
                if (dtbConflict != null)
                {
                    if (dtbConflict.Rows.Count > 0)
                    {
                        if (new frmInventoryJoin_ResolveConflictData(dtbConflict, inventoryTermID, storeID).ShowDialog() == DialogResult.OK)
                        {
                            isResolveConflict = true;
                        }
                    }
                    else
                    {
                        isResolveConflict = true;
                    }
                }
                else
                {
                    isResolveConflict = false;
                    MessageBoxObject.ShowWarningMessage(this, "Lỗi lấy dữ liệu conflict hoặc các phiếu đang được nối ở một tiến trình khác!");
                }


                if (isResolveConflict)
                {
                    string newInventoryID = string.Empty;
                    if (new PLC.Inventory.PLCInventory().InventorySuperJoin(inventoryTermID, storeID, ref newInventoryID))
                    {
                        MessageBoxObject.ShowInfoMessage(this, "Nối dữ liệu kiểm kê thành công!");
                        ERP.Inventory.PLC.Inventory.WSInventory.Inventory objInventory = null;
                        objPLCInventory.GetInventory(ref objInventory, newInventoryID);
                        if (objInventory.IsParent)
                        {
                            frmInventoryVTS_AFJoin frmInventory1 = new frmInventoryVTS_AFJoin();
                            frmInventory1.InventoryID = newInventoryID;
                            frmInventory1.InventoryTermID = objInventory.InventoryTermID;
                            frmInventory1.MaingroupIDList = objInventory.MainGroupIDList;
                            frmInventory1.SubgroupIDList = objInventory.SubGroupIDList;
                            frmInventory1.InventoryUser = objInventory.InventoryUser;
                            frmInventory1.StoreID = objInventory.InventoryStoreID;
                            frmInventory1.ProductStateID = objInventory.ProductStatusID;
                            frmInventory1.ShowDialog();
                        }
                    }
                    else
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Nối dữ liệu kiểm kê thất bại!");
                    }
                }
            }
            finally
            {
                cmdJoin.Enabled = true;
            }
        }
        #endregion

        private void cboInventoryType1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            DateTime dtmServerDate = Library.AppCore.Globals.GetServerDateTime();
            DataTable dtbINVTerm = objPLCInventoryTerm.SearchData(new object[] { "@Keywords", string.Empty, "@InventoryTypeID", Convert.ToInt32(cboInventoryType1.ColumnID), "@FromDate", dtmServerDate.AddDays(-100), "@ToDate", dtmServerDate.AddDays(1) });
            if (cboInventoryType1.ColumnID < 1)
            {
                dtbINVTerm.DefaultView.RowFilter = "1=2";
                dtbINVTerm.DefaultView.Sort = "InventoryDate DESC";
                dtbINVTerm = dtbINVTerm.DefaultView.ToTable();
                cboInventoryTerm1.InitControl(false, dtbINVTerm, "INVENTORYTERMID", "INVENTORYTERMNAME", "--Chọn kỳ kiểm kê--");
                cboInventoryTerm1_SelectionChangeCommitted(null, null);
                return;
            }

            //string filterDate = string.Format(System.Globalization.CultureInfo.InvariantCulture,
            //                                "InventoryDate >= #{0:MM/dd/yyyy}# AND InventoryDate < #{1:MM/dd/yyyy}#",
            //                                dtmServerDate.AddDays(-100),
            //                                dtmServerDate.AddDays(1));
            //dtbINVTerm.DefaultView.RowFilter = filterDate;
            dtbINVTerm.DefaultView.Sort = "InventoryDate DESC";
            dtbINVTerm = dtbINVTerm.DefaultView.ToTable();
            cboInventoryTerm1.InitControl(false, dtbINVTerm, "INVENTORYTERMID", "INVENTORYTERMNAME", "--Chọn kỳ kiểm kê--");
            //Library.AppCore.LoadControls.ComboBoxObject.LoadDataFromDataTable(dtbINVTerm, cboInventoryTerm, "-- Chọn kỳ kiểm kê --");
            //cboInventoryTerm.Enabled = true;
            cboInventoryTerm1.Enabled = true;

            ClearInfoTerm();
        }

        private void cboInventoryTerm1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            objPLCInventoryTerm.LoadInfo(ref objInventoryTerm, Convert.ToInt32(cboInventoryTerm1.ColumnID));
            objInventoryTerm.dtbTerm_MainGroup.DefaultView.RowFilter = "IsSelect = 1";
            DataTable dtbTerm_MainGroup = objInventoryTerm.dtbTerm_MainGroup.DefaultView.ToTable();
            if (dtbTerm_MainGroup.Columns.Contains("IsSelect"))
            {
                dtbTerm_MainGroup.Columns.Remove("IsSelect");
            }
            objInventoryTerm.dtbTerm_Store.DefaultView.RowFilter = "IsSelect = 1";
            DataTable dtbTerm_Store = objInventoryTerm.dtbTerm_Store.DefaultView.ToTable();
            if (dtbTerm_Store.Columns.Contains("IsSelect"))
            {
                dtbTerm_Store.Columns.Remove("IsSelect");
            }

            if (cboInventoryTerm1.ColumnID < 1)
            {
                objInventoryTerm.dtbTerm_Store.DefaultView.RowFilter = "1=2";
                dtbTerm_Store = objInventoryTerm.dtbTerm_Store.DefaultView.ToTable();
                cboStore1.InitControl(false, dtbTerm_Store, "STOREID", "STORENAME", "--Chọn chi nhánh--");
                return;
            }

            cboStore1.InitControl(false, dtbTerm_Store, "STOREID", "STORENAME", "--Chọn chi nhánh--");
            cboStore1.Enabled = true;
            dtbINVTerm_Brand = objInventoryTerm.dtbTerm_Brand.Copy();
        }

        private void cboStore1_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }
    }
}
