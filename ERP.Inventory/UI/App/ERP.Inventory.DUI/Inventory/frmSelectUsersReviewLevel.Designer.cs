﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmSelectUsersReviewLevel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cboReviewStatus = new System.Windows.Forms.ComboBox();
            this.btnReviewStatus = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(145, 41);
            this.txtPassword.MaxLength = 200;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(155, 22);
            this.txtPassword.TabIndex = 51;
            // 
            // txtUserName
            // 
            this.txtUserName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtUserName.Location = new System.Drawing.Point(145, 13);
            this.txtUserName.MaxLength = 200;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.ReadOnly = true;
            this.txtUserName.Size = new System.Drawing.Size(155, 22);
            this.txtUserName.TabIndex = 50;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(34, 16);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 16);
            this.label7.TabIndex = 52;
            this.label7.Text = "Tên đăng nhập:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(34, 44);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 16);
            this.label8.TabIndex = 53;
            this.label8.Text = "Mật khẩu:";
            // 
            // cboReviewStatus
            // 
            this.cboReviewStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReviewStatus.FormattingEnabled = true;
            this.cboReviewStatus.Items.AddRange(new object[] {
            "Chưa duyệt",
            "Đang xử lý",
            "Đồng ý",
            "Từ chối"});
            this.cboReviewStatus.Location = new System.Drawing.Point(146, 68);
            this.cboReviewStatus.Name = "cboReviewStatus";
            this.cboReviewStatus.Size = new System.Drawing.Size(121, 24);
            this.cboReviewStatus.TabIndex = 68;
            // 
            // btnReviewStatus
            // 
            this.btnReviewStatus.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.btnReviewStatus.Location = new System.Drawing.Point(267, 67);
            this.btnReviewStatus.Name = "btnReviewStatus";
            this.btnReviewStatus.Size = new System.Drawing.Size(33, 25);
            this.btnReviewStatus.TabIndex = 67;
            this.btnReviewStatus.UseVisualStyleBackColor = true;
            this.btnReviewStatus.Click += new System.EventHandler(this.btnReviewStatus_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 72);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 16);
            this.label1.TabIndex = 69;
            this.label1.Text = "Trạng thái duyệt:";
            // 
            // frmSelectUsersReviewLevel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 107);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboReviewStatus);
            this.Controls.Add(this.btnReviewStatus);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(355, 135);
            this.Name = "frmSelectUsersReviewLevel";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chọn duyệt";
            this.Load += new System.EventHandler(this.frmSelectUsersReviewLevel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboReviewStatus;
        private System.Windows.Forms.Button btnReviewStatus;
        private System.Windows.Forms.Label label1;
    }
}