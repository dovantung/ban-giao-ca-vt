﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using Library.AppCore;
using System.Windows.Forms;
using System.Collections;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmInventoryManager : Form
    {
        public frmInventoryManager()
        {
            InitializeComponent();
        }

        private ERP.Inventory.PLC.Inventory.PLCInventoryTerm objPLCInventoryTerm = new PLC.Inventory.PLCInventoryTerm();
        private ERP.Inventory.PLC.Inventory.PLCInventory objPLCInventory = new PLC.Inventory.PLCInventory();
        private bool bolIsLockByClosingDataMonth = false;
        private DataTable dtbSource = null;
        private int intReviewUser = 0;
        private void frmInventoryManager_Load(object sender, EventArgs e)
        {
            LoadComboBox();
            cboIsReviewed.SelectedIndex = 0;
            cboSearchType.SelectedIndex = 0;
        }

        private void LoadComboBox()
        {
            DateTime dtmServerDate = Library.AppCore.Globals.GetServerDateTime();
            dtFromDate.Value = new DateTime(dtmServerDate.Year, dtmServerDate.Month, dtmServerDate.Day, 0, 0, 0);
            dtToDate.Value = new DateTime(dtmServerDate.Year, dtmServerDate.Month, dtmServerDate.Day, 23, 59, 59);
            try
            {
                Library.AppCore.LoadControls.SetDataSource.SetInventoryType(this, cboInventoryType);
                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                objStoreFilter.IsCheckPermission = true;
                objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.VIEWREPORT;
                cboStoreIDList.InitControl(false, objStoreFilter);
                cboMainGroupID.InitControl(false, true);

                cboInventoryTermID.Caption = "-- Chọn kỳ kiểm kê --";
                //cboInventoryTermID.SetCombo(objPLCInventoryTerm.SearchData(new object[] { "@Keywords", string.Empty, "@FromDate", dtFromDate.Value, "@ToDate", dtToDate.Value }));
                cboInventoryType_SelectionChangeCommitted(null, null);
            }
            catch (Exception objEx)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp ComboBox");
                SystemErrorWS.Insert("Lỗi nạp ComboBox", objEx.ToString(), this.Name + " -> LoadComboBox", DUIInventory_Globals.ModuleName);
            }

            cboInventoryType.SelectedIndex = 0;
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            if (dtFromDate.Value.Date > dtToDate.Value.Date)
            {
                MessageBox.Show(this, "Từ ngày phải nhỏ hơn hoặc bằng đến ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            cmdSearch.Enabled = false;
            try
            {
                //                  1.Phiếu chưa nối
                //                  2.Phiếu đã nối
                //                  3.Phiếu xác nhận kiểm kê -chưa duyệt
                //                  4.Phiếu xác nhận kiểm kê -đã duyệt
                int intIsReviewed = -1;
                int intIsConnected = -1;
                int intIsParent = -1;
                if (cboIsReviewed.SelectedIndex > 0)
                {
                    if (cboIsReviewed.SelectedIndex == 1)
                    {
                        intIsReviewed = 0;
                        intIsConnected = 0;
                        intIsParent = 0;
                    }
                    else if (cboIsReviewed.SelectedIndex == 2)
                    {
                        intIsReviewed = 0;
                        intIsConnected = 1;
                        intIsParent = 0;
                    }
                    else if (cboIsReviewed.SelectedIndex == 3)
                    {
                        intIsReviewed = 0;
                        intIsConnected = -1;
                        intIsParent = 1;
                    }
                    else if (cboIsReviewed.SelectedIndex == 4)
                    {
                        intIsReviewed = 1;
                        intIsConnected = -1;
                        intIsParent = 1;
                    }
                }
                int intCheckInventoryID = 0;
                int intCheckInventoryType = 0;
                int intCheckInventoryTerm = 0;
                int intCheckStoreName = 0;
                int intCheckMainGroup = 0;
                int intCheckInventoryUser = 0;
                int intCheckCreatedUser = 0;
                int intCheckReviewedUser = 0;
                intReviewUser = 0;

                if (Convert.ToString(txtKeywords.Text).Trim() != string.Empty && cboSearchType.SelectedIndex > 0)
                {
                    if (cboSearchType.SelectedIndex == 1)
                    {
                        intCheckInventoryID = 1;
                    }
                    else if (cboSearchType.SelectedIndex == 2)
                    {
                        intCheckInventoryType = 1;
                    }
                    else if (cboSearchType.SelectedIndex == 3)
                    {
                        intCheckInventoryTerm = 1;
                    }
                    else if (cboSearchType.SelectedIndex == 4)
                    {
                        intCheckStoreName = 1;
                    }
                    else if (cboSearchType.SelectedIndex == 5)
                    {
                        intCheckMainGroup = 1;
                    }
                    else if (cboSearchType.SelectedIndex == 6)
                    {
                        intCheckInventoryUser = 1;
                    }
                    else if (cboSearchType.SelectedIndex == 7)
                    {
                        intCheckCreatedUser = 1;
                    }
                    else if (cboSearchType.SelectedIndex == 8)
                    {
                        intCheckReviewedUser = 1;
                    }
                    else if (cboSearchType.SelectedIndex == 9)
                    {
                        intReviewUser = 1;
                    }
                }
                if (cboSearchType.SelectedIndex == 1 || cboSearchType.SelectedIndex == 7 || cboSearchType.SelectedIndex == 9 || cboSearchType.SelectedIndex == 6 || cboSearchType.SelectedIndex == 4 || cboSearchType.SelectedIndex == 2 || cboSearchType.SelectedIndex == 5 || cboSearchType.SelectedIndex == 8)
                {
                    if (txtKeywords.Text == string.Empty)
                    {
                        MessageBox.Show(this, "Chưa nhập chuỗi tìm kiếm !", "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        cmdSearch.Enabled = true;
                        txtKeywords.Focus();
                        return;
                    }
                }
                ERP.Inventory.PLC.Inventory.PLCInventory objPLCInventory = new PLC.Inventory.PLCInventory();
                object[] objKeywords = new object[]
                {
                    "@INVENTORYID", txtKeywords.Text.Trim(),
                    "@STOREID", cboStoreIDList.StoreID,
                    "@MAINGROUPID", cboMainGroupID.MainGroupID,
                    "@INVENTORYUSER", (ucInventoryUser.UserName == string.Empty ? "-1" : ucInventoryUser.UserName),
                    "@INVENTORYTYPEID", Convert.ToInt32(cboInventoryType.SelectedValue),
                    "@INVENTORYTERMID", cboInventoryTermID.intID,
                    "@FromDate", dtFromDate.Value,
                    "@ToDate", dtToDate.Value,
                    "@UserName", Library.AppCore.SystemConfig.objSessionUser.UserName,
                    "@IsDeleted", chkDeleted.Checked,
                    "@ISCONNECTED", intIsConnected,
                    "@ISPARENT", intIsParent,
                    "@IsReviewed", intIsReviewed,
                    "@Keyword", Convert.ToString(txtKeywords.Text).Trim(),
                    "@CheckInventoryID", intCheckInventoryID,
                    "@CheckInventoryType", intCheckInventoryType,
                    "@CheckInventoryTerm", intCheckInventoryTerm,
                    "@CheckStoreName", intCheckStoreName,
                    "@CheckMainGroup", intCheckMainGroup,
                    "@CheckInventoryUser", intCheckInventoryUser,
                    "@CheckCreatedUser", intCheckCreatedUser,
                    "@CheckReviewedUser", intCheckReviewedUser,
                    "@ReviewedUser",intReviewUser
                };
                dtbSource = null;
                dtbSource = objPLCInventory.GetInventorySearch(objKeywords);
                dtbSource.Columns.Add("IsSelect", typeof(bool));
                dtbSource.Columns["IsSelect"].DefaultValue = false;
                dtbSource.Columns["IsSelect"].SetOrdinal(0);
                //DataRow row = dtbSource.NewRow();
                //DataTable dtbCopy = dtbSource.Copy();
                foreach (DataRow dr in dtbSource.Rows)
                {
                    dr["IsSelect"] = false;
                }
                flexListInventory.DataSource = dtbSource;
                CustomFlex();
                btnReview.Enabled = dtbSource.Rows.Count > 0;
            }
            catch (Exception objEx)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Lỗi tìm kiếm phiếu kiểm kê");
                SystemErrorWS.Insert("Lỗi tìm kiếm phiếu kiểm kê", objEx.ToString(), this.Name + " -> LoadComboBox", DUIInventory_Globals.ModuleName);
            }
            cmdSearch.Enabled = true;
        }

        private void CustomFlex()
        {
            flexListInventory.Cols[0].Visible = false; 
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexListInventory, false, "INVENTORYSTOREID","ISCONNECTED","ISPARENT", "ISNEW", "PRODUCTSTATUSID", "MAINGROUPNAME", "SUBGROUPNAME");
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnCaption(flexListInventory, "INVENTORYID,Mã phiếu,INVENTORYDATE,Ngày kiểm kê,INVENTORYTYPENAME,Loại kiểm kê,INVENTORYTERMNAME,Kỳ kiểm kê,STORENAME,Kho kiểm kê,PRODUCTSTATUSNAME,Trạng thái,INVENTORYUSER,Nhân viên KK,FULLNAME,Nhân viên KK,CREATEDUSER,Nhân viên tạo,USERCREATED,Nhân viên tạo,ISREVIEWUNEVENT,Đã duyệt,REVIEWUSER,Nhân viên duyệt,REVIEWUNEVENTUSER,Nhân viên duyệt,REVIEWUNEVENTTIME,Ngày duyệt,DELETEDUSER,Nhân viên hủy,USERDELETED,Nhân viên hủy,DELETEDDATE,Ngày hủy");
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowMerging(flexListInventory, "INVENTORYUSER,FULLNAME,CREATEDUSER,USERCREATED,REVIEWUSER,REVIEWUNEVENTUSER,DELETEDUSER,USERDELETED", true);
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnWidth(flexListInventory,
                "IsSelect", 30,
                "INVENTORYID", 130,
                "INVENTORYDATE", 110,
                "INVENTORYTYPENAME", 130,
                "INVENTORYTERMNAME", 130,
                "STORENAME", 160,
                //  "MAINGROUPNAME", 90,
                //  "SUBGROUPNAME", 90,
                //"ISNEW", 30,
                "PRODUCTSTATUSNAME", 120,
                "INVENTORYUSER", 70,
                "FULLNAME", 100,
                "CREATEDUSER", 70,
                "USERCREATED", 110,
                "ISREVIEWUNEVENT", 70,
                "REVIEWUSER", 70,
                "REVIEWUNEVENTUSER", 70,
                "REVIEWUNEVENTTIME", 100,
                "DELETEDUSER", 70,
                "USERDELETED", 110,
                "DELETEDDATE", 110);
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowEditing(flexListInventory, false, "INVENTORYID", "INVENTORYDATE", "INVENTORYTYPENAME", "STORENAME", //"MAINGROUPNAME", "SUBGROUPNAME", 
                "PRODUCTSTATUSNAME", "INVENTORYUSER", "FULLNAME", "CREATEDUSER", "USERCREATED", "ISREVIEWUNEVENT", "REVIEWUSER", "REVIEWUNEVENTUSER", "REVIEWUNEVENTTIME", "DELETEDUSER", "USERDELETED", "DELETEDDATE", "INVENTORYTERMNAME");
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowEditing(flexListInventory, true, "ISSELECT");
            // flexListInventory.Cols["ISNEW"].DataType = typeof(bool);
            // flexListInventory.Cols["ISNEW"].AllowEditing = false;
            flexListInventory.Cols["ISREVIEWUNEVENT"].DataType = typeof(bool);
            flexListInventory.Cols["ISREVIEWUNEVENT"].AllowEditing = false;
            flexListInventory.Cols["INVENTORYDATE"].Format = "dd/MM/yyyy HH:mm";
            flexListInventory.Cols["INVENTORYDATE"].AllowEditing = false;
            flexListInventory.Cols["REVIEWUNEVENTTIME"].Format = "dd/MM/yyyy HH:mm";
            flexListInventory.Cols["REVIEWUNEVENTTIME"].AllowEditing = false;
            flexListInventory.Cols["DELETEDDATE"].Format = "dd/MM/yyyy HH:mm";
            flexListInventory.Cols["DELETEDDATE"].AllowEditing = false;

            flexListInventory.Cols["DELETEDUSER"].Visible = chkDeleted.Checked;
            flexListInventory.Cols["USERDELETED"].Visible = chkDeleted.Checked;
            flexListInventory.Cols["DELETEDDATE"].Visible = chkDeleted.Checked;

            flexListInventory.Cols["IsSelect"].DataType = typeof(bool);
            flexListInventory.Cols["IsSelect"].Caption = "Chọn";
            flexListInventory.Cols["IsSelect"].AllowEditing = true;

            C1.Win.C1FlexGrid.CellStyle style = flexListInventory.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexListInventory.GetCellRange(0, 0, flexListInventory.Rows.Fixed - 1, flexListInventory.Cols.Count - 1);
            range.Style = style;

            flexListInventory.AllowEditing = true;
            flexListInventory.Rows[0].AllowMerging = true;
            flexListInventory.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            Library.AppCore.CustomControls.C1FlexGrid.CustomizeC1FlexGrid.AddColumnHeaderCheckBox(flexListInventory, "IsSelect");
        }

        private void flexListInventory_DoubleClick(object sender, EventArgs e)
        {
            if (flexListInventory.Rows.Count <= flexListInventory.Rows.Fixed)
            {
                return;
            }
            if (flexListInventory.RowSel < flexListInventory.Rows.Fixed)
            {
                return;
            }

            string strInventoryID = Convert.ToString(flexListInventory[flexListInventory.RowSel, "InventoryID"]).Trim();

            ERP.Inventory.PLC.Inventory.WSInventory.Inventory objInventory = null;
            objPLCInventory.GetInventory(ref objInventory, strInventoryID);

            int intEditALL = -1;
            int intEdit = -1;
            int intDeleteALL = -1;
            int intDelete = -1;

            if (objInventory.IsParent)
            {
                frmInventoryVTS_AFJoin frmInventory1 = new frmInventoryVTS_AFJoin();
                frmInventory1.InventoryID = strInventoryID;
                frmInventory1.InventoryTermID = objInventory.InventoryTermID;
                frmInventory1.MaingroupIDList = objInventory.MainGroupIDList;
                frmInventory1.SubgroupIDList = objInventory.SubGroupIDList;
                frmInventory1.InventoryUser = objInventory.InventoryUser;
                frmInventory1.StoreID = objInventory.InventoryStoreID;
                frmInventory1.ProductStateID = objInventory.ProductStatusID;
                if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
                {
                    int intInventoryTypeID = objInventory.InventoryTypeID;
                    if (intInventoryTypeID < 1)
                    {
                        DataTable dtbTerm = objPLCInventoryTerm.SearchData(new object[] { "@Keywords", string.Empty, "@FromDate", dtFromDate.Value, "@ToDate", dtToDate.Value });
                        if (dtbTerm != null && dtbTerm.Rows.Count > 0)
                        {
                            DataRow[] rTerm = dtbTerm.Select("InventoryTermID = " + objInventory.InventoryTermID);
                            if (rTerm.Length > 0)
                            {
                                intInventoryTypeID = Convert.ToInt32(rTerm[0]["InventoryTypeID"]);
                            }
                        }
                    }

                    ERP.MasterData.PLC.MD.WSInventoryType.InventoryType objInventoryType = new MasterData.PLC.MD.WSInventoryType.InventoryType();
                    ERP.MasterData.PLC.MD.PLCInventoryType objPLCInventoryType = new MasterData.PLC.MD.PLCInventoryType();
                    objPLCInventoryType.LoadInfo(ref objInventoryType, intInventoryTypeID);
                }
                bolIsLockByClosingDataMonth = false;
                try
                {
                    bolIsLockByClosingDataMonth = ERP.MasterData.SYS.PLC.PLCClosingDataMonth.CheckLockClosingDataMonth(ERP.MasterData.SYS.PLC.PLCClosingDataMonth.ClosingDataType.INVENTORY,
                        Convert.ToDateTime(flexListInventory[flexListInventory.RowSel, "INVENTORYDATE"]), Convert.ToInt32(flexListInventory[flexListInventory.RowSel, "INVENTORYSTOREID"]));
                }
                catch { }
                if (bolIsLockByClosingDataMonth)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Tháng " + Convert.ToDateTime(flexListInventory[flexListInventory.RowSel, "INVENTORYDATE"]).Month.ToString("00") + " đã khóa sổ. Bạn không thể chỉnh sửa");
                    //frmInventory1.intIsEdit = 0;
                    //frmInventory1.intIsDelete = 0;
                }

                frmInventory1.ShowDialog();
                return;
            }

            frmInventoryVTS frmInventory2 = new frmInventoryVTS();
            frmInventory2.InventoryID = strInventoryID;
            frmInventory2.InventoryTermID = objInventory.InventoryTermID;
            frmInventory2.MaingroupIDList = objInventory.MainGroupIDList;
            frmInventory2.SubgroupIDList = objInventory.SubGroupIDList;
            frmInventory2.InventoryUser = objInventory.InventoryUser;
            frmInventory2.StoreID = objInventory.InventoryStoreID;
            frmInventory2.ProductStateID = objInventory.ProductStatusID;
            if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
            {
                int intInventoryTypeID = objInventory.InventoryTypeID;
                if (intInventoryTypeID < 1)
                {
                    DataTable dtbTerm = objPLCInventoryTerm.SearchData(new object[] { "@Keywords", string.Empty, "@FromDate", dtFromDate.Value, "@ToDate", dtToDate.Value });
                    if (dtbTerm != null && dtbTerm.Rows.Count > 0)
                    {
                        DataRow[] rTerm = dtbTerm.Select("InventoryTermID = " + objInventory.InventoryTermID);
                        if (rTerm.Length > 0)
                        {
                            intInventoryTypeID = Convert.ToInt32(rTerm[0]["InventoryTypeID"]);
                        }
                    }
                }

                ERP.MasterData.PLC.MD.WSInventoryType.InventoryType objInventoryType = new MasterData.PLC.MD.WSInventoryType.InventoryType();
                ERP.MasterData.PLC.MD.PLCInventoryType objPLCInventoryType = new MasterData.PLC.MD.PLCInventoryType();
                objPLCInventoryType.LoadInfo(ref objInventoryType, intInventoryTypeID);
                if (objInventoryType != null && objInventoryType.InventoryTypeID > 0)
                {
                    if (Convert.ToString(objInventoryType.EditALLFunctionID).Trim() != string.Empty)
                    {
                        if (Library.AppCore.SystemConfig.objSessionUser.IsPermission(Convert.ToString(objInventoryType.EditALLFunctionID).Trim()))
                        {
                            intEditALL = 1;
                        }
                        else
                        {
                            intEditALL = 0;
                        }
                    }
                    if (intEditALL != 1)
                    {
                        if (Convert.ToString(objInventory.CreatedUser).Trim() == Convert.ToString(Library.AppCore.SystemConfig.objSessionUser.UserName).Trim())
                        {
                            if (Convert.ToString(objInventoryType.EditFunctionID).Trim() != string.Empty)
                            {
                                if (Library.AppCore.SystemConfig.objSessionUser.IsPermission(Convert.ToString(objInventoryType.EditFunctionID).Trim()))
                                {
                                    intEdit = 1;
                                }
                                else
                                {
                                    intEdit = 0;
                                }
                            }
                            else
                            {
                                intEdit = 0;
                            }
                        }
                        else
                        {
                            intEdit = 0;
                        }
                    }

                    if (Convert.ToString(objInventoryType.DeleteALLFunctionID).Trim() != string.Empty)
                    {
                        if (Library.AppCore.SystemConfig.objSessionUser.IsPermission(Convert.ToString(objInventoryType.DeleteALLFunctionID).Trim()))
                        {
                            intDeleteALL = 1;
                        }
                        else
                        {
                            intDeleteALL = 0;
                        }
                    }
                    if (intDeleteALL != 1)
                    {
                        if (Convert.ToString(objInventory.CreatedUser).Trim() == Convert.ToString(Library.AppCore.SystemConfig.objSessionUser.UserName).Trim())
                        {
                            if (Convert.ToString(objInventoryType.DeleteFunctionID).Trim() != string.Empty)
                            {
                                if (Library.AppCore.SystemConfig.objSessionUser.IsPermission(Convert.ToString(objInventoryType.DeleteFunctionID).Trim()))
                                {
                                    intDelete = 1;
                                }
                                else
                                {
                                    intDelete = 0;
                                }
                            }
                            else
                            {
                                intDelete = 0;
                            }
                        }
                        else
                        {
                            intDelete = 0;
                        }
                    }
                }
            }
            //frmInventory1.intIsEdit = (intEditALL == 1 ? intEditALL : intEdit);
            //if (frmInventory1.intIsEdit != 1 && intEdit == -1)
            //{
            //    frmInventory1.intIsEdit = 1;
            //}
            //frmInventory1.intIsDelete = (intDeleteALL == 1 ? intDeleteALL : intDelete);
            //if (frmInventory1.intIsDelete != 1)
            //{
            //    frmInventory1.intIsDelete = 0;
            //}

            bolIsLockByClosingDataMonth = false;
            try
            {
                bolIsLockByClosingDataMonth = ERP.MasterData.SYS.PLC.PLCClosingDataMonth.CheckLockClosingDataMonth(ERP.MasterData.SYS.PLC.PLCClosingDataMonth.ClosingDataType.INVENTORY,
                    Convert.ToDateTime(flexListInventory[flexListInventory.RowSel, "INVENTORYDATE"]), Convert.ToInt32(flexListInventory[flexListInventory.RowSel, "INVENTORYSTOREID"]));
            }
            catch { }
            if (bolIsLockByClosingDataMonth)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Tháng " + Convert.ToDateTime(flexListInventory[flexListInventory.RowSel, "INVENTORYDATE"]).Month.ToString("00") + " đã khóa sổ. Bạn không thể chỉnh sửa");
                //frmInventory1.intIsEdit = 0;
                //frmInventory1.intIsDelete = 0;
            }

            frmInventory2.ShowDialog();
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboInventoryType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboInventoryType.SelectedIndex > 0)
                cboInventoryTermID.SetCombo(objPLCInventoryTerm.SearchData(new object[] { "@Keywords", string.Empty, "@InventoryTypeID", Convert.ToInt32(cboInventoryType.SelectedValue), "@FromDate", dtFromDate.Value, "@ToDate", dtToDate.Value }));
            else
                cboInventoryTermID.SetCombo(objPLCInventoryTerm.SearchData(new object[] { "@Keywords", string.Empty, "@FromDate", dtFromDate.Value, "@ToDate", dtToDate.Value }));
        }

        private void txtInventoryID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                cmdSearch_Click(null, null);
            }
        }

        private void cmdExportExcel_Click(object sender, EventArgs e)
        {
            if (flexListInventory.Rows.Count > 1)
                Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexListInventory);
        }

        private void dtFromDate_ValueChanged(object sender, EventArgs e)
        {
            cboInventoryType_SelectionChangeCommitted(null, null);
        }

        private void dtToDate_ValueChanged(object sender, EventArgs e)
        {
            cboInventoryType_SelectionChangeCommitted(null, null);
        }

        private void btnReview_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtbData = flexListInventory.DataSource as DataTable;
                List<ERP.Inventory.PLC.Inventory.WSInventory.Inventory> lstInventorySelect = new List<ERP.Inventory.PLC.Inventory.WSInventory.Inventory>();
                string strInventoryIDUnion = string.Empty;
                //string strUserHasReviewed = string.Empty;
                int intCountSelect = 0;
                DataRow[] lstSelect = dtbData.Select("IsSelect = 1");
                intCountSelect = lstSelect.Length;
                DataRow[] lst = dtbData.Select("IsSelect = 1 AND ISREVIEWUNEVENT = 1");
                if (lst.Length > 0)
                {
                    MessageBox.Show(this, "Vui lòng chỉ chọn những phiếu chưa được duyệt!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    return;
                }

                DataRow[] lst2 = dtbData.Select("IsSelect = 1 AND ISPARENT = 0");
                if (lst2.Length > 0)
                {
                    MessageBox.Show(this, "Chỉ được phép duyệt những phiếu xác nhận kiểm kê!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    return;
                }

                foreach (DataRow row in dtbData.Rows)
                {
                    if (Convert.ToBoolean(row["IsSelect"]))
                    {
                        string strInventoryID = Convert.ToString(row["INVENTORYID"]).Trim();
                        ERP.Inventory.PLC.Inventory.WSInventory.Inventory objInventory = null;
                        objPLCInventory.GetInventory(ref objInventory, strInventoryID);

                        //#region Lấy danh sách những User đã duyệt mức duyệt hiện tại
                        //int intCurrentReviewLevel = objInventory.CurrentReviewLevelID;
                        //MasterData.PLC.MD.WSInventoryType.InventoryType_ReviewLevel obj = new MasterData.PLC.MD.WSInventoryType.InventoryType_ReviewLevel();
                        //new MasterData.PLC.MD.PLCInventoryType().LoadInfoReviewLevel(ref obj, intCurrentReviewLevel);
                        //if (obj != null)
                        //{
                        //    if (!obj.ReviewType)
                        //    {

                        //        DataTable dtbInventoryRvwLevel = objPLCInventory.GetInventoryReviewLevel(objInventory.InventoryID);
                        //        DataRow[] lstCheckReview = dtbInventoryRvwLevel.Select("REVIEWLEVELID = " + intCurrentReviewLevel + " AND REVIEWSTATUS >0");
                        //        foreach(DataRow rc in lstCheckReview)
                        //        {
                        //            strUserHasReviewed += rc["USERNAME"].ToString() + ",";
                        //        }
                        //    }
                        //}
                        //#endregion

                        if (objInventory != null && Convert.ToString(objInventory.InventoryID) != string.Empty)
                        {
                            lstInventorySelect.Add(objInventory);
                            strInventoryIDUnion = strInventoryIDUnion + "<" + Convert.ToString(row["INVENTORYID"]).Trim() + ">";
                        }
                    }
                }
                //if (strUserHasReviewed.Length > 0)
                //    strUserHasReviewed = "(" + strUserHasReviewed.Remove(strUserHasReviewed.Length -1, 1) + ")";
                DataTable dtbUserCanReviewAll = null;
                if (intCountSelect > 1)
                {
                    dtbUserCanReviewAll = objPLCInventory.GetUserCanReviewAll(strInventoryIDUnion);
                    if (dtbUserCanReviewAll == null)
                    {
                        MessageBox.Show(this, "Lỗi kiểm tra danh sách người duyệt!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (dtbUserCanReviewAll.Rows.Count == 0)
                    {
                        MessageBox.Show(this, "Không có người dùng nào có thể duyệt tất cả các phiếu được chọn. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                if (lstInventorySelect.Count > 0)
                {
                    string strUserList = string.Empty;
                    if (intCountSelect > 1)
                    {
                        foreach (DataRow row in dtbUserCanReviewAll.Rows)
                        {
                            strUserList = string.IsNullOrEmpty(strUserList) ? strUserList + row["USERNAME"].ToString() : strUserList + "," + row["USERNAME"].ToString();
                        }
                    }
                    string[] arrUserReviewList = string.IsNullOrEmpty(strUserList) ? null : strUserList.Split(',');
                    frmInventoryDifferenceResult frmInv = new frmInventoryDifferenceResult();
                    frmInv.lstInventory = lstInventorySelect;
                    frmInv.ReviewUserList = arrUserReviewList;
                    frmInv.ShowDialog();
                    if (frmInv.IsUpdateReviewStatus)
                        cmdSearch_Click(null, null);
                }
                else
                {
                    MessageBox.Show(this, "Chọn ít nhất 1 phiếu chưa duyệt! ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception objExce)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Lỗi duyệt phiếu kiểm kê");
                SystemErrorWS.Insert("Lỗi duyệt phiếu kiểm kê", objExce.ToString(), this.Name + " -> btnReview_Click", DUIInventory_Globals.ModuleName);
            }
        }
    }
}