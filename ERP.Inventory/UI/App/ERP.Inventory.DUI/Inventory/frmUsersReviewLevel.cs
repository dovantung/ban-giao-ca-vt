﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmUsersReviewLevel : Form
    {
        public DataTable dtbUsersReviewLevel = null;
        public frmUsersReviewLevel()
        {
            InitializeComponent();
        }

        private ERP.Inventory.PLC.Inventory.PLCInventory objPLCInventory = new PLC.Inventory.PLCInventory();
        public int intInventoryTypeID = 0;
        public DataTable dtbReviewLevel = null;
        public int intReviewLevel = 0;
        public int intSelectIndexReviewLevel = 0;
        public string strFunctionReview = string.Empty;
        public string strInventoryID = string.Empty;
        public string strInventoryUser = string.Empty;
        public int intStoreID = 0;
        public string strStoreName = string.Empty;
        public DateTime dtmInventoryDate;
        public int intIsReviewedInventory = 0;
        public bool bolIsUpdate = false;

        private void frmUsersReviewLevel_Load(object sender, EventArgs e)
        {
            intIsReviewedInventory = 0;
            bolIsUpdate = false;
            flexImportResult.DataSource = dtbUsersReviewLevel;
            CustomFlex();
            flexImportResult.AllowEditing = false;
        }
        private void CustomFlex()
        {
            flexImportResult.Cols["UserName"].Visible = false;
            flexImportResult.Cols["ReviewStatus"].Visible = false;
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnCaption(flexImportResult, "FullName,Người duyệt,StatusName,Trạng thái duyệt,IsReviewed,Đã duyệt,ReviewedDate,Ngày duyệt");
            flexImportResult.Cols["IsReviewed"].DataType = typeof(bool);
            flexImportResult.Cols["ReviewedDate"].DataType = typeof(DateTime);
            flexImportResult.Cols["ReviewedDate"].Format = "dd/MM/yyyy HH:mm";
            flexImportResult.Cols["FullName"].Width = 300;
            flexImportResult.Cols["StatusName"].Width = 100;
            flexImportResult.Cols["IsReviewed"].Width = 100;
            flexImportResult.Cols["ReviewedDate"].Width = 130;
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexImportResult);
        }

        private void mnuItemSelectUserReview_Click(object sender, EventArgs e)
        {
            frmSelectUsersReviewLevel frmSelectUsersReviewLevel1 = new frmSelectUsersReviewLevel();
            frmSelectUsersReviewLevel1.strUserName = Convert.ToString(flexImportResult[flexImportResult.RowSel, "UserName"]);
            frmSelectUsersReviewLevel1.intInventoryTypeID = intInventoryTypeID;
            frmSelectUsersReviewLevel1.dtbReviewLevel = dtbReviewLevel;
            frmSelectUsersReviewLevel1.intReviewLevel = intReviewLevel;
            frmSelectUsersReviewLevel1.intSelectIndexReviewLevel = intSelectIndexReviewLevel;
            frmSelectUsersReviewLevel1.strFunctionReview = strFunctionReview;
            frmSelectUsersReviewLevel1.strInventoryID = strInventoryID;
            frmSelectUsersReviewLevel1.strInventoryUser = strInventoryUser;
            frmSelectUsersReviewLevel1.intStoreID = intStoreID;
            frmSelectUsersReviewLevel1.strStoreName = strStoreName;
            frmSelectUsersReviewLevel1.dtmInventoryDate = dtmInventoryDate;
            frmSelectUsersReviewLevel1.ShowDialog();
            intIsReviewedInventory = frmSelectUsersReviewLevel1.intIsReviewedInventory;
            bolIsUpdate = frmSelectUsersReviewLevel1.bolIsUpdate;
            if (frmSelectUsersReviewLevel1.bolIsUpdate)
            {
                //dtbUsersReviewLevel = objPLCInventory.GetInventoryUserReviewed(strFunctionReview, strInventoryID);
                flexImportResult.DataSource = dtbUsersReviewLevel;
                CustomFlex();
                flexImportResult.AllowEditing = false;
            }
        }

        private void mnuExportExcel_Opening(object sender, CancelEventArgs e)
        {
            if (flexImportResult.Rows.Count <= flexImportResult.Rows.Fixed)
            {
                return;
            }
            if (flexImportResult.RowSel < flexImportResult.Rows.Fixed)
            {
                return;
            }

            mnuItemSelectUserReview.Enabled = false;
            if (!Convert.ToBoolean(flexImportResult[flexImportResult.RowSel, "IsReviewed"]))
            {
                mnuItemSelectUserReview.Enabled = true;
            }
        }
    }
}