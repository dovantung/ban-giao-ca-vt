﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using DevExpress.XtraGrid;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmInputResolveQuantity : Form
    {
        private bool bolAnswerYes = false;
        private bool bolAnswerNo = false;
        public DataTable dtbProduct = null;
        private bool bolOutputChange = false;
        private bool bolOutputChangeStatus = false;
        private bool bolInOutHandling = false;

        public bool AnswerYes
        {
            get
            {
                return bolAnswerYes;
            }
            set
            {
                bolAnswerYes = value;
            }
        }
        public bool AnswerNo
        {
            get
            {
                return bolAnswerNo;
            }
            set
            {
                bolAnswerNo = value;
            }
        }
        public bool OutputChange
        {
            get
            {
                return bolOutputChange;
            }
            set
            {
                bolOutputChange = value;
            }
        }
        public bool InOutHandling
        {
            get
            {
                return bolInOutHandling;
            }
            set
            {
                bolInOutHandling = value;
            }
        }

        public frmInputResolveQuantity()
        {
            InitializeComponent();
        }

        private void frmInputResolveQuantity_Load(object sender, EventArgs e)
        {
            DataTable dtbProductStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
            repItemProductStatusIDList.DataSource = dtbProductStatus;
            if (dtbProduct == null)
            {
                dtbProduct = new DataTable();
                dtbProduct.Columns.Add("PRODUCTID", typeof(string));
                dtbProduct.Columns.Add("PRODUCTNAME", typeof(string));
                dtbProduct.Columns.Add("DELTAADJUSTUNEVENTQUANTITY", typeof(int));
                dtbProduct.Columns.Add("DELTAREMAINQUANTITY", typeof(int));
                dtbProduct.Columns.Add("RESOLVEQUANTITY", typeof(int));
                dtbProduct.Columns.Add("RESOLVEQUANTITY2", typeof(int));
                dtbProduct.Columns.Add("RESOLVENOTE", typeof(string));
                dtbProduct.Columns.Add("RESOLVETYPE", typeof(int));
                dtbProduct.Columns.Add("UNEVENTID", typeof(string));
                dtbProduct.Columns.Add("CHANGEUNEVENTID", typeof(string));
                dtbProduct.Columns.Add("ERP_PRODUCTSTATUSID", typeof(int));
                dtbProduct.Columns.Add("ERP_PRODUCTSTATUSNAME", typeof(string));

                dtbProduct.Columns.Add("INV_PRODUCTSTATUSID", typeof(int));
                dtbProduct.Columns.Add("INV_PRODUCTSTATUSNAME", typeof(string));

                dtbProduct.Columns.Add("CHG_PRODUCTSTATUSID", typeof(int));
            }

            grdCtlProducts.DataSource = dtbProduct;
            //FormatFlex();
        }

        private void FormatFlex()
        {
            //if (grdCtlProducts.DataSource == null) return;
            //Library.AppCore.LoadControls.C1FlexGridObject.SetColumnCaption(flexProduct,
            //    "ProductID", "Mã sản phẩm",
            //    "ProductName", "Tên sản phẩm",
            //    "DeltaRemainQuantity", "Chênh lệch còn lại",
            //    "ResolveQuantity", "Số lượng cần xử lý",
            //    "ResolveNote", "Nội dung xử lý");

            //Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexProduct,
            //    false, "UnEventID,ChangeUnEventID,DeltaAdjustUnEventQuantity,ResolveType,ResolveQuantity2");

            //Library.AppCore.LoadControls.C1FlexGridObject.SetColumnWidth(flexProduct,
            //    "", 0,
            //    "ProductID", 100,
            //    "ProductName", 160,
            //    "DeltaRemainQuantity", 80,
            //    "ResolveQuantity", 70,
            //    "ResolveNote", 200);

            //flexProduct.Cols["DeltaRemainQuantity"].Format = "##0";
            //flexProduct.Cols["ResolveQuantity"].Format = "##0";

            //for (int i = 1; i < flexProduct.Cols.Count; i++)
            //{
            //    flexProduct.Cols[i].AllowEditing = false;
            //}

            //flexProduct.Cols["ResolveQuantity"].AllowEditing = true;

            //C1.Win.C1FlexGrid.CellStyle style = flexProduct.Styles.Add("flexStyle");
            //style.WordWrap = true;
            //style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            //C1.Win.C1FlexGrid.CellRange range = flexProduct.GetCellRange(0, 0, 0, flexProduct.Cols.Count - 1);
            //range.Style = style;
            //flexProduct.Rows[0].Height = 34;
        }

        private void cmdYes_Click(object sender, EventArgs e)
        {
            grdViewProducts.CloseEditor();
            grdViewProducts.UpdateCurrentRow();
            if (dtbProduct != null && dtbProduct.Rows.Count > 0)
            {
                dtbProduct.AcceptChanges();
                DataRow[] drProduct = dtbProduct.Select("ResolveQuantity = 0");
                if (drProduct.Length > 0)
                {
                    MessageBox.Show(this, "Sản phẩm " + Convert.ToString(drProduct[0]["ProductName"]).Trim() + " chưa nhập số lượng cần xử lý.\r\nSố lượng cần xử lý phải khác 0.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            AnswerNo = false;
            AnswerYes = true;
            this.Close();
        }

        private void cmdNo_Click(object sender, EventArgs e)
        {
            AnswerYes = false;
            AnswerNo = true;
            this.Close();
        }

        private void flexProduct_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            //try
            //{
            //    if (flexProduct.Rows.Count <= flexProduct.Rows.Fixed)
            //    {
            //        return;
            //    }
            //    switch (e.Col)
            //    {
            //        case 5:
            //            string strUnEventID = Convert.ToString(flexProduct[e.Row, "UnEventID"]);
            //            int intResolveQuantity = Convert.ToInt32(flexProduct[e.Row, "ResolveQuantity"]);
            //            int intDeltaAdjustUnEventQuantity = Convert.ToInt32(flexProduct[e.Row, "DeltaAdjustUnEventQuantity"]);
            //            string strProductName = Convert.ToString(flexProduct[e.Row, "ProductName"]);
            //            string strResolveQuantity = string.Empty;

            //            if (intDeltaAdjustUnEventQuantity < 0 && intResolveQuantity > 0)
            //            {
            //                MessageBox.Show(this, "Số lượng cần xử lý không hợp lệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //                flexProduct[e.Row, "ResolveQuantity"] = 0;
            //                return;
            //            }
            //            if (intDeltaAdjustUnEventQuantity > 0 && intResolveQuantity < 0)
            //            {
            //                MessageBox.Show(this, "Số lượng cần xử lý không hợp lệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //                flexProduct[e.Row, "ResolveQuantity"] = 0;
            //                return;
            //            }
            //            if (Math.Abs(intResolveQuantity) > Math.Abs(intDeltaAdjustUnEventQuantity))
            //            {
            //                MessageBox.Show(this, "Số lượng cần xử lý không hợp lệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //                flexProduct[e.Row, "ResolveQuantity"] = 0;
            //                return;
            //            }

            //            if (OutputChange)
            //            {
            //                string strProductNameOther = string.Empty;
            //                int intResolveQuantityOther = (-1) * intResolveQuantity;
            //                int intDeltaAdjustUnEventQuantityOther = 0;
            //                int intIndexOther = 0;
            //                string strResolveQuantityOther = string.Empty;
            //                if (Convert.ToString(dtbProduct.Rows[0]["UnEventID"]) != strUnEventID)
            //                {
            //                    strProductNameOther = Convert.ToString(dtbProduct.Rows[0]["ProductName"]).Trim();
            //                    intDeltaAdjustUnEventQuantityOther = Convert.ToInt32(dtbProduct.Rows[0]["DeltaAdjustUnEventQuantity"]);
            //                    intIndexOther = 0;
            //                }
            //                else
            //                {
            //                    strProductNameOther = Convert.ToString(dtbProduct.Rows[1]["ProductName"]).Trim();
            //                    intDeltaAdjustUnEventQuantityOther = Convert.ToInt32(dtbProduct.Rows[1]["DeltaAdjustUnEventQuantity"]);
            //                    intIndexOther = 1;
            //                }

            //                flexProduct[flexProduct.Rows.Fixed + intIndexOther, "ResolveQuantity"] = intResolveQuantityOther;

            //                flexProduct[e.Row, "DeltaRemainQuantity"] = intDeltaAdjustUnEventQuantity - intResolveQuantity - Convert.ToInt32(flexProduct[e.Row, "ResolveQuantity2"]);
            //                flexProduct[flexProduct.Rows.Fixed + intIndexOther, "DeltaRemainQuantity"] = intDeltaAdjustUnEventQuantityOther - intResolveQuantityOther - Convert.ToInt32(flexProduct[flexProduct.Rows.Fixed + intIndexOther, "ResolveQuantity2"]);

            //                strResolveQuantity = string.Empty;
            //                strResolveQuantityOther = string.Empty;
            //                if (intResolveQuantity == 0)
            //                {
            //                    strResolveQuantity = string.Empty;
            //                    flexProduct[e.Row, "ResolveType"] = 0;
            //                    strResolveQuantityOther = string.Empty;
            //                    flexProduct[flexProduct.Rows.Fixed + intIndexOther, "ResolveType"] = 0;
            //                }
            //                else
            //                {
            //                    if (intResolveQuantity < 0)
            //                    {
            //                        strResolveQuantity = intResolveQuantity.ToString().TrimStart('-') + ": nhập đổi " + strProductNameOther;
            //                        flexProduct[e.Row, "ResolveType"] = 1;

            //                        strResolveQuantityOther = intResolveQuantityOther.ToString() + ": xuất đổi " + strProductName;
            //                        flexProduct[flexProduct.Rows.Fixed + intIndexOther, "ResolveType"] = 1;
            //                    }
            //                    else
            //                    {
            //                        strResolveQuantity = intResolveQuantity.ToString() + ": xuất đổi " + strProductNameOther;
            //                        flexProduct[e.Row, "ResolveType"] = 1;

            //                        strResolveQuantityOther = intResolveQuantityOther.ToString().TrimStart('-') + ": nhập đổi " + strProductName;
            //                        flexProduct[flexProduct.Rows.Fixed + intIndexOther, "ResolveType"] = 1;
            //                    }
            //                }
            //                flexProduct[e.Row, "ResolveNote"] = strResolveQuantity;
            //                flexProduct[flexProduct.Rows.Fixed + intIndexOther, "ResolveNote"] = strResolveQuantityOther;

            //                try
            //                {
            //                    flexProduct[e.Row, "ChangeUnEventID"] = Convert.ToString(flexProduct[flexProduct.Rows.Fixed + intIndexOther, "UnEventID"]);
            //                }
            //                catch { }
            //                try
            //                {
            //                    flexProduct[flexProduct.Rows.Fixed + intIndexOther, "ChangeUnEventID"] = Convert.ToString(flexProduct[e.Row, "UnEventID"]);
            //                }
            //                catch { }
            //            }
            //            else
            //            {
            //                flexProduct[e.Row, "DeltaRemainQuantity"] = intDeltaAdjustUnEventQuantity - intResolveQuantity - Convert.ToInt32(flexProduct[e.Row, "ResolveQuantity2"]);
            //                strResolveQuantity = string.Empty;
            //                if (intResolveQuantity == 0)
            //                {
            //                    strResolveQuantity = string.Empty;
            //                    flexProduct[e.Row, "ResolveType"] = 0;
            //                }
            //                else
            //                {
            //                    if (intResolveQuantity < 0)
            //                    {
            //                        strResolveQuantity = intResolveQuantity.ToString().TrimStart('-') + ": nhập thừa";
            //                        flexProduct[e.Row, "ResolveType"] = 2;
            //                    }
            //                    else
            //                    {
            //                        strResolveQuantity = intResolveQuantity.ToString() + ": xuất thiếu";
            //                        flexProduct[e.Row, "ResolveType"] = 3;
            //                    }
            //                }
            //                flexProduct[e.Row, "ResolveNote"] = strResolveQuantity;
            //            }
            //            break;
            //    }
            //}
            //catch
            //{
            //}
        }

        private void grdViewProducts_HiddenEditor(object sender, EventArgs e)
        {
            int intRowFocus = grdViewProducts.FocusedRowHandle;
            if (intRowFocus == GridControl.AutoFilterRowHandle)
                return;
            string strFieldName = grdViewProducts.FocusedColumn.FieldName;
            DataRow dr = grdViewProducts.GetDataRow(intRowFocus);
            if (dr == null)
                return;
            string strUnEventID = Convert.ToString(grdViewProducts.GetFocusedRowCellValue("UNEVENTID"));
            string strProductName = Convert.ToString(grdViewProducts.GetFocusedRowCellValue("PRODUCTNAME"));
            int intResolveQuantity = 0;
            if (!Convert.IsDBNull(grdViewProducts.GetFocusedRowCellValue("RESOLVEQUANTITY")))
                intResolveQuantity = Convert.ToInt32(grdViewProducts.GetFocusedRowCellValue("RESOLVEQUANTITY"));
            int intDeltaAdjustUnEventQuantity = 0;
            if (!Convert.IsDBNull(grdViewProducts.GetFocusedRowCellValue("DELTAADJUSTUNEVENTQUANTITY")))
                intDeltaAdjustUnEventQuantity = Convert.ToInt32(grdViewProducts.GetFocusedRowCellValue("DELTAADJUSTUNEVENTQUANTITY"));

            string strResolveQuantity = string.Empty;
            try
            {

                switch (strFieldName)
                {
                    case "RESOLVEQUANTITY":
                        if (intDeltaAdjustUnEventQuantity < 0 && intResolveQuantity > 0)
                        {
                            MessageBox.Show(this, "Số lượng cần xử lý không hợp lệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            grdViewProducts.SetRowCellValue(intRowFocus, strFieldName, 0);
                            intResolveQuantity = 0;
                        }
                        if (intDeltaAdjustUnEventQuantity > 0 && intResolveQuantity < 0)
                        {
                            MessageBox.Show(this, "Số lượng cần xử lý không hợp lệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            grdViewProducts.SetRowCellValue(intRowFocus, strFieldName, 0);
                            intResolveQuantity = 0;
                        }
                        if (Math.Abs(intResolveQuantity) > Math.Abs(intDeltaAdjustUnEventQuantity))
                        {
                            MessageBox.Show(this, "Số lượng cần xử lý không hợp lệ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            grdViewProducts.SetRowCellValue(intRowFocus, strFieldName, 0);
                            intResolveQuantity = 0;
                        }

                        if (OutputChange)
                        {
                            string strProductNameOther = string.Empty;
                            int intResolveQuantityOther = (-1) * intResolveQuantity;
                            int intDeltaAdjustUnEventQuantityOther = 0;
                            int intIndexOther = 0;
                            string strResolveQuantityOther = string.Empty;
                            if (Convert.ToString(dtbProduct.Rows[0]["UNEVENTID"]) != strUnEventID)
                            {
                                strProductNameOther = Convert.ToString(dtbProduct.Rows[0]["PRODUCTNAME"]).Trim();
                                intDeltaAdjustUnEventQuantityOther = Convert.ToInt32(dtbProduct.Rows[0]["DELTAADJUSTUNEVENTQUANTITY"]);
                                intIndexOther = 0;
                            }
                            else
                            {
                                strProductNameOther = Convert.ToString(dtbProduct.Rows[1]["PRODUCTNAME"]).Trim();
                                intDeltaAdjustUnEventQuantityOther = Convert.ToInt32(dtbProduct.Rows[1]["DELTAADJUSTUNEVENTQUANTITY"]);
                                intIndexOther = 1;
                            }
                            grdViewProducts.SetRowCellValue(intRowFocus + intIndexOther, strFieldName, intResolveQuantityOther);
                            //flexProduct[flexProduct.Rows.Fixed + intIndexOther, "ResolveQuantity"] = intResolveQuantityOther;
                            grdViewProducts.SetRowCellValue(intRowFocus, "DELTAREMAINQUANTITY",
                                intDeltaAdjustUnEventQuantity - intResolveQuantity - Convert.ToInt32(dr["RESOLVEQUANTITY2"]));
                            //flexProduct[e.Row, "DeltaRemainQuantity"] = intDeltaAdjustUnEventQuantity - intResolveQuantity - Convert.ToInt32(flexProduct[e.Row, "ResolveQuantity2"]);
                            grdViewProducts.SetRowCellValue(intRowFocus + intIndexOther, "DELTAREMAINQUANTITY",
                                intDeltaAdjustUnEventQuantityOther - intResolveQuantityOther - Convert.ToInt32(dtbProduct.Rows[intRowFocus + intIndexOther]["RESOLVEQUANTITY2"]));
                            // flexProduct[flexProduct.Rows.Fixed + intIndexOther, "DeltaRemainQuantity"] = intDeltaAdjustUnEventQuantityOther - intResolveQuantityOther - Convert.ToInt32(flexProduct[flexProduct.Rows.Fixed + intIndexOther, "ResolveQuantity2"]);

                            strResolveQuantity = string.Empty;
                            strResolveQuantityOther = string.Empty;
                            if (intResolveQuantity == 0)
                            {
                                strResolveQuantity = string.Empty;
                                dr["RESOLVETYPE"] = 0;
                                strResolveQuantityOther = string.Empty;
                                dtbProduct.Rows[intRowFocus + intIndexOther]["RESOLVETYPE"] = 0;
                            }
                            else
                            {
                                if (intResolveQuantity < 0)
                                {
                                    strResolveQuantity = intResolveQuantity.ToString().TrimStart('-') + ": nhập đổi " + strProductNameOther;
                                    dr["RESOLVETYPE"] = 1;

                                    strResolveQuantityOther = intResolveQuantityOther.ToString() + ": xuất đổi " + strProductName;
                                    dtbProduct.Rows[intRowFocus + intIndexOther]["RESOLVETYPE"] = 1;
                                }
                                else
                                {
                                    strResolveQuantity = intResolveQuantity.ToString() + ": xuất đổi " + strProductNameOther;
                                    dr["RESOLVETYPE"] = 1;

                                    strResolveQuantityOther = intResolveQuantityOther.ToString().TrimStart('-') + ": nhập đổi " + strProductName;
                                    dtbProduct.Rows[intRowFocus + intIndexOther]["RESOLVETYPE"] = 1;
                                }
                            }
                            dr["RESOLVENOTE"] = strResolveQuantity;
                            dtbProduct.Rows[intRowFocus + intIndexOther]["RESOLVENOTE"] = strResolveQuantityOther;

                            try
                            {
                                dr["CHANGEUNEVENTID"] = Convert.ToString(dtbProduct.Rows[intRowFocus + intIndexOther]["UNEVENTID"]);
                            }
                            catch { }
                            try
                            {
                                dtbProduct.Rows[intRowFocus + intIndexOther]["CHANGEUNEVENTID"] = Convert.ToString(dr["UNEVENTID"]);
                            }
                            catch { }
                        }
                        else
                        {
                            dr["DELTAREMAINQUANTITY"] = intDeltaAdjustUnEventQuantity - intResolveQuantity - Convert.ToInt32(dr["ResolveQuantity2"]);
                            strResolveQuantity = string.Empty;
                            if (intResolveQuantity == 0)
                            {
                                strResolveQuantity = string.Empty;
                                dr["RESOLVETYPE"] = 0;
                            }
                            else
                            {
                                if (intResolveQuantity < 0)
                                {
                                    strResolveQuantity = intResolveQuantity.ToString().TrimStart('-') + ": nhập thừa";
                                    dr["RESOLVETYPE"] = 2;
                                }
                                else
                                {
                                    strResolveQuantity = intResolveQuantity.ToString() + ": xuất thiếu";
                                    dr["RESOLVETYPE"] = 3;
                                }
                            }
                            dr["RESOLVENOTE"] = strResolveQuantity;
                        }
                        break;
                }
            }
            catch
            {
            }
        }

    }
}