﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmInventoryInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnUndo = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ucUserName = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.cboStore = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.cboInventoryTerm = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.cboMainGroup = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.cboSubGroup = new Library.AppControl.ComboBoxControl.ComboBoxSubGroup();
            this.cboProductStateID = new System.Windows.Forms.ComboBox();
            this.chkBCCS = new System.Windows.Forms.CheckBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "Loại hàng:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 76);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Kho kiểm kê:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 106);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "Ngành hàng:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 46);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Kỳ kiểm kê:";
            // 
            // btnUndo
            // 
            this.btnUndo.Image = global::ERP.Inventory.DUI.Properties.Resources.undo2;
            this.btnUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUndo.Location = new System.Drawing.Point(351, 190);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(100, 25);
            this.btnUndo.TabIndex = 14;
            this.btnUndo.Text = "  Bỏ qua";
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnConfirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirm.Location = new System.Drawing.Point(244, 190);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(100, 25);
            this.btnConfirm.TabIndex = 13;
            this.btnConfirm.Text = "   Xác nhận";
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 138);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Nhóm hàng:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên đăng nhập:";
            // 
            // ucUserName
            // 
            this.ucUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucUserName.IsOnlyShowRealStaff = true;
            this.ucUserName.IsValidate = true;
            this.ucUserName.Location = new System.Drawing.Point(137, 14);
            this.ucUserName.Margin = new System.Windows.Forms.Padding(0);
            this.ucUserName.MaximumSize = new System.Drawing.Size(400, 22);
            this.ucUserName.MinimumSize = new System.Drawing.Size(0, 22);
            this.ucUserName.Name = "ucUserName";
            this.ucUserName.Size = new System.Drawing.Size(314, 22);
            this.ucUserName.TabIndex = 1;
            this.ucUserName.UserName = "";
            this.ucUserName.WorkingPositionID = 0;
            this.ucUserName.Validating += new System.EventHandler(this.ucUserName_Validating);
            this.ucUserName.Leave += new System.EventHandler(this.ucUserName_Leave);
            // 
            // cboStore
            // 
            this.cboStore.Enabled = false;
            this.cboStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStore.Location = new System.Drawing.Point(137, 71);
            this.cboStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStore.Name = "cboStore";
            this.cboStore.Size = new System.Drawing.Size(314, 24);
            this.cboStore.TabIndex = 5;
            this.cboStore.SelectionChangeCommitted += new System.EventHandler(this.cboStore_SelectionChangeCommitted);
            // 
            // cboInventoryTerm
            // 
            this.cboInventoryTerm.Enabled = false;
            this.cboInventoryTerm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInventoryTerm.Location = new System.Drawing.Point(137, 41);
            this.cboInventoryTerm.Margin = new System.Windows.Forms.Padding(0);
            this.cboInventoryTerm.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInventoryTerm.Name = "cboInventoryTerm";
            this.cboInventoryTerm.Size = new System.Drawing.Size(314, 24);
            this.cboInventoryTerm.TabIndex = 3;
            this.cboInventoryTerm.SelectionChangeCommitted += new System.EventHandler(this.cboInventoryTerm_SelectionChangeCommitted_1);
            // 
            // cboMainGroup
            // 
            this.cboMainGroup.Enabled = false;
            this.cboMainGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroup.Location = new System.Drawing.Point(137, 102);
            this.cboMainGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroup.Name = "cboMainGroup";
            this.cboMainGroup.Size = new System.Drawing.Size(314, 24);
            this.cboMainGroup.TabIndex = 7;
            this.cboMainGroup.SelectionChangeCommitted += new System.EventHandler(this.cboMainGroup_SelectionChangeCommitted_1);
            // 
            // cboSubGroup
            // 
            this.cboSubGroup.Enabled = false;
            this.cboSubGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSubGroup.Location = new System.Drawing.Point(137, 134);
            this.cboSubGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboSubGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboSubGroup.Name = "cboSubGroup";
            this.cboSubGroup.Size = new System.Drawing.Size(314, 24);
            this.cboSubGroup.TabIndex = 9;
            // 
            // cboProductStateID
            // 
            this.cboProductStateID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProductStateID.DropDownWidth = 250;
            this.cboProductStateID.Enabled = false;
            this.cboProductStateID.Location = new System.Drawing.Point(137, 162);
            this.cboProductStateID.Name = "cboProductStateID";
            this.cboProductStateID.Size = new System.Drawing.Size(314, 24);
            this.cboProductStateID.TabIndex = 80;
            // 
            // chkBCCS
            // 
            this.chkBCCS.AutoSize = true;
            this.chkBCCS.Location = new System.Drawing.Point(137, 195);
            this.chkBCCS.Name = "chkBCCS";
            this.chkBCCS.Size = new System.Drawing.Size(63, 20);
            this.chkBCCS.TabIndex = 81;
            this.chkBCCS.Text = "BCCS";
            this.chkBCCS.UseVisualStyleBackColor = true;
            // 
            // btnExport
            // 
            this.btnExport.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExport.Location = new System.Drawing.Point(244, 221);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(207, 23);
            this.btnExport.TabIndex = 82;
            this.btnExport.Text = "Xuất excel tồn kho hiện tại";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // frmInventoryInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 250);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.chkBCCS);
            this.Controls.Add(this.cboProductStateID);
            this.Controls.Add(this.cboSubGroup);
            this.Controls.Add(this.cboMainGroup);
            this.Controls.Add(this.cboInventoryTerm);
            this.Controls.Add(this.cboStore);
            this.Controls.Add(this.ucUserName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnUndo);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(460, 278);
            this.Name = "frmInventoryInput";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kiểm kê";
            this.Load += new System.EventHandler(this.frmInventoryInput_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ucUserName;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStore;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboInventoryTerm;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroup;
        private Library.AppControl.ComboBoxControl.ComboBoxSubGroup cboSubGroup;
        private System.Windows.Forms.ComboBox cboProductStateID;
        private System.Windows.Forms.CheckBox chkBCCS;
        private System.Windows.Forms.Button btnExport;
    }
}