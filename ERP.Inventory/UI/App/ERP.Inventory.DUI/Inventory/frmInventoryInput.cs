﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmInventoryInput : Form
    {
        public frmInventoryInput()
        {
            InitializeComponent();
        }

        #region variable
        private ERP.MasterData.SYS.PLC.PLCUser objPLCUser = new MasterData.SYS.PLC.PLCUser();
        private MasterData.PLC.MD.PLCMainGroup objPLCMainGroup = new MasterData.PLC.MD.PLCMainGroup();
        private ERP.Inventory.PLC.Inventory.PLCInventoryTerm objPLCInventoryTerm = new PLC.Inventory.PLCInventoryTerm();
        private ERP.Inventory.PLC.Inventory.PLCInventory objPLCInventory = new PLC.Inventory.PLCInventory();
        private ERP.Inventory.PLC.Inventory.WSInventoryTerm.InventoryTerm objInventoryTerm = null;
        private int intSelectedIndex_InventoryType = 0;
        private int intSelectedIndex_Store = 0;
        private int intSelectedIndex_MainGroup = 0;
        private DataTable dtbINVTerm_Brand = null;
        private int intIsLoadInventoryTerm = -1;
        private bool bolUpdateStockdatatime = false;
        private string strusername = string.Empty;
        bool bolIsHasSubGroup = false;
        #endregion

        #region method
        private void LoadComboBox()
        {
            DataTable dtbINVTerm = new DataTable();
            dtbINVTerm.Columns.Add("INVENTORYTERMID", typeof(int));
            dtbINVTerm.Columns.Add("INVENTORYTERMNAME", typeof(string));
            cboInventoryTerm.InitControl(false, dtbINVTerm, "INVENTORYTERMID", "INVENTORYTERMNAME", "-- Chọn kỳ kiểm kê --");
            ucUserName.IsOnlyActived = true;
            cboMainGroup.InitControl(true, false);
            cboMainGroup.IsReturnAllWhenNotChoose = true;
            cboSubGroup.InitControl(true, cboMainGroup.MainGroupIDList);
            cboStore.InitControl(false, false);
            intIsLoadInventoryTerm = 0;
            DataTable dtbProductStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
            cboProductStateID.DataSource = dtbProductStatus;
            cboProductStateID.DisplayMember = "PRODUCTSTATUSNAME";
            cboProductStateID.ValueMember = "PRODUCTSTATUSID";
            cboProductStateID.SelectedValue = 1;
        }

        private void Reset()
        {
            intSelectedIndex_InventoryType = 0;
            ucUserName.UserName = string.Empty;
            intIsLoadInventoryTerm = 0;
            cboInventoryTerm.SetValue(-1);
            cboInventoryTerm.Enabled = false;
            strusername = string.Empty;
            cboProductStateID.SelectedValue = 1;
            ClearInfoTerm();
        }

        private void ClearInfoTerm()
        {
            if (cboMainGroup.DataSource != null && (cboMainGroup.DataSource as DataTable).Rows.Count > 0)
            {
                DataTable dtbTerm_MainGroup = (cboMainGroup.DataSource as DataTable).Clone();
                DataRow rNewMainGroup = dtbTerm_MainGroup.NewRow();
                rNewMainGroup.ItemArray = (cboMainGroup.DataSource as DataTable).Rows[0].ItemArray;
                dtbTerm_MainGroup.Rows.Add(rNewMainGroup);
                //cboMainGroup.DataSource = dtbTerm_MainGroup;
                //cboMainGroup.ValueMember = dtbTerm_MainGroup.Columns[0].ColumnName;
                //cboMainGroup.DisplayMember = dtbTerm_MainGroup.Columns[1].ColumnName;
                cboMainGroup.InitControl(true, dtbTerm_MainGroup);
                cboMainGroup.Enabled = false;
                if (cboMainGroup.DataSource != null)
                    cboMainGroup.SetValue(-1);
                cboMainGroup_SelectionChangeCommitted_1(null, null);
            }

            if (cboStore.DataSource != null && (cboStore.DataSource as DataTable).Rows.Count > 0)
            {
                DataTable dtbTerm_Store = (cboStore.DataSource as DataTable).Clone();
                DataRow rNewStore = dtbTerm_Store.NewRow();
                rNewStore.ItemArray = (cboStore.DataSource as DataTable).Rows[0].ItemArray;
                dtbTerm_Store.Rows.Add(rNewStore);
                //cboStore.DataSource = dtbTerm_Store;
                //cboStore.ValueMember = dtbTerm_Store.Columns[0].ColumnName;
                //cboStore.DisplayMember = dtbTerm_Store.Columns[1].ColumnName;
                cboStore.InitControl(false, dtbTerm_Store);
                cboStore.Enabled = false;
                if (cboStore.DataSource != null)
                    cboStore.SetValue(-1);
            }

            if (cboSubGroup.DataSource != null)
                cboSubGroup.SetValue(-1);
            cboSubGroup.Enabled = false;

        }

        private bool CheckInput()
        {

            if (cboInventoryTerm.ColumnID > 0)
            {
                objInventoryTerm = new PLC.Inventory.WSInventoryTerm.InventoryTerm();
                objPLCInventoryTerm.LoadInfo_ByUser(ref objInventoryTerm, cboInventoryTerm.ColumnID, ucUserName.UserName);
                DataTable dtbTerm_Store = objInventoryTerm.dtbTerm_Store.DefaultView.ToTable();
                if (!bolUpdateStockdatatime)
                {
                    if (dtbTerm_Store.Select("UPDATESTOCKDATATIME IS NOT NULL ").Length == 0)
                    {
                        MessageBox.Show(this, "Kỳ kiểm kê chưa tính tồn kho. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        bolUpdateStockdatatime = true;
                        cboInventoryTerm.SetValue(-1);
                        cboInventoryTerm_SelectionChangeCommitted_1(null, null);
                        bolUpdateStockdatatime = false;

                        return false;
                    }
                }
            }
            if (ucUserName.UserName == "")
            {
                MessageBox.Show(this, "Vui lòng chọn tên đăng nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ucUserName.Focus();
                return false;
            }
            if (cboInventoryTerm.ColumnID < 0)
            {
                MessageBox.Show(this, "Vui lòng chọn kỳ kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboInventoryTerm.Focus();
                SendKeys.Send("{F4}");
                return false;
            }
            string strTempt = ucUserName.UserName.Trim();
            if (strTempt == "")
            {
                MessageBox.Show(this, "Vui lòng chọn tên đăng nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ucUserName.Focus();
                return false;

            }

            if (cboStore.StoreID < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboStore.Focus();
                return false;
            }
            //if (cboMainGroup.MainGroupIDList == "-1")
            //{
            //    MessageBox.Show(this, "Vui lòng chọn ngành hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    cboMainGroup.Focus();
            //    return false;
            //}

            if (Convert.ToInt32(cboProductStateID.SelectedValue) == -1)
            {
                MessageBox.Show(this, "Vui lòng chọn loại hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }



            if (objInventoryTerm != null)
            {
                if (objInventoryTerm.ProductStatusID != -1 && objInventoryTerm.ProductStatusID != Convert.ToInt32(cboProductStateID.SelectedValue))
                {
                    DataTable dtb = (DataTable)cboProductStateID.DataSource;
                    DataRow[] drs = dtb.Select("PRODUCTSTATUSID = " + objInventoryTerm.ProductStatusID);
                    string strName = (drs[0]["PRODUCTSTATUSNAME"] ?? "").ToString().Trim();
                    MessageBox.Show(this, "Kỳ kiểm kê [" + Convert.ToString(cboInventoryTerm.ColumnName).Trim() + "] chỉ được khai báo loại hàng " + strName + ". Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

            }

            return true;
        }

        private bool CheckFunction()
        {
            DataTable dtbTerm_Store = cboStore.DataSource as DataTable;
            int intSubGroupID = -1;
            if (cboSubGroup.Visible && cboSubGroup.SubGroupID > 0)
                intSubGroupID = Convert.ToInt32(cboSubGroup.SubGroupID);
            string strInventoryID = objPLCInventory.CheckExistInventory(cboInventoryTerm.ColumnID, Convert.ToInt32(cboStore.StoreID), Convert.ToInt32(cboMainGroup.MainGroupID), Convert.ToInt32(cboProductStateID.SelectedValue), intSubGroupID);
            if (strInventoryID.Trim() != string.Empty)
            {
                MessageBox.Show(this, "Kho " + Convert.ToString(cboStore.Text).Trim() + " đã tồn tại phiếu kết nối " + strInventoryID + ". Không thể tiếp tục kiểm kê. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            //DataTable dtbInStockProduct = objPLCInventory.GetListTermStock(objInventoryTerm.InventoryTermID, Convert.ToInt32(cboStore.SelectedValue), Convert.ToInt32(cboMainGroup.SelectedValue));
            //if (dtbInStockProduct.Rows.Count < 1)
            //{
            //    MessageBox.Show(this, "Sản phẩm không tồn trong kho " + Convert.ToString(cboStore.Text).Trim() + ". Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return false;
            //}

            DateTime dtmInventoryDate = Library.AppCore.Globals.GetServerDateTime();
            if (dtmInventoryDate < objInventoryTerm.LockDataTime)
            {
                MessageBox.Show(this, "Thời điểm kiểm kê " + dtmInventoryDate.ToString("dd/MM/yyyy HH:mm") + " không được nhỏ hơn thời gian chốt tồn kho " + ((DateTime)objInventoryTerm.LockDataTime).ToString("dd/MM/yyyy HH:mm") + ". Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        #endregion

        #region event
        private void frmInventoryInput_Load(object sender, EventArgs e)
        {
            LoadComboBox();
            intIsLoadInventoryTerm = 0;
        }


        //private void cboStore_DropDown(object sender, EventArgs e)
        //{
        //    intSelectedIndex_Store = cboStore.StoreID;
        //}

        //private void cboMainGroup_DropDown(object sender, EventArgs e)
        //{
        //    intSelectedIndex_MainGroup = cboMainGroup.MainGroupID;
        //}


        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            if (!CheckFunction())
            {
                return;
            }

            List<string> ListUserName = new List<string>();
            //// Mo ra khi len cac moi truong khac (HUE)
            ListUserName.Add(ucUserName.UserName.Trim());
            Library.Login.frmCertifyLogin frmCertifyLogin = new Library.Login.frmCertifyLogin("Xác nhận kỳ kiểm kê", "!\nChỉ nhân viên đăng ký mới được xác nhận!", ListUserName, null, true, true);
            frmCertifyLogin.ShowDialog(this);

            if (!frmCertifyLogin.IsDisposed && !frmCertifyLogin.IsCorrect)
            {
                return;

            }

            #region Man hinh cũ

            //frmInventory frmInventory1 = new frmInventory();
            //frmInventory1.objInventoryTerm = objInventoryTerm;
            //frmInventory1.intMainGroupID = Convert.ToInt32(cboMainGroup.MainGroupID);
            //frmInventory1.intStoreID = Convert.ToInt32(cboStore.StoreID);
            //frmInventory1.intProductStatusID = Convert.ToInt32(cboProductStateID.SelectedValue);
            //frmInventory1.strInventoryUser = Convert.ToString(ucUserName.UserName).Trim();
            //frmInventory1.dtbINVTerm_Brand = dtbINVTerm_Brand.Copy();
            //int intSubGroupID = -1;
            //if (cboSubGroup.Visible && cboSubGroup.SubGroupID > 0)
            //    intSubGroupID = Convert.ToInt32(cboSubGroup.SubGroupID);
            //frmInventory1.intSubGroupID = intSubGroupID;
            #endregion

            frmInventoryVTS frmInventory1 = new frmInventoryVTS();
            frmInventory1.objInventoryTerm = objInventoryTerm;
            frmInventory1.InventoryTermID = cboInventoryTerm.ColumnID;
            frmInventory1.MaingroupIDList = cboMainGroup.MainGroupIDList;// Convert.ToInt32(cboMainGroup.MainGroupID);
            frmInventory1.SubgroupIDList = cboSubGroup.SubGroupIDList;
            frmInventory1.StoreID = Convert.ToInt32(cboStore.StoreID);
            frmInventory1.ProductStateID = Convert.ToInt32(cboProductStateID.SelectedValue);
            frmInventory1.InventoryUser = Convert.ToString(ucUserName.UserName).Trim();
            frmInventory1.IsBCCS = chkBCCS.Checked;

            //frmInventory1.dtbINVTerm_Brand = dtbINVTerm_Brand.Copy();
            frmInventory1.ShowDialog();
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            Reset();
        }

        #endregion

        private void ucUserName_Leave(object sender, EventArgs e)
        {
            if (ucUserName.UserName != strusername)
            {
                intIsLoadInventoryTerm = 0;
                strusername = ucUserName.UserName;
                DateTime dtmServerDate = Library.AppCore.Globals.GetServerDateTime();
                object[] objKeyword = new object[] { "@FROMDATE", dtmServerDate.AddDays(-100), "@TODATE", dtmServerDate.AddDays(1), "@USER", ucUserName.UserName.Trim() };
                DataTable tblInvTerm = objPLCInventoryTerm.SearchData_ByUser(objKeyword);
                tblInvTerm.DefaultView.Sort = "InventoryDate DESC";
                tblInvTerm = tblInvTerm.DefaultView.ToTable();
                if (tblInvTerm.Rows.Count == 0)
                {
                    DataRow r = tblInvTerm.NewRow();
                    cboInventoryTerm.InitControl(false, tblInvTerm, "INVENTORYTERMID", "INVENTORYTERMNAME", "-- Chọn kỳ kiểm kê --");
                    cboInventoryTerm.Enabled = true;
                    ClearInfoTerm();
                }
                else
                {
                    //cboInventoryTerm.SetCombo(tblInvTerm);
                    cboInventoryTerm.InitControl(false, tblInvTerm, "INVENTORYTERMID", "INVENTORYTERMNAME", "");
                    cboInventoryTerm.Enabled = true;
                    intIsLoadInventoryTerm = 1;
                    if (tblInvTerm.Rows.Count == 1)
                    {
                        objPLCInventoryTerm.LoadInfo_ByUser(ref objInventoryTerm, Convert.ToInt32(tblInvTerm.Rows[0]["INVENTORYTERMID"]), ucUserName.UserName);
                        DataTable dtbTerm_Store = objInventoryTerm.dtbTerm_Store.DefaultView.ToTable();
                        if (dtbTerm_Store.Select("UPDATESTOCKDATATIME IS NOT NULL ").Length == 0)
                        {
                            bolUpdateStockdatatime = true;
                            cboInventoryTerm.SetValue(-1);
                            cboInventoryTerm_SelectionChangeCommitted_1(null, null);
                            bolUpdateStockdatatime = false;
                            return;
                        }
                        else
                        {

                            cboInventoryTerm.SetValue(Convert.ToInt32(tblInvTerm.Rows[0]["INVENTORYTERMID"]));
                            cboInventoryTerm_SelectionChangeCommitted_1(null, null);
                        }
                    }
                    else

                        cboInventoryTerm.SetValue(-1);
                    cboInventoryTerm_SelectionChangeCommitted_1(null, null);
                }
            }

        }
        private void cboInventoryTerm_SelectionChangeCommitted_1(object sender, EventArgs e)
        {

            if (intIsLoadInventoryTerm != 1)
                return;
            if (cboInventoryTerm.ColumnID < 0)
            {
                cboMainGroup.SetValue(-1);
                cboSubGroup.SetValue(-1);
                cboStore.SetValue(-1);
                cboProductStateID.SelectedValue = 1;
                cboMainGroup.Enabled = false;
                cboSubGroup.Enabled = false;
                cboStore.Enabled = false;
            }
            else
            {
                objPLCInventoryTerm.LoadInfo_ByUser(ref objInventoryTerm, cboInventoryTerm.ColumnID, ucUserName.UserName);

                DataTable dtbTerm_Store = objInventoryTerm.dtbTerm_Store.DefaultView.ToTable();
                if (!bolUpdateStockdatatime)
                {
                    if (dtbTerm_Store.Select("UPDATESTOCKDATATIME IS NOT NULL ").Length == 0)
                    {
                        MessageBox.Show(this, "Kỳ kiểm kê chưa tính tồn kho. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        bolUpdateStockdatatime = true;
                        cboInventoryTerm.SetValue(-1);
                        cboInventoryTerm_SelectionChangeCommitted_1(null, null);
                        //cboMainGroup.SetValue(-1);
                        //cboSubGroup.SetValue(-1);
                        //cboStore.SetValue(-1);
                        //radIsNew.Checked = true;
                        //cboMainGroup.Enabled = false;
                        //cboSubGroup.Enabled = false;
                        //cboStore.Enabled = false;
                        //radIsNew.Enabled = false;
                        //radIsNew.Enabled = false;
                        bolUpdateStockdatatime = false;
                        return;
                    }
                }

                objInventoryTerm.dtbTerm_MainGroup.DefaultView.RowFilter = "IsSelect = 1";
                DataTable dtbTerm_MainGroup = objInventoryTerm.dtbTerm_MainGroup.DefaultView.ToTable();

                if (dtbTerm_MainGroup.Columns.Contains("IsSelect"))
                {
                    dtbTerm_MainGroup.Columns.Remove("IsSelect");
                }
                if (dtbTerm_MainGroup.Rows.Count > 0)
                {
                    cboMainGroup.InitControl(true, dtbTerm_MainGroup);
                    cboMainGroup.SetValue(-1);
                    cboMainGroup.Enabled = true;
                }

                if (objInventoryTerm.dtbTerm_SubGroup.Rows.Count > 0)
                {
                    if (objInventoryTerm.dtbTerm_SubGroup.Rows.Count > 1)
                    {
                        cboSubGroup.SetValue(-1);
                    }
                    objInventoryTerm.dtbTerm_SubGroup.DefaultView.RowFilter = "IsSelect = 1";
                    DataTable dtbTerm_SubGroup = objInventoryTerm.dtbTerm_SubGroup.DefaultView.ToTable();
                    if (dtbTerm_SubGroup.Columns.Contains("IsSelect"))
                        dtbTerm_SubGroup.Columns.Remove("IsSelect");
                    if (dtbTerm_SubGroup.Rows.Count > 0)
                        bolIsHasSubGroup = true;
                }
                cboMainGroup_SelectionChangeCommitted_1(null, null);
                objInventoryTerm.dtbTerm_Store.DefaultView.RowFilter = "IsSelect = 1";

                if (dtbTerm_Store.Columns.Contains("IsSelect"))
                {
                    dtbTerm_Store.Columns.Remove("IsSelect");
                }
                cboStore.InitControl(false, dtbTerm_Store);
                cboStore.Enabled = true;

                dtbINVTerm_Brand = objInventoryTerm.dtbTerm_Brand.Copy();

                if (cboSubGroup.DataSource != null)
                    cboSubGroup.SetValue(-1);
                cboSubGroup.Enabled = true;
                cboProductStateID.Enabled = true;

                if (cboInventoryTerm.ColumnID < 0)
                {
                    cboStore.Enabled = false;
                    cboMainGroup.Enabled = false;
                    cboSubGroup.Enabled = false;
                    cboProductStateID.Enabled = false;
                }
                int intProductStatusID = objInventoryTerm.ProductStatusID;
                if (intProductStatusID < 0)
                    intProductStatusID = 1;
                cboProductStateID.SelectedValue = intProductStatusID;
            }
        }

        private void cboMainGroup_SelectionChangeCommitted_1(object sender, EventArgs e)
        {
            if (cboSubGroup.DataSource != null)
                cboSubGroup.SetIndex(0);

            if (objInventoryTerm != null)
            {
                if (objInventoryTerm.dtbTerm_SubGroup != null)
                {
                    if (objInventoryTerm.dtbTerm_SubGroup.Rows.Count > 0)
                    {
                        string strMainGroupList = cboMainGroup.MainGroupIDList;
                        strMainGroupList = strMainGroupList.Replace("><", ",");
                        strMainGroupList = strMainGroupList.Replace("<", string.Empty);
                        strMainGroupList = strMainGroupList.Replace(">", string.Empty);
                        objInventoryTerm.dtbTerm_SubGroup.DefaultView.RowFilter = "IsSelect = 1 AND MainGroupID IN (" + strMainGroupList + ")";
                        DataTable dtbTerm_SubGroup = objInventoryTerm.dtbTerm_SubGroup.DefaultView.ToTable();
                        if (dtbTerm_SubGroup.Columns.Contains("IsSelect"))
                            dtbTerm_SubGroup.Columns.Remove("IsSelect");
                        // Library.AppCore.LoadControls.ComboBoxObject.LoadDataFromDataTable(dtbTerm_SubGroup, cboSubGroup, "-- Tất cả --");  
                        cboSubGroup.InitControl(true, dtbTerm_SubGroup);
                    }
                }
            }
            bolIsHasSubGroup = false;
        }

        private void cboStore_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboStore.StoreID < 1)
            {
                cboStore.SetValue(-1);
                cboMainGroup.SetValue(-1);
                cboSubGroup.SetValue(-1);
            }
        }

        private void ucUserName_Validating(object sender, EventArgs e)
        {
            if (ucUserName.UserName == "")
            {
                Reset();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            DataTable dt = new ERP.Report.PLC.PLCReportDataSource().GetHeavyDataSource("RPT_PM_STOCKREPORT_KK", new object[] { "@STOREID", cboStore.StoreID});
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(this, dt);
        }
    }
}
