﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERP.MasterData.PLC.MD.WSProduct;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmInventory : Form
    {
        /*
         *  LÊ VĂN ĐÔNG: 12/10/2017: Điều chỉnh hiển thị thông tin kiểm kê sau khi duyệt
         */
        public frmInventory()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        #region variable

        private ERP.Inventory.PLC.Inventory.WSInventory.Inventory objInventory = null;
        private DataRow rOld = null;
        private int intKeyIMEI = -1;
        private int intInventoryTypeID = 0;
        private DataTable dtbData = null;
        private DataTable dtbInStockProduct = null;
        private DataTable dtbInStockProductIMEI = null;
        private DataTable dtbInStockProductIMEI_Unique = null;
        private DataTable dtbInStockProductIMEI_Unique_Exclude = null;
        private DataTable dtbTmpStockIMEI = null;
        //private DataTable dtbStoreInStock = null;
        private ERP.Inventory.PLC.ProductChange.PLCProductChange objPLCProductChange = new PLC.ProductChange.PLCProductChange();
        private ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        private ERP.MasterData.PLC.MD.WSInventoryType.InventoryType objInventoryType = new MasterData.PLC.MD.WSInventoryType.InventoryType();
        private ERP.MasterData.PLC.MD.PLCInventoryType objPLCInventoryType = new MasterData.PLC.MD.PLCInventoryType();
        private ERP.Inventory.PLC.Inventory.PLCInventoryTerm objPLCInventoryTerm = new PLC.Inventory.PLCInventoryTerm();
        private ERP.Inventory.PLC.Inventory.PLCInventory objPLCInventory = new PLC.Inventory.PLCInventory();
        private PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();
        public string strInventoryID = string.Empty;
        public DataTable dtbINVTerm_Brand = null;
        public ERP.Inventory.PLC.Inventory.WSInventoryTerm.InventoryTerm objInventoryTerm = null;
        public int intMainGroupID = 0;
        public int intStoreID = 0;
        public int intProductStatusID = 0;
        public string strInventoryUser = string.Empty;
        public int intSubGroupID = 0;

        private bool bolIsCheckDuplicateIMEIAllProduct = false;
        private bool bolIsCheckDuplicateIMEIAllStore = false;

        private DataTable dtbReviewLevel = null;
        //private int intReviewLevelCurrent = -1;
        //private int intSelectedIndex_ReviewLevel = -1;
        //private string strReviewFunctionID = string.Empty;
        //private DataTable dtbUserReviewFunction = null;

        public int intIsEdit = -1;
        public int intIsDelete = -1;

        public int intIsAddProduct_ReviewLevel = -1;
        public int intIsEditProduct_ReviewLevel = -1;
        public int intIsRemoveProduct_ReviewLevel = -1;

        private List<ERP.Inventory.PLC.Inventory.WSInventory.InventoryDetailIMEI> lstProductIMEI_Delete = new List<PLC.Inventory.WSInventory.InventoryDetailIMEI>();

        private bool bolUpdate_IsAddReviewList = false;

        private DataTable dtbSubGroup = null;
        private string strSubGroupName = string.Empty;
        private string strIMEICheck = string.Empty;

        #endregion

        #region method

        private void LoadComboBox()
        {
            Library.AppCore.LoadControls.SetDataSource.SetStore(this, cboStore, false);
            Library.AppCore.LoadControls.SetDataSource.SetMainGroup(this, cboMainGroup, false);
            if (intMainGroupID > 0)
            {
                ComboBox cbTMP = new ComboBox();
                Library.AppCore.LoadControls.SetDataSource.SetSubGroup(this, cbTMP, intMainGroupID);
                dtbSubGroup = (DataTable)cbTMP.DataSource;
                if (dtbSubGroup.Rows.Count > 0 && Convert.ToInt32(dtbSubGroup.Rows[0]["SubGroupID"]) < 1)
                    dtbSubGroup.Rows.RemoveAt(0);
            }
            DataTable dtbProductStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
            DataRow drALL = dtbProductStatus.NewRow();
            drALL["PRODUCTSTATUSID"] = -1;
            drALL["PRODUCTSTATUSNAME"] = "--- Tất cả ---";
            dtbProductStatus.Rows.InsertAt(drALL, 0);
            cboProductStateID.DataSource = dtbProductStatus;
            cboProductStateID.DisplayMember = "PRODUCTSTATUSNAME";
            cboProductStateID.ValueMember = "PRODUCTSTATUSID";
        }

        private void LoadInventoryTerm(int intInventoryTermID)
        {
            if (objInventoryTerm == null || objInventoryTerm.InventoryTermID < 1)
            {
                objPLCInventoryTerm.LoadInfo(ref objInventoryTerm, intInventoryTermID);
            }
            cboStore.SelectedValue = intStoreID;
            cboMainGroup.SelectedValue = intMainGroupID;
            dtpInventoryDate.Value = (DateTime)objInventoryTerm.InventoryDate;
            dtpBeginInventoryTime.Value = (DateTime)objInventoryTerm.BeginInventoryTime;
            cboProductStateID.SelectedValue  = intProductStatusID;
            //radIsNew.Checked = Convert.ToBoolean(intIsNew);
            //radIsOld.Checked = !radIsNew.Checked;
            if (intSubGroupID > 0)
            {
                DataRow[] drSubGroup = dtbSubGroup.Select("SubGroupID = " + intSubGroupID);
                if (drSubGroup.Length > 0)
                    strSubGroupName = drSubGroup[0]["SubGroupName"].ToString().Trim();
            }
            txtSubGroupName.Text = (strSubGroupName != string.Empty ? strSubGroupName : "Tất cả");

            intInventoryTypeID = objInventoryTerm.InventoryTypeID;
            objPLCInventoryType.LoadInfo(ref objInventoryType, intInventoryTypeID);

            DataTable tblINVTerm = new DataTable();
            tblINVTerm.Columns.Add("InventoryTermID", typeof(int));
            tblINVTerm.Columns.Add("InventoryTermName", typeof(string));
            DataRow rINVTerm = tblINVTerm.NewRow();
            rINVTerm["InventoryTermID"] = objInventoryTerm.InventoryTermID;
            rINVTerm["InventoryTermName"] = objInventoryTerm.InventoryTermName;
            tblINVTerm.Rows.Add(rINVTerm);
            Library.AppCore.LoadControls.ComboBoxObject.LoadDataFromDataTable(tblINVTerm, cboInventoryTerm);
            cboInventoryTerm.SelectedValue = objInventoryTerm.InventoryTermID;

            if (dtbINVTerm_Brand == null || dtbINVTerm_Brand.Rows.Count < 1)
            {
                dtbINVTerm_Brand = objInventoryTerm.dtbTerm_Brand.Copy();
            }
        }

        private void LoadInventoryUser()
        {
            ERP.MasterData.SYS.PLC.WSUser.User objUser = new MasterData.SYS.PLC.WSUser.User();
            ERP.MasterData.SYS.PLC.PLCUser objPLCUser = new MasterData.SYS.PLC.PLCUser();
            objUser = objPLCUser.LoadInfo(strInventoryUser);
            string strUserFullName = objUser.UserName;
            if (Convert.ToString(objUser.FullName).Trim() != string.Empty)
            {
                strUserFullName = objUser.UserName + "-" + Convert.ToString(objUser.FullName).Trim();
            }
            txtInventoryUser.Text = strUserFullName;
        }

        private void InitDataFlex()
        {
            dtbData = new DataTable();
            dtbData.Columns.Add("ProductID", typeof(string));
            dtbData.Columns.Add("ProductName", typeof(string));
            dtbData.Columns.Add("IMEI", typeof(string));
            dtbData.Columns.Add("Quantity", typeof(decimal));
            dtbData.Columns["Quantity"].DefaultValue = 0;
            dtbData.Columns.Add("CabinetNumber", typeof(int));
            dtbData.Columns.Add("IsError", typeof(int));
            dtbData.Columns["IsError"].DefaultValue = 0;
            dtbData.Columns.Add("ErrorContent", typeof(string));
            dtbData.Columns.Add("IsRequestIMEI", typeof(int));
            dtbData.Columns.Add("IsInStock", typeof(int));
            dtbData.Columns.Add("InStockQuantity", typeof(decimal));
            dtbData.Columns["InStockQuantity"].DefaultValue = 0;
            dtbData.Columns.Add("IsInsert", typeof(int));
            dtbData.Columns["IsInsert"].DefaultValue = -1;
            dtbData.Columns.Add("IsEdit", typeof(int));
            dtbData.Columns["IsEdit"].DefaultValue = -1;
            dtbData.Columns.Add("INVENTORYDETAILID", typeof(string));
            dtbData.Columns.Add("SubGroupID", typeof(int));
            dtbData.Columns.Add("ProductInstockID", typeof(int));
        }

        private void InitGrid()
        {
            string[] fieldNames = new[] { "REVIEWSEQUENCE", "REVIEWLEVELNAME", "ISREVIEWED", "REVIEWEDDATE", "FULLNAME", "STATUSNAME", "ISSELECT" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdUserReviewLevel, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewUserReviewLevel);

        }

        private void FormatGridUserReviewLevel(bool bolIsCanEdit)
        {
            //REVIEWSEQUENCE","REVIEWLEVELNAME","ISREVIEWED","REVIEWEDDATE","USERNAME","FULLNAME","STATUSNAME","ISSELECT
            grdViewUserReviewLevel.Columns["REVIEWSEQUENCE"].Caption = "Thứ tự duyệt";
            grdViewUserReviewLevel.Columns["REVIEWLEVELNAME"].Caption = "Mức duyệt";
            grdViewUserReviewLevel.Columns["ISREVIEWED"].Caption = "Đã duyệt";
            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].Caption = "Ngày duyệt";
            //grdViewUserReviewLevel.Columns["USERNAME"].Caption = "Mã Người duyệt";
            grdViewUserReviewLevel.Columns["FULLNAME"].Caption = "Người duyệt";
            grdViewUserReviewLevel.Columns["STATUSNAME"].Caption = "Trạng thái duyệt";
            grdViewUserReviewLevel.Columns["ISSELECT"].Caption = "Chọn";

            grdViewUserReviewLevel.Columns["REVIEWSEQUENCE"].Width = 60;
            grdViewUserReviewLevel.Columns["REVIEWSEQUENCE"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.Columns["REVIEWSEQUENCE"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewUserReviewLevel.Columns["REVIEWSEQUENCE"].OptionsColumn.AllowEdit = false;

            grdViewUserReviewLevel.Columns["REVIEWLEVELNAME"].Width = 200;
            grdViewUserReviewLevel.Columns["REVIEWLEVELNAME"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.Columns["REVIEWLEVELNAME"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewUserReviewLevel.Columns["REVIEWLEVELNAME"].OptionsColumn.AllowEdit = false;

            grdViewUserReviewLevel.Columns["ISREVIEWED"].Width = 70;
            grdViewUserReviewLevel.Columns["ISREVIEWED"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.Columns["ISREVIEWED"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewUserReviewLevel.Columns["ISREVIEWED"].OptionsColumn.AllowEdit = false;

            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].Width = 110;
            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].OptionsColumn.AllowEdit = false;

            grdViewUserReviewLevel.Columns["FULLNAME"].Width = 220;
            grdViewUserReviewLevel.Columns["FULLNAME"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.Columns["FULLNAME"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewUserReviewLevel.Columns["FULLNAME"].OptionsColumn.AllowEdit = false;

            grdViewUserReviewLevel.Columns["STATUSNAME"].Width = 80;
            grdViewUserReviewLevel.Columns["STATUSNAME"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.Columns["STATUSNAME"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewUserReviewLevel.Columns["STATUSNAME"].OptionsColumn.AllowEdit = false;

            grdViewUserReviewLevel.Columns["ISSELECT"].Width = 40;
            grdViewUserReviewLevel.Columns["ISSELECT"].AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.Columns["ISSELECT"].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdViewUserReviewLevel.Columns["ISSELECT"].OptionsColumn.AllowEdit = true;
            grdViewUserReviewLevel.Columns["ISSELECT"].Visible = bolIsCanEdit;


            grdViewUserReviewLevel.OptionsView.AllowCellMerge = true;
            grdViewUserReviewLevel.Columns["FULLNAME"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            grdViewUserReviewLevel.Columns["ISREVIEWED"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            grdViewUserReviewLevel.Columns["STATUSNAME"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            grdViewUserReviewLevel.Columns["ISSELECT"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;

            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grdViewUserReviewLevel.Columns["REVIEWEDDATE"].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";


            DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            chkCheckBoxIsSelect.ValueChecked = ((Decimal)(1));
            chkCheckBoxIsSelect.ValueUnchecked = ((Decimal)(0));
            grdViewUserReviewLevel.Columns["ISSELECT"].ColumnEdit = chkCheckBoxIsSelect;


            DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsReviewed = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            chkCheckBoxIsReviewed.ValueChecked = ((Decimal)(1));
            chkCheckBoxIsReviewed.ValueUnchecked = ((Decimal)(0));
            grdViewUserReviewLevel.Columns["ISREVIEWED"].ColumnEdit = chkCheckBoxIsReviewed;

            DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboLookUpEditStorePermissionType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();


            grdViewUserReviewLevel.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            grdViewUserReviewLevel.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            grdViewUserReviewLevel.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            grdViewUserReviewLevel.OptionsView.ShowAutoFilterRow = true;
            grdViewUserReviewLevel.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            grdViewUserReviewLevel.OptionsView.ShowFooter = false;
            grdViewUserReviewLevel.OptionsView.ShowGroupPanel = false;
            grdViewUserReviewLevel.ColumnPanelRowHeight = 50;
            //grdViewReviewLevel.OptionsFilter.Reset();
            for (int i = 0; i < this.grdViewUserReviewLevel.Columns.Count; i++)
            {
                this.grdViewUserReviewLevel.Columns[i].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            }
        }

        private void FormatFlex()
        {
            if (flexDetail.DataSource == null)
            {
                return;
            }
            for (int i = 0; i < flexDetail.Cols.Count; i++)
            {
                flexDetail.Cols[i].Visible = false;
                flexDetail.Cols[i].AllowEditing = false;
                flexDetail.Cols[i].AllowDragging = false;
            }
            flexDetail.Cols[0].Visible = true;
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexDetail, true, "ProductID,ProductName,IMEI,Quantity,CabinetNumber");
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnWidth(flexDetail,
                "ProductID", 110,
                "ProductName", 220,
                "IMEI", 160,
                "Quantity", 80,
                "CabinetNumber", 80);
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnCaption(flexDetail,
                "ProductID", "Mã sản phẩm",
                "ProductName", "Tên sản phẩm",
                "IMEI", "IMEI",
                "Quantity", "Số lượng",
                "CabinetNumber", "Tủ");
            flexDetail.Cols[0].Width = 10;

            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowEditing(flexDetail, true, "IMEI", "Quantity");

            C1.Win.C1FlexGrid.CellStyle style = flexDetail.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexDetail.GetCellRange(0, 0, 0, flexDetail.Cols.Count - 1);
            range.Style = style;
            flexDetail.Rows[0].Height = 40;
            flexDetail.Rows[0].AllowDragging = false;

            Library.AppCore.LoadControls.C1FlexGridObject.AddFormSearchProduct(flexDetail, flexDetail.Cols["ProductID"].Index, flexDetail.Cols["ProductName"].Index, flexDetail.Cols["IMEI"].Index);
        }

        private void LoadInventoryInfo()
        {
            if (strInventoryID.Trim() == string.Empty)
            {
                return;
            }
            if (objInventory == null)
            {
                objInventory = new PLC.Inventory.WSInventory.Inventory();
            }
            dtbData.Rows.Clear();
            DataTable dtbInventoryDetail = null;
            objPLCInventory.LoadInventoryInfo(ref objInventory, strInventoryID.Trim(), ref  dtbInventoryDetail);
            strInventoryUser = objInventory.InventoryUser;
            intStoreID = objInventory.InventoryStoreID;
            intMainGroupID = objInventory.MainGroupID;
            intProductStatusID = objInventory.ProductStatusID;
            //  intIsNew = (objInventory.IsNew == true ? 1 : 0);
            if (intMainGroupID > 0)
            {
                ComboBox cbTMP = new ComboBox();
                Library.AppCore.LoadControls.SetDataSource.SetSubGroup(this, cbTMP, intMainGroupID);
                dtbSubGroup = (DataTable)cbTMP.DataSource;
                if (dtbSubGroup.Rows.Count > 0 && Convert.ToInt32(dtbSubGroup.Rows[0]["SubGroupID"]) < 1)
                    dtbSubGroup.Rows.RemoveAt(0);
            }
            intSubGroupID = objInventory.SubGroupID;
            if (intSubGroupID > 0)
            {
                DataRow[] drSubGroup = dtbSubGroup.Select("SubGroupID = " + intSubGroupID);
                if (drSubGroup.Length > 0)
                    strSubGroupName = drSubGroup[0]["SubGroupName"].ToString().Trim();
            }

            cboStore.SelectedValue = intStoreID;
            cboMainGroup.SelectedValue = intMainGroupID;
            cboProductStateID.SelectedValue = intProductStatusID;
            //radIsOld.Checked = !radIsNew.Checked;
            txtSubGroupName.Text = (strSubGroupName != string.Empty ? strSubGroupName : "Tất cả");

            txtInventoryID.Text = Convert.ToString(objInventory.InventoryID).Trim();
            txtContent.Text = Convert.ToString(objInventory.Content).Trim();
            txtContentDelete.Text = Convert.ToString(objInventory.DeletedNote).Trim();
            chkIsReviewed.Checked = objInventory.IsReviewed;
            chkIsDeleted.Checked = objInventory.IsDeleted;
            if (objInventory.IsReviewed)
            {
                txtContent.ReadOnly = true;
            }
            if (objInventory.IsDeleted)
            {
                txtContent.ReadOnly = true;
                txtContentDelete.ReadOnly = true;
            }

            if (dtbInStockProduct == null || dtbInStockProduct.Rows.Count < 1)
            {
                dtbInStockProduct = objPLCInventory.GetListTermStock(objInventory.InventoryTermID, intStoreID, intMainGroupID, intSubGroupID);
                #region
                dtbInStockProduct.Columns.Add("CabinetNumber", typeof(int));
                dtbInStockProduct.Columns["CabinetNumber"].SetOrdinal(0);
                dtbInStockProduct.Columns.Add("IMEI", typeof(string));
                dtbInStockProduct.Columns["IMEI"].SetOrdinal(3);
                dtbInStockProduct.Columns.Add("Col_IMEI", typeof(string));
                dtbInStockProduct.Columns["Col_IMEI"].SetOrdinal(4);
                dtbInStockProduct.Columns.Add("IMEI_Inventory", typeof(string));
                dtbInStockProduct.Columns.Add("Quantity_Inventory", typeof(decimal));
                dtbInStockProduct.Columns.Add("Different", typeof(decimal));
               // dtbInStockProduct.Columns.Add("ProductInstockID", typeof(int));
                #endregion
                dtbInStockProduct.PrimaryKey = new DataColumn[] { dtbInStockProduct.Columns["ProductID"] };
            }

            if (dtbInStockProductIMEI == null || dtbInStockProductIMEI.Rows.Count < 1)
            {
                dtbInStockProductIMEI = objPLCInventory.GetListTermStockIMEI(objInventory.InventoryTermID, intStoreID, intMainGroupID, intSubGroupID);
                #region
                dtbInStockProductIMEI.Columns.Add("CabinetNumber", typeof(int));
                dtbInStockProductIMEI.Columns["CabinetNumber"].SetOrdinal(0);
                dtbInStockProductIMEI.Columns.Add("Col_IMEI", typeof(string));
                dtbInStockProductIMEI.Columns["Col_IMEI"].SetOrdinal(4);
                dtbInStockProductIMEI.Columns.Add("IMEI_Inventory", typeof(string));
                dtbInStockProductIMEI.Columns.Add("Quantity_Inventory", typeof(decimal));
                dtbInStockProductIMEI.Columns.Add("Different", typeof(decimal));
                dtbInStockProductIMEI.Columns.Add("IsRequestIMEI", typeof(bool));
                dtbInStockProductIMEI.Columns["IsRequestIMEI"].DefaultValue = false;
                //dtbInStockProductIMEI.Columns.Add("ProductInstockID", typeof(int));
                #endregion
                for (int i = dtbInStockProductIMEI.Rows.Count - 1; i > -1; i--)
                {
                    if (dtbInStockProductIMEI.Rows[i]["ProductInstockID"] != null && (!Convert.IsDBNull(dtbInStockProductIMEI.Rows[i]["ProductInstockID"])) && Convert.ToInt32(dtbInStockProductIMEI.Rows[i]["ProductInstockID"]) != Convert.ToInt32(cboProductStateID.SelectedValue))
                    {
                        dtbInStockProductIMEI.Rows.RemoveAt(i);
                    }
                }
                dtbInStockProductIMEI.PrimaryKey = new DataColumn[] { dtbInStockProductIMEI.Columns["ProductID"], dtbInStockProductIMEI.Columns["IMEI"] };
            }

            if (!dtbInventoryDetail.Columns.Contains("IsInsert"))
            {
                dtbInventoryDetail.Columns.Add("IsInsert", typeof(int));
                dtbInventoryDetail.Columns["IsInsert"].DefaultValue = -1;
            }
            if (!dtbInventoryDetail.Columns.Contains("IsEdit"))
            {
                dtbInventoryDetail.Columns.Add("IsEdit", typeof(int));
                dtbInventoryDetail.Columns["IsEdit"].DefaultValue = -1;
            }
            if (!dtbData.Columns.Contains("IsInsert"))
            {
                dtbData.Columns.Add("IsInsert", typeof(int));
                dtbData.Columns["IsInsert"].DefaultValue = -1;
            }
            if (!dtbData.Columns.Contains("IsEdit"))
            {
                dtbData.Columns.Add("IsEdit", typeof(int));
                dtbData.Columns["IsEdit"].DefaultValue = -1;
            }

            if (dtbInventoryDetail.Rows.Count > 0)
            {
                for (int i = 0; i < dtbInventoryDetail.Rows.Count; i++)
                {
                    DataRow rDetail = dtbInventoryDetail.Rows[i];
                    rDetail["IsInsert"] = 0;
                    rDetail["IsEdit"] = 0;
                    DataRow rData = dtbData.NewRow();
                    rData["IsInsert"] = 0;
                    rData["IsEdit"] = 0;
                    rData["ProductID"] = rDetail["ProductID"];
                    rData["ProductName"] = rDetail["ProductName"];
                    rData["IMEI"] = rDetail["IMEI"];
                    rData["Quantity"] = rDetail["Quantity"];
                    rData["CabinetNumber"] = rDetail["CabinetNumber"];
                    rData["IsRequestIMEI"] = rDetail["IsRequestIMEI"];
                    rData["IsInStock"] = rDetail["IsInStock"];
                    rData["InStockQuantity"] = rDetail["InStockQuantity"];
                    rData["INVENTORYDETAILID"] = rDetail["INVENTORYDETAILID"];
                    rData["ProductInstockID"] = rDetail["PRODUCTSTATUSID"];
                    rData["SubGroupID"] = rDetail["SubGroupID"];

                    #region
                    //xem chenh lech: xet IsRequestIMEI = 0 truoc, tiep theo IsRequestIMEI = 1 ( xet IsInStock = 1 truoc, tiep theo IsInStock = 0 )
                    string strProductID = Convert.ToString(rData["ProductID"]).Trim();
                    string strProductName = Convert.ToString(rData["ProductName"]).Trim();
                    int intCabinetNumber = Convert.ToInt32(rData["CabinetNumber"]);
                    decimal decQuantity = Convert.ToDecimal(rData["Quantity"]);
                    decimal decQuantityInStock = Convert.ToDecimal(rData["InStockQuantity"]);
                    if (!Convert.ToBoolean(rData["IsRequestIMEI"]))
                    {
                        DataRow rInStockProduct = dtbInStockProduct.Rows.Find(strProductID);
                        if (rInStockProduct != null)
                        {
                            rInStockProduct["CabinetNumber"] = intCabinetNumber;
                            rInStockProduct["Quantity_Inventory"] = decQuantity;
                            rInStockProduct["Different"] = decQuantityInStock - decQuantity;
                        }
                    }
                    else
                    {
                        string strIMEI = Convert.ToString(rData["IMEI"]).Trim();
                        if (Convert.ToBoolean(rData["IsInStock"]))
                        {
                            DataRow rInStockIMEI = dtbInStockProductIMEI.Rows.Find(new object[] { strProductID, strIMEI });
                            if (rInStockIMEI != null)
                            {
                                rInStockIMEI["CabinetNumber"] = intCabinetNumber;
                                rInStockIMEI["Col_IMEI"] = strIMEI;
                                rInStockIMEI["IMEI_Inventory"] = strIMEI;
                                rInStockIMEI["Quantity_Inventory"] = 1;
                                rInStockIMEI["Different"] = decQuantityInStock - 1;
                            }
                        }
                        else
                        {
                            DataRow[] rExistIMEINotInStock = dtbInStockProductIMEI.Select("TRIM(ProductID) = '" + strProductID + "' AND (Col_IMEI IS NULL OR TRIM(Col_IMEI)='') AND TRIM(IMEI_Inventory) = '" + strIMEI + "'");
                            if (rExistIMEINotInStock.Length < 1)
                            {
                                DataRow rNew = dtbInStockProductIMEI.NewRow();
                                rNew["CabinetNumber"] = intCabinetNumber;
                                rNew["ProductID"] = strProductID;
                                rNew["ProductName"] = strProductName;
                                rNew["IMEI"] = (--intKeyIMEI);
                                rNew["Col_IMEI"] = string.Empty;
                                rNew["IMEI_Inventory"] = strIMEI;
                                rNew["Quantity_Inventory"] = 1;
                                decimal decQuantity_InStock = 0;
                                rNew["Different"] = decQuantity_InStock - 1;
                                rNew["SubGroupID"] = intSubGroupID;
                                dtbInStockProductIMEI.Rows.Add(rNew);
                            }
                        }
                    }

                    #endregion

                    dtbData.Rows.Add(rData);
                }
            }

            mnuItemImportExcel.Enabled = false;

            txtUserName.Enabled = true;
            txtPassword.Enabled = true;
            cboReviewStatus.Enabled = true;
            btnReviewStatus.Enabled = true;

            SumQuantity();
        }

        private bool AddProduct(string strBarcode, decimal decQuantity, ref string strProductID, ref int intIsRequestIMEI, ref string strProductName, ref string strIMEI, ref int intIsError, ref string strErrorContent, ref int intProductSubGroupID)
        {
            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = null;
            int intProductInstockID = -1;
            decimal decQuantity_IMEI = 0;
            decimal decQuantity_Product = 0;

            DataRow rInStockIMEI = null;
            DataRow rInStockProduct = null;
            int intCabinet = Convert.ToInt32(cboCabinet.Text);

            bool bolIsImportExcel = false;

            if (strBarcode != string.Empty)
            {
                rInStockIMEI = dtbInStockProductIMEI_Unique.Rows.Find(strBarcode);
                if (rInStockIMEI != null)
                {
                    strIMEI = strBarcode;
                    strProductID = Convert.ToString(rInStockIMEI["ProductID"]).Trim();
                    intProductInstockID = Convert.ToInt32(rInStockIMEI["ProductInstockID"]);
                    decQuantity_IMEI = Convert.ToDecimal(rInStockIMEI["Quantity"]);
                    intProductSubGroupID = Convert.ToInt32(rInStockIMEI["SubGroupID"]);

                    if (strProductID != string.Empty)
                    {
                        rInStockProduct = dtbInStockProduct.Rows.Find(strProductID);
                        if (rInStockProduct != null)
                        {
                            rInStockProduct["CabinetNumber"] = intCabinet;
                        }
                    }

                    if (strProductID != string.Empty && strIMEI != string.Empty)
                    {
                        rInStockIMEI = dtbInStockProductIMEI.Rows.Find(new object[] { strProductID, strIMEI });
                        if (rInStockIMEI != null)
                        {
                            rInStockIMEI["CabinetNumber"] = intCabinet;
                            rInStockIMEI["Col_IMEI"] = rInStockIMEI["IMEI"];
                        }
                    }
                }
                else
                {
                    rInStockProduct = dtbInStockProduct.Rows.Find(strBarcode);
                    if (rInStockProduct != null)
                    {
                        strProductID = strBarcode;
                        rInStockProduct["CabinetNumber"] = intCabinet;
                        decQuantity_Product = Convert.ToDecimal(rInStockProduct["Quantity"]);
                    }
                }
            }
            else
            {
                if (strProductID != string.Empty || strIMEI != string.Empty)
                {
                    bolIsImportExcel = true;
                    if (strProductID != string.Empty && strIMEI != string.Empty)
                    {
                        rInStockIMEI = dtbInStockProductIMEI.Rows.Find(new object[] { strProductID, strIMEI });
                        if (rInStockIMEI != null)
                        {
                            intProductInstockID = Convert.ToInt32(rInStockIMEI["ProductInstockID"]);
                            decQuantity_IMEI = Convert.ToDecimal(rInStockIMEI["Quantity"]);
                            rInStockIMEI["CabinetNumber"] = intCabinet;
                            rInStockIMEI["Col_IMEI"] = rInStockIMEI["IMEI"];
                        }
                        else
                        {
                            rInStockProduct = dtbInStockProduct.Rows.Find(strProductID);
                            if (rInStockProduct != null)
                            {
                                rInStockProduct["CabinetNumber"] = intCabinet;
                                decQuantity_Product = Convert.ToDecimal(rInStockProduct["Quantity"]);
                            }
                        }
                    }
                    else if (strProductID != string.Empty)
                    {
                        rInStockProduct = dtbInStockProduct.Rows.Find(strProductID);
                        if (rInStockProduct != null)
                        {
                            rInStockProduct["CabinetNumber"] = intCabinet;
                            decQuantity_Product = Convert.ToDecimal(rInStockProduct["Quantity"]);
                        }
                    }
                }
            }

            if (strProductID != string.Empty)
            {
                objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
            }
            else
            {
                objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strBarcode);
            }
            if (objProduct == null || Convert.ToString(objProduct.ProductName).Trim() == string.Empty)
            {
                intIsError = 1;
                DataRow rExclude = dtbInStockProductIMEI_Unique_Exclude.Rows.Find(strBarcode);
                if (rExclude != null && Convert.ToInt32(rExclude["ProductInstockID"]) != Convert.ToInt32(cboProductStateID.SelectedValue))
                    strErrorContent = "IMEI [" + strBarcode + "] đang tồn ở trạng thái khác " + cboProductStateID.SelectedText + ". Vui lòng kiểm tra lại";
                else
                    strErrorContent = "Sai mã sản phẩm hoặc IMEI";
            }
            else
            {
                strProductName = Convert.ToString(objProduct.ProductName).Trim();
                if (rInStockIMEI != null)
                {
                    rInStockIMEI["Col_IMEI"] = rInStockIMEI["IMEI"];
                }
                if (rInStockProduct != null)
                {
                    rInStockProduct["CabinetNumber"] = intCabinet;
                }
                intIsRequestIMEI = (objProduct.IsRequestIMEI == true ? 1 : 0);
                intProductSubGroupID = objProduct.SubGroupID;
                if (intSubGroupID > 0 && objProduct.SubGroupID != intSubGroupID)
                {
                    intIsError = 1;
                    strErrorContent = "Sản phẩm " + strProductName + " không thuộc nhóm hàng " + txtSubGroupName.Text;
                }
                else
                {
                    if (objProduct.MainGroupID != intMainGroupID)
                    {
                        intIsError = 1;
                        strErrorContent = "Sản phẩm " + strProductName + " không thuộc ngành hàng " + Convert.ToString(cboMainGroup.Text).Trim();
                    }
                    else
                    {
                        DataRow[] rTerm_Brand = dtbINVTerm_Brand.Select("BrandID = " + objProduct.BrandID);
                        if (rTerm_Brand.Length < 1)
                        {
                            intIsError = 1;
                            strErrorContent = "Nhà sản xuất " + Convert.ToString(objProduct.BrandName).Trim() + " của mã sản phẩm " + objProduct.ProductID + " không được khai báo kiểm kê trong kỳ này";
                        }
                        else
                        {
                            if (strIMEI != string.Empty)
                            {
                                DataRow[] rExitIMEI = dtbData.Select("TRIM(IMEI) = '" + strIMEI + "'");
                                if (rExitIMEI.Length > 0)
                                {
                                    intIsError = 1;
                                    strErrorContent = "Số IMEI này đã tồn tại trong phiếu kiểm kê hàng";
                                }
                                else
                                {
                                    if (intProductInstockID > -1 && intProductInstockID != Convert.ToInt32(cboProductStateID.SelectedValue))
                                    {
                                        intIsError = 1;
                                        strErrorContent = "Không được kiểm kê hàng " + cboProductStateID.SelectedText + " trong phiếu kiểm kê hiện tại!";// " + (radIsNew.Checked ? "mới" : "cũ");
                                    }
                                    else
                                    {
                                        if (rInStockIMEI == null)
                                        {
                                            intIsError = 2;
                                            strErrorContent = "IMEI " + strIMEI + " không tồn tại trong kho";
                                        }
                                        else
                                        {
                                            if (decQuantity_IMEI != 1)
                                            {
                                                intIsError = 2;
                                                strErrorContent = "Số lượng của IMEI " + strIMEI + " không hợp lệ";
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (rInStockProduct == null)
                                {
                                    DataRow rNewProduct = dtbInStockProduct.NewRow();
                                    rNewProduct["ProductID"] = objProduct.ProductID;
                                    rNewProduct["ProductName"] = objProduct.ProductName;
                                    rNewProduct["Quantity"] = 0;
                                    rNewProduct["IsRequestIMEI"] = intIsRequestIMEI;
                                    rNewProduct["SubGroupID"] = objProduct.SubGroupID;
                                    dtbInStockProduct.Rows.Add(rNewProduct);
                                    rInStockProduct = dtbInStockProduct.Rows.Find(objProduct.ProductID);
                                    if (rInStockProduct != null)
                                    {
                                        strProductID = objProduct.ProductID;
                                        rInStockProduct["CabinetNumber"] = intCabinet;
                                        decQuantity_Product = Convert.ToDecimal(rInStockProduct["Quantity"]);
                                    }
                                }

                                if (strProductID == string.Empty)
                                {
                                    intIsError = 1;
                                    strErrorContent = "Sản phẩm " + Convert.ToString(objProduct.ProductID).Trim() + " không tồn kho";
                                    strProductID = Convert.ToString(objProduct.ProductID).Trim();
                                }
                                else
                                {
                                    if (objProduct.IsRequestIMEI)
                                    {
                                        if (bolIsImportExcel)
                                        {
                                            intIsError = 1;
                                        }
                                        else
                                        {
                                            intIsError = 3;
                                        }
                                        strErrorContent = "Sản phẩm yêu cầu nhập IMEI";
                                    }
                                    else
                                    {
                                        if (decQuantity > 0 && decQuantity_Product > 0 && decQuantity != decQuantity_Product)
                                        {
                                            intIsError = 2;
                                            strErrorContent = "Số lượng của mã sản phẩm " + strProductID + " không hợp lệ";
                                        }
                                        //else
                                        //{
                                        //    if (objProduct.IsRequestIMEI)
                                        //    {
                                        //        intIsError = 3;
                                        //        strErrorContent = "Sản phẩm yêu cầu nhập IMEI";
                                        //    }
                                        //}
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (intIsError == 1 && strErrorContent != string.Empty)
            {
                return false;
            }

            return true;
        }

        private void ImportExcel()
        {
            DataTable dtbExcel = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(5);
            if (dtbExcel == null || dtbExcel.Rows.Count < 2)
            {
                return;
            }
            dtbExcel.Rows.RemoveAt(0);

            DataTable dtbExcelResult = dtbData.Clone();

            for (int i = 0; i < dtbExcel.Rows.Count; i++)
            {
                DataRow rExcel = dtbExcel.Rows[i];
                string strIMEI = string.Empty;
                if (!Convert.IsDBNull(rExcel[2])) strIMEI = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(Convert.ToString(rExcel[2]).Trim());
                string strProductID = string.Empty;
                if (!Convert.IsDBNull(rExcel[0])) strProductID = Convert.ToString(rExcel[0]).ToUpper().Trim();
                string strProductName = string.Empty;
                if (!Convert.IsDBNull(rExcel[1])) strProductName = Convert.ToString(rExcel[1]).Trim();
                if (strProductID == string.Empty && strIMEI == string.Empty)
                    continue;
                decimal decQuantity = 0;
                if (!Convert.IsDBNull(rExcel[3]))
                {
                    try
                    {
                        decQuantity = Convert.ToDecimal(rExcel[3]);
                    }
                    catch
                    {
                        MessageBox.Show(this, "Lỗi nhập dữ liệu kiểm kê từ Excel - cột Số lượng của mã sản phẩm " + strProductID, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        return;
                    }
                }
                int intCabinetNumber = 1;
                if (!Convert.IsDBNull(rExcel[4]))
                {
                    try
                    {
                        intCabinetNumber = Convert.ToInt32(rExcel[4]);
                    }
                    catch
                    {
                        MessageBox.Show(this, "Lỗi nhập dữ liệu kiểm kê từ Excel - cột Tủ của mã sản phẩm " + strProductID, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                int intIsRequestIMEI = 0;
                int intIsError = 0;
                string strErrorContent = string.Empty;
                int intSubGroupID = -1;
                AddProduct(string.Empty, decQuantity, ref strProductID, ref intIsRequestIMEI, ref strProductName, ref strIMEI, ref intIsError, ref strErrorContent, ref intSubGroupID);

                DataRow rResult = dtbExcelResult.NewRow();
                rResult["ProductID"] = strProductID;
                rResult["IsRequestIMEI"] = intIsRequestIMEI;
                rResult["ProductName"] = strProductName;
                rResult["IMEI"] = strIMEI;
                rResult["Quantity"] = decQuantity;
                rResult["SubGroupID"] = intSubGroupID;
                rResult["CabinetNumber"] = intCabinetNumber;
                rResult["IsError"] = intIsError;
                rResult["ErrorContent"] = strErrorContent;

                if (intIsError != 1)
                {
                    DataRow rDetail = dtbData.NewRow();
                    rDetail.ItemArray = rResult.ItemArray;
                    rDetail["IsEdit"] = 1;

                    if (decQuantity <= 0)
                    {
                        if (intIsRequestIMEI > 0)
                        {
                            rResult["IsError"] = 1;
                            rResult["ErrorContent"] = "Số lượng không hợp lệ. Vui lòng kiểm tra lại";

                            dtbExcelResult.Rows.Add(rResult);
                            continue;
                        }
                        else
                        {
                            if (decQuantity < 0)
                            {
                                rResult["IsError"] = 1;
                                rResult["ErrorContent"] = "Số lượng không hợp lệ. Vui lòng kiểm tra lại";

                                dtbExcelResult.Rows.Add(rResult);
                                continue;
                            }
                        }
                    }
                    else
                    {
                        ERP.MasterData.PLC.MD.WSProduct.Product objProduct = new MasterData.PLC.MD.WSProduct.Product();
                        objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                        if (!objProduct.IsAllowDecimal)
                        {
                            if (Convert.ToInt32(rResult["Quantity"]) % 1 > 0)
                            {
                                rResult["IsError"] = 1;
                                rResult["ErrorContent"] = "Số lượng không hợp lệ (Sản phẩm không cho phép số lượng là số thực). Vui lòng kiểm tra lại";
                                dtbExcelResult.Rows.Add(rResult);
                                continue;
                            }
                        }
                    }

                    if (strIMEI != string.Empty)
                    {
                        DataRow rInStockIMEI = dtbInStockProductIMEI.Rows.Find(new object[] { strProductID, strIMEI });
                        if (rInStockIMEI != null)
                        {
                            rInStockIMEI["IMEI_Inventory"] = strIMEI;
                            rInStockIMEI["Quantity_Inventory"] = 1;
                            rInStockIMEI["CabinetNumber"] = intCabinetNumber;
                            decimal decQuantityInStock = 0;
                            if (!Convert.IsDBNull(rInStockIMEI["Quantity"]))
                            {
                                decQuantityInStock = Convert.ToDecimal(rInStockIMEI["Quantity"]);
                            }
                            rDetail["IsInStock"] = 1;
                            rDetail["InStockQuantity"] = decQuantityInStock;
                            rInStockIMEI["Different"] = decQuantityInStock - 1;
                        }
                        else
                        {
                            if (intIsError == 2 && strErrorContent.IndexOf("không tồn tại trong kho") > 0)
                            {
                                if (bolIsCheckDuplicateIMEIAllStore && bolIsCheckDuplicateIMEIAllProduct)
                                {
                                    if (objPLCProductInStock.CheckIsExistIMEI(string.Empty, strIMEI, 0, 0, -1, true, true))
                                    //DataRow[] drExistInSystem = dtbStoreInStock.Select("TRIM(IMEI)='" + strIMEI + "'");
                                    //if (drExistInSystem.Length > 0)
                                    {
                                        rResult["IsError"] = 1;
                                        rResult["ErrorContent"] = "IMEI " + strIMEI + " đã tồn tại trong hệ thống. Vui lòng kiểm tra lại tồn kho hiện tại";

                                        dtbExcelResult.Rows.Add(rResult);
                                        continue;
                                    }
                                }

                                if (!Library.AppCore.Other.CheckObject.CheckIMEI(strIMEI))
                                {
                                    rResult["IsError"] = 1;
                                    rResult["ErrorContent"] = "IMEI " + strIMEI + " không đúng định dạng. Dữ liệu nhập vào phải không có khoảng trắng hoặc không có ký tự xuống hàng. Vui lòng kiểm tra lại";

                                    dtbExcelResult.Rows.Add(rResult);
                                    continue;
                                }

                                DataRow rTmpStockIMEI = dtbTmpStockIMEI.Rows.Find(new object[] { strProductID, strIMEI });
                                if (rTmpStockIMEI != null)
                                {
                                    rResult["IsError"] = 1;
                                    rResult["ErrorContent"] = "IMEI " + strIMEI +" không thuộc trạng thái của phiếu kiểm kê hàng " + cboProductStateID.SelectedText;

                                    dtbExcelResult.Rows.Add(rResult);
                                    continue;
                                }
                            }
                            rDetail["IsInStock"] = 0;
                            rDetail["InStockQuantity"] = 0;

                            DataRow[] rExistIMEINotInStock = dtbInStockProductIMEI.Select("TRIM(ProductID) = '" + strProductID + "' AND (Col_IMEI IS NULL OR TRIM(Col_IMEI)='') AND TRIM(IMEI_Inventory) = '" + strIMEI + "'");
                            if (rExistIMEINotInStock.Length < 1)
                            {
                                DataRow rNew = dtbInStockProductIMEI.NewRow();
                                rNew["CabinetNumber"] = intCabinetNumber;
                                rNew["ProductID"] = strProductID;
                                rNew["ProductName"] = strProductName;
                                rNew["IMEI"] = (--intKeyIMEI);
                                rNew["Col_IMEI"] = string.Empty;
                                rNew["IMEI_Inventory"] = strIMEI;
                                rNew["Quantity_Inventory"] = 1;
                                decimal decQuantityInStock = 0;
                                rNew["Different"] = decQuantityInStock - 1;
                                dtbInStockProductIMEI.Rows.Add(rNew);
                            }
                        }

                        rDetail["Quantity"] = 1;
                    }
                    else
                    {
                        if (intIsRequestIMEI == 0)
                        {
                            DataRow[] rExist = dtbData.Select("TRIM(ProductID) = '" + strProductID + "'");
                            if (rExist.Length > 0)
                            {
                                continue;
                            }
                        }

                        DataRow rInStockProduct = dtbInStockProduct.Rows.Find(new object[] { strProductID });
                        if (rInStockProduct != null)
                        {
                            rInStockProduct["CabinetNumber"] = intCabinetNumber;
                            rDetail["IsInStock"] = 0;
                            decimal decQuantityInStock = 0;
                            if (!Convert.IsDBNull(rInStockProduct["Quantity"]))
                            {
                                decQuantityInStock = Convert.ToDecimal(rInStockProduct["Quantity"]);
                                if (decQuantityInStock > 0)
                                {
                                    rDetail["IsInStock"] = 1;
                                }
                            }
                            rDetail["InStockQuantity"] = decQuantityInStock;
                        }
                        else
                        {
                            rDetail["IsInStock"] = 0;
                            rDetail["InStockQuantity"] = 0;
                        }
                    }

                    dtbData.Rows.Add(rDetail);

                    if (intIsError > 0)
                    {
                        if (intIsError == 2)
                        {
                            Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexDetail, flexDetail.Rows.Count - 1, Color.Pink);
                        }
                        else
                        {
                            Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexDetail, flexDetail.Rows.Count - 1, Color.Yellow);
                        }
                    }

                    flexDetail.Select(flexDetail.Rows.Count - 1, flexDetail.Cols["ProductID"].Index);
                }
                else
                {
                }

                dtbExcelResult.Rows.Add(rResult);
            }

            SumQuantity();

            frmImportResult frmImportResult1 = new frmImportResult();
            frmImportResult1.ImportResult = dtbExcelResult.Copy();
            frmImportResult1.ShowDialog();
        }

        private bool CheckUpdate()
        {
            if (dtbData.Rows.Count < 1)
            {
                MessageBox.Show(this, "Vui lòng nhập dữ liệu kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            DataRow[] rIsError = dtbData.Select("IsError > 0 AND IsRequestIMEI = 1");
            if (rIsError.Length > 0)
            {
                for (int i = 0; i < dtbData.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dtbData.Rows[i]["IsError"]) > 0 && Convert.ToInt32(dtbData.Rows[i]["IsRequestIMEI"]) == 1)
                    {
                        if (Convert.IsDBNull(dtbData.Rows[i]["IMEI"]) || Convert.ToString(dtbData.Rows[i]["IMEI"]).Trim() == string.Empty)
                        {
                            flexDetail.Select(i + flexDetail.Rows.Fixed, flexDetail.Cols["ProductID"].Index);
                            MessageBox.Show(this, "Sản phẩm " + Convert.ToString(dtbData.Rows[i]["ProductID"]).Trim() + " yêu cầu nhập IMEI", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return false;
                        }
                    }
                }
            }
            DataRow[] rCheckQuantity = dtbData.Select("Quantity IS NULL AND IsRequestIMEI <> 1");
            if (rCheckQuantity.Length > 0)
            {
                for (int i = 0; i < dtbData.Rows.Count; i++)
                {
                    if (Convert.IsDBNull(dtbData.Rows[i]["Quantity"]) && Convert.ToInt32(dtbData.Rows[i]["IsRequestIMEI"]) != 1)
                    {
                        dtbData.Rows[i]["Quantity"] = 0;
                    }
                }
            }

            if (strInventoryID.Trim() == string.Empty || bolUpdate_IsAddReviewList)
            {
                DataTable dtbUserReviewLevel = grdUserReviewLevel.DataSource as DataTable;
                if (dtbUserReviewLevel != null && dtbUserReviewLevel.Rows.Count > 0)
                {
                    if (dtbUserReviewLevel.Columns.Contains("ISSELECT"))
                    {
                        dtbUserReviewLevel.AcceptChanges();
                        DataRow[] rIsSelect = dtbUserReviewLevel.Select("ISSELECT = 1");
                        if (rIsSelect.Length < 1)
                        {
                            MessageBox.Show(this, "Vui lòng chọn người duyệt", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            tabControl1.SelectedTab = tabPage2;
                            return false;
                        }
                        for (int i = 0; i < dtbReviewLevel.Rows.Count; i++)
                        {
                            DataRow[] rIsSelectRvwLvl = dtbUserReviewLevel.Select("IsSelect = 1 AND ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]));
                            if (rIsSelectRvwLvl.Length < 1)
                            {
                                MessageBox.Show(this, "Vui lòng chọn người duyệt cho mức duyệt " + Convert.ToString(dtbReviewLevel.Rows[i]["ReviewLevelName"]).Trim(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                tabControl1.SelectedTab = tabPage2;
                                return false;
                            }
                            if (Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewType"]) == 0)
                            {
                                DataRow[] rCountReviewLevel = dtbUserReviewLevel.Select("ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]));
                                DataRow[] rCountSelect = dtbUserReviewLevel.Select("IsSelect = 1 AND ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]));
                                //if (rCountSelect.Length < rCountReviewLevel.Length)
                                //{
                                //    MessageBox.Show(this, "Mức duyệt " + Convert.ToString(dtbReviewLevel.Rows[i]["ReviewLevelName"]).Trim() + " có hình thức duyệt là tất cả phải duyệt. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                //    tabControl1.SelectedTab = tabPage2;
                                //    return false;
                                //}
                            }
                        }
                    }
                }
            }

            return true;
        }

        private void CheckReviewLevel()
        {
            DataTable dtbUserReviewLevel = grdUserReviewLevel.DataSource as DataTable;
            DataRow[] rUserReviewLevel = dtbUserReviewLevel.Select("TRIM(UserName) = '" + Library.AppCore.SystemConfig.objSessionUser.UserName + "'");
            if (rUserReviewLevel.Length < 1)
            {
                #region
                txtContent.ReadOnly = true;
                txtBarcode.Enabled = false;
                btnSearch.Enabled = false;
                flexDetail.AllowEditing = false;
                btnSave.Enabled = false;
                cboCabinet.Enabled = false;
                btnDelete.Enabled = (intIsDelete == 1);
                if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
                {
                    chkIsReviewed.Checked = objInventory.IsReviewed;
                    if (objInventory.IsDeleted || objInventory.IsUpdateUnEvent)
                    {
                        btnReviewStatus.Enabled = false;

                        if (objInventory.IsDeleted)
                        {
                            btnDelete.Enabled = false;
                            chkIsDeleted.Checked = objInventory.IsDeleted;
                        }
                        if (objInventory.IsUpdateUnEvent)
                        {
                            mnuItemExplainUnEvent.Enabled = true;
                            if (objInventory.IsExplainUnEvent)
                            {
                                mnuEventHandlingUnEvent.Enabled = true;
                            }
                        }
                    }
                }
                #endregion

                DataRow[] drReview = dtbUserReviewLevel.Select("IsReviewed = 1");
                if (drReview.Length == dtbUserReviewLevel.Rows.Count)
                {
                    txtUserName.Enabled = false;
                    txtPassword.Enabled = false;
                    cboReviewStatus.Enabled = false;
                    btnReviewStatus.Enabled = false;
                }

                return;
            }

            int intReviewLevelID = 0;
            bool bolIsCanAddNewProduct = false;
            bool bolIsCanEditProduct = false;
            bool bolIsCanRemoveProduct = false;
            bool bolIsCanDeleteInventory = false;

            bool bolIsReviewed = false;
            for (int i = 0; i < dtbUserReviewLevel.Rows.Count; i++)
            {
                if (Convert.ToString(dtbUserReviewLevel.Rows[i]["UserName"]).Trim() == Library.AppCore.SystemConfig.objSessionUser.UserName)
                {
                    if (Convert.ToBoolean(dtbUserReviewLevel.Rows[i]["IsReviewed"]))
                    {
                        bolIsReviewed = true;
                        continue;
                    }
                    intReviewLevelID = Convert.ToInt32(dtbUserReviewLevel.Rows[i]["ReviewLevelID"]);
                    break;
                }
            }

            if (intReviewLevelID < 1)
            {
                if (bolIsReviewed)
                {
                    for (int i = dtbUserReviewLevel.Rows.Count - 1; i > -1; i--)
                    {
                        if (Convert.ToString(dtbUserReviewLevel.Rows[i]["UserName"]).Trim() == Library.AppCore.SystemConfig.objSessionUser.UserName)
                        {
                            if (Convert.ToBoolean(dtbUserReviewLevel.Rows[i]["IsReviewed"]))
                            {
                                intReviewLevelID = Convert.ToInt32(dtbUserReviewLevel.Rows[i]["ReviewLevelID"]);
                                break;
                            }
                        }
                    }
                }
                if (intReviewLevelID < 1)
                {
                    #region
                    txtContent.ReadOnly = true;
                    txtBarcode.Enabled = false;
                    btnSearch.Enabled = false;
                    flexDetail.AllowEditing = false;
                    btnSave.Enabled = false;
                    cboCabinet.Enabled = false;
                    btnDelete.Enabled = (intIsDelete == 1);
                    if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
                    {
                        if (objInventory.IsDeleted || objInventory.IsUpdateUnEvent)
                        {
                            btnReviewStatus.Enabled = false;

                            if (objInventory.IsDeleted)
                            {
                                btnDelete.Enabled = false;
                                chkIsDeleted.Checked = objInventory.IsDeleted;
                            }
                            if (objInventory.IsUpdateUnEvent)
                            {
                                mnuItemExplainUnEvent.Enabled = true;
                                if (objInventory.IsExplainUnEvent)
                                {
                                    mnuEventHandlingUnEvent.Enabled = true;
                                }
                            }
                        }
                    }
                    #endregion

                    DataRow[] drReview = dtbUserReviewLevel.Select("IsReviewed = 1");
                    if (drReview.Length == dtbUserReviewLevel.Rows.Count)
                    {
                        txtUserName.Enabled = false;
                        txtPassword.Enabled = false;
                        cboReviewStatus.Enabled = false;
                        btnReviewStatus.Enabled = false;
                    }

                    return;
                }
            }

            for (int i = 0; i < dtbReviewLevel.Rows.Count; i++)
            {
                if (Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]) == intReviewLevelID)
                {
                    if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCanAddNewProduct"]))
                    {
                        bolIsCanAddNewProduct = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCanAddNewProduct"]);
                    }
                    if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCanEditProduct"]))
                    {
                        bolIsCanEditProduct = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCanEditProduct"]);
                    }
                    if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCanRemoveProduct"]))
                    {
                        bolIsCanRemoveProduct = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCanRemoveProduct"]);
                    }
                    if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCanDeleteInventory"]))
                    {
                        bolIsCanDeleteInventory = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCanDeleteInventory"]);
                    }
                    break;
                }
            }

            intIsAddProduct_ReviewLevel = Convert.ToInt32(bolIsCanAddNewProduct);
            intIsEditProduct_ReviewLevel = Convert.ToInt32(bolIsCanEditProduct);
            intIsRemoveProduct_ReviewLevel = Convert.ToInt32(bolIsCanRemoveProduct);
            btnDelete.Enabled = (bolIsCanDeleteInventory && (intIsDelete == 1));

            if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
            {
                txtContent.ReadOnly = true;
                txtBarcode.Enabled = false;
                btnSearch.Enabled = false;
                flexDetail.AllowEditing = false;
                btnSave.Enabled = false;
                cboCabinet.Enabled = false;

                if (objInventory.IsDeleted || objInventory.IsUpdateUnEvent)
                {
                    btnReviewStatus.Enabled = false;

                    if (objInventory.IsDeleted)
                    {
                        btnDelete.Enabled = false;
                        chkIsDeleted.Checked = objInventory.IsDeleted;
                    }
                    if (objInventory.IsUpdateUnEvent)
                    {
                        mnuItemExplainUnEvent.Enabled = true;
                        if (objInventory.IsExplainUnEvent)
                        {
                            mnuEventHandlingUnEvent.Enabled = true;
                        }
                    }
                }
                else
                {
                    if (intIsEdit == 1)
                    {
                        txtContent.ReadOnly = false;
                        cboCabinet.Enabled = true;
                        flexDetail.AllowEditing = true;
                        if (intIsAddProduct_ReviewLevel == 1)
                        {
                            txtBarcode.Enabled = true;
                            btnSearch.Enabled = true;
                        }
                        btnSave.Enabled = true;
                    }
                }
                DataRow[] drReview = dtbUserReviewLevel.Select("IsReviewed = 1");
                if (drReview.Length == dtbUserReviewLevel.Rows.Count)
                {
                    txtUserName.Enabled = false;
                    txtPassword.Enabled = false;
                    cboReviewStatus.Enabled = false;
                    btnReviewStatus.Enabled = false;
                }
            }
        }

        private void LoadUserReviewLevel()
        {
            objPLCInventoryType.LoadInfo(ref objInventoryType, intInventoryTypeID);
            if (objInventoryType == null || objInventoryType.InventoryTypeID < 1)
            {
                if ((grdUserReviewLevel.DataSource as DataTable) == null)
                {
                    grdUserReviewLevel.DataSource = objPLCInventory.GetInventoryUserReviewed("-1", Convert.ToString(txtInventoryID.Text).Trim(), 0, Convert.ToInt32(cboStore.SelectedValue), false);
                }
                if (!(grdUserReviewLevel.DataSource as DataTable).Columns.Contains("ISSELECT"))
                {
                    (grdUserReviewLevel.DataSource as DataTable).Columns.Add("ISSELECT", typeof(Decimal));
                    (grdUserReviewLevel.DataSource as DataTable).Columns["ISSELECT"].DefaultValue = Convert.ToDecimal(1);
                }
                //CustomFlexUserRvwLvl();
                //flexUserReviewLevel.Cols["IsSelect"].Visible = false;
                FormatGridUserReviewLevel(false);
                return;
            }
            dtbReviewLevel = null;
            objPLCInventoryType.SearchDataReviewLevel(ref dtbReviewLevel, new object[] { "@InventoryTypeID", intInventoryTypeID });
            if (dtbReviewLevel.Rows.Count < 1)
            {
                if ((grdUserReviewLevel.DataSource as DataTable) == null)
                {
                    grdUserReviewLevel.DataSource = objPLCInventory.GetInventoryUserReviewed("-1", Convert.ToString(txtInventoryID.Text).Trim(), 0, Convert.ToInt32(cboStore.SelectedValue), false);
                }
                if (!(grdUserReviewLevel.DataSource as DataTable).Columns.Contains("ISSELECT"))
                {
                    (grdUserReviewLevel.DataSource as DataTable).Columns.Add("ISSELECT", typeof(Decimal));
                    (grdUserReviewLevel.DataSource as DataTable).Columns["ISSELECT"].DefaultValue = Convert.ToDecimal(1);
                }
                //CustomFlexUserRvwLvl();
                //flexUserReviewLevel.Cols["IsSelect"].Visible = false;
                FormatGridUserReviewLevel(false);
                return;
            }

            dtbReviewLevel.DefaultView.Sort = "ReviewSequence ASC";
            dtbReviewLevel = dtbReviewLevel.DefaultView.ToTable();
            dtbReviewLevel.PrimaryKey = new DataColumn[] { dtbReviewLevel.Columns["ReviewLevelID"] };

            if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
            {
                DataTable dtbInventoryRvwLevel = objPLCInventory.GetInventoryReviewLevel(objInventory.InventoryID);
                if (!dtbInventoryRvwLevel.Columns.Contains("ISSELECT"))
                {
                    dtbInventoryRvwLevel.Columns.Add("ISSELECT", typeof(Decimal));
                    dtbInventoryRvwLevel.Columns["ISSELECT"].DefaultValue = Convert.ToDecimal(1);
                }
                if (dtbInventoryRvwLevel.Rows.Count > 0)
                {
                    grdUserReviewLevel.DataSource = dtbInventoryRvwLevel;
                    //CustomFlexUserRvwLvl();
                    //flexUserReviewLevel.Cols["IsSelect"].Visible = false;
                    FormatGridUserReviewLevel(false);
                    return;
                }
                else
                {
                    grdUserReviewLevel.DataSource = dtbInventoryRvwLevel;
                    FormatGridUserReviewLevel(true);
                    //CustomFlexUserRvwLvl();
                    bolUpdate_IsAddReviewList = true;
                    txtUserName.Enabled = false;
                    txtPassword.Enabled = false;
                    cboReviewStatus.Enabled = false;
                    btnReviewStatus.Enabled = false;
                }
            }

            string strReviewFunctionID = string.Empty;
            DataTable dtbUserReviewLevel = null;
            for (int i = 0; i < dtbReviewLevel.Rows.Count; i++)
            {
                if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["ReviewFunctionID"]))
                {
                    strReviewFunctionID = Convert.ToString(dtbReviewLevel.Rows[i]["ReviewFunctionID"]).Trim();
                }
                bool bolIsCheckStorePermission = false;
                if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCheckStorePermission"]))
                {
                    bolIsCheckStorePermission = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCheckStorePermission"]);
                }
                bool bolIsCheckInventoryUser = false;
                if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCheckInventoryUser"]))
                {
                    bolIsCheckInventoryUser = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCheckInventoryUser"]);
                }
                DataTable dtbTEMP = objPLCInventory.GetInventoryUserReviewed(strReviewFunctionID, Convert.ToString(txtInventoryID.Text).Trim(), Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]), Convert.ToInt32(cboStore.SelectedValue), bolIsCheckStorePermission);
                if (dtbUserReviewLevel == null)
                {
                    dtbUserReviewLevel = dtbTEMP.Clone();
                    if (!dtbUserReviewLevel.Columns.Contains("ISSELECT"))
                    {
                        dtbUserReviewLevel.Columns.Add("ISSELECT", typeof(Decimal));
                        dtbUserReviewLevel.Columns["ISSELECT"].DefaultValue = Convert.ToDecimal(0);
                    }
                }
                int intReviewType = Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewType"]);
                if (bolIsCheckInventoryUser)
                {
                    DataRow[] rUserRvwFunct = dtbTEMP.Select("ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]) + " AND UserName = '" + strInventoryUser + "'");
                    if (rUserRvwFunct.Length > 0)
                    {
                        DataRow rInventoryUser = dtbUserReviewLevel.NewRow();
                        rInventoryUser.ItemArray = rUserRvwFunct[0].ItemArray;
                        rInventoryUser["ISSELECT"] = Convert.ToDecimal(1);
                        dtbUserReviewLevel.Rows.Add(rInventoryUser);
                    }
                }
                else
                {
                    for (int j = 0; j < dtbTEMP.Rows.Count; j++)
                    {
                        DataRow rInventoryUser = dtbUserReviewLevel.NewRow();
                        rInventoryUser.ItemArray = dtbTEMP.Rows[j].ItemArray;
                        if (intReviewType == 0)
                        {
                            //rInventoryUser["IsSelect"] = true;
                        }
                        dtbUserReviewLevel.Rows.Add(rInventoryUser);
                    }
                }
            }
            grdUserReviewLevel.DataSource = dtbUserReviewLevel;
            FormatGridUserReviewLevel(true);
            //CustomFlexUserRvwLvl();
        }

        private void CustomFlexUserRvwLvl()
        {
            if (flexUserReviewLevel.DataSource == null)
            {
                return;
            }

            for (int i = 0; i < flexUserReviewLevel.Cols.Count; i++)
            {
                flexUserReviewLevel.Cols[i].Visible = false;
                flexUserReviewLevel.Cols[i].AllowEditing = false;
                flexUserReviewLevel.Cols[i].AllowDragging = false;
            }
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexUserReviewLevel, true, "ReviewSequence,ReviewLevelName,IsReviewed,ReviewedDate,UserName,FullName,StatusName,IsSelect");
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnCaption(flexUserReviewLevel, "ReviewSequence,Thứ tự duyệt,ReviewLevelName,Mức duyệt,IsReviewed,Đã duyệt,ReviewedDate,Ngày duyệt,UserName,Người duyệt,FullName,Người duyệt,StatusName,Trạng thái duyệt,IsSelect,Chọn");
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnAllowMerging(flexUserReviewLevel, "ReviewSequence,ReviewLevelName", true);
            flexUserReviewLevel.Cols["IsReviewed"].DataType = typeof(bool);
            flexUserReviewLevel.Cols["ReviewedDate"].DataType = typeof(DateTime);
            flexUserReviewLevel.Cols["ReviewedDate"].Format = "dd/MM/yyyy HH:mm";
            flexUserReviewLevel.Cols["ReviewSequence"].Width = 60;
            flexUserReviewLevel.Cols["ReviewLevelName"].Width = 200;
            flexUserReviewLevel.Cols["IsReviewed"].Width = 70;
            flexUserReviewLevel.Cols["ReviewedDate"].Width = 110;
            flexUserReviewLevel.Cols["UserName"].Width = 80;
            flexUserReviewLevel.Cols["FullName"].Width = 180;
            flexUserReviewLevel.Cols["StatusName"].Width = 74;
            flexUserReviewLevel.Cols["IsSelect"].Width = 50;

            flexUserReviewLevel.Cols["IsSelect"].AllowEditing = true;
            flexUserReviewLevel.Cols["ReviewSequence"].AllowMerging = true;
            flexUserReviewLevel.Cols["ReviewLevelName"].AllowMerging = true;

            C1.Win.C1FlexGrid.CellStyle style = flexUserReviewLevel.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop;
            C1.Win.C1FlexGrid.CellRange range = flexUserReviewLevel.GetCellRange(0, 0, flexUserReviewLevel.Rows.Fixed - 1, flexUserReviewLevel.Cols.Count - 1);
            range.Style = style;
            flexUserReviewLevel.Rows[0].Height = 40;
            flexUserReviewLevel.Rows[0].AllowMerging = true;
        }

        #endregion

        #region event

        private void frmInventory_Load(object sender, EventArgs e)
        {

            txtInventoryID.Text = objPLCInventory.GetInventoryNewID(Library.AppCore.SystemConfig.intDefaultStoreID);
            cboReviewStatus.SelectedIndex = 0;
            cboCabinet.SelectedIndex = 0;
            LoadComboBox();

            InitDataFlex();
            InitGrid();
            LoadInventoryInfo();
            int intInventoryTermID = 0;
            if (objInventory != null)
            {
                intInventoryTermID = objInventory.InventoryTermID;
            }
            LoadInventoryTerm(intInventoryTermID);
            LoadInventoryUser();

            if (dtbInStockProduct == null || dtbInStockProduct.Rows.Count < 1)
            {
                dtbInStockProduct = objPLCInventory.GetListTermStock(objInventoryTerm.InventoryTermID, intStoreID, intMainGroupID, intSubGroupID);
                #region
                dtbInStockProduct.Columns.Add("CABINETNUMBER", typeof(int));
                dtbInStockProduct.Columns["CABINETNUMBER"].SetOrdinal(0);
                dtbInStockProduct.Columns.Add("IMEI", typeof(string));
                dtbInStockProduct.Columns["IMEI"].SetOrdinal(3);
                dtbInStockProduct.Columns.Add("COL_IMEI", typeof(string));
                dtbInStockProduct.Columns["COL_IMEI"].SetOrdinal(4);
                dtbInStockProduct.Columns.Add("IMEI_INVENTORY", typeof(string));
                dtbInStockProduct.Columns.Add("QUANTITY_INVENTORY", typeof(decimal));
                dtbInStockProduct.Columns.Add("DIFFERENT", typeof(decimal));
                #endregion
                dtbInStockProduct.PrimaryKey = new DataColumn[] { dtbInStockProduct.Columns["PRODUCTID"] };
            }

            if (dtbInStockProductIMEI == null || dtbInStockProductIMEI.Rows.Count < 1)
            {
                dtbInStockProductIMEI = objPLCInventory.GetListTermStockIMEI(objInventoryTerm.InventoryTermID, intStoreID, intMainGroupID, intSubGroupID);
                #region
                dtbInStockProductIMEI.Columns.Add("CABINETNUMBER", typeof(int));
                dtbInStockProductIMEI.Columns["CABINETNUMBER"].SetOrdinal(0);
                dtbInStockProductIMEI.Columns.Add("COL_IMEI", typeof(string));
                dtbInStockProductIMEI.Columns["COL_IMEI"].SetOrdinal(4);
                dtbInStockProductIMEI.Columns.Add("IMEI_INVENTORY", typeof(string));
                dtbInStockProductIMEI.Columns.Add("QUANTITY_INVENTORY", typeof(decimal));
                dtbInStockProductIMEI.Columns.Add("DIFFERENT", typeof(decimal));
                dtbInStockProductIMEI.Columns.Add("ISREQUESTIMEI", typeof(bool));
                dtbInStockProductIMEI.Columns["ISREQUESTIMEI"].DefaultValue = false;
                #endregion
            }
            dtbTmpStockIMEI = dtbInStockProductIMEI.Clone();
            for (int i = dtbInStockProductIMEI.Rows.Count - 1; i > -1; i--)
            {
                if (dtbInStockProductIMEI.Rows[i]["ProductInstockID"] != null && (!Convert.IsDBNull(dtbInStockProductIMEI.Rows[i]["ProductInstockID"]))
                    && Convert.ToInt32(dtbInStockProductIMEI.Rows[i]["ProductInstockID"]) != Convert.ToInt32(cboProductStateID.SelectedValue))
                {
                    DataRow rTmpStockIMEI = dtbTmpStockIMEI.NewRow();
                    rTmpStockIMEI.ItemArray = dtbInStockProductIMEI.Rows[i].ItemArray;
                    dtbTmpStockIMEI.Rows.Add(rTmpStockIMEI);
                    dtbInStockProductIMEI.Rows.RemoveAt(i);
                }
            }
            dtbInStockProductIMEI.PrimaryKey = new DataColumn[] { dtbInStockProductIMEI.Columns["ProductID"], dtbInStockProductIMEI.Columns["IMEI"] };
            dtbTmpStockIMEI.PrimaryKey = new DataColumn[] { dtbTmpStockIMEI.Columns["ProductID"], dtbTmpStockIMEI.Columns["IMEI"] };

            dtbInStockProductIMEI_Unique = objPLCInventory.GetListTermStockIMEI_Unique(objInventoryTerm.InventoryTermID, intStoreID, intMainGroupID, intSubGroupID);
            dtbInStockProductIMEI_Unique_Exclude = dtbInStockProductIMEI_Unique.Clone();
            for (int i = dtbInStockProductIMEI_Unique.Rows.Count - 1; i > -1; i--)
            {
                if (dtbInStockProductIMEI_Unique.Rows[i]["ProductInstockID"] != null && (!Convert.IsDBNull(dtbInStockProductIMEI_Unique.Rows[i]["ProductInstockID"]))
                    && Convert.ToInt32(dtbInStockProductIMEI_Unique.Rows[i]["ProductInstockID"]) != Convert.ToInt32(cboProductStateID.SelectedValue))
                {
                    DataRow rExclude = dtbInStockProductIMEI_Unique_Exclude.NewRow();
                    rExclude.ItemArray = dtbInStockProductIMEI_Unique.Rows[i].ItemArray;
                    dtbInStockProductIMEI_Unique_Exclude.Rows.Add(rExclude);
                    dtbInStockProductIMEI_Unique.Rows.RemoveAt(i);
                }
            }
            dtbInStockProductIMEI_Unique.PrimaryKey = new DataColumn[] { dtbInStockProductIMEI_Unique.Columns["IMEI"] };
            dtbInStockProductIMEI_Unique_Exclude.PrimaryKey = new DataColumn[] { dtbInStockProductIMEI_Unique_Exclude.Columns["IMEI"] };

            flexDetail.DataSource = dtbData;
            FormatFlex();

            LoadUserReviewLevel();

            if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
            {
                CheckReviewLevel();
            }

            //dtbStoreInStock = objPLCReportDataSource.GetDataSource("PM_PrdChange_GetStoreInStock", new object[] { "@StoreID", 0 });

            ERP.MasterData.SYS.PLC.WSAppConfig.AppConfig objAppConfig_IsCheckDuplicateIMEIAllProduct = null;
            ERP.MasterData.SYS.PLC.WSAppConfig.AppConfig objAppConfig_IsCheckDuplicateIMEIAllStore = null;
            ERP.MasterData.SYS.PLC.PLCAppConfig objPLCAppConfig = new MasterData.SYS.PLC.PLCAppConfig();
            objPLCAppConfig.LoadInfo(ref objAppConfig_IsCheckDuplicateIMEIAllProduct, "ISCHECKDUPLICATEIMEIALLPRODUCT");
            if (objAppConfig_IsCheckDuplicateIMEIAllProduct != null && objAppConfig_IsCheckDuplicateIMEIAllProduct.ConfigValue != string.Empty)
            {
                try
                {
                    bolIsCheckDuplicateIMEIAllProduct = Convert.ToBoolean(Convert.ToInt32(objAppConfig_IsCheckDuplicateIMEIAllProduct.ConfigValue));
                }
                catch
                {
                }
            }
            objPLCAppConfig.LoadInfo(ref objAppConfig_IsCheckDuplicateIMEIAllStore, "ISCHECKDUPLICATEIMEIALLSTORE");
            if (objAppConfig_IsCheckDuplicateIMEIAllStore != null && objAppConfig_IsCheckDuplicateIMEIAllStore.ConfigValue != string.Empty)
            {
                try
                {
                    bolIsCheckDuplicateIMEIAllStore = Convert.ToBoolean(Convert.ToInt32(objAppConfig_IsCheckDuplicateIMEIAllStore.ConfigValue));
                }
                catch
                {
                }
            }

            if (btnSave.Enabled && btnSave.Visible)
            {
                tmr.Enabled = true;
            }
        }

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToString(txtBarcode.Text).Trim() != string.Empty && e.KeyChar == (char)Keys.Enter)
            {
                txtBarcode.Text = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(txtBarcode.Text.Trim());
                string strProductID = string.Empty;
                int intIsRequestIMEI = 0;
                string strProductName = string.Empty;
                string strIMEI = string.Empty;
                int intIsError = 0;
                string strErrorContent = string.Empty;
                int intProductSubGroupID = -1;
                if (!AddProduct(txtBarcode.Text, 0, ref strProductID, ref intIsRequestIMEI, ref strProductName, ref strIMEI, ref intIsError, ref strErrorContent, ref intProductSubGroupID))
                {
                    MessageBox.Show(this, strErrorContent, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    DataRow rDetail = dtbData.NewRow();
                    rDetail["ProductID"] = strProductID;
                    rDetail["IsRequestIMEI"] = intIsRequestIMEI;
                    rDetail["ProductName"] = strProductName;
                    rDetail["IsEdit"] = 1;
                    rDetail["SubGroupID"] = intProductSubGroupID;
                    //rDetail["ProductInstockID"] = intProductInstockID;
                    if (strIMEI != string.Empty)
                    {
                        rDetail["IMEI"] = strIMEI;
                        rDetail["Quantity"] = 1;
                        DataRow rInStockIMEI = dtbInStockProductIMEI.Rows.Find(new object[] { strProductID, strIMEI });
                        if (rInStockIMEI != null)
                        {
                            rInStockIMEI["IMEI_Inventory"] = strIMEI;
                            rInStockIMEI["Quantity_Inventory"] = 1;
                            decimal decQuantityInStock = 0;
                            if (!Convert.IsDBNull(rInStockIMEI["Quantity"]))
                            {
                                decQuantityInStock = Convert.ToDecimal(rInStockIMEI["Quantity"]);
                            }
                            rDetail["IsInStock"] = 1;
                            rDetail["InStockQuantity"] = decQuantityInStock;
                            rInStockIMEI["Different"] = decQuantityInStock - 1;
                        }
                        else
                        {
                            if (intIsError == 2 && strErrorContent.IndexOf("không tồn tại trong kho") > 0)
                            {
                                if (bolIsCheckDuplicateIMEIAllStore && bolIsCheckDuplicateIMEIAllProduct)
                                {
                                    if (objPLCProductInStock.CheckIsExistIMEI(string.Empty, strIMEI, 0, 0, -1, true, true))
                                    //DataRow[] drExistInSystem = dtbStoreInStock.Select("TRIM(IMEI)='" + strIMEI + "'");
                                    //if (drExistInSystem.Length > 0)
                                    {
                                        MessageBox.Show(this, "IMEI " + strIMEI + " đã tồn tại trong hệ thống. Vui lòng kiểm tra lại tồn kho hiện tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                }

                                DataRow rTmpStockIMEI = dtbTmpStockIMEI.Rows.Find(new object[] { strProductID, strIMEI });
                                if (rTmpStockIMEI != null)
                                {
                                    MessageBox.Show(this, "IMEI " + strIMEI + " không thuộc trạng thái kiểm kê hàng " + cboProductStateID.SelectedText, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                            rDetail["IsInStock"] = 0;
                            rDetail["InStockQuantity"] = 0;
                        }
                    }
                    else
                    {
                        if (intIsRequestIMEI == 0)
                        {
                            DataRow[] rExist = dtbData.Select("TRIM(ProductID) = '" + strProductID + "'");
                            if (rExist.Length > 0)
                            {
                                //Khi bắn mã sp, nếu đã có thì tăng thêm 1
                                int intIndexExist = dtbData.Rows.IndexOf(rExist[0]);
                                if (intIndexExist > -1 && intIndexExist < dtbData.Rows.Count)
                                {
                                    dtbData.Rows[intIndexExist]["Quantity"] = Convert.ToDecimal(dtbData.Rows[intIndexExist]["Quantity"]) + 1;
                                    txtBarcode.Text = string.Empty;
                                }
                                return;
                            }
                        }

                        rDetail["IMEI"] = string.Empty;
                        rDetail["Quantity"] = 1;

                        DataRow rInStockProduct = dtbInStockProduct.Rows.Find(new object[] { strProductID });
                        if (rInStockProduct != null)
                        {
                            rDetail["IsInStock"] = 0;
                            decimal decQuantityInStock = 0;
                            if (!Convert.IsDBNull(rInStockProduct["Quantity"]))
                            {
                                decQuantityInStock = Convert.ToDecimal(rInStockProduct["Quantity"]);
                                if (decQuantityInStock > 0)
                                {
                                    rDetail["IsInStock"] = 1;
                                }
                            }
                            rDetail["InStockQuantity"] = decQuantityInStock;
                        }
                        else
                        {
                            rDetail["IsInStock"] = 0;
                            rDetail["InStockQuantity"] = 0;
                        }
                    }
                    rDetail["CabinetNumber"] = Convert.ToInt32(cboCabinet.Text);
                    rDetail["IsError"] = intIsError;
                    rDetail["ErrorContent"] = strErrorContent;

                    dtbData.Rows.InsertAt(rDetail, 0);
                    flexDetail.Select(1, flexDetail.Cols["ProductID"].Index, 1, flexDetail.Cols["CabinetNumber"].Index);

                    if (intIsError > 1)
                    {
                        if (intIsError == 2)
                        {
                            Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexDetail, dtbData.Rows.Count, Color.Pink);
                        }
                        else
                        {
                            Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexDetail, dtbData.Rows.Count, Color.Yellow);
                        }
                    }

                    txtBarcode.Text = string.Empty;
                }
                SumQuantity();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ERP.MasterData.DUI.Search.frmProductSearch frmProductSearch1 = MasterData.DUI.MasterData_Globals.frmProductSearch;
            frmProductSearch1.MainGroupID = Convert.ToInt32(cboMainGroup.SelectedValue);
            frmProductSearch1.IsMultiSelect = false;
            frmProductSearch1.ShowDialog();
            if (frmProductSearch1.Product != null && Convert.ToString(frmProductSearch1.Product.ProductName).Trim() != string.Empty)
            {
                txtBarcode.Text = Convert.ToString(frmProductSearch1.Product.ProductID).Trim();
                txtBarcode.Focus();
            }
        }

        private void flexDetail_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexDetail.Rows.Count <= flexDetail.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexDetail.Rows.Fixed)
            {
                return;
            }
            dtbData.AcceptChanges();
            if (e.Col == flexDetail.Cols["IMEI"].Index)
            {
                DataRow rOldIMEI = dtbData.NewRow();
                rOldIMEI.ItemArray = rOld.ItemArray;
                string strProductID = Convert.ToString(flexDetail[e.Row, "ProductID"]).Trim();
                string strProductName = Convert.ToString(flexDetail[e.Row, "ProductName"]).Trim();
                string strIMEI = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(Convert.ToString(flexDetail[e.Row, "IMEI"]).Trim());
                int intSubGroupID = Convert.ToInt32(flexDetail[e.Row, "SubGroupID"]);
                flexDetail[e.Row, "IMEI"] = strIMEI;

                string strOldIMEI = Convert.ToString(rOldIMEI["IMEI"]).Trim();
                if (strOldIMEI != string.Empty && strOldIMEI != strIMEI)
                {
                    if (strIMEI == string.Empty && objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
                    {
                        flexDetail[e.Row, "IMEI"] = strOldIMEI;
                        return;
                    }
                    DataRow rOldCheck = dtbInStockProductIMEI.Rows.Find(new object[] { strProductID, strOldIMEI });
                    if (rOldCheck != null)
                    {
                        rOldCheck["Col_IMEI"] = string.Empty;
                        rOldCheck["IMEI_Inventory"] = string.Empty;
                        rOldCheck["Quantity_Inventory"] = System.DBNull.Value;
                        rOldCheck["Different"] = System.DBNull.Value;
                        rOldCheck["IsRequestIMEI"] = System.DBNull.Value;
                    }

                    for (int i = dtbInStockProductIMEI.Rows.Count - 1; i > -1; i--)
                    {
                        if ((!Convert.IsDBNull(dtbInStockProductIMEI.Rows[i]["IMEI_Inventory"]))
                            && Convert.ToString(dtbInStockProductIMEI.Rows[i]["IMEI_Inventory"]) == strOldIMEI)
                        {
                            if ((!Convert.IsDBNull(dtbInStockProductIMEI.Rows[i]["IMEI"]))
                            && Convert.ToString(dtbInStockProductIMEI.Rows[i]["IMEI"]).IndexOf('-') == 0)
                                dtbInStockProductIMEI.Rows.RemoveAt(i);
                            break;
                        }
                    }
                }

                if (strIMEI == string.Empty)
                {
                    if (Convert.ToBoolean(flexDetail[e.Row, "IsRequestIMEI"]))
                    {
                        flexDetail[e.Row, "IsError"] = 3;
                        flexDetail[e.Row, "ErrorContent"] = "Sản phẩm yêu cầu nhập IMEI";

                        flexDetail[e.Row, "IsInStock"] = 0;
                        flexDetail[e.Row, "InStockQuantity"] = 0;
                        flexDetail[e.Row, "Quantity"] = 0;
                        flexDetail[e.Row, "IsEdit"] = 1;
                        Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexDetail, e.Row, Color.Yellow);
                    }

                    SumQuantity();
                    return;
                }
                else
                {
                    if ((strOldIMEI == string.Empty) || (strOldIMEI != string.Empty && strOldIMEI != strIMEI))
                    {
                        DataRow[] rExitIMEI = dtbData.Select("TRIM(IMEI) = '" + strIMEI + "'");
                        if ((!Library.AppCore.Other.CheckObject.CheckIMEI(strIMEI)) || rExitIMEI.Length > 1)
                        {
                            if (!Library.AppCore.Other.CheckObject.CheckIMEI(strIMEI))
                            {
                                MessageBox.Show(this, "IMEI không đúng định dạng. Dữ liệu nhập vào phải không có khoảng trắng hoặc không có ký tự xuống hàng. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            else
                            {
                                MessageBox.Show(this, "IMEI " + strIMEI + " đã tồn tại trong phiếu kiểm kê hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }

                            flexDetail[e.Row, "IMEI"] = string.Empty;
                            if (Convert.ToBoolean(flexDetail[e.Row, "IsRequestIMEI"]))
                            {
                                flexDetail[e.Row, "IsError"] = 3;
                                flexDetail[e.Row, "ErrorContent"] = "Sản phẩm yêu cầu nhập IMEI";

                                flexDetail[e.Row, "IsInStock"] = 0;
                                flexDetail[e.Row, "InStockQuantity"] = 0;
                                flexDetail[e.Row, "Quantity"] = 0;
                                flexDetail[e.Row, "IsEdit"] = 1;
                                Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexDetail, e.Row, Color.Yellow);
                            }

                            SumQuantity();
                            return;
                        }
                    }
                }

                DataRow rInStockIMEI = dtbInStockProductIMEI.Rows.Find(new object[] { strProductID, strIMEI });
                if (rInStockIMEI == null)
                {
                    DataRow rTmpStockIMEI = dtbTmpStockIMEI.Rows.Find(new object[] { strProductID, strIMEI });
                    if (rTmpStockIMEI != null)
                    {
                        MessageBox.Show(this, "IMEI " + strIMEI + " không thuộc trạng thái phiếu kiểm kê hàng " +cboProductStateID.SelectedText, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexDetail[e.Row, "IMEI"] = string.Empty;

                        SumQuantity();
                        return;
                    }

                    flexDetail[e.Row, "IsInStock"] = 0;
                    flexDetail[e.Row, "InStockQuantity"] = 0;
                    flexDetail[e.Row, "Quantity"] = 1;
                    flexDetail[e.Row, "IsError"] = 2;
                    flexDetail[e.Row, "ErrorContent"] = "IMEI " + strIMEI + " không tồn tại trong kho";
                    flexDetail[e.Row, "IsEdit"] = 1;
                    Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexDetail, e.Row, Color.Pink);

                    DataRow[] rExistIMEINotInStock = dtbInStockProductIMEI.Select("TRIM(ProductID) = '" + strProductID + "' AND (Col_IMEI IS NULL OR TRIM(Col_IMEI)='') AND TRIM(IMEI_Inventory) = '" + strIMEI + "'");
                    if (rExistIMEINotInStock.Length < 1)
                    {
                        DataRow rNew = dtbInStockProductIMEI.NewRow();
                        rNew["CabinetNumber"] = Convert.ToInt32(flexDetail[e.Row, "CabinetNumber"]);
                        rNew["ProductID"] = strProductID;
                        rNew["ProductName"] = strProductName;
                        rNew["IMEI"] = (--intKeyIMEI);
                        rNew["Col_IMEI"] = string.Empty;
                        rNew["IMEI_Inventory"] = strIMEI;
                        rNew["Quantity_Inventory"] = 1;
                        decimal decQuantityInStock = 0;
                        rNew["Different"] = decQuantityInStock - 1;
                        rNew["SubGroupID"] = intSubGroupID;
                        dtbInStockProductIMEI.Rows.Add(rNew);
                    }

                    SumQuantity();
                    return;
                }
                else
                {
                    flexDetail[e.Row, "Quantity"] = 1;
                    flexDetail[e.Row, "IsInStock"] = 1;
                    flexDetail[e.Row, "InStockQuantity"] = Convert.ToDecimal(rInStockIMEI["Quantity"]);

                    rInStockIMEI["IMEI_Inventory"] = strIMEI;
                    rInStockIMEI["Quantity_Inventory"] = 1;
                }
                flexDetail[e.Row, "IsError"] = 0;
                flexDetail[e.Row, "ErrorContent"] = string.Empty;
                flexDetail[e.Row, "IsEdit"] = 1;
                Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexDetail, e.Row, flexDetail.BackColor);
            }
            else if (e.Col == flexDetail.Cols["Quantity"].Index)
            {
                string strProductID = Convert.ToString(flexDetail[e.Row, "ProductID"]).Trim();
                if (flexDetail[e.Row, "Quantity"] == null || flexDetail[e.Row, "Quantity"] == DBNull.Value || Convert.ToString(flexDetail[e.Row, "Quantity"]).Trim() == string.Empty)
                {
                    flexDetail[e.Row, "Quantity"] = 0;
                }
                decimal decQuantity = Convert.ToDecimal(flexDetail[e.Row, "Quantity"]);
                if (!Convert.ToBoolean(flexDetail[e.Row, "IsRequestIMEI"]))
                {
                    DataRow rInStockProduct = dtbInStockProduct.Rows.Find(strProductID);
                    if (rInStockProduct != null)
                    {
                        flexDetail[e.Row, "IsInStock"] = 0;
                        decimal decQuantityInStock = Convert.ToDecimal(rInStockProduct["Quantity"]);
                        if (decQuantityInStock > 0)
                        {
                            flexDetail[e.Row, "IsInStock"] = 1;
                        }
                        flexDetail[e.Row, "InStockQuantity"] = decQuantityInStock;
                        rInStockProduct["Quantity_Inventory"] = decQuantity;
                        rInStockProduct["Different"] = decQuantityInStock - decQuantity;
                        ERP.MasterData.PLC.MD.WSProduct.Product objProduct = new MasterData.PLC.MD.WSProduct.Product();
                        objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                        if (!objProduct.IsAllowDecimal)
                        {

                            if (decQuantity % 1 > 0)
                            {
                                rInStockProduct["Quantity_Inventory"] = DBNull.Value;
                                rInStockProduct["Different"] = decQuantityInStock;
                                flexDetail[e.Row, "IsError"] = 2;
                                flexDetail[e.Row, "ErrorContent"] = "Số lượng của mã sản phẩm " + strProductID + "không hợp lệ (Sản phẩm không cho phép số lượng là số thực). Vui lòng kiểm tra lại";

                                flexDetail[e.Row, "IsEdit"] = 0;
                                Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexDetail, e.Row, Color.Yellow);
                                return;
                            }
                        }
                        if (decQuantityInStock < decQuantity)
                        {
                            flexDetail[e.Row, "IsError"] = 2;
                            flexDetail[e.Row, "ErrorContent"] = "Số lượng của mã sản phẩm " + strProductID + " không hợp lệ";
                            flexDetail[e.Row, "IsEdit"] = 1;
                            Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexDetail, e.Row, Color.Pink);

                            SumQuantity();
                            return;
                        }
                        SumQuantity();
                    }
                    else
                    {
                        flexDetail[e.Row, "IsInStock"] = 0;
                        flexDetail[e.Row, "InStockQuantity"] = 0;

                        flexDetail[e.Row, "IsError"] = 1;
                        flexDetail[e.Row, "ErrorContent"] = "Sản phẩm " + strProductID + " không tồn kho";
                        flexDetail[e.Row, "IsEdit"] = 1;
                        Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexDetail, e.Row, Color.Pink);

                        SumQuantity();
                        return;
                    }
                    flexDetail[e.Row, "IsError"] = 0;
                    flexDetail[e.Row, "ErrorContent"] = string.Empty;
                    flexDetail[e.Row, "IsEdit"] = 1;
                    Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexDetail, e.Row, flexDetail.BackColor);
                }
            }

            SumQuantity();
        }

        private void flexDetail_SetupEditor(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Col == flexDetail.Cols["IMEI"].Index)
            {
                TextBox txtEditor = flexDetail.Editor as TextBox;
                txtEditor.MaxLength = 50;
            }
        }

        private void flexDetail_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexDetail.Rows.Count <= flexDetail.Rows.Fixed)
            {
                return;
            }
            if (e.Row < flexDetail.Rows.Fixed)
            {
                return;
            }
            if (e.Col == flexDetail.Cols["IMEI"].Index)
            {
                if (!Convert.ToBoolean(flexDetail[e.Row, "IsRequestIMEI"]))
                {
                    e.Cancel = true;
                    return;
                }
            }
            else if (e.Col == flexDetail.Cols["Quantity"].Index)
            {
                if (Convert.ToBoolean(flexDetail[e.Row, "IsRequestIMEI"]))
                {
                    e.Cancel = true;
                    return;
                }
            }

            if (Convert.ToInt32(flexDetail[e.Row, "IsInsert"]) == 0)
            {
                if (intIsEdit != 1 || intIsEditProduct_ReviewLevel != 1)
                {
                    e.Cancel = true;
                    return;
                }
            }
            else
            {
                if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
                {
                    if (intIsEdit != 1 || intIsAddProduct_ReviewLevel != 1)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
            }

            rOld = dtbData.NewRow();
            rOld.ItemArray = dtbData.Rows[e.Row - flexDetail.Rows.Fixed].ItemArray;
        }

        private void flexDetail2_BeforeDeleteRow(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
            {
                e.Cancel = true;
                return;
            }

            if (((!Convert.IsDBNull(flexDetail[e.Row, "IsRequestIMEI"])) && Convert.ToInt32(flexDetail[e.Row, "IsRequestIMEI"]) == 1)
                && ((!Convert.IsDBNull(flexDetail[e.Row, "IsInStock"])) && Convert.ToInt32(flexDetail[e.Row, "IsInStock"]) == 1))
            {
                e.Cancel = true;
                return;
            }

            if (Convert.IsDBNull(flexDetail[e.Row, "IMEI"]) || Convert.ToString(flexDetail[e.Row, "IMEI"]).Trim() == string.Empty)
            {
                if (Convert.IsDBNull(flexDetail[e.Row, "IsRequestIMEI"]) || (!Convert.IsDBNull(flexDetail[e.Row, "IsRequestIMEI"])))
                {
                    string strProductID = Convert.ToString(flexDetail[e.Row, "ProductID"]).Trim();
                    DataRow rInStockProduct = dtbInStockProduct.Rows.Find(strProductID);
                    if (rInStockProduct != null)
                    {
                        rInStockProduct["Col_IMEI"] = string.Empty;
                        rInStockProduct["IMEI_Inventory"] = string.Empty;
                        rInStockProduct["Quantity_Inventory"] = System.DBNull.Value;
                        rInStockProduct["Different"] = System.DBNull.Value;
                    }
                }
            }
            else
            {
                string strProductID = Convert.ToString(flexDetail[e.Row, "ProductID"]).Trim();
                string strIMEI = Convert.ToString(flexDetail[e.Row, "IMEI"]).Trim();
                DataRow rInStockIMEI = dtbInStockProductIMEI.Rows.Find(new object[] { strProductID, strIMEI });
                if (rInStockIMEI != null)
                {
                    rInStockIMEI["Col_IMEI"] = string.Empty;
                    rInStockIMEI["IMEI_Inventory"] = string.Empty;
                    rInStockIMEI["Quantity_Inventory"] = System.DBNull.Value;
                    rInStockIMEI["Different"] = System.DBNull.Value;
                    rInStockIMEI["IsRequestIMEI"] = System.DBNull.Value;
                }
                else
                {
                    for (int i = 0; i < dtbInStockProductIMEI.Rows.Count; i++)
                    {
                        DataRow r = dtbInStockProductIMEI.Rows[i];
                        if ((!Convert.IsDBNull(r["IMEI_Inventory"])) && Convert.ToString(r["IMEI_Inventory"]).Trim() == strIMEI)
                        {
                            dtbInStockProductIMEI.Rows.RemoveAt(i);
                            break;
                        }
                    }
                }
            }
        }

        private void mnuItemExportTemplateExcel_Click(object sender, EventArgs e)
        {
            DataTable tblTemplate = new DataTable();
            {
                tblTemplate.Columns.Add("Mã sản phẩm", typeof(string));
                tblTemplate.Columns.Add("Tên sản phẩm", typeof(string));
                tblTemplate.Columns.Add("IMEI", typeof(string));
                tblTemplate.Columns.Add("Số lượng", typeof(decimal));
                tblTemplate.Columns.Add("Tủ", typeof(int));
            }
            Library.AppCore.LoadControls.C1FlexGridObject.ExportDataTableToExcel(this, tblTemplate);
        }

        private void mnuItemImportExcel_Click(object sender, EventArgs e)
        {
            ImportExcel();
        }

        private void flexDetail_DoubleClick(object sender, EventArgs e)
        {
            if (flexDetail.RowSel < flexDetail.Rows.Fixed)
            {
                return;
            }
            if (flexDetail.Rows.Count <= flexDetail.Rows.Fixed)
            {
                return;
            }
            if (Convert.ToInt32(flexDetail[flexDetail.RowSel, "IsError"]) > 0)
            {
                MessageBox.Show(this, Convert.ToString(flexDetail[flexDetail.RowSel, "ErrorContent"]).Trim(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void mnuItemViewUnEvent_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intCabinet = Convert.ToInt32(cboCabinet.Text);
            frmShowUnEvent frmShowUnEvent1 = new frmShowUnEvent();
            DataTable tblUnEventTable = dtbInStockProductIMEI.Clone();
            DataTable dtbInventoryUnEvent = null;
            if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
            {
                #region
                dtbInventoryUnEvent = objPLCInventory.GetInventoryUnEvent(objInventory.InventoryID);
                if (dtbInventoryUnEvent != null && dtbInventoryUnEvent.Rows.Count > 0)
                {
                    #region Code trước đó
                    //for (int i = 0; i < dtbInventoryUnEvent.Rows.Count; i++)
                    //{
                    //    DataRow r = dtbInventoryUnEvent.Rows[i];
                    //    DataRow rUnEvent = tblUnEventTable.NewRow();
                    //    rUnEvent["CabinetNumber"] = r["CabinetNumber"];
                    //    rUnEvent["ProductID"] = r["ProductID"];
                    //    rUnEvent["ProductName"] = r["ProductName"];
                    //    rUnEvent["IMEI"] = r["StockIMEI"];
                    //    rUnEvent["Col_IMEI"] = r["StockIMEI"];
                    //    if (Convert.ToDecimal(r["StockQuantity"]) != 0)
                    //    {
                    //        rUnEvent["Quantity"] = r["StockQuantity"];
                    //    }
                    //    rUnEvent["IMEI_Inventory"] = r["InventoryIMEI"];
                    //    if (Convert.ToDecimal(r["InventoryQuantity"]) != 0)
                    //    {
                    //        rUnEvent["Quantity_Inventory"] = r["InventoryQuantity"];
                    //    }
                    //    rUnEvent["Different"] = r["UnEventQuantity"];
                    //    if (rUnEvent["IMEI"] == null || Convert.IsDBNull(rUnEvent["IMEI"]))
                    //    {
                    //        rUnEvent["IMEI"] = (--intKeyIMEI);
                    //    }
                    //    if (rUnEvent["IMEI"].ToString() == Convert.ToString(--intKeyIMEI))
                    //    {
                    //        intKeyIMEI--;
                    //    }
                    //    tblUnEventTable.Rows.Add(rUnEvent);
                    //}
                    #endregion

                    // LÊ VĂN ĐÔNG: 12/10/2017
                    #region Chưa duyệt
                    DataTable dtbFlex = (DataTable)flexDetail.DataSource;
                    if (dtbFlex.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtbFlex.Rows.Count; i++)
                        {
                            DataRow rowFlex = dtbFlex.Rows[i];
                            if (!Convert.ToBoolean(rowFlex["IsRequestIMEI"]))
                            {
                                DataRow rProduct = dtbInStockProduct.Rows.Find(rowFlex["ProductID"].ToString());
                                if (rProduct == null)
                                {
                                    rProduct = dtbInStockProduct.NewRow();
                                    rProduct["IsRequestIMEI"] = 0;
                                    rProduct["CabinetNumber"] = rowFlex["CabinetNumber"];
                                    rProduct["ProductID"] = rowFlex["ProductID"];
                                    rProduct["ProductName"] = rowFlex["ProductName"];
                                    rProduct["IMEI"] = string.Empty;
                                    rProduct["Col_IMEI"] = string.Empty;
                                    rProduct["IMEI_Inventory"] = string.Empty;
                                    rProduct["Quantity_Inventory"] = rowFlex["Quantity"];
                                    rProduct["Quantity"] = 0;
                                    if (Convert.ToBoolean(rowFlex["IsInStock"]))
                                        rProduct["Quantity"] = rowFlex["InStockQuantity"];
                                    rProduct["Different"] = Convert.ToDecimal(rProduct["Quantity"]) - Convert.ToDecimal(rProduct["Quantity_Inventory"]);
                                    dtbInStockProduct.Rows.Add(rProduct);
                                }
                            }
                            else
                            {
                                DataRow rIMEI = dtbInStockProductIMEI.Rows.Find(new object[] { rowFlex["ProductID"].ToString(), rowFlex["IMEI"].ToString() });
                                if (rIMEI == null && rowFlex["IMEI"].ToString() != strIMEICheck)
                                {
                                    strIMEICheck = rowFlex["IMEI"].ToString();
                                    rIMEI = dtbInStockProductIMEI.NewRow();
                                    rIMEI["IsRequestIMEI"] = 1;
                                    rIMEI["CabinetNumber"] = rowFlex["CabinetNumber"];
                                    rIMEI["ProductID"] = rowFlex["ProductID"];
                                    rIMEI["ProductName"] = rowFlex["ProductName"];
                                    rIMEI["IMEI"] = (--intKeyIMEI);
                                    rIMEI["Col_IMEI"] = string.Empty;
                                    rIMEI["IMEI_Inventory"] = rowFlex["IMEI"];
                                    rIMEI["Quantity_Inventory"] = rowFlex["Quantity"];
                                    rIMEI["Quantity"] = 0;
                                    if (Convert.ToBoolean(rowFlex["IsInStock"]))
                                        rIMEI["Quantity"] = rowFlex["InStockQuantity"];
                                    rIMEI["Different"] = Convert.ToDecimal(rIMEI["Quantity"]) - Convert.ToDecimal(rIMEI["Quantity_Inventory"]);
                                    DataRow[] dr = dtbInStockProductIMEI.Select("IMEI_Inventory = '" + rowFlex["IMEI"] + "'");
                                    if (dr.Length == 0)
                                        dtbInStockProductIMEI.Rows.Add(rIMEI);
                                }
                            }
                        }
                    }

                    if (dtbInStockProduct.Rows.Count > 0)
                    {
                        DataTable dtbStockProduct = dtbInStockProduct.Clone();
                        var lstData = from o in dtbInStockProduct.AsEnumerable()
                                      //from i in dtbProductCache.AsEnumerable()
                                      where Convert.ToInt32(o["SubGroupID"].ToString().Trim()) == intSubGroupID
                                      select o;
                        if (lstData.Count() > 0)
                            dtbStockProduct = lstData.CopyToDataTable();
                        for (int i = 0; i < dtbStockProduct.Rows.Count; i++)
                        {
                            DataRow rInStockProduct = dtbStockProduct.Rows[i];
                            //if (intSubGroupID > 0)
                            //{
                            //    if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(rInStockProduct["ProductID"].ToString().Trim()) == null)
                            //        continue;
                            //    else
                            //    {
                            //        if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(rInStockProduct["ProductID"].ToString().Trim()).SubGroupID != intSubGroupID)
                            //            continue;
                            //    }
                            //}
                            if (!Convert.ToBoolean(rInStockProduct["IsRequestIMEI"]))
                            {
                                DataRow rUnEvent = tblUnEventTable.NewRow();
                                rUnEvent.ItemArray = rInStockProduct.ItemArray;
                                if (rUnEvent["CabinetNumber"] == null || (Convert.IsDBNull(rUnEvent["CabinetNumber"])) || Convert.ToString(rUnEvent["CabinetNumber"]).Trim() == string.Empty)
                                {
                                    rUnEvent["CabinetNumber"] = intCabinet;
                                }
                                if (!Convert.IsDBNull(rUnEvent["Quantity"]))
                                {
                                    rUnEvent["Col_IMEI"] = rUnEvent["IMEI"];
                                }
                                if (Convert.IsDBNull(rUnEvent["Different"]))
                                {
                                    decimal decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]);
                                    if (!Convert.IsDBNull(rUnEvent["Quantity_Inventory"]))
                                    {
                                        decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]) - Convert.ToDecimal(rUnEvent["Quantity_Inventory"]);
                                    }
                                    else
                                    {
                                        DataRow[] drDetail = dtbData.Select("TRIM(ProductID)='" + Convert.ToString(rInStockProduct["ProductID"]).Trim() + "'");
                                        if (drDetail.Length > 0 && (!Convert.IsDBNull(drDetail[0]["Quantity"])))
                                        {
                                            rUnEvent["Quantity_Inventory"] = Convert.ToDecimal(drDetail[0]["Quantity"]);
                                            decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]) - Convert.ToDecimal(drDetail[0]["Quantity"]);
                                        }
                                    }
                                    rUnEvent["Different"] = decDifferent;
                                }
                                if (Convert.IsDBNull(rUnEvent["IMEI"]) || Convert.ToString(rUnEvent["IMEI"]).Trim() == string.Empty)
                                {
                                    rUnEvent["IMEI"] = (--intKeyIMEI);
                                }
                                tblUnEventTable.Rows.Add(rUnEvent);
                            }
                        }
                    }
                    if (dtbInStockProductIMEI.Rows.Count > 0)
                    {
                        DataTable dtbStockIMEI = dtbInStockProductIMEI.Clone();
                        var lstData = from o in dtbInStockProductIMEI.AsEnumerable()
                                      //from i in dtbProductCache.AsEnumerable()
                                      where Convert.ToInt32(o["SubGroupID"].ToString().Trim()) == intSubGroupID
                                      select o;
                        if (lstData.Count() > 0)
                            dtbStockIMEI = lstData.CopyToDataTable();
                        for (int i = 0; i < dtbStockIMEI.Rows.Count; i++)
                        {
                            DataRow rInStockProductIMEI = dtbStockIMEI.Rows[i];
                            //if (intSubGroupID > 0)
                            //{
                            //    if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(rInStockProductIMEI["ProductID"].ToString().Trim()) == null)
                            //        continue;
                            //    else
                            //    {
                            //        if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(rInStockProductIMEI["ProductID"].ToString().Trim()).SubGroupID != intSubGroupID)
                            //            continue;
                            //    }

                            //}
                            DataRow rUnEvent = tblUnEventTable.NewRow();
                            rUnEvent.ItemArray = rInStockProductIMEI.ItemArray;
                            if (rUnEvent["CabinetNumber"] == null || (Convert.IsDBNull(rUnEvent["CabinetNumber"])) || Convert.ToString(rUnEvent["CabinetNumber"]).Trim() == string.Empty)
                            {
                                rUnEvent["CabinetNumber"] = intCabinet;
                            }
                            if (!Convert.IsDBNull(rUnEvent["Quantity"]))
                            {
                                rUnEvent["Col_IMEI"] = rUnEvent["IMEI"];
                            }
                            if (Convert.IsDBNull(rUnEvent["Different"]))
                            {
                                decimal decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]);
                                if (!Convert.IsDBNull(rUnEvent["Quantity_Inventory"]))
                                {
                                    decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]) - Convert.ToDecimal(rUnEvent["Quantity_Inventory"]);
                                }
                                rUnEvent["Different"] = decDifferent;
                            }
                            tblUnEventTable.Rows.Add(rUnEvent);
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Chưa duyệt
                    DataTable dtbFlex = (DataTable)flexDetail.DataSource;
                    if (dtbFlex.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtbFlex.Rows.Count; i++)
                        {
                            DataRow rowFlex = dtbFlex.Rows[i];
                            if (!Convert.ToBoolean(rowFlex["IsRequestIMEI"]))
                            {
                                DataRow rProduct = dtbInStockProduct.Rows.Find(rowFlex["ProductID"].ToString());
                                if (rProduct == null)
                                {
                                    rProduct = dtbInStockProduct.NewRow();
                                    rProduct["IsRequestIMEI"] = 0;
                                    rProduct["CabinetNumber"] = rowFlex["CabinetNumber"];
                                    rProduct["ProductID"] = rowFlex["ProductID"];
                                    rProduct["ProductName"] = rowFlex["ProductName"];
                                    rProduct["ProductInstockID"] = rowFlex["ProductInstockID"];
                                    rProduct["SubGroupID"] = rowFlex["SubGroupID"];
                                    rProduct["IMEI"] = string.Empty;
                                    rProduct["Col_IMEI"] = string.Empty;
                                    rProduct["IMEI_Inventory"] = string.Empty;
                                    rProduct["Quantity_Inventory"] = rowFlex["Quantity"];
                                    rProduct["Quantity"] = 0;
                                    if (Convert.ToBoolean(rowFlex["IsInStock"]))
                                        rProduct["Quantity"] = rowFlex["InStockQuantity"];
                                    rProduct["Different"] = Convert.ToDecimal(rProduct["Quantity"]) - Convert.ToDecimal(rProduct["Quantity_Inventory"]);
                                    dtbInStockProduct.Rows.Add(rProduct);
                                }
                            }
                            else
                            {
                                DataRow rIMEI = dtbInStockProductIMEI.Rows.Find(new object[] { rowFlex["ProductID"].ToString(), rowFlex["IMEI"].ToString() });
                                if (rIMEI == null && rowFlex["IMEI"].ToString() != strIMEICheck)
                                {
                                    strIMEICheck = rowFlex["IMEI"].ToString();
                                    rIMEI = dtbInStockProductIMEI.NewRow();
                                    rIMEI["IsRequestIMEI"] = 1;
                                    rIMEI["CabinetNumber"] = rowFlex["CabinetNumber"];
                                    rIMEI["ProductID"] = rowFlex["ProductID"];
                                    rIMEI["ProductName"] = rowFlex["ProductName"];
                                    rIMEI["ProductInstockID"] = rowFlex["ProductInstockID"];
                                    rIMEI["SubGroupID"] = rowFlex["SubGroupID"];
                                    rIMEI["IMEI"] = (--intKeyIMEI);
                                    rIMEI["Col_IMEI"] = string.Empty;
                                    rIMEI["IMEI_Inventory"] = rowFlex["IMEI"];
                                    rIMEI["Quantity_Inventory"] = rowFlex["Quantity"];
                                    rIMEI["Quantity"] = 0;
                                    if (Convert.ToBoolean(rowFlex["IsInStock"]))
                                        rIMEI["Quantity"] = rowFlex["InStockQuantity"];
                                    rIMEI["Different"] = Convert.ToDecimal(rIMEI["Quantity"]) - Convert.ToDecimal(rIMEI["Quantity_Inventory"]);
                                    DataRow[] dr = dtbInStockProductIMEI.Select("IMEI_Inventory = '" + rowFlex["IMEI"] + "'");
                                    if (dr.Length == 0)
                                        dtbInStockProductIMEI.Rows.Add(rIMEI);
                                }
                            }
                        }
                    }
                    if (dtbInStockProduct.Rows.Count > 0)
                    {
                        DataTable dtbStockProduct = dtbInStockProduct.Clone();
                        var lstData = from o in dtbInStockProduct.AsEnumerable()
                                      //from i in dtbProductCache.AsEnumerable()
                                      where Convert.ToInt32(o["SubGroupID"].ToString().Trim()) == intSubGroupID
                                      select o;
                        if (lstData.Count() > 0)
                            dtbStockProduct = lstData.CopyToDataTable();
                        for (int i = 0; i < dtbStockProduct.Rows.Count; i++)
                        {
                            DataRow rInStockProduct = dtbStockProduct.Rows[i];
                            //if (intSubGroupID > 0)
                            //{
                            //    if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(rInStockProduct["ProductID"].ToString().Trim()) == null)
                            //        continue;
                            //    else
                            //    {
                            //        if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(rInStockProduct["ProductID"].ToString().Trim()).SubGroupID != intSubGroupID)
                            //            continue;
                            //    }
                            //}
                            if (!Convert.ToBoolean(rInStockProduct["IsRequestIMEI"]))
                            {
                                DataRow rUnEvent = tblUnEventTable.NewRow();
                                rUnEvent.ItemArray = rInStockProduct.ItemArray;
                                if (rUnEvent["CabinetNumber"] == null || (Convert.IsDBNull(rUnEvent["CabinetNumber"])) || Convert.ToString(rUnEvent["CabinetNumber"]).Trim() == string.Empty)
                                {
                                    rUnEvent["CabinetNumber"] = intCabinet;
                                }
                                if (!Convert.IsDBNull(rUnEvent["Quantity"]))
                                {
                                    rUnEvent["Col_IMEI"] = rUnEvent["IMEI"];
                                }
                                if (Convert.IsDBNull(rUnEvent["Different"]))
                                {
                                    decimal decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]);
                                    if (!Convert.IsDBNull(rUnEvent["Quantity_Inventory"]))
                                    {
                                        decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]) - Convert.ToDecimal(rUnEvent["Quantity_Inventory"]);
                                    }
                                    else
                                    {
                                        DataRow[] drDetail = dtbData.Select("TRIM(ProductID)='" + Convert.ToString(rInStockProduct["ProductID"]).Trim() + "'");
                                        if (drDetail.Length > 0 && (!Convert.IsDBNull(drDetail[0]["Quantity"])))
                                        {
                                            rUnEvent["Quantity_Inventory"] = Convert.ToDecimal(drDetail[0]["Quantity"]);
                                            decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]) - Convert.ToDecimal(drDetail[0]["Quantity"]);
                                        }
                                    }
                                    rUnEvent["Different"] = decDifferent;
                                }
                                if (Convert.IsDBNull(rUnEvent["IMEI"]) || Convert.ToString(rUnEvent["IMEI"]).Trim() == string.Empty)
                                {
                                    rUnEvent["IMEI"] = (--intKeyIMEI);
                                }
                                tblUnEventTable.Rows.Add(rUnEvent);
                            }
                        }
                    }
                    if (dtbInStockProductIMEI.Rows.Count > 0)
                    {
                        DataTable dtbStockIMEI = dtbInStockProductIMEI.Clone();
                        var lstData = from o in dtbInStockProductIMEI.AsEnumerable()
                                      //from i in dtbProductCache.AsEnumerable()
                                      where Convert.ToInt32(o["SubGroupID"].ToString().Trim()) == intSubGroupID
                                      select o;
                        if (lstData.Count() > 0)
                            dtbStockIMEI = lstData.CopyToDataTable();
                        for (int i = 0; i < dtbStockIMEI.Rows.Count; i++)
                        {
                            DataRow rInStockProductIMEI = dtbStockIMEI.Rows[i];
                            //if (intSubGroupID > 0)
                            //{
                            //    if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(rInStockProductIMEI["ProductID"].ToString().Trim()) == null)
                            //        continue;
                            //    else
                            //    {
                            //        if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(rInStockProductIMEI["ProductID"].ToString().Trim()).SubGroupID != intSubGroupID)
                            //            continue;
                            //    }

                            //}
                            DataRow rUnEvent = tblUnEventTable.NewRow();
                            rUnEvent.ItemArray = rInStockProductIMEI.ItemArray;
                            if (rUnEvent["CabinetNumber"] == null || (Convert.IsDBNull(rUnEvent["CabinetNumber"])) || Convert.ToString(rUnEvent["CabinetNumber"]).Trim() == string.Empty)
                            {
                                rUnEvent["CabinetNumber"] = intCabinet;
                            }
                            if (!Convert.IsDBNull(rUnEvent["Quantity"]))
                            {
                                rUnEvent["Col_IMEI"] = rUnEvent["IMEI"];
                            }
                            if (Convert.IsDBNull(rUnEvent["Different"]))
                            {
                                decimal decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]);
                                if (!Convert.IsDBNull(rUnEvent["Quantity_Inventory"]))
                                {
                                    decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]) - Convert.ToDecimal(rUnEvent["Quantity_Inventory"]);
                                }
                                rUnEvent["Different"] = decDifferent;
                            }
                            tblUnEventTable.Rows.Add(rUnEvent);
                        }
                    }
                    #endregion
                }
                #endregion
            }
            else
            {
                #region
                if (dtbInStockProduct.Rows.Count > 0)
                {
                    DataTable dtbStockProduct = dtbInStockProduct.Clone();
                    var lstData = from o in dtbInStockProduct.AsEnumerable()
                                  //from i in dtbProductCache.AsEnumerable()
                                  where Convert.ToInt32(o["SubGroupID"].ToString().Trim()) == intSubGroupID
                                  select o;
                    if (lstData.Count() > 0)
                        dtbStockProduct = lstData.CopyToDataTable();

                    for (int i = 0; i < dtbStockProduct.Rows.Count; i++)
                    {
                        DataRow rInStockProduct = dtbStockProduct.Rows[i];
                        //if (intSubGroupID > 0)
                        //{

                        //    string strProductid1= rInStockProduct["ProductID"].ToString().Trim();
                        //    Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(rInStockProduct["ProductID"].ToString().Trim());
                        //    if (objProduct != null)
                        //    {
                        //        if (objProduct.SubGroupID != intSubGroupID)
                        //            continue;
                        //    }
                        //    else
                        //        continue;
                        //}
                        if (!Convert.ToBoolean(rInStockProduct["IsRequestIMEI"]))
                        {
                            DataRow rUnEvent = tblUnEventTable.NewRow();
                            rUnEvent.ItemArray = rInStockProduct.ItemArray;
                            if (rUnEvent["CabinetNumber"] == null || (Convert.IsDBNull(rUnEvent["CabinetNumber"])) || Convert.ToString(rUnEvent["CabinetNumber"]).Trim() == string.Empty)
                            {
                                rUnEvent["CabinetNumber"] = intCabinet;
                            }
                            if (!Convert.IsDBNull(rUnEvent["Quantity"]))
                            {
                                rUnEvent["Col_IMEI"] = rUnEvent["IMEI"];
                            }
                            if (Convert.IsDBNull(rUnEvent["Different"]))
                            {
                                decimal decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]);
                                if (!Convert.IsDBNull(rUnEvent["Quantity_Inventory"]))
                                {
                                    decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]) - Convert.ToDecimal(rUnEvent["Quantity_Inventory"]);
                                }
                                else
                                {
                                    DataRow[] drDetail = dtbData.Select("TRIM(ProductID)='" + Convert.ToString(rInStockProduct["ProductID"]).Trim() + "'");
                                    if (drDetail.Length > 0 && (!Convert.IsDBNull(drDetail[0]["Quantity"])))
                                    {
                                        rUnEvent["Quantity_Inventory"] = Convert.ToDecimal(drDetail[0]["Quantity"]);
                                        decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]) - Convert.ToDecimal(drDetail[0]["Quantity"]);
                                    }
                                }
                                rUnEvent["Different"] = decDifferent;
                            }
                            if (Convert.IsDBNull(rUnEvent["IMEI"]) || Convert.ToString(rUnEvent["IMEI"]).Trim() == string.Empty)
                            {
                                rUnEvent["IMEI"] = (--intKeyIMEI);
                            }
                            tblUnEventTable.Rows.Add(rUnEvent);
                        }
                    }
                }
                if (dtbInStockProductIMEI.Rows.Count > 0)
                {
                    DataTable dtbStockIMEI = dtbInStockProductIMEI.Clone();
                    var lstData = from o in dtbInStockProductIMEI.AsEnumerable()
                                  //from i in dtbProductCache.AsEnumerable()
                                  where Convert.ToInt32(o["SubGroupID"].ToString().Trim()) == intSubGroupID
                                  select o;
                    if (lstData.Count() > 0)
                        dtbStockIMEI = lstData.CopyToDataTable();
                    for (int i = 0; i < dtbStockIMEI.Rows.Count; i++)
                    {
                        DataRow rInStockProductIMEI = dtbStockIMEI.Rows[i];
                        //if (intSubGroupID > 0)
                        //{
                        //    if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(rInStockProductIMEI["ProductID"].ToString().Trim()) == null)
                        //        continue;
                        //    else
                        //    {
                        //        if (ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(rInStockProductIMEI["ProductID"].ToString().Trim()).SubGroupID != intSubGroupID)
                        //            continue;
                        //    }

                        //}
                        DataRow rUnEvent = tblUnEventTable.NewRow();
                        rUnEvent.ItemArray = rInStockProductIMEI.ItemArray;
                        if (rUnEvent["CabinetNumber"] == null || (Convert.IsDBNull(rUnEvent["CabinetNumber"])) || Convert.ToString(rUnEvent["CabinetNumber"]).Trim() == string.Empty)
                        {
                            rUnEvent["CabinetNumber"] = intCabinet;
                        }
                        if (!Convert.IsDBNull(rUnEvent["Quantity"]))
                        {
                            rUnEvent["Col_IMEI"] = rUnEvent["IMEI"];
                        }
                        else
                            rUnEvent["Quantity"] = 0;
                        if (Convert.IsDBNull(rUnEvent["Different"]))
                        {
                            decimal decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]);
                            if (!Convert.IsDBNull(rUnEvent["Quantity_Inventory"]))
                            {
                                decDifferent = Convert.ToDecimal(rUnEvent["Quantity"]) - Convert.ToDecimal(rUnEvent["Quantity_Inventory"]);
                            }
                            rUnEvent["Different"] = decDifferent;
                        }
                        tblUnEventTable.Rows.Add(rUnEvent);
                    }
                }
                #endregion
            }

            if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
            {
                frmShowUnEvent1.strInventoryID = objInventory.InventoryID;
                frmShowUnEvent1.dtmInventoryDate = (DateTime)objInventory.InventoryDate;
                frmShowUnEvent1.strStoreName = cboStore.Text;
                frmShowUnEvent1.strMainGroupName = cboMainGroup.Text;
                frmShowUnEvent1.intProductInstockID = Convert.ToInt32(cboProductStateID.SelectedValue);
                frmShowUnEvent1.intCreatedStoreID = objInventory.CreatedStoreID;
                frmShowUnEvent1.bolIsReviewed = objInventory.IsReviewed;
                frmShowUnEvent1.bolIsUpdateUnEvent = objInventory.IsUpdateUnEvent;
                frmShowUnEvent1.bolIsDeleted = objInventory.IsDeleted;
            }
            else
            {
                frmShowUnEvent1.strInventoryID = txtInventoryID.Text;
                frmShowUnEvent1.dtmInventoryDate = (DateTime)objInventoryTerm.InventoryDate;
                frmShowUnEvent1.strStoreName = cboStore.Text;
                frmShowUnEvent1.strMainGroupName = cboMainGroup.Text;
                frmShowUnEvent1.intProductInstockID = Convert.ToInt32(cboProductStateID.SelectedValue);
                frmShowUnEvent1.intCreatedStoreID = intStoreID;
                frmShowUnEvent1.bolIsReviewed = false;
                frmShowUnEvent1.bolIsUpdateUnEvent = false;
                frmShowUnEvent1.bolIsDeleted = false;
            }
            if (Convert.ToString(objInventoryType.UpdateUneventFunctionID).Trim() != string.Empty)
            {
                try
                {
                    if (Library.AppCore.SystemConfig.objSessionUser.IsPermission(Convert.ToString(objInventoryType.UpdateUneventFunctionID).Trim()))
                    {
                        frmShowUnEvent1.intIsPermitUpdateUnEvent = 1;
                    }
                    else
                    {
                        frmShowUnEvent1.intIsPermitUpdateUnEvent = 0;
                    }
                }
                catch { }
            }
            else
            {
                frmShowUnEvent1.intIsPermitUpdateUnEvent = 1;
            }
            frmShowUnEvent1.tblUnEventTable = tblUnEventTable;
            if (objInventory != null && Convert.ToString(objInventory.InventoryID).Trim() != string.Empty)
            {
                frmShowUnEvent1.bolIsSaveInventory = true;
            }
            frmShowUnEvent1.ShowDialog();
            if (frmShowUnEvent1.bolIsSave)
            {
                if (objInventory != null)
                {
                    objInventory.IsUpdateUnEvent = frmShowUnEvent1.bolIsUpdateUnEvent;
                }
            }

            if (objInventory != null)
            {
                if (objInventory.IsUpdateUnEvent)
                {
                    mnuItemExplainUnEvent.Enabled = true;
                }
                if (objInventory.IsExplainUnEvent)
                {
                    mnuEventHandlingUnEvent.Enabled = true;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if ((!btnSave.Enabled) || (!btnSave.Visible))
            {
                return;
            }
            btnSave.Enabled = false;
            try
            {
                dtbData.AcceptChanges();

                if (!CheckUpdate())
                {
                    btnSave.Enabled = true;
                    return;
                }

                if (objInventory == null || Convert.ToString(objInventory.InventoryID).Trim() == string.Empty)
                {
                    objInventory = new PLC.Inventory.WSInventory.Inventory();
                }
                objInventory.InventoryID = txtInventoryID.Text;
                objInventory.InventoryTermID = objInventoryTerm.InventoryTermID;
                objInventory.InventoryStoreID = intStoreID;
                objInventory.CreatedStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                objInventory.MainGroupID = intMainGroupID;
                objInventory.SubGroupID = intSubGroupID;
                objInventory.InventoryDate = (DateTime)objInventoryTerm.InventoryDate;
                objInventory.BeginInventoryTime = (DateTime)objInventoryTerm.BeginInventoryTime;
                objInventory.EndInventoryTime = (DateTime)objInventoryTerm.LockDataTime;
                objInventory.Content = Convert.ToString(txtContent.Text).Trim();
                objInventory.ProductStatusID = Convert.ToInt32(cboProductStateID.SelectedValue);
                objInventory.InventoryUser = strInventoryUser;
                objInventory.CreatedUser = Library.AppCore.SystemConfig.objSessionUser.UserName;

                int intCabinetNumber = Convert.ToInt32(cboCabinet.Text);
                dtbData.DefaultView.RowFilter = "IsEdit = 1";
                dtbData.DefaultView.Sort = "IsRequestIMEI ASC, ProductID ASC";
                DataTable dtbDetail = dtbData.DefaultView.ToTable();
                List<ERP.Inventory.PLC.Inventory.WSInventory.InventoryDetail> objInventoryDetailList = new List<PLC.Inventory.WSInventory.InventoryDetail>();
                for (int i = 0; i < dtbDetail.Rows.Count; i++)
                {
                    DataRow rDetail = dtbDetail.Rows[i];
                    string strProductID = Convert.ToString(rDetail["ProductID"]);
                    string strIMEI = string.Empty;
                    decimal decQuantity = 0;
                    decimal decInStockQuantity = 0;
                    int intIsInStock = 0;
                    if (!Convert.IsDBNull(rDetail["IMEI"])) strIMEI = Convert.ToString(rDetail["IMEI"]);
                    if (!Convert.IsDBNull(rDetail["Quantity"])) decQuantity = Convert.ToDecimal(rDetail["Quantity"]);
                    if (!Convert.IsDBNull(rDetail["InStockQuantity"])) decInStockQuantity = Convert.ToDecimal(rDetail["InStockQuantity"]);
                    if (!Convert.IsDBNull(rDetail["IsInStock"])) intIsInStock = Convert.ToInt32(rDetail["IsInStock"]);

                    int intCabinet = 0;
                    if (rDetail["CabinetNumber"] == null || (Convert.IsDBNull(rDetail["CabinetNumber"])) || Convert.ToString(rDetail["CabinetNumber"]).Trim() == string.Empty) intCabinet = intCabinetNumber;
                    else intCabinet = Convert.ToInt32(rDetail["CabinetNumber"]);

                    if (objInventoryDetailList.Count > 0 && objInventoryDetailList[objInventoryDetailList.Count - 1].ProductID == strProductID)
                    {
                        if (strIMEI != string.Empty)
                        {
                            ERP.Inventory.PLC.Inventory.WSInventory.InventoryDetailIMEI objInventoryDetailIMEI = new ERP.Inventory.PLC.Inventory.WSInventory.InventoryDetailIMEI();
                            objInventoryDetailIMEI.CabinetNumber = intCabinet;
                            objInventoryDetailIMEI.IMEI = strIMEI;
                            objInventoryDetailIMEI.InventoryDate = (DateTime)objInventoryTerm.InventoryDate;
                            objInventoryDetailIMEI.InventoryStoreID = intStoreID;
                            objInventoryDetailIMEI.CreatedStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                            objInventoryDetailIMEI.IsInStock = Convert.ToBoolean(intIsInStock);
                            objInventoryDetailIMEI.CreatedUser = objInventory.CreatedUser;

                            List<ERP.Inventory.PLC.Inventory.WSInventory.InventoryDetailIMEI> objInventoryDetailIMEIList = new List<ERP.Inventory.PLC.Inventory.WSInventory.InventoryDetailIMEI>();
                            if (objInventoryDetailList[objInventoryDetailList.Count - 1].InventoryDetailIMEIList.Length > 0)
                            {
                                objInventoryDetailIMEIList.AddRange(objInventoryDetailList[objInventoryDetailList.Count - 1].InventoryDetailIMEIList);
                            }
                            objInventoryDetailIMEIList.Add(objInventoryDetailIMEI);
                            objInventoryDetailList[objInventoryDetailList.Count - 1].InventoryDetailIMEIList = objInventoryDetailIMEIList.ToArray();
                            objInventoryDetailList[objInventoryDetailList.Count - 1].Quantity = Convert.ToDecimal(objInventoryDetailList[objInventoryDetailList.Count - 1].InventoryDetailIMEIList.Length);
                        }
                    }
                    else
                    {
                        ERP.Inventory.PLC.Inventory.WSInventory.InventoryDetail objInventoryDetail = new ERP.Inventory.PLC.Inventory.WSInventory.InventoryDetail();
                        objInventoryDetail.CabinetNumber = intCabinet;
                        objInventoryDetail.ProductID = strProductID;
                        objInventoryDetail.InventoryDate = (DateTime)objInventoryTerm.InventoryDate;
                        objInventoryDetail.InventoryStoreID = intStoreID;
                        objInventoryDetail.CreatedStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                        objInventoryDetail.Quantity = decQuantity;
                        objInventoryDetail.CreatedUser = objInventory.CreatedUser;

                        DataRow rInStockProduct = dtbInStockProduct.Rows.Find(strProductID);
                        if (rInStockProduct != null) decInStockQuantity = Convert.ToDecimal(rInStockProduct["Quantity"]);
                        objInventoryDetail.InStockQuantity = decInStockQuantity;
                        if (strIMEI != string.Empty)
                        {
                            ERP.Inventory.PLC.Inventory.WSInventory.InventoryDetailIMEI objInventoryDetailIMEI = new ERP.Inventory.PLC.Inventory.WSInventory.InventoryDetailIMEI();
                            objInventoryDetailIMEI.CabinetNumber = intCabinet;
                            objInventoryDetailIMEI.IMEI = strIMEI;
                            objInventoryDetailIMEI.InventoryDate = (DateTime)objInventoryTerm.InventoryDate;
                            objInventoryDetailIMEI.InventoryStoreID = intStoreID;
                            objInventoryDetailIMEI.CreatedStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                            objInventoryDetailIMEI.IsInStock = Convert.ToBoolean(intIsInStock);
                            objInventoryDetailIMEI.CreatedUser = objInventory.CreatedUser;

                            List<ERP.Inventory.PLC.Inventory.WSInventory.InventoryDetailIMEI> objInventoryDetailIMEIList = new List<ERP.Inventory.PLC.Inventory.WSInventory.InventoryDetailIMEI>();
                            objInventoryDetailIMEIList.Add(objInventoryDetailIMEI);
                            objInventoryDetail.InventoryDetailIMEIList = objInventoryDetailIMEIList.ToArray();
                            objInventoryDetail.Quantity = Convert.ToDecimal(objInventoryDetail.InventoryDetailIMEIList.Length);
                        }
                        objInventoryDetailList.Add(objInventoryDetail);
                    }
                }

                if (objInventoryType == null || objInventoryType.InventoryTypeID < 1)
                {
                    objPLCInventoryType.LoadInfo(ref objInventoryType, intInventoryTypeID);
                }
                objInventory.InventoryTypeID = objInventoryType.InventoryTypeID;
                objInventory.IsAutoReview = objInventoryType.IsAutoReview;

                string strRef_InventoryID = string.Empty;

                bool bolIsCreateNew = false;

                if (strInventoryID.Trim() == string.Empty)
                {
                    bolIsCreateNew = true;
                    if (!objPLCInventory.AddInventory(objInventory, objInventoryDetailList, (DataTable)grdUserReviewLevel.DataSource, ref strRef_InventoryID))
                    {
                        btnSave.Enabled = true;
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }

                    objInventory.InventoryID = strRef_InventoryID;
                    txtInventoryID.Text = strRef_InventoryID;
                    strInventoryID = strRef_InventoryID;
                    if (objInventory.IsAutoReview && (!objInventory.IsReviewed))
                    {
                        objInventory.IsReviewed = true;
                        chkIsReviewed.Checked = objInventory.IsReviewed;
                    }
                }
                else
                {
                    if (lstProductIMEI_Delete == null)
                    {
                        lstProductIMEI_Delete = new List<PLC.Inventory.WSInventory.InventoryDetailIMEI>();
                    }
                    if (!bolUpdate_IsAddReviewList)
                    {
                        if (!objPLCInventory.UpdateInventory(objInventory, objInventoryDetailList, lstProductIMEI_Delete))
                        {
                            btnSave.Enabled = true;
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            return;
                        }
                    }
                    else
                    {
                        if (!objPLCInventory.UpdateInventory_AddReviewList(objInventory, objInventoryDetailList, lstProductIMEI_Delete, (DataTable)grdUserReviewLevel.DataSource))
                        {
                            btnSave.Enabled = true;
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                            return;
                        }
                        bolUpdate_IsAddReviewList = false;
                        DataTable dtbTmp = (DataTable)grdUserReviewLevel.DataSource;
                        for (int i = dtbTmp.Rows.Count - 1; i > -1; i--)
                        {
                            if (!Convert.ToBoolean(dtbTmp.Rows[i]["IsSelect"]))
                            {
                                dtbTmp.Rows.RemoveAt(i);
                            }
                        }
                        dtbTmp.AcceptChanges();
                        //flexUserReviewLevel.Cols["IsSelect"].Visible = false;
                        //flexUserReviewLevel.Refresh();
                        grdUserReviewLevel.RefreshDataSource();
                        FormatGridUserReviewLevel(false);
                        if (objInventory.IsAutoReview && (!objInventory.IsReviewed))
                        {
                            objInventory.IsReviewed = true;
                            chkIsReviewed.Checked = objInventory.IsReviewed;
                        }
                    }
                }

                for (int i = 0; i < dtbData.Rows.Count; i++)
                {
                    dtbData.Rows[i]["IsInsert"] = 0;
                    dtbData.Rows[i]["IsEdit"] = 0;
                }

                dtbData.DefaultView.RowFilter = string.Empty;
                dtbData.DefaultView.Sort = string.Empty;
                if (flexDetail.Rows.Count > flexDetail.Rows.Fixed)
                    flexDetail.Select(1, flexDetail.Cols["ProductID"].Index, 1, flexDetail.Cols["CabinetNumber"].Index);

                txtUserName.Enabled = true;
                txtPassword.Enabled = true;
                cboReviewStatus.Enabled = true;
                btnReviewStatus.Enabled = true;
                if (bolIsCreateNew)
                {
                    DataTable dtbInventoryRvwLevel = objPLCInventory.GetInventoryReviewLevel(objInventory.InventoryID);
                    if (!dtbInventoryRvwLevel.Columns.Contains("ISSELECT"))
                    {
                        dtbInventoryRvwLevel.Columns.Add("ISSELECT", typeof(Decimal));
                        dtbInventoryRvwLevel.Columns["ISSELECT"].DefaultValue = Convert.ToDecimal(0);
                    }
                    grdUserReviewLevel.DataSource = dtbInventoryRvwLevel;
                    //CustomFlexUserRvwLvl();
                    //flexUserReviewLevel.Cols["IsSelect"].Visible = false;
                    FormatGridUserReviewLevel(false);
                }

                CheckReviewLevel();

                MessageBox.Show(this, "Cập nhật dữ liệu kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                mnuItemImportExcel.Enabled = false;
                flexDetail.AllowDelete = false;

                btnSave.Enabled = true;
            }
            catch
            {
                MessageBox.Show(this, "Lỗi lấy thông tin dữ liệu kiểm kê", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnSave.Enabled = true;
            }
        }

        private void btnReviewStatus_Click(object sender, EventArgs e)
        {
            int intInventoryCount = objPLCInventory.CountSimilarInventory(objInventoryTerm.InventoryTermID, intStoreID, intMainGroupID, Convert.ToInt32(cboProductStateID.SelectedValue), intSubGroupID);
            if (intInventoryCount > 1)
            {
                MessageBox.Show(this, "Tồn tại " + intInventoryCount.ToString() + " phiếu kiểm kê cùng kho " + Convert.ToString(cboStore.Text).Trim() + "\nVui lòng nối các phiếu này lại với nhau trước khi duyệt", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            btnReviewStatus.Enabled = false;
            string strUserName = string.Empty;
            string strFullName = string.Empty;
            if (Convert.ToString(txtUserName.Text).Trim() != string.Empty)
            {
                if (Convert.ToString(txtPassword.Text).Trim() == string.Empty)
                {
                    txtPassword.Focus();
                    MessageBox.Show(this, "Vui lòng nhập mật khẩu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    btnReviewStatus.Enabled = true;
                    return;
                }
                Library.AppCore.SessionUser objSessionUser = new Library.AppCore.SessionUser(Convert.ToString(txtUserName.Text).Trim(), Convert.ToString(txtPassword.Text).Trim(), true);
                objSessionUser.Login();
                if (!objSessionUser.IsLogin)
                {
                    txtUserName.Focus();
                    txtUserName.SelectAll();
                    txtPassword.Text = string.Empty;
                    MessageBox.Show(this, "Tên đăng nhập hoặc mật khẩu không đúng. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    btnReviewStatus.Enabled = true;
                    return;
                }
                strUserName = Convert.ToString(txtUserName.Text).Trim();
                strFullName = Convert.ToString(objSessionUser.FullName).Trim();
            }
            if (strUserName == string.Empty)
            {
                strUserName = Library.AppCore.SystemConfig.objSessionUser.UserName;
                strFullName = Convert.ToString(Library.AppCore.SystemConfig.objSessionUser.FullName).Trim();
            }

            DataTable dtbUserReviewLevel = grdUserReviewLevel.DataSource as DataTable;
            DataRow[] rUserReviewLevel = dtbUserReviewLevel.Select("TRIM(UserName) = '" + strUserName + "'");
            if (rUserReviewLevel.Length < 1)
            {
                MessageBox.Show(this, "Người dùng [" + strUserName + "] không nằm trong danh sách duyệt. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnReviewStatus.Enabled = true;
                return;
            }
            int intReviewLevelID = 0;
            bool bolIsCheckStorePermission = false;
            int intReviewType = 0;
            string strRvwFunctionID = string.Empty;
            int intIndexRvl = -1;
            for (int i = 0; i < dtbUserReviewLevel.Rows.Count; i++)
            {
                if (Convert.ToString(dtbUserReviewLevel.Rows[i]["UserName"]).Trim() == strUserName)
                {
                    if (Convert.ToBoolean(dtbUserReviewLevel.Rows[i]["IsReviewed"]))
                    {
                        continue;
                    }
                    intReviewLevelID = Convert.ToInt32(dtbUserReviewLevel.Rows[i]["ReviewLevelID"]);
                    intIndexRvl = i;
                    break;
                }
            }

            if (intReviewLevelID < 1)
            {
                MessageBox.Show(this, "Người dùng [" + strUserName + "] đã duyệt rồi. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnReviewStatus.Enabled = true;
                return;
            }

            for (int i = 0; i < dtbReviewLevel.Rows.Count; i++)
            {
                if (Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewLevelID"]) == intReviewLevelID)
                {
                    if (i + 1 < dtbReviewLevel.Rows.Count)
                    {
                        DataRow[] rIsHasUserRvwNextLvl = dtbUserReviewLevel.Select("ReviewStatus >0 AND ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i + 1]["ReviewLevelID"]));
                        if (rIsHasUserRvwNextLvl.Length > 0)
                        {
                            MessageBox.Show(this, "Mức duyệt [" + Convert.ToString(dtbReviewLevel.Rows[i + 1]["ReviewLevelName"]).Trim() + "] đã có người duyệt. Không thể cập nhật trạng thái duyệt mức hiện tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            btnReviewStatus.Enabled = true;
                            return;
                        }
                    }
                    if (i > 0)
                    {
                        DataRow[] rIsDeniedUserRvwLvl = dtbUserReviewLevel.Select("ReviewStatus = 3");
                        if (rIsDeniedUserRvwLvl.Length > 0)
                        {
                            MessageBox.Show(this, "Phiếu kiểm kê [" + objInventory.InventoryID + "] đã bị duyệt chối. Không thể cập nhật trạng thái duyệt!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            btnReviewStatus.Enabled = true;
                            return;
                        }
                        DataRow[] rIsReviewedUserRvwLvl = dtbUserReviewLevel.Select("IsReviewed = 1 AND ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i - 1]["ReviewLevelID"]));

                        if (rIsReviewedUserRvwLvl.Length < 1)
                        {
                            MessageBox.Show(this, "Mức duyệt [" + Convert.ToString(dtbReviewLevel.Rows[i - 1]["ReviewLevelName"]).Trim() + "] chưa được duyệt. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            btnReviewStatus.Enabled = true;
                            return;
                        }
                        else
                        {
                            if (Convert.ToInt32(dtbReviewLevel.Rows[i - 1]["ReviewType"]) == 0)
                            {
                                if (rIsReviewedUserRvwLvl.Length < (dtbUserReviewLevel.Select("ReviewLevelID = " + Convert.ToInt32(dtbReviewLevel.Rows[i - 1]["ReviewLevelID"])).Length))
                                {
                                    MessageBox.Show(this, "Mức duyệt [" + Convert.ToString(dtbReviewLevel.Rows[i - 1]["ReviewLevelName"]).Trim() + "] phải được tất cả duyệt. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    btnReviewStatus.Enabled = true;
                                    return;
                                }
                            }
                        }
                    }

                    intReviewType = Convert.ToInt32(dtbReviewLevel.Rows[i]["ReviewType"]);
                    if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["ReviewFunctionID"]))
                    {
                        strRvwFunctionID = Convert.ToString(dtbReviewLevel.Rows[i]["ReviewFunctionID"]).Trim();
                    }
                    if (!Convert.IsDBNull(dtbReviewLevel.Rows[i]["IsCheckStorePermission"]))
                    {
                        bolIsCheckStorePermission = Convert.ToBoolean(dtbReviewLevel.Rows[i]["IsCheckStorePermission"]);
                    }
                    break;
                }
            }

            ERP.MasterData.SYS.PLC.PLCUser objPLCUser = new MasterData.SYS.PLC.PLCUser();
            if (bolIsCheckStorePermission)
            {
                DataTable dtbStorePermission = objPLCUser.GetDataUser_Store(Convert.ToString(txtUserName.Text).Trim());
                DataRow[] rStorePermission = dtbStorePermission.Select("IsSelect > 0 AND StoreID = " + Convert.ToInt32(cboStore.SelectedValue));
                if (rStorePermission.Length < 1)
                {
                    MessageBox.Show(this, "Mã nhân viên kiểm kê [" + Convert.ToString(txtUserName.Text).Trim() + "] không có quyền trên kho [" + Convert.ToString(cboStore.Text).Trim() + "]. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            DataTable dtbMainGroupSource = null;
            ERP.MasterData.PLC.MD.PLCMainGroup objPLCMainGroup = new MasterData.PLC.MD.PLCMainGroup();
            objPLCMainGroup.GetMainGroup(ref dtbMainGroupSource, Convert.ToString(txtUserName.Text).Trim());//dtbMainGroupSource = objPLCUser.GetDataUserMainGroup(Convert.ToString(txtUserName.Text).Trim());
            DataRow[] rMainGroupPermission = dtbMainGroupSource.Select("HASRIGHT = 1 AND MAINGROUPID = " + Convert.ToInt32(cboMainGroup.SelectedValue));//dtbMainGroupSource.Select("IsSelect > 0 AND MainGroupID = " + Convert.ToInt32(cboMainGroup.SelectedValue));
            if (rMainGroupPermission.Length < 1)
            {
                MessageBox.Show(this, "Mã nhân viên kiểm kê [" + Convert.ToString(txtUserName.Text).Trim() + "] không có quyền trên ngành hàng [" + Convert.ToString(cboMainGroup.Text).Trim() + "]. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            ERP.Inventory.PLC.Inventory.WSInventory.Inventory_ReviewList objInventoryReview = new PLC.Inventory.WSInventory.Inventory_ReviewList();
            objInventoryReview.InventoryID = objInventory.InventoryID;
            objInventoryReview.ReviewLevelID = intReviewLevelID;
            objInventoryReview.UserName = strUserName;
            objInventoryReview.ReviewStatus = cboReviewStatus.SelectedIndex;
            objInventoryReview.InventoryDate = objInventory.InventoryDate;
            objInventoryReview.ReviewedDate = Library.AppCore.Globals.GetServerDateTime();

            bool bolIsReviewedInventory = false;
            if (intReviewLevelID == Convert.ToInt32(dtbReviewLevel.Rows[dtbReviewLevel.Rows.Count - 1]["ReviewLevelID"]))
            {
                DataTable dtbInventoryRvwLevel = null;
                if (strRvwFunctionID != string.Empty)
                {
                    dtbInventoryRvwLevel = objPLCInventory.GetInventoryReviewLevel(objInventory.InventoryID);
                }
                if (intReviewType == 0)
                {
                    DataRow[] rUserRvwLvl = dtbInventoryRvwLevel.Select("ReviewLevelID = " + intReviewLevelID);
                    DataRow[] rReviewed = dtbInventoryRvwLevel.Select("IsReviewed = 1 AND ReviewLevelID = " + intReviewLevelID);
                    if (objInventoryReview.ReviewStatus == 2)
                    {
                        if (rReviewed.Length == (rUserRvwLvl.Length - 1))
                        {
                            bolIsReviewedInventory = true;
                        }
                    }
                }
                else
                {
                    //DataRow[] rReviewed = dtbInventoryRvwLevel.Select("IsReviewed = 1 AND ReviewLevelID = " + intReviewLevelID);
                    //if (rReviewed.Length > 0)
                    if (objInventoryReview.ReviewStatus == 2)
                    {
                        bolIsReviewedInventory = true;
                    }
                    else
                    {
                        if (dtbUserReviewLevel.Select("ReviewLevelID = " + intReviewLevelID).Length == 1)
                        {
                            bolIsReviewedInventory = true;
                        }
                    }
                }
            }

            objPLCInventory.UpdateInventoryReviewStatus(objInventoryReview, bolIsReviewedInventory);

            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            else
            {
                if ((bolIsReviewedInventory) && objInventoryReview.ReviewStatus == 2)
                {
                    objInventory.IsReviewed = true;
                    chkIsReviewed.Checked = objInventory.IsReviewed;
                }
            }

            if (intIndexRvl > -1 && intIndexRvl < dtbUserReviewLevel.Rows.Count)
            {
                dtbUserReviewLevel.Rows[intIndexRvl]["ReviewStatus"] = objInventoryReview.ReviewStatus;
                dtbUserReviewLevel.Rows[intIndexRvl]["StatusName"] = cboReviewStatus.Text;
                if (objInventoryReview.ReviewStatus == 2)
                {
                    dtbUserReviewLevel.Rows[intIndexRvl]["IsReviewed"] = true;
                    dtbUserReviewLevel.Rows[intIndexRvl]["ReviewedDate"] = objInventoryReview.ReviewedDate;
                }
                else
                {
                    dtbUserReviewLevel.Rows[intIndexRvl]["IsReviewed"] = false;
                    dtbUserReviewLevel.Rows[intIndexRvl]["ReviewedDate"] = DBNull.Value;
                }
                dtbUserReviewLevel.Rows[intIndexRvl]["UserName"] = strUserName;
                dtbUserReviewLevel.Rows[intIndexRvl]["FullName"] = strFullName;
            }

            CheckReviewLevel();

            MessageBox.Show(this, "Cập nhật trạng thái duyệt phiếu kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            txtPassword.Text = string.Empty;
            btnReviewStatus.Enabled = true;
        }

        private void flexUserReviewLevel_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexUserReviewLevel.Rows.Count <= flexUserReviewLevel.Rows.Fixed)
            {
                return;
            }
            if (flexUserReviewLevel.RowSel < flexUserReviewLevel.Rows.Fixed)
            {
                return;
            }
            if (e.Col == flexUserReviewLevel.Cols["IsSelect"].Index)
            {
                if (Convert.ToInt32(flexUserReviewLevel[flexUserReviewLevel.RowSel, "ReviewType"]) == 0)
                {
                    DataTable tblTmp = (DataTable)flexUserReviewLevel.DataSource;
                    DataRow[] drIsSelectRvLvl = tblTmp.Select("IsSelect = 1 AND ReviewLevelID = " + Convert.ToInt32(flexUserReviewLevel[flexUserReviewLevel.RowSel, "ReviewLevelID"]));
                    if (drIsSelectRvLvl.Length < 1)
                    {
                        flexUserReviewLevel[flexUserReviewLevel.RowSel, "IsSelect"] = true;
                    }
                }
            }
        }

        private void mnuItemExplainUnEvent_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmExplainUnEvent frmExplainUnEvent1 = new frmExplainUnEvent();
            DataTable tblUnEventTable = dtbInStockProductIMEI.Clone();
            if (!tblUnEventTable.Columns.Contains("UnEventExplain"))
            {
                tblUnEventTable.Columns.Add("UnEventExplain", typeof(string));
            }
            if (!tblUnEventTable.Columns.Contains("ExplainNote"))
            {
                tblUnEventTable.Columns.Add("ExplainNote", typeof(string));
            }
            if (!tblUnEventTable.Columns.Contains("UnEventID"))
            {
                tblUnEventTable.Columns.Add("UnEventID", typeof(string));
            }

            tblUnEventTable.Columns["IsRequestIMEI"].SetOrdinal(tblUnEventTable.Columns["UnEventID"].Ordinal);

            DataTable dtbInventoryUnEvent = objPLCInventory.GetInventoryUnEvent(objInventory.InventoryID);
            if (dtbInventoryUnEvent != null && dtbInventoryUnEvent.Rows.Count > 0)
            {
                for (int i = 0; i < dtbInventoryUnEvent.Rows.Count; i++)
                {
                    DataRow r = dtbInventoryUnEvent.Rows[i];
                    DataRow rUnEvent = tblUnEventTable.NewRow();
                    rUnEvent["UnEventID"] = r["UnEventID"];
                    rUnEvent["CabinetNumber"] = r["CabinetNumber"];
                    rUnEvent["ProductID"] = r["ProductID"];
                    rUnEvent["ProductName"] = r["ProductName"];
                    rUnEvent["IMEI"] = r["StockIMEI"];
                    rUnEvent["Col_IMEI"] = r["StockIMEI"];
                    if (Convert.ToDecimal(r["StockQuantity"]) != 0)
                    {
                        rUnEvent["Quantity"] = r["StockQuantity"];
                    }
                    rUnEvent["IMEI_Inventory"] = r["InventoryIMEI"];
                    if (Convert.ToDecimal(r["InventoryQuantity"]) != 0)
                    {
                        rUnEvent["Quantity_Inventory"] = r["InventoryQuantity"];
                    }
                    rUnEvent["Different"] = r["UnEventQuantity"];
                    if (rUnEvent["IMEI"] == null || Convert.IsDBNull(rUnEvent["IMEI"]))
                    {
                        rUnEvent["IMEI"] = (--intKeyIMEI);
                    }
                    rUnEvent["UnEventExplain"] = r["UnEventExplain"];
                    rUnEvent["ExplainNote"] = r["ExplainNote"];
                    tblUnEventTable.Rows.Add(rUnEvent);
                }
            }
            frmExplainUnEvent1.strInventoryID = objInventory.InventoryID;
            frmExplainUnEvent1.dtmInventoryDate = (DateTime)objInventory.InventoryDate;
            frmExplainUnEvent1.intCreatedStoreID = objInventory.CreatedStoreID;
            frmExplainUnEvent1.bolIsReviewed = objInventory.IsReviewed;
            frmExplainUnEvent1.bolIsUpdateUnEvent = objInventory.IsUpdateUnEvent;
            frmExplainUnEvent1.bolIsExplainUnEvent = objInventory.IsExplainUnEvent;
            frmExplainUnEvent1.bolIsDeleted = objInventory.IsDeleted;
            frmExplainUnEvent1.tblUnEventTable = tblUnEventTable;
            if (Convert.ToString(objInventoryType.ExplainUneventFunctionID).Trim() != string.Empty)
            {
                try
                {
                    if (Library.AppCore.SystemConfig.objSessionUser.IsPermission(Convert.ToString(objInventoryType.ExplainUneventFunctionID).Trim()))
                    {
                        frmExplainUnEvent1.intIsPermitExplainUnEvent = 1;
                    }
                    else
                    {
                        frmExplainUnEvent1.intIsPermitExplainUnEvent = 0;
                    }
                }
                catch { }
            }
            else
            {
                frmExplainUnEvent1.intIsPermitExplainUnEvent = 1;
            }
            frmExplainUnEvent1.ShowDialog();
            if (frmExplainUnEvent1.bolIsSave)
            {
                if (objInventory != null)
                {
                    objInventory.IsExplainUnEvent = frmExplainUnEvent1.bolIsExplainUnEvent;
                }
            }

            if (objInventory != null)
            {
                if (objInventory.IsUpdateUnEvent)
                {
                    mnuItemExplainUnEvent.Enabled = true;
                }
                if (objInventory.IsExplainUnEvent)
                {
                    mnuEventHandlingUnEvent.Enabled = true;
                }
            }
        }

        private void mnuEventHandlingUnEvent_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string strInventoryProcessID = string.Empty;
            ERP.Inventory.PLC.Inventory.PLCInventoryProcess objPLCInventoryProcess = new PLC.Inventory.PLCInventoryProcess();
            strInventoryProcessID = objPLCInventoryProcess.CheckExistInventoryProcess(objInventory.InventoryID).Trim();
            if (strInventoryProcessID != string.Empty)
            {
                frmResolveQuantity frmResolveQuantity1 = new frmResolveQuantity();
                frmResolveQuantity1.InventoryProcessID = strInventoryProcessID;
                frmResolveQuantity1.ShowDialog();
            }
            else
            {
                int intIsPermitProcessInventory = -1;
                if (Convert.ToString(objInventoryType.ProcessFunctionID).Trim() != string.Empty)
                {
                    try
                    {
                        if (Library.AppCore.SystemConfig.objSessionUser.IsPermission(Convert.ToString(objInventoryType.ProcessFunctionID).Trim()))
                        {
                            intIsPermitProcessInventory = 1;
                        }
                        else
                        {
                            intIsPermitProcessInventory = 0;
                        }
                    }
                    catch { }
                }
                else
                {
                    intIsPermitProcessInventory = 1;
                }
                if (intIsPermitProcessInventory == 0)
                {
                    MessageBox.Show(this, "Bạn không có quyền xử lý chênh lệch kiểm kê. Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                frmHandlingUnEvent frmHandlingUnEvent1 = new frmHandlingUnEvent();
                frmHandlingUnEvent1.UnEventTable = objPLCInventory.GetInventoryHandleUnEvent(objInventory.InventoryID);
                frmHandlingUnEvent1.intInventoryTermID = objInventory.InventoryTermID;
                frmHandlingUnEvent1.strInventoryID = objInventory.InventoryID;
                frmHandlingUnEvent1.intStoreID = objInventory.InventoryStoreID;
                frmHandlingUnEvent1.dtmInventoryDate = (DateTime)objInventory.InventoryDate;
                frmHandlingUnEvent1.intMainGroupID = objInventory.MainGroupID;
                frmHandlingUnEvent1.intIsNew = Convert.ToInt32(objInventory.IsNew);
                frmHandlingUnEvent1.bolIsDeleted = objInventory.IsDeleted;
                frmHandlingUnEvent1.ShowDialog();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(txtInventoryID.Text).Trim() == string.Empty)
            {
                return;
            }

            string strInventoryProcessID = string.Empty;
            ERP.Inventory.PLC.Inventory.PLCInventoryProcess objPLCInventoryProcess = new PLC.Inventory.PLCInventoryProcess();
            strInventoryProcessID = objPLCInventoryProcess.CheckExistInventoryProcess(objInventory.InventoryID).Trim();
            if (strInventoryProcessID != string.Empty)
            {
                MessageBox.Show(this, "Phiếu kiểm kê này đã được xử lý kiểm kê. Vui lòng xóa phiếu yêu cầu xử lý kiểm kê " + strInventoryProcessID + " trước", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string strContentDelete = Convert.ToString(txtContentDelete.Text).Trim();
            if (strContentDelete == string.Empty)
            {
                txtContentDelete.Focus();
                MessageBox.Show(this, "Vui lòng nhập lý do hủy.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (!objPLCInventory.DeleteInventory(Convert.ToString(txtInventoryID.Text).Trim(), strContentDelete))
            {
                btnDelete.Enabled = true;
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
            }

            MessageBox.Show(this, "Hủy phiếu kiểm kê thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            btnDelete.Enabled = false;
            this.Close();
        }

        private void mnuItemDeleteProduct_Click(object sender, EventArgs e)
        {
            if (flexDetail.Rows.Count <= flexDetail.Rows.Fixed)
            {
                return;
            }
            if (flexDetail.RowSel < flexDetail.Rows.Fixed)
            {
                return;
            }
            bool bolIsDelete = false;
            if (dtbData.Rows[flexDetail.RowSel - flexDetail.Rows.Fixed]["IMEI"] != null && dtbData.Rows[flexDetail.RowSel - flexDetail.Rows.Fixed]["IMEI"] != DBNull.Value && Convert.ToString(dtbData.Rows[flexDetail.RowSel - flexDetail.Rows.Fixed]["IMEI"]).Trim() != string.Empty)
            {
                dtbData.AcceptChanges();
                string strIMEI_delete = Convert.ToString(dtbData.Rows[flexDetail.RowSel - flexDetail.Rows.Fixed]["IMEI"]).Trim();
                string strProductID_delete = Convert.ToString(dtbData.Rows[flexDetail.RowSel - flexDetail.Rows.Fixed]["ProductID"]).Trim();
                for (int i = dtbInStockProductIMEI.Rows.Count - 1; i > -1; i--)
                {
                    if ((!Convert.IsDBNull(dtbInStockProductIMEI.Rows[i]["IMEI_Inventory"]))
                        && Convert.ToString(dtbInStockProductIMEI.Rows[i]["IMEI_Inventory"]).Trim() == strIMEI_delete
                        && Convert.ToString(dtbInStockProductIMEI.Rows[i]["ProductID"]).Trim() == strProductID_delete)
                    {
                        if (Convert.ToString(dtbData.Rows[flexDetail.RowSel - flexDetail.Rows.Fixed]["INVENTORYDETAILID"]).Trim() != string.Empty)
                        {
                            ERP.Inventory.PLC.Inventory.WSInventory.InventoryDetailIMEI objProductIMEI_Delete = new PLC.Inventory.WSInventory.InventoryDetailIMEI();
                            objProductIMEI_Delete.InventoryDetailID = Convert.ToString(dtbData.Rows[flexDetail.RowSel - flexDetail.Rows.Fixed]["INVENTORYDETAILID"]).Trim();
                            objProductIMEI_Delete.IMEI = strIMEI_delete;
                            lstProductIMEI_Delete.Add(objProductIMEI_Delete);
                        }
                        if (dtbInStockProductIMEI.Rows[i]["IMEI"] != null && dtbInStockProductIMEI.Rows[i]["IMEI"] != DBNull.Value && Convert.ToString(dtbInStockProductIMEI.Rows[i]["IMEI"]).Trim() != string.Empty)
                        {
                            if (Convert.ToString(dtbInStockProductIMEI.Rows[i]["IMEI"]).IndexOf('-') >= 0)
                            {
                                dtbInStockProductIMEI.Rows.RemoveAt(i);
                            }
                            else
                            {
                                dtbInStockProductIMEI.Rows[i]["IMEI_Inventory"] = string.Empty;
                                dtbInStockProductIMEI.Rows[i]["Quantity_Inventory"] = System.DBNull.Value;
                                dtbInStockProductIMEI.Rows[i]["Different"] = System.DBNull.Value;
                            }
                        }
                        break;
                    }
                }
                dtbData.Rows.RemoveAt(flexDetail.RowSel - flexDetail.Rows.Fixed);
                bolIsDelete = true;

                SumQuantity();
            }
            else
            {
                dtbData.AcceptChanges();
                string strProductID_delete = Convert.ToString(dtbData.Rows[flexDetail.RowSel - flexDetail.Rows.Fixed]["ProductID"]).Trim();
                string strIMEI_delete = Convert.ToString(dtbData.Rows[flexDetail.RowSel - flexDetail.Rows.Fixed]["IMEI"]).Trim();
                //Kiểm tra và xóa trong dtbStockProduct
                for (int i = dtbInStockProduct.Rows.Count - 1; i > -1; i--)
                {
                    if ((!Convert.IsDBNull(dtbInStockProduct.Rows[i]["ProductID"]))
                        && Convert.ToString(dtbInStockProduct.Rows[i]["ProductID"]).Trim() == strProductID_delete)
                    {

                        if (dtbInStockProduct.Rows[i]["PRODUCTID"] != null && dtbInStockProduct.Rows[i]["PRODUCTID"] != DBNull.Value && Convert.ToString(dtbInStockProduct.Rows[i]["PRODUCTID"]).Trim() != string.Empty)
                        {
                            if (Convert.ToInt32(dtbInStockProduct.Rows[i]["ISREQUESTIMEI"]) == 1 && dtbInStockProduct.Rows[i]["IMEI"] != null && dtbInStockProduct.Rows[i]["IMEI"] != DBNull.Value && Convert.ToString(dtbInStockProduct.Rows[i]["IMEI"]).Trim() != string.Empty)
                            {
                                if (Convert.ToString(dtbInStockProduct.Rows[i]["IMEI"]).Trim() == strIMEI_delete)
                                {
                                    if (Convert.ToString(dtbInStockProduct.Rows[i]["IMEI"]).IndexOf('-') >= 0)
                                    {
                                        dtbInStockProduct.Rows.RemoveAt(i);
                                    }
                                    else
                                    {
                                        dtbInStockProduct.Rows[i]["IMEI_Inventory"] = string.Empty;
                                        dtbInStockProduct.Rows[i]["Quantity_Inventory"] = System.DBNull.Value;
                                        dtbInStockProduct.Rows[i]["Different"] = System.DBNull.Value;
                                    }
                                }
                            }
                            else
                                dtbInStockProduct.Rows.RemoveAt(i);
                        }
                        break;
                    }
                }
                dtbData.Rows.RemoveAt(flexDetail.RowSel - flexDetail.Rows.Fixed);
                bolIsDelete = true;

                SumQuantity();
            }
            if (!bolIsDelete)
            {
                dtbData.AcceptChanges();
                if (dtbData.Rows.Count > 0)
                {
                    if (dtbData.Rows[dtbData.Rows.Count - 1]["INVENTORYDETAILID"] == null || dtbData.Rows[dtbData.Rows.Count - 1]["INVENTORYDETAILID"] == DBNull.Value || Convert.ToString(dtbData.Rows[dtbData.Rows.Count - 1]["INVENTORYDETAILID"]).Trim() == string.Empty)
                    {
                        string strProduct_delete = Convert.ToString(dtbData.Rows[dtbData.Rows.Count - 1]["ProductID"]).Trim();
                        ERP.MasterData.PLC.MD.WSProduct.Product objProductDelete = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProduct_delete);
                        if (objProductDelete.IsRequestIMEI)
                        {
                            if (dtbData.Rows[dtbData.Rows.Count - 1]["IMEI"] == null || dtbData.Rows[dtbData.Rows.Count - 1]["IMEI"] == DBNull.Value || Convert.ToString(dtbData.Rows[dtbData.Rows.Count - 1]["IMEI"]).Trim() == string.Empty)
                            {
                                //DataRow[] rInStockIMEI = dtbInStockProductIMEI.Select("ProductID = '" + strProduct_delete + "'");
                                //if (rInStockIMEI.Length < 1)
                                {
                                    dtbData.Rows.RemoveAt(dtbData.Rows.Count - 1);

                                    SumQuantity();
                                }
                            }
                        }
                        else
                        {
                            try
                            {
                                if (Convert.ToDecimal(dtbData.Rows[flexDetail.RowSel - flexDetail.Rows.Fixed]["Quantity"]) == 0)
                                {
                                    dtbData.Rows.RemoveAt(flexDetail.RowSel - flexDetail.Rows.Fixed);

                                    SumQuantity();
                                }
                                else
                                {
                                    DataRow rRemove = dtbData.Rows[flexDetail.RowSel - flexDetail.Rows.Fixed];
                                    if (MessageBox.Show(this, "Sản phẩm " + rRemove["ProductName"].ToString() + " có số lượng kiểm kê là " + Convert.ToDecimal(rRemove["Quantity"]) + ". Bạn có chắc muốn xóa sản phẩm " + rRemove["ProductName"].ToString() + " không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                                    {
                                        return;
                                    }
                                    else
                                    {
                                        dtbData.Rows.Remove(rRemove);

                                        SumQuantity();
                                    }
                                }
                            }
                            catch { }
                        }
                    }
                }
            }

        }

        private void SumQuantity()
        {
            lblSumQuantity.Text = "0";
            lblSum.Text = "0";
            if (dtbData != null && dtbData.Rows.Count > 0)
            {
                lblSumQuantity.Text = Convert.ToDouble(dtbData.Compute("Sum(Quantity)", string.Empty)).ToString("#,##0");
                lblSum.Text = dtbData.Rows.Count.ToString("#,##0");
            }
        }

        private void mnuFlexDetail_Opening(object sender, CancelEventArgs e)
        {
            mnuItemDeleteProduct.Enabled = false;
            if (objInventory == null || strInventoryID.Trim() == string.Empty)
            {
                mnuItemDeleteProduct.Enabled = true;
            }
            else
            {
                if (!objInventory.IsUpdateUnEvent)
                {
                    if (intIsEdit == 1 && intIsRemoveProduct_ReviewLevel == 1)
                    {
                        mnuItemDeleteProduct.Enabled = true;
                    }
                }
            }
        }

        #endregion

        private void txtUserName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (Convert.ToString(txtUserName.Text).Trim() != string.Empty)
                {
                    txtPassword.Focus();
                }
            }
        }

        private void tmr_Tick(object sender, EventArgs e)
        {
            if ((!btnSave.Enabled) || (!btnSave.Visible))
            {
                return;
            }
            DataTable tblExcelExport = new DataTable("ExcelExport");
            {
                tblExcelExport.Columns.Add("Mã sản phẩm", typeof(string));
                tblExcelExport.Columns.Add("Tên sản phẩm", typeof(string));
                tblExcelExport.Columns.Add("IMEI", typeof(string));
                tblExcelExport.Columns.Add("Số lượng", typeof(decimal));
                tblExcelExport.Columns.Add("Tủ", typeof(int));
            }
            for (int i = flexDetail.Rows.Fixed; i < flexDetail.Rows.Count; i++)
            {
                DataRow rExcel = tblExcelExport.NewRow();
                rExcel[0] = flexDetail[i, "ProductID"];
                rExcel[1] = flexDetail[i, "ProductName"];
                rExcel[2] = flexDetail[i, "IMEI"];
                rExcel[3] = flexDetail[i, "Quantity"];
                rExcel[4] = flexDetail[i, "CabinetNumber"];
                tblExcelExport.Rows.Add(rExcel);
            }
            tblExcelExport.AcceptChanges();
            DateTime dtmServerDate = Library.AppCore.Globals.GetServerDateTime();
            string strFileName = dtmServerDate.Day.ToString("00") + dtmServerDate.Month.ToString("00") + dtmServerDate.Year.ToString("0000") + "Kho" + Convert.ToString(cboStore.SelectedValue) + "Nganhhang" + Convert.ToString(cboMainGroup.SelectedValue) + "Loaihang" + cboProductStateID.SelectedText + txtInventoryID.Text.Trim() + "User" + strInventoryUser;
            string strDirectoryPath = Application.StartupPath + "\\ImportInventory";
            if (!System.IO.Directory.Exists(strDirectoryPath))
                System.IO.Directory.CreateDirectory(strDirectoryPath);
            string strPath = strDirectoryPath + "\\" + strFileName + ".xls";
            C1.Win.C1FlexGrid.C1FlexGrid _flexTmp = new C1.Win.C1FlexGrid.C1FlexGrid();
            _flexTmp.Visible = false;
            _flexTmp.Cols.Fixed = 0;
            this.Controls.Add(_flexTmp);
            _flexTmp.DataSource = tblExcelExport;
            _flexTmp.SaveExcel(strPath, C1.Win.C1FlexGrid.FileFlags.IncludeFixedCells | C1.Win.C1FlexGrid.FileFlags.VisibleOnly | C1.Win.C1FlexGrid.FileFlags.SaveMergedRanges);
            this.Controls.Remove(_flexTmp);
        }



    }
}

