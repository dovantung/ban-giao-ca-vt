﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Inventory
{
    public partial class frmImportResult : Form
    {
        private DataTable tblImportResult = null;
        public bool bolIsError = false;

        public DataTable ImportResult
        {
            get { return tblImportResult; }
            set { tblImportResult = value; }
        }
        public frmImportResult()
        {
            InitializeComponent();
        }

        private void frmImportResult_Load(object sender, EventArgs e)
        {
            flexImportResult.DataSource = tblImportResult;
            CustomFlex();
        }
        private void CustomFlex()
        {
            if (tblImportResult == null)
            {
                return;
            }

            if (!tblImportResult.Columns.Contains("IsOK"))
            {
                tblImportResult.Columns.Add("IsOK", typeof(bool));
            }

            for (int i = 0; i < flexImportResult.Cols.Count; i++)
            {
                flexImportResult.Cols[i].Visible = false;
                flexImportResult.Cols[i].AllowEditing = false;
                flexImportResult.Cols[i].AllowDragging = false;
            }

            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnVisible(flexImportResult, true, "CabinetNumber,ProductID,ProductName,IMEI,Quantity,ErrorContent,IsOK");

            for (int i = 0; i < tblImportResult.Rows.Count; i++)
            {
                if (!Convert.IsDBNull(tblImportResult.Rows[i]["IsError"]))
                {
                    if (Convert.ToInt32(tblImportResult.Rows[i]["IsError"]) == 1)
                    {
                        tblImportResult.Rows[i]["IsOK"] = false;
                    }
                    else
                    {
                        tblImportResult.Rows[i]["IsOK"] = true;
                    }
                }
            }
            Library.AppCore.LoadControls.C1FlexGridObject.SetColumnCaption(flexImportResult, "CabinetNumber,Tủ,ProductID,Mã sản phẩm,ProductName,Tên sản phẩm,Quantity,Số lượng,ErrorContent,Kết quả,IsOK,OK");
            flexImportResult.Cols["IsOK"].DataType = typeof(bool);
            for (int i = flexImportResult.Rows.Fixed; i < flexImportResult.Rows.Count; i++)
            {
                int intIsError = Convert.ToInt32(flexImportResult[i, "IsError"]);
                if (intIsError > 0)
                {
                    bolIsError = true;
                    if (intIsError == 1)
                    {
                        Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexImportResult, i, Color.Pink);
                    }
                    else
                    {
                        if (intIsError == 2)
                        {
                            Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexImportResult, i, Color.Orange);
                        }
                        else if (intIsError == 3)
                        {
                            Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexImportResult, i, Color.Yellow);
                        }
                    }
                }
            }
            flexImportResult.AutoSizeCols();
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexImportResult);
        }
    }
}