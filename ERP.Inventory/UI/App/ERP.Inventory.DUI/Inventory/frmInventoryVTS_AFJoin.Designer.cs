﻿namespace ERP.Inventory.DUI.Inventory
{
    partial class frmInventoryVTS_AFJoin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInventoryVTS_AFJoin));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.grdCtlProducts = new DevExpress.XtraGrid.GridControl();
            this.mnuFlexDetail = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemExportTemplateExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.grdViewProducts = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repTextImei = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtContentDelete = new System.Windows.Forms.TextBox();
            this.chkIsParent = new System.Windows.Forms.CheckBox();
            this.chkIsConnected = new System.Windows.Forms.CheckBox();
            this.cboInventoryTermID = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.cboSubGroupIDList = new Library.AppControl.ComboBoxControl.ComboBoxSubGroup();
            this.cboStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.cboMainGroupIDList = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.label1 = new System.Windows.Forms.Label();
            this.cboProductStateID = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtInventoryUser = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.chkIsDeleted = new System.Windows.Forms.CheckBox();
            this.chkIsReviewed = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpBeginInventoryTime = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpInventoryDate = new System.Windows.Forms.DateTimePicker();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.txtInventoryID = new System.Windows.Forms.TextBox();
            this.lblSum = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblSumQuantity = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.flexUserReviewLevel = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grdUserReviewLevel = new DevExpress.XtraGrid.GridControl();
            this.grdViewUserReviewLevel = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lnkUsersReview = new System.Windows.Forms.LinkLabel();
            this.lblReview = new System.Windows.Forms.Label();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cboItemUnEvent = new DevExpress.XtraEditors.DropDownButton();
            this.popupItemUnEvent = new DevExpress.XtraBars.PopupMenu(this.components);
            this.mnuItemViewUnEvent = new DevExpress.XtraBars.BarButtonItem();
            this.mnuItemExplainUnEvent = new DevExpress.XtraBars.BarButtonItem();
            this.mnuEventHandlingUnEvent = new DevExpress.XtraBars.BarButtonItem();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.dropDownBtnApprove = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.btnApprove = new DevExpress.XtraBars.BarButtonItem();
            this.btnUnApprove = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCtlProducts)).BeginInit();
            this.mnuFlexDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextImei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexUserReviewLevel)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdUserReviewLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewUserReviewLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupItemUnEvent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1189, 546);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.grdCtlProducts);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.lblSum);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.lblSumQuantity);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1181, 513);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thông tin kiểm kê";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // grdCtlProducts
            // 
            this.grdCtlProducts.ContextMenuStrip = this.mnuFlexDetail;
            this.grdCtlProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCtlProducts.Location = new System.Drawing.Point(3, 172);
            this.grdCtlProducts.MainView = this.grdViewProducts;
            this.grdCtlProducts.Name = "grdCtlProducts";
            this.grdCtlProducts.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repTextImei,
            this.repQuantity,
            this.repNote});
            this.grdCtlProducts.Size = new System.Drawing.Size(1175, 338);
            this.grdCtlProducts.TabIndex = 81;
            this.grdCtlProducts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewProducts});
            // 
            // mnuFlexDetail
            // 
            this.mnuFlexDetail.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mnuFlexDetail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemExportTemplateExcel});
            this.mnuFlexDetail.Name = "mnuFlexDetail";
            this.mnuFlexDetail.Size = new System.Drawing.Size(151, 30);
            this.mnuFlexDetail.Opening += new System.ComponentModel.CancelEventHandler(this.mnuFlexDetail_Opening);
            // 
            // mnuItemExportTemplateExcel
            // 
            this.mnuItemExportTemplateExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportTemplateExcel.Name = "mnuItemExportTemplateExcel";
            this.mnuItemExportTemplateExcel.Size = new System.Drawing.Size(150, 26);
            this.mnuItemExportTemplateExcel.Text = "Xuất excel";
            this.mnuItemExportTemplateExcel.Click += new System.EventHandler(this.mnuItemExportTemplateExcel_Click);
            // 
            // grdViewProducts
            // 
            this.grdViewProducts.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewProducts.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewProducts.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.Preview.Options.UseFont = true;
            this.grdViewProducts.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewProducts.Appearance.Row.Options.UseFont = true;
            this.grdViewProducts.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewProducts.GridControl = this.grdCtlProducts;
            this.grdViewProducts.Name = "grdViewProducts";
            this.grdViewProducts.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewProducts.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewProducts.OptionsNavigation.UseTabKey = false;
            this.grdViewProducts.OptionsView.ShowAutoFilterRow = true;
            this.grdViewProducts.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewProducts.OptionsView.ShowFooter = true;
            this.grdViewProducts.OptionsView.ShowGroupPanel = false;
            this.grdViewProducts.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grdViewProducts_ShowingEditor);
            // 
            // repTextImei
            // 
            this.repTextImei.AutoHeight = false;
            this.repTextImei.MaxLength = 50;
            this.repTextImei.Name = "repTextImei";
            // 
            // repQuantity
            // 
            this.repQuantity.AutoHeight = false;
            this.repQuantity.DisplayFormat.FormatString = "N0";
            this.repQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repQuantity.Mask.EditMask = "N0";
            this.repQuantity.Mask.UseMaskAsDisplayFormat = true;
            this.repQuantity.MaxValue = new decimal(new int[] {
            1215752191,
            23,
            0,
            0});
            this.repQuantity.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repQuantity.Name = "repQuantity";
            // 
            // repNote
            // 
            this.repNote.AutoHeight = false;
            this.repNote.MaxLength = 2000;
            this.repNote.Name = "repNote";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtContentDelete);
            this.panel1.Controls.Add(this.chkIsParent);
            this.panel1.Controls.Add(this.chkIsConnected);
            this.panel1.Controls.Add(this.cboInventoryTermID);
            this.panel1.Controls.Add(this.cboSubGroupIDList);
            this.panel1.Controls.Add(this.cboStoreID);
            this.panel1.Controls.Add(this.cboMainGroupIDList);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cboProductStateID);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtInventoryUser);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.chkIsDeleted);
            this.panel1.Controls.Add(this.chkIsReviewed);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.dtpBeginInventoryTime);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.dtpInventoryDate);
            this.panel1.Controls.Add(this.txtContent);
            this.panel1.Controls.Add(this.txtInventoryID);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1175, 169);
            this.panel1.TabIndex = 80;
            // 
            // txtContentDelete
            // 
            this.txtContentDelete.Location = new System.Drawing.Point(817, 90);
            this.txtContentDelete.MaxLength = 2000;
            this.txtContentDelete.Multiline = true;
            this.txtContentDelete.Name = "txtContentDelete";
            this.txtContentDelete.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContentDelete.Size = new System.Drawing.Size(233, 69);
            this.txtContentDelete.TabIndex = 86;
            // 
            // chkIsParent
            // 
            this.chkIsParent.AutoSize = true;
            this.chkIsParent.Enabled = false;
            this.chkIsParent.Location = new System.Drawing.Point(817, 65);
            this.chkIsParent.Name = "chkIsParent";
            this.chkIsParent.Size = new System.Drawing.Size(191, 24);
            this.chkIsParent.TabIndex = 85;
            this.chkIsParent.Text = "Là phiếu KK tổng hợp";
            this.chkIsParent.UseVisualStyleBackColor = true;
            // 
            // chkIsConnected
            // 
            this.chkIsConnected.AutoSize = true;
            this.chkIsConnected.Enabled = false;
            this.chkIsConnected.Location = new System.Drawing.Point(651, 134);
            this.chkIsConnected.Name = "chkIsConnected";
            this.chkIsConnected.Size = new System.Drawing.Size(124, 24);
            this.chkIsConnected.TabIndex = 84;
            this.chkIsConnected.Text = "Đã nối phiếu";
            this.chkIsConnected.UseVisualStyleBackColor = true;
            this.chkIsConnected.Visible = false;
            // 
            // cboInventoryTermID
            // 
            this.cboInventoryTermID.Enabled = false;
            this.cboInventoryTermID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInventoryTermID.Location = new System.Drawing.Point(409, 9);
            this.cboInventoryTermID.Margin = new System.Windows.Forms.Padding(0);
            this.cboInventoryTermID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInventoryTermID.Name = "cboInventoryTermID";
            this.cboInventoryTermID.Size = new System.Drawing.Size(223, 24);
            this.cboInventoryTermID.TabIndex = 81;
            // 
            // cboSubGroupIDList
            // 
            this.cboSubGroupIDList.Enabled = false;
            this.cboSubGroupIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSubGroupIDList.Location = new System.Drawing.Point(731, 36);
            this.cboSubGroupIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboSubGroupIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboSubGroupIDList.Name = "cboSubGroupIDList";
            this.cboSubGroupIDList.Size = new System.Drawing.Size(319, 24);
            this.cboSubGroupIDList.TabIndex = 83;
            // 
            // cboStoreID
            // 
            this.cboStoreID.Enabled = false;
            this.cboStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreID.Location = new System.Drawing.Point(92, 36);
            this.cboStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreID.Name = "cboStoreID";
            this.cboStoreID.Size = new System.Drawing.Size(223, 24);
            this.cboStoreID.TabIndex = 81;
            // 
            // cboMainGroupIDList
            // 
            this.cboMainGroupIDList.Enabled = false;
            this.cboMainGroupIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroupIDList.Location = new System.Drawing.Point(409, 36);
            this.cboMainGroupIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroupIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroupIDList.Name = "cboMainGroupIDList";
            this.cboMainGroupIDList.Size = new System.Drawing.Size(223, 24);
            this.cboMainGroupIDList.TabIndex = 82;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 20);
            this.label1.TabIndex = 52;
            this.label1.Text = "Mã phiếu:";
            // 
            // cboProductStateID
            // 
            this.cboProductStateID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProductStateID.DropDownWidth = 250;
            this.cboProductStateID.Enabled = false;
            this.cboProductStateID.Location = new System.Drawing.Point(92, 63);
            this.cboProductStateID.Name = "cboProductStateID";
            this.cboProductStateID.Size = new System.Drawing.Size(223, 28);
            this.cboProductStateID.TabIndex = 79;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(648, 40);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(99, 20);
            this.label17.TabIndex = 74;
            this.label17.Text = "Nhóm hàng:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(323, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 20);
            this.label9.TabIndex = 61;
            this.label9.Text = "NV kiểm kê:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 20);
            this.label7.TabIndex = 59;
            this.label7.Text = "Loại hàng:";
            // 
            // txtInventoryUser
            // 
            this.txtInventoryUser.BackColor = System.Drawing.SystemColors.Info;
            this.txtInventoryUser.Location = new System.Drawing.Point(409, 63);
            this.txtInventoryUser.Name = "txtInventoryUser";
            this.txtInventoryUser.ReadOnly = true;
            this.txtInventoryUser.Size = new System.Drawing.Size(223, 26);
            this.txtInventoryUser.TabIndex = 75;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 20);
            this.label10.TabIndex = 62;
            this.label10.Text = "Nội dung:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(832, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 20);
            this.label6.TabIndex = 58;
            this.label6.Text = "TG bắt đầu:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(648, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 20);
            this.label5.TabIndex = 57;
            this.label5.Text = "Ngày KK:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(648, 65);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(89, 20);
            this.label15.TabIndex = 57;
            this.label15.Text = "Trạng thái:";
            // 
            // chkIsDeleted
            // 
            this.chkIsDeleted.AutoSize = true;
            this.chkIsDeleted.Enabled = false;
            this.chkIsDeleted.Location = new System.Drawing.Point(731, 90);
            this.chkIsDeleted.Name = "chkIsDeleted";
            this.chkIsDeleted.Size = new System.Drawing.Size(83, 24);
            this.chkIsDeleted.TabIndex = 70;
            this.chkIsDeleted.Text = "Đã hủy";
            this.chkIsDeleted.UseVisualStyleBackColor = true;
            // 
            // chkIsReviewed
            // 
            this.chkIsReviewed.AutoSize = true;
            this.chkIsReviewed.Enabled = false;
            this.chkIsReviewed.Location = new System.Drawing.Point(731, 64);
            this.chkIsReviewed.Name = "chkIsReviewed";
            this.chkIsReviewed.Size = new System.Drawing.Size(97, 24);
            this.chkIsReviewed.TabIndex = 70;
            this.chkIsReviewed.Text = "Đã duyệt";
            this.chkIsReviewed.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(323, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 20);
            this.label4.TabIndex = 56;
            this.label4.Text = "Kỳ kiểm kê:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(323, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 20);
            this.label3.TabIndex = 55;
            this.label3.Text = "Ngành hàng:";
            // 
            // dtpBeginInventoryTime
            // 
            this.dtpBeginInventoryTime.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpBeginInventoryTime.Enabled = false;
            this.dtpBeginInventoryTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBeginInventoryTime.Location = new System.Drawing.Point(915, 11);
            this.dtpBeginInventoryTime.Name = "dtpBeginInventoryTime";
            this.dtpBeginInventoryTime.Size = new System.Drawing.Size(135, 26);
            this.dtpBeginInventoryTime.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 20);
            this.label2.TabIndex = 53;
            this.label2.Text = "Kho kiểm kê:";
            // 
            // dtpInventoryDate
            // 
            this.dtpInventoryDate.CustomFormat = "dd/MM/yyyy";
            this.dtpInventoryDate.Enabled = false;
            this.dtpInventoryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInventoryDate.Location = new System.Drawing.Point(731, 11);
            this.dtpInventoryDate.Name = "dtpInventoryDate";
            this.dtpInventoryDate.Size = new System.Drawing.Size(101, 26);
            this.dtpInventoryDate.TabIndex = 4;
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(92, 91);
            this.txtContent.MaxLength = 2000;
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContent.Size = new System.Drawing.Size(540, 68);
            this.txtContent.TabIndex = 10;
            // 
            // txtInventoryID
            // 
            this.txtInventoryID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInventoryID.Location = new System.Drawing.Point(92, 11);
            this.txtInventoryID.Name = "txtInventoryID";
            this.txtInventoryID.ReadOnly = true;
            this.txtInventoryID.Size = new System.Drawing.Size(223, 26);
            this.txtInventoryID.TabIndex = 0;
            // 
            // lblSum
            // 
            this.lblSum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSum.AutoSize = true;
            this.lblSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSum.ForeColor = System.Drawing.Color.Blue;
            this.lblSum.Location = new System.Drawing.Point(132, 495);
            this.lblSum.Name = "lblSum";
            this.lblSum.Size = new System.Drawing.Size(19, 20);
            this.lblSum.TabIndex = 77;
            this.lblSum.Text = "0";
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(183, 495);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(85, 20);
            this.label19.TabIndex = 76;
            this.label19.Text = "Tổng SL:";
            // 
            // lblSumQuantity
            // 
            this.lblSumQuantity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSumQuantity.AutoSize = true;
            this.lblSumQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSumQuantity.ForeColor = System.Drawing.Color.Blue;
            this.lblSumQuantity.Location = new System.Drawing.Point(260, 517);
            this.lblSumQuantity.Name = "lblSumQuantity";
            this.lblSumQuantity.Size = new System.Drawing.Size(19, 20);
            this.lblSumQuantity.TabIndex = 72;
            this.lblSumQuantity.Text = "0";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 517);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(143, 20);
            this.label16.TabIndex = 71;
            this.label16.Text = "Tổng sản phẩm:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.flexUserReviewLevel);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.lnkUsersReview);
            this.tabPage2.Controls.Add(this.lblReview);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1181, 513);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Duyệt";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Enter += new System.EventHandler(this.tabPage2_Enter);
            // 
            // flexUserReviewLevel
            // 
            this.flexUserReviewLevel.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexUserReviewLevel.AllowFreezing = C1.Win.C1FlexGrid.AllowFreezingEnum.Both;
            this.flexUserReviewLevel.AllowMerging = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexUserReviewLevel.AllowMergingFixed = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexUserReviewLevel.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flexUserReviewLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flexUserReviewLevel.AutoClipboard = true;
            this.flexUserReviewLevel.ColumnInfo = "10,1,0,0,0,125,Columns:0{Width:20;}\t";
            this.flexUserReviewLevel.Location = new System.Drawing.Point(893, 549);
            this.flexUserReviewLevel.Name = "flexUserReviewLevel";
            this.flexUserReviewLevel.Rows.Count = 1;
            this.flexUserReviewLevel.Rows.DefaultSize = 25;
            this.flexUserReviewLevel.Size = new System.Drawing.Size(280, 0);
            this.flexUserReviewLevel.StyleInfo = resources.GetString("flexUserReviewLevel.StyleInfo");
            this.flexUserReviewLevel.TabIndex = 57;
            this.flexUserReviewLevel.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.grdUserReviewLevel);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1175, 507);
            this.groupBox1.TabIndex = 71;
            this.groupBox1.TabStop = false;
            // 
            // grdUserReviewLevel
            // 
            this.grdUserReviewLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdUserReviewLevel.Location = new System.Drawing.Point(3, 22);
            this.grdUserReviewLevel.MainView = this.grdViewUserReviewLevel;
            this.grdUserReviewLevel.Name = "grdUserReviewLevel";
            this.grdUserReviewLevel.Size = new System.Drawing.Size(1169, 482);
            this.grdUserReviewLevel.TabIndex = 34;
            this.grdUserReviewLevel.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewUserReviewLevel});
            // 
            // grdViewUserReviewLevel
            // 
            this.grdViewUserReviewLevel.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewUserReviewLevel.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewUserReviewLevel.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewUserReviewLevel.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewUserReviewLevel.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewUserReviewLevel.Appearance.Preview.Options.UseFont = true;
            this.grdViewUserReviewLevel.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewUserReviewLevel.Appearance.Row.Options.UseFont = true;
            this.grdViewUserReviewLevel.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewUserReviewLevel.GridControl = this.grdUserReviewLevel;
            this.grdViewUserReviewLevel.Name = "grdViewUserReviewLevel";
            this.grdViewUserReviewLevel.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewUserReviewLevel.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewUserReviewLevel.OptionsNavigation.UseTabKey = false;
            this.grdViewUserReviewLevel.OptionsView.ShowAutoFilterRow = true;
            this.grdViewUserReviewLevel.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewUserReviewLevel.OptionsView.ShowFooter = true;
            this.grdViewUserReviewLevel.OptionsView.ShowGroupPanel = false;
            // 
            // lnkUsersReview
            // 
            this.lnkUsersReview.AutoSize = true;
            this.lnkUsersReview.Enabled = false;
            this.lnkUsersReview.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkUsersReview.Location = new System.Drawing.Point(300, 274);
            this.lnkUsersReview.Name = "lnkUsersReview";
            this.lnkUsersReview.Size = new System.Drawing.Size(180, 20);
            this.lnkUsersReview.TabIndex = 12;
            this.lnkUsersReview.TabStop = true;
            this.lnkUsersReview.Text = "Danh sách người duyệt";
            this.lnkUsersReview.Visible = false;
            // 
            // lblReview
            // 
            this.lblReview.AutoSize = true;
            this.lblReview.Enabled = false;
            this.lblReview.Location = new System.Drawing.Point(215, 250);
            this.lblReview.Name = "lblReview";
            this.lblReview.Size = new System.Drawing.Size(91, 20);
            this.lblReview.TabIndex = 63;
            this.lblReview.Text = "Mức duyệt:";
            this.lblReview.Visible = false;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.cboItemUnEvent);
            this.panelControl1.Controls.Add(this.dropDownBtnApprove);
            this.panelControl1.Controls.Add(this.btnDelete);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 546);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1189, 34);
            this.panelControl1.TabIndex = 82;
            // 
            // cboItemUnEvent
            // 
            this.cboItemUnEvent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboItemUnEvent.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.cboItemUnEvent.Appearance.Options.UseFont = true;
            this.cboItemUnEvent.DropDownControl = this.popupItemUnEvent;
            this.cboItemUnEvent.Location = new System.Drawing.Point(766, 4);
            this.cboItemUnEvent.LookAndFeel.SkinName = "Caramel";
            this.cboItemUnEvent.Name = "cboItemUnEvent";
            this.cboItemUnEvent.Size = new System.Drawing.Size(114, 25);
            this.cboItemUnEvent.TabIndex = 71;
            this.cboItemUnEvent.Text = "Chênh lệch";
            // 
            // popupItemUnEvent
            // 
            this.popupItemUnEvent.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuItemViewUnEvent),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuItemExplainUnEvent),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuEventHandlingUnEvent)});
            this.popupItemUnEvent.Manager = this.barManager2;
            this.popupItemUnEvent.Name = "popupItemUnEvent";
            // 
            // mnuItemViewUnEvent
            // 
            this.mnuItemViewUnEvent.Caption = "Xem chênh lệch";
            this.mnuItemViewUnEvent.Id = 0;
            this.mnuItemViewUnEvent.Name = "mnuItemViewUnEvent";
            this.mnuItemViewUnEvent.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuItemViewUnEvent_ItemClick);
            // 
            // mnuItemExplainUnEvent
            // 
            this.mnuItemExplainUnEvent.Caption = "Giải trình chênh lệch";
            this.mnuItemExplainUnEvent.Enabled = false;
            this.mnuItemExplainUnEvent.Id = 1;
            this.mnuItemExplainUnEvent.Name = "mnuItemExplainUnEvent";
            this.mnuItemExplainUnEvent.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuItemExplainUnEvent_ItemClick);
            // 
            // mnuEventHandlingUnEvent
            // 
            this.mnuEventHandlingUnEvent.Caption = "Xử lý chênh lệch";
            this.mnuEventHandlingUnEvent.Enabled = false;
            this.mnuEventHandlingUnEvent.Id = 2;
            this.mnuEventHandlingUnEvent.Name = "mnuEventHandlingUnEvent";
            this.mnuEventHandlingUnEvent.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuEventHandlingUnEvent_ItemClick);
            // 
            // barManager2
            // 
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.mnuItemViewUnEvent,
            this.mnuItemExplainUnEvent,
            this.mnuEventHandlingUnEvent});
            this.barManager2.MaxItemId = 3;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(1189, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 580);
            this.barDockControl2.Size = new System.Drawing.Size(1189, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 580);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1189, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 580);
            // 
            // dropDownBtnApprove
            // 
            this.dropDownBtnApprove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownBtnApprove.Appearance.BackColor = System.Drawing.SystemColors.ControlLight;
            this.dropDownBtnApprove.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropDownBtnApprove.Appearance.Options.UseBackColor = true;
            this.dropDownBtnApprove.Appearance.Options.UseFont = true;
            this.dropDownBtnApprove.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.dropDownBtnApprove.DropDownControl = this.popupMenu1;
            this.dropDownBtnApprove.Image = ((System.Drawing.Image)(resources.GetObject("dropDownBtnApprove.Image")));
            this.dropDownBtnApprove.Location = new System.Drawing.Point(886, 5);
            this.dropDownBtnApprove.Name = "dropDownBtnApprove";
            this.dropDownBtnApprove.Size = new System.Drawing.Size(92, 23);
            this.dropDownBtnApprove.TabIndex = 69;
            this.dropDownBtnApprove.Text = "Duyệt";
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnApprove),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnUnApprove)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // btnApprove
            // 
            this.btnApprove.Caption = "Đồng ý";
            this.btnApprove.Id = 0;
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnApprove_ItemClick);
            // 
            // btnUnApprove
            // 
            this.btnUnApprove.Caption = "Từ chối";
            this.btnUnApprove.Id = 1;
            this.btnUnApprove.Name = "btnUnApprove";
            this.btnUnApprove.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnUnApprove_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnApprove,
            this.btnUnApprove});
            this.barManager1.MaxItemId = 4;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1189, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 580);
            this.barDockControlBottom.Size = new System.Drawing.Size(1189, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 580);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1189, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 580);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Enabled = false;
            this.btnDelete.Image = global::ERP.Inventory.DUI.Properties.Resources.delete_cancel;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(1084, 4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(94, 25);
            this.btnDelete.TabIndex = 16;
            this.btnDelete.Text = "      Hủy";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(982, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(96, 25);
            this.btnSave.TabIndex = 19;
            this.btnSave.Text = "       Cập nhật";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmInventoryVTS_AFJoin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1189, 580);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmInventoryVTS_AFJoin";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phiếu xác nhận kiểm kê";
            this.Load += new System.EventHandler(this.frmInventoryVTS_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCtlProducts)).EndInit();
            this.mnuFlexDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextImei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repNote)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexUserReviewLevel)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdUserReviewLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewUserReviewLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupItemUnEvent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ComboBox cboProductStateID;
        private System.Windows.Forms.Label lblSum;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtInventoryUser;
        private System.Windows.Forms.Label lblSumQuantity;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chkIsReviewed;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DateTimePicker dtpInventoryDate;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.TextBox txtInventoryID;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpBeginInventoryTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabPage tabPage2;
        private C1.Win.C1FlexGrid.C1FlexGrid flexUserReviewLevel;
        private DevExpress.XtraGrid.GridControl grdUserReviewLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewUserReviewLevel;
        private System.Windows.Forms.LinkLabel lnkUsersReview;
        private System.Windows.Forms.Label lblReview;
        private System.Windows.Forms.Panel panel1;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStoreID;
        private Library.AppControl.ComboBoxControl.ComboBoxSubGroup cboSubGroupIDList;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroupIDList;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboInventoryTermID;
        private DevExpress.XtraGrid.GridControl grdCtlProducts;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewProducts;
        private System.Windows.Forms.ContextMenuStrip mnuFlexDetail;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportTemplateExcel;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.CheckBox chkIsParent;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.DropDownButton dropDownBtnApprove;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTextImei;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repQuantity;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repNote;
        private DevExpress.XtraEditors.DropDownButton cboItemUnEvent;
        private System.Windows.Forms.TextBox txtContentDelete;
        private System.Windows.Forms.CheckBox chkIsConnected;
        private System.Windows.Forms.CheckBox chkIsDeleted;
        private DevExpress.XtraBars.PopupMenu popupItemUnEvent;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnApprove;
        private DevExpress.XtraBars.BarButtonItem btnUnApprove;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarButtonItem mnuItemViewUnEvent;
        private DevExpress.XtraBars.BarButtonItem mnuItemExplainUnEvent;
        private DevExpress.XtraBars.BarButtonItem mnuEventHandlingUnEvent;
    }
}