﻿namespace ERP.Inventory.DUI.Output
{
    partial class frmOutputVoucher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOutputVoucher));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtTaxAddress = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtOutputNote = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDiscountReasonName = new System.Windows.Forms.TextBox();
            this.txtPayableType = new System.Windows.Forms.TextBox();
            this.txtOutputStore = new System.Windows.Forms.TextBox();
            this.txtCustomerID = new System.Windows.Forms.TextBox();
            this.dteDueDate = new System.Windows.Forms.DateTimePicker();
            this.dtInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtCustomerTaxID = new System.Windows.Forms.TextBox();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.ctrlOutputUser = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.txtOutputContent = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dtOutputDate = new System.Windows.Forms.DateTimePicker();
            this.txtOutputVoucherID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtInVoiceID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOrderID = new System.Windows.Forms.TextBox();
            this.txtInvoiceSymbol = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDiscountOutput = new System.Windows.Forms.TextBox();
            this.lblTaxID = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lnkCustomer = new System.Windows.Forms.LinkLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.grdOutputVoucherDetail = new DevExpress.XtraGrid.GridControl();
            this.grvOutputVoucherDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOutputType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndWarrantyDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSalePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountBFVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmountVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVATPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductStatusName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdOutputVoucherDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvOutputVoucherDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtTaxAddress);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtOutputNote);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtDiscountReasonName);
            this.groupBox1.Controls.Add(this.txtPayableType);
            this.groupBox1.Controls.Add(this.txtOutputStore);
            this.groupBox1.Controls.Add(this.txtCustomerID);
            this.groupBox1.Controls.Add(this.dteDueDate);
            this.groupBox1.Controls.Add(this.dtInvoiceDate);
            this.groupBox1.Controls.Add(this.lblAddress);
            this.groupBox1.Controls.Add(this.txtAddress);
            this.groupBox1.Controls.Add(this.txtPhone);
            this.groupBox1.Controls.Add(this.txtCustomerTaxID);
            this.groupBox1.Controls.Add(this.txtCustomerName);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.ctrlOutputUser);
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Controls.Add(this.txtOutputContent);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.dtOutputDate);
            this.groupBox1.Controls.Add(this.txtOutputVoucherID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtInVoiceID);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtOrderID);
            this.groupBox1.Controls.Add(this.txtInvoiceSymbol);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtDiscountOutput);
            this.groupBox1.Controls.Add(this.lblTaxID);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lblPhone);
            this.groupBox1.Controls.Add(this.lnkCustomer);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(3, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1075, 227);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // txtTaxAddress
            // 
            this.txtTaxAddress.Location = new System.Drawing.Point(108, 124);
            this.txtTaxAddress.MaxLength = 400;
            this.txtTaxAddress.Name = "txtTaxAddress";
            this.txtTaxAddress.Size = new System.Drawing.Size(390, 22);
            this.txtTaxAddress.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 16);
            this.label8.TabIndex = 24;
            this.label8.Text = "Địa chỉ hóa đơn:";
            // 
            // txtOutputNote
            // 
            this.txtOutputNote.Location = new System.Drawing.Point(108, 179);
            this.txtOutputNote.MaxLength = 200;
            this.txtOutputNote.Multiline = true;
            this.txtOutputNote.Name = "txtOutputNote";
            this.txtOutputNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOutputNote.Size = new System.Drawing.Size(390, 42);
            this.txtOutputNote.TabIndex = 37;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 182);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 16);
            this.label6.TabIndex = 36;
            this.label6.Text = "Ghi chú:";
            // 
            // txtDiscountReasonName
            // 
            this.txtDiscountReasonName.BackColor = System.Drawing.SystemColors.Info;
            this.txtDiscountReasonName.Location = new System.Drawing.Point(679, 42);
            this.txtDiscountReasonName.MaxLength = 15;
            this.txtDiscountReasonName.Name = "txtDiscountReasonName";
            this.txtDiscountReasonName.ReadOnly = true;
            this.txtDiscountReasonName.Size = new System.Drawing.Size(189, 22);
            this.txtDiscountReasonName.TabIndex = 11;
            // 
            // txtPayableType
            // 
            this.txtPayableType.BackColor = System.Drawing.SystemColors.Info;
            this.txtPayableType.Location = new System.Drawing.Point(438, 42);
            this.txtPayableType.MaxLength = 15;
            this.txtPayableType.Name = "txtPayableType";
            this.txtPayableType.ReadOnly = true;
            this.txtPayableType.Size = new System.Drawing.Size(165, 22);
            this.txtPayableType.TabIndex = 9;
            // 
            // txtOutputStore
            // 
            this.txtOutputStore.BackColor = System.Drawing.SystemColors.Info;
            this.txtOutputStore.Location = new System.Drawing.Point(679, 17);
            this.txtOutputStore.MaxLength = 15;
            this.txtOutputStore.Name = "txtOutputStore";
            this.txtOutputStore.ReadOnly = true;
            this.txtOutputStore.Size = new System.Drawing.Size(291, 22);
            this.txtOutputStore.TabIndex = 5;
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.BackColor = System.Drawing.SystemColors.Info;
            this.txtCustomerID.Location = new System.Drawing.Point(438, 70);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.ReadOnly = true;
            this.txtCustomerID.Size = new System.Drawing.Size(60, 22);
            this.txtCustomerID.TabIndex = 16;
            this.txtCustomerID.TabStop = false;
            // 
            // dteDueDate
            // 
            this.dteDueDate.CustomFormat = "dd/MM/yyyy";
            this.dteDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteDueDate.Location = new System.Drawing.Point(869, 152);
            this.dteDueDate.Name = "dteDueDate";
            this.dteDueDate.Size = new System.Drawing.Size(101, 22);
            this.dteDueDate.TabIndex = 35;
            // 
            // dtInvoiceDate
            // 
            this.dtInvoiceDate.CustomFormat = "dd/MM/yyyy";
            this.dtInvoiceDate.Enabled = false;
            this.dtInvoiceDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtInvoiceDate.Location = new System.Drawing.Point(869, 124);
            this.dtInvoiceDate.Name = "dtInvoiceDate";
            this.dtInvoiceDate.Size = new System.Drawing.Size(101, 22);
            this.dtInvoiceDate.TabIndex = 29;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(5, 101);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(51, 16);
            this.lblAddress.TabIndex = 18;
            this.lblAddress.Text = "Địa chỉ:";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(108, 98);
            this.txtAddress.MaxLength = 400;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(390, 22);
            this.txtAddress.TabIndex = 19;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(582, 98);
            this.txtPhone.MaxLength = 15;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(205, 22);
            this.txtPhone.TabIndex = 21;
            // 
            // txtCustomerTaxID
            // 
            this.txtCustomerTaxID.Location = new System.Drawing.Point(869, 98);
            this.txtCustomerTaxID.MaxLength = 20;
            this.txtCustomerTaxID.Name = "txtCustomerTaxID";
            this.txtCustomerTaxID.Size = new System.Drawing.Size(101, 22);
            this.txtCustomerTaxID.TabIndex = 23;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Location = new System.Drawing.Point(499, 70);
            this.txtCustomerName.MaxLength = 200;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(471, 22);
            this.txtCustomerName.TabIndex = 17;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 43);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(98, 16);
            this.label15.TabIndex = 6;
            this.label15.Text = "Nhân viên xuất:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ctrlOutputUser
            // 
            this.ctrlOutputUser.Enabled = false;
            this.ctrlOutputUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrlOutputUser.IsOnlyShowRealStaff = false;
            this.ctrlOutputUser.IsValidate = true;
            this.ctrlOutputUser.Location = new System.Drawing.Point(108, 42);
            this.ctrlOutputUser.Margin = new System.Windows.Forms.Padding(4);
            this.ctrlOutputUser.MaximumSize = new System.Drawing.Size(400, 22);
            this.ctrlOutputUser.MinimumSize = new System.Drawing.Size(0, 22);
            this.ctrlOutputUser.Name = "ctrlOutputUser";
            this.ctrlOutputUser.Size = new System.Drawing.Size(226, 22);
            this.ctrlOutputUser.TabIndex = 7;
            this.ctrlOutputUser.UserName = "";
            this.ctrlOutputUser.WorkingPositionID = 0;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(869, 179);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(101, 25);
            this.btnUpdate.TabIndex = 38;
            this.btnUpdate.Text = "   Cập &nhật";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // txtOutputContent
            // 
            this.txtOutputContent.BackColor = System.Drawing.SystemColors.Window;
            this.txtOutputContent.Location = new System.Drawing.Point(108, 152);
            this.txtOutputContent.MaxLength = 990;
            this.txtOutputContent.Multiline = true;
            this.txtOutputContent.Name = "txtOutputContent";
            this.txtOutputContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOutputContent.Size = new System.Drawing.Size(390, 22);
            this.txtOutputContent.TabIndex = 31;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 155);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 16);
            this.label10.TabIndex = 30;
            this.label10.Text = "Nội dung xuất:";
            // 
            // dtOutputDate
            // 
            this.dtOutputDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtOutputDate.Enabled = false;
            this.dtOutputDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtOutputDate.Location = new System.Drawing.Point(438, 14);
            this.dtOutputDate.Name = "dtOutputDate";
            this.dtOutputDate.Size = new System.Drawing.Size(165, 22);
            this.dtOutputDate.TabIndex = 3;
            // 
            // txtOutputVoucherID
            // 
            this.txtOutputVoucherID.BackColor = System.Drawing.SystemColors.Info;
            this.txtOutputVoucherID.Location = new System.Drawing.Point(108, 14);
            this.txtOutputVoucherID.Name = "txtOutputVoucherID";
            this.txtOutputVoucherID.ReadOnly = true;
            this.txtOutputVoucherID.Size = new System.Drawing.Size(226, 22);
            this.txtOutputVoucherID.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã phiếu xuất:";
            // 
            // txtInVoiceID
            // 
            this.txtInVoiceID.BackColor = System.Drawing.SystemColors.Info;
            this.txtInVoiceID.Location = new System.Drawing.Point(582, 124);
            this.txtInVoiceID.MaxLength = 15;
            this.txtInVoiceID.Name = "txtInVoiceID";
            this.txtInVoiceID.ReadOnly = true;
            this.txtInVoiceID.Size = new System.Drawing.Size(205, 22);
            this.txtInVoiceID.TabIndex = 27;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 16);
            this.label3.TabIndex = 13;
            this.label3.Text = "Mã đơn hàng:";
            // 
            // txtOrderID
            // 
            this.txtOrderID.BackColor = System.Drawing.SystemColors.Info;
            this.txtOrderID.Location = new System.Drawing.Point(108, 70);
            this.txtOrderID.MaxLength = 20;
            this.txtOrderID.Name = "txtOrderID";
            this.txtOrderID.ReadOnly = true;
            this.txtOrderID.Size = new System.Drawing.Size(226, 22);
            this.txtOrderID.TabIndex = 14;
            // 
            // txtInvoiceSymbol
            // 
            this.txtInvoiceSymbol.BackColor = System.Drawing.SystemColors.Info;
            this.txtInvoiceSymbol.Location = new System.Drawing.Point(582, 152);
            this.txtInvoiceSymbol.MaxLength = 20;
            this.txtInvoiceSymbol.Name = "txtInvoiceSymbol";
            this.txtInvoiceSymbol.ReadOnly = true;
            this.txtInvoiceSymbol.Size = new System.Drawing.Size(205, 22);
            this.txtInvoiceSymbol.TabIndex = 33;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(508, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 16);
            this.label4.TabIndex = 32;
            this.label4.Text = "Ký hiệu:";
            // 
            // txtDiscountOutput
            // 
            this.txtDiscountOutput.BackColor = System.Drawing.SystemColors.Info;
            this.txtDiscountOutput.Location = new System.Drawing.Point(869, 42);
            this.txtDiscountOutput.Name = "txtDiscountOutput";
            this.txtDiscountOutput.ReadOnly = true;
            this.txtDiscountOutput.Size = new System.Drawing.Size(101, 22);
            this.txtDiscountOutput.TabIndex = 12;
            // 
            // lblTaxID
            // 
            this.lblTaxID.AutoSize = true;
            this.lblTaxID.Location = new System.Drawing.Point(792, 101);
            this.lblTaxID.Name = "lblTaxID";
            this.lblTaxID.Size = new System.Drawing.Size(76, 16);
            this.lblTaxID.TabIndex = 22;
            this.lblTaxID.Text = "Mã số thuế:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(792, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 16);
            this.label7.TabIndex = 34;
            this.label7.Text = "Hạn TT:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(612, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 16);
            this.label13.TabIndex = 4;
            this.label13.Text = "Kho xuất:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(792, 127);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(66, 16);
            this.label18.TabIndex = 28;
            this.label18.Text = "Ngày HĐ:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(612, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 16);
            this.label9.TabIndex = 10;
            this.label9.Text = "Giảm giá:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(508, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 16);
            this.label2.TabIndex = 26;
            this.label2.Text = "Số HĐ:";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(508, 101);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(70, 16);
            this.lblPhone.TabIndex = 20;
            this.lblPhone.Text = "Điện thoại:";
            // 
            // lnkCustomer
            // 
            this.lnkCustomer.AutoSize = true;
            this.lnkCustomer.Location = new System.Drawing.Point(344, 73);
            this.lnkCustomer.Name = "lnkCustomer";
            this.lnkCustomer.Size = new System.Drawing.Size(81, 16);
            this.lnkCustomer.TabIndex = 15;
            this.lnkCustomer.TabStop = true;
            this.lnkCustomer.Text = "Khách hàng:";
            this.lnkCustomer.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkCustomer_LinkClicked);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(344, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "Ngày xuất:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(344, 45);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 16);
            this.label11.TabIndex = 8;
            this.label11.Text = "HT thanh toán:";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "icon-16-download.png");
            // 
            // grdOutputVoucherDetail
            // 
            this.grdOutputVoucherDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdOutputVoucherDetail.Location = new System.Drawing.Point(2, 25);
            this.grdOutputVoucherDetail.MainView = this.grvOutputVoucherDetail;
            this.grdOutputVoucherDetail.Name = "grdOutputVoucherDetail";
            this.grdOutputVoucherDetail.Size = new System.Drawing.Size(1073, 256);
            this.grdOutputVoucherDetail.TabIndex = 0;
            this.grdOutputVoucherDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvOutputVoucherDetail});
            // 
            // grvOutputVoucherDetail
            // 
            this.grvOutputVoucherDetail.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvOutputVoucherDetail.Appearance.FocusedRow.Options.UseFont = true;
            this.grvOutputVoucherDetail.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvOutputVoucherDetail.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Blue;
            this.grvOutputVoucherDetail.Appearance.FooterPanel.Options.UseFont = true;
            this.grvOutputVoucherDetail.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grvOutputVoucherDetail.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvOutputVoucherDetail.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvOutputVoucherDetail.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvOutputVoucherDetail.Appearance.Row.Options.UseFont = true;
            this.grvOutputVoucherDetail.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.grvOutputVoucherDetail.ColumnPanelRowHeight = 40;
            this.grvOutputVoucherDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOutputType,
            this.colProductID,
            this.colProductName,
            this.colIMEI,
            this.colEndWarrantyDate,
            this.colSalePrice,
            this.colTotalAmountBFVAT,
            this.colVAT,
            this.colTotalAmountVAT,
            this.colQuantity,
            this.colAmount,
            this.colVATPercent,
            this.colProductStatusName});
            this.grvOutputVoucherDetail.GridControl = this.grdOutputVoucherDetail;
            this.grvOutputVoucherDetail.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Amount", this.colAmount, "###,###,###,###.####")});
            this.grvOutputVoucherDetail.Name = "grvOutputVoucherDetail";
            this.grvOutputVoucherDetail.OptionsBehavior.Editable = false;
            this.grvOutputVoucherDetail.OptionsView.AllowCellMerge = true;
            this.grvOutputVoucherDetail.OptionsView.ColumnAutoWidth = false;
            this.grvOutputVoucherDetail.OptionsView.ShowFooter = true;
            this.grvOutputVoucherDetail.OptionsView.ShowGroupPanel = false;
            this.grvOutputVoucherDetail.OptionsView.ShowIndicator = false;
            this.grvOutputVoucherDetail.CellMerge += new DevExpress.XtraGrid.Views.Grid.CellMergeEventHandler(this.grvOutputVoucherDetail_CellMerge);
            this.grvOutputVoucherDetail.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.grvOutputVoucherDetail_CustomSummaryCalculate);
            // 
            // colOutputType
            // 
            this.colOutputType.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutputType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutputType.Caption = "Hình thức xuất";
            this.colOutputType.FieldName = "OUTPUTTYPENAME";
            this.colOutputType.Name = "colOutputType";
            this.colOutputType.OptionsColumn.AllowEdit = false;
            this.colOutputType.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colOutputType.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colOutputType.OptionsColumn.ReadOnly = true;
            this.colOutputType.Visible = true;
            this.colOutputType.VisibleIndex = 0;
            this.colOutputType.Width = 139;
            // 
            // colProductID
            // 
            this.colProductID.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductID.Caption = "Mã sản phẩm";
            this.colProductID.FieldName = "PRODUCTID";
            this.colProductID.Name = "colProductID";
            this.colProductID.OptionsColumn.AllowEdit = false;
            this.colProductID.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colProductID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colProductID.OptionsColumn.ReadOnly = true;
            this.colProductID.Visible = true;
            this.colProductID.VisibleIndex = 1;
            this.colProductID.Width = 110;
            // 
            // colProductName
            // 
            this.colProductName.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductName.Caption = "Tên sản phẩm";
            this.colProductName.FieldName = "PRODUCTNAME";
            this.colProductName.Name = "colProductName";
            this.colProductName.OptionsColumn.AllowEdit = false;
            this.colProductName.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colProductName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colProductName.OptionsColumn.ReadOnly = true;
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 2;
            this.colProductName.Width = 220;
            // 
            // colIMEI
            // 
            this.colIMEI.AppearanceHeader.Options.UseTextOptions = true;
            this.colIMEI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIMEI.Caption = "IMEI";
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsColumn.AllowEdit = false;
            this.colIMEI.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colIMEI.OptionsColumn.ReadOnly = true;
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 3;
            this.colIMEI.Width = 140;
            // 
            // colEndWarrantyDate
            // 
            this.colEndWarrantyDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colEndWarrantyDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEndWarrantyDate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEndWarrantyDate.Caption = "Ngày bảo hành";
            this.colEndWarrantyDate.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colEndWarrantyDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colEndWarrantyDate.FieldName = "ENDWARRANTYDATE";
            this.colEndWarrantyDate.Name = "colEndWarrantyDate";
            this.colEndWarrantyDate.OptionsColumn.AllowEdit = false;
            this.colEndWarrantyDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colEndWarrantyDate.OptionsColumn.ReadOnly = true;
            this.colEndWarrantyDate.Visible = true;
            this.colEndWarrantyDate.VisibleIndex = 4;
            this.colEndWarrantyDate.Width = 100;
            // 
            // colSalePrice
            // 
            this.colSalePrice.AppearanceCell.Options.UseTextOptions = true;
            this.colSalePrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colSalePrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colSalePrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSalePrice.Caption = "Giá bán";
            this.colSalePrice.DisplayFormat.FormatString = "N0";
            this.colSalePrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSalePrice.FieldName = "SALEPRICE";
            this.colSalePrice.Name = "colSalePrice";
            this.colSalePrice.OptionsColumn.AllowEdit = false;
            this.colSalePrice.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colSalePrice.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colSalePrice.OptionsColumn.ReadOnly = true;
            this.colSalePrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "SALEPRICE", "{0:#,##0}")});
            this.colSalePrice.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.colSalePrice.Visible = true;
            this.colSalePrice.VisibleIndex = 6;
            this.colSalePrice.Width = 99;
            // 
            // colTotalAmountBFVAT
            // 
            this.colTotalAmountBFVAT.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountBFVAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountBFVAT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountBFVAT.Caption = "Tổng tiền trước VAT";
            this.colTotalAmountBFVAT.DisplayFormat.FormatString = "N0";
            this.colTotalAmountBFVAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalAmountBFVAT.FieldName = "TOTALAMOUNTBFVAT";
            this.colTotalAmountBFVAT.Name = "colTotalAmountBFVAT";
            this.colTotalAmountBFVAT.OptionsColumn.AllowEdit = false;
            this.colTotalAmountBFVAT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colTotalAmountBFVAT.OptionsColumn.ReadOnly = true;
            this.colTotalAmountBFVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "TOTALAMOUNTBFVAT", "{0:#,##0}")});
            this.colTotalAmountBFVAT.Visible = true;
            this.colTotalAmountBFVAT.VisibleIndex = 7;
            this.colTotalAmountBFVAT.Width = 110;
            // 
            // colVAT
            // 
            this.colVAT.AppearanceCell.Options.UseTextOptions = true;
            this.colVAT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colVAT.AppearanceHeader.Options.UseTextOptions = true;
            this.colVAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVAT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colVAT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVAT.Caption = "VAT";
            this.colVAT.FieldName = "VAT";
            this.colVAT.Name = "colVAT";
            this.colVAT.OptionsColumn.AllowEdit = false;
            this.colVAT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colVAT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colVAT.OptionsColumn.ReadOnly = true;
            this.colVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "VAT", "{0:#,##0}")});
            this.colVAT.Visible = true;
            this.colVAT.VisibleIndex = 8;
            this.colVAT.Width = 50;
            // 
            // colTotalAmountVAT
            // 
            this.colTotalAmountVAT.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmountVAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmountVAT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalAmountVAT.Caption = "Tổng tiền VAT";
            this.colTotalAmountVAT.DisplayFormat.FormatString = "N0";
            this.colTotalAmountVAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalAmountVAT.FieldName = "TOTALAMOUNTVAT";
            this.colTotalAmountVAT.Name = "colTotalAmountVAT";
            this.colTotalAmountVAT.OptionsColumn.AllowEdit = false;
            this.colTotalAmountVAT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colTotalAmountVAT.OptionsColumn.ReadOnly = true;
            this.colTotalAmountVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "TOTALAMOUNTVAT", "{0:#,##0}")});
            this.colTotalAmountVAT.Visible = true;
            this.colTotalAmountVAT.VisibleIndex = 9;
            this.colTotalAmountVAT.Width = 84;
            // 
            // colQuantity
            // 
            this.colQuantity.AppearanceCell.Options.UseTextOptions = true;
            this.colQuantity.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colQuantity.AppearanceHeader.Options.UseTextOptions = true;
            this.colQuantity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colQuantity.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colQuantity.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colQuantity.Caption = "Số lượng";
            this.colQuantity.FieldName = "QUANTITY";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.OptionsColumn.AllowEdit = false;
            this.colQuantity.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colQuantity.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colQuantity.OptionsColumn.ReadOnly = true;
            this.colQuantity.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "QUANTITY", "{0:n0}")});
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 5;
            this.colQuantity.Width = 79;
            // 
            // colAmount
            // 
            this.colAmount.AppearanceCell.Options.UseTextOptions = true;
            this.colAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colAmount.AppearanceHeader.Options.UseTextOptions = true;
            this.colAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAmount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colAmount.Caption = "Tổng tiền";
            this.colAmount.DisplayFormat.FormatString = "#,##0";
            this.colAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAmount.FieldName = "TOTALAMOUNT";
            this.colAmount.Name = "colAmount";
            this.colAmount.OptionsColumn.AllowEdit = false;
            this.colAmount.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colAmount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colAmount.OptionsColumn.ReadOnly = true;
            this.colAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "TOTALAMOUNT", "{0:n0}")});
            this.colAmount.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.colAmount.Visible = true;
            this.colAmount.VisibleIndex = 10;
            this.colAmount.Width = 120;
            // 
            // colVATPercent
            // 
            this.colVATPercent.AppearanceCell.Options.UseTextOptions = true;
            this.colVATPercent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colVATPercent.AppearanceHeader.Options.UseTextOptions = true;
            this.colVATPercent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVATPercent.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colVATPercent.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVATPercent.Caption = "VAT nộp";
            this.colVATPercent.FieldName = "VATPERCENT";
            this.colVATPercent.Name = "colVATPercent";
            this.colVATPercent.OptionsColumn.AllowEdit = false;
            this.colVATPercent.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colVATPercent.OptionsColumn.ReadOnly = true;
            // 
            // colProductStatusName
            // 
            this.colProductStatusName.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductStatusName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductStatusName.Caption = "Trạng thái sản phẩm";
            this.colProductStatusName.FieldName = "PRODUCTSTATUSNAME";
            this.colProductStatusName.Name = "colProductStatusName";
            this.colProductStatusName.OptionsColumn.AllowEdit = false;
            this.colProductStatusName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colProductStatusName.OptionsColumn.ReadOnly = true;
            this.colProductStatusName.Visible = true;
            this.colProductStatusName.VisibleIndex = 11;
            this.colProductStatusName.Width = 180;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.grdOutputVoucherDetail);
            this.groupControl1.Location = new System.Drawing.Point(3, 222);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1077, 283);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Chi tiết phiếu xuất";
            // 
            // frmOutputVoucher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 509);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(993, 536);
            this.Name = "frmOutputVoucher";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông tin phiếu xuất";
            this.Load += new System.EventHandler(this.frmOutputVoucher_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdOutputVoucherDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvOutputVoucherDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtOutputContent;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtOutputDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOutputVoucherID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtInVoiceID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtOrderID;
        private System.Windows.Forms.TextBox txtInvoiceSymbol;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraGrid.GridControl grdOutputVoucherDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView grvOutputVoucherDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colProductID;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
        private DevExpress.XtraGrid.Columns.GridColumn colEndWarrantyDate;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colSalePrice;
        private DevExpress.XtraGrid.Columns.GridColumn colAmount;
        private System.Windows.Forms.Button btnUpdate;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label15;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ctrlOutputUser;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblTaxID;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtCustomerTaxID;
        private System.Windows.Forms.TextBox txtCustomerName;
        private DevExpress.XtraGrid.Columns.GridColumn colOutputType;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private System.Windows.Forms.DateTimePicker dtInvoiceDate;
        private System.Windows.Forms.TextBox txtDiscountOutput;
        private System.Windows.Forms.LinkLabel lnkCustomer;
        private System.Windows.Forms.TextBox txtCustomerID;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colVATPercent;
        private System.Windows.Forms.TextBox txtOutputStore;
        private System.Windows.Forms.TextBox txtPayableType;
        private System.Windows.Forms.TextBox txtDiscountReasonName;
        private System.Windows.Forms.TextBox txtOutputNote;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dteDueDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTaxAddress;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraGrid.Columns.GridColumn colProductStatusName;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountBFVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmountVAT;
    }
}