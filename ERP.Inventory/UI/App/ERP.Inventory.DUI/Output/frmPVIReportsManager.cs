﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using System.Collections;
using Library.AppCore.LoadControls;
namespace ERP.Inventory.DUI.Output
{
    /// <summary>
    /// Created by: Đặng Minh Hùng
    /// Desc: Quản lý phiếu xuất
    /// </summary>
    public partial class frmPVIReportsManager : Form
    {
        public frmPVIReportsManager()
        {
            InitializeComponent();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvData);
        }

        #region variable
        private ERP.Inventory.PLC.Output.PLCOutputVoucher objPLCOutputVoucher = new PLC.Output.PLCOutputVoucher();
        private PLC.InputChangeOrder.PLCInputChangeOrder objPLCInputChangeOrder = new PLC.InputChangeOrder.PLCInputChangeOrder();
        private bool bolIsRight_InputVoucherReturn_Add = false;
        private bool bolIsRight_InputChange_Add = false;
        private bool bolIsRight_IMEIChange_Add = false;
        private DataTable dtbOutputTypeAll = null;
        private ERP.MasterData.PLC.MD.WSReport.Report objReport = new MasterData.PLC.MD.WSReport.Report();
        private string strCompany = string.Empty;
        private bool bolIsViettel = false;
        private DataTable dtbStore = Library.AppCore.DataSource.GetDataFilter.GetStore(true, 0, 0, 0, Library.AppCore.Constant.EnumType.StorePermissionType.VIEWREPORT_OUTPUT);
        private DataTable dtbProduct = Library.AppCore.DataSource.GetDataSource.GetProduct();
        public bool IsViettel
        {
            get { return bolIsViettel; }
            set { bolIsViettel = value; }
        }
        #endregion

        #region method

        private void LoadComboBox()
        {
            try
            {
                cboOutputType.InitControl(true);
                cboCustomer.InitControl(true);
                cboBranch.InitControl(true);
                cboStoreIDList.InitControl(true, dtbStore, "STOREID", "STORENAME", "-- Tất cả --");
                cboStoreIDList.IsReturnAllWhenNotChoose = true;
                dtpFromDate.Value = DateTime.Now;
                dtpToDate.Value = DateTime.Now;
                txtTop.Text = "500";
                cboSearchType.SelectedIndex = 0;
                cboErrorType.SelectedIndex = 0;
                dtbOutputTypeAll = Library.AppCore.DataSource.GetDataSource.GetOutputType().Copy();

                object[] objKeywords = new object[] { "@Keywords", "",
                                        "@DOCUMENTTYPEID", 3,// 1: Hóa đơn, 2: Nhập, 3: Xuất, 4: Thu/Chi
                                        "@IsDelete", 0};
                DataTable dtbData = null;
                dtbData = new MasterData.VAT.PLC.VAT.PLCDataReportType().SearchData(objKeywords);
                if (dtbData != null)
                {
                    cboDataReport.InitControl(true, dtbData, "DATAREPORTTYPEID", "DATAREPORTTYPENAME", "Tất cả");
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi nạp dữ liệu vào các combobox", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi khởi tạo dữ liệu tìm kiếm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private bool ValidatingSearch()
        {
            string strResult = Library.AppCore.Other.CheckObject.CheckMonthView(Library.AppCore.Constant.EnumType.Month_BussinessType.INVENTORY_OUTPUT,
                dtpFromDate.Value, dtpToDate.Value);
            if (!string.IsNullOrEmpty(strResult))
            {
                MessageBox.Show(strResult, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtpToDate.Focus();
                return false;
            }
            return true;
        }

        #endregion

        #region event

        private void frmOutputVoucherManager_Load(object sender, EventArgs e)
        {
            GetParameterForm();            
            LoadComboBox();
            try
            {
                bolIsRight_InputVoucherReturn_Add = Library.AppCore.SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHERRETURN_ADD");
                bolIsRight_InputChange_Add = Library.AppCore.SystemConfig.objSessionUser.IsPermission("PM_INPUTCHANGE_ADD");
                bolIsRight_IMEIChange_Add = Library.AppCore.SystemConfig.objSessionUser.IsPermission("PM_IMEICHANGE_ADD");
            }
            catch { }
         
            cboSearchType.SelectedIndexChanged += new EventHandler(cboSearchType_SelectedIndexChanged);
        }

        void cboSearchType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strOldKeyWords = string.Empty;
            strOldKeyWords = txtKeywords.Text.Trim();
            if (cboSearchType.SelectedIndex < 1)
            {
                if (txtKeywords.Text.Trim().Length > 200)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 200);
                txtKeywords.MaxLength = 200;//Tất cả
            }
            if (cboSearchType.SelectedIndex == 1)
            {
                if (txtKeywords.Text.Trim().Length > 20)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 20);
                txtKeywords.MaxLength = 20;//Mã phiếu xuất OutputVoucherID
            }
            else if (cboSearchType.SelectedIndex == 2)
            {
                if (txtKeywords.Text.Trim().Length > 20)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 20);
                txtKeywords.MaxLength = 20;//Mã đơn hàng OrderID
            }
            else if (cboSearchType.SelectedIndex == 3)
            {
                if (txtKeywords.Text.Trim().Length > 20)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 20);
                txtKeywords.MaxLength = 20;//Số hóa đơn InVoiceID
            }
            else if (cboSearchType.SelectedIndex == 4)
            {
                if (txtKeywords.Text.Trim().Length > 20)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 20);
                txtKeywords.MaxLength = 20;//Mã sản phẩm ProductID
            }
            else if (cboSearchType.SelectedIndex == 5)
            {
                if (txtKeywords.Text.Trim().Length > 50)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 50);
                txtKeywords.MaxLength = 50;//Số IMEI IMEI
            }
            else if (cboSearchType.SelectedIndex == 6)
            {
                if (txtKeywords.Text.Trim().Length > 20)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 20);
                txtKeywords.MaxLength = 20;//Mã nhân viên CreatedUser
            }
            else if (cboSearchType.SelectedIndex == 7)
            {
                if (txtKeywords.Text.Trim().Length > 200)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 200);
                txtKeywords.MaxLength = 200;//Tên nhân viên FullName
            }
            else if (cboSearchType.SelectedIndex == 8)
            {
                if (txtKeywords.Text.Trim().Length > 200)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 200);
                txtKeywords.MaxLength = 200;//Tên khách hàng Customername
            }
            else if (cboSearchType.SelectedIndex == 9)
            {
                if (txtKeywords.Text.Trim().Length > 20)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 20);
                txtKeywords.MaxLength = 20;//Số ĐT KH CustomerPhone
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!ValidatingSearch())
                return;
            //Tạo trạng thái chờ trong khi tạo báo cáo
            this.Cursor = Cursors.WaitCursor;
            btnSearch.Enabled = false;
            this.Refresh();
            bool bolResult = false;
            try
            {
                string strFilterKeyWords = string.Empty;
                strFilterKeyWords = Library.AppCore.Globals.FilterVietkey(txtKeywords.Text.Trim());
                string strCustomerIDList = cboCustomer.CustomerIDList;
                string strStoreIDList = cboStoreIDList.ColumnIDList;
                string strOutputTypeIDList = cboOutputType.OutputTypeIDList;
                bool bolCheckOutputVoucherID = false;
                bool bolCheckInVoiceID = false;
                bool bolCheckUserName = false;
                bool bolCheckFullName = false;
                bool bolCheckProductCode = false;
                bool bolCheckIMEI = false;
                bool bolCheckOrderID = false;
                bool bolCheckCustomerName = false;
                bool bolCheckCustomerPhone = false;
                bool bolIsViewDetail = chkIsViewDetail.Checked;
                if (txtTop.Text.Trim().Length == 0)
                    txtTop.Text = "500";
                if (cboSearchType.SelectedIndex < 1)
                {
                    strFilterKeyWords = txtKeywords.Text.Trim();
                }
                else if (cboSearchType.SelectedIndex == 1)
                {
                    bolCheckOutputVoucherID = true;
                }
                else if (cboSearchType.SelectedIndex == 2)
                {
                    bolCheckOrderID = true;
                }
                else if (cboSearchType.SelectedIndex == 3)
                {
                    bolCheckInVoiceID = true;
                }
                else if (cboSearchType.SelectedIndex == 4)
                {
                    bolCheckProductCode = true;
                }
                else if (cboSearchType.SelectedIndex == 5)
                {
                    bolCheckIMEI = true;
                }
                else if (cboSearchType.SelectedIndex == 6)
                {
                    bolCheckUserName = true;
                }
                else if (cboSearchType.SelectedIndex == 7)
                {
                    bolCheckFullName = true;
                    strFilterKeyWords = txtKeywords.Text.Trim();
                }
                else if (cboSearchType.SelectedIndex == 8)
                {
                    bolCheckCustomerName = true;
                    strFilterKeyWords = txtKeywords.Text.Trim();
                }
                else if (cboSearchType.SelectedIndex == 9)
                {
                    bolCheckCustomerPhone = true;
                }
                object[] objKeywords = new object[]
                {
                    "@FromDate", dtpFromDate.Value,
                    "@ToDate", dtpToDate.Value,
                    "@UserName", SystemConfig.objSessionUser.UserName,
                    "@Keyword", strFilterKeyWords,
                    "@ErrorType", cboErrorType.SelectedIndex,
                    "@CheckOutputVoucherID", bolCheckOutputVoucherID,
                    "@CheckInVoiceID", bolCheckInVoiceID,
                    "@CheckOrderID", bolCheckOrderID,
                    "@CheckProductCode", bolCheckProductCode,
                    "@CheckIMEI", bolCheckIMEI,
                    "@CheckUserName", bolCheckUserName,
                    "@CheckFullName", bolCheckFullName,
                    "@CheckCustomerName", bolCheckCustomerName,
                    "@CheckCustomerPhone", bolCheckCustomerPhone,
                    "@StoreIDList", strStoreIDList,
                    "@CustomerIDList", strCustomerIDList,
                    "@OutputTypeIDList", strOutputTypeIDList,
                    "@Top", Convert.ToInt32(txtTop.Text.Trim()),
                    "@IsViewDetail", bolIsViewDetail,
                  //  "@intMaingroupID", intMaingroupID
                };
                DataTable dtbSearchData = objPLCOutputVoucher.SearchDataPVI(objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                SetVisibleColumn(bolIsViewDetail);
                grdData.DataSource = dtbSearchData;
                bolResult = true;
            }
            catch (Exception objExe)
            {
                SystemErrorWS.Insert("Lỗi tìm kiếm danh sách thông tin phiếu xuất", objExe.ToString(), "btnSearch_Click()", DUIInventory_Globals.ModuleName);
                bolResult = false;
            }
            if (!bolResult)
                MessageBox.Show(this, "Lỗi tìm kiếm danh sách thông tin phiếu xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //Bỏ trạng thái chờ sau khi tạo xong báo cáo			
            this.Cursor = Cursors.Default;
            btnSearch.Enabled = true;
            this.Refresh();
        }

        private void SetVisibleColumn(bool bolIsViewDetail)
        {
            if (bolIsViewDetail)
            {
                colOutputTypeName.Visible = true;
                colProductName.Visible = true;
                colProductID.Visible = true;
                colIMEI.Visible = true;
                colProductID.VisibleIndex = 2;
                colProductName.VisibleIndex = 3;
                colIMEI.VisibleIndex = 4;
                colOutputTypeName.VisibleIndex = 5;
                colOutputContent.Visible = false;
                colIsInputReturnChangeOrder.Visible = false;
                colDiscount.Visible = false;
                grvData.OptionsView.AllowCellMerge = true;
                colOutputVoucherID.Summary.Clear();
            }
            else
            {
                colOutputContent.Visible = true;
                colIsInputReturnChangeOrder.Visible = true;
                colDiscount.Visible = true;
                colProductID.Visible = false;
                colProductName.Visible = false;
                colIMEI.Visible = false;
                colOutputTypeName.Visible = false;
                colDiscount.VisibleIndex = 12;
                colOutputContent.VisibleIndex = 18;
                colOutputNote.VisibleIndex = 19;
                colIsInputReturnChangeOrder.VisibleIndex = 20;
                grvData.OptionsView.AllowCellMerge = false;
            }

        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            string strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();
            frmOutputVoucher frmOutputVoucher1 = new frmOutputVoucher();
            frmOutputVoucher1.strOutputVoucherID = strOutputVoucherID;
            frmOutputVoucher1.ShowDialog();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData, DevExpress.XtraPrinting.TextExportMode.Text, false, string.Empty, false);
        }


        private void txtKeywords_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToString(txtKeywords.Text).Trim() != string.Empty && e.KeyChar == (char)Keys.Enter)
            {
                btnSearch_Click(null, null);
            }
        }

        private void GetParameterForm()
        {
            try
            {
                if (this.Tag == null || this.Tag.ToString() == string.Empty)
                {
                    IsViettel = false;
                    return;
                }
                Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                if (hstbParam.ContainsKey("COMPANY"))
                    strCompany = (hstbParam["COMPANY"] ?? "").ToString().Trim();
                if (string.IsNullOrEmpty(strCompany))
                {
                    IsViettel = false;
                }
                else if (strCompany.CompareTo("1") == 0)
                    IsViettel = true;
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi lấy tham số cấu hình form " + this.Text, objExce, "frmInputChangeOrderInput ->GetParameterForm");
                MessageBoxObject.ShowWarningMessage(this, "Lỗi khi lấy tham số cấu hình form " + this.Text);
                return;
            }
        }
        private void cboBranch_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cboBranch.BranchIDList))
            {
                if (dtbStore != null)
                {
                    cboStoreIDList.InitControl(true, dtbStore, "STOREID", "STORENAME", "--Tất cả--");
                }
            }
            else
            {
                var lst = dtbStore.Select("'" + cboBranch.BranchIDList + "' LIKE '%<' + BRANCHID + '>%'");
                if (lst.Any())
                {
                    DataTable dtb = lst.CopyToDataTable();
                    cboStoreIDList.InitControl(true, dtb, "STOREID", "STORENAME", "--Tất cả--");
                }
            }
        }

        private void cboDataReport_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(cboDataReport.ColumnIDList))
            {
                cboOutputType.InitControl(true, dtbOutputTypeAll);
                cboOutputType.IsReturnAllWhenNotChoose = true;
            }
            else
            {
                object[] objKeywords = new object[] { "@DATAREPORTTYPEIDLIST", cboDataReport.ColumnIDList };
                DataTable dtbOutPutType = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_DATAREPORTTYPE_OUTTYPE_GET", objKeywords);
                if (dtbOutPutType != null)
                {
                    cboOutputType.InitControl(true, dtbOutPutType);
                    cboOutputType.IsReturnAllWhenNotChoose = true;
                }
            }
        }
        #endregion


    }
}
