﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using System.Collections;
using Library.AppCore.LoadControls;
namespace ERP.Inventory.DUI.Output
{
    /// <summary>
    /// Created by: Đặng Minh Hùng
    /// Desc: Quản lý phiếu xuất
    /// </summary>
    public partial class frmOutputVoucherManager : Form
    {
        public frmOutputVoucherManager()
        {
            InitializeComponent();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvData);
        }

        #region variable
        private ERP.Inventory.PLC.Output.PLCOutputVoucher objPLCOutputVoucher = new PLC.Output.PLCOutputVoucher();
        private PLC.InputChangeOrder.PLCInputChangeOrder objPLCInputChangeOrder = new PLC.InputChangeOrder.PLCInputChangeOrder();
        private bool bolIsRight_InputVoucherReturn_Add = false;
        private bool bolIsRight_InputChange_Add = false;
        private bool bolIsRight_IMEIChange_Add = false;
        private PLC.ProductChange.WSProductChange.ProductChange objProductChange = null;
        private DataTable dtbOutputTypeAll = null;
        private int intReportID = 0;//In phieu xuat kho hàng hóa chi tiết
        private int intReportID1 = 0;//In phieu xuat thu 2
        private int intReportGroupbyID = 0;//In phieu xuat kho hàng hóa 
        private ERP.MasterData.PLC.MD.WSReport.Report objReport = new MasterData.PLC.MD.WSReport.Report();
        private string strCompany = string.Empty;
        private bool bolIsViettel = false;
        private string strPermission_CreateInvoiceHold = "VAT_CREATEINVOICEHOLD";
        private DataTable dtbStore = Library.AppCore.DataSource.GetDataFilter.GetStore(true, 0, 0, 0, Library.AppCore.Constant.EnumType.StorePermissionType.VIEWREPORT_OUTPUT);

        decimal totalVAT = 0, totalAmount = 0;
        public bool IsViettel
        {
            get { return bolIsViettel; }
            set { bolIsViettel = value; }
        }
        #endregion

        #region method

        private void LoadComboBox()
        {
            try
            {
                cboOutputType.InitControl(true);
                cboCustomer.InitControl(true);
                cboBranch.InitControl(true);
                cboStoreIDList.InitControl(true, dtbStore, "STOREID", "STORENAME", "-- Tất cả --");
                cboStoreIDList.IsReturnAllWhenNotChoose = true;
                dtpFromDate.Value = DateTime.Now;
                dtpToDate.Value = DateTime.Now;
                txtTop.Text = "500";
                cboSearchType.SelectedIndex = 0;
                cboErrorType.SelectedIndex = 0;
                dtbOutputTypeAll = Library.AppCore.DataSource.GetDataSource.GetOutputType().Copy();

                object[] objKeywords = new object[] { "@Keywords", "",
                                        "@DOCUMENTTYPEID", 3,// 1: Hóa đơn, 2: Nhập, 3: Xuất, 4: Thu/Chi
                                        "@IsDelete", 0};
                DataTable dtbData = null;
                dtbData = new MasterData.VAT.PLC.VAT.PLCDataReportType().SearchData(objKeywords);
                if (dtbData != null)
                {
                    cboDataReport.InitControl(true, dtbData, "DATAREPORTTYPEID", "DATAREPORTTYPENAME", "Tất cả");
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi nạp dữ liệu vào các combobox", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi khởi tạo dữ liệu tìm kiếm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private bool ValidatingSearch()
        {
            string strResult = Library.AppCore.Other.CheckObject.CheckMonthView(Library.AppCore.Constant.EnumType.Month_BussinessType.INVENTORY_OUTPUT,
                dtpFromDate.Value, dtpToDate.Value);
            if (!string.IsNullOrEmpty(strResult))
            {
                MessageBox.Show(strResult, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                dtpToDate.Focus();
                return false;
            }
            return true;
        }

        #endregion

        #region event

        private void frmOutputVoucherManager_Load(object sender, EventArgs e)
        {
            GetParameterForm();
            if (this.Tag != null && this.Tag.ToString().Trim() != string.Empty)
            {
                try
                {
                    Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                    intReportID = Convert.ToInt32(hstbParam["ReportID"]);
                    if (intReportID > 0)
                    {
                        objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID);
                        if (objReport != null)
                        {
                            if (SystemConfig.objSessionUser.IsPermission(objReport.FunctionID))
                                mnuItemPrintOutputStore.Enabled = true;
                        }
                    }
                    intReportID1 = Convert.ToInt32(hstbParam["ReportID1"]);
                    if (intReportID1 > 0)
                    {
                        objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID1);
                        if (objReport != null)
                        {
                            mnuItemPrintOutputVoucher.Text = "In " + objReport.ReportName.ToLower();
                            if (SystemConfig.objSessionUser.IsPermission(objReport.FunctionID))
                                mnuItemPrintOutputVoucher.Enabled = true;
                        }
                    }

                    intReportGroupbyID = Convert.ToInt32(hstbParam["ReportGroupbyID"]);
                    if (intReportGroupbyID > 0)
                    {
                        objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID);
                        if (objReport != null)
                        {
                            if (SystemConfig.objSessionUser.IsPermission(objReport.FunctionID))
                                mnuItemPrintOutputStoreGroupBy.Enabled = true;
                        }
                    }
                }
                catch { }
            }
            LoadComboBox();
            try
            {
                bolIsRight_InputVoucherReturn_Add = Library.AppCore.SystemConfig.objSessionUser.IsPermission("PM_INPUTVOUCHERRETURN_ADD");
                bolIsRight_InputChange_Add = Library.AppCore.SystemConfig.objSessionUser.IsPermission("PM_INPUTCHANGE_ADD");
                bolIsRight_IMEIChange_Add = Library.AppCore.SystemConfig.objSessionUser.IsPermission("PM_IMEICHANGE_ADD");
            }
            catch { }
            mnuInputChangeOrderInput.Visible = true;
            mnuInputChangeOrderInputHasError.Visible = true;
            toolStripSeparator3.Visible = true;
            cboSearchType.SelectedIndexChanged += new EventHandler(cboSearchType_SelectedIndexChanged);
        }

        void cboSearchType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strOldKeyWords = string.Empty;
            strOldKeyWords = txtKeywords.Text.Trim();
            if (cboSearchType.SelectedIndex < 1)
            {
                if (txtKeywords.Text.Trim().Length > 200)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 200);
                txtKeywords.MaxLength = 200;//Tất cả
            }
            if (cboSearchType.SelectedIndex == 1)
            {
                if (txtKeywords.Text.Trim().Length > 20)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 20);
                txtKeywords.MaxLength = 20;//Mã phiếu xuất OutputVoucherID
            }
            else if (cboSearchType.SelectedIndex == 2)
            {
                if (txtKeywords.Text.Trim().Length > 20)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 20);
                txtKeywords.MaxLength = 20;//Mã đơn hàng OrderID
            }
            else if (cboSearchType.SelectedIndex == 3)
            {
                if (txtKeywords.Text.Trim().Length > 20)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 20);
                txtKeywords.MaxLength = 20;//Số hóa đơn InVoiceID
            }
            else if (cboSearchType.SelectedIndex == 4)
            {
                if (txtKeywords.Text.Trim().Length > 20)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 20);
                txtKeywords.MaxLength = 20;//Mã sản phẩm ProductID
            }
            else if (cboSearchType.SelectedIndex == 5)
            {
                if (txtKeywords.Text.Trim().Length > 50)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 50);
                txtKeywords.MaxLength = 50;//Số IMEI IMEI
            }
            else if (cboSearchType.SelectedIndex == 6)
            {
                if (txtKeywords.Text.Trim().Length > 20)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 20);
                txtKeywords.MaxLength = 20;//Mã nhân viên CreatedUser
            }
            else if (cboSearchType.SelectedIndex == 7)
            {
                if (txtKeywords.Text.Trim().Length > 200)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 200);
                txtKeywords.MaxLength = 200;//Tên nhân viên FullName
            }
            else if (cboSearchType.SelectedIndex == 8)
            {
                if (txtKeywords.Text.Trim().Length > 200)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 200);
                txtKeywords.MaxLength = 200;//Tên khách hàng Customername
            }
            else if (cboSearchType.SelectedIndex == 9)
            {
                if (txtKeywords.Text.Trim().Length > 20)
                    txtKeywords.Text = strOldKeyWords.Substring(0, 20);
                txtKeywords.MaxLength = 20;//Số ĐT KH CustomerPhone
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!ValidatingSearch())
                return;
            //Tạo trạng thái chờ trong khi tạo báo cáo
            this.Cursor = Cursors.WaitCursor;
            btnSearch.Enabled = false;
            this.Refresh();
            bool bolResult = false;
            try
            {
                string strFilterKeyWords = string.Empty;
                strFilterKeyWords = Library.AppCore.Globals.FilterVietkey(txtKeywords.Text.Trim());
                string strCustomerIDList = cboCustomer.CustomerIDList;
                string strStoreIDList = cboStoreIDList.ColumnIDList;
                string strOutputTypeIDList = cboOutputType.OutputTypeIDList;
                bool bolCheckOutputVoucherID = false;
                bool bolCheckInVoiceID = false;
                bool bolCheckUserName = false;
                bool bolCheckFullName = false;
                bool bolCheckProductCode = false;
                bool bolCheckIMEI = false;
                bool bolCheckOrderID = false;
                bool bolCheckCustomerName = false;
                bool bolCheckCustomerPhone = false;
                bool bolIsViewDetail = chkIsViewDetail.Checked;
                if (txtTop.Text.Trim().Length == 0)
                    txtTop.Text = "500";
                if (cboSearchType.SelectedIndex < 1)
                {
                    strFilterKeyWords = txtKeywords.Text.Trim();
                }
                else if (cboSearchType.SelectedIndex == 1)
                {
                    bolCheckOutputVoucherID = true;
                }
                else if (cboSearchType.SelectedIndex == 2)
                {
                    bolCheckOrderID = true;
                }
                else if (cboSearchType.SelectedIndex == 3)
                {
                    bolCheckInVoiceID = true;
                }
                else if (cboSearchType.SelectedIndex == 4)
                {
                    bolCheckProductCode = true;
                }
                else if (cboSearchType.SelectedIndex == 5)
                {
                    bolCheckIMEI = true;
                }
                else if (cboSearchType.SelectedIndex == 6)
                {
                    bolCheckUserName = true;
                }
                else if (cboSearchType.SelectedIndex == 7)
                {
                    bolCheckFullName = true;
                    strFilterKeyWords = txtKeywords.Text.Trim();
                }
                else if (cboSearchType.SelectedIndex == 8)
                {
                    bolCheckCustomerName = true;
                    strFilterKeyWords = txtKeywords.Text.Trim();
                }
                else if (cboSearchType.SelectedIndex == 9)
                {
                    bolCheckCustomerPhone = true;
                }
                object[] objKeywords = new object[]
                {
                    "@FromDate", dtpFromDate.Value,
                    "@ToDate", dtpToDate.Value,
                    "@UserName", SystemConfig.objSessionUser.UserName,
                    "@Keyword", strFilterKeyWords,
                    "@ErrorType", cboErrorType.SelectedIndex,
                    "@CheckOutputVoucherID", bolCheckOutputVoucherID,
                    "@CheckInVoiceID", bolCheckInVoiceID,
                    "@CheckOrderID", bolCheckOrderID,
                    "@CheckProductCode", bolCheckProductCode,
                    "@CheckIMEI", bolCheckIMEI,
                    "@CheckUserName", bolCheckUserName,
                    "@CheckFullName", bolCheckFullName,
                    "@CheckCustomerName", bolCheckCustomerName,
                    "@CheckCustomerPhone", bolCheckCustomerPhone,
                    "@StoreIDList", strStoreIDList,
                    "@CustomerIDList", strCustomerIDList,
                    "@OutputTypeIDList", strOutputTypeIDList,
                    "@Top", Convert.ToInt32(txtTop.Text.Trim()),
                    "@IsViewDetail", bolIsViewDetail
                };
                DataTable dtbSearchData = objPLCOutputVoucher.SearchData(objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                CaclMoney(dtbSearchData, ref totalVAT, ref totalAmount);
                SetVisibleColumn(bolIsViewDetail);
                grdData.DataSource = dtbSearchData;
                bolResult = true;
            }
            catch (Exception objExe)
            {
                SystemErrorWS.Insert("Lỗi tìm kiếm danh sách thông tin phiếu xuất", objExe.ToString(), "btnSearch_Click()", DUIInventory_Globals.ModuleName);
                bolResult = false;
            }
            if (!bolResult)
                MessageBox.Show(this, "Lỗi tìm kiếm danh sách thông tin phiếu xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //Bỏ trạng thái chờ sau khi tạo xong báo cáo			
            this.Cursor = Cursors.Default;
            btnSearch.Enabled = true;
            this.Refresh();
        }

        private void SetVisibleColumn(bool bolIsViewDetail)
        {
            if (bolIsViewDetail)
            {
                colOutputTypeName.Visible = true;
                colProductName.Visible = true;
               
              
                colProductID.Visible = true;
                colIMEI.Visible = true;
                colProductID.VisibleIndex = 2;
                colProductName.VisibleIndex = 3;
                colIMEI.VisibleIndex = 5;
                QUANTITY.Visible = true;
                QUANTITY.VisibleIndex = 5;
                colOutputTypeName.VisibleIndex = 5;
                colOutputContent.Visible = false;
                colIsInputReturnChangeOrder.Visible = false;
                colDiscount.Visible = false;
                grvData.OptionsView.AllowCellMerge = true;
                colOutputVoucherID.Summary.Clear();
            }
            else
            {
                colOutputContent.Visible = true;
                colIsInputReturnChangeOrder.Visible = true;
                colDiscount.Visible = true;
                colProductID.Visible = false;
                colProductName.Visible = false;
                colIMEI.Visible = false;
                QUANTITY.Visible = false;
                colOutputTypeName.Visible = false;
                colDiscount.VisibleIndex = 12;
                colOutputContent.VisibleIndex = 18;
                colOutputNote.VisibleIndex = 19;
                colIsInputReturnChangeOrder.VisibleIndex = 20;
                grvData.OptionsView.AllowCellMerge = false;
            }

        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            string strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();
            frmOutputVoucher frmOutputVoucher1 = new frmOutputVoucher();
            frmOutputVoucher1.strOutputVoucherID = strOutputVoucherID;
            frmOutputVoucher1.ShowDialog();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData, DevExpress.XtraPrinting.TextExportMode.Text, false, string.Empty, false);
        }

        #endregion

        private void mnuItemInputVoucherReturn_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            String strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();
            //DateTime dtmNow = Library.AppCore.Globals.GetServerDateTime();
            //DateTime dtmOutputDate = Convert.ToDateTime(row["OutputDate"]);
            //int intHourValid = 48;
            //ERP.MasterData.SYS.PLC.WSAppConfig.AppConfig objAppConfig = null;
            //ERP.MasterData.SYS.PLC.PLCAppConfig objPLCAppConfig = new MasterData.SYS.PLC.PLCAppConfig();
            //objPLCAppConfig.LoadInfo(ref objAppConfig, "InputVoucherReturn_Hours");//Số giờ cho phép nhập trả hàng
            //if (objAppConfig != null && objAppConfig.ConfigValue != string.Empty)
            //{
            //    try
            //    {
            //        intHourValid = Convert.ToInt32(objAppConfig.ConfigValue);
            //    }
            //    catch
            //    {
            //    }
            //}
            //if (intHourValid > 0)
            //{
            //    if (dtmNow.Date > dtmOutputDate.AddHours(intHourValid).Date)
            //    {
            //        MessageBox.Show(this, "Phiếu xuất " + strOutputVoucherID + " đã quá hạn để nhập trả hàng. Vui lòng chọn phiếu xuất khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        return;
            //    }
            //}

            PLC.Input.PLCInputVoucherReturn objPLCInputVoucherReturn = new PLC.Input.PLCInputVoucherReturn();
            DataTable tblOutputVoucherDetail = objPLCInputVoucherReturn.LoadOutputVoucherDetailForReturn(strOutputVoucherID, false, false);
            if (tblOutputVoucherDetail.Rows.Count < 1)
            {
                DataTable tblCheckOutputType = objPLCOutputVoucher.GetList(strOutputVoucherID);
                for (int i = 0; i < tblCheckOutputType.Rows.Count; i++)
                {
                    DataRow rCheck = tblCheckOutputType.Rows[i];
                    if (Convert.ToInt32(rCheck["Quantity"]) > 0)
                    {
                        DataRow[] drCanReturn = dtbOutputTypeAll.Select("IsCanReturn = 1 AND OutputTypeID = " + Convert.ToInt32(rCheck["OutputTypeID"]));
                        if (drCanReturn.Length < 1)
                        {
                            MessageBox.Show(this, "Hình thức xuất " + Convert.ToString(rCheck["OutputTypeName"]).Trim() + " không cho phép nhập trả hàng. Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                }
                MessageBox.Show(this, "Phiếu xuất " + strOutputVoucherID + " không có sản phẩm(hoặc IMEI) nào để nhập trả hàng. Vui lòng chọn phiếu xuất khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            ERP.Inventory.DUI.Input.frmInputVoucherReturn frmInputVoucherReturn1 = new Input.frmInputVoucherReturn();
            frmInputVoucherReturn1.OutputVoucherID = strOutputVoucherID;
            frmInputVoucherReturn1.ShowDialog();
        }

        private void mnuItemInputChange_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            String strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();
            DataTable dtbOutputVoucherDetail = objPLCOutputVoucher.GetList(strOutputVoucherID);
            ERP.Inventory.PLC.InputChange.PLCInputChange objPLCInputChange = new PLC.InputChange.PLCInputChange();
            DataTable dtbInputChangeDetail = objPLCInputChange.GetListByOutputVoucher(strOutputVoucherID);
            if (dtbInputChangeDetail.Rows.Count < 1)
            {
                MessageBox.Show(this, "Phiếu xuất " + strOutputVoucherID + " không có sản phẩm(hoặc IMEI) nào để nhập đổi hàng. Vui lòng chọn phiếu xuất khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            ERP.Inventory.DUI.InputChange.frmInputChange frmInputChange1 = new InputChange.frmInputChange();
            frmInputChange1.strOutputVoucherID = strOutputVoucherID;
            frmInputChange1.ShowDialog();
        }

        private void mnuContext_Opening(object sender, CancelEventArgs e)
        {
            mnuItemPrintInputChange.Enabled = bolIsRight_InputChange_Add;
            mnuInputChangeOrderInput.Enabled = false;
            mnuInputChangeOrderInputHasError.Enabled = false;
            mnuIMEIChange.Enabled = false;
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            String strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();
            mnuInputChangeOrderInput.Enabled = true;
            mnuInputChangeOrderInputHasError.Enabled = true;
            mnuIMEIChange.Enabled = bolIsRight_IMEIChange_Add;
            bool bolCreateInvoiceVAT = SystemConfig.objSessionUser.IsPermission("PM_OUTPUTVOUCHER_PRINTVAT");
            if (!bolCreateInvoiceVAT)
            {
                mnuCreateInvoiceVAT.Visible = false;
            }
            else
            {
                mnuCreateInvoiceVAT.Visible = true;
                bool bolIsINVOICEID = false;
                DataTable dtbInvoiceId = new ERP.Report.PLC.PLCReportDataSource()
                      .GetDataSource("VAT_INVOICE_SELBYOUTPUTID", new object[] { "@OUTPUTVOUCHERID", strOutputVoucherID });
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->mnuContext_Opening");
                }
                mnuCreateInvoiceVAT.Enabled = false;
                if (dtbInvoiceId != null && dtbInvoiceId.Rows.Count == 0)
                {
                    mnuCreateInvoiceVAT.Enabled = true;
                }
            }
            if (!Convert.IsDBNull(row["ORDERID"]))
            {
                mnuItemPrintPOReturnHandovers.Enabled = objPLCOutputVoucher.CheckOV_Poreturn(row["ORDERID"].ToString().Trim());
            }
            else mnuItemPrintPOReturnHandovers.Enabled = false;
        }

        private void mnuItemInputReturnWithFee_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            String strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();
            //DateTime dtmNow = Library.AppCore.Globals.GetServerDateTime();
            //DateTime dtmOutputDate = Convert.ToDateTime(row["OutputDate"]);
            //int intHourValid = 48;
            //ERP.MasterData.SYS.PLC.WSAppConfig.AppConfig objAppConfig = null;
            //ERP.MasterData.SYS.PLC.PLCAppConfig objPLCAppConfig = new MasterData.SYS.PLC.PLCAppConfig();
            //objPLCAppConfig.LoadInfo(ref objAppConfig, "InputVoucherReturn_Hours");//Số giờ cho phép nhập trả hàng
            //if (objAppConfig != null && objAppConfig.ConfigValue != string.Empty)
            //{
            //    try
            //    {
            //        intHourValid = Convert.ToInt32(objAppConfig.ConfigValue);
            //    }
            //    catch
            //    {
            //    }
            //}
            //if (intHourValid > 0)
            //{
            //    if (dtmNow.Date > dtmOutputDate.AddHours(intHourValid).Date)
            //    {
            //        MessageBox.Show(this, "Phiếu xuất " + strOutputVoucherID + " đã quá hạn để nhập trả hàng. Vui lòng chọn phiếu xuất khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        return;
            //    }
            //}

            PLC.Input.PLCInputVoucherReturn objPLCInputVoucherReturn = new PLC.Input.PLCInputVoucherReturn();
            DataTable tblOutputVoucherDetail = objPLCInputVoucherReturn.LoadOutputVoucherDetailForReturn(strOutputVoucherID, false, false);
            if (tblOutputVoucherDetail.Rows.Count < 1)
            {
                DataTable tblCheckOutputType = objPLCOutputVoucher.GetList(strOutputVoucherID);
                for (int i = 0; i < tblCheckOutputType.Rows.Count; i++)
                {
                    DataRow rCheck = tblCheckOutputType.Rows[i];
                    if (Convert.ToInt32(rCheck["Quantity"]) > 0)
                    {
                        DataRow[] drCanReturn = dtbOutputTypeAll.Select("IsCanReturn = 1 AND OutputTypeID = " + Convert.ToInt32(rCheck["OutputTypeID"]));
                        if (drCanReturn.Length < 1)
                        {
                            MessageBox.Show(this, "Hình thức xuất " + Convert.ToString(rCheck["OutputTypeName"]).Trim() + " không cho phép nhập trả hàng. Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                }
                MessageBox.Show(this, "Phiếu xuất " + strOutputVoucherID + " không có sản phẩm(hoặc IMEI) nào để nhập trả hàng. Vui lòng chọn phiếu xuất khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            ERP.Inventory.DUI.Input.frmInputVoucherReturn frmInputVoucherReturn1 = new Input.frmInputVoucherReturn();
            frmInputVoucherReturn1.OutputVoucherID = strOutputVoucherID;
            frmInputVoucherReturn1.IsReturnWithFee = true;
            frmInputVoucherReturn1.ShowDialog();
        }

        private void mnuItemPrintOutputStore_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            if (intReportID < 1)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            String strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();
            Hashtable hstbReportParam = new Hashtable();
            bool bolIsViewCostPrice = SystemConfig.objSessionUser.IsPermission("OUTPUTREPORT_VIEWCOSTPRICE");
            hstbReportParam.Add("OutputVoucherID", strOutputVoucherID);
            hstbReportParam.Add("IsCostPriceHide", !bolIsViewCostPrice);
            hstbReportParam.Add("IsSalePriceHide", false);
            MasterData.PLC.MD.PLCReport objPLCReport = new MasterData.PLC.MD.PLCReport();
            ERP.MasterData.PLC.MD.WSReport.Report objReport = new MasterData.PLC.MD.WSReport.Report();
            objPLCReport.LoadInfo(ref objReport, intReportID);
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, hstbReportParam);
        }

        private void mnuItemPrintInputChange_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            String strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();
            object[] objKeywords = new object[]
            {
                "@OutputVoucherID", strOutputVoucherID
            };

            ERP.Inventory.PLC.InputChange.PLCInputChange objPLCInputChange = new PLC.InputChange.PLCInputChange();
            string strInputChangeID = objPLCInputChange.CheckInputChangeByOutputVoucher(strOutputVoucherID);
            string strNewInputVoucherID = string.Empty;
            if (strInputChangeID != string.Empty)
            {
                ERP.Inventory.PLC.InputChange.WSInputChange.InputChange objInputChange = objPLCInputChange.LoadInfo(strInputChangeID);
                strNewInputVoucherID = objInputChange.NewInputVoucherID;
            }
            else
            {
                DataTable dtbInputChangeOrderData = objPLCInputChangeOrder.SearchData(new object[] {
                    "@Keyword", strOutputVoucherID,
                    "@SearchType", 2,
                    "@FromDate", new DateTime(2014,09,01),
                    "@ToDate", Library.AppCore.Globals.GetServerDateTime(),
                    "@StoreIDList", string.Empty,
                    "@IsDeleted", false
                });

                if (dtbInputChangeOrderData.Rows.Count > 0)
                {
                    string strNewOutputVoucherID = string.Empty;
                    for (int i = 0; i < dtbInputChangeOrderData.Rows.Count; i++)
                    {
                        if (dtbInputChangeOrderData.Rows[i]["NewOutputVoucherID"] != DBNull.Value && dtbInputChangeOrderData.Rows[i]["NewOutputVoucherID"].ToString().Trim() != string.Empty)
                        {
                            strNewInputVoucherID = dtbInputChangeOrderData.Rows[i]["NewInputVoucherID"].ToString().Trim();
                            strNewOutputVoucherID = dtbInputChangeOrderData.Rows[i]["NewOutputVoucherID"].ToString().Trim();
                        }
                    }
                    if (strNewOutputVoucherID == string.Empty)
                    {
                        MessageBox.Show(this, "Phiếu xuất này không có nhập đổi. Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show(this, "Phiếu xuất này không có nhập đổi. Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            if (strNewInputVoucherID != string.Empty)
            {
                Reports.Input.frmReportView frm = new Reports.Input.frmReportView();
                frm.InputVoucherID = strNewInputVoucherID;
                frm.bolIsPriceHide = false;
                frm.ShowDialog();
            }
        }

        private void mnuItemViewInputProductChange_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            String strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();

            objProductChange = new PLC.ProductChange.PLCProductChange().LoadInfo(strOutputVoucherID);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            if (objProductChange != null && objProductChange.InputVoucherID != string.Empty)
            {
                Input.frmInputVoucher frm = new Input.frmInputVoucher();
                frm.InputVoucherID = objProductChange.InputVoucherID;
                frm.IsEdit = true;
                frm.LoadControl();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show(this, "Phiếu xuất này không có xuất đổi hàng. Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }

        private void mnuItemViewInputVoucher_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle >= 0)
            {
                DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
                PLC.InputChange.WSInputChange.InputChange objInputChange = new PLC.InputChange.PLCInputChange().LoadInfo(Convert.ToString(row["OUTPUTVOUCHERID"]).Trim());
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnSearch.Enabled = true;
                    return;
                }
                if (objInputChange != null && objInputChange.NewInputVoucherID != null && objInputChange.NewInputVoucherID.ToString().Trim() != string.Empty)
                {
                    Input.frmInputVoucher frm = new Input.frmInputVoucher();
                    frm.InputVoucherID = objInputChange.NewInputVoucherID;
                    frm.IsEdit = true;
                    frm.LoadControl();
                    frm.ShowDialog();
                }
                else
                {
                    DataTable dtbInputChangeOrderData = objPLCInputChangeOrder.SearchData(new object[] {
                        "@Keyword", Convert.ToString(row["OUTPUTVOUCHERID"]).Trim(),
                        "@SearchType", 2,
                        "@FromDate", new DateTime(2014,09,01),
                        "@ToDate", Library.AppCore.Globals.GetServerDateTime(),
                        "@StoreIDList", string.Empty,
                        "@IsDeleted", false
                    });
                    if (dtbInputChangeOrderData.Rows.Count > 0)
                    {
                        string strNewInputVoucherID = string.Empty;
                        for (int i = 0; i < dtbInputChangeOrderData.Rows.Count; i++)
                        {
                            if (dtbInputChangeOrderData.Rows[i]["NewInputVoucherID"] != DBNull.Value && dtbInputChangeOrderData.Rows[i]["NewInputVoucherID"].ToString().Trim() != string.Empty)
                                strNewInputVoucherID = dtbInputChangeOrderData.Rows[i]["NewInputVoucherID"].ToString().Trim();
                        }
                        if (strNewInputVoucherID != string.Empty)
                        {
                            Input.frmInputVoucher frm = new Input.frmInputVoucher();
                            frm.InputVoucherID = strNewInputVoucherID;
                            frm.IsEdit = true;
                            frm.LoadControl();
                            frm.ShowDialog();
                        }
                        else
                        {
                            MessageBox.Show(this, "Phiếu xuất này không có nhập đổi. Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show(this, "Phiếu xuất này không có nhập đổi. Vui lòng kiểm tra lại.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                }
            }
        }

        private void txtKeywords_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToString(txtKeywords.Text).Trim() != string.Empty && e.KeyChar == (char)Keys.Enter)
            {
                btnSearch_Click(null, null);
            }
        }

        private void mnuInputChangeOrderInput_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            String strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();
            CreateInputChangeOrder(strOutputVoucherID, 0);
        }
        private void CreateInputChangeOrder(String strOutputVoucherID, int intApplyErrorType)
        {
            //ERP.Inventory.PLC.InputChangeOrder.PLCInputChangeOrder objPLCInputChangeOrder = new PLC.InputChangeOrder.PLCInputChangeOrder();
            //DataTable tblInputchangeOrderDetail = objPLCInputChangeOrder.GetDetail(strOutputVoucherID);
            //if (tblInputchangeOrderDetail.Rows.Count < 1)
            //{
            //    MessageBox.Show(this, "Phiếu xuất " + strOutputVoucherID + " không có sản phẩm(hoặc IMEI) nào để tạo yêu cầu nhập đổi/trả hàng. Vui lòng chọn phiếu xuất khác.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}
            ERP.Inventory.DUI.InputChangeOrder.frmInputChangeOrderInput frm1 = new InputChangeOrder.frmInputChangeOrderInput(strOutputVoucherID, intApplyErrorType);
            frm1.IsViettel = IsViettel;
            if (!frm1.GetInputChangeOrderType(this, strOutputVoucherID, intApplyErrorType))
                return;
            frm1.evtShowInputChangeOrderForm += new EventHandler(frm1_evtShowInputChangeOrderForm);
            frm1.ShowDialog();
        }
        private void frm1_evtShowInputChangeOrderForm(object sender, EventArgs e)
        {
            if (sender != null && sender is ERP.Inventory.DUI.InputChangeOrder.frmInputChangeOrderInput)
            {
                string[] splTextForm = ((ERP.Inventory.DUI.InputChangeOrder.frmInputChangeOrderInput)sender).Text.Split('$');
                ((ERP.Inventory.DUI.InputChangeOrder.frmInputChangeOrderInput)sender).Dispose();

                //if (!IsViettel)
                //{
                //    ERP.Inventory.DUI.InputChangeOrder.frmInputChangeOrder frm1 = new ERP.Inventory.DUI.InputChangeOrder.frmInputChangeOrder(Convert.ToInt32(splTextForm[0]));
                //    frm1.IsPrivilege = ((ERP.Inventory.DUI.InputChangeOrder.frmInputChangeOrderInput)sender).IsPrivilege;
                //    frm1.strOutputVoucherID = splTextForm[1];
                //    frm1.Text = splTextForm[2];
                //    frm1.ApplyErrorType = Convert.ToBoolean(splTextForm[3]);
                //    frm1.evtCloseParentForm += new EventHandler(frm1_evtCloseParentForm);
                //    frm1.ShowDialog();
                //}
                //else if (IsViettel)
                //{
                ERP.Inventory.DUI.InputChangeOrder.frmInputChangeOrderViettel frm1 = new ERP.Inventory.DUI.InputChangeOrder.frmInputChangeOrderViettel(Convert.ToInt32(splTextForm[0]));
                frm1.strOutputVoucherID = splTextForm[1];
                frm1.Text = splTextForm[2];
                frm1.ApplyErrorType = Convert.ToBoolean(splTextForm[3]);
                frm1.evtCloseParentForm += new EventHandler(frm1_evtCloseParentForm);
                frm1.ShowDialog();
                //}
            }
        }

        private void frm1_evtCloseParentForm(object sender, EventArgs e)
        {
            //if (sender != null && sender is ERP.Inventory.DUI.InputChangeOrder.frmInputChangeOrder)
            //{
            //}
        }

        private void mnuInputChangeOrderInputHasError_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            String strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();
            CreateInputChangeOrder(strOutputVoucherID, 1);
        }

        private void mnuItemPrintOutputVoucher_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID1);
            if (objReport == null)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin report in báo cáo xuất hàng");
                return;
            }
            Hashtable htbParameterValue = new Hashtable();
            htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
            htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
            htbParameterValue.Add("CompanyPhone", SystemConfig.DefaultCompany.Phone);
            htbParameterValue.Add("CompanyFax", SystemConfig.DefaultCompany.Fax);
            htbParameterValue.Add("CompanyEmail", SystemConfig.DefaultCompany.Email);
            htbParameterValue.Add("CompanyWebsite", SystemConfig.DefaultCompany.Website);
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            htbParameterValue.Add("OUTPUTVOUCHERID", Convert.ToString(row["OutputVoucherID"]).Trim());
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);
        }

        private void mnuIMEIChange_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            if (row == null)
                return;
            string strBrandIDList = Library.AppCore.AppConfig.GetStringConfigValue("IMEIChange_BrandIDList");
            if (string.IsNullOrEmpty(strBrandIDList))
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Chưa khai báo mã cấu hình ứng dụng IMEIChange_BrandIDList");
                return;
            }
            String strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();
            PLC.IMEIChange.PLCIMEIChange objPLCIMEIChange = new PLC.IMEIChange.PLCIMEIChange();
            DataTable tblDetail = objPLCIMEIChange.GetList(strOutputVoucherID);

            if (tblDetail.Select("IMEI IS NOT NULL").Length == 0)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Phiếu xuất này không có IMEI đổi");
                return;
            }
            else
            {
                if (tblDetail.Select("IMEI IS NOT NULL AND BrandID IN(" + strBrandIDList + ")").Length == 0)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Phiếu xuất này không thỏa nhà sản xuất áp dụng đổi");
                    return;
                }
            }
            tblDetail.DefaultView.RowFilter = "IMEI IS NOT NULL AND BrandID IN(" + strBrandIDList + ")";
            tblDetail = tblDetail.DefaultView.ToTable();
            if (tblDetail.Rows.Count == 0)
            {
                //Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Phiếu xuất " + strOutputVoucherID + " không có IMEI phù hợp để đổi IMEI. Vui lòng kiểm tra lại");
                return;
            }
            if (!tblDetail.Columns.Contains("IMEICHANGE"))
            {
                tblDetail.Columns.Add("IMEICHANGE", typeof(string));
            }
            for (int i = 0; i < tblDetail.Rows.Count; i++)
            {
                tblDetail.Rows[i]["IMEICHANGE"] = string.Empty;
                tblDetail.Rows[i]["IMEI"] = tblDetail.Rows[i]["IMEI"].ToString().Trim();
                tblDetail.Rows[i]["ProductID"] = tblDetail.Rows[i]["ProductID"].ToString().Trim();
            }
            frmSelectIMEI frm1 = new frmSelectIMEI(tblDetail);
            frm1.ShowDialog();
        }

        #region Đăng bổ sung insert Hóa đơn trên phiếu xuất
        private void mnuCreateInvoiceVAT_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            String strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();
            var objOutputVoucher = objPLCOutputVoucher.LoadInfo(strOutputVoucherID);

            // LE VAN DONG - 28/12/2017
            // KIỂM TRA TỒN TẠI 1 CHI TIẾT PHIẾU XUẤT CÓ HÌNH THỨC XUẤT LÀ XUẤT TRẢ NHÀ CUNG CẤP
            // THÌ KHÔNG CHO TẠO HÓA ĐƠN
            bool bolIsError = objPLCOutputVoucher.CheckOutputType_IsSubDebtOutput(strOutputVoucherID);
            if (bolIsError)
            {
                MessageBox.Show(this, "Trong phiếu xuất tồn tại hình thức xuất trả!\nKhông được phép tạo hóa đơn.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (objOutputVoucher.IsStoreChange)
            {
                PLC.PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrder = new ERP.Inventory.PLC.StoreChange.PLCStoreChangeOrder().LoadInfo(objOutputVoucher.OrderID, 0);
                string strInputVoucherID = string.Empty;
                DataTable dtbInputVoucher = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_STORECHANGE_GETBYOPVID",
                new object[] { "@OUTPUTVOUCHERID", strOutputVoucherID });
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->mnuCreateInvoiceVAT_Click");
                }
                if (dtbInputVoucher != null && dtbInputVoucher.Rows.Count > 0)
                {
                    strInputVoucherID = dtbInputVoucher.Rows[0][0].ToString();
                }
                else
                {
                    MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy phiếu nhập để tạo hóa đơn xuất chuyển kho!");
                    return;
                }
                if (objOutputVoucher.IsSInvoice)
                {
                    #region Tạo hóa đơn điện tử xuất chuyển kho
                    ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT objPLCPrintVAT = new PrintVAT.PLC.PrintVAT.PLCPrintVAT();
                    string strVATInvoiceID = string.Empty;
                    string strInvoiceNo = string.Empty;
                    string strTemplateCode = string.Empty;
                    PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVATInvoice = null;
                    var objResultMessage = objPLCPrintVAT.CreateVATSInvoiceStoreChange(strOutputVoucherID, strInputVoucherID, objStoreChangeOrder.StoreChangeOrderTypeID, ref objVATInvoice);
                    if (objResultMessage.IsError)
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, objResultMessage.MessageDetail);
                        SystemErrorWS.Insert("Lỗi khi thêm mơi hóa đơn GTGT!", objResultMessage.MessageDetail, DUIInventory_Globals.ModuleName + "->mnuCreateInvoiceVAT_Click");
                        return;
                    }

                    PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                    if (objVATInvoice != null)
                    {
                        objPrintVAT_PrepareData_Viettel.PrintVATInvoice(this, objVATInvoice, false, false);
                    }
                    #endregion
                }
                else
                {
                    #region tạo hóa đơn từ xuất chuyển kho
                    ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = null;
                    new ERP.MasterData.PLC.MD.PLCStoreChangeOrderType().LoadInfo(ref objStoreChangeOrderType, objStoreChangeOrder.StoreChangeOrderTypeID);
                    ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType = new ERP.MasterData.PLC.MD.PLCStoreChangeType().LoadInfo(Convert.ToInt32(objStoreChangeOrderType.StoreChangeTypeID));
                    if (objStoreChangeOrder != null)
                    {
                        if (objStoreChangeOrderType == null)
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Loại yêu cầu chuyển kho chưa khai báo Loại chuyển kho!");
                            return;
                        }
                        else
                        {
                            if (objStoreChangeOrderType.CreateVATInvoice == 0)
                            {
                                MessageBoxObject.ShowWarningMessage(this, "Loại yêu cầu chuyển kho không cho xuất hóa đơn!");
                                return;
                            }                           
                            ERP.Inventory.PLC.Input.WSInputVoucher.InputVoucher objInputVoucher = new PLC.Input.PLCInputVoucher().LoadInfo(strInputVoucherID);
                            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                            {
                                //Library.AppCore.Forms.frmWaitDialog.Close();
                                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                                return;
                            }
                            MasterData.PLC.MD.WSInputType.InputType objInputType = new MasterData.PLC.MD.PLCInputType().LoadInfo(objInputVoucher.InputTypeID);
                            if (objInputType == null)
                            {
                                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy hình thức nhập để tạo hóa đơn!");
                                return;
                            }
                            if (objInputType.InvoiceTransTypeID <= 0 || objInputType.IsReturnSale || objInputType.IsReturnOutput)
                            {
                                MessageBoxObject.ShowWarningMessage(this, "Hình thức nhập không đúng để tạo hóa đơn. Vui lòng kiểm tra lại!");
                                return;
                            }
                            PrintVAT.PLC.WSPrintVAT.VAT_PInvoice objVAT_PInvoice = CreateNewInputVATInvoice(this, objInputType, objInputVoucher);
                            object[] objsKeyword = new object[]
                            {
                            "@STOREID", objOutputVoucher.OutputStoreID,
                            "@TYPE", 2,
                            "@STORECHANGEORDERTYPEID",objStoreChangeOrder.StoreChangeOrderTypeID,
                            "@INVOICETRANSACTIONTYPE", objStoreChangeOrderType.InvoiceTransTypeID
                            };
                            DataTable dtsFormType = null;// new PLC.StoreChange.PLCStoreChange().GetFormTypebyStoreChangeOrderType(objsKeyword);
                            var objResultMessage = new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().SearchData_Invoiceno(ref dtsFormType, objsKeyword);
                            if (dtsFormType != null && dtsFormType.Rows.Count > 0)
                            {
                                int intFormTypeID = 0;
                                string strDenominator = string.Empty;
                                string strInvoiceSymbol1 = string.Empty;
                                int intBranchID = 0;
                                string strINVOICENO = string.Empty;
                                string strFormTypeBranchID = "0";
                                int.TryParse(dtsFormType.Rows[0]["FORMTYPEID"].ToString(), out intFormTypeID);
                                ERP.MasterData.PLC.MD.WSStore.Store objStoreOutput = null;
                                new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objStoreOutput, Convert.ToInt32(objOutputVoucher.OutputStoreID));
                                intBranchID = objStoreOutput.BranchID;
                                strDenominator = dtsFormType.Rows[0]["Denominator"].ToString();
                                strInvoiceSymbol1 = dtsFormType.Rows[0]["InvoiceSymbol"].ToString();
                                strFormTypeBranchID = dtsFormType.Rows[0]["FormTypeBranchID"].ToString();
                                strINVOICENO = dtsFormType.Rows[0]["INVOICENO"].ToString();
                                ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = null;
                                ERP.MasterData.PLC.MD.WSStore.Store objToStore = null;
                                new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, Convert.ToInt32(objStoreChangeOrder.ToStoreID));
                                var objBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + objToStore.BranchID);
                                if (objStoreChangeType.IsStoreCustomer)
                                {
                                    objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objToStore.CustomerID));
                                }
                                else
                                {
                                    objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objStoreChangeType.CustomerID));
                                    if (objCustomer.IsDefault)
                                    {
                                        objCustomer.CustomerName = objOutputVoucher.CustomerName;
                                        objCustomer.TaxCustomerName = objOutputVoucher.CustomerName;
                                    }
                                }
                                ERP.PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVATInvoice = new ERP.PrintVAT.PLC.WSPrintVAT.VAT_Invoice();
                                objCustomer.CustomerPhone = string.IsNullOrWhiteSpace(objOutputVoucher.CustomerPhone) ? objBranch[0]["PhoneNumber"].ToString().Trim() : objOutputVoucher.CustomerPhone.Trim();
                                objCustomer.TaxCustomerAddress = string.IsNullOrWhiteSpace(objOutputVoucher.TaxCustomerAddress) ? objBranch[0]["Address"].ToString().Trim() : objOutputVoucher.TaxCustomerAddress.Trim();
                                objCustomer.CustomerAddress = string.IsNullOrWhiteSpace(objOutputVoucher.TaxCustomerAddress) ? objBranch[0]["Address"].ToString().Trim() : objOutputVoucher.TaxCustomerAddress.Trim();
                                objCustomer.CustomerTaxID = string.IsNullOrWhiteSpace(objOutputVoucher.CustomerTaxID) ? objBranch[0]["TaxCode"].ToString().Trim() : objOutputVoucher.CustomerTaxID.Trim();
                                string strOrganizationName = objBranch[0]["BranchName"].ToString().Trim();
                                DataTable dtbDataProduct = null;
                                object[] objsKeyword1 = new object[]
                                {
                                "@OutputVoucherID", objOutputVoucher.OutputVoucherID,
                                "@StoreID", objOutputVoucher.OutputStoreID,
                                "@IsStoreChange", 1
                                };
                                new PrintVAT.PLC.PrintVAT.PLCPrintVAT().GetDataProductInvoice(ref dtbDataProduct, objsKeyword1);
                                if (dtbDataProduct != null && dtbDataProduct.Rows.Count > 0)
                                {
                                    string strAddress = string.Empty;
                                    string strTax = string.Empty;
                                    var objBranch1 = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + intBranchID);
                                    if (objBranch1.Count() > 0)
                                    {
                                        strAddress = objBranch1[0]["TAXADDRESS"].ToString();
                                        strTax = objBranch1[0]["TAXCODE"].ToString();
                                    }
                                    string strLstCustomerName = objOutputVoucher.CustomerPhone + "-" + objOutputVoucher.CustomerName;
                                    PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT objfrmInvoiceVAT = new PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT(
                                        dtbDataProduct, objCustomer, intBranchID, objOutputVoucher.OutputStoreID, strAddress, strTax, strDenominator,
                                        strInvoiceSymbol1, strINVOICENO, intFormTypeID, objOutputVoucher.PayableTypeID, 1, strFormTypeBranchID
                                        , objOutputVoucher.DiscountReasonID, objOutputVoucher.Discount, objOutputVoucher.StaffUser, strLstCustomerName, strOrganizationName, objVAT_PInvoice);
                                    objfrmInvoiceVAT.IsStoreChange = true;
                                    objfrmInvoiceVAT.StoreChangeOrderType = objStoreChangeOrderType;
                                    objfrmInvoiceVAT.ShowDialog();
                                }
                            }
                            else
                            {
                                DataTable dtCheckHoldInvoiceNo = null;
                                #region Kiểm tra số hóa đơn giữ

                                if (SystemConfig.objSessionUser.IsPermission(strPermission_CreateInvoiceHold))
                                {
                                    int intBranchIDCheck = 0;
                                    DataTable dtbBranchTemp = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy();
                                    if (dtbBranchTemp != null)
                                    {
                                        ERP.MasterData.PLC.MD.WSStore.Store objStore = ERP.MasterData.PLC.MD.PLCStore.GetinfoFromDataCache(objOutputVoucher.OutputStoreID);
                                        if (objStore != null)
                                        {
                                            intBranchIDCheck = objStore.BranchID;
                                        }
                                    }
                                    object[] objKeywords = new object[]{"@STORECHANGEORDERTYPEID", objStoreChangeOrder.StoreChangeOrderTypeID,
                                                                    "@BRANCHID",intBranchIDCheck,
                                                                    "@STOREID",objOutputVoucher.OutputStoreID};
                                    dtCheckHoldInvoiceNo = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_FORMTYPE_CHKHOLD", objKeywords);
                                    if (dtCheckHoldInvoiceNo != null && dtCheckHoldInvoiceNo.Rows.Count > 0)
                                    {
                                        #region Tạo hóa đơn

                                        int intFormTypeID = 0;
                                        string strDenominator = string.Empty;
                                        string strInvoiceSymbol1 = string.Empty;
                                        int intBranchID = 0;
                                        string strINVOICENO = string.Empty;
                                        string strFormTypeBranchID = "0";
                                        int.TryParse(dtCheckHoldInvoiceNo.Rows[0]["FORMTYPEID"].ToString(), out intFormTypeID);
                                        ERP.MasterData.PLC.MD.WSStore.Store objStoreOutput = null;
                                        new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objStoreOutput, Convert.ToInt32(objOutputVoucher.OutputStoreID));
                                        intBranchID = objStoreOutput.BranchID;
                                        strDenominator = dtCheckHoldInvoiceNo.Rows[0]["Denominator"].ToString();
                                        strInvoiceSymbol1 = dtCheckHoldInvoiceNo.Rows[0]["InvoiceSymbol"].ToString();
                                        //int.TryParse(dtsFormType.Rows[0]["CURRENTNO"].ToString(), out intCurrentNo);
                                        strFormTypeBranchID = dtCheckHoldInvoiceNo.Rows[0]["FormTypeBranchID"].ToString();
                                        //strINVOICENO = dtsFormType.Rows[0]["INVOICENO"].ToString();
                                        ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = null;
                                        ERP.MasterData.PLC.MD.WSStore.Store objToStore = null;
                                        new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, Convert.ToInt32(objStoreChangeOrder.ToStoreID));
                                        var objBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + objToStore.BranchID);
                                        if (objStoreChangeType.IsStoreCustomer)
                                        {
                                            objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objToStore.CustomerID));
                                        }
                                        else
                                        {
                                            objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objStoreChangeType.CustomerID));
                                            if (objCustomer.IsDefault)
                                            {
                                                objCustomer.CustomerName = objOutputVoucher.CustomerName;
                                                objCustomer.TaxCustomerName = objOutputVoucher.CustomerName;
                                            }
                                        }
                                        ERP.PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVATInvoice = new ERP.PrintVAT.PLC.WSPrintVAT.VAT_Invoice();
                                        objCustomer.CustomerPhone = objBranch[0]["PhoneNumber"].ToString().Trim();
                                        objCustomer.TaxCustomerAddress = objBranch[0]["Address"].ToString().Trim();
                                        objCustomer.CustomerAddress = objBranch[0]["Address"].ToString().Trim();
                                        objCustomer.CustomerTaxID = objBranch[0]["TaxCode"].ToString().Trim();
                                        string strOrganizationName = objBranch[0]["BranchName"].ToString().Trim();
                                        DataTable dtbDataProduct = null;
                                        object[] objsKeyword1 = new object[]
                                                                            {
                                                                            "@OutputVoucherID", objOutputVoucher.OutputVoucherID,
                                                                            "@StoreID", objOutputVoucher.OutputStoreID,
                                                                            "@IsStoreChange", 1
                                                                            };
                                        new PrintVAT.PLC.PrintVAT.PLCPrintVAT().GetDataProductInvoice(ref dtbDataProduct, objsKeyword1);
                                        if (dtbDataProduct != null && dtbDataProduct.Rows.Count > 0)
                                        {
                                            string strAddress = string.Empty;
                                            string strTax = string.Empty;
                                            var objBranch1 = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + intBranchID);
                                            if (objBranch1.Count() > 0)
                                            {
                                                strAddress = objBranch1[0]["TAXADDRESS"].ToString();
                                                strTax = objBranch1[0]["TAXCODE"].ToString();
                                            }
                                            string strLstCustomerName = objOutputVoucher.CustomerPhone + "-" + objOutputVoucher.CustomerName;
                                            PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT objfrmInvoiceVAT = new PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT(
                                                dtbDataProduct, objCustomer, intBranchID, objOutputVoucher.OutputStoreID, strAddress, strTax, strDenominator,
                                                strInvoiceSymbol1, strINVOICENO, intFormTypeID, objOutputVoucher.PayableTypeID, 1, strFormTypeBranchID
                                                , objOutputVoucher.DiscountReasonID, objOutputVoucher.Discount, objOutputVoucher.StaffUser, strLstCustomerName, strOrganizationName, objVAT_PInvoice, "", "", Convert.ToDateTime(dtCheckHoldInvoiceNo.Rows[0]["INVOICEDATE"]));
                                            objfrmInvoiceVAT.IsStoreChange = true;
                                            objfrmInvoiceVAT.StoreChangeOrderType = objStoreChangeOrderType;
                                            objfrmInvoiceVAT.ShowDialog();
                                        }

                                        #endregion
                                    }
                                }

                                #endregion

                                if (dtCheckHoldInvoiceNo == null || dtCheckHoldInvoiceNo.Rows.Count == 0)
                                {
                                    DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
                                    var drStore = dtbStore.Select("STOREID=" + objOutputVoucher.OutputStoreID);
                                    if (drStore.Any())
                                    {
                                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin thiết lập số hóa đơn cho kho xuất: " + objOutputVoucher.OutputStoreID + " - " + drStore[0]["STORENAME"].ToString() + "!");
                                    }
                                    else
                                    {
                                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin thiết lập số hóa đơn cho kho xuất!");
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            else
            {
                ERP.SalesAndServices.SaleOrders.PLC.WSSaleOrders.SaleOrder objSaleOrder = new ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders().LoadInfo(objOutputVoucher.OrderID);
                if (objSaleOrder != null)
                {
                    // bán hàng
                    string strSaleOrderTypeID_WholeSale = string.Empty;
                    try
                    {
                        strSaleOrderTypeID_WholeSale = Library.AppCore.AppConfig.GetStringConfigValue("SM_SALEORDERTYPE_WHOLESALE");
                    }
                    catch
                    {
                        strSaleOrderTypeID_WholeSale = string.Empty;
                    }
                    string[] arrSaleOrderTypeID_WholeSale = strSaleOrderTypeID_WholeSale.Split(',');
                    if (objOutputVoucher.IsSInvoice && (arrSaleOrderTypeID_WholeSale == null || !arrSaleOrderTypeID_WholeSale.Contains(objSaleOrder.SaleOrderTypeID.ToString())))
                    {
                        #region Tạo hóa đơn điện tử
                        ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT objPLCPrintVAT = new PrintVAT.PLC.PrintVAT.PLCPrintVAT();
                        string strVATInvoiceID = string.Empty;
                        string strInvoiceNo = string.Empty;
                        string strTemplateCode = string.Empty;
                        PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVATInvoice = null;
                        var objResultMessage = objPLCPrintVAT.CreateVATSInvoiceOutputVoucher(strOutputVoucherID, objSaleOrder.SaleOrderTypeID, ref objVATInvoice);
                        if (objResultMessage.IsError)
                        {
                            Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, objResultMessage.MessageDetail);
                            SystemErrorWS.Insert("Lỗi khi thêm mơi hóa đơn GTGT!", objResultMessage.MessageDetail, DUIInventory_Globals.ModuleName + "->mnuCreateInvoiceVAT_Click");
                            return;
                        }

                        PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                        if (objVATInvoice != null)
                        {
                            objPrintVAT_PrepareData_Viettel.PrintVATInvoice(this, objVATInvoice, false, false);
                        }
                        #endregion
                    }
                    else
                    {
                        #region  Tạo hóa đơn từ bán hàng
                        var objSaleOrderType = new ERP.MasterData.PLC.MD.PLCSaleOrderType().LoadInfoFromCache(objSaleOrder.SaleOrderTypeID);
                        object[] objsKeyword = new object[]
                            {
                            "@STOREID", objOutputVoucher.OutputStoreID,
                            "@TYPE", 1,
                            "@SALEORDERTYPEID",objSaleOrderType.SaleOrderTypeID,
                            "@INVOICETRANSACTIONTYPE", objSaleOrderType.InvoiceTransTypeID,
                            "@ISSINVOICE", objOutputVoucher.IsSInvoice
                            };
                        DataTable dtsFormType = null;// new ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders().GetFormTypebySaleOrderType(objsKeyword);
                        var objResultMessage = new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().SearchData_Invoiceno(ref dtsFormType, objsKeyword);
                        if (dtsFormType != null && dtsFormType.Rows.Count > 0)
                        {
                            int intFormTypeID = 0;
                            string strDenominator = string.Empty;
                            string strInvoiceSymbol1 = string.Empty;
                            int intBranchID = 0;
                            string strINVOICENO = string.Empty;
                            string strFormTypeBranchID = "0";
                            int.TryParse(dtsFormType.Rows[0]["FORMTYPEID"].ToString(), out intFormTypeID);
                            int.TryParse(dtsFormType.Rows[0]["BRANCHID"].ToString(), out intBranchID);
                            strDenominator = dtsFormType.Rows[0]["Denominator"].ToString();
                            strInvoiceSymbol1 = dtsFormType.Rows[0]["InvoiceSymbol"].ToString();
                            strFormTypeBranchID = dtsFormType.Rows[0]["FormTypeBranchID"].ToString();
                            strINVOICENO = dtsFormType.Rows[0]["INVOICENO"].ToString();
                            ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = new MasterData.PLC.MD.WSCustomer.Customer();
                            objCustomer.CustomerID = objOutputVoucher.CustomerID;
                            objCustomer.CustomerName = objOutputVoucher.CustomerName;
                            objCustomer.TaxCustomerName = objOutputVoucher.CustomerName;
                            objCustomer.CustomerPhone = objOutputVoucher.CustomerPhone;
                            objCustomer.TaxCustomerAddress = string.IsNullOrWhiteSpace(objSaleOrder.TaxCustomerAddress) ? objOutputVoucher.CustomerAddress : objSaleOrder.TaxCustomerAddress;
                            objCustomer.CustomerAddress = objOutputVoucher.CustomerAddress;
                            objCustomer.CustomerTaxID = objOutputVoucher.CustomerTaxID;
                            DataTable dtbDataProduct = null;
                            object[] objsKeyword1 = new object[]
                            {
                            "@OutputVoucherID", objOutputVoucher.OutputVoucherID,
                            "@StoreID", objOutputVoucher.OutputStoreID
                            };
                            new PrintVAT.PLC.PrintVAT.PLCPrintVAT().GetDataProductInvoice(ref dtbDataProduct, objsKeyword1);
                            if (dtbDataProduct != null && dtbDataProduct.Rows.Count > 0)
                            {
                                string strAddress = string.Empty;
                                string strTax = string.Empty;
                                var objBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + intBranchID);
                                if (objBranch.Count() > 0)
                                {
                                    strAddress = objBranch[0]["TAXADDRESS"].ToString();
                                    strTax = objBranch[0]["TAXCODE"].ToString();
                                }
                                string strLstCustomerName = objOutputVoucher.CustomerPhone + "-" + objOutputVoucher.CustomerName;
                                PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT objfrmInvoiceVAT = new PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT(
                                    dtbDataProduct, objCustomer, intBranchID, objOutputVoucher.OutputStoreID, strAddress, strTax, strDenominator,
                                    strInvoiceSymbol1, strINVOICENO, intFormTypeID, objOutputVoucher.PayableTypeID, 1, strFormTypeBranchID
                                    , objOutputVoucher.DiscountReasonID, objOutputVoucher.Discount, objOutputVoucher.StaffUser, strLstCustomerName, objSaleOrder.OrganizationName, null, objSaleOrder.Buyer, objSaleOrder.SaleOrderID);
                                objfrmInvoiceVAT.SaleOrderType = objSaleOrderType;
                                objfrmInvoiceVAT.IsSInvoice = objOutputVoucher.IsSInvoice;
                                objfrmInvoiceVAT.bolIsCloseForm = true;
                                objfrmInvoiceVAT.ShowDialog();
                                //if (objVAT_InvoiceMain != null)
                                //{
                                //    PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                                //    objPrintVAT_PrepareData_Viettel.PrintVATInvoice(objVAT_InvoiceMain, false, false);
                                //}
                            }
                        }
                        else
                        {
                            DataTable dtCheckHoldInvoiceNo = null;
                            #region Kiểm tra số hóa đơn giữ

                            if (SystemConfig.objSessionUser.IsPermission(strPermission_CreateInvoiceHold))
                            {
                                int intBranchIDCheck = 0;
                                DataTable dtbBranchTemp = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy();
                                if (dtbBranchTemp != null)
                                {
                                    ERP.MasterData.PLC.MD.WSStore.Store objStore = ERP.MasterData.PLC.MD.PLCStore.GetinfoFromDataCache(objOutputVoucher.OutputStoreID);
                                    if (objStore != null)
                                    {
                                        intBranchIDCheck = objStore.BranchID;
                                    }
                                }
                                object[] objKeywords = new object[]{"@SaleOrderTypeID", objSaleOrderType.SaleOrderTypeID,
                                                                "@BRANCHID",intBranchIDCheck,
                                                                "@STOREID",objOutputVoucher.OutputStoreID};
                                dtCheckHoldInvoiceNo = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_FORMTYPE_CHKHOLD", objKeywords);
                                if (dtCheckHoldInvoiceNo != null && dtCheckHoldInvoiceNo.Rows.Count > 0)
                                {
                                    #region Tạo hóa đơn

                                    int intFormTypeID = 0;
                                    string strDenominator = string.Empty;
                                    string strInvoiceSymbol1 = string.Empty;
                                    int intCurrentNo = 0;
                                    int intBranchID = 0;
                                    string strINVOICENO = string.Empty;
                                    string strFormTypeBranchID = "0";
                                    int.TryParse(dtCheckHoldInvoiceNo.Rows[0]["FORMTYPEID"].ToString(), out intFormTypeID);
                                    int.TryParse(dtCheckHoldInvoiceNo.Rows[0]["BRANCHID"].ToString(), out intBranchID);
                                    strDenominator = dtCheckHoldInvoiceNo.Rows[0]["Denominator"].ToString();
                                    strInvoiceSymbol1 = dtCheckHoldInvoiceNo.Rows[0]["InvoiceSymbol"].ToString();
                                    //int.TryParse(dtCheckHoldInvoiceNo.Rows[0]["CURRENTNO"].ToString(), out intCurrentNo);
                                    strFormTypeBranchID = dtCheckHoldInvoiceNo.Rows[0]["FormTypeBranchID"].ToString();
                                    //strINVOICENO = dtCheckHoldInvoiceNo.Rows[0]["INVOICENO"].ToString();
                                    ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = new MasterData.PLC.MD.WSCustomer.Customer();
                                    objCustomer.CustomerID = objOutputVoucher.CustomerID;
                                    objCustomer.CustomerName = objOutputVoucher.CustomerName;
                                    objCustomer.TaxCustomerName = objOutputVoucher.CustomerName;
                                    objCustomer.CustomerPhone = objOutputVoucher.CustomerPhone;
                                    objCustomer.TaxCustomerAddress = objOutputVoucher.CustomerAddress;
                                    objCustomer.CustomerAddress = objOutputVoucher.CustomerAddress;
                                    objCustomer.CustomerTaxID = objOutputVoucher.CustomerTaxID;
                                    DataTable dtbDataProduct = null;
                                    object[] objsKeyword1 = new object[]
                                                            {
                                                            "@OutputVoucherID", objOutputVoucher.OutputVoucherID,
                                                            "@StoreID", objOutputVoucher.OutputStoreID
                                                            };
                                    new PrintVAT.PLC.PrintVAT.PLCPrintVAT().GetDataProductInvoice(ref dtbDataProduct, objsKeyword1);
                                    if (dtbDataProduct != null && dtbDataProduct.Rows.Count > 0)
                                    {
                                        string strAddress = string.Empty;
                                        string strTax = string.Empty;
                                        var objBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + intBranchID);
                                        if (objBranch.Count() > 0)
                                        {
                                            strAddress = objBranch[0]["TAXADDRESS"].ToString();
                                            strTax = objBranch[0]["TAXCODE"].ToString();
                                        }
                                        string strLstCustomerName = objOutputVoucher.CustomerPhone + "-" + objOutputVoucher.CustomerName;
                                        PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT objfrmInvoiceVAT = new PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT(
                                            dtbDataProduct, objCustomer, intBranchID, objOutputVoucher.OutputStoreID, strAddress, strTax, strDenominator,
                                            strInvoiceSymbol1, strINVOICENO, intFormTypeID, objOutputVoucher.PayableTypeID, 1, strFormTypeBranchID
                                            , objOutputVoucher.DiscountReasonID, objOutputVoucher.Discount, objOutputVoucher.StaffUser, strLstCustomerName, objSaleOrder.OrganizationName, null, objSaleOrder.Buyer, objSaleOrder.SaleOrderID, Convert.ToDateTime(dtCheckHoldInvoiceNo.Rows[0]["INVOICEDATE"]));
                                        objfrmInvoiceVAT.SaleOrderType = objSaleOrderType;
                                        objfrmInvoiceVAT.bolIsCloseForm = true;
                                        objfrmInvoiceVAT.ShowDialog();
                                        //if (objVAT_InvoiceMain != null)
                                        //{
                                        //    PrintVAT.DUI.PrintVAT_PrepareData_Viettel objPrintVAT_PrepareData_Viettel = new PrintVAT.DUI.PrintVAT_PrepareData_Viettel();
                                        //    objPrintVAT_PrepareData_Viettel.PrintVATInvoice(objVAT_InvoiceMain, false, false);
                                        //}
                                    }

                                    #endregion
                                }
                            }

                            #endregion

                            if (dtCheckHoldInvoiceNo == null || dtCheckHoldInvoiceNo.Rows.Count == 0)
                            {
                                DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
                                var drStore = dtbStore.Select("STOREID=" + objOutputVoucher.OutputStoreID);
                                if (drStore.Any())
                                {
                                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin thiết lập số hóa đơn cho kho xuất: " + objOutputVoucher.OutputStoreID + " - " + drStore[0]["STORENAME"].ToString() + "!");
                                }
                                else
                                {
                                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin thiết lập số hóa đơn cho kho xuất!");
                                }
                            }
                        }

                        #endregion
                    }
                }
                else
                {
                    MessageBox.Show(this, "Màn hình phiếu xuất khổng thể tạo hóa đơn xuất trả hàng. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                    #region Hóa đơn đổi trả (Không có đơn hàng)
                    //DataTable dtbData = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_INPUTCHANGEORDER_GETBYOV", new object[] { "@OUTPUTVOUCHERID", objOutputVoucher.OutputVoucherID });
                    //if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    //{
                    //    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    //    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, ERP.Inventory.PLC.PLCInventory_Globals.ModuleName + "->PM_INPUTCHANGEORDER_GETBYOV");
                    //}
                    //if (dtbData == null || dtbData.Rows.Count < 1)
                    //    return;
                    //int intInputChangeOrderTypeID = 0;
                    //ERP.MasterData.PLC.MD.WSInputChangeOrderType.InputChangeOrderType objInputChangeOrderType = null;
                    //try
                    //{
                    //    intInputChangeOrderTypeID = Convert.ToInt32(dtbData.Rows[0]["INPUTCHANGEORDERTYPEID"].ToString());
                    //    new ERP.MasterData.PLC.MD.PLCInputChangeOrderType().LoadInfo(ref objInputChangeOrderType, intInputChangeOrderTypeID);
                    //}
                    //catch
                    //{
                    //}

                    //object[] objsKeyword = new object[]
                    //{
                    //"@STOREID", objOutputVoucher.OutputStoreID,
                    //"@INVOICETRANSACTIONTYPE",objInputChangeOrderType.InvoiceTransTypeID
                    //};
                    //DataTable dtsFormType = null;//new ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders().GetFormTypebySaleOrderType(objsKeyword);
                    //var objResultMessage = new PrintVAT.PLC.PrintVAT.PLCPrintVAT().SearchData_Invoiceno(ref dtsFormType, objsKeyword);
                    //if (dtsFormType != null && dtsFormType.Rows.Count > 0)// && intInvoiceNo > 0)
                    //{
                    //    int intInvoiceTransactionTypeID = 0;
                    //    ERP.MasterData.PLC.MD.WSSaleOrderType.SaleOrderType objSaleOrderType = null;
                    //    try
                    //    {
                    //        DataTable dtGetOldInvoice = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_OUTPUTCHANGEORDERGETINVOICE", new object[] { "@OUTPUTVOUCHERID", strOutputVoucherID });
                    //        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    //        {
                    //            Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    //            SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->mnuCreateInvoiceVAT_Click");
                    //        }
                    //        if (dtGetOldInvoice != null && dtGetOldInvoice.Rows.Count > 0)
                    //        {
                    //            intInvoiceTransactionTypeID = Convert.ToInt32(dtGetOldInvoice.Rows[0]["INVOICETRANSACTIONTYPEID"]);
                    //            objSaleOrderType = new MasterData.PLC.MD.WSSaleOrderType.SaleOrderType();
                    //            objSaleOrderType.InvoiceTransTypeID = intInvoiceTransactionTypeID;
                    //        }
                    //    }
                    //    catch { }

                    //    int intFormTypeID = 0;
                    //    string strDenominator = string.Empty;
                    //    string strInvoiceSymbol1 = string.Empty;
                    //    int intBranchID = 0;
                    //    string strINVOICENO = string.Empty;
                    //    string strFormTypeBranchID = "0";
                    //    int.TryParse(dtsFormType.Rows[0]["FORMTYPEID"].ToString(), out intFormTypeID);
                    //    int.TryParse(dtsFormType.Rows[0]["BRANCHID"].ToString(), out intBranchID);
                    //    strDenominator = dtsFormType.Rows[0]["Denominator"].ToString();
                    //    strInvoiceSymbol1 = dtsFormType.Rows[0]["InvoiceSymbol"].ToString();
                    //    strFormTypeBranchID = dtsFormType.Rows[0]["FormTypeBranchID"].ToString();
                    //    strINVOICENO = dtsFormType.Rows[0]["INVOICENO"].ToString();
                    //    ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = new MasterData.PLC.MD.WSCustomer.Customer();
                    //    new MasterData.PLC.MD.PLCCustomer().LoadInfo(ref objCustomer, objOutputVoucher.CustomerID);
                    //    objCustomer.CustomerID = objOutputVoucher.CustomerID;
                    //    objCustomer.CustomerName = objOutputVoucher.CustomerName;
                    //    objCustomer.TaxCustomerName = objOutputVoucher.CustomerName;
                    //    objCustomer.CustomerPhone = objOutputVoucher.CustomerPhone;
                    //    objCustomer.TaxCustomerAddress = string.IsNullOrWhiteSpace(objCustomer.TaxCustomerAddress) ? objOutputVoucher.CustomerAddress : objCustomer.TaxCustomerAddress;
                    //    objCustomer.CustomerAddress = objOutputVoucher.CustomerAddress;
                    //    objCustomer.CustomerTaxID = objOutputVoucher.CustomerTaxID;
                    //    DataTable dtbDataProduct = null;
                    //    object[] objsKeyword1 = new object[]
                    //    {
                    //        "@OutputVoucherID", objOutputVoucher.OutputVoucherID,
                    //        "@StoreID", objOutputVoucher.OutputStoreID
                    //    };
                    //    new PrintVAT.PLC.PrintVAT.PLCPrintVAT().GetDataProductInvoice(ref dtbDataProduct, objsKeyword1);
                    //    if (dtbDataProduct != null && dtbDataProduct.Rows.Count > 0)
                    //    {
                    //        string strAddress = string.Empty;
                    //        string strTax = string.Empty;
                    //        var objBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + intBranchID);
                    //        if (objBranch.Count() > 0)
                    //        {
                    //            strAddress = objBranch[0]["TAXADDRESS"].ToString();
                    //            strTax = objBranch[0]["TAXCODE"].ToString();
                    //        }
                    //        string strLstCustomerName = objOutputVoucher.CustomerPhone + "-" + objOutputVoucher.CustomerName;
                    //        PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT objfrmInvoiceVAT = new PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT(
                    //            dtbDataProduct, objCustomer, intBranchID, objOutputVoucher.OutputStoreID, strAddress, strTax, strDenominator,
                    //            strInvoiceSymbol1, strINVOICENO, intFormTypeID, objOutputVoucher.PayableTypeID, 1, strFormTypeBranchID
                    //            , objOutputVoucher.DiscountReasonID, objOutputVoucher.Discount, objOutputVoucher.StaffUser, strLstCustomerName);
                    //        if (objSaleOrderType != null)
                    //            objfrmInvoiceVAT.SaleOrderType = objSaleOrderType;
                    //        objfrmInvoiceVAT.ShowDialog();
                    //    }
                    //}
                    //else
                    //{
                    //    DataTable dtCheckHoldInvoiceNo = null;
                    //    #region Kiểm tra số hóa đơn giữ

                    //    if (SystemConfig.objSessionUser.IsPermission(strPermission_CreateInvoiceHold))
                    //    {
                    //        int intBranchIDCheck = 0;
                    //        DataTable dtbBranchTemp = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy();
                    //        if (dtbBranchTemp != null)
                    //        {
                    //            ERP.MasterData.PLC.MD.WSStore.Store objStore = ERP.MasterData.PLC.MD.PLCStore.GetinfoFromDataCache(objOutputVoucher.OutputStoreID);
                    //            if (objStore != null)
                    //            {
                    //                intBranchIDCheck = objStore.BranchID;
                    //            }
                    //        }
                    //        object[] objKeywords = new object[]{"@NEWOUTPUTVOUCHERID", strOutputVoucherID,
                    //                                            "@BRANCHID",intBranchIDCheck,
                    //                                            "@STOREID",objOutputVoucher.OutputStoreID,
                    //                                            "@TYPE", 1};
                    //        dtCheckHoldInvoiceNo = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("VAT_FORMTYPE_CHKHOLD", objKeywords);
                    //        if (dtCheckHoldInvoiceNo != null && dtCheckHoldInvoiceNo.Rows.Count > 0)
                    //        {
                    //            #region Tạo hóa đơn

                    //            int intInvoiceTransactionTypeID = 0;
                    //            ERP.MasterData.PLC.MD.WSSaleOrderType.SaleOrderType objSaleOrderType = null;
                    //            try
                    //            {
                    //                DataTable dtGetOldInvoice = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("PM_OUTPUTCHANGEORDERGETINVOICE", new object[] { "@OUTPUTVOUCHERID", strOutputVoucherID });
                    //                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    //                {
                    //                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    //                    SystemErrorWS.Insert("Lỗi khi kiểm tra thông tin!", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->mnuCreateInvoiceVAT_Click");
                    //                }
                    //                if (dtGetOldInvoice != null && dtGetOldInvoice.Rows.Count > 0)
                    //                {
                    //                    intInvoiceTransactionTypeID = Convert.ToInt32(dtGetOldInvoice.Rows[0]["INVOICETRANSACTIONTYPEID"]);
                    //                    objSaleOrderType = new MasterData.PLC.MD.WSSaleOrderType.SaleOrderType();
                    //                    objSaleOrderType.InvoiceTransTypeID = intInvoiceTransactionTypeID;
                    //                }
                    //            }
                    //            catch { }

                    //            int intFormTypeID = 0;
                    //            string strDenominator = string.Empty;
                    //            string strInvoiceSymbol1 = string.Empty;
                    //            int intCurrentNo = 0;
                    //            int intBranchID = 0;
                    //            string strINVOICENO = string.Empty;
                    //            string strFormTypeBranchID = "0";
                    //            int.TryParse(dtCheckHoldInvoiceNo.Rows[0]["FORMTYPEID"].ToString(), out intFormTypeID);
                    //            int.TryParse(dtCheckHoldInvoiceNo.Rows[0]["BRANCHID"].ToString(), out intBranchID);
                    //            strDenominator = dtCheckHoldInvoiceNo.Rows[0]["Denominator"].ToString();
                    //            strInvoiceSymbol1 = dtCheckHoldInvoiceNo.Rows[0]["InvoiceSymbol"].ToString();
                    //            //int.TryParse(dtCheckHoldInvoiceNo.Rows[0]["CURRENTNO"].ToString(), out intCurrentNo);
                    //            strFormTypeBranchID = dtCheckHoldInvoiceNo.Rows[0]["FormTypeBranchID"].ToString();
                    //            //strINVOICENO = dtCheckHoldInvoiceNo.Rows[0]["INVOICENO"].ToString();
                    //            ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = new MasterData.PLC.MD.WSCustomer.Customer();
                    //            objCustomer.CustomerID = objOutputVoucher.CustomerID;
                    //            objCustomer.CustomerName = objOutputVoucher.CustomerName;
                    //            objCustomer.TaxCustomerName = objOutputVoucher.CustomerName;
                    //            objCustomer.CustomerPhone = objOutputVoucher.CustomerPhone;
                    //            objCustomer.TaxCustomerAddress = objOutputVoucher.CustomerAddress;
                    //            objCustomer.CustomerAddress = objOutputVoucher.CustomerAddress;
                    //            objCustomer.CustomerTaxID = objOutputVoucher.CustomerTaxID;
                    //            DataTable dtbDataProduct = null;
                    //            object[] objsKeyword1 = new object[]
                    //    {
                    //        "@OutputVoucherID", objOutputVoucher.OutputVoucherID,
                    //        "@StoreID", objOutputVoucher.OutputStoreID
                    //    };
                    //            new PrintVAT.PLC.PrintVAT.PLCPrintVAT().GetDataProductInvoice(ref dtbDataProduct, objsKeyword1);
                    //            if (dtbDataProduct != null && dtbDataProduct.Rows.Count > 0)
                    //            {
                    //                string strAddress = string.Empty;
                    //                string strTax = string.Empty;
                    //                var objBranch = Library.AppCore.DataSource.GetDataSource.GetBranch().Copy().Select("BRANCHID = " + intBranchID);
                    //                if (objBranch.Count() > 0)
                    //                {
                    //                    strAddress = objBranch[0]["TAXADDRESS"].ToString();
                    //                    strTax = objBranch[0]["TAXCODE"].ToString();
                    //                }
                    //                string strLstCustomerName = objOutputVoucher.CustomerPhone + "-" + objOutputVoucher.CustomerName;
                    //                PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT objfrmInvoiceVAT = new PrintVAT.DUI.PrintVATViettel.frmInvoiceVAT(
                    //                    dtbDataProduct, objCustomer, intBranchID, objOutputVoucher.OutputStoreID, strAddress, strTax, strDenominator,
                    //                    strInvoiceSymbol1, strINVOICENO, intFormTypeID, objOutputVoucher.PayableTypeID, 1, strFormTypeBranchID
                    //                    , objOutputVoucher.DiscountReasonID, objOutputVoucher.Discount, objOutputVoucher.StaffUser, strLstCustomerName, "", null, "", "", Convert.ToDateTime(dtCheckHoldInvoiceNo.Rows[0]["INVOICEDATE"]));
                    //                if (objSaleOrderType != null)
                    //                    objfrmInvoiceVAT.SaleOrderType = objSaleOrderType;
                    //                objfrmInvoiceVAT.ShowDialog();
                    //            }

                    //            #endregion
                    //        }
                    //    }

                    //    #endregion

                    //    if (dtCheckHoldInvoiceNo == null || dtCheckHoldInvoiceNo.Rows.Count == 0)
                    //    {
                    //        DataTable dtbStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
                    //        var drStore = dtbStore.Select("STOREID=" + objOutputVoucher.OutputStoreID);
                    //        if (drStore.Any())
                    //        {
                    //            Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin thiết lập số hóa đơn cho kho xuất: " + objOutputVoucher.OutputStoreID + " - " + drStore[0]["STORENAME"].ToString() + "!");
                    //        }
                    //        else
                    //        {
                    //            Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy thông tin thiết lập số hóa đơn cho kho xuất!");
                    //        }
                    //    }
                    //}
                    #endregion
                }
            }
            btnSearch_Click(null, null);
        }
        /// <summary>
        /// Đăng bổ sung hàm insertVATInvoice
        /// <paramref name="intFormTypeID"/>
        /// <paramref name="strDenominator"/>
        /// <paramref name="strInvoiceSymbol"/>
        /// <paramref name="objOutputVoucher"/>
        /// </summary>
        private void InsertVATInvoice(ref PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVATInvoiceMain, ERP.SalesAndServices.SaleOrders.PLC.WSSaleOrders.SaleOrder objSaleOrder, int intFormTypeID, string strDenominator, string strInvoiceSymbol, int intBranchID, int intCurrentNo, string strFormTypeBranchID, ERP.Inventory.PLC.Output.WSOutputVoucher.OutputVoucher objOutputVoucher)
        {
            try
            {
                ERP.PrintVAT.PLC.WSPrintVAT.VAT_Invoice objVATInvoice = new ERP.PrintVAT.PLC.WSPrintVAT.VAT_Invoice();
                objVATInvoice.FormTypeID = intFormTypeID;//Mã thiết lập
                objVATInvoice.InvoiceDate = Globals.GetServerDateTime();
                objVATInvoice.Denominator = strDenominator;//Mẫu số
                objVATInvoice.InvoiceSymbol = strInvoiceSymbol;//ký hiệu
                objVATInvoice.InvoiceNO = intCurrentNo;
                objVATInvoice.SalemanID = objSaleOrder.StaffUser;
                ERP.MasterData.SYS.PLC.WSUser.User objUser = ERP.MasterData.SYS.PLC.PLCUser.LoadInfoFromCache(objSaleOrder.StaffUser.Trim());
                objVATInvoice.Saleman = objUser.FullName;

                objVATInvoice.CustomerID = objSaleOrder.CustomerID;
                objVATInvoice.CustomerName = objSaleOrder.CustomerName;
                objVATInvoice.Buyer = string.Empty;
                objVATInvoice.TaxCustomerAddress = objSaleOrder.CustomerAddress.Trim();
                objVATInvoice.CustomerTaxID = objSaleOrder.CustomerTaxID;
                objVATInvoice.CustomerPhone = objSaleOrder.CustomerPhone;
                objVATInvoice.PayableTypeID = objSaleOrder.PayableTypeID;
                objVATInvoice.Description = objSaleOrder.Content;

                objVATInvoice.CreatedStore = objSaleOrder.OutputStoreID;
                //objVATInvoice.TypeID = this.intTypeId;
                objVATInvoice.BranchID = intBranchID;
                objVATInvoice.INVOICETYPE = 0;
                objVATInvoice.VATINVOICEMAINID = "";
                objVATInvoice.InvoiceKind = 1;
                objVATInvoice.VATCustomerName = objSaleOrder.OrganizationName;
                objVATInvoice.Discount = objSaleOrder.Discount;
                objVATInvoice.DiscountReasonID = objSaleOrder.DiscountReasonID;
                List<ERP.PrintVAT.PLC.WSPrintVAT.VAT_Invoice_Product> lstProduct = new List<ERP.PrintVAT.PLC.WSPrintVAT.VAT_Invoice_Product>();
                DataTable dtbProdcut = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
                DataTable dtbProductNotax = new ERP.SalesAndServices.SaleOrders.PLC.PLCSaleOrders().GetAllProductNoTax().Copy();
                DataTable dtbOutputVoucherDetai = objPLCOutputVoucher.GetAll(objOutputVoucher.OutputVoucherID);
                foreach (DataRow item in dtbOutputVoucherDetai.Rows)
                {
                    ERP.PrintVAT.PLC.WSPrintVAT.VAT_Invoice_Product obj = new ERP.PrintVAT.PLC.WSPrintVAT.VAT_Invoice_Product();
                    obj.OutputVoucherDetailID = item["OutputVoucherDetailID"].ToString();
                    obj.MISAProductID = item["ProductID"].ToString();
                    obj.ProductID = item["ProductID"].ToString();
                    obj.Quantity = Convert.ToDecimal(item["Quantity"].ToString());
                    obj.PriceInvoice = Convert.ToDecimal(item["SalePrice"].ToString());
                    obj.VATInvoice = Convert.ToDecimal(item["VAT"].ToString());
                    if (dtbProductNotax != null && dtbProductNotax.Rows.Count > 0)
                    {
                        if (dtbProductNotax.AsEnumerable().Count(x => x.Field<string>("PRODUCTID").Trim() == obj.ProductID.Trim()) > 0)
                        {
                            obj.VATInvoice = -1;
                        }
                    }
                    DataRow[] drProduct = dtbProdcut.Select("ProductID='" + obj.ProductID + "'");
                    if (drProduct.Any())
                    {
                        int intQuantityUnitId = 0;
                        int.TryParse(drProduct[0]["QuantityUnitId"].ToString(), out intQuantityUnitId);
                        obj.QuantityUnitId = intQuantityUnitId;
                    }
                    obj.DifferenceAmount = 0;
                    obj.ISPROMOTIONPRODUCTMISA = false;
                    obj.APPLYMISAPRODUCTID = "";
                    obj.PROMOTIONMISADETAILID = "";
                    obj.OUTQuantity = 0;
                    obj.OUTQuantityOld = 0;
                    obj.ISPROMOTIONPRODUCTMISA = Convert.ToBoolean(item["IsPromotionProduct"]);
                    lstProduct.Add(obj);
                    //Cộng tiền chưa VAT
                    objVATInvoice.AmountBF += Convert.ToDecimal(item["SalePrice"].ToString()) * Convert.ToDecimal(item["Quantity"].ToString());
                    //Cộng tiền thuế VAT
                    objVATInvoice.VATAmount += Convert.ToDecimal(item["Quantity"].ToString()) * (Convert.ToDecimal(item["SalePrice"].ToString()) * (Convert.ToDecimal(item["VAT"].ToString()) * Convert.ToDecimal(item["VATPercent"].ToString())) / 10000);

                    //Cộng tiền có  VAT
                    objVATInvoice.TotalAmount += (Convert.ToDecimal(item["Quantity"].ToString()) * Convert.ToDecimal(item["SalePrice"].ToString())) + (Convert.ToDecimal(item["Quantity"].ToString()) * (Convert.ToDecimal(item["SalePrice"].ToString()) * (Convert.ToDecimal(item["VAT"].ToString()) * Convert.ToDecimal(item["VATPercent"].ToString())) / 10000));//item.TotalCost;

                    //Cộng tiền hóa đơn trước thuế
                    objVATInvoice.AmountInvoiceBF += Convert.ToDecimal(item["Quantity"].ToString()) * (Convert.ToDecimal(item["SalePrice"].ToString()) * (Convert.ToDecimal(item["VAT"].ToString()) * Convert.ToDecimal(item["VATPercent"].ToString())) / 10000);
                    //Cộng tiền thuế VAT hóa đơn
                    objVATInvoice.VATInvoice += Convert.ToDecimal(item["SalePrice"].ToString());

                    //Cộng tiền hóa đơn có VAT
                    objVATInvoice.TotalAmountInvoice += (Convert.ToDecimal(item["Quantity"].ToString()) * Convert.ToDecimal(item["SalePrice"].ToString())) + (Convert.ToDecimal(item["Quantity"].ToString()) * (Convert.ToDecimal(item["SalePrice"].ToString()) * (Convert.ToDecimal(item["VAT"].ToString()) * Convert.ToDecimal(item["VATPercent"].ToString())) / 10000));//item.TotalCost;
                }
                //Thuế suất hóa đơn GTGT
                objVATInvoice.InvoiceVAT = objSaleOrder.SaleOrderDetailList.Max(x => x.VAT);

                objVATInvoice.OutputvoucherInfo = objSaleOrder.CustomerPhone + "-" + objSaleOrder.CustomerName;
                objVATInvoice.VATInvoiceProduct = lstProduct.ToArray();
                string strVATInvoiceID = "";
                var objResultMessage = new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().Insert_SaleOrder(objVATInvoice, strFormTypeBranchID, ref strVATInvoiceID);
                if (objResultMessage.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, objResultMessage.MessageDetail);
                    SystemErrorWS.Insert("Lỗi khi thêm mơi hóa đơn GTGT!", objResultMessage.MessageDetail, DUIInventory_Globals.ModuleName + "->InsertVATInvoice");
                }
                new ERP.PrintVAT.PLC.PrintVAT.PLCPrintVAT().LoadInfo(ref objVATInvoiceMain, strVATInvoiceID);
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi thêm mơi hóa đơn GTGT!", objExce, DUIInventory_Globals.ModuleName + "->InsertVATInvoice");
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Lỗi khi thêm mơi hóa đơn GTGT");
            }
        }

        private DataTable dtbProduct = Library.AppCore.DataSource.GetDataSource.GetProduct();
        private PrintVAT.PLC.WSPrintVAT.VAT_PInvoice CreateNewInputVATInvoice(IWin32Window hwdOwner, MasterData.PLC.MD.WSInputType.InputType objInputType, PLC.Input.WSInputVoucher.InputVoucher objInputVoucher)
        {
            try
            {
                DataTable dtbInvoiceTransactionType = Library.AppCore.DataSource.GetDataSource.GetInvoiceTransactionType().Copy();
                DataRow[] rowGroup = dtbInvoiceTransactionType.Select("INVOICETRANSTYPEID=" + objInputType.InvoiceTransTypeID);
                bool bolIsTaxDeclaration = Convert.ToBoolean(rowGroup[0]["ISTAXDECLARATION"]);
                ERP.MasterData.PLC.MD.WSStore.Store objToStore = null;
                new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, objInputVoucher.InputStoreID);
                PrintVAT.PLC.WSPrintVAT.VAT_PInvoice objVATInvoice = new PrintVAT.PLC.WSPrintVAT.VAT_PInvoice();
                objVATInvoice.InvoiceDate = Globals.GetServerDateTime();
                objVATInvoice.FormTypeID = -1;
                objVATInvoice.Denominator = "";//Mẫu số
                objVATInvoice.InvoiceSymbol = "";//ký hiệu
                objVATInvoice.InvoiceNO = 0;
                objVATInvoice.PurchaseMANID = "";
                objVATInvoice.PurchaseMAN = "";
                ERP.MasterData.PLC.MD.WSCustomer.Customer objCustomer = ERP.MasterData.PLC.MD.PLCCustomer.LoadInfoFromCache(Convert.ToInt32(objInputVoucher.CustomerID));
                if (!string.IsNullOrWhiteSpace(objCustomer.TaxCustomerName))
                {
                    objCustomer.CustomerName = objCustomer.TaxCustomerName;
                }
                objVATInvoice.CustomerID = objCustomer.CustomerID;
                objVATInvoice.CustomerName = objCustomer.CustomerName;
                objVATInvoice.SELLER = objCustomer.ContactName;
                objVATInvoice.TaxCustomerAddress = objCustomer.TaxCustomerAddress.Length == 0 ? objCustomer.CustomerAddress : objCustomer.TaxCustomerAddress;
                objVATInvoice.CustomerTaxID = objCustomer.CustomerTaxID;
                objVATInvoice.CustomerPhone = objCustomer.CustomerPhone;
                objVATInvoice.VATCustomerName = objCustomer.TaxCustomerName;
                objVATInvoice.Description = "";

                objVATInvoice.CurrencyUnit = objInputVoucher.CurrencyUnitID;//Convert.ToInt32(cboCurrencyUnit.SelectedValue);
                objVATInvoice.CurrencyExchange = objInputVoucher.CurrencyExchange;//Convert.ToDecimal(txtExchangeRate.Text);

                objVATInvoice.InvoiceStoreID = objInputVoucher.InputStoreID;
                objVATInvoice.CreatedStoreID = Library.AppCore.SystemConfig.intDefaultStoreID;
                //objVATInvoice.TypeID = this.intTypeId;
                objVATInvoice.BranchID = objToStore.BranchID;
                objVATInvoice.InvoiceType = 0;
                objVATInvoice.VATInvoiceMainID = "";
                objVATInvoice.PayableTypeID = Convert.ToInt32(objInputVoucher.PayableTypeID);
                objVATInvoice.InvoiceDUEDate = objInputVoucher.PayableDate;
                objVATInvoice.TypeID = 2;
                objVATInvoice.InvoiceType = 8;
                objVATInvoice.InvoiceTRANSTypeID = objInputType.InvoiceTransTypeID;
                objVATInvoice.OutputVoucherInfo = objCustomer.CustomerPhone + " - " + objCustomer.CustomerName;
                List<PLC.Input.WSInputVoucher.VAT_Invoice_Product> lstProduct = new List<PLC.Input.WSInputVoucher.VAT_Invoice_Product>();
                var lst = objInputVoucher.InputVoucherDetailList;
                //Cộng tiền chưa VAT
                objVATInvoice.AmountBF = lst.Sum(x => x.InputPrice);
                //Cộng tiền thuế VAT
                objVATInvoice.VATAmount = lst.Sum(x => x.VAT);
                //Cộng tiền có  VAT
                objVATInvoice.TotalAmount = lst.Sum(x => x.Quantity * x.InputPrice + (x.Quantity * x.InputPrice * x.VAT / 100));
                //Cộng tiền hóa đơn trước thuế
                objVATInvoice.AmountInvoiceBF = lst.Sum(x => x.Quantity * x.InputPrice);
                //Cộng tiền thuế VAT hóa đơn
                objVATInvoice.VATInvoice = lst.Sum(x => (x.Quantity * x.InputPrice * x.VAT / 100));
                //Cộng tiền hóa đơn có VAT
                objVATInvoice.TotalAmountInvoice = lst.Sum(x => x.Quantity * x.InputPrice + (x.Quantity * x.InputPrice * x.VAT / 100));
                //Thuế suất hóa đơn GTGT
                objVATInvoice.InvoiceVAT = lst.Max(x => x.VAT);

                //// nap du lieu danh sach san pham 
                List<PrintVAT.PLC.WSPrintVAT.VAT_PInvoice_Product> lstProductInvoice = new List<PrintVAT.PLC.WSPrintVAT.VAT_PInvoice_Product>();
                foreach (var item in objInputVoucher.InputVoucherDetailList)
                {
                    PrintVAT.PLC.WSPrintVAT.VAT_PInvoice_Product obj = new PrintVAT.PLC.WSPrintVAT.VAT_PInvoice_Product();
                    obj.VATPInvoiceDTID = "";
                    obj.VATPInvoiceID = "";

                    obj.InputVoucherDetailID = item.InputVoucherDetailID == string.Empty ? "-1" : item.InputVoucherDetailID;
                    obj.OrderDetailId = item.OrderDetailID == string.Empty ? "-1" : item.OrderDetailID;

                    obj.ProductID = (item.ProductID ?? "").ToString();
                    obj.Quantity = Convert.ToDecimal(item.Quantity);
                    obj.PriceInvoice = Convert.ToDecimal(item.InputPrice);
                    obj.VATInvoice = Convert.ToDecimal(item.VAT);
                    DataRow[] objProduct = dtbProduct.Select("PRODUCTID = '" + item.ProductID + "'");
                    if (objProduct != null)
                    {
                        obj.QuantityUnitID = Convert.ToInt32(objProduct[0]["QUANTITYUNITID"]);
                    }

                    obj.DIFFERENCEAmount = 0;
                    if (bolIsTaxDeclaration)
                        obj.GroupServiceId = Convert.ToInt32(rowGroup[0]["GROUPSERVICEID"]);
                    else
                        obj.GroupServiceId = null;

                    lstProductInvoice.Add(obj);
                }
                //Thuế suất hóa đơn GTGT
                objVATInvoice.ProductList = lstProductInvoice.ToArray();
                return objVATInvoice;
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi thêm mới hóa đơn GTGT!", objExce, DUIInventory_Globals.ModuleName + "->InsertVATInvoice");
                MessageBoxObject.ShowWarningMessage(hwdOwner, "Lỗi khi thêm mơi hóa đơn GTGT");
                return null;
            }
        }
        #endregion

        private void GetParameterForm()
        {
            try
            {
                if (this.Tag == null || this.Tag.ToString() == string.Empty)
                {
                    IsViettel = false;
                    return;
                }
                Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                if (hstbParam.ContainsKey("COMPANY"))
                    strCompany = (hstbParam["COMPANY"] ?? "").ToString().Trim();
                if (string.IsNullOrEmpty(strCompany))
                {
                    IsViettel = false;
                }
                else if (strCompany.CompareTo("1") == 0)
                    IsViettel = true;
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi khi lấy tham số cấu hình form " + this.Text, objExce, "frmInputChangeOrderInput ->GetParameterForm");
                MessageBoxObject.ShowWarningMessage(this, "Lỗi khi lấy tham số cấu hình form " + this.Text);
                return;
            }
        }

        private void mnuItemPrintPOReturnHandovers_Click(object sender, EventArgs e)
        {
            try
            {
                if (grvData.FocusedRowHandle < 0)
                    return;
                DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
                string strOutputVoucherID = row["OUTPUTVOUCHERID"].ToString().Trim();
                if (!string.IsNullOrEmpty(strOutputVoucherID))
                {
                    PrintPoreturnHandOvers(strOutputVoucherID);
                }
            }
            catch
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi!");
            }
        }
        /// <summary>
        /// In biên bản bàn giao
        /// </summary>
        private void PrintPoreturnHandOvers(string strOutputVoucherID)
        {
            DataTable dtbReport = Library.AppCore.DataSource.GetDataSource.GetReport().Copy();
            var obj = dtbReport.Select("REPORTSTOREPROCNAME = 'RPT_PORETURN_HANDOVERS'");
            int intPrintStoreChangeHandovers = 0;
            if (obj.Any())
            {
                intPrintStoreChangeHandovers = Convert.ToInt32(obj[0]["REPORTID"]);
            }
            if (intPrintStoreChangeHandovers <= 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Chưa khai báo biên bản bàn giao xuất trả!");
                return;
            }
            ERP.MasterData.PLC.MD.WSReport.Report objReport = ERP.MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intPrintStoreChangeHandovers);
            try
            {

                object[] objKeywords = new object[] { "@OUTPUTVOUCHERID", strOutputVoucherID };
                DataTable dtbFull = new ERP.Report.PLC.PLCReportDataSource().GetHeavyDataSource("RPT_PORETURN_HANDOVERS", objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    SystemErrorWS.Insert(SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, DUIInventory_Globals.ModuleName + "->PrintPoreturnHandOvers");
                }
                if (dtbFull != null && dtbFull.Rows.Count > 0)
                {
                    dtbFull.AsEnumerable().OrderBy(x => x["PRODUCTID"].ToString().Trim()).OrderBy(y => y["IMEI"]);

                    //DataTable dtbReportHandover = null;
                    string strProductID = string.Empty;
                    int intSTT = 1;
                    for (int i = 0; i < dtbFull.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            strProductID = dtbFull.Rows[i]["PRODUCTID"].ToString().Trim();
                            dtbFull.Rows[i]["STT"] = intSTT;
                            //dtbReportHandover = dtbFull.Clone();
                            //dtbReportHandover.ImportRow(dtbFull.Rows[i]);

                        }
                        else
                        {
                            string strProductIDitem = dtbFull.Rows[i]["PRODUCTID"].ToString().Trim();
                            if (strProductID == strProductIDitem)
                            {
                                dtbFull.Rows[i]["PRODUCTNAME"] = string.Empty;
                                dtbFull.Rows[i]["QUANTITYUNITNAME"] = string.Empty;
                                dtbFull.Rows[i]["STT"] = -1;
                                //dtbReportHandover.ImportRow(dtbFull.Rows[i]);
                            }
                            else
                            {
                                intSTT++;
                                strProductID = strProductIDitem;
                                dtbFull.Rows[i]["STT"] = intSTT;
                            }
                        }
                    }
                }

                Hashtable htbParameterValue = new Hashtable();

                if (dtbFull != null && dtbFull.Rows.Count > 0)
                {
                    htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
                    htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
                    htbParameterValue.Add("CustomerName", dtbFull.Rows[0]["CUSTOMERNAME"].ToString().Trim());
                    htbParameterValue.Add("StoreName", dtbFull.Rows[0]["STORENAME"].ToString().Trim());
                    htbParameterValue.Add("StoreAddress", dtbFull.Rows[0]["STOREADDRESS"].ToString().Trim());
                    htbParameterValue.Add("PrintDate", Convert.ToDateTime(dtbFull.Rows[0]["CREATEDDATE"]));
                    htbParameterValue.Add("OutputNote", dtbFull.Rows[0]["OUTPUTNOTE"].ToString().Trim());
                    //htbParameterValue.Add("OutputVoucherID", dtbFull.Rows[0]["OUTPUTVOUCHERID"].ToString().Trim());
                }
                else
                {
                    htbParameterValue.Add("CompanyName", string.Empty);
                    htbParameterValue.Add("CompanyAddress", string.Empty);
                    htbParameterValue.Add("CustomerName", string.Empty);
                    htbParameterValue.Add("StoreName", string.Empty);
                    htbParameterValue.Add("StoreAddress", string.Empty);
                    htbParameterValue.Add("PrintDate", DateTime.Now);
                    //htbParameterValue.Add("ToUser", string.Empty);
                    htbParameterValue.Add("OutputNote", string.Empty);
                }
                htbParameterValue.Add("OutputVoucherID", strOutputVoucherID);

                objReport.DataTableSource = dtbFull;

                ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, htbParameterValue);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void cboBranch_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cboBranch.BranchIDList))
            {
                if (dtbStore != null)
                {
                    cboStoreIDList.InitControl(true, dtbStore, "STOREID", "STORENAME", "--Tất cả--");
                }
            }
            else
            {
                var lst = dtbStore.Select("'" + cboBranch.BranchIDList + "' LIKE '%<' + BRANCHID + '>%'");
                if (lst.Any())
                {
                    DataTable dtb = lst.CopyToDataTable();
                    cboStoreIDList.InitControl(true, dtb, "STOREID", "STORENAME", "--Tất cả--");
                }
            }
        }

        private void cboDataReport_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(cboDataReport.ColumnIDList))
            {
                cboOutputType.InitControl(true, dtbOutputTypeAll);
                cboOutputType.IsReturnAllWhenNotChoose = true;
            }
            else
            {
                object[] objKeywords = new object[] { "@DATAREPORTTYPEIDLIST", cboDataReport.ColumnIDList };
                DataTable dtbOutPutType = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_DATAREPORTTYPE_OUTTYPE_GET", objKeywords);
                if (dtbOutPutType != null)
                {
                    cboOutputType.InitControl(true, dtbOutPutType);
                    cboOutputType.IsReturnAllWhenNotChoose = true;
                }
            }
        }

        private void mnuItemPrintOutputStoreGroupBy_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            if (intReportID < 1)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            String strOutputVoucherID = Convert.ToString(row["OutputVoucherID"]).Trim();
            Hashtable hstbReportParam = new Hashtable();
            bool bolIsViewCostPrice = SystemConfig.objSessionUser.IsPermission("OUTPUTREPORT_VIEWCOSTPRICE");
            hstbReportParam.Add("OutputVoucherID", strOutputVoucherID);
            hstbReportParam.Add("IsCostPriceHide", !bolIsViewCostPrice);
            hstbReportParam.Add("IsSalePriceHide", false);
            MasterData.PLC.MD.PLCReport objPLCReport = new MasterData.PLC.MD.PLCReport();
            ERP.MasterData.PLC.MD.WSReport.Report objReport = new MasterData.PLC.MD.WSReport.Report();
            objPLCReport.LoadInfo(ref objReport, intReportGroupbyID);
            ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport, hstbReportParam);
        }

        private void grvData_CellMerge(object sender, DevExpress.XtraGrid.Views.Grid.CellMergeEventArgs e)
        {
            string columnHandle = e.Column.FieldName;

            if (columnHandle == "OUTPUTVOUCHERID"
                || columnHandle == "ORDERID"
                || columnHandle == "PRODUCTID"
                || columnHandle == "TOTALVAT"
                || columnHandle == "TOTALAMOUNT"
                )
            {
                #region
                DataRow drRow1 = grvData.GetDataRow(e.RowHandle1);
                string value1 = (drRow1[e.Column.FieldName] ?? "").ToString().Trim();

                DataRow drRow2 = grvData.GetDataRow(e.RowHandle2);
                string value2 = (drRow2[e.Column.FieldName] ?? "").ToString().Trim();


                string outputvoucher1 = drRow1["OUTPUTVOUCHERID"].ToString().Trim(),
                    outputvoucher2 = drRow2["OUTPUTVOUCHERID"].ToString().Trim();

                bool isEqualOutputvoucherid = outputvoucher1.Equals(outputvoucher2);

                string orderid1 = drRow1["ORDERID"].ToString().Trim(),
                    orderid2 = drRow2["ORDERID"].ToString().Trim();

                bool isEqualOrderID = orderid1.Equals(orderid2);

                string productid1 = drRow1["PRODUCTID"].ToString().Trim(),
                    productid2 = drRow2["PRODUCTID"].ToString().Trim();

                bool isEqualValue = value1.Equals(value2);


                bool isEqualProductID = productid1.Equals(productid2);
                #endregion


                #region
                if ((columnHandle == "OUTPUTVOUCHERID" ||
                    columnHandle == "ORDERID") && isEqualValue)
                {
                    e.Merge = true;
                    e.Handled = true;
                }
                else if (columnHandle == "PRODUCTID" &&
                    isEqualOutputvoucherid && isEqualOrderID && isEqualProductID)
                {
                    e.Merge = true;
                    e.Handled = true;
                }
                else if ((columnHandle == "TOTALVAT" || columnHandle == "TOTALAMOUNT") &&
                   isEqualOutputvoucherid && isEqualOrderID &&
                   isEqualProductID && isEqualValue)
                {
                    e.Merge = true;
                    e.Handled = true;
                }
                else
                {
                    e.Merge = false;
                    e.Handled = true;
                }
                #endregion
                return;
            }
            e.Merge = false;
            e.Handled = true;
        }

        private void grvData_CustomSummaryCalculate(object sender, DevExpress.Data.CustomSummaryEventArgs e)
        {
           if(e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Finalize )
            {
                DevExpress.XtraGrid.GridSummaryItem iTem = (DevExpress.XtraGrid.GridSummaryItem)e.Item;
                if(iTem.FieldName== "TOTALVAT")
                    e.TotalValue = totalVAT;
                else if (iTem.FieldName == "TOTALAMOUNT")
                    e.TotalValue = totalAmount;
            }
        }

        private void CaclMoney(DataTable dtbInput, ref decimal vat, ref decimal total)
        {
            if (dtbInput == null || dtbInput.Rows.Count < 1) return;
            DataTable tem = new DataTable();
            tem = dtbInput.DefaultView.ToTable(true, "OUTPUTVOUCHERID", "ORDERID", "PRODUCTID", "TOTALVAT", "TOTALAMOUNT");
            vat = 0;
            total = 0;
            foreach (DataRow it in tem.Rows)
            {
                vat += Convert.ToDecimal(it["TOTALVAT"]);
                total += Convert.ToDecimal(it["TOTALAMOUNT"]);
            }
        }

    }
}
