﻿namespace ERP.Inventory.DUI.Output
{
    partial class frmOutputVoucherManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOutputVoucherManager));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cboDataReport = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label4 = new System.Windows.Forms.Label();
            this.cboBranch = new Library.AppControl.ComboBoxControl.ComboBoxBranch();
            this.label2 = new System.Windows.Forms.Label();
            this.chkIsViewDetail = new System.Windows.Forms.CheckBox();
            this.txtTop = new DevExpress.XtraEditors.TextEdit();
            this.cboOutputType = new Library.AppControl.ComboBoxControl.ComboBoxOutputType();
            this.cboCustomer = new Library.AppControl.ComboBoxControl.ComboBoxCustomer();
            this.btnExport = new System.Windows.Forms.Button();
            this.cboStoreIDList = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.cboErrorType = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cboSearchType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.mnuContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemInputVoucherReturn = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemInputReturnWithFee = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemInputChange = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuInputChangeOrderInput = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuInputChangeOrderInputHasError = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCreateInvoiceVAT = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemPrintOutputStoreGroupBy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintOutputStore = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintOutputVoucher = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintInputChange = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemViewInputProductChange = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemViewInputVoucher = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuIMEIChange = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemPrintPOReturnHandovers = new System.Windows.Forms.ToolStripMenuItem();
            this.grvData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOutputVoucherID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.QUANTITY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutputTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceSysbol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutputDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutputUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserOutput = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutputContent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOutputNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsInputReturnChangeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repIsInputReturnChangeOrder = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            this.mnuContext.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsInputReturnChangeOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseImage = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.Controls.Add(this.cboDataReport);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.cboBranch);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.chkIsViewDetail);
            this.groupControl1.Controls.Add(this.txtTop);
            this.groupControl1.Controls.Add(this.cboOutputType);
            this.groupControl1.Controls.Add(this.cboCustomer);
            this.groupControl1.Controls.Add(this.btnExport);
            this.groupControl1.Controls.Add(this.cboStoreIDList);
            this.groupControl1.Controls.Add(this.btnSearch);
            this.groupControl1.Controls.Add(this.dtpToDate);
            this.groupControl1.Controls.Add(this.dtpFromDate);
            this.groupControl1.Controls.Add(this.txtKeywords);
            this.groupControl1.Controls.Add(this.cboErrorType);
            this.groupControl1.Controls.Add(this.label14);
            this.groupControl1.Controls.Add(this.cboSearchType);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.label12);
            this.groupControl1.Controls.Add(this.label9);
            this.groupControl1.Controls.Add(this.label8);
            this.groupControl1.Controls.Add(this.label10);
            this.groupControl1.Controls.Add(this.label11);
            this.groupControl1.Controls.Add(this.label13);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.SkinName = "Blue";
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1030, 140);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin tìm kiếm";
            // 
            // cboDataReport
            // 
            this.cboDataReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDataReport.Location = new System.Drawing.Point(101, 82);
            this.cboDataReport.Margin = new System.Windows.Forms.Padding(0);
            this.cboDataReport.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboDataReport.Name = "cboDataReport";
            this.cboDataReport.Size = new System.Drawing.Size(225, 24);
            this.cboDataReport.TabIndex = 15;
            this.cboDataReport.SelectionChangeCommitted += new System.EventHandler(this.cboDataReport_SelectionChangeCommitted);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 87);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 16);
            this.label4.TabIndex = 14;
            this.label4.Text = "Nhóm dữ liệu:";
            // 
            // cboBranch
            // 
            this.cboBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBranch.Location = new System.Drawing.Point(101, 28);
            this.cboBranch.Margin = new System.Windows.Forms.Padding(0);
            this.cboBranch.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.Size = new System.Drawing.Size(225, 24);
            this.cboBranch.TabIndex = 1;
            this.cboBranch.SelectionChangeCommitted += new System.EventHandler(this.cboBranch_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Chi nhánh:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkIsViewDetail
            // 
            this.chkIsViewDetail.AutoSize = true;
            this.chkIsViewDetail.Location = new System.Drawing.Point(413, 113);
            this.chkIsViewDetail.Name = "chkIsViewDetail";
            this.chkIsViewDetail.Size = new System.Drawing.Size(94, 20);
            this.chkIsViewDetail.TabIndex = 20;
            this.chkIsViewDetail.Text = "Xem chi tiết";
            this.chkIsViewDetail.UseVisualStyleBackColor = true;
            // 
            // txtTop
            // 
            this.txtTop.Location = new System.Drawing.Point(85, 118);
            this.txtTop.Name = "txtTop";
            this.txtTop.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtTop.Properties.Appearance.Options.UseFont = true;
            this.txtTop.Properties.Mask.EditMask = "[0-9]{1,5}";
            this.txtTop.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTop.Properties.NullText = "0";
            this.txtTop.Size = new System.Drawing.Size(117, 22);
            this.txtTop.TabIndex = 18;
            this.txtTop.Visible = false;
            // 
            // cboOutputType
            // 
            this.cboOutputType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboOutputType.Location = new System.Drawing.Point(413, 82);
            this.cboOutputType.Margin = new System.Windows.Forms.Padding(0);
            this.cboOutputType.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboOutputType.Name = "cboOutputType";
            this.cboOutputType.Size = new System.Drawing.Size(225, 24);
            this.cboOutputType.TabIndex = 17;
            // 
            // cboCustomer
            // 
            this.cboCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCustomer.IsActivatedType = Library.AppCore.Constant.EnumType.IsActivatedType.ACTIVATED;
            this.cboCustomer.IsRequireActive = false;
            this.cboCustomer.Location = new System.Drawing.Point(733, 28);
            this.cboCustomer.Margin = new System.Windows.Forms.Padding(0);
            this.cboCustomer.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboCustomer.Name = "cboCustomer";
            this.cboCustomer.Size = new System.Drawing.Size(225, 24);
            this.cboCustomer.TabIndex = 5;
            // 
            // btnExport
            // 
            this.btnExport.Image = ((System.Drawing.Image)(resources.GetObject("btnExport.Image")));
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExport.Location = new System.Drawing.Point(850, 109);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(108, 26);
            this.btnExport.TabIndex = 22;
            this.btnExport.Text = "       Xuất Excel";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // cboStoreIDList
            // 
            this.cboStoreIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreIDList.Location = new System.Drawing.Point(413, 28);
            this.cboStoreIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreIDList.Name = "cboStoreIDList";
            this.cboStoreIDList.Size = new System.Drawing.Size(225, 24);
            this.cboStoreIDList.TabIndex = 3;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(733, 109);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(117, 26);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(214, 57);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(112, 22);
            this.dtpToDate.TabIndex = 9;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(101, 57);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(112, 22);
            this.dtpFromDate.TabIndex = 8;
            // 
            // txtKeywords
            // 
            this.txtKeywords.Location = new System.Drawing.Point(413, 57);
            this.txtKeywords.MaxLength = 200;
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.Size = new System.Drawing.Size(225, 22);
            this.txtKeywords.TabIndex = 11;
            this.txtKeywords.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeywords_KeyPress);
            // 
            // cboErrorType
            // 
            this.cboErrorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboErrorType.DropDownWidth = 250;
            this.cboErrorType.Items.AddRange(new object[] {
            "--Tất cả--",
            "Đã xử lý",
            "Chưa xử lý",
            "Không lỗi"});
            this.cboErrorType.Location = new System.Drawing.Point(733, 82);
            this.cboErrorType.Name = "cboErrorType";
            this.cboErrorType.Size = new System.Drawing.Size(225, 24);
            this.cboErrorType.TabIndex = 19;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 122);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 16);
            this.label14.TabIndex = 17;
            this.label14.Text = "Top:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label14.Visible = false;
            // 
            // cboSearchType
            // 
            this.cboSearchType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchType.DropDownWidth = 250;
            this.cboSearchType.Items.AddRange(new object[] {
            "-- Chọn tìm theo --",
            "Mã phiếu xuất",
            "Mã đơn hàng",
            "Số hóa đơn",
            "Mã sản phẩm",
            "IMEI",
            "Mã nhân viên",
            "Tên nhân viên",
            "Tên khách hàng",
            "SĐT khách hàng"});
            this.cboSearchType.Location = new System.Drawing.Point(733, 55);
            this.cboSearchType.Name = "cboSearchType";
            this.cboSearchType.Size = new System.Drawing.Size(225, 24);
            this.cboSearchType.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(649, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 16);
            this.label1.TabIndex = 12;
            this.label1.Text = "Tìm theo:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(649, 87);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 16);
            this.label12.TabIndex = 18;
            this.label12.Text = "Trạng thái:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(334, 59);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 16);
            this.label9.TabIndex = 10;
            this.label9.Text = "Chuỗi tìm:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "Ngày xuất:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(334, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 16);
            this.label10.TabIndex = 16;
            this.label10.Text = "Loại phiếu:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(334, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 16);
            this.label11.TabIndex = 2;
            this.label11.Text = "Kho xuất:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(649, 32);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 16);
            this.label13.TabIndex = 4;
            this.label13.Text = "Khách hàng:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grdData
            // 
            this.grdData.ContextMenuStrip = this.mnuContext;
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(0, 140);
            this.grdData.MainView = this.grvData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1,
            this.repIsInputReturnChangeOrder});
            this.grdData.Size = new System.Drawing.Size(1030, 382);
            this.grdData.TabIndex = 1;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvData});
            this.grdData.DoubleClick += new System.EventHandler(this.grdData_DoubleClick);
            // 
            // mnuContext
            // 
            this.mnuContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemInputVoucherReturn,
            this.mnuItemInputReturnWithFee,
            this.toolStripSeparator1,
            this.mnuItemInputChange,
            this.toolStripSeparator2,
            this.mnuInputChangeOrderInput,
            this.mnuInputChangeOrderInputHasError,
            this.mnuCreateInvoiceVAT,
            this.toolStripSeparator3,
            this.mnuItemPrintOutputStoreGroupBy,
            this.mnuItemPrintOutputStore,
            this.mnuItemPrintOutputVoucher,
            this.mnuItemPrintInputChange,
            this.mnuItemViewInputProductChange,
            this.mnuItemViewInputVoucher,
            this.toolStripSeparator4,
            this.mnuIMEIChange,
            this.mnuItemPrintPOReturnHandovers});
            this.mnuContext.Name = "mnuContext";
            this.mnuContext.Size = new System.Drawing.Size(292, 336);
            this.mnuContext.Opening += new System.ComponentModel.CancelEventHandler(this.mnuContext_Opening);
            // 
            // mnuItemInputVoucherReturn
            // 
            this.mnuItemInputVoucherReturn.Enabled = false;
            this.mnuItemInputVoucherReturn.Name = "mnuItemInputVoucherReturn";
            this.mnuItemInputVoucherReturn.Size = new System.Drawing.Size(291, 22);
            this.mnuItemInputVoucherReturn.Text = "Nhập trả hàng";
            this.mnuItemInputVoucherReturn.Visible = false;
            this.mnuItemInputVoucherReturn.Click += new System.EventHandler(this.mnuItemInputVoucherReturn_Click);
            // 
            // mnuItemInputReturnWithFee
            // 
            this.mnuItemInputReturnWithFee.Enabled = false;
            this.mnuItemInputReturnWithFee.Name = "mnuItemInputReturnWithFee";
            this.mnuItemInputReturnWithFee.Size = new System.Drawing.Size(291, 22);
            this.mnuItemInputReturnWithFee.Text = "Nhập trả hàng có thu phí";
            this.mnuItemInputReturnWithFee.Visible = false;
            this.mnuItemInputReturnWithFee.Click += new System.EventHandler(this.mnuItemInputReturnWithFee_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(288, 6);
            this.toolStripSeparator1.Visible = false;
            // 
            // mnuItemInputChange
            // 
            this.mnuItemInputChange.Enabled = false;
            this.mnuItemInputChange.Name = "mnuItemInputChange";
            this.mnuItemInputChange.Size = new System.Drawing.Size(291, 22);
            this.mnuItemInputChange.Text = "Nhập đổi hàng";
            this.mnuItemInputChange.Visible = false;
            this.mnuItemInputChange.Click += new System.EventHandler(this.mnuItemInputChange_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(288, 6);
            this.toolStripSeparator2.Visible = false;
            // 
            // mnuInputChangeOrderInput
            // 
            this.mnuInputChangeOrderInput.Name = "mnuInputChangeOrderInput";
            this.mnuInputChangeOrderInput.Size = new System.Drawing.Size(291, 22);
            this.mnuInputChangeOrderInput.Text = "Tạo yêu cầu đổi trả (Không lỗi)";
            this.mnuInputChangeOrderInput.Visible = false;
            this.mnuInputChangeOrderInput.Click += new System.EventHandler(this.mnuInputChangeOrderInput_Click);
            // 
            // mnuInputChangeOrderInputHasError
            // 
            this.mnuInputChangeOrderInputHasError.Name = "mnuInputChangeOrderInputHasError";
            this.mnuInputChangeOrderInputHasError.Size = new System.Drawing.Size(291, 22);
            this.mnuInputChangeOrderInputHasError.Text = "Tạo yêu cầu đổi trả (Máy lỗi)";
            this.mnuInputChangeOrderInputHasError.Visible = false;
            this.mnuInputChangeOrderInputHasError.Click += new System.EventHandler(this.mnuInputChangeOrderInputHasError_Click);
            // 
            // mnuCreateInvoiceVAT
            // 
            this.mnuCreateInvoiceVAT.Name = "mnuCreateInvoiceVAT";
            this.mnuCreateInvoiceVAT.Size = new System.Drawing.Size(291, 22);
            this.mnuCreateInvoiceVAT.Text = "Tạo hóa đơn GTGT";
            this.mnuCreateInvoiceVAT.Click += new System.EventHandler(this.mnuCreateInvoiceVAT_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(288, 6);
            this.toolStripSeparator3.Visible = false;
            // 
            // mnuItemPrintOutputStoreGroupBy
            // 
            this.mnuItemPrintOutputStoreGroupBy.Enabled = false;
            this.mnuItemPrintOutputStoreGroupBy.Name = "mnuItemPrintOutputStoreGroupBy";
            this.mnuItemPrintOutputStoreGroupBy.Size = new System.Drawing.Size(291, 22);
            this.mnuItemPrintOutputStoreGroupBy.Text = "In phiếu xuất kho hàng hóa";
            this.mnuItemPrintOutputStoreGroupBy.Click += new System.EventHandler(this.mnuItemPrintOutputStoreGroupBy_Click);
            // 
            // mnuItemPrintOutputStore
            // 
            this.mnuItemPrintOutputStore.Enabled = false;
            this.mnuItemPrintOutputStore.Name = "mnuItemPrintOutputStore";
            this.mnuItemPrintOutputStore.Size = new System.Drawing.Size(291, 22);
            this.mnuItemPrintOutputStore.Text = "In phiếu xuất kho hàng hóa chi tiết";
            this.mnuItemPrintOutputStore.Click += new System.EventHandler(this.mnuItemPrintOutputStore_Click);
            // 
            // mnuItemPrintOutputVoucher
            // 
            this.mnuItemPrintOutputVoucher.Enabled = false;
            this.mnuItemPrintOutputVoucher.Name = "mnuItemPrintOutputVoucher";
            this.mnuItemPrintOutputVoucher.Size = new System.Drawing.Size(291, 22);
            this.mnuItemPrintOutputVoucher.Text = "In phiếu xuất hàng";
            this.mnuItemPrintOutputVoucher.Click += new System.EventHandler(this.mnuItemPrintOutputVoucher_Click);
            // 
            // mnuItemPrintInputChange
            // 
            this.mnuItemPrintInputChange.Name = "mnuItemPrintInputChange";
            this.mnuItemPrintInputChange.Size = new System.Drawing.Size(291, 22);
            this.mnuItemPrintInputChange.Text = "In phiếu nhập đổi";
            this.mnuItemPrintInputChange.Click += new System.EventHandler(this.mnuItemPrintInputChange_Click);
            // 
            // mnuItemViewInputProductChange
            // 
            this.mnuItemViewInputProductChange.Name = "mnuItemViewInputProductChange";
            this.mnuItemViewInputProductChange.Size = new System.Drawing.Size(291, 22);
            this.mnuItemViewInputProductChange.Text = "Xem phiếu nhập từ Xuất đổi hàng";
            this.mnuItemViewInputProductChange.Click += new System.EventHandler(this.mnuItemViewInputProductChange_Click);
            // 
            // mnuItemViewInputVoucher
            // 
            this.mnuItemViewInputVoucher.Name = "mnuItemViewInputVoucher";
            this.mnuItemViewInputVoucher.Size = new System.Drawing.Size(291, 22);
            this.mnuItemViewInputVoucher.Text = "Xem phiếu nhập đổi hàng bán tại siêu thị";
            this.mnuItemViewInputVoucher.Click += new System.EventHandler(this.mnuItemViewInputVoucher_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(288, 6);
            // 
            // mnuIMEIChange
            // 
            this.mnuIMEIChange.Name = "mnuIMEIChange";
            this.mnuIMEIChange.Size = new System.Drawing.Size(291, 22);
            this.mnuIMEIChange.Text = "Đổi IMEI";
            this.mnuIMEIChange.Click += new System.EventHandler(this.mnuIMEIChange_Click);
            // 
            // mnuItemPrintPOReturnHandovers
            // 
            this.mnuItemPrintPOReturnHandovers.Name = "mnuItemPrintPOReturnHandovers";
            this.mnuItemPrintPOReturnHandovers.Size = new System.Drawing.Size(291, 22);
            this.mnuItemPrintPOReturnHandovers.Text = "In biên bản bàn giao xuất trả NCC";
            this.mnuItemPrintPOReturnHandovers.Click += new System.EventHandler(this.mnuItemPrintPOReturnHandovers_Click);
            // 
            // grvData
            // 
            this.grvData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvData.Appearance.FocusedRow.Options.UseFont = true;
            this.grvData.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvData.Appearance.FooterPanel.Options.UseFont = true;
            this.grvData.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grvData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.grvData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvData.Appearance.Preview.Options.UseFont = true;
            this.grvData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvData.Appearance.Row.Options.UseFont = true;
            this.grvData.ColumnPanelRowHeight = 50;
            this.grvData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOutputVoucherID,
            this.colOrderID,
            this.colProductID,
            this.colProductName,
            this.colIMEI,
            this.QUANTITY,
            this.colOutputTypeName,
            this.colCustomer,
            this.colInvoiceID,
            this.colInvoiceSysbol,
            this.colInvoiceDate,
            this.colOutputDate,
            this.colStore,
            this.colOutputUser,
            this.colUserOutput,
            this.colUserStaff,
            this.colStaffUser,
            this.colDiscount,
            this.colTotalVAT,
            this.colTotalAmount,
            this.colOutputContent,
            this.colOutputNote,
            this.colIsInputReturnChangeOrder});
            this.grvData.GridControl = this.grdData;
            this.grvData.Name = "grvData";
            this.grvData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grvData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grvData.OptionsView.AllowCellMerge = true;
            this.grvData.OptionsView.ColumnAutoWidth = false;
            this.grvData.OptionsView.ShowAutoFilterRow = true;
            this.grvData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvData.OptionsView.ShowFooter = true;
            this.grvData.OptionsView.ShowGroupPanel = false;
            this.grvData.CellMerge += new DevExpress.XtraGrid.Views.Grid.CellMergeEventHandler(this.grvData_CellMerge);
            this.grvData.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.grvData_CustomSummaryCalculate);
            // 
            // colOutputVoucherID
            // 
            this.colOutputVoucherID.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutputVoucherID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutputVoucherID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOutputVoucherID.Caption = "Mã phiếu xuất";
            this.colOutputVoucherID.FieldName = "OUTPUTVOUCHERID";
            this.colOutputVoucherID.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colOutputVoucherID.Name = "colOutputVoucherID";
            this.colOutputVoucherID.OptionsColumn.AllowEdit = false;
            this.colOutputVoucherID.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colOutputVoucherID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colOutputVoucherID.OptionsColumn.ReadOnly = true;
            this.colOutputVoucherID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colOutputVoucherID.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "OUTPUTVOUCHERID", "Tổng cộng:")});
            this.colOutputVoucherID.Visible = true;
            this.colOutputVoucherID.VisibleIndex = 0;
            this.colOutputVoucherID.Width = 120;
            // 
            // colOrderID
            // 
            this.colOrderID.AppearanceHeader.Options.UseTextOptions = true;
            this.colOrderID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOrderID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOrderID.Caption = "Mã đơn hàng";
            this.colOrderID.FieldName = "ORDERID";
            this.colOrderID.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colOrderID.Name = "colOrderID";
            this.colOrderID.OptionsColumn.AllowEdit = false;
            this.colOrderID.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colOrderID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colOrderID.OptionsColumn.ReadOnly = true;
            this.colOrderID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colOrderID.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "OUTPUTVOUCHERID", "{0:N0}")});
            this.colOrderID.Visible = true;
            this.colOrderID.VisibleIndex = 1;
            this.colOrderID.Width = 140;
            // 
            // colProductID
            // 
            this.colProductID.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProductID.Caption = "Mã sản phẩm";
            this.colProductID.FieldName = "PRODUCTID";
            this.colProductID.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colProductID.Name = "colProductID";
            this.colProductID.OptionsColumn.AllowEdit = false;
            this.colProductID.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colProductID.OptionsColumn.ReadOnly = true;
            this.colProductID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colProductID.Visible = true;
            this.colProductID.VisibleIndex = 2;
            this.colProductID.Width = 120;
            // 
            // colProductName
            // 
            this.colProductName.AppearanceHeader.Options.UseTextOptions = true;
            this.colProductName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colProductName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colProductName.Caption = "Tên sản phẩm";
            this.colProductName.FieldName = "PRODUCTNAME";
            this.colProductName.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colProductName.Name = "colProductName";
            this.colProductName.OptionsColumn.AllowEdit = false;
            this.colProductName.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colProductName.OptionsColumn.ReadOnly = true;
            this.colProductName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 3;
            this.colProductName.Width = 220;
            // 
            // colIMEI
            // 
            this.colIMEI.AppearanceHeader.Options.UseTextOptions = true;
            this.colIMEI.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIMEI.Caption = "IMEI";
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsColumn.AllowEdit = false;
            this.colIMEI.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colIMEI.OptionsColumn.ReadOnly = true;
            this.colIMEI.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 7;
            this.colIMEI.Width = 120;
            // 
            // QUANTITY
            // 
            this.QUANTITY.Caption = "Số Lượng";
            this.QUANTITY.FieldName = "QUANTITY";
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.QUANTITY.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QUANTITY", "{0:#,##0}")});
            this.QUANTITY.Visible = true;
            this.QUANTITY.VisibleIndex = 4;
            // 
            // colOutputTypeName
            // 
            this.colOutputTypeName.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutputTypeName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutputTypeName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOutputTypeName.Caption = "Hình thức xuất";
            this.colOutputTypeName.FieldName = "OUTPUTTYPENAME";
            this.colOutputTypeName.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colOutputTypeName.Name = "colOutputTypeName";
            this.colOutputTypeName.OptionsColumn.AllowEdit = false;
            this.colOutputTypeName.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOutputTypeName.OptionsColumn.ReadOnly = true;
            this.colOutputTypeName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colOutputTypeName.Visible = true;
            this.colOutputTypeName.VisibleIndex = 5;
            this.colOutputTypeName.Width = 220;
            // 
            // colCustomer
            // 
            this.colCustomer.AppearanceHeader.Options.UseTextOptions = true;
            this.colCustomer.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCustomer.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCustomer.Caption = "Khách hàng";
            this.colCustomer.FieldName = "CUSTOMERNAME";
            this.colCustomer.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colCustomer.Name = "colCustomer";
            this.colCustomer.OptionsColumn.AllowEdit = false;
            this.colCustomer.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colCustomer.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colCustomer.OptionsColumn.ReadOnly = true;
            this.colCustomer.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colCustomer.Visible = true;
            this.colCustomer.VisibleIndex = 10;
            this.colCustomer.Width = 150;
            // 
            // colInvoiceID
            // 
            this.colInvoiceID.AppearanceHeader.Options.UseTextOptions = true;
            this.colInvoiceID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInvoiceID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInvoiceID.Caption = "Số hóa đơn";
            this.colInvoiceID.FieldName = "INVOICEID";
            this.colInvoiceID.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colInvoiceID.Name = "colInvoiceID";
            this.colInvoiceID.OptionsColumn.AllowEdit = false;
            this.colInvoiceID.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colInvoiceID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colInvoiceID.OptionsColumn.ReadOnly = true;
            this.colInvoiceID.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colInvoiceID.Visible = true;
            this.colInvoiceID.VisibleIndex = 6;
            this.colInvoiceID.Width = 80;
            // 
            // colInvoiceSysbol
            // 
            this.colInvoiceSysbol.AppearanceHeader.Options.UseTextOptions = true;
            this.colInvoiceSysbol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInvoiceSysbol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInvoiceSysbol.Caption = "Ký hiệu HĐ";
            this.colInvoiceSysbol.FieldName = "INVOICESYMBOL";
            this.colInvoiceSysbol.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colInvoiceSysbol.Name = "colInvoiceSysbol";
            this.colInvoiceSysbol.OptionsColumn.AllowEdit = false;
            this.colInvoiceSysbol.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colInvoiceSysbol.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colInvoiceSysbol.OptionsColumn.ReadOnly = true;
            this.colInvoiceSysbol.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colInvoiceSysbol.Visible = true;
            this.colInvoiceSysbol.VisibleIndex = 12;
            this.colInvoiceSysbol.Width = 80;
            // 
            // colInvoiceDate
            // 
            this.colInvoiceDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colInvoiceDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInvoiceDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInvoiceDate.Caption = "Ngày HĐ";
            this.colInvoiceDate.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colInvoiceDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colInvoiceDate.FieldName = "INVOICEDATE";
            this.colInvoiceDate.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colInvoiceDate.Name = "colInvoiceDate";
            this.colInvoiceDate.OptionsColumn.AllowEdit = false;
            this.colInvoiceDate.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colInvoiceDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colInvoiceDate.OptionsColumn.ReadOnly = true;
            this.colInvoiceDate.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colInvoiceDate.Visible = true;
            this.colInvoiceDate.VisibleIndex = 13;
            this.colInvoiceDate.Width = 80;
            // 
            // colOutputDate
            // 
            this.colOutputDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutputDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutputDate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOutputDate.Caption = "Ngày xuất";
            this.colOutputDate.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.colOutputDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colOutputDate.FieldName = "OUTPUTDATE";
            this.colOutputDate.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colOutputDate.Name = "colOutputDate";
            this.colOutputDate.OptionsColumn.AllowEdit = false;
            this.colOutputDate.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOutputDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colOutputDate.OptionsColumn.ReadOnly = true;
            this.colOutputDate.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colOutputDate.Visible = true;
            this.colOutputDate.VisibleIndex = 14;
            this.colOutputDate.Width = 120;
            // 
            // colStore
            // 
            this.colStore.AppearanceHeader.Options.UseTextOptions = true;
            this.colStore.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStore.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colStore.Caption = "Kho xuất";
            this.colStore.FieldName = "STORENAME";
            this.colStore.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colStore.Name = "colStore";
            this.colStore.OptionsColumn.AllowEdit = false;
            this.colStore.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colStore.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colStore.OptionsColumn.ReadOnly = true;
            this.colStore.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colStore.Visible = true;
            this.colStore.VisibleIndex = 15;
            this.colStore.Width = 200;
            // 
            // colOutputUser
            // 
            this.colOutputUser.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutputUser.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutputUser.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colOutputUser.Caption = "Mã nhân viên xuất";
            this.colOutputUser.FieldName = "CREATEDUSER";
            this.colOutputUser.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colOutputUser.Name = "colOutputUser";
            this.colOutputUser.OptionsColumn.AllowEdit = false;
            this.colOutputUser.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOutputUser.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colOutputUser.OptionsColumn.ReadOnly = true;
            this.colOutputUser.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colOutputUser.Visible = true;
            this.colOutputUser.VisibleIndex = 16;
            this.colOutputUser.Width = 90;
            // 
            // colUserOutput
            // 
            this.colUserOutput.AppearanceHeader.Options.UseTextOptions = true;
            this.colUserOutput.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserOutput.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colUserOutput.Caption = "Tên nhân viên xuất";
            this.colUserOutput.FieldName = "OUTPUTUSERFULLNAME";
            this.colUserOutput.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colUserOutput.Name = "colUserOutput";
            this.colUserOutput.OptionsColumn.AllowEdit = false;
            this.colUserOutput.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colUserOutput.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colUserOutput.OptionsColumn.ReadOnly = true;
            this.colUserOutput.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colUserOutput.Visible = true;
            this.colUserOutput.VisibleIndex = 11;
            this.colUserOutput.Width = 160;
            // 
            // colUserStaff
            // 
            this.colUserStaff.AppearanceHeader.Options.UseTextOptions = true;
            this.colUserStaff.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserStaff.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUserStaff.Caption = "Mã nhân viên bán hàng";
            this.colUserStaff.FieldName = "STAFFUSER";
            this.colUserStaff.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colUserStaff.Name = "colUserStaff";
            this.colUserStaff.OptionsColumn.AllowEdit = false;
            this.colUserStaff.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colUserStaff.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colUserStaff.OptionsColumn.ReadOnly = true;
            this.colUserStaff.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colUserStaff.Visible = true;
            this.colUserStaff.VisibleIndex = 17;
            this.colUserStaff.Width = 90;
            // 
            // colStaffUser
            // 
            this.colStaffUser.AppearanceHeader.Options.UseTextOptions = true;
            this.colStaffUser.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStaffUser.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colStaffUser.Caption = "Tên nhân viên bán hàng";
            this.colStaffUser.FieldName = "STAFFUSERFULLNAME";
            this.colStaffUser.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colStaffUser.Name = "colStaffUser";
            this.colStaffUser.OptionsColumn.AllowEdit = false;
            this.colStaffUser.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colStaffUser.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colStaffUser.OptionsColumn.ReadOnly = true;
            this.colStaffUser.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colStaffUser.Visible = true;
            this.colStaffUser.VisibleIndex = 18;
            this.colStaffUser.Width = 160;
            // 
            // colDiscount
            // 
            this.colDiscount.AppearanceHeader.Options.UseTextOptions = true;
            this.colDiscount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDiscount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDiscount.Caption = "Giảm giá";
            this.colDiscount.DisplayFormat.FormatString = "#,##0";
            this.colDiscount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDiscount.FieldName = "DISCOUNT";
            this.colDiscount.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colDiscount.Name = "colDiscount";
            this.colDiscount.OptionsColumn.AllowEdit = false;
            this.colDiscount.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colDiscount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colDiscount.OptionsColumn.ReadOnly = true;
            this.colDiscount.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colDiscount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "DISCOUNT", "{0:#,##0}")});
            this.colDiscount.Visible = true;
            this.colDiscount.VisibleIndex = 19;
            this.colDiscount.Width = 100;
            // 
            // colTotalVAT
            // 
            this.colTotalVAT.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalVAT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalVAT.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalVAT.Caption = "Tổng tiền VAT";
            this.colTotalVAT.DisplayFormat.FormatString = "#,##0.##";
            this.colTotalVAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalVAT.FieldName = "TOTALVAT";
            this.colTotalVAT.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalVAT.Name = "colTotalVAT";
            this.colTotalVAT.OptionsColumn.AllowEdit = false;
            this.colTotalVAT.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colTotalVAT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colTotalVAT.OptionsColumn.ReadOnly = true;
            this.colTotalVAT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "TOTALVAT", "{0:#,##0}")});
            this.colTotalVAT.Visible = true;
            this.colTotalVAT.VisibleIndex = 8;
            this.colTotalVAT.Width = 120;
            // 
            // colTotalAmount
            // 
            this.colTotalAmount.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalAmount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalAmount.Caption = "Tổng tiền";
            this.colTotalAmount.DisplayFormat.FormatString = "#,##0";
            this.colTotalAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalAmount.FieldName = "TOTALAMOUNT";
            this.colTotalAmount.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colTotalAmount.Name = "colTotalAmount";
            this.colTotalAmount.OptionsColumn.AllowEdit = false;
            this.colTotalAmount.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colTotalAmount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colTotalAmount.OptionsColumn.ReadOnly = true;
            this.colTotalAmount.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colTotalAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "TOTALAMOUNT", "{0:#,##0}")});
            this.colTotalAmount.Visible = true;
            this.colTotalAmount.VisibleIndex = 9;
            this.colTotalAmount.Width = 120;
            // 
            // colOutputContent
            // 
            this.colOutputContent.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutputContent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutputContent.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOutputContent.Caption = "Nội dung";
            this.colOutputContent.FieldName = "OUTPUTCONTENT";
            this.colOutputContent.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colOutputContent.Name = "colOutputContent";
            this.colOutputContent.OptionsColumn.AllowEdit = false;
            this.colOutputContent.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOutputContent.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colOutputContent.OptionsColumn.ReadOnly = true;
            this.colOutputContent.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colOutputContent.Visible = true;
            this.colOutputContent.VisibleIndex = 20;
            this.colOutputContent.Width = 400;
            // 
            // colOutputNote
            // 
            this.colOutputNote.AppearanceHeader.Options.UseTextOptions = true;
            this.colOutputNote.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOutputNote.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colOutputNote.Caption = "Ghi chú";
            this.colOutputNote.FieldName = "OUTPUTNOTE";
            this.colOutputNote.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colOutputNote.Name = "colOutputNote";
            this.colOutputNote.OptionsColumn.AllowEdit = false;
            this.colOutputNote.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colOutputNote.Visible = true;
            this.colOutputNote.VisibleIndex = 21;
            this.colOutputNote.Width = 220;
            // 
            // colIsInputReturnChangeOrder
            // 
            this.colIsInputReturnChangeOrder.AppearanceHeader.Options.UseTextOptions = true;
            this.colIsInputReturnChangeOrder.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIsInputReturnChangeOrder.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIsInputReturnChangeOrder.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIsInputReturnChangeOrder.Caption = "Đã nhập trả / đổi trả";
            this.colIsInputReturnChangeOrder.ColumnEdit = this.repIsInputReturnChangeOrder;
            this.colIsInputReturnChangeOrder.FieldName = "ISINPUTRETURNCHANGEORDER";
            this.colIsInputReturnChangeOrder.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colIsInputReturnChangeOrder.Name = "colIsInputReturnChangeOrder";
            this.colIsInputReturnChangeOrder.OptionsColumn.AllowEdit = false;
            this.colIsInputReturnChangeOrder.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colIsInputReturnChangeOrder.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colIsInputReturnChangeOrder.OptionsColumn.ReadOnly = true;
            this.colIsInputReturnChangeOrder.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colIsInputReturnChangeOrder.Visible = true;
            this.colIsInputReturnChangeOrder.VisibleIndex = 22;
            this.colIsInputReturnChangeOrder.Width = 100;
            // 
            // repIsInputReturnChangeOrder
            // 
            this.repIsInputReturnChangeOrder.AutoHeight = false;
            this.repIsInputReturnChangeOrder.Name = "repIsInputReturnChangeOrder";
            this.repIsInputReturnChangeOrder.ValueChecked = ((short)(1));
            this.repIsInputReturnChangeOrder.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.EditFormat.FormatString = "N4";
            this.repositoryItemCalcEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemCalcEdit1.Mask.EditMask = "##,###,###,###.##";
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // frmOutputVoucherManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 522);
            this.Controls.Add(this.grdData);
            this.Controls.Add(this.groupControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(969, 549);
            this.Name = "frmOutputVoucherManager";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý phiếu xuất";
            this.Load += new System.EventHandler(this.frmOutputVoucherManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            this.mnuContext.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIsInputReturnChangeOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cboSearchType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repIsInputReturnChangeOrder;
        private System.Windows.Forms.ComboBox cboErrorType;
        private System.Windows.Forms.TextBox txtKeywords;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ContextMenuStrip mnuContext;
        private System.Windows.Forms.ToolStripMenuItem mnuItemInputVoucherReturn;
        private System.Windows.Forms.ToolStripMenuItem mnuItemInputChange;
        private System.Windows.Forms.ToolStripMenuItem mnuItemInputReturnWithFee;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintOutputStore;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintInputChange;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboStoreIDList;
        private System.Windows.Forms.ToolStripMenuItem mnuItemViewInputProductChange;
        private System.Windows.Forms.ToolStripMenuItem mnuItemViewInputVoucher;
        private System.Windows.Forms.Button btnExport;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomer cboCustomer;
        private Library.AppControl.ComboBoxControl.ComboBoxOutputType cboOutputType;
        private System.Windows.Forms.ToolStripMenuItem mnuInputChangeOrderInput;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem mnuInputChangeOrderInputHasError;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintOutputVoucher;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem mnuIMEIChange;
        private DevExpress.XtraEditors.TextEdit txtTop;
        private DevExpress.XtraGrid.Views.Grid.GridView grvData;
        private DevExpress.XtraGrid.Columns.GridColumn colOutputVoucherID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomer;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceSysbol;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colOutputDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStore;
        private DevExpress.XtraGrid.Columns.GridColumn colOutputUser;
        private DevExpress.XtraGrid.Columns.GridColumn colUserOutput;
        private DevExpress.XtraGrid.Columns.GridColumn colUserStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffUser;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscount;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colOutputContent;
        private DevExpress.XtraGrid.Columns.GridColumn colIsInputReturnChangeOrder;
        private System.Windows.Forms.CheckBox chkIsViewDetail;
        private DevExpress.XtraGrid.Columns.GridColumn colProductID;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
        private DevExpress.XtraGrid.Columns.GridColumn colOutputTypeName;
        private System.Windows.Forms.ToolStripMenuItem mnuCreateInvoiceVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colOutputNote;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintPOReturnHandovers;
        private Library.AppControl.ComboBoxControl.ComboBoxBranch cboBranch;
        private System.Windows.Forms.Label label2;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboDataReport;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripMenuItem mnuItemPrintOutputStoreGroupBy;
        private DevExpress.XtraGrid.Columns.GridColumn QUANTITY;
    }
}