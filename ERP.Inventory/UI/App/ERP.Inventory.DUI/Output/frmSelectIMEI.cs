﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Output
{
    public partial class frmSelectIMEI : Form
    {
        public frmSelectIMEI()
        {
            InitializeComponent();
        }

        public frmSelectIMEI(DataTable dtbDetail)
        {
            InitializeComponent();
            this.dtbDetail = dtbDetail;
        }

        private PLC.IMEIChange.PLCIMEIChange objPLCIMEIChange = new PLC.IMEIChange.PLCIMEIChange();
        private DataTable dtbDetail = null;

        private bool ValidData()
        {
            bool bolIMEIChangeNotNull = false;
            for (int i = 0; i < grvDetail.RowCount; i++)
            {
                DataRow row = (DataRow)grvDetail.GetDataRow(i);
                string strIMEI = Convert.ToString(row["IMEI"]).Trim();
                string strIMEIChange = Convert.ToString(row["IMEICHANGE"]).Trim();
                if (!string.IsNullOrEmpty(strIMEIChange))
                {
                    string strBarcode = Library.AppCore.Other.ConvertObject.ConvertTextToBarcode(strIMEIChange);
                    if (strBarcode.Length == 0 || !Library.AppCore.Other.CheckObject.CheckIMEI(strIMEIChange))
                    {
                        MessageBox.Show(this, "IMEI đổi không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        grvDetail.SelectCell(i, colIMEI);
                        return false;
                    }
                    bolIMEIChangeNotNull = true;
                    string strFromIMEI = objPLCIMEIChange.GetIMEI(strIMEIChange);
                    if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return false;
                    }
                    if (!string.IsNullOrEmpty(strFromIMEI))
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "IMEI " + strIMEIChange + " đã được khai báo đổi. Vui lòng kiểm tra lại");
                        grvDetail.SelectCell(i, colIMEI);
                        return false;
                    }
                }
            }
            if (!bolIMEIChangeNotNull)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập IMEI đổi");
                return false;
            }
            return true;
        }

        private void frmSelectIMEI_Load(object sender, EventArgs e)
        {
            grdDetail.DataSource = dtbDetail;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!ValidData())
                return;
            btnUpdate.Enabled = false;
            string strCreatedUser = Library.AppCore.SystemConfig.objSessionUser.UserName;
            List<PLC.IMEIChange.WSIMEIChange.IMEIChange> lstIMEIChange = new List<PLC.IMEIChange.WSIMEIChange.IMEIChange>();
            try
            {
                if (MessageBox.Show(this, "Bạn có chắc muốn đổi IMEI không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    btnUpdate.Enabled = true;
                    return;
                }
                for (int i = 0; i < grvDetail.RowCount; i++)
                {
                    DataRow row = (DataRow)grvDetail.GetDataRow(i);
                    string strIMEI = Convert.ToString(row["IMEI"]).Trim();
                    string strIMEIChange = Convert.ToString(row["IMEICHANGE"]).Trim();
                    if (!string.IsNullOrEmpty(strIMEIChange))
                    {
                        PLC.IMEIChange.WSIMEIChange.IMEIChange objIMEIChange = new PLC.IMEIChange.WSIMEIChange.IMEIChange();
                        objIMEIChange.OutputVoucherDetailID = row["OutputVoucherDetailID"].ToString();
                        objIMEIChange.FromIMEI = strIMEI;
                        objIMEIChange.ToIMEI = strIMEIChange;
                        objIMEIChange.CreatedUser = strCreatedUser;
                        lstIMEIChange.Add(objIMEIChange);
                    }
                }
                if (!objPLCIMEIChange.Insert(lstIMEIChange))
                {
                    btnUpdate.Enabled = true;
                    if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show(this, "Đổi IMEI thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {
                btnUpdate.Enabled = true;
            }
        }
    }
}
