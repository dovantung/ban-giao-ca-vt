﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraGrid.Views.Grid;
using Library.AppCore;
using ERP.Inventory.PLC;
using Library.AppCore.LoadControls;

namespace ERP.Inventory.DUI.Output
{
    /// <summary>
    /// Created by: Đặng Minh Hùng
    /// Desc: Phiếu xuất
    /// </summary>
    public partial class frmOutputVoucher : Form
    {
        public frmOutputVoucher()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvOutputVoucherDetail);
        }

        #region variable
        public string strOutputVoucherID = string.Empty;
        private ERP.Inventory.PLC.Output.PLCOutputVoucher objPLCOutputVoucher = new PLC.Output.PLCOutputVoucher();
        private ERP.Inventory.PLC.Output.WSOutputVoucher.OutputVoucher objOutputVoucher = new PLC.Output.WSOutputVoucher.OutputVoucher();
        private ERP.SalesAndServices.Payment.PLC.PLCVoucher objPLCVoucher = new SalesAndServices.Payment.PLC.PLCVoucher();
        private ERP.SalesAndServices.Payment.PLC.WSVoucher.Voucher objVoucher = new SalesAndServices.Payment.PLC.WSVoucher.Voucher();
        private bool bolIsRight_OutputVoucher_EditAfterPosted = false;
        private bool bolIsRight_OutputVoucher_UpdateCustomer = false;
        private bool bolIsRight_OutputVoucher_EditCustomer = false;
        private bool bolIsLockByClosingDataMonth = false;
        private string strDenominator = string.Empty;
        private bool bolIsOnlyView = false;

        decimal TotalBFVAT = 0,
                totalVAT = 0,
                totalSalePrice = 0,
                totalAmount = 0,
                Vat = 0,
                totalNumber = 0;

        public bool IsOnlyView
        {
            get { return bolIsOnlyView; }
            set { bolIsOnlyView = value; }
        }
        #endregion

        #region method
        private void LoadOutputVoucher()
        {
            try
            {
                if (strOutputVoucherID != string.Empty)
                {
                    objOutputVoucher = objPLCOutputVoucher.LoadInfo(strOutputVoucherID);
                    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                    {
                        Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return;
                    }
                    if (objOutputVoucher != null)
                    {
                        if (!bolIsRight_OutputVoucher_EditCustomer)
                        {
                            LockCustomerInfo();
                        }
                        if (!bolIsRight_OutputVoucher_UpdateCustomer)
                        {
                            LockOthers();
                        }
                        if (!bolIsRight_OutputVoucher_UpdateCustomer && !bolIsRight_OutputVoucher_EditCustomer)
                        {
                            btnUpdate.Enabled = false;
                        }
                        if (objOutputVoucher.IsPosted && (!bolIsRight_OutputVoucher_EditAfterPosted))
                        {
                            btnUpdate.Enabled = false;
                        }
                        txtOutputVoucherID.Text = objOutputVoucher.OutputVoucherID;
                        dtOutputDate.Value = objOutputVoucher.OutputDate.Value;
                        try
                        {
                            bolIsLockByClosingDataMonth = ERP.MasterData.SYS.PLC.PLCClosingDataMonth.CheckLockClosingDataMonth(ERP.MasterData.SYS.PLC.PLCClosingDataMonth.ClosingDataType.OUTPUTVOUCHER,
                                objOutputVoucher.OutputDate.Value, objOutputVoucher.OutputStoreID);
                        }
                        catch { }
                        dtInvoiceDate.Value = objOutputVoucher.InvoiceDate.Value;
                        //Nguyen Van Tai bo sung theo yeu cau BA Binh
                        dteDueDate.Value = objOutputVoucher.PayableDate.Value;
                        //End
                        txtInVoiceID.Text = objOutputVoucher.InvoiceID;
                        txtInvoiceSymbol.Text = objOutputVoucher.InvoiceSymbol;
                        strDenominator = objOutputVoucher.Denominator;
                        ctrlOutputUser.UserName = objOutputVoucher.CreatedUser;
                        txtOutputStore.Text = ERP.MasterData.PLC.MD.PLCStore.GetStoreName(objOutputVoucher.OutputStoreID);
                        txtOrderID.Text = objOutputVoucher.OrderID;
                        txtCustomerID.Text = objOutputVoucher.CustomerID.ToString();
                        txtCustomerName.Text = objOutputVoucher.CustomerName;
                        txtAddress.Text = objOutputVoucher.CustomerAddress;
                        txtTaxAddress.Text = objOutputVoucher.TaxCustomerAddress;
                        txtPhone.Text = objOutputVoucher.CustomerPhone;
                        txtCustomerTaxID.Text = objOutputVoucher.CustomerTaxID;
                        txtOutputNote.Text = objOutputVoucher.OutputNote;
                        txtPayableType.Text = ERP.MasterData.PLC.MD.PLCPayableType.GetPayableTypeName(objOutputVoucher.PayableTypeID);
                        txtOutputContent.Text = Convert.ToString(objOutputVoucher.OutputContent).Trim().Replace("\n", "\r\n");
                        txtDiscountReasonName.Text = ERP.MasterData.PLC.MD.PLCDiscountReason.GetDiscountReasonName(objOutputVoucher.DiscountReasonID);
                        txtDiscountOutput.Text = objOutputVoucher.Discount.ToString("#,###,##0");
                        grdOutputVoucherDetail.DataSource = objPLCOutputVoucher.GetAll(objOutputVoucher.OutputVoucherID);
                        grdOutputVoucherDetail.RefreshDataSource();
                        SumGroup((DataTable)grdOutputVoucherDetail.DataSource);
                        if (objOutputVoucher.IsStoreChange)
                        {
                            lnkCustomer.Enabled = false;
                        }
                        colOutputType.Summary.Add(DevExpress.Data.SummaryItemType.Custom, "OUTPUTTYPENAME", "Tổng dòng: " + grvOutputVoucherDetail.RowCount.ToString("#,##0"));
                    }
                }
                if (bolIsOnlyView)
                {
                    txtOutputVoucherID.ReadOnly = true;
                    txtOutputVoucherID.BackColor = SystemColors.Info;
                    dtOutputDate.Enabled = false;
                    txtOutputStore.ReadOnly = true;
                    txtOutputVoucherID.BackColor = SystemColors.Info;
                    ctrlOutputUser.Enabled = false;
                    txtPayableType.ReadOnly = true;
                    txtPayableType.BackColor = SystemColors.Info;
                    txtDiscountReasonName.ReadOnly = true;
                    txtDiscountReasonName.BackColor = SystemColors.Info;
                    txtDiscountOutput.ReadOnly = true;
                    txtDiscountOutput.BackColor = SystemColors.Info;
                    txtOrderID.ReadOnly = true;
                    txtOrderID.BackColor = SystemColors.Info;
                    lnkCustomer.Enabled = false;
                    txtCustomerID.ReadOnly = true;
                    txtCustomerID.BackColor = SystemColors.Info;
                    txtCustomerName.ReadOnly = true;
                    txtCustomerName.BackColor = SystemColors.Info;
                    txtAddress.ReadOnly = true;
                    txtAddress.BackColor = SystemColors.Info;
                    txtTaxAddress.ReadOnly = true;
                    txtTaxAddress.BackColor = SystemColors.Info;
                    txtPhone.ReadOnly = true;
                    txtPhone.BackColor = SystemColors.Info;
                    txtCustomerTaxID.ReadOnly = true;
                    txtCustomerTaxID.BackColor = SystemColors.Info;
                    txtOutputContent.ReadOnly = true;
                    txtOutputContent.BackColor = SystemColors.Info;
                    txtInVoiceID.ReadOnly = true;
                    txtInVoiceID.BackColor = SystemColors.Info;
                    dtInvoiceDate.Enabled = false;
                    txtInvoiceSymbol.ReadOnly = true;
                    txtInvoiceSymbol.BackColor = SystemColors.Info;
                    btnUpdate.Enabled = false;
                    grvOutputVoucherDetail.OptionsBehavior.Editable = false;
                }
            }
            catch (Exception objExec)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy thông tin phiếu xuất");
                SystemErrorWS.Insert("Lỗi lấy thông tin phiếu xuất", objExec, DUIInventory_Globals.ModuleName);
            }
        }

        private void LockCustomerInfo()
        {
            lnkCustomer.Enabled = false;
            txtCustomerName.ReadOnly = true;
            txtCustomerName.BackColor = System.Drawing.SystemColors.Info;
            txtAddress.ReadOnly = true;
            txtAddress.BackColor = System.Drawing.SystemColors.Info;
            txtTaxAddress.ReadOnly = true;
            txtTaxAddress.BackColor = System.Drawing.SystemColors.Info;
            txtPhone.ReadOnly = true;
            txtPhone.BackColor = System.Drawing.SystemColors.Info;
            txtCustomerTaxID.ReadOnly = true;
            txtCustomerTaxID.BackColor = System.Drawing.SystemColors.Info;
        }
        private void LockOthers()
        {
            //dtInvoiceDate.Enabled = false;
            //txtInVoiceID.ReadOnly = true;
            //txtInVoiceID.BackColor = System.Drawing.SystemColors.Info;
            //txtInvoiceSymbol.ReadOnly = true;
            //txtInvoiceSymbol.BackColor = System.Drawing.SystemColors.Info;
            //txtOutputContent.ReadOnly = true;
            //txtOutputContent.BackColor = System.Drawing.SystemColors.Info;
            txtOutputContent.ReadOnly = true;
            txtOutputContent.BackColor = System.Drawing.SystemColors.Info;
            txtOutputNote.ReadOnly = true;
            txtOutputNote.BackColor = System.Drawing.SystemColors.Info;
        }

        /// <summary>
        /// Lấy tỉ giá tiền
        /// </summary>
        /// <param name="CurrencyUnitID"></param>
        /// <returns></returns>
        public float GetCurrencyExchange(int CurrencyUnitID)
        {
            DataTable dtbCurrencyUnit = Library.AppCore.DataSource.GetDataSource.GetCurrencyUnit();
            DataRow[] rows = dtbCurrencyUnit.Select("CURRENCYUNITID = " + CurrencyUnitID);
            if (rows.Length < 1)
            {
                return 0;
            }
            return Convert.ToSingle(rows[0]["CURRENCYEXCHANGE"]);
        }

        #region Kiểm tra

        private bool CheckInputCustomer()
        {
            if (txtCustomerID.Text.Trim() == "")
            {
                txtCustomerID.Focus();
                MessageBox.Show(this, "Vui lòng chọn khách hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (txtCustomerName.Text.Trim() == "")
            {
                txtCustomerName.Focus();
                MessageBox.Show(this, "Vui lòng nhập tên khách hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (txtPhone.Text.Trim().Length > 0 && (!Globals.CheckIsPhoneNumber(txtPhone.Text.Trim())
                || txtPhone.Text.Trim().Length < 10))
            {
                txtPhone.Focus();
                MessageBox.Show(this, "Số điện thoại khách hàng không đúng định dạng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            txtInVoiceID.Text = txtInVoiceID.Text.Trim();
            if (txtInVoiceID.Text != string.Empty && txtInVoiceID.Text.Length < 7)
            {
                txtInVoiceID.Focus();
                MessageBox.Show(this, "Số hóa đơn tối thiểu phải 7 ký tự! Vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else
            {
                if (txtInVoiceID.Text == string.Empty)
                {
                    txtInVoiceID.Text = "0000000";
                }
            }

            if (dteDueDate.Value.Date.CompareTo(dtInvoiceDate.Value.Date) < 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Hạn thanh toán phải lớn hơn hoặc bằng ngày hóa đơn");
                dteDueDate.Focus();
                return false;
            }
            return true;
        }

        #endregion

        #endregion

        #region Events
        private void frmOutputVoucher_Load(object sender, EventArgs e)
        {
            bolIsRight_OutputVoucher_EditAfterPosted = Library.AppCore.SystemConfig.objSessionUser.IsPermission("PM_OUTPUTVOUCHER_EDITAFTERPOSTED");
            bolIsRight_OutputVoucher_UpdateCustomer = Library.AppCore.SystemConfig.objSessionUser.IsPermission("PM_OUTPUTVOUCHER_UPDATECUSTOMER");
            bolIsRight_OutputVoucher_EditCustomer = Library.AppCore.SystemConfig.objSessionUser.IsPermission("PM_OUTPUTVOUCHER_EDITCUSTOMER");
            LoadOutputVoucher();
            if (bolIsLockByClosingDataMonth)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, "Tháng " + dtOutputDate.Value.Month.ToString("00") + " đã khóa sổ. Bạn không thể chỉnh sửa");
                btnUpdate.Enabled = false;
                return;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (bolIsLockByClosingDataMonth)
            {
                return;
            }
            if (!CheckInputCustomer())
            {
                return;
            }
            btnUpdate.Enabled = false;

            if (bolIsRight_OutputVoucher_EditCustomer)
            {
                try
                {
                    if (Convert.ToString(txtCustomerID.Text).Trim() != string.Empty)
                    {
                        objOutputVoucher.CustomerID = Convert.ToInt32(txtCustomerID.Text);
                    }
                }
                catch
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi lấy mã khách hàng! Vui lòng kiểm tra lại");
                    btnUpdate.Enabled = true;
                    return;
                }
                objOutputVoucher.CustomerName = Convert.ToString(txtCustomerName.Text).Trim();
                objOutputVoucher.CustomerAddress = Convert.ToString(txtAddress.Text).Trim();
                objOutputVoucher.TaxCustomerAddress = Convert.ToString(txtTaxAddress.Text).Trim();
                if (!txtPhone.ReadOnly)
                    objOutputVoucher.CustomerPhone = Convert.ToString(txtPhone.Text).Trim();
                objOutputVoucher.CustomerTaxID = Convert.ToString(txtCustomerTaxID.Text).Trim();

            }

            if (bolIsRight_OutputVoucher_UpdateCustomer)
            {
                objOutputVoucher.OutputContent = txtOutputContent.Text;
                objOutputVoucher.OutputNote = txtOutputNote.Text.Trim();
                objOutputVoucher.PayableDate = dteDueDate.Value.Date;
                //objOutputVoucher.InvoiceDate = dtInvoiceDate.Value;
                //objOutputVoucher.InvoiceID = Convert.ToString(txtInVoiceID.Text).Trim();
                //objOutputVoucher.InvoiceSymbol = Convert.ToString(txtInvoiceSymbol.Text).Trim();
            }
            else
                objOutputVoucher.PayableDate = Globals.GetServerDateTime().Date;
            objOutputVoucher.Denominator = strDenominator;
            // objOutputVoucher.OutputNote = txtOutputNote.Text;

            if (!objPLCOutputVoucher.UpdateCustomer(objOutputVoucher))
            {
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnUpdate.Enabled = true;
                    return;
                }
            }
            MessageBox.Show(this, "Cập nhật thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            btnUpdate.Enabled = true;
        }

        private void lnkCustomer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ERP.MasterData.DUI.Search.frmCustomerSearch frmCustomerSearch1 = new MasterData.DUI.Search.frmCustomerSearch();
            frmCustomerSearch1.ShowDialog();
            if (frmCustomerSearch1.Customer != null && frmCustomerSearch1.Customer.CustomerID > 0)
            {
                txtCustomerID.Text = frmCustomerSearch1.Customer.CustomerID.ToString();
                txtCustomerName.Text = frmCustomerSearch1.Customer.CustomerName;
                txtAddress.Text = frmCustomerSearch1.Customer.CustomerAddress;
                txtTaxAddress.Text = frmCustomerSearch1.Customer.TaxCustomerAddress;
                txtCustomerTaxID.Text = frmCustomerSearch1.Customer.CustomerTaxID;
                txtPhone.Text = frmCustomerSearch1.Customer.CustomerPhone;
            }
            else
            {
                if (Convert.ToString(txtCustomerID.Text).Trim() != string.Empty)
                {
                    //if (MessageBox.Show(this, "Bạn muốn bỏ chọn khách hàng hiện tại không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
                }
                txtCustomerID.Text = string.Empty;
                txtCustomerName.Text = string.Empty;
                txtAddress.Text = string.Empty;
                txtTaxAddress.Text = string.Empty;
                txtCustomerTaxID.Text = string.Empty;
                txtPhone.Text = string.Empty;
            }
        }

        #endregion

        private void grvOutputVoucherDetail_CellMerge(object sender, CellMergeEventArgs e)
        {
            string columnHandle = e.Column.FieldName;

            if (columnHandle == "OUTPUTTYPENAME"
                || columnHandle == "PRODUCTID"
                || columnHandle == "PRODUCTNAME"
                || columnHandle == "TOTALAMOUNTBFVAT"
                || columnHandle == "TOTALAMOUNTVAT"
                || columnHandle == "SALEPRICE"
                || columnHandle == "VAT"
                || columnHandle == "TOTALAMOUNT"
                || columnHandle == "QUANTITY"
                )
            {
                #region
                DataRow drRow1 = grvOutputVoucherDetail.GetDataRow(e.RowHandle1);
                string value1 = (drRow1[e.Column.FieldName] ?? "").ToString().Trim();

                DataRow drRow2 = grvOutputVoucherDetail.GetDataRow(e.RowHandle2);
                string value2 = (drRow2[e.Column.FieldName] ?? "").ToString().Trim();


                string outputvoucher1 = drRow1["OUTPUTTYPENAME"].ToString().Trim(),
                    outputvoucher2 = drRow2["OUTPUTTYPENAME"].ToString().Trim();

                bool isEqualOutputvoucherid = outputvoucher1.Equals(outputvoucher2);

                string productid1 = drRow1["PRODUCTID"].ToString().Trim(),
                    productid2 = drRow2["PRODUCTID"].ToString().Trim();

                bool isEqualValue = value1.Equals(value2);

                string saleprice1 = drRow1["SALEPRICE"].ToString().Trim(),
                    saleprice2 = drRow2["SALEPRICE"].ToString().Trim();

                bool isEqualSalePrice = saleprice1.Equals(saleprice2);
                bool isEqualProductID = productid1.Equals(productid2);

                #endregion


                #region
                if ((columnHandle == "OUTPUTTYPENAME" ||
                    columnHandle == "PRODUCTID" || columnHandle == "PRODUCTNAME" ) && isEqualValue)
                {
                    e.Merge = true;
                    e.Handled = true;
                }
                else if (columnHandle == "PRODUCTID"  &&
                    isEqualOutputvoucherid && isEqualProductID)
                {
                    e.Merge = true;
                    e.Handled = true;
                }
                else if (columnHandle == "VAT" &&
                   isEqualOutputvoucherid && isEqualProductID && isEqualSalePrice && isEqualProductID)
                {
                    e.Merge = true;
                    e.Handled = true;
                }
                else if ((columnHandle == "TOTALAMOUNTBFVAT" || columnHandle == "TOTALAMOUNTVAT" ||
                    columnHandle == "SALEPRICE" || columnHandle == "TOTALAMOUNT" || columnHandle == "QUANTITY") &&
                   isEqualOutputvoucherid && isEqualProductID && isEqualValue)
                {
                    e.Merge = true;
                    e.Handled = true;
                }
                else
                {
                    e.Merge = false;
                    e.Handled = true;
                }
                #endregion
                return;
            }
            e.Merge = false;
            e.Handled = true;
        }

        private void grvOutputVoucherDetail_CustomSummaryCalculate(object sender, DevExpress.Data.CustomSummaryEventArgs e)
        {
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Finalize)
            {
                DevExpress.XtraGrid.GridSummaryItem iTem = (DevExpress.XtraGrid.GridSummaryItem)e.Item;
                if (iTem.FieldName == "TOTALAMOUNTBFVAT")
                    e.TotalValue = TotalBFVAT;
                else if (iTem.FieldName == "TOTALAMOUNTVAT")
                    e.TotalValue = totalVAT;

                else if (iTem.FieldName == "VAT")
                    e.TotalValue = Vat;
                else if (iTem.FieldName == "SALEPRICE")
                    e.TotalValue = totalSalePrice;
                else if (iTem.FieldName == "TOTALAMOUNT")
                    e.TotalValue = totalAmount;
                else if (iTem.FieldName == "QUANTITY")
                    e.TotalValue = totalNumber;
            }
        }

        private void SumGroup(DataTable dtbInput)
        {
            if (dtbInput == null || dtbInput.Rows.Count < 1) return;
            DataTable tem = new DataTable();
            tem = dtbInput.DefaultView.ToTable(true, "OUTPUTTYPENAME", "PRODUCTID", "TotalAmountBFVAT", "TotalAmountVAT", "VAT", "SALEPRICE", "TOTALAMOUNT", "QUANTITY");
            totalVAT = 0;
            TotalBFVAT = 0;
            Vat = 0;
            totalAmount = 0;
            totalSalePrice = 0;
            totalNumber = 0;
            foreach (DataRow it in tem.Rows)
            {
                totalVAT += Convert.ToDecimal(it["TotalAmountVAT"]);
                TotalBFVAT += Convert.ToDecimal(it["TotalAmountBFVAT"]);

                Vat += Convert.ToDecimal(it["VAT"]);
                totalSalePrice += Convert.ToDecimal(it["SALEPRICE"]);
                totalAmount += Convert.ToDecimal(it["TOTALAMOUNT"]);
                totalNumber += Convert.ToDecimal(it["QUANTITY"]);
            }
        }
    }
}
