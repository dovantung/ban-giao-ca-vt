﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Library.AppCore;
using ERP.MasterData.PLC.MD;
using System.Data;
using System.Xml;

namespace ERP.Inventory.DUI
{
    public class DUIInventoryCommon
    {
        public enum ReportActionType
        {
            VIEW = 1,
            PRINT = 2,
        }
        public static bool ShowOutputReport(IWin32Window hwdOwner, String strOutputVoucherID)
        {
            int intReportID = Library.AppCore.AppConfig.GetIntConfigValue("OUTPUTVOUCHER_REPORTID");
            if (intReportID < 1)
                return false;
            Hashtable htbParameterValue = new Hashtable();
            bool bolIsViewCostPrice = SystemConfig.objSessionUser.IsPermission("OUTPUTREPORT_VIEWCOSTPRICE");
            htbParameterValue.Add("OutputVoucherID", strOutputVoucherID);
            htbParameterValue.Add("IsCostPriceHide", !bolIsViewCostPrice);
            htbParameterValue.Add("IsSalePriceHide", false);
            htbParameterValue.Add("CompanyName", SystemConfig.DefaultCompany.CompanyName);
            htbParameterValue.Add("CompanyAddress", SystemConfig.DefaultCompany.Address);
            htbParameterValue.Add("CompanyPhone", SystemConfig.DefaultCompany.Phone);
            htbParameterValue.Add("CompanyFax", SystemConfig.DefaultCompany.Fax);
            htbParameterValue.Add("CompanyEmail", SystemConfig.DefaultCompany.Email);
            htbParameterValue.Add("CompanyWebsite", SystemConfig.DefaultCompany.Website);
            string strPrinterTypeID = string.Empty;
            return ActionReport(hwdOwner, strOutputVoucherID, intReportID, htbParameterValue, strPrinterTypeID, 0, ReportActionType.VIEW);
        }
        public static bool ShowReport(IWin32Window hwdOwner, String strVoucherConcernID, int intReportID, Hashtable htbParameterValue, String strPrinterTypeID)
        {
            return ActionReport(hwdOwner, strVoucherConcernID, intReportID, htbParameterValue, strPrinterTypeID, 0, ReportActionType.VIEW);
        }
        private static bool ActionReport(IWin32Window hwdOwner, string strVoucherConcernID, int intReportID, Hashtable htbParameterValue, String strPrinterTypeID, int intNumberOfCopy,
           ReportActionType enuReportActionType)
        {
            ERP.MasterData.PLC.MD.WSReport.Report objReport = PLCReport.LoadInfoFromCache(intReportID);
            if (objReport == null)
            {
                return false;
            }
            if (string.IsNullOrEmpty(strVoucherConcernID))
                return false;
            bool bolPrintResult = false;
            objReport.NumberOfCopy = intNumberOfCopy;
            objReport.PrinterTypeID = strPrinterTypeID;
            if (enuReportActionType == ReportActionType.VIEW)
            {
                bolPrintResult = ERP.Report.DUI.DUIReportDataSource.ShowReport(hwdOwner, objReport, htbParameterValue);
            }
            else
                bolPrintResult = ERP.Report.DUI.DUIReportDataSource.PrintReport(hwdOwner, objReport, htbParameterValue);
            return bolPrintResult;
        }

        public static string CreateXMLFromDataTable(DataTable dtb, string tableName)
        {
            if (dtb == null || dtb.Columns.Count == 0 || string.IsNullOrWhiteSpace(tableName))
            {
                return string.Empty;
            }
            var sb = new StringBuilder();
            var xtx = XmlWriter.Create(sb);
            xtx.WriteStartDocument();
            xtx.WriteStartElement("", tableName.ToUpper() + "LIST", "");
            foreach (DataRow dr in dtb.Rows)
            {
                xtx.WriteStartElement("", tableName.ToUpper(), "");
                foreach (DataColumn dc in dtb.Columns)
                {
                    xtx.WriteAttributeString(dc.ColumnName.ToUpper(), dr[dc.ColumnName].ToString());
                }
                xtx.WriteEndElement();
            }
            xtx.WriteEndElement();
            xtx.Flush();
            xtx.Close();
            return sb.ToString();
        }
        public static string CreateStoreXMLFromDataTable(DataTable dtb, string storeName)
        {
            if (dtb == null || dtb.Columns.Count == 0 || string.IsNullOrWhiteSpace(storeName))
            {
                return string.Empty;
            }
            string s = "";
            s += "CREATE OR REPLACE PROCEDURE " + storeName.ToUpper() + "_IMPORT" + "(XML NCLOB,V_VALUEOUT OUT SYS_REFCURSOR)\r\n";
            s += "AS\r\n";
            s += "BEGIN\r\n";
            s += " INSERT INTO " + storeName.ToUpper() + " (\r\n";
            for (int i = 0; i < dtb.Columns.Count; i++)
            {
                if (i == dtb.Columns.Count - 1)
                {
                    s += dtb.Columns[i].ColumnName;
                }
                else
                {
                    s += dtb.Columns[i].ColumnName + ",";
                }
            }
            s += " )\r\n";
            s += " SELECT \r\n";
            for (int i = 0; i < dtb.Columns.Count; i++)
            {
                if (i == dtb.Columns.Count - 1)
                {
                    s += "V_" + dtb.Columns[i].ColumnName;
                }
                else
                {
                    s += "V_" + dtb.Columns[i].ColumnName + ",";
                }
            }
            s += "\r\n";
            s += " FROM\r\n";
            s += "  XMLTABLE('/" + storeName.ToUpper() + "LIST/" + storeName.ToUpper() + "'\r\n";
            s += "  PASSING XMLTYPE(XML) COLUMNS\r\n";
            for (int i = 0; i < dtb.Columns.Count; i++)
            {
                if (i == dtb.Columns.Count - 1)
                {
                    s += "V_" + dtb.Columns[i].ColumnName + " NVARCHAR2(2000) PATH     './@" + dtb.Columns[i].ColumnName + "') TBLS;\r\n";
                }
                else
                {
                    s += "V_" + dtb.Columns[i].ColumnName + " NVARCHAR2(2000) PATH     './@" + dtb.Columns[i].ColumnName + "',\r\n";
                }
            }
            s += " COMMIT;\r\n";
            s += " OPEN V_VALUEOUT FOR SELECT 1 FROM DUAL;\r\n";
            s += "\r\n";
            s += " EXCEPTION WHEN OTHERS THEN\r\n";
            s += " BEGIN\r\n";
            s += "  ROLLBACK;\r\n";
            s += "  RAISE_APPLICATION_ERROR(-20002, '[+LỖI IMPORT DỮ LIỆU+]');\r\n";
            s += " END;\r\n";
            s += "END;\r\n";
            return s;
        }

        public class clsData
        {
            public string ProductID { get; set; }
            public string FromStoreID { get; set; }
            public string ToStoreID { get; set; }
            public decimal Quantity { get; set; }
            public bool IsUrgent { get; set; }
            public bool IsAllowDecimal { get; set; }
            public List<string> lstIMEI { get; set; }
            public clsData() { lstIMEI = new List<string>(); Quantity = 0; IsUrgent = false; IsAllowDecimal = false; }
        }

    }
}
