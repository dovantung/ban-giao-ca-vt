﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using System.Collections;
using ERP.Inventory.DUI.Inventory;
using ERP.Inventory.DUI.PM.PriceProtect;
using System.Xml;

namespace ERP.Inventory.DUI
{
    public partial class frmTest : Form
    {
        public frmTest()
        {
            InitializeComponent();

        }
        private SystemConfig objSystemConfig = new SystemConfig();

        public string CreateXMLFromDataTable(DataTable dtb, string tableName)
        {
            if (dtb == null || dtb.Columns.Count == 0 || string.IsNullOrWhiteSpace(tableName))
            { return string.Empty; }
            var sb = new StringBuilder();
            var xtx = XmlWriter.Create(sb);
            xtx.WriteStartDocument();
            xtx.WriteStartElement("", tableName.ToUpper() + "LIST", "");
            foreach (DataRow dr in dtb.Rows)
            {
                xtx.WriteStartElement("", tableName.ToUpper(), "");
                foreach (DataColumn dc in dtb.Columns)
                { xtx.WriteAttributeString(dc.ColumnName.ToUpper(), dr[dc.ColumnName].ToString()); }
                xtx.WriteEndElement();
            }
            xtx.WriteEndElement(); xtx.Flush(); xtx.Close(); return sb.ToString();
        }
        public static string CreateStoreXMLFromDataTable(DataTable dtb, string storeName)
        {
            if (dtb == null || dtb.Columns.Count == 0 || string.IsNullOrWhiteSpace(storeName))
            { return string.Empty; }
            string s = ""; s += "CREATE OR REPLACE PROCEDURE " + storeName.ToUpper() + "_IMPORT" + "(XML NCLOB,V_VALUEOUT OUT SYS_REFCURSOR)\r\n"; s += "AS\r\n"; s += "BEGIN\r\n"; s += " INSERT INTO " + storeName.ToUpper() + " (\r\n"; for (int i = 0; i < dtb.Columns.Count; i++) { if (i == dtb.Columns.Count - 1) { s += dtb.Columns[i].ColumnName; } else { s += dtb.Columns[i].ColumnName + ","; } }
            s += " )\r\n"; s += " SELECT \r\n"; for (int i = 0; i < dtb.Columns.Count; i++) { if (i == dtb.Columns.Count - 1) { s += "V_" + dtb.Columns[i].ColumnName; } else { s += "V_" + dtb.Columns[i].ColumnName + ","; } }
            s += "\r\n"; s += " FROM\r\n"; s += "  XMLTABLE('/" + storeName.ToUpper() + "LIST/" + storeName.ToUpper() + "'\r\n"; s += "  PASSING XMLTYPE(XML) COLUMNS\r\n"; for (int i = 0; i < dtb.Columns.Count; i++) { if (i == dtb.Columns.Count - 1) { s += "V_" + dtb.Columns[i].ColumnName + " NVARCHAR2(2000) PATH     './@" + dtb.Columns[i].ColumnName + "') TBLS;\r\n"; } else { s += "V_" + dtb.Columns[i].ColumnName + " NVARCHAR2(2000) PATH     './@" + dtb.Columns[i].ColumnName + "',\r\n"; } }
            s += " COMMIT;\r\n"; s += " OPEN V_VALUEOUT FOR SELECT 1 FROM DUAL;\r\n"; s += "\r\n"; s += " EXCEPTION WHEN OTHERS THEN\r\n"; s += " BEGIN\r\n"; s += "  ROLLBACK;\r\n"; s += "  RAISE_APPLICATION_ERROR(-20002, '[+LỖI IMPORT DỮ LIỆU+]');\r\n"; s += " END;\r\n"; s += "END;\r\n"; return s;
        }
        private void frmTest_Load(object sender, EventArgs e)
        {


            DataTable dt = new DataTable();
            dt.Columns.Add("STORECHANGEORDERDETAILID");
            dt.Columns.Add("IMEI");
            dt.Columns.Add("ORDERDATE");
            dt.Columns.Add("ISSTORECHANGE");
            dt.Columns.Add("NOTE");
            dt.Columns.Add("CREATEDSTOREID");
            dt.Columns.Add("FROMSTORE");
            dt.Columns.Add("CREATEDUSER");
            dt.Columns.Add("STORECHANGEORDERID");
            dt.Columns.Add("PRODUCTID");
            string a = CreateStoreXMLFromDataTable(dt, "SCODETAILIMEI");
            string xml = CreateXMLFromDataTable(dt, "SCODETAILIMEI");
            //string s = DateTime.Now.ToShortTimeString();
            //if (MessageBox.Show(this, "Phiếu xuất 32363w646w6 chưa nhập trả Care+\r\nNếu đồng ý IMEI sẽ không xuất được Care+ cho lần sau\r\nBạn có chắc muốn đồng ý không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
            //{
            //    return;
            //}
            try
            {
                objSystemConfig.LoadConfig();

            }
            catch { }
            //string strUser = "administrator";
            //string strPas = "Erp123456";
            // User Test bang chính
            //strUser = "12245";
            //strPas = "Tuyen200293";
            //strUser = "NC12736";
            string strUser = "138934";
            string strPas = "Lan12345";            
            txtUser.Text = strUser;
            txtPass.Text = strPas;
            btnLogin_Click(null, null);
            //DataTable dt = Library.AppCore.DataSource.GetDataSource.GetProduct();
            //var obj = dt.Select("PRODUCTID = '1010110200001'").CopyToDataTable();
            SystemConfig.GetWebServiceHost("WSHost_ERPTransactionServices").HostURL = @"http://localhost:3124/";
            SystemConfig.GetWebServiceHost("WSHost_ERPTransactionServices").IsHandShake = false;
            //SystemConfig.GetWebServiceHost("WSHost_ERPAccountServices").HostURL = @"http://localhost:57946/";
            //SystemConfig.GetWebServiceHost("WSHost_ERPAccountServices").IsHandShake = false;
            //SystemConfig.GetWebServiceHost("WSHost_ERPMasterDataServices").HostURL = @"http://localhost:30104/";
            //SystemConfig.GetWebServiceHost("WSHost_ERPMasterDataServices").IsHandShake = false;


            if (cboOutput.Items.Count > 0)
            {
                cboOutput.SelectedIndex = 0;
            }
            if (cboPrChange.Items.Count > 0)
            {
                cboPrChange.SelectedIndex = 0;
            }
            if (cboInputChange.Items.Count > 0)
            {
                cboInputChange.SelectedIndex = 0;
            }
            if (cboInventory.Items.Count > 0)
            {
                cboInventory.SelectedIndex = 0;
            }
            GroupBox2.Paint += PaintBorderlessGroupBox;
            //Library.AppCore.SystemConfig.objSessionUser.InsertActionLog("abc", "abc","1","test");
            ////Library.AppCore.LoadControls.SetDataSource.SetResourcesGroup(this, comboBox3);

            ////
            //ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
            //DataTable dtbData = objPLCReportDataSource.GetDataSource("SYS_USERGROUP_STORE_SRH", new object[] { "@STOREID", 100 });

            //string strBirthday1 = "2014/12/23";
            //string strBirthday2 = "2014/23/12";
            //string strBirthday3 = "23/12/2014";
            //string strBirthday4 = "12/23/2014";
            //IFormatProvider theCultureInfo = new System.Globalization.CultureInfo("vi-VN", true);
            //try
            //{
            //    DateTime dtBirthDay1 = new DateTime();
            //    DateTime dtBirthDay2 = new DateTime();
            //    DateTime dtBirthDay3 = new DateTime();
            //    DateTime dtBirthDay4 = new DateTime();
            //    //dtBirthDay = DateTime.ParseExact(strBirthday, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            //    dtBirthDay1 = Convert.ToDateTime(strBirthday1, theCultureInfo);
            //    //dtBirthDay1 = DateTime.ParseExact(strBirthday1, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            //    //dtBirthDay2 = Convert.ToDateTime(strBirthday2, theCultureInfo);
            //    //dtBirthDay2 = DateTime.ParseExact(strBirthday2, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            //    dtBirthDay3 = Convert.ToDateTime(strBirthday3, theCultureInfo);
            //    //dtBirthDay3 = DateTime.ParseExact(strBirthday3, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            //    //dtBirthDay4 = Convert.ToDateTime(strBirthday4, theCultureInfo);
            //    //dtBirthDay4 = DateTime.ParseExact(strBirthday4, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            //}
            //catch (Exception objEx)
            //{
            //}

            //MasterData.PLC.MD.PLCMainGroup objPLCMainGroup = new MasterData.PLC.MD.PLCMainGroup();
            //DataTable dtbMainGroup = null;
            //objPLCMainGroup.GetMainGroup(ref dtbMainGroup, "QA");
            //DataRow[] rMainGroupPermissionDT = dtbMainGroup.Select("HASRIGHT = 1 AND MAINGROUPID = 10");
            //DataRow[] rMainGroupPermissionPK = dtbMainGroup.Select("HASRIGHT = 1 AND MAINGROUPID = 12");
            //DataRow[] rMainGroupPermissionMTB = dtbMainGroup.Select("HASRIGHT = 1 AND MAINGROUPID = 11");
            //ERP.Inventory.PLC.Inventory.PLCQuickInventory objPLCQuickInventory = new PLC.Inventory.PLCQuickInventory();
            //DataTable dtbInStockData = null;
            //DataTable dtbInStockIMEI = null;
            //objPLCQuickInventory.GetInstockData(ref dtbInStockData, ref dtbInStockIMEI, new DateTime(2015, 8, 28, 11, 40, 59), 101, 12, 109, string.Empty, 1, 1);
            //if (dtbInStockData != null && dtbInStockIMEI != null)
            //{
            //    string strIMEI = "1010110700222";
            //    DataRow[] drIMEI = dtbInStockIMEI.Select("TRIM(ProductID) = '" + strIMEI + "'");
            //    if (drIMEI.Length > 0)
            //    {
            //        DataTable tblIMEI = dtbInStockIMEI.Clone();
            //        for (int i = 0; i < drIMEI.Length; i++)
            //        {
            //            DataRow r = tblIMEI.NewRow();
            //            r.ItemArray = drIMEI[i].ItemArray;
            //            tblIMEI.Rows.Add(r);
            //        }
            //    }
            //}
        }
        private void PaintBorderlessGroupBox(object sender, PaintEventArgs p)
        {
            GroupBox box = (GroupBox)sender;
            p.Graphics.Clear(SystemColors.Control);
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (!objSystemConfig.NewLogin(txtUser.Text.Trim(), txtPass.Text.Trim()))
            {
                MessageBox.Show("Login Failed!");
            }
            else
            {
                MessageBox.Show("Login OK!");
                SystemConfig.DefaultCompany = new Company(SystemConfig.objSessionUser.CompanyPermission.Rows[0]);
                List<Product> product = new List<Product>();
                Product objProduct = new Product();
                objProduct.ProductID = "1";
                objProduct.ProductName = "1";
                ProductImei objImei = new ProductImei();
                objImei.ProductID = "1";
                objImei.ProductName = "1";
                objImei.Imei = "134534523463113";
                ProductImei objImei2 = new ProductImei();
                objImei2.ProductID = "1";
                objImei2.ProductName = "1";
                objImei2.Imei = "134534523463113";
                objProduct.lstImei = new List<ProductImei>();
                objProduct.lstImei.Add(objImei2);
                objProduct.lstImei.Add(objImei);


                Product objProduct2 = new Product();
                objProduct2.ProductID = "2";
                objProduct2.ProductName = "2";
                ProductImei objImei3 = new ProductImei();
                objImei3.ProductID = "1";
                objImei3.ProductName = "1";
                objImei3.Imei = "134534523463113";
                ProductImei objImei4 = new ProductImei();
                objImei4.ProductID = "1";
                objImei4.ProductName = "1";
                objImei4.Imei = "134534523463113";
                objProduct2.lstImei = new List<ProductImei>();
                objProduct2.lstImei.Add(objImei4);
                objProduct2.lstImei.Add(objImei3);

                product.Add(objProduct);
                product.Add(objProduct2);

                List<ProductImei> lstImei = product.SelectMany(x => x.lstImei).Cast<ProductImei>().ToList();
                var lstKey = lstImei.GroupBy(x => x.Imei)
                    .Where(g => g.Count() > 1)
                    .Select(y => y.Key).ToList();
                product.SelectMany(x => x.lstImei).Cast<ProductImei>().ToList().Where(x => lstKey.Contains(x.Imei)).Skip(1).All(c => { c.IsError = true; c.Error = "Lỗi duplicate file import"; return true; });
            }
        }

        public class Product
        {
            public string ProductID { get; set; }
            public string ProductName { get; set; }
            public bool IsRequestImei { get; set; }
            public bool IsError { get; set; }
            public string Error { get; set; }
            public List<ProductImei> lstImei { get; set; }
        }
        public class ProductImei
        {
            public string ProductID { get; set; }
            public string ProductName { get; set; }
            public string Imei { get; set; }
            public bool IsError { get; set; }
            public string Error { get; set; }
        }

        private void btnInput_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "frmInputVoucher")
            {
                Input.frmInputVoucher frmInputVoucher1 = new Input.frmInputVoucher();
                frmInputVoucher1.Show();
            }

            if (comboBox1.Text == "frmInputVoucherManager")
            {
                Input.frmInputVoucherManager frmInputVoucherManager1 = new Input.frmInputVoucherManager();
                frmInputVoucherManager1.Show();
            }

            if (comboBox1.Text == "frmProductIMEIHistory")
            {
                IMEI.frmProductIMEIHistory frmProductIMEIHistory = new IMEI.frmProductIMEIHistory();
                frmProductIMEIHistory.Show();
            }
            if (comboBox1.Text == "frmBorrowProduct")
            {
                BorrowProduct.frmBorrowProduct frm = new BorrowProduct.frmBorrowProduct();
                frm.Tag = "ISONLYCERTIFYFINGER  = False";
                frm.Show();
            }
            if (comboBox1.Text == "frmBorrowProduct_Update")
            {
                BorrowProduct.frmBorrowProduct_Update frm = new BorrowProduct.frmBorrowProduct_Update();
                frm.Tag = "BORROWPRODUCTTYPEID=41";
                frm.Show();
            }

        }

        private void btnStoreChangeOrder_Click(object sender, EventArgs e)
        {
            if (cboSC.Text == "frmStoreChangeOrder")
            {
                StoreChange.frmStoreChangeOrder frmStoreChangeOrder = new StoreChange.frmStoreChangeOrder();
                //frmStoreChangeOrder.Tag = "StoreChangeOrderType = 1";
                frmStoreChangeOrder.Tag = "StoreChangeOrderType = 39";
                frmStoreChangeOrder.Show();
            }
            else if (cboSC.Text == "frmStoreChangeOrderManager")
            {
                StoreChange.frmStoreChangeOrderManager frmStoreChangeOrderManage1 = new StoreChange.frmStoreChangeOrderManager();
                frmStoreChangeOrderManage1.Tag = "BillGoodsReportID = 85,123 && IsOnlyCertifyFinger = 0&StoreChangeCommandReportID=166";

                frmStoreChangeOrderManage1.Show();
            }
            else if (cboSC.Text == "frmStoreChange")
            {
                StoreChange.frmStoreChange frmStoreChange = new StoreChange.frmStoreChange();
                frmStoreChange.Tag = "PrintStoreChangeHandovers=135";
                frmStoreChange.Show();
            }
            else if (cboSC.Text == "frmStoreChangeManager")
            {
                StoreChange.frmStoreChangeManager frmStoreChangeManager1 = new StoreChange.frmStoreChangeManager();
                frmStoreChangeManager1.Tag = "StoreChangeReportID=22&StoreChange3_A4=23&StoreChange3_A5=26&StoreChange3_A6=27&storeChangeCommandID=133&internalOutput_TransferID=132";
                frmStoreChangeManager1.Show();
            }

            else if (cboSC.Text == "frmStoreChangeManagerFix")
            {
                StoreChange.frmStoreChangeManagerFix frmStoreChangeManagerFix = new StoreChange.frmStoreChangeManagerFix();
                frmStoreChangeManagerFix.Tag = "StoreChangeReportID=22&StoreChange3_A4=23&StoreChange3_A5=26&StoreChange3_A6=27&storeChangeCommandID=133&internalOutput_TransferID=132";
                frmStoreChangeManagerFix.Show();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ERP.Inventory.DUI.Input.frmInputVoucherReturn frmInputVoucherReturn = new Input.frmInputVoucherReturn();
            frmInputVoucherReturn.OutputVoucherID = txtOutputVoucher.Text.Trim();
            frmInputVoucherReturn.ShowDialog();
        }

        private void btnOutput_Click(object sender, EventArgs e)
        {
            if (cboOutput.Text == "frmOutputVoucherManager")
            {
                ERP.Inventory.DUI.Output.frmOutputVoucherManager frmOutputVoucherManager1 = new Output.frmOutputVoucherManager();
                frmOutputVoucherManager1.Tag = "COMPANY=1";
                frmOutputVoucherManager1.ShowDialog();
            }
        }

        private void btnPrChange_Click(object sender, EventArgs e)
        {

            if (cboPrChange.Text == "frmProductChangeOrderManagement")
            {
                ERP.Inventory.DUI.ProductChange.frmProductChangeOrderManagement frmProductChange1 = new ProductChange.frmProductChangeOrderManagement();
                frmProductChange1.ShowDialog();
            }
            if (cboPrChange.Text == "frmProductChange")
            {
                ERP.Inventory.DUI.ProductChange.frmProductChange frm = new ProductChange.frmProductChange();
                frm.ShowDialog();
            }
            if (cboPrChange.Text == "frmProductChangeOrder")
            {
                ERP.Inventory.DUI.ProductChange.frmProductChangeOrder frm = new ProductChange.frmProductChangeOrder();
                //frm.Tag = "PRODUCTCHANGETYPEID=104";//ĐỔI SANG SP KHÁC
                //frm.Tag = "PRODUCTCHANGETYPEID=105";//ĐỔI SANG IMEI KHÁC
                frm.Tag = "PRODUCTCHANGETYPEID=22";//ĐỔI TRẠNG THÁI IMEI
                //frm.Tag = "PRODUCTCHANGETYPEID=163";//ĐỔI SANG IMEI KHÁC
                frm.ShowDialog();
            }
        }

        private void btnBarcode_Click(object sender, EventArgs e)
        {
            if (cboBarcode.Text == "frmPrintProductBarcode")
            {
                ERP.Inventory.DUI.Barcode.frmPrintProductBarcode frmPrintProductBarcode1 = new Barcode.frmPrintProductBarcode();
                frmPrintProductBarcode1.Tag = textEdit1.Text;
                frmPrintProductBarcode1.Show();
            }
        }

        private void btnInputChange_Click(object sender, EventArgs e)
        {
            //DataTable dtbArea = null;
            //ComboBox cbo = new ComboBox();
            //ERP.MasterData.PLC.MD.PLCArea objPLCArea = new MasterData.PLC.MD.PLCArea();
            //objPLCArea.SearchData(ref dtbArea);
            //dtbArea = Library.AppCore.DataSource.GetDataSource.GetArea();
            //Library.AppCore.LoadControls.SetDataSource.SetArea(this, cbo);
            //dtbArea = cbo.DataSource as DataTable;
            if (cboInputChange.Text == "frmInputChange")
            {
                ERP.Inventory.DUI.InputChange.frmInputChange frmInputChange1 = new InputChange.frmInputChange();
                frmInputChange1.strOutputVoucherID = "119OV1211000012";
                frmInputChange1.ShowDialog();
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (cboStoreChangeCommand.Text == "frmAutoDistributeInventory")
            {
                ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange.frmAutoDistributeInventory frm = new StoreChangeCommand.AutoStoreChange.frmAutoDistributeInventory();
                frm.ShowDialog();
            }
            if (cboStoreChangeCommand.Text == "frmShowProductManager")
            {
                ERP.Inventory.DUI.StoreChangeCommand.frmShowProductManager frmShowProductManager1 = new StoreChangeCommand.frmShowProductManager();
                frmShowProductManager1.Tag = txtStoreChangeOrderTypeID.Text;
                frmShowProductManager1.Show();
            }

            if (cboStoreChangeCommand.Text == "frmAutoSplitProduct")
            {
                ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange.frmAutoSplitProduct frmAutoSplitProduct1 = new StoreChangeCommand.AutoStoreChange.frmAutoSplitProduct();
                frmAutoSplitProduct1.Show();
            }

            if (cboStoreChangeCommand.Text == "frmStoreChangeCommandManager")
            {
                ERP.Inventory.DUI.StoreChangeCommand.frmStoreChangeCommandManager frmStoreChangeCommandManager1 = new StoreChangeCommand.frmStoreChangeCommandManager();
                frmStoreChangeCommandManager1.Tag = txtStoreChangeOrderTypeID.Text;
                frmStoreChangeCommandManager1.Show();
            }

            if (cboStoreChangeCommand.Text == "frmAutoStoreChange")
            {
                ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange.frmAutoStoreChange frmAutoStoreChange1 = new StoreChangeCommand.AutoStoreChange.frmAutoStoreChange();
                frmAutoStoreChange1.Show();
            }


            if (cboStoreChangeCommand.Text == "frmAutoStoreChangeDetail")
            {
                ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange.frmAutoStoreChangeDetail frmAutoStoreChangeDetail1 = new StoreChangeCommand.AutoStoreChange.frmAutoStoreChangeDetail();
                frmAutoStoreChangeDetail1.Tag = "IsCanUseOldCode=1&AutoCreateOrder=1";
                frmAutoStoreChangeDetail1.Show();
            }

            if (cboStoreChangeCommand.Text == "frmStoreChangeShowProduct")
            {
                ERP.Inventory.DUI.StoreChangeCommand.frmStoreChangeShowProduct frm = new StoreChangeCommand.frmStoreChangeShowProduct();
                frm.Tag = txtStoreChangeOrderTypeID.Text;
                frm.Show();
            }
            if (cboStoreChangeCommand.Text == "frmAutoStoreChangeDetailIMEI")
            {
                ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange.frmAutoStoreChangeDetail_WithIMEI frmAutoStoreChangeDetail1 = new StoreChangeCommand.AutoStoreChange.frmAutoStoreChangeDetail_WithIMEI();
                frmAutoStoreChangeDetail1.Tag = "IsCanUseOldCode=1&AutoCreateOrder=1";
                frmAutoStoreChangeDetail1.Show();
            }
            if (cboStoreChangeCommand.Text == "frmStoreChangeDistribute")
            {
                ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange.frmStoreChangeDistribute frmAutoStoreChangeDetail1 = new StoreChangeCommand.AutoStoreChange.frmStoreChangeDistribute();
                frmAutoStoreChangeDetail1.Tag = "IsCanUseOldCode=1&AutoCreateOrder=1";
                frmAutoStoreChangeDetail1.Show();
            }


        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            //ERP.SalesAndServices.Price.DUI.RetailInputPrice.frmRetailPriceManager frmRetailPriceManager = new SalesAndServices.Price.DUI.RetailInputPrice.frmRetailPriceManager();
            //frmRetailPriceManager.ShowDialog();
        }

        private void btnBeginTermInStock_Click(object sender, EventArgs e)
        {
            if (cboBeginTermInStock.Text == "frmBeginTermInStock")
            {
                ERP.Inventory.DUI.BGT.frmBeginTermInStock frmBeginTermInStock1 = new BGT.frmBeginTermInStock();
                frmBeginTermInStock1.ShowDialog();
            }
            if (cboBeginTermInStock.Text == "frmBeginTermMoneyCal")
            {
                ERP.Inventory.DUI.BGT.frmBeginTermMoneyCal frmBeginTermMoneyCal = new BGT.frmBeginTermMoneyCal();
                frmBeginTermMoneyCal.ShowDialog();
            }
            if (cboBeginTermInStock.Text == "frmSearchBeginTermMoney")
            {
                ERP.Inventory.DUI.BGT.frmSearchBeginTermMoney frmSearchBeginTermMoney = new BGT.frmSearchBeginTermMoney();
                frmSearchBeginTermMoney.ShowDialog();
            }
            if (cboBeginTermInStock.Text == "frmCumulationTimeOffCal")
            {
                ERP.Inventory.DUI.BGT.frmCumulationTimeOffCal frmBeginTermInStock1 = new BGT.frmCumulationTimeOffCal();
                frmBeginTermInStock1.ShowDialog();
            }
        }

        private void btnInventory_Click(object sender, EventArgs e)
        {
            if (cboInventory.Text == "frmCreateQuickInventory")
            {
                frmCreateQuickInventory frmCreateQuickInventory = new frmCreateQuickInventory();
                frmCreateQuickInventory.ShowDialog();
            }

            if (cboInventory.Text == "AverageMonth.frmAverageMonthList")
            {
                ERP.Inventory.DUI.AverageMonth.frmAverageMonthList frmAverageMonthList = new ERP.Inventory.DUI.AverageMonth.frmAverageMonthList();
                frmAverageMonthList.ShowDialog();
            }
            if (cboInventory.Text == "frmInventoryTerm")
            {
                ERP.Inventory.DUI.Inventory.frmInventoryTerm frmInventoryTerm1 = new Inventory.frmInventoryTerm();
                frmInventoryTerm1.ShowDialog();
            }

            if (cboInventory.Text == "frmInventory")
            {
                ERP.Inventory.DUI.Inventory.frmInventory frmInventory = new Inventory.frmInventory();
                frmInventory.ShowDialog();
            }
            else if (cboInventory.Text == "frmInventoryInput")
            {
                ERP.Inventory.DUI.Inventory.frmInventoryInput frmInventoryInput1 = new Inventory.frmInventoryInput();
                frmInventoryInput1.ShowDialog();
            }
            else if (cboInventory.Text == "frmInventoryManager")
            {
                ERP.Inventory.DUI.Inventory.frmInventoryManager frmInventoryManager1 = new Inventory.frmInventoryManager();
                //frmInventoryManager1.Tag = "TESTFORMTAG=5678";
                frmInventoryManager1.ShowDialog();
            }
            else if (cboInventory.Text == "frmResolveManager")
            {
                ERP.Inventory.DUI.Inventory.frmResolveManager frmResolveManager1 = new Inventory.frmResolveManager();
                frmResolveManager1.ShowDialog();//
            }
            else if (cboInventory.Text == "frmQuickInventorySearch")
            {
                ERP.Inventory.DUI.Inventory.frmQuickInventorySearch frm1 = new Inventory.frmQuickInventorySearch();
                frm1.ShowDialog();
            }
            else if (cboInventory.Text == "frmInventoryJoin")
            {
                ERP.Inventory.DUI.Inventory.frmInventoryJoin frm1 = new Inventory.frmInventoryJoin();
                frm1.ShowDialog();
            }
            else if (cboInventory.Text == "frmInventoryDifferenceResult")
            {
                ERP.Inventory.DUI.Inventory.frmInventoryDifferenceResult frm = new Inventory.frmInventoryDifferenceResult();
                frm.ShowDialog();
            }
            else if (cboInventory.Text == "frmInventorySuperJoin")
            {
                ERP.Inventory.DUI.Inventory.frmInventorySuperJoin frm = new Inventory.frmInventorySuperJoin();
                frm.ShowDialog();
            }
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            ERP.Inventory.DUI.IMEI.frmProductIMEIHistory frmProductIMEIHistory1 = new IMEI.frmProductIMEIHistory();
            frmProductIMEIHistory1.ShowDialog();
        }

        private void btnWarranty_Click(object sender, EventArgs e)
        {
            if (comboBox2.Text == "frmWarrantyInfo.cs")
            {
                ERP.Inventory.DUI.Warranty.frmWarrantyInfoManager frmWarrantyInfoManager1 = new Warranty.frmWarrantyInfoManager();
                frmWarrantyInfoManager1.ShowDialog();
            }

            if (comboBox2.Text == "frmWarrantyRegisterManager.cs")
            {
                ERP.Inventory.DUI.Warranty.frmWarrantyRegisterManager frmWarrantyInfoManager1 = new Warranty.frmWarrantyRegisterManager();
                frmWarrantyInfoManager1.ShowDialog();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IMEISales.frmIMEISalesInfo_OrderIndexEdit frm = new IMEISales.frmIMEISalesInfo_OrderIndexEdit();
            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            IMEISales.frmIMEISalesInfoManager frm = new IMEISales.frmIMEISalesInfoManager();
            frm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (cboPM.Text == "frmLotIMEISalesInfo")
            {
                ERP.Inventory.DUI.PM.frmLotIMEISalesInfo frmLotIMEISalesInfo = new PM.frmLotIMEISalesInfo();
                frmLotIMEISalesInfo.ShowDialog();
            }
            if (cboPM.Text == "frmProductIMEIHistory_Old")
            {
                ERP.Inventory.DUI.IMEI.frmProductIMEIHistory_Old frmProductIMEIHistory_Old = new IMEI.frmProductIMEIHistory_Old();
                frmProductIMEIHistory_Old.ShowDialog();
            }


        }

        private void btnInputChangeOrderInput_Click(object sender, EventArgs e)
        {
            ERP.Inventory.DUI.InputChangeOrder.frmInputChangeOrderInput frm1 = new InputChangeOrder.frmInputChangeOrderInput();
            frm1.ShowDialog();
        }

        private void butbtnInputChangeOrderInputton5_Click(object sender, EventArgs e)
        {
            ERP.Inventory.DUI.InputChangeOrder.frmInputChangeOrderManager frm1 = new InputChangeOrder.frmInputChangeOrderManager();
            frm1.Tag = "ReportID=74&ReportID1=82&ReportID2=74&ReportID3=85&ReportID4=84&COMPANY=1&TYPE=1";
            frm1.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (cboIMEI.Text == "frmProductIMEI_OrderIndexEdit")
            {
                ERP.Inventory.DUI.IMEI.frmProductIMEI_OrderIndexEdit frmLotIMEISalesInfo = new IMEI.frmProductIMEI_OrderIndexEdit();
                frmLotIMEISalesInfo.Show();
            }
            if (cboIMEI.Text == "frmProductIMEI_OrderIndexSearch")
            {
                ERP.Inventory.DUI.IMEI.frmProductIMEI_OrderIndexSearch frmLotIMEISalesInfo = new IMEI.frmProductIMEI_OrderIndexSearch();
                frmLotIMEISalesInfo.Show();
            }
        }

        private void btnSplitProduct_Click(object sender, EventArgs e)
        {
            SplitProduct.frmSplitProduct frm = new SplitProduct.frmSplitProduct();
            frm.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            bool bolCheck = Library.AppCore.Other.CheckObject.CheckProductID(textBox1.Text);
        }

        private void label1_DoubleClick(object sender, EventArgs e)
        {
            Clipboard.SetText(label1.Text);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ERP.Inventory.DUI.IMEIChange.frmIMEIChangeManager frm1 = new IMEIChange.frmIMEIChangeManager();
            frm1.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            object obj = Library.AppCore.AppConfig.GetConfigValue("testconfig");
            int intconfigValue = Convert.ToInt32(obj);
            MessageBox.Show(intconfigValue.ToString());
        }

        private void button10_Click(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            frmPriceStockManager frm = new frmPriceStockManager();
            frm.Tag = "PRODUCTID=A";
            frm.ShowDialog();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            ERP.Inventory.DUI.InputChangeOrder.frmInputChangeOrderInput frm = new InputChangeOrder.frmInputChangeOrderInput();
            frm.IsViettel = true;
            frm.ShowDialog();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            ERP.Inventory.DUI.Output.frmOutputVoucherManager frm = new Output.frmOutputVoucherManager();
            frm.Tag = "COMPANY=1";
            frm.ShowDialog();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            frmPriceStockManager frm = new frmPriceStockManager();
            frm.Tag = "PRODUCTID=2015615100001";
            frm.Show();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            frmPriceProtectStock frm = new frmPriceProtectStock();
            frm.Tag = "PRODUCTID=2015615100001";
            frm.Show();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Search.frmMachineErrorSearch frm = new Search.frmMachineErrorSearch();
            frm.ShowDialog();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            StoreChange.frmEvictionIMEI frm = new StoreChange.frmEvictionIMEI();
            frm.Tag = "AutoCreateOrder=1";
            frm.Show();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            ERP.Inventory.DUI.Consignment.frmConsignmentArisingReport frm = new Consignment.frmConsignmentArisingReport();
            frm.Tag = "ProductTypeID=" + txtProductTypeID.Text.Trim();
            frm.ShowDialog();
        }

        private void cboInventory_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button21_Click(object sender, EventArgs e)
        {
            frmInventoryOnWay frm = new frmInventoryOnWay();
            frm.ShowDialog();
        }

        private void button20_Click(object sender, EventArgs e)
        {
            frmInventoryTermStock frm = new frmInventoryTermStock();
            frm.ShowDialog();
        }

        private void button22_Click(object sender, EventArgs e)
        {
            frmInventoryOnWayReport frm = new frmInventoryOnWayReport();
            frm.ShowDialog();
        }

        private void button23_Click(object sender, EventArgs e)
        {
            ERP.Inventory.DUI.PM.frmIMEIExcludeFIFORequest frm = new PM.frmIMEIExcludeFIFORequest();
            frm.ShowDialog();
        }

        StoreChange.frmStoreChangeOrderManager _frmStoreChangeOrderManager;
        private void btnQuanLyYeuCauChuyenKho_Click(object sender, EventArgs e)
        {
            if (_frmStoreChangeOrderManager == null || _frmStoreChangeOrderManager.IsDisposed)
            {
                _frmStoreChangeOrderManager = new StoreChange.frmStoreChangeOrderManager();
                _frmStoreChangeOrderManager.WindowState = FormWindowState.Maximized;
                _frmStoreChangeOrderManager.Tag = "StoreChangeReportID=22&StoreChange3_A4=23&StoreChange3_A5=26&StoreChange3_A6=27&storeChangeCommandID=133&internalOutput_TransferID=132";
                _frmStoreChangeOrderManager.Show(this);
            }
            _frmStoreChangeOrderManager.Activate();
        }

        StoreChange.frmStoreChangeManager _frmStoreChangeManager;
        private void btnQuanLyXuatChuyenKho_Click(object sender, EventArgs e)
        {
            if (_frmStoreChangeManager == null || _frmStoreChangeManager.IsDisposed)
            {
                _frmStoreChangeManager = new StoreChange.frmStoreChangeManager();
                _frmStoreChangeManager.WindowState = FormWindowState.Maximized;
                _frmStoreChangeManager.Tag = "StoreChangeReportID=22&StoreChange3_A4=23&StoreChange3_A5=26&StoreChange3_A6=27&storeChangeCommandID=133&internalOutput_TransferID=132";
                _frmStoreChangeManager.Show(this);
            }
            _frmStoreChangeManager.Activate();
        }      
    }
}
