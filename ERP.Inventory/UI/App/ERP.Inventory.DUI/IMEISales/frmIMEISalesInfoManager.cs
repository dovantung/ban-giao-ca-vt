﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using ERP.Inventory.PLC.IMEISales;
using ERP.Inventory.PLC.IMEISales.WSIMEISalesInfo;
using ERP.Inventory.DUI.IMEISale;
using System.IO;

namespace ERP.Inventory.DUI.IMEISales
{
    public partial class frmIMEISalesInfoManager : Form
    {
        #region Variable
        private string strPermission_Add = "PM_IMEISALESINFO_ADD";
        private string strPermission_Edit = "PM_IMEISALESINFO_EDIT";
        private string strPermission_Delete = "PM_IMEISALESINFO_DELETE";
        private string strPermission_ExportExcel = "PM_IMEISALESINFO_EXPORTEXCEL";
        private string strPermission_Review = "PM_IMEISALESINFO_REVIEW";
        private string strPermission_UnReview = "PM_IMEISALESINFO_UNREVIEW";
        private string strPermission_CommentView = "PM_IMEISALESINFO_COMMENT_VIEW";

        private bool bolPermission_Add = false;
        private bool bolPermission_Edit = false;
        private bool bolPermission_Delete = false;
        private bool bolPermission_ExportExcel = false;
        private bool bolPermission_Review = false;
        private bool bolPermission_UnReview = false;
        private bool bolPermission_CommentView = false;

        private int intMainGroupID = -1;
        private int intStoreID = Library.AppCore.SystemConfig.ConfigStoreID;
        private string strInputVoucherDetailID = string.Empty;
        private string strReviewedUser = string.Empty;
        private DateTime? dteReviewedDate = null;
        private bool bolIsReview = false;
        private DateTime dtEndWarantyDate = DateTime.Now;
        private DataTable dtbDataSpecStatus = null;
        private List<MasterData.PLC.MD.WSProductSpec_SpecStatus.ProductSpec_SpecStatus> lstProductSpecStatus = null;
        private List<IMEISalesInfo_Images> objProduct_ImagesList = new List<IMEISalesInfo_Images>();
        private List<IMEISalesInfo_Images> objProduct_ImagesList_Del = new List<IMEISalesInfo_Images>();
        private PLC.IMEISales.PLCIMEISalesInfo objPLCIMEISalesInfo = new PLC.IMEISales.PLCIMEISalesInfo();
        #endregion

        #region Constructor
        public frmIMEISalesInfoManager()
        {
            InitializeComponent();
        }
        #endregion

        #region Event
        private void frmIMEISalesInfoManager_Load(object sender, EventArgs e)
        {
            InitControl();
            LoadPermission();
            FormState = FormStateType.LIST;
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            if (!ValidateSearch())
            {
                return;
            }
            LoadData();
            FormState = FormStateType.LIST;
        }

        private bool ValidateSearch()
        {
            if (!ERP.MasterData.DUI.Common.CommonFunction.EndDateValidation(dtpFromDate.Value, dtpToDate.Value, false))
            {
                dtpToDate.Focus();
                MessageBox.Show(this, "Đến ngày không thể nhỏ hơn từ ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            DateTime dtFromDate = dtpFromDate.Value;
            dtFromDate = dtFromDate.AddMonths(1);
            DateTime dtToDate = dtpToDate.Value;
            if (dtFromDate.CompareTo(dtToDate) < 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chỉ xem báo cáo trong vòng 1 tháng!");
                dtpToDate.Focus();
                return false;
            }
            return true;
        }
        private void cmdExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(this.grdData);
        }

        private void mnuItemAdd_Click(object sender, EventArgs e)
        {
            if (!btnAdd.Enabled)
                return;
            ClearValueControl();
            FormState = FormStateType.ADD;
        }

        private void mnuItemDelete_Click(object sender, EventArgs e)
        {
            if (!this.mnuItemDelete.Enabled) return;
        }

        private void mnuItemEdit_Click(object sender, EventArgs e)
        {
            if (!btnEdit.Enabled)
                return;
            if (grdViewData.FocusedRowHandle < 0 || grdViewData.RowCount == 0)
                return;
            if (!EditData())
                return;
            FormState = FormStateType.EDIT;
        }

        private void mnuItemExport_Click(object sender, EventArgs e)
        {
            cmdExportExcel_Click(null, null);
        }

        private void grvProductSpec_DoubleClick(object sender, EventArgs e)
        {
            if (this.grdViewData.FocusedRowHandle < 0) return;
            this.mnuItemEdit_Click(null, null);
        }
        #endregion

        #region Method
        private void LoadPermission()
        {
            bolPermission_Add = SystemConfig.objSessionUser.IsPermission(strPermission_Add);
            bolPermission_Delete = SystemConfig.objSessionUser.IsPermission(strPermission_Delete);
            bolPermission_Edit = SystemConfig.objSessionUser.IsPermission(strPermission_Edit);
            bolPermission_ExportExcel = SystemConfig.objSessionUser.IsPermission(strPermission_ExportExcel);
            bolPermission_Review = SystemConfig.objSessionUser.IsPermission(strPermission_Review);
            bolPermission_UnReview = SystemConfig.objSessionUser.IsPermission(strPermission_UnReview);
            bolPermission_CommentView = SystemConfig.objSessionUser.IsPermission(strPermission_CommentView);
        }

        private void InitControl()
        {
            cboInputStore.InitControl(true);
            grdViewData.DoubleClick += new EventHandler(grvProductSpec_DoubleClick);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(this.grdViewData);
            LoadDataForRepCombo();
            
        }

        private void LoadDataForRepCombo()
        {
            int intMainGroupID = -1;
            object[] objKeywords = new object[] { "@MainGroupID", intMainGroupID };
            ERP.Inventory.PLC.IMEISales.PLCIMEISalesInfo objPLCIMEISalesInfo = new PLC.IMEISales.PLCIMEISalesInfo();
            dtbDataSpecStatus = objPLCIMEISalesInfo.SearchDataSpecStatus(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            DataRow dtrSpecStatus = dtbDataSpecStatus.NewRow();
            dtrSpecStatus["ProductSpecID"] = -1;
            dtrSpecStatus["ProductSpecStatusID"] = -1;
            dtrSpecStatus["ProductSpecStatusName"] = "--Chọn trạng thái--";
            dtrSpecStatus["IsCanPrint"] = 0;
            dtbDataSpecStatus.Rows.Add(dtrSpecStatus);
            dtbDataSpecStatus.DefaultView.Sort = "ProductSpecStatusID asc";
            dtbDataSpecStatus = dtbDataSpecStatus.DefaultView.ToTable();
            lstProductSpecStatus = new List<MasterData.PLC.MD.WSProductSpec_SpecStatus.ProductSpec_SpecStatus>();
            foreach (DataRow row in dtbDataSpecStatus.Rows)
            {
                MasterData.PLC.MD.WSProductSpec_SpecStatus.ProductSpec_SpecStatus objProductSpecStatus = new MasterData.PLC.MD.WSProductSpec_SpecStatus.ProductSpec_SpecStatus();
                objProductSpecStatus.ProductSpecID = Convert.ToInt32(row["ProductSpecID"]);
                objProductSpecStatus.ProductSpecStatusID = Convert.ToInt32(row["ProductSpecStatusID"]);
                objProductSpecStatus.ProductSpecStatusName = Convert.ToString(row["ProductSpecStatusName"]);
                objProductSpecStatus.IsCanPrint = Convert.ToBoolean(row["IsCanPrint"]);
                lstProductSpecStatus.Add(objProductSpecStatus);
            }

            DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboProduct = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            cboProduct.NullText = "--Chọn trạng thái--";
            cboProduct.DisplayMember = "ProductSpecStatusName";
            cboProduct.ValueMember = "ProductSpecStatusID";
            cboProduct.DataSource = lstProductSpecStatus;
            cboProduct.PopupWidth = 400;
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col = new DevExpress.XtraEditors.Controls.LookUpColumnInfo();
            col.Caption = "Mã trạng thái";
            col.FieldName = "ProductSpecStatusID";
            cboProduct.Columns.Add(col);
            DevExpress.XtraEditors.Controls.LookUpColumnInfo col1 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo();
            col1.Caption = "Tên trạng thái";
            col1.FieldName = "ProductSpecStatusName";
            cboProduct.Columns.Add(col1);
            cboProduct.Enter += new EventHandler(cboProduct_Enter);
            //cboProduct.ed += new EventHandler(cboProduct_Enter);
            grvProductSpec.Columns["ProductSpecStatusID"].ColumnEdit = cboProduct;
        }

        private void cboProduct_Enter(object sender, EventArgs e)
        {
            try
            {
                if (grvProductSpec.FocusedColumn.FieldName == "ProductSpecStatusID")
                {
                    IMEISalesInfo_ProSpec objLotIMEISalesInfo_ProSpec = (IMEISalesInfo_ProSpec)grvProductSpec.GetFocusedRow();
                    if (objLotIMEISalesInfo_ProSpec.IsUsedBySalesInfo)
                    {
                        List<MasterData.PLC.MD.WSProductSpec_SpecStatus.ProductSpec_SpecStatus> lstProductSpecStatusTemp = lstProductSpecStatus.FindAll(p => (p.ProductSpecID == objLotIMEISalesInfo_ProSpec.ProductSpecID || p.ProductSpecStatusID == -1));
                        DevExpress.XtraEditors.LookUpEdit edit = ((DevExpress.XtraEditors.LookUpEdit)(sender));
                        edit.Properties.DataSource = lstProductSpecStatusTemp;
                    }
                    
                }
            }
            catch { }
        }

        private void LoadData()
        {
            DataTable dtbData = null;
            object[] objKeywords = new object[] { "@FromDate", dtpFromDate.Checked ? dtpFromDate.Value : Convert.ToDateTime("1800-01-01"),
                                                  "@ToDate", dtpToDate.Checked ? dtpToDate.Value : DateTime.Now,
                                                  "@ProductIDList", cboProductList.ProductIDList,
                                                  "@StoreIDList", cboInputStore.StoreIDList,
                                                  "@IMEI", txtIMEISearch.Text.Trim()};
            dtbData = this.objPLCIMEISalesInfo.SearchData(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            this.grdData.DataSource = dtbData;
        }

        private bool LoadDataComment(ref DataTable dtbData)
        {
            object[] objKeywords = new object[] { 
                                                  "@ProductID", grdViewData.GetFocusedRowCellValue("PRODUCTID"),
                                                  "@IMEI", grdViewData.GetFocusedRowCellValue("IMEI")
            };
            dtbData = this.objPLCIMEISalesInfo.SearchDataComment(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Hàm lấy dữ liệu mặc định các control
        /// </summary>
        private void ClearValueControl()
        {
            tabCtrlDetail.SelectedTab = tabDetailInfo;
            txtProductID.Text = string.Empty;
            txtProductName.Text = string.Empty;
            txtIMEI.Text = string.Empty;
            chkIsBrandNewWarranty.Checked = false;
            dteEndWarantyDate.Value = DateTime.Now;
            txtNote.Text = string.Empty;
            txtPrintVATContent.Text = string.Empty;
            txtWebContent.Text = string.Empty;
            chkIsComfirm.Checked = false;
            chkIsReviewed.Checked = false;
            grdProduct.DataSource = new List<IMEISalesInfo_ProSpec>();
            grvProductSpec.Columns["ProductSpecStatusID"].OptionsColumn.AllowEdit = false;
            objProduct_ImagesList = new List<IMEISalesInfo_Images>();
            grdImages.DataSource = objProduct_ImagesList;
        }

        private bool EditData()
        {
            tabCtrlDetail.SelectedTab = tabDetailInfo;
            try
            {
                DataRow dtRow = (DataRow)grdViewData.GetFocusedDataRow();
                strIMEI = dtRow["IMEI"].ToString().Trim();
                string strProductID = dtRow["ProductID"].ToString().Trim();
                IMEISalesInfo objIMEISalesInfo = objPLCIMEISalesInfo.LoadInfo(strIMEI, strProductID);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                }
                txtIMEI.Text = strIMEI;
                txtProductID.Text = strProductID;
                txtProductName.Text = dtRow["ProductName"].ToString().Trim();
                txtPrintVATContent.Lines = objIMEISalesInfo.PrintVATContent.Split('\n');
                txtWebContent.Lines = objIMEISalesInfo.WebProductSpecContent.Split('\n');
                txtNote.Lines = objIMEISalesInfo.Note.Split('\n');
                chkIsBrandNewWarranty.Checked = objIMEISalesInfo.IsBrandNewWarranty;
                if (objIMEISalesInfo.EndWarrantyDate == null)
                {
                    dteEndWarantyDate.Text = string.Empty;
                }
                else
                {
                    dteEndWarantyDate.Value = objIMEISalesInfo.EndWarrantyDate.Value;
                }
                strInputVoucherDetailID = objIMEISalesInfo.InputVoucherDetailID;
                intStoreID = objIMEISalesInfo.StoreID;
                chkIsComfirm.Checked = objIMEISalesInfo.IsConfirm;
                chkIsReviewed.Checked = objIMEISalesInfo.IsReviewed;
                strReviewedUser = objIMEISalesInfo.ReviewedUser;
                intMainGroupID = Convert.ToInt32(dtRow["MainGroupID"].ToString());
                dteReviewedDate = objIMEISalesInfo.ReviewedDate;
                bolIsReview = chkIsReviewed.Checked;
                dtEndWarantyDate = dteEndWarantyDate.Value;
                foreach (var item in objIMEISalesInfo.IMEISalesInfo_ProSpec)
                {
                    if (item.IsUsedBySalesInfo == true && lstProductSpecStatus.Find(p => p.ProductSpecStatusID == item.ProductSpecStatusID) == null)
                    {
                        item.ProductSpecStatusID = -1;
                    }
                }

                try
                {
                    grdProduct.DataSource = objIMEISalesInfo.IMEISalesInfo_ProSpec.ToList();
                    objProduct_ImagesList = objIMEISalesInfo.IMEISalesInfo_ImagesList.OrderBy(img => img.OrderIndex).ToList();
                    grdImages.DataSource = objProduct_ImagesList;
                    grdImages.RefreshDataSource();
                }
                catch 
                {

                }
       
                chkIsReviewed.Enabled = ((!chkIsReviewed.Checked && bolPermission_Review) || (chkIsReviewed.Checked && bolPermission_UnReview));
                EnableReview(!chkIsReviewed.Checked && bolPermission_Edit);
                grvProductSpec.Columns["ProductSpecStatusID"].OptionsColumn.AllowEdit = false;
                List<IMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpec = (List<IMEISalesInfo_ProSpec>)grdProduct.DataSource;
                txtWebContent.Text = GetWebProductSpecContent(lstIMEISalesInfo_ProSpec);
                lstIMEISalesInfo_ProSpec = lstIMEISalesInfo_ProSpec.FindAll(p => p.IsCanPrint == true);
                txtPrintVATContent.Text = GetWebProductSpecContent(lstIMEISalesInfo_ProSpec);
                return true;
            }
            catch (Exception objExc)
            {
                //MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin bán hàng của IMEI");
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi nạp thông tin bán hàng của IMEI", objExc.Message + System.Environment.NewLine + objExc.StackTrace);
                SystemErrorWS.Insert("Lỗi nạp thông tin bán hàng của IMEI", objExc.ToString() + System.Environment.NewLine + objExc.StackTrace, this.Name + " -> EditData", Globals.ModuleName);
                return false;
            }
        }

        private bool UpdateData()
        {
            string strMessage = string.Empty;
            try
            {
                PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo objIMEISalesInfo = new PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo();
                objIMEISalesInfo.ProductID = txtProductID.Text.Trim();
                objIMEISalesInfo.IMEI = txtIMEI.Text.Trim();
                objIMEISalesInfo.InputVoucherDetailID = (strInputVoucherDetailID.Trim() == string.Empty) ? "-1" : strInputVoucherDetailID.Trim();
                objIMEISalesInfo.StoreID = intStoreID;
                objIMEISalesInfo.IsBrandNewWarranty = chkIsBrandNewWarranty.Checked;
                objIMEISalesInfo.EndWarrantyDate = dteEndWarantyDate.Value.Date;
                objIMEISalesInfo.Note = txtNote.Text.Trim();
                objIMEISalesInfo.IsConfirm = chkIsComfirm.Checked;
                objIMEISalesInfo.IsReviewed = chkIsReviewed.Checked;
                objIMEISalesInfo.ReviewedUser = strReviewedUser;
                objIMEISalesInfo.ReviewedDate = dteReviewedDate;
                //Hủy duyệt
                if (!bolIsReview && objIMEISalesInfo.IsReviewed)
                {
                    objIMEISalesInfo.ReviewedUser = SystemConfig.objSessionUser.UserName;
                    objIMEISalesInfo.ReviewedDate = DateTime.Now;
                }
                else if (bolIsReview && !objIMEISalesInfo.IsReviewed)
                {
                    objIMEISalesInfo.ReviewedUser = string.Empty;
                    objIMEISalesInfo.ReviewedDate = null;
                }
                objIMEISalesInfo.PrintVATContent = txtPrintVATContent.Text.Trim(',');
                List<IMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpec = (List<IMEISalesInfo_ProSpec>)grdProduct.DataSource;
                //lstIMEISalesInfo_ProSpec = lstIMEISalesInfo_ProSpec.FindAll(p => p.ProductSpecStatusID != -1);
                objIMEISalesInfo.IMEISalesInfo_ProSpec = lstIMEISalesInfo_ProSpec.ToArray();
                objIMEISalesInfo.WebProductSpecContent = txtWebContent.Text.Trim();//GetWebProductSpecContent(lstIMEISalesInfo_ProSpec);
                if (objProduct_ImagesList != null && objProduct_ImagesList.Count > 0)
                {
                    var NewImages = from img in objProduct_ImagesList
                                    where img.IsNew == true
                                    select img;
                    if (NewImages != null && NewImages.Count() > 0)
                    {
                        List<IMEISalesInfo_Images> objNewList = NewImages.ToList();
                        foreach (var item in objNewList)
                        {
                            string strLargesizeImageFileID = string.Empty;
                            item.LargeSizeImage = SaveAttachment(ref strLargesizeImageFileID, item.LargesizeImage_Local);
                            item.LargeSizeImageFileID = strLargesizeImageFileID;
                            string strMediumSizeImageFileID = string.Empty;
                            item.MediumSizeImage = SaveAttachment(ref strMediumSizeImageFileID, item.MediumSizeImage_Local);
                            item.MediumSizeImageFileID = strMediumSizeImageFileID;
                            string strSmallSizeImageFileID = string.Empty;
                            item.SmallSizeImage = SaveAttachment(ref strSmallSizeImageFileID, item.SmallSizeImage_Local);
                            item.SmallSizeImageFileID = strSmallSizeImageFileID;
                            item.IsNew = false;
                        }
                    }
                }
                objIMEISalesInfo.IMEISalesInfo_ImagesList = objProduct_ImagesList.ToArray();
                //objProduct.Product_ImagesList_Del = objProduct_ImagesList_Del.ToArray();
                if (FormState == FormStateType.EDIT)
                {
                    strMessage = "Cập nhật ";
                    objIMEISalesInfo.UpdatedUser = Library.AppCore.SystemConfig.objSessionUser.UserName;
                    if (!this.objPLCIMEISalesInfo.Update(objIMEISalesInfo))
                    {
                        MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return false;
                    }
                }
                else
                {
                    strMessage = "Thêm ";
                    objIMEISalesInfo.CreatedUser = Library.AppCore.SystemConfig.objSessionUser.UserName;
                    if (!this.objPLCIMEISalesInfo.Insert(objIMEISalesInfo))
                    {
                        MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return false;
                    }
                }
                MessageBoxObject.ShowInfoMessage(this, strMessage + "thông tin bán hàng của IMEI thành công !");
                return true;
            }
            catch (Exception objExc)
            {
                //MessageBoxObject.ShowWarningMessage(this, "Lỗi " + strMessage + "thông tin bán hàng của IMEI !");
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi " + strMessage + "thông tin bán hàng của IMEI !", objExc.Message + System.Environment.NewLine + objExc.StackTrace);
                SystemErrorWS.Insert("Lỗi " + strMessage + "thông tin bán hàng của IMEI !", objExc.ToString() + System.Environment.NewLine + objExc.StackTrace, this.Name + " -> UpdateData", Globals.ModuleName);
                return false;
            }
        }

        /// <summary>
        /// Hàm load dữ liệu lên lưới
        /// </summary>
        private void SearchData_IMEISalesInfo_ProSpec_ToList()
        {
            List<IMEISalesInfo_ProSpec> lstLotIMEISalesInfo_ProSpec = null;
            object[] objKeywords = new object[]
            {
                "@ProductID", txtProductID.Text.Trim()
            };
            lstLotIMEISalesInfo_ProSpec = objPLCIMEISalesInfo.SearchData_IMEISalesInfo_ProSpec_ToList(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
            else
            {
                grdProduct.DataSource = lstLotIMEISalesInfo_ProSpec;
            }
        }

        private string GetWebProductSpecContent(List<IMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpec)
        {
            string strWebProductSpecContent = string.Empty;
            List<IMEISalesInfo_ProSpec> lstUsed = new List<IMEISalesInfo_ProSpec>();
            foreach (IMEISalesInfo_ProSpec item in lstIMEISalesInfo_ProSpec)
            {
                IMEISalesInfo_ProSpec IMEISalesInfo_ProSpecTemp = lstUsed.Find(p => p.ProductID == item.ProductID && p.ProductID == item.ProductID && p.ProductSpecID == item.ProductSpecID);
                if (IMEISalesInfo_ProSpecTemp == null)
                {
                    lstUsed.Add(item);
                    List<IMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpecTemp = lstIMEISalesInfo_ProSpec.FindAll
                                                                (p => p.ProductID == item.ProductID && p.ProductID == item.ProductID && p.ProductSpecID == item.ProductSpecID);
                    if (item.ProductSpecStatusID > 0)
                    {

                        //strWebProductSpecContent += item.ProductSpecName;
                        //strWebProductSpecContent += " : ";
                        foreach (IMEISalesInfo_ProSpec item1 in lstIMEISalesInfo_ProSpecTemp)
                        {
                            if (item1.ProductSpecStatusID > 0)
                            {
                                DataRow[] dr = dtbDataSpecStatus.Select("ProductSpecStatusID = " + item1.ProductSpecStatusID);
                                if (dr != null || dr.Length > 0)
                                {
                                    strWebProductSpecContent += dr[0]["ProductSpecStatusName"].ToString().Trim();
                                    if (item.Note != string.Empty )
	                                {
                                        strWebProductSpecContent += "(" + item.Note + ")";
	                                }
                                    strWebProductSpecContent += ",";
                                }
                            }
                        }
                        strWebProductSpecContent = strWebProductSpecContent.TrimEnd(',');
                        strWebProductSpecContent += " ; ";
                    }
                }
            }
            strWebProductSpecContent = strWebProductSpecContent.TrimEnd(';');
            return strWebProductSpecContent;
        }

        /// <summary>
        /// Upload File
        /// </summary>
        /// <param name="strFileID"></param>
        /// <param name="strUrlLocal"></param>
        /// <returns>đường dẫn trên server</returns>
        private string SaveAttachment(ref string strFileID, string strUrlLocal)
        {
            //Tạo trạng thái chờ trong khi đính kèm tập tin
            string strURL = string.Empty;
            this.Refresh();
            try
            {
                if (strUrlLocal != string.Empty)
                {
                    FileInfo objFileInfo = new FileInfo(strUrlLocal);
                    if (objFileInfo != null)
                    {
                        ERP.FMS.DUI.DUIFileManager.UploadFile(this, "FMSAplication_ProERP_ProductImage", strUrlLocal, ref strFileID, ref strURL);
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        }
                    }
                }
            }
            catch (Exception objEx)
            {
                //MessageBox.Show("Lỗi đính kèm tập tin ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi đính kèm tập tin ", objEx.Message + System.Environment.NewLine + objEx.StackTrace);
                SystemErrorWS.Insert("Lỗi đính kèm tập tin ", objEx + System.Environment.NewLine + objEx.StackTrace, DUIInventory_Globals.ModuleName);
                return string.Empty;
            }
            //Bỏ trạng thái chờ sau khi đính kèm tập tin			

            return strURL;
        }

        private void EnableReview(bool bolIsEnale)
        {
            //txtIMEI.ReadOnly = !bolIsEnale;
            txtNote.ReadOnly = !bolIsEnale;
            txtPrintVATContent.ReadOnly = !bolIsEnale;
            txtProductID.ReadOnly = !bolIsEnale;
            txtProductName.ReadOnly = !bolIsEnale;
            chkIsBrandNewWarranty.Enabled = bolIsEnale;
            dteEndWarantyDate.Enabled = chkIsBrandNewWarranty.Checked && bolIsEnale;            

            btnAddNewImage.Enabled = bolIsEnale;
            btnDelImage.Enabled = bolIsEnale;
            btnEditImage.Enabled = bolIsEnale;
            btnMoveDown.Enabled = bolIsEnale;
            btnMoveUp.Enabled = bolIsEnale;
            grvProductSpec.Columns["Note"].OptionsColumn.AllowEdit = bolIsEnale;
            if (!bolIsEnale)
	        {
                grvProductSpec.Columns["ProductSpecStatusID"].OptionsColumn.AllowEdit = false;
	        }

            //foreach (DevExpress.XtraGrid.Columns.GridColumn item in grvProductSpec.Columns)
            //{
            //    item.OptionsColumn.AllowEdit = bolIsEnale;
            //}
            //
            chkIsComfirm.Enabled = bolIsEnale;
            mnuIMAGE.Enabled = bolIsEnale;
        }

        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(txtIMEI.Text.Trim()))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập IMEI !");
                tabCtrlDetail.SelectedTab = tabDetailInfo;
                txtIMEI.Focus();
                return false;
            }
            //if (FormState == FormStateType.ADD)
            //{            
            //    DataTable dtbIMEI = objPLCIMEISalesInfo.CheckIMEI(txtIMEI.Text.Trim());
            //    if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            //    {
            //        Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            //        return false;
            //    }
            //    if (dtbIMEI != null)
            //    {
            //        if (dtbIMEI.Rows.Count == 0)
            //        {
            //            MessageBoxObject.ShowWarningMessage(this, "IMEI không tồn tại!");
            //            txtProductID.Text = string.Empty;
            //            txtProductName.Text = string.Empty;
            //            grdProduct.DataSource = new List<IMEISalesInfo_ProSpec>();
            //            tabCtrlDetail.SelectedTab = tabDetailInfo;
            //            txtIMEI.Focus();
            //            return false;
            //        }
            //        else if (Convert.ToBoolean(dtbIMEI.Rows[0]["ISNEW"]))
            //        {
            //            MessageBoxObject.ShowWarningMessage(this, "IMEI là mới nên không thể thêm vào!");
            //            txtProductID.Text = string.Empty;
            //            txtProductName.Text = string.Empty;
            //            grdProduct.DataSource = new List<IMEISalesInfo_ProSpec>();
            //            tabCtrlDetail.SelectedTab = tabDetailInfo;
            //            txtIMEI.Focus();
            //            return false;
            //        }
            //        else if (Convert.ToBoolean(dtbIMEI.Rows[0]["ISEXISTS"]))
            //        {
            //            MessageBoxObject.ShowWarningMessage(this, "IMEI đã thuộc thông tin bán hàng khác!");
            //            txtProductID.Text = string.Empty;
            //            txtProductName.Text = string.Empty;
            //            grdProduct.DataSource = new List<IMEISalesInfo_ProSpec>();
            //            tabCtrlDetail.SelectedTab = tabDetailInfo;
            //            txtIMEI.Focus();
            //            return false;
            //        }
            //    }
            //}
            //Hủy duyệt và không thay đổi ngày hết hạn bảo hành thì không check
            if (!(bolIsReview && !chkIsReviewed.Checked && dtEndWarantyDate == dteEndWarantyDate.Value))
            {
                if (this.chkIsBrandNewWarranty.Checked)
                {
                    if (this.dteEndWarantyDate.Value.CompareTo(DateTime.Now.Date) < 0)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Ngày hết hạn bảo hành không thể nhỏ hơn ngày hiện tại !");
                        this.dteEndWarantyDate.Focus();
                        return false;
                    }
                }
            }

            if (FormState == FormStateType.ADD)
            {
                PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo objIMEISalesInfo = this.objPLCIMEISalesInfo.LoadInfo(this.txtIMEI.Text.Trim(), this.txtProductID.Text.Trim());
                if (objIMEISalesInfo != null && SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                if (objIMEISalesInfo != null && objIMEISalesInfo.IsExist)
                {
                    MessageBoxObject.ShowWarningMessage(this, "IMEI đã tồn tại. Vui lòng kiểm tra lại !");
                    this.txtIMEI.Focus();
                    return false;
                }
            }
            return true;
        }

        #endregion


        #region Form Status
        /// <summary>
        /// Các trạng thái của Form
        /// </summary>
        enum FormStateType
        {
            LIST,
            ADD,
            EDIT
        }

        FormStateType intFormState = FormStateType.LIST;
        FormStateType FormState
        {
            set
            {
                intFormState = value;
                if (intFormState == FormStateType.LIST)
                {
                    //Kiểm tra quyền:
                    btnAdd.Enabled = mnuItemAdd.Enabled = bolPermission_Add;
                    btnEdit.Enabled = mnuItemEdit.Enabled = (bolPermission_UnReview || bolPermission_Review || bolPermission_Edit) && grdViewData.RowCount > 0;
                    btnReview.Enabled = mnuItemReview.Enabled = bolPermission_Review && grdViewData.RowCount > 0;
                    mnuItemViewComment.Enabled = bolPermission_CommentView && grdViewData.RowCount > 0;
                    btnUndo.Enabled = false;
                    btnUpdate.Enabled = false;
                    btnExportExcel.Enabled = mnuItemExport.Enabled = bolPermission_ExportExcel && grdViewData.RowCount > 0;
                    tabControl.SelectedTab = tabList;
                    grdViewData.ClearColumnsFilter();
                    strIMEI = string.Empty;
                }
                else if (intFormState == FormStateType.ADD || intFormState == FormStateType.EDIT)
                {
                    btnAdd.Enabled = mnuItemAdd.Enabled = false;
                    btnEdit.Enabled = mnuItemEdit.Enabled = false;
                    btnReview.Enabled = mnuItemReview.Enabled = false;
                    //btnDelete.Enabled = false;
                    btnExportExcel.Enabled = mnuItemExport.Enabled= false;
                    btnUndo.Enabled = true;
                    btnUpdate.Enabled = true;
                    tabControl.SelectedTab = tabDetail;
                    //EnableControl(!chkIsReviewed.Checked && !chkIsSystem.Checked);
                    if (intFormState == FormStateType.ADD)
                    {
                        txtIMEI.ReadOnly = false;
                    }
                    else
                    {
                        txtIMEI.ReadOnly = true;
                    }
                }
            }
            get
            {
                return intFormState;
            }
        }

        #endregion  

        private void mnuItemReview_Click(object sender, EventArgs e)
        {
            grdData.RefreshDataSource();
            DataTable dtbResource = (DataTable)grdData.DataSource;
            dtbResource.AcceptChanges();
            DataRow[] rows = dtbResource.Select("ISSELECT = 1");
            if (rows == null || rows.Length == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn thông tin cần duyệt!");
                return;
            }
            if (!Review())
            {
                return;
            }
            cmdSearch_Click(null, null);
        }

        private bool Review()
        {
            bool bolIsResult = false;
            List<ERP.Inventory.PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo> lstIMEISalesInfo = new List<PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo>();
            foreach (DataRow row in ((DataTable)grdData.DataSource).Rows)
            {
                if (Convert.ToBoolean(row["IsSelect"]))
                {
                    ERP.Inventory.PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo objIMEISalesInfo = new PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo();
                    objIMEISalesInfo.ProductID = Convert.ToString(row["ProductID"]);
                    objIMEISalesInfo.IMEI = Convert.ToString(row["IMEI"]);
                    objIMEISalesInfo.ReviewedUser = SystemConfig.objSessionUser.UserName;
                    lstIMEISalesInfo.Add(objIMEISalesInfo);
                }              
            }
            bolIsResult = this.objPLCIMEISalesInfo.Review(lstIMEISalesInfo.ToArray());
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
            else
            {
                MessageBoxObject.ShowInfoMessage(this, "Duyệt thông tin bán hàng của IMEI thành công !");
            }
            return bolIsResult;
        }

        private void chkIsSelect_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit chkEdit = (DevExpress.XtraEditors.CheckEdit)sender;
            grdViewData.SetFocusedRowCellValue("ISSELECT", Convert.ToDecimal(chkEdit.Checked));
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            mnuItemAdd_Click(null, null);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            mnuItemEdit_Click(null, null);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            mnuItemDelete_Click(null, null);
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            mnuItemExport_Click(null, null);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
                return;
            if (!UpdateData())
                return;
            LoadData();
            FormState = FormStateType.LIST;
        }

        private void btnReview_Click(object sender, EventArgs e)
        {
            mnuItemReview_Click(null, null);
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            FormState = FormStateType.LIST;
        }

        private void btnAddNewImage_Click(object sender, EventArgs e)
        {
            if (txtProductID.Text.Trim() == string.Empty || txtIMEI.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this, "Vui lòng tạo mã sản phẩm và IMEI trước khi thêm hình ảnh!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            frmProductImage frm = new frmProductImage();
            frm.ProductID = txtProductID.Text.Trim();
            frm.ShowDialog();
            if (frm.Product_Images != null && !string.IsNullOrEmpty(frm.Product_Images.ImageName))
            {
                frm.Product_Images.OrderIndex = objProduct_ImagesList.Count + 1;
                if (frm.Product_Images.IsDefault)
                {
                    foreach (PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_Images img in objProduct_ImagesList.Where(img => img.IsDefault = true))
                    {
                        img.IsDefault = false;
                    }
                }
                frm.Product_Images.CreatedUser = SystemConfig.objSessionUser.UserName;
                frm.Product_Images.ProductID = txtProductID.Text.Trim();
                frm.Product_Images.IMEI = txtIMEI.Text.Trim();
                objProduct_ImagesList.Add(frm.Product_Images);
            }
            grdImages.RefreshDataSource();
        }

        private void btnEditImage_Click(object sender, EventArgs e)
        {
            if (grvImages.FocusedRowHandle < 0)
                return;
            IMEISalesInfo_Images obj = (IMEISalesInfo_Images)grvImages.GetRow(grvImages.FocusedRowHandle);
            frmProductImage frm = new frmProductImage();
            frm.IsReview = chkIsReviewed.Checked;
            frm.Product_Images = obj;
            frm.ProductID = txtProductID.Text.Trim();
            frm.ShowDialog();
            if (frm.Product_Images.IsDefault)
            {
                foreach (IMEISalesInfo_Images img in objProduct_ImagesList.Where(img => img.IsDefault = true))
                {
                    img.IsDefault = false;
                }
                frm.Product_Images.IsDefault = true;
            }
            frm.Product_Images.UpdatedUser = SystemConfig.objSessionUser.UserName;
            grdImages.RefreshDataSource();
        }

        private void btnDelImage_Click(object sender, EventArgs e)
        {
            if (grvImages.FocusedRowHandle < 0)
                return;
            IMEISalesInfo_Images obj = (IMEISalesInfo_Images)grvImages.GetRow(grvImages.FocusedRowHandle);
            if (obj.IsSystem)
            {
                MessageBox.Show(this, "Không được xóa hình sản phẩm có thuộc tính hệ thống?", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Question);
                return;
            }

            if (MessageBox.Show(this, "Bạn chắc chắn muốn xóa hình ảnh sản phẩm này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            if (obj != null)
            {
                for (int i = obj.OrderIndex; i < objProduct_ImagesList.Count; i++)
                {
                    objProduct_ImagesList[i].OrderIndex = i;
                }
                objProduct_ImagesList.Remove(obj);

            }
            if (obj.ImageID != string.Empty)
                objProduct_ImagesList_Del.Add(obj);
            grdImages.RefreshDataSource();
        }

        private void btnMoveUp_Click(object sender, EventArgs e)
        {
            grvImages.ClearColumnsFilter();
            if (grvImages.FocusedRowHandle < 0 || grvImages.FocusedRowHandle == 0)
                return;
            grvImages.ClearColumnsFilter();
            var ItemOld = (IMEISalesInfo_Images)grvImages.GetRow(grvImages.FocusedRowHandle);
            var ItemNew = objProduct_ImagesList[grvImages.FocusedRowHandle - 1];
            int intCurrentIndex = grvImages.FocusedRowHandle;
            objProduct_ImagesList.RemoveAt(intCurrentIndex);
            objProduct_ImagesList.RemoveAt(intCurrentIndex - 1);

            ItemNew.OrderIndex = ItemNew.OrderIndex + 1;
            ItemOld.OrderIndex = ItemOld.OrderIndex - 1;

            objProduct_ImagesList.Insert(intCurrentIndex - 1, ItemOld);
            objProduct_ImagesList.Insert(intCurrentIndex, ItemNew);
            grvImages.FocusedRowHandle = grvImages.FocusedRowHandle - 1;
            grdImages.RefreshDataSource();
        }

        private void btnMoveDown_Click(object sender, EventArgs e)
        {
            grvImages.ClearColumnsFilter();
            if (grvImages.FocusedRowHandle < 0 || grvImages.FocusedRowHandle == grvImages.RowCount - 1)
                return;
            var ItemOld = (IMEISalesInfo_Images)grvImages.GetRow(grvImages.FocusedRowHandle);
            var ItemNew = objProduct_ImagesList[grvImages.FocusedRowHandle + 1];
            int intCurrentIndex = grvImages.FocusedRowHandle;
            objProduct_ImagesList.RemoveAt(intCurrentIndex + 1);
            objProduct_ImagesList.RemoveAt(intCurrentIndex);

            ItemNew.OrderIndex = ItemNew.OrderIndex - 1;
            ItemOld.OrderIndex = ItemOld.OrderIndex + 1;

            objProduct_ImagesList.Insert(intCurrentIndex, ItemNew);
            objProduct_ImagesList.Insert(intCurrentIndex + 1, ItemOld);
            grvImages.FocusedRowHandle = grvImages.FocusedRowHandle + 1;
            grdImages.RefreshDataSource();
        }

        private void mnuAddImage_Click(object sender, EventArgs e)
        {
            if (btnAddNewImage.Enabled)
                btnAddNewImage_Click(null, null);
        }

        private void mnuEditImage_Click(object sender, EventArgs e)
        {
            if (btnEditImage.Enabled)
                btnEditImage_Click(null, null);
        }

        private void mnuDelImage_Click(object sender, EventArgs e)
        {
            if (btnDelImage.Enabled)
                btnDelImage_Click(null, null);
        }

        private void chkIsReviewed_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsReviewed.Checked)
            {
                chkIsComfirm.Checked = true;
            }
            if (intFormState == FormStateType.ADD) return;
            EnableReview(bolPermission_Edit && !chkIsReviewed.Checked); 
        }

        private void btnSelectProduct_Click(object sender, EventArgs e)
        {
            try
            {
                ERP.MasterData.DUI.Search.frmProductSearch frm = ERP.MasterData.DUI.MasterData_Globals.frmProductSearch;
                frm.ShowDialog();
                if (frm.Product != null)
                {
                    txtProductID.Text = frm.Product.ProductID.Trim();
                    txtProductName.Text = frm.Product.ProductName.Trim();
                    intMainGroupID = frm.Product.MainGroupID;
                    SearchData_IMEISalesInfo_ProSpec_ToList();
                }
             
            }
            catch { }
        }

        private void chkIsBrandNewWarranty_CheckedChanged(object sender, EventArgs e)
        {
            dteEndWarantyDate.Enabled = chkIsBrandNewWarranty.Checked;
        }

        private string strIMEI = string.Empty;
        private bool bolIsShowWarning = false;
        private void txtIMEI_Leave(object sender, EventArgs e)
        {
            if (FormState != FormStateType.ADD || txtIMEI.Text.Trim() == string.Empty)
            {
                return;
            }
            DataTable dtbIMEI = objPLCIMEISalesInfo.CheckIMEI(txtIMEI.Text.Trim());
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            if (dtbIMEI != null)
            {
                if (dtbIMEI.Rows.Count == 0)
                {   
                    MessageBoxObject.ShowWarningMessage(this, "IMEI không tồn tại!");
                    txtIMEI.Text = string.Empty;
                    txtProductID.Text = string.Empty;
                    txtProductName.Text = string.Empty;
                    grdProduct.DataSource = new List<IMEISalesInfo_ProSpec>();
                    //tabCtrlDetail.SelectedTab = tabDetailInfo;
                    txtIMEI.Focus();
                    bolIsShowWarning = true;
                }
                else if (Convert.ToBoolean(dtbIMEI.Rows[0]["ISNEW"]))
                {
                    MessageBoxObject.ShowWarningMessage(this, "IMEI là mới nên không thể thêm vào!");
                    txtIMEI.Text = string.Empty;
                    txtProductID.Text = string.Empty;
                    txtProductName.Text = string.Empty;
                    grdProduct.DataSource = new List<IMEISalesInfo_ProSpec>();
                    //tabCtrlDetail.SelectedTab = tabDetailInfo;
                    txtIMEI.Focus();
                    bolIsShowWarning = true;
                }
                else if (Convert.ToBoolean(dtbIMEI.Rows[0]["ISEXISTS"]))
                {
                    MessageBoxObject.ShowWarningMessage(this, "IMEI đã thuộc thông tin bán hàng khác!");
                    txtIMEI.Text = string.Empty;
                    txtProductID.Text = string.Empty;
                    txtProductName.Text = string.Empty;
                    grdProduct.DataSource = new List<IMEISalesInfo_ProSpec>();
                    //tabCtrlDetail.SelectedTab = tabDetailInfo;
                    txtIMEI.Focus();
                    bolIsShowWarning = true;
                }
                else
                {
                    txtProductID.Text = dtbIMEI.Rows[0]["ProductID"].ToString();
                    txtProductName.Text = dtbIMEI.Rows[0]["ProductName"].ToString();
                    SearchData_IMEISalesInfo_ProSpec_ToList();
                    strIMEI = txtIMEI.Text.Trim();
                    bolIsShowWarning = false;
                }
                
            }     
        }

        private void txtIMEI_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtIMEI_Leave(null, null);
            }
        }

        private void tabList_Enter(object sender, EventArgs e)
        {
            if (FormState == FormStateType.ADD || FormState == FormStateType.EDIT)
            {
                tabControl.SelectedTab = tabDetail;
            }
        }

        private void tabDetail_Enter(object sender, EventArgs e)
        {
            if (FormState == FormStateType.LIST)
            {
                tabControl.SelectedTab = tabList;
            }
        }

        private void grdImages_DoubleClick(object sender, EventArgs e)
        {
            btnEditImage_Click(null, null);
        }

        private void txtIMEISearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && cmdSearch.Enabled)
            {
                cmdSearch_Click(null, null);
            }
        }

        private void tabCtrlDetail_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (bolIsShowWarning)
            {
                e.Cancel = true;
                bolIsShowWarning = false;
            }
        }

        private void grvProductSpec_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvProductSpec.FocusedRowHandle < 0 || grvProductSpec.RowCount == 0)
            {
                return;
            }
            IMEISalesInfo_ProSpec objLotIMEISalesInfo_ProSpec = (IMEISalesInfo_ProSpec)grvProductSpec.GetFocusedRow();
            grvProductSpec.Columns["ProductSpecStatusID"].OptionsColumn.AllowEdit = !chkIsReviewed.Checked && objLotIMEISalesInfo_ProSpec.IsUsedBySalesInfo;

        }


        private void grvProductSpec_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (grvProductSpec.FocusedRowHandle < 0)
            {
                return;
            }
            if (grvProductSpec.FocusedColumn.FieldName == "ProductSpecStatusID" || grvProductSpec.FocusedColumn.FieldName == "Note")
            {
                IMEISalesInfo_ProSpec objLotIMEISalesInfo_ProSpec = (IMEISalesInfo_ProSpec)grvProductSpec.GetFocusedRow();
                MasterData.PLC.MD.WSProductSpec_SpecStatus.ProductSpec_SpecStatus objProductSpecStatusTemp = lstProductSpecStatus.Find(p => (p.ProductSpecID == objLotIMEISalesInfo_ProSpec.ProductSpecID && p.ProductSpecStatusID == objLotIMEISalesInfo_ProSpec.ProductSpecStatusID));
                if (objProductSpecStatusTemp != null)
                {
                    objLotIMEISalesInfo_ProSpec.IsCanPrint = objProductSpecStatusTemp.IsCanPrint;
                    grdProduct.RefreshDataSource();
                   
                }
                List<IMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpec = (List<IMEISalesInfo_ProSpec>)grdProduct.DataSource;
                txtWebContent.Text = GetWebProductSpecContent(lstIMEISalesInfo_ProSpec);
                lstIMEISalesInfo_ProSpec = lstIMEISalesInfo_ProSpec.FindAll(p => p.IsCanPrint == true);
                txtPrintVATContent.Text = GetWebProductSpecContent(lstIMEISalesInfo_ProSpec);
              
            }
        }

        private void mnuItemViewComment_Click(object sender, EventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0)
            {
                return;
            }
            DataTable dtbComment = null;
            if (!LoadDataComment(ref dtbComment))
            {
                return;
            }
            if (dtbComment == null || dtbComment.Rows.Count == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "IMEI chưa có thông tin bình luận");
                return;
            }
            frmIMEISaleInfo_Comment frm = new frmIMEISaleInfo_Comment();
            frm.CommentTable = dtbComment;
            frm.IMEI = Convert.ToString(grdViewData.GetFocusedRowCellValue("IMEI"));
            frm.ProductID = Convert.ToString(grdViewData.GetFocusedRowCellValue("ProductID"));
            frm.ShowDialog();
        }
    }
}
