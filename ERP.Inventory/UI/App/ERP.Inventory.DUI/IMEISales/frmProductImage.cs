﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using ERP.Inventory.PLC.IMEISales;
using ERP.Inventory.PLC.IMEISales.WSIMEISalesInfo;
using ERP.Inventory.DUI.IMEISales;

namespace ERP.Inventory.DUI.IMEISale
{
    public partial class frmProductImage : Form
    {
        public frmProductImage()
        {
            InitializeComponent();
        }
        private bool bolIsReview = false;

        public bool IsReview
        {
            get { return bolIsReview; }
            set { bolIsReview = value; }
        }
        private string strProductID = string.Empty;
        public string ProductID
        {
            get { return strProductID; }
            set { strProductID = value; }
        }
        private PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_Images objProduct_Images = null;
        private PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_Images objProduct_Images_Undo = null;
        public PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_Images Product_Images
        {
            get { return objProduct_Images; }
            set { objProduct_Images = value; }
        }
        private string strURLLocal_Large = string.Empty;
        private string strURLLocal_Medium = string.Empty;
        private string strURLLocal_Small = string.Empty;

        private bool CheckInputImageInfo()
        {
            if (txtImageName.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this, "Vui lòng nhập tên hình ảnh.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtImageName.Focus();
                return false;
            }

            if (strURLLocal_Large == string.Empty)
            {
                MessageBox.Show(this, "Vui lòng chọn hình ảnh.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnPictureBrowse.Focus();
                return false;
            }
            return true;
        }

        private void ClearImageInfo()
        {
            txtImageDescription.Text = string.Empty;
            txtImageName.Text = string.Empty;
            strURLLocal_Large = string.Empty;
            strURLLocal_Medium = string.Empty;
            strURLLocal_Small = string.Empty;
            chkImageIsActive.Checked = true;
            chkImageSystem.Checked = false;
            chkIsDefaultImage.Checked = false;
            lblImageID.Text = string.Empty;
            //picIMG.Image = Properties.Resources.no_photos_icon;
        }

        private void btnPictureBrowse_Click(object sender, EventArgs e)
        {
            btnLargeView.Enabled = btnMediumView.Enabled = btnSmall.Enabled = false;
            BrowsePicture();
            objProduct_Images.IsNew = true;
        }

        private void btnUpdateImages_Click(object sender, EventArgs e)
        {
            if (!CheckInputImageInfo())
                return;
            objProduct_Images.ImageID = lblImageID.Text;
            objProduct_Images.ImageName = txtImageName.Text;
            objProduct_Images.Description = txtImageDescription.Text;
            objProduct_Images.LargeSizeImage = strURLLocal_Large;
            objProduct_Images.MediumSizeImage = strURLLocal_Medium;
            objProduct_Images.SmallSizeImage = strURLLocal_Small;
            if (objProduct_Images.IsNew)
            {
                objProduct_Images.LargesizeImage_Local = strURLLocal_Large;
                objProduct_Images.MediumSizeImage_Local = strURLLocal_Medium;
                objProduct_Images.SmallSizeImage_Local = strURLLocal_Small;
            }
            objProduct_Images.IsDefault = chkIsDefaultImage.Checked;
            objProduct_Images.IsSystem = chkImageSystem.Checked;
            objProduct_Images.IsActive = chkImageIsActive.Checked;
            this.Close();
        }

        private void BrowsePicture()
        {
            try
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Filter = "Images (*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG";
                if (dlg.ShowDialog(this) == DialogResult.OK)
                {
                    if (!File.Exists(dlg.FileName))
                    {
                        MessageBox.Show(this, "File không tồn tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else
                    {
                        FileInfo objFileInfo = new FileInfo(dlg.FileName);
                        decimal FileSize = Math.Round(objFileInfo.Length / Convert.ToDecimal((1024)), 2); //dung lượng tính theo kb
                        if (FileSize > 5000)
                        {
                            MessageBox.Show(this, "Vui lòng chọn tập tin đính kèm dung lượng không vượt quá 5MB", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        string strFileName = dlg.SafeFileName;
                        if (!Directory.Exists(@"ProductImages\"+ProductID))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(@"ProductImages\" + ProductID);
                        }
                        string strLargeSize = AppConfig.GetStringConfigValue("MD_PRODUCT_IMAGE_LARGESIZE");
                        string strMediumSize = AppConfig.GetStringConfigValue("MD_PRODUCT_IMAGE_MEDIUMSIZE");
                        string strSmallSize = AppConfig.GetStringConfigValue("MD_PRODUCT_IMAGE_SMALLSIZE");
                        if (strLargeSize == string.Empty || strMediumSize == string.Empty || strSmallSize == string.Empty)
                        {
                            MessageBox.Show(this, "Cấu hình kích thước hình sản phẩm chưa được khai báo!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }

                        int intLargeWidth = 0;
                        int intLargeHeight = 0;
                        int intMediumWidth = 0;
                        int intMediumHeight = 0;
                        int intSmallWidth = 0;
                        int intSmallHeight = 0;
                        try
                        {
                            intLargeWidth = Convert.ToInt32(strLargeSize.Split('x')[0].ToString());
                            intLargeHeight = Convert.ToInt32(strLargeSize.Split('x')[1].ToString());

                            intMediumWidth = Convert.ToInt32(strMediumSize.Split('x')[0].ToString());
                            intMediumHeight = Convert.ToInt32(strMediumSize.Split('x')[1].ToString());

                            intSmallWidth = Convert.ToInt32(strSmallSize.Split('x')[0].ToString());
                            intSmallHeight = Convert.ToInt32(strSmallSize.Split('x')[1].ToString());
                        }
                        catch
                        {
                            MessageBox.Show(this, "Cấu hình kích thước hình sản phẩm chưa đúng định dạng 'Width x Height'!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        String strGuid = Guid.NewGuid().ToString();
                        strURLLocal_Large = ERP.MasterData.DUI.Common.ImageHandler.Save(dlg.FileName, intLargeWidth, intLargeHeight, 75, @"ProductImages\" + ProductID + @"\Large" + strGuid.Trim()+ strFileName);
                        strURLLocal_Medium = ERP.MasterData.DUI.Common.ImageHandler.Save(dlg.FileName, intMediumWidth, intMediumHeight, 75, @"ProductImages\" + ProductID + @"\Medium" + strGuid.Trim() + strFileName);
                        strURLLocal_Small = ERP.MasterData.DUI.Common.ImageHandler.Save(dlg.FileName, intSmallWidth, intSmallHeight, 75, @"ProductImages\" + ProductID + @"\Small" + strGuid.Trim() + strFileName);
                        using (var fs = new System.IO.FileStream(strURLLocal_Medium, System.IO.FileMode.Open))
                        {
                            var bmp = new Bitmap(fs);
                            picIMG.Image = (Bitmap)bmp.Clone();
                        }
                        if (strURLLocal_Large.Trim()!= string.Empty)
                        {
                            btnLargeView.Enabled = btnMediumView.Enabled = btnSmall.Enabled = true;
                        }
                        
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Load image tu Server len pictureBox
        /// </summary>
        /// <param name="strURL"></param>
        /// <param name="intSize">1:large,2:medium,3:small</param>
        /// <returns></returns>
        private string LoadImage(string strFileID, string strURL, int intSize)
        {
            string strFileName = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(strURL))
                {
                    if (!Directory.Exists(@"ProductImages\"+ProductID))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(@"ProductImages\"+ProductID);
                    }
                    if (intSize == 1)
                        strFileName = "ProductImages\\" + ProductID + "\\Large"+ strFileName;
                    else
                        if (intSize == 2)
                            strFileName = "ProductImages\\" + ProductID + "\\Medium" + strFileName;
                        else
                            strFileName = "ProductImages\\" + ProductID + "\\Small" + strFileName;
                    if (!System.IO.File.Exists(strURL))
                    {
                        //ERP.FMS.DUI.DUIFileManager.DownloadFile(this, strFileID, strFileName);
                        ERP.FMS.DUI.DUIFileManager.DownloadFile(this, "FMSAplication_ProERP_ProductImage", strFileID, string.Empty, strFileName);
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        }
                    }
                    else
                        strFileName = strURL;
                }
            }
            catch
            {
                MessageBox.Show(this, "Không thể tải hình lên được", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return strFileName;
        }


        private void btnLargeView_Click(object sender, EventArgs e)
        {
            string strURL = string.Empty;
            if (objProduct_Images == null || objProduct_Images.IsNew == true)
            {
                strURL =Path.GetFullPath(strURLLocal_Large);
            }
            else
            {
                strURL = LoadImage(objProduct_Images.LargeSizeImageFileID, objProduct_Images.LargeSizeImage, 1);
            }

            if (System.IO.File.Exists(strURL))
            {
                frmImageView frm = new frmImageView();
                frm.strURLImage = strURL;
                frm.strCaption = "Hình lớn";
                frm.ShowDialog();
            }
            objProduct_Images.LargesizeImage_Local = strURL;
        }

        private void btnMediumView_Click(object sender, EventArgs e)
        {
            string strURL = string.Empty;
            if (objProduct_Images == null || objProduct_Images.IsNew == true)
            {
                strURL = Path.GetFullPath(strURLLocal_Medium);
            }
            else
            {
                strURL = LoadImage(objProduct_Images.MediumSizeImageFileID,objProduct_Images.MediumSizeImage, 2);
            }

            if (System.IO.File.Exists(strURL))
            {
                frmImageView frm = new frmImageView();
                frm.strURLImage = strURL;
                frm.strCaption = "Hình vừa";
                frm.ShowDialog();
            }
            objProduct_Images.MediumSizeImage_Local = strURL;
        }

        private void btnSmall_Click(object sender, EventArgs e)
        {
            string strURL = string.Empty;
            if (objProduct_Images == null || objProduct_Images.IsNew==true)
            {
                strURL = Path.GetFullPath(strURLLocal_Small);
            }
            else
            {
                strURL = LoadImage(objProduct_Images.SmallSizeImageFileID, objProduct_Images.SmallSizeImage, 3);
                
            }
            if (System.IO.File.Exists(strURL))
            {
                frmImageView frm = new frmImageView();
                frm.strURLImage = strURL;
                frm.strCaption = "Hình nhỏ";
                frm.ShowDialog();
            }
            objProduct_Images.SmallSizeImage_Local = strURL;
        }

        private void frmProductImage_Load(object sender, EventArgs e)
        {
                //if (SystemConfig.objSessionUser.UserName != "administrator")
                //{
                //    chkImageSystem.Enabled = false;
                //}
            chkImageSystem.Enabled = SystemConfig.objSessionUser.UserName == "administrator" && !bolIsReview;
            LoadEdit();
        }

        private void LoadEdit()
        {
            if (objProduct_Images != null)
            {
                objProduct_Images_Undo = new PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_Images();
                lblImageID.Text = objProduct_Images.ImageID;
                
                txtImageName.Text = objProduct_Images.ImageName;
                txtImageDescription.Text = objProduct_Images.Description;
                chkImageIsActive.Checked = objProduct_Images.IsActive;
                chkImageSystem.Checked = objProduct_Images.IsSystem;
                chkIsDefaultImage.Checked = objProduct_Images.IsDefault;
                strURLLocal_Large = objProduct_Images.LargeSizeImage;
                strURLLocal_Medium = objProduct_Images.MediumSizeImage;
                strURLLocal_Small = objProduct_Images.SmallSizeImage;
                string strImageView = LoadImage(objProduct_Images.MediumSizeImageFileID,objProduct_Images.MediumSizeImage, 2);
                if (!string.IsNullOrEmpty(strImageView)&&File.Exists(strImageView))
                {
                    using (var fs = new System.IO.FileStream(strImageView, System.IO.FileMode.Open))
                    {
                        var bmp = new Bitmap(fs);
                        picIMG.Image = (Bitmap)bmp.Clone();
                    }
                }
                objProduct_Images_Undo.ImageID=lblImageID.Text;
                objProduct_Images_Undo.ImageName = txtImageName.Text;
                objProduct_Images_Undo.Description=txtImageDescription.Text;
                objProduct_Images_Undo.IsActive=chkImageIsActive.Checked;
                objProduct_Images_Undo.IsSystem=chkImageSystem.Checked;
                objProduct_Images_Undo.IsDefault=chkIsDefaultImage.Checked;
                objProduct_Images_Undo.LargeSizeImage = objProduct_Images.LargeSizeImage;
                objProduct_Images_Undo.MediumSizeImage = objProduct_Images.MediumSizeImage;
                objProduct_Images_Undo.SmallSizeImage = objProduct_Images.SmallSizeImage; 
            }
            else
            {
                objProduct_Images = new PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_Images();
                objProduct_Images.IsNew = true;
            }
            btnLargeView.Enabled = btnMediumView.Enabled = btnSmall.Enabled = false;
            if (strURLLocal_Large.Trim() != string.Empty)
            {
                btnLargeView.Enabled = btnMediumView.Enabled = btnSmall.Enabled = true;
            }
            chkImageSystem_CheckedChanged(null, null);
        }

        /// <summary>
        /// Ẩn hiện control
        /// </summary>
        /// <param name="bolValue">true: hiện; false: ẩn</param>
        private void EnableControl(bool bolValue)
        {
            txtImageName.ReadOnly = !bolValue;
            txtImageDescription.ReadOnly = !bolValue;
            chkImageIsActive.Enabled = bolValue;
            chkIsDefaultImage.Enabled = bolValue;
            btnPictureBrowse.Enabled = bolValue;
            btnUpdateImages.Enabled = bolValue;
        }

        private void chkImageSystem_CheckedChanged(object sender, EventArgs e)
        {
            EnableControl(!chkImageSystem.Checked && !bolIsReview);
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            if (objProduct_Images_Undo != null)
                objProduct_Images = objProduct_Images_Undo;
            this.Close();
        }
    }
}
