﻿namespace ERP.Inventory.DUI.IMEISales
{
    partial class frmIMEISalesInfo_OrderIndexEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIMEISalesInfo_OrderIndexEdit));
            this.cmdSearch = new System.Windows.Forms.Button();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkActiveItem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkSystemItem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.cboProductList = new ERP.MasterData.DUI.CustomControl.Product.cboProductList();
            this.txtIMEISearch = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActiveItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSystemItem)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdSearch
            // 
            this.cmdSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSearch.Image = ((System.Drawing.Image)(resources.GetObject("cmdSearch.Image")));
            this.cmdSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSearch.Location = new System.Drawing.Point(824, 19);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(94, 25);
            this.cmdSearch.TabIndex = 7;
            this.cmdSearch.Text = "Tìm kiếm";
            this.cmdSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdSearch.UseVisualStyleBackColor = true;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // grdData
            // 
            this.grdData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdData.Location = new System.Drawing.Point(1, 61);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chkIsSelect,
            this.chkActiveItem,
            this.chkSystemItem});
            this.grdData.Size = new System.Drawing.Size(1025, 499);
            this.grdData.TabIndex = 8;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Preview.Options.UseFont = true;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewData.OptionsView.ShowFooter = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            // 
            // chkActiveItem
            // 
            this.chkActiveItem.AutoHeight = false;
            this.chkActiveItem.Name = "chkActiveItem";
            this.chkActiveItem.ValueChecked = ((short)(1));
            this.chkActiveItem.ValueUnchecked = ((short)(0));
            // 
            // chkSystemItem
            // 
            this.chkSystemItem.AutoHeight = false;
            this.chkSystemItem.Name = "chkSystemItem";
            this.chkSystemItem.ValueChecked = ((short)(1));
            this.chkSystemItem.ValueUnchecked = ((short)(0));
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(924, 19);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(95, 25);
            this.btnUpdate.TabIndex = 9;
            this.btnUpdate.Text = "   Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.button1_Click);
            // 
            // cboProductList
            // 
            this.cboProductList.BackColor = System.Drawing.Color.Transparent;
            this.cboProductList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductList.Location = new System.Drawing.Point(454, 20);
            this.cboProductList.Margin = new System.Windows.Forms.Padding(4);
            this.cboProductList.Name = "cboProductList";
            this.cboProductList.ProductIDList = "";
            this.cboProductList.Size = new System.Drawing.Size(250, 23);
            this.cboProductList.TabIndex = 22;
            this.cboProductList.Visible = false;
            // 
            // txtIMEISearch
            // 
            this.txtIMEISearch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIMEISearch.Location = new System.Drawing.Point(119, 21);
            this.txtIMEISearch.MaxLength = 50;
            this.txtIMEISearch.Name = "txtIMEISearch";
            this.txtIMEISearch.Size = new System.Drawing.Size(238, 22);
            this.txtIMEISearch.TabIndex = 24;
            this.txtIMEISearch.Visible = false;
            this.txtIMEISearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtIMEISearch_KeyUp);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 16);
            this.label8.TabIndex = 23;
            this.label8.Text = "Mã IMEI/ Lô IMEI:";
            this.label8.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(375, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 25;
            this.label4.Text = "Sản phẩm:";
            this.label4.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cmdSearch);
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Controls.Add(this.cboProductList);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtIMEISearch);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(1, -4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1025, 59);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            // 
            // frmIMEISalesInfo_OrderIndexEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 562);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grdData);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmIMEISalesInfo_OrderIndexEdit";
            this.ShowIcon = false;
            this.Text = "Cập nhật thứ tự thông tin bán hàng của IMEI/ Lô IMEI";
            this.Load += new System.EventHandler(this.frmIMEISalesInfoManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActiveItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSystemItem)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        //private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repChkIsReviewed;
        private System.Windows.Forms.Button cmdSearch;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkActiveItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkSystemItem;
        private System.Windows.Forms.Button btnUpdate;
        private MasterData.DUI.CustomControl.Product.cboProductList cboProductList;
        private System.Windows.Forms.TextBox txtIMEISearch;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;

    }
}