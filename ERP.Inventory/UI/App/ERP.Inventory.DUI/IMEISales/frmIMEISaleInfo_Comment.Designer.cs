﻿namespace ERP.Inventory.DUI.IMEISales
{
    partial class frmIMEISaleInfo_Comment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIMEISaleInfo_Comment));
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemExport = new System.Windows.Forms.ToolStripMenuItem();
            this.treeComment = new DevExpress.XtraTreeList.TreeList();
            this.colTreeDepartment_IsSelect = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repTreeDepartment_IsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTreeCommentID = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTreeDepartment_ReplyCommentID = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTreeDepartment_CommentDate = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTreeDepartment_CommentTitle = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTreeDepartment_CommentContent = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTreeDepartment_CommentUser = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTreeDepartment_CommentEmail = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTreeDepartment_PhoneNumber = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTreeDepartment_ModelRate = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTreeDepartment_IsActive = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repTreeDepartment_Check = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTreeDepartment_IsSystem = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repSpinTreeDepartment = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.mnuAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTreeDepartment_IsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTreeDepartment_Check)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSpinTreeDepartment)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuAction
            // 
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemEdit,
            this.mnuItemDelete,
            this.mnuItemExport});
            this.mnuAction.Name = "contextMenuStrip1";
            this.mnuAction.Size = new System.Drawing.Size(168, 92);
            // 
            // mnuItemEdit
            // 
            this.mnuItemEdit.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemEdit.Image")));
            this.mnuItemEdit.Name = "mnuItemEdit";
            this.mnuItemEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.mnuItemEdit.Size = new System.Drawing.Size(167, 22);
            this.mnuItemEdit.Text = "Chỉnh sửa";
            this.mnuItemEdit.Click += new System.EventHandler(this.mnuItemEdit_Click);
            // 
            // mnuItemDelete
            // 
            this.mnuItemDelete.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemDelete.Image")));
            this.mnuItemDelete.Name = "mnuItemDelete";
            this.mnuItemDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.mnuItemDelete.Size = new System.Drawing.Size(167, 22);
            this.mnuItemDelete.Text = "Xóa";
            this.mnuItemDelete.Click += new System.EventHandler(this.mnuItemDelete_Click);
            // 
            // mnuItemExport
            // 
            this.mnuItemExport.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemExport.Image")));
            this.mnuItemExport.Name = "mnuItemExport";
            this.mnuItemExport.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.mnuItemExport.Size = new System.Drawing.Size(167, 22);
            this.mnuItemExport.Text = "Xuất excel";
            this.mnuItemExport.Click += new System.EventHandler(this.mnuItemExport_Click);
            // 
            // treeComment
            // 
            this.treeComment.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.treeComment.Appearance.HeaderPanel.Options.UseFont = true;
            this.treeComment.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.treeComment.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.treeComment.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.treeComment.ColumnPanelRowHeight = 40;
            this.treeComment.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colTreeDepartment_IsSelect,
            this.colTreeCommentID,
            this.colTreeDepartment_ReplyCommentID,
            this.colTreeDepartment_CommentDate,
            this.colTreeDepartment_CommentTitle,
            this.colTreeDepartment_CommentContent,
            this.colTreeDepartment_CommentUser,
            this.colTreeDepartment_CommentEmail,
            this.colTreeDepartment_PhoneNumber,
            this.colTreeDepartment_ModelRate,
            this.colTreeDepartment_IsActive,
            this.colTreeDepartment_IsSystem});
            this.treeComment.ContextMenuStrip = this.mnuAction;
            this.treeComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeComment.KeyFieldName = "COMMENTID";
            this.treeComment.Location = new System.Drawing.Point(0, 0);
            this.treeComment.Name = "treeComment";
            this.treeComment.OptionsBehavior.PopulateServiceColumns = true;
            this.treeComment.OptionsView.AutoWidth = false;
            this.treeComment.ParentFieldName = "REPLYTOCOMMENTID";
            this.treeComment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repTreeDepartment_Check,
            this.repTreeDepartment_IsSelect,
            this.repSpinTreeDepartment});
            this.treeComment.Size = new System.Drawing.Size(954, 479);
            this.treeComment.TabIndex = 5;
            this.treeComment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeComment_KeyDown);
            // 
            // colTreeDepartment_IsSelect
            // 
            this.colTreeDepartment_IsSelect.AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_IsSelect.AppearanceCell.Options.UseFont = true;
            this.colTreeDepartment_IsSelect.AppearanceCell.Options.UseTextOptions = true;
            this.colTreeDepartment_IsSelect.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTreeDepartment_IsSelect.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_IsSelect.AppearanceHeader.Options.UseFont = true;
            this.colTreeDepartment_IsSelect.Caption = "Chọn";
            this.colTreeDepartment_IsSelect.ColumnEdit = this.repTreeDepartment_IsSelect;
            this.colTreeDepartment_IsSelect.FieldName = "ISSELECT";
            this.colTreeDepartment_IsSelect.Fixed = DevExpress.XtraTreeList.Columns.FixedStyle.Left;
            this.colTreeDepartment_IsSelect.Name = "colTreeDepartment_IsSelect";
            this.colTreeDepartment_IsSelect.OptionsColumn.AllowSort = false;
            this.colTreeDepartment_IsSelect.Visible = true;
            this.colTreeDepartment_IsSelect.VisibleIndex = 0;
            this.colTreeDepartment_IsSelect.Width = 48;
            // 
            // repTreeDepartment_IsSelect
            // 
            this.repTreeDepartment_IsSelect.AutoHeight = false;
            this.repTreeDepartment_IsSelect.Name = "repTreeDepartment_IsSelect";
            this.repTreeDepartment_IsSelect.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repTreeDepartment_IsSelect.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.repTreeDepartment_IsSelect.CheckedChanged += new System.EventHandler(this.repTreeDepartment_IsSelect_CheckedChanged);
            // 
            // colTreeCommentID
            // 
            this.colTreeCommentID.AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeCommentID.AppearanceCell.Options.UseFont = true;
            this.colTreeCommentID.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeCommentID.AppearanceHeader.Options.UseFont = true;
            this.colTreeCommentID.Caption = "Mã bình luận";
            this.colTreeCommentID.FieldName = "COMMENTID";
            this.colTreeCommentID.Fixed = DevExpress.XtraTreeList.Columns.FixedStyle.Left;
            this.colTreeCommentID.Name = "colTreeCommentID";
            this.colTreeCommentID.OptionsColumn.AllowEdit = false;
            this.colTreeCommentID.OptionsColumn.AllowSort = false;
            this.colTreeCommentID.Visible = true;
            this.colTreeCommentID.VisibleIndex = 1;
            this.colTreeCommentID.Width = 80;
            // 
            // colTreeDepartment_ReplyCommentID
            // 
            this.colTreeDepartment_ReplyCommentID.AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_ReplyCommentID.AppearanceCell.Options.UseFont = true;
            this.colTreeDepartment_ReplyCommentID.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_ReplyCommentID.AppearanceHeader.Options.UseFont = true;
            this.colTreeDepartment_ReplyCommentID.Caption = "Mã bình luận trả lời";
            this.colTreeDepartment_ReplyCommentID.FieldName = "REPLYTOCOMMENTID";
            this.colTreeDepartment_ReplyCommentID.Name = "colTreeDepartment_ReplyCommentID";
            this.colTreeDepartment_ReplyCommentID.OptionsColumn.AllowEdit = false;
            this.colTreeDepartment_ReplyCommentID.OptionsColumn.AllowSort = false;
            this.colTreeDepartment_ReplyCommentID.Visible = true;
            this.colTreeDepartment_ReplyCommentID.VisibleIndex = 2;
            this.colTreeDepartment_ReplyCommentID.Width = 80;
            // 
            // colTreeDepartment_CommentDate
            // 
            this.colTreeDepartment_CommentDate.AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_CommentDate.AppearanceCell.Options.UseFont = true;
            this.colTreeDepartment_CommentDate.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_CommentDate.AppearanceHeader.Options.UseFont = true;
            this.colTreeDepartment_CommentDate.Caption = "Ngày bình luận";
            this.colTreeDepartment_CommentDate.FieldName = "COMMENTDATE";
            this.colTreeDepartment_CommentDate.Format.FormatString = "dd/MM/yyyy HH:mm:ss";
            this.colTreeDepartment_CommentDate.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTreeDepartment_CommentDate.Name = "colTreeDepartment_CommentDate";
            this.colTreeDepartment_CommentDate.OptionsColumn.AllowEdit = false;
            this.colTreeDepartment_CommentDate.OptionsColumn.AllowSort = false;
            this.colTreeDepartment_CommentDate.Visible = true;
            this.colTreeDepartment_CommentDate.VisibleIndex = 3;
            this.colTreeDepartment_CommentDate.Width = 150;
            // 
            // colTreeDepartment_CommentTitle
            // 
            this.colTreeDepartment_CommentTitle.AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_CommentTitle.AppearanceCell.Options.UseFont = true;
            this.colTreeDepartment_CommentTitle.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_CommentTitle.AppearanceHeader.Options.UseFont = true;
            this.colTreeDepartment_CommentTitle.Caption = "Tiêu đề bình luận";
            this.colTreeDepartment_CommentTitle.FieldName = "COMMENTTITLE";
            this.colTreeDepartment_CommentTitle.Name = "colTreeDepartment_CommentTitle";
            this.colTreeDepartment_CommentTitle.OptionsColumn.AllowEdit = false;
            this.colTreeDepartment_CommentTitle.OptionsColumn.AllowSort = false;
            this.colTreeDepartment_CommentTitle.Visible = true;
            this.colTreeDepartment_CommentTitle.VisibleIndex = 4;
            this.colTreeDepartment_CommentTitle.Width = 150;
            // 
            // colTreeDepartment_CommentContent
            // 
            this.colTreeDepartment_CommentContent.AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_CommentContent.AppearanceCell.Options.UseFont = true;
            this.colTreeDepartment_CommentContent.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_CommentContent.AppearanceHeader.Options.UseFont = true;
            this.colTreeDepartment_CommentContent.Caption = "Nội dung bình luận";
            this.colTreeDepartment_CommentContent.FieldName = "COMMENTCONTENT";
            this.colTreeDepartment_CommentContent.Name = "colTreeDepartment_CommentContent";
            this.colTreeDepartment_CommentContent.OptionsColumn.AllowEdit = false;
            this.colTreeDepartment_CommentContent.OptionsColumn.AllowSort = false;
            this.colTreeDepartment_CommentContent.Visible = true;
            this.colTreeDepartment_CommentContent.VisibleIndex = 5;
            this.colTreeDepartment_CommentContent.Width = 200;
            // 
            // colTreeDepartment_CommentUser
            // 
            this.colTreeDepartment_CommentUser.AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_CommentUser.AppearanceCell.Options.UseFont = true;
            this.colTreeDepartment_CommentUser.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_CommentUser.AppearanceHeader.Options.UseFont = true;
            this.colTreeDepartment_CommentUser.Caption = "Người bình luận";
            this.colTreeDepartment_CommentUser.FieldName = "COMMENTUSER";
            this.colTreeDepartment_CommentUser.Name = "colTreeDepartment_CommentUser";
            this.colTreeDepartment_CommentUser.OptionsColumn.AllowEdit = false;
            this.colTreeDepartment_CommentUser.OptionsColumn.AllowSort = false;
            this.colTreeDepartment_CommentUser.Visible = true;
            this.colTreeDepartment_CommentUser.VisibleIndex = 6;
            this.colTreeDepartment_CommentUser.Width = 220;
            // 
            // colTreeDepartment_CommentEmail
            // 
            this.colTreeDepartment_CommentEmail.AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_CommentEmail.AppearanceCell.Options.UseFont = true;
            this.colTreeDepartment_CommentEmail.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_CommentEmail.AppearanceHeader.Options.UseFont = true;
            this.colTreeDepartment_CommentEmail.Caption = "Email";
            this.colTreeDepartment_CommentEmail.FieldName = "COMMENTEMAIL";
            this.colTreeDepartment_CommentEmail.Name = "colTreeDepartment_CommentEmail";
            this.colTreeDepartment_CommentEmail.OptionsColumn.AllowEdit = false;
            this.colTreeDepartment_CommentEmail.OptionsColumn.AllowSort = false;
            this.colTreeDepartment_CommentEmail.Visible = true;
            this.colTreeDepartment_CommentEmail.VisibleIndex = 7;
            this.colTreeDepartment_CommentEmail.Width = 150;
            // 
            // colTreeDepartment_PhoneNumber
            // 
            this.colTreeDepartment_PhoneNumber.AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_PhoneNumber.AppearanceCell.Options.UseFont = true;
            this.colTreeDepartment_PhoneNumber.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_PhoneNumber.AppearanceHeader.Options.UseFont = true;
            this.colTreeDepartment_PhoneNumber.Caption = "Số điện thoại";
            this.colTreeDepartment_PhoneNumber.FieldName = "PHONENUMBER";
            this.colTreeDepartment_PhoneNumber.Name = "colTreeDepartment_PhoneNumber";
            this.colTreeDepartment_PhoneNumber.OptionsColumn.AllowEdit = false;
            this.colTreeDepartment_PhoneNumber.OptionsColumn.AllowSort = false;
            this.colTreeDepartment_PhoneNumber.Visible = true;
            this.colTreeDepartment_PhoneNumber.VisibleIndex = 8;
            this.colTreeDepartment_PhoneNumber.Width = 100;
            // 
            // colTreeDepartment_ModelRate
            // 
            this.colTreeDepartment_ModelRate.AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_ModelRate.AppearanceCell.Options.UseFont = true;
            this.colTreeDepartment_ModelRate.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_ModelRate.AppearanceHeader.Options.UseFont = true;
            this.colTreeDepartment_ModelRate.Caption = "Đánh giá";
            this.colTreeDepartment_ModelRate.FieldName = "MODELRATE";
            this.colTreeDepartment_ModelRate.Name = "colTreeDepartment_ModelRate";
            this.colTreeDepartment_ModelRate.OptionsColumn.AllowEdit = false;
            this.colTreeDepartment_ModelRate.OptionsColumn.AllowSort = false;
            this.colTreeDepartment_ModelRate.Visible = true;
            this.colTreeDepartment_ModelRate.VisibleIndex = 9;
            this.colTreeDepartment_ModelRate.Width = 50;
            // 
            // colTreeDepartment_IsActive
            // 
            this.colTreeDepartment_IsActive.AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_IsActive.AppearanceCell.Options.UseFont = true;
            this.colTreeDepartment_IsActive.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_IsActive.AppearanceHeader.Options.UseFont = true;
            this.colTreeDepartment_IsActive.Caption = "Kích hoạt";
            this.colTreeDepartment_IsActive.ColumnEdit = this.repTreeDepartment_Check;
            this.colTreeDepartment_IsActive.FieldName = "ISACTIVE";
            this.colTreeDepartment_IsActive.Name = "colTreeDepartment_IsActive";
            this.colTreeDepartment_IsActive.OptionsColumn.AllowEdit = false;
            this.colTreeDepartment_IsActive.OptionsColumn.AllowSort = false;
            this.colTreeDepartment_IsActive.Visible = true;
            this.colTreeDepartment_IsActive.VisibleIndex = 10;
            // 
            // repTreeDepartment_Check
            // 
            this.repTreeDepartment_Check.AutoHeight = false;
            this.repTreeDepartment_Check.Name = "repTreeDepartment_Check";
            this.repTreeDepartment_Check.ValueChecked = ((short)(1));
            this.repTreeDepartment_Check.ValueUnchecked = ((short)(0));
            // 
            // colTreeDepartment_IsSystem
            // 
            this.colTreeDepartment_IsSystem.AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_IsSystem.AppearanceCell.Options.UseFont = true;
            this.colTreeDepartment_IsSystem.AppearanceHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colTreeDepartment_IsSystem.AppearanceHeader.Options.UseFont = true;
            this.colTreeDepartment_IsSystem.Caption = "Hệ thống";
            this.colTreeDepartment_IsSystem.ColumnEdit = this.repTreeDepartment_Check;
            this.colTreeDepartment_IsSystem.FieldName = "ISSYSTEM";
            this.colTreeDepartment_IsSystem.Name = "colTreeDepartment_IsSystem";
            this.colTreeDepartment_IsSystem.OptionsColumn.AllowEdit = false;
            this.colTreeDepartment_IsSystem.OptionsColumn.AllowSort = false;
            this.colTreeDepartment_IsSystem.Visible = true;
            this.colTreeDepartment_IsSystem.VisibleIndex = 11;
            // 
            // repSpinTreeDepartment
            // 
            this.repSpinTreeDepartment.AutoHeight = false;
            this.repSpinTreeDepartment.DisplayFormat.FormatString = "##0.#";
            this.repSpinTreeDepartment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repSpinTreeDepartment.EditFormat.FormatString = "##0.#";
            this.repSpinTreeDepartment.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repSpinTreeDepartment.MaxLength = 5;
            this.repSpinTreeDepartment.MaxValue = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this.repSpinTreeDepartment.Name = "repSpinTreeDepartment";
            // 
            // frmIMEISaleInfo_Comment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(954, 479);
            this.Controls.Add(this.treeComment);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(970, 517);
            this.Name = "frmIMEISaleInfo_Comment";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmIMEISaleInfo_Comment";
            this.Load += new System.EventHandler(this.frmIMEISaleInfo_Comment_Load);
            this.mnuAction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTreeDepartment_IsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTreeDepartment_Check)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repSpinTreeDepartment)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuItemEdit;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDelete;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExport;
        private DevExpress.XtraTreeList.TreeList treeComment;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTreeDepartment_IsSelect;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repTreeDepartment_IsSelect;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTreeCommentID;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTreeDepartment_ReplyCommentID;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTreeDepartment_CommentDate;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTreeDepartment_CommentTitle;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTreeDepartment_CommentContent;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTreeDepartment_CommentUser;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTreeDepartment_CommentEmail;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTreeDepartment_PhoneNumber;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTreeDepartment_ModelRate;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTreeDepartment_IsActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repTreeDepartment_Check;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTreeDepartment_IsSystem;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repSpinTreeDepartment;
    }
}