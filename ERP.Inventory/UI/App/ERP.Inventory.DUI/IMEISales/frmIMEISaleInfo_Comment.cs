﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERP.Inventory.PLC.IMEISales;
using ERP.Inventory.PLC.IMEISales.WSIMEISalesInfo;
using Library.AppCore;
using Library.AppCore.LoadControls;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;

namespace ERP.Inventory.DUI.IMEISales
{
    public partial class frmIMEISaleInfo_Comment : Form
    {
        #region Variable
        private int intIDFocus = -1;
        private string strIMEI = string.Empty;
        private string strProductID = string.Empty;
        private DataTable dbtComment = null;
        private PLC.IMEISales.PLCIMEISalesInfo objPLCIMEISalesInfo = new PLC.IMEISales.PLCIMEISalesInfo();

        private string strPermission_View = "PM_IMEISALESINFO_COMMENT_VIEW";
        private string strPermission_Delete = "PM_IMEISALESINFO_COMMENT_DELETE";
        private string strPermission_Add = "PM_IMEISALESINFO_COMMENT_ADD";
        private string strPermission_Edit = "PM_IMEISALESINFO_COMMENT_EDIT";
        //private string strPermission_ExportExcel = "PM_IMEISALESINFO_COMMENT_EXPORTEXCEL";

        private bool bolIsPermission_Delete = false;
        private bool bolIsPermission_View = false;
        private bool bolIsPermission_Add = false;
        private bool bolIsPermission_Edit = false;

        public string ProductID
        {
            get { return strProductID; }
            set { strProductID = value; }
        }

        public string IMEI
        {
            set { strIMEI = value; }
        }
        public DataTable CommentTable
        {
            set { dbtComment = value; }
        }
        #endregion

        #region Constructor
        public frmIMEISaleInfo_Comment()
        {
            InitializeComponent();
        }
        #endregion

        #region Event

        private void frmIMEISaleInfo_Comment_Load(object sender, EventArgs e)
        {
            LoadPermission();
            this.Text = "Quản lý bình luận thông tin bán hàng của IMEI: " + strIMEI;
            treeComment.DataSource = dbtComment;
            treeComment.ExpandAll();
            //Library.AppCore.CustomControls.TreeList.FormatTreeList.CurrentInstance.SetClipboard(treeComment);
            mnuItemDelete.Enabled = bolIsPermission_Delete;
            mnuItemEdit.Enabled = bolIsPermission_Add || bolIsPermission_Edit;
        }

        private void mnuItemExport_Click(object sender, EventArgs e)
        {
            Library.AppCore.CustomControls.TreeList.FormatTreeList.CurrentInstance.ExcelExport(treeComment);
            //ExcelExport(treeComment);

        }

        private void mnuItemEdit_Click(object sender, EventArgs e)
        {
            if (treeComment.FocusedNode == null)
            {
                return;
            }
            EditData();

            //this.FocusRow(this.intIDFocus);
        }

        private void mnuItemDelete_Click(object sender, EventArgs e)
        {
            if (treeComment.FocusedNode == null)
            {
                return;
            }
            DataTable dtbComment = (DataTable)treeComment.DataSource;
            if (dtbComment.Select("IsSelect = 0").Length == dtbComment.Rows.Count)
            {
                MessageBoxObject.ShowWarningMessage(this, "Bạn chưa chọn thông tin cần xóa!");
                return;
            }
            if (MessageBox.Show(this, "Bạn có chắc muốn xóa thông tin bình luận đang chọn không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
            {
                return;
            }
            if (!DeleteData())
            {
                return;
            }
            MessageBoxObject.ShowInfoMessage(this, "Xóa thông tin bình luận thành công!");
            LoadDataComment();
        }

        #endregion

        #region Method

        private void LoadPermission()
        {
            bolIsPermission_View = SystemConfig.objSessionUser.IsPermission(strPermission_View);
            bolIsPermission_Edit = SystemConfig.objSessionUser.IsPermission(strPermission_Edit);
            bolIsPermission_Delete = SystemConfig.objSessionUser.IsPermission(strPermission_Delete);
            bolIsPermission_Add = SystemConfig.objSessionUser.IsPermission(strPermission_Add);
        }

        private void LoadDataComment()
        {
            object[] objKeywords = new object[] { 
                                                  "@ProductID", strProductID,
                                                  "@IMEI", strIMEI
            };
            DataTable dtbData = this.objPLCIMEISalesInfo.SearchDataComment(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            treeComment.DataSource = dtbData;
            treeComment.ExpandAll();
        }

        private bool DeleteData()
        {
            DataTable dtbComment = (DataTable)treeComment.DataSource;
            List<IMEISalesInfo_Comment> lstIMEISalesInfo_Comment = new List<IMEISalesInfo_Comment>();
            foreach (DataRow dtr in dtbComment.Rows)
            {
                if (!Convert.ToBoolean(dtr["IsSelect"]))
                {
                    continue;   
                }
                if (Convert.ToBoolean(dtr["IsSystem"]))
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng không xóa bình luận là hệ thống!");
                    continue;   
                }
                IMEISalesInfo_Comment objIMEISalesInfo_Comment = new IMEISalesInfo_Comment();
                objIMEISalesInfo_Comment.CommentID = Convert.ToInt32(dtr["CommentID"]);
                objIMEISalesInfo_Comment.DeletedUser = SystemConfig.objSessionUser.UserName;
                lstIMEISalesInfo_Comment.Add(objIMEISalesInfo_Comment);
            }
            if (!objPLCIMEISalesInfo.DeteteComment(lstIMEISalesInfo_Comment.ToArray()))
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }
            return true;
        }

        /// <summary>
        /// EditData
        /// </summary>
        /// <returns></returns>
        private bool EditData()
        {
            try
            {
                IMEISalesInfo_Comment objIMEISalesInfo_Comment = objPLCIMEISalesInfo.LoadInfoComment(Convert.ToInt32(treeComment.FocusedNode.GetValue("COMMENTID")));
                frmIMEISalesInfo_CommentDetail frm = new frmIMEISalesInfo_CommentDetail();
                frm.IMEISalesInfo_Comment = objIMEISalesInfo_Comment;
                frm.IsCheckPermissionAdd = bolIsPermission_Add;
                frm.IsCheckPermissionEdit = bolIsPermission_Edit;
                frm.ShowDialog();
                if (frm.IsUpdated)
                {
                    LoadDataComment();
                }
                //this.FocusRow(this.intIDFocus);
                return true;
            }
            catch (Exception objExc)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi chỉnh sửa bình luận model !", objExc.ToString());
                SystemErrorWS.Insert("Lỗi chỉnh sửa bình luận model !", objExc.ToString(), this.Name + " -> EditData", DUIInventory_Globals.ModuleName);
                return false;
            }
        }

        public void ExcelExport(TreeList trList)
        {
            using (var dialog = new SaveFileDialog())
            {
                dialog.Title = "Excel Export";
                dialog.Filter = "Excel2003 Excel|*.xls|Excel2007 Excel|*.xlsx";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    trList.OptionsPrint.AutoWidth = false;
                    trList.ExportToXlsx(dialog.FileName);
                }
            }
        }

        /// <summary>
        /// FocusRow
        /// </summary>
        /// <param name="intID"></param>
        private void FocusRow(int intID)
        {
            try
            {
                if (treeComment.AllNodesCount > 0)
                {
                    for (int i = 0; i < treeComment.AllNodesCount; i++)
                    {
                        int intTemp = Convert.ToInt32(treeComment.Nodes[i].GetValue("COMMENTID"));
                        if (intID == intTemp)
                        {
                            treeComment.FocusedNode = treeComment.Nodes[i];
                            return;
                        }
                    }
                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi focus trên lưới");
                SystemErrorWS.Insert("Lỗi focus trên lưới", objExce.ToString(), this.Name + " -> FocusRow", DUIInventory_Globals.ModuleName);
                return;
            }
        }

        private int intOrder = 0;
        private void SetDepartment(int intDepartmentID, ref DataTable dtbResource, string strChar)
        {
            string str = strChar + "--";
            DataRow[] dtrRows = dtbResource.Select("REPLYTOCOMMENTID = " + intDepartmentID);
            if (dtrRows != null)
            {
                foreach (DataRow dtRow in dtrRows)
                {
                    dtRow["ORDERINDEX"] = intOrder++;
                    int intDepartmentIDTemp = Convert.ToInt32(dtRow["COMMENTID"].ToString().Trim());
                    dtRow["COMMENTID"] = str + ">" + dtRow["COMMENTID"].ToString();
                    SetDepartment(intDepartmentIDTemp, ref dtbResource, str);

                }
            }
        }
        #endregion

        private void grdViewData_DoubleClick(object sender, EventArgs e)
        {
            mnuItemEdit_Click(null, null);
        }

        private void repTreeDepartment_IsSelect_CheckedChanged(object sender, EventArgs e)
        {

            Cursor curent = Cursor.Current;
            this.Cursor = Cursors.WaitCursor;
            CheckEdit chkIsSelect = (CheckEdit)sender;

            DevExpress.XtraTreeList.Nodes.TreeListNode childNode = treeComment.FocusedNode;
            DevExpress.XtraTreeList.Nodes.TreeListNode parentNode = childNode.ParentNode;
            if (!childNode.HasChildren && childNode.ParentNode != null && Convert.ToBoolean(parentNode.GetValue("ISSELECT")))
            {
                childNode.SetValue("ISSELECT", true);
                chkIsSelect.Checked = true;
            }

            repTreeDepartment_Select_CheckedChanged(chkIsSelect.Checked, treeComment.FocusedNode);
            this.Cursor = curent;
        }

        private void repTreeDepartment_Select_CheckedChanged(bool bolSelect, DevExpress.XtraTreeList.Nodes.TreeListNode treeListNode)
        {
            try
            {
                treeListNode.SetValue(colTreeDepartment_IsSelect, bolSelect);
                if (treeListNode.HasChildren)
                {
                    for (int i = 0; i < treeListNode.Nodes.Count; i++)
                    {
                        DevExpress.XtraTreeList.Nodes.TreeListNode childNode = treeListNode.Nodes[i];
                        repTreeDepartment_Select_CheckedChanged(bolSelect, childNode);
                    }
                }
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi kiểm tra phòng ban trong phân quyền phòng ban.", ex.ToString());
            }
        }

        private void treeComment_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Control && e.KeyCode == Keys.C)
                {
                    TreeList treeList = (TreeList)sender;
                    Clipboard.SetText(treeList.FocusedNode.GetDisplayText(treeList.FocusedColumn));
                    e.Handled = true;
                }
            }
            catch
            {
            }
        }

    }
}
