﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using ERP.Inventory.PLC.IMEISales;
using ERP.Inventory.PLC.IMEISales.WSIMEISalesInfo;
using ERP.Inventory.DUI.IMEISale;
using System.IO;

namespace ERP.Inventory.DUI.IMEISales
{
    /// <summary>
    /// Cập nhật thứ tự thông tin bán hàng của IMEI/ Lô IMEI
    /// </summary>
    public partial class frmIMEISalesInfo_OrderIndexEdit : Form
    {
        #region Variable
        private string strPermission_Edit = "PM_LOTIMEI_IMEISALESINFO_ORDERINDEX_EDIT";

        private PLC.IMEISales.PLCIMEISalesInfo objPLCIMEISalesInfo = new PLC.IMEISales.PLCIMEISalesInfo();
        #endregion

        #region Constructor
        public frmIMEISalesInfo_OrderIndexEdit()
        {
            InitializeComponent();
        }
        #endregion

        #region Event
        private void frmIMEISalesInfoManager_Load(object sender, EventArgs e)
        {
            btnUpdate.Enabled = false;
            InitControl();
            FormatGrid();
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void cmdExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(this.grdData);
        }

        #endregion

        #region Method

        private void InitControl()
        {
            cmdSearch.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Edit);
            cboProductList.InitControl();
            string[] fieldNames = new string[] { "ORDERINDEX", "IMEI", "PRODUCTID", "PRODUCTNAME", "ISIMEI" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);          
        }


        private void LoadData()
        {
            DataTable dtbData = null;
            object[] objKeywords = new object[] {
                "@ProductIDList", cboProductList.ProductIDList,
                "@IMEI", txtIMEISearch.Text.Trim()};
            dtbData = this.objPLCIMEISalesInfo.SearchDataIMEIOrLotIMEI(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            dtbData.DefaultView.Sort = "OrderIndex asc";
            dtbData = dtbData.DefaultView.ToTable();
            if (cboProductList.ProductIDList == string.Empty && txtIMEISearch.Text.Trim() == string.Empty)
            {
                int intOrderIndex = 0;
                for (int i = 0; i < dtbData.Rows.Count; i++)
                {
                    intOrderIndex++;
                    if (Convert.ToInt32(dtbData.Rows[i]["OrderIndex"]) == 100000)
                    {
                        dtbData.Rows[i]["ISEDIT"] = 1;
                    }
                    dtbData.Rows[i]["OrderIndex"] = intOrderIndex;
                }
            }
        
            dtbData.AcceptChanges();
            grdData.DataSource = dtbData;
            btnUpdate.Enabled = grdViewData.RowCount > 0;
        }

        private bool FormatGrid()
        {
            try
            {
                                    
                grdViewData.Columns["ISIMEI"].Caption = "Là IMEI";
                grdViewData.Columns["IMEI"].Caption = "IMEI/Mã lô IMEI";
                grdViewData.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
                grdViewData.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
                grdViewData.Columns["ORDERINDEX"].Caption = "STT";
                grdViewData.Columns["ORDERINDEX"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;    
                grdViewData.OptionsView.ColumnAutoWidth = false;
                grdViewData.Columns["ISIMEI"].Width = 100;
                grdViewData.Columns["ISIMEI"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                grdViewData.Columns["IMEI"].Width = 200;
                grdViewData.Columns["PRODUCTID"].Width = 200;
                grdViewData.Columns["PRODUCTID"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                grdViewData.Columns["PRODUCTNAME"].Width = 300;
                grdViewData.Columns["PRODUCTNAME"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
                grdViewData.Columns["ORDERINDEX"].Width = 100;

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsIMEI = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsIMEI.ValueChecked = ((decimal)(1));
                chkCheckBoxIsIMEI.ValueUnchecked = ((decimal)(0));
                grdViewData.Columns["ISIMEI"].ColumnEdit = chkCheckBoxIsIMEI;

                grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewData.OptionsView.ShowAutoFilterRow = true;
                grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewData.OptionsView.ShowFooter = false;
                grdViewData.OptionsView.ShowGroupPanel = false;
                grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewData.ColumnPanelRowHeight = 40;

                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTxtOrderIndex = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                repTxtOrderIndex.MaxLength = 5;
                repTxtOrderIndex.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
                repTxtOrderIndex.Mask.EditMask = @"\d{1,9}";
                
                grdViewData.Columns["ORDERINDEX"].ColumnEdit = repTxtOrderIndex;
                repTxtOrderIndex.EditValueChanged += new EventHandler(repTxtOrderIndex_EditValueChanged);
                repTxtOrderIndex.Click += new EventHandler(repTxtOrderIndex_Click);       
                repTxtOrderIndex.KeyUp += new KeyEventHandler(repTxtOrderIndex_KeyUp);
                repTxtOrderIndex.Leave += new EventHandler(repTxtOrderIndex_Leave);
                grdViewData.Columns["ORDERINDEX"].OptionsColumn.AllowEdit = true;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách thông tin bán hàng của lô IMEI");
                SystemErrorWS.Insert("Định dạng lưới danh sách thông tin bán hàng của lô IMEI", objExce.ToString(), this.Name + " -> FormatGrid", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private void repTxtOrderIndex_EditValueChanged(object sender, EventArgs e)
        {
            //grdViewData.PostEditor();
            //DevExpress.XtraEditors.TextEdit txt = (DevExpress.XtraEditors.TextEdit)sender;
            //txt.Text = (Convert.ToInt32(txt.Text) - 1).ToString();
            //grdViewData.SetFocusedRowCellValue("OrderIndex", Convert.ToInt32(txt.Text));
            //DataTable dtbData = (DataTable)grdData.DataSource;
            //dtbData.DefaultView.Sort = "OrderIndex asc";
            //dtbData.AcceptChanges();
            //DataTable dtbData_Tmp = dtbData.DefaultView.ToTable();
            //for (int i = 0; i < dtbData_Tmp.Rows.Count; i++)
            //{
            //    dtbData_Tmp.Rows[i]["OrderIndex"] = i + 1;
            //}

            //dtbData_Tmp.AcceptChanges();
            //grdData.DataSource = dtbData_Tmp;
        }

        private int intOrderBefore = 0;
        private void repTxtOrderIndex_Click(object sender, EventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0)
            {
                return;
            }
            DevExpress.XtraEditors.TextEdit txt = (DevExpress.XtraEditors.TextEdit)sender;
            if (string.IsNullOrEmpty(txt.Text.Trim()))
            {
                return;
            }
            intOrderBefore = Convert.ToInt32(txt.Text.Trim());
        }

        private void repTxtOrderIndex_Leave(object sender, EventArgs e)
        {
            grdViewData.PostEditor();
            DevExpress.XtraEditors.TextEdit txt = (DevExpress.XtraEditors.TextEdit)sender;
            //txt.Text = (Convert.ToInt32(txt.Text) - 1).ToString();
            //grdViewData.SetFocusedRowCellValue("OrderIndex", Convert.ToInt32(txt.Text));

            DataTable dtbData = (DataTable)grdData.DataSource;
            dtbData.DefaultView.Sort = "OrderIndex asc";
            dtbData.AcceptChanges();
            DataTable dtbData_Tmp = dtbData.DefaultView.ToTable();
            for (int i = 0; i < dtbData_Tmp.Rows.Count; i++)
            {
                dtbData_Tmp.Rows[i]["OrderIndex"] = i + 1;
            }

            dtbData_Tmp.AcceptChanges();
            grdData.DataSource = dtbData_Tmp;
            grdViewData.FocusedRowHandle = Convert.ToInt32(txt.Text);


            //if (grdViewData.FocusedRowHandle < 0)
            //{
            //    return;
            //}
            //DevExpress.XtraEditors.TextEdit txt = (DevExpress.XtraEditors.TextEdit)sender;
            //string strIMEI = grdViewData.GetFocusedRowCellValue("IMEI").ToString().Trim();
            //if (string.IsNullOrEmpty(txt.Text.Trim()))
            //{
            //    return;
            //}
            //int intOrderIndex = Convert.ToInt32(txt.Text.Trim());
            //DataTable dtbTable = (DataTable)grdData.DataSource;
            //if (intOrderIndex > intOrderBefore)
            //{
            //    foreach (DataRow item in dtbTable.Rows)
            //    {
            //        int intOrderIndexTemp = Convert.ToInt32(item["OrderIndex"]);
            //        string strIMEITemp = item["IMEI"].ToString().Trim();
            //        if (strIMEITemp == strIMEI)
            //        {
            //            item["ISEDIT"] = 1;
            //        }
            //        else if (intOrderIndexTemp <= intOrderIndex && intOrderIndexTemp > intOrderBefore)
            //        {
            //            intOrderIndexTemp--;
            //            item["OrderIndex"] = intOrderIndexTemp;
            //            item["ISEDIT"] = 1;
            //        }

            //    }
            //}
            //else
            //{
            //    foreach (DataRow item in dtbTable.Rows)
            //    {
            //        int intOrderIndexTemp = Convert.ToInt32(item["OrderIndex"]);
            //        string strIMEITemp = item["IMEI"].ToString().Trim();
            //        if (strIMEITemp == strIMEI)
            //        {
            //            item["ISEDIT"] = 1;
            //        }
            //        else if (intOrderIndexTemp >= intOrderIndex && intOrderIndexTemp < intOrderBefore)
            //        {
            //            intOrderIndexTemp++;
            //            item["OrderIndex"] = intOrderIndexTemp;
            //            item["ISEDIT"] = 1;
            //        }
            //    }
            //}

            //dtbTable.AcceptChanges();
            //dtbTable.DefaultView.Sort = "OrderIndex asc";
            //grdData.DataSource = dtbTable;
        }

        private void repTxtOrderIndex_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                repTxtOrderIndex_Leave(sender, null);
            }
            //if (grdViewData.FocusedRowHandle < 0)
            //{
            //    return;
            //}
            //if (e.KeyCode == Keys.Enter)
            //{
            //    DevExpress.XtraEditors.TextEdit txt = (DevExpress.XtraEditors.TextEdit)sender;
            //    string strIMEI = grdViewData.GetFocusedRowCellValue("IMEI").ToString().Trim();
            //    if (string.IsNullOrEmpty(txt.Text.Trim()))
            //    {
            //        return;
            //    }
            //    int intOrderIndex = Convert.ToInt32(txt.Text.Trim());
            //    DataTable dtbTable = (DataTable)grdData.DataSource;
            //    foreach (DataRow item in dtbTable.Rows)
            //    {
            //        int intOrderIndexTemp = Convert.ToInt32(item["OrderIndex"]);
            //        string strIMEITemp = item["IMEI"].ToString().Trim();
            //        if (intOrderIndexTemp >= intOrderIndex && intOrderIndexTemp != 100000 && strIMEITemp != strIMEI)
            //        {
            //            intOrderIndexTemp++;
            //            item["OrderIndex"] = intOrderIndexTemp;
            //        }
                    
            //    }
            //    dtbTable.DefaultView.Sort = "OrderIndex asc";
            //    grdData.DataSource = dtbTable;
            //}
            
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dtbTable = ((DataTable)grdData.DataSource).Copy();
            //dtbTable.DefaultView.RowFilter = "ISEDIT = 1";
            dtbTable = dtbTable.DefaultView.ToTable();
            objPLCIMEISalesInfo.UpdateOrderIndex(dtbTable);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            else
            {
                MessageBoxObject.ShowInfoMessage(this, "Cập nhật index IMEI/lô IMEI thành công!");
            }
        }

        private void txtIMEISearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cmdSearch_Click(null, null);
            }
        }       
    }
}
