﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.IMEISales
{
    public partial class frmImageView : Form
    {
        public frmImageView()
        {
            InitializeComponent();
        }
        public string strCaption = string.Empty;
        public string strURLImage = string.Empty;
        public bool CustomSize = false;
        private void frmImageView_Load(object sender, EventArgs e)
        {
            if (strURLImage == string.Empty)
                this.Close();
            using (var fs = new System.IO.FileStream(strURLImage, System.IO.FileMode.Open))
            {
                var bmp = new Bitmap(fs);
                Image img= (Bitmap)bmp.Clone();
                if (!CustomSize)
                {
                    int originalWidth = img.Width;
                    int originalHeight = img.Height;
                    pictureBox1.Image = img;
                    this.Text = strCaption + " (" + originalWidth + " x " + originalHeight + ")";
                }
                else
                {
                    this.Text = strCaption;
                    pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                    pictureBox1.Image = img;
                }
            }
        }
    }
}
