﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using ERP.Inventory.PLC.IMEISales;
using ERP.Inventory.PLC.IMEISales.WSIMEISalesInfo;
using System.Text.RegularExpressions;
using Library.AppCore.LoadControls;

namespace ERP.Inventory.DUI.IMEISales
{
    public partial class frmIMEISalesInfo_CommentDetail : Form
    {
        public frmIMEISalesInfo_CommentDetail()
        {
            InitializeComponent();
        }
        private bool bolIsReply = false;
        private bool bolIsShowReply = false;
        private IMEISalesInfo_Comment objIMEISalesInfo_Comment_Reply = null;
        private List<IMEISalesInfo_Comment> objIMEISalesInfo_CommentList = null;
        private IMEISalesInfo_Comment objIMEISalesInfo_Comment = null;
        private PLCIMEISalesInfo objPLCIMEISalesInfo = new PLCIMEISalesInfo();

        private bool bolIsUpdated = false;
        private bool bolIsCheckPermissionAdd = false;
        private bool bolIsCheckPermissionEdit = false;

        public List<IMEISalesInfo_Comment> IMEISalesInfo_CommentList
        {
            get {

                if (objIMEISalesInfo_CommentList == null)
                    objIMEISalesInfo_CommentList = new List<IMEISalesInfo_Comment>();
                return objIMEISalesInfo_CommentList; }
            set { objIMEISalesInfo_CommentList = value; }
        }
      

        public bool IsReply
        {
            get { return bolIsReply;  }
            set { bolIsReply = value; }
        }

        public bool IsCheckPermissionAdd
        {
            get { return bolIsCheckPermissionAdd; }
            set { bolIsCheckPermissionAdd = value; }
        }

        public bool IsCheckPermissionEdit
        {
            get { return bolIsCheckPermissionEdit; }
            set { bolIsCheckPermissionEdit = value; }
        }

        public bool IsUpdated
        {
            get { return bolIsUpdated; }
            set { bolIsUpdated = value; }
        }
        public IMEISalesInfo_Comment IMEISalesInfo_Comment
        {
            get { return objIMEISalesInfo_Comment; }
            set { objIMEISalesInfo_Comment = value; }
        }

        public IMEISalesInfo_Comment IMEISalesInfo_Comment_Reply
        {
            get { return objIMEISalesInfo_Comment_Reply; }
            set { objIMEISalesInfo_Comment_Reply = value; }
        }

        private void frmModelComment_Load(object sender, EventArgs e)
        {
            if (SystemConfig.objSessionUser.UserName != "administrator")
            {
                chkIsSystem.Enabled = false;
            }
            try
            {
                if (objIMEISalesInfo_Comment != null)
                {
                    txtCommentID.Text = Convert.ToString(objIMEISalesInfo_Comment.CommentID);
                    txtCommentTitle.Text = objIMEISalesInfo_Comment.CommentTitle;
                    txtCommentUser.Text = objIMEISalesInfo_Comment.CommentUser;
                    txtContent.Lines = objIMEISalesInfo_Comment.CommentContent.Split('\n');
                    txtEmail.Text = objIMEISalesInfo_Comment.CommentEmail;
                    txtPhoneNumber.Text = objIMEISalesInfo_Comment.PhoneNumber;
                    chkIsActive.Checked = objIMEISalesInfo_Comment.IsActive;
                    chkIsSystem.Checked = objIMEISalesInfo_Comment.IsSystem;
                    dtCommentDate.Value = objIMEISalesInfo_Comment.CommentDate.Value;
                    trbRate.Value = objIMEISalesInfo_Comment.ModelRate;
                    //if (objIMEISalesInfo_Comment.ReplyToCommentID > 0)
                    //    txtCommentUser.ReadOnly = true;
                    //else
                    //    txtCommentUser.ReadOnly = false;
                }
                else
                    this.Close();
                if (!bolIsCheckPermissionEdit)
                {
                    txtCommentID.ReadOnly = true;
                    txtCommentTitle.ReadOnly = true;
                    //txtCommentUser.ReadOnly = true;
                    txtContent.Properties.ReadOnly = true;
                    txtEmail.ReadOnly = true;
                    chkIsActive.Enabled = false;
                    chkIsSystem.Enabled = false;
                    dtCommentDate.Enabled = false;
                    trbRate.Properties.ReadOnly = true;
                    this.btnUpdate.Enabled = false;
                }
                txtContentReply.Enabled = false;
                txtCommentTitleReply.Enabled = false;
                lnkReply.Visible = objIMEISalesInfo_Comment.ReplyToCommentID < 1;
            }
            catch (Exception)
            {
            }
        }
        private bool CheckInput()
        {
            if (txtContent.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this, "Bạn chưa nhập nội dung bình luận", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtContent.Focus();
                return false;
            }
            if (txtCommentUser.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this, "Bạn chưa nhập người bình luận", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCommentUser.Focus();
                return false;
            }
            if (txtEmail.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this, "Bạn chưa nhập email", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmail.Focus();
                return false;   
            }
            if (txtPhoneNumber.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this, "Bạn chưa nhập số điện thoại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPhoneNumber.Focus();
                return false;
            }
            if (!Regex.IsMatch(txtEmail.Text,
                   @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$"))
            {
                MessageBox.Show(this, "Email không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmail.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtPhoneNumber.Text.Trim()))
            {
                txtPhoneNumber.Focus();
                MessageBox.Show(this, "Vui lòng nhập số điện thoại khách hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (txtPhoneNumber.Text.Trim().Length > 0 && (!Globals.CheckIsPhoneNumber(txtPhoneNumber.Text.Trim())
                || txtPhoneNumber.Text.Trim().Length < 10 || txtPhoneNumber.Text.Trim().Length > 11))
            {
                txtPhoneNumber.Focus();
                MessageBox.Show(this, "Số điện thoại khách hàng không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (bolIsShowReply)
            {
                if (txtContentReply.Text.Trim() == string.Empty)
                {
                    MessageBox.Show(this, "Bạn chưa nhập nội dung trả lời bình luận", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtContentReply.Focus();
                    return false;
                }
            }
            return true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;
            objIMEISalesInfo_Comment.CommentID = Convert.ToInt32(txtCommentID.Text);
            objIMEISalesInfo_Comment.CommentTitle = txtCommentTitle.Text;
            objIMEISalesInfo_Comment.CommentUser = txtCommentUser.Text;
            objIMEISalesInfo_Comment.CommentContent = txtContent.Text;
            objIMEISalesInfo_Comment.CommentEmail = txtEmail.Text;
            objIMEISalesInfo_Comment.PhoneNumber = txtPhoneNumber.Text.Trim();
            objIMEISalesInfo_Comment.IsActive = chkIsActive.Checked;
            objIMEISalesInfo_Comment.IsSystem = chkIsSystem.Checked;
            objIMEISalesInfo_Comment.CommentDate = dtCommentDate.Value;
            objIMEISalesInfo_Comment.ModelRate = trbRate.Value;
            if (txtCommentTitleReply.Text.Trim() != string.Empty || txtContentReply.Text.Trim() != string.Empty)
            {
                if (objIMEISalesInfo_Comment_Reply == null)
                {
                    objIMEISalesInfo_Comment_Reply = new IMEISalesInfo_Comment();
                    objIMEISalesInfo_Comment_Reply.CommentID = IMEISalesInfo_CommentList.Count + 1;
                    objIMEISalesInfo_Comment_Reply.IsAddNew = true;                    
                }
                if (objIMEISalesInfo_Comment.ReplyToCommentID > 0)
                {
                    var lst = from o in objIMEISalesInfo_CommentList
                              where o.ReplyToCommentID == objIMEISalesInfo_Comment.ReplyToCommentID
                              select o;
                    objIMEISalesInfo_Comment_Reply.OrderIndex = lst.Count() + 1;
                    objIMEISalesInfo_Comment_Reply.ReplyToCommentID = Convert.ToUInt32(objIMEISalesInfo_Comment.ReplyToCommentID);
                }
                else
                {
                    var lst = from o in objIMEISalesInfo_CommentList
                              where o.ReplyToCommentID == objIMEISalesInfo_Comment.CommentID
                              select o;
                    objIMEISalesInfo_Comment_Reply.OrderIndex = lst.Count() + 1;
                    objIMEISalesInfo_Comment_Reply.ReplyToCommentID = Convert.ToUInt32(objIMEISalesInfo_Comment.CommentID);
                }
                objIMEISalesInfo_Comment_Reply.IMEI= objIMEISalesInfo_Comment.IMEI;
                objIMEISalesInfo_Comment_Reply.ProductID = objIMEISalesInfo_Comment.ProductID;
                objIMEISalesInfo_Comment_Reply.CommentTitle = txtCommentTitle.Text;
                objIMEISalesInfo_Comment_Reply.CommentUser = SystemConfig.objSessionUser.UserName;
                objIMEISalesInfo_Comment_Reply.CommentContent = txtContentReply.Text;
                objIMEISalesInfo_Comment_Reply.CommentEmail = SystemConfig.DefaultCompany.Email;
                objIMEISalesInfo_Comment_Reply.IsActive = true;
                objIMEISalesInfo_Comment_Reply.IsSystem = false;
                objIMEISalesInfo_Comment_Reply.CommentDate = Library.AppCore.Globals.GetServerDateTime();
                objIMEISalesInfo_Comment_Reply.ModelRate = 0;
                objIMEISalesInfo_Comment_Reply.IsStaffComment = true;
                objIMEISalesInfo_Comment_Reply.CommentFullName = SystemConfig.objSessionUser.FullName;
                objIMEISalesInfo_Comment_Reply.ISCommentUserLogin = true;
            }

            bool bolUpdate = false;
            if (IMEISalesInfo_Comment != null)
            {
                List<object> lstOrderIndex = new List<object>();
                objPLCIMEISalesInfo.UpdateComment(objIMEISalesInfo_Comment, lstOrderIndex);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Lỗi cập nhật bình luận thông tin bán hàng của IMEI !", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    SystemErrorWS.Insert("Lỗi cập nhật bình luận thông tin bán hàng của IMEI !", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, this.Name + " -> EditData", DUIInventory_Globals.ModuleName);
                    return;
                }
            }
            if (objIMEISalesInfo_Comment_Reply != null && objIMEISalesInfo_Comment_Reply.IsAddNew)
            {
                List<object> lstOrderIndex = new List<object>();
                objIMEISalesInfo_Comment_Reply.CreatedUser = SystemConfig.objSessionUser.UserName;
                objIMEISalesInfo_Comment_Reply.CreatedDate = Globals.GetServerDateTime();
                objPLCIMEISalesInfo.InsertComment(objIMEISalesInfo_Comment_Reply);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Lỗi trả lời bình luận thông tin bán hàng của IMEI !", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    SystemErrorWS.Insert("Lỗi trả lời bình luận thông tin bán hàng của IMEI !", SystemConfig.objSessionUser.ResultMessageApp.MessageDetail, this.Name + " -> EditData", DUIInventory_Globals.ModuleName);
                    return;
                }
            }
            bolUpdate = true;
            if (bolUpdate)
            {
                MessageBox.Show(this, "Cập nhật bình luận thông tin bán hàng của IMEI thành công !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            this.bolIsUpdated = true;
            this.Close();
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chkIsSystem_CheckedChanged(object sender, EventArgs e)
        {
            EnableControl(!chkIsSystem.Checked);
        }
        /// <summary>
        /// Ẩn hiện control
        /// </summary>
        /// <param name="bolValue">true: hiện; false: ẩn</param>
        private void EnableControl(bool bolValue)
        {
            dtCommentDate.Enabled = bolValue;
            txtCommentTitle.ReadOnly = !bolValue;
            //txtCommentUser.ReadOnly = !bolValue;
            txtContent.Properties.ReadOnly = !bolValue;
            txtEmail.ReadOnly = !bolValue;
            txtPhoneNumber.ReadOnly = !bolValue;
            chkIsActive.Enabled = bolValue;
            trbRate.Properties.ReadOnly = !bolValue;
        }

        private void frmModelComment_FormClosing(object sender, FormClosingEventArgs e)
        {
        }
        
        private void lnkReply_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (!bolIsShowReply && objIMEISalesInfo_Comment.ReplyToCommentID < 1 && bolIsCheckPermissionAdd)//ClientSize.Height == 339)
            {
                this.ClientSize = new System.Drawing.Size(618, 524);
                lnkReply.Text = "<< Trả lời";
                bolIsShowReply = true;
                txtContentReply.Enabled = true;
                txtCommentTitleReply.Enabled = true;
            }
            else
                //if (ClientSize.Height == 524)
                {
                    this.ClientSize = new System.Drawing.Size(618, 374);
                    lnkReply.Text = "Trả lời >>";
                    bolIsShowReply = false;
                    txtContentReply.Enabled = false;
                    txtCommentTitleReply.Enabled = false;
                }
        }
    }
}
