﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using ERP.Inventory.DUI.IMEISale;
using ERP.Inventory.PLC.IMEISales;
using ERP.Inventory.PLC.IMEISales.WSIMEISalesInfo;

namespace ERP.Inventory.DUI.IMEISales
{
    public partial class frmIMEISalesInfo : Form
    {
        #region Variable
        private PLC.IMEISales.PLCIMEISalesInfo objPLCIMEISalesInfo = new PLC.IMEISales.PLCIMEISalesInfo();
        private PLC.IMEISales.WSIMEISalesInfo.ResultMessage objResultMessage = new PLC.IMEISales.WSIMEISalesInfo.ResultMessage();
        private int intMainGroupID = -1, intStoreID = Library.AppCore.SystemConfig.ConfigStoreID;
        private DataTable dtbDataSpecStatus = null, dtbDataSpec = null;
        private DataRow dr = null;
        private bool bolIsEdit = false;
        private bool bolIsReview = false;
        private DateTime dtEndWarantyDate = DateTime.Now;
        //bolIsConfirm = false, bolIsReviewed = false;
        private string strInputVoucherDetailID = string.Empty, strReviewedUser = string.Empty, strPrintVATContent = string.Empty;
        private DateTime? dteReviewedDate = null;
        public bool bolIsUpdate = false;
        private bool bolPermission_Edit = false;        
        private bool bolPermission_Review = false;
        private bool bolPermission_UnReview = false;
        
        #endregion

        #region Constructor

        public bool Permission_Edit
        {
            get { return bolPermission_Edit; }
            set { bolPermission_Edit = value; }
        }

        public bool Permission_Review
        {
            get { return bolPermission_Review; }
            set { bolPermission_Review = value; }
        }

        public bool Permission_UnReview
        {
            get { return bolPermission_UnReview; }
            set { bolPermission_UnReview = value; }
        }

        public frmIMEISalesInfo()
        {
            InitializeComponent();
        }

        public frmIMEISalesInfo(DataRow _dr)
        {
            InitializeComponent();
            this.bolIsEdit = true;
            this.dr = _dr;
        }
        #endregion

        #region Event
        private void frmIMEISalesInfo_Load(object sender, EventArgs e)
        {
            this.InitControl();
            tabCtrlIMEI.SelectedTab = tabIMEIInfo;
            if (this.bolIsEdit)
            {
                EnableControl(bolIsEdit);
                //this.EnableControl(this.bolIsEdit);
                EditData();
            }
        }

        private void cmdUpdate_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
                return;
            if (!this.UpdateData())
                return;
            this.ClearText();
            this.cmdClose.PerformClick();
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            GC.Collect();
            this.Close();
        }

        private void btnSelectProduct_Click(object sender, EventArgs e)
        {
            try
            {
                ERP.MasterData.DUI.Search.frmProductSearch frm = new ERP.MasterData.DUI.Search.frmProductSearch();
                frm.ShowDialog();
                this.txtProductID.Text = frm.Product.ProductID.Trim();
                this.txtProductName.Text = frm.Product.ProductName.Trim();
                this.intMainGroupID = frm.Product.MainGroupID;
                this.LoadData();
                this.FormatGrid(true);
            }
            catch { }
        }

        private void cboProduct_Enter(object sender, EventArgs e)
        {
            try
            {
                if (grdViewProduct.FocusedColumn.FieldName == "PRODUCTSPECSTATUSID")
                {
                    DevExpress.XtraEditors.LookUpEdit edit = ((DevExpress.XtraEditors.LookUpEdit)(sender));
                    DataTable dt = (DataTable)edit.Properties.DataSource;
                    DataView dv = new DataView(dt);
                    DataRow row = grdViewProduct.GetFocusedDataRow();
                    dv.RowFilter = "PRODUCTSPECID=" + row["ProductSpecID"].ToString().Trim() + " OR PRODUCTSPECSTATUSID=-1";
                    dv.Sort = "PRODUCTSPECSTATUSID";
                    edit.Properties.DataSource = dv;
                }
            }
            catch { }
        }

        private void chkIsBrandNewWarranty_CheckedChanged(object sender, EventArgs e)
        {
            this.dteEndWarantyDate.Enabled = this.chkIsBrandNewWarranty.Checked;
        }

        private void mnuItemDelete_Click(object sender, EventArgs e)
        {
            this.ClearText();
        }
        #endregion

        #region Method
        private void InitControl()
        {
            this.btnSelectProduct.Click += new EventHandler(btnSelectProduct_Click);
            chkIsReviewed.Enabled = bolPermission_Review;
        }

        private void LoadData()
        {
            object[] objKeywords = new object[] { "@MainGroupID", this.intMainGroupID };
            this.dtbDataSpecStatus = this.objPLCIMEISalesInfo.SearchDataSpecStatus(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            this.dtbDataSpec = this.objPLCIMEISalesInfo.SearchDataSpec(objKeywords);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
        }

        private bool UpdateData()
        {
            string strMessage = string.Empty;
            try
            {
                PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo objIMEISalesInfo = new PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo();
                objIMEISalesInfo.ProductID = this.txtProductID.Text.Trim();
                objIMEISalesInfo.IMEI = this.txtIMEI.Text.Trim();
                objIMEISalesInfo.InputVoucherDetailID = (this.strInputVoucherDetailID.Trim() == string.Empty) ? "-1" : this.strInputVoucherDetailID.Trim();
                objIMEISalesInfo.StoreID = this.intStoreID;
                objIMEISalesInfo.IsBrandNewWarranty = this.chkIsBrandNewWarranty.Checked;
                objIMEISalesInfo.EndWarrantyDate = this.dteEndWarantyDate.Value.Date;
                objIMEISalesInfo.Note = this.txtNote.Text.Trim();
                objIMEISalesInfo.IsConfirm = chkIsComfirm.Checked;
                objIMEISalesInfo.IsReviewed = chkIsReviewed.Checked;
                objIMEISalesInfo.ReviewedUser = this.strReviewedUser.Trim();
                objIMEISalesInfo.ReviewedDate = this.dteReviewedDate;
                objIMEISalesInfo.PrintVATContent = this.strPrintVATContent.Trim(',');
                List<IMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpec = this.IMEISalesInfo_ProSpec();
                objIMEISalesInfo.IMEISalesInfo_ProSpec = lstIMEISalesInfo_ProSpec.ToArray();
                objIMEISalesInfo.WebProductSpecContent = GetWebProductSpecContent(lstIMEISalesInfo_ProSpec);
                if (objProduct_ImagesList != null && objProduct_ImagesList.Count > 0)
                {
                    var NewImages = from img in objProduct_ImagesList
                                    where img.IsNew == true
                                    select img;
                    if (NewImages != null && NewImages.Count() > 0)
                    {
                        List<IMEISalesInfo_Images> objNewList = NewImages.ToList();
                        foreach (var item in objNewList)
                        {
                            string strLargesizeImageFileID = string.Empty;
                            item.LargeSizeImage = SaveAttachment(ref strLargesizeImageFileID, item.LargesizeImage_Local);
                            item.LargeSizeImageFileID = strLargesizeImageFileID;
                            string strMediumSizeImageFileID = string.Empty;
                            item.MediumSizeImage = SaveAttachment(ref strMediumSizeImageFileID, item.MediumSizeImage_Local);
                            item.MediumSizeImageFileID = strMediumSizeImageFileID;
                            string strSmallSizeImageFileID = string.Empty;
                            item.SmallSizeImage = SaveAttachment(ref strSmallSizeImageFileID, item.SmallSizeImage_Local);
                            item.SmallSizeImageFileID = strSmallSizeImageFileID;
                            item.IsNew = false;
                        }
                    }
                }
                objIMEISalesInfo.IMEISalesInfo_ImagesList = objProduct_ImagesList.ToArray();
                //objProduct.Product_ImagesList_Del = objProduct_ImagesList_Del.ToArray();
                if (this.bolIsEdit)
                {
                    strMessage = "Cập nhật ";
                    objIMEISalesInfo.UpdatedUser = Library.AppCore.SystemConfig.objSessionUser.UserName;
                    if (!bolIsReview && objIMEISalesInfo.IsReviewed)
                    {
                        objIMEISalesInfo.ReviewedUser = SystemConfig.objSessionUser.UserName;
                        objIMEISalesInfo.ReviewedDate = DateTime.Now;
                    }
                    if (!this.objPLCIMEISalesInfo.Update(objIMEISalesInfo))
                    {
                        MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return false;
                    }
                }
                else
                {
                    strMessage = "Thêm ";
                    objIMEISalesInfo.CreatedUser = Library.AppCore.SystemConfig.objSessionUser.UserName;
                    if (objIMEISalesInfo.IsReviewed)
                    {
                        objIMEISalesInfo.ReviewedUser = SystemConfig.objSessionUser.UserName;
                        objIMEISalesInfo.ReviewedDate = DateTime.Now;
                    }
                    if (!this.objPLCIMEISalesInfo.Insert(objIMEISalesInfo))
                    {
                        MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        return false;
                    }
                }
                MessageBoxObject.ShowInfoMessage(this, strMessage + "thông tin bán hàng của IMEI thành công !");
                this.bolIsUpdate = true;
                return true;
            }
            catch (Exception objExc)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi " + strMessage + "thông tin bán hàng của IMEI !");
                SystemErrorWS.Insert("Lỗi " + strMessage + "thông tin bán hàng của IMEI !", objExc.ToString(), this.Name + " -> UpdateData", Globals.ModuleName);
                return false;
            }
        }

        private string GetWebProductSpecContent(List<IMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpec)
        {
            string strWebProductSpecContent = string.Empty;
            List<IMEISalesInfo_ProSpec> lstUsed = new List<IMEISalesInfo_ProSpec>();
            foreach (IMEISalesInfo_ProSpec item in lstIMEISalesInfo_ProSpec)
            {
                IMEISalesInfo_ProSpec IMEISalesInfo_ProSpecTemp = lstUsed.Find(p => p.ProductID == item.ProductID && p.ProductID == item.ProductID && p.ProductSpecID == item.ProductSpecID);
                if (IMEISalesInfo_ProSpecTemp == null)
                {
                    lstUsed.Add(item);
                    List<IMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpecTemp = lstIMEISalesInfo_ProSpec.FindAll
                                                                (p => p.ProductID == item.ProductID && p.ProductID == item.ProductID && p.ProductSpecID == item.ProductSpecID);
                    if (item.ProductSpecStatusID > 0)
                    {

                        strWebProductSpecContent += item.ProductSpecName;
                        strWebProductSpecContent += " : ";
                        foreach (IMEISalesInfo_ProSpec item1 in lstIMEISalesInfo_ProSpecTemp)
                        {
                            if (item1.ProductSpecStatusID > 0)
                            {
                                DataRow[] dr = dtbDataSpecStatus.Select("ProductSpecStatusID = " + item1.ProductSpecStatusID);
                                if (dr != null || dr.Length > 0)
                                {
                                    strWebProductSpecContent += dr[0]["ProductSpecStatusName"].ToString().Trim();
                                    strWebProductSpecContent += ",";
                                }
                            }
                        }
                        strWebProductSpecContent = strWebProductSpecContent.TrimEnd(',');
                        strWebProductSpecContent += " ; "; 
                    }           
                }              
            }
            strWebProductSpecContent = strWebProductSpecContent.TrimEnd(';');
            return strWebProductSpecContent;
        }

        private List<PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_ProSpec> IMEISalesInfo_ProSpec()
        {
            try
            {
                List<PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_ProSpec> lstIMEISalesInfo_ProSpec = new List<PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_ProSpec>();
                PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_ProSpec objIMEISalesInfo_ProSpec;
                DataTable dtbSalesInfo_ProSpec = (DataTable)this.grdProduct.DataSource;
                if (dtbSalesInfo_ProSpec != null)
                {
                    foreach (DataRow r in dtbSalesInfo_ProSpec.Rows)//.Select("ProductSpecStatusID Is Not Null"))
                    {
                        objIMEISalesInfo_ProSpec = new PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_ProSpec();
                        objIMEISalesInfo_ProSpec.ProductID = this.txtProductID.Text.Trim();
                        objIMEISalesInfo_ProSpec.IMEI = this.txtIMEI.Text.Trim();
                        objIMEISalesInfo_ProSpec.ProductSpecID = Convert.ToInt32(r["ProductSpecID"].ToString().Trim());
                        objIMEISalesInfo_ProSpec.ProductSpecName = r["ProductSpecName"].ToString().Trim();
                        objIMEISalesInfo_ProSpec.ProductSpecStatusID = Convert.ToInt32(r["ProductSpecStatusID"].ToString().Trim());
                        objIMEISalesInfo_ProSpec.ProductSpecStatusName = r["ProductSpecStatusName"].ToString().Trim();
                        objIMEISalesInfo_ProSpec.Note = r["Note"].ToString().Trim();
                        objIMEISalesInfo_ProSpec.UpdatedUser = Library.AppCore.SystemConfig.objSessionUser.UserName;
                        DataRow[] row = this.dtbDataSpecStatus.Select("PRODUCTSPECSTATUSID=" + r["ProductSpecStatusID"].ToString().Trim() + " AND ProductSpecStatusID<>-1");
                        if (row != null && row.Length > 0)
                            this.strPrintVATContent += r["ProductSpecName"].ToString().Trim() + ": " + row[0]["ProductSpecStatusName"].ToString() + ", ";
                        lstIMEISalesInfo_ProSpec.Add(objIMEISalesInfo_ProSpec);
                    }
                }
                return lstIMEISalesInfo_ProSpec;
            }
            catch (Exception objExc)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi tạo danh sách trạng thái thông tin sản phẩm !", objExc.ToString());
                SystemErrorWS.Insert("Lỗi tạo danh sách trạng thái thông tin sản phẩm !", objExc.ToString(), this.Name + " --> IMEISalesInfo_ProSpec", DUIInventory_Globals.ModuleName);
                return null;
            }
        }

        private void ClearText()
        {
            this.txtProductID.Text = string.Empty;
            this.txtProductName.Text = string.Empty;
            this.txtIMEI.Text = string.Empty;
            this.txtPrintVATContent.Text = string.Empty;
            this.txtNote.Text = string.Empty;
            this.chkIsBrandNewWarranty.Checked = false;
            this.dteEndWarantyDate.Value = DateTime.Now;
            DataTable dtb = (DataTable)this.grdProduct.DataSource;
            if (dtb != null) dtb.Clear();
        }

        private void EnableControl(bool _value)
        {
            this.SetColorTextReadOnly(this.txtProductID, _value);
            this.SetColorTextReadOnly(this.txtProductName, _value);
            this.btnSelectProduct.Enabled = !_value;
            this.SetColorTextReadOnly(this.txtIMEI, _value);
        }

        private bool ValidateData()
        {
            if (string.IsNullOrWhiteSpace(this.txtIMEI.Text))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập IMEI !");
                this.txtIMEI.Focus();
                return false;

            }
            //Hủy duyệt và không thay đổi ngày hết hạn bảo hành thì không check
            if (!(bolIsReview && !chkIsReviewed.Checked && dtEndWarantyDate == dteEndWarantyDate.Value))
            {
                if (this.chkIsBrandNewWarranty.Checked)
                {
                    if (this.dteEndWarantyDate.Value.CompareTo(DateTime.Now.Date) < 0)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Ngày hết hạn bảo hành không thể nhỏ hơn ngày hiện tại !");
                        this.dteEndWarantyDate.Focus();
                        return false;
                    }
                }
            {
                if (this.dteEndWarantyDate.Value.CompareTo(DateTime.Now.Date) < 0)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Ngày hết hạn bảo hành không thể nhỏ hơn ngày hiện tại !");
                    this.dteEndWarantyDate.Focus();
                    return false;
                }
            }
            }
            
            if (!this.bolIsEdit)
            {
                PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo objIMEISalesInfo = this.objPLCIMEISalesInfo.LoadInfo(this.txtIMEI.Text.Trim(), this.txtProductID.Text.Trim());
                if (objIMEISalesInfo != null && SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                if (objIMEISalesInfo != null && objIMEISalesInfo.IsExist)
                {
                    MessageBoxObject.ShowWarningMessage(this, "IMEI đã tồn tại. Vui lòng kiểm tra lại !");
                    this.txtIMEI.Focus();
                    return false;
                }
            }
            return true;
        }

        private bool EditData()
        {
            try
            {
                txtIMEI.Text = dr["IMEI"].ToString().Trim();
                txtProductID.Text = dr["ProductID"].ToString().Trim();
                PLCIMEISalesInfo objPLCIMEISalesInfo = new PLCIMEISalesInfo();
                IMEISalesInfo objIMEISalesInfo = objPLCIMEISalesInfo.LoadInfo(txtIMEI.Text.Trim(), txtProductID.Text.Trim());
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                }               
                txtProductName.Text = dr["ProductName"].ToString().Trim();
                txtPrintVATContent.Lines = objIMEISalesInfo.PrintVATContent.Split('\n');
                txtNote.Lines = objIMEISalesInfo.Note.Split('\n');
                chkIsBrandNewWarranty.Checked = objIMEISalesInfo.IsBrandNewWarranty;
                if (objIMEISalesInfo.EndWarrantyDate == null)
                {
                    dteEndWarantyDate.Text = string.Empty;
                }
                else
                {
                    dteEndWarantyDate.Value = objIMEISalesInfo.EndWarrantyDate.Value;
                }                
                strInputVoucherDetailID = objIMEISalesInfo.InputVoucherDetailID;
                intStoreID = objIMEISalesInfo.StoreID;
                chkIsComfirm.Checked = objIMEISalesInfo.IsConfirm;
                chkIsReviewed.Checked = objIMEISalesInfo.IsReviewed;
                strReviewedUser = objIMEISalesInfo.ReviewedUser;
                intMainGroupID = Convert.ToInt32(dr["MainGroupID"].ToString());
                dteReviewedDate = objIMEISalesInfo.ReviewedDate == null ? DateTime.Now : objIMEISalesInfo.ReviewedDate.Value;
                bolIsReview = chkIsReviewed.Checked;
                dtEndWarantyDate = dteEndWarantyDate.Value;
                grdProduct.DataSource = objIMEISalesInfo.IMEISalesInfo_ProSpec.ToList();
                //this.LoadData();
                //this.FormatGrid(false);            
                if (objIMEISalesInfo.IMEISalesInfo_ImagesList != null)
                {
                    objProduct_ImagesList = objIMEISalesInfo.IMEISalesInfo_ImagesList.OrderBy(img => img.OrderIndex).ToList();
                    grdProductImages.DataSource = objProduct_ImagesList;
                }
                grdProductImages.RefreshDataSource();
                chkIsReviewed.Enabled = ((!chkIsReviewed.Checked && bolPermission_Review) || (chkIsReviewed.Checked && bolPermission_UnReview));
                EnableReview(!chkIsReviewed.Checked && bolPermission_Edit);                
                return true;
            }
            catch (Exception objExc)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin bán hàng của IMEI");
                SystemErrorWS.Insert("Lỗi nạp thông tin bán hàng của IMEI", objExc.ToString(), this.Name + " -> EditData", Globals.ModuleName);
                return false;
            }
        }

        private void FormatGrid(bool _IsNew)
        {
            try
            {
                DataTable dtbTemp = new DataTable();
                if (!_IsNew)
                {
                    DataTable dtbDataProSpec = null;
                    dtbDataProSpec = this.objPLCIMEISalesInfo.SearchDataProSpec(new object[] { "@ProductID", this.txtProductID.Text.Trim(), "@IMEI", this.txtIMEI.Text.Trim() });
                    dtbTemp = dtbDataProSpec.DefaultView.ToTable(true, new string[] { "PRODUCTSPECID", "PRODUCTSPECNAME", "PRODUCTSPECSTATUSID", "PRODUCTSPECSTATUSNAME", "NOTE" });
                    DataTable dtbTempSpec = this.dtbDataSpec.Copy();
                    dtbTempSpec.Columns.Add("PRODUCTSPECSTATUSID", typeof(Int64)).DefaultValue = 0;
                    dtbTempSpec.Columns.Add("PRODUCTSPECSTATUSNAME", typeof(string)).DefaultValue = string.Empty;
                    dtbTempSpec.Columns.Add("NOTE", typeof(string)).DefaultValue = string.Empty;
                    foreach (DataRow r in dtbTempSpec.Rows)
                    {
                        DataRow[] dr = dtbTemp.Select("PRODUCTSPECID=" + r["PRODUCTSPECID"].ToString());
                        if (dr != null && dr.Length > 0)
                            r.Delete();
                    }
                    dtbTempSpec.AcceptChanges();
                    dtbTemp.Merge(dtbTempSpec);
                    dtbTemp.DefaultView.Sort = "PRODUCTSPECNAME";
                    this.grdProduct.DataSource = dtbTemp;
                }
                else
                {
                    dtbTemp = this.dtbDataSpec.Copy();
                    dtbTemp.Columns.Add("PRODUCTSPECSTATUSID", typeof(Int64)).DefaultValue = 0;
                    dtbTemp.Columns.Add("PRODUCTSPECSTATUSNAME", typeof(string)).DefaultValue = string.Empty;
                    dtbTemp.Columns.Add("NOTE", typeof(string)).DefaultValue = string.Empty;
                    dtbTemp.DefaultView.Sort = "PRODUCTSPECNAME";
                    this.grdProduct.DataSource = dtbTemp;
                }

                DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboProductSpec = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
                cboProductSpec.NullText = "--Chọn thông tin--";
                cboProductSpec.NullText = "";
                cboProductSpec.DisplayMember = "PRODUCTSPECNAME";
                cboProductSpec.ValueMember = "PRODUCTSPECID";
                cboProductSpec.DataSource = dtbTemp;
                cboProductSpec.PopupWidth = 400;
                DevExpress.XtraEditors.Controls.LookUpColumnInfo colSpec = new DevExpress.XtraEditors.Controls.LookUpColumnInfo();
                colSpec.Caption = "Mã thông tin SP";
                colSpec.FieldName = "PRODUCTSPECID";
                cboProductSpec.Columns.Add(colSpec);
                DevExpress.XtraEditors.Controls.LookUpColumnInfo colSpec1 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo();
                colSpec1.Caption = "Tên thông tin SP";
                colSpec1.FieldName = "PRODUCTSPECNAME";
                cboProductSpec.Columns.Add(colSpec1);
                this.grdViewProduct.Columns["PRODUCTSPECID"].ColumnEdit = cboProductSpec;
                this.grdViewProduct.Columns["PRODUCTSPECID"].OptionsColumn.AllowEdit = false;

                try
                {
                    DataRow nr = this.dtbDataSpecStatus.NewRow();
                    nr["PRODUCTSPECSTATUSID"] = -1;
                    nr["PRODUCTSPECSTATUSNAME"] = "--Chọn trạng thái--";
                    this.dtbDataSpecStatus.Rows.Add(nr);
                }
                catch { }
                this.dtbDataSpecStatus.DefaultView.Sort = "PRODUCTSPECSTATUSID";
                DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboProduct = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
                cboProduct.NullText = "--Chọn trạng thái--";
                cboProduct.DisplayMember = "PRODUCTSPECSTATUSNAME";
                cboProduct.ValueMember = "PRODUCTSPECSTATUSID";
                cboProduct.DataSource = this.dtbDataSpecStatus;
                cboProduct.PopupWidth = 400;
                DevExpress.XtraEditors.Controls.LookUpColumnInfo col = new DevExpress.XtraEditors.Controls.LookUpColumnInfo();
                col.Caption = "Mã trạng thái";
                col.FieldName = "PRODUCTSPECSTATUSID";
                cboProduct.Columns.Add(col);
                DevExpress.XtraEditors.Controls.LookUpColumnInfo col1 = new DevExpress.XtraEditors.Controls.LookUpColumnInfo();
                col1.Caption = "Tên trạng thái";
                col1.FieldName = "PRODUCTSPECSTATUSNAME";
                cboProduct.Columns.Add(col1);
                cboProduct.Enter += new EventHandler(cboProduct_Enter);                
                this.grdViewProduct.Columns["PRODUCTSPECSTATUSID"].ColumnEdit = cboProduct;
                this.grdViewProduct.Columns["PRODUCTSPECSTATUSID"].OptionsColumn.AllowEdit = true;

                this.grdViewProduct.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                this.grdViewProduct.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                this.grdViewProduct.OptionsView.ShowAutoFilterRow = false;
                this.grdViewProduct.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                this.grdViewProduct.OptionsView.ShowFooter = false;
                this.grdViewProduct.OptionsView.ShowGroupPanel = false;
            }
            catch(Exception objExc)
            {
                MessageBoxObject.ShowWarningMessage(this,"Lỗi định dạng lưới thông tin bán hàng của IMEI !");
                SystemErrorWS.Insert("Lỗi định dạng lưới thông tin bán hàng của IMEI !", objExc.ToString(), this.Name + " --> FormatGrid", DUIInventory_Globals.ModuleName);
            }
        }
        /// <summary>
        /// Chỉ mới tạo 1 số control, ai có nhu cầu thêm thì vào đây viết thêm
        /// </summary>
        /// <param name="_ctrl">TextBox, RichTextBox, TextEdit, NumericUpDown</param>
        /// <param name="_value">true, false</param>
        private void SetColorTextReadOnly(Control _ctrl, bool _value)
        {
            if (_ctrl.GetType().ToString().Trim() == "System.Windows.Forms.TextBox")
            {
                TextBox text = (TextBox)_ctrl;
                text.ReadOnly = _value;
                if (_value) text.BackColor = SystemColors.Info;
                else text.BackColor = SystemColors.Window;
            }
            else if (_ctrl.GetType().ToString().Trim() == "System.Windows.Forms.RichTextBox")
            {
                RichTextBox text = (RichTextBox)_ctrl;
                text.ReadOnly = _value;
                if (_value) text.BackColor = SystemColors.Info;
                else text.BackColor = SystemColors.Window;
            }
            else if (_ctrl.GetType().ToString().Trim() == "DevExpress.XtraEditors.TextEdit")
            {
                DevExpress.XtraEditors.TextEdit text = (DevExpress.XtraEditors.TextEdit)_ctrl;
                text.Properties.ReadOnly = _value;
                if (_value) text.BackColor = SystemColors.Info;
                else text.BackColor = SystemColors.Window;
            }
            else if (_ctrl.GetType().ToString().Trim() == "System.Windows.Forms.NumericUpDown")
            {
                NumericUpDown num = (NumericUpDown)_ctrl;
                num.ReadOnly = _value;
                if (_value) num.BackColor = SystemColors.Info;
                else num.BackColor = SystemColors.Window;
            }
        }
        #endregion
        private List<PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_Images> objProduct_ImagesList = new List<PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_Images>();
        private List<PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_Images> objProduct_ImagesList_Del = new List<PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_Images>();

        /// <summary>
        /// Upload File
        /// </summary>
        /// <param name="strFileID"></param>
        /// <param name="strUrlLocal"></param>
        /// <returns>đường dẫn trên server</returns>
        private string SaveAttachment(ref string strFileID, string strUrlLocal)
        {
            //Tạo trạng thái chờ trong khi đính kèm tập tin
            string strURL = string.Empty;
            this.Refresh();
            try
            {
                if (strUrlLocal != string.Empty)
                {
                    System.IO.FileInfo objFileInfo = new FileInfo(strUrlLocal);
                    if (objFileInfo != null)
                    {
                        ERP.FMS.DUI.DUIFileManager.UploadFile(this, "FMSAplication_ProERP_ProductImage", strUrlLocal, ref strFileID, ref strURL);
                        if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                        {
                            Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                        }
                    }
                }
            }
            catch (Exception objEx)
            {
                MessageBox.Show("Lỗi đính kèm tập tin ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                SystemErrorWS.Insert("Lỗi đính kèm tập tin ", objEx, DUIInventory_Globals.ModuleName);
                return string.Empty;
            }
            //Bỏ trạng thái chờ sau khi đính kèm tập tin			

            return strURL;
        }
        private void btnAddNewImage_Click(object sender, EventArgs e)
        {
            if (txtProductID.Text.Trim() == string.Empty || txtIMEI.Text.Trim() == string.Empty)
            {
                MessageBox.Show(this, "Vui lòng tạo mã sản phẩm và IMEI trước khi thêm hình ảnh!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            frmProductImage frm = new frmProductImage();
            frm.ProductID = txtProductID.Text.Trim();
            frm.ShowDialog();
            if (frm.Product_Images != null && !string.IsNullOrEmpty(frm.Product_Images.ImageName))
            {
                frm.Product_Images.OrderIndex = objProduct_ImagesList.Count + 1;
                if (frm.Product_Images.IsDefault)
                {
                    foreach (PLC.IMEISales.WSIMEISalesInfo.IMEISalesInfo_Images img in objProduct_ImagesList.Where(img => img.IsDefault = true))
                    {
                        img.IsDefault = false;
                    }
                }
                frm.Product_Images.CreatedUser = SystemConfig.objSessionUser.UserName;
                frm.Product_Images.ProductID = txtProductID.Text.Trim();
                frm.Product_Images.IMEI = txtIMEI.Text.Trim();
                objProduct_ImagesList.Add(frm.Product_Images);
            }
            grdProductImages.RefreshDataSource();
        }

        private void btnEditImage_Click(object sender, EventArgs e)
        {
             if (grvImages.FocusedRowHandle < 0)
                return;
            IMEISalesInfo_Images obj = (IMEISalesInfo_Images)grvImages.GetRow(grvImages.FocusedRowHandle);
            frmProductImage frm = new frmProductImage();
            frm.Product_Images = obj;
            frm.ProductID = txtProductID.Text.Trim();
            frm.ShowDialog();
            if (frm.Product_Images.IsDefault)
            {
                foreach (IMEISalesInfo_Images img in objProduct_ImagesList.Where(img => img.IsDefault = true))
                {
                    img.IsDefault = false;
                }
                frm.Product_Images.IsDefault = true;
            }
            frm.Product_Images.UpdatedUser = SystemConfig.objSessionUser.UserName;
            grdProductImages.RefreshDataSource();
        }

        private void btnDelImage_Click(object sender, EventArgs e)
        {
            if (grvImages.FocusedRowHandle < 0)
                return;
            IMEISalesInfo_Images obj = (IMEISalesInfo_Images)grvImages.GetRow(grvImages.FocusedRowHandle);
            if (obj.IsSystem)
            {
                MessageBox.Show(this, "Không được xóa hình sản phẩm có thuộc tính hệ thống?", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Question);
                return;
            }

            if (MessageBox.Show(this, "Bạn chắc chắn muốn xóa hình ảnh sản phẩm này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            if (obj != null)
            {
                for (int i = obj.OrderIndex; i < objProduct_ImagesList.Count; i++)
                {
                    objProduct_ImagesList[i].OrderIndex = i;
                }
                objProduct_ImagesList.Remove(obj);

            }
            if (obj.ImageID != string.Empty)
                objProduct_ImagesList_Del.Add(obj);
            grdProductImages.RefreshDataSource();
        }

        private void btnMoveUp_Click(object sender, EventArgs e)
        {
                        grvImages.ClearColumnsFilter();
            if (grvImages.FocusedRowHandle < 0 || grvImages.FocusedRowHandle == 0)
                return;
            grvImages.ClearColumnsFilter();
            var ItemOld = (IMEISalesInfo_Images)grvImages.GetRow(grvImages.FocusedRowHandle);
            var ItemNew = objProduct_ImagesList[grvImages.FocusedRowHandle - 1];
            int intCurrentIndex = grvImages.FocusedRowHandle;
            objProduct_ImagesList.RemoveAt(intCurrentIndex);
            objProduct_ImagesList.RemoveAt(intCurrentIndex - 1);

            ItemNew.OrderIndex = ItemNew.OrderIndex + 1;
            ItemOld.OrderIndex = ItemOld.OrderIndex - 1;

            objProduct_ImagesList.Insert(intCurrentIndex - 1, ItemOld);
            objProduct_ImagesList.Insert(intCurrentIndex, ItemNew);
            grvImages.FocusedRowHandle = grvImages.FocusedRowHandle - 1;
            grdProductImages.RefreshDataSource();
        }

        private void btnMoveDown_Click(object sender, EventArgs e)
        {
            grvImages.ClearColumnsFilter();
            if (grvImages.FocusedRowHandle < 0 || grvImages.FocusedRowHandle == grvImages.RowCount - 1)
                return;
            var ItemOld = (IMEISalesInfo_Images)grvImages.GetRow(grvImages.FocusedRowHandle);
            var ItemNew = objProduct_ImagesList[grvImages.FocusedRowHandle + 1];
            int intCurrentIndex = grvImages.FocusedRowHandle;
            objProduct_ImagesList.RemoveAt(intCurrentIndex + 1);
            objProduct_ImagesList.RemoveAt(intCurrentIndex);

            ItemNew.OrderIndex = ItemNew.OrderIndex - 1;
            ItemOld.OrderIndex = ItemOld.OrderIndex + 1;

            objProduct_ImagesList.Insert(intCurrentIndex, ItemNew);
            objProduct_ImagesList.Insert(intCurrentIndex + 1, ItemOld);
            grvImages.FocusedRowHandle = grvImages.FocusedRowHandle + 1;
            grdProductImages.RefreshDataSource();
        }

        private void mnuAddImage_Click(object sender, EventArgs e)
        {
            if (btnAddNewImage.Enabled)
                btnAddNewImage_Click(null, null);
        }

        private void mnuEditImage_Click(object sender, EventArgs e)
        {
            if (btnEditImage.Enabled)
                btnEditImage_Click(null, null);
        }

        private void mnuDelImage_Click(object sender, EventArgs e)
        {
            if (btnDelImage.Enabled)
                btnDelImage_Click(null, null);
        }

        private void EnableReview(bool bolIsEnale)
        {
            txtIMEI.ReadOnly = !bolIsEnale;
            txtNote.ReadOnly = !bolIsEnale;
            txtPrintVATContent.ReadOnly = !bolIsEnale;
            txtProductID.ReadOnly = !bolIsEnale;
            txtProductName.ReadOnly = !bolIsEnale;
            dteEndWarantyDate.Enabled = bolIsEnale;
            chkIsBrandNewWarranty.Enabled = bolIsEnale;

            btnAddNewImage.Enabled = bolIsEnale;
            btnDelImage.Enabled = bolIsEnale;
            btnEditImage.Enabled = bolIsEnale;
            btnMoveDown.Enabled = bolIsEnale;
            btnMoveUp.Enabled = bolIsEnale;
            foreach (DevExpress.XtraGrid.Columns.GridColumn item in grdViewProduct.Columns)
            {
                item.OptionsColumn.AllowEdit = bolIsEnale;
            }
            //foreach (DevExpress.XtraGrid.Columns.GridColumn item in grvImages.Columns)
            //{
            //    item.OptionsColumn.AllowEdit = bolIsEnale;
            //}
            chkIsComfirm.Enabled = bolIsEnale;
            mnuAction.Enabled = bolIsEnale;
            mnuIMAGE.Enabled = bolIsEnale;
        }
      

    }
}
