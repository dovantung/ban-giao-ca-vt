﻿namespace ERP.Inventory.DUI.IMEISales
{
    partial class frmIMEISalesInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIMEISalesInfo));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.cboProduct = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdViewProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cboProductSpec = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdProduct = new DevExpress.XtraGrid.GridControl();
            this.dteEndWarantyDate = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.txtIMEI = new System.Windows.Forms.TextBox();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.cmdClose = new System.Windows.Forms.Button();
            this.cmdUpdate = new System.Windows.Forms.Button();
            this.chkIsBrandNewWarranty = new System.Windows.Forms.CheckBox();
            this.grpPrice = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSelectProduct = new System.Windows.Forms.Button();
            this.txtProductID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grpInfo = new System.Windows.Forms.GroupBox();
            this.chkIsReviewed = new System.Windows.Forms.CheckBox();
            this.txtPrintVATContent = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkIsComfirm = new System.Windows.Forms.CheckBox();
            this.tabCtrlIMEI = new System.Windows.Forms.TabControl();
            this.tabIMEIInfo = new System.Windows.Forms.TabPage();
            this.tabIMEIImageInfo = new System.Windows.Forms.TabPage();
            this.grdProductImages = new DevExpress.XtraGrid.GridControl();
            this.mnuIMAGE = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuAddImage = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEditImage = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDelImage = new System.Windows.Forms.ToolStripMenuItem();
            this.grvImages = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnMoveDown = new System.Windows.Forms.Button();
            this.btnEditImage = new System.Windows.Forms.Button();
            this.btnMoveUp = new System.Windows.Forms.Button();
            this.btnDelImage = new System.Windows.Forms.Button();
            this.btnAddNewImage = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboProduct)).BeginInit();
            this.mnuAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboProductSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdProduct)).BeginInit();
            this.grpPrice.SuspendLayout();
            this.grpInfo.SuspendLayout();
            this.tabCtrlIMEI.SuspendLayout();
            this.tabIMEIInfo.SuspendLayout();
            this.tabIMEIImageInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProductImages)).BeginInit();
            this.mnuIMAGE.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            this.SuspendLayout();
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "Mô tả";
            this.gridColumn3.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn3.FieldName = "NOTE";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 2000;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // cboProduct
            // 
            this.cboProduct.AutoHeight = false;
            this.cboProduct.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboProduct.Name = "cboProduct";
            // 
            // mnuAction
            // 
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemDelete});
            this.mnuAction.Name = "contextMenuStrip1";
            this.mnuAction.Size = new System.Drawing.Size(95, 26);
            // 
            // mnuItemDelete
            // 
            this.mnuItemDelete.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemDelete.Image")));
            this.mnuItemDelete.Name = "mnuItemDelete";
            this.mnuItemDelete.Size = new System.Drawing.Size(94, 22);
            this.mnuItemDelete.Text = "Xóa";
            this.mnuItemDelete.Click += new System.EventHandler(this.mnuItemDelete_Click);
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "Trạng thái";
            this.gridColumn2.ColumnEdit = this.cboProduct;
            this.gridColumn2.FieldName = "PRODUCTSPECSTATUSID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn1.Caption = "Thông tin sản phẩm";
            this.gridColumn1.FieldName = "PRODUCTSPECNAME";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            // 
            // grdViewProduct
            // 
            this.grdViewProduct.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grdViewProduct.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewProduct.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProduct.Appearance.Row.Options.UseFont = true;
            this.grdViewProduct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5});
            this.grdViewProduct.GridControl = this.grdProduct;
            this.grdViewProduct.Name = "grdViewProduct";
            this.grdViewProduct.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn4.Caption = "Thông tin sản phẩm";
            this.gridColumn4.ColumnEdit = this.cboProductSpec;
            this.gridColumn4.FieldName = "PRODUCTSPECID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            // 
            // cboProductSpec
            // 
            this.cboProductSpec.AutoHeight = false;
            this.cboProductSpec.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboProductSpec.Name = "cboProductSpec";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Trạng thái sản phẩm";
            this.gridColumn5.FieldName = "PRODUCTSPECSTATUSNAME";
            this.gridColumn5.Name = "gridColumn5";
            // 
            // grdProduct
            // 
            this.grdProduct.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdProduct.Location = new System.Drawing.Point(7, 46);
            this.grdProduct.MainView = this.grdViewProduct;
            this.grdProduct.Name = "grdProduct";
            this.grdProduct.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.cboProduct,
            this.cboProductSpec,
            this.repositoryItemTextEdit1});
            this.grdProduct.Size = new System.Drawing.Size(947, 246);
            this.grdProduct.TabIndex = 100;
            this.grdProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewProduct});
            // 
            // dteEndWarantyDate
            // 
            this.dteEndWarantyDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dteEndWarantyDate.CustomFormat = "dd/MM/yyyy";
            this.dteEndWarantyDate.Enabled = false;
            this.dteEndWarantyDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteEndWarantyDate.Location = new System.Drawing.Point(435, 118);
            this.dteEndWarantyDate.Name = "dteEndWarantyDate";
            this.dteEndWarantyDate.Size = new System.Drawing.Size(133, 22);
            this.dteEndWarantyDate.TabIndex = 7;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(321, 120);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(112, 16);
            this.label18.TabIndex = 6;
            this.label18.Text = "Ngày hết hạn BH:";
            // 
            // txtIMEI
            // 
            this.txtIMEI.BackColor = System.Drawing.Color.White;
            this.txtIMEI.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIMEI.Location = new System.Drawing.Point(622, 19);
            this.txtIMEI.MaxLength = 50;
            this.txtIMEI.Name = "txtIMEI";
            this.txtIMEI.Size = new System.Drawing.Size(158, 22);
            this.txtIMEI.TabIndex = 3;
            // 
            // txtProductName
            // 
            this.txtProductName.BackColor = System.Drawing.SystemColors.Info;
            this.txtProductName.Location = new System.Drawing.Point(267, 19);
            this.txtProductName.MaxLength = 20;
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.ReadOnly = true;
            this.txtProductName.Size = new System.Drawing.Size(319, 22);
            this.txtProductName.TabIndex = 2;
            // 
            // cmdClose
            // 
            this.cmdClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdClose.Image = ((System.Drawing.Image)(resources.GetObject("cmdClose.Image")));
            this.cmdClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdClose.Location = new System.Drawing.Point(887, 524);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Size = new System.Drawing.Size(88, 26);
            this.cmdClose.TabIndex = 3;
            this.cmdClose.Text = "     Đóng lại";
            this.cmdClose.UseVisualStyleBackColor = true;
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // cmdUpdate
            // 
            this.cmdUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdUpdate.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdate.Image")));
            this.cmdUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdUpdate.Location = new System.Drawing.Point(798, 524);
            this.cmdUpdate.Name = "cmdUpdate";
            this.cmdUpdate.Size = new System.Drawing.Size(88, 26);
            this.cmdUpdate.TabIndex = 2;
            this.cmdUpdate.Text = "      Cập nhật";
            this.cmdUpdate.Click += new System.EventHandler(this.cmdUpdate_Click);
            // 
            // chkIsBrandNewWarranty
            // 
            this.chkIsBrandNewWarranty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkIsBrandNewWarranty.AutoSize = true;
            this.chkIsBrandNewWarranty.Location = new System.Drawing.Point(164, 121);
            this.chkIsBrandNewWarranty.Name = "chkIsBrandNewWarranty";
            this.chkIsBrandNewWarranty.Size = new System.Drawing.Size(130, 17);
            this.chkIsBrandNewWarranty.TabIndex = 5;
            this.chkIsBrandNewWarranty.Text = "Bảo hành chính hãng";
            this.chkIsBrandNewWarranty.UseVisualStyleBackColor = true;
            this.chkIsBrandNewWarranty.CheckedChanged += new System.EventHandler(this.chkIsBrandNewWarranty_CheckedChanged);
            // 
            // grpPrice
            // 
            this.grpPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpPrice.Controls.Add(this.txtIMEI);
            this.grpPrice.Controls.Add(this.txtProductName);
            this.grpPrice.Controls.Add(this.grdProduct);
            this.grpPrice.Controls.Add(this.label3);
            this.grpPrice.Controls.Add(this.btnSelectProduct);
            this.grpPrice.Controls.Add(this.txtProductID);
            this.grpPrice.Controls.Add(this.label2);
            this.grpPrice.Location = new System.Drawing.Point(3, 6);
            this.grpPrice.Name = "grpPrice";
            this.grpPrice.Size = new System.Drawing.Size(960, 298);
            this.grpPrice.TabIndex = 0;
            this.grpPrice.TabStop = false;
            this.grpPrice.Text = "Thông tin sản phẩm";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(589, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 16);
            this.label3.TabIndex = 71;
            this.label3.Text = "IMEI:";
            // 
            // btnSelectProduct
            // 
            this.btnSelectProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectProduct.Location = new System.Drawing.Point(237, 18);
            this.btnSelectProduct.Name = "btnSelectProduct";
            this.btnSelectProduct.Size = new System.Drawing.Size(28, 23);
            this.btnSelectProduct.TabIndex = 1;
            this.btnSelectProduct.Text = "...";
            this.btnSelectProduct.UseVisualStyleBackColor = true;
            // 
            // txtProductID
            // 
            this.txtProductID.BackColor = System.Drawing.SystemColors.Info;
            this.txtProductID.ContextMenuStrip = this.mnuAction;
            this.txtProductID.Location = new System.Drawing.Point(73, 19);
            this.txtProductID.MaxLength = 20;
            this.txtProductID.Name = "txtProductID";
            this.txtProductID.ReadOnly = true;
            this.txtProductID.Size = new System.Drawing.Size(162, 22);
            this.txtProductID.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 16);
            this.label2.TabIndex = 67;
            this.label2.Text = "Sản phẩm:";
            // 
            // grpInfo
            // 
            this.grpInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpInfo.Controls.Add(this.chkIsReviewed);
            this.grpInfo.Controls.Add(this.txtPrintVATContent);
            this.grpInfo.Controls.Add(this.label5);
            this.grpInfo.Controls.Add(this.txtNote);
            this.grpInfo.Controls.Add(this.label4);
            this.grpInfo.Controls.Add(this.chkIsComfirm);
            this.grpInfo.Controls.Add(this.chkIsBrandNewWarranty);
            this.grpInfo.Controls.Add(this.dteEndWarantyDate);
            this.grpInfo.Controls.Add(this.label18);
            this.grpInfo.Location = new System.Drawing.Point(6, 347);
            this.grpInfo.Name = "grpInfo";
            this.grpInfo.Size = new System.Drawing.Size(973, 171);
            this.grpInfo.TabIndex = 2;
            this.grpInfo.TabStop = false;
            this.grpInfo.Text = "Thông tin chung";
            // 
            // chkIsReviewed
            // 
            this.chkIsReviewed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkIsReviewed.AutoSize = true;
            this.chkIsReviewed.Location = new System.Drawing.Point(324, 147);
            this.chkIsReviewed.Name = "chkIsReviewed";
            this.chkIsReviewed.Size = new System.Drawing.Size(54, 17);
            this.chkIsReviewed.TabIndex = 8;
            this.chkIsReviewed.Text = "Duyệt";
            this.chkIsReviewed.UseVisualStyleBackColor = true;
            // 
            // txtPrintVATContent
            // 
            this.txtPrintVATContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPrintVATContent.BackColor = System.Drawing.SystemColors.Info;
            this.txtPrintVATContent.Location = new System.Drawing.Point(164, 22);
            this.txtPrintVATContent.MaxLength = 2000;
            this.txtPrintVATContent.Multiline = true;
            this.txtPrintVATContent.Name = "txtPrintVATContent";
            this.txtPrintVATContent.ReadOnly = true;
            this.txtPrintVATContent.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPrintVATContent.Size = new System.Drawing.Size(799, 40);
            this.txtPrintVATContent.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(160, 16);
            this.label5.TabIndex = 1;
            this.label5.Text = "Nội dung in hoá đơn VAT:";
            // 
            // txtNote
            // 
            this.txtNote.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNote.BackColor = System.Drawing.Color.White;
            this.txtNote.Location = new System.Drawing.Point(164, 66);
            this.txtNote.MaxLength = 2000;
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtNote.Size = new System.Drawing.Size(799, 40);
            this.txtNote.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(95, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ghi chú:";
            // 
            // chkIsComfirm
            // 
            this.chkIsComfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkIsComfirm.AutoSize = true;
            this.chkIsComfirm.Location = new System.Drawing.Point(164, 144);
            this.chkIsComfirm.Name = "chkIsComfirm";
            this.chkIsComfirm.Size = new System.Drawing.Size(72, 17);
            this.chkIsComfirm.TabIndex = 8;
            this.chkIsComfirm.Text = "Xác nhận";
            this.chkIsComfirm.UseVisualStyleBackColor = true;
            // 
            // tabCtrlIMEI
            // 
            this.tabCtrlIMEI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabCtrlIMEI.Controls.Add(this.tabIMEIInfo);
            this.tabCtrlIMEI.Controls.Add(this.tabIMEIImageInfo);
            this.tabCtrlIMEI.Location = new System.Drawing.Point(2, 2);
            this.tabCtrlIMEI.Name = "tabCtrlIMEI";
            this.tabCtrlIMEI.SelectedIndex = 0;
            this.tabCtrlIMEI.Size = new System.Drawing.Size(977, 339);
            this.tabCtrlIMEI.TabIndex = 4;
            // 
            // tabIMEIInfo
            // 
            this.tabIMEIInfo.Controls.Add(this.grpPrice);
            this.tabIMEIInfo.Location = new System.Drawing.Point(4, 25);
            this.tabIMEIInfo.Name = "tabIMEIInfo";
            this.tabIMEIInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabIMEIInfo.Size = new System.Drawing.Size(969, 310);
            this.tabIMEIInfo.TabIndex = 0;
            this.tabIMEIInfo.Text = "Thông tin sản phẩm";
            this.tabIMEIInfo.UseVisualStyleBackColor = true;
            // 
            // tabIMEIImageInfo
            // 
            this.tabIMEIImageInfo.Controls.Add(this.grdProductImages);
            this.tabIMEIImageInfo.Controls.Add(this.btnMoveDown);
            this.tabIMEIImageInfo.Controls.Add(this.btnEditImage);
            this.tabIMEIImageInfo.Controls.Add(this.btnMoveUp);
            this.tabIMEIImageInfo.Controls.Add(this.btnDelImage);
            this.tabIMEIImageInfo.Controls.Add(this.btnAddNewImage);
            this.tabIMEIImageInfo.Location = new System.Drawing.Point(4, 25);
            this.tabIMEIImageInfo.Name = "tabIMEIImageInfo";
            this.tabIMEIImageInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabIMEIImageInfo.Size = new System.Drawing.Size(969, 310);
            this.tabIMEIImageInfo.TabIndex = 1;
            this.tabIMEIImageInfo.Text = "Thông tin ảnh sản phẩm";
            this.tabIMEIImageInfo.UseVisualStyleBackColor = true;
            // 
            // grdProductImages
            // 
            this.grdProductImages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdProductImages.ContextMenuStrip = this.mnuIMAGE;
            this.grdProductImages.Location = new System.Drawing.Point(6, 37);
            this.grdProductImages.MainView = this.grvImages;
            this.grdProductImages.Name = "grdProductImages";
            this.grdProductImages.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.repositoryItemCheckEdit2});
            this.grdProductImages.Size = new System.Drawing.Size(924, 267);
            this.grdProductImages.TabIndex = 29;
            this.grdProductImages.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvImages});
            this.grdProductImages.DoubleClick += new System.EventHandler(this.btnEditImage_Click);
            // 
            // mnuIMAGE
            // 
            this.mnuIMAGE.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAddImage,
            this.mnuEditImage,
            this.mnuDelImage});
            this.mnuIMAGE.Name = "contextMenuStrip1";
            this.mnuIMAGE.Size = new System.Drawing.Size(121, 70);
            // 
            // mnuAddImage
            // 
            this.mnuAddImage.Image = ((System.Drawing.Image)(resources.GetObject("mnuAddImage.Image")));
            this.mnuAddImage.Name = "mnuAddImage";
            this.mnuAddImage.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.mnuAddImage.ShowShortcutKeys = false;
            this.mnuAddImage.Size = new System.Drawing.Size(120, 22);
            this.mnuAddImage.Text = "Thêm";
            this.mnuAddImage.Click += new System.EventHandler(this.mnuAddImage_Click);
            // 
            // mnuEditImage
            // 
            this.mnuEditImage.Image = ((System.Drawing.Image)(resources.GetObject("mnuEditImage.Image")));
            this.mnuEditImage.Name = "mnuEditImage";
            this.mnuEditImage.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.mnuEditImage.ShowShortcutKeys = false;
            this.mnuEditImage.Size = new System.Drawing.Size(120, 22);
            this.mnuEditImage.Text = "Chỉnh sửa";
            this.mnuEditImage.Click += new System.EventHandler(this.mnuEditImage_Click);
            // 
            // mnuDelImage
            // 
            this.mnuDelImage.Image = ((System.Drawing.Image)(resources.GetObject("mnuDelImage.Image")));
            this.mnuDelImage.Name = "mnuDelImage";
            this.mnuDelImage.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.mnuDelImage.ShowShortcutKeys = false;
            this.mnuDelImage.Size = new System.Drawing.Size(120, 22);
            this.mnuDelImage.Text = "Xóa";
            this.mnuDelImage.Click += new System.EventHandler(this.mnuDelImage_Click);
            // 
            // grvImages
            // 
            this.grvImages.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grvImages.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grvImages.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grvImages.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Transparent;
            this.grvImages.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvImages.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grvImages.Appearance.FocusedRow.Options.UseFont = true;
            this.grvImages.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvImages.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvImages.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvImages.Appearance.Preview.Options.UseFont = true;
            this.grvImages.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvImages.Appearance.Row.Options.UseFont = true;
            this.grvImages.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18});
            this.grvImages.GridControl = this.grdProductImages;
            this.grvImages.Name = "grvImages";
            this.grvImages.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grvImages.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grvImages.OptionsNavigation.UseTabKey = false;
            this.grvImages.OptionsView.ShowAutoFilterRow = true;
            this.grvImages.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvImages.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Tên hình";
            this.gridColumn10.FieldName = "ImageName";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            this.gridColumn10.Width = 131;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Mô tả";
            this.gridColumn11.FieldName = "Description";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            this.gridColumn11.Width = 253;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "Hình lớn";
            this.gridColumn12.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn12.FieldName = "LargesizeImage";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn12.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            this.gridColumn12.Width = 108;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "Hình vừa";
            this.gridColumn13.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn13.FieldName = "MediumSizeImage";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn13.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 3;
            this.gridColumn13.Width = 107;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "Hình nhỏ";
            this.gridColumn14.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn14.FieldName = "SmallSizeImage";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn14.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 4;
            this.gridColumn14.Width = 107;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.Caption = "Mặc định";
            this.gridColumn15.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn15.FieldName = "IsDefault";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 5;
            this.gridColumn15.Width = 72;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.Caption = "Kích hoạt";
            this.gridColumn16.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn16.FieldName = "IsActive";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 6;
            this.gridColumn16.Width = 71;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.Caption = "Hệ thống";
            this.gridColumn17.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn17.FieldName = "IsSystem";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 7;
            this.gridColumn17.Width = 72;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.Caption = "Thứ tự";
            this.gridColumn18.FieldName = "OrderIndex";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 8;
            this.gridColumn18.Width = 85;
            // 
            // btnMoveDown
            // 
            this.btnMoveDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveDown.Image")));
            this.btnMoveDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMoveDown.Location = new System.Drawing.Point(933, 187);
            this.btnMoveDown.Name = "btnMoveDown";
            this.btnMoveDown.Size = new System.Drawing.Size(25, 33);
            this.btnMoveDown.TabIndex = 28;
            this.btnMoveDown.Text = "     Thêm";
            this.btnMoveDown.UseVisualStyleBackColor = true;
            this.btnMoveDown.Click += new System.EventHandler(this.btnMoveDown_Click);
            // 
            // btnEditImage
            // 
            this.btnEditImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditImage.Image = ((System.Drawing.Image)(resources.GetObject("btnEditImage.Image")));
            this.btnEditImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditImage.Location = new System.Drawing.Point(722, 5);
            this.btnEditImage.Name = "btnEditImage";
            this.btnEditImage.Size = new System.Drawing.Size(102, 25);
            this.btnEditImage.TabIndex = 26;
            this.btnEditImage.Text = "     Chỉnh sửa";
            this.btnEditImage.UseVisualStyleBackColor = true;
            this.btnEditImage.Click += new System.EventHandler(this.btnEditImage_Click);
            // 
            // btnMoveUp
            // 
            this.btnMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveUp.Image")));
            this.btnMoveUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMoveUp.Location = new System.Drawing.Point(933, 148);
            this.btnMoveUp.Name = "btnMoveUp";
            this.btnMoveUp.Size = new System.Drawing.Size(25, 33);
            this.btnMoveUp.TabIndex = 27;
            this.btnMoveUp.Text = "     Thêm";
            this.btnMoveUp.UseVisualStyleBackColor = true;
            this.btnMoveUp.Click += new System.EventHandler(this.btnMoveUp_Click);
            // 
            // btnDelImage
            // 
            this.btnDelImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelImage.Image = ((System.Drawing.Image)(resources.GetObject("btnDelImage.Image")));
            this.btnDelImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelImage.Location = new System.Drawing.Point(830, 5);
            this.btnDelImage.Name = "btnDelImage";
            this.btnDelImage.Size = new System.Drawing.Size(102, 25);
            this.btnDelImage.TabIndex = 25;
            this.btnDelImage.Text = "     Xóa";
            this.btnDelImage.UseVisualStyleBackColor = true;
            this.btnDelImage.Click += new System.EventHandler(this.btnDelImage_Click);
            // 
            // btnAddNewImage
            // 
            this.btnAddNewImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNewImage.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNewImage.Image")));
            this.btnAddNewImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddNewImage.Location = new System.Drawing.Point(619, 5);
            this.btnAddNewImage.Name = "btnAddNewImage";
            this.btnAddNewImage.Size = new System.Drawing.Size(98, 25);
            this.btnAddNewImage.TabIndex = 24;
            this.btnAddNewImage.Text = "     Thêm";
            this.btnAddNewImage.UseVisualStyleBackColor = true;
            this.btnAddNewImage.Click += new System.EventHandler(this.btnAddNewImage_Click);
            // 
            // frmIMEISalesInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 557);
            this.Controls.Add(this.tabCtrlIMEI);
            this.Controls.Add(this.grpInfo);
            this.Controls.Add(this.cmdClose);
            this.Controls.Add(this.cmdUpdate);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmIMEISalesInfo";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông tin bán hàng của IMEI";
            this.Load += new System.EventHandler(this.frmIMEISalesInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboProduct)).EndInit();
            this.mnuAction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboProductSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdProduct)).EndInit();
            this.grpPrice.ResumeLayout(false);
            this.grpPrice.PerformLayout();
            this.grpInfo.ResumeLayout(false);
            this.grpInfo.PerformLayout();
            this.tabCtrlIMEI.ResumeLayout(false);
            this.tabIMEIInfo.ResumeLayout(false);
            this.tabIMEIImageInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProductImages)).EndInit();
            this.mnuIMAGE.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboProduct;
        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDelete;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewProduct;
        private DevExpress.XtraGrid.GridControl grdProduct;
        private System.Windows.Forms.DateTimePicker dteEndWarantyDate;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtIMEI;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.Button cmdClose;
        private System.Windows.Forms.Button cmdUpdate;
        private System.Windows.Forms.CheckBox chkIsBrandNewWarranty;
        private System.Windows.Forms.GroupBox grpPrice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSelectProduct;
        private System.Windows.Forms.TextBox txtProductID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grpInfo;
        private System.Windows.Forms.TextBox txtPrintVATContent;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboProductSpec;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private System.Windows.Forms.TabControl tabCtrlIMEI;
        private System.Windows.Forms.TabPage tabIMEIInfo;
        private System.Windows.Forms.TabPage tabIMEIImageInfo;
        private DevExpress.XtraGrid.GridControl grdProductImages;
        private DevExpress.XtraGrid.Views.Grid.GridView grvImages;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private System.Windows.Forms.Button btnMoveDown;
        private System.Windows.Forms.Button btnEditImage;
        private System.Windows.Forms.Button btnMoveUp;
        private System.Windows.Forms.Button btnDelImage;
        private System.Windows.Forms.Button btnAddNewImage;
        private System.Windows.Forms.ContextMenuStrip mnuIMAGE;
        private System.Windows.Forms.ToolStripMenuItem mnuAddImage;
        private System.Windows.Forms.ToolStripMenuItem mnuEditImage;
        private System.Windows.Forms.ToolStripMenuItem mnuDelImage;
        private System.Windows.Forms.CheckBox chkIsReviewed;
        private System.Windows.Forms.CheckBox chkIsComfirm;

    }
}