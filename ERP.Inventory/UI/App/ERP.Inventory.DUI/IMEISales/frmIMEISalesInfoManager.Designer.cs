﻿namespace ERP.Inventory.DUI.IMEISales
{
    partial class frmIMEISalesInfoManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIMEISalesInfoManager));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemExport = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemReview = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemViewComment = new System.Windows.Forms.ToolStripMenuItem();
            this.cboInputStore = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.txtIMEISearch = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboProductList = new ERP.MasterData.DUI.CustomControl.Product.cboProductList();
            this.cmdSearch = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repChkIsReviewed = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkIsInput = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckedComboBoxEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabList = new System.Windows.Forms.TabPage();
            this.tabDetail = new System.Windows.Forms.TabPage();
            this.grpInfo = new System.Windows.Forms.GroupBox();
            this.txtWebContent = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chkIsReviewed = new System.Windows.Forms.CheckBox();
            this.txtPrintVATContent = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chkIsComfirm = new System.Windows.Forms.CheckBox();
            this.chkIsBrandNewWarranty = new System.Windows.Forms.CheckBox();
            this.dteEndWarantyDate = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.tabCtrlDetail = new System.Windows.Forms.TabControl();
            this.tabDetailInfo = new System.Windows.Forms.TabPage();
            this.grpPrice = new System.Windows.Forms.GroupBox();
            this.grdProduct = new DevExpress.XtraGrid.GridControl();
            this.grvProductSpec = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cboProduct = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cboProductSpec = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.txtIMEI = new System.Windows.Forms.TextBox();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtProductID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSelectProduct = new System.Windows.Forms.Button();
            this.tabIMEIImageInfo = new System.Windows.Forms.TabPage();
            this.grdImages = new DevExpress.XtraGrid.GridControl();
            this.mnuIMAGE = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuAddImage = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEditImage = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDelImage = new System.Windows.Forms.ToolStripMenuItem();
            this.grvImages = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnMoveDown = new System.Windows.Forms.Button();
            this.btnEditImage = new System.Windows.Forms.Button();
            this.btnMoveUp = new System.Windows.Forms.Button();
            this.btnDelImage = new System.Windows.Forms.Button();
            this.btnAddNewImage = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnReview = new System.Windows.Forms.Button();
            this.btnUndo = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.mnuAction.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkIsReviewed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabList.SuspendLayout();
            this.tabDetail.SuspendLayout();
            this.grpInfo.SuspendLayout();
            this.tabCtrlDetail.SuspendLayout();
            this.tabDetailInfo.SuspendLayout();
            this.grpPrice.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvProductSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboProductSpec)).BeginInit();
            this.tabIMEIImageInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdImages)).BeginInit();
            this.mnuIMAGE.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuAction
            // 
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemAdd,
            this.mnuItemDelete,
            this.mnuItemEdit,
            this.mnuItemExport,
            this.mnuItemReview,
            this.mnuItemViewComment});
            this.mnuAction.Name = "contextMenuStrip1";
            this.mnuAction.Size = new System.Drawing.Size(168, 136);
            // 
            // mnuItemAdd
            // 
            this.mnuItemAdd.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemAdd.Image")));
            this.mnuItemAdd.Name = "mnuItemAdd";
            this.mnuItemAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.mnuItemAdd.Size = new System.Drawing.Size(167, 22);
            this.mnuItemAdd.Text = "Thêm";
            this.mnuItemAdd.Click += new System.EventHandler(this.mnuItemAdd_Click);
            // 
            // mnuItemDelete
            // 
            this.mnuItemDelete.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemDelete.Image")));
            this.mnuItemDelete.Name = "mnuItemDelete";
            this.mnuItemDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.mnuItemDelete.Size = new System.Drawing.Size(167, 22);
            this.mnuItemDelete.Text = "Xóa";
            this.mnuItemDelete.Visible = false;
            this.mnuItemDelete.Click += new System.EventHandler(this.mnuItemDelete_Click);
            // 
            // mnuItemEdit
            // 
            this.mnuItemEdit.Enabled = false;
            this.mnuItemEdit.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemEdit.Image")));
            this.mnuItemEdit.Name = "mnuItemEdit";
            this.mnuItemEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.mnuItemEdit.Size = new System.Drawing.Size(167, 22);
            this.mnuItemEdit.Text = "Chỉnh sửa";
            this.mnuItemEdit.Click += new System.EventHandler(this.mnuItemEdit_Click);
            // 
            // mnuItemExport
            // 
            this.mnuItemExport.Enabled = false;
            this.mnuItemExport.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemExport.Image")));
            this.mnuItemExport.Name = "mnuItemExport";
            this.mnuItemExport.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.mnuItemExport.Size = new System.Drawing.Size(167, 22);
            this.mnuItemExport.Text = "Xuất excel";
            this.mnuItemExport.Click += new System.EventHandler(this.mnuItemExport_Click);
            // 
            // mnuItemReview
            // 
            this.mnuItemReview.Image = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.mnuItemReview.Name = "mnuItemReview";
            this.mnuItemReview.Size = new System.Drawing.Size(167, 22);
            this.mnuItemReview.Text = "Duyệt";
            this.mnuItemReview.Click += new System.EventHandler(this.mnuItemReview_Click);
            // 
            // mnuItemViewComment
            // 
            this.mnuItemViewComment.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.mnuItemViewComment.Name = "mnuItemViewComment";
            this.mnuItemViewComment.Size = new System.Drawing.Size(167, 22);
            this.mnuItemViewComment.Text = "Xem bình luận";
            this.mnuItemViewComment.Click += new System.EventHandler(this.mnuItemViewComment_Click);
            // 
            // cboInputStore
            // 
            this.cboInputStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInputStore.Location = new System.Drawing.Point(83, 42);
            this.cboInputStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboInputStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboInputStore.Name = "cboInputStore";
            this.cboInputStore.Size = new System.Drawing.Size(230, 24);
            this.cboInputStore.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 16);
            this.label10.TabIndex = 13;
            this.label10.Text = "Chi nhánh:";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(83, 15);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.ShowCheckBox = true;
            this.dtpFromDate.Size = new System.Drawing.Size(113, 22);
            this.dtpFromDate.TabIndex = 0;
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(200, 15);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.ShowCheckBox = true;
            this.dtpToDate.Size = new System.Drawing.Size(113, 22);
            this.dtpToDate.TabIndex = 1;
            // 
            // txtIMEISearch
            // 
            this.txtIMEISearch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIMEISearch.Location = new System.Drawing.Point(398, 44);
            this.txtIMEISearch.MaxLength = 50;
            this.txtIMEISearch.Name = "txtIMEISearch";
            this.txtIMEISearch.Size = new System.Drawing.Size(250, 22);
            this.txtIMEISearch.TabIndex = 4;
            this.txtIMEISearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtIMEISearch_KeyUp);
            this.txtIMEISearch.Leave += new System.EventHandler(this.txtIMEI_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(320, 47);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 16);
            this.label8.TabIndex = 3;
            this.label8.Text = "IMEI:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboProductList);
            this.groupBox1.Controls.Add(this.cboInputStore);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.dtpToDate);
            this.groupBox1.Controls.Add(this.dtpFromDate);
            this.groupBox1.Controls.Add(this.txtIMEISearch);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cmdSearch);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1016, 77);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.UseCompatibleTextRendering = true;
            // 
            // cboProductList
            // 
            this.cboProductList.BackColor = System.Drawing.Color.Transparent;
            this.cboProductList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductList.Location = new System.Drawing.Point(398, 15);
            this.cboProductList.Margin = new System.Windows.Forms.Padding(4);
            this.cboProductList.Name = "cboProductList";
            this.cboProductList.ProductIDList = "";
            this.cboProductList.Size = new System.Drawing.Size(250, 23);
            this.cboProductList.TabIndex = 2;
            // 
            // cmdSearch
            // 
            this.cmdSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSearch.Image = ((System.Drawing.Image)(resources.GetObject("cmdSearch.Image")));
            this.cmdSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSearch.Location = new System.Drawing.Point(657, 14);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(94, 25);
            this.cmdSearch.TabIndex = 7;
            this.cmdSearch.Text = "Tìm kiếm";
            this.cmdSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdSearch.UseVisualStyleBackColor = true;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(320, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 21;
            this.label4.Text = "Sản phẩm:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Ngày:";
            // 
            // grdData
            // 
            this.grdData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdData.ContextMenuStrip = this.mnuAction;
            this.grdData.Location = new System.Drawing.Point(3, 86);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repChkIsReviewed,
            this.chkIsInput,
            this.chkIsSelect,
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckedComboBoxEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3});
            this.grdData.Size = new System.Drawing.Size(1016, 389);
            this.grdData.TabIndex = 6;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.ColumnPanelRowHeight = 40;
            this.grdViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn22,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn17,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn32,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31});
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsView.ColumnAutoWidth = false;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn22
            // 
            this.gridColumn22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn22.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn22.Caption = "Chọn";
            this.gridColumn22.ColumnEdit = this.chkIsSelect;
            this.gridColumn22.FieldName = "ISSELECT";
            this.gridColumn22.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 0;
            this.gridColumn22.Width = 60;
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            this.chkIsSelect.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chkIsSelect.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.chkIsSelect.CheckedChanged += new System.EventHandler(this.chkIsSelect_CheckedChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn1.Caption = "Mã sản phẩm";
            this.gridColumn1.FieldName = "PRODUCTID";
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 120;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "Tên sản phẩm";
            this.gridColumn2.FieldName = "PRODUCTNAME";
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            this.gridColumn2.Width = 200;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "IMEI";
            this.gridColumn3.FieldName = "IMEI";
            this.gridColumn3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            this.gridColumn3.Width = 120;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn17.Caption = "Mã phiếu nhập";
            this.gridColumn17.FieldName = "INPUTVOUCHERID";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 5;
            this.gridColumn17.Width = 120;
            // 
            // gridColumn23
            // 
            this.gridColumn23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn23.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn23.Caption = "Bảo hành chính hãng";
            this.gridColumn23.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn23.FieldName = "ISBRANDNEWWARRANTY";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 7;
            this.gridColumn23.Width = 80;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit1.ValueUnchecked = ((short)(0));
            // 
            // gridColumn24
            // 
            this.gridColumn24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn24.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn24.Caption = "Ngày hết hạn BH";
            this.gridColumn24.FieldName = "ENDWARRANTYDATE";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 8;
            this.gridColumn24.Width = 100;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn4.Caption = "Chi nhánh";
            this.gridColumn4.FieldName = "STORENAME";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 6;
            this.gridColumn4.Width = 200;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn5.Caption = "Ngày nhập";
            this.gridColumn5.FieldName = "INPUTDATE";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 120;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Xác nhận";
            this.gridColumn6.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn6.FieldName = "ISCONFIRM";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 9;
            this.gridColumn6.Width = 80;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit3.ValueUnchecked = ((short)(0));
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Duyệt";
            this.gridColumn7.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn7.FieldName = "ISREVIEWED";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 10;
            this.gridColumn7.Width = 80;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit2.ValueUnchecked = ((short)(0));
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Người duyệt";
            this.gridColumn8.FieldName = "REVIEWEDUSER";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 11;
            this.gridColumn8.Width = 220;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Ngày duyệt";
            this.gridColumn9.FieldName = "REVIEWEDDATE";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 12;
            this.gridColumn9.Width = 120;
            // 
            // gridColumn32
            // 
            this.gridColumn32.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn32.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn32.Caption = "Số bình luận";
            this.gridColumn32.DisplayFormat.FormatString = "N0";
            this.gridColumn32.FieldName = "TOTALCOMMENT";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowMove = false;
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 13;
            this.gridColumn32.Width = 120;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Người tạo";
            this.gridColumn28.FieldName = "CREATEDUSER";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 14;
            this.gridColumn28.Width = 220;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Ngày tạo";
            this.gridColumn29.FieldName = "CREATEDDATE";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 15;
            this.gridColumn29.Width = 120;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Người cập nhật";
            this.gridColumn30.FieldName = "UPDATEDUSER";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 16;
            this.gridColumn30.Width = 220;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Ngày cập nhật";
            this.gridColumn31.FieldName = "UPDATEDDATE";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 17;
            this.gridColumn31.Width = 120;
            // 
            // repChkIsReviewed
            // 
            this.repChkIsReviewed.AutoHeight = false;
            this.repChkIsReviewed.Name = "repChkIsReviewed";
            this.repChkIsReviewed.ValueChecked = ((short)(1));
            this.repChkIsReviewed.ValueUnchecked = ((short)(0));
            // 
            // chkIsInput
            // 
            this.chkIsInput.AutoHeight = false;
            this.chkIsInput.Name = "chkIsInput";
            // 
            // repositoryItemCheckedComboBoxEdit1
            // 
            this.repositoryItemCheckedComboBoxEdit1.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit1.Name = "repositoryItemCheckedComboBoxEdit1";
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabList);
            this.tabControl.Controls.Add(this.tabDetail);
            this.tabControl.Location = new System.Drawing.Point(0, 3);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1030, 510);
            this.tabControl.TabIndex = 0;
            // 
            // tabList
            // 
            this.tabList.Controls.Add(this.grdData);
            this.tabList.Controls.Add(this.groupBox1);
            this.tabList.Location = new System.Drawing.Point(4, 25);
            this.tabList.Name = "tabList";
            this.tabList.Padding = new System.Windows.Forms.Padding(3);
            this.tabList.Size = new System.Drawing.Size(1022, 481);
            this.tabList.TabIndex = 1;
            this.tabList.Text = "Danh sách";
            this.tabList.UseVisualStyleBackColor = true;
            this.tabList.Enter += new System.EventHandler(this.tabList_Enter);
            // 
            // tabDetail
            // 
            this.tabDetail.Controls.Add(this.grpInfo);
            this.tabDetail.Controls.Add(this.tabCtrlDetail);
            this.tabDetail.Location = new System.Drawing.Point(4, 25);
            this.tabDetail.Name = "tabDetail";
            this.tabDetail.Padding = new System.Windows.Forms.Padding(3);
            this.tabDetail.Size = new System.Drawing.Size(1022, 481);
            this.tabDetail.TabIndex = 0;
            this.tabDetail.Text = "Thông tin chi tiết";
            this.tabDetail.UseVisualStyleBackColor = true;
            this.tabDetail.Enter += new System.EventHandler(this.tabDetail_Enter);
            // 
            // grpInfo
            // 
            this.grpInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpInfo.Controls.Add(this.txtWebContent);
            this.grpInfo.Controls.Add(this.label7);
            this.grpInfo.Controls.Add(this.chkIsReviewed);
            this.grpInfo.Controls.Add(this.txtPrintVATContent);
            this.grpInfo.Controls.Add(this.label1);
            this.grpInfo.Controls.Add(this.txtNote);
            this.grpInfo.Controls.Add(this.label6);
            this.grpInfo.Controls.Add(this.chkIsComfirm);
            this.grpInfo.Controls.Add(this.chkIsBrandNewWarranty);
            this.grpInfo.Controls.Add(this.dteEndWarantyDate);
            this.grpInfo.Controls.Add(this.label18);
            this.grpInfo.Location = new System.Drawing.Point(5, 278);
            this.grpInfo.Name = "grpInfo";
            this.grpInfo.Size = new System.Drawing.Size(1014, 197);
            this.grpInfo.TabIndex = 1;
            this.grpInfo.TabStop = false;
            this.grpInfo.Text = "Thông tin chung";
            // 
            // txtWebContent
            // 
            this.txtWebContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWebContent.BackColor = System.Drawing.SystemColors.Info;
            this.txtWebContent.Location = new System.Drawing.Point(599, 24);
            this.txtWebContent.MaxLength = 2000;
            this.txtWebContent.Multiline = true;
            this.txtWebContent.Name = "txtWebContent";
            this.txtWebContent.ReadOnly = true;
            this.txtWebContent.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtWebContent.Size = new System.Drawing.Size(404, 63);
            this.txtWebContent.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(511, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 32);
            this.label7.TabIndex = 9;
            this.label7.Text = "Nội dung\r\nhiển thị web:";
            // 
            // chkIsReviewed
            // 
            this.chkIsReviewed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkIsReviewed.AutoSize = true;
            this.chkIsReviewed.Location = new System.Drawing.Point(254, 166);
            this.chkIsReviewed.Name = "chkIsReviewed";
            this.chkIsReviewed.Size = new System.Drawing.Size(62, 20);
            this.chkIsReviewed.TabIndex = 8;
            this.chkIsReviewed.Text = "Duyệt";
            this.chkIsReviewed.UseVisualStyleBackColor = true;
            this.chkIsReviewed.CheckedChanged += new System.EventHandler(this.chkIsReviewed_CheckedChanged);
            // 
            // txtPrintVATContent
            // 
            this.txtPrintVATContent.BackColor = System.Drawing.SystemColors.Info;
            this.txtPrintVATContent.Location = new System.Drawing.Point(94, 24);
            this.txtPrintVATContent.MaxLength = 2000;
            this.txtPrintVATContent.Multiline = true;
            this.txtPrintVATContent.Name = "txtPrintVATContent";
            this.txtPrintVATContent.ReadOnly = true;
            this.txtPrintVATContent.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPrintVATContent.Size = new System.Drawing.Size(404, 63);
            this.txtPrintVATContent.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nội dung in \r\nhoá đơn VAT:";
            // 
            // txtNote
            // 
            this.txtNote.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNote.BackColor = System.Drawing.Color.White;
            this.txtNote.Location = new System.Drawing.Point(94, 93);
            this.txtNote.MaxLength = 2000;
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtNote.Size = new System.Drawing.Size(910, 40);
            this.txtNote.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 16);
            this.label6.TabIndex = 2;
            this.label6.Text = "Ghi chú:";
            // 
            // chkIsComfirm
            // 
            this.chkIsComfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkIsComfirm.AutoSize = true;
            this.chkIsComfirm.Location = new System.Drawing.Point(94, 169);
            this.chkIsComfirm.Name = "chkIsComfirm";
            this.chkIsComfirm.Size = new System.Drawing.Size(82, 20);
            this.chkIsComfirm.TabIndex = 7;
            this.chkIsComfirm.Text = "Xác nhận";
            this.chkIsComfirm.UseVisualStyleBackColor = true;
            // 
            // chkIsBrandNewWarranty
            // 
            this.chkIsBrandNewWarranty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkIsBrandNewWarranty.AutoSize = true;
            this.chkIsBrandNewWarranty.Location = new System.Drawing.Point(94, 141);
            this.chkIsBrandNewWarranty.Name = "chkIsBrandNewWarranty";
            this.chkIsBrandNewWarranty.Size = new System.Drawing.Size(151, 20);
            this.chkIsBrandNewWarranty.TabIndex = 4;
            this.chkIsBrandNewWarranty.Text = "Bảo hành chính hãng";
            this.chkIsBrandNewWarranty.UseVisualStyleBackColor = true;
            this.chkIsBrandNewWarranty.CheckedChanged += new System.EventHandler(this.chkIsBrandNewWarranty_CheckedChanged);
            // 
            // dteEndWarantyDate
            // 
            this.dteEndWarantyDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dteEndWarantyDate.CustomFormat = "dd/MM/yyyy";
            this.dteEndWarantyDate.Enabled = false;
            this.dteEndWarantyDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteEndWarantyDate.Location = new System.Drawing.Point(365, 141);
            this.dteEndWarantyDate.Name = "dteEndWarantyDate";
            this.dteEndWarantyDate.Size = new System.Drawing.Size(133, 22);
            this.dteEndWarantyDate.TabIndex = 6;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(251, 143);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(112, 16);
            this.label18.TabIndex = 5;
            this.label18.Text = "Ngày hết hạn BH:";
            // 
            // tabCtrlDetail
            // 
            this.tabCtrlDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabCtrlDetail.Controls.Add(this.tabDetailInfo);
            this.tabCtrlDetail.Controls.Add(this.tabIMEIImageInfo);
            this.tabCtrlDetail.Location = new System.Drawing.Point(5, 5);
            this.tabCtrlDetail.Name = "tabCtrlDetail";
            this.tabCtrlDetail.SelectedIndex = 0;
            this.tabCtrlDetail.Size = new System.Drawing.Size(1014, 267);
            this.tabCtrlDetail.TabIndex = 0;
            this.tabCtrlDetail.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabCtrlDetail_Selecting);
            // 
            // tabDetailInfo
            // 
            this.tabDetailInfo.Controls.Add(this.grpPrice);
            this.tabDetailInfo.Location = new System.Drawing.Point(4, 25);
            this.tabDetailInfo.Name = "tabDetailInfo";
            this.tabDetailInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabDetailInfo.Size = new System.Drawing.Size(1006, 238);
            this.tabDetailInfo.TabIndex = 0;
            this.tabDetailInfo.Text = "Thông tin sản phẩm";
            this.tabDetailInfo.UseVisualStyleBackColor = true;
            // 
            // grpPrice
            // 
            this.grpPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpPrice.Controls.Add(this.grdProduct);
            this.grpPrice.Controls.Add(this.txtIMEI);
            this.grpPrice.Controls.Add(this.txtProductName);
            this.grpPrice.Controls.Add(this.label3);
            this.grpPrice.Controls.Add(this.txtProductID);
            this.grpPrice.Controls.Add(this.label2);
            this.grpPrice.Controls.Add(this.btnSelectProduct);
            this.grpPrice.Location = new System.Drawing.Point(3, 6);
            this.grpPrice.Name = "grpPrice";
            this.grpPrice.Size = new System.Drawing.Size(997, 238);
            this.grpPrice.TabIndex = 0;
            this.grpPrice.TabStop = false;
            this.grpPrice.Text = "Thông tin sản phẩm";
            // 
            // grdProduct
            // 
            this.grdProduct.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdProduct.Location = new System.Drawing.Point(3, 47);
            this.grdProduct.MainView = this.grvProductSpec;
            this.grdProduct.Name = "grdProduct";
            this.grdProduct.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.cboProduct,
            this.cboProductSpec,
            this.repositoryItemTextEdit1});
            this.grdProduct.Size = new System.Drawing.Size(994, 185);
            this.grdProduct.TabIndex = 6;
            this.grdProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvProductSpec});
            // 
            // grvProductSpec
            // 
            this.grvProductSpec.Appearance.FocusedRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvProductSpec.Appearance.FocusedRow.Options.UseFont = true;
            this.grvProductSpec.Appearance.HeaderPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvProductSpec.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvProductSpec.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvProductSpec.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvProductSpec.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvProductSpec.Appearance.Row.Options.UseFont = true;
            this.grvProductSpec.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14});
            this.grvProductSpec.GridControl = this.grdProduct;
            this.grvProductSpec.Name = "grvProductSpec";
            this.grvProductSpec.OptionsView.ShowGroupPanel = false;
            this.grvProductSpec.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.grvProductSpec_FocusedRowChanged);
            this.grvProductSpec.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grvProductSpec_CellValueChanged);
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn10.Caption = "Thông tin sản phẩm";
            this.gridColumn10.FieldName = "PRODUCTSPECNAME";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn11.Caption = "Trạng thái bán hàng";
            this.gridColumn11.ColumnEdit = this.cboProduct;
            this.gridColumn11.FieldName = "ProductSpecStatusID";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 2;
            // 
            // cboProduct
            // 
            this.cboProduct.AutoHeight = false;
            this.cboProduct.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboProduct.Name = "cboProduct";
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn12.Caption = "Mô tả";
            this.gridColumn12.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn12.FieldName = "Note";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 3;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 100;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn13.Caption = "Thông tin sản phẩm";
            this.gridColumn13.FieldName = "ProductSpecName";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 0;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Trạng thái định giá";
            this.gridColumn14.FieldName = "SetPriceProductSpecStatusName";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 1;
            // 
            // cboProductSpec
            // 
            this.cboProductSpec.AutoHeight = false;
            this.cboProductSpec.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboProductSpec.Name = "cboProductSpec";
            // 
            // txtIMEI
            // 
            this.txtIMEI.BackColor = System.Drawing.Color.White;
            this.txtIMEI.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIMEI.Location = new System.Drawing.Point(622, 19);
            this.txtIMEI.MaxLength = 50;
            this.txtIMEI.Name = "txtIMEI";
            this.txtIMEI.Size = new System.Drawing.Size(158, 22);
            this.txtIMEI.TabIndex = 5;
            this.txtIMEI.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtIMEI_KeyUp);
            this.txtIMEI.Leave += new System.EventHandler(this.txtIMEI_Leave);
            // 
            // txtProductName
            // 
            this.txtProductName.BackColor = System.Drawing.SystemColors.Info;
            this.txtProductName.Location = new System.Drawing.Point(267, 19);
            this.txtProductName.MaxLength = 20;
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.ReadOnly = true;
            this.txtProductName.Size = new System.Drawing.Size(319, 22);
            this.txtProductName.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(589, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "IMEI:";
            // 
            // txtProductID
            // 
            this.txtProductID.BackColor = System.Drawing.SystemColors.Info;
            this.txtProductID.ContextMenuStrip = this.mnuAction;
            this.txtProductID.Location = new System.Drawing.Point(73, 19);
            this.txtProductID.MaxLength = 20;
            this.txtProductID.Name = "txtProductID";
            this.txtProductID.ReadOnly = true;
            this.txtProductID.Size = new System.Drawing.Size(192, 22);
            this.txtProductID.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Sản phẩm:";
            // 
            // btnSelectProduct
            // 
            this.btnSelectProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectProduct.Location = new System.Drawing.Point(237, 18);
            this.btnSelectProduct.Name = "btnSelectProduct";
            this.btnSelectProduct.Size = new System.Drawing.Size(28, 23);
            this.btnSelectProduct.TabIndex = 2;
            this.btnSelectProduct.Text = "...";
            this.btnSelectProduct.UseVisualStyleBackColor = true;
            this.btnSelectProduct.Click += new System.EventHandler(this.btnSelectProduct_Click);
            // 
            // tabIMEIImageInfo
            // 
            this.tabIMEIImageInfo.Controls.Add(this.grdImages);
            this.tabIMEIImageInfo.Controls.Add(this.btnMoveDown);
            this.tabIMEIImageInfo.Controls.Add(this.btnEditImage);
            this.tabIMEIImageInfo.Controls.Add(this.btnMoveUp);
            this.tabIMEIImageInfo.Controls.Add(this.btnDelImage);
            this.tabIMEIImageInfo.Controls.Add(this.btnAddNewImage);
            this.tabIMEIImageInfo.Location = new System.Drawing.Point(4, 25);
            this.tabIMEIImageInfo.Name = "tabIMEIImageInfo";
            this.tabIMEIImageInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabIMEIImageInfo.Size = new System.Drawing.Size(1006, 238);
            this.tabIMEIImageInfo.TabIndex = 1;
            this.tabIMEIImageInfo.Text = "Thông tin ảnh sản phẩm";
            this.tabIMEIImageInfo.UseVisualStyleBackColor = true;
            // 
            // grdImages
            // 
            this.grdImages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdImages.ContextMenuStrip = this.mnuIMAGE;
            this.grdImages.Location = new System.Drawing.Point(6, 37);
            this.grdImages.MainView = this.grvImages;
            this.grdImages.Name = "grdImages";
            this.grdImages.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1,
            this.repositoryItemCheckEdit4});
            this.grdImages.Size = new System.Drawing.Size(961, 207);
            this.grdImages.TabIndex = 3;
            this.grdImages.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvImages});
            this.grdImages.DoubleClick += new System.EventHandler(this.grdImages_DoubleClick);
            // 
            // mnuIMAGE
            // 
            this.mnuIMAGE.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAddImage,
            this.mnuEditImage,
            this.mnuDelImage});
            this.mnuIMAGE.Name = "contextMenuStrip1";
            this.mnuIMAGE.Size = new System.Drawing.Size(121, 70);
            // 
            // mnuAddImage
            // 
            this.mnuAddImage.Image = ((System.Drawing.Image)(resources.GetObject("mnuAddImage.Image")));
            this.mnuAddImage.Name = "mnuAddImage";
            this.mnuAddImage.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.mnuAddImage.ShowShortcutKeys = false;
            this.mnuAddImage.Size = new System.Drawing.Size(120, 22);
            this.mnuAddImage.Text = "Thêm";
            this.mnuAddImage.Click += new System.EventHandler(this.mnuAddImage_Click);
            // 
            // mnuEditImage
            // 
            this.mnuEditImage.Image = ((System.Drawing.Image)(resources.GetObject("mnuEditImage.Image")));
            this.mnuEditImage.Name = "mnuEditImage";
            this.mnuEditImage.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.mnuEditImage.ShowShortcutKeys = false;
            this.mnuEditImage.Size = new System.Drawing.Size(120, 22);
            this.mnuEditImage.Text = "Chỉnh sửa";
            this.mnuEditImage.Click += new System.EventHandler(this.mnuEditImage_Click);
            // 
            // mnuDelImage
            // 
            this.mnuDelImage.Image = ((System.Drawing.Image)(resources.GetObject("mnuDelImage.Image")));
            this.mnuDelImage.Name = "mnuDelImage";
            this.mnuDelImage.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.mnuDelImage.ShowShortcutKeys = false;
            this.mnuDelImage.Size = new System.Drawing.Size(120, 22);
            this.mnuDelImage.Text = "Xóa";
            this.mnuDelImage.Click += new System.EventHandler(this.mnuDelImage_Click);
            // 
            // grvImages
            // 
            this.grvImages.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grvImages.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grvImages.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grvImages.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Transparent;
            this.grvImages.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvImages.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grvImages.Appearance.FocusedRow.Options.UseFont = true;
            this.grvImages.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvImages.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvImages.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvImages.Appearance.Preview.Options.UseFont = true;
            this.grvImages.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvImages.Appearance.Row.Options.UseFont = true;
            this.grvImages.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27});
            this.grvImages.GridControl = this.grdImages;
            this.grvImages.Name = "grvImages";
            this.grvImages.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grvImages.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grvImages.OptionsNavigation.UseTabKey = false;
            this.grvImages.OptionsView.ShowAutoFilterRow = true;
            this.grvImages.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvImages.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Tên hình";
            this.gridColumn15.FieldName = "ImageName";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            this.gridColumn15.Width = 131;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Mô tả";
            this.gridColumn16.FieldName = "Description";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 1;
            this.gridColumn16.Width = 253;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.Caption = "Hình lớn";
            this.gridColumn18.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn18.FieldName = "LargesizeImage";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn18.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 2;
            this.gridColumn18.Width = 108;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // gridColumn19
            // 
            this.gridColumn19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn19.Caption = "Hình vừa";
            this.gridColumn19.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn19.FieldName = "MediumSizeImage";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn19.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 3;
            this.gridColumn19.Width = 107;
            // 
            // gridColumn20
            // 
            this.gridColumn20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn20.Caption = "Hình nhỏ";
            this.gridColumn20.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn20.FieldName = "SmallSizeImage";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn20.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 4;
            this.gridColumn20.Width = 107;
            // 
            // gridColumn21
            // 
            this.gridColumn21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn21.Caption = "Mặc định";
            this.gridColumn21.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn21.FieldName = "IsDefault";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 5;
            this.gridColumn21.Width = 72;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // gridColumn25
            // 
            this.gridColumn25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn25.Caption = "Kích hoạt";
            this.gridColumn25.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn25.FieldName = "IsActive";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 6;
            this.gridColumn25.Width = 71;
            // 
            // gridColumn26
            // 
            this.gridColumn26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn26.Caption = "Hệ thống";
            this.gridColumn26.ColumnEdit = this.repositoryItemCheckEdit4;
            this.gridColumn26.FieldName = "IsSystem";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 7;
            this.gridColumn26.Width = 72;
            // 
            // gridColumn27
            // 
            this.gridColumn27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn27.Caption = "Thứ tự";
            this.gridColumn27.FieldName = "OrderIndex";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 8;
            this.gridColumn27.Width = 85;
            // 
            // btnMoveDown
            // 
            this.btnMoveDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveDown.Image")));
            this.btnMoveDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMoveDown.Location = new System.Drawing.Point(970, 187);
            this.btnMoveDown.Name = "btnMoveDown";
            this.btnMoveDown.Size = new System.Drawing.Size(25, 33);
            this.btnMoveDown.TabIndex = 5;
            this.btnMoveDown.Text = "     Thêm";
            this.btnMoveDown.UseVisualStyleBackColor = true;
            this.btnMoveDown.Click += new System.EventHandler(this.btnMoveDown_Click);
            // 
            // btnEditImage
            // 
            this.btnEditImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditImage.Image = ((System.Drawing.Image)(resources.GetObject("btnEditImage.Image")));
            this.btnEditImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditImage.Location = new System.Drawing.Point(759, 5);
            this.btnEditImage.Name = "btnEditImage";
            this.btnEditImage.Size = new System.Drawing.Size(102, 25);
            this.btnEditImage.TabIndex = 1;
            this.btnEditImage.Text = "     Chỉnh sửa";
            this.btnEditImage.UseVisualStyleBackColor = true;
            this.btnEditImage.Click += new System.EventHandler(this.btnEditImage_Click);
            // 
            // btnMoveUp
            // 
            this.btnMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveUp.Image")));
            this.btnMoveUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMoveUp.Location = new System.Drawing.Point(970, 148);
            this.btnMoveUp.Name = "btnMoveUp";
            this.btnMoveUp.Size = new System.Drawing.Size(25, 33);
            this.btnMoveUp.TabIndex = 4;
            this.btnMoveUp.Text = "     Thêm";
            this.btnMoveUp.UseVisualStyleBackColor = true;
            this.btnMoveUp.Click += new System.EventHandler(this.btnMoveUp_Click);
            // 
            // btnDelImage
            // 
            this.btnDelImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelImage.Image = ((System.Drawing.Image)(resources.GetObject("btnDelImage.Image")));
            this.btnDelImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelImage.Location = new System.Drawing.Point(867, 5);
            this.btnDelImage.Name = "btnDelImage";
            this.btnDelImage.Size = new System.Drawing.Size(102, 25);
            this.btnDelImage.TabIndex = 2;
            this.btnDelImage.Text = "     Xóa";
            this.btnDelImage.UseVisualStyleBackColor = true;
            this.btnDelImage.Click += new System.EventHandler(this.btnDelImage_Click);
            // 
            // btnAddNewImage
            // 
            this.btnAddNewImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNewImage.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNewImage.Image")));
            this.btnAddNewImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddNewImage.Location = new System.Drawing.Point(656, 5);
            this.btnAddNewImage.Name = "btnAddNewImage";
            this.btnAddNewImage.Size = new System.Drawing.Size(98, 25);
            this.btnAddNewImage.TabIndex = 0;
            this.btnAddNewImage.Text = "     Thêm";
            this.btnAddNewImage.UseVisualStyleBackColor = true;
            this.btnAddNewImage.Click += new System.EventHandler(this.btnAddNewImage_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Info;
            this.panel1.Controls.Add(this.btnReview);
            this.panel1.Controls.Add(this.btnUndo);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.btnExportExcel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 524);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1030, 38);
            this.panel1.TabIndex = 1;
            // 
            // btnReview
            // 
            this.btnReview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReview.Image = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.btnReview.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReview.Location = new System.Drawing.Point(830, 7);
            this.btnReview.Name = "btnReview";
            this.btnReview.Size = new System.Drawing.Size(95, 25);
            this.btnReview.TabIndex = 4;
            this.btnReview.Text = "   Duyệt";
            this.btnReview.UseVisualStyleBackColor = true;
            this.btnReview.Click += new System.EventHandler(this.btnReview_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUndo.Image = ((System.Drawing.Image)(resources.GetObject("btnUndo.Image")));
            this.btnUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUndo.Location = new System.Drawing.Point(931, 7);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(95, 25);
            this.btnUndo.TabIndex = 5;
            this.btnUndo.Text = "   Bỏ qua";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(730, 7);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(95, 25);
            this.btnUpdate.TabIndex = 3;
            this.btnUpdate.Text = "   Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(630, 7);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(95, 25);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "   Chỉnh sửa";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(530, 7);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(95, 25);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "   Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportExcel.Location = new System.Drawing.Point(7, 7);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(95, 25);
            this.btnExportExcel.TabIndex = 1;
            this.btnExportExcel.Text = "    Xuất Excel";
            this.btnExportExcel.UseVisualStyleBackColor = true;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // frmIMEISalesInfoManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 562);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmIMEISalesInfoManager";
            this.ShowIcon = false;
            this.Text = "Quản lý thông tin bán hàng của IMEI";
            this.Load += new System.EventHandler(this.frmIMEISalesInfoManager_Load);
            this.mnuAction.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repChkIsReviewed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabList.ResumeLayout(false);
            this.tabDetail.ResumeLayout(false);
            this.grpInfo.ResumeLayout(false);
            this.grpInfo.PerformLayout();
            this.tabCtrlDetail.ResumeLayout(false);
            this.tabDetailInfo.ResumeLayout(false);
            this.grpPrice.ResumeLayout(false);
            this.grpPrice.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvProductSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboProductSpec)).EndInit();
            this.tabIMEIImageInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdImages)).EndInit();
            this.mnuIMAGE.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuItemAdd;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDelete;
        private System.Windows.Forms.ToolStripMenuItem mnuItemEdit;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExport;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboInputStore;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.TextBox txtIMEISearch;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button cmdSearch;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        //private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repChkIsReviewed;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsInput;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private MasterData.DUI.CustomControl.Product.cboProductList cboProductList;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private System.Windows.Forms.ToolStripMenuItem mnuItemReview;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabDetail;
        private System.Windows.Forms.TabPage tabList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnReview;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnExportExcel;
        private System.Windows.Forms.TabControl tabCtrlDetail;
        private System.Windows.Forms.TabPage tabDetailInfo;
        private System.Windows.Forms.GroupBox grpPrice;
        private System.Windows.Forms.TextBox txtIMEI;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSelectProduct;
        private System.Windows.Forms.TextBox txtProductID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabIMEIImageInfo;
        private DevExpress.XtraGrid.GridControl grdImages;
        private DevExpress.XtraGrid.Views.Grid.GridView grvImages;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private System.Windows.Forms.Button btnMoveDown;
        private System.Windows.Forms.Button btnEditImage;
        private System.Windows.Forms.Button btnMoveUp;
        private System.Windows.Forms.Button btnDelImage;
        private System.Windows.Forms.Button btnAddNewImage;
        private System.Windows.Forms.GroupBox grpInfo;
        private System.Windows.Forms.CheckBox chkIsReviewed;
        private System.Windows.Forms.TextBox txtPrintVATContent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkIsComfirm;
        private System.Windows.Forms.CheckBox chkIsBrandNewWarranty;
        private System.Windows.Forms.DateTimePicker dteEndWarantyDate;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ContextMenuStrip mnuIMAGE;
        private System.Windows.Forms.ToolStripMenuItem mnuAddImage;
        private System.Windows.Forms.ToolStripMenuItem mnuEditImage;
        private System.Windows.Forms.ToolStripMenuItem mnuDelImage;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repChkIsReviewed;
        private DevExpress.XtraGrid.GridControl grdProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView grvProductSpec;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboProduct;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cboProductSpec;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private System.Windows.Forms.TextBox txtWebContent;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolStripMenuItem mnuItemViewComment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;

    }
}