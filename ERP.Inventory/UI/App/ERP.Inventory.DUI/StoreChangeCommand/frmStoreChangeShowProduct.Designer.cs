﻿namespace ERP.Inventory.DUI.StoreChangeCommand
{
    partial class frmStoreChangeShowProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colISSTORECHANGEORDER = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.bgrvData = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cboMainGroupID = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.chkIsNew = new System.Windows.Forms.CheckBox();
            this.btnCreateStoreChangeOrder = new System.Windows.Forms.Button();
            this.cboSubGroupIDList = new Library.AppControl.ComboBoxControl.ComboBoxSubGroup();
            this.cboBrandIDList = new Library.AppControl.ComboBoxControl.ComboBoxBrand();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNumDays = new C1.Win.C1Input.C1NumericEdit();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.label8 = new System.Windows.Forms.Label();
            this.cboAuxiliaryStore = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.label1 = new System.Windows.Forms.Label();
            this.cboRealStore = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgrvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumDays)).BeginInit();
            this.SuspendLayout();
            // 
            // colISSTORECHANGEORDER
            // 
            this.colISSTORECHANGEORDER.FieldName = "ISSTORECHANGEORDER";
            this.colISSTORECHANGEORDER.Name = "colISSTORECHANGEORDER";
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.groupControl2.Controls.Add(this.grdData);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 113);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(986, 401);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Danh sách sản phẩm đảo hàng";
            // 
            // grdData
            // 
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(2, 22);
            this.grdData.MainView = this.bgrvData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1,
            this.chkIsSelect});
            this.grdData.Size = new System.Drawing.Size(982, 377);
            this.grdData.TabIndex = 0;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bgrvData,
            this.bandedGridView1});
            // 
            // bgrvData
            // 
            this.bgrvData.Appearance.BandPanel.BackColor = System.Drawing.Color.Gray;
            this.bgrvData.Appearance.BandPanel.BorderColor = System.Drawing.Color.Gray;
            this.bgrvData.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.bgrvData.Appearance.BandPanel.Options.UseBackColor = true;
            this.bgrvData.Appearance.BandPanel.Options.UseBorderColor = true;
            this.bgrvData.Appearance.BandPanel.Options.UseFont = true;
            this.bgrvData.Appearance.BandPanelBackground.BackColor = System.Drawing.Color.Black;
            this.bgrvData.Appearance.BandPanelBackground.Options.UseBackColor = true;
            this.bgrvData.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.bgrvData.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.bgrvData.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.bgrvData.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.bgrvData.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.bgrvData.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.bgrvData.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.bgrvData.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.bgrvData.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.bgrvData.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.bgrvData.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.bgrvData.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.bgrvData.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.bgrvData.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.bgrvData.Appearance.Empty.Options.UseBackColor = true;
            this.bgrvData.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.bgrvData.Appearance.EvenRow.Options.UseBackColor = true;
            this.bgrvData.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.bgrvData.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.bgrvData.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.bgrvData.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.bgrvData.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.bgrvData.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.bgrvData.Appearance.FilterPanel.Options.UseBackColor = true;
            this.bgrvData.Appearance.FilterPanel.Options.UseForeColor = true;
            this.bgrvData.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.bgrvData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bgrvData.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.bgrvData.Appearance.FocusedRow.Options.UseBackColor = true;
            this.bgrvData.Appearance.FocusedRow.Options.UseFont = true;
            this.bgrvData.Appearance.FocusedRow.Options.UseForeColor = true;
            this.bgrvData.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.bgrvData.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.bgrvData.Appearance.FooterPanel.Options.UseBackColor = true;
            this.bgrvData.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.bgrvData.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.bgrvData.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.bgrvData.Appearance.GroupButton.Options.UseBackColor = true;
            this.bgrvData.Appearance.GroupButton.Options.UseBorderColor = true;
            this.bgrvData.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.bgrvData.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.bgrvData.Appearance.GroupFooter.Options.UseBackColor = true;
            this.bgrvData.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.bgrvData.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.bgrvData.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.bgrvData.Appearance.GroupPanel.Options.UseBackColor = true;
            this.bgrvData.Appearance.GroupPanel.Options.UseForeColor = true;
            this.bgrvData.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.bgrvData.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.bgrvData.Appearance.GroupRow.Options.UseBackColor = true;
            this.bgrvData.Appearance.GroupRow.Options.UseFont = true;
            this.bgrvData.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.bgrvData.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.bgrvData.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.bgrvData.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.bgrvData.Appearance.HeaderPanelBackground.BackColor = System.Drawing.Color.Black;
            this.bgrvData.Appearance.HeaderPanelBackground.Options.UseBackColor = true;
            this.bgrvData.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.bgrvData.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.bgrvData.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.bgrvData.Appearance.HorzLine.Options.UseBackColor = true;
            this.bgrvData.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bgrvData.Appearance.OddRow.Options.UseBackColor = true;
            this.bgrvData.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.bgrvData.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.bgrvData.Appearance.Preview.Options.UseBackColor = true;
            this.bgrvData.Appearance.Preview.Options.UseForeColor = true;
            this.bgrvData.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.bgrvData.Appearance.Row.Options.UseBackColor = true;
            this.bgrvData.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.bgrvData.Appearance.RowSeparator.Options.UseBackColor = true;
            this.bgrvData.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.bgrvData.Appearance.SelectedRow.Options.UseBackColor = true;
            this.bgrvData.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.bgrvData.Appearance.VertLine.Options.UseBackColor = true;
            this.bgrvData.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2,
            this.gridBand3});
            this.bgrvData.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn7,
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn3,
            this.bandedGridColumn4,
            this.bandedGridColumn5,
            this.bandedGridColumn6,
            this.colISSTORECHANGEORDER});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colISSTORECHANGEORDER;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition1.Expression = "[ISSTORECHANGEORDER]=true";
            styleFormatCondition1.Tag = true;
            styleFormatCondition1.Value1 = true;
            this.bgrvData.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.bgrvData.GridControl = this.grdData;
            this.bgrvData.Name = "bgrvData";
            this.bgrvData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.bgrvData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.bgrvData.OptionsView.ColumnAutoWidth = false;
            this.bgrvData.OptionsView.EnableAppearanceEvenRow = true;
            this.bgrvData.OptionsView.EnableAppearanceOddRow = true;
            this.bgrvData.OptionsView.ShowAutoFilterRow = true;
            this.bgrvData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.bgrvData.OptionsView.ShowFooter = true;
            this.bgrvData.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Columns.Add(this.bandedGridColumn7);
            this.gridBand1.Columns.Add(this.bandedGridColumn1);
            this.gridBand1.Columns.Add(this.bandedGridColumn2);
            this.gridBand1.MinWidth = 20;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 417;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn7.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn7.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn7.Caption = "Chọn";
            this.bandedGridColumn7.ColumnEdit = this.chkIsSelect;
            this.bandedGridColumn7.FieldName = "ISSELECT";
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.Visible = true;
            this.bandedGridColumn7.Width = 58;
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            this.chkIsSelect.CheckedChanged += new System.EventHandler(this.chkIsSelect_CheckedChanged);
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn1.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn1.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn1.Caption = "Mã sản phẩm";
            this.bandedGridColumn1.FieldName = "PRODUCTID";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn1.OptionsColumn.AllowMove = false;
            this.bandedGridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn1.Visible = true;
            this.bandedGridColumn1.Width = 130;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn2.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn2.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn2.Caption = "Tên sản phẩm";
            this.bandedGridColumn2.FieldName = "PRODUCTNAME";
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn2.OptionsColumn.AllowMove = false;
            this.bandedGridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn2.Visible = true;
            this.bandedGridColumn2.Width = 229;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Kho chính";
            this.gridBand2.Columns.Add(this.bandedGridColumn3);
            this.gridBand2.Columns.Add(this.bandedGridColumn4);
            this.gridBand2.MinWidth = 20;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 300;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn3.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn3.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn3.Caption = "IMEI";
            this.bandedGridColumn3.FieldName = "REALIMEI";
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn3.OptionsColumn.AllowMove = false;
            this.bandedGridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn3.Visible = true;
            this.bandedGridColumn3.Width = 182;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn4.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn4.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn4.Caption = "Ngày nhập";
            this.bandedGridColumn4.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.bandedGridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.bandedGridColumn4.FieldName = "REALINPUTDATE";
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn4.OptionsColumn.AllowMove = false;
            this.bandedGridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn4.Visible = true;
            this.bandedGridColumn4.Width = 118;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Kho bày mẫu";
            this.gridBand3.Columns.Add(this.bandedGridColumn5);
            this.gridBand3.Columns.Add(this.bandedGridColumn6);
            this.gridBand3.MinWidth = 20;
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 302;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn5.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn5.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn5.Caption = "IMEI";
            this.bandedGridColumn5.FieldName = "AUXILIARYIMEI";
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn5.OptionsColumn.AllowMove = false;
            this.bandedGridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn5.Visible = true;
            this.bandedGridColumn5.Width = 181;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn6.AppearanceCell.Options.UseFont = true;
            this.bandedGridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bandedGridColumn6.AppearanceHeader.Options.UseFont = true;
            this.bandedGridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.bandedGridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn6.Caption = "Ngày nhập";
            this.bandedGridColumn6.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.bandedGridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.bandedGridColumn6.FieldName = "AUXILIARYINPUTDATE";
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn6.OptionsColumn.AllowMove = false;
            this.bandedGridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn6.Visible = true;
            this.bandedGridColumn6.Width = 121;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.EditFormat.FormatString = "N4";
            this.repositoryItemCalcEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemCalcEdit1.Mask.EditMask = "##,###,###,###.##";
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.bandedGridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.bandedGridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.bandedGridView1.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridView1.Appearance.Preview.Options.UseFont = true;
            this.bandedGridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.bandedGridView1.Appearance.Row.Options.UseFont = true;
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn8,
            this.bandedGridColumn9,
            this.bandedGridColumn10,
            this.bandedGridColumn11,
            this.bandedGridColumn12,
            this.bandedGridColumn13});
            styleFormatCondition2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            styleFormatCondition2.Appearance.Options.UseBackColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition2.Expression = "[ISDELETED]>0";
            styleFormatCondition2.Value1 = true;
            this.bandedGridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.bandedGridView1.GridControl = this.grdData;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.bandedGridView1.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.ShowAutoFilterRow = true;
            this.bandedGridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.bandedGridView1.OptionsView.ShowFooter = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "gridBand4";
            this.gridBand4.Columns.Add(this.bandedGridColumn8);
            this.gridBand4.Columns.Add(this.bandedGridColumn9);
            this.gridBand4.Columns.Add(this.bandedGridColumn10);
            this.gridBand4.Columns.Add(this.bandedGridColumn11);
            this.gridBand4.Columns.Add(this.bandedGridColumn12);
            this.gridBand4.Columns.Add(this.bandedGridColumn13);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Width = 450;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.Caption = "Mã sản phẩm";
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.Visible = true;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.Caption = "Tên sản phẩm";
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.Visible = true;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.Caption = "IMEI";
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.Visible = true;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.Caption = "Ngày nhập";
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.Visible = true;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.Caption = "IMEI";
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.Visible = true;
            // 
            // bandedGridColumn13
            // 
            this.bandedGridColumn13.Caption = "Ngày nhập";
            this.bandedGridColumn13.Name = "bandedGridColumn13";
            this.bandedGridColumn13.Visible = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Mã sản phẩm";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tên sản phẩm";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "IMEI";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Ngày nhập";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "IMEI";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Ngày nhập";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.groupControl1.Controls.Add(this.cboMainGroupID);
            this.groupControl1.Controls.Add(this.chkIsNew);
            this.groupControl1.Controls.Add(this.btnCreateStoreChangeOrder);
            this.groupControl1.Controls.Add(this.cboSubGroupIDList);
            this.groupControl1.Controls.Add(this.cboBrandIDList);
            this.groupControl1.Controls.Add(this.label7);
            this.groupControl1.Controls.Add(this.label6);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.txtNumDays);
            this.groupControl1.Controls.Add(this.btnSearch);
            this.groupControl1.Controls.Add(this.label8);
            this.groupControl1.Controls.Add(this.cboAuxiliaryStore);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.cboRealStore);
            this.groupControl1.Controls.Add(this.label5);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(986, 113);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin tìm kiếm";
            // 
            // cboMainGroupID
            // 
            this.cboMainGroupID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroupID.Location = new System.Drawing.Point(91, 57);
            this.cboMainGroupID.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroupID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroupID.Name = "cboMainGroupID";
            this.cboMainGroupID.Size = new System.Drawing.Size(224, 24);
            this.cboMainGroupID.TabIndex = 7;
            this.cboMainGroupID.SelectionChangeCommitted += new System.EventHandler(this.cboMainGroupID_SelectionChangeCommitted);
            // 
            // chkIsNew
            // 
            this.chkIsNew.AutoSize = true;
            this.chkIsNew.Checked = true;
            this.chkIsNew.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsNew.Location = new System.Drawing.Point(647, 87);
            this.chkIsNew.Name = "chkIsNew";
            this.chkIsNew.Size = new System.Drawing.Size(113, 20);
            this.chkIsNew.TabIndex = 59;
            this.chkIsNew.Text = "Sản phẩm mới";
            this.chkIsNew.UseVisualStyleBackColor = true;
            // 
            // btnCreateStoreChangeOrder
            // 
            this.btnCreateStoreChangeOrder.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.btnCreateStoreChangeOrder.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCreateStoreChangeOrder.Location = new System.Drawing.Point(774, 84);
            this.btnCreateStoreChangeOrder.Name = "btnCreateStoreChangeOrder";
            this.btnCreateStoreChangeOrder.Size = new System.Drawing.Size(106, 25);
            this.btnCreateStoreChangeOrder.TabIndex = 58;
            this.btnCreateStoreChangeOrder.Text = "Tạo yêu cầu";
            this.btnCreateStoreChangeOrder.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCreateStoreChangeOrder.UseVisualStyleBackColor = true;
            this.btnCreateStoreChangeOrder.Click += new System.EventHandler(this.btnCreateStoreChangeOrder_Click);
            // 
            // cboSubGroupIDList
            // 
            this.cboSubGroupIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSubGroupIDList.Location = new System.Drawing.Point(415, 57);
            this.cboSubGroupIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboSubGroupIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboSubGroupIDList.Name = "cboSubGroupIDList";
            this.cboSubGroupIDList.Size = new System.Drawing.Size(224, 24);
            this.cboSubGroupIDList.TabIndex = 9;
            // 
            // cboBrandIDList
            // 
            this.cboBrandIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBrandIDList.Location = new System.Drawing.Point(753, 57);
            this.cboBrandIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboBrandIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBrandIDList.Name = "cboBrandIDList";
            this.cboBrandIDList.Size = new System.Drawing.Size(224, 24);
            this.cboBrandIDList.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(644, 61);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 16);
            this.label7.TabIndex = 10;
            this.label7.Text = "Nhà sản xuất:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "Ngành hàng:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(327, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "Nhóm hàng:";
            // 
            // txtNumDays
            // 
            this.txtNumDays.CustomFormat = "##0";
            this.txtNumDays.DataType = typeof(int);
            this.txtNumDays.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtNumDays.Location = new System.Drawing.Point(753, 30);
            this.txtNumDays.Name = "txtNumDays";
            this.txtNumDays.Size = new System.Drawing.Size(88, 22);
            this.txtNumDays.TabIndex = 5;
            this.txtNumDays.Tag = null;
            this.txtNumDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNumDays.Value = 7;
            this.txtNumDays.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // btnSearch
            // 
            this.btnSearch.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Appearance.Options.UseFont = true;
            this.btnSearch.Appearance.Options.UseTextOptions = true;
            this.btnSearch.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.btnSearch.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearch.Location = new System.Drawing.Point(886, 84);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(91, 25);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "&Tìm kiếm ";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(644, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 16);
            this.label8.TabIndex = 4;
            this.label8.Text = "Số ngày tối thiểu:";
            // 
            // cboAuxiliaryStore
            // 
            this.cboAuxiliaryStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAuxiliaryStore.Location = new System.Drawing.Point(415, 30);
            this.cboAuxiliaryStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboAuxiliaryStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboAuxiliaryStore.Name = "cboAuxiliaryStore";
            this.cboAuxiliaryStore.Size = new System.Drawing.Size(224, 24);
            this.cboAuxiliaryStore.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(327, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Kho bày mẫu:";
            // 
            // cboRealStore
            // 
            this.cboRealStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRealStore.Location = new System.Drawing.Point(91, 30);
            this.cboRealStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboRealStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboRealStore.Name = "cboRealStore";
            this.cboRealStore.Size = new System.Drawing.Size(224, 24);
            this.cboRealStore.TabIndex = 1;
            this.cboRealStore.SelectionChangeCommitted += new System.EventHandler(this.cboRealStore_SelectionChangeCommitted);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Kho chính:";
            // 
            // frmStoreChangeShowProduct
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(986, 514);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmStoreChangeShowProduct";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý đảo hàng kho bày mẫu";
            this.Load += new System.EventHandler(this.frmStoreChangeShowProduct_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgrvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumDays)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private Library.AppControl.ComboBoxControl.ComboBoxSubGroup cboSubGroupIDList;
        private Library.AppControl.ComboBoxControl.ComboBoxBrand cboBrandIDList;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private C1.Win.C1Input.C1NumericEdit txtNumDays;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private System.Windows.Forms.Label label8;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboAuxiliaryStore;
        private System.Windows.Forms.Label label1;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboRealStore;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bgrvData;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private System.Windows.Forms.Button btnCreateStoreChangeOrder;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colISSTORECHANGEORDER;
        private System.Windows.Forms.CheckBox chkIsNew;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroupID;
    }
}