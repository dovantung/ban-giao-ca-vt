﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.StoreChangeCommand
{
    public partial class frmShowProductManager : Form
    {
        public frmShowProductManager()
        {
            InitializeComponent();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvData);
            objSelection = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection(true,true,grvData);
            objSelection.CheckMarkColumn.VisibleIndex = 0;
            objSelection.CheckMarkColumn.Width = 60;
            
        }
        private Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection objSelection;
        private DataTable dtbStoreData = null;
        private DataTable dtbData = null;
        private ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = null;
        private int intTransportTypeID = 0;// phuong tien van chuyen
        private int intFromStoreID = 0;
        private int intToStoreID = 0;
        private bool bolIsNew = true;
        private void frmStoreChangeShowProduct_Load(object sender, EventArgs e)
        {
            LoadCombobox();
            if (!LoadStoreChangeOrderTypeInfo())
            {
                MessageBox.Show(this,"Chức năng chưa được khai báo tham số","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                this.Close();
            }
            btnCreateStoreChangeOrder.Enabled = false;
            timer1.Enabled = false;
        }

        /// <summary>
        /// Load tham số Form
        /// </summary>
        private bool LoadStoreChangeOrderTypeInfo()
        {
            int intStoreChangeOrderTypeID = 0;
            if (this.Tag != null)
            {
                Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                try
                {
                    intStoreChangeOrderTypeID = Convert.ToInt32(hstbParam["StoreChangeOrderTypeID"]);
                }
                catch (Exception objExce)
                {
                    SystemErrorWS.Insert("Lỗi khi lấy tham số form", objExce.ToString(), DUIInventory_Globals.ModuleName);
                    MessageBox.Show(this, "Lỗi khi lấy tham số form", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            else
            {
                MessageBox.Show(this, "Lỗi khi lấy tham số form", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            new ERP.MasterData.PLC.MD.PLCStoreChangeOrderType().LoadInfo(ref objStoreChangeOrderType, intStoreChangeOrderTypeID);
            if (objStoreChangeOrderType == null)
                return false;
            return true;
        }

        private void LoadDataIntock()
        {
            object[] objKeywords = new object[] { 
                      "@STOREID1", cboRealStore.StoreID,
                      "@STOREID2", cboAuxiliaryStore.StoreID,
                      "@MAINGROUPID", cboMainGroupID.MainGroupID,
                      "@SUBGROUPID", cboSubGroupIDList.SubGroupID,
                      "@BRANDID", cboBrandIDList.BrandID,
                      "@ISNEW", chkIsNew.Checked
                };
            dtbData = new PLC.StoreChange.PLCStoreChange().GetStoreChange_GetInstock_1(objKeywords);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                btnSearch.Enabled = true;
                return;
            }
            if (dtbData != null)
            {
                if (dtbData.Rows.Count > 0)
                {
                    dtbData.DefaultView.Sort = "PRODUCTID ASC, INPUTDATE ASC";
                    dtbData = dtbData.DefaultView.ToTable();
                    int intQuantityView = Convert.ToInt32(txtQuantity.Value);
                    string strProductID = string.Empty;
                    int intCount = 0;
                    for (int i = dtbData.Rows.Count - 1; i > -1; i--)
                    {
                        DataRow r = dtbData.Rows[i];
                        if (strProductID == string.Empty)
                        {
                            strProductID = r["PRODUCTID"].ToString();
                            intCount++;
                        }
                        else
                        {
                            if (strProductID == r["PRODUCTID"].ToString())
                            {
                                intCount++;
                                if (intCount > intQuantityView)
                                    dtbData.Rows.RemoveAt(i);
                            }
                            else
                            {
                                intCount = 0;
                                strProductID = r["PRODUCTID"].ToString();
                                intCount++;
                            }
                        }
                    }
                }
                dtbData.DefaultView.Sort = "PRODUCTID ASC, INPUTDATE DESC";
                dtbData = dtbData.DefaultView.ToTable();
                lblTotal.Text = "Tổng số dòng: " + dtbData.Rows.Count.ToString("##,###,##0");
            }
            intFromStoreID = cboRealStore.StoreID;
            intToStoreID = cboAuxiliaryStore.StoreID;
            bolIsNew = chkIsNew.Checked;
        }

        private void LoadCombobox()
        {
            
            ComboBox cboFromStoreID = new ComboBox();
            Library.AppCore.LoadControls.SetDataSource.SetStore(this, cboFromStoreID, true, -1, -1, -1, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER);
            DataTable dtbFromStoreID = (cboFromStoreID.DataSource as DataTable).Copy();
            dtbStoreData = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
            if (dtbFromStoreID != null)
            {
                DataTable dtbRealStore = Library.AppCore.DataTableClass.Select(dtbFromStoreID, "ISREALSTORE=1");
                if (dtbRealStore != null)
                {
                    cboRealStore.InitControl(false, dtbRealStore);
                    cboAuxiliaryStore.InitControl(false, dtbRealStore.Clone());
                }
            }
            cboMainGroupID.InitControl(false, true, Library.AppCore.Constant.EnumType.IsRequestIMEIType.REQUEST_IMEI);
            cboSubGroupIDList.InitControl(false, -1);
            cboBrandIDList.InitControl(false, -1);

            //Load phuowng tien van chuyen mac dinh
            DataTable dtbTransportType = Library.AppCore.DataSource.GetDataSource.GetTransportType();
            if (dtbTransportType != null && dtbTransportType.Rows.Count > 0)
            {
                DataRow[] drTransportTypeDefault = dtbTransportType.Select("IsDefault=1");
                if (drTransportTypeDefault.Length > 0)
                    intTransportTypeID = Convert.ToInt32(drTransportTypeDefault[0]["TransportTypeID"]);
                else
                    intTransportTypeID = Convert.ToInt32(dtbTransportType.Rows[0]["TransportTypeID"]);
            }
        }

        private void cboRealStore_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (dtbStoreData != null)
            {
                DataTable dtbAuxiliaryStore = Library.AppCore.DataTableClass.Select(dtbStoreData, string.Format(" HOSTSTOREID={0} and ISAUXILIARYSTORE=1", cboRealStore.StoreID));
                cboAuxiliaryStore.InitControl(false, dtbAuxiliaryStore);
            }
        }

        private void cboMainGroupID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cboSubGroupIDList.InitControl(false, cboMainGroupID.MainGroupID);
            cboBrandIDList.InitControl(false, cboMainGroupID.MainGroupID);
        }

        private bool CheckInput()
        {
            if (cboRealStore.StoreID <= 0)
            {
                MessageBox.Show(this,"Bạn chưa chọn kho chính!","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                cboRealStore.Focus();
                return false;
            }
            if (cboAuxiliaryStore.StoreID <= 0)
            {
                MessageBox.Show(this, "Bạn chưa chọn kho bày mẫu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboAuxiliaryStore.Focus();
                return false;
            }
      
            if (cboMainGroupID.MainGroupID < 1)
            {
                MessageBox.Show(this, "Bạn chưa chọn ngành hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMainGroupID.Focus();
                return false;
            }
            if (cboSubGroupIDList.SubGroupID<= 0)
            {
                MessageBox.Show(this, "Bạn chưa chọn nhóm hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboSubGroupIDList.Focus();
                return false;
            }
            return true;
        }
        private DataTable DataTableGroupBy(string strGroupByColumn, string strAggregateColumn, DataTable dtbSourceTable)
        {
            DataView dv = new DataView(dtbSourceTable);
            string[] strGroupByColumnList = strGroupByColumn.Split(',');
            DataTable dtGroup = dv.ToTable(true, strGroupByColumnList);
            if (strAggregateColumn != null && strAggregateColumn != string.Empty)
            {
                dtGroup.Columns.Add("Sum", typeof(Decimal));
                foreach (DataRow dr in dtGroup.Rows)
                {
                    dr["Sum"] = dtbSourceTable.Compute("Sum(" + strAggregateColumn + ")", strGroupByColumn + " = '" + dr[strGroupByColumn] + "'");
                }
            }
            return dtGroup;
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;
            if (!btnSearch.Enabled)
                return;
            try
            {
                btnSearch.Enabled = false;
                this.Cursor = Cursors.WaitCursor;
                LoadDataIntock();
                grdData.DataSource = dtbData;
                grdData.RefreshDataSource();
                btnSearch.Enabled = true;
                if (dtbData == null || dtbData.Rows.Count == 0)
                    btnCreateStoreChangeOrder.Enabled = false;
                else
                    btnCreateStoreChangeOrder.Enabled = true;
                this.Cursor = Cursors.Default;
            }
            catch (Exception objEx)
            {
                btnSearch.Enabled = true;
                this.Cursor = Cursors.Default;
                SystemErrorWS.Insert("Lỗi tìm kiếm đảo kho bày mẫu", objEx.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi tìm kiếm đảo kho bày mẫu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            objSelection.ClearSelection();
            timer1.Enabled = true;
            timer1.Start();
        }

        private void chkIsSelect_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit chk = (DevExpress.XtraEditors.CheckEdit)sender;
            if (chk != null)
            {
                if (grvData.FocusedRowHandle < 0)
                    return;
                DataRow row = grvData.GetDataRow(grvData.FocusedRowHandle);
                //if (Convert.ToBoolean(row["ISSTORECHANGEORDER"])&&chk.Checked)
                //{
                //    MessageBox.Show(this, "Sản phẩm này đã tạo yêu cầu chuyển kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    chk.Checked = false;
                //}
                row["ISSELECT"] = chk.Checked;
            }
            DataTable dtbData = grdData.DataSource as DataTable;
            if (dtbData != null)
            {
                if (dtbData.Select("ISSELECT=true").Length > 0)
                {
                    btnCreateStoreChangeOrder.Enabled = true;
                }
                else
                    btnCreateStoreChangeOrder.Enabled = false;
            }
        }

        private void btnCreateStoreChangeOrder_Click(object sender, EventArgs e)
        {
            if (!CheckInputCreateOrder())
                return;
            if (MessageBox.Show(this, "Bạn chắc chắn muốn tạo yêu cầu chuyển kho?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            CreateStoreChangeOrder();
        }

        /// <summary>
        /// Kiểm tra cùng tỉnh
        /// </summary>
        /// <param name="intFromStoreID">Mã kho xuất.</param>
        /// <param name="intToStoreID">Mã kho nhập.</param>
        private bool CheckSameProvince(int intFromStoreID, int intToStoreID)
        {
            ERP.MasterData.PLC.MD.WSStore.Store objFromStore = null;
            ERP.MasterData.PLC.MD.WSStore.ResultMessage objResultMessage = new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objFromStore, intFromStoreID);
            ERP.MasterData.PLC.MD.WSStore.Store objToStore = null;
            objResultMessage = new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, intToStoreID);
            return (objFromStore.ProvinceID == objToStore.ProvinceID);
        }

        /// <summary>
        /// Kiểm tra dữ liệu nhập vào
        /// </summary>
        private bool CheckInputCreateOrder()
        {
            if (objSelection.SelectedCount <= 0)
            {
                MessageBox.Show(this, "Bạn chưa chọn dòng nào để tạo yêu cầu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            bool bolIsSameProvince = CheckSameProvince(intFromStoreID, intToStoreID);
            if (objStoreChangeOrderType.StoreChangeTypeID == 1 && !bolIsSameProvince)
            {
                MessageBox.Show(this, "Kho xuất và kho nhập phải cùng tỉnh/thành phố", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;

            }
            if (objStoreChangeOrderType.StoreChangeTypeID == 2 && bolIsSameProvince)
            {
                MessageBox.Show(this, "Kho xuất và kho nhập phải khác tỉnh/thành phố", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }



        /// <summary>
        /// Tạo phiếu chuyển kho
        /// </summary>
        private void CreateStoreChangeOrder()
        {
            try
            {
                DataTable dtbDataDetail = dtbData.Clone();
                for (int i = 0; i < objSelection.SelectedCount; i++)
                {
                    dtbDataDetail.ImportRow(((System.Data.DataRowView)(objSelection.GetSelectedRow(i))).Row);
                }
                dtbDataDetail.TableName = "Detail";
                dtbDataDetail.AcceptChanges();
                //Thông tin phiếu chuyển kho
                PLC.StoreChange.PLCStoreChangeOrder objPLCStoreChangeOrder = new PLC.StoreChange.PLCStoreChangeOrder();
                ERP.Inventory.PLC.PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrderBO = new PLC.PM.WSStoreChangeOrder.StoreChangeOrder();
                objStoreChangeOrderBO.TransportTypeID = intTransportTypeID;
                objStoreChangeOrderBO.StoreChangeOrderTypeID = objStoreChangeOrderType.StoreChangeOrderTypeID;
                objStoreChangeOrderBO.StoreChangeTypeID = Convert.ToInt32(objStoreChangeOrderType.StoreChangeTypeID);
                objStoreChangeOrderBO.Content = string.Empty;
                objStoreChangeOrderBO.OrderDate = Library.AppCore.Globals.GetServerDateTime();
                objStoreChangeOrderBO.CreatedUser = SystemConfig.objSessionUser.UserName;
                objStoreChangeOrderBO.FromStoreID = intFromStoreID;
                objStoreChangeOrderBO.FromStoreName = cboRealStore.StoreName;
                objStoreChangeOrderBO.ToStoreID = intToStoreID;
                objStoreChangeOrderBO.ToStoreName = cboAuxiliaryStore.StoreName;
                objStoreChangeOrderBO.CreatedDate = Globals.GetServerDateTime();
                objStoreChangeOrderBO.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objStoreChangeOrderBO.ExpiryDate = Globals.GetServerDateTime().AddDays(2);
                objStoreChangeOrderBO.IsNew = bolIsNew;
                objStoreChangeOrderBO.IsReviewed = objStoreChangeOrderType.IsAutoReview;
                if (objStoreChangeOrderBO.IsReviewed)
                {
                    objStoreChangeOrderBO.ReviewedDate = Library.AppCore.Globals.GetServerDateTime();
                    objStoreChangeOrderBO.ReviewedUser = SystemConfig.objSessionUser.UserName;
                }
                objPLCStoreChangeOrder.CreateStoreChangeOrderShowProduct_1(objStoreChangeOrderBO, dtbDataDetail);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnCreateStoreChangeOrder.Enabled = true;
                    objSelection.ClearSelection();
                    return;
                }
                MessageBox.Show(this, "Tạo yêu cầu chuyển kho thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                objSelection.ClearSelection();
                btnSearch_Click(null, null);
                
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi cập nhật yêu cầu chuyển kho", objEx.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi cập nhật yêu cầu chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (objSelection != null && objSelection.SelectedCount >= 0)
                lblChoose.Text = "Tổng số dòng chọn: " + objSelection.SelectedCount.ToString("##,###,##0");
        }
    }
}
