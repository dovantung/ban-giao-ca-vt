﻿namespace ERP.Inventory.DUI.StoreChangeCommand
{
    partial class frmStoreChangeCommandManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStoreChangeCommandManager));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnCreateStoreChangeOrder = new System.Windows.Forms.Button();
            this.ucUserQuickSearch1 = new ERP.MasterData.DUI.CustomControl.User.ucUserQuickSearch();
            this.chkIsUrgent = new DevExpress.XtraEditors.CheckEdit();
            this.cboToStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.chkIsStoreChangeCommandID = new DevExpress.XtraEditors.CheckEdit();
            this.chkIsOrderID = new DevExpress.XtraEditors.CheckEdit();
            this.cboFromStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.btnSearch = new System.Windows.Forms.Button();
            this.cboStoreChangeCommandType = new System.Windows.Forms.ComboBox();
            this.cboIsCreateOrder = new System.Windows.Forms.ComboBox();
            this.dtToDate = new DevExpress.XtraEditors.DateEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.txtKeyword = new DevExpress.XtraEditors.TextEdit();
            this.dtFromDate = new DevExpress.XtraEditors.DateEdit();
            this.chkSearchProductName = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.flexListStoreChangeCommand = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.mnuPopUp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemDelete = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsUrgent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsStoreChangeCommandID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsOrderID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtToDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeyword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFromDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSearchProductName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexListStoreChangeCommand)).BeginInit();
            this.panel1.SuspendLayout();
            this.mnuPopUp.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.btnCreateStoreChangeOrder);
            this.groupControl1.Controls.Add(this.ucUserQuickSearch1);
            this.groupControl1.Controls.Add(this.chkIsUrgent);
            this.groupControl1.Controls.Add(this.cboToStoreID);
            this.groupControl1.Controls.Add(this.chkIsStoreChangeCommandID);
            this.groupControl1.Controls.Add(this.chkIsOrderID);
            this.groupControl1.Controls.Add(this.cboFromStoreID);
            this.groupControl1.Controls.Add(this.btnSearch);
            this.groupControl1.Controls.Add(this.cboStoreChangeCommandType);
            this.groupControl1.Controls.Add(this.cboIsCreateOrder);
            this.groupControl1.Controls.Add(this.dtToDate);
            this.groupControl1.Controls.Add(this.label10);
            this.groupControl1.Controls.Add(this.txtKeyword);
            this.groupControl1.Controls.Add(this.dtFromDate);
            this.groupControl1.Controls.Add(this.chkSearchProductName);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.label7);
            this.groupControl1.Controls.Add(this.label8);
            this.groupControl1.Controls.Add(this.label11);
            this.groupControl1.Controls.Add(this.label13);
            this.groupControl1.Controls.Add(this.label14);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.SkinName = "Blue";
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1027, 147);
            this.groupControl1.TabIndex = 5;
            this.groupControl1.Text = "Thông tin tìm kiếm";
            // 
            // btnCreateStoreChangeOrder
            // 
            this.btnCreateStoreChangeOrder.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.btnCreateStoreChangeOrder.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCreateStoreChangeOrder.Location = new System.Drawing.Point(782, 113);
            this.btnCreateStoreChangeOrder.Name = "btnCreateStoreChangeOrder";
            this.btnCreateStoreChangeOrder.Size = new System.Drawing.Size(106, 25);
            this.btnCreateStoreChangeOrder.TabIndex = 57;
            this.btnCreateStoreChangeOrder.Text = "Tạo yêu cầu";
            this.btnCreateStoreChangeOrder.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCreateStoreChangeOrder.UseVisualStyleBackColor = true;
            this.btnCreateStoreChangeOrder.Click += new System.EventHandler(this.btnCreateStoreChangeOrder_Click);
            // 
            // ucUserQuickSearch1
            // 
            this.ucUserQuickSearch1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucUserQuickSearch1.IsValidate = true;
            this.ucUserQuickSearch1.Location = new System.Drawing.Point(95, 85);
            this.ucUserQuickSearch1.Margin = new System.Windows.Forms.Padding(0);
            this.ucUserQuickSearch1.MaximumSize = new System.Drawing.Size(400, 22);
            this.ucUserQuickSearch1.MinimumSize = new System.Drawing.Size(0, 22);
            this.ucUserQuickSearch1.Name = "ucUserQuickSearch1";
            this.ucUserQuickSearch1.Size = new System.Drawing.Size(226, 22);
            this.ucUserQuickSearch1.TabIndex = 56;
            this.ucUserQuickSearch1.UserName = "";
            // 
            // chkIsUrgent
            // 
            this.chkIsUrgent.Location = new System.Drawing.Point(685, 27);
            this.chkIsUrgent.Name = "chkIsUrgent";
            this.chkIsUrgent.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsUrgent.Properties.Appearance.Options.UseFont = true;
            this.chkIsUrgent.Properties.Caption = "Chỉ hiển thị lệnh chuyển gấp";
            this.chkIsUrgent.Properties.LookAndFeel.SkinName = "Blue";
            this.chkIsUrgent.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.chkIsUrgent.Size = new System.Drawing.Size(189, 21);
            this.chkIsUrgent.TabIndex = 53;
            // 
            // cboToStoreID
            // 
            this.cboToStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboToStoreID.Location = new System.Drawing.Point(406, 55);
            this.cboToStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboToStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboToStoreID.Name = "cboToStoreID";
            this.cboToStoreID.Size = new System.Drawing.Size(226, 24);
            this.cboToStoreID.TabIndex = 55;
            // 
            // chkIsStoreChangeCommandID
            // 
            this.chkIsStoreChangeCommandID.Location = new System.Drawing.Point(337, 27);
            this.chkIsStoreChangeCommandID.Name = "chkIsStoreChangeCommandID";
            this.chkIsStoreChangeCommandID.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsStoreChangeCommandID.Properties.Appearance.Options.UseFont = true;
            this.chkIsStoreChangeCommandID.Properties.Caption = "Tìm theo mã lệnh chuyển";
            this.chkIsStoreChangeCommandID.Properties.LookAndFeel.SkinName = "Blue";
            this.chkIsStoreChangeCommandID.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.chkIsStoreChangeCommandID.Size = new System.Drawing.Size(177, 21);
            this.chkIsStoreChangeCommandID.TabIndex = 53;
            // 
            // chkIsOrderID
            // 
            this.chkIsOrderID.Location = new System.Drawing.Point(520, 27);
            this.chkIsOrderID.Name = "chkIsOrderID";
            this.chkIsOrderID.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsOrderID.Properties.Appearance.Options.UseFont = true;
            this.chkIsOrderID.Properties.Caption = "Tìm theo mã đơn hàng";
            this.chkIsOrderID.Properties.LookAndFeel.SkinName = "Blue";
            this.chkIsOrderID.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.chkIsOrderID.Size = new System.Drawing.Size(159, 21);
            this.chkIsOrderID.TabIndex = 53;
            // 
            // cboFromStoreID
            // 
            this.cboFromStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFromStoreID.Location = new System.Drawing.Point(95, 55);
            this.cboFromStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboFromStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboFromStoreID.Name = "cboFromStoreID";
            this.cboFromStoreID.Size = new System.Drawing.Size(226, 24);
            this.cboFromStoreID.TabIndex = 54;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(902, 113);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(106, 25);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cboStoreChangeCommandType
            // 
            this.cboStoreChangeCommandType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStoreChangeCommandType.FormattingEnabled = true;
            this.cboStoreChangeCommandType.Items.AddRange(new object[] {
            "--Tất cả--",
            "Chưa tạo yêu cầu",
            "Đã tạo yêu cầu"});
            this.cboStoreChangeCommandType.Location = new System.Drawing.Point(782, 54);
            this.cboStoreChangeCommandType.Name = "cboStoreChangeCommandType";
            this.cboStoreChangeCommandType.Size = new System.Drawing.Size(226, 24);
            this.cboStoreChangeCommandType.TabIndex = 12;
            // 
            // cboIsCreateOrder
            // 
            this.cboIsCreateOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIsCreateOrder.FormattingEnabled = true;
            this.cboIsCreateOrder.Items.AddRange(new object[] {
            "--Tất cả--",
            "Chưa tạo yêu cầu",
            "Đã tạo yêu cầu"});
            this.cboIsCreateOrder.Location = new System.Drawing.Point(782, 84);
            this.cboIsCreateOrder.Name = "cboIsCreateOrder";
            this.cboIsCreateOrder.Size = new System.Drawing.Size(226, 24);
            this.cboIsCreateOrder.TabIndex = 12;
            // 
            // dtToDate
            // 
            this.dtToDate.EditValue = null;
            this.dtToDate.Location = new System.Drawing.Point(522, 86);
            this.dtToDate.Name = "dtToDate";
            this.dtToDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtToDate.Properties.Appearance.Options.UseFont = true;
            this.dtToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtToDate.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtToDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtToDate.Properties.LookAndFeel.SkinName = "Blue";
            this.dtToDate.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dtToDate.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtToDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtToDate.Size = new System.Drawing.Size(110, 22);
            this.dtToDate.TabIndex = 52;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 16);
            this.label10.TabIndex = 0;
            this.label10.Text = "Từ khóa:";
            // 
            // txtKeyword
            // 
            this.txtKeyword.Location = new System.Drawing.Point(95, 28);
            this.txtKeyword.Name = "txtKeyword";
            this.txtKeyword.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtKeyword.Properties.Appearance.Options.UseFont = true;
            this.txtKeyword.Properties.LookAndFeel.SkinName = "Blue";
            this.txtKeyword.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtKeyword.Properties.MaxLength = 20;
            this.txtKeyword.Size = new System.Drawing.Size(226, 22);
            this.txtKeyword.TabIndex = 11;
            // 
            // dtFromDate
            // 
            this.dtFromDate.EditValue = null;
            this.dtFromDate.Location = new System.Drawing.Point(406, 86);
            this.dtFromDate.Name = "dtFromDate";
            this.dtFromDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFromDate.Properties.Appearance.Options.UseFont = true;
            this.dtFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtFromDate.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.dtFromDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtFromDate.Properties.LookAndFeel.SkinName = "Blue";
            this.dtFromDate.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dtFromDate.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dtFromDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtFromDate.Size = new System.Drawing.Size(110, 22);
            this.dtFromDate.TabIndex = 52;
            // 
            // chkSearchProductName
            // 
            this.chkSearchProductName.Location = new System.Drawing.Point(887, 27);
            this.chkSearchProductName.Name = "chkSearchProductName";
            this.chkSearchProductName.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSearchProductName.Properties.Appearance.Options.UseFont = true;
            this.chkSearchProductName.Properties.Caption = "Tên sản phẩm";
            this.chkSearchProductName.Properties.LookAndFeel.SkinName = "Blue";
            this.chkSearchProductName.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.chkSearchProductName.Size = new System.Drawing.Size(125, 21);
            this.chkSearchProductName.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(647, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 16);
            this.label1.TabIndex = 18;
            this.label1.Text = "Loại lệnh chuyển kho:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(329, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 16);
            this.label7.TabIndex = 21;
            this.label7.Text = "Kho nhập:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(647, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 16);
            this.label8.TabIndex = 18;
            this.label8.Text = "Trạng thái:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 88);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 16);
            this.label11.TabIndex = 4;
            this.label11.Text = "Người tạo:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 16);
            this.label13.TabIndex = 2;
            this.label13.Text = "Kho xuất:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(339, 88);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 16);
            this.label14.TabIndex = 15;
            this.label14.Text = "Ngày tạo:";
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.flexListStoreChangeCommand);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 147);
            this.groupControl2.LookAndFeel.SkinName = "Blue";
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1027, 305);
            this.groupControl2.TabIndex = 54;
            this.groupControl2.Text = "Thông tin tìm kiếm";
            // 
            // flexListStoreChangeCommand
            // 
            this.flexListStoreChangeCommand.ColumnInfo = "9,0,0,0,0,105,Columns:";
            this.flexListStoreChangeCommand.ContextMenuStrip = this.mnuPopUp;
            this.flexListStoreChangeCommand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexListStoreChangeCommand.Location = new System.Drawing.Point(2, 23);
            this.flexListStoreChangeCommand.Name = "flexListStoreChangeCommand";
            this.flexListStoreChangeCommand.Rows.Count = 1;
            this.flexListStoreChangeCommand.Rows.DefaultSize = 21;
            this.flexListStoreChangeCommand.Size = new System.Drawing.Size(1023, 280);
            this.flexListStoreChangeCommand.StyleInfo = resources.GetString("flexListStoreChangeCommand.StyleInfo");
            this.flexListStoreChangeCommand.TabIndex = 1;
            this.flexListStoreChangeCommand.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            this.flexListStoreChangeCommand.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexListStoreChangeCommand_BeforeEdit);
            this.flexListStoreChangeCommand.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexListStoreChangeCommand_AfterEdit);
            this.flexListStoreChangeCommand.SetupEditor += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexListStoreChangeCommand_SetupEditor);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(921, 3);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(103, 25);
            this.btnUpdate.TabIndex = 1;
            this.btnUpdate.Text = "Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 452);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1027, 31);
            this.panel1.TabIndex = 55;
            // 
            // mnuPopUp
            // 
            this.mnuPopUp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemDelete});
            this.mnuPopUp.Name = "mnuPopUp";
            this.mnuPopUp.Size = new System.Drawing.Size(210, 26);
            // 
            // mnuItemDelete
            // 
            this.mnuItemDelete.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemDelete.Image")));
            this.mnuItemDelete.Name = "mnuItemDelete";
            this.mnuItemDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.mnuItemDelete.Size = new System.Drawing.Size(209, 22);
            this.mnuItemDelete.Text = "Xóa lệnh chuyển kho";
            this.mnuItemDelete.Click += new System.EventHandler(this.mnuItemDelete_Click);
            // 
            // frmStoreChangeCommandManager
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1027, 483);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmStoreChangeCommandManager";
            this.ShowIcon = false;
            this.Text = "Quản lý lệnh xuất chuyển kho";
            this.Load += new System.EventHandler(this.frmStoreChangeCommandManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsUrgent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsStoreChangeCommandID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsOrderID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtToDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeyword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFromDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSearchProductName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexListStoreChangeCommand)).EndInit();
            this.panel1.ResumeLayout(false);
            this.mnuPopUp.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit chkIsUrgent;
        private DevExpress.XtraEditors.CheckEdit chkIsOrderID;
        private System.Windows.Forms.ComboBox cboIsCreateOrder;
        private DevExpress.XtraEditors.DateEdit dtToDate;
        private DevExpress.XtraEditors.CheckEdit chkIsStoreChangeCommandID;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.TextEdit txtKeyword;
        private DevExpress.XtraEditors.DateEdit dtFromDate;
        private DevExpress.XtraEditors.CheckEdit chkSearchProductName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Panel panel1;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboToStoreID;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboFromStoreID;
        private C1.Win.C1FlexGrid.C1FlexGrid flexListStoreChangeCommand;
        private System.Windows.Forms.ContextMenuStrip mnuPopUp;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDelete;
        private MasterData.DUI.CustomControl.User.ucUserQuickSearch ucUserQuickSearch1;
        private System.Windows.Forms.Button btnCreateStoreChangeOrder;
        private System.Windows.Forms.ComboBox cboStoreChangeCommandType;
        private System.Windows.Forms.Label label1;
    }
}