﻿namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    partial class frmAutoSplitProductDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAutoSplitProductDetail));
            this.txtTotalQuantity = new C1.Win.C1Input.C1NumericEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.txtQuantity = new C1.Win.C1Input.C1NumericEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOrderID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.flexListAutoSplitProduct = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cboProduct = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cboToStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.cboFromStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.btnAdd = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.btnCreateStoreChangeCommand = new System.Windows.Forms.Button();
            this.mnuAddProduct = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemAddProduct = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexListAutoSplitProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.panel1.SuspendLayout();
            this.mnuAddProduct.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtTotalQuantity
            // 
            this.txtTotalQuantity.BackColor = System.Drawing.SystemColors.Info;
            this.txtTotalQuantity.CustomFormat = "###,##0.####";
            this.txtTotalQuantity.DisabledForeColor = System.Drawing.SystemColors.Info;
            this.txtTotalQuantity.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtTotalQuantity.Location = new System.Drawing.Point(584, 27);
            this.txtTotalQuantity.Name = "txtTotalQuantity";
            this.txtTotalQuantity.ReadOnly = true;
            this.txtTotalQuantity.Size = new System.Drawing.Size(254, 22);
            this.txtTotalQuantity.TabIndex = 3;
            this.txtTotalQuantity.Tag = null;
            this.txtTotalQuantity.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTotalQuantity.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "Sản phẩm:";
            // 
            // txtQuantity
            // 
            this.txtQuantity.CustomFormat = "###,##0.####";
            this.txtQuantity.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtQuantity.Location = new System.Drawing.Point(584, 84);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.NumericInputKeys = ((C1.Win.C1Input.NumericInputKeyFlags)((((C1.Win.C1Input.NumericInputKeyFlags.F9 | C1.Win.C1Input.NumericInputKeyFlags.Plus) 
            | C1.Win.C1Input.NumericInputKeyFlags.Decimal) 
            | C1.Win.C1Input.NumericInputKeyFlags.X)));
            this.txtQuantity.Size = new System.Drawing.Size(150, 22);
            this.txtQuantity.TabIndex = 11;
            this.txtQuantity.Tag = null;
            this.txtQuantity.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtQuantity.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(447, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "Số lượng chuyển:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(447, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "Đến kho:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(54, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 16);
            this.label7.TabIndex = 4;
            this.label7.Text = "Từ kho:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(447, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tổng số lượng:";
            // 
            // txtOrderID
            // 
            this.txtOrderID.BackColor = System.Drawing.SystemColors.Info;
            this.txtOrderID.Location = new System.Drawing.Point(180, 27);
            this.txtOrderID.Name = "txtOrderID";
            this.txtOrderID.ReadOnly = true;
            this.txtOrderID.Size = new System.Drawing.Size(249, 22);
            this.txtOrderID.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã đơn hàng:";
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.groupControl2.Controls.Add(this.flexListAutoSplitProduct);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 115);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(959, 366);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Danh sách sản phẩm chia hàng";
            // 
            // flexListAutoSplitProduct
            // 
            this.flexListAutoSplitProduct.AutoClipboard = true;
            this.flexListAutoSplitProduct.ColumnInfo = "9,0,0,0,0,105,Columns:";
            this.flexListAutoSplitProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexListAutoSplitProduct.Location = new System.Drawing.Point(2, 22);
            this.flexListAutoSplitProduct.Name = "flexListAutoSplitProduct";
            this.flexListAutoSplitProduct.Rows.Count = 1;
            this.flexListAutoSplitProduct.Rows.DefaultSize = 21;
            this.flexListAutoSplitProduct.Size = new System.Drawing.Size(955, 342);
            this.flexListAutoSplitProduct.StyleInfo = resources.GetString("flexListAutoSplitProduct.StyleInfo");
            this.flexListAutoSplitProduct.TabIndex = 0;
            this.flexListAutoSplitProduct.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            this.flexListAutoSplitProduct.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexListAutoSplitProduct_BeforeEdit);
            this.flexListAutoSplitProduct.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexListAutoSplitProduct_AfterEdit);
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.groupControl1.Controls.Add(this.cboProduct);
            this.groupControl1.Controls.Add(this.cboToStoreID);
            this.groupControl1.Controls.Add(this.cboFromStoreID);
            this.groupControl1.Controls.Add(this.txtOrderID);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.txtTotalQuantity);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.label7);
            this.groupControl1.Controls.Add(this.label6);
            this.groupControl1.Controls.Add(this.btnAdd);
            this.groupControl1.Controls.Add(this.txtQuantity);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(959, 115);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin";
            // 
            // cboProduct
            // 
            this.cboProduct.Location = new System.Drawing.Point(180, 83);
            this.cboProduct.Name = "cboProduct";
            this.cboProduct.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.cboProduct.Properties.Appearance.Options.UseFont = true;
            this.cboProduct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboProduct.Properties.DisplayMember = "PRODUCTNAME";
            this.cboProduct.Properties.NullText = "Chọn sản phẩm";
            this.cboProduct.Properties.NullValuePromptShowForEmptyValue = true;
            this.cboProduct.Properties.ValueMember = "PRODUCTID";
            this.cboProduct.Properties.View = this.gridView2;
            this.cboProduct.Size = new System.Drawing.Size(248, 22);
            this.cboProduct.TabIndex = 9;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn12,
            this.gridColumn13});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Mã sản phẩm";
            this.gridColumn12.FieldName = "PRODUCTID";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 0;
            this.gridColumn12.Width = 159;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Tên sản phẩm";
            this.gridColumn13.FieldName = "PRODUCTNAME";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 1;
            this.gridColumn13.Width = 959;
            // 
            // cboToStoreID
            // 
            this.cboToStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboToStoreID.Location = new System.Drawing.Point(584, 52);
            this.cboToStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboToStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboToStoreID.Name = "cboToStoreID";
            this.cboToStoreID.Size = new System.Drawing.Size(254, 24);
            this.cboToStoreID.TabIndex = 7;
            // 
            // cboFromStoreID
            // 
            this.cboFromStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFromStoreID.Location = new System.Drawing.Point(180, 52);
            this.cboFromStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboFromStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboFromStoreID.Name = "cboFromStoreID";
            this.cboFromStoreID.Size = new System.Drawing.Size(249, 24);
            this.cboFromStoreID.TabIndex = 5;
            // 
            // btnAdd
            // 
            this.btnAdd.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(740, 82);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(98, 25);
            this.btnAdd.TabIndex = 12;
            this.btnAdd.Text = "Thêm SP";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnExportExcel);
            this.panel1.Controls.Add(this.btnCreateStoreChangeCommand);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 481);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(959, 32);
            this.panel1.TabIndex = 2;
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportExcel.Location = new System.Drawing.Point(739, 3);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(105, 25);
            this.btnExportExcel.TabIndex = 1;
            this.btnExportExcel.Text = "   Xuất Excel";
            this.btnExportExcel.UseVisualStyleBackColor = true;
            this.btnExportExcel.Click += new System.EventHandler(this.cmdExportExcel_Click);
            // 
            // btnCreateStoreChangeCommand
            // 
            this.btnCreateStoreChangeCommand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreateStoreChangeCommand.Image = global::ERP.Inventory.DUI.Properties.Resources.update;
            this.btnCreateStoreChangeCommand.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCreateStoreChangeCommand.Location = new System.Drawing.Point(850, 3);
            this.btnCreateStoreChangeCommand.Name = "btnCreateStoreChangeCommand";
            this.btnCreateStoreChangeCommand.Size = new System.Drawing.Size(103, 25);
            this.btnCreateStoreChangeCommand.TabIndex = 0;
            this.btnCreateStoreChangeCommand.Text = "Cập nhật";
            this.btnCreateStoreChangeCommand.UseVisualStyleBackColor = true;
            this.btnCreateStoreChangeCommand.Click += new System.EventHandler(this.btnCreateStoreChangeCommand_Click);
            // 
            // mnuAddProduct
            // 
            this.mnuAddProduct.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemAddProduct});
            this.mnuAddProduct.Name = "mnuAddProduct";
            this.mnuAddProduct.Size = new System.Drawing.Size(161, 26);
            // 
            // mnuItemAddProduct
            // 
            this.mnuItemAddProduct.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemAddProduct.Image")));
            this.mnuItemAddProduct.Name = "mnuItemAddProduct";
            this.mnuItemAddProduct.Size = new System.Drawing.Size(160, 22);
            this.mnuItemAddProduct.Text = "Thêm sản phẩm";
            // 
            // frmAutoSplitProductDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 513);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmAutoSplitProductDetail";
            this.ShowIcon = false;
            this.Text = "Chi tiết chia hàng tự động từ một đơn đặt hàng";
            this.Load += new System.EventHandler(this.frmAutoSplitProductDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexListAutoSplitProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.mnuAddProduct.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private C1.Win.C1Input.C1NumericEdit txtTotalQuantity;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnExportExcel;
        private System.Windows.Forms.Button btnCreateStoreChangeCommand;
        private System.Windows.Forms.Button btnAdd;
        private C1.Win.C1Input.C1NumericEdit txtQuantity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtOrderID;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Panel panel1;
        private C1.Win.C1FlexGrid.C1FlexGrid flexListAutoSplitProduct;
        private System.Windows.Forms.ContextMenuStrip mnuAddProduct;
        private System.Windows.Forms.ToolStripMenuItem mnuItemAddProduct;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboToStoreID;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboFromStoreID;
        private DevExpress.XtraEditors.SearchLookUpEdit cboProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;

    }
}