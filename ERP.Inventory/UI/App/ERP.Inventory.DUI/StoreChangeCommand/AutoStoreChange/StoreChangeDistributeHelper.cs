﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    public static class StoreChangeDistributeHelper
    {
        //public static string GetString(this object obj)
        //{
        //    return obj.IsNull() ? string.Empty : obj.ToString();
        //}
        //public static bool IsNull(this object obj)
        //{
        //    return obj == null;
        //}
        //public static bool IsNumber(this object obj)
        //{
        //    if (string.IsNullOrEmpty(obj.GetString()))
        //        return false;
        //    Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
        //    return regex.IsMatch(obj.GetString());
        //}
        public static List<dynamic> EnumToList<TEnum>() where TEnum : struct, IConvertible
        {
            List<object> result = new List<object>();
            foreach (var @enum in Enum.GetValues(typeof(TEnum)))
            {
                result.Add(new
                {
                    DisplayText = ((TEnum)@enum).ToString().Translate(),
                    BehideValue = Convert.ToInt32(((TEnum)@enum))
                });
            }
            return result;
        }
        public static DataTable EnumToDataTable<TEnum>() where TEnum : struct, IConvertible
        {
            DataTable result = new DataTable();
            result.Columns.Add("DisplayText", typeof(string));
            result.Columns.Add("BehideValue", typeof(int));
            foreach (var @enum in Enum.GetValues(typeof(TEnum)))
            {
                DataRow row = result.NewRow();
                row["DisplayText"] = ((TEnum)@enum).ToString().Translate();
                row["BehideValue"] = Convert.ToInt32(((TEnum)@enum));
                result.Rows.Add(row);
            }
            return result;
        }
        public static string Translate(this string word, string languageCode = "VN")
        {
            switch (languageCode)
            {
                case "VN":
                    {
                        switch (word)
                        {
                            case "Hide": { return "Ẩn"; }
                            case "OnlySampleProduct": { return "Chỉ hiển thị hàng trưng bày"; }
                            case "All": { return "Tất cả"; }
                            case "Send": { return "Chuyển đi"; }
                            case "Receive": { return "Nhận về"; }
                            case "IMEI": { return "Theo IMEI"; }
                            case "Quantity": { return "Theo số lượng"; }
                            case "MultipleStore": { return "Chuyển nhiều kho"; }
                            default: { return word; }
                        }
                    }
                case "EN":
                    {
                        switch (word.ToLower())
                        {
                            default: { return word; }
                        }
                    }
                default: { return "LanguageCode not supported"; }
            }
        }
        
    }
    public class PageDataInfo
    {
        public PageDataInfo(int pageSize)
        {
            this.PageSize = pageSize;
            this.PageIndex = 0;
            this.TotaPage = 0;
        }
        public double TotaPage { get; set; }
        public double PageIndex { get; set; }
        public double PageSize { get; set; }
        public string ProgressMessage { get; set; }
        public double GetPercent()
        {
            double per1 = (this.TotaPage / this.PageIndex);
            double percent = Convert.ToInt32(100 / per1);
            return percent;
        }
    }
    public static class XlsMultiColumnMapper
    {

        //orignal columns
        public const int FromStoreID = 0;
        public const int ProductID = 1;
        public const int ProductState = 2;
        public const int Quantity = 3;
        public const int ToStoreID = 4;

        public const int ProductName = 5;

        public const int FromStoreName = 6;
        public const int ToStoreName = 7;
        public const int FromInStockQuantity = 8;
        public const int ToInStockQuantity = 9;
        public const int IMEI = 10;
        public const int IsUrgent = 11;
        public const int AutoQuantity = 12;
        public const int ProductStateName = 13;

        public const int Status = 14;
        public const int StatusID = 15;
        public const int ErrorContent = 16;
    }

    public enum ERecordImportStatus
    {
        Invalid = 0,
        IsValid = 1
    }

    public enum EActionType
    {
        //Chuyển theo IMEI
        IMEI = 1,
        //Chuyển 1 kho tới nhiều kho theo mã sản phẩm
        Quantity = 0,
        //Chuyển nhiều kho tới nhiều kho theo mã sản phẩm
        MultipleStore = 2
    }
    //Hình thức chuyển
    public enum ETransferType
    {
        //Chuyển đi
        Send = 0,
        //Nhận về
        Receive = 1
    }
}

