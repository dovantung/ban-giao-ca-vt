﻿namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    partial class frmAutoStoreChangeDetail_WithIMEI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAutoStoreChangeDetail_WithIMEI));
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.grdListAutoStoreChange = new DevExpress.XtraGrid.GridControl();
            this.mnuGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemUnSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDeleteRow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.grvListAutoStoreChange = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtItemQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txtTaxedFee = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txtInputPrice = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.flexExportTemplate = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.txtProductID = new System.Windows.Forms.TextBox();
            this.btnSelectProduct = new System.Windows.Forms.Button();
            this.grpSearch = new DevExpress.XtraEditors.GroupControl();
            this.txtCommandID = new DevExpress.XtraEditors.TextEdit();
            this.dropDownButton1 = new DevExpress.XtraEditors.DropDownButton();
            this.pMnuImport = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnAction = new DevExpress.XtraEditors.DropDownButton();
            this.pMnuExport = new DevExpress.XtraBars.PopupMenu(this.components);
            this.cboProductList = new ERP.MasterData.DUI.CustomControl.Product.cboProductList();
            this.chkIsCanUseOldCode = new System.Windows.Forms.CheckBox();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chkIsCheckRealInput = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cboIsShowProduct = new System.Windows.Forms.ComboBox();
            this.btnHintStock = new System.Windows.Forms.Button();
            this.btnExportTemplate = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cboMainGroupID = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.cmdCreateStoreChangeCommand = new System.Windows.Forms.Button();
            this.cboToStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.cboFromStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.txtQuantity = new C1.Win.C1Input.C1NumericEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmdAdd = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblStore2 = new System.Windows.Forms.Label();
            this.lblStore1 = new System.Windows.Forms.Label();
            this.cmdImportExcel = new System.Windows.Forms.Button();
            this.cachedrptInputVoucherReport1 = new ERP.Inventory.DUI.Reports.Input.CachedrptInputVoucherReport();
            this.cachedrptInputVoucherReport2 = new ERP.Inventory.DUI.Reports.Input.CachedrptInputVoucherReport();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdListAutoStoreChange)).BeginInit();
            this.mnuGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvListAutoStoreChange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxedFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flexExportTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpSearch)).BeginInit();
            this.grpSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommandID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuImport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.groupControl2.Controls.Add(this.grdData);
            this.groupControl2.Controls.Add(this.grdListAutoStoreChange);
            this.groupControl2.Controls.Add(this.flexExportTemplate);
            this.groupControl2.Controls.Add(this.txtProductID);
            this.groupControl2.Controls.Add(this.btnSelectProduct);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 115);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1127, 402);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Danh sách sản phẩm chia hàng";
            // 
            // grdData
            // 
            this.grdData.Location = new System.Drawing.Point(678, 69);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3});
            this.grdData.Size = new System.Drawing.Size(353, 144);
            this.grdData.TabIndex = 6;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            this.grdData.Visible = false;
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Preview.Options.UseFont = true;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsPrint.AutoWidth = false;
            this.grdViewData.OptionsPrint.PrintHeader = false;
            this.grdViewData.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.grdViewData.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewData.OptionsView.ShowFooter = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit2.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit3.ValueUnchecked = ((short)(0));
            // 
            // grdListAutoStoreChange
            // 
            this.grdListAutoStoreChange.ContextMenuStrip = this.mnuGrid;
            this.grdListAutoStoreChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdListAutoStoreChange.Location = new System.Drawing.Point(2, 22);
            this.grdListAutoStoreChange.MainView = this.grvListAutoStoreChange;
            this.grdListAutoStoreChange.Name = "grdListAutoStoreChange";
            this.grdListAutoStoreChange.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.txtItemQuantity,
            this.txtTaxedFee,
            this.txtInputPrice});
            this.grdListAutoStoreChange.Size = new System.Drawing.Size(1123, 378);
            this.grdListAutoStoreChange.TabIndex = 2;
            this.grdListAutoStoreChange.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvListAutoStoreChange});
            // 
            // mnuGrid
            // 
            this.mnuGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemSelectAll,
            this.mnuItemUnSelectAll,
            this.mnuDeleteRow,
            this.toolStripSeparator1,
            this.mnuExportExcel});
            this.mnuGrid.Name = "mnuGrid";
            this.mnuGrid.Size = new System.Drawing.Size(151, 98);
            this.mnuGrid.Opening += new System.ComponentModel.CancelEventHandler(this.mnuGrid_Opening);
            // 
            // mnuItemSelectAll
            // 
            this.mnuItemSelectAll.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.mnuItemSelectAll.Name = "mnuItemSelectAll";
            this.mnuItemSelectAll.Size = new System.Drawing.Size(150, 22);
            this.mnuItemSelectAll.Text = "Chọn tất cả";
            this.mnuItemSelectAll.Click += new System.EventHandler(this.mnuItemSelectAll_Click);
            // 
            // mnuItemUnSelectAll
            // 
            this.mnuItemUnSelectAll.Image = global::ERP.Inventory.DUI.Properties.Resources.undo;
            this.mnuItemUnSelectAll.Name = "mnuItemUnSelectAll";
            this.mnuItemUnSelectAll.Size = new System.Drawing.Size(150, 22);
            this.mnuItemUnSelectAll.Text = "Bỏ chọn tất cả";
            this.mnuItemUnSelectAll.Click += new System.EventHandler(this.mnuItemUnSelectAll_Click);
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Size = new System.Drawing.Size(150, 22);
            this.mnuDeleteRow.Text = "Xóa dòng này";
            this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(147, 6);
            // 
            // mnuExportExcel
            // 
            this.mnuExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuExportExcel.Name = "mnuExportExcel";
            this.mnuExportExcel.Size = new System.Drawing.Size(150, 22);
            this.mnuExportExcel.Text = "Xuất Excel";
            this.mnuExportExcel.Click += new System.EventHandler(this.mnuExportExcel_Click);
            // 
            // grvListAutoStoreChange
            // 
            this.grvListAutoStoreChange.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvListAutoStoreChange.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvListAutoStoreChange.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvListAutoStoreChange.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvListAutoStoreChange.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grvListAutoStoreChange.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvListAutoStoreChange.Appearance.Row.Options.UseFont = true;
            this.grvListAutoStoreChange.ColumnPanelRowHeight = 40;
            this.grvListAutoStoreChange.GridControl = this.grdListAutoStoreChange;
            this.grvListAutoStoreChange.Name = "grvListAutoStoreChange";
            this.grvListAutoStoreChange.OptionsNavigation.UseTabKey = false;
            this.grvListAutoStoreChange.OptionsPrint.AutoWidth = false;
            this.grvListAutoStoreChange.OptionsView.ColumnAutoWidth = false;
            this.grvListAutoStoreChange.OptionsView.ShowFooter = true;
            this.grvListAutoStoreChange.OptionsView.ShowGroupPanel = false;
            this.grvListAutoStoreChange.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grvListAutoStoreChange_ShowingEditor);
            this.grvListAutoStoreChange.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.grvListAutoStoreChange_InvalidRowException);
            this.grvListAutoStoreChange.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.grvListAutoStoreChange_ValidateRow);
            // 
            // txtItemQuantity
            // 
            this.txtItemQuantity.AutoHeight = false;
            this.txtItemQuantity.DisplayFormat.FormatString = "#,##0.####";
            this.txtItemQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtItemQuantity.Mask.EditMask = "###,###,###,##0.####";
            this.txtItemQuantity.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtItemQuantity.Name = "txtItemQuantity";
            // 
            // txtTaxedFee
            // 
            this.txtTaxedFee.AutoHeight = false;
            this.txtTaxedFee.DisplayFormat.FormatString = "#,##0.##";
            this.txtTaxedFee.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTaxedFee.Mask.EditMask = "###,###,###,##0.####";
            this.txtTaxedFee.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTaxedFee.Name = "txtTaxedFee";
            // 
            // txtInputPrice
            // 
            this.txtInputPrice.AutoHeight = false;
            this.txtInputPrice.DisplayFormat.FormatString = "#,##0.####";
            this.txtInputPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtInputPrice.Mask.EditMask = "###,###,###,##0.####";
            this.txtInputPrice.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtInputPrice.Name = "txtInputPrice";
            // 
            // flexExportTemplate
            // 
            this.flexExportTemplate.AllowMerging = C1.Win.C1FlexGrid.AllowMergingEnum.FixedOnly;
            this.flexExportTemplate.AutoClipboard = true;
            this.flexExportTemplate.ColumnInfo = "9,0,0,0,0,105,Columns:";
            this.flexExportTemplate.Location = new System.Drawing.Point(2, 25);
            this.flexExportTemplate.Name = "flexExportTemplate";
            this.flexExportTemplate.Rows.Count = 1;
            this.flexExportTemplate.Rows.DefaultSize = 21;
            this.flexExportTemplate.Size = new System.Drawing.Size(666, 40);
            this.flexExportTemplate.StyleInfo = resources.GetString("flexExportTemplate.StyleInfo");
            this.flexExportTemplate.TabIndex = 1;
            this.flexExportTemplate.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            // 
            // txtProductID
            // 
            this.txtProductID.BackColor = System.Drawing.SystemColors.Window;
            this.txtProductID.Location = new System.Drawing.Point(370, 0);
            this.txtProductID.MaxLength = 20;
            this.txtProductID.Name = "txtProductID";
            this.txtProductID.Size = new System.Drawing.Size(184, 22);
            this.txtProductID.TabIndex = 100;
            this.txtProductID.Visible = false;
            // 
            // btnSelectProduct
            // 
            this.btnSelectProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnSelectProduct.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSelectProduct.Location = new System.Drawing.Point(553, -1);
            this.btnSelectProduct.Name = "btnSelectProduct";
            this.btnSelectProduct.Size = new System.Drawing.Size(25, 25);
            this.btnSelectProduct.TabIndex = 5;
            this.btnSelectProduct.UseVisualStyleBackColor = true;
            this.btnSelectProduct.Visible = false;
            this.btnSelectProduct.Click += new System.EventHandler(this.btnSelectProduct_Click);
            // 
            // grpSearch
            // 
            this.grpSearch.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSearch.AppearanceCaption.Options.UseFont = true;
            this.grpSearch.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.grpSearch.Controls.Add(this.txtCommandID);
            this.grpSearch.Controls.Add(this.dropDownButton1);
            this.grpSearch.Controls.Add(this.btnAction);
            this.grpSearch.Controls.Add(this.cboProductList);
            this.grpSearch.Controls.Add(this.chkIsCanUseOldCode);
            this.grpSearch.Controls.Add(this.cboType);
            this.grpSearch.Controls.Add(this.label6);
            this.grpSearch.Controls.Add(this.chkIsCheckRealInput);
            this.grpSearch.Controls.Add(this.label10);
            this.grpSearch.Controls.Add(this.cboIsShowProduct);
            this.grpSearch.Controls.Add(this.btnHintStock);
            this.grpSearch.Controls.Add(this.btnExportTemplate);
            this.grpSearch.Controls.Add(this.label5);
            this.grpSearch.Controls.Add(this.cboMainGroupID);
            this.grpSearch.Controls.Add(this.cmdCreateStoreChangeCommand);
            this.grpSearch.Controls.Add(this.cboToStoreID);
            this.grpSearch.Controls.Add(this.cboFromStoreID);
            this.grpSearch.Controls.Add(this.txtQuantity);
            this.grpSearch.Controls.Add(this.label4);
            this.grpSearch.Controls.Add(this.label3);
            this.grpSearch.Controls.Add(this.cmdAdd);
            this.grpSearch.Controls.Add(this.label1);
            this.grpSearch.Controls.Add(this.lblStore2);
            this.grpSearch.Controls.Add(this.lblStore1);
            this.grpSearch.Controls.Add(this.cmdImportExcel);
            this.grpSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpSearch.Location = new System.Drawing.Point(0, 0);
            this.grpSearch.Name = "grpSearch";
            this.grpSearch.Size = new System.Drawing.Size(1127, 115);
            this.grpSearch.TabIndex = 0;
            this.grpSearch.Text = "Thông tin";
            // 
            // txtCommandID
            // 
            this.txtCommandID.Location = new System.Drawing.Point(949, 22);
            this.txtCommandID.Name = "txtCommandID";
            this.txtCommandID.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtCommandID.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtCommandID.Properties.Appearance.Options.UseBackColor = true;
            this.txtCommandID.Properties.Appearance.Options.UseFont = true;
            this.txtCommandID.Properties.LookAndFeel.SkinName = "Blue";
            this.txtCommandID.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtCommandID.Properties.ReadOnly = true;
            this.txtCommandID.Size = new System.Drawing.Size(129, 22);
            this.txtCommandID.TabIndex = 7;
            // 
            // dropDownButton1
            // 
            this.dropDownButton1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropDownButton1.Appearance.Options.UseFont = true;
            this.dropDownButton1.DropDownControl = this.pMnuImport;
            this.dropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButton1.Image")));
            this.dropDownButton1.Location = new System.Drawing.Point(673, 82);
            this.dropDownButton1.Name = "dropDownButton1";
            this.dropDownButton1.Size = new System.Drawing.Size(111, 25);
            this.dropDownButton1.TabIndex = 20;
            this.dropDownButton1.Text = "Nhập Excel";
            // 
            // pMnuImport
            // 
            this.pMnuImport.Manager = this.barManager1;
            this.pMnuImport.Name = "pMnuImport";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MaxItemId = 0;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1127, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 517);
            this.barDockControlBottom.Size = new System.Drawing.Size(1127, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 517);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1127, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 517);
            // 
            // btnAction
            // 
            this.btnAction.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAction.Appearance.Options.UseFont = true;
            this.btnAction.DropDownControl = this.pMnuExport;
            this.btnAction.Image = ((System.Drawing.Image)(resources.GetObject("btnAction.Image")));
            this.btnAction.Location = new System.Drawing.Point(728, 51);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(151, 25);
            this.btnAction.TabIndex = 13;
            this.btnAction.Text = "Xuất tập tin mẫu";
            // 
            // pMnuExport
            // 
            this.pMnuExport.Manager = this.barManager1;
            this.pMnuExport.Name = "pMnuExport";
            // 
            // cboProductList
            // 
            this.cboProductList.BackColor = System.Drawing.Color.Transparent;
            this.cboProductList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductList.Location = new System.Drawing.Point(389, 53);
            this.cboProductList.Margin = new System.Windows.Forms.Padding(4);
            this.cboProductList.Name = "cboProductList";
            this.cboProductList.ProductIDList = "";
            this.cboProductList.Size = new System.Drawing.Size(207, 23);
            this.cboProductList.TabIndex = 11;
            // 
            // chkIsCanUseOldCode
            // 
            this.chkIsCanUseOldCode.AutoSize = true;
            this.chkIsCanUseOldCode.Location = new System.Drawing.Point(607, 53);
            this.chkIsCanUseOldCode.Name = "chkIsCanUseOldCode";
            this.chkIsCanUseOldCode.Size = new System.Drawing.Size(115, 20);
            this.chkIsCanUseOldCode.TabIndex = 12;
            this.chkIsCanUseOldCode.Text = "Sử dụng mã cũ";
            this.chkIsCanUseOldCode.UseVisualStyleBackColor = true;
            this.chkIsCanUseOldCode.CheckedChanged += new System.EventHandler(this.chkIsCanUseOldCode_CheckedChanged);
            // 
            // cboType
            // 
            this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboType.FormattingEnabled = true;
            this.cboType.Items.AddRange(new object[] {
            "Chuyển đi",
            "Nhận về"});
            this.cboType.Location = new System.Drawing.Point(89, 22);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(222, 24);
            this.cboType.TabIndex = 1;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            this.cboType.SelectionChangeCommitted += new System.EventHandler(this.cboType_SelectionChangeCommitted);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "Hình thức:";
            // 
            // chkIsCheckRealInput
            // 
            this.chkIsCheckRealInput.AutoSize = true;
            this.chkIsCheckRealInput.Checked = true;
            this.chkIsCheckRealInput.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsCheckRealInput.Location = new System.Drawing.Point(319, 85);
            this.chkIsCheckRealInput.Name = "chkIsCheckRealInput";
            this.chkIsCheckRealInput.Size = new System.Drawing.Size(144, 20);
            this.chkIsCheckRealInput.TabIndex = 16;
            this.chkIsCheckRealInput.Text = "Gồm hàng đi đường";
            this.chkIsCheckRealInput.UseVisualStyleBackColor = true;
            this.chkIsCheckRealInput.CheckStateChanged += new System.EventHandler(this.chkIsCheckRealInput_CheckStateChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 16);
            this.label10.TabIndex = 14;
            this.label10.Text = "Trưng bày:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboIsShowProduct
            // 
            this.cboIsShowProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIsShowProduct.DropDownWidth = 300;
            this.cboIsShowProduct.Items.AddRange(new object[] {
            "--Tất cả--",
            "Không hiển thị hàng trưng bày",
            "Chỉ hiển thị hàng trưng bày"});
            this.cboIsShowProduct.Location = new System.Drawing.Point(89, 83);
            this.cboIsShowProduct.Name = "cboIsShowProduct";
            this.cboIsShowProduct.Size = new System.Drawing.Size(222, 24);
            this.cboIsShowProduct.TabIndex = 15;
            this.cboIsShowProduct.SelectionChangeCommitted += new System.EventHandler(this.cboIsShowProduct_SelectionChangeCommitted);
            // 
            // btnHintStock
            // 
            this.btnHintStock.Image = global::ERP.Inventory.DUI.Properties.Resources.calculator;
            this.btnHintStock.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHintStock.Location = new System.Drawing.Point(949, 50);
            this.btnHintStock.Name = "btnHintStock";
            this.btnHintStock.Size = new System.Drawing.Size(129, 58);
            this.btnHintStock.TabIndex = 23;
            this.btnHintStock.Text = "    Xem tồn kho giả lập";
            this.btnHintStock.UseVisualStyleBackColor = true;
            // 
            // btnExportTemplate
            // 
            this.btnExportTemplate.Image = ((System.Drawing.Image)(resources.GetObject("btnExportTemplate.Image")));
            this.btnExportTemplate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportTemplate.Location = new System.Drawing.Point(963, 67);
            this.btnExportTemplate.Name = "btnExportTemplate";
            this.btnExportTemplate.Size = new System.Drawing.Size(36, 25);
            this.btnExportTemplate.TabIndex = 24;
            this.btnExportTemplate.Text = "    Xuất tập tin mẫu";
            this.btnExportTemplate.UseVisualStyleBackColor = true;
            this.btnExportTemplate.Visible = false;
            this.btnExportTemplate.Click += new System.EventHandler(this.btnExportTemplate_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "Ngành hàng:";
            // 
            // cboMainGroupID
            // 
            this.cboMainGroupID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroupID.Location = new System.Drawing.Point(89, 52);
            this.cboMainGroupID.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroupID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroupID.Name = "cboMainGroupID";
            this.cboMainGroupID.Size = new System.Drawing.Size(223, 24);
            this.cboMainGroupID.TabIndex = 9;
            this.cboMainGroupID.SelectionChangeCommitted += new System.EventHandler(this.cboMainGroupID_SelectionChangeCommitted);
            // 
            // cmdCreateStoreChangeCommand
            // 
            this.cmdCreateStoreChangeCommand.Image = ((System.Drawing.Image)(resources.GetObject("cmdCreateStoreChangeCommand.Image")));
            this.cmdCreateStoreChangeCommand.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdCreateStoreChangeCommand.Location = new System.Drawing.Point(786, 82);
            this.cmdCreateStoreChangeCommand.Name = "cmdCreateStoreChangeCommand";
            this.cmdCreateStoreChangeCommand.Size = new System.Drawing.Size(157, 25);
            this.cmdCreateStoreChangeCommand.TabIndex = 21;
            this.cmdCreateStoreChangeCommand.Text = "Tạo lệnh chuyển kho";
            this.cmdCreateStoreChangeCommand.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdCreateStoreChangeCommand.UseVisualStyleBackColor = true;
            this.cmdCreateStoreChangeCommand.Click += new System.EventHandler(this.cmdCreateStoreChangeCommand_Click);
            // 
            // cboToStoreID
            // 
            this.cboToStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboToStoreID.Location = new System.Drawing.Point(670, 21);
            this.cboToStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboToStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboToStoreID.Name = "cboToStoreID";
            this.cboToStoreID.Size = new System.Drawing.Size(209, 24);
            this.cboToStoreID.TabIndex = 5;
            // 
            // cboFromStoreID
            // 
            this.cboFromStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFromStoreID.Location = new System.Drawing.Point(389, 21);
            this.cboFromStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboFromStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboFromStoreID.Name = "cboFromStoreID";
            this.cboFromStoreID.Size = new System.Drawing.Size(207, 24);
            this.cboFromStoreID.TabIndex = 3;
            // 
            // txtQuantity
            // 
            this.txtQuantity.CustomFormat = "#,##0";
            this.txtQuantity.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtQuantity.Location = new System.Drawing.Point(538, 83);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.NumericInputKeys = ((C1.Win.C1Input.NumericInputKeyFlags)((((C1.Win.C1Input.NumericInputKeyFlags.F9 | C1.Win.C1Input.NumericInputKeyFlags.Plus)
                        | C1.Win.C1Input.NumericInputKeyFlags.Decimal)
                        | C1.Win.C1Input.NumericInputKeyFlags.X)));
            this.txtQuantity.Size = new System.Drawing.Size(58, 22);
            this.txtQuantity.TabIndex = 18;
            this.txtQuantity.Tag = null;
            this.txtQuantity.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtQuantity.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            this.txtQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuantity_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(469, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 16);
            this.label4.TabIndex = 17;
            this.label4.Text = "SL chuyển:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(317, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Sản phẩm:";
            // 
            // cmdAdd
            // 
            this.cmdAdd.Image = ((System.Drawing.Image)(resources.GetObject("cmdAdd.Image")));
            this.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAdd.Location = new System.Drawing.Point(607, 82);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(65, 25);
            this.cmdAdd.TabIndex = 19;
            this.cmdAdd.Text = "Thêm";
            this.cmdAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdAdd.UseVisualStyleBackColor = true;
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(883, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Mã lệnh:";
            // 
            // lblStore2
            // 
            this.lblStore2.AutoSize = true;
            this.lblStore2.Location = new System.Drawing.Point(604, 25);
            this.lblStore2.Name = "lblStore2";
            this.lblStore2.Size = new System.Drawing.Size(60, 16);
            this.lblStore2.TabIndex = 4;
            this.lblStore2.Text = "Đến kho:";
            // 
            // lblStore1
            // 
            this.lblStore1.AutoSize = true;
            this.lblStore1.Location = new System.Drawing.Point(317, 25);
            this.lblStore1.Name = "lblStore1";
            this.lblStore1.Size = new System.Drawing.Size(52, 16);
            this.lblStore1.TabIndex = 2;
            this.lblStore1.Text = "Từ kho:";
            // 
            // cmdImportExcel
            // 
            this.cmdImportExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdImportExcel.Image")));
            this.cmdImportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdImportExcel.Location = new System.Drawing.Point(1015, 67);
            this.cmdImportExcel.Name = "cmdImportExcel";
            this.cmdImportExcel.Size = new System.Drawing.Size(16, 25);
            this.cmdImportExcel.TabIndex = 25;
            this.cmdImportExcel.Text = "Nhập Excel";
            this.cmdImportExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdImportExcel.UseVisualStyleBackColor = true;
            this.cmdImportExcel.Visible = false;
            this.cmdImportExcel.Click += new System.EventHandler(this.cmdImportExcel_Click);
            // 
            // frmAutoStoreChangeDetail_WithIMEI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1127, 517);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.grpSearch);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1050, 555);
            this.Name = "frmAutoStoreChangeDetail_WithIMEI";
            this.ShowIcon = false;
            this.Text = "Chi tiết chuyển kho tự động";
            this.Load += new System.EventHandler(this.frmAutoStoreChangeDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdListAutoStoreChange)).EndInit();
            this.mnuGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvListAutoStoreChange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxedFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flexExportTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpSearch)).EndInit();
            this.grpSearch.ResumeLayout(false);
            this.grpSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommandID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuImport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl grpSearch;
        private System.Windows.Forms.Button btnSelectProduct;
        private System.Windows.Forms.TextBox txtProductID;
        private System.Windows.Forms.Button btnExportTemplate;
        private System.Windows.Forms.Button cmdImportExcel;
        private C1.Win.C1Input.C1NumericEdit txtQuantity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button cmdCreateStoreChangeCommand;
        private System.Windows.Forms.Button cmdAdd;
        private System.Windows.Forms.Label lblStore2;
        private System.Windows.Forms.Label lblStore1;
        private Reports.Input.CachedrptInputVoucherReport cachedrptInputVoucherReport1;
        private Reports.Input.CachedrptInputVoucherReport cachedrptInputVoucherReport2;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboToStoreID;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboFromStoreID;
        private C1.Win.C1FlexGrid.C1FlexGrid flexExportTemplate;
        private System.Windows.Forms.Label label5;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroupID;
        private System.Windows.Forms.ContextMenuStrip mnuGrid;
        private System.Windows.Forms.ToolStripMenuItem mnuExportExcel;
        private System.Windows.Forms.CheckBox chkIsCheckRealInput;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboIsShowProduct;
        private System.Windows.Forms.ToolStripMenuItem mnuDeleteRow;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Button btnHintStock;
        private System.Windows.Forms.ComboBox cboType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkIsCanUseOldCode;
        private DevExpress.XtraGrid.GridControl grdListAutoStoreChange;
        private DevExpress.XtraGrid.Views.Grid.GridView grvListAutoStoreChange;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtItemQuantity;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtTaxedFee;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtInputPrice;
        private System.Windows.Forms.ToolStripMenuItem mnuItemSelectAll;
        private System.Windows.Forms.ToolStripMenuItem mnuItemUnSelectAll;
        private MasterData.DUI.CustomControl.Product.cboProductList cboProductList;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.PopupMenu pMnuExport;
        private DevExpress.XtraBars.PopupMenu pMnuImport;
        private DevExpress.XtraEditors.DropDownButton btnAction;
        private DevExpress.XtraEditors.DropDownButton dropDownButton1;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.TextEdit txtCommandID;
        private System.Windows.Forms.Label label1;
    }
}