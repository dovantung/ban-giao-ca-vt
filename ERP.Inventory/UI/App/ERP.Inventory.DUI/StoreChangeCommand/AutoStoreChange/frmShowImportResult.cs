﻿using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    public partial class frmShowImportResult : Form
    {
        private bool bolHaveIMEI = false;
        private bool bolEvictionIMEI = false;
        public frmShowImportResult()
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }
        private DataTable tblImportResult = null;

        public DataTable ImportResult
        {
            get { return tblImportResult; }
            set { tblImportResult = value; }
        }

        public bool EvictionIMEI
        {
            get { return bolEvictionIMEI; }
            set { bolEvictionIMEI = value; }
        }

        public bool HaveIMEI
        {
            get { return bolHaveIMEI; }
            set { bolHaveIMEI = value; }
        }

        private void frmShowImportResult_Load(object sender, EventArgs e)
        {
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvViewData);
            if (tblImportResult != null)
            {
                gridControl1.DataSource = tblImportResult;
                gridControl1.RefreshDataSource();
            }
            grvViewData.Columns[4].Visible = HaveIMEI;
        }


        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            if (grvViewData.DataRowCount > 0)
                Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(gridControl1);
        }

        private void gridViewData_RowStyle(object sender, RowStyleEventArgs e)
        {
            
        }

        private void mnuExportExcel_Opening(object sender, CancelEventArgs e)
        {
            mnuItemExportExcel.Enabled = grvViewData.DataRowCount > 0;
        }
    }
}
