﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System.Threading;
using System.Collections;
using System.Text.RegularExpressions;
using Library.AppCore.Forms;
using System.Diagnostics;
using Library.AppCore.Constant;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraBars;

namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    public partial class frmAutoStoreChangeDetail_WithIMEI : Form
    {
        private class clsIMEI
        {
            public string StoreID { get; set; }
            public string ProductID { get; set; }
            public List<string> lstIMEI { get; set; }

            public clsIMEI() { lstIMEI = new List<string>(); }
        }
        private class clsMainGroup
        {
            public string MainGroupID { get; set; }
            public string StoreID { get; set; }

            public clsMainGroup() { }
        }
        public frmAutoStoreChangeDetail_WithIMEI()
        {
            InitializeComponent();
        }

        #region Variables
        // private List<DUIInventoryCommon.clsData> lstData = null;
        // private List<clsProductAdd> lstProductAdd = null;
        // private DataTable dtbIMEI = null;
        private DataTable dtbQuantity = null;
        private DataTable dtbImportQuantityWithIMEI = null;
        //private List<string> lstStoreID = null;
        private List<clsIMEI> lstFromExcelIMEI = new List<clsIMEI>();
        private DevExpress.XtraBars.BarButtonItem barExportQuantity = new DevExpress.XtraBars.BarButtonItem();
        private DevExpress.XtraBars.BarButtonItem barExportIMEI = new DevExpress.XtraBars.BarButtonItem();
        private DevExpress.XtraBars.BarButtonItem barImportQuantity = new DevExpress.XtraBars.BarButtonItem();
        private DevExpress.XtraBars.BarButtonItem barImportIMEI = new DevExpress.XtraBars.BarButtonItem();
        private DevExpress.XtraBars.BarButtonItem barImportMultiStore = new DevExpress.XtraBars.BarButtonItem();
        private DevExpress.XtraBars.BarButtonItem barExportMultiStore = new DevExpress.XtraBars.BarButtonItem();
        private List<string> lstProductIDNotPass = null;
        //   private bool bolIsStop = false;
        //private bool bolIsHaveIMEI = false;
        private bool bolIsCallFromImportIMEI = false;
        private int intStoreChangeCommandTypeID = -1;
        private PLC.StoreChangeCommand.PLCStoreChangeCommand objPLCStoreChangeCommand = new PLC.StoreChangeCommand.PLCStoreChangeCommand();
        private DataTable dtbAutoStoreChange = null;
        public DataTable dtbStore = null;
        DataTable dtbStoreMainGroup = null;
        DataTable dtbMainGroupPermission = null;
        public int intMainGroupID = 0;
        private DataTable dtbAutoStoreChangeQuantity = null;
        private int intIsShowProduct = -1;
        private int intIsCheckRealInput = -1;
        private PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();
        private DataTable dtbStoreInStock = null; // Bảng tồn kho các sản phẩm nhập từ excel
        private ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        private int intOldIndex = 0;
        private DataTable dtbAutoDistribute = null;
        private bool bolIsCallFromAutoDistribute = false;
        private bool bolIsReceive = false;
        private int intMainGroupIDFromAutoDistribute = -1;
        private int intStoreID = -1;
        private string strStoreIDList = null;
        private DataTable dtbProductCache = null;
        private bool bolIsCanUseOldCode = false;
        private DataTable dtbStoreOldId = null;
        private DataTable dtbProductOld = null;
        private bool bolIsAutoCreateOrder = false;

        // LE VAN DONG - 01/10/2017
        private DataTable dtbStoreCache = null;
        private ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType = null;
        #endregion

        #region Property

        public string StoreIDList
        {
            get { return strStoreIDList; }
            set { strStoreIDList = value; }
        }

        public int StoreID
        {
            get { return intStoreID; }
            set { intStoreID = value; }
        }

        public int MainGroupIDFromAutoDistribute
        {
            get { return intMainGroupIDFromAutoDistribute; }
            set { intMainGroupIDFromAutoDistribute = value; }
        }

        public bool ISCallFromAutoDistribute
        {
            get { return bolIsCallFromAutoDistribute; }
            set { bolIsCallFromAutoDistribute = value; }
        }

        public bool IsReceive
        {
            get { return bolIsReceive; }
            set { bolIsReceive = value; }
        }
        private string CommandID
        {
            get { return txtCommandID.Text; }
            set
            {
                txtCommandID.Text = value;
            }
        }

        public DataTable DtbAutoDistribute
        {
            get { return dtbAutoDistribute; }
            set { dtbAutoDistribute = value; }
        }

        public int IsCheckRealInput
        {
            set { intIsCheckRealInput = value; }
        }
        public int IsShowProduct
        {
            set { intIsShowProduct = value; }
        }
        public DataTable AutoStoreChangeTable
        {
            set { dtbAutoStoreChange = value; }
        }
        #endregion

        #region Support Functions

        private void EnableControl(bool bolIsEnable)
        {
            grpSearch.Enabled = bolIsEnable;
            mnuGrid.Enabled = bolIsEnable;
        }
        private void ExportTemplate()
        {
            if (string.IsNullOrEmpty(cboToStoreID.StoreIDList))
            {
                string strMess = "Bạn phải chọn kho nhập";
                if (cboType.SelectedIndex == 1)
                    strMess = "Bạn phải chọn kho xuất";
                MessageBoxObject.ShowWarningMessage(this, strMess);
                cboToStoreID.Focus();
                return;
            }
            string[] strArrStoreID = cboToStoreID.StoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
            if (strArrStoreID.Contains(cboFromStoreID.StoreID.ToString().Trim()))
            {
                MessageBox.Show("Kho nhập phải khác với kho xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboToStoreID.Focus();
                return;
            }
            DataTable dtbExportTemplate = new DataTable();
            dtbExportTemplate.Columns.Add("Mã sản phẩm", typeof(string));
            string strToStoreIDList = cboToStoreID.Text;
            string[] strArrToStore = strToStoreIDList.Trim('[', ']').Split(new string[] { "] [" }, StringSplitOptions.None);
            List<string> lstToStoreID = new List<string>();
            for (int i = 0; i < strArrToStore.Length; i++)
            {
                string[] strToStoreInfo = strArrToStore[i].Split(new string[] { " - " }, 2, StringSplitOptions.None);
                if (strToStoreInfo.Length != 2)
                    continue;
                lstToStoreID.Add(strToStoreInfo[0]);
                dtbExportTemplate.Columns.Add(strArrToStore[i], typeof(string));
            }
            DataRow row = dtbExportTemplate.NewRow();
            row[0] = "PRODUCTID";
            for (int i = 1; i <= lstToStoreID.Count; i++)
            {
                // LÊ VĂN ĐÔNG - 04/07/2017
                if (chkIsCanUseOldCode.Checked && dtbStoreOldId != null)
                {
                    DataRow[] rowsel = dtbStoreOldId.Select("STOREID=" + lstToStoreID[i - 1] + "");
                    if (rowsel.Length > 0)
                        row[i] = rowsel[0]["OLDSTOREID"];
                }
                else
                    row[i] = lstToStoreID[i - 1];
            }
            dtbExportTemplate.Rows.Add(row);

            //Thêm mã sản phẩm đã chọn 
            string[] strArrProductID = cboProductList.ProductIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
            if (!string.IsNullOrEmpty(strArrProductID[0].ToString()))
            {
                foreach (var item in strArrProductID)
                {
                    DataRow rowPro = dtbExportTemplate.NewRow();
                    rowPro[0] = item.ToString().Trim();
                    dtbExportTemplate.Rows.Add(rowPro);
                }
            }
            dtbExportTemplate.AcceptChanges();
            flexExportTemplate.DataSource = dtbExportTemplate;
            flexExportTemplate.AutoSizeCols();
            Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexExportTemplate);
        }

        private void ExportTemplateIMEI()
        {
            try
            {
                if (string.IsNullOrEmpty(cboToStoreID.StoreIDList))
                {
                    string strMess = "Bạn phải chọn kho nhập";
                    if (cboType.SelectedIndex == 1)
                        strMess = "Bạn phải chọn kho xuất";
                    MessageBoxObject.ShowWarningMessage(this, strMess);
                    // cboExportTemplate.SelectedIndex = 0;
                    cboToStoreID.Focus();
                    return;
                }

                string[] strArrStoreID = cboToStoreID.StoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
                if (strArrStoreID.Contains(cboFromStoreID.StoreID.ToString().Trim()))
                {
                    MessageBox.Show("Kho nhập phải khác với kho xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboToStoreID.Focus();
                    return;
                }

                DataTable dtbExportTemplate = new DataTable();
                dtbExportTemplate.Clear();
                //Cột đầu

                DataTable dtbStoreTemp = dtbStore.Copy();

                string strToStoreIDList = cboToStoreID.StoreIDList;
                string[] strArrToStore = strToStoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
                List<string> lstToStoreID = new List<string>();
                string strProductIDList = cboProductList.ProductIDList.ToString();
                string[] strProductIDListAdd = strProductIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);

                #region 1sp hoặc không chọn
                if (strProductIDListAdd.Length <= 1)
                {
                    int countColumns = (strArrToStore.Length * strProductIDListAdd.Length) + 1;
                    for (int i = 0; i < countColumns; i++)
                        dtbExportTemplate.Columns.Add("Columns" + i, typeof(string));
                    DataRow rowT = dtbExportTemplate.NewRow();
                    rowT[0] = "Mã kho";
                    //dtbExportTemplate.Columns.Add("Mã kho", typeof(string));
                    //for (int i = 0; i < strArrToStore.Length; i++)
                    //    dtbExportTemplate.Columns.Add(strArrToStore[i], typeof(string));
                    DataRow row = dtbExportTemplate.NewRow();
                    row[0] = "Tên kho";
                    for (int i = 1; i <= strArrToStore.Length; i++)
                    {
                        string strStoreID = strArrToStore[i - 1].Trim();
                        DataRow[] drr = dtbStoreTemp.Select("STOREID = '" + strStoreID + "'");
                        string strStoreName = drr[0]["STORENAME"].ToString().Trim();
                        if (chkIsCanUseOldCode.Checked && dtbStoreOldId != null)
                        {
                            DataRow[] rowsel = dtbStoreOldId.Select("STOREID=" + strStoreID + "");
                            if (rowsel.Length > 0 && rowsel[0]["OLDSTOREID"].ToString().Trim() != "-1")
                                strStoreID = rowsel[0]["OLDSTOREID"].ToString().Trim();
                        }
                        rowT[i] = strStoreID;
                        row[i] = strStoreName;
                    }
                    dtbExportTemplate.Rows.Add(rowT);
                    dtbExportTemplate.Rows.Add(row);
                    if (string.IsNullOrEmpty(strProductIDListAdd[0]))
                    {
                        DataRow row1 = dtbExportTemplate.NewRow();
                        row1[0] = "Mã sản phẩm";
                        dtbExportTemplate.Rows.Add(row1);
                        DataRow row2 = dtbExportTemplate.NewRow();
                        row2[0] = "Tên sản phẩm";
                        dtbExportTemplate.Rows.Add(row2);
                    }
                    else
                    {
                        DataRow row1 = dtbExportTemplate.NewRow();
                        row1[0] = "Mã sản phẩm";
                        DataRow row2 = dtbExportTemplate.NewRow();
                        row2[0] = "Tên sản phẩm";
                        for (int i = 1; i <= strArrToStore.Length; i++)
                        {
                            string strProductID = strProductIDListAdd[0].ToString().Trim();
                            DataRow[] drr = dtbProductCache.Select("PRODUCTID = '" + strProductID + "'");

                            //mã cũ
                            if (chkIsCanUseOldCode.Checked && dtbProductOld != null)
                            {
                                DataRow[] rowsel = dtbProductOld.Select("PRODUCTID='" + strProductID + "'");
                                if (rowsel.Length > 0 && !string.IsNullOrEmpty(rowsel[0]["PRODUCTIDOLD"].ToString().Trim()))
                                    strProductID = rowsel[0]["PRODUCTIDOLD"].ToString().Trim();
                            }

                            row1[i] = strProductID;
                            row2[i] = drr[0]["PRODUCTNAME"].ToString().Trim();
                        }
                        dtbExportTemplate.Rows.Add(row1);
                        dtbExportTemplate.Rows.Add(row2);
                    }
                }
                #endregion

                #region lớn hơn 2 sp
                else
                {
                    int countColumns = (strArrToStore.Length * strProductIDListAdd.Length) + 1;
                    for (int i = 0; i < countColumns; i++)
                        dtbExportTemplate.Columns.Add("Columns" + i, typeof(string));
                    int position = 1;
                    DataRow rowT = dtbExportTemplate.NewRow();
                    rowT[0] = "Mã kho";
                    DataRow row = dtbExportTemplate.NewRow();
                    row[0] = "Tên kho";
                    DataRow row1 = dtbExportTemplate.NewRow();
                    row1[0] = "Mã sản phẩm";
                    DataRow row2 = dtbExportTemplate.NewRow();
                    row2[0] = "Tên sản phẩm";
                    for (int i = 0; i < strArrToStore.Length; i++)
                    {
                        for (int j = 0; j < strProductIDListAdd.Length; j++)
                        {
                            string strStoreID = strArrToStore[i].Trim();
                            DataRow[] drr = dtbStoreTemp.Select("STOREID = '" + strArrToStore[i] + "'");
                            string strStoreName = drr[0]["STORENAME"].ToString().Trim();

                            if (chkIsCanUseOldCode.Checked && dtbStoreOldId != null)
                            {
                                DataRow[] rowsel = dtbStoreOldId.Select("STOREID=" + strStoreID + "");
                                if (rowsel.Length > 0 && rowsel[0]["OLDSTOREID"].ToString().Trim() != "-1")
                                    strStoreID = rowsel[0]["OLDSTOREID"].ToString().Trim();
                            }
                            //Tên kho
                            rowT[position] = strStoreID;
                            row[position] = strStoreName;
                            //Mã sản phẩm
                            string strProductID = strProductIDListAdd[j].ToString().Trim();
                            DataRow[] drrProduct = dtbProductCache.Select("PRODUCTID = '" + strProductID + "'");

                            //Mã cũ
                            if (chkIsCanUseOldCode.Checked && dtbProductOld != null)
                            {
                                DataRow[] rowsel = dtbProductOld.Select("PRODUCTID='" + strProductID + "'");
                                if (rowsel.Length > 0 && !string.IsNullOrEmpty(rowsel[0]["PRODUCTIDOLD"].ToString().Trim()))
                                    strProductID = rowsel[0]["PRODUCTIDOLD"].ToString().Trim();
                            }

                            row1[position] = strProductID;
                            //Tên sản phẩms
                            row2[position] = drrProduct[0]["PRODUCTNAME"].ToString().Trim();
                            position += 1;
                        }
                    }
                    dtbExportTemplate.Rows.Add(rowT);
                    dtbExportTemplate.Rows.Add(row);
                    dtbExportTemplate.Rows.Add(row1);
                    dtbExportTemplate.Rows.Add(row2);
                }

                #endregion
                DataRow row3 = dtbExportTemplate.NewRow();
                row3[0] = "IMEI";
                dtbExportTemplate.Rows.Add(row3);
                dtbExportTemplate.AcceptChanges();

                string[] fieldNames = new string[dtbExportTemplate.Columns.Count];
                for (int i = 0; i < dtbExportTemplate.Columns.Count; i++)
                    fieldNames[i] = ("Columns" + i.ToString());
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
                for (int i = 0; i < dtbExportTemplate.Columns.Count; i++)
                    grdViewData.Columns["Columns" + i.ToString()].Width = 130;
                grdData.DataSource = dtbExportTemplate;
                Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData, DevExpress.XtraPrinting.TextExportMode.Text, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void GetStoreInStock(DataTable tblProduct, int intFromStoreID)
        {
            if (tblProduct == null)
                return;
            StringBuilder strProductIDList = new StringBuilder();
            string strStoreIDList = "<" + intFromStoreID + ">";
            DataRow rowStoreIDList = tblProduct.Rows[1];
            for (int i = 1; i < tblProduct.Columns.Count; i++)
            {
                string strStoreID = Convert.ToString(rowStoreIDList[i]).Trim();
                if (!string.IsNullOrWhiteSpace(strStoreID))
                    strStoreIDList = strStoreIDList + "<" + Convert.ToString(rowStoreIDList[i]).Trim() + ">";
            }
            for (int i = 2; i < tblProduct.Rows.Count; i++)
            {
                if (Convert.ToString(tblProduct.Rows[i][0]).Trim().Length <= 20)
                {
                    strProductIDList.Append("<");
                    strProductIDList.Append(Convert.ToString(tblProduct.Rows[i][0]).Trim());
                    strProductIDList.Append(">");
                }
            }
            object[] objKeywords = new object[]{"@PRODUCTIDLIST", strProductIDList.ToString(),
                                                "@STOREIDLIST", strStoreIDList,
                                                //"@ISSHOWPRODUCT", intIsShowProduct,
                                                "@ISCHECKREALINPUT", intIsCheckRealInput,
                                                //"@ISNEW",1
                                                "@InStockStatusIDList",1 ,
                                                //"@IsExistStock", 1
                                                };
            dtbStoreInStock = objPLCReportDataSource.GetDataSource("PM_STOCKREPORT_AUTOCHANGE", objKeywords);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                MessageBoxObject.ShowWarningMessage(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
        }

        private void GetStoreInStockWithIMEI(List<clsIMEI> lst, int intFromStoreID)
        {

            if (lst == null || lst.Count() < 1)
                return;
            //StringBuilder strProductIDList = new StringBuilder();
            string strProductIDList = string.Empty;
            string strStoreIDList = "<" + intFromStoreID + ">";

            foreach (var item in lst)
            {
                string strStoreID = Convert.ToString(item.StoreID).Trim();
                if (!string.IsNullOrWhiteSpace(strStoreID))
                    strStoreIDList = strStoreIDList + "<" + Convert.ToString(item.StoreID).Trim() + ">";

                if (Convert.ToString(item.ProductID).Trim().Length <= 20)
                {
                    strProductIDList = strProductIDList + "<" + Convert.ToString(item.ProductID).Trim() + ">";
                    //strProductIDList.Append("<");
                    //strProductIDList.Append(Convert.ToString(item.ProductID).Trim());
                    //strProductIDList.Append(">");
                }
            }
            string strInStockStatusIDList = string.Empty;
            strInStockStatusIDList = "1";
            object[] objKeywords = new object[] { "@PRODUCTIDLIST", strProductIDList.ToString(),
                                                "@STOREIDLIST", strStoreIDList,
                                               // "@ISSHOWPRODUCT", intIsShowProduct,
                                                "@ISCHECKREALINPUT", intIsCheckRealInput,
                                                //"@ISNEW",1
                                                "@InStockStatusIDList", strInStockStatusIDList,
                                                //"@IsExistStock", 1
                                                };
            dtbStoreInStock = objPLCReportDataSource.GetDataSource("PM_STOCKREPORT_AUTOCHANGE", objKeywords);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                MessageBoxObject.ShowWarningMessage(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);

        }

        private decimal GetQuantity(string strProductID, int intStoreID)
        {
            if (dtbStoreInStock == null || dtbStoreInStock.Rows.Count < 1)
                return 0;
            DataRow[] row = dtbStoreInStock.Select("ProductID ='" + strProductID + "'" + " and StoreID = " + intStoreID);
            if (row != null && row.Length > 0)
                return Convert.ToDecimal(row[0]["QUANTITY"]);
            return 0;
        }

        // le vandong--1
        private void ImportFromAutoDistribute()
        {
            if (dtbAutoDistribute == null || dtbAutoDistribute.Rows.Count < 1)
            {
                return;
            }

            DataTable tblResult = CreateImportResultTable();
            dtbAutoStoreChange.Clear();
            bool bolIsPermission = Library.AppCore.SystemConfig.objSessionUser.IsPermission("AUTOSTORECHANGE_CHECKEMERGENCY");
            int intIsUrgent = 0;
            bool bolIsExistsError = false;
            try
            {
                if (bolIsReceive)
                {
                    string strMainGroupList = cboMainGroupID.MainGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");
                    string[] mainGroupList = strMainGroupList.Split(',');
                    int intToStoreID = cboFromStoreID.StoreID;
                    string strToStoreName = ERP.MasterData.PLC.MD.PLCStore.GetStoreName(intToStoreID);
                    int intFromStoreID = -1;
                    string strStatus = "";
                    string strErrorContent = "";
                    int intStatusID = 0;
                    decimal decQuanity = 0;
                    string strFromStoreName = string.Empty;
                    string strProductID = string.Empty;

                    for (int i = 0; i < dtbAutoDistribute.Rows.Count; i++)
                    {
                        DataRow objRow = dtbAutoDistribute.Rows[i];

                        strProductID = Convert.ToString(objRow[0]).Trim();
                        intFromStoreID = -1;
                        strStatus = "";
                        strErrorContent = "";
                        intStatusID = 0;
                        decQuanity = 0;
                        strFromStoreName = string.Empty;

                        intFromStoreID = Convert.ToInt32(objRow["STOREID"]);
                        decQuanity = -Convert.ToDecimal(objRow["TRANSFERQUANTITY"]);
                        DataRow[] rowFromStore = dtbStoreCache.Select("STOREID = " + intFromStoreID);
                        if (rowFromStore.Length > 0)
                            strFromStoreName = rowFromStore[0]["STORENAME"].ToString();
                        decimal decRealQuanity = decQuanity;
                        decimal decFromInstockQuantity = 0;
                        decimal decToInstockQuantity = 0;
                        string strProductName = string.Empty;
                        strProductName = objRow["PRODUCTNAME"].ToString();

                        if (strFromStoreName.Length > 0 && strToStoreName.Length > 0)
                        {
                            decFromInstockQuantity = Convert.ToInt32(objRow["QUANTITY"]);
                            decToInstockQuantity = Convert.ToInt32(objRow["CENTERQUANTITY"]);
                            //decFromInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intFromStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                            //decToInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);

                            if (intFromStoreID == intToStoreID)
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Kho nhập trùng với kho xuất";
                                intStatusID = 1;
                            }

                            else if (!CheckMainGroupPermission(intFromStoreID, cboMainGroupID.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + cboMainGroupID.MainGroupName;
                                intStatusID = 1;
                            }
                            else if (!CheckMainGroupPermission(intToStoreID, cboMainGroupID.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + cboMainGroupID.MainGroupName;
                                intStatusID = 1;
                            }

                            else if (!CheckPermission(cboMainGroupID.MainGroupID, EnumType.IsNewPermissionType.ALL))
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Bạn không có quyền thao tác trên ngành hàng " + cboMainGroupID.MainGroupName;
                                intStatusID = 1;
                            }
                            else if (decQuanity > 0)
                            {
                                if (CheckProductExist(intFromStoreID, intToStoreID, strProductID))
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Sản phẩm chuyển kho đã tồn tại";
                                    intStatusID = 1;
                                }
                                else
                                {
                                    if (decFromInstockQuantity <= 0)
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Sản phẩm không tồn kho";
                                        intStatusID = 1;
                                    }
                                    else
                                    {
                                        if (decQuanity > decFromInstockQuantity)
                                        {
                                            strStatus = "Cảnh báo";
                                            strErrorContent = "Số lượng chuyển lớn hơn số lượng tồn";
                                            decRealQuanity = decFromInstockQuantity;
                                            intStatusID = 2;
                                        }
                                        else
                                        {
                                            strStatus = "Thành công";
                                            strErrorContent = "";
                                            intStatusID = 0;
                                        }
                                        AddProduct(strProductID, strProductName, intFromStoreID, strFromStoreName, intToStoreID,
                                            strToStoreName, decFromInstockQuantity, decToInstockQuantity, decRealQuanity, false, Convert.ToBoolean(intIsUrgent));

                                    }
                                }
                            }
                            else
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Số lượng chuyển phải lớn hơn 0";
                                intStatusID = 1;
                            }
                        }
                        else
                        {
                            if (strFromStoreName.Length == 0)
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Sai kho xuất";
                                intStatusID = 1;
                            }
                            else
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Sai kho nhập";
                                intStatusID = 1;
                            }
                        }
                        if (!bolIsExistsError && intStatusID != 0)
                        {
                            bolIsExistsError = true;
                        }
                        DataRow objRowResult = tblResult.NewRow();
                        objRowResult.ItemArray = new object[] { strProductID, strProductName, strFromStoreName,
                        decFromInstockQuantity, strToStoreName, decToInstockQuantity, decQuanity, strStatus,
                        intStatusID, intIsUrgent, strErrorContent };
                        tblResult.Rows.Add(objRowResult);
                    }
                }
                else
                {
                    string strProductID = string.Empty;
                    int intFromStoreID = cboFromStoreID.StoreID;
                    string strFromStoreName = string.Empty;
                    DataRow[] rowFromStore = dtbStoreCache.Select("STOREID = " + intFromStoreID);
                    if (rowFromStore.Length > 0)
                        strFromStoreName = rowFromStore[0]["STORENAME"].ToString();

                    int intToStoreID = -1;
                    String strStatus = "";
                    String strErrorContent = "";
                    int intStatusID = 0;
                    decimal decQuanity = 0;
                    string strProductName = string.Empty;

                    for (int i = 0; i < dtbAutoDistribute.Rows.Count; i++)
                    {
                        DataRow objRow = dtbAutoDistribute.Rows[i];
                        strProductID = Convert.ToString(objRow[0]).Trim();
                        intToStoreID = -1;
                        strStatus = "";
                        strErrorContent = "";
                        intStatusID = 0;
                        decQuanity = 0;

                        intToStoreID = Convert.ToInt32(objRow["STOREID"]);
                        decQuanity = Convert.ToDecimal(objRow["TRANSFERQUANTITY"]);
                        decimal decRealQuanity = decQuanity;
                        string strToStoreName = string.Empty;
                        DataRow[] rowToStore = dtbStoreCache.Select("STOREID = " + intToStoreID);
                        if (rowToStore.Length > 0)
                            strToStoreName = rowToStore[0]["STORENAME"].ToString();

                        decimal decFromInstockQuantity = 0;
                        decimal decToInstockQuantity = 0;

                        strProductName = objRow["PRODUCTNAME"].ToString();
                        if (strFromStoreName.Length > 0 && strToStoreName.Length > 0)
                        {
                            decFromInstockQuantity = Convert.ToInt32(objRow["CENTERQUANTITY"]);
                            decToInstockQuantity = Convert.ToInt32(objRow["QUANTITY"]);
                            //decFromInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intFromStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                            //decToInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);

                            if (intFromStoreID == intToStoreID)
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Kho nhập trùng với kho xuất";
                                intStatusID = 1;
                            }
                            else if (!CheckMainGroupPermission(intFromStoreID, cboMainGroupID.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + cboMainGroupID.MainGroupName;
                                intStatusID = 1;
                            }
                            else if (!CheckMainGroupPermission(intToStoreID, cboMainGroupID.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + cboMainGroupID.MainGroupName;
                                intStatusID = 1;
                            }

                            else if (!CheckPermission(cboMainGroupID.MainGroupID, EnumType.IsNewPermissionType.ALL))
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Bạn không có quyền thao tác trên ngành hàng " + cboMainGroupID.MainGroupName;
                                intStatusID = 1;
                            }
                            else if (decQuanity > 0)
                            {
                                if (CheckProductExist(intFromStoreID, intToStoreID, strProductID))
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Sản phẩm chuyển kho đã tồn tại";
                                    intStatusID = 1;
                                }
                                else
                                {
                                    if (decFromInstockQuantity <= 0)
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Sản phẩm không tồn kho";
                                        intStatusID = 1;
                                    }
                                    else
                                    {
                                        if (decQuanity > decFromInstockQuantity)
                                        {
                                            strStatus = "Cảnh báo";
                                            strErrorContent = "Số lượng chuyển lớn hơn số lượng tồn";
                                            decRealQuanity = decFromInstockQuantity;
                                            intStatusID = 2;
                                        }
                                        else
                                        {
                                            strStatus = "Thành công";
                                            strErrorContent = "";
                                            intStatusID = 0;
                                        }
                                        AddProduct(strProductID, strProductName, intFromStoreID, strFromStoreName, intToStoreID,
                                            strToStoreName, decFromInstockQuantity, decToInstockQuantity, decRealQuanity, false, Convert.ToBoolean(intIsUrgent));

                                    }
                                }
                            }
                            else
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Số lượng chuyển phải lớn hơn 0";
                                intStatusID = 1;
                            }
                        }
                        else
                        {
                            if (strFromStoreName.Length == 0)
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Sai kho xuất";
                                intStatusID = 1;
                            }
                            else
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Sai kho nhập";
                                intStatusID = 1;
                            }
                        }
                        if (!bolIsExistsError && intStatusID != 0)
                        {
                            bolIsExistsError = true;
                        }
                        DataRow objRowResult = tblResult.NewRow();
                        objRowResult.ItemArray = new object[] { strProductID, strProductName, strFromStoreName,
                        decFromInstockQuantity, strToStoreName, decToInstockQuantity, decQuanity, strStatus,
                        intStatusID, intIsUrgent, strErrorContent };
                        tblResult.Rows.Add(objRowResult);

                    }
                }
            }
            catch (Exception objExce)
            {
                Library.AppCore.SystemErrorWS.Insert("Lỗi nhập thông tin chuyển kho từ Excel ", objExce, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (bolIsExistsError)
            {
                frmShowImportResult frmShowImportResult1 = new frmShowImportResult();
                frmShowImportResult1.Text = "Kết quả nhập từ chia hàng tự động";
                frmShowImportResult1.ImportResult = tblResult;
                frmShowImportResult1.ShowDialog();
            }
        }

        public bool IsNumber(string pText)
        {
            if (string.IsNullOrEmpty(pText))
                return false;
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            return regex.IsMatch(pText);
        }

        private void ImportExcelNew(DataTable tblProduct)
        {
            Thread objThread = new Thread(new ThreadStart(Excute));

            if (!bolIsCallFromImportIMEI)
            {
                if (tblProduct == null)
                {
                    MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (tblProduct.Rows.Count > 0 && tblProduct.Rows[1] != null)
                {
                    if (!CheckExcelQuantity(tblProduct))
                    {
                        MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                    return;
                if (chkIsCanUseOldCode.Checked)
                {
                    // LÊ VĂN ĐÔNG - THAY THẾ MÃ VIETTEL THÀNH MÃ HỆ THỐNG ERP
                    if (dtbStoreOldId != null)
                    {
                        for (int i = 1; i < tblProduct.Columns.Count; i++)
                        {
                            DataRow[] rowsel = dtbStoreOldId.Select("OLDSTOREID='" + tblProduct.Rows[1][i].ToString() + "'");
                            if (rowsel.Length > 0)
                                tblProduct.Rows[1][i] = rowsel[0]["STOREID"];
                        }
                    }

                    // Thay thế mã sản phẩm
                    if (dtbProductOld != null)
                    {
                        for (int i = 2; i < tblProduct.Rows.Count; i++)
                        {
                            DataRow[] rowsel = dtbProductOld.Select("PRODUCTIDOLD='" + tblProduct.Rows[i][0].ToString() + "'");
                            if (rowsel.Length > 0)
                                tblProduct.Rows[i][0] = rowsel[0]["PRODUCTID"];
                        }
                    }
                }
            }
            else
                tblProduct = dtbQuantity;
            //tblProduct.Rows.RemoveAt(0);
            DataTable tblResult = CreateImportResultTable();
            tblResult.Clear();
            dtbAutoStoreChange.Clear();
            bool bolIsPermission = Library.AppCore.SystemConfig.objSessionUser.IsPermission("AUTOSTORECHANGE_CHECKEMERGENCY");
            int intIsUrgent = 0;
            bool bolIsExistsError = false;

            string strMainGroupList = cboMainGroupID.MainGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");
            string[] mainGroupList = strMainGroupList.Split(',');


            try
            {
                if (cboType.SelectedIndex == 1)
                {
                    #region cboType.SelectedIndex == 1 : Nhận về
                    if (!bolIsCallFromImportIMEI)
                    {
                        objThread.Start();
                        GetStoreInStock(tblProduct, cboFromStoreID.StoreID);
                    }
                    if (dtbStoreInStock == null)
                        return;


                    string strToStoreName = string.Empty;
                    int intToStoreID = cboFromStoreID.StoreID;
                    DataRow[] rowToStore = dtbStoreCache.Select("STOREID = " + intToStoreID);
                    if (rowToStore.Length > 0)
                        strToStoreName = rowToStore[0]["STORENAME"].ToString();

                    int intFromStoreID = -1;
                    string strStatus = "";
                    string strErrorContent = "";
                    int intStatusID = 0;
                    decimal decQuanity = 0;


                    DataRow rowStoreID = tblProduct.Rows[1];

                    var rlsProductID = tblProduct.AsEnumerable().Where(r => !string.IsNullOrEmpty(r[0].ToString())).Select(r => r[0].ToString()).ToList();
                    var rlsToStoreID = tblProduct.AsEnumerable().Where(r => !string.IsNullOrEmpty(r[0].ToString())).Select(r => r[0].ToString()).ToList();
                    rlsProductID.RemoveAt(0);
                    var rlsDup = rlsProductID.Distinct().ToList();
                    var arrMainGroupID = (from r in rlsDup
                                          join x in dtbProductOld.AsEnumerable()
                                          on r.ToString().Trim() equals x["PRODUCTID"].ToString().Trim()
                                          select x["MAINGROUPID"].ToString()).ToList();


                    //Key: Ma kho, Value: Nganh hang
                    Dictionary<int, List<int>> dicCheckMainGroup = new Dictionary<int, List<int>>();
                    int intStoreIDCheck = 0;

                    for (int icol = 1; icol < tblProduct.Columns.Count; icol++)
                    {
                        if (IsNumber(rowStoreID[icol].ToString()))
                        {
                            intStoreIDCheck = Convert.ToInt32(rowStoreID[icol].ToString());

                            if (dicCheckMainGroup.ContainsKey(intStoreIDCheck))
                                continue;
                            else
                            {
                                List<int> lstMainID = new List<int>();
                                foreach (string strMainGroupID in arrMainGroupID)
                                {
                                    if (!CheckMainGroupPermission(intStoreIDCheck, Convert.ToInt32(strMainGroupID), Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                                        lstMainID.Add(Convert.ToInt32(strMainGroupID));
                                }
                                if (lstMainID.Count > 0)
                                    dicCheckMainGroup.Add(intStoreIDCheck, lstMainID);
                            }
                        }
                    }


                    for (int i = 2; i < tblProduct.Rows.Count; i++)
                    {
                        DataRow objRow = tblProduct.Rows[i];
                        string strProductID = Convert.ToString(objRow[0]).Trim();


                        string strProductName = string.Empty;
                        bool bolIsAllowDecimal = false;
                        int intMainGroupID = 0;
                        string strMainGroupName = "";
                        string strErrorMainGroup = string.Empty;

                        DataRow[] rowProduct = dtbProductOld.Select("PRODUCTID = '" + strProductID.Trim() + "'");
                        if (rowProduct.Length > 0)
                        {
                            strProductName = rowProduct[0]["PRODUCTNAME"].ToString();
                            bolIsAllowDecimal = Convert.ToBoolean(rowProduct[0]["ISALLOWDECIMAL"]);
                            intMainGroupID = Convert.ToInt32(rowProduct[0]["MAINGROUPID"]);
                            strMainGroupName = rowProduct[0]["MAINGROUPNAME"].ToString();

                            // Check phân quyền trên ngành hàng
                            if (!string.IsNullOrEmpty(strMainGroupList.Trim()) && !mainGroupList.Any(x => x.Trim().Equals(intMainGroupID.ToString())))
                                strErrorMainGroup = "Sản phẩm không thuộc ngành hàng đang xét";
                            else if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intToStoreID, intMainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                                strErrorMainGroup = "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + strMainGroupName;
                            // >Phan quyền
                        }

                        for (int j = 1; j < tblProduct.Columns.Count; j++)
                        {
                            string strStoreID = rowStoreID[j].ToString();
                            string strQuantity = objRow[j].ToString();
                            if (bolIsCallFromImportIMEI && string.IsNullOrEmpty(strQuantity))
                                continue;
                            if (!string.IsNullOrWhiteSpace(strStoreID))
                            {
                                intFromStoreID = -1;
                                strStatus = "";
                                strErrorContent = "";
                                intStatusID = 0;
                                decQuanity = 0;
                                string strFromStoreName = string.Empty;

                                bool bolValidData = false;
                                if (!IsNumber(strStoreID))
                                    bolValidData = true;
                                else
                                    intFromStoreID = Convert.ToInt32(strStoreID.Trim());

                                if (!IsNumber(strQuantity))
                                    bolValidData = true;
                                else
                                    decQuanity = Convert.ToDecimal(strQuantity.Trim());

                                if (bolValidData)
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Dữ liệu trên từng kho phải là số";
                                    intStatusID = 1;
                                }

                                DataRow[] rowStore = dtbStoreCache.Select("STOREID = " + intFromStoreID);
                                if (rowStore.Length > 0)
                                    strFromStoreName = rowStore[0]["STORENAME"].ToString();

                                decimal decRealQuanity = decQuanity;
                                decimal decFromInstockQuantity = 0;
                                decimal decToInstockQuantity = 0;

                                if (rowProduct.Length == 0)
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Mã sản phẩm không tồn tại";
                                    intStatusID = 1;
                                    DataRow objRowResult1 = tblResult.NewRow();
                                    objRowResult1.ItemArray = new object[] { strProductID, strProductName, strFromStoreName,
                                    decFromInstockQuantity, strToStoreName, decToInstockQuantity, decQuanity, strStatus,
                                    intStatusID, intIsUrgent, strErrorContent };
                                    tblResult.Rows.Add(objRowResult1);
                                    if (!bolIsExistsError && intStatusID != 0)
                                    {
                                        bolIsExistsError = true;
                                    }
                                    continue;
                                }

                                if (strFromStoreName.Length > 0 && strToStoreName.Length > 0)
                                {
                                    decFromInstockQuantity = GetQuantity(strProductID, intFromStoreID);
                                    decToInstockQuantity = GetQuantity(strProductID, intToStoreID);
                                    //decFromInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intFromStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                                    //decToInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                                    if (intFromStoreID == intToStoreID)
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Kho nhập trùng với kho xuất";
                                        intStatusID = 1;
                                    }
                                    else if (!string.IsNullOrEmpty(strErrorMainGroup))
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = strErrorMainGroup;
                                        intStatusID = 1;
                                    }
                                    else if (dicCheckMainGroup.ContainsKey(intFromStoreID) && dicCheckMainGroup[intFromStoreID].Contains(intMainGroupID))
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + strMainGroupName;
                                        intStatusID = 1;
                                    }
                                    else if (!ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(intMainGroupID))
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Bạn không có quyền thao tác trên ngành hàng " + strMainGroupName;
                                        intStatusID = 1;
                                    }
                                    else if (decQuanity > 0)
                                    {
                                        if (CheckProductExist(intFromStoreID, intToStoreID, strProductID))
                                        {
                                            strStatus = "Lỗi";
                                            strErrorContent = "Sản phẩm chuyển kho đã tồn tại";
                                            intStatusID = 1;
                                        }
                                        else
                                        {
                                            if (decFromInstockQuantity <= 0)
                                            {
                                                strStatus = "Lỗi";
                                                strErrorContent = "Sản phẩm không tồn kho";
                                                intStatusID = 1;
                                            }
                                            else
                                            {
                                                if (decQuanity > decFromInstockQuantity)
                                                {
                                                    strStatus = "Cảnh báo";
                                                    strErrorContent = "Số lượng chuyển lớn hơn số lượng tồn";
                                                    decRealQuanity = decFromInstockQuantity;
                                                    intStatusID = 2;
                                                }
                                                else
                                                {
                                                    strStatus = "Thành công";
                                                    strErrorContent = "";
                                                    intStatusID = 0;
                                                }
                                                AddProduct(strProductID, strProductName, intFromStoreID, intFromStoreID.ToString() + "-" + strFromStoreName, intToStoreID,
                                                    intToStoreID.ToString() + "-" + strToStoreName, decFromInstockQuantity, decToInstockQuantity, decRealQuanity, bolIsAllowDecimal, Convert.ToBoolean(intIsUrgent));

                                            }
                                        }
                                    }
                                    else
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Số lượng chuyển phải lớn hơn 0";
                                        intStatusID = 1;
                                    }
                                }
                                else
                                {
                                    if (strFromStoreName.Length == 0)
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Sai kho xuất";
                                        intStatusID = 1;
                                    }
                                    else
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Sai kho nhập";
                                        intStatusID = 1;
                                    }
                                }
                                if (!bolIsExistsError && intStatusID != 0)
                                {
                                    bolIsExistsError = true;
                                }
                                DataRow objRowResult = tblResult.NewRow();
                                objRowResult.ItemArray = new object[] { strProductID, strProductName, intFromStoreID.ToString() + "-" + strFromStoreName,
                                    decFromInstockQuantity, intToStoreID.ToString() + "-" + strToStoreName, decToInstockQuantity, decQuanity, strStatus,
                                    intStatusID, intIsUrgent, strErrorContent };
                                tblResult.Rows.Add(objRowResult);
                            }
                        }
                    }

                    frmWaitDialog.Close();
                    Thread.Sleep(0);
                    objThread.Abort();

                    #endregion
                }
                else
                {
                    #region cboType.SelectedIndex == 0: Chuyển Đi
                    if (!bolIsCallFromImportIMEI)
                    {
                        objThread.Start();
                        GetStoreInStock(tblProduct, cboFromStoreID.StoreID);
                    }
                    if (dtbStoreInStock == null)
                        return;
                    DataRow rowStoreID = tblProduct.Rows[1];

                    var rlsProductID = tblProduct.AsEnumerable().Where(r => !string.IsNullOrEmpty(r[0].ToString())).Select(r => r[0].ToString()).ToList();
                    var rlsToStoreID = tblProduct.AsEnumerable().Where(r => !string.IsNullOrEmpty(r[0].ToString())).Select(r => r[0].ToString()).ToList();
                    rlsProductID.RemoveAt(0);
                    var rlsDup = rlsProductID.Distinct().ToList();
                    var arrMainGroupID = (from r in rlsDup
                                          join x in dtbProductOld.AsEnumerable()
                                          on r.ToString().Trim() equals x["PRODUCTID"].ToString().Trim()
                                          select x["MAINGROUPID"].ToString()).ToList();


                    //Key: Ma kho, Value: Nganh hang
                    Dictionary<int, List<int>> dicCheckMainGroup = new Dictionary<int, List<int>>();
                    int intStoreIDCheck = 0;

                    for (int icol = 1; icol < tblProduct.Columns.Count; icol++)
                    {
                        if (IsNumber(rowStoreID[icol].ToString()))
                        {
                            intStoreIDCheck = Convert.ToInt32(rowStoreID[icol].ToString());

                            if (dicCheckMainGroup.ContainsKey(intStoreIDCheck))
                                continue;
                            else
                            {
                                List<int> lstMainID = new List<int>();
                                foreach (string strMainGroupID in arrMainGroupID)
                                {
                                    if (!CheckMainGroupPermission(intStoreIDCheck, Convert.ToInt32(strMainGroupID), Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                                        lstMainID.Add(Convert.ToInt32(strMainGroupID));
                                }
                                if (lstMainID.Count > 0)
                                    dicCheckMainGroup.Add(intStoreIDCheck, lstMainID);
                            }
                        }
                    }

                    int intFromStoreID = cboFromStoreID.StoreID;
                    string strFromStoreName = "";
                    DataRow[] rowStore = dtbStoreCache.Select("STOREID = " + intFromStoreID);
                    if (rowStore.Length > 0)
                        strFromStoreName = rowStore[0]["STORENAME"].ToString();

                    int intToStoreID = -1;
                    string strStatus = "";
                    string strErrorContent = "";
                    int intStatusID = 0;
                    decimal decQuanity = 0;

                    Dictionary<string, decimal> dicCheckInStock = new Dictionary<string, decimal>();

                    for (int i = 2; i < tblProduct.Rows.Count; i++)
                    {
                        DataRow objRow = tblProduct.Rows[i];

                        string strProductID = Convert.ToString(objRow[0]);
                        string strProductName = string.Empty;
                        int intMainGroupID = 0;
                        string strMainGroupName = string.Empty;
                        bool bolIsAllowDecimal = false;
                        string strErrorMainGroup = string.Empty;

                        DataRow[] rowProduct = dtbProductOld.Select("PRODUCTID = '" + strProductID.Trim() + "'");
                        if (rowProduct.Length > 0)
                        {
                            strProductName = rowProduct[0]["PRODUCTNAME"].ToString();
                            bolIsAllowDecimal = Convert.ToBoolean(rowProduct[0]["ISALLOWDECIMAL"]);
                            intMainGroupID = Convert.ToInt32(rowProduct[0]["MAINGROUPID"]);
                            strMainGroupName = rowProduct[0]["MAINGROUPNAME"].ToString();

                            // Check phân quyền trên ngành hàng
                            if (!string.IsNullOrEmpty(strMainGroupList.Trim()) && !mainGroupList.Any(x => x.Trim().Equals(intMainGroupID.ToString())))
                                strErrorMainGroup = "Sản phẩm không thuộc ngành hàng đang xét";
                            else if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intFromStoreID, intMainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                                strErrorMainGroup = "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + strMainGroupName;
                            else if (!ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(intMainGroupID))
                                strErrorMainGroup = "Bạn không có quyền thao tác trên ngành hàng " + strMainGroupName;
                            // >Phan quyền
                        }

                        for (int j = 1; j < tblProduct.Columns.Count; j++)
                        {
                            string strStoreID = rowStoreID[j].ToString();
                            string strQuantity = objRow[j].ToString();
                            if (bolIsCallFromImportIMEI && string.IsNullOrEmpty(strQuantity))
                                continue;
                            if (!string.IsNullOrWhiteSpace(strStoreID))
                            {
                                intToStoreID = -1;
                                strStatus = "";
                                strErrorContent = "";
                                intStatusID = 0;
                                decQuanity = 0;

                                bool bolValidData = false;
                                if (!IsNumber(strStoreID))
                                    bolValidData = true;
                                else
                                    intToStoreID = Convert.ToInt32(strStoreID.Trim());

                                if (!IsNumber(strQuantity))
                                    bolValidData = true;
                                else
                                    decQuanity = Convert.ToDecimal(strQuantity.Trim());

                                if (bolValidData)
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Dữ liệu trên từng kho phải là số";
                                    intStatusID = 1;
                                }

                                decimal decRealQuanity = decQuanity;

                                string strToStoreName = "";
                                DataRow[] rowToStore = dtbStoreCache.Select("STOREID = " + intToStoreID);
                                if (rowToStore.Length > 0)
                                    strToStoreName = rowToStore[0]["STORENAME"].ToString();

                                decimal decFromInstockQuantity = 0;
                                decimal decToInstockQuantity = 0;

                                if (rowProduct.Length == 0)
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Mã sản phẩm không tồn tại";
                                    intStatusID = 1;
                                    DataRow objRowResult1 = tblResult.NewRow();
                                    objRowResult1.ItemArray = new object[] { strProductID, strProductName, intFromStoreID.ToString() + "-" + strFromStoreName,
                                        decFromInstockQuantity, intToStoreID.ToString() + "-" + strToStoreName, decToInstockQuantity, decQuanity, strStatus,
                                        intStatusID, intIsUrgent, strErrorContent };
                                    tblResult.Rows.Add(objRowResult1);
                                    if (!bolIsExistsError && intStatusID != 0)
                                    {
                                        bolIsExistsError = true;
                                    }
                                    continue;
                                }

                                if (strFromStoreName.Length > 0 && strToStoreName.Length > 0)
                                {
                                    decFromInstockQuantity = GetQuantity(strProductID, intFromStoreID);
                                    //Lấy dữ liệu tồn kho theo sản phẩm để kiểm tra dữ liệu import excel
                                    if (!dicCheckInStock.ContainsKey(strProductID))
                                    {
                                        dicCheckInStock.Add(strProductID, decFromInstockQuantity);
                                    }
                                    decToInstockQuantity = GetQuantity(strProductID, intToStoreID);
                                    //decFromInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intFromStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                                    //decToInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);

                                    if (intFromStoreID == intToStoreID)
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Kho nhập trùng với kho xuất";
                                        intStatusID = 1;
                                    }
                                    else if (!string.IsNullOrEmpty(strErrorMainGroup))
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = strErrorMainGroup;
                                        intStatusID = 1;
                                    }
                                    else if (dicCheckMainGroup.ContainsKey(intToStoreID) && dicCheckMainGroup[intToStoreID].Contains(intMainGroupID))
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + strMainGroupName;
                                        intStatusID = 1;
                                    }
                                    else if (decQuanity > 0)
                                    {
                                        if (CheckProductExist(intFromStoreID, intToStoreID, strProductID))
                                        {
                                            strStatus = "Lỗi";
                                            strErrorContent = "Sản phẩm chuyển kho đã tồn tại";
                                            intStatusID = 1;
                                        }
                                        else
                                        {
                                            if (decQuanity > dicCheckInStock[strProductID])
                                            {
                                                strStatus = "Cảnh báo";
                                                strErrorContent = "Số lượng chuyển lớn hơn số lượng tồn";
                                                decRealQuanity = dicCheckInStock[strProductID];
                                                dicCheckInStock[strProductID] -= dicCheckInStock[strProductID];
                                                intStatusID = 2;
                                            }
                                            else
                                            {
                                                strStatus = "Thành công";
                                                strErrorContent = "";
                                                intStatusID = 0;
                                                dicCheckInStock[strProductID] -= decQuanity;
                                            }

                                            #region Code cũ
                                            //if (decQuanity > decFromInstockQuantity)
                                            //{
                                            //    strStatus = "Cảnh báo";
                                            //    strErrorContent = "Số lượng chuyển lớn hơn số lượng tồn";
                                            //    decRealQuanity = decFromInstockQuantity;
                                            //    intStatusID = 2;
                                            //}
                                            //else
                                            //{
                                            //    strStatus = "Thành công";
                                            //    strErrorContent = "";
                                            //    intStatusID = 0;
                                            //}
                                            #endregion

                                            if (decRealQuanity > 0)
                                            {
                                                AddProduct(strProductID, strProductName, intFromStoreID, intFromStoreID.ToString() + "-" + strFromStoreName, intToStoreID,
                                                intToStoreID.ToString() + "-" + strToStoreName, decFromInstockQuantity, decToInstockQuantity, decRealQuanity, bolIsAllowDecimal, Convert.ToBoolean(intIsUrgent));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Số lượng chuyển phải lớn hơn 0";
                                        intStatusID = 1;
                                    }
                                }
                                else
                                {
                                    if (strFromStoreName.Length == 0)
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Sai kho xuất";
                                        intStatusID = 1;
                                    }
                                    else
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Sai kho nhập";
                                        intStatusID = 1;
                                    }
                                }
                                if (!bolIsExistsError && intStatusID != 0)
                                {
                                    bolIsExistsError = true;
                                }
                                DataRow objRowResult = tblResult.NewRow();
                                objRowResult.ItemArray = new object[] { strProductID, strProductName, intFromStoreID.ToString() + "-" +strFromStoreName,
                                    decFromInstockQuantity, intToStoreID.ToString() + "-" + strToStoreName, decToInstockQuantity, decQuanity, strStatus,
                                    intStatusID, intIsUrgent, strErrorContent, string.Empty };
                                tblResult.Rows.Add(objRowResult);
                            }
                        }
                    }

                    frmWaitDialog.Close();
                    Thread.Sleep(0);
                    objThread.Abort();

                    #endregion
                }

            }
            catch (Exception objExce)
            {
                Library.AppCore.SystemErrorWS.Insert("Lỗi nhập thông tin chuyển kho từ Excel ", objExce, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (bolIsCallFromImportIMEI)
                dtbImportQuantityWithIMEI = tblResult.Copy();
            else
            {
                if (bolIsExistsError)
                {
                    frmShowImportResult frmShowImportResult1 = new frmShowImportResult();
                    frmShowImportResult1.ImportResult = tblResult;
                    frmShowImportResult1.ShowDialog();
                }
            }

        }

        private bool CheckExcelQuantity(DataTable tProduct)
        {
            try
            {
                if (string.IsNullOrEmpty(tProduct.Rows[0][0].ToString().Trim().ToLower())
                    || tProduct.Rows[0][0].ToString().Trim().ToLower() != "mã sản phẩm")
                    return false;
                if (string.IsNullOrEmpty(tProduct.Rows[1][0].ToString().Trim().ToLower())
                    || tProduct.Rows[1][0].ToString().Trim().ToLower() != "productid")
                    return false;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
            return true;
        }

        private bool CheckExcelIMEI(DataTable tProduct)
        {
            try
            {
                if (string.IsNullOrEmpty(tProduct.Rows[0][0].ToString().Trim().ToLower())
                    || tProduct.Rows[0][0].ToString().Trim().ToLower() != "mã kho")
                    return false;
                if (string.IsNullOrEmpty(tProduct.Rows[1][0].ToString().Trim().ToLower())
                    || tProduct.Rows[1][0].ToString().Trim().ToLower() != "tên kho")
                    return false;
                if (string.IsNullOrEmpty(tProduct.Rows[2][0].ToString().Trim().ToLower())
                    || tProduct.Rows[2][0].ToString().Trim().ToLower() != "mã sản phẩm")
                    return false;
                if (string.IsNullOrEmpty(tProduct.Rows[3][0].ToString().Trim().ToLower())
                    || tProduct.Rows[3][0].ToString().Trim().ToLower() != "tên sản phẩm")
                    return false;
                if (string.IsNullOrEmpty(tProduct.Rows[4][0].ToString().Trim().ToLower())
                    || tProduct.Rows[4][0].ToString().Trim().ToLower() != "imei")
                    return false;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
            return true;
        }

        private bool CheckMainGroupPermission(int intStoreID, int intMainGroupID, EnumType.StoreMainGroupPermissionType enuStoreMainGroupPermissionType)
        {
            if (dtbStoreMainGroup == null)
                return false;

            StringBuilder arrCondition = new StringBuilder();
            arrCondition.Append("StoreID = " + intStoreID + " and MainGroupID = " + intMainGroupID);
            switch (enuStoreMainGroupPermissionType)
            {
                case EnumType.StoreMainGroupPermissionType.AUTOSTORECHANGE:
                    arrCondition.Append(" and ISCANAUTOSTORECHANGE = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.INPUT:
                    arrCondition.Append(" and ISCANINPUT = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.INPUTRETURN:
                    arrCondition.Append(" and ISCANINPUTRETURN = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.OUTPUT:
                    arrCondition.Append(" and ISCANOUTPUT = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.PURCHASEORDER:
                    arrCondition.Append(" and ISCANPURCHASEORDER = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.SALEORDER:
                    arrCondition.Append(" and ISCANSALEORDER = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT:
                    arrCondition.Append(" and ISCANSTORECHANGEINPUT = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT:
                    arrCondition.Append(" and ISCANSTORECHANGEOUTPUT = 1");
                    break;
            }
            return DataTableClass.CheckIsExist(dtbStoreMainGroup, arrCondition.ToString());
        }

        private bool CheckPermission(int intMainGroupID, EnumType.IsNewPermissionType enumIsNewPermissionType)
        {
            if (SystemConfig.objSessionUser.UserName == "administrator")
                return true;

            if (dtbMainGroupPermission == null)
            {
                return false;
            }
            StringBuilder strFilter = new StringBuilder();
            strFilter.Append("MainGroupID=" + intMainGroupID);
            if (enumIsNewPermissionType == EnumType.IsNewPermissionType.ISNEW)
                strFilter.Append(" and ISNEW = 1");
            else if (enumIsNewPermissionType == EnumType.IsNewPermissionType.ISOLD)
                strFilter.Append(" and ISOLD = 1");
            DataRow[] rows = dtbMainGroupPermission.Select(strFilter.ToString());
            if (rows.Length < 1)
                return false;
            return true;
        }
        private void Excute()
        {
            frmWaitDialog.Show(string.Empty, "Đang tạo dữ liệu chia hàng");
        }

        private void ExcuteHint()
        {
            frmWaitDialog.Show(string.Empty, "Đang lấy dữ liệu từ Excel");
        }
        private void CustomFlex()
        {
            string[] FieldName = new string[] { "QuantityInStock", "Select", "ProductID", "ProductName", "FromStoreName", "FromStoreInStock", "ToStoreName", "ToStoreInStock", "Quantity", "StoreChangeCommandQuantity", "StoreChangeOrderQuantity", "IsUrgent", "IsAllowDecimal", "IMEI" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdListAutoStoreChange, true, false, true, true, FieldName);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvListAutoStoreChange);

            grvListAutoStoreChange.Columns["QuantityInStock"].Visible = false;
            grvListAutoStoreChange.Columns["Select"].OptionsColumn.AllowEdit = true;
            grvListAutoStoreChange.Columns["ProductID"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["ProductID"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "ProductID", "Cộng:")});
            grvListAutoStoreChange.Columns["ProductName"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["ProductName"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "ProductID", "{0:N0}")});
            grvListAutoStoreChange.Columns["FromStoreName"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["FromStoreInStock"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["ToStoreName"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["ToStoreInStock"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["Quantity"].OptionsColumn.AllowEdit = true;
            grvListAutoStoreChange.Columns["IMEI"].OptionsColumn.AllowEdit = false;

            //Nếu là phần gọi IMEI 
            if (bolIsCallFromImportIMEI)
            {
                grvListAutoStoreChange.Columns["IMEI"].Visible = true;
                grvListAutoStoreChange.Columns["IMEI"].VisibleIndex = 5;
            }
            else
                grvListAutoStoreChange.Columns["IMEI"].Visible = false;

            grvListAutoStoreChange.Columns["IsAllowDecimal"].Visible = false;
            grvListAutoStoreChange.Columns["Select"].Caption = "Chọn";
            grvListAutoStoreChange.Columns["ProductID"].Caption = "Mã sản phẩm";
            grvListAutoStoreChange.Columns["ProductName"].Caption = "Tên sản phẩm";
            grvListAutoStoreChange.Columns["FromStoreName"].Caption = "Từ kho";
            grvListAutoStoreChange.Columns["FromStoreInStock"].Caption = "Số lượng tồn";
            grvListAutoStoreChange.Columns["ToStoreName"].Caption = "Đến kho";
            grvListAutoStoreChange.Columns["ToStoreInStock"].Caption = "Số lượng tồn";
            grvListAutoStoreChange.Columns["Quantity"].Caption = "Số lượng chuyển";
            grvListAutoStoreChange.Columns["IsUrgent"].Caption = "Chuyển gấp";
            grvListAutoStoreChange.Columns["IMEI"].Caption = "IMEI";
            grvListAutoStoreChange.Columns["ProductID"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            grvListAutoStoreChange.Columns["ProductName"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            grvListAutoStoreChange.Columns["FromStoreName"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            grvListAutoStoreChange.Columns["FromStoreInStock"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            grvListAutoStoreChange.Columns["Select"].Width = 50;
            grvListAutoStoreChange.Columns["FromStoreName"].Width = 240;
            grvListAutoStoreChange.Columns["ToStoreName"].Width = 240;
            grvListAutoStoreChange.Columns["ProductID"].Width = 110;
            grvListAutoStoreChange.Columns["ToStoreInStock"].Width = 80;
            grvListAutoStoreChange.Columns["Quantity"].Width = 80;
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].Width = 80;
            grvListAutoStoreChange.Columns["StoreChangeOrderQuantity"].Width = 80;
            grvListAutoStoreChange.Columns["IsUrgent"].Width = 90;
            grvListAutoStoreChange.Columns["ProductName"].Width = 200;
            grvListAutoStoreChange.Columns["QuantityInStock"].Width = 80;
            grvListAutoStoreChange.Columns["FromStoreInStock"].Width = 80;
            grvListAutoStoreChange.Columns["IMEI"].Width = 135;
            grvListAutoStoreChange.Columns["ProductID"].Visible = true;
            grvListAutoStoreChange.Columns["FromStoreInStock"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grvListAutoStoreChange.Columns["FromStoreInStock"].DisplayFormat.FormatString = "#,##0.####";
            grvListAutoStoreChange.Columns["ToStoreInStock"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grvListAutoStoreChange.Columns["ToStoreInStock"].DisplayFormat.FormatString = "#,##0.####";
            grvListAutoStoreChange.Columns["QuantityInStock"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grvListAutoStoreChange.Columns["QuantityInStock"].DisplayFormat.FormatString = "#,##0.####";
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].DisplayFormat.FormatString = "#,##0.####";
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].AppearanceCell.Options.UseTextOptions = true;
            grvListAutoStoreChange.Columns["StoreChangeOrderQuantity"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grvListAutoStoreChange.Columns["StoreChangeOrderQuantity"].DisplayFormat.FormatString = "#,##0.####";
            grvListAutoStoreChange.Columns["StoreChangeOrderQuantity"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            grvListAutoStoreChange.Columns["StoreChangeOrderQuantity"].AppearanceCell.Options.UseTextOptions = true;
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].Caption = "SL đã tạo lệnh";
            grvListAutoStoreChange.Columns["StoreChangeOrderQuantity"].Caption = "SL đã tạo yêu cầu";
            grvListAutoStoreChange.Columns["StoreChangeOrderQuantity"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["Select"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            grvListAutoStoreChange.Columns["ProductID"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].Visible = false;

            DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsUrgent = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            chkCheckBoxIsUrgent.ValueChecked = ((String)("True"));
            chkCheckBoxIsUrgent.ValueUnchecked = ((String)("False"));
            grvListAutoStoreChange.Columns["IsUrgent"].ColumnEdit = chkCheckBoxIsUrgent;

            DevExpress.XtraEditors.Repository.RepositoryItemTextEdit resQuantityEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            resQuantityEdit.AutoHeight = false;
            resQuantityEdit.DisplayFormat.FormatString = "#,##0";
            resQuantityEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            resQuantityEdit.EditFormat.FormatString = "#,###,###,##0.####";
            resQuantityEdit.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            resQuantityEdit.Mask.EditMask = "#,###,###,###,###,##0.####;";
            resQuantityEdit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            resQuantityEdit.Name = "resQuantityEdit";
            resQuantityEdit.NullText = "0";
            resQuantityEdit.NullValuePrompt = "0";
            resQuantityEdit.NullValuePromptShowForEmptyValue = true;
            grvListAutoStoreChange.Columns["Quantity"].ColumnEdit = resQuantityEdit;

            grvListAutoStoreChange.Columns["ProductID"].AppearanceCell.Font = new Font("Tahoma", 9.75F, FontStyle.Regular);
            grvListAutoStoreChange.Columns["ProductID"].AppearanceCell.ForeColor = Color.Red;
            grvListAutoStoreChange.Columns["ProductName"].AppearanceCell.Font = new Font("Tahoma", 9.75F, FontStyle.Regular);
            grvListAutoStoreChange.Columns["ProductName"].AppearanceCell.ForeColor = Color.Red;

            for (int i = 0; i < grvListAutoStoreChange.Columns.Count; i++)
            {
                grvListAutoStoreChange.Columns[i].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            }
            grvListAutoStoreChange.OptionsView.ShowAutoFilterRow = true;


        }
        private bool SaveAutoStoreChange()
        {
            List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand> lstStoreChangeCommand = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand>();
            for (int i = 0; i < dtbAutoStoreChange.Rows.Count; i++)
            {
                if (!Convert.ToBoolean(dtbAutoStoreChange.Rows[i]["Select"]))
                    continue;
                PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand objStoreChangeCommandBO = new PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand();
                objStoreChangeCommandBO.FromStoreID = Convert.ToInt32(dtbAutoStoreChange.Rows[i]["FromStoreID"]);
                objStoreChangeCommandBO.ToStoreID = Convert.ToInt32(dtbAutoStoreChange.Rows[i]["ToStoreID"]);
                objStoreChangeCommandBO.StoreChangeCommandTypeID = intStoreChangeCommandTypeID;
                objStoreChangeCommandBO.CreatedUser = SystemConfig.objSessionUser.UserName;
                objStoreChangeCommandBO.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objStoreChangeCommandBO.StoreChangeCommandTypeID = 1;
                objStoreChangeCommandBO.IsUrgent = Convert.ToBoolean(dtbAutoStoreChange.Rows[i]["IsUrgent"]);
                string strProductID = Convert.ToString(dtbAutoStoreChange.Rows[i]["ProductID"]).Trim();
                decimal decQuantity = Convert.ToDecimal("0" + Convert.ToDecimal(dtbAutoStoreChange.Rows[i]["Quantity"]));
                if (decQuantity > 0)
                {
                    List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail> lstStoreChangeCommandDetail = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail>();
                    PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail objStoreChangeCommandDetail = new PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail();
                    objStoreChangeCommandDetail.ProductID = strProductID;
                    objStoreChangeCommandDetail.Quantity = decQuantity;
                    if (!Convert.ToBoolean(dtbAutoStoreChange.Rows[i]["IsAllowDecimal"]))
                    {
                        objStoreChangeCommandDetail.Quantity = Math.Round(objStoreChangeCommandDetail.Quantity, 0, MidpointRounding.AwayFromZero);
                    }
                    objStoreChangeCommandDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;
                    lstStoreChangeCommandDetail.Add(objStoreChangeCommandDetail);
                    objStoreChangeCommandBO.StoreChangeCommandDetailList = lstStoreChangeCommandDetail.ToArray();
                }
                lstStoreChangeCommand.Add(objStoreChangeCommandBO);
            }
            objPLCStoreChangeCommand.InsertMulti(lstStoreChangeCommand);
            return !SystemConfig.objSessionUser.ResultMessageApp.IsError;
        }
        private void LoadComboBox()
        {
            cboType.SelectedIndex = 0;
            cboIsShowProduct.SelectedIndex = 0;
            //cboMainGroupID.InitControl(false);
            cboMainGroupID.InitControl(true);
            if (dtbStore != null && dtbStore.Rows.Count > 0)
            {
                cboFromStoreID.InitControl(false, dtbStore.Copy());
                cboToStoreID.InitControl(true, dtbStore.Copy());
            }
            else
            {
                cboFromStoreID.InitControl(false);
                dtbStore = cboFromStoreID.DataSource as DataTable;
                cboToStoreID.InitControl(true);
            }
            cboType_SelectionChangeCommitted(null, null);

            if (chkIsCanUseOldCode.Checked)
                dtbStoreOldId = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_STORE_OLDSTORE_SRH");
            dtbProductOld = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
        }
        private void InitAutoStoreChangeTable()
        {
            dtbAutoStoreChange = new DataTable();
            dtbAutoStoreChange.Columns.Add("Select", typeof(bool));
            dtbAutoStoreChange.Columns.Add("ProductID", typeof(string));
            dtbAutoStoreChange.Columns.Add("ProductName", typeof(String));
            dtbAutoStoreChange.Columns.Add("FromStoreID", typeof(int));
            dtbAutoStoreChange.Columns.Add("FromStoreName", typeof(String));
            dtbAutoStoreChange.Columns.Add("FromStoreInStock", typeof(decimal));
            dtbAutoStoreChange.Columns.Add("ToStoreID", typeof(int));
            dtbAutoStoreChange.Columns.Add("ToStoreName", typeof(String));
            dtbAutoStoreChange.Columns.Add("ToStoreInStock", typeof(decimal));
            dtbAutoStoreChange.Columns.Add("Quantity", typeof(decimal));
            dtbAutoStoreChange.Columns.Add("QuantityInStock", typeof(decimal));
            dtbAutoStoreChange.Columns.Add("IsAllowDecimal", typeof(bool));
            dtbAutoStoreChange.Columns.Add("IMEI", typeof(string)).DefaultValue = string.Empty;
            if (!dtbAutoStoreChange.Columns.Contains("StoreChangeCommandQuantity"))
            {
                DataColumn dtColumn = new DataColumn("StoreChangeCommandQuantity", typeof(decimal));
                dtColumn.DefaultValue = 0;
                dtbAutoStoreChange.Columns.Add(dtColumn);
            }

            if (!dtbAutoStoreChange.Columns.Contains("StoreChangeOrderQuantity"))
            {
                DataColumn dtColumn = new DataColumn("StoreChangeOrderQuantity", typeof(decimal));
                dtColumn.DefaultValue = 0;
                dtbAutoStoreChange.Columns.Add(dtColumn);
            }
            if (!dtbAutoStoreChange.Columns.Contains("IsUrgent"))
            {
                DataColumn dtColumn = new DataColumn("IsUrgent");
                dtColumn.DefaultValue = false;
                dtbAutoStoreChange.Columns.Add(dtColumn);
            }
        }
        private void BindData()
        {
            if (dtbAutoStoreChange == null || dtbAutoStoreChange.Rows.Count == 0)
            {
                InitAutoStoreChangeTable();
                return;
            }
            if (!dtbAutoStoreChange.Columns.Contains("StoreChangeCommandQuantity"))
            {
                DataColumn dtColumn = new DataColumn("StoreChangeCommandQuantity");
                dtColumn.DefaultValue = 0;
                dtbAutoStoreChange.Columns.Add(dtColumn);
            }

            if (!dtbAutoStoreChange.Columns.Contains("StoreChangeOrderQuantity"))
            {
                DataColumn dtColumn = new DataColumn("StoreChangeOrderQuantity");
                dtColumn.DefaultValue = 0;
                dtbAutoStoreChange.Columns.Add(dtColumn);
            }
            string strProductIDList = string.Empty;
            string strFromStoreIDList = string.Empty;
            string strToStoreIDList = string.Empty;
            if (dtbAutoStoreChange.Rows.Count > 0)
            {
                //Gen lại List 
                foreach (DataRow item in dtbAutoStoreChange.Rows)
                {
                    if (string.IsNullOrEmpty(item["IMEI"].ToString().Trim()))
                    {
                        if (strProductIDList != string.Empty)
                            strProductIDList += ",";
                        strProductIDList += item["ProductID"].ToString();
                        if (strFromStoreIDList != string.Empty)
                            strFromStoreIDList += ",";
                        strFromStoreIDList += item["FromStoreID"].ToString();
                        if (strToStoreIDList != string.Empty)
                            strToStoreIDList += ",";
                        strToStoreIDList += item["ToStoreID"].ToString();
                    }
                }
            }
            dtbAutoStoreChangeQuantity = new PLC.StoreChangeCommand.PLCAutoSplitProduct().GetAutoStoreChangeQuantity(strProductIDList, strFromStoreIDList, strToStoreIDList);
            if (!dtbAutoStoreChange.Columns.Contains("IsUrgent"))
            {
                DataColumn dtColumn = new DataColumn("IsUrgent");
                dtColumn.DefaultValue = false;
                dtbAutoStoreChange.Columns.Add(dtColumn);
            }
            if (dtbAutoStoreChange.Rows.Count > 0 && dtbAutoStoreChangeQuantity != null && dtbAutoStoreChangeQuantity.Rows.Count > 0)
            {
                foreach (DataRow item in dtbAutoStoreChange.Rows)
                {
                    if (string.IsNullOrEmpty(item["IMEI"].ToString().Trim()))
                    {
                        string strFilter = string.Format("ProductID='{0}' and FromStoreID={1} and ToStoreID={2}", item["ProductID"].ToString().Trim(), item["FromStoreID"].ToString(), item["ToStoreID"].ToString());
                        DataRow[] drdata = dtbAutoStoreChangeQuantity.Select(strFilter + " and TypeID=0");
                        if (drdata.Length > 0)
                            item["StoreChangeCommandQuantity"] = Convert.ToDecimal(drdata[0]["Quantity"]);
                        drdata = dtbAutoStoreChangeQuantity.Select(strFilter + " and TypeID=1");
                        if (drdata.Length > 0)
                            item["StoreChangeOrderQuantity"] = Convert.ToDecimal(drdata[0]["Quantity"]);
                    }
                }
            }
            DataView dvAutoStoreChange = new DataView(dtbAutoStoreChange);
            dvAutoStoreChange.Sort = "ProductID, FromStoreID";

            grdListAutoStoreChange.DataSource = dvAutoStoreChange;

            CustomFlex();
            if (dtbAutoStoreChange.Rows.Count > 0)
                cmdCreateStoreChangeCommand.Enabled = true;
        }
        private void AddProduct(String strProductID, String strProductName, int intFromStoreID, String strFromStoreName,
            int intToStoreID, String strToStoreName, decimal decFromInstockQuantity, decimal decToInstockQuantity,
            decimal decQuantity, bool bolIsAllowDecimal, bool bolIsUrgent)
        {

            DataRow objRow = dtbAutoStoreChange.NewRow();
            objRow.ItemArray = new object[] {true, strProductID, strProductName, intFromStoreID, strFromStoreName,
                decFromInstockQuantity, intToStoreID, strToStoreName, decToInstockQuantity, decQuantity,
                decFromInstockQuantity, bolIsAllowDecimal, string.Empty,0, 0, bolIsUrgent};
            dtbAutoStoreChange.Rows.Add(objRow);
        }

        private void AddProductWithIMEI(String strProductID, String strProductName, int intFromStoreID, String strFromStoreName, decimal decFromInstockQuantity,
           int intToStoreID, String strToStoreName, decimal decToInstockQuantity,
           decimal decQuantity, bool bolIsAllowDecimal, bool bolIsUrgent, string strIMEI)
        {

            DataRow objRow = dtbAutoStoreChange.NewRow();
            objRow.ItemArray = new object[] {true, strProductID, strProductName, intFromStoreID, strFromStoreName,
                decFromInstockQuantity, intToStoreID, strToStoreName, decToInstockQuantity, 1, 1
                , bolIsAllowDecimal, strIMEI, 0, 0, bolIsUrgent};
            dtbAutoStoreChange.Rows.Add(objRow);
        }

        private bool CheckProductExist(int intFromStoreID, int intToStoreID, String strProductID)
        {
            String strSelectExp = "FromStoreID = " + Convert.ToString(intFromStoreID) + " and ToStoreID = "
                + Convert.ToString(intToStoreID) + " and ProductID = '" + strProductID + "'";
            DataRow[] arrRow = dtbAutoStoreChange.Select(strSelectExp);
            return (arrRow.Length > 0);
        }

        private bool CheckQuantity()
        {
            dtbAutoStoreChange.AcceptChanges();
            for (int i = 0; i < grvListAutoStoreChange.RowCount; i++)
            {
                DataRow row = grvListAutoStoreChange.GetDataRow(i);
                if (row == null)
                    continue;
                if (string.IsNullOrEmpty(row["IMEI"].ToString().Trim()))
                {
                    decimal decFromInstockQuanity = Convert.ToDecimal(row["FromStoreInStock"]);
                    decimal decTotalQuantity = Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbAutoStoreChange.Compute("sum(Quantity)",
                        "Select = 1 and ProductID = '" + Convert.ToString(row["ProductID"]).Trim()
                        + "' and FromStoreID = " + Convert.ToString(row["FromStoreID"])), 0));
                    if (decTotalQuantity > decFromInstockQuanity)
                    {
                        MessageBox.Show(this, "Tổng số lượng chuyển kho của sản phẩm "
                            + Convert.ToString(row["ProductName"]).Trim()
                            + " \nTại kho " + Convert.ToString(row["FromStoreName"]).Trim()
                            + " lớn hơn số lượng tồn." + (decFromInstockQuanity > decTotalQuantity ? (". Số lượng còn lại " + (decFromInstockQuanity - decTotalQuantity)) : ""), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        grvListAutoStoreChange.FocusedColumn = grvListAutoStoreChange.Columns["Quantity"];
                        grvListAutoStoreChange.FocusedRowHandle = i;
                        return false;
                    }
                }
            }

            //for (int i = 1; i < flexListAutoStoreChange.Rows.Count; i++)
            //{
            //    decimal decFromInstockQuanity = Convert.ToDecimal(flexListAutoStoreChange[i, "FromStoreInStock"]);
            //    decimal decTotalQuantity = Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbAutoStoreChange.Compute("sum(Quantity)",
            //        "Select = 1 and ProductID = '" + Convert.ToString(flexListAutoStoreChange[i, "ProductID"]).Trim()
            //        + "' and FromStoreID = " + Convert.ToString(flexListAutoStoreChange[i, "FromStoreID"])), 0));
            //    if (decTotalQuantity > decFromInstockQuanity)
            //    {
            //        MessageBox.Show(this, "Tổng số lượng chuyển kho của sản phẩm "
            //            + Convert.ToString(flexListAutoStoreChange[i, "ProductName"]).Trim()
            //            + " \nTại kho " + Convert.ToString(flexListAutoStoreChange[i, "FromStoreName"]).Trim()
            //            + " lớn hơn số lượng tồn." + (decFromInstockQuanity > decTotalQuantity ? (". Số lượng còn lại " + (decFromInstockQuanity - decTotalQuantity)) : ""), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        flexListAutoStoreChange.Select(i, 9);
            //        return false;
            //    }
            //}

            return true;
        }

        private bool CheckQuantity(string strProductID, string strProductName, int intFromStoreID, string strFromStoreName, decimal decFromInstockQuanity, decimal decQuanityAdd)
        {
            dtbAutoStoreChange.AcceptChanges();
            decimal decTotalQuantity = Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbAutoStoreChange.Compute("sum(Quantity)",
                "Select = 1 and ProductID = '" + strProductID.Trim()
                + "' and FromStoreID = " + intFromStoreID), 0));
            if (decTotalQuantity + decQuanityAdd > decFromInstockQuanity)
            {
                MessageBox.Show(this, "Tổng số lượng chuyển kho của sản phẩm "
                    + strProductName.Trim()
                    + " \nTại kho " + strFromStoreName
                    + " lớn hơn số lượng tồn" + (decFromInstockQuanity > decTotalQuantity ? (". Số lượng còn lại " + (decFromInstockQuanity - decTotalQuantity)) : ""), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private DataTable CreateImportResultTable()
        {
            DataTable dataImproted = new DataTable();
            dataImproted.Columns.Add("ProductID", typeof(String));
            dataImproted.Columns.Add("ProductName", typeof(String));
            dataImproted.Columns.Add("FromStoreName", typeof(String));
            dataImproted.Columns.Add("FromInStockQuantity", typeof(decimal));
            dataImproted.Columns.Add("ToStoreName", typeof(String));
            dataImproted.Columns.Add("ToInStockQuantity", typeof(decimal));
            dataImproted.Columns.Add("Quantity", typeof(decimal));
            dataImproted.Columns.Add("Status", typeof(String));
            dataImproted.Columns.Add("StatusID", typeof(Int32));
            dataImproted.Columns.Add("IsUrgent", typeof(Boolean));
            dataImproted.Columns.Add("ErrorContent", typeof(String));
            dataImproted.Columns.Add("IMEI", typeof(string)).DefaultValue = string.Empty;
            return dataImproted;
        }

        private void ExtendColumnMultipleImport(ref DataTable importedData)
        {
            importedData.Columns[XlsColumnMapper.FROMSTOREID].ColumnName = XlsColumnMapper.FROMSTOREID.ToString();
            importedData.Columns[XlsColumnMapper.FromStoreName].ColumnName = XlsColumnMapper.FromStoreName.ToString();
            importedData.Columns[XlsColumnMapper.ProductID].ColumnName = XlsColumnMapper.ProductID.ToString();
            importedData.Columns[XlsColumnMapper.ProductName].ColumnName = XlsColumnMapper.ProductName.ToString();
            importedData.Columns[XlsColumnMapper.Quantity].ColumnName = XlsColumnMapper.Quantity.ToString();
            importedData.Columns[XlsColumnMapper.ToStoreID].ColumnName = XlsColumnMapper.ToStoreID.ToString();
            importedData.Columns[XlsColumnMapper.ToStoreName].ColumnName = XlsColumnMapper.ToStoreName.ToString();
            while (importedData.Columns.Count > 7)
                importedData.Columns.RemoveAt(7);
            importedData.Columns.Add(XlsColumnMapper.ErrorContent.ToString());
            importedData.Columns.Add(XlsColumnMapper.Status.ToString());
            importedData.Columns.Add(XlsColumnMapper.StatusID.ToString());
            importedData.Columns.Add(XlsColumnMapper.FromInStockQuantity.ToString());
            importedData.Columns.Add(XlsColumnMapper.ToInStockQuantity.ToString());
            importedData.Columns.Add(XlsColumnMapper.IMEI.ToString());
            importedData.Columns.Add(XlsColumnMapper.IsUrgent.ToString(), typeof(bool));
        }

        private void ImportExcel()
        {
            DataTable tblProduct = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(6);
            if (tblProduct == null)
            {
                MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (tblProduct.Rows.Count == 0)
                return;
            tblProduct.Rows.RemoveAt(0);
            DataTable tblResult = CreateImportResultTable();
            dtbAutoStoreChange.Clear();
            bool bolIsPermission = Library.AppCore.SystemConfig.objSessionUser.IsPermission("AUTOSTORECHANGE_CHECKEMERGENCY");
            int intIsUrgent = 0;
            bool bolIsExistsError = false;

            try
            {
                foreach (DataRow objRow in tblProduct.Rows)
                {
                    String strProductID = Convert.ToString(objRow[0]).Trim();
                    int intFromStoreID = -1;
                    int intToStoreID = -1;
                    String strStatus = "";
                    String strErrorContent = "";
                    int intStatusID = 0;
                    decimal decQuanity = 0;
                    try
                    {
                        intFromStoreID = Convert.ToInt32(objRow[1]);
                        intToStoreID = Convert.ToInt32(objRow[2]);
                        decQuanity = Convert.ToDecimal(objRow[3]);
                    }
                    catch (Exception)
                    {
                        strStatus = "Lỗi";
                        strErrorContent = "[Từ kho] và [Đến kho] phải là số";
                        intStatusID = 1;
                    }
                    decimal decRealQuanity = decQuanity;
                    String strFromStoreName = ERP.MasterData.PLC.MD.PLCStore.GetStoreName(intFromStoreID);
                    String strToStoreName = ERP.MasterData.PLC.MD.PLCStore.GetStoreName(intToStoreID);
                    decimal decFromInstockQuantity = 0;
                    decimal decToInstockQuantity = 0;
                    //Nếu có quyền AutoStoreChange_CheckEmergency thì cho chuyển gấp
                    if (bolIsPermission)
                    {
                        try
                        {
                            if (Convert.IsDBNull(objRow[4]))
                                intIsUrgent = 0;
                            else
                            {
                                intIsUrgent = Convert.ToInt32(objRow[4]);
                                if (intIsUrgent > 1 || intIsUrgent < 0)
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Sai thông tin Chuyển gấp. Chỉ có thể là 0 hoặc 1";
                                    intStatusID = 1;
                                    intIsUrgent = 0;
                                }
                            }
                        }
                        catch
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Sai thông tin Chuyển gấp. Chỉ có thể là 0 hoặc 1";
                            intStatusID = 1;
                        }
                    }
                    String strProductName = string.Empty;
                    if (strFromStoreName.Length > 0 && strToStoreName.Length > 0)
                    {
                        decFromInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intFromStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                        decToInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                        ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                        string strMainGroupList = cboMainGroupID.MainGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");
                        string[] mainGroupList = strMainGroupList.Split(',');
                        if (objProduct == null)
                            return;
                        strProductName = objProduct.ProductName;
                        if (intFromStoreID == intToStoreID)
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Kho nhập trùng với kho xuất";
                            intStatusID = 1;
                        }
                        else if (!string.IsNullOrEmpty(strMainGroupList.Trim()) && !mainGroupList.Any(x => x.Trim().Equals(objProduct.MainGroupID.ToString().Trim())))
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Sản phẩm không thuộc ngành hàng đang xét";
                            intStatusID = 1;
                        }
                        else if (!CheckMainGroupPermission(intFromStoreID, objProduct.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + objProduct.MainGroupName;
                            intStatusID = 1;
                        }
                        else if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intToStoreID, objProduct.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + objProduct.MainGroupName;
                            intStatusID = 1;
                        }

                        else if (!CheckPermission(objProduct.MainGroupID, EnumType.IsNewPermissionType.ALL))
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Bạn không có quyền thao tác trên ngành hàng " + objProduct.MainGroupName;
                            intStatusID = 1;
                        }
                        else if (decQuanity > 0)
                        {
                            if (CheckProductExist(intFromStoreID, intToStoreID, strProductID))
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Sản phẩm chuyển kho đã tồn tại";
                                intStatusID = 1;
                            }
                            else
                            {
                                if (decFromInstockQuantity <= 0)
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Sản phẩm không tồn kho";
                                    intStatusID = 1;
                                }
                                else
                                {
                                    if (decQuanity > decFromInstockQuantity)
                                    {
                                        strStatus = "Cảnh báo";
                                        strErrorContent = "Số lượng chuyển lớn hơn số lượng tồn";
                                        decRealQuanity = decFromInstockQuantity;
                                        intStatusID = 2;
                                    }
                                    else
                                    {
                                        strStatus = "Thành công";
                                        strErrorContent = "";
                                        intStatusID = 0;
                                    }
                                    AddProduct(strProductID, strProductName, intFromStoreID, strFromStoreName, intToStoreID,
                                        strToStoreName, decFromInstockQuantity, decToInstockQuantity, decRealQuanity, true, Convert.ToBoolean(intIsUrgent));

                                }
                            }
                        }
                        else
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Số lượng chuyển phải lớn hơn 0";
                            intStatusID = 1;
                        }
                    }
                    else
                    {
                        if (strFromStoreName.Length == 0)
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Sai kho xuất";
                            intStatusID = 1;
                        }
                        else
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Sai kho nhập";
                            intStatusID = 1;
                        }
                    }
                    if (!bolIsExistsError && intStatusID != 0)
                    {
                        bolIsExistsError = true;
                    }
                    DataRow objRowResult = tblResult.NewRow();
                    objRowResult.ItemArray = new object[] { strProductID, strProductName, strFromStoreName,
                        decFromInstockQuantity, strToStoreName, decToInstockQuantity, decQuanity, strStatus,
                        intStatusID, intIsUrgent, strErrorContent };
                    tblResult.Rows.Add(objRowResult);
                }
            }
            catch (Exception objExce)
            {
                Library.AppCore.SystemErrorWS.Insert("Lỗi nhập thông tin chuyển kho từ Excel ", objExce, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (bolIsExistsError)
            {
                frmShowImportResult frmShowImportResult1 = new frmShowImportResult();
                frmShowImportResult1.ImportResult = tblResult;
                frmShowImportResult1.ShowDialog();
            }
        }

        /// <summary>
        /// Kiểm tra dữ liệu nhập vào
        /// </summary>
        private bool CheckInputCreateOrder(ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType, int intFromStoreID, int intToStoreID)
        {
            DataRow[] rowFromStore = dtbStoreCache.Select("STOREID = " + intFromStoreID);
            DataRow[] rowToStore = dtbStoreCache.Select("STOREID = " + intToStoreID);
            // 1 Kiem tra cong ty
            if (objStoreChangeType.CheckCompanyType == 1)
            {
                if (Convert.ToInt32(rowFromStore[0]["COMPANYID"]) != Convert.ToInt32(rowToStore[0]["COMPANYID"]))
                    return false;
            }
            else if (objStoreChangeType.CheckCompanyType == 2)
            {
                if (Convert.ToInt32(rowFromStore[0]["COMPANYID"]) == Convert.ToInt32(rowToStore[0]["COMPANYID"]))
                    return false;
            }
            // 2 kiem tra tỉnh
            if (objStoreChangeType.CheckProvinceType == 1)
            {
                if (Convert.ToInt32(rowFromStore[0]["PROVINCEID"]) != Convert.ToInt32(rowToStore[0]["PROVINCEID"]))
                    return false;
            }
            else if (objStoreChangeType.CheckProvinceType == 2)
            {
                if (Convert.ToInt32(rowFromStore[0]["PROVINCEID"]) == Convert.ToInt32(rowToStore[0]["PROVINCEID"]))
                    return false;
            }

            // 3 Kiem tra chi nhánh
            if (objStoreChangeType.CheckBranchType == 1)
            {
                if (Convert.ToInt32(rowFromStore[0]["BRANCHID"]) != Convert.ToInt32(rowToStore[0]["BRANCHID"]))
                    return false;
            }
            else if (objStoreChangeType.CheckBranchType == 2)
            {
                if (Convert.ToInt32(rowFromStore[0]["BRANCHID"]) == Convert.ToInt32(rowToStore[0]["BRANCHID"]))
                    return false;
            }

            return true;

        }

        /// <summary>
        /// Kiểm tra cùng tỉnh
        /// </summary>
        /// <param name="intFromStoreID">Mã kho xuất.</param>
        /// <param name="intToStoreID">Mã kho nhập.</param>
        private bool CheckSameProvince(int intFromStoreID, int intToStoreID)
        {
            ERP.MasterData.PLC.MD.WSStore.Store objFromStore = null;
            ERP.MasterData.PLC.MD.WSStore.ResultMessage objResultMessage = new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objFromStore, intFromStoreID);
            ERP.MasterData.PLC.MD.WSStore.Store objToStore = null;
            objResultMessage = new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, intToStoreID);
            return (objFromStore.ProvinceID == objToStore.ProvinceID);
        }

        private void CheckInputAdd()
        {
            lstProductIDNotPass.Clear();
            if (cboFromStoreID.StoreID <= 0)
            {
                string strMess = "Vui lòng chọn kho xuất";
                if (cboType.SelectedIndex == 1)
                    strMess = "Vui lòng chọn kho nhập";
                MessageBox.Show(this, strMess, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboFromStoreID.Focus();
                //bolIsStop = true;
                return;
            }
            if (string.IsNullOrEmpty(cboToStoreID.StoreIDList))
            {
                string strMess = "Vui lòng chọn kho nhập";
                if (cboType.SelectedIndex == 1)
                    strMess = "Vui lòng chọn kho xuất";
                MessageBox.Show(this, strMess, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboToStoreID.Focus();
                //bolIsStop = true;
                return;
            }
            string[] strArrProductID = cboProductList.ProductIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);

            if (string.IsNullOrEmpty(strArrProductID[0].ToString()))
            {
                MessageBox.Show(this, "Vui lòng chọn ít nhất một sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboProductList.Focus();
                //bolIsStop = true;
                return;
            }
            if (Convert.ToDecimal(txtQuantity.Value) <= 0)
            {
                MessageBox.Show(this, "Vui lòng nhập số lượng cần chuyển", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtQuantity.Focus();
                //bolIsStop = true;
                return;
            }
            //Kiểm tra theo list
            if (cboType.SelectedIndex == 0)
            {
                int intFromStoreID = Convert.ToInt32(cboFromStoreID.StoreID);
                string strToStoreIDList = cboToStoreID.StoreIDList;
                if (strToStoreIDList.Contains(intFromStoreID.ToString()))
                {
                    MessageBox.Show("Kho nhập phải khác với kho xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboToStoreID.Focus();
                    // bolIsStop = true;
                    return;
                }

                string[] strArrToStoreID = strToStoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
                foreach (var strProductID in strArrProductID)
                {
                    string strProductName = ERP.MasterData.PLC.MD.PLCProduct.GetProductName(strProductID);
                    for (int i = 0; i < strArrToStoreID.Length; i++)
                    {
                        if (CheckProductExist(intFromStoreID, Convert.ToInt32(strArrToStoreID[i]), strProductID))
                        {
                            MessageBox.Show(this, "Sản phẩm " + strProductName + " chuyển kho đã tồn tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            lstProductIDNotPass.Add(strProductID);
                            break;
                        }
                    }
                    decimal decInStockQuantity = new PLC.PLCProductInStock().GetQuantity(strProductID,
                        Convert.ToInt32(cboFromStoreID.StoreID), 1, intIsShowProduct, intIsCheckRealInput);
                    if (Convert.ToDecimal(txtQuantity.Value) > decInStockQuantity)
                    {
                        MessageBox.Show(this, "Số lượng cần chuyển của sản phẩm " + strProductName + " lớn hơn số lượng tồn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        lstProductIDNotPass.Add(strProductID);
                        continue;
                    }

                    if (!CheckQuantity(strProductID, ERP.MasterData.PLC.MD.PLCProduct.GetProductName(strProductID)
                        , Convert.ToInt32(cboFromStoreID.StoreID), ERP.MasterData.PLC.MD.PLCStore.GetStoreName(Convert.ToInt32(cboFromStoreID.StoreID)), decInStockQuantity,
                        Convert.ToDecimal(txtQuantity.Value)))
                    {
                        lstProductIDNotPass.Add(strProductID);
                        continue;
                    }
                }
            }
            else
            {
                int intToStoreID = Convert.ToInt32(cboFromStoreID.StoreID);
                string strFromStoreIDList = cboToStoreID.StoreIDList;
                if (strFromStoreIDList.Contains(intToStoreID.ToString()))
                {
                    MessageBox.Show("Kho nhập phải khác với kho xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboToStoreID.Focus();
                    //bolIsStop = true;
                    return;
                }

                string[] strArrFromStoreID = strFromStoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
                foreach (var strProductID in strArrProductID)
                {
                    string strProductName = ERP.MasterData.PLC.MD.PLCProduct.GetProductName(strProductID);
                    for (int i = 0; i < strArrFromStoreID.Length; i++)
                    {
                        if (CheckProductExist(Convert.ToInt32(strArrFromStoreID[i]), intToStoreID, strProductID))
                        {
                            MessageBox.Show(this, "Sản phẩm " + strProductName + " chuyển kho đã tồn tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            lstProductIDNotPass.Add(strProductID);
                            break;
                        }
                    }
                    for (int i = 0; i < strArrFromStoreID.Length; i++)
                    {
                        decimal decInStockQuantity = new PLC.PLCProductInStock().GetQuantity(strProductID,
                            Convert.ToInt32(strArrFromStoreID[i]), 1, intIsShowProduct, intIsCheckRealInput);
                        if (Convert.ToDecimal(txtQuantity.Value) > decInStockQuantity)
                        {
                            string strStoreName = ERP.MasterData.PLC.MD.PLCStore.GetStoreName(Convert.ToInt32(strArrFromStoreID[i]));
                            MessageBox.Show(this, "Số lượng cần chuyển của sản phẩm " + strProductName + "\n tại kho " + strStoreName + " lớn hơn số lượng tồn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            lstProductIDNotPass.Add(strProductID);
                            {
                                lstProductIDNotPass.Add(strProductID);
                                continue;
                            }
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Kiểm tra những trường dữ liệu bắt buộc có
        /// </summary>
        private bool CheckInputMustBeHave()
        {
            if (cboFromStoreID.StoreID <= 0)
            {
                string strMess = "Vui lòng chọn kho xuất";
                if (cboType.SelectedIndex == 1)
                    strMess = "Vui lòng chọn kho nhập";
                MessageBox.Show(this, strMess, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboFromStoreID.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(cboToStoreID.StoreIDList))
            {
                string strMess = "Vui lòng chọn kho nhập";
                if (cboType.SelectedIndex == 1)
                    strMess = "Vui lòng chọn kho xuất";
                MessageBox.Show(this, strMess, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboToStoreID.Focus();
                return false;
            }
            string[] strArrStoreID = cboToStoreID.StoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
            if (strArrStoreID.Contains(cboFromStoreID.StoreID.ToString().Trim()))
            {
                MessageBox.Show("Kho nhập phải khác với kho xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboToStoreID.Focus();
                return false;
            }
            // string a = cboProductList.ProductIDList;
            string[] strArrProductID = cboProductList.ProductIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);

            if (string.IsNullOrEmpty(strArrProductID[0].ToString()))
            {
                MessageBox.Show(this, "Vui lòng chọn ít nhất một sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboProductList.Focus();
                return false;
            }
            if (Convert.ToDecimal(txtQuantity.Value) <= 0)
            {
                MessageBox.Show(this, "Vui lòng nhập số lượng cần chuyển", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtQuantity.Focus();
                return false;
            }
            return true;
        }

        private void CheckInputQuantity()
        {
            Thread objThread = new Thread(new ThreadStart(Excute));
            objThread.Start();

            string[] strArrProductID = cboProductList.ProductIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
            string[] strComboToStoreID = cboToStoreID.StoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
            string strComboFromStoreID = cboFromStoreID.StoreID.ToString().Trim();
            decimal quantity = Convert.ToDecimal(txtQuantity.Text.Trim());

            DataTable tableResult = CreateImportResultTable();
            //  var lstProductAdd = tableResult.AsEnumerable().ToList();
            tableResult.Columns.Add("FromStoreID", typeof(string));
            tableResult.Columns.Add("ToStoreID", typeof(string));
            tableResult.Columns.Add("IsAllowDecimal", typeof(bool));

            if (cboType.SelectedIndex == 0)
            {
                #region Chuyển đi = 0
                //Gán List
                string fromStoreName = dtbStoreCache.Select("STOREID = '" + strComboFromStoreID + "'").FirstOrDefault() == null
                                    ? string.Empty : strComboFromStoreID + "-" + dtbStoreCache.Select("STOREID = '" + strComboFromStoreID + "'")
                                                                                .FirstOrDefault()["STORENAME"].ToString().Trim();
                foreach (var product in strArrProductID)
                {
                    //Lấy tên cần thiết
                    string productName = dtbProductCache.Select("PRODUCTID = '" + product.Trim() + "'").FirstOrDefault() == null
                          ? string.Empty : dtbProductCache.Select("PRODUCTID = '" + product.Trim() + "'").FirstOrDefault()["PRODUCTNAME"].ToString().Trim();
                    bool bolIsAllowDecimal = dtbProductCache.Select("PRODUCTID = '" + product.Trim() + "'").FirstOrDefault() == null
                         ? false : Convert.ToBoolean(dtbProductCache.Select("PRODUCTID = '" + product.Trim() + "'").FirstOrDefault()["ISALLOWDECIMAL"]);

                    //Số lượng 
                    decimal decFromInstockQuantity = objPLCProductInStock.GetQuantity(product.Trim(), Convert.ToInt32(strComboFromStoreID), 1, intIsShowProduct, intIsCheckRealInput);
                    foreach (var toStore in strComboToStoreID)
                    {
                        string toStoreName = dtbStoreCache.Select("STOREID = '" + toStore.Trim() + "'").FirstOrDefault() == null
                                    ? string.Empty : toStore.Trim() + "-" + dtbStoreCache.Select("STOREID = '" + toStore.Trim() + "'")
                                                                                    .FirstOrDefault()["STORENAME"].ToString().Trim();
                        decimal decToInStockQuantity = objPLCProductInStock.GetQuantity(product.Trim(), Convert.ToInt32(toStore.Trim()), 1, intIsShowProduct, intIsCheckRealInput);
                        tableResult.Rows.Add(
                            product.Trim(),
                            productName,
                            fromStoreName,
                            decFromInstockQuantity,
                            toStoreName,
                            decToInStockQuantity,
                            quantity,
                            string.Empty,
                            0,
                            false,
                            string.Empty,
                            string.Empty,
                            strComboFromStoreID,
                            toStore.Trim(),
                            bolIsAllowDecimal
                            );
                    }
                }
                #endregion
            }
            else
            {
                #region Nhập về  = 1
                //Gán List
                string toStoreName = dtbStoreCache.Select("STOREID = '" + strComboFromStoreID + "'").FirstOrDefault() == null
                                     ? string.Empty : strComboFromStoreID + "-" + dtbStoreCache.Select("STOREID = '" + strComboFromStoreID + "'")
                                                                                 .FirstOrDefault()["STORENAME"].ToString().Trim();

                foreach (var product in strArrProductID)
                {
                    //Lấy tên cần thiết
                    string productName = dtbProductCache.Select("PRODUCTID = '" + product.Trim() + "'").FirstOrDefault() == null
                          ? string.Empty : dtbProductCache.Select("PRODUCTID = '" + product.Trim() + "'").FirstOrDefault()["PRODUCTNAME"].ToString().Trim();

                    bool bolIsAllowDecimal = dtbProductCache.Select("PRODUCTID = '" + product.Trim() + "'").FirstOrDefault() == null
                        ? false : Convert.ToBoolean(dtbProductCache.Select("PRODUCTID = '" + product.Trim() + "'").FirstOrDefault()["ISALLOWDECIMAL"]);
                    //Số lượng 
                    decimal decToInstockQuantity = objPLCProductInStock.GetQuantity(product.Trim(), Convert.ToInt32(strComboFromStoreID), 1, intIsShowProduct, intIsCheckRealInput);
                    foreach (var fromStore in strComboToStoreID)
                    {
                        string fromStoreName = dtbStoreCache.Select("STOREID = '" + fromStore.Trim() + "'").FirstOrDefault() == null
                                    ? string.Empty : fromStore.Trim() + "-" + dtbStoreCache.Select("STOREID = '" + fromStore.Trim() + "'")
                                                                                    .FirstOrDefault()["STORENAME"].ToString().Trim();
                        decimal decFromInStockQuantity = objPLCProductInStock.GetQuantity(product.Trim(), Convert.ToInt32(fromStore.Trim()), 1, intIsShowProduct, intIsCheckRealInput);
                        tableResult.Rows.Add(
                            product.Trim(),
                            productName,
                            fromStoreName,
                            decFromInStockQuantity,
                            toStoreName,
                            decToInstockQuantity,
                            quantity,
                            string.Empty,
                            0,
                            false,
                            string.Empty,
                            string.Empty,
                            fromStore.Trim(),
                            strComboFromStoreID,
                            bolIsAllowDecimal
                         );
                    }
                }

                #endregion
            }

            #region CheckData
            var listCheck = tableResult.AsEnumerable().ToList();
            decimal afterQuantity = 0;
            foreach (var item in listCheck)
            {
                afterQuantity = 0;
                //Kiểm tra kho
                int intFromStoreID = Convert.ToInt32(item["FromStoreID"].ToString().Trim());
                int intToStoreID = Convert.ToInt32(item["ToStoreID"].ToString().Trim());
                string strProductID = item["PRODUCTID"].ToString().Trim();
                decimal decFromInStockQuantity = Convert.ToDecimal(item["FromInStockQuantity"]);
                decimal decToInStockQuantity = Convert.ToDecimal(item["ToInStockQuantity"]);
                decimal decQuantity = Convert.ToDecimal(item["Quantity"]);
                bool bolIsAllowDecimal = Convert.ToBoolean(item["IsAllowDecimal"]);

                ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                string strMainGroupList = cboMainGroupID.MainGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");
                string[] mainGroupList = strMainGroupList.Split(',');
                if (objProduct == null)
                {
                    item["Status"] = "Lỗi";
                    item["ErrorContent"] = "Lỗi không tìm được sản phẩm";
                    item["StatusID"] = 1;
                    continue;
                }
                if (!ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(objProduct.MainGroupID))
                {
                    item["Status"] = "Lỗi";
                    item["ErrorContent"] = "Bạn không có quyền thao tác trên ngành hàng " + objProduct.MainGroupName.Trim();
                    item["StatusID"] = 1;
                    continue;
                }
                if (!string.IsNullOrEmpty(strMainGroupList.Trim()) && !mainGroupList.Any(x => x.Trim().Equals(objProduct.MainGroupID.ToString().Trim())))
                {
                    item["Status"] = "Lỗi";
                    item["ErrorContent"] = "Sản phẩm không thuộc ngành hàng " + objProduct.MainGroupName.Trim();
                    item["StatusID"] = 1;
                    continue;
                }
                if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intFromStoreID, objProduct.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                {
                    item["Status"] = "Lỗi";
                    item["ErrorContent"] = "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + objProduct.MainGroupName.Trim();
                    item["StatusID"] = 1;
                    continue;
                }
                if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intToStoreID, objProduct.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                {
                    item["Status"] = "Lỗi";
                    item["ErrorContent"] = "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + objProduct.MainGroupName.Trim();
                    item["StatusID"] = 1;
                    continue;

                }
                //Kiểm tra có trên lưới
                if (CheckProductExist(intFromStoreID, intToStoreID, strProductID))
                {
                    item["Status"] = "Lỗi";
                    item["ErrorContent"] = "Sản phẩm đã tồn tại trên lưới";
                    item["StatusID"] = 1;
                    continue;
                }
                //Cập nhật làm tròn số lượng
                if (!bolIsAllowDecimal)
                {
                    item["FromInStockQuantity"] = Math.Round(Convert.ToDecimal(item["FromInStockQuantity"]));
                    item["ToInStockQuantity"] = Math.Round(Convert.ToDecimal(item["ToInStockQuantity"]));
                    decQuantity = Math.Round(decQuantity);
                }
                decimal decFromInStockQuantity2 = Convert.ToDecimal(item["FromInStockQuantity"]);
                decimal decQuantity2 = Convert.ToDecimal(item["Quantity"]);
                afterQuantity = decQuantity2;
                if (decFromInStockQuantity2 < decQuantity2)
                {
                    if (decFromInStockQuantity2 == 0)
                    {
                        item["Status"] = "Lỗi";
                        item["ErrorContent"] = "Sản phẩm không tồn trong kho";
                        item["StatusID"] = 1;
                        continue;
                    }
                    else
                    {
                        item["Status"] = "Cảnh báo";
                        item["ErrorContent"] = "Số lượng chuyển lớn số lượng tồn kho";
                        item["StatusID"] = 2;
                        afterQuantity = decFromInStockQuantity2;
                    }
                }
            }

            //Thêm sản phẩm vào danh sách thông qua
            var listPass = listCheck.Where(p => p["StatusID"].ToString().Trim() != "1").ToList();
            //  DataTable dtb = listPass.CopyToDataTable();
            if (listPass != null && listPass.Count() > 0)
            {
                foreach (var item in listPass)
                {
                    decimal decFromInstockQuantity = Convert.ToDecimal(item["FromInStockQuantity"]);
                    decimal decToInstockQuantity = Convert.ToDecimal(item["ToInStockQuantity"]);
                    decimal decQuantity = Convert.ToDecimal(item["Quantity"]);
                    bool IsAllowDecimal = Convert.ToBoolean(item["IsAllowDecimal"]);

                    if (item["StatusID"].ToString().Trim() == "2")
                        decQuantity = decFromInstockQuantity;

                    AddProduct(item["ProductID"].ToString().Trim(), item["ProductName"].ToString().Trim(), Convert.ToInt32(item["FromStoreID"].ToString().Trim()), item["FromStoreName"].ToString().Trim(),
                              Convert.ToInt32(item["ToStoreID"].ToString().Trim()), item["ToStoreName"].ToString().Trim(), decFromInstockQuantity, decToInstockQuantity, decQuantity, IsAllowDecimal, false);
                }
            }

            frmWaitDialog.Close();
            Thread.Sleep(0);
            objThread.Abort();

            //Kiểm tra xem có phải xem trên Excel (có lỗi)
            if (listCheck.Where(p => Convert.ToInt32(p["StatusID"]) > 0).FirstOrDefault() != null)
            {
                frmShowImportResult frmShowImportResult1 = new frmShowImportResult();
                frmShowImportResult1.ImportResult = listCheck.CopyToDataTable();
                frmShowImportResult1.HaveIMEI = false;
                frmShowImportResult1.Text = "Kết quả thêm sản phẩm";
                frmShowImportResult1.ShowDialog();
            }
            #endregion
        }

        private void ImportExcelIMEIBoost3()
        {
            #region Check+Parse

            lstFromExcelIMEI.Clear();
            bolIsCallFromImportIMEI = true;
            DataTable tblProductIMEI = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable();
            if (tblProductIMEI == null)
            {
                MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (tblProductIMEI.Rows.Count > 0 && tblProductIMEI.Rows[1] != null)
            {
                if (!CheckExcelIMEI(tblProductIMEI))
                {
                    MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            else
                return;
            //Kiểm tra định dạng


            Thread objThread = new Thread(new ThreadStart(Excute));
            objThread.Start();

            //Thay thế mã cũ 

            if (chkIsCanUseOldCode.Checked)
            {
                if (dtbProductOld != null)
                {
                    for (int i = 1; i <= tblProductIMEI.Columns.Count - 1; i++)
                    {
                        DataRow[] rowsel = dtbProductOld.Select("PRODUCTIDOLD='" + tblProductIMEI.Rows[2][i].ToString().Trim() + "'");
                        if (rowsel.Length > 0)
                            tblProductIMEI.Rows[2][i] = rowsel[0]["PRODUCTID"].ToString().Trim();
                    }
                }
                if (dtbStoreOldId != null)
                {
                    for (int i = 1; i <= tblProductIMEI.Columns.Count - 1; i++)
                    {
                        DataRow[] rowsel = dtbStoreOldId.Select("OLDSTOREID='" + tblProductIMEI.Rows[0][i].ToString().Trim() + "'");
                        if (rowsel.Length > 0)
                            tblProductIMEI.Rows[0][i] = rowsel[0]["STOREID"].ToString().Trim();
                    }
                }
            }

            tblProductIMEI.Rows.RemoveAt(1);
            tblProductIMEI.Rows.RemoveAt(2);
            tblProductIMEI.Columns.RemoveAt(0);

            //Gen List 
            #region GenList
            for (int i = 0; i < tblProductIMEI.Columns.Count; i++)
            {
                clsIMEI item = new clsIMEI();
                string strStoreID = tblProductIMEI.Rows[0][i].ToString().Trim();
                string strProductID = tblProductIMEI.Rows[1][i].ToString().Trim();
                string strFirstRow = tblProductIMEI.Rows[2][i].ToString().Trim();
                if (string.IsNullOrEmpty(strStoreID) || string.IsNullOrEmpty(strProductID) || string.IsNullOrEmpty(strFirstRow))
                    continue;
                item.StoreID = strStoreID;
                item.ProductID = strProductID;
                for (int j = 2; j < tblProductIMEI.Rows.Count; j++)
                {
                    string checkIMEI = tblProductIMEI.Rows[j][i].ToString().Trim();
                    if (!string.IsNullOrEmpty(checkIMEI))
                    {
                        if (item.lstIMEI.Count() > 0)
                        {
                            var temp = item.lstIMEI.FirstOrDefault(p => p.ToString() == checkIMEI);
                            if (temp == null)
                                item.lstIMEI.Add(checkIMEI);
                        }
                        else item.lstIMEI.Add(checkIMEI);
                    }
                }
                lstFromExcelIMEI.Add(item);
            }
            #endregion

            GetStoreInStockWithIMEI(lstFromExcelIMEI, cboFromStoreID.StoreID);



            dtbQuantity = new DataTable();
            DataTable dtbIMEI = new DataTable();
            dtbIMEI.Columns.Add("IMEI", typeof(String));
            dtbIMEI.Clear();
            DataTable dtbCheck = new DataTable();
            DataTable tblResultIMEI = CreateImportResultTable();
            tblResultIMEI.Columns.Add("FromStoreID", typeof(string));
            tblResultIMEI.Columns.Add("ToStoreID", typeof(string));
            tblResultIMEI.Columns.Add("MainGroupID", typeof(string));
            tblResultIMEI.Columns.Add("MainGroupName", typeof(string)).DefaultValue = string.Empty;
            tblResultIMEI.Columns.Add("IsAllowDecimal", typeof(bool)).DefaultValue = false;

            //Gồm hàng đi đường

            //Hàng trưng bày

            #endregion

            #region Kt IMEI tồn
            string strMainGroupList = cboMainGroupID.MainGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");
            string[] mainGroupList = strMainGroupList.Split(',');

            List<string> listStoreCheck = new List<string>();
            List<string> listProductCheck = new List<string>();

            string strComboFromStoreID = cboFromStoreID.StoreID.ToString().Trim();
            listStoreCheck.Add(strComboFromStoreID);
            //Gen List theo tblResultIMEI
            if (cboType.SelectedIndex == 0)
            {
                #region Chuyển đi
                string fromStoreID = strComboFromStoreID;
                string fromStoreName = string.Empty;
                if (IsNumber(fromStoreID))
                {
                    var objFromStore = dtbStoreCache.Select("STOREID =" + fromStoreID).FirstOrDefault();
                    if (objFromStore != null)
                        fromStoreName = fromStoreID + "-" + objFromStore["STORENAME"].ToString().Trim();
                }

                foreach (var item in lstFromExcelIMEI)
                {
                    string strProductID = item.ProductID.Trim();
                    string productName = string.Empty;
                    bool bolIsRequestIMEI = false;
                    decimal decFromInStockQuantity = 0;
                    decimal decToInStockQuantity = 0;
                    decimal decQuantity = 0;
                    string strMainGroupID = string.Empty;
                    string strMainGroupName = string.Empty;
                    bool bolIsAllowDecimal = false;

                    var objProduct = dtbProductCache.AsEnumerable().Where(p => p["PRODUCTID"].ToString().Trim() == strProductID)
                                                    .FirstOrDefault();
                    if (objProduct != null)
                    {
                        productName = objProduct["PRODUCTNAME"].ToString().Trim();
                        bolIsRequestIMEI = Convert.ToBoolean(objProduct["ISREQUESTIMEI"]);
                        strMainGroupID = objProduct["MAINGROUPID"].ToString().Trim();
                        strMainGroupName = objProduct["MAINGROUPNAME"].ToString().Trim();
                        bolIsAllowDecimal = Convert.ToBoolean(objProduct["ISALLOWDECIMAL"]);
                    }

                    string toStoreID = item.StoreID.Trim();
                    string toStoreName = string.Empty;
                    if (IsNumber(toStoreID))
                    {
                        var objToStoreName = dtbStoreCache.Select("STOREID =" + toStoreID).FirstOrDefault();
                        if (objToStoreName != null)
                            toStoreName = toStoreID + "-" + objToStoreName["STORENAME"].ToString().Trim();
                    }
                    if (!listStoreCheck.Contains(toStoreID))
                        listStoreCheck.Add(toStoreID);
                    if (!listProductCheck.Contains(strProductID))
                        listProductCheck.Add(strProductID);

                    if (bolIsRequestIMEI)
                    {
                        foreach (var strIMEI in item.lstIMEI)
                        {
                            if (!string.IsNullOrEmpty(fromStoreName))
                                decFromInStockQuantity = GetQuantity(strProductID, Convert.ToInt32(fromStoreID));
                            if (!string.IsNullOrEmpty(toStoreName))
                                decToInStockQuantity = GetQuantity(strProductID, Convert.ToInt32(toStoreID));
                            tblResultIMEI.Rows.Add(strProductID, productName, fromStoreName,
                                    decFromInStockQuantity, toStoreName, decToInStockQuantity, 1, "Thành công", 0, false, string.Empty,
                                    strIMEI.Trim(), fromStoreID, toStoreID, strMainGroupID, strMainGroupName, bolIsAllowDecimal);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(fromStoreName))
                            decFromInStockQuantity = GetQuantity(strProductID, Convert.ToInt32(fromStoreID));
                        if (!string.IsNullOrEmpty(toStoreName))
                            decToInStockQuantity = GetQuantity(strProductID, Convert.ToInt32(toStoreID));
                        if (IsNumber(item.lstIMEI[0].Trim()) && item.lstIMEI[0].Trim().Length < 12)
                            decQuantity = Convert.ToDecimal(item.lstIMEI[0].Trim());
                        tblResultIMEI.Rows.Add(strProductID, productName, fromStoreName,
                                    decFromInStockQuantity, toStoreName, decToInStockQuantity, decQuantity,
                                    "Thành công", 0, false, string.Empty, string.Empty,
                                    fromStoreID, toStoreID, strMainGroupID, strMainGroupName, bolIsAllowDecimal);
                    }
                }
                #endregion
            }
            else
            {
                #region Nhận về
                string toStoreID = strComboFromStoreID.Trim();
                string toStoreName = string.Empty;
                if (IsNumber(toStoreID))
                {
                    var objToStore = dtbStoreCache.Select("STOREID =" + toStoreID).FirstOrDefault();
                    if (objToStore != null)
                        toStoreName = toStoreID + "-" + objToStore["STORENAME"].ToString().Trim();
                }
                foreach (var item in lstFromExcelIMEI)
                {
                    string strProductID = item.ProductID.Trim();
                    string productName = string.Empty;
                    bool bolIsRequestIMEI = false;
                    decimal decFromInStockQuantity = 0;
                    decimal decToInStockQuantity = 0;
                    decimal decQuantity = 0;
                    bool bolIsAllowDecimal = false;

                    string strMainGroupID = string.Empty;
                    string strMainGroupName = string.Empty;

                    var objProduct = dtbProductCache.AsEnumerable().Where(p => p["PRODUCTID"].ToString().Trim() == strProductID)
                                                    .FirstOrDefault();
                    if (objProduct != null)
                    {
                        productName = objProduct["PRODUCTNAME"].ToString().Trim();
                        bolIsRequestIMEI = Convert.ToBoolean(objProduct["ISREQUESTIMEI"]);
                        strMainGroupID = objProduct["MAINGROUPID"].ToString().Trim();
                        strMainGroupName = objProduct["MAINGROUPNAME"].ToString().Trim();
                        bolIsAllowDecimal = Convert.ToBoolean(objProduct["IsAllowDecimal"]);
                    }

                    string fromStoreID = item.StoreID.Trim();
                    string fromStoreName = string.Empty;
                    if (IsNumber(fromStoreID))
                    {
                        var objFromStore = dtbStoreCache.Select("STOREID =" + fromStoreID).FirstOrDefault();
                        if (objFromStore != null)
                            fromStoreName = fromStoreID + "-" + objFromStore["STORENAME"].ToString().Trim();
                    }
                    if (!listStoreCheck.Contains(fromStoreID))
                        listStoreCheck.Add(fromStoreID);
                    if (!listProductCheck.Contains(strProductID))
                        listProductCheck.Add(strProductID);

                    if (bolIsRequestIMEI)
                    {
                        foreach (var strIMEI in item.lstIMEI)
                        {
                            if (!string.IsNullOrEmpty(fromStoreName))
                                decFromInStockQuantity = GetQuantity(strProductID, Convert.ToInt32(fromStoreID));
                            if (!string.IsNullOrEmpty(toStoreName))
                                decToInStockQuantity = GetQuantity(strProductID, Convert.ToInt32(toStoreID));
                            tblResultIMEI.Rows.Add(strProductID, productName, fromStoreName,
                                    decFromInStockQuantity, toStoreName, decToInStockQuantity, 1, "Thành công", 0, false, string.Empty,
                                    strIMEI.Trim(), fromStoreID, toStoreID, strMainGroupID, strMainGroupName, bolIsAllowDecimal);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(fromStoreName))
                            decFromInStockQuantity = GetQuantity(strProductID, Convert.ToInt32(fromStoreID));
                        if (!string.IsNullOrEmpty(toStoreName))
                            decToInStockQuantity = GetQuantity(strProductID, Convert.ToInt32(toStoreID));
                        if (IsNumber(item.lstIMEI[0].Trim()) && item.lstIMEI[0].Trim().Length < 12)
                            decQuantity = Convert.ToDecimal(item.lstIMEI[0].Trim());
                        tblResultIMEI.Rows.Add(strProductID, productName, fromStoreName,
                                    decFromInStockQuantity, toStoreName, decToInStockQuantity, decQuantity,
                                    "Thành công", 0, false, string.Empty, string.Empty,
                                    fromStoreID, toStoreID, strMainGroupID, strMainGroupName, bolIsAllowDecimal);
                    }
                }
                #endregion
            }


            //--------------------Kiểm tra dữ liệu đúng (Kho, mã sản phẩm)

            var listCheckFirst = tblResultIMEI.AsEnumerable().ToList();

            var listError1 = listCheckFirst.Where(p => string.IsNullOrEmpty(p["FromStoreName"].ToString().Trim())
                                                        && p["StatusID"].ToString() == "0").ToList();
            if (listError1.Any())
                listError1.All(c =>
                {
                    c["StatusID"] = 1; c["Status"] = "Lỗi";
                    c["ErrorContent"] = "Không tìm thấy dữ liệu của kho xuất";
                    c["FromInStockQuantity"] = 0; return true;
                });

            var listError2 = listCheckFirst.Where(p => string.IsNullOrEmpty(p["ToStoreName"].ToString().Trim())
                                                        && p["StatusID"].ToString() == "0").ToList();
            if (listError2.Any())
                listError2.All(c =>
                {
                    c["StatusID"] = 1; c["Status"] = "Lỗi";
                    c["FromInStockQuantity"] = 0; c["ErrorContent"] = "Không tìm thấy dữ liệu của kho nhập"; return true;
                });

            var listError3 = listCheckFirst.Where(p => string.IsNullOrEmpty(p["ProductName"].ToString().Trim())
                                                        && p["StatusID"].ToString() == "0").ToList();
            if (listError3.Any())
                listError3.All(c =>
                {
                    c["StatusID"] = 1; c["FromInStockQuantity"] = 0;
                    c["Status"] = "Lỗi"; c["ErrorContent"] = "Không tìm thấy dữ liệu của sản phẩm"; return true;
                });

            //------------------------------------------------------------


            // --------------------Kiểm tra ngành hàng

            if (!string.IsNullOrEmpty(strMainGroupList.Trim())) //Nếu không phải chọn tất cả ngành hàng thì xét
            {
                List<string> listMainGroupCheck = new List<string>(mainGroupList);
                var listMainGroupInList = listCheckFirst.Where(p => p["StatusID"].ToString() == "0")
                    .Select(p => p["MainGroupID"].ToString().Trim()).Distinct().ToList();

                //Lấy list thuộc danh sách
                var listOnMain = from o in listMainGroupInList
                                 join i in listMainGroupCheck
                                 on o.ToString() equals i.ToString().Trim() into newList
                                 from z in newList.DefaultIfEmpty()
                                 select new { mainGroupID = o.ToString(), key = (z == null ? 0 : 1) };

                var listError4 = from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0")
                                 join i in listOnMain
                                 on o["MainGroupID"].ToString() equals i.mainGroupID.ToString()
                                 where i.key == 0
                                 select o;

                if (listError4.Any())
                    listError4.All(c =>
                    {
                        c["StatusID"] = 1; c["Status"] = "Lỗi";
                        c["ErrorContent"] = "Sản phẩm không thuộc ngành hàng đang xét"; return true;
                    });
            }


            //Xuất kho trên ngành hàng
            var listMainGroupCheck2 = (from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0").ToList()
                                       select new
                                       {
                                           MainGroupID = o["MainGroupID"].ToString(),
                                           FromStoreID = o["FromStoreID"].ToString()
                                       }).Distinct();

            List<clsMainGroup> listNotPermissionMainGroupOut = new List<clsMainGroup>();
            foreach (var item in listMainGroupCheck2)
            {
                if (!CheckMainGroupPermission(Convert.ToInt32(item.FromStoreID),
                    Convert.ToInt32(item.MainGroupID), EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                {
                    clsMainGroup cls = new clsMainGroup();
                    cls.MainGroupID = item.MainGroupID;
                    cls.StoreID = item.FromStoreID;
                    listNotPermissionMainGroupOut.Add(cls);
                }
            }

            var listError5 = from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0").ToList()
                             from i in listNotPermissionMainGroupOut
                             where (o["MainGroupID"].ToString() == i.MainGroupID && o["FromStoreID"].ToString() == i.StoreID)
                             select o;
            if (listError5.Any())
                listError5.All(c =>
                {
                    c["StatusID"] = 1; c["Status"] = "Lỗi";
                    c["ErrorContent"] = "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + c["MainGroupName"].ToString(); return true;
                });

            //Chuyển kho trên ngành ngàng
            var listMainGroupCheck3 = (from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0")
                                       select new
                                       {
                                           MainGroupID = o["MainGroupID"].ToString(),
                                           ToStoreID = o["ToStoreID"].ToString()
                                       }).Distinct();

            List<clsMainGroup> listNotPermissionMainGroupIn = new List<clsMainGroup>();
            foreach (var item in listMainGroupCheck3)
            {
                if (!CheckMainGroupPermission(Convert.ToInt32(item.ToStoreID),
                    Convert.ToInt32(item.MainGroupID), EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                {
                    clsMainGroup cls = new clsMainGroup();
                    cls.MainGroupID = item.MainGroupID;
                    cls.StoreID = item.ToStoreID;
                    listNotPermissionMainGroupIn.Add(cls);
                }
            }

            var listError6 = from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0").ToList()
                             from i in listNotPermissionMainGroupIn
                             where (o["MainGroupID"].ToString() == i.MainGroupID && o["ToStoreID"].ToString() == i.StoreID)
                             select o;
            if (listError6.Any())
                listError6.All(c =>
                {
                    c["StatusID"] = 1; c["Status"] = "Lỗi";
                    c["ErrorContent"] = "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + c["MainGroupName"].ToString(); return true;
                });

            //Quyền thao tác trên ngành hàng

            var listMainGroupCheck4 = (from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0")
                                       select new
                                       {
                                           MainGroupID = o["MainGroupID"].ToString()
                                       }).Distinct();
            List<string> listNotPermission = new List<string>();
            foreach (var item in listMainGroupCheck4)
            {
                if (!ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(Convert.ToInt32(item.MainGroupID)))
                    listNotPermission.Add(item.MainGroupID);
            }

            var listError7 = from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0").ToList()
                             join i in listNotPermission
                             on o["MainGroupID"].ToString() equals i.ToString()
                             select o;
            if (listError7.Any())
                listError7.All(c =>
                {
                    c["StatusID"] = 1; c["Status"] = "Lỗi";
                    c["ErrorContent"] = "Bạn không có quyền thao tác trên ngành hàng " + c["MainGroupName"].ToString(); return true;
                });


            var listError8 = from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0"
                             && (p["FromStoreID"].ToString() == p["ToStoreID"].ToString()))
                             select o;
            if (listError8.Any())
                listError8.All(c =>
                {
                    c["StatusID"] = 1; c["Status"] = "Lỗi";
                    c["ErrorContent"] = "Kho nhập trùng với kho xuất"; return true;
                });

            //--------------------Kiểm tra số lượng

            var listError9 = from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0"
                             && string.IsNullOrEmpty(p["IMEI"].ToString())
                             && p["FromInStockQuantity"].ToString() == "0")
                             select o;
            if (listError9.Any())
                listError9.All(c =>
                {
                    c["StatusID"] = 1; c["Status"] = "Lỗi";
                    c["ErrorContent"] = "Sản phẩm không có tồn trong kho"; return true;
                });

            var listError10 = from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0"
                             && string.IsNullOrEmpty(p["IMEI"].ToString())
                             && Convert.ToDecimal(p["FromInStockQuantity"].ToString()) < Convert.ToDecimal(p["Quantity"].ToString()))
                              select o;
            if (listError10.Any())
                listError10.All(c =>
                {
                    c["StatusID"] = 2; c["Status"] = "Cảnh báo";
                    c["ErrorContent"] = "Số lượng chuyển lớn hơn số lượng tồn"; return true;
                });

            //--------------------Kiểm tra IMEI 

            var listIMEI = listCheckFirst.Where(p => p["StatusID"].ToString() == "0"
                            && !string.IsNullOrEmpty(p["IMEI"].ToString()));
            if (listIMEI.Any())
            {
                foreach (var imei in listIMEI)
                    dtbIMEI.Rows.Add(imei["IMEI"].ToString());

                if (listIMEI.Any())
                    listIMEI.All(c =>
                    { c["StatusID"] = 1; c["Status"] = "Lỗi"; c["ErrorContent"] = "IMEI không tồn trong kho"; return true; });
                //Gen bảng IMEI có tồn/tồn tại
                string xmlIMEI = DUIInventoryCommon.CreateXMLFromDataTable(dtbIMEI, "IMEI");
                DataTable dtbTotalImei = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("CHECKIMEI_INSTOCK_1", new object[] { "@IMEI", xmlIMEI });
                //------------------------------------------------------------

                bool IsCheckRealInputIMEI = chkIsCheckRealInput.Checked;
                int IsShowProductIMEI = cboIsShowProduct.SelectedIndex;

                #region New

                //Xét IMEI tồn
                if (dtbTotalImei != null && dtbTotalImei.Rows.Count > 0)
                {
                    var listIMEICheck = from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "1"
                                                     && !string.IsNullOrEmpty(p["IMEI"].ToString()))
                                        join i in dtbTotalImei.AsEnumerable()
                                        on o["IMEI"].ToString() equals i["IMEI"].ToString().Trim()
                                        select new { lstWithIMEI = o, listDataIMEI = i };
                    if (listIMEICheck.Any())
                    {
                        var listSuccess = from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "1"
                                                     && !string.IsNullOrEmpty(p["IMEI"].ToString()))
                                          join i in listIMEICheck
                                          on o["IMEI"].ToString() equals i.lstWithIMEI["IMEI"].ToString()
                                          select o;
                        if (listSuccess.Any())
                            listSuccess.All(c =>
                            {
                                c["StatusID"] = 0; c["Status"] = "Thành công";
                                c["ErrorContent"] = ""; return true;
                            });
                    }

                    var listIMEIProduct = from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0"
                                                        && !string.IsNullOrEmpty(p["IMEI"].ToString()))
                                          join i in dtbTotalImei.AsEnumerable()
                                          on o["IMEI"].ToString() equals i["IMEI"].ToString().Trim()
                                          select new { lstWithIMEI = o, listDataIMEI = i };

                    if (listIMEIProduct.Any())
                    {
                        var listError = from o in listIMEIProduct
                                        where o.lstWithIMEI["ProductID"].ToString()
                                            != o.listDataIMEI["PRODUCTID"].ToString().Trim()
                                        select o;

                        if (listError.Any())
                            listError.All(c =>
                            {
                                c.lstWithIMEI["StatusID"] = 1; c.lstWithIMEI["Status"] = "Lỗi";
                                c.lstWithIMEI["ErrorContent"] = "IMEI không thuộc sản phẩm"; return true;
                            });

                    }

                    var listIMEIGenInStore = from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0"
                                                        && !string.IsNullOrEmpty(p["IMEI"].ToString()))
                                             join i in dtbTotalImei.AsEnumerable()
                                             on o["IMEI"].ToString() equals i["IMEI"].ToString().Trim()
                                             select new { lstWithIMEI = o, listDataIMEI = i };

                    if (listIMEIGenInStore.Any())
                    {
                        var listError = from o in listIMEIGenInStore
                                        where o.lstWithIMEI["FromStoreID"].ToString()
                                        != o.listDataIMEI["STOREID"].ToString().Trim()
                                        select o;

                        if (listError.Any())
                            listError.All(c =>
                            {
                                c.lstWithIMEI["StatusID"] = 1; c.lstWithIMEI["Status"] = "Lỗi";
                                c.lstWithIMEI["ErrorContent"] = "IMEI không tồn trong kho"; return true;
                            });
                    }

                    var listIMEIGenIsOrder = from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0"
                                                        && !string.IsNullOrEmpty(p["IMEI"].ToString()))
                                             join i in dtbTotalImei.AsEnumerable()
                                             on o["IMEI"].ToString() equals i["IMEI"].ToString().Trim()
                                             select new { lstWithIMEI = o, listDataIMEI = i };

                    if (listIMEIGenIsOrder.Any())
                    {
                        var listError = from o in listIMEIGenIsOrder
                                        where o.listDataIMEI["ISORDER"].ToString().Trim() == "1"
                                        select o;

                        if (listError.Any())
                            listError.All(c =>
                            {
                                c.lstWithIMEI["StatusID"] = 1; c.lstWithIMEI["Status"] = "Lỗi";
                                c.lstWithIMEI["ErrorContent"] = "IMEI đã được sử dụng"; return true;
                            });
                    }

                    if (intIsShowProduct >= 0)
                    {
                        var listIMEIGenIsShowProduct = from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0"
                                                        && !string.IsNullOrEmpty(p["IMEI"].ToString()))
                                                       join i in dtbTotalImei.AsEnumerable()
                                                       on o["IMEI"].ToString() equals i["IMEI"].ToString().Trim()
                                                       select new { lstWithIMEI = o, listDataIMEI = i };

                        if (listIMEIGenIsShowProduct.Any())
                        {
                            string strError = string.Empty;
                            if (intIsShowProduct == 0)
                                strError = "IMEI là hàng trưng bày";
                            else
                                strError = "IMEI không phải là hàng trưng bày";

                            var listError = from o in listIMEIGenIsShowProduct
                                            where o.listDataIMEI["ISSHOWPRODUCT"].ToString().Trim() != intIsShowProduct.ToString()
                                            select o;

                            if (listError.Any())
                                listError.All(c =>
                                {
                                    c.lstWithIMEI["StatusID"] = 1; c.lstWithIMEI["Status"] = "Lỗi";
                                    c.lstWithIMEI["ErrorContent"] = strError; return true;
                                });
                        }
                    }

                    if (!IsCheckRealInputIMEI) //Nếu không check thì kiểm tra
                    {
                        var listIMEIGenIsRealInput = from o in listCheckFirst.Where(p => p["StatusID"].ToString() == "0"
                                                        && !string.IsNullOrEmpty(p["IMEI"].ToString()))
                                                     join i in dtbTotalImei.AsEnumerable()
                                                     on o["IMEI"].ToString() equals i["IMEI"].ToString().Trim()
                                                     select new { lstWithIMEI = o, listDataIMEI = i };

                        if (listIMEIGenIsRealInput.Any())
                        {
                            var listError = from o in listIMEIGenIsRealInput
                                            where o.listDataIMEI["ISCHECKREALINPUT"].ToString().Trim() == "0"
                                            select o;

                            if (listError.Any())
                                listError.All(c =>
                                {
                                    c.lstWithIMEI["StatusID"] = 1; c.lstWithIMEI["Status"] = "Lỗi";
                                    c.lstWithIMEI["ErrorContent"] = "IMEI là hàng đi đường"; return true;
                                });
                        }
                    }
                }
            }
                #endregion

            //------------------------------------------------------------

            var listAdd = listCheckFirst.Where(p => p["StatusID"].ToString().Trim() != "1").ToList();
            foreach (var item in listAdd)
            {
                //Số lượng bị cảnh cáo
                if (string.IsNullOrEmpty(item["IMEI"].ToString()))
                {
                    if (item["StatusID"].ToString() == "2")
                    {
                        AddProduct(item["ProductID"].ToString(), item["ProductName"].ToString(), Convert.ToInt32(item["FromStoreID"].ToString()),
                            item["FromStoreName"].ToString(), Convert.ToInt32(item["ToStoreID"].ToString()), item["ToStoreName"].ToString(),
                            Convert.ToDecimal(item["FromInStockQuantity"]), Convert.ToDecimal(item["ToInStockQuantity"]), Convert.ToDecimal(item["FromInStockQuantity"])
                            , Convert.ToBoolean(item["IsAllowDecimal"]), false);
                    }
                    else
                    {
                        AddProduct(item["ProductID"].ToString(), item["ProductName"].ToString(), Convert.ToInt32(item["FromStoreID"].ToString()),
                            item["FromStoreName"].ToString(), Convert.ToInt32(item["ToStoreID"].ToString()), item["ToStoreName"].ToString(),
                            Convert.ToDecimal(item["FromInStockQuantity"]), Convert.ToDecimal(item["ToInStockQuantity"]), Convert.ToDecimal(item["Quantity"])
                            , Convert.ToBoolean(item["IsAllowDecimal"]), false);
                    }
                }
                else
                    AddProductWithIMEI(item["ProductID"].ToString(), item["ProductName"].ToString(), Convert.ToInt32(item["FromStoreID"].ToString()),
                            item["FromStoreName"].ToString(), Convert.ToDecimal(item["FromInStockQuantity"]), Convert.ToInt32(item["ToStoreID"].ToString()), item["ToStoreName"].ToString(),
                             Convert.ToDecimal(item["ToInStockQuantity"]), Convert.ToDecimal(item["Quantity"])
                            , Convert.ToBoolean(item["IsAllowDecimal"]), false, item["IMEI"].ToString());
                // Thành công
            }


            frmWaitDialog.Close();
            Thread.Sleep(0);
            objThread.Abort();

            //--------------------Kiểm tra và chạy Kết quả Excel

            if (listCheckFirst != null && listCheckFirst.Count() > 0
                && listCheckFirst.Where(p => p["StatusID"].ToString().Trim() != "0").FirstOrDefault() != null)
            {
                frmShowImportResult frmShowImportResult1 = new frmShowImportResult();
                frmShowImportResult1.ImportResult = listCheckFirst.CopyToDataTable();
                frmShowImportResult1.HaveIMEI = true;
                frmShowImportResult1.ShowDialog();
            }

            //------------------------------------------------------------
            #endregion

        }

        private void ExportTemplateMultiStore()
        {
            DataTable dtbExportTemplate = new DataTable();
            dtbExportTemplate.Columns.Add("Mã ST chuyển", typeof(string));
            dtbExportTemplate.Columns.Add("Tên ST chuyển", typeof(string));
            dtbExportTemplate.Columns.Add("Mã sản phẩm", typeof(string));
            dtbExportTemplate.Columns.Add("Tên sản phẩm", typeof(string));
            dtbExportTemplate.Columns.Add("Số lượng", typeof(string));
            dtbExportTemplate.Columns.Add("Mã ST nhận", typeof(string));
            dtbExportTemplate.Columns.Add("Tên ST nhận", typeof(string));
            dtbExportTemplate.AcceptChanges();
            flexExportTemplate.DataSource = dtbExportTemplate;
            flexExportTemplate.Cols[0].Width = 100;
            flexExportTemplate.Cols[1].Width = 220;
            flexExportTemplate.Cols[2].Width = 150;
            flexExportTemplate.Cols[3].Width = 220;
            flexExportTemplate.Cols[4].Width = 100;
            flexExportTemplate.Cols[5].Width = 150;
            flexExportTemplate.Cols[6].Width = 220;
            Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexExportTemplate);
        }

        #endregion

        #region events
        private void cmdAdd_Click(object sender, EventArgs e)
        {
            try
            {
                EnableControl(false);
                //Kiểm tra trường dữ liệu bắt buộc 
                if (!CheckInputMustBeHave())
                {
                    EnableControl(true);
                    return;
                }
                CheckInputQuantity();
                BindData();
                EnableControl(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //CheckInputAdd();
            #region Old
            //if (!bolIsStop)
            //{
            //   string[] strArrProductID = cboProductList.ProductIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
            //    var listNew = from o in strArrProductID.ToList() select o;
            //    if (lstProductIDNotPass.Count() > 0)
            //    {
            //        listNew.ToList().Clear();
            //        listNew = from o in lstProductIDNotPass
            //                  from i in strArrProductID.ToList()
            //                  where o.ToString() != i.ToString()
            //                  select i;
            //    }
            //    if (cboType.SelectedIndex == 0)
            //    {
            //        int intFromStoreID = Convert.ToInt32(cboFromStoreID.StoreID);
            //        string strToStoreIDList = cboToStoreID.StoreIDList;

            //        foreach (var strProductID in listNew)
            //        {
            //            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
            //            string strMainGroupList = cboMainGroupID.MainGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");
            //            string[] mainGroupList = strMainGroupList.Split(',');
            //            if (objProduct == null)
            //                return;
            //            if (!string.IsNullOrEmpty(strMainGroupList.Trim()) && !mainGroupList.Any(x => x.Trim().Equals(objProduct.MainGroupID.ToString().Trim())))
            //            {
            //                MessageBox.Show(this, "Sản phẩm cần thêm không thuộc ngành hàng đang xét", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //                return;
            //            }
            //            if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intFromStoreID, objProduct.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
            //            {
            //                MessageBox.Show(this, "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + objProduct.MainGroupName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //                return;
            //            }

            //            decimal decQuantity = Convert.ToDecimal(txtQuantity.Value);
            //            decimal decFromInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intFromStoreID, 1, intIsShowProduct, intIsCheckRealInput);
            //            string[] strArrToStoreID = strToStoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
            //            for (int i = 0; i < strArrToStoreID.Length; i++)
            //            {
            //                int intToStoreID = Convert.ToInt32(strArrToStoreID[i]);
            //                string strToStoreName = ERP.MasterData.PLC.MD.PLCStore.GetStoreName(Convert.ToInt32(intToStoreID));
            //                if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intToStoreID, objProduct.MainGroupID,
            //                Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
            //                {
            //                    MessageBox.Show(this, "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + objProduct.MainGroupName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //                    return;
            //                }
            //                decimal decToInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);
            //                if (!objProduct.IsAllowDecimal)
            //                {
            //                    decFromInstockQuantity = Math.Round(decFromInstockQuantity);
            //                    decToInstockQuantity = Math.Round(decToInstockQuantity);
            //                    decQuantity = Math.Round(decQuantity);
            //                }

            //                AddProduct(objProduct.ProductID, objProduct.ProductName, intFromStoreID, cboFromStoreID.Text.Trim(),
            //                    intToStoreID, strToStoreName, decFromInstockQuantity, decToInstockQuantity, decQuantity, objProduct.IsAllowDecimal, false);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        int intToStoreID = Convert.ToInt32(cboFromStoreID.StoreID);
            //        string strFromStoreIDList = cboToStoreID.StoreIDList;
            //        foreach (var strProductID in listNew)
            //        {
            //            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
            //            string strMainGroupList = cboMainGroupID.MainGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");
            //            string[] mainGroupList = strMainGroupList.Split(',');
            //            if (objProduct == null)
            //                return;
            //            if (!string.IsNullOrEmpty(strMainGroupList.Trim()) && !mainGroupList.Any(x => x.Trim().Equals(objProduct.MainGroupID.ToString().Trim())))
            //            {
            //                MessageBox.Show(this, "Sản phẩm cần thêm không thuộc ngành hàng đang xét", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //                return;
            //            }
            //            if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intToStoreID, objProduct.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
            //            {
            //                MessageBox.Show(this, "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + objProduct.MainGroupName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //                return;
            //            }

            //            decimal decQuantity = Convert.ToDecimal(txtQuantity.Value);
            //            decimal decToInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);
            //            string[] strArrFromStoreID = strFromStoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
            //            for (int i = 0; i < strArrFromStoreID.Length; i++)
            //            {
            //                int intFromStoreID = Convert.ToInt32(strArrFromStoreID[i]);
            //                string strFromStoreName = ERP.MasterData.PLC.MD.PLCStore.GetStoreName(Convert.ToInt32(intFromStoreID));
            //                if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intFromStoreID, objProduct.MainGroupID,
            //                Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
            //                {
            //                    MessageBox.Show(this, "Kho " + strFromStoreName + " không được phép xuất chuyển kho trên ngành hàng " + objProduct.MainGroupName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //                    return;
            //                }
            //                decimal decFromInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);
            //                if (!objProduct.IsAllowDecimal)
            //                {
            //                    decFromInstockQuantity = Math.Round(decFromInstockQuantity);
            //                    decToInstockQuantity = Math.Round(decToInstockQuantity);
            //                    decQuantity = Math.Round(decQuantity);
            //                }

            //                AddProduct(objProduct.ProductID, objProduct.ProductName, intFromStoreID, strFromStoreName,
            //                    intToStoreID, cboFromStoreID.Text.Trim(), decFromInstockQuantity, decToInstockQuantity, decQuantity, objProduct.IsAllowDecimal, false);
            //            }
            //        }
            //    }
            //    BindData();
            // }
            #endregion
        }

        private void btnSelectProduct_Click(object sender, EventArgs e)
        {
            if (!btnSelectProduct.Enabled)
                return;
            ERP.MasterData.DUI.Search.frmProductSearch frmProductSearch1 = ERP.MasterData.DUI.MasterData_Globals.frmProductSearch;
            frmProductSearch1.IsServiceType = Library.AppCore.Constant.EnumType.IsServiceType.NOT_ISSERVICE;
            //frmProductSearch1.MainGroupID = cboMainGroupID.MainGroupID;
            frmProductSearch1.MainGroupIDList = cboMainGroupID.MainGroupIDList;
            frmProductSearch1.IsMultiSelect = false;
            frmProductSearch1.ShowDialog();
            if (frmProductSearch1.Product != null)
            {
                txtProductID.Text = frmProductSearch1.Product.ProductID.Trim();
            }
            else
                txtProductID.Text = string.Empty;

        }

        private void btnExportTemplate_Click(object sender, EventArgs e)
        {
            ExportTemplate();
        }

        private void cmdImportExcel_Click(object sender, EventArgs e)
        {
            if (grvListAutoStoreChange.RowCount > 1)
            {
                if (MessageBox.Show(this, "Trên lưới đã có dữ liệu.\nChương trình sẽ xóa dữ liệu cũ và thay thế bằng dữ liệu mới", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                {
                    return;
                }
            }
            dtbAutoStoreChange.Clear();
            if (cboFromStoreID.StoreID < 1)
            {
                string strMess = "Bạn phải chọn kho xuất";
                if (cboType.SelectedIndex == 1)
                    strMess = "Bạn phải chọn kho nhập";
                MessageBoxObject.ShowWarningMessage(this, strMess);
                cboFromStoreID.Focus();
                return;
            }
            EnableControl(false);

            Thread objThread = new Thread(new ThreadStart(Excute));
            objThread.Start();

            // ImportExcelNew();
            BindData();

            frmWaitDialog.Close();
            Thread.Sleep(0);
            objThread.Abort();

            EnableControl(true);
        }

        private void cmdCreateStoreChangeCommand_Click(object sender, EventArgs e)
        {
            Boolean bolIsSelected = false;
            for (int i = 0; i < grvListAutoStoreChange.RowCount; i++)
            {
                if (Convert.ToBoolean(grvListAutoStoreChange.GetDataRow(i)["Select"]))
                {
                    bolIsSelected = true;
                    break;
                }
            }

            if (!bolIsSelected)
            {
                MessageBox.Show(this, "Vui lòng chọn ít nhất một mặt hàng cần chuyển!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            bool bolIsReceive = cboType.SelectedIndex == 1 ? true : false;
            if (!bolIsReceive)
            {
                if (!CheckQuantity())
                    return;
            }
            if (bolIsAutoCreateOrder)
            {
                #region bolIsAutoCreateOrder
                // lstData = new List<DUIInventoryCommon.clsData>();
                frmSelectInfo frm = new frmSelectInfo();
                frm.STORECHANGECOMMANDTYPEID = intStoreChangeCommandTypeID;
                frm.ShowDialog();
                if (frm.IsAccept)
                {
                    ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = frm.StoreChangeOrderType;
                    objStoreChangeType = new ERP.MasterData.PLC.MD.PLCStoreChangeType().LoadInfo((int)objStoreChangeOrderType.StoreChangeTypeID);
                    DataTable dtbDataSource = dtbAutoStoreChange.Clone();
                    //CheckInputCreateOrder()
                    //Lấy listKhôngChọn 
                    var listExcept = dtbAutoStoreChange.Copy().AsEnumerable()
                                    .Where(p => !Convert.ToBoolean(p["SELECT"].ToString())).ToList();

                    //Kiểm tra 

                    var list = dtbAutoStoreChange.Copy().AsEnumerable()
                                .Where(p => string.IsNullOrEmpty(p["IMEI"].ToString())
                                && Convert.ToBoolean(p["SELECT"].ToString())).ToList();

                    if (list != null && list.Count() > 0)
                    {
                        foreach (var item in list)
                        {
                            if (CheckInputCreateOrder(objStoreChangeType
                                , Convert.ToInt32(item["FROMSTOREID"].ToString().Trim())
                                , Convert.ToInt32(item["TOSTOREID"].ToString().Trim())))
                                dtbDataSource.ImportRow(item);
                            else
                                listExcept.Add(item);
                        }
                    }

                    DataTable TblListIMEI = new DataTable();
                    TblListIMEI.Columns.Add("FROMSTOREID", typeof(string));
                    TblListIMEI.Columns.Add("TOSTOREID", typeof(string));
                    TblListIMEI.Columns.Add("PRODUCTID", typeof(string));
                    TblListIMEI.Columns.Add("IMEI", typeof(string));

                    if (bolIsCallFromImportIMEI)
                    {
                        var rlsProductFull = dtbAutoStoreChange.Copy().AsEnumerable()
                                    .Where(r => !string.IsNullOrEmpty(r["IMEI"].ToString())
                                    && Convert.ToBoolean(r["SELECT"].ToString())).ToList();

                        if (rlsProductFull != null && rlsProductFull.Count() > 0)
                        {

                            DataTable dtbTemp = rlsProductFull.CopyToDataTable();
                            dtbTemp.Columns.Remove("IMEI");

                            var rlsProductID = (from x in dtbTemp.AsEnumerable()
                                                select new
                                                {
                                                    SELECT = x["SELECT"],
                                                    PRODUCTID = x["PRODUCTID"],
                                                    PRODUCTNAME = x["PRODUCTNAME"],
                                                    FROMSTOREID = x["FROMSTOREID"],
                                                    FROMSTORENAME = x["FROMSTORENAME"],
                                                    TOSTOREID = x["TOSTOREID"],
                                                    TOSTORENAME = x["TOSTORENAME"],
                                                    TOSTOREINSTOCK = x["TOSTOREINSTOCK"],
                                                    QUANTITYINSTOCK = x["QUANTITYINSTOCK"],
                                                    ISALLOWDECIMAL = x["ISALLOWDECIMAL"],
                                                    ISURGENT = x["ISURGENT"]
                                                })
                                               .Distinct();


                            foreach (var item in rlsProductID)
                            {
                                var lstTemp = rlsProductFull.Where(p => p["FROMSTOREID"].ToString().Trim() == item.FROMSTOREID.ToString().Trim()
                                                                       && p["TOSTOREID"].ToString().Trim() == item.TOSTOREID.ToString().Trim()
                                                                       && p["PRODUCTID"].ToString().Trim() == item.PRODUCTID.ToString().Trim()).ToList();
                                //Kiểm tra CheckInputCreateOrder 
                                if (!(CheckInputCreateOrder(objStoreChangeType
                                , Convert.ToInt32(item.FROMSTOREID.ToString().Trim())
                                , Convert.ToInt32(item.TOSTOREID.ToString().Trim()))))
                                {
                                    if (lstTemp != null && lstTemp.Count() > 0)
                                    {
                                        foreach (var temp in lstTemp)
                                            listExcept.Add(temp);

                                    }
                                    continue;
                                }
                                else
                                {
                                    if (lstTemp != null && lstTemp.Count() > 0)
                                    {
                                        foreach (var temp in lstTemp)
                                            TblListIMEI.Rows.Add(
                                                temp["FromStoreID"].ToString().Trim(),
                                                temp["ToStoreID"].ToString().Trim(),
                                                temp["ProductID"].ToString().Trim(),
                                                temp["IMEI"].ToString().Trim()
                                                );
                                    }
                                }
                                DataRow dr = dtbDataSource.NewRow();
                                dr["SELECT"] = item.SELECT;
                                dr["PRODUCTID"] = item.PRODUCTID.ToString().Trim();
                                dr["PRODUCTNAME"] = item.PRODUCTNAME;
                                dr["FROMSTOREID"] = item.FROMSTOREID;
                                dr["FROMSTORENAME"] = item.FROMSTORENAME;
                                dr["FROMSTOREINSTOCK"] = GetQuantity(item.PRODUCTID.ToString().Trim(), Convert.ToInt32(item.FROMSTOREID.ToString()));
                                dr["TOSTOREID"] = item.TOSTOREID;
                                dr["TOSTORENAME"] = item.TOSTORENAME;
                                dr["TOSTOREINSTOCK"] = GetQuantity(item.PRODUCTID.ToString().Trim(), Convert.ToInt32(item.TOSTOREID.ToString()));
                                dr["QUANTITY"] = rlsProductFull.Where(p => p["PRODUCTID"].ToString().Trim() == item.PRODUCTID.ToString().Trim()
                                                                        && p["FROMSTOREID"].ToString().Trim() == item.FROMSTOREID.ToString().Trim()
                                                                        && p["TOSTOREID"].ToString().Trim() == item.TOSTOREID.ToString().Trim()
                                                                        && Convert.ToBoolean(p["SELECT"].ToString())).ToList().Count;
                                dr["QUANTITYINSTOCK"] = dr["FROMSTOREINSTOCK"];
                                dr["ISALLOWDECIMAL"] = item.ISALLOWDECIMAL;
                                dr["IMEI"] = string.Empty;
                                dr["ISURGENT"] = item.ISURGENT;
                                dtbDataSource.Rows.Add(dr);
                            }
                        }
                    }

                    frmCreateStoreChangeOrder frmCreateStoreChangeOrder = new frmCreateStoreChangeOrder();
                    frmCreateStoreChangeOrder.DataSource = dtbDataSource;
                    frmCreateStoreChangeOrder.StoreChangeOrderType = objStoreChangeOrderType;
                    frmCreateStoreChangeOrder.StoreChangeCommandTypeID = intStoreChangeCommandTypeID;
                    frmCreateStoreChangeOrder.FromIMEI = bolIsCallFromImportIMEI;
                    frmCreateStoreChangeOrder.TblListIMEI = TblListIMEI;
                    frmCreateStoreChangeOrder.CommandID = CommandID;
                    // frmCreateStoreChangeOrder.LstData = lstData;
                    frmCreateStoreChangeOrder.ShowDialog();
                    if (frmCreateStoreChangeOrder.IsAccept)
                    {
                        // Clear;

                        dtbAutoStoreChange.Clear();


                        //for (int j = lstIndex.Count - 1; j >= 0; j--)
                        //    dtbAutoStoreChange.Rows.RemoveAt(lstIndex[j]);

                        cmdCreateStoreChangeCommand.Enabled = false;
                        grvListAutoStoreChange.OptionsBehavior.Editable = false;
                        cmdAdd.Enabled = false;
                        cmdImportExcel.Enabled = false;
                        //if (dtbDataSource.Rows.Count == dtbAutoStoreChange.Rows.Count)
                        //{
                        if (listExcept == null || listExcept.Count() < 1)
                        {
                            if (intMainGroupID == 0)
                            {
                                dtbAutoStoreChange = null;
                                BindData();
                                grvListAutoStoreChange.OptionsBehavior.Editable = true;
                                grdListAutoStoreChange.DataSource = dtbAutoStoreChange;
                                CustomFlex();
                                cmdCreateStoreChangeCommand.Enabled = false;
                                cmdAdd.Enabled = true;
                                cmdImportExcel.Enabled = true;
                                cboFromStoreID.SetValue(-1);
                                cboToStoreID.SetValue(-1);
                                cboProductList.ClearProductComboSource();
                                txtProductID.Text = string.Empty;
                                txtQuantity.Value = 0;
                            }
                        }
                        else
                        {
                            dtbAutoStoreChange = listExcept.CopyToDataTable();
                            dtbAutoStoreChange.AcceptChanges();
                            BindData();
                            grvListAutoStoreChange.OptionsBehavior.Editable = true;
                            grdListAutoStoreChange.DataSource = dtbAutoStoreChange;
                            CustomFlex();
                            cmdCreateStoreChangeCommand.Enabled = dtbAutoStoreChange.Rows.Count > 0;
                            cmdAdd.Enabled = true;
                            cmdImportExcel.Enabled = true;
                            //cboFromStoreID.SetValue(-1);
                            //cboToStoreID.SetValue(-1);
                            txtProductID.Text = string.Empty;
                            txtQuantity.Value = 0;
                        }
                    }
                    else
                    {
                        dtbAutoStoreChange.AcceptChanges();
                    }
                    if (bolIsCallFromAutoDistribute)
                        this.Close();
                }

                #endregion
            }
            else
            {
                #region !bolIsAutoCreateOrder
                if (!SaveAutoStoreChange())
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                            SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                cmdCreateStoreChangeCommand.Enabled = false;
                grvListAutoStoreChange.OptionsBehavior.Editable = false;
                cmdAdd.Enabled = false;
                cmdImportExcel.Enabled = false;
                MessageBox.Show(this, "Tạo lệnh chuyển kho thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (intMainGroupID == 0)
                {
                    dtbAutoStoreChange = null;
                    BindData();
                    grvListAutoStoreChange.OptionsBehavior.Editable = true;
                    grdListAutoStoreChange.DataSource = dtbAutoStoreChange;
                    CustomFlex();
                    cmdCreateStoreChangeCommand.Enabled = false;
                    cmdAdd.Enabled = true;
                    cmdImportExcel.Enabled = true;
                    cboFromStoreID.SetValue(-1);
                    cboToStoreID.SetValue(-1);
                    txtProductID.Text = string.Empty;
                    txtQuantity.Value = 0;
                }
                if (bolIsCallFromAutoDistribute)
                    this.Close();
                #endregion
            }
        }

        private void frmAutoStoreChangeDetail_Load(object sender, EventArgs e)
        {
            Rectangle screen = Screen.PrimaryScreen.WorkingArea;
            this.Location = new Point((screen.Width - 1050) / 2, (screen.Height - screen.Height) / 2);
            this.Size = new Size(1050, screen.Height);
            lstProductIDNotPass = new List<string>();
            CreateActionItem();
            dtbStoreCache = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
            dtbProductCache = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
            dtbStoreMainGroup = Library.AppCore.DataSource.GetDataSource.GetStoreMainGroup().Copy();
            cboProductList.InitControl(0, string.Empty, string.Empty, false, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL, Library.AppCore.Constant.EnumType.IsServiceType.ALL, Library.AppCore.Constant.EnumType.MainGroupPermissionType.VIEWREPORT);
            string _commandID = objPLCStoreChangeCommand.GetStoreChangeCommandNewID(SystemConfig.intDefaultStoreID);
            txtCommandID.Text = _commandID;
            if (string.IsNullOrEmpty(_commandID))
                MessageBoxObject.ShowWarningMessage(this, "Không tạo được mã lệnh yêu cầu chuyển kho");
            intStoreChangeCommandTypeID = AppConfig.GetIntConfigValue("AUTOSTORECHANGE_STORECHANGECOMMANDTYPEID");
            if (intStoreChangeCommandTypeID < 0)
            {
                MessageBox.Show(this, "Bạn chưa cấu hình loại lệnh chuyển kho mặc định", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }

            // LE VAN DONG - 04/07/2017
            if (this.Tag != null && this.Tag.ToString().Trim() != string.Empty)
            {
                try
                {
                    Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());

                    if (hstbParam.Contains("IsCanUseOldID"))
                    {
                        bolIsCanUseOldCode = hstbParam["IsCanUseOldID"].ToString().Trim() == "1";
                        chkIsCanUseOldCode.Checked = bolIsCanUseOldCode;
                        chkIsCanUseOldCode.Enabled = bolIsCanUseOldCode;
                    }
                    else
                    {
                        chkIsCanUseOldCode.Checked = false;
                        chkIsCanUseOldCode.Enabled = true;
                    }

                    if (hstbParam.Contains("AutoCreateOrder"))
                    {
                        cmdCreateStoreChangeCommand.Text = "Tạo YC chuyển kho";
                        bolIsAutoCreateOrder = hstbParam["AutoCreateOrder"].ToString().Trim() == "1";
                    }
                }
                catch { }
            }
            else
            {
                chkIsCanUseOldCode.Checked = false;
                chkIsCanUseOldCode.Enabled = false;
            }




            if (bolIsCallFromAutoDistribute)
            {
                dtbMainGroupPermission = Library.AppCore.DataSource.GetDataFilter.GetMainGroup(true, EnumType.IsRequestIMEIType.ALL, EnumType.IsServiceType.ALL, EnumType.MainGroupPermissionType.ALL);
                cboType.Enabled = false;
                LoadComboBox();
                if (bolIsReceive)
                    cboType.SelectedIndex = 1;
                else
                    cboType.SelectedIndex = 0;
                cboType_SelectionChangeCommitted(null, null);
                BindData();
                cmdAdd.Enabled = SystemConfig.objSessionUser.IsPermission("AUTOSTORECHANGE_ADDPRODUCT");
                btnHintStock.Click += new EventHandler(btnHintStock_Click);
                cboFromStoreID.SetValue(intStoreID);
                cboToStoreID.SetValue(strStoreIDList);
                cboMainGroupID.SetValue(intMainGroupIDFromAutoDistribute);
                btnExportTemplate.Enabled = false;
                cmdImportExcel.Enabled = false;
                ImportFromAutoDistribute();
                BindData();
                EnableControl(true);
            }
            else
            {
                if (dtbStore != null && dtbStore.Columns.Count > 0)
                {
                    if (dtbStore.Columns[0].ColumnName.ToUpper() == "ISSELECT")
                    {
                        dtbStore.Columns.RemoveAt(0);
                    }
                }
                cmdCreateStoreChangeCommand.Enabled = false;
                if (dtbAutoStoreChange != null && dtbAutoStoreChange.Rows.Count > 0)
                {
                    cboIsShowProduct.SelectedIndex = intIsShowProduct + 1;
                    cboIsShowProduct.Enabled = false;
                    if (intIsCheckRealInput == -1)
                        chkIsCheckRealInput.Checked = true;
                    else
                        chkIsCheckRealInput.Checked = false;
                    chkIsCheckRealInput.Enabled = false;
                }
                else
                {
                    cboIsShowProduct.SelectedIndex = 1;
                    chkIsCheckRealInput.Checked = false;
                }
                LoadComboBox();
                if (intMainGroupID > 0)
                {
                    cboMainGroupID.SetValue(intMainGroupID);
                    cboMainGroupID.Enabled = false;
                }
                BindData();
                cmdAdd.Enabled = SystemConfig.objSessionUser.IsPermission("AUTOSTORECHANGE_ADDPRODUCT");
                btnHintStock.Click += new EventHandler(btnHintStock_Click);
            }
        }

        private void CreateActionItem()
        {
            barExportQuantity.Caption = "Theo số lượng";
            barExportQuantity.Name = "0";
            barExportQuantity.Tag = "0";
            barExportQuantity.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barExport_ItemClick);
            pMnuExport.AddItem(barExportQuantity);

            barExportIMEI.Caption = "Theo IMEI";
            barExportIMEI.Name = "1";
            barExportIMEI.Tag = "1";
            barExportIMEI.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barExport_ItemClick);
            pMnuExport.AddItem(barExportIMEI);

            barExportMultiStore.Caption = "Chuyển nhiều kho";
            barExportMultiStore.Name = "2";
            barExportMultiStore.Tag = "2";
            barExportMultiStore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barExport_ItemClick);
            pMnuExport.AddItem(barExportMultiStore);

            barImportQuantity.Caption = "Theo số lượng";
            barImportQuantity.Name = "0";
            barImportQuantity.Tag = "0";
            barImportQuantity.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barImport_ItemClick);
            pMnuImport.AddItem(barImportQuantity);

            barImportIMEI.Caption = "Theo IMEI";
            barImportIMEI.Name = "1";
            barImportIMEI.Tag = "1";
            barImportIMEI.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barImport_ItemClick);
            pMnuImport.AddItem(barImportIMEI);

            barImportMultiStore.Caption = "Chuyển nhiều kho";
            barImportMultiStore.Name = "2";
            barImportMultiStore.Tag = "2";
            barImportMultiStore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barImport_ItemClick);
            pMnuImport.AddItem(barImportMultiStore);

        }

        private void barExport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intAction = Convert.ToInt32(e.Item.Tag);
            if (intAction == 0)
                ExportTemplate();
            if (intAction == 1)
                ExportTemplateIMEI();
            if (intAction == 2)
                ExportTemplateMultiStore();
        }

        private void barImport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intAction = Convert.ToInt32(e.Item.Tag);

            if (grvListAutoStoreChange.RowCount > 0)
            {
                if (MessageBox.Show(this, "Trên lưới đã có dữ liệu.\nChương trình sẽ xóa dữ liệu cũ và thay thế bằng dữ liệu mới", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                {
                    return;
                }
            }
            dtbAutoStoreChange.Clear();
            if (intAction < 2)
            {
                if (cboFromStoreID.StoreID < 1)
                {
                    string strMess = "Bạn phải chọn kho xuất";
                    if (cboType.SelectedIndex == 1)
                        strMess = "Bạn phải chọn kho nhập";
                    MessageBoxObject.ShowWarningMessage(this, strMess);
                    cboFromStoreID.Focus();
                    return;
                }
            }
            EnableControl(false);
            bolIsCallFromImportIMEI = false;

            if (intAction == 0)
            {
                DataTable tblProduct = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable();
                ImportExcelNew(tblProduct);
            }
            if (intAction == 1)
                ImportExcelIMEIBoost3();

            if (intAction == 2)
                ImportExcelMulti();
            BindData();
            EnableControl(true);
        }

        private void ImportExcelMulti()
        {
            DataTable tblProduct = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable();
            if (tblProduct == null)
            {
                return;
            }
            if (tblProduct.Rows.Count > 0 && tblProduct.Rows[0] != null)
            {
                if (!CheckExcelMulti(tblProduct))
                {
                    MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            else
            {
                if (tblProduct.Rows.Count == 0)
                {
                    MessageBox.Show(this, "Không tìm thấy dữ cập nhật!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            if (chkIsCanUseOldCode.Checked)
            {
                if (dtbStoreOldId != null)
                {
                    for (int i = 1; i < tblProduct.Rows.Count; i++)
                    {
                        DataRow[] rowsel = dtbStoreOldId.Select("OLDSTOREID='" + tblProduct.Rows[i][0].ToString() + "'");
                        if (rowsel.Length > 0)
                            tblProduct.Rows[i][0] = rowsel[0]["STOREID"];
                    }
                }

                // Thay thế mã sản phẩm
                if (dtbProductOld != null)
                {
                    for (int i = 1; i < tblProduct.Rows.Count; i++)
                    {
                        DataRow[] rowsel = dtbProductOld.Select("PRODUCTIDOLD='" + tblProduct.Rows[i][0].ToString() + "'");
                        if (rowsel.Length > 0)
                            tblProduct.Rows[i][5] = rowsel[0]["PRODUCTID"];
                    }
                }
            }
            DataTable dtbResult = null;
            tblProduct.Columns[XlsColumnMapper.FROMSTOREID].ColumnName = "FROMSTOREID";
            tblProduct.Columns[XlsColumnMapper.FromStoreName].ColumnName = "FromStoreName";
            tblProduct.Columns[XlsColumnMapper.ProductID].ColumnName = "ProductID";
            tblProduct.Columns[XlsColumnMapper.ProductName].ColumnName = "ProductName";
            tblProduct.Columns[XlsColumnMapper.Quantity].ColumnName = "Quantity";
            tblProduct.Columns[XlsColumnMapper.ToStoreID].ColumnName = "ToStoreID";
            tblProduct.Columns[XlsColumnMapper.ToStoreName].ColumnName = "ToStoreName";
            while (tblProduct.Columns.Count > 7)
                tblProduct.Columns.RemoveAt(7);
            tblProduct.Columns.Add("ErrorContent");
            tblProduct.Columns.Add("Status");
            tblProduct.Columns.Add("StatusID");
            tblProduct.Columns.Add("FromInStockQuantity");
            tblProduct.Columns.Add("ToInStockQuantity");
            tblProduct.Columns.Add("IMEI");
            tblProduct.Columns.Add("IsUrgent", typeof(bool));
            tblProduct.Rows.RemoveAt(0);

            dtbAutoStoreChange.Clear();
            bool bolIsPermission = Library.AppCore.SystemConfig.objSessionUser.IsPermission("AUTOSTORECHANGE_CHECKEMERGENCY");
            int intIsUrgent = 0;
            bool bolIsExistsError = false;

            string strMainGroupList = cboMainGroupID.MainGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");
            string[] mainGroupList = strMainGroupList.Split(',');

            try
            {
                tblProduct.AsEnumerable().ToList().ForEach(record =>
                {
                    for (int i = 0; i < tblProduct.Columns.Count; i++)
                    {
                        if (tblProduct.Columns[i].DataType == typeof(System.Boolean))
                        {
                            record[i] = false;
                            continue;
                        }

                        record[i] = record[i].GetString().Trim();
                    }
                });
                dtbResult = MultipleStoreChange_Import_PerformValidate(tblProduct);
                bolIsExistsError = dtbResult.AsEnumerable()
                    .Any(record => string.IsNullOrEmpty(record[XlsColumnMapper.StatusID].ToString())
                        || Convert.ToInt32(record[XlsColumnMapper.StatusID]) != 0);

                dtbResult.AsEnumerable().Where(record => !string.IsNullOrEmpty(record[XlsColumnMapper.StatusID].GetString())
                    && Convert.ToInt32(record[XlsColumnMapper.StatusID]) != 1).ToList().ForEach(record =>
                    {
                        decimal realQuantity = Convert.ToDecimal(record[XlsColumnMapper.FromInStockQuantity]) < Convert.ToDecimal(record[XlsColumnMapper.Quantity]) ?
                            Convert.ToDecimal(record[XlsColumnMapper.FromInStockQuantity]) : Convert.ToDecimal(record[XlsColumnMapper.Quantity]);
                        AddProduct(record[XlsColumnMapper.ProductID].GetString(),
                            record[XlsColumnMapper.ProductName].GetString(),
                            Convert.ToInt32(record[XlsColumnMapper.FROMSTOREID]),
                            record[XlsColumnMapper.FromStoreName].GetString(),
                            Convert.ToInt32(record[XlsColumnMapper.ToStoreID]),
                            record[XlsColumnMapper.ToStoreName].GetString(),
                             Convert.ToDecimal(record[XlsColumnMapper.FromInStockQuantity]),
                             Convert.ToDecimal(record[XlsColumnMapper.ToInStockQuantity]),
                             realQuantity, false, false);
                    });
            }
            catch (Exception objExce)
            {
                Library.AppCore.SystemErrorWS.Insert("Lỗi nhập thông tin chuyển kho từ Excel ", objExce, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (bolIsExistsError)
            {
                frmShowImportResult frmShowImportResult1 = new frmShowImportResult();
                frmShowImportResult1.ImportResult = dtbResult;
                frmShowImportResult1.ShowDialog();
            }
        }


        private DataTable MultipleStoreChange_Import_PerformValidate(DataTable _dataImported)
        {
            DataTable dtbResult = _dataImported.Copy();
            string errorMessage = string.Empty;

            StringBuilder recordMessage = new StringBuilder();
            dtbResult.AsEnumerable().ToList().ForEach(record =>
            {
                recordMessage.Clear();
                //Kiểm tra mã kho chuyển
                if (string.IsNullOrEmpty(record[XlsColumnMapper.FROMSTOREID].GetString()))
                    recordMessage.AppendLine("Mã kho chuyển không được bỏ trống!");
                else if (!record[XlsColumnMapper.FROMSTOREID].IsNumber())
                    recordMessage.AppendLine("Mã kho chuyển sai định dạng");
                else if (record[XlsColumnMapper.FROMSTOREID].GetString().Length > Int32.MaxValue.ToString().Length)
                    recordMessage.AppendLine("Mã kho chuyển vượt quá số ký tự cho phép: " + Int32.MaxValue.ToString().Length);

                //Kiểm tra sản phẩm chuyển
                if (string.IsNullOrEmpty(record[XlsColumnMapper.ProductID].GetString()))
                    recordMessage.AppendLine("Mã sản phẩm không được bỏ trống!");
                else if (record[XlsColumnMapper.ProductID].GetString().Length > 20)
                    recordMessage.AppendLine("Mã sản phẩm vượt quá số ký tự cho phép: 20");

                //Kiểm tra số lượng chuyển
                if (string.IsNullOrEmpty(record[XlsColumnMapper.Quantity].GetString()))
                    recordMessage.AppendLine("Số lượng không được bỏ trống!");
                else if (!record[XlsColumnMapper.Quantity].IsNumber())
                    recordMessage.AppendLine("Số lượng phải là kiểu số");
                else if (Convert.ToInt32(record[XlsColumnMapper.Quantity]) <= 0)
                    recordMessage.AppendLine("Số lượng chuyển phải lớn hơn 0");

                //Kiểm tra kho nhập
                if (string.IsNullOrEmpty(record[XlsColumnMapper.ToStoreID].GetString()))
                    recordMessage.AppendLine("Mã kho nhận không được bỏ trống!");
                else if (!record[XlsColumnMapper.ToStoreID].IsNumber())
                    recordMessage.AppendLine("Mã kho nhận sai định dạng");
                else if (record[XlsColumnMapper.ToStoreID].GetString().Length > Int32.MaxValue.ToString().Length)
                    recordMessage.AppendLine("Mã kho nhận vượt quá số ký tự cho phép: " + Int32.MaxValue.ToString().Length);
                bool isError = recordMessage.Length != 0;
                recordMessage.ToString();
                if (isError)
                {
                    record[XlsColumnMapper.StatusID] = 1;
                    record[XlsColumnMapper.ErrorContent] = recordMessage.ToString();
                }
            });


            recordMessage.Clear();
            dtbResult.AsEnumerable()
                .Where(record => string.IsNullOrEmpty(record[XlsColumnMapper.StatusID].GetString())
                    && dtbResult.AsEnumerable().Where(tempR =>
                        tempR[XlsColumnMapper.FROMSTOREID].GetString() == record[XlsColumnMapper.FROMSTOREID].GetString()
                        && tempR[XlsColumnMapper.ProductID].GetString() == record[XlsColumnMapper.ProductID].GetString()
                        && tempR[XlsColumnMapper.ToStoreID].GetString() == record[XlsColumnMapper.ToStoreID].GetString()
                        ).ToList().Count > 1)
                        .ToList().ForEach(record =>
                        {
                            record[XlsColumnMapper.Status] = "Lỗi";
                            record[XlsColumnMapper.StatusID] = 1;
                            record[XlsColumnMapper.ErrorContent] = "Dữ liệu bị trùng lặp";
                        });




            string strMainGroupList = cboMainGroupID.MainGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");
            string[] mainGroupList = strMainGroupList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            recordMessage.Clear();
            dtbResult.AsEnumerable().Where(record => string.IsNullOrEmpty(record[XlsColumnMapper.ErrorContent].GetString())).ToList().ForEach(record =>
            {
                recordMessage.Clear();
                DataRow rowOutStore = dtbStoreCache.AsEnumerable()
                .Where(row => Convert.ToInt32(row["STOREID"]) == Convert.ToInt32(record[XlsColumnMapper.FROMSTOREID]))
                .FirstOrDefault();
                DataRow rowInStore = dtbStoreCache.AsEnumerable().Where(row => Convert.ToInt32(row["STOREID"]) == Convert.ToInt32(record[XlsColumnMapper.ToStoreID])).FirstOrDefault();
                DataRow rowProduct = dtbProductOld.AsEnumerable().Where(row => row["PRODUCTID"].ToString().Trim() == record[XlsColumnMapper.ProductID].ToString().Trim()).FirstOrDefault();

                if (rowOutStore == null)
                    recordMessage.AppendLine("Mã kho xuất " + record[XlsColumnMapper.FROMSTOREID] + " không tồn tại trên hệ thống!");
                else
                {
                    record[XlsColumnMapper.FromStoreName] = rowOutStore["STOREID"].GetString() + " - " + rowOutStore["STORENAME"].GetString();
                    if (rowProduct != null && !ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(Convert.ToInt32(record[XlsColumnMapper.FROMSTOREID].ToString()),
                        Convert.ToInt32(rowProduct["MAINGROUPID"]), Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                        recordMessage.AppendLine("Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + rowProduct["MAINGROUPNAME"]);
                }

                if (rowInStore == null)
                    recordMessage.AppendLine("Mã kho nhập " + record[XlsColumnMapper.ToStoreID] + " không tồn tại trên hệ thống!");
                else
                {
                    record[XlsColumnMapper.ToStoreName] = rowInStore["STOREID"].GetString() + " - " + rowInStore["STORENAME"].GetString();
                    if (rowProduct != null && !ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(Convert.ToInt32(record[XlsColumnMapper.ToStoreID].ToString()),
                        Convert.ToInt32(rowProduct["MAINGROUPID"]), Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                        recordMessage.AppendLine("Kho nhập này không được phép nhập kho trên ngành hàng " + rowProduct["MAINGROUPNAME"]);
                }

                if (rowProduct != null && !ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(Convert.ToInt32(rowProduct["MAINGROUPID"])))
                    recordMessage.AppendLine("Bạn không có quyền thao tác trên ngành hàng " + rowProduct["MAINGROUPNAME"]);

                if (rowProduct == null)
                    recordMessage.AppendLine("Mã sản phẩm " + record[XlsColumnMapper.ProductID] + " không tồn tại trên hệ thống");
                else
                {
                    record[XlsColumnMapper.ProductName] = rowProduct["PRODUCTNAME"];
                    if (!string.IsNullOrEmpty(rowProduct["MAINGROUPNAME"].ToString()) && (mainGroupList.Any() && !mainGroupList.Any(d => d.Trim() == intMainGroupID.ToString())))
                        recordMessage.AppendLine("Sản phẩm " + record[XlsColumnMapper.ProductID] + " - " + rowProduct["PRODUCTNAME"] + " không thuộc ngành hàng đang xét");
                }
                if (recordMessage.Length != 0)
                {
                    record[XlsColumnMapper.Status] = "Lỗi";
                    record[XlsColumnMapper.StatusID] = 1;
                    record[XlsColumnMapper.ErrorContent] = recordMessage.ToString();
                }
            });

            dtbResult.AsEnumerable()
                .Where(record => string.IsNullOrEmpty(record[XlsColumnMapper.StatusID].GetString())
                && record[XlsColumnMapper.FROMSTOREID].GetString() == record[XlsColumnMapper.ToStoreID].GetString())
                .ToList().ForEach(record =>
                {
                    record[XlsColumnMapper.Status] = "Lỗi";
                    record[XlsColumnMapper.StatusID] = 1;
                    record[XlsColumnMapper.ErrorContent] = "Kho nhập không được trùng với kho xuất";
                });


            List<string> listProductID = dtbResult.AsEnumerable().Where(record => string.IsNullOrEmpty(record[XlsColumnMapper.StatusID].GetString())).Select(record => record[XlsColumnMapper.ProductID].GetString()).Distinct().ToList();
            List<int> listFromStoreID = dtbResult.AsEnumerable().Where(record => string.IsNullOrEmpty(record[XlsColumnMapper.StatusID].GetString())).Select(record => Convert.ToInt32(record[XlsColumnMapper.FROMSTOREID])).Distinct().ToList();
            List<int> listToStoreID = dtbResult.AsEnumerable().Where(record => string.IsNullOrEmpty(record[XlsColumnMapper.StatusID].GetString())).Select(record => Convert.ToInt32(record[XlsColumnMapper.ToStoreID])).Distinct().ToList();
            DataTable FromStoreInstock = MultiImport_GetStoreInStock(listProductID, listFromStoreID);
            DataTable ToStoreInstock = MultiImport_GetStoreInStock(listProductID, listToStoreID);
            dtbResult.AsEnumerable().Where(record => string.IsNullOrEmpty(record[XlsColumnMapper.StatusID].GetString())).ToList()
                .ForEach(record =>
                {
                    recordMessage.Clear();
                    decimal productQuantityInstockFrom = 0;
                    decimal productQuantityInstockTo = 0;
                    if (FromStoreInstock != null && FromStoreInstock.Rows.Count > 0)
                    {
                        DataRow rowFrom = FromStoreInstock.Select("ProductID ='" + record[XlsColumnMapper.ProductID].GetString() + "'" + " and StoreID = " + record[XlsColumnMapper.FROMSTOREID].GetString()).FirstOrDefault();
                        if (rowFrom != null)
                            productQuantityInstockFrom = Convert.ToDecimal(rowFrom["QUANTITY"]);
                    }
                    if (ToStoreInstock != null && ToStoreInstock.Rows.Count > 0)
                    {
                        DataRow rowTo = ToStoreInstock.Select("ProductID ='" + record[XlsColumnMapper.ProductID].GetString() + "'" + " and StoreID = " + record[XlsColumnMapper.ToStoreID].GetString()).FirstOrDefault();
                        if (rowTo != null)
                            productQuantityInstockTo = Convert.ToDecimal(rowTo["QUANTITY"]);
                    }

                    record[XlsColumnMapper.FromInStockQuantity] = productQuantityInstockFrom;
                    record[XlsColumnMapper.ToInStockQuantity] = productQuantityInstockTo;
                    if (productQuantityInstockFrom <= 0)
                    {
                        record[XlsColumnMapper.Status] = "Lỗi";
                        record[XlsColumnMapper.StatusID] = 1;
                        recordMessage.AppendLine("Sản phẩm không tồn kho");
                        record[XlsColumnMapper.ErrorContent] = recordMessage.ToString();
                    }
                    else if (Convert.ToInt32(record[XlsColumnMapper.Quantity]) > productQuantityInstockFrom)
                    {
                        record[XlsColumnMapper.Status] = "Cảnh báo";
                        record[XlsColumnMapper.StatusID] = 2;
                        recordMessage.AppendLine("Số lượng chuyển lớn hơn số lượng tồn");
                        record[XlsColumnMapper.ErrorContent] = recordMessage.ToString();
                    }
                });


            dtbResult.AsEnumerable().Where(record => string.IsNullOrEmpty(record[XlsColumnMapper.StatusID].GetString())).ToList()
                .ForEach(record =>
                {
                    record[XlsColumnMapper.StatusID] = 0;
                    record[XlsColumnMapper.Status] = "Thành công";
                });
            return dtbResult;
        }
        //Kiểm tra required, format
        private void IsValid_MultipleStoreChange_Import_RequiredData(ref DataRow record)
        {
            StringBuilder recordMessage = new StringBuilder();

            //Kiểm tra mã kho chuyển
            if (string.IsNullOrEmpty(record[XlsColumnMapper.FROMSTOREID].GetString()))
                recordMessage.AppendLine("Mã kho chuyển không được bỏ trống!");
            else if (!record[XlsColumnMapper.FROMSTOREID].IsNumber())
                recordMessage.AppendLine("Mã kho chuyển sai định dạng");
            else if (record[XlsColumnMapper.FROMSTOREID].GetString().Length > Int32.MaxValue.ToString().Length)
                recordMessage.AppendLine("Mã kho chuyển vượt quá số ký tự cho phép: " + Int32.MaxValue.ToString().Length);

            //Kiểm tra sản phẩm chuyển
            if (string.IsNullOrEmpty(record[XlsColumnMapper.ProductID].GetString()))
                recordMessage.AppendLine("Mã sản phẩm không được bỏ trống!");
            else if (record[XlsColumnMapper.ProductID].GetString().Length > 20)
                recordMessage.AppendLine("Mã sản phẩm vượt quá số ký tự cho phép: 20");

            //Kiểm tra số lượng chuyển
            if (string.IsNullOrEmpty(record[XlsColumnMapper.Quantity].GetString()))
                recordMessage.AppendLine("Số lượng không được bỏ trống!");
            else if (!record[XlsColumnMapper.Quantity].IsNumber())
                recordMessage.AppendLine("Số lượng phải là kiểu số");
            else if (Convert.ToInt32(record[XlsColumnMapper.Quantity]) <= 0)
                recordMessage.AppendLine("Số lượng chuyển phải lớn hơn 0");

            //Kiểm tra kho nhập
            if (string.IsNullOrEmpty(record[XlsColumnMapper.ToStoreID].GetString()))
                recordMessage.AppendLine("Mã kho nhận không được bỏ trống!");
            else if (!record[XlsColumnMapper.ToStoreID].IsNumber())
                recordMessage.AppendLine("Mã kho nhận sai định dạng");
            else if (record[XlsColumnMapper.ToStoreID].GetString().Length > Int32.MaxValue.ToString().Length)
                recordMessage.AppendLine("Mã kho nhận vượt quá số ký tự cho phép: " + Int32.MaxValue.ToString().Length);
            bool isError = recordMessage.Length != 0;
            recordMessage.ToString();
            if (isError)
            {
                record[XlsColumnMapper.StatusID] = 1;
                record[XlsColumnMapper.ErrorContent] = recordMessage.ToString();
            }
        }
        private DataTable MultiImport_GetStoreInStock(List<string> productIDs, List<int> storeIDs)
        {
            DataTable result = null;
            if (!productIDs.Any() || !storeIDs.Any())
                return result; ;

            string strProductIDList = string.Empty;
            string strStoreIDList = string.Empty;
            productIDs.ForEach(prodID => { strProductIDList += ("<" + prodID + ">"); });
            storeIDs.ForEach(storeID => { strStoreIDList += ("<" + storeID + ">"); });

            object[] objKeywords = new object[]{"@PRODUCTIDLIST", strProductIDList,
                                                "@STOREIDLIST", strStoreIDList,
                                                //"@ISSHOWPRODUCT", intIsShowProduct,
                                                "@ISCHECKREALINPUT", intIsCheckRealInput,
                                                //"@ISNEW",1,
                                                "@InStockStatusIDList",1 ,
                                                //"@IsExistStock", 1
                                                };
            result = objPLCReportDataSource.GetDataSource("PM_STOCKREPORT_AUTOCHANGE", objKeywords);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                MessageBoxObject.ShowWarningMessage(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            return result;
        }

        private bool CheckExcelMulti(DataTable tProduct)
        {
            try
            {
                if (string.IsNullOrEmpty(tProduct.Rows[0][0].ToString().Trim().ToLower())
                    || tProduct.Rows[0][0].ToString().Trim().ToLower() != "mã st chuyển")
                    return false;
                if (string.IsNullOrEmpty(tProduct.Rows[0][1].ToString().Trim().ToLower())
                    || tProduct.Rows[0][1].ToString().Trim().ToLower() != "tên st chuyển")
                    return false;
                if (string.IsNullOrEmpty(tProduct.Rows[0][2].ToString().Trim().ToLower())
                    || tProduct.Rows[0][2].ToString().Trim().ToLower() != "mã sản phẩm")
                    return false;
                if (string.IsNullOrEmpty(tProduct.Rows[0][3].ToString().Trim().ToLower())
                    || tProduct.Rows[0][3].ToString().Trim().ToLower() != "tên sản phẩm")
                    return false;
                if (string.IsNullOrEmpty(tProduct.Rows[0][4].ToString().Trim().ToLower())
                    || tProduct.Rows[0][4].ToString().Trim().ToLower() != "số lượng")
                    return false;
                if (string.IsNullOrEmpty(tProduct.Rows[0][5].ToString().Trim().ToLower())
                    || tProduct.Rows[0][5].ToString().Trim().ToLower() != "mã st nhận")
                    return false;
                if (string.IsNullOrEmpty(tProduct.Rows[0][6].ToString().Trim().ToLower())
                    || tProduct.Rows[0][6].ToString().Trim().ToLower() != "tên st nhận")
                    return false;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
            return true;
        }

        void btnHintStock_Click(object sender, EventArgs e)
        {
            if (grvListAutoStoreChange.RowCount < 1)
            {
                MessageBox.Show(this, "Trên lưới không có dữ liệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            DataTable dtbHint = new DataTable();
            if (bolIsCallFromImportIMEI)
            {
                dtbHint = dtbAutoStoreChange.Clone();
                var list = dtbAutoStoreChange.Copy().AsEnumerable().Where(p => p["IMEI"].ToString() == string.Empty).ToList();
                if (list != null && list.Count() > 0)
                {
                    foreach (var item in list)
                        dtbHint.ImportRow(item);
                }

                var rlsProductFull = dtbAutoStoreChange.Copy().AsEnumerable()
                                    .Where(r => !string.IsNullOrEmpty(r["IMEI"].ToString())).ToList();
                if (rlsProductFull != null && rlsProductFull.Count() > 0)
                {
                    DataTable dtbTemp = rlsProductFull.CopyToDataTable();
                    dtbTemp.Columns.Remove("IMEI");

                    var rlsProductID = (from x in dtbTemp.AsEnumerable()
                                        select new
                                        {
                                            PRODUCTID = x["PRODUCTID"],
                                            PRODUCTNAME = x["PRODUCTNAME"],
                                            FROMSTOREID = x["FROMSTOREID"],
                                            FROMSTORENAME = x["FROMSTORENAME"],
                                            TOSTOREID = x["TOSTOREID"],
                                            TOSTORENAME = x["TOSTORENAME"]
                                        })
                                       .Distinct();


                    foreach (var item in rlsProductID)
                    {
                        decimal sum = rlsProductFull.Where(p => p["PRODUCTID"].ToString().Trim() == item.PRODUCTID.ToString().Trim()
                                                                && p["FROMSTOREID"].ToString().Trim() == item.FROMSTOREID.ToString().Trim()
                                                                && p["TOSTOREID"].ToString().Trim() == item.TOSTOREID.ToString().Trim()).ToList().Count;
                        DataRow dr = dtbHint.NewRow();
                        dr["SELECT"] = true;
                        dr["PRODUCTID"] = item.PRODUCTID.ToString().Trim();
                        dr["PRODUCTNAME"] = item.PRODUCTNAME;
                        dr["FROMSTOREID"] = item.FROMSTOREID;
                        dr["FROMSTORENAME"] = item.FROMSTORENAME;
                        dr["FROMSTOREINSTOCK"] = GetQuantity(item.PRODUCTID.ToString().Trim(), Convert.ToInt32(item.FROMSTOREID.ToString())); ;
                        dr["TOSTOREID"] = item.TOSTOREID;
                        dr["TOSTORENAME"] = item.TOSTORENAME;
                        dr["TOSTOREINSTOCK"] = GetQuantity(item.PRODUCTID.ToString().Trim(), Convert.ToInt32(item.TOSTOREID.ToString()));
                        dr["QUANTITY"] = sum;
                        dr["QUANTITYINSTOCK"] = dr["FROMSTOREINSTOCK"];
                        dr["ISALLOWDECIMAL"] = false;
                        dr["IMEI"] = string.Empty;
                        dr["ISURGENT"] = false;
                        dtbHint.Rows.Add(dr);
                    }
                }
            }
            else
                dtbHint = dtbAutoStoreChange.Copy();
            frmAutoStoreChangeHintStock frmHint = new frmAutoStoreChangeHintStock();
            frmHint.dtbAutoStoreChange_Receive = dtbHint;
            frmHint.ShowDialog();
        }

        private void mnuExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdListAutoStoreChange);
        }

        private void cboIsShowProduct_SelectionChangeCommitted(object sender, EventArgs e)
        {
            intIsShowProduct = cboIsShowProduct.SelectedIndex - 1;
        }

        private void chkIsCheckRealInput_CheckStateChanged(object sender, EventArgs e)
        {
            if (chkIsCheckRealInput.Checked)
                intIsCheckRealInput = -1;
            else
                intIsCheckRealInput = 1;
        }

        private void mnuDeleteRow_Click(object sender, EventArgs e)
        {
            if (grvListAutoStoreChange.FocusedRowHandle < 0)
                return;
            cmdCreateStoreChangeCommand.Focus();
            grvListAutoStoreChange.DeleteSelectedRows();
            grdListAutoStoreChange.RefreshDataSource();
            grvListAutoStoreChange.Focus();
        }

        private void mnuGrid_Opening(object sender, CancelEventArgs e)
        {
            mnuDeleteRow.Enabled = mnuExportExcel.Enabled = mnuGrid.Enabled = mnuItemSelectAll.Enabled
                    = mnuItemUnSelectAll.Enabled = grvListAutoStoreChange.RowCount < 1 ? false : true;
        }

        private void cboType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboType.SelectedIndex == 0)
            {
                lblStore1.Text = "Từ kho:";
                lblStore2.Text = "Đến kho:";
            }
            else
            {
                lblStore1.Text = "Đến kho:";
                lblStore2.Text = "Từ kho:";
            }
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboType.SelectedIndex != intOldIndex)
            {
                if (grvListAutoStoreChange.RowCount > 1)
                {
                    if (MessageBox.Show(this, "Trên lưới đã có dữ liệu.\nChương trình sẽ xóa dữ liệu cũ và thay thế bằng dữ liệu mới", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    {
                        cboType.SelectedIndex = intOldIndex;
                        return;
                    }
                    dtbAutoStoreChange.Clear();
                }
            }
            intOldIndex = cboType.SelectedIndex;

        }

        private void txtQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
            {
                cmdAdd_Click(null, null);
            }
        }

        private void flexListAutoStoreChange_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            //if (flexListAutoStoreChange.Cols[e.Col].Name == "Quantity")
            //{
            //    try
            //    {
            //        decimal decQuantity = Convert.ToDecimal(flexListAutoStoreChange.Rows[e.Row][e.Col]);
            //        if (decQuantity % 2 != 0)
            //        {
            //            MessageBox.Show(this, "Sản phẩm không được phép nhập số lẻ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //            e.Cancel = true;
            //        }
            //    }
            //    catch
            //    { }
            //}
        }

        private void chkIsCanUseOldCode_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsCanUseOldCode.Checked && dtbStoreOldId == null)
            {
                dtbStoreOldId = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_STORE_OLDSTORE_SRH");
            }
        }

        private void grvListAutoStoreChange_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (grvListAutoStoreChange.FocusedColumn.FieldName.ToUpper() == "QUANTITY")
            {
                DataRow rowFocused = grvListAutoStoreChange.GetFocusedDataRow();
                if (rowFocused == null)
                    return;

                if (Convert.ToDecimal(rowFocused["Quantity"]) <= 0)
                {
                    MessageBox.Show(this, "Số lượng chuyển kho phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (!Convert.ToBoolean(rowFocused["IsAllowDecimal"]))
                {
                    rowFocused["Quantity"] = Math.Round(Convert.ToDecimal(rowFocused["Quantity"]), 0, MidpointRounding.AwayFromZero);
                }

                decimal decTotalInStockQuantity = 0;
                for (int i = 0; i < grvListAutoStoreChange.RowCount; i++)
                {
                    DataRow row = grvListAutoStoreChange.GetDataRow(i);
                    if (row == null)
                        continue;

                    if (row["PRODUCTID"].ToString().Trim() == rowFocused["PRODUCTID"].ToString().Trim())
                    {
                        decTotalInStockQuantity += Convert.ToDecimal(row["Quantity"]);
                    }
                }

                if (decTotalInStockQuantity > Convert.ToDecimal(rowFocused["QuantityInStock"]))
                {
                    MessageBox.Show(this, "Tổng số lượng chuyển kho lớn hơn số lượng tồn. \nTổng SL chuyển kho: " + decTotalInStockQuantity + " .Số lượng tồn hiện tại là: " + rowFocused["QuantityInStock"].ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Valid = false;
                    return;
                }

                //if (Convert.ToDecimal(rowFocused["Quantity"]) > Convert.ToDecimal(rowFocused["QuantityInStock"]))
                //{
                //    MessageBox.Show(this, "Số lượng chuyển kho lớn hơn số lượng tồn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    return;
                //}
            }

            #region Code cũ
            //if (grvListAutoStoreChange.FocusedColumn.FieldName.ToUpper() == "QUANTITY")
            //{
            //    DataRow row = grvListAutoStoreChange.GetFocusedDataRow();
            //    if (row == null)
            //        return;

            //    if (Convert.ToDecimal(row["Quantity"]) > Convert.ToDecimal(row["QuantityInStock"]))
            //    {
            //        MessageBox.Show(this, "Số lượng chuyển kho lớn hơn số lượng tồn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        return;
            //    }

            //    if (Convert.ToDecimal(row["Quantity"]) <= 0)
            //    {
            //        MessageBox.Show(this, "Số lượng chuyển kho phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        return;
            //    }

            //    if (!Convert.ToBoolean(row["IsAllowDecimal"]))
            //    {
            //        row["Quantity"] = Math.Round(Convert.ToDecimal(row["Quantity"]), 0, MidpointRounding.AwayFromZero);
            //    }
            //}
            #endregion
        }

        private void mnuItemSelectAll_Click(object sender, EventArgs e)
        {
            foreach (DataRow row in dtbAutoStoreChange.Rows)
            {
                if (row.RowState != DataRowState.Deleted)
                    row["Select"] = true;
            }
            grdListAutoStoreChange.RefreshDataSource();
        }

        private void mnuItemUnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (DataRow row in dtbAutoStoreChange.Rows)
            {
                if (row.RowState != DataRowState.Deleted)
                    row["Select"] = false;
            }
            grdListAutoStoreChange.RefreshDataSource();
        }

        private void grvListAutoStoreChange_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        //Hiếu chỉnh sửa chuyển chọn sản phẩm từ chọn 1 sang chọn nhiều 
        private void cboMainGroupID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cboProductList.InitControl(cboMainGroupID.MainGroupIDList, string.Empty, string.Empty, false, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL, Library.AppCore.Constant.EnumType.IsServiceType.ALL, Library.AppCore.Constant.EnumType.MainGroupPermissionType.VIEWREPORT);
        }
        private void grvListAutoStoreChange_ShowingEditor(object sender, CancelEventArgs e)
        {
            try
            {
                GridView view = sender as GridView;
                string strIMEI = view.GetRowCellValue(view.FocusedRowHandle, "IMEI").ToString().Trim();
                if (!string.IsNullOrEmpty(strIMEI))
                {
                    if (view.FocusedColumn.FieldName.ToString().ToLower() == "quantity")
                        e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        private static class XlsColumnMapper
        {
            //orignal columns
            public const int FROMSTOREID = 0;
            public const int FromStoreName = 1;
            public const int ProductID = 2;
            public const int ProductName = 3;
            public const int Quantity = 4;
            public const int ToStoreID = 5;
            public const int ToStoreName = 6;

            //ext
            public const int ErrorContent = 7;
            public const int Status = 8;
            public const int StatusID = 9;
            public const int FromInStockQuantity = 10;
            public const int ToInStockQuantity = 11;
            public const int IMEI = 12;
            public const int IsUrgent = 13;
        }

        private enum ERecordImportStatus
        {
            Invalid = 0,
            IsValid = 1
        }




    }

    public static class StoreChangeHelper
    {
        public static string GetString(this object obj)
        {
            return obj.IsNull() ? string.Empty : obj.ToString();
        }
        public static bool IsNull(this object obj)
        {
            return obj == null;
        }
        public static bool IsNumber(this object obj)
        {
            if (string.IsNullOrEmpty(obj.GetString()))
                return false;
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            return regex.IsMatch(obj.GetString());
        }
    }
}
