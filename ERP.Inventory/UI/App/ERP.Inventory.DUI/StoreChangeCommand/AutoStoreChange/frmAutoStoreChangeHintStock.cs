﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using DevExpress.XtraGrid.Views.Grid;
using ERP.MasterData.DUI.Common;
namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    /// <summary>
    /// Created by  :   Nguyễn Minh Triều
    /// Description :   Quản lý loại xác nhận chấm công
    /// </summary>
    public partial class frmAutoStoreChangeHintStock : Form
    {
        #region Khai báo biến
        public DataTable dtbAutoStoreChange_Receive = null;
        public DataTable dtbDataSource = null;
        #endregion

        #region Constructor
        public frmAutoStoreChangeHintStock()
        {
           InitializeComponent();
           this.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }
        #endregion

        #region Các hàm sự kiện

        private void frmAutoStoreChangeHintStock_Load(object sender, EventArgs e)
        {
            InitControl();
            FormatGrid();
            UpdateSource();
            grdData.DataSource = dtbDataSource;
        }

       
        #endregion

        #region Các hàm hỗ trợ
        /// <summary>
        /// Khởi tạo cho các control
        /// Xét các thuộc tính
        /// </summary>
        private void InitControl()
        {
            List<string> lstFieldName = new List<string>();
            ///////////////////////
            string[] arrCols = new string[] { "PRODUCTID", "PRODUCTNAME" };
            DataTable dtbTemp = dtbAutoStoreChange_Receive.DefaultView.ToTable(true, arrCols);

            dtbDataSource = new DataTable();
            DataColumn dcProductID = new DataColumn();
            dcProductID.ColumnName = "PRODUCTID";
            dcProductID.DataType = typeof(string);
            dcProductID.DefaultValue = string.Empty;
            dtbDataSource.Columns.Add(dcProductID);

            DataColumn dcProductName = new DataColumn();
            dcProductName.ColumnName = "PRODUCTNAME";
            dcProductName.DataType = typeof(string);
            dcProductName.DefaultValue = string.Empty;
            dtbDataSource.Columns.Add(dcProductName);
            //Add row///////////////////////////////
            foreach (DataRow drTemp in dtbTemp.Rows)
            {
                dtbDataSource.ImportRow(drTemp);
            }
            ////////////////////////////////////////
            lstFieldName.Add("PRODUCTID");
            lstFieldName.Add("PRODUCTNAME");

            DataColumn dcBeforeQuantity = new DataColumn();
            dcBeforeQuantity.ColumnName = "BEFOREQUANTITY";
            dcBeforeQuantity.DataType = typeof(decimal);
            dcBeforeQuantity.DefaultValue = 0;
            dtbDataSource.Columns.Add(dcBeforeQuantity);

            lstFieldName.Add("BEFOREQUANTITY");
            ///////////////////////
            DataTable dtbDistinctToStore = null;
            DataTable dtbDistinctFromStore = null;
            DataTable dtbDistinctStore = null;
            arrCols = new string[] { "TOSTOREID", "TOSTORENAME" };
            dtbDistinctToStore = dtbAutoStoreChange_Receive.DefaultView.ToTable(true, arrCols);

            arrCols = new string[] { "FROMSTOREID", "FROMSTORENAME" };
            dtbDistinctFromStore = dtbAutoStoreChange_Receive.DefaultView.ToTable(true, arrCols);
            if (dtbDistinctToStore != null && dtbDistinctFromStore != null)
            {
                dtbDistinctStore = dtbDistinctToStore.Copy();
                int intStoreID = 0;
                foreach (DataRow drItem in dtbDistinctFromStore.Rows)
                {
                    intStoreID = Convert.ToInt32(drItem["FROMSTOREID"]);
                    DataRow[] arrDRs = dtbDistinctToStore.Select("TOSTOREID=" + intStoreID);
                    if (arrDRs != null)
                    {
                        if (arrDRs.Length == 0)
                        {
                            DataRow drNew = dtbDistinctStore.NewRow();
                            drNew["TOSTOREID"] = intStoreID;
                            drNew["TOSTORENAME"] = Convert.ToString(drItem["FROMSTORENAME"]);

                            DataColumn dcCol = new DataColumn();
                            dcCol.ColumnName = "STOREID" + intStoreID;
                            dcCol.DataType = typeof(decimal);
                            dcCol.DefaultValue = 0;
                            dtbDataSource.Columns.Add(dcCol);
                            lstFieldName.Add("STOREID" + intStoreID);
                        }
                    }
                }
                // for  dtbDistinctToStore
                foreach (DataRow drItem in dtbDistinctToStore.Rows)
                {
                    intStoreID = Convert.ToInt32(drItem["TOSTOREID"]);
                    DataColumn dcCol = new DataColumn();
                    dcCol.ColumnName = "STOREID" + intStoreID;
                    dcCol.DataType = typeof(decimal);
                    dcCol.DefaultValue = 0;
                    dtbDataSource.Columns.Add(dcCol);
                    lstFieldName.Add("STOREID" + intStoreID);
                }
            }
            lstFieldName = lstFieldName.Distinct().ToList();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, lstFieldName.ToArray());
            grdViewData.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(grdViewData_CustomDrawCell);
        }

        void grdViewData_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            GridView currentView = sender as GridView;
            if (e.RowHandle >= 0)
            {
                if (e.Column.FieldName == "PRODUCTNAME")
                {
                    e.Appearance.ForeColor = Color.Red;
                    //e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
                }
            }
        }

        private void UpdateSource()
        {
            if (dtbDataSource != null)
            {
                string strExpressFilter, strExpressToStore, strExpressFromStore = string.Empty;
                string strColumnName = string.Empty;
                string strProductID = string.Empty;
                DataTable dtbByProductIDByStoreId = null;
                int intStoreId = 0;
                decimal decInStockOld_TO = 0;// tồn kho ban đầu
                decimal decInStockOld_FROM = 0;// tồn kho ban đầu
                decimal decInStockNew = 0;// tồn kho mới
                decimal decQuantity_Receive = 0;// tổng nhập
                decimal decQuantity_Send = 0;// tổng xuất
                foreach (DataRow drFetch in dtbDataSource.Rows)
                {
                    strProductID = Convert.ToString(drFetch["PRODUCTID"]);
                    strExpressFilter = " PRODUCTID='" + strProductID + "' ";
                    for (int i = 3; i < dtbDataSource.Columns.Count; i++)
                    {
                        strColumnName = dtbDataSource.Columns[i].ColumnName.Trim().ToUpper();
                        if (strColumnName.Contains("STOREID"))
                        {
                            intStoreId = Convert.ToInt32(strColumnName.Replace("STOREID", ""));
                            decInStockOld_TO = 0;
                            decInStockOld_FROM = 0;
                            decInStockNew = 0;
                            //tổng xuất/chuyển kho
                            strExpressFromStore = strExpressFilter + " AND FROMSTOREID=" + intStoreId;
                            dtbByProductIDByStoreId = Library.AppCore.DataTableClass.Select(dtbAutoStoreChange_Receive, strExpressFromStore);
                            if (dtbByProductIDByStoreId.Rows.Count > 0)
                                decInStockOld_FROM = Convert.ToDecimal(dtbByProductIDByStoreId.Rows[0]["FROMSTOREINSTOCK"]);
                            else
                                decInStockOld_FROM = 0;
                            decQuantity_Send = 0;
                            object objTotalQuantity_Send = dtbByProductIDByStoreId.Compute("Sum(QUANTITY)", "");
                            if (objTotalQuantity_Send != DBNull.Value)
                            {
                                decQuantity_Send = Math.Round(Convert.ToDecimal(objTotalQuantity_Send),2);
                            }
                            //tổng nhập/nhận từ kho khác chuyển sang
                            decQuantity_Receive = 0;
                            strExpressToStore = strExpressFilter + " AND TOSTOREID=" + intStoreId;
                            dtbByProductIDByStoreId = Library.AppCore.DataTableClass.Select(dtbAutoStoreChange_Receive, strExpressToStore);
                            if (dtbByProductIDByStoreId.Rows.Count > 0)
                                decInStockOld_TO = Convert.ToDecimal(dtbByProductIDByStoreId.Rows[0]["TOSTOREINSTOCK"]);
                            else
                                decInStockOld_TO = 0;
                            object objTotalQuantity_Receive = dtbByProductIDByStoreId.Compute("Sum(QUANTITY)", "");
                            if (objTotalQuantity_Receive != DBNull.Value)
                            {
                                decQuantity_Receive = Math.Round(Convert.ToDecimal(objTotalQuantity_Receive), 2);
                            }
                            // tồn kho mới  = tồn đầu - xuất + nhập
                            if (decInStockOld_TO != 0 && decInStockOld_FROM != 0) decInStockOld_TO = 0;
                            decInStockNew = decInStockOld_TO + decInStockOld_FROM - decQuantity_Send + decQuantity_Receive;
                            drFetch[strColumnName] = decInStockNew;
                        }
                    }

                }
            }
        }

        private bool FormatGrid()
        {
            try
            {
                grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewData.OptionsView.ShowAutoFilterRow = true;
                grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewData.OptionsView.ShowFooter = false;
                grdViewData.OptionsView.ShowGroupPanel = false;
                grdViewData.OptionsView.ColumnAutoWidth = false;

                grdViewData.ColumnPanelRowHeight = 40;
                string strColName = string.Empty;
                int intStoreId = 0;
                string strCaption = string.Empty;
                foreach (DataColumn dcol in dtbDataSource.Columns)
                {
                    strColName = dcol.ColumnName.ToUpper();
                    grdViewData.Columns[strColName].FieldName = strColName;
                    grdViewData.Columns[strColName].Name = "gridCol" + strColName;
                    grdViewData.Columns[strColName].OptionsColumn.ReadOnly = true;
                    grdViewData.Columns[strColName].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
                    grdViewData.Columns[strColName].Visible = true;
                    grdViewData.Columns[strColName].AppearanceHeader.Options.UseTextOptions = true;
                    grdViewData.Columns[strColName].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                    if (strColName == "PRODUCTID")
                    {
                        grdViewData.Columns[strColName].Caption = "Mã sản phẩm";
                        grdViewData.Columns[strColName].Width = 110;
                    }
                    else
                    {
                        if (strColName == "PRODUCTNAME")
                        {
                            grdViewData.Columns[strColName].Caption = "Tên sản phẩm";
                            grdViewData.Columns[strColName].Width = 200;
                        }
                        else
                        {
                            if (strColName == "BEFOREQUANTITY")
                                grdViewData.Columns[strColName].Visible = false;
                            else
                            {
                                intStoreId = Convert.ToInt32(strColName.Replace("STOREID", ""));
                                strCaption = GetStoreName(intStoreId);
                                grdViewData.Columns[strColName].Caption = strCaption;
                                grdViewData.Columns[strColName].Width = 170;// GetWidthFromString(strCaption, grdViewData.Columns[strColName].AppearanceHeader.Font);
                                grdViewData.Columns[strColName].DisplayFormat.FormatString = "#,##0.##";
                                grdViewData.Columns[strColName].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                            }
                        }
                    }
                }
                grdViewData.OptionsView.ShowFooter = true;
                grdViewData.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, FontStyle.Regular);
                grdViewData.Columns[0].Summary.Clear();
                grdViewData.Columns[0].Summary.Add(DevExpress.Data.SummaryItemType.Custom, grdViewData.Columns[0].FieldName, "Tổng số dòng : {0}");
                grdViewData.Columns[1].Summary.Clear();
                grdViewData.Columns[1].Summary.Add(DevExpress.Data.SummaryItemType.Count, grdViewData.Columns[1].FieldName, "{0}");
                grdViewData.Columns["PRODUCTID"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                grdViewData.Columns["PRODUCTNAME"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách", objExce.ToString());
                return false;
            }
            return true;
        }
        private int GetWidthFromString(string strInput, System.Drawing.Font yourFont)
        {
            int iMinWidthCol = 140;
            int iDisplaySize = strInput.Trim().Length;
            iDisplaySize = TextRenderer.MeasureText(strInput.Trim(), yourFont).Width + 40;
            if (iDisplaySize < iMinWidthCol)
                iDisplaySize = iMinWidthCol;
            return iDisplaySize;
        }
        private string GetStoreName(int intStoreId)
        {
            string strStoreName = string.Empty;
            if (dtbAutoStoreChange_Receive != null)
            {
                if (dtbAutoStoreChange_Receive.Rows.Count > 0)
                {
                    DataRow [] drRowSels = dtbAutoStoreChange_Receive.Select("TOSTOREID=" + intStoreId);
                    if (drRowSels == null || drRowSels.Length == 0)
                    {
                        // fromstore
                        drRowSels = dtbAutoStoreChange_Receive.Select("FROMSTOREID=" + intStoreId);
                        if (drRowSels == null || drRowSels.Length == 0)
                            strStoreName = string.Empty;
                        else
                        {
                            strStoreName = Convert.ToString(drRowSels[0]["FROMSTORENAME"]);
                        }
                    }
                    else
                    {
                        //tostore
                        strStoreName = Convert.ToString(drRowSels[0]["TOSTORENAME"]); 
                    }
                }
            }
            return strStoreName;
        }

        #endregion

        private void mnuItemExport_Click(object sender, EventArgs e)
        {
            try
            {
                if (!mnuItemExport.Enabled)
                    return;
                Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi xuất excel - form xem dữ liệu giả lập tồn kho- mnuItemExport_Click()", objExce.ToString());
                return;
            }
        }


      
        
    }
}
