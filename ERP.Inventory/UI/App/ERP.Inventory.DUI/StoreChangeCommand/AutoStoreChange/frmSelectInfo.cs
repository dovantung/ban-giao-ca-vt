﻿using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    public partial class frmSelectInfo : Form
    {
        #region Variable
        private bool bolIsAccept = false;
        private ERP.Inventory.PLC.StoreChangeCommand.PLCStoreChangeCommand objPLCStoreChangeCommand = new ERP.Inventory.PLC.StoreChangeCommand.PLCStoreChangeCommand();
        private ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = null;
        private ERP.MasterData.PLC.MD.WSStoreChangeOrderType.ResultMessage objResultMessage = null;
        private int intSTORECHANGECOMMANDTYPEID = 0;
        private DataTable dtbData = null;
        #endregion
        #region Property
        public bool IsAccept
        {
            get { return bolIsAccept; }
        }
        public ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType StoreChangeOrderType
        {
            get { return objStoreChangeOrderType; }
            set { objStoreChangeOrderType = value; }
        }

        public int STORECHANGECOMMANDTYPEID
        {
            get { return intSTORECHANGECOMMANDTYPEID; }
            set { intSTORECHANGECOMMANDTYPEID = value; }
        }
        #endregion
        #region Constructor
        public frmSelectInfo()
        {
            InitializeComponent();
            //this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            //this.Top = 0;
        }
        #endregion

        private void frmSelectInfo_Load(object sender, EventArgs e)
        {
            object[] objKeyWords = new object[] { "@STORECHANGECOMMANDTYPEID", intSTORECHANGECOMMANDTYPEID };
            dtbData = objPLCStoreChangeCommand.SearchDataStoreChangeOrderType(objKeyWords);
            if (dtbData == null)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, objResultMessage.Message, objResultMessage.MessageDetail);
                return;
            }
            grdData.DataSource = dtbData.Copy();
        }
        private void LoadData(string strSearchInfo)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(strSearchInfo))
                {
                    var drData = dtbData.AsEnumerable().Where(x => (x.Field<Int64>("STORECHANGEORDERTYPEID")).ToString().Contains(strSearchInfo.ToUpper()) || Library.AppCore.Globals.ConvertVN(x.Field<string>("STORECHANGEORDERTYPENAME").ToUpper()).Contains(Library.AppCore.Globals.ConvertVN(strSearchInfo.ToUpper()))).ToArray();
                    if (drData.Any())
                    {
                        grdData.DataSource = drData.CopyToDataTable();
                    }
                    else
                    {
                        grdData.DataSource = dtbData.Clone();
                    }
                }
                else
                {
                    grdData.DataSource = dtbData.Copy();
                }
            }
            catch (Exception objExc)
            {
                SystemErrorWS.Insert("Lỗi nạp danh sách loại yêu cầu chuyển kho", objExc.ToString(), "frmSelectInfo -> LoadData", DUIInventory_Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp danh sách loại yêu cầu chuyển kho");
            }
        }
        private void txtSearchInfo_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtSearchInfo.Text))
            {
                LoadData(txtSearchInfo.Text.Trim());
            }
            else
            {
                LoadData(txtSearchInfo.Text.Trim());
            }
        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetDataRow(grvData.FocusedRowHandle);
            int intStoreChangeOrderTypeID = 0;
            int.TryParse(row["StoreChangeOrderTypeID"].ToString(), out intStoreChangeOrderTypeID);
            this.bolIsAccept = true;
            new ERP.MasterData.PLC.MD.PLCStoreChangeOrderType().LoadInfo(ref objStoreChangeOrderType, intStoreChangeOrderTypeID);
            if (objStoreChangeOrderType == null)
                return;
            this.Close();
        }

        private void grdData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                grdData_DoubleClick(null, null);
        }

        private void txtSearchInfo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                grdData_DoubleClick(null, null);
        }
    }
}
