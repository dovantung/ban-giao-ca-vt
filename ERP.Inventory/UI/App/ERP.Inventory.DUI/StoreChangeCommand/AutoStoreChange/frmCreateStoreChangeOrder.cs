﻿using Library.AppCore;
using Library.AppCore.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    public partial class frmCreateStoreChangeOrder : Form
    {
        #region Variables
        private DataTable dtbDataSource = null;
        private PLC.StoreChangeCommand.PLCStoreChangeCommand objPLCStoreChangeCommand = new PLC.StoreChangeCommand.PLCStoreChangeCommand();
        private int intStoreChangeCommandTypeID = 0;
        private ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = new MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType();
        private bool bolIsAccept = false;
        private bool bolFromIMEI = false;
        private DataTable tblListIMEI = null;
        private bool bolFromEviction = false;
        public bool IsAllowEditNote = true;
        private DataTable dtbTransportCompany = null;
        private DataTable dtbTransportServices = null;

        public DataTable TblListIMEI
        {
            get { return tblListIMEI; }
            set { tblListIMEI = value; }
        }

        public bool FromIMEI
        {
            get { return bolFromIMEI; }
            set { bolFromIMEI = value; }
        }

        public bool FromEviction
        {
            get { return bolFromEviction; }
            set { bolFromEviction = value; }
        }
        #endregion
        #region Property
        public int StoreChangeCommandTypeID
        {
            get { return intStoreChangeCommandTypeID; }
            set { intStoreChangeCommandTypeID = value; }
        }

        public ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType StoreChangeOrderType
        {
            get { return objStoreChangeOrderType; }
            set { objStoreChangeOrderType = value; }
        }
        public DataTable DataSource
        {
            get { return dtbDataSource; }
            set { dtbDataSource = value; }
        }

        private List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> ListAutoStoreChangeContext { get; set; }
        public bool IsAccept
        {
            get { return bolIsAccept; }
            set { bolIsAccept = value; }
        }
        public string CommandID { get; set; }
        #endregion
        #region Support Functions

        private void CustomFlex()
        {
            if (FromEviction)
            {
                colIsUrgent.Visible = colStoreChangeCommandQuantity.Visible = colStoreChangeOrderQuantity.Visible = false;
                colNote.Width = 220;
            }
            //flexListAutoStoreChange.Cols[0].Visible = false;
            //flexListAutoStoreChange.Cols["FromStoreID"].Visible = false;
            //flexListAutoStoreChange.Cols["ToStoreID"].Visible = false;
            //flexListAutoStoreChange.Cols["QuantityInStock"].Visible = false;
            //flexListAutoStoreChange.Cols["Select"].AllowEditing = false;
            //flexListAutoStoreChange.Cols["ProductID"].AllowEditing = false;
            //flexListAutoStoreChange.Cols["ProductName"].AllowEditing = false;
            //flexListAutoStoreChange.Cols["FromStoreID"].AllowEditing = false;
            //flexListAutoStoreChange.Cols["FromStoreName"].AllowEditing = false;
            //flexListAutoStoreChange.Cols["FromStoreInStock"].AllowEditing = false;
            //flexListAutoStoreChange.Cols["ToStoreID"].AllowEditing = false;
            //flexListAutoStoreChange.Cols["ToStoreName"].AllowEditing = false;
            //flexListAutoStoreChange.Cols["ToStoreInStock"].AllowEditing = false;
            //flexListAutoStoreChange.Cols["Quantity"].AllowEditing = false;
            //flexListAutoStoreChange.Cols["Select"].Caption = "Chọn";
            //flexListAutoStoreChange.Cols["ProductID"].Caption = "Mã sản phẩm";
            //flexListAutoStoreChange.Cols["ProductName"].Caption = "Tên sản phẩm";
            //flexListAutoStoreChange.Cols["FromStoreName"].Caption = "Từ kho";
            //flexListAutoStoreChange.Cols["FromStoreInStock"].Caption = "Số lượng tồn";
            //flexListAutoStoreChange.Cols["ToStoreName"].Caption = "Đến kho";
            //flexListAutoStoreChange.Cols["ToStoreInStock"].Caption = "Số lượng tồn";
            //flexListAutoStoreChange.Cols["Quantity"].Caption = "Số lượng chuyển";
            //flexListAutoStoreChange.Cols["IsUrgent"].Caption = "Chuyển gấp";
            //flexListAutoStoreChange.Cols["ProductID"].AllowMerging = true;
            //flexListAutoStoreChange.Cols["ProductName"].AllowMerging = true;
            //flexListAutoStoreChange.Cols["FromStoreID"].AllowMerging = true;
            //flexListAutoStoreChange.Cols["FromStoreName"].AllowMerging = true;
            //flexListAutoStoreChange.Cols["FromStoreInStock"].AllowMerging = true;
            //flexListAutoStoreChange.Cols["Select"].Width = 60;
            //flexListAutoStoreChange.Cols["FromStoreInStock"].Width = 80;
            //flexListAutoStoreChange.Cols["ToStoreInStock"].Width = 80;
            //flexListAutoStoreChange.Cols["Quantity"].Width = 110;
            //flexListAutoStoreChange.Cols["IsUrgent"].Width = 80;
            //flexListAutoStoreChange.Cols["ProductName"].Width = 230;
            //flexListAutoStoreChange.Cols["QuantityInStock"].Width = 130;
            //flexListAutoStoreChange.Cols["ProductID"].Visible = true;
            //flexListAutoStoreChange.Cols["FromStoreInStock"].Format = "#,##0.####";
            //flexListAutoStoreChange.Cols["ToStoreInStock"].Format = "#,##0.####";
            //flexListAutoStoreChange.Cols["QuantityInStock"].Format = "#,##0.####";
            //flexListAutoStoreChange.Cols["StoreChangeCommandQuantity"].Format = "#,##0.####";
            //flexListAutoStoreChange.Cols["StoreChangeOrderQuantity"].Format = "#,##0.####";
            //flexListAutoStoreChange.Cols["StoreChangeCommandQuantity"].Caption = "SL đã tạo lệnh";
            //flexListAutoStoreChange.Cols["StoreChangeOrderQuantity"].Caption = "SL đã tạo yêu cầu";
            //flexListAutoStoreChange.Cols["StoreChangeOrderQuantity"].AllowEditing = false;
            //flexListAutoStoreChange.Cols["StoreChangeCommandQuantity"].AllowEditing = false;
            //flexListAutoStoreChange.Cols["IsUrgent"].DataType = typeof(Boolean);
            //flexListAutoStoreChange.Cols["IsAllowDecimal"].Visible = false;

            //C1.Win.C1Input.C1TextBox txtEditor1 = new C1.Win.C1Input.C1TextBox();
            //txtEditor1.NumericInputKeys = C1.Win.C1Input.NumericInputKeyFlags.Plus;
            //flexListAutoStoreChange.Cols["Quantity"].Editor = txtEditor1;
            //flexListAutoStoreChange.Cols["Quantity"].Format = "#,###,###,##0.####";
            //C1.Win.C1FlexGrid.CellStyle _style = flexListAutoStoreChange.Cols[1].StyleNew;
            //_style.ForeColor = Color.Red;
            //_style.Font = new Font("Arial", 10, FontStyle.Bold);

            //C1.Win.C1FlexGrid.CellStyle _style1 = flexListAutoStoreChange.Cols[2].StyleNew;
            //_style1.ForeColor = Color.Red;
            //_style1.Font = new Font("Arial", 10, FontStyle.Bold);
            //Library.AppCore.LoadControls.C1FlexGridObject.SetAllowDragging(flexListAutoStoreChange, false);

            //C1.Win.C1FlexGrid.CellStyle styleTitle = flexListAutoStoreChange.Styles.Add("flexStyleTitle");
            //styleTitle.WordWrap = true;
            //styleTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Bold);
            //styleTitle.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            //C1.Win.C1FlexGrid.CellRange range = flexListAutoStoreChange.GetCellRange(0, 0, 0, flexListAutoStoreChange.Cols.Count - 1);
            //range.Style = styleTitle;
            //flexListAutoStoreChange.Rows[0].Height = 40;


        }

        private bool SaveAutoStoreChange()
        {
            List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand> lstStoreChangeCommand = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand>();
            for (int i = 0; i < dtbDataSource.Rows.Count; i++)
            {
                PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand objStoreChangeCommandBO = new PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand();
                objStoreChangeCommandBO.FromStoreID = Convert.ToInt32(dtbDataSource.Rows[i]["FromStoreID"]);
                objStoreChangeCommandBO.ToStoreID = Convert.ToInt32(dtbDataSource.Rows[i]["ToStoreID"]);
                objStoreChangeCommandBO.StoreChangeCommandTypeID = intStoreChangeCommandTypeID;
                objStoreChangeCommandBO.CreatedUser = SystemConfig.objSessionUser.UserName;
                objStoreChangeCommandBO.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objStoreChangeCommandBO.Note = Convert.ToString(dtbDataSource.Rows[i]["Note"]);
                objStoreChangeCommandBO.IsUrgent = Convert.ToBoolean(dtbDataSource.Rows[i]["IsUrgent"]);
                objStoreChangeCommandBO.IsCreateStoreChangeOrder = true;
                string strProductID = Convert.ToString(dtbDataSource.Rows[i]["ProductID"]).Trim();
                decimal decQuantity = Convert.ToDecimal("0" + Convert.ToDecimal(dtbDataSource.Rows[i]["Quantity"]));
                if (decQuantity > 0)
                {
                    List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail> lstStoreChangeCommandDetail = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail>();
                    PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail objStoreChangeCommandDetail = new PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail();
                    objStoreChangeCommandDetail.ProductID = strProductID;
                    objStoreChangeCommandDetail.Quantity = decQuantity;
                    if (!Convert.ToBoolean(dtbDataSource.Rows[i]["IsAllowDecimal"]))
                    {
                        objStoreChangeCommandDetail.Quantity = Math.Round(objStoreChangeCommandDetail.Quantity, 0, MidpointRounding.AwayFromZero);
                    }
                    objStoreChangeCommandDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;

                    lstStoreChangeCommandDetail.Add(objStoreChangeCommandDetail);

                    objStoreChangeCommandBO.StoreChangeCommandDetailList = lstStoreChangeCommandDetail.ToArray();
                }
                lstStoreChangeCommand.Add(objStoreChangeCommandBO);
            }
            DataTable tblStoreChangeCommand = gridControl.DataSource as DataTable;
            tblStoreChangeCommand = Globals.DataTableSelect(tblStoreChangeCommand, "Select = 1");
            DataTable tblFromStore = Globals.SelectDistinct(tblStoreChangeCommand, "FromStoreID");
            if (tblFromStore != null)
            {
                tblFromStore.TableName = "tblFromStore";
            }
            PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeOrder objStoreChangeOrder = CreateStoreChangeOrder();
            #region An insert Note
            DataTable dtbNote = new DataTable();
            dtbNote.TableName = "tblNote";
            dtbNote.Columns.Add("PRODUCTID", typeof(string));
            dtbNote.Columns.Add("FROMSTOREID", typeof(int));
            dtbNote.Columns.Add("TOSTOREID", typeof(int));
            dtbNote.Columns.Add("NOTE", typeof(string));
            DataTable dtb = dtbDataSource.Copy();
            for (int i = 0; i < dtbDataSource.Rows.Count; i++)
            {
                DataRow rowNote = dtbNote.NewRow();
                rowNote["PRODUCTID"] = Convert.ToString(dtb.Rows[i]["ProductID"]);
                rowNote["FROMSTOREID"] = Convert.ToInt32(dtb.Rows[i]["FromStoreID"]);
                rowNote["TOSTOREID"] = Convert.ToInt32(dtb.Rows[i]["ToStoreID"]);
                rowNote["NOTE"] = Convert.ToString(dtb.Rows[i]["Note"]);
                dtbNote.Rows.Add(rowNote);
            }
            #endregion
            //objPLCStoreChangeCommand.InsertMultiAndStoreChangeOrder(lstStoreChangeCommand, objStoreChangeOrder, tblFromStore);
            objPLCStoreChangeCommand.InsertMultiAndStoreChangeOrder5(lstStoreChangeCommand, objStoreChangeOrder, tblFromStore, dtbNote);

            return !SystemConfig.objSessionUser.ResultMessageApp.IsError;
        }

        private void Excute()
        {
            frmWaitDialog.Show(string.Empty, "Đang tạo yêu cầu chuyển kho");
        }
        private bool SaveAutoStoreChangeIMEI3()
        {

            Thread objThread = new Thread(new ThreadStart(Excute));
            objThread.Start();
            List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand> lstStoreChangeCommand = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand>();
            for (int i = 0; i < dtbDataSource.Rows.Count; i++)
            {
                PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand objStoreChangeCommandBO = new PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand();
                objStoreChangeCommandBO.FromStoreID = Convert.ToInt32(dtbDataSource.Rows[i]["FromStoreID"]);
                objStoreChangeCommandBO.ToStoreID = Convert.ToInt32(dtbDataSource.Rows[i]["ToStoreID"]);
                objStoreChangeCommandBO.StoreChangeCommandTypeID = intStoreChangeCommandTypeID;
                objStoreChangeCommandBO.CreatedUser = SystemConfig.objSessionUser.UserName;
                objStoreChangeCommandBO.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objStoreChangeCommandBO.IsUrgent = Convert.ToBoolean(dtbDataSource.Rows[i]["IsUrgent"]);
                objStoreChangeCommandBO.Note = Convert.ToString(dtbDataSource.Rows[i]["Note"]);

                objStoreChangeCommandBO.IsCreateStoreChangeOrder = true;
                string strProductID = Convert.ToString(dtbDataSource.Rows[i]["ProductID"]).Trim();
                decimal decQuantity = Convert.ToDecimal("0" + Convert.ToDecimal(dtbDataSource.Rows[i]["Quantity"]));
                if (decQuantity > 0)
                {
                    List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail> lstStoreChangeCommandDetail = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail>();
                    PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail objStoreChangeCommandDetail = new PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail();
                    objStoreChangeCommandDetail.ProductID = strProductID;
                    objStoreChangeCommandDetail.Quantity = decQuantity;
                    if (!Convert.ToBoolean(dtbDataSource.Rows[i]["IsAllowDecimal"]))
                    {
                        objStoreChangeCommandDetail.Quantity = Math.Round(objStoreChangeCommandDetail.Quantity, 0, MidpointRounding.AwayFromZero);
                    }
                    objStoreChangeCommandDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;
                    lstStoreChangeCommandDetail.Add(objStoreChangeCommandDetail);
                    objStoreChangeCommandBO.StoreChangeCommandDetailList = lstStoreChangeCommandDetail.ToArray();
                }
                lstStoreChangeCommand.Add(objStoreChangeCommandBO);
            }
            DataTable tblStoreChangeCommand = gridControl.DataSource as DataTable;
            tblStoreChangeCommand = Globals.DataTableSelect(tblStoreChangeCommand, "Select = 1");
            DataTable tblFromStore = Globals.SelectDistinct(tblStoreChangeCommand, "FromStoreID");
            if (tblFromStore != null)
                tblFromStore.TableName = "tblFromStore";
            if (TblListIMEI != null)
                TblListIMEI.TableName = "TblListIMEI";
            #region An insert Note
            DataTable dtbNote = new DataTable();
            dtbNote.TableName = "tblNote";
            dtbNote.Columns.Add("PRODUCTID", typeof(string));
            dtbNote.Columns.Add("FROMSTOREID", typeof(int));
            dtbNote.Columns.Add("TOSTOREID", typeof(int));
            dtbNote.Columns.Add("NOTE", typeof(string));
            DataTable dtb = dtbDataSource.Copy();
            for (int i = 0; i < dtbDataSource.Rows.Count; i++)
            {
                DataRow rowNote = dtbNote.NewRow();
                rowNote["PRODUCTID"] = Convert.ToString(dtb.Rows[i]["ProductID"]);
                rowNote["FROMSTOREID"] = Convert.ToInt32(dtb.Rows[i]["FromStoreID"]);
                rowNote["TOSTOREID"] = Convert.ToInt32(dtb.Rows[i]["ToStoreID"]);
                rowNote["NOTE"] = Convert.ToString(dtb.Rows[i]["Note"]);
                dtbNote.Rows.Add(rowNote);
            }
            #endregion
            PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeOrder objStoreChangeOrder = CreateStoreChangeOrder();

            objPLCStoreChangeCommand.InsertMultiAndStoreChangeOrder3(lstStoreChangeCommand, objStoreChangeOrder, tblFromStore, TblListIMEI, dtbNote);

            frmWaitDialog.Close();
            Thread.Sleep(0);
            objThread.Abort();

            return !SystemConfig.objSessionUser.ResultMessageApp.IsError;

        }


        /// <summary>
        /// Tạo phiếu chuyển kho
        /// </summary>
        private PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeOrder CreateStoreChangeOrder()
        {
            try
            {
                //Load phuong tien van chuyen mac dinh
                int intTransportTypeID = 0;
                DataTable dtbTransportType = Library.AppCore.DataSource.GetDataSource.GetTransportType();
                if (dtbTransportType != null && dtbTransportType.Rows.Count > 0)
                {
                    DataRow[] drTransportTypeDefault = dtbTransportType.Select("IsDefault=1");
                    if (drTransportTypeDefault.Length > 0)
                        intTransportTypeID = Convert.ToInt32(drTransportTypeDefault[0]["TransportTypeID"]);
                    else
                        intTransportTypeID = Convert.ToInt32(dtbTransportType.Rows[0]["TransportTypeID"]);
                }

                //Thông tin phiếu chuyển kho
                PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeOrder objStoreChangeOrderBO = new PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeOrder();
                objStoreChangeOrderBO.TransportTypeID = intTransportTypeID;
                objStoreChangeOrderBO.StoreChangeOrderTypeID = objStoreChangeOrderType.StoreChangeOrderTypeID;
                objStoreChangeOrderBO.StoreChangeTypeID = Convert.ToInt32(objStoreChangeOrderType.StoreChangeTypeID);
                objStoreChangeOrderBO.Content = string.Empty;
                objStoreChangeOrderBO.OrderDate = Library.AppCore.Globals.GetServerDateTime();
                objStoreChangeOrderBO.CreatedUser = SystemConfig.objSessionUser.UserName;
                objStoreChangeOrderBO.CreatedDate = Globals.GetServerDateTime();
                objStoreChangeOrderBO.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objStoreChangeOrderBO.ExpiryDate = Globals.GetServerDateTime().AddDays(2);
                objStoreChangeOrderBO.IsNew = true;
                objStoreChangeOrderBO.IsReviewed = objStoreChangeOrderType.IsAutoReview;
                objStoreChangeOrderBO.Content = txtContent.Text.Trim();
                objStoreChangeOrderBO.ExpiryDate = dteExpiryDate.Value.Date;
                objStoreChangeOrderBO.TransportTypeID = Convert.ToInt32(cboTransportTypeID.SelectedValue);
                if (CommandID != null)
                    objStoreChangeOrderBO.StoreChangeCommandID = CommandID;
                if (objStoreChangeOrderBO.IsReviewed)
                {
                    objStoreChangeOrderBO.ReviewedDate = Library.AppCore.Globals.GetServerDateTime();
                    objStoreChangeOrderBO.ReviewedUser = SystemConfig.objSessionUser.UserName;

                }
                return objStoreChangeOrderBO;
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi cập nhật yêu cầu chuyển kho", objEx.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi cập nhật yêu cầu chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        #endregion
        #region Event
        public frmCreateStoreChangeOrder()
        {
            InitializeComponent();
        }
        public frmCreateStoreChangeOrder(List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> listAutoStoreChangeContext)
        {
            InitializeComponent();
            ListAutoStoreChangeContext = listAutoStoreChangeContext;

        }

        private void frmCreateStoreChangeOrder_Load(object sender, EventArgs e)
        {
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
            this.Text = objStoreChangeOrderType.StoreChangeOrderTypeName;
            DataTable dtb = ConvertDataSource();

            if (!IsAllowEditNote)
            {
                colNote.OptionsColumn.AllowEdit = false;
                colNote.OptionsColumn.ReadOnly = true;
            }


            gridControl.DataSource = dtb;
            if (dtb != null && dtb.Rows.Count > 0)
                btnCreateStoreChangeCommand.Enabled = true;
            LoadCombobox();
        }
        private DataTable ConvertDataSource()
        {
            DataTable result = new DataTable();
            result.Columns.AddRange(new DataColumn[] {
                new DataColumn() { ColumnName = "FromStoreID" },
                new DataColumn() { ColumnName = "FromStoreName" },
                new DataColumn() { ColumnName = "ToStoreID" },
                new DataColumn() { ColumnName = "ToStoreName" },
                new DataColumn() { ColumnName = "ProductID" },
                new DataColumn() { ColumnName = "ProductName" },
                new DataColumn() { ColumnName = "InstockStatusID" },
                new DataColumn() { ColumnName = "InstockStatusName" },
                new DataColumn() { ColumnName = "FromStoreInStock" },
                new DataColumn() { ColumnName = "ToStoreInStock" },
                new DataColumn() { ColumnName = "Quantity" },
                new DataColumn() { ColumnName = "Note", DataType = typeof(string) },
            });

            ListAutoStoreChangeContext.Where(d => d.Detail.Any()).ToList().ForEach(d =>
            {
                var prod_name = d.Detail.Where(f => f.IsSelected && f.IsValidConsignmentType).Select(f => new { ProductID = f.ProductID, ProductName = f.ProductName, FromInStockQuantity = f.FromInStockQuantity, AutoQuantity = f.AutoQuantity, ToInStockQuantity = f.ToInStockQuantity }).Distinct().ToList();
                Dictionary<string, decimal> product_quantity = new Dictionary<string, decimal>();
                prod_name.ForEach(prod =>
                {
                    product_quantity.Add(prod.ProductID, d.Detail.Where(f => f.ProductID == prod.ProductID && f.IsSelected).Sum(f => f.AutoQuantity));
                    DataRow r = result.NewRow();
                    r.ItemArray = new object[] {
                        d.FromStore,
                        d.FromStoreName,
                        d.ToStore,
                        d.ToStoreName,
                       prod.ProductID,
                        prod.ProductName,
                        d.ProductState,
                        d.ImportProductStateName,
                        prod.FromInStockQuantity,
                        prod.ToInStockQuantity,
                        product_quantity.FirstOrDefault(g=>g.Key == prod.ProductID).Value
                    };
                    result.Rows.Add(r);
                });
            });

            return result;
        }
        private void LoadCombobox()
        {
            DataTable dtbTransportTypeID = Library.AppCore.DataSource.GetDataSource.GetTransportType();
            cboTransportTypeID.DataSource = dtbTransportTypeID;
            cboTransportTypeID.DisplayMember = "TRANSPORTTYPENAME";
            cboTransportTypeID.ValueMember = "TRANSPORTTYPEID";
            DateTime dteExpiry = DateTime.Now.AddDays(7);
            dteExpiryDate.Value = dteExpiry;
            object[] objKeywords = new object[] { "@TRANSPORTTYPEID", -1
            };
            PLC.StoreChangeCommand.PLCStoreChangeCommand objPLCStoreChangeCommand = new PLC.StoreChangeCommand.PLCStoreChangeCommand();
            dtbTransportCompany = objPLCStoreChangeCommand.SearchDataTranCompany(objKeywords);
            //if (dtbTransportCompany != null)
            //{
            //    cboTransportCompany.InitControl(false, dtbTransportCompany, "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đơn vị chuyển phát--");
            //    if (dtbTransportCompany.Rows.Count > 0)
            //        cboTransportCompany.SetValue(Convert.ToInt32(dtbTransportCompany.Rows[0]["TRANSPORTCOMPANYID"]));
            //}



            // Dịch vụ
            dtbTransportServices = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("TP_TRANSPORTSERVICE_CACHE");
            //if (dtbTransportServices != null)
            //{
            //    cboTransportServices.InitControl(false, dtbTransportServices, "TRANSPORTSERVICEID", "TRANSPORTSERVICENAME", "--Chọn dịch vụ chuyển phát--");
            //    if (dtbTransportServices.Rows.Count > 0)
            //        cboTransportServices.SetValue(Convert.ToInt32(dtbTransportServices.Rows[0]["TRANSPORTSERVICEID"]));
            //}
            cboTransportTypeID_SelectedIndexChanged(null,null);

        }

        private void btnCreateStoreChangeCommand_Click(object sender, EventArgs e)
        {
            if (cboTransportTypeID.SelectedIndex < 0)
            {
                MessageBox.Show(this, "Vui lòng chọn phương tiện vận chuyển", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Focus();
                cboTransportTypeID.Focus();
                SendKeys.Send("{F4}");
                return;
            }
            if (cboTransportCompany.ColumnID <= 0)
            {
                MessageBox.Show(this, "Vui lòng chọn phương tiện vận chuyển", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Focus();
                cboTransportTypeID.Focus();
                SendKeys.Send("{F4}");
                return;
            }
            if (cboTransportServices.ColumnID <= 0)
            {
                MessageBox.Show(this, "Vui lòng chọn đối dịch vụ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Focus();
                cboTransportServices.Focus();
                SendKeys.Send("{F4}");
                return;
            }
            if (dteExpiryDate.Value.Date < DateTime.Now.Date)
            {
                MessageBox.Show(this, "Ngày hết hạn phải lớn hơn hoặc bằng ngày hiện tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Focus();
                dteExpiryDate.Focus();
                return;
            }

            var dtbSource = (DataTable)gridControl.DataSource;
            dtbSource.AsEnumerable().Where(d => !string.IsNullOrEmpty(d["NOTE"].GetString())).ToList().ForEach(row =>
            {
                var objStoreChange = ListAutoStoreChangeContext.Where(d => d.FromStore == Convert.ToInt32(row["FromStoreID"])
                && d.ToStore == Convert.ToInt32(row["ToStoreID"]) && d.ProductState == Convert.ToInt32(row["InstockStatusID"]))
                .FirstOrDefault();
                var details = objStoreChange.Detail.ToList();
                details.Where(d => d.ProductID == row["ProductID"].GetString()).ToList().ForEach(d => d.Note = row["Note"].GetString());
                objStoreChange.Detail = details.ToArray();
            });



            ListAutoStoreChangeContext.ForEach(item =>
            {
                item.Content = txtContent.Text;
                item.ExpiredDate = dteExpiryDate.Value;
                item.TransportTypeID = Convert.ToInt32(cboTransportTypeID.SelectedValue);
                item.TransportCompanyID = Convert.ToInt32(cboTransportCompany.ColumnID);
                item.TransportServicesID = Convert.ToInt32(cboTransportServices.ColumnID);
                item.IsReviewed = objStoreChangeOrderType.IsAutoReview;
                if (item.Detail.Any(d => d.IsUrgent && d.IsSelected && d.IsValidConsignmentType))
                    item.IsUrgent = true;
            });


            DoInsert(ListAutoStoreChangeContext.Where(d => !d.IsEmptyDetail).ToList());
            //objPLCStoreChangeCommand.InsertAutoStoreChange(ListAutoStoreChangeContext.Where(d => !d.IsEmptyDetail).ToList(), SystemConfig.intDefaultStoreID, true);
            //if (!SystemConfig.objSessionUser.ResultMessageApp.IsError)
            //{
            //    MessageBox.Show(this, "Tạo lệnh chuyển kho thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    this.IsAccept = true;
            //    this.Close();
            //}
            //else
            //{
            //    this.IsAccept = false;
            //    MessageBox.Show(this, "Tạo lệnh chuyển kho thất bại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        /// <summary>
        /// Implementing
        /// </summary>
        /// <param name="list"></param>
        private void DoInsert(List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> list)
        {
           

                var listFailure = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeOrderItemFailure>();
            int succeedCounter = 0;
            double totalPage = 0;
            try
            {
                BeginSuspendForm();
                int pageIndex = 0;
                const int pageSize = 1;
                totalPage = Math.Ceiling((double)list.Count / pageSize);
                psInsert.Maximum = (int)totalPage;
                psInsert.Value = pageIndex;
                lblProcessing.Visible = true;
                psInsert.Visible = true;
                Application.DoEvents();
                while (!isCancel && pageIndex < totalPage)
                {
                  
                    var listInsert = list.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                    foreach (var item in listInsert)
                    {
                        item.StoreChangeCommandID = item.StoreChangeCommandID + pageIndex;
                        foreach (var item1 in item.Detail)
                        {
                             
                            item1.Quantity = item1.AutoQuantity;
                        }
                    }

                    var _listFailure = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeOrderItemFailure>();
                    var resultMessage = objPLCStoreChangeCommand.InsertAutoStoreChange(listInsert, SystemConfig.intDefaultStoreID, true, ref _listFailure);
                    if (!resultMessage.IsError)
                    {
                        listInsert.ForEach(d => d.IsSent = true);
                        succeedCounter += listInsert.Count;
                        if (_listFailure != null && _listFailure.Any())
                            listFailure.AddRange(_listFailure);
                    }
                    else
                    {
                        SystemErrorWS.Insert(resultMessage.Message, resultMessage.MessageDetail, Globals.ModuleName);
                    }

                    pageIndex++;
                    psInsert.Value++;
                    Application.DoEvents();

                }
            }
            finally
            {
                EndSuspendForm();
                if (listFailure != null && listFailure.Any())
                {
                    DataTable dtbProduct = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
                    DataTable dtbProductStatus = Library.AppCore.DataSource.GetDataSource.GetProductStatus().Copy();
                    listFailure.ForEach(failed =>
                    {
                        var itemByFromStore = list.FirstOrDefault(item => item.FromStore == failed.FromStoreID);
                        if (itemByFromStore != null)
                            failed.FromStoreName = itemByFromStore.FromStoreName;

                        var itembyToStore = list.FirstOrDefault(item => item.ToStore == failed.ToStoreID);
                        if (itembyToStore != null)
                            failed.ToStoreName = itembyToStore.ToStoreName;

                        DataRow rProductStatus = dtbProductStatus.AsEnumerable().FirstOrDefault(r => Convert.ToInt32(r["PRODUCTSTATUSID"]) == failed.InStockStatusID);
                        if (rProductStatus != null)
                            failed.InStockStatusName = Convert.ToString(rProductStatus["PRODUCTSTATUSNAME"]);

                        DataRow rProduct = dtbProduct.AsEnumerable().FirstOrDefault(r => Convert.ToString(r["PRODUCTID"]).Trim() == failed.ProductID.Trim());
                        if (rProduct != null)
                            failed.ProductName = Convert.ToString(rProduct["PRODUCTNAME"]);
                    });
                    frmCreateStoreChangeOrderFailedItem frm = new frmCreateStoreChangeOrderFailedItem(listFailure);
                    frm.ShowDialog();
                }
                if (totalPage != 0)
                {
                    this.IsAccept = true;
                    StringBuilder str = new StringBuilder();
                    str.AppendLine("Tổng số phiếu: " + list.Count);
                    str.AppendLine("Đã tạo thành công: " + succeedCounter + "/" + list.Count);
                    MessageBox.Show(this, str.ToString(), "Thông tin tác vụ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }



            }

        }
        private bool isCancel = false;
        private void BeginSuspendForm()
        {
            btnCreateStoreChangeCommand.Enabled = cboTransportTypeID.Enabled = txtContent.Enabled = gridControl.Enabled = false;
        }
        private void EndSuspendForm()
        {
            btnCreateStoreChangeCommand.Enabled = cboTransportTypeID.Enabled = txtContent.Enabled = gridControl.Enabled = true;
        }

        #endregion

        private void grvViewData_ShowingEditor(object sender, CancelEventArgs e)
        {
            if (grvViewData.FocusedColumn.FieldName == "Note")
            {
                DevExpress.XtraEditors.BaseEdit edit = (sender as DevExpress.XtraGrid.Views.Base.BaseView).ActiveEditor;
                if (edit is DevExpress.XtraEditors.TextEdit)
                    (edit as DevExpress.XtraEditors.TextEdit).Properties.MaxLength = 2000;
            }
        }

        private void grvViewData_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Control && e.KeyCode == Keys.C)
                {
                    Clipboard.SetText(grvViewData.GetFocusedDisplayText());
                    e.Handled = true;
                }
            }
            catch { }
        }

        private void cboTransportTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int TransportTypeID = -1;
            DataTable dtb = new DataTable();
            dtb.Columns.Add("TRANSPORTSERVICEID", typeof(int));
            dtb.Columns.Add("TRANSPORTSERVICENAME", typeof(string));
            if (!int.TryParse((cboTransportTypeID.SelectedValue ?? "").ToString().Trim(), out TransportTypeID))
            {
                cboTransportServices.InitControl(false, dtb, "TRANSPORTSERVICEID", "TRANSPORTSERVICENAME", "--- Dịch vụ vận chuyển ---");
                return;
            }
            if(TransportTypeID < 1)
            {
                cboTransportServices.InitControl(false, dtb, "TRANSPORTSERVICEID", "TRANSPORTSERVICENAME", "--- Dịch vụ vận chuyển ---");
                return;
            }
            var varResult = dtbTransportCompany.AsEnumerable().Where(row => (row.Field<object>("TRANSPORTTYPEID")??"").ToString().Trim() == TransportTypeID.ToString());
            if(!varResult.Any())
            {
                cboTransportServices.InitControl(false, dtb, "TRANSPORTSERVICEID", "TRANSPORTSERVICENAME", "--- Dịch vụ vận chuyển ---");
                return;
            }
            cboTransportCompany.InitControl(false, varResult.CopyToDataTable(), "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đơn vị chuyển phát--");
            //object[] objKeywords = new object[] { "@TRANSPORTTYPEID", cboTransportTypeID.SelectedValue
            //};
            //PLC.StoreChangeCommand.PLCStoreChangeCommand objPLCStoreChangeCommand = new PLC.StoreChangeCommand.PLCStoreChangeCommand();
            //dtbTransportCompany = objPLCStoreChangeCommand.SearchDataTranCompany(objKeywords);
            //if (dtbTransportCompany != null)
            //{
            //    cboTransportCompany.InitControl(false, dtbTransportCompany, "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đơn vị chuyển phát--");
            //    if (dtbTransportCompany.Rows.Count > 0)
            //        cboTransportCompany.SetValue(Convert.ToInt32(dtbTransportCompany.Rows[0]["TRANSPORTCOMPANYID"]));
            //}




            //object[] objKeywords = new object[] { "@TRANSPORTTYPEID", cboTransportTypeID.SelectedValue
            //};

            //dtbTransportCompany = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_TRANSPORTCOMPANY_SHRBYID", objKeywords);
            //if (dtbTransportCompany != null)
            //{
            //    cboTransportCompany.InitControl(false, dtbTransportCompany, "TRANSPORTCOMPANYID", "TRANSPORTCOMPANYNAME", "--Chọn đơn vị chuyển phát--");
            //    if (dtbTransportCompany.Rows.Count > 0)
            //      cboTransportCompany.SetValue(Convert.ToInt32(dtbTransportCompany.Rows[0]["TRANSPORTCOMPANYID"]));
            //}
        }

        private void cboTransportCompany_SelectionChangeCommitted(object sender, EventArgs e)
        {

            DataRow[] rowService = dtbTransportServices.Select("TRANSPORTCOMPANYID = " + cboTransportCompany.ColumnID);
            if (!rowService.Any())
            {
                DataTable dtb = new DataTable();
                dtb.Columns.Add("TRANSPORTSERVICEID", typeof(int));
                dtb.Columns.Add("TRANSPORTSERVICENAME", typeof(string));
                cboTransportServices.InitControl(false, dtb, "TRANSPORTSERVICEID", "TRANSPORTSERVICENAME", "--- Dịch vụ vận chuyển ---");
                return;
            }
            cboTransportServices.InitControl(false, rowService.CopyToDataTable(), "TRANSPORTSERVICEID", "TRANSPORTSERVICENAME", "--- Dịch vụ vận chuyển ---");
        }
    }
}
