﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    public partial class frmCreateStoreChangeOrderFailedItem : Form
    {
        private List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeOrderItemFailure> FailureItem { get; set; }
        public frmCreateStoreChangeOrderFailedItem(List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeOrderItemFailure> failureItem)
        {
            InitializeComponent();
            FailureItem = failureItem;
        }

        private void frmCreateStoreChangeOrderFailedItem_Load(object sender, EventArgs e)
        {
            gridControl.DataSource = FailureItem;
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog dialog = new SaveFileDialog())
            {
                if(dialog.ShowDialog() == DialogResult.OK)
                {
                    string filename = dialog.FileName;
                    gridControl.ExportToXlsx(filename);
                    if (MessageBox.Show("Bạn có muốn mở file vừa export?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.OK)
                        Process.Start(filename);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
