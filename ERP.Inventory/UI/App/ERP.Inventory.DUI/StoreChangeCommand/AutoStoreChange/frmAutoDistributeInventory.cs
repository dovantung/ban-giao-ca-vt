﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.LoadControls;
using Library.AppCore;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    public partial class frmAutoDistributeInventory : Form
    {
        /// <summary>
        /// Created by : Đinh Đăng Thiên
        /// Date       : 19/01/2015
        /// Desc       : Chia hàng tự động
        /// </summary>
        #region constructors
        public frmAutoDistributeInventory()
        {
            InitializeComponent();
        }
        #endregion

        #region variables
        private List<string> fieldNames = null;
        private DataTable dtbResource = null;
        private DataTable dtbAutoDistribute_Main = null;
        private DataTable dtbAutoDistribute = null;
        private int intDistributeQuantity = 0;
        private int intTransferQuantity = 0;
        private int intRowHandle = -1;
        string strFeildName = string.Empty;
        #endregion

        #region functions
        private void InitControl()
        {
            cboMainGroup.SelectionChangeCommitted += new EventHandler(cboMainGroup_SelectionChangeCommitted);
            cboSubGroup.SelectionChangeCommitted += new EventHandler(cboSubGroup_SelectionChangeCommitted);
            cboBrand.SelectionChangeCommitted += new EventHandler(cboBrand_SelectionChangeCommitted);
        }

        private void LoadCombobox()
        {
            cboCenterStore.InitControl(false, false);
            cboMainGroup.InitControl(false, false);
            cboSubGroup.InitControl(true, 0);
            cboBrand.InitControl(true, 0);
            cboIsShowProduct.SelectedIndex = 1;
            cboCenterStore_SelectionChangeCommitted(null, null);
            cboSubGroup_SelectionChangeCommitted(null, null);
            DateTime date = DateTime.Now.AddDays(-7);
            dtpFromDate.Value = date;
        }

        private bool FormatGrid()
        {
            try
            {
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames.ToArray());
                Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);

                grdViewData.OptionsView.ColumnAutoWidth = false;
                grdViewData.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
                grdViewData.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
                grdViewData.Columns["CENTERQUANTITY"].Caption = "SL tồn";
                grdViewData.Columns["DISTRIBUTEQUANTITY"].Caption = chkShortView.Checked ? " " : "SL chia";
                grdViewData.Columns["PRODUCTID"].Width = 120;
                grdViewData.Columns["PRODUCTNAME"].Width = 220;
                grdViewData.Columns["CENTERQUANTITY"].Width = 60;
                grdViewData.Columns["DISTRIBUTEQUANTITY"].Width = chkShortView.Checked ? 40 : 60;
                grdViewData.Columns["DISTRIBUTEQUANTITY"].OptionsColumn.AllowEdit = true;
                grdViewData.Columns["DISTRIBUTEQUANTITY"].ColumnEdit = repDistributeQuantity;
                grdViewData.Bands.Clear();
                grdViewData.Bands.Add(gridBand1);
                grdViewData.Bands.Add(gridBand2);
                gridBand1.Columns.Clear();
                gridBand2.Columns.Clear();
                gridBand2.Children.Clear();

                gridBand1.Columns.Add(grdViewData.Columns["PRODUCTID"]);
                gridBand1.Columns.Add(grdViewData.Columns["PRODUCTNAME"]);
                if (!chkShortView.Checked)
                    gridBand2.Columns.Add(grdViewData.Columns["CENTERQUANTITY"]);

                gridBand2.Columns.Add(grdViewData.Columns["DISTRIBUTEQUANTITY"]);
                gridBand2.Caption = chkShortView.Checked ? ERP.MasterData.PLC.MD.PLCStore.GetStoreShortName(cboCenterStore.StoreID) : ERP.MasterData.PLC.MD.PLCStore.GetStoreName(cboCenterStore.StoreID);

                for (int i = grdViewData.Columns["CENTERSTOREID"].VisibleIndex + 1; i < grdViewData.Columns.Count - 3; i += 4)
                {
                    grdViewData.Columns[i + 1].Width = 60;
                    grdViewData.Columns[i + 1].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    grdViewData.Columns[i + 1].DisplayFormat.FormatString = "#,##0";
                    grdViewData.Columns[i + 1].Caption = "SL tồn";

                    grdViewData.Columns[i + 2].Width = 60;
                    grdViewData.Columns[i + 2].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    grdViewData.Columns[i + 2].DisplayFormat.FormatString = "#,##0";
                    grdViewData.Columns[i + 2].Caption = "SL bán";

                    grdViewData.Columns[i + 3].Width = chkShortView.Checked ? 40 : 60;
                    grdViewData.Columns[i + 3].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    grdViewData.Columns[i + 3].DisplayFormat.FormatString = "#,##0";
                    grdViewData.Columns[i + 3].Caption = chkShortView.Checked ? " " : "SL chuyển";
                    grdViewData.Columns[i + 3].OptionsColumn.AllowEdit = true;
                    grdViewData.Columns[i + 3].ColumnEdit = repTransferQuantity;

                    DevExpress.XtraGrid.Views.BandedGrid.GridBand band = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
                    string strStoreName = grdViewData.Columns[i].Name.ToString().Substring(3, grdViewData.Columns[i].Name.ToString().Length - 3);
                    string[] arrCaption = strStoreName.Split('-').ToArray();
                    int intStoreID = -1;
                    int.TryParse(arrCaption[0], out intStoreID);
                    string strCaption = chkShortView.Checked ? ERP.MasterData.PLC.MD.PLCStore.GetStoreShortName(intStoreID) : ERP.MasterData.PLC.MD.PLCStore.GetStoreName(intStoreID);
                    band.Caption = strCaption;
                    if (!chkShortView.Checked)
                    {
                        band.Columns.Add(grdViewData.Columns[i + 1]);
                        band.Columns.Add(grdViewData.Columns[i + 2]);
                    }
                    band.Columns.Add(grdViewData.Columns[i + 3]);
                    grdViewData.Bands.Add(band);
                }
                //if (cboViewType.SelectedIndex == 1)
                //    gridBand2.Caption = "Điểm thưởng";
                grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewData.OptionsView.ShowAutoFilterRow = true;
                grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewData.OptionsView.ShowGroupPanel = false;
                grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewData.ColumnPanelRowHeight = 50;
                gridBand1.Fixed = FixedStyle.Left;
                gridBand2.Fixed = FixedStyle.Left;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới danh sách xem báo cáo giá sản phẩm");
                SystemErrorWS.Insert("Lỗi định dạng lưới danh sách xem báo cáo giá sản phẩm", objExce.ToString(), this.Name + " -> FormatGrid", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private bool ValidateData()
        {
            if (cboCenterStore.StoreID < 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho trung tâm");
                cboCenterStore.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(cboStore.StoreIDList))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn siêu thị");
                cboStore.Focus();
                return false;
            }
            if (cboMainGroup.MainGroupID < 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn ngành hàng");
                cboMainGroup.Focus();
                return false;
            }
            if (Globals.DateDiff(dtpToDate.Value, dtpFromDate.Value, Globals.DateDiffType.DATE) < 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Từ ngày phải nhỏ hơn hoặc bằng đến ngày");
                dtpFromDate.Focus();
                return false;
            }
            return true;
        }

        private bool CreateDataTable(DataTable dtbDataMain, DataTable dtbData)
        {
            try
            {
                dtbResource = new DataTable();
                dtbResource.Columns.Add("PRODUCTID", typeof(string));
                dtbResource.Columns.Add("PRODUCTNAME", typeof(string));
                dtbResource.Columns.Add("TOTALSALESQUANTITY", typeof(int));
                dtbResource.Columns.Add("TOTALQUANTITY", typeof(int));
                dtbResource.Columns.Add("CENTERQUANTITY", typeof(int));
                dtbResource.Columns.Add("DISTRIBUTEQUANTITY", typeof(int));
                dtbResource.Columns.Add("CENTERSTOREID", typeof(string));
                fieldNames = new List<string>();
                fieldNames.Add("PRODUCTID");
                fieldNames.Add("PRODUCTNAME");
                fieldNames.Add("TOTALSALESQUANTITY");
                fieldNames.Add("TOTALQUANTITY");
                fieldNames.Add("CENTERQUANTITY");
                fieldNames.Add("DISTRIBUTEQUANTITY");
                fieldNames.Add("CENTERSTOREID");
                string strStoreIDList = cboStore.Text;
                strStoreIDList = strStoreIDList.Replace("] [", ";");
                strStoreIDList = strStoreIDList.Replace("[", "");
                strStoreIDList = strStoreIDList.Replace("]", "");
                List<string> lstStoreID = strStoreIDList.Split(';').ToList();
                int intHoldingQuantity = Convert.ToInt32(txtHoldingQuantity.Value);
                foreach (string strStoreID in lstStoreID)
                {
                    DataColumn colStore = new DataColumn(strStoreID.Trim(), typeof(int));
                    dtbResource.Columns.Add(colStore);
                    fieldNames.Add(strStoreID.Trim());
                    DataColumn colQuantity = new DataColumn("Quantity-" + strStoreID.Trim(), typeof(int));
                    dtbResource.Columns.Add(colQuantity);
                    fieldNames.Add("Quantity-" + strStoreID.Trim());
                    DataColumn colSalesQuantity = new DataColumn("SalesQuantity-" + strStoreID.Trim(), typeof(int));
                    dtbResource.Columns.Add(colSalesQuantity);
                    fieldNames.Add("SalesQuantity-" + strStoreID.Trim());
                    DataColumn colTransferQuantity = new DataColumn("TransferQuantity-" + strStoreID.Trim(), typeof(int));
                    dtbResource.Columns.Add(colTransferQuantity);
                    fieldNames.Add("TransferQuantity-" + strStoreID.Trim());
                }
                dtbResource.PrimaryKey = new DataColumn[] { dtbResource.Columns["PRODUCTID"] };

                //Đổ dữ liệu vào các cột chính
                foreach (DataRow rowData in dtbDataMain.Rows)
                {
                    DataRow row = dtbResource.Rows.Find(new object[] { rowData["PRODUCTID"].ToString() });
                    if (row == null)
                    {
                        row = dtbResource.NewRow();
                        row["PRODUCTID"] = rowData["PRODUCTID"];
                        row["PRODUCTNAME"] = rowData["PRODUCTNAME"];
                        row["CENTERQUANTITY"] = rowData["CENTERQUANTITY"];
                        row["DISTRIBUTEQUANTITY"] = rowData["DISTRIBUTEQUANTITY"];
                        row["CENTERSTOREID"] = rowData["CENTERSTOREID"];
                        for (int i = dtbResource.Columns["CENTERSTOREID"].Ordinal + 1; i < dtbResource.Columns.Count - 3; i += 4)
                        {
                            string strStoreID = dtbResource.Columns[i].ColumnName;
                            string[] array = strStoreID.Split('-').ToArray();
                            int intStoreID = -1;
                            int.TryParse(array[0], out intStoreID);
                            row[i] = intStoreID;
                            row[i + 1] = row[i + 2] = row[i + 3] = 0;
                        }
                        dtbResource.Rows.Add(row);
                    }

                    //Cập nhật số lượng, số lượng bán các kho
                    foreach (DataRow r in dtbData.Rows)
                    {
                        if (r["PRODUCTID"].ToString().Trim() == row["PRODUCTID"].ToString().Trim())
                        {
                            string strStoreName = r["STORENAME"].ToString().Trim();
                            if (dtbResource.Columns.Contains(strStoreName))
                            {
                                row["Quantity-" + strStoreName] = r["QUANTITY"];
                                row["SalesQuantity-" + strStoreName] = r["SALESQUANTITY"];
                            }
                        }
                    }
                    int intTotal = 0;
                    int intTotalSales = 0;
                    //Cập nhật tổng số lượng và tổng số lượng bán tất cả các kho
                    for (int i = dtbResource.Columns["CENTERSTOREID"].Ordinal + 1; i < dtbResource.Columns.Count - 3; i += 4)
                    {
                        intTotal += Convert.ToInt32(row[i + 1]);
                        intTotalSales += Convert.ToInt32(row[i + 2]);
                    }
                    row["TOTALQUANTITY"] = intTotal;
                    row["TOTALSALESQUANTITY"] = intTotalSales;

                    //Cập nhật số lượng chuyển cho các kho
                    int intDistributeQuantity = 0;
                    int.TryParse(row["DISTRIBUTEQUANTITY"].ToString(), out intDistributeQuantity);
                    int intTotalQuantity = 0;
                    int.TryParse(row["TOTALQUANTITY"].ToString(), out intTotalQuantity);
                    int intTotalSalesQuantity = 0;
                    int.TryParse(row["TOTALSALESQUANTITY"].ToString(), out intTotalSalesQuantity);
                    DataRow[] rowsProduct = dtbData.Select("ProductID='" + row["PRODUCTID"].ToString().Trim() + "'");
                    if (rowsProduct.Length < 1)
                        continue;
                    bool bolIsExistHolding = false;
                    foreach (DataRow r in rowsProduct)
                    {
                        string strStoreName = r["STORENAME"].ToString().Trim();
                        int intTransferQ = 0;
                        if (dtbResource.Columns.Contains(strStoreName))
                        {
                            int intQuantity = 0;
                            int.TryParse(row["Quantity-" + strStoreName].ToString(), out intQuantity);
                            if (intTotalSalesQuantity == 0)
                            {
                                row["TransferQuantity-" + strStoreName] = -intQuantity;
                                intTransferQ = -intQuantity;
                            }
                            else
                            {
                                int intSalesQuantity = 0;
                                int.TryParse(row["SalesQuantity-" + strStoreName].ToString(), out intSalesQuantity);

                                intTransferQ = Convert.ToInt32(((decimal)intSalesQuantity / (decimal)intTotalSalesQuantity) * (intDistributeQuantity + intTotalQuantity) - intQuantity);
                                row["TransferQuantity-" + strStoreName] = intTransferQ;
                            }
                            if (!bolIsExistHolding && intTransferQ < 0)
                                bolIsExistHolding = true;
                        }
                    }
                    #region Giữ hàng lại cửa hàng (Nhận về)
                    if (intHoldingQuantity > 0 && bolIsExistHolding)
                    {
                        decimal decTotalHolding = 0;
                        foreach (DataRow r in rowsProduct)
                        {
                            string strStoreName = r["STORENAME"].ToString().Trim();
                            if (dtbResource.Columns.Contains(strStoreName))
                            {
                                decimal intTransferQ = 0;
                                decimal.TryParse(row["TransferQuantity-" + strStoreName].ToString(), out intTransferQ);
                                if (intTransferQ >= 0)
                                    continue;
                                if (intHoldingQuantity > Math.Abs(intTransferQ))
                                {
                                    decTotalHolding += Math.Abs(intTransferQ);
                                    intTransferQ = 0;
                                }
                                else
                                {
                                    decTotalHolding += intHoldingQuantity;
                                    intTransferQ = Math.Abs(intTransferQ) - intHoldingQuantity;
                                }
                                row["TransferQuantity-" + strStoreName] = -intTransferQ;
                            }
                        }
                        if (decTotalHolding > 0)
                        {
                            bool bolIsFinish = false;
                            while (decTotalHolding > 0 && !bolIsFinish)
                            {
                                int intLoop = 0;
                                foreach (DataRow r in rowsProduct)
                                {
                                    string strStoreName = r["STORENAME"].ToString().Trim();
                                    if (dtbResource.Columns.Contains(strStoreName))
                                    {
                                        decimal intTransferQ = 0;
                                        decimal.TryParse(row["TransferQuantity-" + strStoreName].ToString(), out intTransferQ);
                                        if (intTransferQ > 1)
                                        {
                                            intTransferQ--;
                                            decTotalHolding--;
                                            intLoop--;
                                        }
                                        else
                                            intLoop++;
                                        row["TransferQuantity-" + strStoreName] = intTransferQ;
                                        if (intLoop == rowsProduct.Length)
                                            bolIsFinish = true;
                                    }
                                }
                            }
                        }
                    }
                    #endregion Giữ hàng lại cửa hàng (Nhận về)
                }
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi chia hàng tự động");
                SystemErrorWS.Insert("Lỗi chia hàng tự động", objExce.ToString(), this.Name + " -> CreateDataTable", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }
        private DataTable CreateAutoStoreChangeData()
        {
            DataTable dtbData = new DataTable();
            dtbData.Columns.Add("PRODUCTID", typeof(string));
            dtbData.Columns.Add("PRODUCTNAME", typeof(string));
            dtbData.Columns.Add("TOTALSALESQUANTITY", typeof(int));
            dtbData.Columns.Add("TOTALQUANTITY", typeof(int));
            dtbData.Columns.Add("CENTERQUANTITY", typeof(int));
            dtbData.Columns.Add("DISTRIBUTEQUANTITY", typeof(int));
            dtbData.Columns.Add("CENTERSTOREID", typeof(string));
            dtbData.Columns.Add("STOREID", typeof(string));
            dtbData.Columns.Add("QUANTITY", typeof(string));
            dtbData.Columns.Add("TRANSFERQUANTITY", typeof(int));
            return dtbData;
        }

        //private void UpdateDistributeQuantity()
        //{
        //    for (int i = 0; i < grdViewData.RowCount; i++)
        //    {
        //        DataRow row = (DataRow)grdViewData.GetDataRow(i);
        //        DataRow[] lstRow = dtbAutoDistribute_Main.Select("PRODUCTID = '" + row["PRODUCTID"].ToString().Trim() + "' AND CENTERSTOREID = " + row["CENTERSTOREID"].ToString().Trim());
        //        if (lstRow.Length > 0)
        //        {
        //            foreach (DataRow rowItem in lstRow)
        //                rowItem["DISTRIBUTEQUANTITY"] = row["DISTRIBUTEQUANTITY"];
        //        }
        //    }
        //}

        private DataTable PrepareData()
        {
            DataTable dtbDataAutoStoreChange = CreateAutoStoreChangeData();
            for (int i = 0; i < grdViewData.RowCount; i++)
            {
                DataRow row = (DataRow)grdViewData.GetDataRow(i);
                for (int j = grdViewData.Columns["CENTERSTOREID"].VisibleIndex + 1; j < grdViewData.Columns.Count - 3; j += 4)
                {
                    DataRow r = dtbDataAutoStoreChange.NewRow();
                    r["PRODUCTID"] = row["PRODUCTID"];
                    r["PRODUCTNAME"] = row["PRODUCTNAME"];
                    r["TOTALSALESQUANTITY"] = row["TOTALSALESQUANTITY"];
                    r["TOTALQUANTITY"] = row["TOTALQUANTITY"];
                    r["CENTERQUANTITY"] = row["CENTERQUANTITY"];
                    r["CENTERSTOREID"] = row["CENTERSTOREID"];
                    r["DISTRIBUTEQUANTITY"] = row["DISTRIBUTEQUANTITY"];
                    r["STOREID"] = row[j];
                    r["QUANTITY"] = row[j + 1];
                    r["TRANSFERQUANTITY"] = row[j + 3];
                    dtbDataAutoStoreChange.Rows.Add(r);
                }
            }
            return dtbDataAutoStoreChange;
        }
        #endregion

        #region events
        private void cboMainGroup_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cboProduct.Enabled = cboMainGroup.MainGroupID > 0;
            cboSubGroup.InitControl(true, cboMainGroup.MainGroupID);
            cboBrand.InitControl(true, cboMainGroup.MainGroupID);
            cboSubGroup_SelectionChangeCommitted(null, null);
        }

        private void cboSubGroup_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cboProduct.InitControl(cboMainGroup.MainGroupID, cboSubGroup.SubGroupIDList, cboBrand.BrandIDList);
        }

        private void cboBrand_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cboSubGroup_SelectionChangeCommitted(null, null);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            if (!ValidateData())
            {
                btnSearch.Enabled = true;
                return;
            }
            int intCenterStoreID = cboCenterStore.StoreID;
            string strStoreIDList = cboStore.StoreIDList;
            int intMainGroupID = cboMainGroup.MainGroupID;
            string strSubGroupIDList = cboSubGroup.SubGroupIDList;
            string strBrandIDList = cboBrand.BrandIDList;
            string strProductIDList = cboProduct.ProductIDList;
            int intIsShowProduct = cboIsShowProduct.SelectedIndex - 1;
            int intIsCheckRealInput = chkIsCheckRealInput.Checked ? 1 : 0;
            DateTime dtFromDate = dtpFromDate.Value;
            DateTime dtToDate = dtpToDate.Value;
            object[] objKeywords = new object[]
                {
                    "@CenterStoreID", intCenterStoreID,
                    "@MaingroupID", intMainGroupID,
                    "@SubGroupIDList", strSubGroupIDList,
                    "@BrandIDList", strBrandIDList,
                    "@ProductIDList", strProductIDList,
                    "@IsShowProduct", intIsShowProduct,
                    "@IsCheckRealInput", intIsCheckRealInput
                };
            object[] objKeywords1 = new object[]
                {
                    "@StoreIDList", strStoreIDList,
                    "@MaingroupID", intMainGroupID,
                    "@SubGroupIDList", strSubGroupIDList,
                    "@BrandIDList", strBrandIDList,
                    "@ProductIDList", strProductIDList,
                    "@IsShowProduct", intIsShowProduct,
                    "@IsCheckRealInput", intIsCheckRealInput,
                    "@FromDate", dtFromDate,
                    "@ToDate", dtToDate
                };
            ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
            dtbAutoDistribute_Main = objPLCReportDataSource.GetDataSource("INV_AUTODISTRIBUTE_MAIN", objKeywords);
            dtbAutoDistribute = objPLCReportDataSource.GetDataSource("INV_AUTODISTRIBUTE", objKeywords1);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                btnSearch.Enabled = true;
                btnReceive.Enabled = false;
                btnTransfer.Enabled = false;
            }
            else
            {
                dtbAutoDistribute_Main.PrimaryKey = new DataColumn[] { dtbAutoDistribute_Main.Columns["PRODUCTID"] };
                if (CreateDataTable(dtbAutoDistribute_Main, dtbAutoDistribute))
                {
                    grdData.DataSource = dtbResource;
                    FormatGrid();
                }
                btnSearch.Enabled = true;
                btnReceive.Enabled = btnTransfer.Enabled = grdViewData.RowCount > 0;
                intRowHandle = -1;
                strFeildName = string.Empty;
            }
        }

        private void frmAutoDistributeInventory_Load(object sender, EventArgs e)
        {
            InitControl();
            LoadCombobox();
        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            DataTable dtbDataAutoStoreChange = PrepareData();
            DataTable dtbData = Library.AppCore.DataTableClass.Select(dtbDataAutoStoreChange, "TRANSFERQUANTITY >0");
            if (dtbData.Rows.Count == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không có sản phẩm nào có thể chia.");
                return;
            }
            int intStoreID = cboCenterStore.StoreID;
            string strStoreIDList = cboStore.StoreIDList;
            int intMainGroupID = cboMainGroup.MainGroupID;
            bool bolIsReceive = false;
            frmAutoStoreChangeDetail frm = new frmAutoStoreChangeDetail();
            frm.StoreID = intStoreID;
            frm.StoreIDList = strStoreIDList;
            frm.MainGroupIDFromAutoDistribute = intMainGroupID;
            frm.IsReceive = bolIsReceive;
            frm.ISCallFromAutoDistribute = true;
            frm.DtbAutoDistribute = dtbData;
            frm.ShowDialog();
        }


        private void btnReceive_Click(object sender, EventArgs e)
        {
            DataTable dtbDataAutoStoreChange = PrepareData();
            DataTable dtbData = Library.AppCore.DataTableClass.Select(dtbDataAutoStoreChange, "TRANSFERQUANTITY <0");
            if (dtbData.Rows.Count == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không có sản phẩm nào cần nhận.");
                return;
            }
            int intStoreID = cboCenterStore.StoreID;
            string strStoreIDList = cboStore.StoreIDList;
            int intMainGroupID = cboMainGroup.MainGroupID;
            bool bolIsReceive = true;
            frmAutoStoreChangeDetail frm = new frmAutoStoreChangeDetail();
            frm.StoreID = intStoreID;
            frm.StoreIDList = strStoreIDList;
            frm.MainGroupIDFromAutoDistribute = intMainGroupID;
            frm.IsReceive = bolIsReceive;
            frm.ISCallFromAutoDistribute = true;
            frm.DtbAutoDistribute = dtbData;
            frm.ShowDialog();
        }

        private void repDistributeQuantity_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            DataRow row = (DataRow)grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
            int intValue = 0;
            int.TryParse(e.NewValue.ToString(), out intValue);
            if (intValue < 0)
                intDistributeQuantity = 0;
            else if (intValue > Convert.ToInt32(row["CENTERQUANTITY"]))
            {
                if (Convert.ToInt32(row["CENTERQUANTITY"]) < 0)
                    intDistributeQuantity = 0;
                else
                    intDistributeQuantity = Convert.ToInt32(row["CENTERQUANTITY"]);
            }
            else
                intDistributeQuantity = intValue;
            intRowHandle = grdViewData.FocusedRowHandle;
        }

        private void repTransferQuantity_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            DataRow row = (DataRow)grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
            intRowHandle = grdViewData.FocusedRowHandle;
            GridColumn col = grdViewData.FocusedColumn;
            string strQuantityFeildName = string.Empty;
            strFeildName = strQuantityFeildName = col.FieldName;
            strQuantityFeildName = strQuantityFeildName.Replace("TransferQuantity", "Quantity");
            int intValue = 0;
            int.TryParse(e.NewValue.ToString(), out intValue);
            if (intValue > 0)
            {
                int intTotal = 0;
                intTotal = Convert.ToInt32(row["DISTRIBUTEQUANTITY"]) + Convert.ToInt32(row["TOTALQUANTITY"]);
                if (intValue > intTotal)
                    intTransferQuantity = intTotal;
                else
                    intTransferQuantity = intValue;
            }
            else
            {
                int intQuantity = Convert.ToInt32(row[strQuantityFeildName]);
                if (intQuantity < 0)
                    intTransferQuantity = 0;
                else if (-intValue > intQuantity)
                    intTransferQuantity = -intQuantity;
                else
                    intTransferQuantity = intValue;
            }
        }
        private void repDistributeQuantity_ParseEditValue(object sender, DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs e)
        {
            if (grdViewData.FocusedRowHandle == intRowHandle)
            {
                DataRow row = (DataRow)grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
                row["DISTRIBUTEQUANTITY"] = intDistributeQuantity;
                int intTotalSalesQuantity = 0;
                int.TryParse(row["TOTALSALESQUANTITY"].ToString(), out intTotalSalesQuantity);
                {
                    for (int i = grdViewData.Columns["CENTERSTOREID"].VisibleIndex + 1; i < grdViewData.Columns.Count - 3; i += 4)
                    {
                        int intQuantity = 0;
                        int.TryParse(row[i + 1].ToString(), out intQuantity);
                        if (intTotalSalesQuantity == 0)
                            row[i + 3] = -intQuantity;
                        else
                        {
                            int intTotalQuantity = 0;
                            int.TryParse(row["TOTALQUANTITY"].ToString(), out intTotalQuantity);
                            int intSalesQuantity = 0;
                            int.TryParse(row[i + 2].ToString(), out intSalesQuantity);
                            int intTransferQuantity = 0;
                            intTransferQuantity = Convert.ToInt32(((decimal)intSalesQuantity / (decimal)intTotalSalesQuantity) * (intDistributeQuantity + intTotalQuantity) - intQuantity);
                            row[i + 3] = intTransferQuantity;
                        }
                    }
                }
                grdData.RefreshDataSource();
                grdViewData.PostEditor();
            }
        }

        private void repTransferQuantity_ParseEditValue(object sender, DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs e)
        {
            if (grdViewData.FocusedRowHandle == intRowHandle && !string.IsNullOrEmpty(strFeildName))
            {
                DataRow row = (DataRow)grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
                row[strFeildName] = intTransferQuantity;
                grdData.RefreshDataSource();
                grdViewData.PostEditor();
            }

        }

        private void cboCenterStore_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            objStoreFilter.IsCheckPermission = false;
            objStoreFilter.OtherCondition = "STOREID <> " + cboCenterStore.StoreID;
            cboStore.InitControl(true, objStoreFilter);
        }

        private void chkShortView_CheckedChanged(object sender, EventArgs e)
        {
            if (grdData.DataSource != null)
                FormatGrid();
        }

        private void grdViewData_CustomRowFilter(object sender, DevExpress.XtraGrid.Views.Base.RowFilterEventArgs e)
        {
            GridView view = sender as GridView;
            DataView dv = view.DataSource as DataView;
            bool bolIsVisible = false;
            if ((Int32)dv[e.ListSourceRow]["CENTERQUANTITY"] != 0 || (Int32)dv[e.ListSourceRow]["DISTRIBUTEQUANTITY"] != 0)
                bolIsVisible = true;
            string strStoreIDList = cboStore.Text;
            strStoreIDList = strStoreIDList.Replace("] [", ";");
            strStoreIDList = strStoreIDList.Replace("[", "");
            strStoreIDList = strStoreIDList.Replace("]", "");
            List<string> lstStoreID = strStoreIDList.Split(';').ToList();
            foreach (string strStoreID in lstStoreID)
            {
                if ((Int32)dv[e.ListSourceRow]["Quantity-" + strStoreID.Trim()] != 0 || (Int32)dv[e.ListSourceRow]["SalesQuantity-" + strStoreID.Trim()] != 0 || (Int32)dv[e.ListSourceRow]["TransferQuantity-" + strStoreID.Trim()] != 0)
                    bolIsVisible = true;
            }
            e.Visible = bolIsVisible;
            e.Handled = true;
        }
        #endregion
    }
}
