﻿using ERP.Inventory.PLC.StoreChangeCommand.WSStoreChangeCommand;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    public partial class frmAutoStoreChangeVirtualEmulator : Form
    {
        private List<AutoStoreChangeContext> ListAutoStoreChangeContext { get; set; }
        public frmAutoStoreChangeVirtualEmulator(List<AutoStoreChangeContext> list)
        {
            InitializeComponent();
            ListAutoStoreChangeContext = list;
            Shown += FrmAutoStoreChangeVirtualEmulator_Shown;
        }
        private void FrmAutoStoreChangeVirtualEmulator_Shown(object sender, EventArgs e)
        {
            if (ListAutoStoreChangeContext != null && ListAutoStoreChangeContext.Any())
            {
                DataTable dtb = new DataTable();
                dtb.Columns.AddRange(new DataColumn[]{
                new DataColumn(){ ColumnName = "PRODUCTID" },
                new DataColumn(){ ColumnName = "PRODUCTNAME" },
                new DataColumn(){ ColumnName = "IMEI" },
                new DataColumn(){ ColumnName = "INSTOCKSTATUSNAME" },
                new DataColumn(){ ColumnName = "FROMSTORENAME" },
                new DataColumn(){ ColumnName = "TOSTORENAME" },
                new DataColumn(){ ColumnName = "FROM_CURRENTQUANTITY" },
                new DataColumn(){ ColumnName = "FROM_VIRTUALQUANTITY" },
                new DataColumn(){ ColumnName = "TO_CURRENTQUANTITY" },
                new DataColumn(){ ColumnName = "TO_VIRTUALQUANTITY" },
                new DataColumn(){ ColumnName = "QUANTITY" }, });
                try
                {
                    psVirtualEmulator.Maximum = ListAutoStoreChangeContext.Sum(d => d.Detail.Count());
                    sttFooter.Visible = true;
                    Application.DoEvents();

                    ListAutoStoreChangeContext.Where(d => d.Detail.Any()).ToList().ForEach(d =>
                    {
                        d.Detail.ToList().ForEach(f =>
                        {
                            DataRow r = dtb.NewRow();
                            r.ItemArray = new object[] {
                           f.ProductID,
                           f.ProductName,
                           f.IMEI,
                           d.ImportProductStateName,
                           d.FromStoreName,
                           d.ToStoreName,

                           f.FromInStockQuantity, //FROM_CURRENTQUANTITY
                           f.FromInStockQuantity - ListAutoStoreChangeContext.Where(g=>g.FromStore == d.FromStore && g.ProductState == d.ProductState)
                           .Sum(g=>g.Detail.Where(h=> h.ProductID == f.ProductID).Sum(h=>h.AutoQuantity)), //FROM_VIRTUALQUANTITY

                           f.ToInStockQuantity, //TO_CURRENTQUANTITY
                           f.ToInStockQuantity + ListAutoStoreChangeContext.Where(g=>g.ToStore == d.ToStore && g.ProductState == d.ProductState)
                           .Sum(g=>g.Detail.Where(h=> h.ProductID == f.ProductID).Sum(h=>h.AutoQuantity)), //TO_VIRTUALQUANTITY
                           f.AutoQuantity };
                            dtb.Rows.Add(r);
                            psVirtualEmulator.Value += 1;
                        });
                        Application.DoEvents();
                    });
                }
                finally
                {
                    psVirtualEmulator.Value = 0;
                    sttFooter.Visible = false;
                }
                grdData.DataSource = dtb;
            }

        }
    }
}
