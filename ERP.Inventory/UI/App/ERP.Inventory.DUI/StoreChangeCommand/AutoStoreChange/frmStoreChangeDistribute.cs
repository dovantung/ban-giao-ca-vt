﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System.Threading;
using System.Collections;
using System.Text.RegularExpressions;
using Library.AppCore.Forms;
using System.Diagnostics;
using Library.AppCore.Constant;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraBars;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ERP.MasterData.PLC.MD.WSStoreChangeType;
using Library.AppCore.DataSource;

namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    public partial class frmStoreChangeDistribute : Form
    {
        #region CONSTANT and VARIABLES

        private const string DisplayText = "DisplayText";
        private const string BehideValue = "BehideValue";
        private const int DEFAULT_SHOWPRODUCT = 1;
        private const ETransferType DEFAULT_TRANSFERTYPE = ETransferType.Send;
        private const bool DEFAULT_ALLOWOLDCODE = false;
        private const bool DEFAULT_ISINCLUDEDROAMING = false;
        private const string MD_STORE_OLDSTORE_SRH = "MD_STORE_OLDSTORE_SRH";
        private bool isExcelImported = false;
        private MasterData.PLC.MD.PLCStoreChangeType plcStoreChangeType { get; set; }
        private MasterData.PLC.MD.PLCStoreChangeOrderType plStoreChangeOrderType { get; set; }
        private PageDataInfo InsertPageDataInfo { get; set; }

        private int intOldIndex { get; set; }
        /// <summary>
        /// Bao gồm hàng đi đường
        /// </summary>
        public bool IsIncludedRoaming
        { get { return chkIsIncludedRoaming.Checked; } private set { chkIsIncludedRoaming.Checked = value; } }

        /// <summary>
        /// Sử dụng mã cũ
        /// </summary>
        public bool IsAllowOldCode
        {
            get
            {
                return chkIsAllowOldCode.Checked;
            }
            private set
            {
                chkIsAllowOldCode.Checked = value;
            }
        }

        private bool IsAutoCreateOrder { get; set; }

        /// <summary>
        /// Số lượng chuyển
        /// </summary>
        public int TransferQuantity
        { get { int temp = 0; int.TryParse(txtQuantity.Text, out temp); return temp; } set { txtQuantity.Text = value.ToString(); } }

        /// <summary>
        /// Mã lệnh chuyển (PM_STORECHANGEORDER.STORECHANGECOMMANDID)
        /// </summary>
        public string StoreChangeCommandID
        { get { return txtLeadCommandID.Text; } set { txtLeadCommandID.Text = value; } }

        private DataTable DataSourceStore { get; set; }
        private DataTable DataSourceProduct { get; set; }
        private DataTable MainDataSource { get; set; }
        private DataTable DataSourceMainGroup { get; set; }
        private DataTable DataSourceProductState { get; set; }
        private DataTable DataSourceMainGroupStore { get; set; }
        List<ERP.Inventory.PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> ListAutoStoreChangeContext { get; set; }
        Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        private DataTable DataSourceOldProduct { get; set; }
        private DataTable DataSourceOldStore { get; set; }
        public StringBuilder ProductStateComment { get; set; }
        private DataTable DataSourceStoreMainGroup { get; set; }
        private PLC.StoreChangeCommand.PLCStoreChangeCommand objPLCStoreChangeCommand { get; set; }
        private string LeadCommandID { get { return txtLeadCommandID.Text; } set { txtLeadCommandID.Text = value; } }
        private int StoreChangeCommandTypeID
        {
            get
            {
                return AppConfig.GetIntConfigValue("AUTOSTORECHANGE_STORECHANGECOMMANDTYPEID");
            }
        }
        #endregion

        #region Contructos
        public frmStoreChangeDistribute()
        {
            InitializeComponent();
            plcStoreChangeType = new MasterData.PLC.MD.PLCStoreChangeType();
            plStoreChangeOrderType = new MasterData.PLC.MD.PLCStoreChangeOrderType();
            objPLCStoreChangeCommand = new PLC.StoreChangeCommand.PLCStoreChangeCommand();
            ProductStateComment = new StringBuilder();
            ListAutoStoreChangeContext = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext>();
            this.Load += new EventHandler(frmStoreChangeDistribute_Load);
            this.Shown += FrmStoreChangeDistribute_Shown;
        }

        #endregion

        #region Form Events
        private void grdViewData_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                if (e.Column != null && !string.IsNullOrEmpty(e.Column.FieldName)
                    && e.Column.FieldName == "ISSELECTED" || e.Column.FieldName == "ISURGENT")
                {
                    var row = ((DataRowView)grdViewData.GetRow(e.RowHandle)).Row;
                    if (row != null && row.ItemArray.Any())
                    {
                        bool isSelected = Convert.ToBoolean(row["ISSELECTED"]);
                        bool isUrgent = Convert.ToBoolean(row["ISURGENT"]);
                        int fromStoreId = Convert.ToInt32(row["FROMSTOREID"]);
                        int toStoreID = Convert.ToInt32(row["TOSTOREID"]);
                        string productID = row["PRODUCTID"].GetString();
                        string imei = row["IMEI"].GetString();
                        int instockStatusID = Convert.ToInt32(row["INSTOCKSTATUSID"]);
                        ListAutoStoreChangeContext.Where(d => d.FromStore == fromStoreId && d.ToStore == toStoreID
                        && d.ProductState == instockStatusID).ToList().ForEach(d =>
                        {
                            if (d.Detail != null && d.Detail.Any())
                            {
                                var detail = d.Detail.Where(f => f.ProductID == productID &&
                                (string.IsNullOrEmpty(f.IMEI) || (!string.IsNullOrEmpty(f.IMEI) && f.IMEI == imei))).FirstOrDefault();
                                detail.IsSelected = isSelected;
                                detail.IsUrgent = isUrgent;
                            }
                        });
                    }
                }
            }
            catch { }
        }

        private void grdViewData_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Control && e.KeyCode == Keys.C)
                {
                    Clipboard.SetText(grdViewData.GetFocusedDisplayText());
                    e.Handled = true;
                }
            }
            catch { }
        }
        private void frmStoreChangeDistribute_Load(object sender, EventArgs e)
        {
            InitControlsAndEvents();
        }
        private void FrmStoreChangeDistribute_Shown(object sender, EventArgs e)
        {
            Rectangle screen = Screen.PrimaryScreen.WorkingArea;
            Location = new Point((screen.Width - 1050) / 2, (screen.Height - screen.Height) / 2);
            Size = new Size(1050, screen.Height);
            grdData.Dock = DockStyle.Fill;
            Application.DoEvents();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (ListAutoStoreChangeContext != null && ListAutoStoreChangeContext.Any() && isExcelImported)
                    if (MessageBox.Show(this, "Trên lưới đã có dữ liệu import từ excel.\nChương trình sẽ xóa dữ liệu cũ và thay thế bằng dữ liệu mới", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                        return;
                    else
                        ResetDataSource();
                isExcelImported = false;
                if (!IsValidRequireFieldBeforeAdding())
                    return;

                BeginSuspendForm();
                ETransferType transferType = (ETransferType)Convert.ToInt32(cboType.SelectedValue);
                List<int> listFromStore = new List<int>();
                List<int> listToStore = new List<int>();
                if (transferType == ETransferType.Send)
                {
                    listFromStore.Add(cboFromStoreID.StoreID);
                    listToStore = GetSelectedStoreID();
                }
                else
                {
                    listFromStore = GetSelectedStoreID();
                    listToStore.Add(cboFromStoreID.StoreID);
                }
                GenerateData(listFromStore, listToStore, GetSelectedProductStates(), GetSelectedProductID(), Convert.ToDecimal(txtQuantity.Text));
                ShowImportResult(false);
            }
            catch (Exception ex) { throw ex; }
            finally { EndSuspendForm(); }
        }

        /// <summary>
        /// Xem tồn kho giả lập
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnVirtualEmulator_Click(object sender, EventArgs e)
        {
            new frmAutoStoreChangeVirtualEmulator(GetSelectedItem()).ShowDialog();
        }

        /// <summary>
        /// Tạo lệnh yêu cầu chuyển kho
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreateStoreChangeCommand_Click(object sender, EventArgs e)
        {
            List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> listData = GetSelectedItem();
            if (!listData.Any() || grdData.DataSource == null)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không có dữ liệu để tạo lệnh yêu cầu chuyển kho");
                return;
            }
            else if (!((DataTable)grdData.DataSource).AsEnumerable().Any(d => Convert.ToBoolean(d["ISSELECTED"]) == true))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn ít nhất một mặt hàng cần chuyển");
                return;
            }
            if (IsAutoCreateOrder)
            {
                frmSelectInfo frm = new frmSelectInfo();
                frm.STORECHANGECOMMANDTYPEID = StoreChangeCommandTypeID;
                frm.ShowDialog();
                if (frm.IsAccept)
                {
                    ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = frm.StoreChangeOrderType;
                    List<int> listConsignMentType = GetConsignmentType(frm.StoreChangeOrderType.StoreChangeOrderTypeID);
                    int storeChangeTypeID = Convert.ToInt32(objStoreChangeOrderType.StoreChangeTypeID);
                    var objStoreChangeType = new MasterData.PLC.MD.PLCStoreChangeType().LoadInfo(storeChangeTypeID);
                    listData.ForEach(item =>
                    {
                        item.StoreChangeCommandID = LeadCommandID;
                        item.StoreChangeCommandType = StoreChangeCommandTypeID;
                        item.StoreChangeTypeID = storeChangeTypeID;
                        item.StoreChangeOrderTypeID = objStoreChangeOrderType.StoreChangeOrderTypeID;
                        item.IsValidInput = CheckInputCreateOrder(objStoreChangeType, item.FromStore, item.ToStore);
                        item.Detail.ToList().ForEach(f =>
                        {
                            if (DataSourceProduct != null && DataSourceProduct.Rows.Count > 0)
                                f.IsValidConsignmentType = listConsignMentType.Any(g => DataSourceProduct.Rows.Find(f.ProductID) != null && g == Convert.ToInt32(DataSourceProduct.Rows.Find(f.ProductID)["CONSIGNMENTTYPE"]));
                        });
                    });


                    frmCreateStoreChangeOrder frmCreateStoreChangeOrder = new frmCreateStoreChangeOrder(listData.Where(d => d.IsValidInput).ToList());
                    frmCreateStoreChangeOrder.StoreChangeOrderType = objStoreChangeOrderType;
                    frmCreateStoreChangeOrder.StoreChangeCommandTypeID = StoreChangeCommandTypeID;
                    frmCreateStoreChangeOrder.CommandID = LeadCommandID;
                    frmCreateStoreChangeOrder.ShowDialog();
                    if (frmCreateStoreChangeOrder.IsAccept)
                    {
                        ResetDataSource(true);
                    }
                }
            }
            else
            {
                MessageBox.Show(this, "Cấu hình không tự động tạo đơn hàng đang tạm thời ngừng hoạt động!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //listData.ForEach(item =>
                //{
                //    item.StoreChangeCommandID = LeadCommandID;
                //    item.StoreChangeCommandType = 1;
                //    item.IsValidInput = true;
                //    item.IsReviewed = false;
                //    item.Detail.ToList().ForEach(f =>
                //    {
                //        f.IsValidConsignmentType = true;
                //    });
                //    if (item.Detail.Any(d => d.IsUrgent && d.IsSelected && d.IsValidConsignmentType))
                //        item.IsUrgent = true;
                //});
                //MakeInsert(listData);
            }
        }

        private void MakeInsert(List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> list)
        {
            BackgroundWorker bwk = new BackgroundWorker();
            int succeedCounter = 0;
            psProgress.Value = 0;
            psProgress.Maximum = 100;
            lblPs.Text = "Đang xử lý";
            bwk.DoWork += (s, e) =>
            {
                BeginSuspendForm();
                try
                {
                    this.InsertPageDataInfo = new PageDataInfo(3);
                    InsertPageDataInfo.TotaPage = Convert.ToInt32(Math.Ceiling((double)list.Count / InsertPageDataInfo.PageSize));
                    this.sttFooter.Visible = true;
                    while (InsertPageDataInfo.PageIndex < InsertPageDataInfo.TotaPage)
                    {
                        try
                        {
                            var listInsert = list.Skip(Convert.ToInt32(InsertPageDataInfo.PageIndex * InsertPageDataInfo.PageSize))
                                             .Take(Convert.ToInt32(InsertPageDataInfo.PageSize)).ToList();
                            //var resultMessage = objPLCStoreChangeCommand.InsertAutoStoreChange(listInsert, SystemConfig.intDefaultStoreID, true);
                            //if (!resultMessage.IsError)
                            //{
                            //    listInsert.ForEach(d => d.IsSent = true);
                            //    succeedCounter += listInsert.Count;
                            //}
                            this.InsertPageDataInfo.PageIndex++;
                            this.Invoke(new MethodInvoker(() =>
                            {
                                this.psProgress.Value = Convert.ToInt32(this.InsertPageDataInfo.GetPercent());
                            }));
                        }
                        catch (Exception ex)
                        {
                            this.InsertPageDataInfo.PageIndex++;
                            SystemErrorWS.Insert("Lỗi trong quá trình tạo lệnh chuyển kho", ex.Message,
                            "ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange.frmStoreChangeDistribute");
                        }
                    }
                }
                finally
                {
                    if (InsertPageDataInfo.TotaPage != 0)
                    {
                        StringBuilder str = new StringBuilder();
                        str.AppendLine("Tổng số phiếu: " + list.Count);
                        str.AppendLine("Đã tạo thành công: " + succeedCounter + "/" + list.Count);
                        MessageBox.Show(this, str.ToString(), "Thông tin tác vụ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            };
            bwk.RunWorkerCompleted += (s, e) =>
            {
                this.Invoke(new MethodInvoker(() =>
                {
                    sttFooter.Visible = false;
                    ResetDataSource(true);
                    EndSuspendForm();
                }));
            };

            bwk.RunWorkerAsync();
        }
        private void cboType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if ((ETransferType)Convert.ToInt32(cboType.SelectedIndex) == ETransferType.Send)
            {
                lblStore1.Text = "Từ kho:";
                lblStore2.Text = "Đến kho:";
            }
            else
            {
                lblStore1.Text = "Đến kho:";
                lblStore2.Text = "Từ kho:";
            }
        }

        /// <summary>
        /// Xuất excel template
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportClicked(object sender, ItemClickEventArgs e)
        {
            switch (e.Item.Id)
            {
                case (int)EActionType.IMEI:
                    {
                        if (IsValidForExportIMEITemplate())
                            ExportIMEITemplate();
                        break;
                    }
                case (int)EActionType.Quantity:
                    {
                        if (IsValidForExportQuantityTemplate())
                            ExportQuantityTemplate();
                        break;
                    }
                case (int)EActionType.MultipleStore:
                    {
                        if (IsValidForExportMultiTemplate())
                            ExportMultiTemplate();
                        break;
                    }
                default: { break; }
            }
        }

        /// <summary>
        /// Import từ file excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportClicked(object sender, ItemClickEventArgs e)
        {
            if (ListAutoStoreChangeContext != null && ListAutoStoreChangeContext.Any())
                if (MessageBox.Show(this, "Trên lưới đã có dữ liệu.\nChương trình sẽ xóa dữ liệu cũ và thay thế bằng dữ liệu mới", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    return;
            try
            {
                ResetDataSource();
                switch (e.Item.Id)
                {
                    case (int)EActionType.IMEI:
                        {
                            if (IsValidForPreImportIMEI())
                                ImportIMEI();
                            break;
                        }
                    case (int)EActionType.Quantity:
                        {
                            if (IsValidForPreImportQuantity())
                                ImportQuantity();
                            break;
                        }
                    case (int)EActionType.MultipleStore:
                        {
                            if (IsValidForPreImportMulti())
                                ImportMulti();
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                SystemErrorWS.Insert("Lỗi import yêu cầu xuất chuyển kho", ex, "ERP.Inventory.DUI");
                MessageBoxObject.ShowErrorMessage(this, "Lỗi trong quá trình xử lý import yêu cầu xuất chuyển kho");
            }
            finally
            {
                isExcelImported = true;
                EndSuspendForm();
            }
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboType.SelectedIndex != intOldIndex)
            {
                if (ListAutoStoreChangeContext.Any())
                {
                    if (MessageBox.Show(this, "Trên lưới đã có dữ liệu.\nChương trình sẽ xóa dữ liệu cũ và thay thế bằng dữ liệu mới", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    {
                        cboType.SelectedIndex = intOldIndex;
                        return;
                    }
                    ResetDataSource();
                }
            }
            intOldIndex = cboType.SelectedIndex;
        }

        private void mnuItemSelectAll_Click(object sender, EventArgs e)
        {
            if (grdViewData.RowCount == 0)
                return;

            for (int i = 0; i < grdViewData.RowCount; i++)
            {
                grdViewData.SetRowCellValue(i, colIsSelected, true);
            }
        }

        private void mnuItemUnSelectAll_Click(object sender, EventArgs e)
        {
            if (grdViewData.RowCount == 0)
                return;

            for (int i = 0; i < grdViewData.RowCount; i++)
            {
                grdViewData.SetRowCellValue(i, colIsSelected, false);
            }
        }

        private void mnuExportExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Excel Document|*.xlsx";
            if (saveFileDialog.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                grdData.ExportToXlsx(saveFileDialog.FileName);
                if (MessageBox.Show("Bạn có muốn mở file?", "Mở file", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    == DialogResult.Yes)
                {
                    Process.Start(saveFileDialog.FileName);
                }
            }
        }

        private void mnuGrid_Opening(object sender, CancelEventArgs e)
        {
            mnuDeleteRow.Visible = false;
            mnuDeleteRow.Enabled = mnuItemUnSelectAll.Enabled = mnuExportExcel.Enabled = mnuItemSelectAll.Enabled = grdViewData.RowCount != 0;
        }
        #endregion

        #region Extension Methods
        /// <summary>
        /// Khởi tạo dữ liệu và sự kiện cho các control 
        /// </summary>
        private void InitControlsAndEvents()
        {
            //Hình thức chuyển
            cboType.DisplayMember = DisplayText;
            cboType.ValueMember = BehideValue;
            cboType.DataSource = StoreChangeDistributeHelper.EnumToList<ETransferType>();
            cboType.SelectedValue = (int)DEFAULT_TRANSFERTYPE;
            intOldIndex = (int)DEFAULT_TRANSFERTYPE;


            //Trạng thái sản phẩm
            DataSourceProductState =  /*new Report.PLC.PLCReportDataSource().GetDataSource("PRODUCTSTATUS_GETVIEW");*/
                Library.AppCore.DataSource.GetDataSource.GetProductStatusView().Copy();
            DataSourceProductState.PrimaryKey = new DataColumn[] { DataSourceProductState.Columns["PRODUCTSTATUSID"] };
            cboProductState.InitControl(true, DataSourceProductState.Copy(),
                "PRODUCTSTATUSID", "PRODUCTSTATUSNAME", "--Tất cả--");
            if (DataSourceProductState != null && DataSourceProductState.Rows.Count > 0)
            {
                DataSourceProductState.AsEnumerable().ToList().ForEach(item =>
                {
                    ProductStateComment.AppendLine(item["PRODUCTSTATUSID"].GetString() + ":" + item["PRODUCTSTATUSNAME"].GetString());
                });
            }

            //Nút xuất excel
            BarButtonItem btnExportIMEITempalte = new BarButtonItem() { Caption = EActionType.IMEI.ToString().Translate(), Id = (int)EActionType.IMEI };
            BarButtonItem btnExportQuantityTemplate = new BarButtonItem() { Caption = EActionType.Quantity.ToString().Translate(), Id = (int)EActionType.Quantity };
            BarButtonItem btnExportMultiTemplate = new BarButtonItem() { Caption = EActionType.MultipleStore.ToString().Translate(), Id = (int)EActionType.MultipleStore };
            btnExportIMEITempalte.ItemClick += ExportClicked;
            btnExportQuantityTemplate.ItemClick += ExportClicked;
            btnExportMultiTemplate.ItemClick += ExportClicked;
            pMnuExport.AddItems(new BarButtonItem[] { btnExportIMEITempalte, btnExportQuantityTemplate, btnExportMultiTemplate });

            //Nút nhập Excel
            BarButtonItem btnImportIMEITempalte = new BarButtonItem() { Caption = EActionType.IMEI.ToString().Translate(), Id = (int)EActionType.IMEI };
            BarButtonItem btnImportQuantityTemplate = new BarButtonItem() { Caption = EActionType.Quantity.ToString().Translate(), Id = (int)EActionType.Quantity };
            BarButtonItem btnImportMultiTemplate = new BarButtonItem() { Caption = EActionType.MultipleStore.ToString().Translate(), Id = (int)EActionType.MultipleStore };
            btnImportIMEITempalte.ItemClick += ImportClicked;
            btnImportQuantityTemplate.ItemClick += ImportClicked;
            btnImportMultiTemplate.ItemClick += ImportClicked;
            pMnuImport.AddItems(new BarButtonItem[] { btnImportIMEITempalte, btnImportQuantityTemplate, btnImportMultiTemplate });


            //Sản phẩm
            cboProductList.InitControl(0,
                string.Empty,
                string.Empty,
                false,
                Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL,
                Library.AppCore.Constant.EnumType.IsServiceType.ALL,
                Library.AppCore.Constant.EnumType.MainGroupPermissionType.VIEWREPORT);

            //Quyền kho _ ngành hàng
            DataSourceMainGroupStore = Library.AppCore.DataSource.GetDataSource.GetStoreMainGroup().Copy();

            //Ngành hàng
            DataSourceMainGroup = Library.AppCore.DataSource.GetDataSource.GetMainGroup();
            cboMainGroupID.InitControl(true, DataSourceMainGroup);
            cboMainGroupID.SelectionChangeCommitted += (sender, @event) =>
            {
                cboProductList.InitControl(cboMainGroupID.MainGroupIDList,
                    string.Empty, string.Empty, false,
                    Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL,
                    Library.AppCore.Constant.EnumType.IsServiceType.ALL,
                    Library.AppCore.Constant.EnumType.MainGroupPermissionType.VIEWREPORT);
            };

            DataSourceStore = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
            if (DataSourceStore != null && DataSourceStore.Rows.Count > 0)
            {
                cboFromStoreID.InitControl(false, DataSourceStore.Copy());
                cboToStoreID.InitControl(true, DataSourceStore.Copy());
            }


            DataSourceProduct = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
            DataSourceProduct.AsEnumerable().ToList().ForEach(row => { row["PRODUCTID"] = row["PRODUCTID"].GetString().Trim(); });

            DataSourceOldProduct = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
            DataSourceOldStore = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_STORE_OLDSTORE_SRH");

            IsAllowOldCode = DEFAULT_ALLOWOLDCODE;
            IsIncludedRoaming = DEFAULT_ISINCLUDEDROAMING;


            DataSourceStoreMainGroup = GetDataSource.GetStoreMainGroup();

            //Mã lệnh tổng
            LeadCommandID = objPLCStoreChangeCommand.GetStoreChangeCommandNewID(SystemConfig.intDefaultStoreID);

            if (StoreChangeCommandTypeID < 0)
            {
                MessageBox.Show(this, "Bạn chưa cấu hình loại lệnh chuyển kho mặc định", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }

            if (Tag != null && !string.IsNullOrWhiteSpace(Tag.ToString()))
            {
                try
                {
                    Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                    if (hstbParam.Contains("IsCanUseOldID"))
                        IsAllowOldCode = hstbParam["IsCanUseOldID"].ToString().Trim() == "1";

                    if (hstbParam.Contains("AutoCreateOrder"))
                    {
                        btnCreateStoreChangeCommand.Text = "Tạo YC chuyển kho";
                        IsAutoCreateOrder = hstbParam["AutoCreateOrder"].ToString().Trim() == "1";
                    }
                }
                catch { }
            }

            btnAdd.Enabled = SystemConfig.objSessionUser.IsPermission("AUTOSTORECHANGE_ADDPRODUCT");

        }
        /// <summary>
        /// Kiểm tra các điều kiện hợp lệ trước khi thực hiện import theo imei
        /// </summary>
        /// <returns></returns>
        private bool IsValidForPreImportIMEI()
        {
            if (cboFromStoreID.StoreID == -1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Bạn chưa chọn kho " +
                    (((ETransferType)Convert.ToInt32(cboType.SelectedValue) == ETransferType.Send) ? "xuất" : "nhập"));
                cboFromStoreID.Focus();
                return false;
            }
            return true;
        }
        /// <summary>
        /// Kiểm tra các điều kiện hợp lệ trước khi thực hiện import theo số lượng
        /// </summary>
        /// <returns></returns>
        private bool IsValidForPreImportQuantity()
        {
            if (cboFromStoreID.StoreID == -1)
            {
                if (Convert.ToInt32(cboType.SelectedValue) == (int)ETransferType.Send)
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho xuất chuyển");
                else MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho nhập");
                cboFromStoreID.Focus();
                return false;
            }

            return true;
        }
        /// <summary>
        /// Kiểm tra các điều kiện hợp lệ trước khi thực hiện import chuyển nhiều kho tới nhiều kho
        /// </summary>
        /// <returns></returns>
        private bool IsValidForPreImportMulti() { return true; }
        /// <summary>
        /// Kiểm tra các điều kiện hợp lệ trước khi thực hiện export template theo IMEI
        /// </summary>
        /// <returns></returns>
        private bool IsValidForExportIMEITemplate()
        {
            if (string.IsNullOrEmpty(cboToStoreID.StoreIDList))
            {
                switch ((ETransferType)cboType.SelectedValue)
                {
                    case ETransferType.Send:
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho nhập!");
                            break;
                        }
                    case ETransferType.Receive:
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho nhập!");
                            break;
                        }
                    default:
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn kho xuất!");
                            break;
                        }
                }
                cboToStoreID.Focus();
                return false;
            }
            string[] strArrStoreID = cboToStoreID.StoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.RemoveEmptyEntries);
            if (strArrStoreID.Contains(cboFromStoreID.StoreID.ToString().Trim()))
            {
                MessageBox.Show("Kho nhập phải khác với kho xuất!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboToStoreID.Focus();
                return false;
            }
            return true;
        }
        /// <summary>
        /// Kiểm tra các điều kiện hợp lệ trước khi thực hiện export template theo số lượng
        /// </summary>
        /// <returns></returns>
        private bool IsValidForExportQuantityTemplate()
        {
            if (string.IsNullOrEmpty(cboToStoreID.StoreIDList))
            {
                if (string.IsNullOrEmpty(cboToStoreID.StoreIDList))
                {
                    string strMess = "Vui lòng chọn kho nhập";
                    if (cboType.SelectedIndex == 1)
                        strMess = "Vui lòng chọn kho xuất";
                    MessageBox.Show(this, strMess, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboToStoreID.Focus();
                    return false;
                }
                cboToStoreID.Focus();
                return false;
            }
            string[] strArrStoreID = cboToStoreID.StoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.RemoveEmptyEntries);
            if (strArrStoreID.Contains(cboFromStoreID.StoreID.ToString().Trim()))
            {
                MessageBox.Show("Kho nhập phải khác với kho xuất!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboToStoreID.Focus();
                return false;
            }
            return true;
        }
        /// <summary>
        /// Kiểm tra các điều kiện hợp lệ trước khi thực hiện export template chuyển nhiều kho tới nhiều kho
        /// </summary>
        /// <returns></returns>
        private bool IsValidForExportMultiTemplate()
        {
            return true;
        }
        /// <summary>
        /// Xuất file template chuyển kho theo IMEI
        /// </summary>
        private void ExportIMEITemplate()
        {
            girdViewTemplate.Columns.Clear();
            string template = Application.StartupPath + "\\" + "Templates\\AutoStoreChangeOrderIMEI.xlsx";
            if (!System.IO.File.Exists(template))
            {
                MessageBox.Show("Không tìm thấy file template", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            GemBox.Spreadsheet.ExcelFile xls = new GemBox.Spreadsheet.ExcelFile(template);
            var workSheet = xls.Worksheets[0];
            if ((ETransferType)Convert.ToInt32(cboType.SelectedValue) == ETransferType.Send)
            {

                workSheet.Cells[1, 1].Value = "Tên kho";
                workSheet.Cells[2, 1].Value = "Mã kho";
                workSheet.Cells[3, 1].Value = "IMEI";
                List<int> listStoreID = GetSelectedStoreID();

                int colIndex = 2;
                listStoreID.ForEach(storeID =>
                {
                    DataRow storeInfo = null;
                    if (DataSourceStore != null && DataSourceStore.Rows.Count > 0)
                        storeInfo = DataSourceStore.Rows.Find(storeID);
                    workSheet.Cells[1, colIndex].Value = storeInfo != null ? storeInfo["STORENAME"].GetString() : string.Empty;
                    workSheet.Cells[2, colIndex].Value = storeID.ToString();
                    colIndex++;
                });
            }
            else
            {
                workSheet.Cells[1, 1].Value = "IMEI";
            }
            SaveFileDialog saveFileDialog2 = new SaveFileDialog();
            saveFileDialog2.Filter = "Excel Document|*.xlsx";
            if (saveFileDialog2.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                xls.SaveExcel(saveFileDialog2.FileName);
                if (MessageBox.Show("Bạn có muốn mở file?", "Mở file", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                   == DialogResult.Yes)
                {
                    Process.Start(saveFileDialog2.FileName);
                }
            }
        }
        /// <summary>
        /// Xuất file template chuyển kho theo số lượng
        /// </summary>
        private void ExportQuantityTemplate()
        {
            //girdViewTemplate.Columns.Clear();

            //DataTable dtbExportTemplate = new DataTable();
            //List<int> listStoreID = new List<int>();
            //girdViewTemplate.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            //{
            //    Name = "colProductID",
            //    Caption = "Mã sản phẩm",
            //    FieldName = "ProductID",
            //    Visible = true,
            //    Width = 120,
            //    VisibleIndex = 0
            //});
            //girdViewTemplate.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            //{
            //    Name = "colProductStatus",
            //    Caption = "Trạng thái",
            //    FieldName = "InstockStatusID",
            //    Visible = true,
            //    Width = 150,
            //    VisibleIndex = 1
            //});


            //dtbExportTemplate.Columns.Add(new DataColumn() { ColumnName = "ProductID" });
            //dtbExportTemplate.Columns.Add(new DataColumn() { ColumnName = "InstockStatusID" });
            //listStoreID = GetSelectedStoreID();
            //int visibleIndex = 2;
            //listStoreID.ForEach(storeID =>
            //{
            //    DataRow storeInfo = DataSourceStore.Rows.Find(storeID);
            //    girdViewTemplate.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn()
            //    {
            //        Name = "STORE" + storeID,
            //        Caption = storeInfo["STORENAME"].GetString(),
            //        FieldName = "STORE" + storeID,
            //        Width = 250,
            //        Visible = true,
            //        VisibleIndex = visibleIndex
            //    });
            //    visibleIndex++;
            //    dtbExportTemplate.Columns.Add(new DataColumn() { ColumnName = "STORE" + storeID });
            //});
            //DataRow r = dtbExportTemplate.NewRow();
            //r["ProductID"] = "PRODUCTID";
            //r["InstockStatusID"] = "STATUSID";
            //for (int i = 2; i < dtbExportTemplate.Columns.Count; i++)
            //{
            //    r[i] = dtbExportTemplate.Columns[i].ColumnName.Replace("STORE", string.Empty).ToString();
            //}
            //dtbExportTemplate.Rows.Add(r);
            //grdTemplate.DataSource = dtbExportTemplate;

            //SaveFileDialog saveFileDialog = new SaveFileDialog();
            //saveFileDialog.Filter = "Excel Document|*.xlsx";
            //if (saveFileDialog.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            //{
            //    grdTemplate.ExportToXlsx(saveFileDialog.FileName);
            //}

            string template = Application.StartupPath + "\\" + "Templates\\AutoStoreChangeOrderQuantity.xlsx";
            if (!System.IO.File.Exists(template))
            {
                MessageBox.Show("Không tìm thấy file template", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            GemBox.Spreadsheet.ExcelFile xls = new GemBox.Spreadsheet.ExcelFile(template);
            var workSheet = xls.Worksheets[0];
            workSheet.Cells[1, 1].Value = "Mã sản phẩm";
            workSheet.Cells[1, 2].Value = "Trạng thái sản phẩm";
            workSheet.Cells[1, 2].Comment.Text = ProductStateComment.ToString();
            workSheet.Cells[1, 2].Comment.IsVisible = false;


            workSheet.Cells[2, 1].Value = "PRODUCTID";
            workSheet.Cells[2, 2].Value = "PRODUCTSTATUSID";
            var listStoreID = GetSelectedStoreID();
            int colIndex = 3;

            listStoreID.ForEach(storeid =>
            {
                DataRow storeInfo = null;
                if (DataSourceStore != null && DataSourceStore.Rows.Count > 0)
                    storeInfo = DataSourceStore.Rows.Find(storeid);
                workSheet.Cells[1, colIndex].Value = storeInfo != null ? storeInfo["STORENAME"].GetString() : string.Empty;
                workSheet.Cells[2, colIndex].Value = storeid.ToString();
                colIndex++;
            });

            SaveFileDialog saveFileDialog2 = new SaveFileDialog();
            saveFileDialog2.Filter = "Excel Document|*.xlsx";
            if (saveFileDialog2.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                xls.SaveExcel(saveFileDialog2.FileName);
                if (MessageBox.Show("Bạn có muốn mở file?", "Mở file", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                   == DialogResult.Yes)
                {
                    Process.Start(saveFileDialog2.FileName);
                }
            }

        }
        /// <summary>
        /// Xuất file template chuyển nhiều kho tới nhiều kho
        /// </summary>
        private void ExportMultiTemplate()
        {
            //girdViewTemplate.Columns.Clear();
            //girdViewTemplate.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            //    new DevExpress.XtraGrid.Columns.GridColumn() { Caption = "Mã kho chuyển", Width = 120, VisibleIndex = 0, Visible = true },
            //    new DevExpress.XtraGrid.Columns.GridColumn() { Caption = "Mã sản phẩm", Width = 120, VisibleIndex = 1, Visible = true },
            //    new DevExpress.XtraGrid.Columns.GridColumn() { Caption = "Trạng thái", Width = 120, VisibleIndex = 2, Visible = true },
            //    new DevExpress.XtraGrid.Columns.GridColumn() { Caption = "Số lượng", Width = 120, VisibleIndex = 3, Visible = true },
            //    new DevExpress.XtraGrid.Columns.GridColumn() { Caption = "Mã kho nhận", Width = 120, VisibleIndex = 4, Visible = true },

            //});

            //grdTemplate.DataSource = null;
            //SaveFileDialog saveFileDialog = new SaveFileDialog();
            //saveFileDialog.Filter = "Excel Document|*.xlsx";
            //if (saveFileDialog.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            //{
            //    grdTemplate.ExportToXlsx(saveFileDialog.FileName);
            //}
            string template = Application.StartupPath + "\\" + "Templates\\AutoStoreChangeOrderMultiStore.xlsx";
            if (!System.IO.File.Exists(template))
            {
                MessageBox.Show("Không tìm thấy file template", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            GemBox.Spreadsheet.ExcelFile xls = new GemBox.Spreadsheet.ExcelFile(template);
            var workSheet = xls.Worksheets[0];
            workSheet.Cells[1, 3].Comment.Text = ProductStateComment.ToString();
            workSheet.Cells[1, 3].Comment.IsVisible = false;
            SaveFileDialog saveFileDialog2 = new SaveFileDialog();
            saveFileDialog2.Filter = "Excel Document|*.xlsx";
            if (saveFileDialog2.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                xls.SaveExcel(saveFileDialog2.FileName);
                if (MessageBox.Show("Bạn có muốn mở file?", "Mở file", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                  == DialogResult.Yes)
                {
                    Process.Start(saveFileDialog2.FileName);
                }
            }
        }

        /// <summary>
        /// Lấy thông tin tồn kho của một danh sách IMEI
        /// </summary>
        /// <param name="listIMEI"></param>
        /// <returns></returns>
        private DataTable GetIMEIInstock(List<string> listIMEI)
        {
            DataTable dtbIMEI = new DataTable();
            dtbIMEI.Columns.Add("IMEI", typeof(string));
            listIMEI.Where(imei => !string.IsNullOrEmpty(imei)).Distinct().ToList().ForEach(imei =>
            {
                DataRow row = dtbIMEI.NewRow();
                row["IMEI"] = imei;
                dtbIMEI.Rows.Add(row);
            });
            string xmlIMEI = DUIInventoryCommon.CreateXMLFromDataTable(dtbIMEI, "IMEI");
            DataTable IMEI_Instock = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("CHECKIMEI_INSTOCK_1", new object[] { "@IMEI", xmlIMEI, "@UserName", Library.AppCore.SystemConfig.objSessionUser.UserName });
            IMEI_Instock.PrimaryKey = new DataColumn[] { IMEI_Instock.Columns["IMEI"] };
            return IMEI_Instock;
        }

        /// <summary>
        /// Kiểm tra dữ liệu đọc được từ file excel có đúng định dạng cho import yêu cầu xuất chuyển theo IMEI
        /// </summary>
        /// <param name="importedData"></param>
        /// <returns></returns>
        private bool IsValidImportIMEITemplate(DataTable importedData)
        {
            if (importedData == null || importedData.Rows.Count == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy dữ liệu trên file import");
                return false;
            }
            for (int i = 0; i < importedData.Columns.Count; i++)
            {
                if (string.IsNullOrEmpty(importedData.Rows[1][i].GetString()))
                {
                    if (importedData.AsEnumerable().Any(d => d[i].GetString() != string.Empty))
                    {
                        MessageBoxObject.ShowWarningMessage(this, "File template sai định dạng");
                        return false;
                    }
                }
            }

            if (Convert.ToInt32(cboType.SelectedValue) == (int)ETransferType.Receive)
            {
                if (importedData.Columns.Count < 1)
                {
                    MessageBoxObject.ShowWarningMessage(this, "File template sai định dạng");
                    return false;
                }

                if (importedData.Rows[0][0].GetString().ToUpper().Trim() != "IMEI")
                {
                    MessageBoxObject.ShowWarningMessage(this, "File template sai định dạng");
                    return false;
                }
            }

            if (Convert.ToInt32(cboType.SelectedValue) == (int)ETransferType.Send)
            {
                if (importedData.Rows.Count < 3)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy dữ liệu trên file import");
                    return false;
                }

                if (importedData.Columns.Count < 2)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy dữ liệu trên file import");
                    return false;
                }

                string _strInvalidColIndex = string.Empty;
                for (int i = 1; i < importedData.Columns.Count; i++)
                {
                    if (string.IsNullOrEmpty(importedData.Rows[1][i].GetString()))
                        _strInvalidColIndex += i + ",";
                }

                if (_strInvalidColIndex.Length > 0)
                {
                    //MessageBoxObject.ShowWarningMessage(this, "Cột " + _strInvalidColIndex + " trên file template - mã kho không được bỏ trống");
                    MessageBoxObject.ShowWarningMessage(this, "File template sai định dạng");
                    return false;
                }

                for (int i = 1; i < importedData.Rows[1].ItemArray.Length; i++)
                {
                    if (!importedData.Rows[1].ItemArray[i].IsNumber())
                    {
                        _strInvalidColIndex += i + ",";
                    }
                }

                if (_strInvalidColIndex.Length > 0)
                {
                    //MessageBoxObject.ShowWarningMessage(this, "Cột " + _strInvalidColIndex + " trên file template - mã kho sai định dạng");
                    MessageBoxObject.ShowWarningMessage(this, "File template sai định dạng");
                    return false;
                }

                for (int i = 1; i < importedData.Rows[1].ItemArray.Length; i++)
                {
                    if (DataSourceStore != null && DataSourceStore.Rows.Count > 0)
                    {
                        if (DataSourceStore.Rows.Find(Convert.ToInt32(importedData.Rows[1].ItemArray[i])) == null)
                        {
                            _strInvalidColIndex += (importedData.Rows[1].ItemArray[i].GetString() + ",");
                        }
                    }
                    else
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Không có dữ liệu kho để kiểm tra");
                        return false;
                    }
                }

                if (_strInvalidColIndex.Length > 0)
                {
                    // MessageBoxObject.ShowWarningMessage(this, "Mã kho " + _strInvalidColIndex + " trên file template - không tồn tại trên hệ thống");
                    MessageBoxObject.ShowWarningMessage(this, "File template sai định dạng");
                    return false;
                }

                importedData.Columns.Remove(importedData.Columns[0]);
                importedData.Rows[1].ItemArray.Where(d => !string.IsNullOrEmpty(d.GetString())).GroupBy(d => d)
                    .Where(d => d.Count() > 1)
                    .ToList().ForEach(d =>
                    {
                        _strInvalidColIndex += d.Key + ",";
                    });

                if (_strInvalidColIndex.Length > 0)
                {
                    //MessageBoxObject.ShowWarningMessage(this, "Mã kho " + _strInvalidColIndex + " trên file template - bị trùng");
                    MessageBoxObject.ShowWarningMessage(this, "File template sai định dạng");
                    return false;
                }
            }


            return true;
        }
        /// <summary>
        /// Kiểm tra nội dung trên file template có đúng định dạng cho import theo số lượng không
        /// </summary>
        /// <param name="importedData"></param>
        /// <returns></returns>
        private bool IsValidImportQuantityTemplate(DataTable importedData)
        {
            if (importedData == null || importedData.Rows.Count < 3)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy dữ liệu trên file import");
                return false;
            }



            if (importedData.Rows[0][0].GetString().Trim().ToUpper() != "MÃ SẢN PHẨM"
                || importedData.Rows[0][1].GetString().Trim().ToUpper() != "TRẠNG THÁI SẢN PHẨM"
                || importedData.Rows[1][0].GetString().Trim().ToUpper() != "PRODUCTID"
                || importedData.Rows[1][1].GetString().Trim().ToUpper() != "PRODUCTSTATUSID")
            {
                MessageBoxObject.ShowWarningMessage(this, "File template sai định dạng");
                return false;
            }

            for (int i = 0; i < importedData.Columns.Count; i++)
            {
                if (string.IsNullOrEmpty(importedData.Rows[1][i].GetString()))
                {
                    if (importedData.AsEnumerable().Any(d => d[i].GetString() != string.Empty))
                    {
                        MessageBoxObject.ShowWarningMessage(this, "File template sai định dạng");
                        return false;
                    }
                }
            }


            if (IsAllowOldCode)
            {
                if (DataSourceOldStore != null)
                {
                    for (int i = 2; i < importedData.Columns.Count; i++)
                    {
                        DataRow[] rowsel = DataSourceOldStore.Select("OLDSTOREID='" + importedData.Rows[1][i].ToString().Trim() + "'");
                        if (rowsel.Length > 0)
                            importedData.Rows[1][i] = rowsel[0]["STOREID"];
                        else
                        {
                            MessageBoxObject.ShowWarningMessage(this, "Mã kho " + importedData.Rows[1][i].ToString() + " không tồn tại");
                            return false;
                        }
                    }
                }
                if (DataSourceOldProduct != null)
                {
                    for (int i = 2; i < importedData.Rows.Count; i++)
                    {
                        DataRow[] rowsel = DataSourceOldProduct.Select("PRODUCTIDOLD='" + importedData.Rows[i][0].ToString().Trim() + "'");
                        if (rowsel.Length > 0)
                            importedData.Rows[i][0] = rowsel[0]["PRODUCTID"];
                    }
                }
            }

            for (int colIndex = 2; colIndex < importedData.Rows[1].ItemArray.Length; colIndex++)
            {
                if (!importedData.Rows[1].ItemArray[colIndex].IsNumber())
                {
                    MessageBoxObject.ShowWarningMessage(this, "File template sai định dạng");
                    return false;
                }
            }

            if (importedData.Rows[1].ItemArray.Any(d => !string.IsNullOrEmpty(d.GetString()) && importedData.Rows[1].ItemArray.Where(e => e.GetString() == d.GetString()).Count() > 1))
            {
                MessageBoxObject.ShowWarningMessage(this, "File template sai định dạng");
                return false;
            }

            string _strInvalidStoreID = string.Empty;
            for (int colIndex = 2; colIndex < importedData.Rows[1].ItemArray.Length; colIndex++)
            {
                if (DataSourceStore != null && DataSourceStore.Rows.Count > 0)
                {
                    if (DataSourceStore.Rows.Find(Convert.ToInt32(importedData.Rows[1].ItemArray[colIndex])) == null)
                        _strInvalidStoreID += importedData.Rows[1].ItemArray[colIndex].GetString() + ",";
                }
                else
                {
                    MessageBoxObject.ShowWarningMessage(this, "Không có dữ liệu kho để kiểm tra");
                    return false;
                }
            }
            if (_strInvalidStoreID.Length > 0)
            {
                //MessageBoxObject.ShowWarningMessage(this, "Mã kho: " + _strInvalidStoreID.Remove(_strInvalidStoreID.Length, 1) + " không tồn tại trên hệ thống");
                MessageBoxObject.ShowWarningMessage(this, "File template sai định dạng");
                return false;
            }
            return true;
        }
        /// <summary>
        /// Kiểm tra nội dung trên file template có đúng định dạng cho import chuyển từ nhiều kho tới nhiều kho
        /// </summary>
        /// <param name="importedData"></param>
        /// <returns></returns>
        private bool IsValidImportMultiTemplate(DataTable importedData)
        {
            if (importedData == null || importedData.Rows.Count < 1)
            {
                MessageBoxObject.ShowWarningMessage(this, "Không tìm thấy dữ liệu trên file import");
                return false;
            }
            if (importedData.Columns.Count < 5)
            {
                MessageBoxObject.ShowWarningMessage(this, "File template sai định dạng");
                return false;
            }
            if (importedData.Rows[0][0].GetString().Trim().ToUpper() != "MÃ KHO CHUYỂN"
               || importedData.Rows[0][1].GetString().Trim().ToUpper() != "MÃ SẢN PHẨM"
               || importedData.Rows[0][2].GetString().Trim().ToUpper() != "TRẠNG THÁI"
               || importedData.Rows[0][3].GetString().Trim().ToUpper() != "SỐ LƯỢNG"
               || importedData.Rows[0][4].GetString().Trim().ToUpper() != "MÃ KHO NHẬN")
            {
                MessageBoxObject.ShowWarningMessage(this, "File template sai định dạng");
                return false;
            }
            return true;
        }

        /// <summary>
        /// Import chuyển kho theo IMEI từ file excel
        /// </summary>
        private void ImportIMEI()
        {
            List<ERP.Inventory.PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> importResult = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext>();
            try
            {
                DataTable importedData = C1ExcelObject.OpenExcel2DataTable();
                importedData = TrimDataTable(importedData);
                if (!IsValidImportIMEITemplate(importedData))
                    return;

                int storeIDKey = cboFromStoreID.StoreID;
                string storeKeyName = string.Empty;
                if (DataSourceStore != null && DataSourceStore.Rows.Count > 0)
                {
                    DataRow iStoreKey = DataSourceStore.Rows.Find(storeIDKey);
                    if (iStoreKey != null)
                        storeKeyName = storeIDKey + " - " + iStoreKey["STORENAME"].GetString();
                }
                ETransferType transferType = (ETransferType)Convert.ToInt32(cboType.SelectedValue);
                BeginSuspendForm();

                List<int> listSelectedMainGroupID = GetSelectedMainGroupID();
                for (int colIndex = 1; colIndex < importedData.Columns.Count; colIndex++)
                {
                    if (string.IsNullOrEmpty(importedData.Rows[0][colIndex].GetString()))
                    {
                        importedData.Columns.RemoveAt(colIndex);
                        colIndex--;
                        continue;
                    }
                }

                #region Gửi đi
                bool IsErrorFiFo = false;
                if (transferType == (int)ETransferType.Send)
                {
                    //Remove store name row
                    importedData.Rows.Remove(importedData.Rows[0]);
                    for (int i = 0; i < importedData.Columns.Count; i++)
                        importedData.Columns[i].ColumnName = "STOREID_" + importedData.Rows[0][i];
                    List<int> ListStoreID = importedData.Rows[0].ItemArray
                        .Where(d => !string.IsNullOrEmpty(d.GetString()) && d.IsNumber())
                        .Select(d => Convert.ToInt32(d)).ToList();

                    importedData.Rows.Remove(importedData.Rows[0]);
                    List<StoreIMEIs> store_imeis = new List<StoreIMEIs>();
                    List<string> listIMEI = new List<string>();
                    ListStoreID.ForEach(receivedStoreID =>
                    {
                        List<string> ListIMEI = importedData.AsEnumerable().Select(d => d["STOREID_" + receivedStoreID].GetString()).ToList();
                        if (ListIMEI.Any())
                        {
                            ListIMEI = ListIMEI.Where(d => !string.IsNullOrEmpty(d)).ToList();
                            store_imeis.Add(new StoreIMEIs()
                            {
                                StoreID = receivedStoreID,
                                ListIMEI = ListIMEI
                            });
                            listIMEI.AddRange(ListIMEI);
                        }
                    });

                    //Thông tin imei và trạng thái instock
                    DataTable IMEI_Instock = GetIMEIInstock(listIMEI);
                    List<string> listProductID = IMEI_Instock.AsEnumerable().Where(d => !string.IsNullOrEmpty(d["PRODUCTID"].GetString())).Select(d => d["PRODUCTID"].GetString()).Distinct().ToList();
                    List<int> totalStoreID = new List<int>();
                    totalStoreID.AddRange(ListStoreID);
                    totalStoreID.Add(storeIDKey);
                    DataTable Product_Instock = GetStoreInStock(listProductID, totalStoreID.Distinct().ToList());
                    //if (Product_Instock == null || Product_Instock.Rows.Count == 0)
                    //{
                    //    MessageBoxObject.ShowWarningMessage(this, "Không có thông tin tồn kho sản phẩm!");
                    //    return;
                    //}

                    if (Product_Instock != null)
                        Product_Instock.PrimaryKey = new DataColumn[] { Product_Instock.Columns["STOREID"], Product_Instock.Columns["PRODUCTID"], Product_Instock.Columns["INSTOCKSTATUSID"] };
                    store_imeis.ForEach(item =>
                    {
                        if (item.ListIMEI.Any())
                        {
                            DataRow toStoreInfo = null;
                            if (DataSourceStore != null && DataSourceStore.Rows.Count > 0)
                                toStoreInfo = DataSourceStore.Rows.Find(item.StoreID);

                            DataTable IMEI_Instock_ByStore = IMEI_Instock.Clone();
                            DataRow[] drs = IMEI_Instock.AsEnumerable().Where(d => item.ListIMEI.Contains(d["IMEI"].GetString())).ToArray();
                            if (drs != null && drs.Any())
                            {
                                IMEI_Instock_ByStore = drs.CopyToDataTable();
                            }
                            if (IMEI_Instock_ByStore != null)
                                IMEI_Instock_ByStore.PrimaryKey = new DataColumn[] { IMEI_Instock_ByStore.Columns["IMEI"] };

                            List<string> listImeiNotFound = new List<string>();
                            if (IMEI_Instock_ByStore != null && IMEI_Instock_ByStore.Rows.Count > 0)
                            {
                                listImeiNotFound = item.ListIMEI.Where(d => IMEI_Instock_ByStore.Rows.Find(d) == null).ToList();
                            }

                            List<string> listDuplicate = item.ListIMEI.Where(d => store_imeis.Where(f => f.ListIMEI.Contains(d)).Count() > 1).ToList();
                            List<string> listDuplicate2 = item.ListIMEI.Where(d => !listImeiNotFound.Contains(d) && item.ListIMEI.Where(e => e == d).Count() > 1).ToList();


                            listDuplicate.AddRange(listDuplicate2);
                            listDuplicate = listDuplicate.Distinct().ToList();
                            List<int> listInstockStatusID = IMEI_Instock_ByStore.AsEnumerable().Select(d => Convert.ToInt32(d["INSTOCKSTATUSID"])).Distinct().ToList();
                            #region gom đống imei not found lại với trạng thái trống
                            if (listImeiNotFound.Any())
                            {
                                PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext objStoreChange = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext();
                                objStoreChange.FromStore = storeIDKey;
                                objStoreChange.ImportFromStoreID = storeIDKey.ToString();
                                objStoreChange.FromStoreName = storeKeyName;
                                objStoreChange.IsUrgent = false;
                                objStoreChange.ImportToStoreID = item.StoreID.ToString();
                                objStoreChange.ToStore = item.StoreID;
                                objStoreChange.ToStoreName = toStoreInfo == null ?
                                string.Empty : (toStoreInfo["STOREID"].GetString() + " - " + toStoreInfo["STORENAME"].GetString());
                                objStoreChange.StoreChangeTypeID = (int)ETransferType.Send;
                                objStoreChange.ImportProductStateID = string.Empty;
                                objStoreChange.ImportProductStateName = string.Empty;

                                List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail> details = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail>();
                                listImeiNotFound.ForEach(imei =>
                                {
                                    PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail detail = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail()
                                    {
                                        IMEI = imei,
                                        IsManualAdd = false,
                                        ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid,
                                        ImportMessage = "Không tìm thấy IMEI",
                                        ImportQuantity = 0.ToString(),
                                        AutoQuantity = 0,
                                        FromInStockQuantity = 0,
                                        ToInStockQuantity = 0,
                                        Quantity = 0,
                                    };
                                    details.Add(detail);
                                });
                                if (details.Any())
                                {
                                    objStoreChange.Detail = details.ToArray();
                                    importResult.Add(objStoreChange);
                                }
                            }
                            #endregion

                            listInstockStatusID.ForEach(instockStatusID =>
                            {
                                DataRow instockStateInfo = null;
                                if (DataSourceProductState != null && DataSourceProductState.Rows.Count > 0)
                                    instockStateInfo = DataSourceProductState.Rows.Find(instockStatusID);

                                PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext objStoreChange = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext();
                                objStoreChange.FromStore = storeIDKey;
                                objStoreChange.ImportFromStoreID = storeIDKey.ToString();
                                objStoreChange.FromStoreName = storeKeyName;
                                objStoreChange.IsUrgent = false;
                                objStoreChange.ImportToStoreID = item.StoreID.ToString();
                                objStoreChange.ToStore = item.StoreID;
                                objStoreChange.ToStoreName = toStoreInfo == null ?
                                string.Empty : (toStoreInfo["STOREID"].GetString() + " - " + toStoreInfo["STORENAME"].GetString());
                                objStoreChange.StoreChangeTypeID = (int)ETransferType.Send;
                                objStoreChange.ImportProductStateID = instockStatusID.ToString();
                                objStoreChange.ProductState = instockStatusID;
                                objStoreChange.ImportProductStateName = instockStateInfo["PRODUCTSTATUSNAME"].GetString();
                                List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail> details = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail>();

                                IMEI_Instock_ByStore.AsEnumerable().Where(d => item.ListIMEI.Contains(d["IMEI"].GetString()) && !listImeiNotFound.Contains(d["IMEI"].GetString())
                                && Convert.ToInt32(d["INSTOCKSTATUSID"]) == instockStatusID).Select(d => d["IMEI"].GetString()).ToList().ForEach(imei =>
                                {
                                    PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail detail = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail();
                                    string productID = string.Empty;
                                    if (IMEI_Instock != null && IMEI_Instock.Rows.Count > 0)
                                        productID = IMEI_Instock.Rows.Find(imei)["PRODUCTID"].GetString();
                                    DataRow productInfo = null;
                                    if (DataSourceProduct != null && DataSourceProduct.Rows.Count > 0)
                                        productInfo = DataSourceProduct.Rows.Find(productID);
                                    DataRow productInstockInfoFrom = null;
                                    DataRow productInstockInfoTo = null;
                                    if (Product_Instock != null)
                                    {
                                        productInstockInfoFrom = Product_Instock.Rows.Find(new object[] { storeIDKey, productID, instockStatusID });
                                        productInstockInfoTo = Product_Instock.Rows.Find(new object[] { item.StoreID, productID, instockStatusID });
                                    }

                                    detail.IMEI = imei;
                                    detail.IsManualAdd = false;
                                    detail.ProductID = productID;
                                    detail.ProductName = productInfo == null ? string.Empty : productInfo["PRODUCTNAME"].GetString();
                                    detail.IsFIFOError = Convert.ToInt32(IMEI_Instock.Rows.Find(imei)["STATUSFIFOID"]);
                                    if (detail.IsFIFOError == 1)
                                        detail.ImportMessage = "IMEI " + detail.IMEI.ToString().Trim() + " vi phạm xuất fifo";

                                    if (productInstockInfoFrom != null)
                                    {
                                        detail.FromInStockQuantity = Convert.ToDecimal(productInstockInfoFrom["QUANTITY"]);
                                    }

                                    if (productInstockInfoTo != null)
                                        detail.ToInStockQuantity = Convert.ToDecimal(productInstockInfoTo["QUANTITY"]);

                                    if (listDuplicate.Any(d => d == imei))
                                    {
                                        detail.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                        detail.ImportMessage = "Dữ liệu bị trùng";
                                    }
                                    else
                                    {
                                        DataRow imeiInstockInfo = null;
                                        if (IMEI_Instock_ByStore != null && IMEI_Instock_ByStore.Rows.Count > 0)
                                            imeiInstockInfo = IMEI_Instock_ByStore.Rows.Find(imei);
                                        if (productInstockInfoFrom == null)
                                        {
                                            detail.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                            detail.ImportMessage = "Sản phẩm không tồn kho";
                                        }
                                        else if (Convert.ToInt32(imeiInstockInfo["STOREID"].GetString()) != storeIDKey)
                                        {
                                            detail.ImportMessage = "IMEI không tồn tại ở kho xuất naỳ";
                                            detail.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                        }
                                        else if (Convert.ToInt32(imeiInstockInfo["ISORDER"].GetString()) != 0)
                                        {
                                            detail.ImportMessage = "IMEI này đã được sử dụng";
                                            detail.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                        }
                                        else
                                        {

                                            int mainGroupID = Convert.ToInt32(productInfo["MAINGROUPID"]);
                                            DataRow mainGroupInfo = null;
                                            if (DataSourceMainGroup != null && DataSourceMainGroup.Rows.Count > 0)
                                                mainGroupInfo = DataSourceMainGroup.Rows.Find(mainGroupID);
                                            StringBuilder strErrorMessage = new StringBuilder();
                                            if (!listSelectedMainGroupID.Any(d => d == mainGroupID))
                                                strErrorMessage.AppendLine("Sản phẩm này không thuộc các ngành hàng đang chọn");
                                            if (!CheckPermission(mainGroupID))
                                                strErrorMessage.AppendLine("Bạn không có quyền thao tác trên ngành hàng " + (mainGroupInfo != null ? mainGroupInfo["MAINGROUPNAME"].GetString() : string.Empty));
                                            if (!IsMainGroupPermission(storeIDKey, mainGroupID, EnumType.StoreMainGroupPermissionType.OUTPUT))
                                                strErrorMessage.AppendLine("Kho xuất này không được xuất chuyển trên ngành hàng " + mainGroupInfo["MAINGROUPNAME"].GetString());
                                            if (!IsMainGroupPermission(item.StoreID, mainGroupID, EnumType.StoreMainGroupPermissionType.INPUT))
                                                strErrorMessage.AppendLine("Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + mainGroupInfo["MAINGROUPNAME"].GetString());
                                            if (!IsIncludedRoaming && Convert.ToInt32(imeiInstockInfo["ISCHECKREALINPUT"]) == 0)
                                                strErrorMessage.AppendLine("IMEI là hàng đi đường");
                                            if (objStoreChange.FromStore == objStoreChange.ToStore && objStoreChange.FromStore != 0)
                                                strErrorMessage.AppendLine("Kho nhập không được trùng với kho xuất");
                                            if (strErrorMessage.Length > 0)
                                            {
                                                detail.ImportMessage = strErrorMessage.ToString();
                                                detail.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                            }
                                            else
                                            {
                                                detail.ImportQuantity = "1";
                                                detail.Quantity = 1;
                                                detail.AutoQuantity = 1;
                                            }
                                        }
                                    }
                                    details.Add(detail);

                                });
                                var varReulst = from o in details
                                                where o.IsFIFOError == 1
                                                    && o.ImportStatus != PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid
                                                select o;
                                if (varReulst.Any())
                                {
                                    IsErrorFiFo = true;
                                    //varReulst.All(c =>
                                    //{
                                    //    c.ImportMessage = "IMEI " + c.IMEI.ToString().Trim() + " vi phạm xuất fifo";
                                    //    return true;
                                    //});
                                    //DialogResult drResult = MessageBox.Show(this, "Có tồn tại dữ liệu vi phạm xuất fifo bạn có muốn tiếp tục?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                                    //if (drResult != System.Windows.Forms.DialogResult.OK)
                                    //{
                                    //    varReulst.All(c =>
                                    //    {
                                    //        c.ImportMessage = "IMEI " + c.IMEI.ToString().Trim() + " vi phạm xuất fifo";
                                    //        c.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                    //        return true;
                                    //    });
                                    //    //varReulst.All(c => { c.IsError = true; c.Error = "IMEI " + c.Imei.ToString().Trim() + " vi phạm xuất fifo"; return true; });
                                    //}
                                }
                                if (details.Any())
                                {
                                    objStoreChange.Detail = details.ToArray();
                                    importResult.Add(objStoreChange);
                                }
                            });
                        }
                    });
                }
                #endregion
                if (IsErrorFiFo)
                {
                    DialogResult drResult = MessageBox.Show(this, "Có tồn tại dữ liệu vi phạm xuất fifo bạn có muốn tiếp tục?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                    if (drResult != System.Windows.Forms.DialogResult.OK)
                    {
                        foreach (var itemDT in importResult)
                        {
                            foreach (var item in itemDT.Detail)
                            {
                                if (item.IsFIFOError == 1 && item.ImportStatus != PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid)
                                {
                                    item.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                }
                            }
                        }
                    }

                }
                //importResult



                #region Nhận về
                if (transferType == ETransferType.Receive)
                {
                    importedData.Rows.Remove(importedData.Rows[0]);
                    List<string> IMEIList = importedData.AsEnumerable().Where(d => !string.IsNullOrEmpty(d[0].GetString())).Select(d => d[0].GetString()).ToList();
                    DataTable IMEI_Instock = GetIMEIInstock(IMEIList.Distinct().ToList());

                    List<string> listDuplicate = IMEIList.GroupBy(d => d).Where(d => d.Count() > 1).Select(d => d.Key).ToList();
                    List<string> IMEIListHasInStock = IMEI_Instock.AsEnumerable().Select(d => d["IMEI"].GetString()).ToList();
                    List<string> listIMEINotFound = IMEIList.Where(d => !IMEIListHasInStock.Contains(d)).ToList();
                    List<int> listStoreIDFrom = IMEI_Instock.AsEnumerable().Select(d => Convert.ToInt32(d["STOREID"])).Distinct().ToList();
                    List<int> listInstockStatusID = IMEI_Instock.AsEnumerable().Select(d => Convert.ToInt32(d["INSTOCKSTATUSID"])).Distinct().ToList();
                    List<string> listProductID = IMEI_Instock.AsEnumerable().Where(d => !string.IsNullOrEmpty(d["PRODUCTID"].GetString())).Select(d => d["PRODUCTID"].GetString()).Distinct().ToList();
                    List<int> totalStoreID = new List<int>();
                    totalStoreID.AddRange(listStoreIDFrom);
                    totalStoreID.Add(storeIDKey);
                    DataTable Product_Instock = GetStoreInStock(listProductID, totalStoreID.Distinct().ToList());
                    if (Product_Instock != null)
                        Product_Instock.PrimaryKey = new DataColumn[] { Product_Instock.Columns["STOREID"], Product_Instock.Columns["PRODUCTID"], Product_Instock.Columns["INSTOCKSTATUSID"] };
                    #region gom đống imei not found lại với trạng thái trống
                    if (listIMEINotFound.Any())
                    {
                        PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext objStoreChange = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext();
                        objStoreChange.FromStore = 0;
                        objStoreChange.ImportFromStoreID = string.Empty;
                        objStoreChange.FromStoreName = string.Empty;
                        objStoreChange.IsUrgent = false;
                        objStoreChange.ImportToStoreID = storeIDKey.ToString();
                        objStoreChange.ToStore = storeIDKey;
                        objStoreChange.ToStoreName = storeKeyName;
                        objStoreChange.StoreChangeTypeID = (int)ETransferType.Receive;
                        objStoreChange.ImportProductStateID = string.Empty;
                        objStoreChange.ImportProductStateName = string.Empty;

                        List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail> details = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail>();
                        listIMEINotFound.ForEach(imei =>
                        {
                            PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail detail = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail()
                            {
                                IMEI = imei,
                                IsManualAdd = false,
                                ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid,
                                ImportMessage = "Không tìm thấy IMEI",
                                ImportQuantity = 0.ToString(),
                                AutoQuantity = 0,
                                FromInStockQuantity = 0,
                                ToInStockQuantity = 0,
                                Quantity = 0,
                            };
                            details.Add(detail);
                        });
                        if (details.Any())
                        {
                            objStoreChange.Detail = details.ToArray();
                            importResult.Add(objStoreChange);
                        }
                    }
                    #endregion
                    listStoreIDFrom.ForEach(fromStoreID =>
                    {
                        DataRow fromStoreInfo = null;
                        if (DataSourceStore != null && DataSourceStore.Rows.Count > 0)
                            fromStoreInfo = DataSourceStore.Rows.Find(fromStoreID);
                        listInstockStatusID.ForEach(instockStatusID =>
                        {
                            DataRow instockStatusInfo = null;
                            if (DataSourceProductState != null && DataSourceProductState.Rows.Count > 0)
                                instockStatusInfo = DataSourceProductState.Rows.Find(instockStatusID);
                            PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext objStoreChange = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext()
                            {
                                FromStore = fromStoreID,
                                FromStoreName = (fromStoreID + " - " + fromStoreInfo["STORENAME"].GetString()),
                                ImportFromStoreID = fromStoreID.ToString(),
                                ToStore = storeIDKey,
                                ToStoreName = storeKeyName,
                                ImportToStoreID = storeIDKey.ToString(),
                                IsUrgent = false,
                                ImportProductStateID = instockStatusID.ToString(),
                                ProductState = instockStatusID,
                                ImportProductStateName = instockStatusInfo["PRODUCTSTATUSNAME"].GetString(),
                                StoreChangeTypeID = (int)ETransferType.Receive
                            };

                            List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail> details = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail>();
                            IMEI_Instock.AsEnumerable().Where(d => Convert.ToInt32(d["STOREID"].GetString()) == fromStoreID
                            && Convert.ToInt32(d["INSTOCKSTATUSID"]) == instockStatusID).Select(d => d["IMEI"].GetString()).ToList()
                            .ForEach(imei =>
                            {
                                PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail detail = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail();
                                DataRow imeiInstockInfo = null;
                                if (IMEI_Instock != null && IMEI_Instock.Rows.Count > 0)
                                    imeiInstockInfo = IMEI_Instock.Rows.Find(imei);
                                string productID = imeiInstockInfo["PRODUCTID"].GetString();
                                DataRow productInfo = null;
                                if (DataSourceProduct != null && DataSourceProduct.Rows.Count > 0)
                                    productInfo = DataSourceProduct.Rows.Find(productID);
                                DataRow productInstockInfoFrom = null;
                                DataRow productInstockIntoTo = null;
                                if (Product_Instock != null)
                                {
                                    productInstockInfoFrom = Product_Instock.Rows.Find(new object[] { fromStoreID, productID, instockStatusID });
                                    productInstockIntoTo = Product_Instock.Rows.Find(new object[] { storeIDKey, productID, instockStatusID });
                                }

                                detail.IMEI = imei;
                                detail.IsManualAdd = false;
                                detail.ProductID = productID;
                                detail.ProductName = productInfo == null ? string.Empty : productInfo["PRODUCTNAME"].GetString();
                                if (productInstockInfoFrom != null)
                                    detail.FromInStockQuantity = Convert.ToDecimal(productInstockInfoFrom["QUANTITY"]);
                                if (productInstockIntoTo != null)
                                    detail.ToInStockQuantity = Convert.ToDecimal(productInstockIntoTo["QUANTITY"]);

                                if (listDuplicate.Any(d => d == imei))
                                {
                                    detail.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                    detail.ImportMessage = "Dữ liệu bị trùng";
                                }
                                else
                                {
                                    if (productInstockInfoFrom == null)
                                    {
                                        detail.ImportMessage = "Sản phẩm không tồn kho";
                                        detail.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                    }
                                    else if (Convert.ToInt32(imeiInstockInfo["ISORDER"].GetString()) != 0)
                                    {
                                        detail.ImportMessage = "IMEI này đã được sử dụng";
                                        detail.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                    }
                                    else
                                    {
                                        int mainGroupID = Convert.ToInt32(productInfo["MAINGROUPID"]);
                                        DataRow mainGroupInfo = null;
                                        if (DataSourceMainGroup != null && DataSourceMainGroup.Rows.Count > 0)
                                            mainGroupInfo = DataSourceMainGroup.Rows.Find(mainGroupID);
                                        StringBuilder strErrorMessage = new StringBuilder();
                                        if (!listSelectedMainGroupID.Any(d => d == mainGroupID))
                                            strErrorMessage.AppendLine("Sản phẩm này không thuộc về danh sách ngành hàng đang chọn");
                                        if (!CheckPermission(mainGroupID))
                                            strErrorMessage.AppendLine("Bạn không có quyền thao tác trên ngành hàng " + (mainGroupInfo != null ? mainGroupInfo["MAINGROUPNAME"].GetString() : string.Empty));
                                        if (!IsMainGroupPermission(fromStoreID, mainGroupID, EnumType.StoreMainGroupPermissionType.OUTPUT))
                                            strErrorMessage.AppendLine("Kho xuất này không được xuất chuyển trên ngành hàng " + mainGroupInfo["MAINGROUPNAME"].GetString());
                                        if (!IsMainGroupPermission(storeIDKey, mainGroupID, EnumType.StoreMainGroupPermissionType.INPUT))
                                            strErrorMessage.AppendLine("Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + mainGroupInfo["MAINGROUPNAME"].GetString());
                                        if (!IsIncludedRoaming && Convert.ToInt32(imeiInstockInfo["ISCHECKREALINPUT"]) == 0)
                                            strErrorMessage.AppendLine("IMEI là hàng đi đường");
                                        if (objStoreChange.FromStore == objStoreChange.ToStore && objStoreChange.FromStore != 0)
                                            strErrorMessage.AppendLine("Kho nhập không được trùng với kho xuất");

                                        if (strErrorMessage.Length > 0)
                                        {
                                            detail.ImportMessage = strErrorMessage.ToString();
                                            detail.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                        }
                                        else
                                        {
                                            detail.ImportQuantity = "1";
                                            detail.Quantity = 1;
                                            detail.AutoQuantity = 1;
                                        }
                                    }
                                }
                                details.Add(detail);
                            });
                            if (details.Any())
                            {
                                objStoreChange.Detail = details.ToArray();
                                importResult.Add(objStoreChange);
                            }
                        });
                    });

                }
                #endregion

                ListAutoStoreChangeContext = importResult;
                ShowImportResult();
            }
            finally
            {
                EndSuspendForm();
            }
        }

        /// <summary>
        /// Import chuyển kho theo số lượng từ file excel
        /// </summary>
        private void ImportQuantity()
        {
            List<ERP.Inventory.PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> importResult = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext>();
            try
            {
                DataTable importedData = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable();
                importedData = TrimDataTable(importedData);
                //Open file dialog has been cancelled
                if (importedData == null)
                    return;
                if (!IsValidImportQuantityTemplate(importedData))
                    return;


                BeginSuspendForm();
                //Remove caption row
                importedData.Rows.Remove(importedData.Rows[0]);
                //Rename columns
                importedData.Columns[0].ColumnName = "PRODUCTID";
                importedData.Columns[1].ColumnName = "PRODUCTSTATE";
                importedData.AsEnumerable().ToList().ForEach(d => { d["PRODUCTID"] = d["PRODUCTID"].GetString().Trim(); });
                for (int colIndex = 2; colIndex < importedData.Columns.Count; colIndex++)
                {
                    if (string.IsNullOrEmpty(importedData.Rows[0][colIndex].GetString()))
                    {
                        importedData.Columns.RemoveAt(colIndex);
                        colIndex--;
                        continue;
                    }
                    importedData.Columns[colIndex].ColumnName = "STOREID_" + importedData.Rows[0][colIndex].GetString().Trim();

                }
                List<int> listSelectedMainGroupID = GetSelectedMainGroupID();

                ETransferType transferType = (ETransferType)(Convert.ToInt32(cboType.SelectedValue));
                List<string> listProductState = new List<string>();
                List<int> listOutStoreID = new List<int>();
                List<int> processStoreID = new List<int>();
                List<string> listProductID = new List<string>();
                List<RealTimeProductInStock> RealTimeQuantityInstock = new List<RealTimeProductInStock>();

                DataSourceProductState.AsEnumerable().Select(record => Convert.ToInt32(record["PRODUCTSTATUSID"].GetString())).ToList();
                if (transferType == ETransferType.Send)
                    listOutStoreID.Add(cboFromStoreID.StoreID);
                else
                    listOutStoreID.AddRange(importedData.Rows[0].ItemArray
                        .Where(d => !string.IsNullOrEmpty(d.GetString()) && d.IsNumber())
                        .Select(d => Convert.ToInt32(d)).Distinct().ToList());
                processStoreID = importedData.Rows[0].ItemArray
                        .Where(d => !string.IsNullOrEmpty(d.GetString()) && d.IsNumber())
                        .Select(d => Convert.ToInt32(d)).Distinct().ToList();
                psProgress.Value = 0;
                psProgress.Maximum = processStoreID.Count;
                sttFooter.Visible = true;
                importedData.Rows.Remove(importedData.Rows[0]);
                listProductID.AddRange(importedData.AsEnumerable().Where(d => !string.IsNullOrEmpty(d[0].GetString())).Select(d => d[0].ToString()).Distinct().ToList());
                listProductState.AddRange(importedData.AsEnumerable().Where(d => !string.IsNullOrEmpty(d[1].GetString())).Select(d => d[1].ToString()).Distinct().ToList());

                //Product _ current instock
                List<int> listTotalStoreID = new List<int>();
                listTotalStoreID.AddRange(processStoreID);
                listTotalStoreID.Add(cboFromStoreID.StoreID);
                DataTable Product_Instock = GetStoreInStock(listProductID.Where(d => d.Length <= 20).ToList(), listTotalStoreID);
                if (Product_Instock != null)
                    Product_Instock.PrimaryKey = new DataColumn[] { Product_Instock.Columns["STOREID"], Product_Instock.Columns["PRODUCTID"], Product_Instock.Columns["INSTOCKSTATUSID"] };

                Product_Instock.AsEnumerable().ToList().ForEach(d =>
                {
                    RealTimeQuantityInstock.Add(new RealTimeProductInStock()
                    {
                        ProductID = d["PRODUCTID"].GetString(),
                        Quantity = Convert.ToDecimal(d["QUANTITY"]),
                        InStockStatusID = Convert.ToInt32(d["INSTOCKSTATUSID"]),
                        StoreID = Convert.ToInt32(d["STOREID"])
                    });
                });

                processStoreID.ForEach(targetStoreID =>
                {
                    List<PrimaryKeyMapper> mappers = new List<PrimaryKeyMapper>();
                    foreach (var d in importedData.AsEnumerable().ToList())
                    {
                        if (!string.IsNullOrEmpty(d["STOREID_" + targetStoreID].GetString()))
                        {
                            mappers.Add(new PrimaryKeyMapper()
                            {
                                ProductID = d["PRODUCTID"].GetString(),
                                StoreID = targetStoreID,
                                Quantity = string.IsNullOrEmpty(d["STOREID_" + targetStoreID].GetString()) ? 0.ToString() : d["STOREID_" + targetStoreID].GetString(),
                                ProductState = d[1].GetString()
                            });
                        }
                    }
                    //mappers.AddRange(importedData.AsEnumerable().Select(d => new PrimaryKeyMapper()
                    //{
                    //    ProductID = d["PRODUCTID"].GetString(),
                    //    StoreID = targetStoreID,
                    //    Quantity = string.IsNullOrEmpty(d["STOREID_" + targetStoreID].GetString()) ? 0.ToString() : d["STOREID_" + targetStoreID].GetString(),
                    //    ProductState = d[1].GetString()
                    //}).ToList());
                    mappers.Where(d => string.IsNullOrEmpty(d.ProductState)).ToList().ForEach(d => { d.Message += "Trạng thái sản phẩm không được bỏ trống"; d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid; });
                    mappers.Where(d => string.IsNullOrEmpty(d.ProductID)).ToList().ForEach(d => { d.Message += "|Mã sản phẩm không được bỏ trống"; d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid; });
                    mappers.Where(d => !string.IsNullOrEmpty(d.Quantity) && !d.Quantity.IsNumber()).ToList().ForEach(d => { d.Message += "|Số lượng phải là kiểu số"; d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid; });

                    mappers.Where(d => string.IsNullOrEmpty(d.Message) && Convert.ToDouble(d.Quantity) < 0).ToList().ForEach(d => { d.Message = "Vui lòng nhập số lượng lớn hơn 0"; d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid; });
                    mappers.Where(d => string.IsNullOrEmpty(d.Message) && Convert.ToDouble(d.Quantity) > Int32.MaxValue).ToList().ForEach(d => { d.Message = "Maximum số lượng: " + Int32.MaxValue; d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid; });
                    mappers.Where(d => string.IsNullOrEmpty(d.Message) && !d.ProductState.IsNumber()).ToList().ForEach(d => { d.Message = "Trạng thái sản phẩm sai định dạng"; d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid; });
                    mappers.Where(d => string.IsNullOrEmpty(d.Message) && d.ProductID.Length > 20).ToList().ForEach(d => { d.Message = "Mã sản phẩm vượt quá độ dài cho phép: 20"; d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid; });
                    mappers.Where(d => string.IsNullOrEmpty(d.Message) && Convert.ToDecimal(d.Quantity) <= 0).ToList().ForEach(d => { d.Message = "Vui lòng nhập số lượng > 0"; d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid; });
                    if (DataSourceProduct != null && DataSourceProduct.Rows.Count > 0)
                        mappers.Where(d => string.IsNullOrEmpty(d.Message) && DataSourceProduct.Rows.Find(d.ProductID) == null).ToList().ForEach(d => { d.Message = "Mã sản phẩm không tồn tại trên hệ thống"; d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid; });
                    else
                        mappers.Where(d => string.IsNullOrEmpty(d.Message)).ToList().ForEach(d => { d.Message = "Mã sản phẩm không tồn tại trên hệ thống"; d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid; });
                    if (DataSourceProductState != null && DataSourceProductState.Rows.Count > 0)
                        mappers.Where(d => string.IsNullOrEmpty(d.Message) && DataSourceProductState.Rows.Find(d.ProductState) == null).ToList().ForEach(d => { d.Message = "Trạng thái sản phẩm chưa được định nghĩa"; d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid; });
                    else
                        mappers.Where(d => string.IsNullOrEmpty(d.Message)).ToList().ForEach(d => { d.Message = "Trạng thái sản phẩm chưa được định nghĩa"; d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid; });

                    mappers.Where(d => string.IsNullOrEmpty(d.Message) && d.StoreID == cboFromStoreID.StoreID).ToList().ForEach(d => { d.Message = "Mã kho xuất không được trùng với mã kho nhập"; d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid; });
                    mappers.Where(d => string.IsNullOrEmpty(d.Message) &&
                        mappers.Where(e => e.ProductID == d.ProductID && e.ProductState == d.ProductState && e.StoreID == d.StoreID).ToList().Count > 1)
                        .ToList().ForEach(d => { d.Message = "Trùng lặp dữ liệu"; d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid; });
                    if (mappers.Any() && listProductState.Any())
                    {
                        listProductState.ForEach(ps =>
                        {
                            ERP.Inventory.PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext objStoreChangeContext = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext();
                            objStoreChangeContext.ImportFromStoreID = transferType == ETransferType.Send ? cboFromStoreID.StoreID.ToString() : targetStoreID.ToString();
                            objStoreChangeContext.ImportToStoreID = transferType == ETransferType.Send ? targetStoreID.ToString() : cboFromStoreID.StoreID.ToString();
                            objStoreChangeContext.ImportProductStateID = ps;
                            DataRow stateInfo = null;
                            if (DataSourceProductState != null && DataSourceProductState.Rows.Count > 0)
                                stateInfo = DataSourceProductState.Rows.Find(ps);
                            if (stateInfo != null)
                            {
                                objStoreChangeContext.ImportProductStateName = stateInfo["PRODUCTSTATUSNAME"].GetString();
                                objStoreChangeContext.ProductState = Convert.ToInt32(stateInfo["PRODUCTSTATUSID"]);
                            }

                            objStoreChangeContext.FromStore = Convert.ToInt32(objStoreChangeContext.ImportFromStoreID);
                            objStoreChangeContext.ToStore = Convert.ToInt32(objStoreChangeContext.ImportToStoreID);
                            DataRow fromStoreInfo = null;
                            DataRow toStoreInfo = null;
                            if (DataSourceStore != null && DataSourceStore.Rows.Count > 0)
                            {
                                fromStoreInfo = DataSourceStore.Rows.Find(objStoreChangeContext.FromStore);
                                toStoreInfo = DataSourceStore.Rows.Find(objStoreChangeContext.ToStore);
                            }
                            objStoreChangeContext.FromStoreName = objStoreChangeContext.FromStore + " - " + fromStoreInfo["STORENAME"].GetString();
                            objStoreChangeContext.ToStoreName = objStoreChangeContext.ToStore + " - " + toStoreInfo["STORENAME"].GetString();


                            List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail> details = new
                             List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail>();
                            foreach (var mapper in mappers.Where(d => d.ProductState == ps).ToList())
                            {
                                DataRow productInfo = null;
                                if (mapper.ImportStatus != PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid)
                                {
                                    if (DataSourceProduct != null && DataSourceProduct.Rows.Count > 0)
                                        productInfo = DataSourceProduct.Rows.Find(mapper.ProductID);
                                    mapper.ProductName = (productInfo != null ? productInfo["PRODUCTNAME"].GetString() : string.Empty);
                                }

                                //In-Out store permission based on maingrorup
                                if (mapper.ImportStatus != PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid)
                                {
                                    StringBuilder mainGroupInvalidMessage = new StringBuilder();
                                    int mainGroupID = Convert.ToInt32(productInfo["MAINGROUPID"]);
                                    DataRow mainGroupInfo = null;
                                    if (DataSourceMainGroup != null && DataSourceMainGroup.Rows.Count > 0)
                                        mainGroupInfo = DataSourceMainGroup.Rows.Find(mainGroupID);
                                    if (!listSelectedMainGroupID.Any(d => d == mainGroupID))
                                        mainGroupInvalidMessage.AppendLine("Sản phẩm này không thuộc về danh sách ngành hàng đang chọn");
                                    if (!IsMainGroupPermission(objStoreChangeContext.FromStore, mainGroupID, EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                                        mainGroupInvalidMessage.AppendLine("Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + mainGroupInfo["MAINGROUPNAME"]);
                                    if (!IsMainGroupPermission(objStoreChangeContext.ToStore, mainGroupID, EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                                        mainGroupInvalidMessage.AppendLine("Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + mainGroupInfo["MAINGROUPNAME"]);
                                    if (!CheckPermission(mainGroupID))
                                        mainGroupInvalidMessage.AppendLine("Bạn không có quyền thao tác trên ngành hàng " + (mainGroupInfo != null ? mainGroupInfo["MAINGROUPNAME"].GetString() : string.Empty));

                                    if (mainGroupInvalidMessage.Length > 0)
                                    {
                                        mapper.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                        mapper.Message = mainGroupInvalidMessage.ToString();
                                    }
                                }

                                //Kiêm tra và chia lại số lượng
                                if (mapper.ImportStatus != PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid)
                                {
                                    int storeIDToCheck = Convert.ToInt32(objStoreChangeContext.ImportFromStoreID);
                                    decimal autoQuantity = 0;
                                    var realtimeInstockInfo = RealTimeQuantityInstock.Where(d => d.StoreID == storeIDToCheck && d.InStockStatusID == Convert.ToInt32(mapper.ProductState) && d.ProductID.Trim() == mapper.ProductID).FirstOrDefault();
                                    if (realtimeInstockInfo == null || realtimeInstockInfo.Quantity <= 0)
                                    {
                                        mapper.Message = "Sản phẩm không còn tồn kho";
                                        mapper.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                        goto realtimeUpdate;
                                    }
                                    if (!string.IsNullOrEmpty(mapper.Quantity) && mapper.Quantity.IsNumber())
                                    {
                                        if (realtimeInstockInfo.Quantity < Convert.ToDecimal(mapper.Quantity))
                                        {
                                            //Cảnh báo
                                            mapper.Message = "Số lượng chuyển vượt quá số lượng tồn kho";
                                            mapper.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Warning;
                                            autoQuantity = realtimeInstockInfo.Quantity;
                                        }
                                        else
                                        {
                                            //Ok
                                            autoQuantity = Convert.ToDecimal(mapper.Quantity);
                                        }
                                        goto realtimeUpdate;
                                    }

                                realtimeUpdate:
                                    {
                                        //Xác nhấn số lượng chia
                                        mapper.AutoQuantity = autoQuantity;
                                        //Cập nhật lại số lượng tồn ảo
                                        if (realtimeInstockInfo != null)
                                            realtimeInstockInfo.Quantity -= autoQuantity;
                                    }
                                }

                                DataRow fromInStock = null;
                                DataRow toInStock = null;

                                if (stateInfo != null && !string.IsNullOrEmpty(mapper.ProductID))
                                {
                                    fromInStock = Product_Instock.Rows.Find(new object[] { objStoreChangeContext.FromStore, mapper.ProductID, stateInfo["PRODUCTSTATUSID"] });
                                    toInStock = Product_Instock.Rows.Find(new object[] { objStoreChangeContext.ToStore, mapper.ProductID, stateInfo["PRODUCTSTATUSID"] });
                                }

                                details.Add(new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail()
                                {
                                    ProductID = mapper.ProductID,
                                    ProductName = mapper.ProductName,
                                    ImportQuantity = mapper.Quantity,
                                    IsManualAdd = false,
                                    AutoQuantity = mapper.AutoQuantity,
                                    Quantity = mapper.AutoQuantity,
                                    ImportStatus = mapper.ImportStatus,
                                    ImportMessage = mapper.Message,
                                    FromInStockQuantity = fromInStock == null ? 0 : Convert.ToDecimal(fromInStock["QUANTITY"]),
                                    ToInStockQuantity = toInStock == null ? 0 : Convert.ToDecimal(toInStock["QUANTITY"]),
                                });
                            }
                            objStoreChangeContext.Detail = details.ToArray();
                            if (objStoreChangeContext.Detail != null && objStoreChangeContext.Detail.Any())
                                importResult.Add(objStoreChangeContext);
                        });
                    }
                    psProgress.Value = (psProgress.Value + 1);
                    Application.DoEvents();
                });
                ListAutoStoreChangeContext = importResult;
                ShowImportResult();
            }
            finally
            {
                EndSuspendForm();
                psProgress.Value = 0;
                sttFooter.Visible = false;
            }
        }
        /// <summary>
        /// Import chuyển kho từ nhiều kho tới nhiều kho từ file excel
        /// </summary>
        private void ImportMulti()
        {
            try
            {
                DataTable importedData = C1ExcelObject.OpenExcel2DataTable();
                importedData = TrimDataTable(importedData);
                //Open file dialog has been cancelled
                if (importedData == null)
                    return;
                if (!IsValidImportMultiTemplate(importedData))
                    return;
                BeginSuspendForm();
                List<int> listSelectedMainGroupID = GetSelectedMainGroupID();
                #region re-format datatable

                //Rename columns
                importedData.Columns[XlsMultiColumnMapper.FromStoreID].ColumnName = "FROMSTOREID";
                importedData.Columns[XlsMultiColumnMapper.ProductID].ColumnName = "PRODUCTID";
                importedData.Columns[XlsMultiColumnMapper.ProductState].ColumnName = "PRODUCTSTATE";
                importedData.Columns[XlsMultiColumnMapper.Quantity].ColumnName = "QUANTITY";
                importedData.Columns[XlsMultiColumnMapper.ToStoreID].ColumnName = "TOSTOREID";

                //remove các column bị dư
                while (importedData.Columns.Count > 5)
                    importedData.Columns.RemoveAt(5);

                importedData.Columns.Add("ProductName");
                importedData.Columns.Add("FromStoreName");
                importedData.Columns.Add("ToStoreName");
                importedData.Columns.Add("FromInStockQuantity");
                importedData.Columns.Add("ToInStockQuantity");
                importedData.Columns.Add("IMEI");
                importedData.Columns.Add("IsUrgent");
                importedData.Columns.Add("AutoQuantity");
                importedData.Columns.Add("ProductStateName");
                importedData.Columns.Add("Status");
                importedData.Columns.Add("StatusID");
                importedData.Columns.Add("ErrorContent");

                //Remove caption row
                importedData.Rows.Remove(importedData.Rows[0]);

                if (IsAllowOldCode)
                {
                    if (DataSourceOldStore != null)
                    {
                        importedData.AsEnumerable().Where(d => !string.IsNullOrEmpty(d[XlsMultiColumnMapper.FromStoreID].GetString()) && d[XlsMultiColumnMapper.FromStoreID].IsNumber()).ToList().ForEach(row =>
                        {
                            int storeID = Convert.ToInt32(row[XlsMultiColumnMapper.FromStoreID]);
                            var rowSel = DataSourceOldStore.AsEnumerable().Where(d => Convert.ToInt32(d["OLDSTOREID"]) == storeID).FirstOrDefault();
                            if (rowSel != null)
                                row[XlsMultiColumnMapper.FromStoreID] = Convert.ToInt32(rowSel["STOREID"]);
                        });
                    }

                    if (DataSourceOldProduct != null)
                    {
                        importedData.AsEnumerable().Where(d => !string.IsNullOrEmpty(d[XlsMultiColumnMapper.ProductID].GetString())).ToList().ForEach(row =>
                        {
                            var rowSel = DataSourceOldProduct.AsEnumerable().Where(d => d["PRODUCTIDOLD"].GetString() == row[XlsMultiColumnMapper.ProductID].GetString()).FirstOrDefault();
                            if (rowSel != null)
                                row[XlsMultiColumnMapper.ProductID] = rowSel["PRODUCTID"];
                        });
                    }
                }

                #endregion

                StringBuilder recordMessage = new StringBuilder();
                //Kiểm tra trống, format
                importedData.AsEnumerable().ToList().ForEach(record =>
                {
                    recordMessage.Clear();
                    //Kiểm tra mã kho chuyển
                    if (string.IsNullOrEmpty(record[XlsMultiColumnMapper.FromStoreID].GetString()))
                        recordMessage.AppendLine("Mã kho chuyển không được bỏ trống!");
                    else if (!record[XlsMultiColumnMapper.FromStoreID].IsNumber())
                        recordMessage.AppendLine("Mã kho chuyển sai định dạng");
                    else if (record[XlsMultiColumnMapper.FromStoreID].GetString().Length > Int32.MaxValue.ToString().Length)
                        recordMessage.AppendLine("Mã kho chuyển vượt quá số ký tự cho phép: " + Int32.MaxValue.ToString().Length);

                    //kiểm tra trạng thái
                    if (string.IsNullOrEmpty(record[XlsMultiColumnMapper.ProductState].GetString()))
                        recordMessage.AppendLine("Trạng thái sản phẩm không được bỏ trống!");
                    else if (!record[XlsMultiColumnMapper.ProductState].IsNumber())
                        recordMessage.AppendLine("Trạng thái sản phẩm phải là kiểu số!");

                    //Kiểm tra sản phẩm chuyển
                    if (string.IsNullOrEmpty(record[XlsMultiColumnMapper.ProductID].GetString()))
                        recordMessage.AppendLine("Mã sản phẩm không được bỏ trống!");
                    else if (record[XlsMultiColumnMapper.ProductID].GetString().Length > 20)
                        recordMessage.AppendLine("Mã sản phẩm vượt quá số ký tự cho phép: 20");

                    //Kiểm tra số lượng chuyển
                    if (string.IsNullOrEmpty(record[XlsMultiColumnMapper.Quantity].GetString()))
                        recordMessage.AppendLine("Số lượng không được bỏ trống!");
                    else if (!record[XlsMultiColumnMapper.Quantity].IsNumber())
                        recordMessage.AppendLine("Số lượng phải là kiểu số");
                    else if (Convert.ToDecimal(record[XlsMultiColumnMapper.Quantity]) <= 0)
                        recordMessage.AppendLine("Số lượng chuyển phải lớn hơn 0");

                    //Kiểm tra kho nhập
                    if (string.IsNullOrEmpty(record[XlsMultiColumnMapper.ToStoreID].GetString()))
                        recordMessage.AppendLine("Mã kho nhận không được bỏ trống!");
                    else if (!record[XlsMultiColumnMapper.ToStoreID].IsNumber())
                        recordMessage.AppendLine("Mã kho nhận sai định dạng");
                    else if (record[XlsMultiColumnMapper.ToStoreID].GetString().Length > Int32.MaxValue.ToString().Length)
                        recordMessage.AppendLine("Mã kho nhận vượt quá số ký tự cho phép: " + Int32.MaxValue.ToString().Length);
                    bool isError = recordMessage.Length != 0;
                    recordMessage.ToString();
                    if (isError)
                    {
                        record[XlsMultiColumnMapper.StatusID] = 1;
                        record[XlsMultiColumnMapper.ErrorContent] = recordMessage.ToString();
                    }
                });

                recordMessage.Clear();
                importedData.AsEnumerable()
                    .Where(record => string.IsNullOrEmpty(record[XlsMultiColumnMapper.StatusID].GetString())
                        && importedData.AsEnumerable().Where(tempR =>
                            tempR[XlsMultiColumnMapper.FromStoreID].GetString() == record[XlsMultiColumnMapper.FromStoreID].GetString()
                            && tempR[XlsMultiColumnMapper.ProductID].GetString() == record[XlsMultiColumnMapper.ProductID].GetString()
                            && tempR[XlsMultiColumnMapper.ToStoreID].GetString() == record[XlsMultiColumnMapper.ToStoreID].GetString()
                            && tempR[XlsMultiColumnMapper.ProductState].GetString() == record[XlsMultiColumnMapper.ProductState].GetString()
                            ).ToList().Count > 1)
                            .ToList().ForEach(record =>
                            {
                                record[XlsMultiColumnMapper.Status] = "Lỗi";
                                record[XlsMultiColumnMapper.StatusID] = 1;
                                record[XlsMultiColumnMapper.ErrorContent] = "Dữ liệu bị trùng lặp";
                            });

                recordMessage.Clear();
                importedData.AsEnumerable().Where(record => string.IsNullOrEmpty(record[XlsMultiColumnMapper.ErrorContent].GetString())).ToList().ForEach(record =>
                {
                    recordMessage.Clear();
                    DataRow rowOutStore = null;
                    DataRow rowInStore = null;
                    if (DataSourceStore != null && DataSourceStore.Rows.Count > 0)
                    {
                        rowOutStore = DataSourceStore.Rows.Find(Convert.ToInt32(record[XlsMultiColumnMapper.FromStoreID]));
                        rowInStore = DataSourceStore.Rows.Find(Convert.ToInt32(record[XlsMultiColumnMapper.ToStoreID]));
                    }
                    DataRow rowProduct = null;
                    if (DataSourceProduct != null && DataSourceProduct.Rows.Count > 0)
                        rowProduct = DataSourceProduct.Rows.Find(record[XlsMultiColumnMapper.ProductID].GetString());
                    DataRow rowState = null;
                    if (DataSourceProductState != null && DataSourceProductState.Rows.Count > 0)
                        rowState = DataSourceProductState.Rows.Find(Convert.ToInt32(record[XlsMultiColumnMapper.ProductState]));

                    if (rowState == null)
                        recordMessage.AppendLine("Mã trạng thái " + record[XlsMultiColumnMapper.ProductState] + " chưa được định nghĩa!");
                    else record[XlsMultiColumnMapper.ProductStateName] = rowState["PRODUCTSTATUSNAME"].GetString();
                    if (rowOutStore == null)
                        recordMessage.AppendLine("Mã kho xuất " + record[XlsMultiColumnMapper.FromStoreID] + " không tồn tại trên hệ thống!");
                    else
                    {
                        record[XlsMultiColumnMapper.FromStoreName] = rowOutStore["STORENAME"].GetString();
                        if (rowProduct != null && !CheckMainGroupPermission(Convert.ToInt32(record[XlsMultiColumnMapper.FromStoreID].ToString()),
                           Convert.ToInt32(rowProduct["MAINGROUPID"]), EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                            recordMessage.AppendLine("Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + rowProduct["MAINGROUPNAME"]);
                    }

                    if (rowInStore == null)
                        recordMessage.AppendLine("Mã kho nhập " + record[XlsMultiColumnMapper.ToStoreID] + " không tồn tại trên hệ thống!");
                    else
                    {
                        record[XlsMultiColumnMapper.ToStoreName] = rowInStore["STORENAME"].GetString();
                        if (rowProduct != null && !CheckMainGroupPermission(Convert.ToInt32(record[XlsMultiColumnMapper.ToStoreID].ToString()),
                            Convert.ToInt32(rowProduct["MAINGROUPID"]), EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                            recordMessage.AppendLine("Kho nhập này không được phép nhập kho trên ngành hàng " + rowProduct["MAINGROUPNAME"]);
                    }

                    if (rowProduct != null && !CheckPermission(Convert.ToInt32(rowProduct["MAINGROUPID"])))
                        recordMessage.AppendLine("Bạn không có quyền thao tác trên ngành hàng " + rowProduct["MAINGROUPNAME"]);
                    if (rowProduct != null && !listSelectedMainGroupID.Any(d => d == Convert.ToInt32(rowProduct["MAINGROUPID"])))
                        recordMessage.AppendLine("Sản phẩm này không thuộc danh sách ngành hàng đang chọn");

                    if (rowProduct == null)
                        recordMessage.AppendLine("Mã sản phẩm " + record[XlsMultiColumnMapper.ProductID] + " không tồn tại trên hệ thống");
                    else
                    {
                        record[XlsMultiColumnMapper.ProductName] = rowProduct["PRODUCTNAME"];
                        //if (!string.IsNullOrEmpty(rowProduct["MAINGROUPNAME"].ToString()) && (mainGroupList.Any() && !mainGroupList.Any(d => d.Trim() == intMainGroupID.ToString())))
                        //    recordMessage.AppendLine("Sản phẩm " + record[XlsMultiColumnMapper.ProductID] + " - " + rowProduct["PRODUCTNAME"] + " không thuộc ngành hàng đang xét");
                    }
                    if (recordMessage.Length != 0)
                    {
                        record[XlsMultiColumnMapper.StatusID] = 1;
                        record[XlsMultiColumnMapper.ErrorContent] = recordMessage.ToString();
                    }
                });

                importedData.AsEnumerable()
                    .Where(record => string.IsNullOrEmpty(record[XlsMultiColumnMapper.StatusID].GetString())
                    && record[XlsMultiColumnMapper.FromStoreID].GetString() == record[XlsMultiColumnMapper.ToStoreID].GetString())
                    .ToList().ForEach(record =>
                    {
                        record[XlsMultiColumnMapper.StatusID] = 1;
                        record[XlsMultiColumnMapper.ErrorContent] = "Kho nhập không được trùng với kho xuất";
                    });


                List<string> listProductID = importedData.AsEnumerable().Where(record => string.IsNullOrEmpty(record[XlsMultiColumnMapper.StatusID].GetString())).Select(record => record[XlsMultiColumnMapper.ProductID].GetString()).Distinct().ToList();
                List<int> listFromStoreID = importedData.AsEnumerable().Where(d => !string.IsNullOrEmpty(d[XlsMultiColumnMapper.FromStoreID].GetString()) && d[XlsMultiColumnMapper.FromStoreID].IsNumber()).Select(d => Convert.ToInt32(d[XlsMultiColumnMapper.FromStoreID])).Distinct().ToList();
                List<int> listToStoreID = importedData.AsEnumerable().Where(d => !string.IsNullOrEmpty(d[XlsMultiColumnMapper.ToStoreID].GetString()) && d[XlsMultiColumnMapper.ToStoreID].IsNumber()).Select(d => Convert.ToInt32(d[XlsMultiColumnMapper.ToStoreID])).Distinct().ToList();
                List<int> listStoreIDTotal = new List<int>();
                listStoreIDTotal.AddRange(listFromStoreID);
                listStoreIDTotal.AddRange(listToStoreID);
                listStoreIDTotal = listStoreIDTotal.Distinct().ToList();
                DataTable Product_Instock = GetStoreInStock(listProductID.Where(d => d.Length <= 20).ToList(), listStoreIDTotal);
                List<RealTimeProductInStock> RealTimeQuantityInstock = new List<RealTimeProductInStock>();
                if (Product_Instock != null)
                    Product_Instock.PrimaryKey = new DataColumn[] { Product_Instock.Columns["STOREID"], Product_Instock.Columns["PRODUCTID"], Product_Instock.Columns["INSTOCKSTATUSID"] };
                if (Product_Instock != null)
                {
                    Product_Instock.AsEnumerable().ToList().ForEach(d =>
                    {
                        RealTimeQuantityInstock.Add(new RealTimeProductInStock()
                        {
                            ProductID = d["PRODUCTID"].GetString(),
                            Quantity = Convert.ToDecimal(d["QUANTITY"]),
                            InStockStatusID = Convert.ToInt32(d["INSTOCKSTATUSID"]),
                            StoreID = Convert.ToInt32(d["STOREID"])
                        });
                    });
                }

                importedData.AsEnumerable().Where(record => string.IsNullOrEmpty(record[XlsMultiColumnMapper.StatusID].GetString())).ToList()
                    .ForEach(record =>
                    {
                        recordMessage.Clear();
                        decimal productQuantityInstockFrom = 0;
                        decimal productQuantityInstockTo = 0;
                        decimal autoQuantity = 0;
                        DataRow rowFrom = null;
                        DataRow rowTo = null;
                        RealTimeProductInStock realTimeInstock = null;

                        if (Product_Instock != null)
                        {
                            rowFrom = Product_Instock.Rows.Find(new object[] { Convert.ToInt32(record[XlsMultiColumnMapper.FromStoreID]),
                            record[XlsMultiColumnMapper.ProductID].GetString(), Convert.ToInt32(record[XlsMultiColumnMapper.ProductState])});
                            rowTo = Product_Instock.Rows.Find(new object[] { Convert.ToInt32(record[XlsMultiColumnMapper.ToStoreID]),
                            record[XlsMultiColumnMapper.ProductID].GetString(), Convert.ToInt32(record[XlsMultiColumnMapper.ProductState])});
                            if (rowFrom != null)
                                productQuantityInstockFrom = Convert.ToDecimal(rowFrom["QUANTITY"]);
                            if (rowTo != null)
                                productQuantityInstockTo = Convert.ToDecimal(rowTo["QUANTITY"]);
                        }
                        if (productQuantityInstockFrom <= 0)
                        {
                            record[XlsMultiColumnMapper.StatusID] = 1;
                            recordMessage.AppendLine("Sản phẩm không còn tồn kho");
                            record[XlsMultiColumnMapper.ErrorContent] = recordMessage.ToString();
                        }
                        else
                        {
                            realTimeInstock = RealTimeQuantityInstock.Where(d => d.StoreID == Convert.ToInt32(record[XlsMultiColumnMapper.FromStoreID])
                               && d.InStockStatusID == Convert.ToInt32(record[XlsMultiColumnMapper.ProductState]) && d.ProductID == record[XlsMultiColumnMapper.ProductID].GetString()).FirstOrDefault();
                            if (realTimeInstock == null)
                            {
                                record[XlsMultiColumnMapper.StatusID] = 1;
                                recordMessage.AppendLine("Sản phẩm không còn tồn kho");
                                record[XlsMultiColumnMapper.ErrorContent] = recordMessage.ToString();
                            }
                            else if (realTimeInstock.Quantity <= 0)
                            {
                                record[XlsMultiColumnMapper.StatusID] = 1;
                                recordMessage.AppendLine("Sản phẩm không còn tồn kho");
                                record[XlsMultiColumnMapper.ErrorContent] = recordMessage.ToString();
                            }
                            else if (Convert.ToDecimal(record[XlsMultiColumnMapper.Quantity]) > realTimeInstock.Quantity)
                            {
                                record[XlsMultiColumnMapper.StatusID] = 2;
                                recordMessage.AppendLine("Số lượng chuyển lớn hơn số lượng tồn");
                                record[XlsMultiColumnMapper.ErrorContent] = recordMessage.ToString();
                                autoQuantity = realTimeInstock.Quantity;
                                record[XlsMultiColumnMapper.AutoQuantity] = autoQuantity;
                            }
                            else
                                autoQuantity = Convert.ToDecimal(record[XlsMultiColumnMapper.Quantity]);
                        }

                        if (realTimeInstock != null)
                            realTimeInstock.Quantity = realTimeInstock.Quantity - autoQuantity;
                        record[XlsMultiColumnMapper.FromInStockQuantity] = productQuantityInstockFrom;
                        record[XlsMultiColumnMapper.ToInStockQuantity] = productQuantityInstockTo;
                        record[XlsMultiColumnMapper.AutoQuantity] = autoQuantity;
                    });


                importedData.AsEnumerable().Where(record => string.IsNullOrEmpty(record[XlsMultiColumnMapper.StatusID].GetString())).ToList()
                    .ForEach(record =>
                    {
                        record[XlsMultiColumnMapper.StatusID] = 0;
                    });

                ListAutoStoreChangeContext = ParseToObject(importedData);
                ShowImportResult();
            }
            finally
            {
                sttFooter.Visible = false;
                psProgress.Value = 0;
                lblPs.Text = string.Empty;
                Application.DoEvents();
            }
        }

        private bool CheckMainGroupPermission(int storeID, int mainGroupId, EnumType.StoreMainGroupPermissionType permissionType)
        {
            if (DataSourceStoreMainGroup == null)
            {
                return false;
            }
            StringBuilder arrCondition = new StringBuilder();
            arrCondition.Append("StoreID = " + storeID + " and MainGroupID = " + mainGroupId);
            switch (permissionType)
            {
                case EnumType.StoreMainGroupPermissionType.AUTOSTORECHANGE:
                    arrCondition.Append(" and ISCANAUTOSTORECHANGE = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.INPUT:
                    arrCondition.Append(" and ISCANINPUT = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.INPUTRETURN:
                    arrCondition.Append(" and ISCANINPUTRETURN = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.OUTPUT:
                    arrCondition.Append(" and ISCANOUTPUT = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.PURCHASEORDER:
                    arrCondition.Append(" and ISCANPURCHASEORDER = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.SALEORDER:
                    arrCondition.Append(" and ISCANSALEORDER = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT:
                    arrCondition.Append(" and ISCANSTORECHANGEINPUT = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT:
                    arrCondition.Append(" and ISCANSTORECHANGEOUTPUT = 1");
                    break;
            }
            return DataTableClass.CheckIsExist(DataSourceStoreMainGroup, arrCondition.ToString());
        }

        public bool CheckPermission(int intMainGroupID)
        {
            return true;
        }


        /// <summary>
        /// Parse datatable của multiple import sang list AutoStoreChangeContext
        /// </summary>
        /// <param name="importedData"></param>
        /// <returns></returns>
        private List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> ParseToObject(DataTable importedData)
        {
            List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> result = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext>();
            importedData.AsEnumerable().ToList().ForEach(record =>
            {
                string fromStoreID = record[XlsMultiColumnMapper.FromStoreID].GetString();
                string fromStoreName = record[XlsMultiColumnMapper.FromStoreName].GetString();
                string toStoreID = record[XlsMultiColumnMapper.ToStoreID].GetString();
                string toStoreName = record[XlsMultiColumnMapper.ToStoreName].GetString();
                string productID = record[XlsMultiColumnMapper.ProductID].GetString();
                string productState = record[XlsMultiColumnMapper.ProductState].GetString();
                string productStateName = record[XlsMultiColumnMapper.ProductStateName].GetString();
                string importQuantity = record[XlsMultiColumnMapper.Quantity].GetString();
                string autoQuantity = record[XlsMultiColumnMapper.AutoQuantity].GetString();
                string fromInstockQuantity = record[XlsMultiColumnMapper.FromInStockQuantity].GetString();
                string toInstockQuantity = record[XlsMultiColumnMapper.ToInStockQuantity].GetString();
                PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext objAutoStoreChange = result.Where(d => d.ImportFromStoreID == fromStoreID
                && d.ImportToStoreID == toStoreID && d.ImportProductStateID == productState).FirstOrDefault();
                bool isNew = false;
                if (objAutoStoreChange == null)
                {
                    isNew = true;
                    objAutoStoreChange = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext()
                    {
                        FromStore = fromStoreID.IsNumber() ? Convert.ToInt32(fromStoreID) : 0,
                        ToStore = toStoreID.IsNumber() ? Convert.ToInt32(toStoreID) : 0,
                        ImportFromStoreID = fromStoreID,
                        ImportToStoreID = toStoreID,
                        FromStoreName = (!string.IsNullOrEmpty(fromStoreID) && !string.IsNullOrEmpty(fromStoreName)) ? fromStoreID + " - " + fromStoreName : string.Empty,
                        ToStoreName = (!string.IsNullOrEmpty(toStoreID) && !string.IsNullOrEmpty(toStoreName)) ? toStoreID + " - " + toStoreName : string.Empty,
                        IsUrgent = false,
                        StoreChangeTypeID = (int)ETransferType.Send,
                        ProductState = productState.IsNumber() ? Convert.ToInt32(productState) : 0,
                        ImportProductStateID = productState,
                        ImportProductStateName = productStateName
                    };
                }
                if (objAutoStoreChange.Detail == null)
                    objAutoStoreChange.Detail = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail[] { };

                List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail> details = objAutoStoreChange.Detail.ToList();
                details.Add(new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail()
                {
                    ProductID = record[XlsMultiColumnMapper.ProductID].GetString(),
                    ProductName = record[XlsMultiColumnMapper.ProductName].GetString(),
                    FromInStockQuantity = record[XlsMultiColumnMapper.FromInStockQuantity].IsNumber() ? Convert.ToDecimal(record[XlsMultiColumnMapper.FromInStockQuantity]) : 0,
                    ToInStockQuantity = record[XlsMultiColumnMapper.ToInStockQuantity].IsNumber() ? Convert.ToDecimal(record[XlsMultiColumnMapper.ToInStockQuantity]) : 0,
                    ImportQuantity = record[XlsMultiColumnMapper.Quantity].GetString(),
                    Quantity = record[XlsMultiColumnMapper.Quantity].IsNumber() ? Convert.ToDecimal(record[XlsMultiColumnMapper.Quantity]) : 0,
                    AutoQuantity = record[XlsMultiColumnMapper.AutoQuantity].IsNumber() ? Convert.ToDecimal(record[XlsMultiColumnMapper.AutoQuantity]) : 0,
                    IsManualAdd = false,
                    ImportStatus = (PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult)Convert.ToInt32(record[XlsMultiColumnMapper.StatusID]),
                    ImportMessage = record[XlsMultiColumnMapper.ErrorContent].GetString()
                });
                objAutoStoreChange.Detail = details.ToArray();
                if (isNew)
                {
                    result.Add(objAutoStoreChange);
                }
            });
            return result;
        }

        /// <summary>
        /// Hiển thị kết quả import
        /// </summary>
        private void ShowImportResult(bool fromExcel = true)
        {
            new frmShowImportResult2(ListAutoStoreChangeContext, fromExcel).ShowDialog();
            ListAutoStoreChangeContext = GetIsValidData(ListAutoStoreChangeContext);
            BindingData();
        }

        /// <summary>
        /// Bo cac dong khong hop le va chuyen cac dong canh bao thanh cac dong hop le
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> GetIsValidData(List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> list)
        {
            foreach (var item in list)
            {
                if (item.Detail != null)
                {
                    var listDetail = item.Detail.ToList();
                    listDetail.Where(d => d.ImportStatus == PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Warning).ToList().ForEach(d =>
                    { d.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.IsValid; });
                    while (listDetail.FirstOrDefault(d => d.ImportStatus == PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid) != null)
                    { listDetail.Remove(listDetail.FirstOrDefault(d => d.ImportStatus == PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid)); }
                    item.Detail = listDetail.ToArray();
                }
            }
            list.RemoveAll(d => d.Detail == null || !d.Detail.Any());
            return list;
        }

        /// <summary>
        /// Kiểm tra quyền nhập|xuất của kho trên ngành hàng
        /// </summary>
        /// <param name="intStoreID"></param>
        /// <param name="intMainGroupID"></param>
        /// <param name="permissionType"></param>
        /// <returns></returns>
        private bool IsMainGroupPermission(int intStoreID, int intMainGroupID, EnumType.StoreMainGroupPermissionType permissionType)
        {
            if (DataSourceMainGroupStore == null)
                return false;

            StringBuilder arrCondition = new StringBuilder();
            arrCondition.Append("StoreID = " + intStoreID + " and MainGroupID = " + intMainGroupID);
            switch (permissionType)
            {
                case EnumType.StoreMainGroupPermissionType.AUTOSTORECHANGE:
                    arrCondition.Append(" and ISCANAUTOSTORECHANGE = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.INPUT:
                    arrCondition.Append(" and ISCANINPUT = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.INPUTRETURN:
                    arrCondition.Append(" and ISCANINPUTRETURN = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.OUTPUT:
                    arrCondition.Append(" and ISCANOUTPUT = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.PURCHASEORDER:
                    arrCondition.Append(" and ISCANPURCHASEORDER = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.SALEORDER:
                    arrCondition.Append(" and ISCANSALEORDER = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT:
                    arrCondition.Append(" and ISCANSTORECHANGEINPUT = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT:
                    arrCondition.Append(" and ISCANSTORECHANGEOUTPUT = 1");
                    break;
            }
            return DataTableClass.CheckIsExist(DataSourceMainGroupStore, arrCondition.ToString());
        }

        /// <summary>
        /// Reset form, datasource
        /// </summary>
        private void ResetDataSource(bool isOnlySelected = false)
        {
            if (isOnlySelected)
            {
                ListAutoStoreChangeContext.ForEach(d => d.RemoveDetailIsSelected(true));
                ListAutoStoreChangeContext.RemoveAll(d => d.IsEmptyDetail);
            }
            else
                ListAutoStoreChangeContext.Clear();
            BindingData();

            LeadCommandID = objPLCStoreChangeCommand.GetStoreChangeCommandNewID(SystemConfig.intDefaultStoreID);
            txtLeadCommandID.Text = LeadCommandID;

        }

        /// <summary>
        /// Lock form để thực hiện tác vụ
        /// </summary>
        private void BeginSuspendForm()
        {
            grpSearch.Enabled = grdData.Enabled = false;
        }

        /// <summary>
        /// Unlock form
        /// </summary>
        private void EndSuspendForm()
        {
            grpSearch.Enabled = grdData.Enabled = true;
        }

        /// <summary>
        /// Lấy thông tin tồn kho
        /// </summary>
        /// <param name="productIDs"></param>
        /// <param name="storeIDs"></param>
        /// <param name="inStockStatusIDList"></param>
        /// <returns></returns>
        private DataTable GetStoreInStock(List<string> productIDs, List<int> storeIDs, List<int> inStockStatusIDList = null)
        {
            DataTable result = null;
            if (!productIDs.Any() || !storeIDs.Any())
                return result;

            if (inStockStatusIDList == null)
                inStockStatusIDList = DataSourceProductState.AsEnumerable().ToList().Select(d => Convert.ToInt32(d["PRODUCTSTATUSID"])).ToList();

            string strProductIDList = string.Empty;
            string strStoreIDList = string.Empty;
            string strInStockStatusList = string.Empty;
            productIDs.Where(d => d.Length <= 20).ToList().ForEach(prodID => { strProductIDList += prodID + ","; });
            storeIDs.ForEach(storeID => { strStoreIDList += storeID.ToString() + ","; });
            inStockStatusIDList.ForEach(status => { strInStockStatusList += (status + ","); });
            object[] objKeywords = new object[]{"@PRODUCTIDLIST", strProductIDList,
                                                "@STOREIDLIST", strStoreIDList,
                                                "@ISCHECKREALINPUT", Convert.ToInt32(!IsIncludedRoaming),
                                                "@InStockStatusIDList", strInStockStatusList ,
                                                };
            result = objPLCReportDataSource.GetDataSource("PM_STOCKREPORT_AUTOCHANGE", objKeywords);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                MessageBoxObject.ShowWarningMessage(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            return result;
        }

        private bool CheckInputCreateOrder(ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType, int intFromStoreID, int intToStoreID)
        {
            DataRow rowFromStore = null;
            DataRow rowToStore = null;
            if (DataSourceStore != null && DataSourceStore.Rows.Count > 0)
            {
                rowFromStore = rowFromStore = DataSourceStore.Rows.Find(intFromStoreID);
                rowToStore = DataSourceStore.Rows.Find(intToStoreID);
            }
            // 1 Kiem tra cong ty
            if (objStoreChangeType.CheckCompanyType == 1)
            {
                if (Convert.ToInt32(rowFromStore["COMPANYID"]) != Convert.ToInt32(rowToStore["COMPANYID"]))
                    return false;
            }
            else if (objStoreChangeType.CheckCompanyType == 2)
            {
                if (Convert.ToInt32(rowFromStore["COMPANYID"]) == Convert.ToInt32(rowToStore["COMPANYID"]))
                    return false;
            }
            // 2 kiem tra tỉnh
            if (objStoreChangeType.CheckProvinceType == 1)
            {
                if (Convert.ToInt32(rowFromStore["PROVINCEID"]) != Convert.ToInt32(rowToStore["PROVINCEID"]))
                    return false;
            }
            else if (objStoreChangeType.CheckProvinceType == 2)
            {
                if (Convert.ToInt32(rowFromStore["PROVINCEID"]) == Convert.ToInt32(rowToStore["PROVINCEID"]))
                    return false;
            }

            // 3 Kiem tra chi nhánh
            if (objStoreChangeType.CheckBranchType == 1)
            {
                if (Convert.ToInt32(rowFromStore["BRANCHID"]) != Convert.ToInt32(rowToStore["BRANCHID"]))
                    return false;
            }
            else if (objStoreChangeType.CheckBranchType == 2)
            {
                if (Convert.ToInt32(rowFromStore["BRANCHID"]) == Convert.ToInt32(rowToStore["BRANCHID"]))
                    return false;
            }

            return true;

        }
        private List<int> GetConsignmentType(int storeChangeOrderTypeID)
        {
            List<int> listProdConsignmentType = new List<int>();
            DataTable dtb = new Report.PLC.PLCReportDataSource().GetDataSource("GET_TORECHANGEORDERTYPE_PRODT", new object[] { "@STORECHANGEORDERTYPEID", storeChangeOrderTypeID });
            if (dtb != null)
            {
                listProdConsignmentType = dtb.AsEnumerable().Select(d => Convert.ToInt32(d["PRODUCTCONSIGNMENTTYPEID"])).ToList();
            }
            return listProdConsignmentType;
        }

        #endregion

        #region Class helper
        private class StoreIMEIs
        {
            public int StoreID { get; set; }
            public List<string> ListIMEI { get; set; }
        }
        private class PrimaryKeyMapper
        {
            public string ProductID { get; set; }
            public string ProductName { get; set; }
            public int StoreID { get; set; }
            public string ProductState { get; set; }
            //Số lượng nhập vào
            public string Quantity { get; set; }
            //Số lượng hệ thống tự động chia
            public decimal AutoQuantity { get; set; }
            public ERP.Inventory.PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult ImportStatus { get; set; }
            public string Message { get; set; }
        }
        private class RealTimeProductInStock
        {
            public string ProductID { get; set; }
            public int StoreID { get; set; }
            public int InStockStatusID { get; set; }
            public decimal Quantity { get; set; }
        }
        #endregion

        private bool IsValidRequireFieldBeforeAdding()
        {
            if (cboFromStoreID.StoreID <= 0)
            {
                string strMess = "Vui lòng chọn kho xuất";
                if (cboType.SelectedIndex == 1)
                    strMess = "Vui lòng chọn kho nhập";
                MessageBox.Show(this, strMess, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboFromStoreID.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(cboToStoreID.StoreIDList))
            {
                string strMess = "Vui lòng chọn kho nhập";
                if (cboType.SelectedIndex == 1)
                    strMess = "Vui lòng chọn kho xuất";
                MessageBox.Show(this, strMess, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboToStoreID.Focus();
                return false;
            }
            string[] strArrStoreID = cboToStoreID.StoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
            if (strArrStoreID.Contains(cboFromStoreID.StoreID.ToString().Trim()))
            {
                MessageBox.Show("Kho nhập phải khác với kho xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboToStoreID.Focus();
                return false;
            }
            string[] strArrProductID = cboProductList.ProductIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);

            if (string.IsNullOrEmpty(strArrProductID[0].ToString()))
            {
                MessageBox.Show(this, "Vui lòng chọn ít nhất một sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboProductList.Focus();
                return false;
            }
            if (Convert.ToDecimal(txtQuantity.Value) <= 0)
            {
                MessageBox.Show(this, "Vui lòng nhập số lượng cần chuyển", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtQuantity.Focus();
                return false;
            }
            return true;
        }
        private List<int> GetSelectedProductStates()
        {
            List<int> listSelectedProductState = new List<int>();
            try
            {
                if (!string.IsNullOrEmpty(cboProductState.ColumnIDList))
                {
                    listSelectedProductState = cboProductState.ColumnIDList.
                        Replace("<", string.Empty).Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(d => Convert.ToInt32(d)).ToList();
                }
                else
                {
                    listSelectedProductState = DataSourceProductState.AsEnumerable().Select(d => Convert.ToInt32(d["PRODUCTSTATUSID"])).ToList();
                }
            }
            catch { }
            return listSelectedProductState;
        }
        private List<int> GetSelectedStoreID()
        {
            List<int> result = new List<int>();
            try
            {
                if (!string.IsNullOrEmpty(cboToStoreID.StoreIDList))
                {
                    result = cboToStoreID.StoreIDList.
                        Replace("<", string.Empty).Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(d => Convert.ToInt32(d)).ToList();
                }
            }
            catch { }
            return result;
        }
        private List<string> GetSelectedProductID()
        {
            List<string> listSelectedProductID = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(cboProductList.ProductIDList))
                {
                    listSelectedProductID = cboProductList.ProductIDList.
                        Replace("<", string.Empty).Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(d => d).ToList();
                }
            }
            catch { }
            return listSelectedProductID;
        }
        private List<int> GetSelectedMainGroupID()
        {
            List<int> result = new List<int>();
            try
            {
                if (!string.IsNullOrEmpty(cboMainGroupID.MainGroupIDList))
                {
                    result = cboMainGroupID.MainGroupIDList.Replace("<", string.Empty).Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(d => Convert.ToInt32(d)).ToList();
                }
                else
                {
                    result = DataSourceMainGroup.AsEnumerable().Select(d => Convert.ToInt32(d["MAINGROUPID"])).ToList();
                }
            }
            catch { }
            return result;
        }
        private List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> GenerateData(List<int> listFromStoreID,
            List<int> listToStoreID, List<int> listProductState, List<string> listProductID,
            decimal quantity)
        {
            List<RealTimeProductInStock> RealTimeInstock = new List<RealTimeProductInStock>();
            List<int> listFromStoreInSource = ListAutoStoreChangeContext.Select(d => d.FromStore).Distinct().ToList();
            List<string> listProductSource = new List<string>();
            ListAutoStoreChangeContext.ForEach(item => { item.Detail.ToList().ForEach(detail => { listProductSource.Add(detail.ProductID); }); });
            List<int> listStateIDSource = ListAutoStoreChangeContext.Select(d => d.ProductState).ToList();
            List<int> totalState = new List<int>();
            List<int> totalStoreID = new List<int>();
            List<string> totalProductID = new List<string>();
            List<int> listSelectedMainGroupID = GetSelectedMainGroupID();
            totalState.AddRange(listProductState);
            totalState.AddRange(listStateIDSource);
            totalStoreID.AddRange(listFromStoreID);
            totalStoreID.AddRange(listToStoreID);
            totalStoreID.AddRange(listFromStoreInSource);
            totalProductID.AddRange(listProductSource);
            totalProductID.AddRange(listProductID);

            totalStoreID = totalStoreID.Distinct().ToList();
            totalProductID = totalProductID.Distinct().ToList();
            totalState = totalState.Distinct().ToList();

            DataTable Product_Instock = GetStoreInStock(totalProductID, totalStoreID);
            Product_Instock.PrimaryKey = new DataColumn[] { Product_Instock.Columns["STOREID"], Product_Instock.Columns["PRODUCTID"], Product_Instock.Columns["INSTOCKSTATUSID"] };
            totalStoreID.ForEach(storeid =>
            {
                totalProductID.ForEach(productid =>
                {
                    totalState.ForEach(state =>
                    {
                        DataRow instockInfo = null;
                        if (Product_Instock != null && Product_Instock.Rows.Count > 0)
                            instockInfo = Product_Instock.Rows.Find(new object[] { storeid, productid, state });
                        if (instockInfo != null)
                        {
                            decimal virtualUsedQuantity = ListAutoStoreChangeContext
                            .Where(d => d.FromStore == storeid && d.ProductState == state)
                            .Sum(d => d.Detail.Where(e => e.ProductID == productid).Sum(e => e.AutoQuantity));
                            RealTimeProductInStock realTime = new RealTimeProductInStock()
                            {
                                StoreID = storeid,
                                ProductID = productid,
                                InStockStatusID = state,
                                Quantity = Convert.ToDecimal(instockInfo["QUANTITY"]) - virtualUsedQuantity
                            };
                            RealTimeInstock.Add(realTime);
                        }
                    });
                });
            });
            listFromStoreID.ForEach(fromStoreID =>
            {
                DataRow fromStoreInfo = null;
                if (DataSourceStore != null && DataSourceStore.Rows.Count > 0)
                    fromStoreInfo = DataSourceStore.Rows.Find(fromStoreID);

                string fromStoreName = fromStoreInfo != null ? fromStoreID + " - " + fromStoreInfo["STORENAME"].GetString() : string.Empty;
                listToStoreID.ForEach(toStoreID =>
                {
                    DataRow toStoreInfo = null;
                    if (DataSourceStore != null && DataSourceStore.Rows.Count > 0)
                        toStoreInfo = DataSourceStore.Rows.Find(toStoreID);
                    string toStoreName = toStoreInfo != null ? toStoreID + " - " + toStoreInfo["STORENAME"].GetString() : string.Empty;
                    listProductState.ForEach(productStateID =>
                    {
                        DataRow productStateInfo = null;
                        if (DataSourceProductState != null && DataSourceProductState.Rows.Count > 0)
                            productStateInfo = DataSourceProductState.Rows.Find(productStateID);
                        PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext objStoreChange = null;
                        objStoreChange = ListAutoStoreChangeContext.Where(d => d.FromStore == fromStoreID && d.ToStore == toStoreID && d.ProductState == productStateID).FirstOrDefault();
                        bool isExistedStoreChange = false;
                        if (objStoreChange == null)
                        {
                            objStoreChange = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext()
                            {
                                FromStore = fromStoreID,
                                FromStoreName = fromStoreName,
                                ToStore = toStoreID,
                                ToStoreName = toStoreName,
                                ProductState = productStateID,
                                ImportProductStateID = productStateID.ToString(),
                                ImportProductStateName = productStateInfo != null ? productStateInfo["PRODUCTSTATUSNAME"].GetString() : string.Empty,
                                IsUrgent = false,
                            };
                        }
                        else
                        {
                            isExistedStoreChange = true;
                        }
                        List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail> details = null;
                        if (objStoreChange.Detail == null || !objStoreChange.Detail.Any())
                            details = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail>();
                        else
                            details = objStoreChange.Detail.ToList();
                        listProductID.ForEach(productid =>
                        {
                            var currentRealtimeInstock = RealTimeInstock.Where(d => d.StoreID == fromStoreID
                            && d.ProductID == productid && d.InStockStatusID == productStateID).FirstOrDefault();
                            DataRow fromInstockInfo = null;
                            DataRow toInstockInfo = null;
                            DataRow productInfo = null;
                            if (Product_Instock != null && Product_Instock.Rows.Count > 0)
                            {
                                fromInstockInfo = Product_Instock.Rows.Find(new object[] { fromStoreID, productid, productStateID });
                                toInstockInfo = Product_Instock.Rows.Find(new object[] { toStoreID, productid, productStateID });
                            }
                            if (DataSourceProduct != null && DataSourceProduct.Rows.Count > 0)
                            {
                                productInfo = DataSourceProduct.Rows.Find(productid);
                            }


                            PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail detail = details.FirstOrDefault(d => d.ProductID == productid);
                            decimal autoQuantity = 0;
                            bool isExistedDetail = false;
                            if (detail == null)
                            {
                                detail = new PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContextDetail()
                                {
                                    ProductID = productid,
                                    ProductName = productInfo != null ? productInfo["PRODUCTNAME"].GetString() : string.Empty,
                                    FromInStockQuantity = fromInstockInfo == null ? 0 : Convert.ToDecimal(fromInstockInfo["QUANTITY"]),
                                    ToInStockQuantity = toInstockInfo == null ? 0 : Convert.ToDecimal(toInstockInfo["QUANTITY"]),
                                    IsManualAdd = false,
                                    ImportQuantity = quantity.ToString(),
                                };
                            }
                            else
                            {
                                isExistedDetail = true;
                                detail.ImportQuantity = ((detail.ImportQuantity.IsNumber() ? Convert.ToDecimal(detail.ImportQuantity) : 0) + quantity).ToString();
                                detail.FromInStockQuantity = fromInstockInfo == null ? 0 : Convert.ToDecimal(fromInstockInfo["QUANTITY"]);
                                detail.ToInStockQuantity = toInstockInfo == null ? 0 : Convert.ToDecimal(toInstockInfo["QUANTITY"]);
                            }

                            autoQuantity = 0;
                            if (currentRealtimeInstock == null)
                            {
                                detail.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                detail.ImportMessage = "Sản phẩm không còn tồn kho";
                            }
                            else
                            {
                                if (quantity > currentRealtimeInstock.Quantity/* && currentRealtimeInstock.Quantity >=0*/)
                                {
                                    if (currentRealtimeInstock.Quantity <= 0)
                                    {
                                        if (isExistedDetail)
                                        {
                                            detail.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Warning;
                                            detail.ImportMessage = "Số lượng chuyển lớn hơn số lượng tồn";
                                        }
                                        else
                                        {
                                            detail.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                            detail.ImportMessage = "Sản phẩm không còn tồn kho";
                                        }
                                    }
                                    else
                                    {
                                        autoQuantity = currentRealtimeInstock.Quantity;
                                        detail.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Warning;
                                        detail.ImportMessage = "Số lượng chuyển lớn hơn số lượng tồn";
                                    }
                                }
                                else
                                {
                                    int mainGroupID = Convert.ToInt32(productInfo["MAINGROUPID"]);
                                    DataRow mainGroupInfo = null;
                                    if (DataSourceMainGroup != null && DataSourceMainGroup.Rows.Count > 0)
                                    {
                                        mainGroupInfo = DataSourceMainGroup.Rows.Find(mainGroupID);
                                    }
                                    StringBuilder strError = new StringBuilder();
                                    if (!listSelectedMainGroupID.Any(d => d == mainGroupID))
                                        strError.AppendLine("Sản phẩm này không thuộc về danh sách ngành hàng đang chọn");
                                    if (!IsMainGroupPermission(fromStoreID, mainGroupID, EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                                        strError.AppendLine("Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + mainGroupInfo["MAINGROUPNAME"]);
                                    if (!IsMainGroupPermission(toStoreID, mainGroupID, EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                                        strError.AppendLine("Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + mainGroupInfo["MAINGROUPNAME"]);
                                    if (!CheckPermission(mainGroupID))
                                        strError.AppendLine("Bạn không có quyền thao tác trên ngành hàng " + (mainGroupInfo != null ? mainGroupInfo["MAINGROUPNAME"].GetString() : string.Empty));
                                    if (strError.Length > 0)
                                    {
                                        detail.ImportStatus = PLC.StoreChangeCommand.WSStoreChangeCommand.EImportResult.Invalid;
                                        detail.ImportMessage = strError.ToString();
                                    }
                                    else
                                    {
                                        autoQuantity = quantity;
                                    }
                                }
                                currentRealtimeInstock.Quantity -= autoQuantity;
                            }
                            detail.AutoQuantity += autoQuantity;
                            detail.Quantity = detail.AutoQuantity;
                            if (!isExistedDetail)
                                details.Add(detail);
                        });
                        objStoreChange.Detail = details.ToArray();
                        if (!isExistedStoreChange)
                            ListAutoStoreChangeContext.Add(objStoreChange);
                    });
                });
            });
            return ListAutoStoreChangeContext;
        }
        private void BindingData()
        {
            DataTable dtb = new DataTable();
            dtb.Columns.AddRange(new DataColumn[]{
                new DataColumn() { ColumnName = "ISSELECTED", DataType = typeof(bool), DefaultValue = true },
                new DataColumn(){ ColumnName = "PRODUCTID" },
                new DataColumn(){ ColumnName = "PRODUCTNAME" },
                new DataColumn(){ ColumnName = "IMEI" },
                new DataColumn() { ColumnName = "INSTOCKSTATUSID" },
                new DataColumn(){ ColumnName = "INSTOCKSTATUSNAME" },
                new DataColumn(){ ColumnName = "FROMSTOREID" },
                new DataColumn(){ ColumnName = "FROMSTORENAME" },
                new DataColumn() { ColumnName = "FROMSTOREINSTOCK" },

                new DataColumn(){ ColumnName = "TOSTOREID" },
                new DataColumn(){ ColumnName = "TOSTORENAME" },
                new DataColumn() { ColumnName = "TOSTOREINSTOCK" },

                new DataColumn(){ ColumnName = "AUTOQUANTITY" },
                new DataColumn(){ ColumnName = "ISURGENT", DataType = typeof(bool), DefaultValue = false },

            });
            ListAutoStoreChangeContext.Where(d => d.Detail.Any()).ToList().ForEach(d =>
            {

                d.Detail.ToList().ForEach(f =>
                {
                    DataRow r = dtb.NewRow();
                    r.ItemArray = new object[] {
                       f.IsSelected,
                       f.ProductID,
                       f.ProductName.GetString(),
                       f.IMEI,
                       d.ProductState,
                       d.ImportProductStateName,
                       d.FromStore,
                       d.FromStoreName,
                       f.FromInStockQuantity,
                       d.ToStore,
                       d.ToStoreName,
                       f.ToInStockQuantity,
                       f.AutoQuantity, //số lượng hệ thống chia lại
                       f.IsUrgent
                   };
                    dtb.Rows.Add(r);
                });
            });
            grdData.DataSource = dtb;
        }
        private List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> GetSelectedItem()
        {
            List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> result = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext>(ListAutoStoreChangeContext);
            if (!result.Any())
            {
                return result;

            }
            else if (!((DataTable)grdData.DataSource).AsEnumerable().Any(d => Convert.ToBoolean(d["ISSELECTED"]) == true))
            {
                return result;
            }


            return result.Where(d => d.IsAnyDetailSelected).ToList();

            //var listNotSelected = ((DataTable)grdData.DataSource).AsEnumerable().Where(d => !Convert.ToBoolean(d["ISSELECTED"])).Select(d => new
            //{
            //    FROMSTOREID = Convert.ToInt32(d["FROMSTOREID"]),
            //    TOSTOREID = Convert.ToInt32(d["TOSTOREID"]),
            //    INSTOCKSTATUSID = Convert.ToInt32(d["INSTOCKSTATUSID"]),
            //    PRODUCTID = d["PRODUCTID"].GetString(),
            //    IMEI = d["IMEI"].GetString()
            //}).ToList();

            //result.ForEach(d =>
            //{
            //    var notSelectedItem = listNotSelected.Where(e => e.FROMSTOREID == d.FromStore && e.TOSTOREID == d.ToStore && e.INSTOCKSTATUSID == d.ProductState).ToList();
            //    if (notSelectedItem.Any())
            //    {
            //        d.Detail.ToList().ForEach(e =>
            //        {
            //            if (string.IsNullOrEmpty(e.IMEI))
            //            {
            //                if (notSelectedItem.Any(f => f.PRODUCTID == e.ProductID))
            //                    e.IsSelected = false;
            //                else e.IsSelected = true;
            //            }
            //            else
            //            {
            //                if (notSelectedItem.Any(f => f.PRODUCTID == e.ProductID && f.IMEI == e.IMEI))
            //                    e.IsSelected = false;
            //                else e.IsSelected = true;
            //            }
            //        });
            //    }
            //    else
            //        d.Detail.ToList().ForEach(e => e.IsSelected = true);

            //});
            //return result.Where(d => d.IsAnyDetailSelected).ToList();
        }
        /// <summary>
        /// Xóa các dòng, cột trống
        /// Trim dữ liệu
        /// </summary>
        /// <param name="dtb"></param>
        /// <returns></returns>
        private DataTable TrimDataTable(DataTable dtb)
        {
            if (dtb == null)
                return dtb;

            //Xóa các cột trống
            for (int i = 0; i < dtb.Columns.Count; i++)
            {
                if (dtb.AsEnumerable().All(r => string.IsNullOrWhiteSpace(r[i].GetString())))
                {
                    dtb.Columns.RemoveAt(i);
                    i--;
                }
            }

            //Xóa các dòng trống
            for (int i = 0; i < dtb.Rows.Count; i++)
            {
                if (dtb.Rows[i].ItemArray.All(c => string.IsNullOrWhiteSpace(c.GetString())))
                {
                    dtb.Rows.RemoveAt(i);
                    i--;
                }
            }

            //trim dữ liệu các cells
            for (int i = 0; i < dtb.Rows.Count; i++)
            {
                for (int j = 0; j < dtb.Columns.Count; j++)
                {
                    dtb.Rows[i][j] = dtb.Rows[i][j].GetString().Trim();
                }
            }

            return dtb;
        }

    }

}
