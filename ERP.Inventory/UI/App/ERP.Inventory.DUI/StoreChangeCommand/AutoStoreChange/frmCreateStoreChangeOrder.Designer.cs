﻿namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    partial class frmCreateStoreChangeOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.cboTransportServices = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label5 = new System.Windows.Forms.Label();
            this.cboTransportCompany = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.label2 = new System.Windows.Forms.Label();
            this.psInsert = new System.Windows.Forms.ProgressBar();
            this.lblProcessing = new System.Windows.Forms.Label();
            this.dteExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.cboTransportTypeID = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCreateStoreChangeCommand = new System.Windows.Forms.Button();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.grvViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colProductID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromStore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStoreChangeCommandQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStoreChangeOrderQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsUrgent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repCheckBox = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colNote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckBox)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cboTransportServices);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.cboTransportCompany);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.psInsert);
            this.panel2.Controls.Add(this.lblProcessing);
            this.panel2.Controls.Add(this.dteExpiryDate);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.cboTransportTypeID);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txtContent);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btnCreateStoreChangeCommand);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(967, 163);
            this.panel2.TabIndex = 0;
            // 
            // cboTransportServices
            // 
            this.cboTransportServices.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTransportServices.Location = new System.Drawing.Point(156, 67);
            this.cboTransportServices.Margin = new System.Windows.Forms.Padding(0);
            this.cboTransportServices.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboTransportServices.Name = "cboTransportServices";
            this.cboTransportServices.Size = new System.Drawing.Size(315, 24);
            this.cboTransportServices.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "Dịch vụ vận chuyển:";
            // 
            // cboTransportCompany
            // 
            this.cboTransportCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTransportCompany.Location = new System.Drawing.Point(156, 38);
            this.cboTransportCompany.Margin = new System.Windows.Forms.Padding(0);
            this.cboTransportCompany.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboTransportCompany.Name = "cboTransportCompany";
            this.cboTransportCompany.Size = new System.Drawing.Size(315, 24);
            this.cboTransportCompany.TabIndex = 10;
            this.cboTransportCompany.SelectionChangeCommitted += new System.EventHandler(this.cboTransportCompany_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "Đối tác vận chuyển:";
            // 
            // psInsert
            // 
            this.psInsert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.psInsert.Location = new System.Drawing.Point(772, 63);
            this.psInsert.Name = "psInsert";
            this.psInsert.Size = new System.Drawing.Size(183, 20);
            this.psInsert.TabIndex = 8;
            this.psInsert.Visible = false;
            // 
            // lblProcessing
            // 
            this.lblProcessing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProcessing.AutoSize = true;
            this.lblProcessing.Location = new System.Drawing.Point(772, 44);
            this.lblProcessing.Name = "lblProcessing";
            this.lblProcessing.Size = new System.Drawing.Size(69, 16);
            this.lblProcessing.TabIndex = 7;
            this.lblProcessing.Text = "Đang xử lý";
            this.lblProcessing.Visible = false;
            // 
            // dteExpiryDate
            // 
            this.dteExpiryDate.CustomFormat = "dd/MM/yyyy ";
            this.dteExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteExpiryDate.Location = new System.Drawing.Point(601, 11);
            this.dteExpiryDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dteExpiryDate.Name = "dteExpiryDate";
            this.dteExpiryDate.Size = new System.Drawing.Size(109, 22);
            this.dteExpiryDate.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(492, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ngày hết hạn:";
            // 
            // cboTransportTypeID
            // 
            this.cboTransportTypeID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransportTypeID.FormattingEnabled = true;
            this.cboTransportTypeID.Location = new System.Drawing.Point(156, 10);
            this.cboTransportTypeID.Name = "cboTransportTypeID";
            this.cboTransportTypeID.Size = new System.Drawing.Size(315, 24);
            this.cboTransportTypeID.TabIndex = 1;
            this.cboTransportTypeID.SelectedIndexChanged += new System.EventHandler(this.cboTransportTypeID_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Phương tiện:";
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(156, 96);
            this.txtContent.MaxLength = 2000;
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtContent.Size = new System.Drawing.Size(554, 40);
            this.txtContent.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Ghi chú YC chuyển kho:";
            // 
            // btnCreateStoreChangeCommand
            // 
            this.btnCreateStoreChangeCommand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreateStoreChangeCommand.Enabled = false;
            this.btnCreateStoreChangeCommand.Image = global::ERP.Inventory.DUI.Properties.Resources.add;
            this.btnCreateStoreChangeCommand.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCreateStoreChangeCommand.Location = new System.Drawing.Point(772, 10);
            this.btnCreateStoreChangeCommand.Name = "btnCreateStoreChangeCommand";
            this.btnCreateStoreChangeCommand.Size = new System.Drawing.Size(183, 25);
            this.btnCreateStoreChangeCommand.TabIndex = 6;
            this.btnCreateStoreChangeCommand.Text = "Tạo yêu cầu chuyển kho";
            this.btnCreateStoreChangeCommand.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCreateStoreChangeCommand.UseVisualStyleBackColor = true;
            this.btnCreateStoreChangeCommand.Click += new System.EventHandler(this.btnCreateStoreChangeCommand_Click);
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 163);
            this.gridControl.MainView = this.grvViewData;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repCheckBox});
            this.gridControl.Size = new System.Drawing.Size(967, 331);
            this.gridControl.TabIndex = 1;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvViewData});
            // 
            // grvViewData
            // 
            this.grvViewData.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvViewData.Appearance.FooterPanel.Options.UseFont = true;
            this.grvViewData.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grvViewData.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.grvViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grvViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grvViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvViewData.Appearance.Row.Options.UseFont = true;
            this.grvViewData.ColumnPanelRowHeight = 40;
            this.grvViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colProductID,
            this.colProductName,
            this.gridColumn2,
            this.gridColumn1,
            this.colFromStore,
            this.gridColumn4,
            this.gridColumn6,
            this.gridColumn5,
            this.gridColumn7,
            this.colStoreChangeCommandQuantity,
            this.colStoreChangeOrderQuantity,
            this.colIsUrgent,
            this.colNote,
            this.gridColumn8,
            this.gridColumn9});
            this.grvViewData.GridControl = this.gridControl;
            this.grvViewData.Name = "grvViewData";
            this.grvViewData.OptionsNavigation.UseTabKey = false;
            this.grvViewData.OptionsPrint.AutoWidth = false;
            this.grvViewData.OptionsView.ColumnAutoWidth = false;
            this.grvViewData.OptionsView.ShowAutoFilterRow = true;
            this.grvViewData.OptionsView.ShowFooter = true;
            this.grvViewData.OptionsView.ShowGroupPanel = false;
            this.grvViewData.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grvViewData_ShowingEditor);
            this.grvViewData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grvViewData_KeyDown);
            // 
            // colProductID
            // 
            this.colProductID.Caption = "Mã sản phẩm";
            this.colProductID.FieldName = "ProductID";
            this.colProductID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colProductID.Name = "colProductID";
            this.colProductID.OptionsColumn.AllowEdit = false;
            this.colProductID.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "ProductID", "Cộng:")});
            this.colProductID.Visible = true;
            this.colProductID.VisibleIndex = 0;
            this.colProductID.Width = 110;
            // 
            // colProductName
            // 
            this.colProductName.Caption = "Tên sản phẩm";
            this.colProductName.FieldName = "ProductName";
            this.colProductName.Name = "colProductName";
            this.colProductName.OptionsColumn.AllowEdit = false;
            this.colProductName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colProductName.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "ProductID", "{0:N0}")});
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 1;
            this.colProductName.Width = 250;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Trạng thái sản phẩm";
            this.gridColumn2.FieldName = "InstockStatusName";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 8;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Trạng thái sản phẩm";
            this.gridColumn1.FieldName = "InstockStatusID";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // colFromStore
            // 
            this.colFromStore.Caption = "Từ kho";
            this.colFromStore.FieldName = "FromStoreName";
            this.colFromStore.Name = "colFromStore";
            this.colFromStore.OptionsColumn.AllowEdit = false;
            this.colFromStore.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colFromStore.Visible = true;
            this.colFromStore.VisibleIndex = 2;
            this.colFromStore.Width = 250;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "SL Tồn";
            this.gridColumn4.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn4.FieldName = "FromStoreInStock";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 80;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Đến kho";
            this.gridColumn6.FieldName = "ToStoreName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            this.gridColumn6.Width = 250;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "SL Tồn";
            this.gridColumn5.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn5.FieldName = "ToStoreInStock";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 5;
            this.gridColumn5.Width = 80;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "SL Chuyển";
            this.gridColumn7.DisplayFormat.FormatString = "#,##0.####";
            this.gridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn7.FieldName = "Quantity";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            this.gridColumn7.Width = 80;
            // 
            // colStoreChangeCommandQuantity
            // 
            this.colStoreChangeCommandQuantity.AppearanceCell.Options.UseTextOptions = true;
            this.colStoreChangeCommandQuantity.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colStoreChangeCommandQuantity.Caption = "SL đã tạo lệnh";
            this.colStoreChangeCommandQuantity.DisplayFormat.FormatString = "#,##0.####";
            this.colStoreChangeCommandQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colStoreChangeCommandQuantity.FieldName = "StoreChangeCommandQuantity";
            this.colStoreChangeCommandQuantity.Name = "colStoreChangeCommandQuantity";
            this.colStoreChangeCommandQuantity.OptionsColumn.AllowEdit = false;
            this.colStoreChangeCommandQuantity.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.colStoreChangeCommandQuantity.Width = 80;
            // 
            // colStoreChangeOrderQuantity
            // 
            this.colStoreChangeOrderQuantity.AppearanceCell.Options.UseTextOptions = true;
            this.colStoreChangeOrderQuantity.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colStoreChangeOrderQuantity.Caption = "SL đã tạo yêu cầu";
            this.colStoreChangeOrderQuantity.DisplayFormat.FormatString = "#,##0.####";
            this.colStoreChangeOrderQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colStoreChangeOrderQuantity.FieldName = "StoreChangeOrderQuantity";
            this.colStoreChangeOrderQuantity.Name = "colStoreChangeOrderQuantity";
            this.colStoreChangeOrderQuantity.OptionsColumn.AllowEdit = false;
            this.colStoreChangeOrderQuantity.Width = 80;
            // 
            // colIsUrgent
            // 
            this.colIsUrgent.Caption = "Chuyển gấp";
            this.colIsUrgent.ColumnEdit = this.repCheckBox;
            this.colIsUrgent.FieldName = "IsUrgent";
            this.colIsUrgent.Name = "colIsUrgent";
            this.colIsUrgent.OptionsColumn.AllowEdit = false;
            this.colIsUrgent.Visible = true;
            this.colIsUrgent.VisibleIndex = 7;
            this.colIsUrgent.Width = 65;
            // 
            // repCheckBox
            // 
            this.repCheckBox.AutoHeight = false;
            this.repCheckBox.Name = "repCheckBox";
            this.repCheckBox.ValueChecked = "True";
            this.repCheckBox.ValueUnchecked = "False";
            // 
            // colNote
            // 
            this.colNote.Caption = "Ghi chú";
            this.colNote.FieldName = "Note";
            this.colNote.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colNote.Name = "colNote";
            this.colNote.Visible = true;
            this.colNote.VisibleIndex = 9;
            this.colNote.Width = 126;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "FromStoreID";
            this.gridColumn8.FieldName = "FromStoreID";
            this.gridColumn8.Name = "gridColumn8";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "ToStoreID";
            this.gridColumn9.FieldName = "ToStoreID";
            this.gridColumn9.Name = "gridColumn9";
            // 
            // frmCreateStoreChangeOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 494);
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmCreateStoreChangeOrder";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tạo yêu cầu chuyển kho";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCreateStoreChangeOrder_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCheckBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnCreateStoreChangeCommand;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView grvViewData;
        private DevExpress.XtraGrid.Columns.GridColumn colProductID;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
        private DevExpress.XtraGrid.Columns.GridColumn colFromStore;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn colIsUrgent;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repCheckBox;
        private DevExpress.XtraGrid.Columns.GridColumn colStoreChangeCommandQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colStoreChangeOrderQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colNote;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboTransportTypeID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dteExpiryDate;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private System.Windows.Forms.Label lblProcessing;
        private System.Windows.Forms.ProgressBar psInsert;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboTransportCompany;
        private System.Windows.Forms.Label label2;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboTransportServices;
        private System.Windows.Forms.Label label5;
    }
}