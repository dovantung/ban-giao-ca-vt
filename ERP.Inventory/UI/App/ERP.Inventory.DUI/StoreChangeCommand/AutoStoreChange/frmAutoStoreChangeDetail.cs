﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System.Threading;
using System.Collections;
using System.Text.RegularExpressions;
using Library.AppCore.Forms;
using System.Diagnostics;
using Library.AppCore.Constant;

namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    public partial class frmAutoStoreChangeDetail : Form
    {
        public frmAutoStoreChangeDetail()
        {
            InitializeComponent();
        }

        #region Variables
        private int intStoreChangeCommandTypeID = -1;
        private PLC.StoreChangeCommand.PLCStoreChangeCommand objPLCStoreChangeCommand = new PLC.StoreChangeCommand.PLCStoreChangeCommand();
        private DataTable dtbAutoStoreChange = null;
        public DataTable dtbStore = null;
        DataTable dtbStoreMainGroup = null;
        DataTable dtbMainGroupPermission = null;
        public int intMainGroupID = 0;
        private DataTable dtbAutoStoreChangeQuantity = null;
        private int intIsShowProduct = -1;
        private int intIsCheckRealInput = -1;
        private PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();
        private DataTable dtbStoreInStock = null; // Bảng tồn kho các sản phẩm nhập từ excel
        private ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        private int intOldIndex = 0;
        private DataTable dtbAutoDistribute = null;
        private bool bolIsCallFromAutoDistribute = false;
        private bool bolIsReceive = false;
        private int intMainGroupIDFromAutoDistribute = -1;
        private int intStoreID = -1;
        private string strStoreIDList = null;

        private bool bolIsCanUseOldCode = false;
        private DataTable dtbStoreOldId = null;
        private DataTable dtbProductOld = null;
        private bool bolIsAutoCreateOrder = false;

        // LE VAN DONG - 01/10/2017
        private DataTable dtbStoreCache = null;
        // Loai chuyen kho
        private ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType = null;
        StringBuilder strbProductIDList = new StringBuilder();//Lưu mã sp để query lấy số lượng đã tạo lệnh
        StringBuilder strbFromStoreIDList = new StringBuilder();//Lưu mã kho để query lấy số lượng đã tạo lệnh
        StringBuilder strbToStoreIDList = new StringBuilder();//Lưu mã kho để query lấy số lượng đã tạo lệnh
        #endregion

        #region Property

        public string StoreIDList
        {
            get { return strStoreIDList; }
            set { strStoreIDList = value; }
        }

        public int StoreID
        {
            get { return intStoreID; }
            set { intStoreID = value; }
        }

        public int MainGroupIDFromAutoDistribute
        {
            get { return intMainGroupIDFromAutoDistribute; }
            set { intMainGroupIDFromAutoDistribute = value; }
        }

        public bool ISCallFromAutoDistribute
        {
            get { return bolIsCallFromAutoDistribute; }
            set { bolIsCallFromAutoDistribute = value; }
        }

        public bool IsReceive
        {
            get { return bolIsReceive; }
            set { bolIsReceive = value; }
        }

        public DataTable DtbAutoDistribute
        {
            get { return dtbAutoDistribute; }
            set { dtbAutoDistribute = value; }
        }

        public int IsCheckRealInput
        {
            set { intIsCheckRealInput = value; }
        }
        public int IsShowProduct
        {
            set { intIsShowProduct = value; }
        }
        public DataTable AutoStoreChangeTable
        {
            set { dtbAutoStoreChange = value; }
        }
        #endregion

        #region Support Functions
        private void EnableControl(bool bolIsEnable)
        {
            grpSearch.Enabled = bolIsEnable;
            mnuGrid.Enabled = bolIsEnable;
        }
        private void ExportTemplate()
        {
            if (string.IsNullOrEmpty(cboToStoreID.StoreIDList))
            {
                string strMess = "Bạn phải chọn kho nhập";
                if (cboType.SelectedIndex == 1)
                    strMess = "Bạn phải chọn kho xuất";
                MessageBoxObject.ShowWarningMessage(this, strMess);
                cboToStoreID.Focus();
                return;
            }
            DataTable dtbExportTemplate = new DataTable();
            dtbExportTemplate.Columns.Add("Mã sản phẩm", typeof(string));
            string strToStoreIDList = cboToStoreID.Text;
            string[] strArrToStore = strToStoreIDList.Trim('[', ']').Split(new string[] { "] [" }, StringSplitOptions.None);
            List<string> lstToStoreID = new List<string>();
            for (int i = 0; i < strArrToStore.Length; i++)
            {
                string[] strToStoreInfo = strArrToStore[i].Split(new string[] { " - " }, 2, StringSplitOptions.None);
                if (strToStoreInfo.Length != 2)
                    continue;
                lstToStoreID.Add(strToStoreInfo[0]);
                //dtbExportTemplate.Columns.Add(strToStoreInfo[1], typeof(string));
                dtbExportTemplate.Columns.Add(strArrToStore[i], typeof(string));
            }
            DataRow row = dtbExportTemplate.NewRow();
            row[0] = "PRODUCTID";
            for (int i = 1; i <= lstToStoreID.Count; i++)
            {
                // LÊ VĂN ĐÔNG - 04/07/2017
                if (chkIsCanUseOldCode.Checked && dtbStoreOldId != null)
                {
                    DataRow[] rowsel = dtbStoreOldId.Select("STOREID=" + lstToStoreID[i - 1] + "");
                    if (rowsel.Length > 0)
                        row[i] = rowsel[0]["OLDSTOREID"];
                }
                else
                    row[i] = lstToStoreID[i - 1];
            }
            dtbExportTemplate.Rows.Add(row);
            dtbExportTemplate.AcceptChanges();
            flexExportTemplate.DataSource = dtbExportTemplate;
            flexExportTemplate.AutoSizeCols();
            Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexExportTemplate);
        }
        private void GetStoreInStock(DataTable tblProduct, int intFromStoreID)
        {
            if (tblProduct == null)
                return;
            StringBuilder strProductIDList = new StringBuilder();
            string strStoreIDList = "<" + intFromStoreID + ">";
            DataRow rowStoreIDList = tblProduct.Rows[1];
            for (int i = 1; i < tblProduct.Columns.Count; i++)
            {
                string strStoreID = Convert.ToString(rowStoreIDList[i]).Trim();
                if (!string.IsNullOrWhiteSpace(strStoreID))
                    strStoreIDList = strStoreIDList + "<" + Convert.ToString(rowStoreIDList[i]).Trim() + ">";
            }
            for (int i = 2; i < tblProduct.Rows.Count; i++)
            {
                if (Convert.ToString(tblProduct.Rows[i][0]).Trim().Length <= 20)
                {
                    strProductIDList.Append("<");
                    strProductIDList.Append(Convert.ToString(tblProduct.Rows[i][0]).Trim());
                    strProductIDList.Append(">");
                }
            }
            string strInStockStatusIDList = string.Empty;
            strInStockStatusIDList = "1";
            object[] objKeywords = new object[]{"@PRODUCTIDLIST", strProductIDList.ToString(),
                                                "@STOREIDLIST", strStoreIDList,
                                                //"@ISSHOWPRODUCT", intIsShowProduct,
                                                "@ISCHECKREALINPUT", intIsCheckRealInput,
                                                //"@ISNEW",1
                                                "@InStockStatusIDList", strInStockStatusIDList,
                                                //"@IsExistStock", 1
                                                };
            dtbStoreInStock = objPLCReportDataSource.GetDataSource("PM_STOCKREPORT_AUTOCHANGE", objKeywords);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                MessageBoxObject.ShowWarningMessage(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            else
                dtbStoreInStock.PrimaryKey = new DataColumn[2] { dtbStoreInStock.Columns["ProductID"], dtbStoreInStock.Columns["StoreID"] };
            
        }
        private decimal GetQuantity(string strProductID, int intStoreID)
        {
            if (dtbStoreInStock == null || dtbStoreInStock.Rows.Count < 1)
                return 0;

            // DataRow[] row = dtbStoreInStock.Select("ProductID ='" + strProductID + "'" + " and StoreID = " + intStoreID);
            DataRow row = dtbStoreInStock.Rows.Find(new object[2] { strProductID,intStoreID });
            if (row != null)//&& row.Length > 0)
                return Convert.ToDecimal(row["QUANTITY"]);//row[0]["QUANTITY"]);
            return 0;
        }

        // le vandong--1
        private void ImportFromAutoDistribute()
        {
            if (dtbAutoDistribute == null || dtbAutoDistribute.Rows.Count < 1)
            {
                return;
            }

            DataTable tblResult = CreateImportResultTable();
            dtbAutoStoreChange.Clear();
            strbFromStoreIDList = new StringBuilder();
            strbProductIDList = new StringBuilder();
            strbToStoreIDList = new StringBuilder();
            bool bolIsPermission = Library.AppCore.SystemConfig.objSessionUser.IsPermission("AUTOSTORECHANGE_CHECKEMERGENCY");
            int intIsUrgent = 0;
            bool bolIsExistsError = false;
            try
            {
                if (bolIsReceive)
                {
                    string strMainGroupList = cboMainGroupID.MainGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");
                    string[] mainGroupList = strMainGroupList.Split(',');
                    int intToStoreID = cboFromStoreID.StoreID;
                    string strToStoreName = ERP.MasterData.PLC.MD.PLCStore.GetStoreName(intToStoreID);
                    int intFromStoreID = -1;
                    string strStatus = "";
                    string strErrorContent = "";
                    int intStatusID = 0;
                    decimal decQuanity = 0;
                    string strFromStoreName = string.Empty;
                    string strProductID = string.Empty;

                    for (int i = 0; i < dtbAutoDistribute.Rows.Count; i++)
                    {
                        DataRow objRow = dtbAutoDistribute.Rows[i];

                        strProductID = Convert.ToString(objRow[0]).Trim();
                        intFromStoreID = -1;
                        strStatus = "";
                        strErrorContent = "";
                        intStatusID = 0;
                        decQuanity = 0;
                        strFromStoreName = string.Empty;

                        intFromStoreID = Convert.ToInt32(objRow["STOREID"]);
                        decQuanity = -Convert.ToDecimal(objRow["TRANSFERQUANTITY"]);
                        //DataRow[] rowFromStore = dtbStoreCache.Select("STOREID = " + intFromStoreID);
                        //if (rowFromStore.Length > 0)
                        //    strFromStoreName = rowFromStore[0]["STORENAME"].ToString();
                        DataRow rowFromStore = dtbStoreCache.Rows.Find(intFromStoreID);
                        if(rowFromStore!=null)
                            strFromStoreName = rowFromStore["STORENAME"].ToString();
                        decimal decRealQuanity = decQuanity;
                        decimal decFromInstockQuantity = 0;
                        decimal decToInstockQuantity = 0;
                        string strProductName = string.Empty;
                        strProductName = objRow["PRODUCTNAME"].ToString();
                        
                        if (strFromStoreName.Length > 0 && strToStoreName.Length > 0)
                        {
                            decFromInstockQuantity = Convert.ToInt32(objRow["QUANTITY"]);
                            decToInstockQuantity = Convert.ToInt32(objRow["CENTERQUANTITY"]);
                            //decFromInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intFromStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                            //decToInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);

                            if (intFromStoreID == intToStoreID)
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Kho nhập trùng với kho xuất";
                                intStatusID = 1;
                            }

                            else if (!CheckMainGroupPermission(intFromStoreID, cboMainGroupID.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + cboMainGroupID.MainGroupName;
                                intStatusID = 1;
                            }
                            else if (!CheckMainGroupPermission(intToStoreID, cboMainGroupID.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + cboMainGroupID.MainGroupName;
                                intStatusID = 1;
                            }

                            else if (!CheckPermission(cboMainGroupID.MainGroupID, EnumType.IsNewPermissionType.ALL))
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Bạn không có quyền thao tác trên ngành hàng " + cboMainGroupID.MainGroupName;
                                intStatusID = 1;
                            }
                            else if (decQuanity > 0)
                            {
                                if (CheckProductExist(intFromStoreID, intToStoreID, strProductID))
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Sản phẩm chuyển kho đã tồn tại";
                                    intStatusID = 1;
                                }
                                else
                                {
                                    if (decFromInstockQuantity <= 0)
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Sản phẩm không tồn kho";
                                        intStatusID = 1;
                                    }
                                    else
                                    {
                                        if (decQuanity > decFromInstockQuantity)
                                        {
                                            strStatus = "Cảnh báo";
                                            strErrorContent = "Số lượng chuyển lớn hơn số lượng tồn";
                                            decRealQuanity = decFromInstockQuantity;
                                            intStatusID = 2;
                                        }
                                        else
                                        {
                                            strStatus = "Thành công";
                                            strErrorContent = "";
                                            intStatusID = 0;
                                        }
                                        AddProduct(strProductID, strProductName, intFromStoreID, strFromStoreName, intToStoreID,
                                            strToStoreName, decFromInstockQuantity, decToInstockQuantity, decRealQuanity, false, Convert.ToBoolean(intIsUrgent));

                                    }
                                }
                            }
                            else
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Số lượng chuyển phải lớn hơn 0";
                                intStatusID = 1;
                            }
                        }
                        else
                        {
                            if (strFromStoreName.Length == 0)
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Sai kho xuất";
                                intStatusID = 1;
                            }
                            else
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Sai kho nhập";
                                intStatusID = 1;
                            }
                        }
                        if (!bolIsExistsError && intStatusID != 0)
                        {
                            bolIsExistsError = true;
                        }
                        DataRow objRowResult = tblResult.NewRow();
                        objRowResult.ItemArray = new object[] { strProductID, strProductName, strFromStoreName,
                        decFromInstockQuantity, strToStoreName, decToInstockQuantity, decQuanity, strStatus,
                        intStatusID, intIsUrgent, strErrorContent };
                        tblResult.Rows.Add(objRowResult);
                    }
                }
                else
                {
                    string strProductID = string.Empty;
                    int intFromStoreID = cboFromStoreID.StoreID;
                    string strFromStoreName = string.Empty;
                    //DataRow[] rowFromStore = dtbStoreCache.Select("STOREID = " + intFromStoreID);
                    //if (rowFromStore.Length > 0)
                    //    strFromStoreName = rowFromStore[0]["STORENAME"].ToString();
                    DataRow rowFromStore = dtbStoreCache.Rows.Find(intFromStoreID);
                    if (rowFromStore != null)
                        strFromStoreName = rowFromStore["STORENAME"].ToString();
                    int intToStoreID = -1;
                    String strStatus = "";
                    String strErrorContent = "";
                    int intStatusID = 0;
                    decimal decQuanity = 0;
                    string strProductName = string.Empty;

                    for (int i = 0; i < dtbAutoDistribute.Rows.Count; i++)
                    {
                        DataRow objRow = dtbAutoDistribute.Rows[i];
                        strProductID = Convert.ToString(objRow[0]).Trim();
                        intToStoreID = -1;
                        strStatus = "";
                        strErrorContent = "";
                        intStatusID = 0;
                        decQuanity = 0;

                        intToStoreID = Convert.ToInt32(objRow["STOREID"]);
                        decQuanity = Convert.ToDecimal(objRow["TRANSFERQUANTITY"]);
                        decimal decRealQuanity = decQuanity;
                        string strToStoreName = string.Empty;
                        //DataRow[] rowToStore = dtbStoreCache.Select("STOREID = " + intToStoreID);
                        //if (rowToStore.Length > 0)
                        //    strToStoreName = rowToStore[0]["STORENAME"].ToString();
                        DataRow rowToStore = dtbStoreCache.Rows.Find(intToStoreID);
                        if (rowToStore != null)
                            strToStoreName = rowToStore["STORENAME"].ToString();

                        decimal decFromInstockQuantity = 0;
                        decimal decToInstockQuantity = 0;
                        
                        strProductName = objRow["PRODUCTNAME"].ToString();
                        if (strFromStoreName.Length > 0 && strToStoreName.Length > 0)
                        {
                            decFromInstockQuantity = Convert.ToInt32(objRow["CENTERQUANTITY"]);
                            decToInstockQuantity = Convert.ToInt32(objRow["QUANTITY"]);
                            //decFromInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intFromStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                            //decToInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);

                            if (intFromStoreID == intToStoreID)
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Kho nhập trùng với kho xuất";
                                intStatusID = 1;
                            }
                            else if (!CheckMainGroupPermission(intFromStoreID, cboMainGroupID.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + cboMainGroupID.MainGroupName;
                                intStatusID = 1;
                            }
                            else if (!CheckMainGroupPermission(intToStoreID, cboMainGroupID.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + cboMainGroupID.MainGroupName;
                                intStatusID = 1;
                            }

                            else if (!CheckPermission(cboMainGroupID.MainGroupID, EnumType.IsNewPermissionType.ALL))
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Bạn không có quyền thao tác trên ngành hàng " + cboMainGroupID.MainGroupName;
                                intStatusID = 1;
                            }
                            else if (decQuanity > 0)
                            {
                                if (CheckProductExist(intFromStoreID, intToStoreID, strProductID))
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Sản phẩm chuyển kho đã tồn tại";
                                    intStatusID = 1;
                                }
                                else
                                {
                                    if (decFromInstockQuantity <= 0)
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Sản phẩm không tồn kho";
                                        intStatusID = 1;
                                    }
                                    else
                                    {
                                        if (decQuanity > decFromInstockQuantity)
                                        {
                                            strStatus = "Cảnh báo";
                                            strErrorContent = "Số lượng chuyển lớn hơn số lượng tồn";
                                            decRealQuanity = decFromInstockQuantity;
                                            intStatusID = 2;
                                        }
                                        else
                                        {
                                            strStatus = "Thành công";
                                            strErrorContent = "";
                                            intStatusID = 0;
                                        }
                                        AddProduct(strProductID, strProductName, intFromStoreID, strFromStoreName, intToStoreID,
                                            strToStoreName, decFromInstockQuantity, decToInstockQuantity, decRealQuanity, false, Convert.ToBoolean(intIsUrgent));

                                    }
                                }
                            }
                            else
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Số lượng chuyển phải lớn hơn 0";
                                intStatusID = 1;
                            }
                        }
                        else
                        {
                            if (strFromStoreName.Length == 0)
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Sai kho xuất";
                                intStatusID = 1;
                            }
                            else
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Sai kho nhập";
                                intStatusID = 1;
                            }
                        }
                        if (!bolIsExistsError && intStatusID != 0)
                        {
                            bolIsExistsError = true;
                        }
                        DataRow objRowResult = tblResult.NewRow();
                        objRowResult.ItemArray = new object[] { strProductID, strProductName, strFromStoreName,
                        decFromInstockQuantity, strToStoreName, decToInstockQuantity, decQuanity, strStatus,
                        intStatusID, intIsUrgent, strErrorContent };
                        tblResult.Rows.Add(objRowResult);

                    }
                }
            }
            catch (Exception objExce)
            {
                Library.AppCore.SystemErrorWS.Insert("Lỗi nhập thông tin chuyển kho từ Excel ", objExce, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (bolIsExistsError)
            {
                frmShowImportResult frmShowImportResult1 = new frmShowImportResult();
                frmShowImportResult1.Text = "Kết quả nhập từ chia hàng tự động";
                frmShowImportResult1.ImportResult = tblResult;
                frmShowImportResult1.ShowDialog();
            }
        }

        public bool IsNumber(string pText)
        {
            if (string.IsNullOrEmpty(pText))
                return false;
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            return regex.IsMatch(pText);
        }

        private void ImportExcelNew()
        {

            DataTable tblProduct = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable();
            if (tblProduct == null)
            {
                MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (tblProduct.Rows.Count < 3)
            {
                return;
            }

            if (chkIsCanUseOldCode.Checked)
            {
                // LÊ VĂN ĐÔNG - THAY THẾ MÃ VIETTEL THÀNH MÃ HỆ THỐNG ERP
                if (dtbStoreOldId != null)
                {
                    for (int i = 1; i < tblProduct.Columns.Count; i++)
                    {
                        DataRow[] rowsel = dtbStoreOldId.Select("OLDSTOREID='" + tblProduct.Rows[1][i].ToString() + "'");
                        if (rowsel.Length > 0)
                            tblProduct.Rows[1][i] = rowsel[0]["STOREID"];
                    }
                }

                // Thay thế mã sản phẩm
                if (dtbProductOld != null)
                {
                    for (int i = 2; i < tblProduct.Rows.Count; i++)
                    {
                        DataRow[] rowsel = dtbProductOld.Select("PRODUCTIDOLD='" + tblProduct.Rows[i][0].ToString() + "'");
                        if (rowsel.Length > 0)
                            tblProduct.Rows[i][0] = rowsel[0]["PRODUCTID"];
                    }
                }
            }


            //tblProduct.Rows.RemoveAt(0);
            DataTable tblResult = CreateImportResultTable();
            dtbAutoStoreChange.Clear();
            strbFromStoreIDList = new StringBuilder();
            strbProductIDList = new StringBuilder();
            strbToStoreIDList = new StringBuilder();
            bool bolIsPermission = Library.AppCore.SystemConfig.objSessionUser.IsPermission("AUTOSTORECHANGE_CHECKEMERGENCY");
            int intIsUrgent = 0;
            bool bolIsExistsError = false;

            string strMainGroupList = cboMainGroupID.MainGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");
            string[] mainGroupList = strMainGroupList.Split(',');

            try
            {
                if (cboType.SelectedIndex == 1)
                {
                    #region cboType.SelectedIndex == 1 : Nhận về

                    GetStoreInStock(tblProduct, cboFromStoreID.StoreID);
                    if (dtbStoreInStock == null)
                        return;

                    Thread objThread = new Thread(new ThreadStart(Excute));
                    objThread.Start();

                    string strToStoreName = string.Empty;
                    int intToStoreID = cboFromStoreID.StoreID;
                    //DataRow[] rowToStore = dtbStoreCache.Select("STOREID = " + intToStoreID);
                    //if (rowToStore.Length > 0)
                    //    strToStoreName = rowToStore[0]["STORENAME"].ToString();
                    DataRow rowToStore = dtbStoreCache.Rows.Find(intToStoreID);
                    if (rowToStore != null)
                        strToStoreName = rowToStore["STORENAME"].ToString();

                    int intFromStoreID = -1;
                    string strStatus = "";
                    string strErrorContent = "";
                    int intStatusID = 0;
                    decimal decQuanity = 0;


                    DataRow rowStoreID = tblProduct.Rows[1];

                    var rlsProductID = tblProduct.AsEnumerable().Where(r => !string.IsNullOrEmpty(r[0].ToString())).Select(r => r[0].ToString()).ToList();
                    var rlsToStoreID = tblProduct.AsEnumerable().Where(r => !string.IsNullOrEmpty(r[0].ToString())).Select(r => r[0].ToString()).ToList();
                    rlsProductID.RemoveAt(0);
                    var rlsDup = rlsProductID.Distinct().ToList();
                    var arrMainGroupID = (from r in rlsDup
                                          join x in dtbProductOld.AsEnumerable()
                                          on r.ToString().Trim() equals x["PRODUCTID"].ToString().Trim()
                                          select x["MAINGROUPID"].ToString()).ToList();


                    //Key: Ma kho, Value: Nganh hang
                    Dictionary<int, List<int>> dicCheckMainGroup = new Dictionary<int, List<int>>();
                    int intStoreIDCheck = 0;

                    for (int icol = 1; icol < tblProduct.Columns.Count; icol++)
                    {
                        if (IsNumber(rowStoreID[icol].ToString()))
                        {
                            intStoreIDCheck = Convert.ToInt32(rowStoreID[icol].ToString());

                            if (dicCheckMainGroup.ContainsKey(intStoreIDCheck))
                                continue;
                            else
                            {
                                List<int> lstMainID = new List<int>();
                                foreach (string strMainGroupID in arrMainGroupID)
                                {
                                    if (!CheckMainGroupPermission(intStoreIDCheck, Convert.ToInt32(strMainGroupID), Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                                        lstMainID.Add(Convert.ToInt32(strMainGroupID));
                                }
                                if (lstMainID.Count > 0)
                                    dicCheckMainGroup.Add(intStoreIDCheck, lstMainID);
                            }
                        }
                    }


                    for (int i = 2; i < tblProduct.Rows.Count; i++)
                    {
                        DataRow objRow = tblProduct.Rows[i];
                        string strProductID = Convert.ToString(objRow[0]).Trim();


                        string strProductName = string.Empty;
                        bool bolIsAllowDecimal = false;
                        int intMainGroupID = 0;
                        string strMainGroupName = "";
                        string strErrorMainGroup = string.Empty;

                        DataRow[] rowProduct = dtbProductOld.Select("PRODUCTID = '" + strProductID.Trim() + "'");
                        if (rowProduct.Length > 0)
                        {
                            strProductName = rowProduct[0]["PRODUCTNAME"].ToString();
                            bolIsAllowDecimal = Convert.ToBoolean(rowProduct[0]["ISALLOWDECIMAL"]);
                            intMainGroupID = Convert.ToInt32(rowProduct[0]["MAINGROUPID"]);
                            strMainGroupName = rowProduct[0]["MAINGROUPNAME"].ToString();

                            // Check phân quyền trên ngành hàng
                            if (!string.IsNullOrEmpty(strMainGroupList.Trim()) && !mainGroupList.Any(x => x.Trim().Equals(intMainGroupID.ToString())))
                                strErrorMainGroup = "Sản phẩm không thuộc ngành hàng đang xét";
                            else if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intToStoreID, intMainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                                strErrorMainGroup = "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + strMainGroupName;
                            // >Phan quyền
                        }

                        for (int j = 1; j < tblProduct.Columns.Count; j++)
                        {
                            string strStoreID = rowStoreID[j].ToString();
                            string strQuantity = objRow[j].ToString();

                            if (!string.IsNullOrWhiteSpace(strStoreID))
                            {
                                intFromStoreID = -1;
                                strStatus = "";
                                strErrorContent = "";
                                intStatusID = 0;
                                decQuanity = 0;
                                string strFromStoreName = string.Empty;

                                bool bolValidData = false;
                                if (!IsNumber(strStoreID))
                                    bolValidData = true;
                                else
                                    intFromStoreID = Convert.ToInt32(strStoreID.Trim());

                                if (!IsNumber(strQuantity))
                                    bolValidData = true;
                                else
                                    decQuanity = Convert.ToDecimal(strQuantity.Trim());

                                if (bolValidData)
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Dữ liệu trên từng kho phải là số";
                                    intStatusID = 1;
                                }

                                //DataRow[] rowStore = dtbStoreCache.Select("STOREID = " + intFromStoreID);
                                //if (rowStore.Length > 0)
                                //    strFromStoreName = rowStore[0]["STORENAME"].ToString();
                                DataRow rowStore = dtbStoreCache.Rows.Find(intFromStoreID);
                                if (rowStore != null)
                                    strFromStoreName = rowStore["STORENAME"].ToString();

                                decimal decRealQuanity = decQuanity;
                                decimal decFromInstockQuantity = 0;
                                decimal decToInstockQuantity = 0;

                                if (rowProduct.Length == 0)
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Mã sản phẩm không tồn tại";
                                    intStatusID = 1;
                                    DataRow objRowResult1 = tblResult.NewRow();
                                    objRowResult1.ItemArray = new object[] { strProductID, strProductName, intFromStoreID.ToString() + "-" + strFromStoreName,
                                    decFromInstockQuantity, intToStoreID.ToString() + "-" + strToStoreName, decToInstockQuantity, decQuanity, strStatus,
                                    intStatusID, intIsUrgent, strErrorContent };
                                    tblResult.Rows.Add(objRowResult1);
                                    if (!bolIsExistsError && intStatusID != 0)
                                    {
                                        bolIsExistsError = true;
                                    }
                                    continue;
                                }

                                if (strFromStoreName.Length > 0 && strToStoreName.Length > 0)
                                {
                                    decFromInstockQuantity = GetQuantity(strProductID, intFromStoreID);
                                    decToInstockQuantity = GetQuantity(strProductID, intToStoreID);
                                    //decFromInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intFromStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                                    //decToInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                                    if (intFromStoreID == intToStoreID)
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Kho nhập trùng với kho xuất";
                                        intStatusID = 1;
                                    }
                                    else if (!string.IsNullOrEmpty(strErrorMainGroup))
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = strErrorMainGroup;
                                        intStatusID = 1;
                                    }
                                    else if (dicCheckMainGroup.ContainsKey(intFromStoreID) && dicCheckMainGroup[intFromStoreID].Contains(intMainGroupID))
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + strMainGroupName;
                                        intStatusID = 1;
                                    }
                                    else if (!ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(intMainGroupID))
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Bạn không có quyền thao tác trên ngành hàng " + strMainGroupName;
                                        intStatusID = 1;
                                    }
                                    else if (decQuanity > 0)
                                    {
                                        if (CheckProductExist(intFromStoreID, intToStoreID, strProductID))
                                        {
                                            strStatus = "Lỗi";
                                            strErrorContent = "Sản phẩm chuyển kho đã tồn tại";
                                            intStatusID = 1;
                                        }
                                        else
                                        {
                                            if (decFromInstockQuantity <= 0)
                                            {
                                                strStatus = "Lỗi";
                                                strErrorContent = "Sản phẩm không tồn kho";
                                                intStatusID = 1;
                                            }
                                            else
                                            {
                                                if (decQuanity > decFromInstockQuantity)
                                                {
                                                    strStatus = "Cảnh báo";
                                                    strErrorContent = "Số lượng chuyển lớn hơn số lượng tồn";
                                                    decRealQuanity = decFromInstockQuantity;
                                                    intStatusID = 2;
                                                }
                                                else
                                                {
                                                    strStatus = "Thành công";
                                                    strErrorContent = "";
                                                    intStatusID = 0;
                                                }
                                                AddProduct(strProductID, strProductName, intFromStoreID, intFromStoreID.ToString() + "-" + strFromStoreName, intToStoreID,
                                                    intToStoreID.ToString() + "-" + strToStoreName, decFromInstockQuantity, decToInstockQuantity, decRealQuanity, bolIsAllowDecimal, Convert.ToBoolean(intIsUrgent));

                                            }
                                        }
                                    }
                                    else
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Số lượng chuyển phải lớn hơn 0";
                                        intStatusID = 1;
                                    }
                                }
                                else
                                {
                                    if (strFromStoreName.Length == 0)
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Sai kho xuất";
                                        intStatusID = 1;
                                    }
                                    else
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Sai kho nhập";
                                        intStatusID = 1;
                                    }
                                }
                                if (!bolIsExistsError && intStatusID != 0)
                                {
                                    bolIsExistsError = true;
                                }
                                DataRow objRowResult = tblResult.NewRow();
                                objRowResult.ItemArray = new object[] { strProductID, strProductName, intFromStoreID.ToString() + "-" + strFromStoreName,
                                    decFromInstockQuantity, intToStoreID.ToString() + "-" + strToStoreName, decToInstockQuantity, decQuanity, strStatus,
                                    intStatusID, intIsUrgent, strErrorContent };
                                tblResult.Rows.Add(objRowResult);
                            }
                        }
                    }
                    #endregion

                    frmWaitDialog.Close();
                    Thread.Sleep(0);
                    objThread.Abort();
                }
                else
                {
                    #region cboType.SelectedIndex == 0: Chuyển Đi

                    GetStoreInStock(tblProduct, cboFromStoreID.StoreID);
                    if (dtbStoreInStock == null)
                        return;

                    Thread objThread = new Thread(new ThreadStart(Excute));
                    objThread.Start();

                    DataRow rowStoreID = tblProduct.Rows[1];

                    var rlsProductID = tblProduct.AsEnumerable().Where(r => !string.IsNullOrEmpty(r[0].ToString())).Select(r => r[0].ToString()).ToList();
                    var rlsToStoreID = tblProduct.AsEnumerable().Where(r => !string.IsNullOrEmpty(r[0].ToString())).Select(r => r[0].ToString()).ToList();
                    rlsProductID.RemoveAt(0);
                    var rlsDup = rlsProductID.Distinct().ToList();
                    var arrMainGroupID = (from r in rlsDup
                                          join x in dtbProductOld.AsEnumerable()
                                          on r.ToString().Trim() equals x["PRODUCTID"].ToString().Trim()
                                          select x["MAINGROUPID"].ToString()).ToList();


                    //Key: Ma kho, Value: Nganh hang
                    Dictionary<int, List<int>> dicCheckMainGroup = new Dictionary<int, List<int>>();
                    int intStoreIDCheck = 0;

                    for (int icol = 1; icol < tblProduct.Columns.Count; icol++)
                    {
                        if (IsNumber(rowStoreID[icol].ToString()))
                        {
                            intStoreIDCheck = Convert.ToInt32(rowStoreID[icol].ToString());

                            if (dicCheckMainGroup.ContainsKey(intStoreIDCheck))
                                continue;
                            else
                            {
                                List<int> lstMainID = new List<int>();
                                foreach (string strMainGroupID in arrMainGroupID)
                                {
                                    if (!CheckMainGroupPermission(intStoreIDCheck, Convert.ToInt32(strMainGroupID), Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                                        lstMainID.Add(Convert.ToInt32(strMainGroupID));
                                }
                                if (lstMainID.Count > 0)
                                    dicCheckMainGroup.Add(intStoreIDCheck, lstMainID);
                            }
                        }
                    }

                    int intFromStoreID = cboFromStoreID.StoreID;
                    string strFromStoreName = "";
                    //DataRow[] rowStore = dtbStoreCache.Select("STOREID = " + intFromStoreID);
                    //if (rowStore.Length > 0)
                    //    strFromStoreName = rowStore[0]["STORENAME"].ToString();

                    DataRow rowStore = dtbStoreCache.Rows.Find(intFromStoreID);
                    if (rowStore != null)
                        strFromStoreName = rowStore["STORENAME"].ToString();

                    int intToStoreID = -1;
                    string strStatus = "";
                    string strErrorContent = "";
                    int intStatusID = 0;
                    decimal decQuanity = 0;

                    Dictionary<string, decimal> dicCheckInStock = new Dictionary<string, decimal>();

                    for (int i = 2; i < tblProduct.Rows.Count; i++)
                    {
                        DataRow objRow = tblProduct.Rows[i];

                        string strProductID = Convert.ToString(objRow[0]);
                        string strProductName = string.Empty;
                        int intMainGroupID = 0;
                        string strMainGroupName = string.Empty;
                        bool bolIsAllowDecimal = false;
                        string strErrorMainGroup = string.Empty;

                        DataRow[] rowProduct = dtbProductOld.Select("PRODUCTID = '" + strProductID.Trim() + "'");
                        if (rowProduct.Length > 0)
                        {
                            strProductID = strProductID.Trim();
                            strProductName = rowProduct[0]["PRODUCTNAME"].ToString();
                            bolIsAllowDecimal = Convert.ToBoolean(rowProduct[0]["ISALLOWDECIMAL"]);
                            intMainGroupID = Convert.ToInt32(rowProduct[0]["MAINGROUPID"]);
                            strMainGroupName = rowProduct[0]["MAINGROUPNAME"].ToString();

                            // Check phân quyền trên ngành hàng
                            if (string.IsNullOrEmpty(strMainGroupList.Trim()) || !mainGroupList.Any(x => x.Trim().Equals(intMainGroupID.ToString())))
                                strErrorMainGroup = "Sản phẩm không thuộc ngành hàng đang xét";
                            else if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intFromStoreID, intMainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                                strErrorMainGroup = "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + strMainGroupName;
                            else if (!ERP.MasterData.PLC.MD.PLCMainGroup.CheckPermission(intMainGroupID))
                                strErrorMainGroup = "Bạn không có quyền thao tác trên ngành hàng " + strMainGroupName;
                            // >Phan quyền
                        }

                        for (int j = 1; j < tblProduct.Columns.Count; j++)
                        {
                            string strStoreID = rowStoreID[j].ToString();
                            string strQuantity = objRow[j].ToString();
                            if (!string.IsNullOrWhiteSpace(strStoreID))
                            {
                                intToStoreID = -1;
                                strStatus = "";
                                strErrorContent = "";
                                intStatusID = 0;
                                decQuanity = 0;

                                bool bolValidData = false;
                                if (!IsNumber(strStoreID))
                                    bolValidData = true;
                                else
                                    intToStoreID = Convert.ToInt32(strStoreID.Trim());

                                if (!IsNumber(strQuantity))
                                    bolValidData = true;
                                else
                                    decQuanity = Convert.ToDecimal(strQuantity.Trim());

                                if (bolValidData)
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Dữ liệu trên từng kho phải là số";
                                    intStatusID = 1;
                                }

                                decimal decRealQuanity = decQuanity;

                                string strToStoreName = "";
                                //DataRow[] rowToStore = dtbStoreCache.Select("STOREID = " + intToStoreID);
                                //if (rowToStore.Length > 0)
                                //    strToStoreName = rowToStore[0]["STORENAME"].ToString();

                                DataRow rowToStore = dtbStoreCache.Rows.Find(intToStoreID);
                                if (rowToStore!=null)
                                    strToStoreName = rowToStore["STORENAME"].ToString();

                                decimal decFromInstockQuantity = 0;
                                decimal decToInstockQuantity = 0;

                                if (rowProduct.Length == 0)
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Mã sản phẩm không tồn tại";
                                    intStatusID = 1;
                                    DataRow objRowResult1 = tblResult.NewRow();
                                    objRowResult1.ItemArray = new object[] { strProductID, strProductName, intFromStoreID.ToString() + "-" + strFromStoreName,
                                        decFromInstockQuantity, intToStoreID.ToString() + "-" + strToStoreName, decToInstockQuantity, decQuanity, strStatus,
                                        intStatusID, intIsUrgent, strErrorContent };
                                    tblResult.Rows.Add(objRowResult1);
                                    if (!bolIsExistsError && intStatusID != 0)
                                    {
                                        bolIsExistsError = true;
                                    }
                                    continue;
                                }

                                if (strFromStoreName.Length > 0 && strToStoreName.Length > 0)
                                {
                                    decFromInstockQuantity = GetQuantity(strProductID, intFromStoreID);
                                    //Lấy dữ liệu tồn kho theo sản phẩm để kiểm tra dữ liệu import excel
                                    if (!dicCheckInStock.ContainsKey(strProductID))
                                    {
                                        dicCheckInStock.Add(strProductID, decFromInstockQuantity);
                                    }
                                    decToInstockQuantity = GetQuantity(strProductID, intToStoreID);
                                    //decFromInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intFromStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                                    //decToInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);

                                    if (intFromStoreID == intToStoreID)
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Kho nhập trùng với kho xuất";
                                        intStatusID = 1;
                                    }
                                    else if (!string.IsNullOrEmpty(strErrorMainGroup))
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = strErrorMainGroup;
                                        intStatusID = 1;
                                    }
                                    else if (dicCheckMainGroup.ContainsKey(intToStoreID) && dicCheckMainGroup[intToStoreID].Contains(intMainGroupID))
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + strMainGroupName;
                                        intStatusID = 1;
                                    }
                                    else if (decQuanity > 0)
                                    {
                                        if (CheckProductExist(intFromStoreID, intToStoreID, strProductID))
                                        {
                                            strStatus = "Lỗi";
                                            strErrorContent = "Sản phẩm chuyển kho đã tồn tại";
                                            intStatusID = 1;
                                        }
                                        else
                                        {
                                            if (decQuanity > dicCheckInStock[strProductID])
                                            {
                                                strStatus = "Cảnh báo";
                                                strErrorContent = "Số lượng chuyển lớn hơn số lượng tồn";
                                                decRealQuanity = dicCheckInStock[strProductID];
                                                dicCheckInStock[strProductID] -= dicCheckInStock[strProductID];
                                                intStatusID = 2;
                                            }
                                            else
                                            {
                                                strStatus = "Thành công";
                                                strErrorContent = "";
                                                intStatusID = 0;
                                                dicCheckInStock[strProductID] -= decQuanity;
                                            }

                                            #region Code cũ
                                            //if (decQuanity > decFromInstockQuantity)
                                            //{
                                            //    strStatus = "Cảnh báo";
                                            //    strErrorContent = "Số lượng chuyển lớn hơn số lượng tồn";
                                            //    decRealQuanity = decFromInstockQuantity;
                                            //    intStatusID = 2;
                                            //}
                                            //else
                                            //{
                                            //    strStatus = "Thành công";
                                            //    strErrorContent = "";
                                            //    intStatusID = 0;
                                            //}
                                            #endregion

                                            if (decRealQuanity > 0)
                                            {
                                                AddProduct(strProductID, strProductName, intFromStoreID, intFromStoreID.ToString() + "-" + strFromStoreName, intToStoreID,
                                                intToStoreID.ToString() + "-" + strToStoreName, decFromInstockQuantity, decToInstockQuantity, decRealQuanity, bolIsAllowDecimal, Convert.ToBoolean(intIsUrgent));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Số lượng chuyển phải lớn hơn 0";
                                        intStatusID = 1;
                                    }
                                }
                                else
                                {
                                    if (strFromStoreName.Length == 0)
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Sai kho xuất";
                                        intStatusID = 1;
                                    }
                                    else
                                    {
                                        strStatus = "Lỗi";
                                        strErrorContent = "Sai kho nhập";
                                        intStatusID = 1;
                                    }
                                }
                                if (!bolIsExistsError && intStatusID != 0)
                                {
                                    bolIsExistsError = true;
                                }
                                DataRow objRowResult = tblResult.NewRow();
                                objRowResult.ItemArray = new object[] { strProductID, strProductName, intFromStoreID.ToString() + "-" +strFromStoreName,
                                    decFromInstockQuantity, intToStoreID.ToString() + "-" + strToStoreName, decToInstockQuantity, decQuanity, strStatus,
                                    intStatusID, intIsUrgent, strErrorContent };
                                tblResult.Rows.Add(objRowResult);
                            }
                        }
                    }
                    #endregion

                    frmWaitDialog.Close();
                    Thread.Sleep(0);
                    objThread.Abort();
                }
            }
            catch (Exception objExce)
            {
                Library.AppCore.SystemErrorWS.Insert("Lỗi nhập thông tin chuyển kho từ Excel ", objExce, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (bolIsExistsError)
            {
                frmShowImportResult frmShowImportResult1 = new frmShowImportResult();
                frmShowImportResult1.ImportResult = tblResult;
                frmShowImportResult1.ShowDialog();
            }
        }

        private bool CheckMainGroupPermission(int intStoreID, int intMainGroupID, EnumType.StoreMainGroupPermissionType enuStoreMainGroupPermissionType)
        {
            if (dtbStoreMainGroup == null)
                return false;
            #region Bỏ
            /*
            StringBuilder arrCondition = new StringBuilder();
            arrCondition.Append("StoreID = " + intStoreID + " and MainGroupID = " + intMainGroupID);

            switch (enuStoreMainGroupPermissionType)
            {
                case EnumType.StoreMainGroupPermissionType.AUTOSTORECHANGE:
                    arrCondition.Append(" and ISCANAUTOSTORECHANGE = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.INPUT:
                    arrCondition.Append(" and ISCANINPUT = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.INPUTRETURN:
                    arrCondition.Append(" and ISCANINPUTRETURN = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.OUTPUT:
                    arrCondition.Append(" and ISCANOUTPUT = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.PURCHASEORDER:
                    arrCondition.Append(" and ISCANPURCHASEORDER = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.SALEORDER:
                    arrCondition.Append(" and ISCANSALEORDER = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT:
                    arrCondition.Append(" and ISCANSTORECHANGEINPUT = 1");
                    break;
                case EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT:
                    arrCondition.Append(" and ISCANSTORECHANGEOUTPUT = 1");
                    break;
            }
            return DataTableClass.CheckIsExist(dtbStoreMainGroup, arrCondition.ToString()); 
            */
            #endregion

            object[] objKey = new object[2] { intStoreID, intMainGroupID };
            DataRow drowcheck = dtbStoreMainGroup.Rows.Find(objKey);
            if (drowcheck == null)
                return false;
            else
            {

                switch (enuStoreMainGroupPermissionType)
                {
                    case EnumType.StoreMainGroupPermissionType.AUTOSTORECHANGE:
                        return Convert.ToBoolean(drowcheck["ISCANAUTOSTORECHANGE"]); 
                    case EnumType.StoreMainGroupPermissionType.INPUT:
                        return Convert.ToBoolean(drowcheck["ISCANINPUT"]);
                    case EnumType.StoreMainGroupPermissionType.INPUTRETURN:
                        return Convert.ToBoolean(drowcheck["ISCANINPUTRETURN"]);
                    case EnumType.StoreMainGroupPermissionType.OUTPUT:
                        return Convert.ToBoolean(drowcheck["ISCANOUTPUT"]);
                    case EnumType.StoreMainGroupPermissionType.PURCHASEORDER:
                        return Convert.ToBoolean(drowcheck["ISCANPURCHASEORDER"]);
                    case EnumType.StoreMainGroupPermissionType.SALEORDER:
                        return Convert.ToBoolean(drowcheck["ISCANSALEORDER"]);
                    case EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT:
                        return Convert.ToBoolean(drowcheck["ISCANSTORECHANGEINPUT"]);
                    case EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT:
                        return Convert.ToBoolean(drowcheck["ISCANSTORECHANGEOUTPUT"]);
                }
                return false;
            }

        }

        private bool CheckPermission(int intMainGroupID, EnumType.IsNewPermissionType enumIsNewPermissionType)
        {
            if (SystemConfig.objSessionUser.UserName == "administrator")
                return true;
            
            if (dtbMainGroupPermission == null)
            {
                return false;
            }
            StringBuilder strFilter = new StringBuilder();
            strFilter.Append("MainGroupID=" + intMainGroupID);
            if (enumIsNewPermissionType == EnumType.IsNewPermissionType.ISNEW)
                strFilter.Append(" and ISNEW = 1");
            else if (enumIsNewPermissionType == EnumType.IsNewPermissionType.ISOLD)
                strFilter.Append(" and ISOLD = 1");
            DataRow[] rows = dtbMainGroupPermission.Select(strFilter.ToString());
            if (rows.Length < 1)
                return false;
            return true;
        }
        private void Excute()
        {
            frmWaitDialog.Show(string.Empty, "Đang tạo dữ liệu chia hàng");
        }
        private void CustomFlex()
        {
            string[] FieldName = new string[] { "QuantityInStock", "Select", "ProductID", "ProductName", "FromStoreName", "FromStoreInStock", "ToStoreName", "ToStoreInStock", "Quantity", "StoreChangeCommandQuantity", "StoreChangeOrderQuantity", "IsUrgent", "IsAllowDecimal" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdListAutoStoreChange, true, false, true, true, FieldName);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvListAutoStoreChange);

            grvListAutoStoreChange.Columns["QuantityInStock"].Visible = false;
            grvListAutoStoreChange.Columns["Select"].OptionsColumn.AllowEdit = true;
            grvListAutoStoreChange.Columns["ProductID"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["ProductID"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "ProductID", "Cộng:")});
            grvListAutoStoreChange.Columns["ProductName"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["ProductName"].Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "ProductID", "{0:N0}")});
            grvListAutoStoreChange.Columns["FromStoreName"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["FromStoreInStock"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["ToStoreName"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["ToStoreInStock"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["Quantity"].OptionsColumn.AllowEdit = true;
            grvListAutoStoreChange.Columns["IsAllowDecimal"].Visible = false;
            grvListAutoStoreChange.Columns["Select"].Caption = "Chọn";
            grvListAutoStoreChange.Columns["ProductID"].Caption = "Mã sản phẩm";
            grvListAutoStoreChange.Columns["ProductName"].Caption = "Tên sản phẩm";
            grvListAutoStoreChange.Columns["FromStoreName"].Caption = "Từ kho";
            grvListAutoStoreChange.Columns["FromStoreInStock"].Caption = "Số lượng tồn";
            grvListAutoStoreChange.Columns["ToStoreName"].Caption = "Đến kho";
            grvListAutoStoreChange.Columns["ToStoreInStock"].Caption = "Số lượng tồn";
            grvListAutoStoreChange.Columns["Quantity"].Caption = "Số lượng chuyển";
            grvListAutoStoreChange.Columns["IsUrgent"].Caption = "Chuyển gấp";
            grvListAutoStoreChange.Columns["ProductID"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            grvListAutoStoreChange.Columns["ProductName"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            grvListAutoStoreChange.Columns["FromStoreName"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            grvListAutoStoreChange.Columns["FromStoreInStock"].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            grvListAutoStoreChange.Columns["Select"].Width = 50;
            grvListAutoStoreChange.Columns["FromStoreName"].Width = 240;
            grvListAutoStoreChange.Columns["ToStoreName"].Width = 240;
            grvListAutoStoreChange.Columns["ProductID"].Width = 110;
            grvListAutoStoreChange.Columns["ToStoreInStock"].Width = 80;
            grvListAutoStoreChange.Columns["Quantity"].Width = 80;
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].Width = 80;
            grvListAutoStoreChange.Columns["StoreChangeOrderQuantity"].Width = 80;
            grvListAutoStoreChange.Columns["IsUrgent"].Width = 90;
            grvListAutoStoreChange.Columns["ProductName"].Width = 200;
            grvListAutoStoreChange.Columns["QuantityInStock"].Width = 80;
            grvListAutoStoreChange.Columns["FromStoreInStock"].Width = 80;
            grvListAutoStoreChange.Columns["ProductID"].Visible = true;
            grvListAutoStoreChange.Columns["FromStoreInStock"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grvListAutoStoreChange.Columns["FromStoreInStock"].DisplayFormat.FormatString = "#,##0.####";
            grvListAutoStoreChange.Columns["ToStoreInStock"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grvListAutoStoreChange.Columns["ToStoreInStock"].DisplayFormat.FormatString = "#,##0.####";
            grvListAutoStoreChange.Columns["QuantityInStock"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grvListAutoStoreChange.Columns["QuantityInStock"].DisplayFormat.FormatString = "#,##0.####";
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].DisplayFormat.FormatString = "#,##0.####";
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].AppearanceCell.Options.UseTextOptions = true;
            grvListAutoStoreChange.Columns["StoreChangeOrderQuantity"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grvListAutoStoreChange.Columns["StoreChangeOrderQuantity"].DisplayFormat.FormatString = "#,##0.####";
            grvListAutoStoreChange.Columns["StoreChangeOrderQuantity"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            grvListAutoStoreChange.Columns["StoreChangeOrderQuantity"].AppearanceCell.Options.UseTextOptions = true;
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].Caption = "SL đã tạo lệnh";
            grvListAutoStoreChange.Columns["StoreChangeOrderQuantity"].Caption = "SL đã tạo yêu cầu";
            grvListAutoStoreChange.Columns["StoreChangeOrderQuantity"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["StoreChangeCommandQuantity"].OptionsColumn.AllowEdit = false;
            grvListAutoStoreChange.Columns["Select"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            grvListAutoStoreChange.Columns["ProductID"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            grvListAutoStoreChange.Columns["ProductName"].Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;

            DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsUrgent = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            chkCheckBoxIsUrgent.ValueChecked = ((String)("True"));
            chkCheckBoxIsUrgent.ValueUnchecked = ((String)("False"));
            grvListAutoStoreChange.Columns["IsUrgent"].ColumnEdit = chkCheckBoxIsUrgent;

            DevExpress.XtraEditors.Repository.RepositoryItemTextEdit resQuantityEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            resQuantityEdit.AutoHeight = false;
            resQuantityEdit.DisplayFormat.FormatString = "#,##0";
            resQuantityEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            resQuantityEdit.EditFormat.FormatString = "#,###,###,##0.####";
            resQuantityEdit.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            resQuantityEdit.Mask.EditMask = "#,###,###,###,###,##0.####;";
            resQuantityEdit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            resQuantityEdit.Name = "resQuantityEdit";
            resQuantityEdit.NullText = "0";
            resQuantityEdit.NullValuePrompt = "0";
            resQuantityEdit.NullValuePromptShowForEmptyValue = true;
            grvListAutoStoreChange.Columns["Quantity"].ColumnEdit = resQuantityEdit;

            grvListAutoStoreChange.Columns["ProductID"].AppearanceCell.Font = new Font("Tahoma", 9.75F, FontStyle.Regular);
            grvListAutoStoreChange.Columns["ProductID"].AppearanceCell.ForeColor = Color.Red;
            grvListAutoStoreChange.Columns["ProductName"].AppearanceCell.Font = new Font("Tahoma", 9.75F, FontStyle.Regular);
            grvListAutoStoreChange.Columns["ProductName"].AppearanceCell.ForeColor = Color.Red;

            for (int i = 0; i < grvListAutoStoreChange.Columns.Count; i++)
            {
                grvListAutoStoreChange.Columns[i].OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            }

            grvListAutoStoreChange.OptionsView.ShowAutoFilterRow = true;
            //C1.Win.C1FlexGrid.CellStyle styleTitle = flexListAutoStoreChange.Styles.Add("flexStyleTitle");
            //styleTitle.WordWrap = true;
            //styleTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, FontStyle.Bold);
            //styleTitle.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            //C1.Win.C1FlexGrid.CellRange range = flexListAutoStoreChange.GetCellRange(0, 0, 0, flexListAutoStoreChange.Cols.Count - 1);
            //range.Style = styleTitle;
            //flexListAutoStoreChange.Rows[0].Height = 40;
        }

        private bool SaveAutoStoreChange()
        {
            List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand> lstStoreChangeCommand = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand>();
            for (int i = 0; i < dtbAutoStoreChange.Rows.Count; i++)
            {
                if (!Convert.ToBoolean(dtbAutoStoreChange.Rows[i]["Select"]))
                    continue;
                PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand objStoreChangeCommandBO = new PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand();
                objStoreChangeCommandBO.FromStoreID = Convert.ToInt32(dtbAutoStoreChange.Rows[i]["FromStoreID"]);
                objStoreChangeCommandBO.ToStoreID = Convert.ToInt32(dtbAutoStoreChange.Rows[i]["ToStoreID"]);
                objStoreChangeCommandBO.StoreChangeCommandTypeID = intStoreChangeCommandTypeID;
                objStoreChangeCommandBO.CreatedUser = SystemConfig.objSessionUser.UserName;
                objStoreChangeCommandBO.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objStoreChangeCommandBO.StoreChangeCommandTypeID = 1;
                objStoreChangeCommandBO.IsUrgent = Convert.ToBoolean(dtbAutoStoreChange.Rows[i]["IsUrgent"]);
                string strProductID = Convert.ToString(dtbAutoStoreChange.Rows[i]["ProductID"]).Trim();
                decimal decQuantity = Convert.ToDecimal("0" + Convert.ToDecimal(dtbAutoStoreChange.Rows[i]["Quantity"]));
                if (decQuantity > 0)
                {
                    List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail> lstStoreChangeCommandDetail = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail>();
                    PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail objStoreChangeCommandDetail = new PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail();
                    objStoreChangeCommandDetail.ProductID = strProductID;
                    objStoreChangeCommandDetail.Quantity = decQuantity;
                    if (!Convert.ToBoolean(dtbAutoStoreChange.Rows[i]["IsAllowDecimal"]))
                    {
                        objStoreChangeCommandDetail.Quantity = Math.Round(objStoreChangeCommandDetail.Quantity, 0, MidpointRounding.AwayFromZero);
                    }
                    objStoreChangeCommandDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;
                    lstStoreChangeCommandDetail.Add(objStoreChangeCommandDetail);
                    objStoreChangeCommandBO.StoreChangeCommandDetailList = lstStoreChangeCommandDetail.ToArray();
                }
                lstStoreChangeCommand.Add(objStoreChangeCommandBO);
            }
            objPLCStoreChangeCommand.InsertMulti(lstStoreChangeCommand);
            return !SystemConfig.objSessionUser.ResultMessageApp.IsError;
        }

        private void LoadComboBox()
        {
            cboType.SelectedIndex = 0;
            cboIsShowProduct.SelectedIndex = 0;
            //cboMainGroupID.InitControl(false);
            cboMainGroupID.InitControl(true);
            if (dtbStore != null && dtbStore.Rows.Count > 0)
            {
                cboFromStoreID.InitControl(false, dtbStore.Copy());
                cboToStoreID.InitControl(true, dtbStore.Copy());
            }
            else
            {
                cboFromStoreID.InitControl(false);
                dtbStore = cboFromStoreID.DataSource as DataTable;
                cboToStoreID.InitControl(true);
            }
            cboType_SelectionChangeCommitted(null, null);

            if (chkIsCanUseOldCode.Checked)
                dtbStoreOldId = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_STORE_OLDSTORE_SRH");
            dtbProductOld = Library.AppCore.DataSource.GetDataSource.GetProduct().Copy();
        }

        private void InitAutoStoreChangeTable()
        {
            strbFromStoreIDList = new StringBuilder();
            strbProductIDList = new StringBuilder();
            strbToStoreIDList = new StringBuilder();

            dtbAutoStoreChange = new DataTable();
            dtbAutoStoreChange.Columns.Add("Select", typeof(bool));
            dtbAutoStoreChange.Columns.Add("ProductID", typeof(string));
            dtbAutoStoreChange.Columns.Add("ProductName", typeof(String));
            dtbAutoStoreChange.Columns.Add("FromStoreID", typeof(int));
            dtbAutoStoreChange.Columns.Add("FromStoreName", typeof(String));
            dtbAutoStoreChange.Columns.Add("FromStoreInStock", typeof(decimal));
            dtbAutoStoreChange.Columns.Add("ToStoreID", typeof(int));
            dtbAutoStoreChange.Columns.Add("ToStoreName", typeof(String));
            dtbAutoStoreChange.Columns.Add("ToStoreInStock", typeof(decimal));
            dtbAutoStoreChange.Columns.Add("Quantity", typeof(decimal));
            dtbAutoStoreChange.Columns.Add("QuantityInStock", typeof(decimal));
            dtbAutoStoreChange.Columns.Add("IsAllowDecimal", typeof(bool));
            if (!dtbAutoStoreChange.Columns.Contains("StoreChangeCommandQuantity"))
            {
                DataColumn dtColumn = new DataColumn("StoreChangeCommandQuantity");
                dtColumn.DefaultValue = 0;
                dtbAutoStoreChange.Columns.Add(dtColumn);
            }

            if (!dtbAutoStoreChange.Columns.Contains("StoreChangeOrderQuantity"))
            {
                DataColumn dtColumn = new DataColumn("StoreChangeOrderQuantity");
                dtColumn.DefaultValue = 0;
                dtbAutoStoreChange.Columns.Add(dtColumn);
            }
            if (!dtbAutoStoreChange.Columns.Contains("IsUrgent"))
            {
                DataColumn dtColumn = new DataColumn("IsUrgent");
                dtColumn.DefaultValue = false;
                dtbAutoStoreChange.Columns.Add(dtColumn);
            }
            dtbAutoStoreChange.PrimaryKey = new DataColumn[3] { dtbAutoStoreChange.Columns["ProductID"], dtbAutoStoreChange.Columns["FromStoreID"], dtbAutoStoreChange.Columns["ToStoreID"] };
        }

        private void BindData()
        {
            if (dtbAutoStoreChange == null || dtbAutoStoreChange.Rows.Count == 0)
            {
                InitAutoStoreChangeTable();
                return;
            }
            else
            {
                if(dtbAutoStoreChange.PrimaryKey.Length==0)
                    dtbAutoStoreChange.PrimaryKey = new DataColumn[3] { dtbAutoStoreChange.Columns["ProductID"], dtbAutoStoreChange.Columns["FromStoreID"], dtbAutoStoreChange.Columns["ToStoreID"] };
            }
            if (!dtbAutoStoreChange.Columns.Contains("StoreChangeCommandQuantity"))
            {
                DataColumn dtColumn = new DataColumn("StoreChangeCommandQuantity");
                dtColumn.DefaultValue = 0;
                dtbAutoStoreChange.Columns.Add(dtColumn);
            }

            if (!dtbAutoStoreChange.Columns.Contains("StoreChangeOrderQuantity"))
            {
                DataColumn dtColumn = new DataColumn("StoreChangeOrderQuantity");
                dtColumn.DefaultValue = 0;
                dtbAutoStoreChange.Columns.Add(dtColumn);
            }
           

            //string strProductIDList = string.Empty;
            //string strFromStoreIDList = string.Empty;
            //string strToStoreIDList = string.Empty;
            if (dtbAutoStoreChange.Rows.Count > 0)
            {
                //foreach (DataRow item in dtbAutoStoreChange.Rows)
                //{
                //    if (strProductIDList != string.Empty)
                //        strProductIDList += ",";
                //    strProductIDList += item["ProductID"].ToString();
                //    if (strFromStoreIDList != string.Empty)
                //        strFromStoreIDList += ",";
                //    strFromStoreIDList += item["FromStoreID"].ToString();
                //    if (strToStoreIDList != string.Empty)
                //        strToStoreIDList += ",";
                //    strToStoreIDList += item["ToStoreID"].ToString();
                //}
            }
            dtbAutoStoreChangeQuantity = new PLC.StoreChangeCommand.PLCAutoSplitProduct().GetAutoStoreChangeQuantity(strbProductIDList.ToString(), strbFromStoreIDList.ToString(), strbToStoreIDList.ToString());
            if (!dtbAutoStoreChange.Columns.Contains("IsUrgent"))
            {
                DataColumn dtColumn = new DataColumn("IsUrgent");
                dtColumn.DefaultValue = false;
                dtbAutoStoreChange.Columns.Add(dtColumn);
            }
            if (dtbAutoStoreChange.Rows.Count > 0 && dtbAutoStoreChangeQuantity != null && dtbAutoStoreChangeQuantity.Rows.Count > 0)
            {
                foreach (DataRow item in dtbAutoStoreChange.Rows)
                {
                    string strFilter = string.Format("ProductID='{0}' and FromStoreID={1} and ToStoreID={2}", item["ProductID"].ToString().Trim(), item["FromStoreID"].ToString(), item["ToStoreID"].ToString());
                    DataRow[] drdata = dtbAutoStoreChangeQuantity.Select(strFilter + " and TypeID=0");
                    if (drdata.Length > 0)
                        item["StoreChangeCommandQuantity"] = drdata[0]["Quantity"];
                    drdata = dtbAutoStoreChangeQuantity.Select(strFilter + " and TypeID=1");
                    if (drdata.Length > 0)
                        item["StoreChangeOrderQuantity"] = drdata[0]["Quantity"];
                }
            }
            DataView dvAutoStoreChange = new DataView(dtbAutoStoreChange);
            dvAutoStoreChange.Sort = "ProductID, FromStoreID";
            //flexListAutoStoreChange.DataSource = dvAutoStoreChange;
            grdListAutoStoreChange.DataSource = dvAutoStoreChange;
            CustomFlex();
            if (dtbAutoStoreChange.Rows.Count > 0)
                cmdCreateStoreChangeCommand.Enabled = true;
        }

        private void AddProduct(String strProductID, String strProductName, int intFromStoreID, String strFromStoreName,
            int intToStoreID, String strToStoreName, decimal decFromInstockQuantity, decimal decToInstockQuantity,
            decimal decQuantity, bool bolIsAllowDecimal, bool bolIsUrgent)
        {

            DataRow objRow = dtbAutoStoreChange.NewRow();
            objRow.ItemArray = new object[] {true, strProductID, strProductName, intFromStoreID, strFromStoreName,
                decFromInstockQuantity, intToStoreID, strToStoreName, decToInstockQuantity, decQuantity,
                decFromInstockQuantity, bolIsAllowDecimal,0,0, bolIsUrgent};
            dtbAutoStoreChange.Rows.Add(objRow);
            //Append mã sản phẩm
            strbProductIDList.Append(strProductID); strbProductIDList.Append(',');
            //Append mã kho xuất
            strbFromStoreIDList.Append(intFromStoreID); strbFromStoreIDList.Append(',');
            //Append mã kho nhập
            strbToStoreIDList.Append(intToStoreID); strbToStoreIDList.Append(',');
        }

        private bool CheckInputAdd()
        {
            if (string.IsNullOrEmpty(cboMainGroupID.MainGroupIDList.Trim()))
            {
                MessageBox.Show(this, "Vui lòng chọn ngành hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMainGroupID.Focus();
                return false;
            }

            if (cboFromStoreID.StoreID <= 0)
            {
                string strMess = "Vui lòng chọn kho xuất!";
                if (cboType.SelectedIndex == 1)
                    strMess = "Vui lòng chọn kho nhập!";
                MessageBox.Show(this, strMess, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboFromStoreID.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(cboToStoreID.StoreIDList))
            {
                string strMess = "Vui lòng chọn kho nhập!";
                if (cboType.SelectedIndex == 1)
                    strMess = "Vui lòng chọn kho xuất!";
                MessageBox.Show(this, strMess, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboToStoreID.Focus();
                return false;
            }
            if (cboType.SelectedIndex == 0)
            {
                int intFromStoreID = Convert.ToInt32(cboFromStoreID.StoreID);
                string strToStoreIDList = cboToStoreID.StoreIDList;
                string[] strArrToStoreID = strToStoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
                if (strArrToStoreID.Contains(intFromStoreID.ToString()))
                {
                    MessageBox.Show("Kho nhập phải khác với kho xuất!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboToStoreID.Focus();
                    return false;
                }
                string strProductID = txtProductID.Text.Trim();
                if (strProductID == string.Empty)
                {
                    MessageBox.Show(this, "Vui lòng chọn sản phẩm!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtProductID.Focus();
                    return false;
                }
                
                for (int i = 0; i < strArrToStoreID.Length; i++)
                {
                    if (CheckProductExist(intFromStoreID, Convert.ToInt32(strArrToStoreID[i]), strProductID))
                    {
                        MessageBox.Show(this, "Sản phẩm chuyển kho đã tồn tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
                if (Convert.ToDecimal(txtQuantity.Value) <= 0)
                {
                    MessageBox.Show(this, "Vui lòng nhập số lượng cần chuyển!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtQuantity.Focus();
                    return false;
                }
                decimal decInStockQuantity = new PLC.PLCProductInStock().GetQuantity(strProductID,
                    Convert.ToInt32(cboFromStoreID.StoreID), 1, intIsShowProduct, intIsCheckRealInput);
                if (Convert.ToDecimal(txtQuantity.Value) > decInStockQuantity)
                {
                    MessageBox.Show(this, "Số lượng cần chuyển lớn hơn số lượng tồn!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtQuantity.Value = decInStockQuantity;
                    txtQuantity.Focus();
                    return false;
                }

                if (!CheckQuantity(strProductID, ERP.MasterData.PLC.MD.PLCProduct.GetProductName(strProductID)
                    , Convert.ToInt32(cboFromStoreID.StoreID), ERP.MasterData.PLC.MD.PLCStore.GetStoreName(Convert.ToInt32(cboFromStoreID.StoreID)), decInStockQuantity,
                    Convert.ToDecimal(txtQuantity.Value)))
                    return false;
                return true;
            }
            else
            {
                int intToStoreID = Convert.ToInt32(cboFromStoreID.StoreID);
                string strFromStoreIDList = cboToStoreID.StoreIDList;
                string[] strArrFromStoreID = strFromStoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
                if (strArrFromStoreID.Contains(intToStoreID.ToString()))
                {
                    MessageBox.Show("Kho nhập phải khác với kho xuất!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboToStoreID.Focus();
                    return false;
                }
                string strProductID = txtProductID.Text.Trim();
                if (strProductID == string.Empty)
                {
                    MessageBox.Show(this, "Vui lòng chọn sản phẩm!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtProductID.Focus();
                    return false;
                }
                
                for (int i = 0; i < strArrFromStoreID.Length; i++)
                {
                    if (CheckProductExist(Convert.ToInt32(strArrFromStoreID[i]), intToStoreID, strProductID))
                    {
                        MessageBox.Show(this, "Sản phẩm chuyển kho đã tồn tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
                if (Convert.ToDecimal(txtQuantity.Value) <= 0)
                {
                    MessageBox.Show(this, "Vui lòng nhập số lượng cần chuyển!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtQuantity.Focus();
                    return false;
                }
                for (int i = 0; i < strArrFromStoreID.Length; i++)
                {
                    decimal decInStockQuantity = new PLC.PLCProductInStock().GetQuantity(strProductID,
                        Convert.ToInt32(strArrFromStoreID[i]), 1, intIsShowProduct, intIsCheckRealInput);
                    if (Convert.ToDecimal(txtQuantity.Value) > decInStockQuantity)
                    {
                        string strProductName = ERP.MasterData.PLC.MD.PLCProduct.GetProductName(strProductID);
                        string strStoreName = ERP.MasterData.PLC.MD.PLCStore.GetStoreName(Convert.ToInt32(strArrFromStoreID[i]));
                        MessageBox.Show(this, "Số lượng cần chuyển của sản phẩm " + strProductName + "\n tại kho " + strStoreName + " lớn hơn số lượng tồn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtQuantity.Value = decInStockQuantity;
                        txtQuantity.Focus();
                        return false;
                    }
                }
                return true;
            }
        }

        private bool CheckProductExist(int intFromStoreID, int intToStoreID, String strProductID)
        {
            //String strSelectExp = "FromStoreID = " + Convert.ToString(intFromStoreID) + " and ToStoreID = "
            //    + Convert.ToString(intToStoreID) + " and ProductID = '" + strProductID + "'";
            //DataRow[] arrRow = dtbAutoStoreChange.Select(strSelectExp);
            //return (arrRow.Length > 0);
            DataRow drowcheck = dtbAutoStoreChange.Rows.Find(new object[3] { strProductID, intFromStoreID, intToStoreID });
            return (drowcheck != null);
        }

        private bool CheckQuantity()
        {
            dtbAutoStoreChange.AcceptChanges();
            for (int i = 0; i < grvListAutoStoreChange.RowCount; i++)
            {
                DataRow row = grvListAutoStoreChange.GetDataRow(i);
                if (row == null)
                    continue;

                decimal decFromInstockQuanity = Convert.ToDecimal(row["FromStoreInStock"]);
                decimal decTotalQuantity = Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbAutoStoreChange.Compute("sum(Quantity)",
                    "Select = 1 and ProductID = '" + Convert.ToString(row["ProductID"]).Trim()
                    + "' and FromStoreID = " + Convert.ToString(row["FromStoreID"])), 0));
                if (decTotalQuantity > decFromInstockQuanity)
                {
                    MessageBox.Show(this, "Tổng số lượng chuyển kho của sản phẩm "
                        + Convert.ToString(row["ProductName"]).Trim()
                        + " \nTại kho " + Convert.ToString(row["FromStoreName"]).Trim()
                        + " lớn hơn số lượng tồn." + (decFromInstockQuanity > decTotalQuantity ? (". Số lượng còn lại " + (decFromInstockQuanity - decTotalQuantity)) : ""), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    grvListAutoStoreChange.FocusedColumn = grvListAutoStoreChange.Columns["Quantity"];
                    grvListAutoStoreChange.FocusedRowHandle = i;
                    return false;
                }
            }

            //for (int i = 1; i < flexListAutoStoreChange.Rows.Count; i++)
            //{
            //    decimal decFromInstockQuanity = Convert.ToDecimal(flexListAutoStoreChange[i, "FromStoreInStock"]);
            //    decimal decTotalQuantity = Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbAutoStoreChange.Compute("sum(Quantity)",
            //        "Select = 1 and ProductID = '" + Convert.ToString(flexListAutoStoreChange[i, "ProductID"]).Trim()
            //        + "' and FromStoreID = " + Convert.ToString(flexListAutoStoreChange[i, "FromStoreID"])), 0));
            //    if (decTotalQuantity > decFromInstockQuanity)
            //    {
            //        MessageBox.Show(this, "Tổng số lượng chuyển kho của sản phẩm "
            //            + Convert.ToString(flexListAutoStoreChange[i, "ProductName"]).Trim()
            //            + " \nTại kho " + Convert.ToString(flexListAutoStoreChange[i, "FromStoreName"]).Trim()
            //            + " lớn hơn số lượng tồn." + (decFromInstockQuanity > decTotalQuantity ? (". Số lượng còn lại " + (decFromInstockQuanity - decTotalQuantity)) : ""), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        flexListAutoStoreChange.Select(i, 9);
            //        return false;
            //    }
            //}

            return true;
        }

        private bool CheckQuantity(string strProductID, string strProductName, int intFromStoreID, string strFromStoreName, decimal decFromInstockQuanity, decimal decQuanityAdd)
        {
            dtbAutoStoreChange.AcceptChanges();
            decimal decTotalQuantity = Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbAutoStoreChange.Compute("sum(Quantity)",
                "Select = 1 and ProductID = '" + strProductID.Trim()
                + "' and FromStoreID = " + intFromStoreID), 0));
            if (decTotalQuantity + decQuanityAdd > decFromInstockQuanity)
            {
                MessageBox.Show(this, "Tổng số lượng chuyển kho của sản phẩm "
                    + strProductName.Trim()
                    + " \nTại kho " + strFromStoreName
                    + " lớn hơn số lượng tồn" + (decFromInstockQuanity > decTotalQuantity ? (". Số lượng còn lại " + (decFromInstockQuanity - decTotalQuantity)) : ""), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private DataTable CreateImportResultTable()
        {
            DataTable tblResult = new DataTable();
            tblResult.Columns.Add("ProductID", typeof(String));
            tblResult.Columns.Add("ProductName", typeof(String));
            tblResult.Columns.Add("FromStoreName", typeof(String));
            tblResult.Columns.Add("FromInStockQuantity", typeof(decimal));
            tblResult.Columns.Add("ToStoreName", typeof(String));
            tblResult.Columns.Add("ToInStockQuantity", typeof(decimal));
            tblResult.Columns.Add("Quantity", typeof(decimal));
            tblResult.Columns.Add("Status", typeof(String));
            tblResult.Columns.Add("StatusID", typeof(Int32));
            tblResult.Columns.Add("IsUrgent", typeof(Boolean));
            tblResult.Columns.Add("ErrorContent", typeof(String));
            return tblResult;
        }
        private void ImportExcel()
        {
            DataTable tblProduct = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(6);
            if (tblProduct == null)
            {
                MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (tblProduct.Rows.Count == 0)
                return;
            tblProduct.Rows.RemoveAt(0);
            DataTable tblResult = CreateImportResultTable();
            dtbAutoStoreChange.Clear();
            strbFromStoreIDList = new StringBuilder();
            strbProductIDList = new StringBuilder();
            strbToStoreIDList = new StringBuilder();
            bool bolIsPermission = Library.AppCore.SystemConfig.objSessionUser.IsPermission("AUTOSTORECHANGE_CHECKEMERGENCY");
            int intIsUrgent = 0;
            bool bolIsExistsError = false;
            try
            {
                foreach (DataRow objRow in tblProduct.Rows)
                {
                    String strProductID = Convert.ToString(objRow[0]).Trim();
                    int intFromStoreID = -1;
                    int intToStoreID = -1;
                    String strStatus = "";
                    String strErrorContent = "";
                    int intStatusID = 0;
                    decimal decQuanity = 0;
                    try
                    {
                        intFromStoreID = Convert.ToInt32(objRow[1]);
                        intToStoreID = Convert.ToInt32(objRow[2]);
                        decQuanity = Convert.ToDecimal(objRow[3]);
                    }
                    catch (Exception)
                    {
                        strStatus = "Lỗi";
                        strErrorContent = "[Từ kho] và [Đến kho] phải là số";
                        intStatusID = 1;
                    }
                    decimal decRealQuanity = decQuanity;
                    String strFromStoreName = ERP.MasterData.PLC.MD.PLCStore.GetStoreName(intFromStoreID);
                    String strToStoreName = ERP.MasterData.PLC.MD.PLCStore.GetStoreName(intToStoreID);
                    decimal decFromInstockQuantity = 0;
                    decimal decToInstockQuantity = 0;
                    //Nếu có quyền AutoStoreChange_CheckEmergency thì cho chuyển gấp
                    if (bolIsPermission)
                    {
                        try
                        {
                            if (Convert.IsDBNull(objRow[4]))
                                intIsUrgent = 0;
                            else
                            {
                                intIsUrgent = Convert.ToInt32(objRow[4]);
                                if (intIsUrgent > 1 || intIsUrgent < 0)
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Sai thông tin Chuyển gấp. Chỉ có thể là 0 hoặc 1";
                                    intStatusID = 1;
                                    intIsUrgent = 0;
                                }
                            }
                        }
                        catch
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Sai thông tin Chuyển gấp. Chỉ có thể là 0 hoặc 1";
                            intStatusID = 1;
                        }
                    }
                    String strProductName = string.Empty;
                    if (strFromStoreName.Length > 0 && strToStoreName.Length > 0)
                    {
                        decFromInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intFromStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                        decToInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                        ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                        string strMainGroupList = cboMainGroupID.MainGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");
                        string[] mainGroupList = strMainGroupList.Split(',');
                        if (objProduct == null)
                            return;
                        strProductName = objProduct.ProductName;
                        if (intFromStoreID == intToStoreID)
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Kho nhập trùng với kho xuất";
                            intStatusID = 1;
                        }
                        else if (string.IsNullOrEmpty(strMainGroupList.Trim()) || !mainGroupList.Any(x => x.Trim().Equals(objProduct.MainGroupID.ToString().Trim())))
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Sản phẩm không thuộc ngành hàng đang xét";
                            intStatusID = 1;
                        }
                        else if (!CheckMainGroupPermission(intFromStoreID, objProduct.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + objProduct.MainGroupName;
                            intStatusID = 1;
                        }
                        else if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intToStoreID, objProduct.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + objProduct.MainGroupName;
                            intStatusID = 1;
                        }

                        else if (!CheckPermission(objProduct.MainGroupID, EnumType.IsNewPermissionType.ALL))
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Bạn không có quyền thao tác trên ngành hàng " + objProduct.MainGroupName;
                            intStatusID = 1;
                        }
                        else if (decQuanity > 0)
                        {
                            if (CheckProductExist(intFromStoreID, intToStoreID, strProductID))
                            {
                                strStatus = "Lỗi";
                                strErrorContent = "Sản phẩm chuyển kho đã tồn tại";
                                intStatusID = 1;
                            }
                            else
                            {
                                if (decFromInstockQuantity <= 0)
                                {
                                    strStatus = "Lỗi";
                                    strErrorContent = "Sản phẩm không tồn kho";
                                    intStatusID = 1;
                                }
                                else
                                {
                                    if (decQuanity > decFromInstockQuantity)
                                    {
                                        strStatus = "Cảnh báo";
                                        strErrorContent = "Số lượng chuyển lớn hơn số lượng tồn";
                                        decRealQuanity = decFromInstockQuantity;
                                        intStatusID = 2;
                                    }
                                    else
                                    {
                                        strStatus = "Thành công";
                                        strErrorContent = "";
                                        intStatusID = 0;
                                    }
                                    AddProduct(strProductID, strProductName, intFromStoreID, strFromStoreName, intToStoreID,
                                        strToStoreName, decFromInstockQuantity, decToInstockQuantity, decRealQuanity, true, Convert.ToBoolean(intIsUrgent));

                                }
                            }
                        }
                        else
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Số lượng chuyển phải lớn hơn 0";
                            intStatusID = 1;
                        }
                    }
                    else
                    {
                        if (strFromStoreName.Length == 0)
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Sai kho xuất";
                            intStatusID = 1;
                        }
                        else
                        {
                            strStatus = "Lỗi";
                            strErrorContent = "Sai kho nhập";
                            intStatusID = 1;
                        }
                    }
                    if (!bolIsExistsError && intStatusID != 0)
                    {
                        bolIsExistsError = true;
                    }
                    DataRow objRowResult = tblResult.NewRow();
                    objRowResult.ItemArray = new object[] { strProductID, strProductName, strFromStoreName,
                        decFromInstockQuantity, strToStoreName, decToInstockQuantity, decQuanity, strStatus,
                        intStatusID, intIsUrgent, strErrorContent };
                    tblResult.Rows.Add(objRowResult);
                }
            }
            catch (Exception objExce)
            {
                Library.AppCore.SystemErrorWS.Insert("Lỗi nhập thông tin chuyển kho từ Excel ", objExce, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Tập tin Excel không đúng định dạng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (bolIsExistsError)
            {
                frmShowImportResult frmShowImportResult1 = new frmShowImportResult();
                frmShowImportResult1.ImportResult = tblResult;
                frmShowImportResult1.ShowDialog();
            }
        }

        /// <summary>
        /// Kiểm tra dữ liệu nhập vào
        /// </summary>
        private bool CheckInputCreateOrder(ERP.MasterData.PLC.MD.WSStoreChangeType.StoreChangeType objStoreChangeType, DataRow drData)
        {
            int intFromStoreID = Convert.ToInt32(drData["FromStoreID"]);
            int intToStoreID = Convert.ToInt32(drData["ToStoreID"]);

            //DataRow[] rowFromStore = dtbStoreCache.Select("STOREID = " + intFromStoreID);
            DataRow rowFromStore = dtbStoreCache.Rows.Find(intFromStoreID);
            //DataRow[] rowToStore = dtbStoreCache.Select("STOREID = " + intToStoreID);
            DataRow rowToStore = dtbStoreCache.Rows.Find(intToStoreID);
            // 1 Kiem tra cong ty
            if (objStoreChangeType.CheckCompanyType == 1)
            {
                if (Convert.ToInt32(rowFromStore["COMPANYID"]) != Convert.ToInt32(rowToStore["COMPANYID"]))
                    return false;
            }
            else if (objStoreChangeType.CheckCompanyType == 2)
            {
                if (Convert.ToInt32(rowFromStore["COMPANYID"]) == Convert.ToInt32(rowToStore["COMPANYID"]))
                    return false;
            }
            // 2 kiem tra tỉnh
            if (objStoreChangeType.CheckProvinceType == 1)
            {
                if (Convert.ToInt32(rowFromStore["PROVINCEID"]) != Convert.ToInt32(rowToStore["PROVINCEID"]))
                    return false;
            }
            else if (objStoreChangeType.CheckProvinceType == 2)
            {
                if (Convert.ToInt32(rowFromStore["PROVINCEID"]) == Convert.ToInt32(rowToStore["PROVINCEID"]))
                    return false;
            }

            // 3 Kiem tra chi nhánh
            if (objStoreChangeType.CheckBranchType == 1)
            {
                if (Convert.ToInt32(rowFromStore["BRANCHID"]) != Convert.ToInt32(rowToStore["BRANCHID"]))
                    return false;
            }
            else if (objStoreChangeType.CheckBranchType == 2)
            {
                if (Convert.ToInt32(rowFromStore["BRANCHID"]) == Convert.ToInt32(rowToStore["BRANCHID"]))
                    return false;
            }

            //bool bolIsSameProvince = CheckSameProvince(, Convert.ToInt32(drData["ToStoreID"]));
            ////if (objStoreChangeOrderType.StoreChangeTypeID == 1 && !bolIsSameProvince)
            ////{
            ////    return false;
            ////}
            ////if (objStoreChangeOrderType.StoreChangeTypeID == 2 && bolIsSameProvince)
            ////{
            ////    return false;
            //}
            return true;

        }


        /// <summary>
        /// Kiểm tra cùng tỉnh
        /// </summary>
        /// <param name="intFromStoreID">Mã kho xuất.</param>
        /// <param name="intToStoreID">Mã kho nhập.</param>
        private bool CheckSameProvince(int intFromStoreID, int intToStoreID)
        {
            ERP.MasterData.PLC.MD.WSStore.Store objFromStore = null;
            ERP.MasterData.PLC.MD.WSStore.ResultMessage objResultMessage = new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objFromStore, intFromStoreID);
            ERP.MasterData.PLC.MD.WSStore.Store objToStore = null;
            objResultMessage = new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, intToStoreID);
            return (objFromStore.ProvinceID == objToStore.ProvinceID);
        }


        #endregion

        private void cmdAdd_Click(object sender, EventArgs e)
        {
            if (!CheckInputAdd())
                return;
            if (cboType.SelectedIndex == 0)
            {
                int intFromStoreID = Convert.ToInt32(cboFromStoreID.StoreID);
                string strToStoreIDList = cboToStoreID.StoreIDList;
                String strProductID = txtProductID.Text.Trim();
                ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                string strMainGroupList = cboMainGroupID.MainGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");
                string[] mainGroupList = strMainGroupList.Split(',');
                if (objProduct == null)
                    return;
                if (string.IsNullOrEmpty(strMainGroupList.Trim()) || !mainGroupList.Any(x => x.Trim().Equals(objProduct.MainGroupID.ToString().Trim())))
                {
                    MessageBox.Show(this, "Sản phẩm cần thêm không thuộc ngành hàng đang xét", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intFromStoreID, objProduct.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                {
                    MessageBox.Show(this, "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + objProduct.MainGroupName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                decimal decQuantity = Convert.ToDecimal(txtQuantity.Value);
                decimal decFromInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intFromStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                string[] strArrToStoreID = strToStoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
                for (int i = 0; i < strArrToStoreID.Length; i++)
                {
                    int intToStoreID = Convert.ToInt32(strArrToStoreID[i]);
                    string strToStoreName = ERP.MasterData.PLC.MD.PLCStore.GetStoreName(Convert.ToInt32(intToStoreID));
                    if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intToStoreID, objProduct.MainGroupID,
                    Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                    {
                        MessageBox.Show(this, "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + objProduct.MainGroupName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    decimal decToInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                    if (!objProduct.IsAllowDecimal)
                    {
                        decFromInstockQuantity = Math.Round(decFromInstockQuantity);
                        decToInstockQuantity = Math.Round(decToInstockQuantity);
                        decQuantity = Math.Round(decQuantity);
                    }

                    AddProduct(objProduct.ProductID, objProduct.ProductName, intFromStoreID, cboFromStoreID.Text.Trim(),
                        intToStoreID, strToStoreName, decFromInstockQuantity, decToInstockQuantity, decQuantity, objProduct.IsAllowDecimal, false);
                }
            }
            else
            {
                int intToStoreID = Convert.ToInt32(cboFromStoreID.StoreID);
                string strFromStoreIDList = cboToStoreID.StoreIDList;
                String strProductID = txtProductID.Text.Trim();
                ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                string strMainGroupList = cboMainGroupID.MainGroupIDList.Replace("><", ", ").Replace("<", "").Replace(">", "");
                string[] mainGroupList = strMainGroupList.Split(',');
                if (objProduct == null)
                    return;
                if (string.IsNullOrEmpty(strMainGroupList.Trim()) || !mainGroupList.Any(x => x.Trim().Equals(objProduct.MainGroupID.ToString().Trim())))
                {
                    MessageBox.Show(this, "Sản phẩm cần thêm không thuộc ngành hàng đang xét", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intToStoreID, objProduct.MainGroupID, Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                {
                    MessageBox.Show(this, "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + objProduct.MainGroupName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                decimal decQuantity = Convert.ToDecimal(txtQuantity.Value);
                decimal decToInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                string[] strArrFromStoreID = strFromStoreIDList.Trim('<', '>').Split(new string[] { "><" }, StringSplitOptions.None);
                for (int i = 0; i < strArrFromStoreID.Length; i++)
                {
                    int intFromStoreID = Convert.ToInt32(strArrFromStoreID[i]);
                    string strFromStoreName = ERP.MasterData.PLC.MD.PLCStore.GetStoreName(Convert.ToInt32(intFromStoreID));
                    if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intFromStoreID, objProduct.MainGroupID,
                    Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                    {
                        MessageBox.Show(this, "Kho " + strFromStoreName + " không được phép xuất chuyển kho trên ngành hàng " + objProduct.MainGroupName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    decimal decFromInstockQuantity = objPLCProductInStock.GetQuantity(strProductID, intToStoreID, 1, intIsShowProduct, intIsCheckRealInput);
                    if (!objProduct.IsAllowDecimal)
                    {
                        decFromInstockQuantity = Math.Round(decFromInstockQuantity);
                        decToInstockQuantity = Math.Round(decToInstockQuantity);
                        decQuantity = Math.Round(decQuantity);
                    }

                    AddProduct(objProduct.ProductID, objProduct.ProductName, intFromStoreID, strFromStoreName,
                        intToStoreID, cboFromStoreID.Text.Trim(), decFromInstockQuantity, decToInstockQuantity, decQuantity, objProduct.IsAllowDecimal, false);
                }
            }
            BindData();
        }

        private void btnSelectProduct_Click(object sender, EventArgs e)
        {
            if (!btnSelectProduct.Enabled)
                return;
            ERP.MasterData.DUI.Search.frmProductSearch frmProductSearch1 = ERP.MasterData.DUI.MasterData_Globals.frmProductSearch;
            frmProductSearch1.IsServiceType = Library.AppCore.Constant.EnumType.IsServiceType.NOT_ISSERVICE;
            //frmProductSearch1.MainGroupID = cboMainGroupID.MainGroupID;
            frmProductSearch1.MainGroupIDList = cboMainGroupID.MainGroupIDList;
            frmProductSearch1.IsMultiSelect = false;
            frmProductSearch1.ShowDialog();
            if (frmProductSearch1.Product != null)
            {
                txtProductID.Text = frmProductSearch1.Product.ProductID.Trim();
            }
            else
                txtProductID.Text = string.Empty;

        }

        private void btnExportTemplate_Click(object sender, EventArgs e)
        {
            ExportTemplate();
            #region 12/01/2016 Mi bỏ, theo mẫu excel mới
            //DataTable dtbExportTemplate = new DataTable();
            //dtbExportTemplate.Columns.Add("Mã sản phẩm", typeof(string));
            //dtbExportTemplate.Columns.Add("Từ kho", typeof(int));
            //dtbExportTemplate.Columns.Add("Đến kho", typeof(int));
            //dtbExportTemplate.Columns.Add("Số lượng", typeof(decimal));
            //if (SystemConfig.objSessionUser.IsPermission("AUTOSTORECHANGE_CHECKEMERGENCY"))
            //    dtbExportTemplate.Columns.Add("Chuyển gấp", typeof(int));

            //DataRow dr = dtbExportTemplate.NewRow();
            //dtbExportTemplate.Rows.Add(dr);
            //dtbExportTemplate.AcceptChanges();
            ////C1.Win.C1FlexGrid.C1FlexGrid flexExportTemplate = new C1.Win.C1FlexGrid.C1FlexGrid();
            //flexExportTemplate.DataSource = dtbExportTemplate;
            //flexExportTemplate.AutoSizeCols();
            ////flexExportTemplate.Cols[0].Visible = false;
            //Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexExportTemplate);//, C1.Win.C1FlexGrid.FileFlags.VisibleOnly | C1.Win.C1FlexGrid.FileFlags.IncludeFixedCells);
            #endregion
        }

        private void cmdImportExcel_Click(object sender, EventArgs e)
        {
            if (grvListAutoStoreChange.RowCount > 1)
            {
                if (MessageBox.Show(this, "Trên lưới đã có dữ liệu.\nChương trình sẽ xóa dữ liệu cũ và thay thế bằng dữ liệu mới", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                {
                    return;
                }
            }
            dtbAutoStoreChange.Clear();
            strbFromStoreIDList = new StringBuilder();
            strbProductIDList = new StringBuilder();
            strbToStoreIDList = new StringBuilder();
            if (cboFromStoreID.StoreID < 1)
            {
                string strMess = "Bạn phải chọn kho xuất";
                if (cboType.SelectedIndex == 1)
                    strMess = "Bạn phải chọn kho nhập";
                MessageBoxObject.ShowWarningMessage(this, strMess);
                cboFromStoreID.Focus();
                return;
            }
            if (string.IsNullOrEmpty(cboMainGroupID.MainGroupIDList.Trim()))
            {
                MessageBox.Show(this, "Vui lòng chọn ngành hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMainGroupID.Focus();
                return;
            }
            EnableControl(false);
            ImportExcelNew();
            BindData();
            EnableControl(true);
        }

        private void cmdCreateStoreChangeCommand_Click(object sender, EventArgs e)
        {
            Boolean bolIsSelected = false;
            for (int i = 0; i < grvListAutoStoreChange.RowCount; i++)
            {
                if (Convert.ToBoolean(grvListAutoStoreChange.GetDataRow(i)["Select"]))
                {
                    bolIsSelected = true;
                    break;
                }
            }

            if (!bolIsSelected)
            {
                MessageBox.Show(this, "Vui lòng chọn ít nhất một mặt hàng cần chuyển!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            bool bolIsReceive = cboType.SelectedIndex == 1 ? true : false;
            if (!bolIsReceive)
            {
                if (!CheckQuantity())
                    return;
            }
            if (bolIsAutoCreateOrder)
            {
                frmSelectInfo frm = new frmSelectInfo();
                frm.STORECHANGECOMMANDTYPEID = intStoreChangeCommandTypeID;
                frm.ShowDialog();
                if (frm.IsAccept)
                {
                    ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = frm.StoreChangeOrderType;
                    objStoreChangeType = new ERP.MasterData.PLC.MD.PLCStoreChangeType().LoadInfo((int)objStoreChangeOrderType.StoreChangeTypeID);
                    DataTable dtbDataSource = dtbAutoStoreChange.Clone();
                    List<int> lstIndex = new List<int>();
                    for (int i = 0; i < dtbAutoStoreChange.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(dtbAutoStoreChange.Rows[i]["Select"]))
                        {
                            if (CheckInputCreateOrder(objStoreChangeType, dtbAutoStoreChange.Rows[i]))
                            {
                                dtbDataSource.ImportRow(dtbAutoStoreChange.Rows[i]);
                                lstIndex.Add(i);
                            }

                        }
                    }

                    //foreach (DataRow dr in dtbAutoStoreChange.Rows)
                    //{
                    //    if (Convert.ToBoolean(dr["Select"]))
                    //    {
                    //        if (CheckInputCreateOrder(objStoreChangeOrderType, dr))
                    //        {
                    //            dtbDataSource.ImportRow(dr);
                    //            //dr.Delete();
                    //        }
                    //    }
                    //}
                    frmCreateStoreChangeOrder frmCreateStoreChangeOrder = new frmCreateStoreChangeOrder();
                    frmCreateStoreChangeOrder.DataSource = dtbDataSource;
                    frmCreateStoreChangeOrder.StoreChangeOrderType = objStoreChangeOrderType;
                    frmCreateStoreChangeOrder.StoreChangeCommandTypeID = intStoreChangeCommandTypeID;
                    frmCreateStoreChangeOrder.ShowDialog();
                    if (frmCreateStoreChangeOrder.IsAccept)
                    {
                        // Clear;
                       
                        for(int j = lstIndex.Count - 1; j >= 0; j--)
                            dtbAutoStoreChange.Rows.RemoveAt(lstIndex[j]);

                        cmdCreateStoreChangeCommand.Enabled = false;
                        grvListAutoStoreChange.OptionsBehavior.Editable = false;
                        cmdAdd.Enabled = false;
                        cmdImportExcel.Enabled = false;
                        if (dtbDataSource.Rows.Count == dtbAutoStoreChange.Rows.Count)
                        {
                            if (intMainGroupID == 0)
                            {
                                dtbAutoStoreChange = null;
                                strbFromStoreIDList = new StringBuilder();
                                strbProductIDList = new StringBuilder();
                                strbToStoreIDList = new StringBuilder();
                                BindData();
                                grvListAutoStoreChange.OptionsBehavior.Editable = true;
                                grdListAutoStoreChange.DataSource = dtbAutoStoreChange;
                                CustomFlex();
                                cmdCreateStoreChangeCommand.Enabled = false;
                                cmdAdd.Enabled = true;
                                cmdImportExcel.Enabled = true;
                                cboFromStoreID.SetValue(-1);
                                cboToStoreID.SetValue(-1);
                                txtProductID.Text = string.Empty;
                                txtQuantity.Value = 0;
                            }
                        }
                        else
                        {
                            dtbAutoStoreChange.AcceptChanges();
                            BindData();
                            grvListAutoStoreChange.OptionsBehavior.Editable = true;
                            grdListAutoStoreChange.DataSource = dtbAutoStoreChange;
                            CustomFlex();
                            cmdCreateStoreChangeCommand.Enabled = dtbAutoStoreChange.Rows.Count > 0;
                            cmdAdd.Enabled = true;
                            cmdImportExcel.Enabled = true;
                            cboFromStoreID.SetValue(-1);
                            cboToStoreID.SetValue(-1);
                            txtProductID.Text = string.Empty;
                            txtQuantity.Value = 0;
                        }
                    }
                    else
                    {
                        dtbAutoStoreChange.AcceptChanges();
                        //if (dtbDataSource != null && dtbDataSource.Rows.Count > 0)
                        //    foreach (DataRow dr in dtbDataSource.Rows)
                        //    {
                        //        dtbAutoStoreChange.ImportRow(dr);
                        //    }
                    }
                    if (bolIsCallFromAutoDistribute)
                        this.Close();
                }
            }
            else
            {
                if (!SaveAutoStoreChange())
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                            SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                cmdCreateStoreChangeCommand.Enabled = false;
                grvListAutoStoreChange.OptionsBehavior.Editable = false;
                cmdAdd.Enabled = false;
                cmdImportExcel.Enabled = false;
                MessageBox.Show(this, "Tạo lệnh chuyển kho thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (intMainGroupID == 0)
                {
                    dtbAutoStoreChange = null;
                    strbFromStoreIDList = new StringBuilder();
                    strbProductIDList = new StringBuilder();
                    strbToStoreIDList = new StringBuilder();
                    BindData();
                    grvListAutoStoreChange.OptionsBehavior.Editable = true;
                    grdListAutoStoreChange.DataSource = dtbAutoStoreChange;
                    CustomFlex();
                    cmdCreateStoreChangeCommand.Enabled = false;
                    cmdAdd.Enabled = true;
                    cmdImportExcel.Enabled = true;
                    cboFromStoreID.SetValue(-1);
                    cboToStoreID.SetValue(-1);
                    txtProductID.Text = string.Empty;
                    txtQuantity.Value = 0;
                }
                if (bolIsCallFromAutoDistribute)
                    this.Close();
            }
        }

        private void frmAutoStoreChangeDetail_Load(object sender, EventArgs e)
        {
            Rectangle screen = Screen.PrimaryScreen.WorkingArea;
            this.Location = new Point((screen.Width - 1050) / 2, (screen.Height - screen.Height) / 2);
            this.Size = new Size(1050, screen.Height);
            dtbStoreCache = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
            dtbStoreMainGroup = Library.AppCore.DataSource.GetDataSource.GetStoreMainGroup().Copy();
            intStoreChangeCommandTypeID = AppConfig.GetIntConfigValue("AUTOSTORECHANGE_STORECHANGECOMMANDTYPEID");
            if (intStoreChangeCommandTypeID < 0)
            {
                MessageBox.Show(this, "Bạn chưa cấu hình loại lệnh chuyển kho mặc định", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }

            // LE VAN DONG - 04/07/2017
            if (this.Tag != null && this.Tag.ToString().Trim() != string.Empty)
            {
                try
                {
                    Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());

                    if (hstbParam.Contains("IsCanUseOldID"))
                    {
                        bolIsCanUseOldCode = hstbParam["IsCanUseOldID"].ToString().Trim() == "1";
                        chkIsCanUseOldCode.Checked = bolIsCanUseOldCode;
                        chkIsCanUseOldCode.Enabled = bolIsCanUseOldCode;
                    }
                    else
                    {
                        chkIsCanUseOldCode.Checked = false;
                        chkIsCanUseOldCode.Enabled = true;
                    }

                    if (hstbParam.Contains("AutoCreateOrder"))
                    {
                        cmdCreateStoreChangeCommand.Text = "Tạo YC chuyển kho";
                        bolIsAutoCreateOrder = hstbParam["AutoCreateOrder"].ToString().Trim() == "1";
                    }
                }
                catch { }
            }
            else
            {
                chkIsCanUseOldCode.Checked = false;
                chkIsCanUseOldCode.Enabled = false;
            }




            if (bolIsCallFromAutoDistribute)
            {
                dtbMainGroupPermission = Library.AppCore.DataSource.GetDataFilter.GetMainGroup(true, EnumType.IsRequestIMEIType.ALL, EnumType.IsServiceType.ALL, EnumType.MainGroupPermissionType.ALL);
                cboType.Enabled = false;
                LoadComboBox();
                if (bolIsReceive)
                    cboType.SelectedIndex = 1;
                else
                    cboType.SelectedIndex = 0;
                cboType_SelectionChangeCommitted(null, null);
                BindData();
                cmdAdd.Enabled = SystemConfig.objSessionUser.IsPermission("AUTOSTORECHANGE_ADDPRODUCT");
                btnHintStock.Click += new EventHandler(btnHintStock_Click);
                cboFromStoreID.SetValue(intStoreID);
                cboToStoreID.SetValue(strStoreIDList);
                cboMainGroupID.SetValue(intMainGroupIDFromAutoDistribute);
                btnExportTemplate.Enabled = false;
                cmdImportExcel.Enabled = false;
                ImportFromAutoDistribute();
                BindData();
                EnableControl(true);
            }
            else
            {
                if (dtbStore != null && dtbStore.Columns.Count > 0)
                {
                    if (dtbStore.Columns[0].ColumnName.ToUpper() == "ISSELECT")
                    {
                        dtbStore.Columns.RemoveAt(0);
                    }
                }
                cmdCreateStoreChangeCommand.Enabled = false;
                if (dtbAutoStoreChange != null && dtbAutoStoreChange.Rows.Count > 0)
                {
                    cboIsShowProduct.SelectedIndex = intIsShowProduct + 1;
                    cboIsShowProduct.Enabled = false;
                    if (intIsCheckRealInput == -1)
                        chkIsCheckRealInput.Checked = true;
                    else
                        chkIsCheckRealInput.Checked = false;
                    chkIsCheckRealInput.Enabled = false;
                }
                else
                {
                    cboIsShowProduct.SelectedIndex = 1;
                    chkIsCheckRealInput.Checked = false;
                }
                LoadComboBox();
                if (intMainGroupID > 0)
                {
                    cboMainGroupID.SetValue(intMainGroupID);
                    cboMainGroupID.Enabled = false;
                }
                BindData();
                cmdAdd.Enabled = SystemConfig.objSessionUser.IsPermission("AUTOSTORECHANGE_ADDPRODUCT");
                btnHintStock.Click += new EventHandler(btnHintStock_Click);
            }
        }

        void btnHintStock_Click(object sender, EventArgs e)
        {
            frmAutoStoreChangeHintStock frmHint = new frmAutoStoreChangeHintStock();
            frmHint.dtbAutoStoreChange_Receive = dtbAutoStoreChange.Copy();
            frmHint.ShowDialog();
        }

        private void flexListAutoStoreChange_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            //if (e.Row < 1) return;
            //if (e.Col == flexListAutoStoreChange.Cols["IsUrgent"].Index)
            //{
            //    //Nếu có check Chọn và có quyền "AutoStoreChange_CheckEmergency" thì cho check Chuyển gấp
            //    if (Convert.ToBoolean(flexListAutoStoreChange.Rows[e.Row]["Select"])
            //        && SystemConfig.objSessionUser.IsPermission("AutoStoreChange_CheckEmergency")) { }
            //    else
            //    {
            //        e.Cancel = true;
            //        return;
            //    }

            //}
            //if (e.Col == flexListAutoStoreChange.Cols["Select"].Index)
            //{
            //    if (Convert.ToDecimal(flexListAutoStoreChange.Rows[e.Row]["Quantity"]) <= 0)
            //    {
            //        e.Cancel = true;
            //        return;
            //    }
            //}
            //if (e.Col == flexListAutoStoreChange.Cols["Quantity"].Index)
            //{
            //    if (!Convert.ToBoolean(flexListAutoStoreChange[e.Row, "IsAllowDecimal"]))
            //        flexListAutoStoreChange.Cols[e.Col].Format = "#,##0";
            //    else
            //        flexListAutoStoreChange.Cols[e.Col].Format = "#,##0.####";
            //}
        }

        private void flexListAutoStoreChange_ValidateEdit(object sender, C1.Win.C1FlexGrid.ValidateEditEventArgs e)
        {
            //if (flexListAutoStoreChange.Cols[e.Col].Name.ToLower() == "quantity")
            //{
            //    if (Convert.ToDecimal(flexListAutoStoreChange.Editor.Text) > Convert.ToDecimal(flexListAutoStoreChange[e.Row, "QuantityInStock"]))
            //    {
            //        MessageBox.Show(this, "Số lượng chuyển kho lớn hơn số lượng tồn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        e.Cancel = true;
            //        return;
            //    }
            //    if (Convert.ToDecimal(flexListAutoStoreChange.Editor.Text) <= 0)
            //    {
            //        MessageBox.Show(this, "Số lượng chuyển kho phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        e.Cancel = true;
            //        return;
            //    }

            //    if (!Convert.ToBoolean(flexListAutoStoreChange[e.Row, "IsAllowDecimal"]))
            //    {
            //        flexListAutoStoreChange.Editor.Text = Math.Round(Convert.ToDecimal(flexListAutoStoreChange.Editor.Text), 0, MidpointRounding.AwayFromZero) + "";
            //    }
            //}
        }

        private void mnuExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdListAutoStoreChange);
        }

        private void cboIsShowProduct_SelectionChangeCommitted(object sender, EventArgs e)
        {
            intIsShowProduct = cboIsShowProduct.SelectedIndex - 1;
        }

        private void chkIsCheckRealInput_CheckStateChanged(object sender, EventArgs e)
        {
            if (chkIsCheckRealInput.Checked)
                intIsCheckRealInput = -1;
            else
                intIsCheckRealInput = 1;
        }

        private void mnuDeleteRow_Click(object sender, EventArgs e)
        {
            if (grvListAutoStoreChange.FocusedRowHandle < 0)
                return;
        }

        private void mnuGrid_Opening(object sender, CancelEventArgs e)
        {
            mnuGrid.Enabled = mnuItemSelectAll.Enabled = mnuItemUnSelectAll.Enabled = grvListAutoStoreChange.RowCount < 2 ? false : true;
        }

        private void cboType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboType.SelectedIndex == 0)
            {
                lblStore1.Text = "Từ kho:";
                lblStore2.Text = "Đến kho:";
            }
            else
            {
                lblStore1.Text = "Đến kho:";
                lblStore2.Text = "Từ kho:";
            }
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboType.SelectedIndex != intOldIndex)
            {
                if (grvListAutoStoreChange.RowCount > 1)
                {
                    if (MessageBox.Show(this, "Trên lưới đã có dữ liệu.\nChương trình sẽ xóa dữ liệu cũ và thay thế bằng dữ liệu mới", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    {
                        cboType.SelectedIndex = intOldIndex;
                        return;
                    }
                    dtbAutoStoreChange.Clear();
                    strbFromStoreIDList = new StringBuilder();
                    strbProductIDList = new StringBuilder();
                    strbToStoreIDList = new StringBuilder();
                }
            }
            intOldIndex = cboType.SelectedIndex;

        }

        private void txtQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
            {
                cmdAdd_Click(null, null);
            }
        }

        private void flexListAutoStoreChange_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            //if (flexListAutoStoreChange.Cols[e.Col].Name == "Quantity")
            //{
            //    try
            //    {
            //        decimal decQuantity = Convert.ToDecimal(flexListAutoStoreChange.Rows[e.Row][e.Col]);
            //        if (decQuantity % 2 != 0)
            //        {
            //            MessageBox.Show(this, "Sản phẩm không được phép nhập số lẻ!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //            e.Cancel = true;
            //        }
            //    }
            //    catch
            //    { }
            //}
        }

        private void chkIsCanUseOldCode_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsCanUseOldCode.Checked && dtbStoreOldId == null)
            {
                dtbStoreOldId = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_STORE_OLDSTORE_SRH");
            }
        }

        private void grvListAutoStoreChange_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (grvListAutoStoreChange.FocusedColumn.FieldName.ToUpper() == "QUANTITY")
            {
                DataRow rowFocused = grvListAutoStoreChange.GetFocusedDataRow();
                if (rowFocused == null)
                    return;

                if (Convert.ToDecimal(rowFocused["Quantity"]) <= 0)
                {
                    MessageBox.Show(this, "Số lượng chuyển kho phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (!Convert.ToBoolean(rowFocused["IsAllowDecimal"]))
                {
                    rowFocused["Quantity"] = Math.Round(Convert.ToDecimal(rowFocused["Quantity"]), 0, MidpointRounding.AwayFromZero);
                }

                decimal decTotalInStockQuantity = 0;
                for (int i = 0; i < grvListAutoStoreChange.RowCount; i++)
                {
                    DataRow row = grvListAutoStoreChange.GetDataRow(i);
                    if (row == null)
                        continue;

                    if (row["PRODUCTID"].ToString().Trim() == rowFocused["PRODUCTID"].ToString().Trim())
                    {
                        decTotalInStockQuantity += Convert.ToDecimal(row["Quantity"]);
                    }
                }

                if (decTotalInStockQuantity > Convert.ToDecimal(rowFocused["QuantityInStock"]))
                {
                    MessageBox.Show(this, "Tổng số lượng chuyển kho lớn hơn số lượng tồn. \nTổng SL chuyển kho: " + decTotalInStockQuantity + " .Số lượng tồn hiện tại là: " + rowFocused["QuantityInStock"].ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Valid = false;
                    return;
                }

                //if (Convert.ToDecimal(rowFocused["Quantity"]) > Convert.ToDecimal(rowFocused["QuantityInStock"]))
                //{
                //    MessageBox.Show(this, "Số lượng chuyển kho lớn hơn số lượng tồn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    return;
                //}
            }

            #region Code cũ
            //if (grvListAutoStoreChange.FocusedColumn.FieldName.ToUpper() == "QUANTITY")
            //{
            //    DataRow row = grvListAutoStoreChange.GetFocusedDataRow();
            //    if (row == null)
            //        return;

            //    if (Convert.ToDecimal(row["Quantity"]) > Convert.ToDecimal(row["QuantityInStock"]))
            //    {
            //        MessageBox.Show(this, "Số lượng chuyển kho lớn hơn số lượng tồn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        return;
            //    }

            //    if (Convert.ToDecimal(row["Quantity"]) <= 0)
            //    {
            //        MessageBox.Show(this, "Số lượng chuyển kho phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        return;
            //    }

            //    if (!Convert.ToBoolean(row["IsAllowDecimal"]))
            //    {
            //        row["Quantity"] = Math.Round(Convert.ToDecimal(row["Quantity"]), 0, MidpointRounding.AwayFromZero);
            //    }
            //}
            #endregion
        }

        private void mnuItemSelectAll_Click(object sender, EventArgs e)
        {
            foreach (DataRow row in dtbAutoStoreChange.Rows)
            {
                row["Select"] = true;
            }
            grdListAutoStoreChange.RefreshDataSource();
        }

        private void mnuItemUnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (DataRow row in dtbAutoStoreChange.Rows)
            {
                row["Select"] = false;
            }
            grdListAutoStoreChange.RefreshDataSource();
        }

        private void grvListAutoStoreChange_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }
    }
}
