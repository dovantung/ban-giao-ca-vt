﻿namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    partial class frmAutoStoreChangeDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAutoStoreChangeDetail));
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grdListAutoStoreChange = new DevExpress.XtraGrid.GridControl();
            this.mnuGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemUnSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDeleteRow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.grvListAutoStoreChange = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtItemQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txtTaxedFee = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.txtInputPrice = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.flexExportTemplate = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.grpSearch = new DevExpress.XtraEditors.GroupControl();
            this.chkIsCanUseOldCode = new System.Windows.Forms.CheckBox();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chkIsCheckRealInput = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cboIsShowProduct = new System.Windows.Forms.ComboBox();
            this.cmdImportExcel = new System.Windows.Forms.Button();
            this.btnHintStock = new System.Windows.Forms.Button();
            this.btnExportTemplate = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cboMainGroupID = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.cmdCreateStoreChangeCommand = new System.Windows.Forms.Button();
            this.cboToStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.cboFromStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.btnSelectProduct = new System.Windows.Forms.Button();
            this.txtProductID = new System.Windows.Forms.TextBox();
            this.txtQuantity = new C1.Win.C1Input.C1NumericEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmdAdd = new System.Windows.Forms.Button();
            this.lblStore2 = new System.Windows.Forms.Label();
            this.lblStore1 = new System.Windows.Forms.Label();
            this.cachedrptInputVoucherReport1 = new ERP.Inventory.DUI.Reports.Input.CachedrptInputVoucherReport();
            this.cachedrptInputVoucherReport2 = new ERP.Inventory.DUI.Reports.Input.CachedrptInputVoucherReport();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdListAutoStoreChange)).BeginInit();
            this.mnuGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvListAutoStoreChange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxedFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flexExportTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpSearch)).BeginInit();
            this.grpSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.groupControl2.Controls.Add(this.grdListAutoStoreChange);
            this.groupControl2.Controls.Add(this.flexExportTemplate);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 115);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1058, 402);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Danh sách sản phẩm chia hàng";
            // 
            // grdListAutoStoreChange
            // 
            this.grdListAutoStoreChange.ContextMenuStrip = this.mnuGrid;
            this.grdListAutoStoreChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdListAutoStoreChange.Location = new System.Drawing.Point(2, 22);
            this.grdListAutoStoreChange.MainView = this.grvListAutoStoreChange;
            this.grdListAutoStoreChange.Name = "grdListAutoStoreChange";
            this.grdListAutoStoreChange.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.txtItemQuantity,
            this.txtTaxedFee,
            this.txtInputPrice});
            this.grdListAutoStoreChange.Size = new System.Drawing.Size(1054, 378);
            this.grdListAutoStoreChange.TabIndex = 2;
            this.grdListAutoStoreChange.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvListAutoStoreChange});
            // 
            // mnuGrid
            // 
            this.mnuGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemSelectAll,
            this.mnuItemUnSelectAll,
            this.mnuDeleteRow,
            this.toolStripSeparator1,
            this.mnuExportExcel});
            this.mnuGrid.Name = "mnuGrid";
            this.mnuGrid.Size = new System.Drawing.Size(151, 98);
            this.mnuGrid.Opening += new System.ComponentModel.CancelEventHandler(this.mnuGrid_Opening);
            // 
            // mnuItemSelectAll
            // 
            this.mnuItemSelectAll.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.mnuItemSelectAll.Name = "mnuItemSelectAll";
            this.mnuItemSelectAll.Size = new System.Drawing.Size(150, 22);
            this.mnuItemSelectAll.Text = "Chọn tất cả";
            this.mnuItemSelectAll.Click += new System.EventHandler(this.mnuItemSelectAll_Click);
            // 
            // mnuItemUnSelectAll
            // 
            this.mnuItemUnSelectAll.Image = global::ERP.Inventory.DUI.Properties.Resources.undo;
            this.mnuItemUnSelectAll.Name = "mnuItemUnSelectAll";
            this.mnuItemUnSelectAll.Size = new System.Drawing.Size(150, 22);
            this.mnuItemUnSelectAll.Text = "Bỏ chọn tất cả";
            this.mnuItemUnSelectAll.Click += new System.EventHandler(this.mnuItemUnSelectAll_Click);
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Image = global::ERP.Inventory.DUI.Properties.Resources.delete;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Size = new System.Drawing.Size(150, 22);
            this.mnuDeleteRow.Text = "Xóa dòng này";
            this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(147, 6);
            // 
            // mnuExportExcel
            // 
            this.mnuExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuExportExcel.Name = "mnuExportExcel";
            this.mnuExportExcel.Size = new System.Drawing.Size(150, 22);
            this.mnuExportExcel.Text = "Xuất Excel";
            this.mnuExportExcel.Click += new System.EventHandler(this.mnuExportExcel_Click);
            // 
            // grvListAutoStoreChange
            // 
            this.grvListAutoStoreChange.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvListAutoStoreChange.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvListAutoStoreChange.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvListAutoStoreChange.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvListAutoStoreChange.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grvListAutoStoreChange.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvListAutoStoreChange.Appearance.Row.Options.UseFont = true;
            this.grvListAutoStoreChange.ColumnPanelRowHeight = 40;
            this.grvListAutoStoreChange.GridControl = this.grdListAutoStoreChange;
            this.grvListAutoStoreChange.Name = "grvListAutoStoreChange";
            this.grvListAutoStoreChange.OptionsNavigation.UseTabKey = false;
            this.grvListAutoStoreChange.OptionsPrint.AutoWidth = false;
            this.grvListAutoStoreChange.OptionsView.ColumnAutoWidth = false;
            this.grvListAutoStoreChange.OptionsView.ShowFooter = true;
            this.grvListAutoStoreChange.OptionsView.ShowGroupPanel = false;
            this.grvListAutoStoreChange.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.grvListAutoStoreChange_InvalidRowException);
            this.grvListAutoStoreChange.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.grvListAutoStoreChange_ValidateRow);
            // 
            // txtItemQuantity
            // 
            this.txtItemQuantity.AutoHeight = false;
            this.txtItemQuantity.DisplayFormat.FormatString = "#,##0.####";
            this.txtItemQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtItemQuantity.Mask.EditMask = "###,###,###,##0.####";
            this.txtItemQuantity.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtItemQuantity.Name = "txtItemQuantity";
            // 
            // txtTaxedFee
            // 
            this.txtTaxedFee.AutoHeight = false;
            this.txtTaxedFee.DisplayFormat.FormatString = "#,##0.##";
            this.txtTaxedFee.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTaxedFee.Mask.EditMask = "###,###,###,##0.####";
            this.txtTaxedFee.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTaxedFee.Name = "txtTaxedFee";
            // 
            // txtInputPrice
            // 
            this.txtInputPrice.AutoHeight = false;
            this.txtInputPrice.DisplayFormat.FormatString = "#,##0.####";
            this.txtInputPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txtInputPrice.Mask.EditMask = "###,###,###,##0.####";
            this.txtInputPrice.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtInputPrice.Name = "txtInputPrice";
            // 
            // flexExportTemplate
            // 
            this.flexExportTemplate.AllowMerging = C1.Win.C1FlexGrid.AllowMergingEnum.FixedOnly;
            this.flexExportTemplate.AutoClipboard = true;
            this.flexExportTemplate.ColumnInfo = "9,0,0,0,0,105,Columns:";
            this.flexExportTemplate.Location = new System.Drawing.Point(2, 25);
            this.flexExportTemplate.Name = "flexExportTemplate";
            this.flexExportTemplate.Rows.Count = 1;
            this.flexExportTemplate.Rows.DefaultSize = 21;
            this.flexExportTemplate.Size = new System.Drawing.Size(666, 40);
            this.flexExportTemplate.StyleInfo = resources.GetString("flexExportTemplate.StyleInfo");
            this.flexExportTemplate.TabIndex = 1;
            this.flexExportTemplate.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            // 
            // grpSearch
            // 
            this.grpSearch.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSearch.AppearanceCaption.Options.UseFont = true;
            this.grpSearch.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.grpSearch.Controls.Add(this.chkIsCanUseOldCode);
            this.grpSearch.Controls.Add(this.cboType);
            this.grpSearch.Controls.Add(this.label6);
            this.grpSearch.Controls.Add(this.chkIsCheckRealInput);
            this.grpSearch.Controls.Add(this.label10);
            this.grpSearch.Controls.Add(this.cboIsShowProduct);
            this.grpSearch.Controls.Add(this.cmdImportExcel);
            this.grpSearch.Controls.Add(this.btnHintStock);
            this.grpSearch.Controls.Add(this.btnExportTemplate);
            this.grpSearch.Controls.Add(this.label5);
            this.grpSearch.Controls.Add(this.cboMainGroupID);
            this.grpSearch.Controls.Add(this.cmdCreateStoreChangeCommand);
            this.grpSearch.Controls.Add(this.cboToStoreID);
            this.grpSearch.Controls.Add(this.cboFromStoreID);
            this.grpSearch.Controls.Add(this.btnSelectProduct);
            this.grpSearch.Controls.Add(this.txtProductID);
            this.grpSearch.Controls.Add(this.txtQuantity);
            this.grpSearch.Controls.Add(this.label4);
            this.grpSearch.Controls.Add(this.label3);
            this.grpSearch.Controls.Add(this.cmdAdd);
            this.grpSearch.Controls.Add(this.lblStore2);
            this.grpSearch.Controls.Add(this.lblStore1);
            this.grpSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpSearch.Location = new System.Drawing.Point(0, 0);
            this.grpSearch.Name = "grpSearch";
            this.grpSearch.Size = new System.Drawing.Size(1058, 115);
            this.grpSearch.TabIndex = 0;
            this.grpSearch.Text = "Thông tin";
            // 
            // chkIsCanUseOldCode
            // 
            this.chkIsCanUseOldCode.AutoSize = true;
            this.chkIsCanUseOldCode.Location = new System.Drawing.Point(607, 53);
            this.chkIsCanUseOldCode.Name = "chkIsCanUseOldCode";
            this.chkIsCanUseOldCode.Size = new System.Drawing.Size(115, 20);
            this.chkIsCanUseOldCode.TabIndex = 19;
            this.chkIsCanUseOldCode.Text = "Sử dụng mã cũ";
            this.chkIsCanUseOldCode.UseVisualStyleBackColor = true;
            this.chkIsCanUseOldCode.CheckedChanged += new System.EventHandler(this.chkIsCanUseOldCode_CheckedChanged);
            // 
            // cboType
            // 
            this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboType.FormattingEnabled = true;
            this.cboType.Items.AddRange(new object[] {
            "Chuyển đi",
            "Nhận về"});
            this.cboType.Location = new System.Drawing.Point(89, 22);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(222, 24);
            this.cboType.TabIndex = 0;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            this.cboType.SelectionChangeCommitted += new System.EventHandler(this.cboType_SelectionChangeCommitted);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 16);
            this.label6.TabIndex = 18;
            this.label6.Text = "Hình thức:";
            // 
            // chkIsCheckRealInput
            // 
            this.chkIsCheckRealInput.AutoSize = true;
            this.chkIsCheckRealInput.Checked = true;
            this.chkIsCheckRealInput.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsCheckRealInput.Location = new System.Drawing.Point(319, 85);
            this.chkIsCheckRealInput.Name = "chkIsCheckRealInput";
            this.chkIsCheckRealInput.Size = new System.Drawing.Size(144, 20);
            this.chkIsCheckRealInput.TabIndex = 7;
            this.chkIsCheckRealInput.Text = "Gồm hàng đi đường";
            this.chkIsCheckRealInput.UseVisualStyleBackColor = true;
            this.chkIsCheckRealInput.CheckStateChanged += new System.EventHandler(this.chkIsCheckRealInput_CheckStateChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 16);
            this.label10.TabIndex = 9;
            this.label10.Text = "Trưng bày:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboIsShowProduct
            // 
            this.cboIsShowProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIsShowProduct.DropDownWidth = 300;
            this.cboIsShowProduct.Items.AddRange(new object[] {
            "--Tất cả--",
            "Không hiển thị hàng trưng bày",
            "Chỉ hiển thị hàng trưng bày"});
            this.cboIsShowProduct.Location = new System.Drawing.Point(89, 83);
            this.cboIsShowProduct.Name = "cboIsShowProduct";
            this.cboIsShowProduct.Size = new System.Drawing.Size(222, 24);
            this.cboIsShowProduct.TabIndex = 6;
            this.cboIsShowProduct.SelectionChangeCommitted += new System.EventHandler(this.cboIsShowProduct_SelectionChangeCommitted);
            // 
            // cmdImportExcel
            // 
            this.cmdImportExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdImportExcel.Image")));
            this.cmdImportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdImportExcel.Location = new System.Drawing.Point(678, 82);
            this.cmdImportExcel.Name = "cmdImportExcel";
            this.cmdImportExcel.Size = new System.Drawing.Size(102, 25);
            this.cmdImportExcel.TabIndex = 11;
            this.cmdImportExcel.Text = "Nhập Excel";
            this.cmdImportExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdImportExcel.UseVisualStyleBackColor = true;
            this.cmdImportExcel.Click += new System.EventHandler(this.cmdImportExcel_Click);
            // 
            // btnHintStock
            // 
            this.btnHintStock.Image = global::ERP.Inventory.DUI.Properties.Resources.calculator;
            this.btnHintStock.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHintStock.Location = new System.Drawing.Point(949, 50);
            this.btnHintStock.Name = "btnHintStock";
            this.btnHintStock.Size = new System.Drawing.Size(105, 58);
            this.btnHintStock.TabIndex = 13;
            this.btnHintStock.Text = "    Xem tồn kho giả lập";
            this.btnHintStock.UseVisualStyleBackColor = true;
            // 
            // btnExportTemplate
            // 
            this.btnExportTemplate.Image = ((System.Drawing.Image)(resources.GetObject("btnExportTemplate.Image")));
            this.btnExportTemplate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportTemplate.Location = new System.Drawing.Point(728, 51);
            this.btnExportTemplate.Name = "btnExportTemplate";
            this.btnExportTemplate.Size = new System.Drawing.Size(215, 25);
            this.btnExportTemplate.TabIndex = 10;
            this.btnExportTemplate.Text = "    Xuất tập tin mẫu";
            this.btnExportTemplate.UseVisualStyleBackColor = true;
            this.btnExportTemplate.Click += new System.EventHandler(this.btnExportTemplate_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Ngành hàng:";
            // 
            // cboMainGroupID
            // 
            this.cboMainGroupID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroupID.Location = new System.Drawing.Point(89, 52);
            this.cboMainGroupID.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroupID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroupID.Name = "cboMainGroupID";
            this.cboMainGroupID.Size = new System.Drawing.Size(223, 24);
            this.cboMainGroupID.TabIndex = 3;
            // 
            // cmdCreateStoreChangeCommand
            // 
            this.cmdCreateStoreChangeCommand.Image = ((System.Drawing.Image)(resources.GetObject("cmdCreateStoreChangeCommand.Image")));
            this.cmdCreateStoreChangeCommand.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdCreateStoreChangeCommand.Location = new System.Drawing.Point(786, 82);
            this.cmdCreateStoreChangeCommand.Name = "cmdCreateStoreChangeCommand";
            this.cmdCreateStoreChangeCommand.Size = new System.Drawing.Size(157, 25);
            this.cmdCreateStoreChangeCommand.TabIndex = 12;
            this.cmdCreateStoreChangeCommand.Text = "Tạo lệnh chuyển kho";
            this.cmdCreateStoreChangeCommand.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdCreateStoreChangeCommand.UseVisualStyleBackColor = true;
            this.cmdCreateStoreChangeCommand.Click += new System.EventHandler(this.cmdCreateStoreChangeCommand_Click);
            // 
            // cboToStoreID
            // 
            this.cboToStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboToStoreID.Location = new System.Drawing.Point(670, 21);
            this.cboToStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboToStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboToStoreID.Name = "cboToStoreID";
            this.cboToStoreID.Size = new System.Drawing.Size(273, 24);
            this.cboToStoreID.TabIndex = 2;
            // 
            // cboFromStoreID
            // 
            this.cboFromStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFromStoreID.Location = new System.Drawing.Point(389, 21);
            this.cboFromStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboFromStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboFromStoreID.Name = "cboFromStoreID";
            this.cboFromStoreID.Size = new System.Drawing.Size(207, 24);
            this.cboFromStoreID.TabIndex = 1;
            // 
            // btnSelectProduct
            // 
            this.btnSelectProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnSelectProduct.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSelectProduct.Location = new System.Drawing.Point(572, 52);
            this.btnSelectProduct.Name = "btnSelectProduct";
            this.btnSelectProduct.Size = new System.Drawing.Size(25, 25);
            this.btnSelectProduct.TabIndex = 5;
            this.btnSelectProduct.UseVisualStyleBackColor = true;
            this.btnSelectProduct.Click += new System.EventHandler(this.btnSelectProduct_Click);
            // 
            // txtProductID
            // 
            this.txtProductID.BackColor = System.Drawing.SystemColors.Window;
            this.txtProductID.Location = new System.Drawing.Point(389, 53);
            this.txtProductID.MaxLength = 20;
            this.txtProductID.Name = "txtProductID";
            this.txtProductID.Size = new System.Drawing.Size(184, 22);
            this.txtProductID.TabIndex = 4;
            // 
            // txtQuantity
            // 
            this.txtQuantity.CustomFormat = "#,##0";
            this.txtQuantity.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtQuantity.Location = new System.Drawing.Point(538, 83);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.NumericInputKeys = ((C1.Win.C1Input.NumericInputKeyFlags)((((C1.Win.C1Input.NumericInputKeyFlags.F9 | C1.Win.C1Input.NumericInputKeyFlags.Plus) 
            | C1.Win.C1Input.NumericInputKeyFlags.Decimal) 
            | C1.Win.C1Input.NumericInputKeyFlags.X)));
            this.txtQuantity.Size = new System.Drawing.Size(58, 22);
            this.txtQuantity.TabIndex = 8;
            this.txtQuantity.Tag = null;
            this.txtQuantity.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtQuantity.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            this.txtQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuantity_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(469, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 16);
            this.label4.TabIndex = 12;
            this.label4.Text = "SL chuyển:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(317, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Sản phẩm:";
            // 
            // cmdAdd
            // 
            this.cmdAdd.Image = ((System.Drawing.Image)(resources.GetObject("cmdAdd.Image")));
            this.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAdd.Location = new System.Drawing.Point(607, 83);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(65, 25);
            this.cmdAdd.TabIndex = 9;
            this.cmdAdd.Text = "Thêm";
            this.cmdAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdAdd.UseVisualStyleBackColor = true;
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // lblStore2
            // 
            this.lblStore2.AutoSize = true;
            this.lblStore2.Location = new System.Drawing.Point(604, 25);
            this.lblStore2.Name = "lblStore2";
            this.lblStore2.Size = new System.Drawing.Size(60, 16);
            this.lblStore2.TabIndex = 2;
            this.lblStore2.Text = "Đến kho:";
            // 
            // lblStore1
            // 
            this.lblStore1.AutoSize = true;
            this.lblStore1.Location = new System.Drawing.Point(317, 25);
            this.lblStore1.Name = "lblStore1";
            this.lblStore1.Size = new System.Drawing.Size(52, 16);
            this.lblStore1.TabIndex = 0;
            this.lblStore1.Text = "Từ kho:";
            // 
            // frmAutoStoreChangeDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1058, 517);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.grpSearch);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1050, 555);
            this.Name = "frmAutoStoreChangeDetail";
            this.ShowIcon = false;
            this.Text = "Chi tiết chuyển kho tự động";
            this.Load += new System.EventHandler(this.frmAutoStoreChangeDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdListAutoStoreChange)).EndInit();
            this.mnuGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvListAutoStoreChange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxedFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInputPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flexExportTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpSearch)).EndInit();
            this.grpSearch.ResumeLayout(false);
            this.grpSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl grpSearch;
        private System.Windows.Forms.Button btnSelectProduct;
        private System.Windows.Forms.TextBox txtProductID;
        private System.Windows.Forms.Button btnExportTemplate;
        private System.Windows.Forms.Button cmdImportExcel;
        private C1.Win.C1Input.C1NumericEdit txtQuantity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button cmdCreateStoreChangeCommand;
        private System.Windows.Forms.Button cmdAdd;
        private System.Windows.Forms.Label lblStore2;
        private System.Windows.Forms.Label lblStore1;
        private Reports.Input.CachedrptInputVoucherReport cachedrptInputVoucherReport1;
        private Reports.Input.CachedrptInputVoucherReport cachedrptInputVoucherReport2;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboToStoreID;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboFromStoreID;
        private C1.Win.C1FlexGrid.C1FlexGrid flexExportTemplate;
        private System.Windows.Forms.Label label5;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroupID;
        private System.Windows.Forms.ContextMenuStrip mnuGrid;
        private System.Windows.Forms.ToolStripMenuItem mnuExportExcel;
        private System.Windows.Forms.CheckBox chkIsCheckRealInput;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboIsShowProduct;
        private System.Windows.Forms.ToolStripMenuItem mnuDeleteRow;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Button btnHintStock;
        private System.Windows.Forms.ComboBox cboType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkIsCanUseOldCode;
        private DevExpress.XtraGrid.GridControl grdListAutoStoreChange;
        private DevExpress.XtraGrid.Views.Grid.GridView grvListAutoStoreChange;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtItemQuantity;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtTaxedFee;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtInputPrice;
        private System.Windows.Forms.ToolStripMenuItem mnuItemSelectAll;
        private System.Windows.Forms.ToolStripMenuItem mnuItemUnSelectAll;
    }
}