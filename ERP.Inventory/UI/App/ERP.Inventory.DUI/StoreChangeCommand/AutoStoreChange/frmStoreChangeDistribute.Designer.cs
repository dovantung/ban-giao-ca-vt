﻿namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    partial class frmStoreChangeDistribute
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStoreChangeDistribute));
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.sttFooter = new System.Windows.Forms.StatusStrip();
            this.lblPs = new System.Windows.Forms.ToolStripStatusLabel();
            this.psProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.mnuGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemUnSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDeleteRow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIsSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductstatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromStoreName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colToStoreName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsUrgent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.txtProductID = new System.Windows.Forms.TextBox();
            this.btnSelectProduct = new System.Windows.Forms.Button();
            this.grdTemplate = new DevExpress.XtraGrid.GridControl();
            this.girdViewTemplate = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.grpSearch = new DevExpress.XtraEditors.GroupControl();
            this.cboProductList = new ERP.MasterData.DUI.CustomControl.Product.cboProductList();
            this.cboProductState = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.txtLeadCommandID = new DevExpress.XtraEditors.TextEdit();
            this.btnImportExcel = new DevExpress.XtraEditors.DropDownButton();
            this.pMnuImport = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnExportTemplate = new DevExpress.XtraEditors.DropDownButton();
            this.pMnuExport = new DevExpress.XtraBars.PopupMenu(this.components);
            this.chkIsAllowOldCode = new System.Windows.Forms.CheckBox();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chkIsIncludedRoaming = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnVirtualEmulator = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cboMainGroupID = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.btnCreateStoreChangeCommand = new System.Windows.Forms.Button();
            this.cboToStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.cboFromStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.txtQuantity = new C1.Win.C1Input.C1NumericEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblStore2 = new System.Windows.Forms.Label();
            this.lblStore1 = new System.Windows.Forms.Label();
            this.cachedrptInputVoucherReport1 = new ERP.Inventory.DUI.Reports.Input.CachedrptInputVoucherReport();
            this.cachedrptInputVoucherReport2 = new ERP.Inventory.DUI.Reports.Input.CachedrptInputVoucherReport();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.sttFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            this.mnuGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.girdViewTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpSearch)).BeginInit();
            this.grpSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLeadCommandID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuImport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.groupControl2.Controls.Add(this.sttFooter);
            this.groupControl2.Controls.Add(this.grdData);
            this.groupControl2.Controls.Add(this.txtProductID);
            this.groupControl2.Controls.Add(this.btnSelectProduct);
            this.groupControl2.Controls.Add(this.grdTemplate);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 115);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1127, 402);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Danh sách sản phẩm chia hàng";
            // 
            // sttFooter
            // 
            this.sttFooter.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblPs,
            this.psProgress});
            this.sttFooter.Location = new System.Drawing.Point(2, 378);
            this.sttFooter.Name = "sttFooter";
            this.sttFooter.Size = new System.Drawing.Size(1123, 22);
            this.sttFooter.TabIndex = 102;
            this.sttFooter.Text = "statusStrip1";
            this.sttFooter.Visible = false;
            // 
            // lblPs
            // 
            this.lblPs.Name = "lblPs";
            this.lblPs.Size = new System.Drawing.Size(126, 17);
            this.lblPs.Text = "Đang kiểm tra dữ liệu: ";
            // 
            // psProgress
            // 
            this.psProgress.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.psProgress.Name = "psProgress";
            this.psProgress.Size = new System.Drawing.Size(300, 16);
            this.psProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // grdData
            // 
            this.grdData.ContextMenuStrip = this.mnuGrid;
            this.grdData.Location = new System.Drawing.Point(2, 22);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3});
            this.grdData.Size = new System.Drawing.Size(1098, 378);
            this.grdData.TabIndex = 0;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            // 
            // mnuGrid
            // 
            this.mnuGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemSelectAll,
            this.mnuItemUnSelectAll,
            this.mnuDeleteRow,
            this.toolStripSeparator1,
            this.mnuExportExcel});
            this.mnuGrid.Name = "mnuGrid";
            this.mnuGrid.Size = new System.Drawing.Size(151, 98);
            this.mnuGrid.Opening += new System.ComponentModel.CancelEventHandler(this.mnuGrid_Opening);
            // 
            // mnuItemSelectAll
            // 
            this.mnuItemSelectAll.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemSelectAll.Image")));
            this.mnuItemSelectAll.Name = "mnuItemSelectAll";
            this.mnuItemSelectAll.Size = new System.Drawing.Size(150, 22);
            this.mnuItemSelectAll.Text = "Chọn tất cả";
            this.mnuItemSelectAll.Click += new System.EventHandler(this.mnuItemSelectAll_Click);
            // 
            // mnuItemUnSelectAll
            // 
            this.mnuItemUnSelectAll.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemUnSelectAll.Image")));
            this.mnuItemUnSelectAll.Name = "mnuItemUnSelectAll";
            this.mnuItemUnSelectAll.Size = new System.Drawing.Size(150, 22);
            this.mnuItemUnSelectAll.Text = "Bỏ chọn tất cả";
            this.mnuItemUnSelectAll.Click += new System.EventHandler(this.mnuItemUnSelectAll_Click);
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Image = ((System.Drawing.Image)(resources.GetObject("mnuDeleteRow.Image")));
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Size = new System.Drawing.Size(150, 22);
            this.mnuDeleteRow.Text = "Xóa dòng này";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(147, 6);
            // 
            // mnuExportExcel
            // 
            this.mnuExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("mnuExportExcel.Image")));
            this.mnuExportExcel.Name = "mnuExportExcel";
            this.mnuExportExcel.Size = new System.Drawing.Size(150, 22);
            this.mnuExportExcel.Text = "Xuất Excel";
            this.mnuExportExcel.Click += new System.EventHandler(this.mnuExportExcel_Click);
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Preview.Options.UseFont = true;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.ColumnPanelRowHeight = 40;
            this.grdViewData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIsSelected,
            this.colProductID,
            this.colProductName,
            this.colIMEI,
            this.colProductstatus,
            this.colFromStoreName,
            this.gridColumn9,
            this.gridColumn6,
            this.colToStoreName,
            this.gridColumn10,
            this.colIsUrgent});
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsPrint.AutoWidth = false;
            this.grdViewData.OptionsPrint.PrintHeader = false;
            this.grdViewData.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.grdViewData.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grdViewData.OptionsView.ColumnAutoWidth = false;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewData.OptionsView.ShowFooter = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            this.grdViewData.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grdViewData_CellValueChanged);
            this.grdViewData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grdViewData_KeyDown);
            // 
            // colIsSelected
            // 
            this.colIsSelected.Caption = "Chọn";
            this.colIsSelected.FieldName = "ISSELECTED";
            this.colIsSelected.Name = "colIsSelected";
            this.colIsSelected.OptionsColumn.AllowMove = false;
            this.colIsSelected.OptionsColumn.AllowSize = false;
            this.colIsSelected.Visible = true;
            this.colIsSelected.VisibleIndex = 0;
            this.colIsSelected.Width = 40;
            // 
            // colProductID
            // 
            this.colProductID.Caption = "Mã sản phẩm";
            this.colProductID.FieldName = "PRODUCTID";
            this.colProductID.Name = "colProductID";
            this.colProductID.OptionsColumn.AllowEdit = false;
            this.colProductID.OptionsColumn.ReadOnly = true;
            this.colProductID.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "PRODUCTID", "Cộng")});
            this.colProductID.Visible = true;
            this.colProductID.VisibleIndex = 1;
            this.colProductID.Width = 120;
            // 
            // colProductName
            // 
            this.colProductName.Caption = "Tên sản phẩm";
            this.colProductName.FieldName = "PRODUCTNAME";
            this.colProductName.Name = "colProductName";
            this.colProductName.OptionsColumn.AllowEdit = false;
            this.colProductName.OptionsColumn.ReadOnly = true;
            this.colProductName.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 2;
            this.colProductName.Width = 150;
            // 
            // colIMEI
            // 
            this.colIMEI.Caption = "IMEI";
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsColumn.AllowEdit = false;
            this.colIMEI.OptionsColumn.ReadOnly = true;
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 3;
            this.colIMEI.Width = 120;
            // 
            // colProductstatus
            // 
            this.colProductstatus.Caption = "Trạng thái";
            this.colProductstatus.FieldName = "INSTOCKSTATUSNAME";
            this.colProductstatus.Name = "colProductstatus";
            this.colProductstatus.OptionsColumn.AllowEdit = false;
            this.colProductstatus.OptionsColumn.ReadOnly = true;
            this.colProductstatus.Visible = true;
            this.colProductstatus.VisibleIndex = 4;
            this.colProductstatus.Width = 150;
            // 
            // colFromStoreName
            // 
            this.colFromStoreName.Caption = "Từ kho";
            this.colFromStoreName.FieldName = "FROMSTORENAME";
            this.colFromStoreName.Name = "colFromStoreName";
            this.colFromStoreName.OptionsColumn.AllowEdit = false;
            this.colFromStoreName.OptionsColumn.ReadOnly = true;
            this.colFromStoreName.Visible = true;
            this.colFromStoreName.VisibleIndex = 5;
            this.colFromStoreName.Width = 250;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "SL tồn";
            this.gridColumn9.FieldName = "FROMSTOREINSTOCK";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 6;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Số lượng chuyển";
            this.gridColumn6.FieldName = "AUTOQUANTITY";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 7;
            // 
            // colToStoreName
            // 
            this.colToStoreName.Caption = "Đến kho";
            this.colToStoreName.FieldName = "TOSTORENAME";
            this.colToStoreName.Name = "colToStoreName";
            this.colToStoreName.OptionsColumn.AllowEdit = false;
            this.colToStoreName.OptionsColumn.ReadOnly = true;
            this.colToStoreName.Visible = true;
            this.colToStoreName.VisibleIndex = 8;
            this.colToStoreName.Width = 250;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "SL tồn";
            this.gridColumn10.FieldName = "TOSTOREINSTOCK";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 9;
            // 
            // colIsUrgent
            // 
            this.colIsUrgent.Caption = "Chuyển gấp";
            this.colIsUrgent.FieldName = "ISURGENT";
            this.colIsUrgent.Name = "colIsUrgent";
            this.colIsUrgent.Visible = true;
            this.colIsUrgent.VisibleIndex = 10;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit2.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit3.ValueUnchecked = ((short)(0));
            // 
            // txtProductID
            // 
            this.txtProductID.BackColor = System.Drawing.SystemColors.Window;
            this.txtProductID.Location = new System.Drawing.Point(370, 0);
            this.txtProductID.MaxLength = 20;
            this.txtProductID.Name = "txtProductID";
            this.txtProductID.Size = new System.Drawing.Size(184, 22);
            this.txtProductID.TabIndex = 100;
            this.txtProductID.Visible = false;
            // 
            // btnSelectProduct
            // 
            this.btnSelectProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnSelectProduct.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectProduct.Image")));
            this.btnSelectProduct.Location = new System.Drawing.Point(553, -1);
            this.btnSelectProduct.Name = "btnSelectProduct";
            this.btnSelectProduct.Size = new System.Drawing.Size(25, 25);
            this.btnSelectProduct.TabIndex = 5;
            this.btnSelectProduct.UseVisualStyleBackColor = true;
            this.btnSelectProduct.Visible = false;
            // 
            // grdTemplate
            // 
            this.grdTemplate.Location = new System.Drawing.Point(650, 35);
            this.grdTemplate.MainView = this.girdViewTemplate;
            this.grdTemplate.Name = "grdTemplate";
            this.grdTemplate.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4,
            this.repositoryItemCheckEdit5,
            this.repositoryItemCheckEdit6});
            this.grdTemplate.Size = new System.Drawing.Size(381, 314);
            this.grdTemplate.TabIndex = 103;
            this.grdTemplate.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.girdViewTemplate});
            // 
            // girdViewTemplate
            // 
            this.girdViewTemplate.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.girdViewTemplate.Appearance.FocusedRow.Options.UseFont = true;
            this.girdViewTemplate.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.girdViewTemplate.Appearance.HeaderPanel.Options.UseFont = true;
            this.girdViewTemplate.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.girdViewTemplate.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.girdViewTemplate.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.girdViewTemplate.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.girdViewTemplate.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.girdViewTemplate.Appearance.Preview.Options.UseFont = true;
            this.girdViewTemplate.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.girdViewTemplate.Appearance.Row.Options.UseFont = true;
            this.girdViewTemplate.ColumnPanelRowHeight = 40;
            this.girdViewTemplate.GridControl = this.grdTemplate;
            this.girdViewTemplate.Name = "girdViewTemplate";
            this.girdViewTemplate.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.girdViewTemplate.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.girdViewTemplate.OptionsNavigation.UseTabKey = false;
            this.girdViewTemplate.OptionsPrint.AutoWidth = false;
            this.girdViewTemplate.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.girdViewTemplate.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.girdViewTemplate.OptionsView.ColumnAutoWidth = false;
            this.girdViewTemplate.OptionsView.ShowAutoFilterRow = true;
            this.girdViewTemplate.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.girdViewTemplate.OptionsView.ShowFooter = true;
            this.girdViewTemplate.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit5.ValueUnchecked = ((short)(0));
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit6.ValueUnchecked = ((short)(0));
            // 
            // grpSearch
            // 
            this.grpSearch.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSearch.AppearanceCaption.Options.UseFont = true;
            this.grpSearch.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.grpSearch.Controls.Add(this.cboProductList);
            this.grpSearch.Controls.Add(this.cboProductState);
            this.grpSearch.Controls.Add(this.txtLeadCommandID);
            this.grpSearch.Controls.Add(this.btnImportExcel);
            this.grpSearch.Controls.Add(this.btnExportTemplate);
            this.grpSearch.Controls.Add(this.chkIsAllowOldCode);
            this.grpSearch.Controls.Add(this.cboType);
            this.grpSearch.Controls.Add(this.label6);
            this.grpSearch.Controls.Add(this.chkIsIncludedRoaming);
            this.grpSearch.Controls.Add(this.label10);
            this.grpSearch.Controls.Add(this.btnVirtualEmulator);
            this.grpSearch.Controls.Add(this.label5);
            this.grpSearch.Controls.Add(this.cboMainGroupID);
            this.grpSearch.Controls.Add(this.btnCreateStoreChangeCommand);
            this.grpSearch.Controls.Add(this.cboToStoreID);
            this.grpSearch.Controls.Add(this.cboFromStoreID);
            this.grpSearch.Controls.Add(this.txtQuantity);
            this.grpSearch.Controls.Add(this.label4);
            this.grpSearch.Controls.Add(this.label3);
            this.grpSearch.Controls.Add(this.btnAdd);
            this.grpSearch.Controls.Add(this.label1);
            this.grpSearch.Controls.Add(this.lblStore2);
            this.grpSearch.Controls.Add(this.lblStore1);
            this.grpSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpSearch.Location = new System.Drawing.Point(0, 0);
            this.grpSearch.Name = "grpSearch";
            this.grpSearch.Size = new System.Drawing.Size(1127, 115);
            this.grpSearch.TabIndex = 0;
            this.grpSearch.Text = "Thông tin";
            // 
            // cboProductList
            // 
            this.cboProductList.BackColor = System.Drawing.Color.Transparent;
            this.cboProductList.ConsignmentType = -1;
            this.cboProductList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductList.Location = new System.Drawing.Point(389, 53);
            this.cboProductList.Margin = new System.Windows.Forms.Padding(4);
            this.cboProductList.Name = "cboProductList";
            this.cboProductList.ProductIDList = "";
            this.cboProductList.Size = new System.Drawing.Size(207, 23);
            this.cboProductList.TabIndex = 11;
            // 
            // cboProductState
            // 
            this.cboProductState.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductState.Location = new System.Drawing.Point(89, 82);
            this.cboProductState.Margin = new System.Windows.Forms.Padding(0);
            this.cboProductState.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboProductState.Name = "cboProductState";
            this.cboProductState.Size = new System.Drawing.Size(223, 24);
            this.cboProductState.TabIndex = 16;
            // 
            // txtLeadCommandID
            // 
            this.txtLeadCommandID.Location = new System.Drawing.Point(949, 22);
            this.txtLeadCommandID.Name = "txtLeadCommandID";
            this.txtLeadCommandID.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtLeadCommandID.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtLeadCommandID.Properties.Appearance.Options.UseBackColor = true;
            this.txtLeadCommandID.Properties.Appearance.Options.UseFont = true;
            this.txtLeadCommandID.Properties.LookAndFeel.SkinName = "Blue";
            this.txtLeadCommandID.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtLeadCommandID.Properties.ReadOnly = true;
            this.txtLeadCommandID.Size = new System.Drawing.Size(129, 22);
            this.txtLeadCommandID.TabIndex = 7;
            // 
            // btnImportExcel
            // 
            this.btnImportExcel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportExcel.Appearance.Options.UseFont = true;
            this.btnImportExcel.DropDownControl = this.pMnuImport;
            this.btnImportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnImportExcel.Image")));
            this.btnImportExcel.Location = new System.Drawing.Point(673, 82);
            this.btnImportExcel.Name = "btnImportExcel";
            this.btnImportExcel.Size = new System.Drawing.Size(111, 25);
            this.btnImportExcel.TabIndex = 21;
            this.btnImportExcel.Text = "Nhập Excel";
            // 
            // pMnuImport
            // 
            this.pMnuImport.Manager = this.barManager1;
            this.pMnuImport.Name = "pMnuImport";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MaxItemId = 0;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1127, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 517);
            this.barDockControlBottom.Size = new System.Drawing.Size(1127, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 517);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1127, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 517);
            // 
            // btnExportTemplate
            // 
            this.btnExportTemplate.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportTemplate.Appearance.Options.UseFont = true;
            this.btnExportTemplate.DropDownControl = this.pMnuExport;
            this.btnExportTemplate.Image = ((System.Drawing.Image)(resources.GetObject("btnExportTemplate.Image")));
            this.btnExportTemplate.Location = new System.Drawing.Point(728, 51);
            this.btnExportTemplate.Name = "btnExportTemplate";
            this.btnExportTemplate.Size = new System.Drawing.Size(151, 25);
            this.btnExportTemplate.TabIndex = 13;
            this.btnExportTemplate.Text = "Xuất tập tin mẫu";
            // 
            // pMnuExport
            // 
            this.pMnuExport.Manager = this.barManager1;
            this.pMnuExport.Name = "pMnuExport";
            // 
            // chkIsAllowOldCode
            // 
            this.chkIsAllowOldCode.AutoSize = true;
            this.chkIsAllowOldCode.Location = new System.Drawing.Point(607, 53);
            this.chkIsAllowOldCode.Name = "chkIsAllowOldCode";
            this.chkIsAllowOldCode.Size = new System.Drawing.Size(115, 20);
            this.chkIsAllowOldCode.TabIndex = 12;
            this.chkIsAllowOldCode.Text = "Sử dụng mã cũ";
            this.chkIsAllowOldCode.UseVisualStyleBackColor = true;
            // 
            // cboType
            // 
            this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboType.FormattingEnabled = true;
            this.cboType.Items.AddRange(new object[] {
            "Chuyển đi",
            "Nhận về"});
            this.cboType.Location = new System.Drawing.Point(89, 22);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(222, 24);
            this.cboType.TabIndex = 1;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            this.cboType.SelectionChangeCommitted += new System.EventHandler(this.cboType_SelectionChangeCommitted);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "Hình thức:";
            // 
            // chkIsIncludedRoaming
            // 
            this.chkIsIncludedRoaming.AutoSize = true;
            this.chkIsIncludedRoaming.Checked = true;
            this.chkIsIncludedRoaming.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsIncludedRoaming.Location = new System.Drawing.Point(319, 85);
            this.chkIsIncludedRoaming.Name = "chkIsIncludedRoaming";
            this.chkIsIncludedRoaming.Size = new System.Drawing.Size(144, 20);
            this.chkIsIncludedRoaming.TabIndex = 17;
            this.chkIsIncludedRoaming.Text = "Gồm hàng đi đường";
            this.chkIsIncludedRoaming.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 16);
            this.label10.TabIndex = 15;
            this.label10.Text = "Trạng thái:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnVirtualEmulator
            // 
            this.btnVirtualEmulator.Image = ((System.Drawing.Image)(resources.GetObject("btnVirtualEmulator.Image")));
            this.btnVirtualEmulator.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVirtualEmulator.Location = new System.Drawing.Point(949, 49);
            this.btnVirtualEmulator.Name = "btnVirtualEmulator";
            this.btnVirtualEmulator.Size = new System.Drawing.Size(129, 58);
            this.btnVirtualEmulator.TabIndex = 0;
            this.btnVirtualEmulator.Text = "    Xem tồn kho giả lập";
            this.btnVirtualEmulator.UseVisualStyleBackColor = true;
            this.btnVirtualEmulator.Click += new System.EventHandler(this.btnVirtualEmulator_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "Ngành hàng:";
            // 
            // cboMainGroupID
            // 
            this.cboMainGroupID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroupID.Location = new System.Drawing.Point(89, 52);
            this.cboMainGroupID.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroupID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroupID.Name = "cboMainGroupID";
            this.cboMainGroupID.Size = new System.Drawing.Size(223, 24);
            this.cboMainGroupID.TabIndex = 9;
            // 
            // btnCreateStoreChangeCommand
            // 
            this.btnCreateStoreChangeCommand.Image = ((System.Drawing.Image)(resources.GetObject("btnCreateStoreChangeCommand.Image")));
            this.btnCreateStoreChangeCommand.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCreateStoreChangeCommand.Location = new System.Drawing.Point(786, 82);
            this.btnCreateStoreChangeCommand.Name = "btnCreateStoreChangeCommand";
            this.btnCreateStoreChangeCommand.Size = new System.Drawing.Size(157, 25);
            this.btnCreateStoreChangeCommand.TabIndex = 22;
            this.btnCreateStoreChangeCommand.Text = "Tạo lệnh chuyển kho";
            this.btnCreateStoreChangeCommand.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCreateStoreChangeCommand.UseVisualStyleBackColor = true;
            this.btnCreateStoreChangeCommand.Click += new System.EventHandler(this.btnCreateStoreChangeCommand_Click);
            // 
            // cboToStoreID
            // 
            this.cboToStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboToStoreID.Location = new System.Drawing.Point(670, 21);
            this.cboToStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboToStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboToStoreID.Name = "cboToStoreID";
            this.cboToStoreID.Size = new System.Drawing.Size(209, 24);
            this.cboToStoreID.TabIndex = 5;
            // 
            // cboFromStoreID
            // 
            this.cboFromStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFromStoreID.Location = new System.Drawing.Point(389, 21);
            this.cboFromStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboFromStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboFromStoreID.Name = "cboFromStoreID";
            this.cboFromStoreID.Size = new System.Drawing.Size(207, 24);
            this.cboFromStoreID.TabIndex = 3;
            // 
            // txtQuantity
            // 
            this.txtQuantity.CustomFormat = "#,##0";
            this.txtQuantity.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtQuantity.Location = new System.Drawing.Point(538, 83);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.NumericInputKeys = ((C1.Win.C1Input.NumericInputKeyFlags)((((C1.Win.C1Input.NumericInputKeyFlags.F9 | C1.Win.C1Input.NumericInputKeyFlags.Plus) 
            | C1.Win.C1Input.NumericInputKeyFlags.Decimal) 
            | C1.Win.C1Input.NumericInputKeyFlags.X)));
            this.txtQuantity.Size = new System.Drawing.Size(58, 22);
            this.txtQuantity.TabIndex = 19;
            this.txtQuantity.Tag = null;
            this.txtQuantity.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtQuantity.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(469, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 16);
            this.label4.TabIndex = 18;
            this.label4.Text = "SL chuyển:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(317, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Sản phẩm:";
            // 
            // btnAdd
            // 
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(607, 82);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(65, 25);
            this.btnAdd.TabIndex = 20;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(883, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Mã lệnh:";
            // 
            // lblStore2
            // 
            this.lblStore2.AutoSize = true;
            this.lblStore2.Location = new System.Drawing.Point(604, 25);
            this.lblStore2.Name = "lblStore2";
            this.lblStore2.Size = new System.Drawing.Size(60, 16);
            this.lblStore2.TabIndex = 4;
            this.lblStore2.Text = "Đến kho:";
            // 
            // lblStore1
            // 
            this.lblStore1.AutoSize = true;
            this.lblStore1.Location = new System.Drawing.Point(317, 25);
            this.lblStore1.Name = "lblStore1";
            this.lblStore1.Size = new System.Drawing.Size(52, 16);
            this.lblStore1.TabIndex = 2;
            this.lblStore1.Text = "Từ kho:";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(126, 17);
            this.toolStripStatusLabel1.Text = "Đang kiểm tra dữ liệu: ";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(2, 378);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1123, 22);
            this.statusStrip1.TabIndex = 102;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // frmStoreChangeDistribute
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1127, 517);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.grpSearch);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1050, 555);
            this.Name = "frmStoreChangeDistribute";
            this.ShowIcon = false;
            this.Text = "Chi tiết chuyển kho tự động";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            this.sttFooter.ResumeLayout(false);
            this.sttFooter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            this.mnuGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.girdViewTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpSearch)).EndInit();
            this.grpSearch.ResumeLayout(false);
            this.grpSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLeadCommandID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuImport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pMnuExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl grpSearch;
        private System.Windows.Forms.Button btnSelectProduct;
        private System.Windows.Forms.TextBox txtProductID;
        private C1.Win.C1Input.C1NumericEdit txtQuantity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCreateStoreChangeCommand;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label lblStore2;
        private System.Windows.Forms.Label lblStore1;
        private Reports.Input.CachedrptInputVoucherReport cachedrptInputVoucherReport1;
        private Reports.Input.CachedrptInputVoucherReport cachedrptInputVoucherReport2;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboToStoreID;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboFromStoreID;
        private System.Windows.Forms.Label label5;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroupID;
        private System.Windows.Forms.ContextMenuStrip mnuGrid;
        private System.Windows.Forms.ToolStripMenuItem mnuExportExcel;
        private System.Windows.Forms.CheckBox chkIsIncludedRoaming;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ToolStripMenuItem mnuDeleteRow;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Button btnVirtualEmulator;
        private System.Windows.Forms.ComboBox cboType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkIsAllowOldCode;
        private System.Windows.Forms.ToolStripMenuItem mnuItemSelectAll;
        private System.Windows.Forms.ToolStripMenuItem mnuItemUnSelectAll;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.PopupMenu pMnuExport;
        private DevExpress.XtraBars.PopupMenu pMnuImport;
        private DevExpress.XtraEditors.DropDownButton btnExportTemplate;
        private DevExpress.XtraEditors.DropDownButton btnImportExcel;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.TextEdit txtLeadCommandID;
        private System.Windows.Forms.Label label1;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboProductState;
        private MasterData.DUI.CustomControl.Product.cboProductList cboProductList;
        private System.Windows.Forms.StatusStrip sttFooter;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblPs;
        private System.Windows.Forms.ToolStripProgressBar psProgress;
        private DevExpress.XtraGrid.Columns.GridColumn colProductID;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colFromStoreName;
        private DevExpress.XtraGrid.Columns.GridColumn colToStoreName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn colProductstatus;
        private DevExpress.XtraGrid.GridControl grdTemplate;
        private DevExpress.XtraGrid.Views.Grid.GridView girdViewTemplate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSelected;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn colIsUrgent;
    }
}