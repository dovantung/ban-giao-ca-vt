﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    public partial class frmAutoStoreChange : Form
    {

        #region Khai báo biến sử dụng
        private bool bolIsPermissoinPredefinedShareProduct = SystemConfig.objSessionUser.IsPermission("PREDEFINEDSHAREPRODUCT_RUN");
        private bool bolIsPermissoinPM_AutoStoreChange_Run = SystemConfig.objSessionUser.IsPermission("PM_AUTOSTORECHANGE_RUN");
        private int intIsShowProduct = -1;
        private int intIsCheckRealInput = -1;
        private DataTable tblInStock = null;
        private DataTable tblAverageSale = null;
        private DataTable tblTotal = null;
        private DataTable tblUnEvent = null;
        private DataTable tblAutoStoreChange = null;
        private DataTable tblProductList = null;
        private int intMainGroupID = 0;
        private DataTable dtbArea = null;
        private DataTable dtbStore = null;
        private DataTable dtbBrand = null;
        private DataTable dtbStoreView = null;
        private DataTable dtbProductStatus = null;
        private int intNumColumnNotEdit = 6;
        private ERP.Inventory.PLC.StoreChangeCommand.PLCAutoSplitProduct objPLCAutoSplitProduct = new PLC.StoreChangeCommand.PLCAutoSplitProduct();
        #endregion
        #region Mức ưu tiên tính SL chuyển kho
        private enum TYPECALCULATE
        {
            SAME_PROVINCE,
            OTHER_PROVINCE,
            OTHER_COMPANY
        }
        #endregion

        public frmAutoStoreChange()
        {
            InitializeComponent();
        }

        private void frmAutoStoreChange_Load(object sender, EventArgs e)
        {
            LoadComboBox();
            btnSearch.Enabled = bolIsPermissoinPM_AutoStoreChange_Run;
            InitAutoStoreChangeTable();
        }

        private void LoadComboBox()
        {
            try
            {
                ComboBox cboAreaTemp = new ComboBox();
                Library.AppCore.LoadControls.SetDataSource.SetArea(this,cboAreaTemp,
                    false,Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER);
                dtbArea = (DataTable)cboAreaTemp.DataSource;
                dtbArea.Rows.RemoveAt(0);
                cboAreaIDList.InitControl(true, dtbArea);

                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER;
                objStoreFilter.AreaType = Library.AppCore.Constant.EnumType.AreaType.AREA;
                objStoreFilter.IsCheckPermission = true;
                objStoreFilter.OtherCondition = "ISACTIVE = 1 and ISAUTOSTORECHANGE = 1";
                cboStoreList.InitControl(true, objStoreFilter);
                dtbStore = cboStoreList.DataSource.Copy();

                cboMainGroupID.InitControl(false, false, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL, Library.AppCore.Constant.EnumType.IsServiceType.NOT_ISSERVICE);

                cboSubGroupID.InitControl(false, 0, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL, Library.AppCore.Constant.EnumType.IsServiceType.NOT_ISSERVICE);

                cboProductList1.InitControl(0, string.Empty, string.Empty, false, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL,Library.AppCore.Constant.EnumType.IsServiceType.NOT_ISSERVICE,Library.AppCore.Constant.EnumType.MainGroupPermissionType.ALL);
                ComboBox cboBrandTemp = new ComboBox();
                Library.AppCore.LoadControls.SetDataSource.SetBrand(this,cboBrandTemp,0);
                dtbBrand = (DataTable)cboBrandTemp.DataSource;
                dtbBrand.Rows.RemoveAt(0);
                cboBrandIDList.InitControl(true, dtbBrand);

                cboIsShowProduct.SelectedIndex = 0;

                ComboBox cboProductStatus = new ComboBox();
                Library.AppCore.LoadControls.SetDataSource.SetProductStatus(this,cboProductStatus);
                dtbProductStatus = (DataTable)cboProductStatus.DataSource;
                dtbProductStatus.Rows.RemoveAt(0);
                Library.AppCore.LoadControls.CheckedComboBoxEditObject.LoadDataFromDataTable(dtbProductStatus, cboStatusList);
                cboStatusList.Text = "Tất cả";
            }
            catch (Exception objExc)
            {
                Library.AppCore.SystemErrorWS.Insert("Lỗi nạp dữ liệu combobox", objExc, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi nạp dữ liệu combobox", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private bool CheckInput()
        {
            if (cboAreaIDList.AreaIDList == string.Empty)
            {
                MessageBox.Show(this, "Vui lòng chọn khu vực", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboAreaIDList.Focus();
                return false;
            }

            if (cboMainGroupID.MainGroupID < 1)
            {
                MessageBox.Show(this, "Vui lòng chọn ngành hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMainGroupID.Focus();
                return false;
            }

            if (Convert.ToInt32(txtNumDays.Value) < 1)
            {
                MessageBox.Show(this, "Số ngày bán phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNumDays.Focus();
                return false;
            }

            if (Convert.ToInt32(txtNumDays.Value) > 365)
            {
                MessageBox.Show(this, "Số ngày bán quá lớn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNumDays.Focus();
                return false;
            }
            return true;
        }


        /// <summary>
        /// Lấy danh sách kho được chọn để xem báo cáo
        /// </summary>
        /// <param name="bolGetStoreIDList"></param>
        private void GetStoreReportView(bool bolGetStoreIDList)
        {
            String strAreaIDList = cboAreaIDList.AreaIDList.Replace("><", ",").Replace("<", "(").Replace(">", ")");
            String strFilter = "1 = 1";
            if (strAreaIDList.Trim() != string.Empty)
            {
                strFilter += " and AreaID in " + strAreaIDList;
            }
            if (bolGetStoreIDList)
            {
                String strStoreIDList = cboStoreList.StoreIDList;
                if (strStoreIDList.Trim() != string.Empty)
                {
                    string strIDList = Library.AppCore.Other.ConvertObject.ConvertIDListBracesToParentheses(strStoreIDList);
                    strFilter += " and StoreID in " + strIDList;
                }
            }
            dtbStore.DefaultView.RowFilter = strFilter;
            dtbStoreView = dtbStore.DefaultView.ToTable();
        }

        private DataTable LoadInStockTable()
        {
            DataTable tblResult = null;
            try
            {
                object[] objKeywords = new object[]
                {
                    "@MainGroupID", cboMainGroupID.MainGroupID,
                    "@MainGroupIDList", ("<" + cboMainGroupID.MainGroupID + ">"),
                    "@SubGroupIDList", (cboSubGroupID.SubGroupID > 0 ? "<" + cboSubGroupID.SubGroupID + ">" : ""),
                    "@BrandIDList", cboBrandIDList.BrandIDList,
                    "@ProductIDList", cboProductList1.ProductIDList,
                    "@COMPANYIDLIST","",
                    "@AreaIDList", cboAreaIDList.AreaIDList,
                    "@StoreIDList", cboStoreList.StoreIDList,
                    "@IsNew", 1,
                    "@IsShowProduct", cboIsShowProduct.SelectedIndex - 1,
                    "@ISCHECKREALINPUT", chkIsCheckRealInput.Checked==false?1:-1,
                    "@PRODUCTSTATUSIDLIST",Library.AppCore.LoadControls.CheckedComboBoxEditObject.GetCheckedItemToString(cboStatusList)
                };
                tblResult = objPLCAutoSplitProduct.GetStoreInStock(objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return null;
                }
                intIsShowProduct = cboIsShowProduct.SelectedIndex - 1;
                intIsCheckRealInput = chkIsCheckRealInput.Checked == false ? 1 : -1;
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi đọc thông tin tồn kho", objEx, DUIInventory_Globals.ModuleName);
                return null;
            }
            return tblResult;
        }

        private void LoadAutoStoreChangeData()
        {
            try
            {
                btnCalculator.Enabled = false;
                btnReCalculator.Enabled = false;
                GetStoreReportView(true);
                tblInStock = LoadInStockTable();
                if (tblInStock == null)
                {
                    MessageBox.Show(this, "Lỗi đọc thông tin tồn kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                DataTable dtbStoreChangeQuantity = LoadStoreChangeQuantity();
                if (dtbStoreChangeQuantity == null)
                {
                    MessageBox.Show(this, "Lỗi đọc thông tin số lượng nhập chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                tblProductList = Globals.SelectDistinct(tblInStock, "ProductID","STOREID>0");
                tblAverageSale = LoadAverageSaleTable();
                if (tblAverageSale == null)
                {
                    MessageBox.Show(this, "Lỗi đọc thông tin số lượng bán bình quân", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                tblTotal = CreateTotalTable(dtbStoreChangeQuantity);
                flexListResult.DataSource = tblTotal;
                CustomFlex();
                intMainGroupID = cboMainGroupID.MainGroupID;
                if (tblTotal.Columns.Count > intNumColumnNotEdit)
                {
                    btnReCalculator.Enabled = true;
                    btnCalculator.Enabled = true;
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi khởi tạo thông tin tính tồn kho tự động", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi khởi tạo thông tin tính tồn kho tự động", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            this.Cursor = Cursors.Default;
        }

        private DataTable LoadAverageSaleTable()
        {
            DataTable tblResult = null;
            try
            {
                object[] objKeywords = new object[]
                {
                    "@MainGroupID", cboMainGroupID.MainGroupID,
                    "@SubGroupID", cboSubGroupID.SubGroupID,
                    "@BrandIDList", cboBrandIDList.BrandIDList,
                    "@ProductIDList", cboProductList1.ProductIDList,
                    "@NumDay", txtNumDays.Value,
                    "@username", SystemConfig.objSessionUser.UserName,
                    "@AreaIDList", cboAreaIDList.AreaIDList,
                    "@StoreIDList", cboStoreList.StoreIDList,
                    "@IsShowProduct", cboIsShowProduct.SelectedIndex - 1,
                    "@StatusIDList", Library.AppCore.LoadControls.CheckedComboBoxEditObject.GetCheckedItemToString(cboStatusList)
                };
                tblResult = objPLCAutoSplitProduct.GetAvgSaleStoreInStock(objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return null;
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi đọc thông tin số lượng bán bình quân", objEx, DUIInventory_Globals.ModuleName);
                return null;
            }
            return tblResult;
        }

        private DataTable LoadStoreChangeQuantity()
        {
            DataTable tblResult = null;
            try
            {
                object[] objKeywords = new object[]
                {
                    "@MainGroupID", cboMainGroupID.MainGroupID,
                    "@SubGroupID", cboSubGroupID.SubGroupID,
                    "@ProductIDList", cboProductList1.ProductIDList,
                    "@username", SystemConfig.objSessionUser.UserName,
                    "@IsNew", 1,
                    "@IsShowProduct", cboIsShowProduct.SelectedIndex - 1,
                    "@AreaIDList", cboAreaIDList.AreaIDList,
                    "@StoreIDList", cboStoreList.StoreIDList,
                    "@BrandIDList", cboBrandIDList.BrandIDList,
                    "@NumDay", Convert.ToInt32(txtNumDay_NotCal.Value),
                    "@StatusIDList", Library.AppCore.LoadControls.CheckedComboBoxEditObject.GetCheckedItemToString(cboStatusList)
                };
                tblResult = objPLCAutoSplitProduct.GetInputVoucherByStorechange(objKeywords);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return null;
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi đọc thông tin số lượng nhập chuyển kho", objEx, DUIInventory_Globals.ModuleName);
                return null;
            }
            return tblResult;
        }

        private DataTable CreateTotalTable(DataTable dtbStoreChangeQuantity)
        {
            DataTable tblResult = new DataTable();
            tblResult.Columns.Add("ProductID", typeof(string));
            tblResult.Columns.Add("ProductName", typeof(String));
            tblResult.Columns.Add("PRODUCTSTATUSNAME", typeof(string));
            tblResult.Columns.Add("NumOfSaleDays", typeof(Int32));
            tblResult.Columns.Add("Quantity", typeof(String));

            for (int i = 0; i < dtbStoreView.Rows.Count; i++)
            {
                tblResult.Columns.Add(Convert.ToString(dtbStoreView.Rows[i]["StoreName"]).Trim(), typeof(decimal));
            }
            DataColumn colIsAllowEdit = new DataColumn("IsAllowEdit");
            colIsAllowEdit.DataType = typeof(bool);
            colIsAllowEdit.DefaultValue = false;
            tblResult.Columns.Add(colIsAllowEdit);
            DataColumn colIsStoreChange = new DataColumn("IsStoreChange");
            colIsStoreChange.DataType = typeof(int);
            colIsStoreChange.DefaultValue = 0;
            tblResult.Columns.Add(colIsStoreChange);
            tblResult.Columns.Add("IsAllowDecimal", typeof(bool));

            for (int i = 0; i < tblProductList.Rows.Count; i++)
            {
                string strProductID = Convert.ToString(tblProductList.Rows[i]["ProductID"]).Trim();
                String strProductName = Convert.ToString(tblProductList.Rows[i]["ProductName"]).Trim();
                bool bolIsAllowDecimal = Convert.ToBoolean(tblProductList.Rows[i]["IsAllowDecimal"]);
                String strStatusName = Convert.ToString(tblProductList.Rows[i]["PRODUCTSTATUSNAME"]).Trim();
                int intAverageSaleDays = 0;
                decimal decTotalQuantityInStock = Convert.ToDecimal(Globals.IsNull(tblInStock.Compute("SUM(Quantity)", "ProductID = '" + strProductID + "'"), 0));
                decimal decTotalAverageSale = Convert.ToDecimal(Globals.IsNull(tblAverageSale.Compute("SUM(TotalQuantity)", "ProductID = '" + strProductID + "'"), 0));
                if (decTotalAverageSale != 0)
                    intAverageSaleDays = (int)(decTotalQuantityInStock / decTotalAverageSale);

                //Row tồn kho
                DataRow drInStock = tblResult.NewRow();
                drInStock["ProductID"] = strProductID;
                drInStock["ProductName"] = strProductName;
                drInStock["Quantity"] = "SL Tồn";
                drInStock["IsAllowDecimal"] = bolIsAllowDecimal;
                drInStock["NumOfSaleDays"] = intAverageSaleDays;
                drInStock["PRODUCTSTATUSNAME"] = strStatusName;
                //Row số lượng nhập chuyển kho
                DataRow drInputStoreChangeQuantity = tblResult.NewRow();
                drInputStoreChangeQuantity["ProductID"] = strProductID;
                drInputStoreChangeQuantity["ProductName"] = strProductName;
                drInputStoreChangeQuantity["Quantity"] = "SL Tồn";
                drInputStoreChangeQuantity["IsAllowDecimal"] = bolIsAllowDecimal;
                drInputStoreChangeQuantity["NumOfSaleDays"] = intAverageSaleDays;
                drInputStoreChangeQuantity["Quantity"] = "SL nhập chuyển kho";

                drInputStoreChangeQuantity["PRODUCTSTATUSNAME"] = strStatusName;
                //Row số lượng đề nghị chia
                DataRow drProposalQuantity = tblResult.NewRow();
                drProposalQuantity["ProductID"] = strProductID;
                drProposalQuantity["ProductName"] = strProductName;
                drProposalQuantity["Quantity"] = "SL Tồn";
                drProposalQuantity["IsAllowDecimal"] = bolIsAllowDecimal;
                drProposalQuantity["NumOfSaleDays"] = intAverageSaleDays;
                drProposalQuantity["Quantity"] = "SL đề nghị chia";
                drProposalQuantity["PRODUCTSTATUSNAME"] = strStatusName;
                //Row số lượng bán TB
                DataRow drAverageSale = tblResult.NewRow();
                drAverageSale["ProductID"] = strProductID;
                drAverageSale["ProductName"] = strProductName;
                drAverageSale["Quantity"] = "SL bán TB";
                drAverageSale["IsAllowDecimal"] = bolIsAllowDecimal;
                drAverageSale["NumOfSaleDays"] = intAverageSaleDays;
                drAverageSale["IsAllowEdit"] = true;
                drAverageSale["PRODUCTSTATUSNAME"] = strStatusName;
                //Row số ngày bán hết hàng
                DataRow drDateSale = tblResult.NewRow();
                drDateSale["ProductID"] = strProductID;
                drDateSale["ProductName"] = strProductName;
                drDateSale["Quantity"] = "Số ngày bán hết hàng";
                drDateSale["IsAllowDecimal"] = bolIsAllowDecimal;
                drDateSale["NumOfSaleDays"] = intAverageSaleDays;
                drDateSale["PRODUCTSTATUSNAME"] = strStatusName;
                //Row số lượng bán dự kiến
                DataRow drExpectedQuantity = tblResult.NewRow();
                drExpectedQuantity["ProductID"] = strProductID;
                drExpectedQuantity["ProductName"] = strProductName;
                drExpectedQuantity["Quantity"] = "SL bán TB";
                drExpectedQuantity["IsAllowDecimal"] = bolIsAllowDecimal;
                drExpectedQuantity["NumOfSaleDays"] = intAverageSaleDays;
                drExpectedQuantity["Quantity"] = "SL bán dự kiến";
                drExpectedQuantity["PRODUCTSTATUSNAME"] = strStatusName;
                //Row số lượng bán dự kiến
                DataRow drDifferenceQuantity = tblResult.NewRow();
                drDifferenceQuantity["ProductID"] = strProductID;
                drDifferenceQuantity["ProductName"] = strProductName;
                drDifferenceQuantity["Quantity"] = "SL bán TB";
                drDifferenceQuantity["IsAllowDecimal"] = bolIsAllowDecimal;
                drDifferenceQuantity["NumOfSaleDays"] = intAverageSaleDays;
                drDifferenceQuantity["Quantity"] = "Chênh lệch dự kiến";
                drDifferenceQuantity["PRODUCTSTATUSNAME"] = strStatusName;
                //Số lượng có thể chia
                DataRow drCanSplitQuantity = tblResult.NewRow();
                drCanSplitQuantity["ProductID"] = strProductID;
                drCanSplitQuantity["ProductName"] = strProductName;
                drCanSplitQuantity["Quantity"] = "SL bán TB";
                drCanSplitQuantity["IsAllowDecimal"] = bolIsAllowDecimal;
                drCanSplitQuantity["NumOfSaleDays"] = intAverageSaleDays;
                drCanSplitQuantity["Quantity"] = "SL có thể chia";
                drCanSplitQuantity["IsAllowEdit"] = true;
                drCanSplitQuantity["PRODUCTSTATUSNAME"] = strStatusName;
                for (int j = 0; j < dtbStoreView.Rows.Count; j++)
                {
                    int intStoreID = Convert.ToInt32(dtbStoreView.Rows[j]["StoreID"]);

                    //Fill vào row tồn kho
                    DataRow[] objRow = tblInStock.Select("ProductID = '" + strProductID + "' and StoreID = " + intStoreID.ToString());
                    if (objRow.Length > 0)
                        drInStock[j + intNumColumnNotEdit - 1] = Convert.ToDecimal(objRow[0]["Quantity"]);
                    else drInStock[j + intNumColumnNotEdit - 1] = 0;
                    DataRow[] objRowStoreChangeQuantity = dtbStoreChangeQuantity.Select("ProductID = '" + strProductID + "' and InputStoreID = " + intStoreID.ToString());
                    if (objRowStoreChangeQuantity.Length > 0)
                        drInputStoreChangeQuantity[j + intNumColumnNotEdit - 1] = Convert.ToDecimal(objRowStoreChangeQuantity[0]["Quantity"]);
                    else
                        drInputStoreChangeQuantity[j + intNumColumnNotEdit - 1] = 0;
                    DataRow[] objRow2 = tblAverageSale.Select("ProductID = '" + strProductID + "' and StoreID = " + intStoreID.ToString());
                    if (objRow2.Length > 0)
                        drAverageSale[j + intNumColumnNotEdit - 1] = Convert.ToDecimal(objRow2[0]["TotalQuantity"]);
                    else drAverageSale[j + intNumColumnNotEdit - 1] = 0;
                    drExpectedQuantity[j + intNumColumnNotEdit - 1] = intAverageSaleDays * Convert.ToDecimal(drAverageSale[j + intNumColumnNotEdit - 1]);
                    if (!bolIsAllowDecimal)
                    {
                        drInStock[j + intNumColumnNotEdit - 1] = Math.Round(Convert.ToDecimal(drInStock[j + intNumColumnNotEdit - 1]));
                        drInputStoreChangeQuantity[j + intNumColumnNotEdit - 1] = Math.Round(Convert.ToDecimal(drInputStoreChangeQuantity[j + intNumColumnNotEdit - 1]));
                        drExpectedQuantity[j + intNumColumnNotEdit - 1] = Math.Ceiling(Convert.ToDecimal(drExpectedQuantity[j + intNumColumnNotEdit - 1]));
                    }
                    drProposalQuantity[j + intNumColumnNotEdit - 1] = Convert.ToDecimal(drInStock[j + intNumColumnNotEdit - 1]) - Convert.ToDecimal(drInputStoreChangeQuantity[j + intNumColumnNotEdit - 1]);
                    drDifferenceQuantity[j + intNumColumnNotEdit - 1] = Convert.ToDecimal(drInStock[j + intNumColumnNotEdit - 1]) - Convert.ToDecimal(drExpectedQuantity[j + intNumColumnNotEdit - 1]);
                    drCanSplitQuantity[j + intNumColumnNotEdit - 1] = Convert.ToDecimal(drProposalQuantity[j + intNumColumnNotEdit - 1]) > Convert.ToDecimal(drDifferenceQuantity[j + intNumColumnNotEdit - 1])
                        ? drDifferenceQuantity[j + intNumColumnNotEdit - 1] : drProposalQuantity[j + intNumColumnNotEdit - 1];
                    decimal decProposalQuantity = Convert.ToDecimal(drProposalQuantity[j + intNumColumnNotEdit - 1]);
                    decimal decAverageSale = Convert.ToDecimal(drAverageSale[j + intNumColumnNotEdit - 1]);
                    drDateSale[j + intNumColumnNotEdit - 1] = decAverageSale != 0 ? Math.Round(decProposalQuantity / decAverageSale) : 0;
                }
                //Thêm row tồn kho                
                tblResult.Rows.Add(drInStock);
                // Thêm row số lượng nhập chuyển kho
                tblResult.Rows.Add(drInputStoreChangeQuantity);
                // Thêm row số lượng đề nghị chia
                tblResult.Rows.Add(drProposalQuantity);
                //Thêm row Số lượng bán TB
                tblResult.Rows.Add(drAverageSale);
                //Thêm row Số ngày bán hết hàng
                tblResult.Rows.Add(drDateSale);
                //Thêm row Số lượng bán dự kiến                
                tblResult.Rows.Add(drExpectedQuantity);
                //Thêm row chênh lệch dự kiến                
                tblResult.Rows.Add(drDifferenceQuantity);
                //Thêm row SL có thể chia                
                tblResult.Rows.Add(drCanSplitQuantity);
            }
            return tblResult;
        }

        private void CustomFlex()
        {
            flexListResult.Cols[0].Visible = false;
            flexListResult.Cols[flexListResult.Cols.Count - 2].Visible = false;
            flexListResult.Cols[flexListResult.Cols.Count - 1].Visible = false;
            flexListResult.Cols["ProductID"].Visible = true;
            flexListResult.Cols["ProductID"].AllowEditing = false;
            flexListResult.Cols["ProductID"].AllowMerging = true;
            flexListResult.Cols["ProductName"].AllowMerging = true;
            flexListResult.Cols["ProductName"].AllowEditing = false;
            flexListResult.Cols["PRODUCTSTATUSNAME"].AllowMerging = true;
            flexListResult.Cols["PRODUCTSTATUSNAME"].AllowEditing = false;
            flexListResult.Cols["NumOfSaleDays"].AllowMerging = true;
            flexListResult.Cols["NumOfSaleDays"].AllowEditing = false;

            for (int i = intNumColumnNotEdit - 1; i < flexListResult.Cols.Count - 2; i++)
            {
                flexListResult.Cols[i].Width = 100;
                flexListResult.Cols[i].AllowEditing= false;
                flexListResult.Cols[i].Format = "###,###,##0.##";
            }
            Library.AppCore.LoadControls.C1FlexGridObject.SetAllowDragging(flexListResult, false);
            flexListResult.Cols["ProductID"].Caption = "Mã sản phẩm";
            flexListResult.Cols["ProductName"].Caption = "Tên sản phẩm";
            flexListResult.Cols["PRODUCTSTATUSNAME"].Caption = "Trạng thái sản phẩm";
            flexListResult.Cols["Quantity"].Caption = "Số lượng";
            flexListResult.Cols["NumOfSaleDays"].Caption = "Số ngày bán dự kiến";
            flexListResult.Cols["IsAllowEdit"].Visible = false;
            flexListResult.Cols["ISAllowDecimal"].Visible = false;

            C1.Win.C1FlexGrid.CellStyle style4 = flexListResult.Styles.Add("flexStyle1");
            style4.WordWrap = true;
            style4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10, FontStyle.Bold);
            style4.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range4 = flexListResult.GetCellRange(0, 0, 0, flexListResult.Cols.Count - 1);
            range4.Style = style4;
            flexListResult.Rows[0].Height = 50;
            CheckIsStoreChange();
        }

        private void CheckIsStoreChange()
        {
            flexListResult.Styles.Clear();
            for (int i =0; i < tblTotal.Rows.Count; i++)
            {
                if (((i + 1) % 8) == 0)
                //if (((i) % 8) == 0)
                {
                    CheckIsStoreChange(i + 1);
                }
                else
                {
                    Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexListResult, i + 1, Color.White);
                }
            }
        }
        private void CheckIsStoreChange(int intFlexRow)
        {
            int intResult = CheckRow(tblTotal.Rows[intFlexRow - 1]);
            if (intResult == 0)
            {
                Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexListResult, intFlexRow, Color.Green);
                tblTotal.Rows[intFlexRow - 1]["IsStoreChange"] = 0;
            }
            else if (intResult == 1)
            {
                Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexListResult, intFlexRow, Color.Red);
                tblTotal.Rows[intFlexRow - 1]["IsStoreChange"] = 0;
            }
            else
            {
                Library.AppCore.LoadControls.C1FlexGridObject.SetRowBackColor(flexListResult, intFlexRow, Color.White);
                tblTotal.Rows[intFlexRow - 1]["IsStoreChange"] = 1;
            }
        }

        private int CheckRow(DataRow objRow)
        {
            int intTotalMinus = 0;
            int intTotalPlus = 0;
            for (int i = intNumColumnNotEdit - 1; i < objRow.ItemArray.Length - 3; i++)
            {
                int intValue = !Convert.IsDBNull(objRow[i]) ? Convert.ToInt32(objRow[i]) : 0;
                if (intValue == 0)
                    continue;
                if (intValue > 0)
                {
                    intTotalPlus = 1;
                }
                else
                {
                    intTotalMinus = 1;
                }
            }
            if (intTotalPlus > 0 && intTotalMinus < 1)
            {
                return 0;
            }
            else if (intTotalPlus < 1 && intTotalMinus > 0)
            {
                return 1;
            }
            return 2;
        }

        private void cboAreaIDList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER;
                objStoreFilter.AreaType = Library.AppCore.Constant.EnumType.AreaType.AREA;
                objStoreFilter.AreaIDList = cboAreaIDList.AreaIDList;
                objStoreFilter.IsCheckPermission = true;
                objStoreFilter.OtherCondition = "ISACTIVE = 1 and ISAUTOSTORECHANGE = 1";
                cboStoreList.InitControl(true, objStoreFilter);
                GetStoreReportView(false);
                btnCalculator.Enabled = false;
            }
            catch (Exception objExc)
            {
                SystemErrorWS.Insert("Lỗi nạp dữ liệu combobox", objExc, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi nạp dữ liệu combobox", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void cboMainGroupID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int intMainGroupID = cboMainGroupID.MainGroupID;
            cboSubGroupID.InitControl(false, intMainGroupID, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL, Library.AppCore.Constant.EnumType.IsServiceType.NOT_ISSERVICE);
            ComboBox cboBrand = new ComboBox();
            Library.AppCore.LoadControls.SetDataSource.SetBrand(this, cboBrand, intMainGroupID);
            dtbBrand = (DataTable)cboBrand.DataSource;
            dtbBrand.Rows.RemoveAt(0);
            cboBrandIDList.InitControl(true, dtbBrand);
            cboProductList1.InitControl(intMainGroupID, string.Empty, string.Empty, false, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL,
                Library.AppCore.Constant.EnumType.IsServiceType.NOT_ISSERVICE, Library.AppCore.Constant.EnumType.MainGroupPermissionType.ALL);
        }

        private void cboSubGroupID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboSubGroupID.SubGroupID > 0)
            {
                cboProductList1.InitControl(cboMainGroupID.MainGroupID, "<" + cboSubGroupID.SubGroupID + ">", string.Empty, false,Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL,
                    Library.AppCore.Constant.EnumType.IsServiceType.NOT_ISSERVICE,Library.AppCore.Constant.EnumType.MainGroupPermissionType.ALL);
            }
            else
            {
                cboProductList1.InitControl(cboMainGroupID.MainGroupID, string.Empty, string.Empty, false,Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL,Library.AppCore.Constant.EnumType.IsServiceType.NOT_ISSERVICE,Library.AppCore.Constant.EnumType.MainGroupPermissionType.ALL);
            }
        }

        private void cboBrandIDList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboSubGroupID.SubGroupID > 0)
            {
                cboProductList1.InitControl(cboMainGroupID.MainGroupID, "<" + cboSubGroupID.SubGroupID + ">", cboBrandIDList.BrandIDList,
                    false, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL, Library.AppCore.Constant.EnumType.IsServiceType.NOT_ISSERVICE,
                    Library.AppCore.Constant.EnumType.MainGroupPermissionType.ALL);
            }
            else
                cboProductList1.InitControl(cboMainGroupID.MainGroupID, string.Empty, cboBrandIDList.BrandIDList,
                    false, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL,
                         Library.AppCore.Constant.EnumType.IsServiceType.NOT_ISSERVICE,
                          Library.AppCore.Constant.EnumType.MainGroupPermissionType.ALL);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            btnCalculator.Enabled = false;
            if (!CheckInput())
            {
                btnSearch.Enabled = true;
                return;
            }
            this.Cursor = Cursors.WaitCursor;
            LoadAutoStoreChangeData();
            this.Cursor = Cursors.Default;
            btnSearch.Enabled = true;
            if (tblTotal != null && tblTotal.Rows.Count > 0)
            {
                btnCalculator.Enabled = true;
            }
        }

        private bool CheckMinimumUnEvent()
        {
            if (Convert.ToInt32(txtMinimumUnEvent.Value) < 0)
            {
                MessageBox.Show(this, "Độ chênh lệch tối thiểu phải lớn hơn hoặc bằng không", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMinimumUnEvent.Focus();
                return false;
            }
            if (Convert.ToInt32(txtMinimumUnEvent.Value) >= 10000)
            {
                MessageBox.Show(this, "Độ chênh lệch tối thiểu quá lớn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMinimumUnEvent.Focus();
                return false;
            }
            int intMaxDay = Convert.ToInt32(txtMaxDay.Value);
            int intMinDay = Convert.ToInt32(txtMinDay.Value);
            if (intMaxDay < 1)
            {
                MessageBox.Show(this, "Số ngày MAX phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMaxDay.Focus();
                return false;
            }
            if (intMinDay < 1)
            {
                MessageBox.Show(this, "Số ngày MIN phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMinDay.Focus();
                return false;
            }
            if (intMinDay > intMaxDay)
            {
                MessageBox.Show(this, "Số ngày MIN phải nhở hơn hoặc bằng số ngày MAX", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMinDay.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Chọn những chi nhánh tồn kho thỏa điều kiện độ chênh lệch tối thiểu
        /// </summary>
        /// <returns></returns>
        private DataTable CreateUnEventTable()
        {
            int intMinimumUnEvent = Convert.ToInt32(txtMinimumUnEvent.Value);
            int intMaxDay = Convert.ToInt32(txtMaxDay.Value);
            int intMinDay = Convert.ToInt32(txtMinDay.Value);
            DataTable tblResult = tblTotal.Clone();

            int i = 7;
            int k = 0;
            while (i < tblTotal.Rows.Count)
            {
                if (Convert.ToBoolean(tblTotal.Rows[i]["IsStoreChange"]))
                {
                    tblResult.ImportRow(tblTotal.Rows[i]);
                    for (int j = 0; j < dtbStoreView.Rows.Count; j++)
                    {
                        // Lấy ra siêu thị có Số ngày bán hết hàng >= SN Max hoặc <= SN Min
                        decimal deSaleDate = !Convert.IsDBNull(tblTotal.Rows[i - 3][dtbStoreView.Rows[j]["StoreName"].ToString().Trim()]) ? Convert.ToDecimal(tblTotal.Rows[i - 3][dtbStoreView.Rows[j]["StoreName"].ToString().Trim()]) : 0;
                        if (deSaleDate <= intMinDay || deSaleDate >= intMaxDay)
                        {
                            decimal decValue = tblTotal.Rows[i][dtbStoreView.Rows[j]["StoreName"].ToString().Trim()] == DBNull.Value ? 0 : Convert.ToDecimal(tblTotal.Rows[i][dtbStoreView.Rows[j]["StoreName"].ToString().Trim()]);
                            // Nếu kho không được phép nhập chuyển kho hoặc không thỏa điều kiện chênh lệch tối thiểu thì không xét
                            if (!(ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(Convert.ToInt32(dtbStoreView.Rows[j]["StoreID"]), intMainGroupID,
                                Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                                    || (decValue < 0 && Math.Abs(decValue) < intMinimumUnEvent))
                            {
                                tblResult.Rows[k][dtbStoreView.Rows[j]["StoreName"].ToString().Trim()] = 0;
                            }
                            // Nếu kho không được phép xuất chuyển kho hoặc không thỏa điếu kiện chênh lệch tối thiểu thì không xét
                            else if (!(ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(Convert.ToInt32(dtbStoreView.Rows[j]["StoreID"]), intMainGroupID,
                                Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
                                    || (decValue > 0 && decValue < intMinimumUnEvent))
                            {
                                tblResult.Rows[k][dtbStoreView.Rows[j]["StoreName"].ToString().Trim()] = 0;
                            }
                        }
                        else
                        {
                            tblResult.Rows[k][dtbStoreView.Rows[j]["StoreName"].ToString().Trim()] = 0;
                        }
                    }
                    k++;
                }
                i += 8;
            }

            tblResult.Columns.Add("CancleStore", typeof(string));
            return tblResult;
        }

        private DataTable CreateInStockTable()
        {
            DataTable tblResult = tblTotal.Clone();
            for (int i = 0; i < tblTotal.Rows.Count; i++)
            {
                if ((i % 8) == 0) tblResult.ImportRow(tblTotal.Rows[i]);
            }
            return tblResult;
        }


        /// <summary>
        /// Tìm giá trị nhỏ nhất
        /// </summary>
        /// <param name="intRowIndex"></param>
        /// <param name="intProvinceID"></param>
        /// <param name="intCompanyID"></param>
        /// <param name="intReturnColIndex"></param>
        /// <returns></returns>
        private bool GetMinValue(int intRowIndex, ref int intProvinceID, ref int intCompanyID, ref int intReturnColIndex)
        {
            int intMinimumUnEvent = Convert.ToInt32(txtMinimumUnEvent.Value);
            intProvinceID = Convert.ToInt32(dtbStoreView.Rows[intReturnColIndex]["ProvinceID"]);
            intCompanyID = Convert.ToInt32(dtbStoreView.Rows[intReturnColIndex]["CompanyID"]);
            intReturnColIndex = tblUnEvent.Columns[dtbStoreView.Rows[0]["StoreName"].ToString().Trim()].Ordinal;
            decimal decValue = Convert.ToDecimal(tblUnEvent.Rows[intRowIndex][intReturnColIndex]);
            if (tblUnEvent.Rows[intRowIndex]["CancleStore"].ToString().Contains("<" + dtbStoreView.Rows[0]["StoreID"].ToString() + ">"))
            {
                decValue = 0;
            }
            for (int i = 1; i < dtbStoreView.Rows.Count; i++)
            {
                if (tblUnEvent.Rows[intRowIndex]["CancleStore"].ToString().Contains("<" + dtbStoreView.Rows[i]["StoreID"].ToString() + ">"))
                {
                    continue;
                }
                string strColNameNext = dtbStoreView.Rows[i]["StoreName"].ToString().Trim();
                decimal decValueNext = Convert.ToDecimal(tblUnEvent.Rows[intRowIndex][strColNameNext]);
                if (decValue > decValueNext && Math.Abs(decValueNext) >= intMinimumUnEvent)
                {
                    decValue = decValueNext;
                    intReturnColIndex = tblUnEvent.Columns[strColNameNext].Ordinal;
                    intProvinceID = Convert.ToInt32(dtbStoreView.Rows[i]["ProvinceID"]);
                    intCompanyID = Convert.ToInt32(dtbStoreView.Rows[i]["CompanyID"]);
                }
            }
            if (decValue >= 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Tìm giá trị lớn nhất tương ứng từng độ ưu tiên
        /// </summary>
        /// <param name="intRowIndex"></param>
        /// <param name="intProvinceID"></param>
        /// <param name="intCompanyID"></param>
        /// <param name="enuTYPECALCULATE"></param>
        /// <param name="intToStoreID"></param>
        /// <param name="intReturnColIndex"></param>
        /// <returns></returns>
        private bool GetMaxValue(int intRowIndex, int intProvinceID, int intCompanyID, TYPECALCULATE enuTYPECALCULATE, int intToStoreID, ref int intReturnColIndex)
        {
            DataRow[] rowStores = null;
            if (enuTYPECALCULATE == TYPECALCULATE.SAME_PROVINCE)
            {
                rowStores = dtbStoreView.Select("CompanyID = " + intCompanyID + " and ProvinceID = " + intProvinceID);
                if (rowStores.Length < 1)
                {
                    tblUnEvent.Rows[intRowIndex]["CancleStore"] = tblUnEvent.Rows[intRowIndex]["CancleStore"].ToString() + "<" + intToStoreID + ">";
                    return false;
                }
            }
            else if (enuTYPECALCULATE == TYPECALCULATE.OTHER_PROVINCE)
            {
                rowStores = dtbStoreView.Select("CompanyID = " + intCompanyID);
                if (rowStores.Length < 1)
                {
                    tblUnEvent.Rows[intRowIndex]["CancleStore"] = tblUnEvent.Rows[intRowIndex]["CancleStore"].ToString() + "<" + intToStoreID + ">";
                    return false;
                }
            }
            else if (enuTYPECALCULATE == TYPECALCULATE.OTHER_COMPANY)
            {
                rowStores = dtbStoreView.Select("1 = 1");
                if (rowStores.Length < 1)
                {
                    tblUnEvent.Rows[intRowIndex]["CancleStore"] = tblUnEvent.Rows[intRowIndex]["CancleStore"].ToString() + "<" + intToStoreID + ">";
                    return false;
                }
            }
            int intMinimumUnEvent = Convert.ToInt32(txtMinimumUnEvent.Value);
            string strColName = rowStores[0]["StoreName"].ToString().Trim();
            decimal decValue = Convert.ToDecimal(tblUnEvent.Rows[intRowIndex][strColName]);
            intReturnColIndex = tblUnEvent.Columns[strColName].Ordinal;
            for (int i = 1; i < rowStores.Length; i++)
            {
                string strColNameNext = rowStores[i]["StoreName"].ToString().Trim();
                decimal decValueNext = Convert.ToDecimal(tblUnEvent.Rows[intRowIndex][strColNameNext]);
                if (decValue < decValueNext && Math.Abs(decValueNext) >= intMinimumUnEvent)
                {
                    decValue = decValueNext;
                    intReturnColIndex = tblUnEvent.Columns[strColNameNext].Ordinal;
                }
            }
            if (decValue <= 0)
            {
                tblUnEvent.Rows[intRowIndex]["CancleStore"] = tblUnEvent.Rows[intRowIndex]["CancleStore"].ToString() + "<" + intToStoreID + ">";
                return false;
            }
            return true;
        }

        private int GetStoreID(int intCol)
        {
            string strStoreName = tblUnEvent.Columns[intCol].ColumnName.Trim();
            DataRow[] objRow = dtbStoreView.Select("StoreName = '" + strStoreName + "'");
            if (objRow.Length > 0)
                return Convert.ToInt32(objRow[0]["StoreID"]);
            return 0;
        }

        private String GetStoreName(int intStoreID)
        {
            DataRow[] objRow = dtbStoreView.Select("StoreID = " + intStoreID.ToString());
            if (objRow.Length > 0)
                return Convert.ToString(objRow[0]["StoreName"]);
            return "";
        }

        private decimal GetStoreInStock(int intStoreID, string strProductID)
        {
            DataRow[] arrRow = tblInStock.Select("ProductID = '" + strProductID + "' and StoreID = " + intStoreID.ToString());
            if (arrRow.Length > 0)
                return Convert.ToDecimal(arrRow[0]["Quantity"]);
            return 0;
        }

        private void InitAutoStoreChangeTable()
        {
            tblAutoStoreChange = new DataTable();
            tblAutoStoreChange.Columns.Add("ProductID", typeof(string));
            tblAutoStoreChange.Columns.Add("ProductName", typeof(String));
            tblAutoStoreChange.Columns.Add("FromStoreID", typeof(int));
            tblAutoStoreChange.Columns.Add("FromStoreName", typeof(String));
            tblAutoStoreChange.Columns.Add("FromStoreInStock", typeof(decimal));
            tblAutoStoreChange.Columns.Add("ToStoreID", typeof(int));
            tblAutoStoreChange.Columns.Add("ToStoreName", typeof(String));
            tblAutoStoreChange.Columns.Add("ToStoreInStock", typeof(decimal));
            tblAutoStoreChange.Columns.Add("Quantity", typeof(decimal));
            tblAutoStoreChange.Columns.Add("Select", typeof(bool));
            tblAutoStoreChange.Columns.Add("QuantityInStock", typeof(decimal));
            tblAutoStoreChange.Columns.Add("IsAllowDecimal", typeof(bool));
        }

        private bool SplitQuantity(TYPECALCULATE enuTYPECALCULATE, DataTable tblNewInStock)
        {
            for (int i = 0; i < tblUnEvent.Rows.Count; i++)
            {
                tblUnEvent.Rows[i]["CancleStore"] = string.Empty;
                decimal decMaxValue = 0;
                decimal decMinValue = 0;
                do
                {
                    int intProvinceID = 0;
                    int intCompanyID = 0;
                    int intMaxCol = 0;
                    int intMinCol = 0;
                    if (!GetMinValue(i, ref intProvinceID, ref intCompanyID, ref intMinCol))
                        goto Finish;
                    decMinValue = Convert.ToDecimal(tblUnEvent.Rows[i][intMinCol]);
                    int intToStoreID = GetStoreID(intMinCol);
                    if (!GetMaxValue(i, intProvinceID, intCompanyID, enuTYPECALCULATE, intToStoreID, ref intMaxCol))
                        continue;
                    int intFromStoreID = GetStoreID(intMaxCol);
                    decMaxValue = Convert.ToDecimal(tblUnEvent.Rows[i][intMaxCol]);
                    decimal decDelta = decMinValue + decMaxValue;
                    decimal decQuantity = 0;
                    if (decDelta < 0)
                    {
                        tblUnEvent.Rows[i][intMaxCol] = 0;
                        tblUnEvent.Rows[i][intMinCol] = decDelta;
                        decQuantity = decMaxValue;
                    }
                    else
                    {
                        tblUnEvent.Rows[i][intMaxCol] = decDelta;
                        tblUnEvent.Rows[i][intMinCol] = 0;
                        decQuantity = (-1) * decMinValue;
                    }
                    string strProductID = tblUnEvent.Rows[i][0].ToString().Trim();
                    string strProductName = tblUnEvent.Rows[i][1].ToString().Trim();
                    bool bolIsAllowDecimal = Convert.ToBoolean(tblUnEvent.Rows[i]["IsAllowDecimal"]);
                    decimal decFromStoreInStock = GetStoreInStock(intFromStoreID, strProductID);
                    decimal decToStoreInStock = GetStoreInStock(intToStoreID, strProductID);
                    String strFromStoreName = intFromStoreID +" - " + GetStoreName(intFromStoreID);
                    String strToStoreName =  intToStoreID + " - " + GetStoreName(intToStoreID);
                    DataRow[] objRowInStock = tblNewInStock.Select("ProductID='" + strProductID + "'");
                    decimal decQuantityInStock = 0;
                    if (objRowInStock.Length > 0)
                        decQuantityInStock = Convert.ToDecimal(objRowInStock[0][intMaxCol]);
                    DataRow objRow = tblAutoStoreChange.NewRow();
                    if (!bolIsAllowDecimal)
                    {
                        decFromStoreInStock = Math.Round(decFromStoreInStock);
                        decToStoreInStock = Math.Round(decToStoreInStock);
                        decQuantity = Math.Round(decQuantity);
                        decQuantityInStock = Math.Round(decQuantityInStock);
                    }
                    objRow.ItemArray = new object[] { strProductID, strProductName, intFromStoreID, 
                                strFromStoreName, decFromStoreInStock, intToStoreID, strToStoreName, decToStoreInStock, 
                                decQuantity, true, decQuantityInStock, bolIsAllowDecimal };
                    tblAutoStoreChange.Rows.Add(objRow);
                }
                while (true);
            Finish:
                continue;
            }
            return true;
        }


        private void btnCalculator_Click(object sender, EventArgs e)
        {
            
            if (!CheckMinimumUnEvent()) return;
            tblUnEvent = CreateUnEventTable();
            #region SL tồn giữ lại kho
            decimal decQuantityKeepAlive = Convert.ToDecimal(txtMinInStock.Value);
            if (decQuantityKeepAlive > 0)
            {
                for (int i = 0; i < tblUnEvent.Rows.Count; i++)
                {
                    for (int j = tblUnEvent.Columns["Quantity"].Ordinal + 1; j < tblUnEvent.Columns["IsAllowEdit"].Ordinal; j++)
                    {
                        decimal decQuantity = Convert.ToDecimal(tblUnEvent.Rows[i][j]);
                        if (decQuantity >= 0)
                        {
                            if (decQuantity >= decQuantityKeepAlive)
                                tblUnEvent.Rows[i][j] = decQuantity - decQuantityKeepAlive;
                            else
                                tblUnEvent.Rows[i][j] = 0;
                        }
                    }
                }
            }
            #endregion
            DataTable tblNewInStock = CreateInStockTable();
            if(tblAutoStoreChange!=null)
               tblAutoStoreChange.Clear();
            // DK1: Chia theo độ ưu tiên cùng thành phố và cùng công ty
            SplitQuantity(TYPECALCULATE.SAME_PROVINCE, tblNewInStock);
            // DK2: Chia theo độ ưu tiên # thành phố và cùng công ty (bao gồm cả DK1)
            SplitQuantity(TYPECALCULATE.OTHER_PROVINCE, tblNewInStock);
            // DK3: Chia theo độ ưu tiên khác công ty (bao gồm cả DK1 + DK2)
            SplitQuantity(TYPECALCULATE.OTHER_COMPANY, tblNewInStock);
            if (tblAutoStoreChange.Rows.Count < 1)
            {
                MessageBox.Show(this, "Chương trình không tự động tạo ra lệnh chuyển kho nào.\n Bạn vui lòng xem lại độ chênh lệch tối thiểu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            frmAutoStoreChangeDetail frmAutoStoreChangeDetail1 = new frmAutoStoreChangeDetail();
            frmAutoStoreChangeDetail1.AutoStoreChangeTable = tblAutoStoreChange;
            frmAutoStoreChangeDetail1.intMainGroupID = intMainGroupID;
            frmAutoStoreChangeDetail1.IsShowProduct = intIsShowProduct;
            frmAutoStoreChangeDetail1.IsCheckRealInput = intIsCheckRealInput;
            frmAutoStoreChangeDetail1.dtbStore = dtbStoreView.Copy();
            frmAutoStoreChangeDetail1.ShowDialog();
        }

        private void CountNumSaleDays(DataRow drInStock, DataRow drProposalQuantity, DataRow drAverageSale, DataRow drSaleDate, DataRow drExpectedQuantity
            , DataRow drDifferenceQuantity, DataRow drCanSplitQuantity, int intStartPos, int intEndPos)
        {
            try
            {
                int intAverageSaleDays = Convert.ToInt32(drAverageSale["NumOfSaleDays"]);
                for (int i = intStartPos; i < intEndPos; i++)
                {
                    drSaleDate[i] = Convert.ToDecimal(drAverageSale[i]) != 0 ? Math.Round(Convert.ToDecimal(drProposalQuantity[i]) / Convert.ToDecimal(drAverageSale[i])) : 0;
                    drExpectedQuantity[i] = Convert.ToDecimal(drAverageSale[i]) * intAverageSaleDays;
                    if (!Convert.ToBoolean(drAverageSale["IsAllowDecimal"]))
                    {
                        drExpectedQuantity[i] = Math.Ceiling(Convert.ToDecimal(drExpectedQuantity[i]));
                    }
                    drDifferenceQuantity[i] = Convert.ToDecimal(drInStock[i]) - Convert.ToDecimal(drExpectedQuantity[i]);
                    drCanSplitQuantity[i] = Convert.ToDecimal(drProposalQuantity[i]) > Convert.ToDecimal(drDifferenceQuantity[i])
                        ? drDifferenceQuantity[i] : drProposalQuantity[i];
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi tính các thông số khởi tạo", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi tính các thông số khởi tạo", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnReCalculator_Click(object sender, EventArgs e)
        {
        
            int intAverageNumSalesDays = Convert.ToInt32(txtAverageNumSalesDays.Value);
            if (intAverageNumSalesDays < 1 || intAverageNumSalesDays >= 100)
            {
                MessageBox.Show(this, "Số ngày bán trung bình dự kiến phải lớn hơn 0 và nhỏ hơn 100", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            btnCalculator.Enabled = false;
            for (int i = 0; i < tblTotal.Rows.Count; i++)
                tblTotal.Rows[i]["NumOfSaleDays"] = txtAverageNumSalesDays.Value;

            for (int i = 0; i < tblTotal.Rows.Count - 7; i += 8)
            {
                CountNumSaleDays(tblTotal.Rows[i], tblTotal.Rows[i + 2], tblTotal.Rows[i + 3], tblTotal.Rows[i + 4], tblTotal.Rows[i + 5],
                    tblTotal.Rows[i + 6], tblTotal.Rows[i + 7], intNumColumnNotEdit - 1, tblTotal.Columns.Count - 3);
            }
            CheckIsStoreChange();
            if (tblTotal != null && tblTotal.Rows.Count > 0)
            {
                btnCalculator.Enabled = true;
            }
        }

        private void tmrFormatFlex_Tick(object sender, EventArgs e)
        {

        }

    }
}
