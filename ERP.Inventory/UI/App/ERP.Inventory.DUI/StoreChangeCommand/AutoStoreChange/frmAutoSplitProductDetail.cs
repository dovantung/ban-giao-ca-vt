﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    public partial class frmAutoSplitProductDetail : Form
    {
        public frmAutoSplitProductDetail()
        {
            InitializeComponent();
        }
        #region Khai báo biến sử dụng
        public int intStoreID = 0;
        public String strStoreName = "";
        public String strOrderID = "";
        public decimal decTotalQuantity = 0;
        public DataTable dtbAutoSplitProduct = null;
        public DataTable dtbProductList = null;
        public DataTable dtbStore = null;
        public bool bolIsFinish = false;
        private int intStoreChangeCommandTypeID = -1;
        private DataRow rPrevEdit = null;
        PLC.StoreChangeCommand.PLCStoreChangeCommand objPLCStoreChangeCommand = new PLC.StoreChangeCommand.PLCStoreChangeCommand();
        #endregion
        #region Methods
        private void LoadComboBox()
        {
            try
            {
                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                cboFromStoreID.InitControl(false, objStoreFilter);
                cboFromStoreID.SetValue(intStoreID);
                cboFromStoreID.Enabled = false;
                cboToStoreID.InitControl(false,dtbStore.Copy());
                cboProduct.Properties.DataSource = dtbProductList;
              
            }
            catch (Exception objExc)
            {
                string strMessage = "Lỗi nạp dữ liệu combobox";
                MessageBox.Show(strMessage, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void BindData()
        {
            if (!dtbAutoSplitProduct.Columns.Contains("IsUrgent"))
            {
                DataColumn dtColumn = new DataColumn("IsUrgent");
                dtColumn.DefaultValue = false;
                dtbAutoSplitProduct.Columns.Add(dtColumn);
            }

            DataView dvAutoSplitProduct = new DataView(dtbAutoSplitProduct);
            dvAutoSplitProduct.Sort = "ProductID, TotalQuantity";
            flexListAutoSplitProduct.DataSource = dvAutoSplitProduct;
            CustomFlex();
        }

        private void CustomFlex()
        {
            try
            {
                flexListAutoSplitProduct.Cols[0].Visible = false;
                flexListAutoSplitProduct.Cols["ToStoreID"].Visible = false;
                flexListAutoSplitProduct.Cols["IsAdjust"].Visible = false;
                flexListAutoSplitProduct.Cols["IsAllowDecimal"].Visible = false;

                flexListAutoSplitProduct.Cols["ProductID"].AllowMerging = true;
                flexListAutoSplitProduct.Cols["ProductName"].AllowMerging = true;
                flexListAutoSplitProduct.Cols["TotalQuantity"].AllowMerging = true;

                flexListAutoSplitProduct.Cols["ProductID"].Caption = "Mã sản phẩm";
                flexListAutoSplitProduct.Cols["ProductName"].Caption = "Tên sản phẩm";
                flexListAutoSplitProduct.Cols["TotalQuantity"].Caption = "Tổng SL";
                flexListAutoSplitProduct.Cols["ToStoreName"].Caption = "Chuyển đến";
                flexListAutoSplitProduct.Cols["Quantity"].Caption = "SL";
                flexListAutoSplitProduct.Cols["Select"].Caption = "Chọn";
                flexListAutoSplitProduct.Cols["IsUrgent"].Caption = "Chuyển gấp";

                flexListAutoSplitProduct.Cols["ProductID"].AllowEditing = false;
                flexListAutoSplitProduct.Cols["ProductID"].Visible = true;
                flexListAutoSplitProduct.Cols["ProductName"].AllowEditing = false;
                flexListAutoSplitProduct.Cols["TotalQuantity"].AllowEditing = false;
                flexListAutoSplitProduct.Cols["ToStoreID"].AllowEditing = false;
                flexListAutoSplitProduct.Cols["ToStoreName"].AllowEditing = false;
                flexListAutoSplitProduct.Cols["Quantity"].AllowEditing = true;
                flexListAutoSplitProduct.Cols["Select"].AllowEditing = true;
                Library.AppCore.LoadControls.C1FlexGridObject.SetAllowDragging(flexListAutoSplitProduct, false);
                flexListAutoSplitProduct.Cols["Quantity"].DataType = typeof(decimal);
                flexListAutoSplitProduct.Cols["TotalQuantity"].Format = "#,##0.####";
                flexListAutoSplitProduct.Cols["IsUrgent"].DataType = typeof(Boolean);

                C1.Win.C1Input.C1TextBox txtEditor1 = new C1.Win.C1Input.C1TextBox();
                txtEditor1.NumericInputKeys = C1.Win.C1Input.NumericInputKeyFlags.Plus;
                flexListAutoSplitProduct.Cols["Quantity"].Editor = txtEditor1;
                flexListAutoSplitProduct.Cols["Quantity"].Format = "#,###,###,##0.####";

                #region Định dạng Style cho lưới
                Library.AppCore.LoadControls.C1FlexGridObject.SetBackColor(flexListAutoSplitProduct, SystemColors.Info);
                C1.Win.C1FlexGrid.CellStyle style = flexListAutoSplitProduct.Styles.Add("flexStyle");
                style.WordWrap = true;
                style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8, FontStyle.Bold);
                style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
                C1.Win.C1FlexGrid.CellRange range = flexListAutoSplitProduct.GetCellRange(0, 0, 0, flexListAutoSplitProduct.Cols.Count - 1);
                range.Style = style;
                flexListAutoSplitProduct.Rows[0].Height = 20;
                #endregion
            }
            catch (Exception objExc)
            {
                string strMessage = "Lỗi định dạng lưới dữ liệu";
                Library.AppCore.SystemErrorWS.Insert(strMessage, objExc, DUIInventory_Globals.ModuleName);
                MessageBox.Show(strMessage, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private decimal GetOrderQuantity(String strProductID)
        {
            DataRow[] arrRow = dtbProductList.Select("ProductID = '" + strProductID + "'");
            if (arrRow.Length > 0)
            {
                return Convert.ToDecimal(arrRow[0]["RemainQuantity"]);
            }
            return 0;

        }

        private decimal GetTotalQuantity(String strProductID)
        {
            return Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbAutoSplitProduct.Compute("sum(Quantity)", "Select = 1 and ProductID = '" + strProductID + "'"), 0));
        }

        private bool CheckInputAdd(ref ERP.MasterData.PLC.MD.WSProduct.Product objProduct, int intToStoreID)
        {
            if (cboToStoreID.StoreID<= 0)
            {
                MessageBox.Show(this, "Vui lòng chọn kho nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboToStoreID.Focus();
                return false;
            }
            if (intStoreID == intToStoreID)
            {
                MessageBox.Show("Kho nhập phải khác với kho xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboToStoreID.Focus();
                return false;
            }
           
            if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intStoreID, objProduct.MainGroupID,
                 Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEOUTPUT))
            {
                MessageBox.Show(this, "Kho xuất này không được phép xuất chuyển kho trên ngành hàng " + objProduct.MainGroupName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (!ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intToStoreID, objProduct.MainGroupID,
                 Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
            {
                MessageBox.Show(this, "Kho nhập này không được phép nhập chuyển kho trên ngành hàng " + objProduct.MainGroupName, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            String strSelectExp = "ToStoreID = " + Convert.ToString(intToStoreID) + " and ProductID = '" + objProduct.ProductID + "'";
            DataRow[] arrRow = dtbAutoSplitProduct.Select(strSelectExp);
            if (arrRow.Length > 0)
            {
                MessageBox.Show(this, "Sản phẩm chuyển kho đã tồn tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (Convert.ToDecimal(txtQuantity.Value) <= 0)
            {
                MessageBox.Show(this, "Vui lòng nhập số lượng cần chuyển", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtQuantity.Focus();
                return false;
            }
            decimal decInStockQuantity = GetOrderQuantity(objProduct.ProductID);
            decimal decTotalQuantity = GetTotalQuantity(objProduct.ProductID);
            decimal decQuantity = Convert.ToDecimal(txtQuantity.Value);
            if (!objProduct.IsAllowDecimal)
            {
                decInStockQuantity = Math.Round(decInStockQuantity);
                decTotalQuantity = Math.Round(decTotalQuantity);
                decQuantity = Math.Round(decQuantity);
            }
            if (decQuantity > (decInStockQuantity - decTotalQuantity))
            {
                MessageBox.Show(this, "Số lượng cần chuyển lớn hơn số lượng đặt hàng còn lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtQuantity.Value = decInStockQuantity - decTotalQuantity;
                txtQuantity.Focus();
                return false;
            }
            if (!CheckQuantity(objProduct.ProductID, ERP.MasterData.PLC.MD.PLCProduct.GetProductName(objProduct.ProductID)
               , Convert.ToInt32(cboFromStoreID.StoreID), ERP.MasterData.PLC.MD.PLCStore.GetStoreName(Convert.ToInt32(cboFromStoreID.StoreID)), decInStockQuantity,
               Convert.ToDecimal(txtQuantity.Value)))
                return false;
            return true;
        }
        private void AddProduct(ERP.MasterData.PLC.MD.WSProduct.Product objProduct, int intToStoreID,
            String strToStoreName)
        {
            decimal decFromInstockQuantity = GetOrderQuantity(objProduct.ProductID);
            PLC.WSProductInStock.ProductInStock objProductInStock =new ERP.Inventory.PLC.PLCProductInStock().LoadInfo(objProduct.ProductID, intToStoreID);
            decimal decToInstockQuantity = objProductInStock.Quantity;
            decimal decQuantity = Convert.ToDecimal(txtQuantity.Value);
            if (!objProduct.IsAllowDecimal)
            {
                decFromInstockQuantity = Math.Round(decFromInstockQuantity);
                decToInstockQuantity = Math.Round(decToInstockQuantity);
                decQuantity = Math.Round(decQuantity);
            }
            DataRow objRow = dtbAutoSplitProduct.NewRow();
            objRow.ItemArray = new object[] { objProduct.ProductID, objProduct.ProductName, decFromInstockQuantity, 
                intToStoreID, strToStoreName, decQuantity, true, true, objProduct.IsAllowDecimal };
            dtbAutoSplitProduct.Rows.Add(objRow);
        }


        #endregion
        private void cmdExportExcel_Click(object sender, EventArgs e)
        {
            //Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(flexListAutoSplitProduct);
            Library.AppCore.LoadControls.C1FlexGridObject.ExportExcel(flexListAutoSplitProduct);
        }

        private void frmAutoSplitProductDetail_Load(object sender, EventArgs e)
        {
            intStoreChangeCommandTypeID = AppConfig.GetIntConfigValue("AUTOSPLITPRODUCT_STORECHANGECOMMANDTYPEID");
            if (intStoreChangeCommandTypeID < 0)
            {
                MessageBox.Show(this, "Bạn chưa cấu hình loại lệnh chuyển kho mặc định", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
            LoadComboBox();
            txtOrderID.Text = strOrderID;
            txtTotalQuantity.Value = decTotalQuantity;
            BindData();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (cboProduct.EditValue == null)
            {
                MessageBox.Show(this, "Vui lòng chọn sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboProduct.Focus();
                return;
            }
            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(cboProduct.EditValue.ToString().Trim());
            if (objProduct == null)
            {
                MessageBox.Show(this, "Sản phẩm không tồn tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            int intToStoreID = cboToStoreID.StoreID;
            if (!CheckInputAdd(ref objProduct, intToStoreID))
                return;
            AddProduct(objProduct, intToStoreID, cboToStoreID.Text.Trim());
            BindData();
            cboProduct.EditValue=null;
        }

        private bool CheckQuantity()
        {
            dtbAutoSplitProduct.AcceptChanges();
            foreach (DataRow objRow in dtbProductList.Rows)
            {
                decimal decQuanity = Convert.ToDecimal(objRow["SplitQuantity"]);
                decimal decTotalQuantity = Convert.ToDecimal(Globals.IsNull(dtbAutoSplitProduct.Compute("sum(Quantity)", "Select = 1 and ProductID = '" + Convert.ToString(objRow["ProductID"]).Trim() + "'"), 0));
                if (decTotalQuantity > decQuanity)
                {
                    MessageBox.Show(this, "Tổng số lượng chuyển kho của sản phẩm " + Convert.ToString(objRow["ProductName"]).Trim() + " lớn hơn số lượng đặt hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            return true;
        }

        private bool CheckQuantity(string strProductID, string strProductName, int intFromStoreID, string strFromStoreName, decimal decFromInstockQuanity, decimal decQuanityAdd)
        {
            dtbAutoSplitProduct.AcceptChanges();
            decimal decTotalQuantity = Convert.ToDecimal(Library.AppCore.Globals.IsNull(dtbAutoSplitProduct.Compute("sum(Quantity)",
                "Select = 1 and ProductID = '" + strProductID.Trim()+ "'"), 0));
            if (decTotalQuantity + decQuanityAdd > decFromInstockQuanity)
            {
                MessageBox.Show(this, "Tổng số lượng chuyển kho của sản phẩm "
                    + strProductName.Trim()
                    + " \nTại kho " + strFromStoreName
                    + " lớn hơn số lượng tồn" + (decFromInstockQuanity > decTotalQuantity ? (". Số lượng còn lại " + (decFromInstockQuanity - decTotalQuantity)) : ""), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private void btnCreateStoreChangeCommand_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Library.AppCore.LoadControls.C1FlexGridObject.CheckIsSelected(flexListAutoSplitProduct, "Select"))
                {
                    MessageBox.Show(this, "Vui lòng chọn ít nhất một mặt hàng cần chuyển", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (!CheckQuantity())
                    return;

                List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand> lstStoreChangeCommand = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand>();

                int intFromStoreID = intStoreID;
                DataRow[] objRow = dtbAutoSplitProduct.Select("[Select] = true", "ToStoreID");
                int intOldStoreID = 0;
                bool bolIsUrgent = false;
                for (int i = 0; i < objRow.Length; i++)
                {
                    int intToStoreID = Convert.ToInt32(objRow[i]["ToStoreID"]);
                    if (!bolIsUrgent)
                        bolIsUrgent = Convert.ToBoolean(objRow[i]["IsUrgent"]);
                    if (intOldStoreID != intToStoreID)
                    {
                        PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand objStoreChangeCommandBO = new PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand();
                        objStoreChangeCommandBO.FromStoreID = intFromStoreID;
                        objStoreChangeCommandBO.ToStoreID = intToStoreID;
                        objStoreChangeCommandBO.StoreChangeCommandTypeID = intStoreChangeCommandTypeID;
                        objStoreChangeCommandBO.CreatedUser = SystemConfig.objSessionUser.UserName;
                        objStoreChangeCommandBO.CreatedStoreID = SystemConfig.intDefaultStoreID;
                        objStoreChangeCommandBO.StoreChangeCommandTypeID = 1;
                        //objStoreChangeCommandBO.OrderID = strOrderID;
                        objStoreChangeCommandBO.IsUrgent = bolIsUrgent;

                        lstStoreChangeCommand.Add(objStoreChangeCommandBO);
                        intOldStoreID = intToStoreID;
                    }
                    string strProductID = Convert.ToString(objRow[i]["ProductID"]).Trim();
                    decimal decQuantity = Convert.ToDecimal("0" + Convert.ToString(objRow[i]["Quantity"]));
                    if (decQuantity > 0)
                    {
                        List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail> lstStoreChangeCommandDetail = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail>();
                        if (lstStoreChangeCommand[lstStoreChangeCommand.Count - 1].StoreChangeCommandDetailList != null && lstStoreChangeCommand[lstStoreChangeCommand.Count - 1].StoreChangeCommandDetailList.Length > 0)
                        {
                            lstStoreChangeCommandDetail.AddRange(lstStoreChangeCommand[lstStoreChangeCommand.Count - 1].StoreChangeCommandDetailList);
                        }

                        PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail objStoreChangeCommandDetail = new PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommandDetail();
                        objStoreChangeCommandDetail.ProductID = strProductID;
                        objStoreChangeCommandDetail.Quantity = decQuantity;
                        objStoreChangeCommandDetail.CreatedStoreID = SystemConfig.intDefaultStoreID;
                        objStoreChangeCommandDetail.OrderID = strOrderID;

                        lstStoreChangeCommandDetail.Add(objStoreChangeCommandDetail);
                        lstStoreChangeCommand[lstStoreChangeCommand.Count - 1].StoreChangeCommandDetailList = lstStoreChangeCommandDetail.ToArray();
                    }
                }
                btnCreateStoreChangeCommand.Enabled = false;
                objPLCStoreChangeCommand.InsertMulti(lstStoreChangeCommand);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                        SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnCreateStoreChangeCommand.Enabled = true;
                    return;
                }
                MessageBox.Show(this,"Tạo lệnh chuyển kho thành công","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
                btnCreateStoreChangeCommand.Enabled = false;
                flexListAutoSplitProduct.AllowEditing = false;
                btnAdd.Enabled = false;
                bolIsFinish = true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void flexListAutoSplitProduct_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Row < 1) return;
            //Thêm cột Chuyển gấp theo yêu cầu 10/08/2012
            if (e.Col == flexListAutoSplitProduct.Cols["IsUrgent"].Index)
            {
                //Nếu có check Chọn và có quyền "AUTOSPLITPRODUCT_CHECKEMERGENCY" thì cho check Chuyển gấp
                if (Convert.ToBoolean(flexListAutoSplitProduct.Rows[e.Row]["Select"])
                    && SystemConfig.objSessionUser.IsPermission("AUTOSPLITPRODUCT_CHECKEMERGENCY")) { }
                else
                {
                    e.Cancel = true;
                    return;
                }
            }

            if (e.Col == flexListAutoSplitProduct.Cols["Select"].Index)
            {
                if (Convert.ToDecimal(flexListAutoSplitProduct.Rows[e.Row]["Quantity"]) <= 0)
                {
                    e.Cancel = true;
                    return;
                }
            }
        }

        private void flexListAutoSplitProduct_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
              if (e.Row < 1)
                return;
            dtbAutoSplitProduct.AcceptChanges();
            string strProductID = Convert.ToString(flexListAutoSplitProduct[e.Row, "ProductID"]);
            bool bolIsUrgent = false;
          
            if (Convert.ToBoolean(flexListAutoSplitProduct[e.Row, "Select"]))
            {
                if (!Convert.ToBoolean(flexListAutoSplitProduct[e.Row, "IsAllowDecimal"]))
                {
                    flexListAutoSplitProduct[e.Row, "Quantity"] = Math.Round(Convert.ToDecimal(flexListAutoSplitProduct[e.Row, "Quantity"]));
                }
                decimal decInStockQuantity = GetOrderQuantity(strProductID);
                decimal decTotalQuantity = GetTotalQuantity(strProductID);
                decimal decQuantity = Convert.ToDecimal(flexListAutoSplitProduct[e.Row, "Quantity"]);
                if (e.Col == flexListAutoSplitProduct.Cols["Quantity"].Index)
                {
                    if (decQuantity <= 0)
                    {
                        MessageBox.Show(this, "Số lượng phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexListAutoSplitProduct[e.Row, "Quantity"] = decInStockQuantity-decTotalQuantity;
                        if (Convert.ToDecimal(flexListAutoSplitProduct[e.Row, "Quantity"]) <= 0)
                        {
                            flexListAutoSplitProduct[e.Row, "Select"] = false;
                        }
                        return;
                    }
                }

              
                if (!Convert.ToBoolean(flexListAutoSplitProduct[e.Row, "IsAllowDecimal"]))
                {
                    decInStockQuantity = Math.Round(decInStockQuantity);
                    decTotalQuantity = Math.Round(decTotalQuantity);
                }
                decTotalQuantity = decTotalQuantity - decQuantity;
                if (decQuantity > (decInStockQuantity - decTotalQuantity))
                {
                    MessageBox.Show(this, "Số lượng cần chuyển lớn hơn số lượng đặt hàng còn lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flexListAutoSplitProduct[e.Row, "Quantity"] = (decInStockQuantity - decTotalQuantity);
                    if (Convert.ToDecimal(flexListAutoSplitProduct[e.Row, "Quantity"]) <= 0)
                    {
                        flexListAutoSplitProduct[e.Row, "Select"] = false;
                    }
                    return;
                }
            }
        }
    }
}
