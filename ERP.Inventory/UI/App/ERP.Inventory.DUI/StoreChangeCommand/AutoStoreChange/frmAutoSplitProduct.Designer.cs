﻿namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    partial class frmAutoSplitProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAutoSplitProduct));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.chkIsCheckRealInput = new System.Windows.Forms.CheckBox();
            this.txtNumDays = new C1.Win.C1Input.C1NumericEdit();
            this.txtTotalSplitQuantity = new C1.Win.C1Input.C1NumericEdit();
            this.txtTotalQuantity = new C1.Win.C1Input.C1NumericEdit();
            this.btnSplitProduct = new DevExpress.XtraEditors.SimpleButton();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.btnInputQuantity = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cboStoreSearch = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOrderID = new DevExpress.XtraEditors.TextEdit();
            this.btnSelectOrder = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.flexListResult = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.cboAreaIDList = new Library.AppControl.ComboBoxControl.ComboBoxArea();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalSplitQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexListResult)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.groupControl1.Controls.Add(this.cboAreaIDList);
            this.groupControl1.Controls.Add(this.chkIsCheckRealInput);
            this.groupControl1.Controls.Add(this.txtNumDays);
            this.groupControl1.Controls.Add(this.txtTotalSplitQuantity);
            this.groupControl1.Controls.Add(this.txtTotalQuantity);
            this.groupControl1.Controls.Add(this.btnSplitProduct);
            this.groupControl1.Controls.Add(this.btnSearch);
            this.groupControl1.Controls.Add(this.btnInputQuantity);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.label8);
            this.groupControl1.Controls.Add(this.cboStoreSearch);
            this.groupControl1.Controls.Add(this.label5);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.txtOrderID);
            this.groupControl1.Controls.Add(this.btnSelectOrder);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(967, 123);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Thông tin chia hàng";
            // 
            // chkIsCheckRealInput
            // 
            this.chkIsCheckRealInput.AutoSize = true;
            this.chkIsCheckRealInput.Location = new System.Drawing.Point(441, 93);
            this.chkIsCheckRealInput.Name = "chkIsCheckRealInput";
            this.chkIsCheckRealInput.Size = new System.Drawing.Size(170, 20);
            this.chkIsCheckRealInput.TabIndex = 22;
            this.chkIsCheckRealInput.Text = "Bao gồm hàng đi đường";
            this.chkIsCheckRealInput.UseVisualStyleBackColor = true;
            // 
            // txtNumDays
            // 
            this.txtNumDays.CustomFormat = "#,##0";
            this.txtNumDays.DataType = typeof(int);
            this.txtNumDays.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtNumDays.Location = new System.Drawing.Point(128, 60);
            this.txtNumDays.Name = "txtNumDays";
            this.txtNumDays.Size = new System.Drawing.Size(88, 22);
            this.txtNumDays.TabIndex = 15;
            this.txtNumDays.Tag = null;
            this.txtNumDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNumDays.Value = 7;
            this.txtNumDays.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // txtTotalSplitQuantity
            // 
            this.txtTotalSplitQuantity.CustomFormat = "#,###,##0.####";
            this.txtTotalSplitQuantity.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtTotalSplitQuantity.Location = new System.Drawing.Point(818, 58);
            this.txtTotalSplitQuantity.Name = "txtTotalSplitQuantity";
            this.txtTotalSplitQuantity.ReadOnly = true;
            this.txtTotalSplitQuantity.Size = new System.Drawing.Size(88, 22);
            this.txtTotalSplitQuantity.TabIndex = 14;
            this.txtTotalSplitQuantity.Tag = null;
            this.txtTotalSplitQuantity.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTotalSplitQuantity.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // txtTotalQuantity
            // 
            this.txtTotalQuantity.CustomFormat = "#,###,##0.####";
            this.txtTotalQuantity.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtTotalQuantity.Location = new System.Drawing.Point(818, 31);
            this.txtTotalQuantity.Name = "txtTotalQuantity";
            this.txtTotalQuantity.ReadOnly = true;
            this.txtTotalQuantity.Size = new System.Drawing.Size(88, 22);
            this.txtTotalQuantity.TabIndex = 13;
            this.txtTotalQuantity.Tag = null;
            this.txtTotalQuantity.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTotalQuantity.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // btnSplitProduct
            // 
            this.btnSplitProduct.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSplitProduct.Appearance.Options.UseFont = true;
            this.btnSplitProduct.Appearance.Options.UseTextOptions = true;
            this.btnSplitProduct.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.btnSplitProduct.Image = global::ERP.Inventory.DUI.Properties.Resources.arrow_split_090;
            this.btnSplitProduct.Location = new System.Drawing.Point(864, 90);
            this.btnSplitProduct.Name = "btnSplitProduct";
            this.btnSplitProduct.Size = new System.Drawing.Size(91, 25);
            this.btnSplitProduct.TabIndex = 12;
            this.btnSplitProduct.Text = "&Chia hàng ";
            this.btnSplitProduct.Click += new System.EventHandler(this.btnSplitProduct_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Appearance.Options.UseFont = true;
            this.btnSearch.Appearance.Options.UseTextOptions = true;
            this.btnSearch.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.btnSearch.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearch.Location = new System.Drawing.Point(768, 90);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(91, 25);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "&Tìm kiếm ";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnInputQuantity
            // 
            this.btnInputQuantity.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInputQuantity.Appearance.Options.UseFont = true;
            this.btnInputQuantity.Appearance.Options.UseTextOptions = true;
            this.btnInputQuantity.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.btnInputQuantity.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.btnInputQuantity.Location = new System.Drawing.Point(650, 90);
            this.btnInputQuantity.Name = "btnInputQuantity";
            this.btnInputQuantity.Size = new System.Drawing.Size(113, 25);
            this.btnInputQuantity.TabIndex = 12;
            this.btnInputQuantity.Text = "&Nhập số lượng ";
            this.btnInputQuantity.Click += new System.EventHandler(this.btnInputQuantity_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 16);
            this.label1.TabIndex = 11;
            this.label1.Text = "Mã đơn hàng:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 16);
            this.label8.TabIndex = 11;
            this.label8.Text = "Số ngày bán:";
            // 
            // cboStoreSearch
            // 
            this.cboStoreSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreSearch.Location = new System.Drawing.Point(441, 58);
            this.cboStoreSearch.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreSearch.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreSearch.Name = "cboStoreSearch";
            this.cboStoreSearch.Size = new System.Drawing.Size(224, 24);
            this.cboStoreSearch.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(370, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 16);
            this.label5.TabIndex = 9;
            this.label5.Text = "Siêu thị:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(697, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Tổng SL cần chia:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(370, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Khu vực:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(695, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "Tổng SL đặt hàng:";
            // 
            // txtOrderID
            // 
            this.txtOrderID.Location = new System.Drawing.Point(128, 31);
            this.txtOrderID.Name = "txtOrderID";
            this.txtOrderID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrderID.Properties.Appearance.Options.UseFont = true;
            this.txtOrderID.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.Info;
            this.txtOrderID.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtOrderID.Properties.ReadOnly = true;
            this.txtOrderID.Size = new System.Drawing.Size(179, 22);
            this.txtOrderID.TabIndex = 1;
            // 
            // btnSelectOrder
            // 
            this.btnSelectOrder.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectOrder.Appearance.Options.UseFont = true;
            this.btnSelectOrder.Location = new System.Drawing.Point(309, 31);
            this.btnSelectOrder.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSelectOrder.Name = "btnSelectOrder";
            this.btnSelectOrder.Size = new System.Drawing.Size(31, 23);
            this.btnSelectOrder.TabIndex = 0;
            this.btnSelectOrder.Text = "....";
            this.btnSelectOrder.Click += new System.EventHandler(this.btnSelectOrder_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.groupControl2.Controls.Add(this.flexListResult);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 123);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(967, 439);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "Danh sách sản phẩm chia hàng";
            // 
            // flexListResult
            // 
            this.flexListResult.AllowMerging = C1.Win.C1FlexGrid.AllowMergingEnum.Free;
            this.flexListResult.AutoClipboard = true;
            this.flexListResult.ColumnInfo = "9,0,0,0,0,105,Columns:";
            this.flexListResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexListResult.Location = new System.Drawing.Point(2, 22);
            this.flexListResult.Name = "flexListResult";
            this.flexListResult.Rows.Count = 1;
            this.flexListResult.Rows.DefaultSize = 21;
            this.flexListResult.Size = new System.Drawing.Size(963, 415);
            this.flexListResult.StyleInfo = resources.GetString("flexListResult.StyleInfo");
            this.flexListResult.TabIndex = 0;
            this.flexListResult.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            this.flexListResult.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexListResult_BeforeEdit);
            this.flexListResult.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexListResult_AfterEdit);
            // 
            // cboAreaIDList
            // 
            this.cboAreaIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAreaIDList.Location = new System.Drawing.Point(441, 31);
            this.cboAreaIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboAreaIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboAreaIDList.Name = "cboAreaIDList";
            this.cboAreaIDList.Size = new System.Drawing.Size(224, 24);
            this.cboAreaIDList.TabIndex = 5;
            this.cboAreaIDList.SelectionChangeCommitted += new System.EventHandler(this.cboAreaIDList_SelectionChangeCommitted);
            // 
            // frmAutoSplitProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 562);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmAutoSplitProduct";
            this.ShowIcon = false;
            this.Text = "Chia hàng từ một đơn đặt hàng";
            this.Load += new System.EventHandler(this.frmAutoSplitProduct_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalSplitQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexListResult)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private C1.Win.C1FlexGrid.C1FlexGrid flexListResult;
        private DevExpress.XtraEditors.SimpleButton btnSplitProduct;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private DevExpress.XtraEditors.SimpleButton btnInputQuantity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStoreSearch;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit txtOrderID;
        private DevExpress.XtraEditors.SimpleButton btnSelectOrder;
        private C1.Win.C1Input.C1NumericEdit txtTotalSplitQuantity;
        private C1.Win.C1Input.C1NumericEdit txtTotalQuantity;
        private C1.Win.C1Input.C1NumericEdit txtNumDays;
        private System.Windows.Forms.CheckBox chkIsCheckRealInput;
        private Library.AppControl.ComboBoxControl.ComboBoxArea cboAreaIDList;
    }
}