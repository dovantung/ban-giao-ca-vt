﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    public partial class frmAutoSplitProduct : Form
    {
        #region Variables
        private DataTable btbTotal = null;
        private DataTable dtbAutoSplitProduct = null;
        private int intRootStoreID = 0;
        private String strStoreName = "";
        private String strOrderID = "";
        private DataTable dtbStore = null;
        private DataTable dtbStoreView = null;
        private DataTable dtbProduct = null;
        private bool bolIsFinishInputQuantity = false;
        private bool bolIsSearchStatus = false;
        private int intQuantityOld = 0;
        ERP.Inventory.PLC.StoreChangeCommand.PLCAutoSplitProduct objDAutoSplitProduct = new PLC.StoreChangeCommand.PLCAutoSplitProduct();
        #endregion

        #region Constructor
        public frmAutoSplitProduct()
        {
            InitializeComponent();
        }
        #endregion
        #region Methods

        /// <summary>
        /// Nạp combobox
        /// </summary>
        private void LoadCombobox()
        {
            try
            {
                ComboBox cboAreaTemp = new ComboBox();
                Library.AppCore.LoadControls.SetDataSource.SetArea(this,cboAreaTemp,
                    false,Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER);
                DataTable dtbArea = (DataTable)cboAreaTemp.DataSource;
                dtbArea.Rows.RemoveAt(0);
                cboAreaIDList.InitControl(true, dtbArea);

                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER;
                objStoreFilter.OtherCondition = "ISAUTOSTORECHANGE=1";
                objStoreFilter.AreaType = Library.AppCore.Constant.EnumType.AreaType.AREA;
                objStoreFilter.IsCheckPermission = true;
                cboStoreSearch.InitControl(true, objStoreFilter);
                if (cboStoreSearch.DataSource!=null)
                   dtbStore = cboStoreSearch.DataSource.Copy();
            }
            catch (Exception objExc)
            {
                SystemErrorWS.Insert("Lỗi nạp dữ liệu combobox", objExc, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi nạp dữ liệu combobox", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Lấy danh sách kho được chọn để xem báo cáo
        /// </summary>
        /// <param name="bolGetStoreIDList"></param>
        private void GetStoreReportView(bool bolGetStoreIDList)
        {
            String strAreaIDList = cboAreaIDList.AreaIDList.Replace("><", ",").Replace("<", "(").Replace(">", ")");
            String strFilter = "1 = 1";
            if (strAreaIDList.Trim() != string.Empty)
            {
                strFilter += " and AreaID in " + strAreaIDList;
            }
            if (bolGetStoreIDList)
            {
                String strStoreIDList = cboStoreSearch.StoreIDList;
                strStoreIDList = Library.AppCore.Other.ConvertObject.ConvertIDListBracesToParentheses(strStoreIDList);
                if (strStoreIDList.Trim() != string.Empty)
                {
                    strFilter += " and StoreID in " + strStoreIDList;
                }
            }
            strFilter += " and StoreID <> " + intRootStoreID;
            dtbStore.DefaultView.RowFilter = strFilter;
            dtbStoreView = dtbStore.DefaultView.ToTable();
        }

        /// <summary>
        /// Tạo table product
        /// </summary>
        private void InitAutoSplitProductTable()
        {
            dtbAutoSplitProduct = new DataTable();
            dtbAutoSplitProduct.Columns.Add("ProductID", typeof(string));
            dtbAutoSplitProduct.Columns.Add("ProductName", typeof(String));
            dtbAutoSplitProduct.Columns.Add("TotalQuantity", typeof(decimal));
            dtbAutoSplitProduct.Columns.Add("ToStoreID", typeof(int));
            dtbAutoSplitProduct.Columns.Add("ToStoreName", typeof(String));
            dtbAutoSplitProduct.Columns.Add("Quantity", typeof(decimal));
            dtbAutoSplitProduct.Columns.Add("Select", typeof(bool));
            dtbAutoSplitProduct.Columns.Add("IsAdjust", typeof(bool));
            dtbAutoSplitProduct.Columns.Add("IsAllowDecimal", typeof(bool));
        }
        /// <summary>
        /// Định dạng lưới
        /// </summary>
        private void CustomeFlex()
        {
            for (int i = 0; i < flexListResult.Cols.Count; i++)
            {
                flexListResult.Cols[i].AllowSorting =false;
            }
            flexListResult.Cols[0].Visible = false;
            flexListResult.Cols["Total"].Visible = false;
            flexListResult.Cols["AlterProductID"].Visible = false;
            flexListResult.Cols["AlterProductName"].Visible = false;
            flexListResult.Cols["IsAllowDecimal"].Visible = false;

            flexListResult.Cols["ProductID"].Width = 120;
            flexListResult.Cols["ProductName"].Width = 150;
            flexListResult.Cols["Quantity"].Width = 70;
            flexListResult.Cols["SN"].Width = 50;
            flexListResult.Cols["SL"].Width = 90;
            flexListResult.Cols["SplitQuantity"].Width = 70;

            flexListResult.Cols["ProductID"].AllowEditing = false;
            flexListResult.Cols["ProductName"].AllowEditing = false;
            flexListResult.Cols["Quantity"].AllowEditing = false;
            flexListResult.Cols["SN"].AllowEditing = false;
            flexListResult.Cols["SL"].AllowEditing = false;
            flexListResult.Cols["SplitQuantity"].AllowEditing = false;

            Library.AppCore.LoadControls.C1FlexGridObject.SetAllowDragging(flexListResult, false);
            flexListResult.Cols[flexListResult.Cols.Count - 1].ComboList = "...";
            flexListResult.Cols["ProductID"].Caption = "Mã sản phẩm";
            flexListResult.Cols["ProductName"].Caption = "Tên sản phẩm";
            flexListResult.Cols["Quantity"].Caption = "SL đặt hàng";
            flexListResult.Cols["SplitQuantity"].Caption = "SL cần chia";
            flexListResult.Cols[flexListResult.Cols.Count - 1].Caption = "Sản phẩm thay thế";
            flexListResult.Cols["Quantity"].Format = "###,##0.####";
            flexListResult.Cols["SplitQuantity"].Format = "###,##0.####";
            flexListResult.Cols["SN"].Format = "###,##0";
            for (int i = flexListResult.Rows.Fixed; i < flexListResult.Rows.Count; i++)
            {
                if (flexListResult[i, "ProductID"] == null || Convert.ToString(flexListResult[i, "ProductID"]).Trim() == string.Empty)
                    flexListResult.Rows[i].Visible = false;
            }
            for (int k = 6; k < flexListResult.Cols.Count - 3; k++)
            {
                flexListResult.Cols[k].DataType = typeof(decimal);
                flexListResult.Cols[k].Width = 70;
                flexListResult.Cols[k].AllowDragging = false;
                Library.AppCore.LoadControls.C1FlexGridObject.SetColumnDigit(flexListResult, 16, 4, flexListResult.Cols[k].Name);
            }

            flexListResult.Cols["ProductID"].AllowMerging = true;
            flexListResult.Cols["ProductID"].Visible= true;
            flexListResult.Cols["ProductName"].AllowMerging = true;
            flexListResult.Cols["Quantity"].AllowMerging = true;
            flexListResult.Cols["SplitQuantity"].AllowMerging = true;
            flexListResult.Cols["SN"].AllowMerging = true;
            C1.Win.C1FlexGrid.CellStyle style = flexListResult.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = flexListResult.GetCellRange(0, 0, 0, flexListResult.Cols.Count - 1);
            range.Style = style;
            flexListResult.Rows[0].Height = 45;
         
        }

        private void FormatFlexInputQuantity()
        {
            try
            {
                flexListResult.Cols[0].Visible = false;
                flexListResult.Cols["REMAINQUANTITY"].Visible = false;
                flexListResult.Cols["ISALLOWDECIMAL"].Visible = false;
                flexListResult.Cols["ProductID"].Width = 120;
                flexListResult.Cols["ProductName"].Width = 150;
                flexListResult.Cols["Quantity"].Width = 70;
                flexListResult.Cols["SplitQuantity"].Width = 70;
                for (int i = 0; i < flexListResult.Cols.Count; i++)
                {
                    flexListResult.Cols[i].AllowDragging = false;
                }
                flexListResult.Cols["ProductID"].AllowEditing = false;
                flexListResult.Cols["ProductID"].Visible = true;
                flexListResult.Cols["ProductName"].AllowEditing = false;
                flexListResult.Cols["Quantity"].AllowEditing = false;
                flexListResult.Cols["SplitQuantity"].AllowEditing = true;
                flexListResult.Cols["ProductID"].Caption = "Mã sản phẩm";
                flexListResult.Cols["ProductName"].Caption = "Tên sản phẩm";
                flexListResult.Cols["Quantity"].Caption = "SL đặt hàng";
                flexListResult.Cols["SplitQuantity"].Caption = "SL cần chia";
                flexListResult.Cols["Quantity"].Format = "###,##0.####";
                flexListResult.Cols["SplitQuantity"].Format = "###,##0.####";
                C1.Win.C1FlexGrid.CellStyle style = flexListResult.Styles.Add("flexStyle");
                style.WordWrap = true;
                style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8, FontStyle.Bold);
                style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
                C1.Win.C1FlexGrid.CellRange range = flexListResult.GetCellRange(0, 0, 0, flexListResult.Cols.Count - 1);
                range.Style = style;
                flexListResult.Rows[0].Height = 40;
            }
            catch (Exception objExc)
            {
                SystemErrorWS.Insert("Lỗi định dạng lưới dữ liệu", objExc, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi định dạng lưới dữ liệu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private DataTable CreateTotalTable(DataTable dtbProduct, DataTable dtbAverageSale, DataTable dtbInstock)
        {
            DataTable tblResult = new DataTable();
            try
            {
                tblResult.Columns.Add("ProductID", typeof(string));
                tblResult.Columns.Add("ProductName", typeof(String));
                tblResult.Columns.Add("Quantity", typeof(decimal));
                tblResult.Columns.Add("SplitQuantity", typeof(decimal));
                tblResult.Columns.Add("SN", typeof(int));
                tblResult.Columns.Add("SL", typeof(String));
                for (int i = 0; i < dtbStoreView.Rows.Count; i++)
                {
                    try
                    {
                        tblResult.Columns.Add(Convert.ToString(dtbStoreView.Rows[i]["StoreShortName"]).Trim(), typeof(decimal));
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }

                }
                tblResult.Columns.Add("Total", typeof(decimal));
                tblResult.Columns.Add("AlterProductID", typeof(string));
                tblResult.Columns.Add("AlterProductName", typeof(string));
                tblResult.Columns.Add("IsAllowDecimal", typeof(bool));
                int intStartIndexColumn = tblResult.Columns["SL"].Ordinal + 1;
                for (int i = 0; i < dtbProduct.Rows.Count; i++)
                {
                    string strProductID = Convert.ToString(dtbProduct.Rows[i]["ProductID"]).Trim();
                    String strProductName = Convert.ToString(dtbProduct.Rows[i]["ProductName"]).Trim();
                    decimal decQuantity = Convert.ToDecimal(dtbProduct.Rows[i]["Quantity"]);
                    decimal decSplitQuantity = Convert.ToDecimal(dtbProduct.Rows[i]["SplitQuantity"]);

                    //Row số lượng bán TB
                    DataRow drAverageSale = tblResult.NewRow();
                    drAverageSale["ProductID"] = strProductID;
                    drAverageSale["ProductName"] = strProductName;
                    drAverageSale["Quantity"] = decQuantity;
                    drAverageSale["SplitQuantity"] = decSplitQuantity;
                    drAverageSale["IsAllowDecimal"] = dtbProduct.Rows[i]["IsAllowDecimal"];
                    drAverageSale["SN"] = 0;
                    drAverageSale["SL"] = "SL bán TB";
                    drAverageSale["AlterProductID"] = "";
                    drAverageSale["AlterProductName"] = "";

                    for (int j = 0; j < dtbStoreView.Rows.Count; j++)
                    {
                        int intStoreID = Convert.ToInt32(dtbStoreView.Rows[j]["StoreID"]);

                        //Fill vào row số lượng bán TB
                        DataRow[] objRow = dtbAverageSale.Select("ProductID = '" + strProductID + "' and StoreID = " + intStoreID.ToString());
                        if (objRow.Length > 0)
                            drAverageSale[j + intStartIndexColumn] = Convert.ToDecimal(objRow[0][2]);
                        else drAverageSale[j + intStartIndexColumn] = 0;

                    }

                    //Tính tổng row Số lượng bán TB
                    CountSumTotalRow(drAverageSale, intStartIndexColumn, tblResult.Columns["Total"].Ordinal - 1);

                    //Row số lượng tồn kho
                    DataRow drInstock = tblResult.NewRow();
                    drInstock["ProductID"] = strProductID;
                    drInstock["ProductName"] = strProductName;
                    drInstock["Quantity"] = decQuantity;
                    drInstock["SplitQuantity"] = decSplitQuantity;
                    drInstock["SN"] = 0;
                    drInstock["SL"] = "SL tồn";
                    drInstock["AlterProductID"] = "";
                    drInstock["AlterProductName"] = "";
                    drInstock["IsAllowDecimal"] = dtbProduct.Rows[i]["IsAllowDecimal"];
                    for (int j = 0; j < dtbStoreView.Rows.Count; j++)
                    {
                        int intStoreID = Convert.ToInt32(dtbStoreView.Rows[j]["StoreID"]);
                        //Fill vào row số lượng bán TB
                        DataRow[] objRow = dtbInstock.Select("ProductID = '" + strProductID + "' and StoreID = " + intStoreID.ToString());
                        if (objRow.Length > 0)
                            drInstock[j + intStartIndexColumn] = Convert.ToDecimal(objRow[0][2]);
                        else drInstock[j + intStartIndexColumn] = 0;
                    }
                    //Tính tổng row tồn kho
                    CountSumTotalRow(drInstock, intStartIndexColumn, tblResult.Columns["Total"].Ordinal - 1);
                    decimal decTotalAverageSale = Convert.ToDecimal(drAverageSale["Total"]);
                    decimal decNumDays = 0;
                    if (decTotalAverageSale > 0)
                        decNumDays = (Convert.ToInt32(drInstock["Total"]) + decSplitQuantity) / decTotalAverageSale;
                    drInstock["SN"] = decNumDays;
                    drAverageSale["SN"] = decNumDays;
                    DataRow drSales = tblResult.NewRow();
                    drSales["ProductID"] = strProductID;
                    drSales["ProductName"] = strProductName;
                    drSales["Quantity"] = decQuantity;
                    drSales["SplitQuantity"] = decSplitQuantity;
                    drSales["SN"] = decNumDays;
                    drSales["SL"] = "SL cần bán";
                    drSales["AlterProductID"] = "";
                    drSales["AlterProductName"] = "";
                    drSales["IsAllowDecimal"] = dtbProduct.Rows[i]["IsAllowDecimal"];
                    for (int j = intStartIndexColumn; j < tblResult.Columns["Total"].Ordinal; j++)
                    {
                        drSales[j] = Math.Round(Convert.ToDecimal(drAverageSale[j]) * decNumDays,2);
                        if (!Convert.ToBoolean(drSales["IsAllowDecimal"]))
                        {
                            drSales[j] = Convert.ToInt32(drSales[j]);
                        }
                    }
                    //Tính tổng row SL cần bán
                    CountSumTotalRow(drSales, intStartIndexColumn, tblResult.Columns["Total"].Ordinal - 1);
                    DataRow drStoreChange = tblResult.NewRow();
                    drStoreChange["ProductID"] = strProductID;
                    drStoreChange["ProductName"] = strProductName;
                    drStoreChange["Quantity"] = decQuantity;
                    drStoreChange["SplitQuantity"] = decSplitQuantity;
                    drStoreChange["SN"] = decNumDays;
                    drStoreChange["SL"] = "SL cần chuyển";
                    drStoreChange["AlterProductID"] = "";
                    drStoreChange["AlterProductName"] = "";
                    drStoreChange["IsAllowDecimal"] = dtbProduct.Rows[i]["IsAllowDecimal"];
                    for (int j = intStartIndexColumn; j < tblResult.Columns["Total"].Ordinal; j++)
                        drStoreChange[j] = Convert.ToDecimal(drSales[j]) - Convert.ToDecimal(drInstock[j]);
                    //Tính tổng row tồn kho
                    CountSumTotalRow(drStoreChange, intStartIndexColumn, tblResult.Columns["Total"].Ordinal - 1);
                    decimal decTotalStoreChange = Math.Round(Convert.ToDecimal(drStoreChange["Total"]),4);
                    for (int j = intStartIndexColumn; j < tblResult.Columns["Total"].Ordinal; j++)
                    {
                        if (Convert.ToDecimal(drStoreChange[j]) > 0 && decTotalStoreChange > 0)
                        {
                            drStoreChange[j] = Math.Round(Convert.ToDecimal(decSplitQuantity * Convert.ToDecimal(drStoreChange[j]) / decTotalStoreChange),4);
                            if (!Convert.ToBoolean(drStoreChange["IsAllowDecimal"]))
                            {
                                drStoreChange[j] = Convert.ToInt32(drStoreChange[j]);
                            }
                        }
                        else
                            drStoreChange[j] = 0;
                    }
                    tblResult.Rows.Add(drAverageSale);
                    tblResult.Rows.Add(drInstock);
                    tblResult.Rows.Add(drSales);
                    tblResult.Rows.Add(drStoreChange);
                }

            }
            catch (Exception objExc)
            {
                SystemErrorWS.Insert("Lỗi nạp dữ liệu chia hàng từ đơn hàng", objExc, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi nạp dữ liệu chia hàng từ đơn hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
            return tblResult;
        }

        private void ReCalSplitProduct(String strProductID, decimal decTotalQuantity, decimal decTotalSplit)
        {

            int intStartIndexColumn = dtbAutoSplitProduct.Columns["Quantity"].Ordinal;
            if (decTotalQuantity < decTotalSplit)
            {
                while (decTotalQuantity != decTotalSplit)
                {
                    int intMaxRow = FindMaxRow(dtbAutoSplitProduct, strProductID, intStartIndexColumn);
                    if (intMaxRow < 0) return;
                    decimal decNewQuantity = Convert.ToDecimal(dtbAutoSplitProduct.Rows[intMaxRow][intStartIndexColumn]) - 1;
                    if (decNewQuantity > 0)
                        dtbAutoSplitProduct.Rows[intMaxRow][intStartIndexColumn] = decNewQuantity;
                    else dtbAutoSplitProduct.Rows.RemoveAt(intMaxRow);
                    decTotalSplit--;
                }
            }
            else
            {
                while (decTotalQuantity != decTotalSplit)
                {
                    int intMinRow = FindMinRow(dtbAutoSplitProduct, strProductID, intStartIndexColumn);
                    if (intMinRow < 0) return;
                    decimal decNewQuantity = Convert.ToDecimal(dtbAutoSplitProduct.Rows[intMinRow][intStartIndexColumn]) + 1;
                    dtbAutoSplitProduct.Rows[intMinRow][intStartIndexColumn] = decNewQuantity;
                    decTotalSplit++;
                }
            }
        }

        private int FindMaxRow(DataTable tblIn, string strProductID, int intCol)
        {
            if (tblIn.Rows.Count < 1) return -1;
            int intResult = -1;
            int intMaxValue = 0;
            for (int i = 0; i < tblIn.Rows.Count; i++)
            {
                int intCurrentValue = Convert.ToInt32(tblIn.Rows[i][intCol]);
                string strCurrentProductID = Convert.ToString(tblIn.Rows[i]["ProductID"]).Trim();
                if (intCurrentValue > intMaxValue && strCurrentProductID == strProductID)
                {
                    intMaxValue = intCurrentValue;
                    intResult = i;
                }
            }

            return intResult;
        }

        private int FindMinRow(DataTable tblIn, string strProductID, int intCol)
        {
            if (tblIn.Rows.Count < 1) return -1;
            int intResult = -1;
            int intMinValue = 1000000;
            for (int i = 1; i < tblIn.Rows.Count; i++)
            {
                int intCurrentValue = Convert.ToInt32(tblIn.Rows[i][intCol]);
                string strCurrentProductID = Convert.ToString(tblIn.Rows[i]["ProductID"]).Trim();
                if (intCurrentValue < intMinValue && strCurrentProductID == strProductID)
                {
                    intMinValue = intCurrentValue;
                    intResult = i;
                }
            }

            return intResult;
        }

        private void SetWarningRow(String strProductID)
        {
            for (int i = 0; i < dtbAutoSplitProduct.Rows.Count; i++)
            {
                String strCurrentProductID = Convert.ToString(dtbAutoSplitProduct.Rows[i]["ProductID"]).Trim();
                if (strProductID == strCurrentProductID)
                    dtbAutoSplitProduct.Rows[i]["IsAdjust"] = 1;
            }
        }

        private void CountSumTotalRow(DataRow objRow, int intStartIndexColumn, int intEndIndexColumn)
        {

            decimal decResult = 0;
            for (int i = intStartIndexColumn; i <= intEndIndexColumn; i++)
                if (Convert.ToDecimal(objRow[i]) > 0)
                    decResult += Convert.ToDecimal(objRow[i]);
            objRow["Total"] = decResult;
        }

        /// <summary>
        /// Kiểm tra dữ liệu
        /// </summary>
        /// <param name="bolIsInput"></param>
        /// <returns></returns>
        private bool CheckInput(bool bolIsInput)
        {
            
            if (bolIsInput)
            {
                if (txtOrderID.Text.Trim().Length == 0)
                {
                    MessageBox.Show(this, "Vui lòng nhập mã đơn đặt hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtOrderID.Focus();
                    return false;
                }

                int intReviewStatus = objDAutoSplitProduct.CheckOrderReviewStatus(txtOrderID.Text.Trim());
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this,SystemConfig.objSessionUser.ResultMessageApp.Message,SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                if (intReviewStatus == -2)
                {
                    MessageBox.Show(this, "Lỗi kiểm tra đơn đặt hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

                if (intReviewStatus == -1)
                {
                    MessageBox.Show(this, "Mã đơn đặt hàng này không tồn tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtOrderID.Focus();
                    return false;
                }

                if (intReviewStatus == 0)
                {
                    MessageBox.Show(this, "Đơn đặt hàng này chưa được duyệt", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtOrderID.Focus();
                    return false;
                }

            }
            else
            {
                if (cboAreaIDList.AreaIDList == string.Empty)
                {
                    MessageBox.Show(this, "Vui lòng chọn khu vực", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboAreaIDList.Focus();
                    return false;
                }

                if (Convert.ToInt32(txtNumDays.Value) < 1)
                {
                    MessageBox.Show(this, "Số ngày bán phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtNumDays.Focus();
                    return false;
                }

                if (Convert.ToInt32(txtNumDays.Value) > 365)
                {
                    MessageBox.Show(this, "Số ngày bán quá lớn", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtNumDays.Focus();
                    return false;
                }
                if (dtbProduct == null)
                {
                    MessageBox.Show(this, "Chưa khởi tạo danh sách sản phẩm của đơn hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtOrderID.Focus();
                    return false;
                }
            }
            return true;
        }

        private DataTable LoadProductList()
        {
            DataTable tblResult = null;
            tblResult = objDAutoSplitProduct.GetProductListByOrderID(txtOrderID.Text.Trim());

            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return null;
            }
            return tblResult;
        }

        private int GetStoreID(int intCol)
        {
            string strStoreShortName = btbTotal.Columns[intCol].ColumnName.Trim();
            DataRow[] objRow = dtbStoreView.Select("StoreShortName = '" + strStoreShortName + "'");
            if (objRow.Length > 0)
                return Convert.ToInt32(objRow[0]["StoreID"]);
            return 0;
        }

        private String GetStoreName(int intStoreID)
        {
            DataRow[] objRow = dtbStoreView.Select("StoreID = " + intStoreID.ToString());
            if (objRow.Length > 0)
                return Convert.ToString(objRow[0]["StoreName"]);
            return "";
        }

        private bool InputQuantity()
        {
            try
            {
                if (!CheckInput(true))
                {
                    btnInputQuantity.Enabled = true;
                    return false;
                }
                dtbProduct = LoadProductList();
                if (dtbProduct == null)
                {
                    MessageBox.Show(this, "Lỗi đọc danh sách sản phẩm của đơn hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTotalQuantity.Value=0;
                    txtTotalSplitQuantity.Value = 0;
                    btnSelectOrder.Focus();
                    flexListResult.DataSource = null;
                    return false;
                }

                if (dtbProduct.Rows.Count == 0)
                {
                    MessageBox.Show(this, "Đơn hàng này đã được chia hết", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTotalQuantity.Value= 0;
                    txtTotalSplitQuantity.Value = 0;
                    btnSelectOrder.Focus();
                    flexListResult.DataSource = dtbProduct;
                    FormatFlexInputQuantity();
                    return false;
                }
                bolIsFinishInputQuantity = true;
                txtTotalQuantity.Value= Convert.ToDecimal(dtbProduct.Compute("Sum(Quantity)", string.Empty));
                txtTotalSplitQuantity.Value = Convert.ToDecimal(dtbProduct.Compute("Sum(SplitQuantity)", string.Empty));
            }
            catch (Exception objExc)
            {
                SystemErrorWS.Insert("Lỗi nạp danh sách sản phẩm chia hàng từ một đơn hàng", objExc.ToString(), "frmAutoSplitProduct -> InputQuantity", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private DataTable LoadAverageSale()
        {
            DataTable tblResult = null;

            tblResult = objDAutoSplitProduct.GetAverageSale(txtOrderID.Text.Trim(), Convert.ToInt32(txtNumDays.Text),
                cboAreaIDList.AreaIDList,
                cboStoreSearch.StoreIDList);

            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return null;
            }

            return tblResult;
        }

        private DataTable LoadInStockTable()
        {
            DataTable tblResult = null;
            
            tblResult = objDAutoSplitProduct.GetInStockByOrder(txtOrderID.Text.Trim(),
                cboAreaIDList.AreaIDList,
                cboStoreSearch.StoreIDList, chkIsCheckRealInput.Checked == false ? 1 : -1);

            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.LoadControls.MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message,
                    SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return null;
            }

            return tblResult;
        }
        #endregion
        #region Events
        private void frmAutoSplitProduct_Load(object sender, EventArgs e)
        {
            LoadCombobox();
            InitAutoSplitProductTable();
            btnSearch.Enabled = false;
        }

        private void btnSelectOrder_Click(object sender, EventArgs e)
        {
            ERP.Procurement.DUI.frmOrderSearch frmSearchOrder = new Procurement.DUI.frmOrderSearch();
            frmSearchOrder.bolIsSearchForm = true;
            frmSearchOrder.ShowDialog();
            if (frmSearchOrder.strOrderID.Length > 0)
            {
                txtOrderID.Text = frmSearchOrder.strOrderID;
                intRootStoreID = frmSearchOrder.intInputStoreID;
                strStoreName = frmSearchOrder.intInputStoreID + " - " + frmSearchOrder.strInputStoreName;
                //string strAreaID = frmSearchOrder.intAreaID.ToString();
                cboAreaIDList.SetValue(frmSearchOrder.intAreaID);
                cboAreaIDList_SelectionChangeCommitted(null, null);

                strOrderID = frmSearchOrder.strOrderID;
                dtbProduct = null;
                btnInputQuantity.Enabled = true;
                btnSearch.Enabled = true;
                bolIsFinishInputQuantity = false;
                bolIsSearchStatus = false;
            }
        }
        private void btnInputQuantity_Click(object sender, EventArgs e)
        {
            if (!InputQuantity())
            {
                btnSearch.Enabled = false;
                return;
            }
            bolIsSearchStatus = false;
            btnSplitProduct.Enabled = false;
            flexListResult.DataSource = dtbProduct;
            FormatFlexInputQuantity();
            btnSearch.Enabled = true;
        }

        private void cboAreaIDList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
                //objStoreFilter.StorePermissionType = Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER;
                objStoreFilter.AreaType = Library.AppCore.Constant.EnumType.AreaType.AREA;
                objStoreFilter.AreaIDList = cboAreaIDList.AreaIDList;
                objStoreFilter.IsCheckPermission = true;
                //objStoreFilter.OtherCondition = "ISACTIVE = 1 ";//AND ISSALESTORE = 1";
                cboStoreSearch.InitControl(true, objStoreFilter);
                GetStoreReportView(false);
            }
            catch (Exception objExc)
            {
                SystemErrorWS.Insert("Lỗi nạp dữ liệu combobox", objExc, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi nạp dữ liệu combobox", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            if (!bolIsFinishInputQuantity)
            {
                if (!InputQuantity())
                {
                    btnSearch.Enabled = false;
                    btnSplitProduct.Enabled = false;
                    return;
                }
            }
            if (!CheckInput(false))
            {
                btnSearch.Enabled = true;
                return;
            }
            bolIsSearchStatus = true;
            btnSelectOrder.Enabled = false;
            GetStoreReportView(true);
            DataTable dtbAverageSale = LoadAverageSale();
            if (dtbAverageSale == null)
            {
                MessageBox.Show(this, "Lỗi đọc thông tin số lượng bán bình quân", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            DataTable dtbInstock = LoadInStockTable();
            if (dtbInstock == null)
            {
                MessageBox.Show(this, "Lỗi đọc thông tin tồn kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            for (int i = dtbProduct.Rows.Count - 1; i >= 0; i--)
            {
                if (Convert.ToDecimal(dtbProduct.Rows[i]["SPLITQUANTITY"]) <= 0)
                {
                    dtbProduct.Rows.RemoveAt(i);
                }
            }

            btbTotal = CreateTotalTable(dtbProduct, dtbAverageSale, dtbInstock);
            DataTable dtbTotalTemp = dtbStore.Clone();
            if (btbTotal != null)
            {
                if (btbTotal.Rows.Count > 0)
                {
                    string strProductID=string.Empty;
                    for (int i = 0; i < btbTotal.Rows.Count-1; i++)
                    {
                        if (i == 0)
                            strProductID = Convert.ToString(btbTotal.Rows[i]["ProductID"]);
                        else
                        {
                            if (strProductID != Convert.ToString(btbTotal.Rows[i]["ProductID"]))
                            {
                                DataRow row = btbTotal.NewRow();
                                btbTotal.Rows.InsertAt(row, i);
                                i++;
                                if (i < btbTotal.Rows.Count - 1)
                                    strProductID = Convert.ToString(btbTotal.Rows[i]["ProductID"]);
                            }

                        }
                    }
                }
                flexListResult.DataSource = btbTotal;
                CustomeFlex();
                btnSplitProduct.Enabled = true;
            }
            btnSelectOrder.Enabled = true;
            btnSearch.Enabled = true;
        }

        private void btnSplitProduct_Click(object sender, EventArgs e)
        {
            try
            {
                int intStartIndexColumn = 6;
                dtbAutoSplitProduct.Clear();
                btnSearch.Enabled = false;
                btnSplitProduct.Enabled = false;
                for (int i = 3; i < btbTotal.Rows.Count; i += 5)
                {
                    String strProductID = Convert.ToString(btbTotal.Rows[i]["ProductID"]).Trim();
                    String strProductName = Convert.ToString(btbTotal.Rows[i]["ProductName"]).Trim();
                    bool bolIsAllowDecimal = Convert.ToBoolean(btbTotal.Rows[i]["IsAllowDecimal"]);
                    decimal decTotalQuantity = Convert.ToDecimal(btbTotal.Rows[i]["SplitQuantity"]);
                    decimal decTotalSplitQuantity = 0;
                    for (int k = intStartIndexColumn; k < btbTotal.Columns["Total"].Ordinal; k++)
                    {
                        decimal decQuantity = Convert.ToDecimal(btbTotal.Rows[i][k]);
                        if (decQuantity > 0)
                        {
                            int intToStoreID = GetStoreID(k);
                            if (intToStoreID != intRootStoreID)
                            {
                                ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                                if (objProduct == null)
                                {
                                    MessageBox.Show(this,"Sản phẩm không tồn tại!","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                                    return;
                                }
                                if (ERP.MasterData.PLC.MD.PLCStore.CheckMainGroupPermission(intToStoreID, objProduct.MainGroupID,Library.AppCore.Constant.EnumType.StoreMainGroupPermissionType.STORECHANGEINPUT))
                                {
                                    decTotalSplitQuantity += decQuantity;
                                    String strToStoreName = intToStoreID+" - "+GetStoreName(intToStoreID);
                                    DataRow objRow = dtbAutoSplitProduct.NewRow();
                                    objRow.ItemArray = new object[] { strProductID, strProductName, decTotalQuantity, 
                                        intToStoreID, strToStoreName, decQuantity, true, false, bolIsAllowDecimal};
                                    dtbAutoSplitProduct.Rows.Add(objRow);
                                }
                            }
                        }

                    }
                    if (decTotalQuantity != decTotalSplitQuantity && !bolIsAllowDecimal)
                    {
                        ReCalSplitProduct(strProductID, decTotalQuantity, decTotalSplitQuantity);
                        if (decTotalQuantity < 11)
                            SetWarningRow(strProductID);
                    }
                }
                frmAutoSplitProductDetail frmAutoSplitProductDetail1 = new frmAutoSplitProductDetail();
                frmAutoSplitProductDetail1.dtbStore = dtbStoreView.Copy();
                frmAutoSplitProductDetail1.strOrderID = strOrderID;
                frmAutoSplitProductDetail1.intStoreID = intRootStoreID;
                frmAutoSplitProductDetail1.strStoreName = strStoreName;
                frmAutoSplitProductDetail1.decTotalQuantity = Convert.ToDecimal(txtTotalSplitQuantity.Value);
                frmAutoSplitProductDetail1.dtbAutoSplitProduct = dtbAutoSplitProduct;
                frmAutoSplitProductDetail1.dtbProductList = dtbProduct;
                frmAutoSplitProductDetail1.ShowDialog();
                btnSearch.Enabled = !frmAutoSplitProductDetail1.bolIsFinish;
                btnSplitProduct.Enabled = !frmAutoSplitProductDetail1.bolIsFinish;
            }
            catch (Exception objExc)
            {
                SystemErrorWS.Insert("Lỗi chia hàng từ đơn hàng", objExc, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi chia hàng từ đơn hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion

        private void flexListResult_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            intQuantityOld = 0;
            if (bolIsSearchStatus)
            {
                e.Cancel = true;
                return;
            }
            //if (flexListResult.Cols.Contains("SL"))
            //{
            //    if (flexListResult[e.Row, "SL"].ToString().ToUpper() != "SL CẦN CHUYỂN")
            //    {
            //        e.Cancel = true;
            //        return;
            //    }
            //    else
            //    {
            //        intQuantityOld = Convert.ToInt32(flexListResult[e.Row, e.Col]);
            //    }
            //}
        }

        private void flexListResult_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Row < 1)
                return;
            
            if (flexListResult[e.Row, e.Col] == DBNull.Value
                || Convert.ToDecimal(flexListResult[e.Row, e.Col]) < 0)
                flexListResult[e.Row, e.Col] = 0;
            if (!Convert.ToBoolean(flexListResult[e.Row, "IsAllowDecimal"]))
            {
                flexListResult[e.Row, e.Col] = Math.Round(Convert.ToDecimal(flexListResult[e.Row, e.Col]));
            }
            
            if (!bolIsSearchStatus)
            {
                if (Convert.ToDecimal(flexListResult[e.Row, "SplitQuantity"])
                   > Convert.ToDecimal(flexListResult[e.Row, "Quantity"]))
                {
                    MessageBox.Show(this, "Số lượng cần chia phải nhỏ hơn số lượng đặt hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    flexListResult[e.Row, "SplitQuantity"] = intQuantityOld; //flexListResult[e.Row, "Quantity"];
                }
                if (Convert.ToDecimal(flexListResult[e.Row, "SplitQuantity"]) < 0)
                    flexListResult[e.Row, "SplitQuantity"] = 0;
                if (!Convert.ToBoolean(flexListResult[e.Row, "IsAllowDecimal"]))
                {
                    flexListResult[e.Row, "SplitQuantity"] = Convert.ToInt32(flexListResult[e.Row, "SplitQuantity"]);
                }
                flexListResult[e.Row, "REMAINQUANTITY"] = flexListResult[e.Row, "SplitQuantity"];
                txtTotalSplitQuantity.Value = Convert.ToDecimal(dtbProduct.Compute("Sum(SplitQuantity)", string.Empty));
            }
            else
            {
                if (Convert.ToDecimal(flexListResult[e.Row, e.Col])
                   > Convert.ToDecimal(flexListResult[e.Row, "SplitQuantity"]))
                {
                    flexListResult[e.Row, e.Col] = intQuantityOld;
                    MessageBox.Show(this, "Số lượng tại kho phải nhỏ hơn số lượng cần chia", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //flexListResult[e.Row, e.Col] = flexListResult[e.Row, "SplitQuantity"];
                }
                decimal decTotalQuantityStore = 0;
                for (int i = 0; i < dtbStoreView.Rows.Count; i++)
                {
                    decTotalQuantityStore += Convert.ToDecimal(flexListResult[e.Row, dtbStoreView.Rows[i]["StoreShortName"].ToString().Trim()]);
                }
                if (decTotalQuantityStore > Convert.ToDecimal(flexListResult[e.Row, "SplitQuantity"]))
                {
                    flexListResult[e.Row, e.Col] = intQuantityOld;
                    MessageBox.Show(this, "Tổng số lượng cần chia của sản phẩm tại các kho phải nhỏ hơn hoặc bằng số lượng cần chia",
                        "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        
    }
}
