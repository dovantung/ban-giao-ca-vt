﻿namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    partial class frmAutoDistributeInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtHoldingQuantity = new C1.Win.C1Input.C1NumericEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.btnSearch = new System.Windows.Forms.Button();
            this.chkIsCheckRealInput = new System.Windows.Forms.CheckBox();
            this.cboIsShowProduct = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboProduct = new ERP.MasterData.DUI.CustomControl.Product.cboProductList();
            this.cboBrand = new Library.AppControl.ComboBoxControl.ComboBoxBrand();
            this.cboSubGroup = new Library.AppControl.ComboBoxControl.ComboBoxSubGroup();
            this.cboMainGroup = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.cboStore = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.cboCenterStore = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.repDistributeQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repTransferQuantity = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chkShortView = new System.Windows.Forms.CheckBox();
            this.btnReceive = new System.Windows.Forms.Button();
            this.btnTransfer = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoldingQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDistributeQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTransferQuantity)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtHoldingQuantity);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.dtpToDate);
            this.panel1.Controls.Add(this.dtpFromDate);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.chkIsCheckRealInput);
            this.panel1.Controls.Add(this.cboIsShowProduct);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cboProduct);
            this.panel1.Controls.Add(this.cboBrand);
            this.panel1.Controls.Add(this.cboSubGroup);
            this.panel1.Controls.Add(this.cboMainGroup);
            this.panel1.Controls.Add(this.cboStore);
            this.panel1.Controls.Add(this.cboCenterStore);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(988, 87);
            this.panel1.TabIndex = 0;
            // 
            // txtHoldingQuantity
            // 
            this.txtHoldingQuantity.CustomFormat = "#,##0";
            this.txtHoldingQuantity.DataType = typeof(int);
            this.txtHoldingQuantity.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat;
            this.txtHoldingQuantity.Location = new System.Drawing.Point(374, 59);
            this.txtHoldingQuantity.Name = "txtHoldingQuantity";
            this.txtHoldingQuantity.Size = new System.Drawing.Size(48, 22);
            this.txtHoldingQuantity.TabIndex = 15;
            this.txtHoldingQuantity.Tag = null;
            this.txtHoldingQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHoldingQuantity.Value = 1;
            this.txtHoldingQuantity.VisibleButtons = C1.Win.C1Input.DropDownControlButtonFlags.None;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(308, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 16);
            this.label10.TabIndex = 14;
            this.label10.Text = "CH giữ lại:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(767, 62);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 16);
            this.label9.TabIndex = 19;
            this.label9.Text = "-";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(779, 59);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(91, 22);
            this.dtpToDate.TabIndex = 20;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(674, 59);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(91, 22);
            this.dtpFromDate.TabIndex = 18;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(877, 58);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(88, 25);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // chkIsCheckRealInput
            // 
            this.chkIsCheckRealInput.AutoSize = true;
            this.chkIsCheckRealInput.Location = new System.Drawing.Point(441, 60);
            this.chkIsCheckRealInput.Name = "chkIsCheckRealInput";
            this.chkIsCheckRealInput.Size = new System.Drawing.Size(129, 20);
            this.chkIsCheckRealInput.TabIndex = 16;
            this.chkIsCheckRealInput.Text = "Gồm cả đi đường";
            this.chkIsCheckRealInput.UseVisualStyleBackColor = true;
            // 
            // cboIsShowProduct
            // 
            this.cboIsShowProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIsShowProduct.DropDownWidth = 210;
            this.cboIsShowProduct.FormattingEnabled = true;
            this.cboIsShowProduct.Items.AddRange(new object[] {
            "--Tất cả--",
            "Không hiển thị hàng trưng bày",
            "Chỉ hiển thị hàng trưng bày"});
            this.cboIsShowProduct.Location = new System.Drawing.Point(96, 58);
            this.cboIsShowProduct.Name = "cboIsShowProduct";
            this.cboIsShowProduct.Size = new System.Drawing.Size(196, 24);
            this.cboIsShowProduct.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(587, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ngành hàng:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(308, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Siêu thị:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(587, 62);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 16);
            this.label8.TabIndex = 17;
            this.label8.Text = "Ngày bán:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(587, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "Sản phẩm:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(308, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "NSX:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 16);
            this.label7.TabIndex = 12;
            this.label7.Text = "Trưng bày:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Nhóm hàng:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kho trung tâm:";
            // 
            // cboProduct
            // 
            this.cboProduct.BackColor = System.Drawing.Color.Transparent;
            this.cboProduct.Enabled = false;
            this.cboProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProduct.Location = new System.Drawing.Point(674, 32);
            this.cboProduct.Margin = new System.Windows.Forms.Padding(4);
            this.cboProduct.Name = "cboProduct";
            this.cboProduct.ProductIDList = "";
            this.cboProduct.Size = new System.Drawing.Size(196, 23);
            this.cboProduct.TabIndex = 11;
            // 
            // cboBrand
            // 
            this.cboBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBrand.Location = new System.Drawing.Point(374, 31);
            this.cboBrand.Margin = new System.Windows.Forms.Padding(0);
            this.cboBrand.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBrand.Name = "cboBrand";
            this.cboBrand.Size = new System.Drawing.Size(196, 24);
            this.cboBrand.TabIndex = 9;
            // 
            // cboSubGroup
            // 
            this.cboSubGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSubGroup.Location = new System.Drawing.Point(96, 31);
            this.cboSubGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboSubGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboSubGroup.Name = "cboSubGroup";
            this.cboSubGroup.Size = new System.Drawing.Size(196, 24);
            this.cboSubGroup.TabIndex = 7;
            // 
            // cboMainGroup
            // 
            this.cboMainGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroup.Location = new System.Drawing.Point(674, 4);
            this.cboMainGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroup.Name = "cboMainGroup";
            this.cboMainGroup.Size = new System.Drawing.Size(196, 24);
            this.cboMainGroup.TabIndex = 5;
            // 
            // cboStore
            // 
            this.cboStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStore.Location = new System.Drawing.Point(374, 4);
            this.cboStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStore.Name = "cboStore";
            this.cboStore.Size = new System.Drawing.Size(196, 24);
            this.cboStore.TabIndex = 3;
            // 
            // cboCenterStore
            // 
            this.cboCenterStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCenterStore.Location = new System.Drawing.Point(96, 4);
            this.cboCenterStore.Margin = new System.Windows.Forms.Padding(0);
            this.cboCenterStore.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboCenterStore.Name = "cboCenterStore";
            this.cboCenterStore.Size = new System.Drawing.Size(196, 24);
            this.cboCenterStore.TabIndex = 1;
            this.cboCenterStore.SelectionChangeCommitted += new System.EventHandler(this.cboCenterStore_SelectionChangeCommitted);
            // 
            // grdData
            // 
            this.grdData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdData.Location = new System.Drawing.Point(1, 88);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repDistributeQuantity,
            this.repTransferQuantity});
            this.grdData.Size = new System.Drawing.Size(986, 376);
            this.grdData.TabIndex = 1;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.BandPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.BandPanel.Options.UseFont = true;
            this.grdViewData.Appearance.BandPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewData.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Red;
            this.grdViewData.Appearance.FooterPanel.Options.UseFont = true;
            this.grdViewData.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grdViewData.Appearance.GroupFooter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.grdViewData.Appearance.GroupFooter.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2});
            this.grdViewData.ColumnPanelRowHeight = 40;
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.GroupRowHeight = 0;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsMenu.EnableColumnMenu = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsView.AllowHtmlDrawHeaders = true;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            this.grdViewData.RowHeight = 20;
            this.grdViewData.CustomRowFilter += new DevExpress.XtraGrid.Views.Base.RowFilterEventHandler(this.grdViewData_CustomRowFilter);
            // 
            // gridBand1
            // 
            this.gridBand1.Name = "gridBand1";
            // 
            // gridBand2
            // 
            this.gridBand2.Name = "gridBand2";
            // 
            // repDistributeQuantity
            // 
            this.repDistributeQuantity.AutoHeight = false;
            this.repDistributeQuantity.Mask.EditMask = "d";
            this.repDistributeQuantity.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repDistributeQuantity.Name = "repDistributeQuantity";
            this.repDistributeQuantity.NullText = "0";
            this.repDistributeQuantity.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.repDistributeQuantity_EditValueChanging);
            this.repDistributeQuantity.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.repDistributeQuantity_ParseEditValue);
            // 
            // repTransferQuantity
            // 
            this.repTransferQuantity.AutoHeight = false;
            this.repTransferQuantity.Mask.EditMask = "d";
            this.repTransferQuantity.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repTransferQuantity.Name = "repTransferQuantity";
            this.repTransferQuantity.NullText = "0";
            this.repTransferQuantity.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.repTransferQuantity_EditValueChanging);
            this.repTransferQuantity.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.repTransferQuantity_ParseEditValue);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chkShortView);
            this.panel2.Controls.Add(this.btnReceive);
            this.panel2.Controls.Add(this.btnTransfer);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 470);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(988, 34);
            this.panel2.TabIndex = 2;
            // 
            // chkShortView
            // 
            this.chkShortView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkShortView.AutoSize = true;
            this.chkShortView.Location = new System.Drawing.Point(688, 5);
            this.chkShortView.Name = "chkShortView";
            this.chkShortView.Size = new System.Drawing.Size(114, 20);
            this.chkShortView.TabIndex = 2;
            this.chkShortView.Text = "Hiển thị rút gọn";
            this.chkShortView.UseVisualStyleBackColor = true;
            this.chkShortView.CheckedChanged += new System.EventHandler(this.chkShortView_CheckedChanged);
            // 
            // btnReceive
            // 
            this.btnReceive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReceive.Enabled = false;
            this.btnReceive.Image = global::ERP.Inventory.DUI.Properties.Resources.mnu_import_16;
            this.btnReceive.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReceive.Location = new System.Drawing.Point(808, 3);
            this.btnReceive.Name = "btnReceive";
            this.btnReceive.Size = new System.Drawing.Size(88, 25);
            this.btnReceive.TabIndex = 0;
            this.btnReceive.Text = "Nhận về";
            this.btnReceive.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReceive.UseVisualStyleBackColor = true;
            this.btnReceive.Click += new System.EventHandler(this.btnReceive_Click);
            // 
            // btnTransfer
            // 
            this.btnTransfer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTransfer.Enabled = false;
            this.btnTransfer.Image = global::ERP.Inventory.DUI.Properties.Resources.mnu_export_16;
            this.btnTransfer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTransfer.Location = new System.Drawing.Point(897, 3);
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.Size = new System.Drawing.Size(88, 25);
            this.btnTransfer.TabIndex = 1;
            this.btnTransfer.Text = "Chuyển đi";
            this.btnTransfer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTransfer.UseVisualStyleBackColor = true;
            this.btnTransfer.Click += new System.EventHandler(this.btnTransfer_Click);
            // 
            // frmAutoDistributeInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(988, 504);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.grdData);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmAutoDistributeInventory";
            this.ShowIcon = false;
            this.Text = "Chia hàng tự động";
            this.Load += new System.EventHandler(this.frmAutoDistributeInventory_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHoldingQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repDistributeQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTransferQuantity)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Library.AppControl.ComboBoxControl.ComboBoxBrand cboBrand;
        private Library.AppControl.ComboBoxControl.ComboBoxSubGroup cboSubGroup;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroup;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStore;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboCenterStore;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private MasterData.DUI.CustomControl.Product.cboProductList cboProduct;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.CheckBox chkIsCheckRealInput;
        private System.Windows.Forms.ComboBox cboIsShowProduct;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView grdViewData;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repDistributeQuantity;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnReceive;
        private System.Windows.Forms.Button btnTransfer;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTransferQuantity;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.CheckBox chkShortView;
        private C1.Win.C1Input.C1NumericEdit txtHoldingQuantity;
        private System.Windows.Forms.Label label10;
    }
}