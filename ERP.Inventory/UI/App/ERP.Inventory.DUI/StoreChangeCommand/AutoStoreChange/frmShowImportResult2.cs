﻿using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.StoreChangeCommand.AutoStoreChange
{
    public partial class frmShowImportResult2 : Form
    {
        List<ERP.Inventory.PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> ListAutoStoreChangeContext = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext>();
        public frmShowImportResult2(List<ERP.Inventory.PLC.StoreChangeCommand.WSStoreChangeCommand.AutoStoreChangeContext> _listAutoStoreChangeContext, bool fromExcell = true)
        {
            InitializeComponent();
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            if (!fromExcell)
                this.Text = "Kết quả";
            else
                this.Text = "Kết quả nhập từ excel";
            ListAutoStoreChangeContext = _listAutoStoreChangeContext;
        }

        private void frmShowImportResult_Load(object sender, EventArgs e)
        {
            DataTable dtb = new DataTable();
            dtb.Columns.AddRange(new DataColumn[]{
                new DataColumn(){ ColumnName = "PRODUCTID" },
                new DataColumn(){ ColumnName = "PRODUCTNAME" },
                new DataColumn(){ ColumnName = "INSTOCKSTATUSID" },
                new DataColumn(){ ColumnName = "INSTOCKSTATUSNAME" },
                new DataColumn(){ ColumnName = "IMEI" },
                new DataColumn(){ ColumnName = "FROMSTORENAME" },
                new DataColumn(){ ColumnName = "FROMINSTOCKQUANTITY" },
                new DataColumn(){ ColumnName = "TOSTORENAME" },
                new DataColumn(){ ColumnName = "TOINSTOCKQUANTITY" },
                new DataColumn(){ ColumnName = "QUANTITY" },
                new DataColumn(){ ColumnName = "AUTOQUANTITY" },
                new DataColumn(){ ColumnName = "IMPORTSTATUSTEXT" },
                new DataColumn(){ ColumnName = "IMPORTMESSAGE" },
                new DataColumn(){ ColumnName = "IMPORTSTATUS" }
            });
            ListAutoStoreChangeContext.Where(d => d.Detail.Any()).ToList().ForEach(d =>
            {
                d.Detail.ToList().ForEach(f =>
                {
                    DataRow r = dtb.NewRow();
                    r.ItemArray = new object[] {
                       f.ProductID,
                       f.ProductName,
                       d.ImportProductStateID,
                       d.ImportProductStateName,
                       f.IMEI,
                       d.FromStoreName,
                       f.FromInStockQuantity,
                       d.ToStoreName,
                       f.ToInStockQuantity,
                       f.ImportQuantity, //số lượng người dùng import
                       f.AutoQuantity, //số lượng hệ thống chia lại
                       f.ImportStatusText, 
                       f.ImportMessage,
                       f.ImportStatus
                   };
                    dtb.Rows.Add(r);
                });
            });
            grdData.DataSource = dtb;
        }


        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            if (grvViewData.DataRowCount > 0)
                Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }

        private void mnuExportExcel_Opening(object sender, CancelEventArgs e)
        {
            mnuItemExportExcel.Enabled = grvViewData.DataRowCount > 0;
        }

        private void grvViewData_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Control && e.KeyCode == Keys.C)
                {
                    Clipboard.SetText(grvViewData.GetFocusedDisplayText());
                    e.Handled = true;
                }
            }
            catch { }
        }
    }
}
