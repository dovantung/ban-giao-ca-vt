﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.StoreChangeCommand
{
    public partial class frmStoreChangeShowProduct : Form
    {
        public frmStoreChangeShowProduct()
        {
            InitializeComponent();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(bgrvData);
        }
        private DataSet dsData = null;
        private DataTable dtbStoreData = null;
        private ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = null;
        private int intTransportTypeID = 0;// phuong tien van chuyen
        private bool bolIsNew = true;
        private void frmStoreChangeShowProduct_Load(object sender, EventArgs e)
        {
            LoadCombobox();
            LoadStoreChangeOrderTypeInfo();
            btnCreateStoreChangeOrder.Enabled = false;
        }

        /// <summary>
        /// Load tham số Form
        /// </summary>
        private bool LoadStoreChangeOrderTypeInfo()
        {
            int intStoreChangeOrderTypeID = 0;
            if (this.Tag != null)
            {
                Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                try
                {
                    intStoreChangeOrderTypeID = Convert.ToInt32(hstbParam["StoreChangeOrderTypeID"]);
                }
                catch (Exception objExce)
                {
                    SystemErrorWS.Insert("Lỗi khi lấy tham số form", objExce.ToString(), DUIInventory_Globals.ModuleName);
                    MessageBox.Show(this, "Lỗi khi lấy tham số form", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            else
            {
                MessageBox.Show(this, "Lỗi khi lấy tham số form", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            new ERP.MasterData.PLC.MD.PLCStoreChangeOrderType().LoadInfo(ref objStoreChangeOrderType, intStoreChangeOrderTypeID);
            if (objStoreChangeOrderType == null)
                return false;
            return true;
        }

        private void LoadDataIntock()
        {
            object[] objKeywords = new object[] { 
                      "@STOREID", cboRealStore.StoreID,
                      "@STOREID1", cboAuxiliaryStore.StoreID,
                      "@MAINGROUPID", cboMainGroupID.MainGroupID,
                      "@SUBGROUPID", cboSubGroupIDList.SubGroupID,
                      "@BRANDID", cboBrandIDList.BrandID,
                      "@ISNEW", chkIsNew.Checked
                };
             dsData = new PLC.StoreChange.PLCStoreChange().GetStoreChange_GetInstock(objKeywords);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                btnSearch.Enabled = true;
                return;
            }
            bolIsNew = chkIsNew.Checked;
        }

        private DataTable CreateDataTable()
        {
            DataTable dt = new DataTable();
            DataColumn colIsSelect = new DataColumn("ISSELECT", typeof(Boolean));
            colIsSelect.DefaultValue=false;
            dt.Columns.Add(colIsSelect);
            dt.Columns.Add("PRODUCTID",typeof(String));
            dt.Columns.Add("PRODUCTNAME", typeof(String));
            dt.Columns.Add("REALSTOREID", typeof(Int32));
            dt.Columns.Add("REALIMEI", typeof(String));
            dt.Columns.Add("REALINPUTDATE", typeof(DateTime));
            dt.Columns.Add("AUXILIARYSTOREID", typeof(Int32));
            dt.Columns.Add("AUXILIARYIMEI", typeof(String));
            dt.Columns.Add("AUXILIARYINPUTDATE", typeof(DateTime));
            dt.Columns.Add("ISSTORECHANGEORDER", typeof(Boolean));
            return dt;
        }

        private void LoadCombobox()
        {
            
            ComboBox cboFromStoreID = new ComboBox();
            Library.AppCore.LoadControls.SetDataSource.SetStore(this, cboFromStoreID, true, -1, -1, -1, Library.AppCore.Constant.EnumType.StorePermissionType.STORECHANGEORDER);
            DataTable dtbFromStoreID = (cboFromStoreID.DataSource as DataTable).Copy();
            dtbStoreData = Library.AppCore.DataSource.GetDataSource.GetStore().Copy();
            if (dtbFromStoreID != null)
            {
                DataTable dtbRealStore = Library.AppCore.DataTableClass.Select(dtbFromStoreID, "ISREALSTORE=1");
                if (dtbRealStore != null)
                {
                    cboRealStore.InitControl(false, dtbRealStore);
                    cboAuxiliaryStore.InitControl(false, dtbRealStore.Clone());
                }
            }

            cboMainGroupID.InitControl(false, true, Library.AppCore.Constant.EnumType.IsRequestIMEIType.REQUEST_IMEI);
            cboSubGroupIDList.InitControl(false, -1);
            cboBrandIDList.InitControl(false, -1);

            //Load phuowng tien van chuyen mac dinh
            DataTable dtbTransportType = Library.AppCore.DataSource.GetDataSource.GetTransportType();
            if (dtbTransportType != null && dtbTransportType.Rows.Count > 0)
            {
                DataRow[] drTransportTypeDefault = dtbTransportType.Select("IsDefault=1");
                if (drTransportTypeDefault.Length > 0)
                    intTransportTypeID = Convert.ToInt32(drTransportTypeDefault[0]["TransportTypeID"]);
                else
                    intTransportTypeID = Convert.ToInt32(dtbTransportType.Rows[0]["TransportTypeID"]);
            }
        }

        private void cboRealStore_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (dtbStoreData != null)
            {
                DataTable dtbAuxiliaryStore = Library.AppCore.DataTableClass.Select(dtbStoreData, string.Format(" HOSTSTOREID={0} and ISAUXILIARYSTORE=1", cboRealStore.StoreID));
                cboAuxiliaryStore.InitControl(false, dtbAuxiliaryStore);
            }
        }

        private void cboMainGroupID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cboSubGroupIDList.InitControl(false, cboMainGroupID.MainGroupID);
            cboBrandIDList.InitControl(false, cboMainGroupID.MainGroupID);
        }

        private bool CheckInput()
        {
            if (cboRealStore.StoreID <= 0)
            {
                MessageBox.Show(this,"Bạn chưa chọn kho chính!","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                cboRealStore.Focus();
                return false;
            }
            if (cboAuxiliaryStore.StoreID <= 0)
            {
                MessageBox.Show(this, "Bạn chưa chọn kho bày mẫu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboAuxiliaryStore.Focus();
                return false;
            }
            if (Convert.ToInt32(txtNumDays.Value) < 1)
            {
                MessageBox.Show(this, "Số ngày tối thiểu phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNumDays.Focus();
                return false;
            }

            if (Convert.ToInt32(txtNumDays.Value) > 365)
            {
                MessageBox.Show(this, "Số ngày tối thiểu không được lớn hơn 365 ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNumDays.Focus();
                return false;
            }
            if (cboMainGroupID.MainGroupID < 1)
            {
                MessageBox.Show(this, "Bạn chưa chọn ngành hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMainGroupID.Focus();
                return false;
            }
            if (cboSubGroupIDList.SubGroupID<= 0)
            {
                MessageBox.Show(this, "Bạn chưa chọn nhóm hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboSubGroupIDList.Focus();
                return false;
            }
            return true;
        }
        private DataTable DataTableGroupBy(string strGroupByColumn, string strAggregateColumn, DataTable dtbSourceTable)
        {
            DataView dv = new DataView(dtbSourceTable);
            string[] strGroupByColumnList = strGroupByColumn.Split(',');
            DataTable dtGroup = dv.ToTable(true, strGroupByColumnList);
            if (strAggregateColumn != null && strAggregateColumn != string.Empty)
            {
                dtGroup.Columns.Add("Sum", typeof(Decimal));
                foreach (DataRow dr in dtGroup.Rows)
                {
                    dr["Sum"] = dtbSourceTable.Compute("Sum(" + strAggregateColumn + ")", strGroupByColumn + " = '" + dr[strGroupByColumn] + "'");
                }
            }
            return dtGroup;
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;
            if (!btnSearch.Enabled)
                return;
            try
            {
                btnSearch.Enabled = false;
                this.Cursor = Cursors.WaitCursor;
                LoadDataIntock();
                DataTable dtbData = CreateDataTable();
                if (dsData != null)
                {
                    DataTable dtbAuxiliaryStoreData = dsData.Tables[0];
                    DataTable dtbRealStoreData = dsData.Tables[1];
                    if (dtbRealStoreData.Rows.Count <= 0 || dtbAuxiliaryStoreData.Rows.Count <= 0)
                    {
                        grdData.DataSource = dtbData;
                        gridBand2.Caption = cboRealStore.Text;//Kho chinh
                        gridBand3.Caption = cboAuxiliaryStore.Text;//Kho bay mau
                        grdData.RefreshDataSource();
                        btnSearch.Enabled = true;
                        btnCreateStoreChangeOrder.Enabled = false;
                        this.Cursor = Cursors.Default;
                        return;
                    }
                    DataColumn col1 = new DataColumn("ISCHECK", typeof(bool));
                    col1.DefaultValue = false;
                    DataColumn col2 = new DataColumn("ISCHECK", typeof(bool));
                    col1.DefaultValue = false;
                    col2.DefaultValue = false;
                    dtbAuxiliaryStoreData.Columns.Add(col1);
                    dtbRealStoreData.Columns.Add(col2);
                    DataTable dtbProduct = DataTableGroupBy("PRODUCTID", "", dtbAuxiliaryStoreData);
                    
                    if (dtbProduct != null)
                    {
                        foreach (DataRow item in dtbProduct.Rows)
                        {
                            DataRow[] drAuxiliaryProduct = dtbAuxiliaryStoreData.Select(string.Format("PRODUCTID={0} and  ISCHECK = false", item["PRODUCTID"]));
                            if (drAuxiliaryProduct != null && drAuxiliaryProduct.Length > 0)
                            {
                                DataRow[] drRealProduct = dtbRealStoreData.Select(string.Format("PRODUCTID={0} and ISCHECK = false", item["PRODUCTID"]));
                                if (drRealProduct.Length > 0)
                                {
                                    for (int i = 0; i < drAuxiliaryProduct.Length; i++)
                                    {
                                        if (i < drRealProduct.Length)
                                        {
                                            DateTime dtmRealInputDate = Convert.ToDateTime(drRealProduct[i]["INPUTDATE"]);
                                            DateTime dtmAuxiliaryInputDate = Convert.ToDateTime(drAuxiliaryProduct[i]["INPUTDATE"]);
                                            if (dtmAuxiliaryInputDate.AddDays(Convert.ToInt32(txtNumDays.Value)) <= dtmRealInputDate)
                                            {
                                                DataRow row = dtbData.NewRow();
                                                row["ISSELECT"] = false;
                                                row["PRODUCTID"] = item["PRODUCTID"];
                                                row["PRODUCTNAME"] = drAuxiliaryProduct[i]["PRODUCTNAME"];
                                                row["REALSTOREID"] = drRealProduct[i]["STOREID"];
                                                row["REALIMEI"] = drRealProduct[i]["IMEI"];
                                                row["REALINPUTDATE"] = dtmRealInputDate;
                                                row["AUXILIARYSTOREID"] = drAuxiliaryProduct[i]["STOREID"];
                                                row["AUXILIARYIMEI"] = drAuxiliaryProduct[i]["IMEI"];
                                                row["AUXILIARYINPUTDATE"] = dtmAuxiliaryInputDate;
                                                row["ISSTORECHANGEORDER"] = Convert.ToBoolean(drAuxiliaryProduct[i]["ISSTORECHANGEORDER"]);
                                                dtbData.Rows.Add(row);
                                                drAuxiliaryProduct[i]["ISCHECK"] = true;
                                                drRealProduct[i]["ISCHECK"] = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                grdData.DataSource = dtbData;
                gridBand2.Caption = cboRealStore.Text;//Kho chinh
                gridBand3.Caption = cboAuxiliaryStore.Text;//Kho bay mau
                grdData.RefreshDataSource();
                btnSearch.Enabled = true;
                btnCreateStoreChangeOrder.Enabled = false;
                this.Cursor = Cursors.Default;
            }
            catch (Exception objEx)
            {
                btnSearch.Enabled = true;
                this.Cursor = Cursors.Default;
                SystemErrorWS.Insert("Lỗi tìm kiếm đảo kho bày mẫu", objEx.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi tìm kiếm đảo kho bày mẫu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void chkIsSelect_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit chk = (DevExpress.XtraEditors.CheckEdit)sender;
            if (chk != null)
            {
                if (bgrvData.FocusedRowHandle < 0)
                    return;
                DataRow row = bgrvData.GetDataRow(bgrvData.FocusedRowHandle);
                if (Convert.ToBoolean(row["ISSTORECHANGEORDER"])&&chk.Checked)
                {
                    MessageBox.Show(this, "Sản phẩm này đã tạo yêu cầu chuyển kho!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    chk.Checked = false;
                }
                row["ISSELECT"] = chk.Checked;
            }
            DataTable dtbData = grdData.DataSource as DataTable;
            if (dtbData != null)
            {
                if (dtbData.Select("ISSELECT=true").Length > 0)
                {
                    btnCreateStoreChangeOrder.Enabled = true;
                }
                else
                    btnCreateStoreChangeOrder.Enabled = false;
            }
        }

        private void btnCreateStoreChangeOrder_Click(object sender, EventArgs e)
        {
            if (!CheckInputCreateOrder())
                return;
            if (MessageBox.Show(this, "Bạn chắc chắn muốn tạo yêu cầu chuyển kho?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            CreateStoreChangeOrder();
        }

        /// <summary>
        /// Kiểm tra cùng tỉnh
        /// </summary>
        /// <param name="intFromStoreID">Mã kho xuất.</param>
        /// <param name="intToStoreID">Mã kho nhập.</param>
        private bool CheckSameProvince(int intFromStoreID, int intToStoreID)
        {
            ERP.MasterData.PLC.MD.WSStore.Store objFromStore = null;
            ERP.MasterData.PLC.MD.WSStore.ResultMessage objResultMessage = new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objFromStore, intFromStoreID);
            ERP.MasterData.PLC.MD.WSStore.Store objToStore = null;
            objResultMessage = new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, intToStoreID);
            return (objFromStore.ProvinceID == objToStore.ProvinceID);
        }

        /// <summary>
        /// Kiểm tra dữ liệu nhập vào
        /// </summary>
        private bool CheckInputCreateOrder()
        {
            DataRow[] drowList = (grdData.DataSource as DataTable).Select("IsSelect=true");
            if (drowList.Length <= 0)
                return false;
            bool bolIsSameProvince = CheckSameProvince(Convert.ToInt32(drowList[0]["RealStoreID"]), Convert.ToInt32(drowList[0]["AUXILIARYSTOREID"]));
            if (objStoreChangeOrderType.StoreChangeTypeID == 1 && !bolIsSameProvince)
            {
                MessageBox.Show(this, "Kho xuất và kho nhập phải cùng tỉnh/thành phố", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;

            }
            if (objStoreChangeOrderType.StoreChangeTypeID == 2 && bolIsSameProvince)
            {
                MessageBox.Show(this, "Kho xuất và kho nhập phải khác tỉnh/thành phố", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }



        /// <summary>
        /// Tạo phiếu chuyển kho
        /// </summary>
        private void CreateStoreChangeOrder()
        {
            try
            {
                DataTable dtbData=Library.AppCore.DataTableClass.Select((grdData.DataSource as DataTable),"ISSELECT=true");
                dtbData.TableName = "Detail";
                //Thông tin phiếu chuyển kho
                PLC.StoreChange.PLCStoreChangeOrder objPLCStoreChangeOrder = new PLC.StoreChange.PLCStoreChangeOrder();
                ERP.Inventory.PLC.PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrderBO = new PLC.PM.WSStoreChangeOrder.StoreChangeOrder();
                objStoreChangeOrderBO.TransportTypeID = intTransportTypeID;
                objStoreChangeOrderBO.StoreChangeOrderTypeID = objStoreChangeOrderType.StoreChangeOrderTypeID;
                objStoreChangeOrderBO.StoreChangeTypeID = Convert.ToInt32(objStoreChangeOrderType.StoreChangeTypeID);
                objStoreChangeOrderBO.Content = string.Empty;
                objStoreChangeOrderBO.FromStoreName = gridBand2.Caption;
                objStoreChangeOrderBO.ToStoreName = gridBand3.Caption;
                objStoreChangeOrderBO.OrderDate = Library.AppCore.Globals.GetServerDateTime();
                objStoreChangeOrderBO.CreatedUser = SystemConfig.objSessionUser.UserName;
                objStoreChangeOrderBO.CreatedDate = Globals.GetServerDateTime();
                objStoreChangeOrderBO.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objStoreChangeOrderBO.ExpiryDate = Globals.GetServerDateTime().AddDays(2);
                objStoreChangeOrderBO.IsNew = bolIsNew;
                objStoreChangeOrderBO.IsReviewed = objStoreChangeOrderType.IsAutoReview;
                if (objStoreChangeOrderBO.IsReviewed)
                {
                    objStoreChangeOrderBO.ReviewedDate = Library.AppCore.Globals.GetServerDateTime();
                    objStoreChangeOrderBO.ReviewedUser = SystemConfig.objSessionUser.UserName;
                }
                objPLCStoreChangeOrder.CreateStoreChangeOrderShowProduct(objStoreChangeOrderBO, dtbData);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnCreateStoreChangeOrder.Enabled = true;
                    return;
                }
                MessageBox.Show(this, "Tạo yêu cầu chuyển kho thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnSearch_Click(null, null);
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi cập nhật yêu cầu chuyển kho", objEx.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi cập nhật yêu cầu chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
