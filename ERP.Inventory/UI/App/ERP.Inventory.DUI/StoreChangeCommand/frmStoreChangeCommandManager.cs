﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.StoreChangeCommand
{
    /// <summary>
    /// Nguyễn Linh Tuấn
    /// 01/12/2012
    /// </summary>
    public partial class frmStoreChangeCommandManager : Form
    {
        private ERP.Inventory.PLC.StoreChangeCommand.PLCStoreChangeCommand objPLCStoreChangeCommand = new PLC.StoreChangeCommand.PLCStoreChangeCommand();
        private List<ERP.Inventory.PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand> objStoreChangeCommandList = null;
        private ERP.MasterData.PLC.MD.WSStoreChangeOrderType.StoreChangeOrderType objStoreChangeOrderType = null;
        private int intTransportTypeID = 0;//Phương tiện vận chuyển mặc định

        public frmStoreChangeCommandManager()
        {
            InitializeComponent();
        }

        private void LoadComboBox()
        {
            try
            {
                cboFromStoreID.InitControl(false, false);
                cboToStoreID.InitControl(false, false);
                DataTable dtbTransportType = Library.AppCore.DataSource.GetDataSource.GetTransportType().Copy();
                if(dtbTransportType!=null&&dtbTransportType.Rows.Count>0)
                {
                    DataRow[] drTransportTypeDefault = dtbTransportType.Select("IsDefault=1");
                    if (drTransportTypeDefault.Length > 0)
                        intTransportTypeID = Convert.ToInt32(drTransportTypeDefault[0]["TransportTypeID"]);
                    else
                        intTransportTypeID = Convert.ToInt32(dtbTransportType.Rows[0]["TransportTypeID"]);
                }
                //DataTable dtbStoreChangeCommandTypeID = null;
                //new ERP.MasterData.PLC.MD.PLCStoreChangeCommandType().SearchData(ref dtbStoreChangeCommandTypeID);
                
                //if (dtbStoreChangeCommandTypeID != null)
                //{
                //    DataRow row = dtbStoreChangeCommandTypeID.NewRow();
                //    row["StoreChangeCommandTypeID"] = -1;
                //    row["StoreChangeCommandTypeName"] = "Tất cả";
                //    dtbStoreChangeCommandTypeID.Rows.InsertAt(row, 0);
                //}
                //Library.AppCore.LoadControls.ComboBoxObject.LoadDataFromDataTable(dtbStoreChangeCommandTypeID,cboStoreChangeCommandType);
                Library.AppCore.LoadControls.SetDataSource.SetStoreChangeCommandType(this, cboStoreChangeCommandType);
                cboFromStoreID.SetValue(SystemConfig.intDefaultStoreID);
                cboIsCreateOrder.SelectedIndex = 0;
                dtFromDate.DateTime = DateTime.Now;
                dtToDate.DateTime = DateTime.Now;
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Lỗi nạp combobox", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void CustomFlex()
        {
            flexListStoreChangeCommand.AutoClipboard = true;
            flexListStoreChangeCommand.ClipboardCopyMode = C1.Win.C1FlexGrid.ClipboardCopyModeEnum.DataOnly;
            flexListStoreChangeCommand.Cols[0].Visible = true;
            flexListStoreChangeCommand.Cols["IsEdit"].Visible = false;
            flexListStoreChangeCommand.Cols["FromStoreID"].Visible = false;
            flexListStoreChangeCommand.Cols["ToStoreID"].Visible = false;
            flexListStoreChangeCommand.Cols["createduser"].Visible = false;

            flexListStoreChangeCommand.Cols["IsSelect"].DataType = typeof(bool);
            flexListStoreChangeCommand.Cols["IsCreateStoreChangeOrder"].DataType = typeof(bool);
            flexListStoreChangeCommand.Cols["createddate"].Format = "dd/MM/yyyy HH:mm";
            flexListStoreChangeCommand.Cols["IsUrgent"].DataType = typeof(bool);

            for (int i = 1; i < flexListStoreChangeCommand.Cols.Count; i++)
            {
                flexListStoreChangeCommand.Cols[i].AllowEditing = false;
                flexListStoreChangeCommand.Cols[i].AllowDragging = false;
            }
            flexListStoreChangeCommand.Cols["IsSelect"].AllowEditing = true;
            flexListStoreChangeCommand.Cols["Note"].AllowEditing = true;
            flexListStoreChangeCommand.Cols["IsSelect"].AllowSorting = false;

            flexListStoreChangeCommand.Cols["IsSelect"].Caption = "Chọn";
            flexListStoreChangeCommand.Cols["StoreChangeCommandID"].Caption = "Mã lệnh chuyển";
            flexListStoreChangeCommand.Cols["storechangeorderid"].Caption = "Mã yêu cầu chuyển kho";
            flexListStoreChangeCommand.Cols["StoreChangeCommandTypeName"].Caption= "Loại lệnh chuyển kho";
            flexListStoreChangeCommand.Cols["createddate"].Caption = "Ngày tạo";

            flexListStoreChangeCommand.Cols["FromStoreName"].Caption = "Kho xuất";
            flexListStoreChangeCommand.Cols["ToStoreName"].Caption = "Kho nhập";
            flexListStoreChangeCommand.Cols["FullName"].Caption = "Nhân viên tạo";
            flexListStoreChangeCommand.Cols["IsCreateStoreChangeOrder"].Caption = "Đã tạo yêu cầu";
            flexListStoreChangeCommand.Cols["Note"].Caption = "Ghi chú";
            flexListStoreChangeCommand.Cols["IsUrgent"].Caption = "Chuyển gấp";

            flexListStoreChangeCommand.Cols["IsSelect"].Width = 50;
            flexListStoreChangeCommand.Cols["StoreChangeCommandID"].Width = 130;
            flexListStoreChangeCommand.Cols["storechangeorderid"].Width = 120;
            flexListStoreChangeCommand.Cols["createddate"].Width = 120;
           flexListStoreChangeCommand.Cols["StoreChangeCommandTypeName"].Width = 230;
            flexListStoreChangeCommand.Cols["FromStoreName"].Width = 210;
            flexListStoreChangeCommand.Cols["ToStoreName"].Width = 210;
            flexListStoreChangeCommand.Cols["FullName"].Width = 190;
            flexListStoreChangeCommand.Cols["IsCreateStoreChangeOrder"].Width = 110;
            flexListStoreChangeCommand.Cols["Note"].Width = 200;
            flexListStoreChangeCommand.Cols["IsUrgent"].Width = 90;

            #region Định dạng Style cho lưới
            Library.AppCore.LoadControls.C1FlexGridObject.SetBackColor(flexListStoreChangeCommand, SystemColors.Info);
            C1.Win.C1FlexGrid.CellStyle style = flexListStoreChangeCommand.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = flexListStoreChangeCommand.GetCellRange(0, 0, 0, flexListStoreChangeCommand.Cols.Count - 1);
            range.Style = style;
            flexListStoreChangeCommand.Rows[0].Height = 24;
            #endregion
        }

        private bool CheckInputDelete()
        {
            if (!Library.AppCore.LoadControls.C1FlexGridObject.CheckIsSelected(flexListStoreChangeCommand, "IsSelect"))
            {
                MessageBox.Show(this, "Vui lòng chọn ít nhất một lệnh chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            for (int i = 1; i < flexListStoreChangeCommand.Rows.Count; i++)
            {
                if (Convert.ToBoolean(flexListStoreChangeCommand[i, "IsSelect"]))
                {
                    if (Convert.ToString(flexListStoreChangeCommand[i, "CreatedUser"]).Trim() != SystemConfig.objSessionUser.UserName.Trim() && !SystemConfig.objSessionUser.IsPermission("PM_StoreChangeCommand_DeleteAll"))
                    {
                        MessageBox.Show(this, "Bạn không có quyền xóa lệnh chuyển kho này", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexListStoreChangeCommand.Select(i, 1, i, 1);
                        return false;
                    }

                    if (Convert.ToBoolean(flexListStoreChangeCommand[i, "IsCreateStoreChangeOrder"]))
                    {
                        MessageBox.Show(this, "Lệnh chuyển kho này đã tạo yêu cầu xuất.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flexListStoreChangeCommand.Select(i, 1, i, 1);
                        return false;
                    }

                }
            }

            if (MessageBox.Show(this, "Bạn có chắc muốn xóa những lệnh chuyển kho này không", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return false;

            return true;
        }

        /// <summary>
        /// Kiểm tra cùng tỉnh
        /// </summary>
        /// <param name="intFromStoreID">Mã kho xuất.</param>
        /// <param name="intToStoreID">Mã kho nhập.</param>
        private bool CheckSameProvince(int intFromStoreID, int intToStoreID)
        {
            ERP.MasterData.PLC.MD.WSStore.Store objFromStore = null;
            ERP.MasterData.PLC.MD.WSStore.ResultMessage objResultMessage = new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objFromStore, intFromStoreID);
            ERP.MasterData.PLC.MD.WSStore.Store objToStore = null;
            objResultMessage = new ERP.MasterData.PLC.MD.PLCStore().LoadInfo(ref objToStore, intToStoreID);
            return (objFromStore.ProvinceID == objToStore.ProvinceID);
        }

        /// <summary>
        /// Kiểm tra dữ liệu nhập vào
        /// </summary>
        private bool CheckInputCreateOrder()
        {

            if (!Library.AppCore.LoadControls.C1FlexGridObject.CheckIsSelected(flexListStoreChangeCommand, "IsSelect"))
            {
                MessageBox.Show(this, "Vui lòng chọn ít nhất một lệnh chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            for (int i = 1; i < flexListStoreChangeCommand.Rows.Count; i++)
            {
                if (Convert.ToBoolean(flexListStoreChangeCommand[i, "IsSelect"]))
                {
                    if (!Convert.ToBoolean(flexListStoreChangeCommand[i, "IsCreateStoreChangeOrder"]))
                    {
                        bool bolIsSameProvince = CheckSameProvince(Convert.ToInt32(flexListStoreChangeCommand[i, "FromStoreID"]), Convert.ToInt32(flexListStoreChangeCommand[i, "ToStoreID"]));
                        if (objStoreChangeOrderType.StoreChangeTypeID == 1 && !bolIsSameProvince)
                        {

                            MessageBox.Show(this, "Kho xuất và kho nhập phải cùng tỉnh/thành phố", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            flexListStoreChangeCommand.Select(i, 1);
                            return false;

                        }

                        if (objStoreChangeOrderType.StoreChangeTypeID == 2 && bolIsSameProvince)
                        {
                            MessageBox.Show(this, "Kho xuất và kho nhập phải khác tỉnh/thành phố", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            flexListStoreChangeCommand.Select(i, 1);
                            return false;
                        }
                    }
                }
            }
            return true;

        }

        

        /// <summary>
        /// Tạo phiếu chuyển kho
        /// </summary>
        private void CreateStoreChangeOrder()
        {
            btnUpdate.Enabled = false;
            DataTable tblStoreChangeCommand = flexListStoreChangeCommand.DataSource as DataTable;
            tblStoreChangeCommand = Globals.DataTableSelect(tblStoreChangeCommand, "IsSelect = 1");
            DataTable tblFromStore = Globals.SelectDistinct(tblStoreChangeCommand, "FromStoreID");
            try
            {
                //Thông tin phiếu chuyển kho
                PLC.StoreChange.PLCStoreChangeOrder objPLCStoreChangeOrder = new PLC.StoreChange.PLCStoreChangeOrder();
                ERP.Inventory.PLC.PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrderBO = new PLC.PM.WSStoreChangeOrder.StoreChangeOrder();
                objStoreChangeOrderBO.TransportTypeID = intTransportTypeID;
                objStoreChangeOrderBO.StoreChangeOrderTypeID = objStoreChangeOrderType.StoreChangeOrderTypeID;
                objStoreChangeOrderBO.StoreChangeTypeID = Convert.ToInt32(objStoreChangeOrderType.StoreChangeTypeID);
                objStoreChangeOrderBO.Content = string.Empty;
                objStoreChangeOrderBO.OrderDate = Library.AppCore.Globals.GetServerDateTime();
                objStoreChangeOrderBO.CreatedUser = SystemConfig.objSessionUser.UserName;
                objStoreChangeOrderBO.CreatedDate = Globals.GetServerDateTime();
                objStoreChangeOrderBO.CreatedStoreID = SystemConfig.intDefaultStoreID;
                objStoreChangeOrderBO.ExpiryDate = Globals.GetServerDateTime().AddDays(2);
                objStoreChangeOrderBO.IsNew = true;
                objStoreChangeOrderBO.IsReviewed = objStoreChangeOrderType.IsAutoReview;
                if (objStoreChangeOrderBO.IsReviewed)
                {
                    objStoreChangeOrderBO.ReviewedDate = Library.AppCore.Globals.GetServerDateTime();
                    objStoreChangeOrderBO.ReviewedUser = SystemConfig.objSessionUser.UserName;
                  
                }
                objPLCStoreChangeOrder.CreateStoreChangeOrder(objStoreChangeOrderBO, tblStoreChangeCommand, tblFromStore);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnUpdate.Enabled = true;
                    return;
                }
                //else
                //{
                //    StoreChange.frmStoreChangeOrder frm = new StoreChange.frmStoreChangeOrder();
                //    frm.bolFromStoreChangeCmd = true;
                //    frm.intStoreChangeOrderTypeID = objStoreChangeOrderType.StoreChangeOrderTypeID;
                //    frm.dtbStoreChangeOrderDetail = tblStoreChangeCommand;
                //    frm.objStoreChangeOrder = objStoreChangeOrderBO;
                //    frm.ShowDialog();
                //    return;
                //}
               
                MessageBox.Show(this,"Tạo yêu cầu chuyển kho thành công","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi cập nhật yêu cầu chuyển kho", objEx.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi cập nhật yêu cầu chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            btnUpdate.Enabled = true;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            btnUpdate.Enabled = false;

            List<object> objList = new List<object>();
            objList.AddRange(new object[] 
                                    { 
                                        "@StoreChangeCommandID", txtKeyword.Text.Trim(),
                                        "@FromStoreID", cboFromStoreID.StoreID,
                                        "@ToStoreID", cboToStoreID.StoreID,
                                        "@createduser", ucUserQuickSearch1.UserName.Trim()== "" ? "-1" : ucUserQuickSearch1.UserName.Trim(),
                                        "@FromDate", dtFromDate.DateTime,
                                        "@ToDate", dtToDate.DateTime,
                                        "@StoreChangeCommandTypeID", cboStoreChangeCommandType.SelectedValue,
                                        "@IsCreateStoreChangeOrder", cboIsCreateOrder.SelectedIndex,
                                        "@IsSearchProductName",chkSearchProductName.Checked,
                                        "@IsStoreChangeCommandID", chkIsStoreChangeCommandID.Checked,
                                        "@IsOrderID", chkIsOrderID.Checked,
                                        "@IsUrgent", chkIsUrgent.Checked ? 1 : -1
                                    });
            if (!SystemConfig.objSessionUser.IsPermission("PM_STORECHANGECOMMAND_CREATE"))
                objList.AddRange(new object[] { "@UserName", SystemConfig.objSessionUser.UserName.Trim() });

            try
            {
                DataTable dtbData = objPLCStoreChangeCommand.SearchData(objList.ToArray());
                if (dtbData == null) return;
                flexListStoreChangeCommand.DataSource = dtbData;
                CustomFlex();
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi tìm kiếm lệnh xuất chuyển kho", objEx.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi tìm kiếm lệnh xuất chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                btnSearch.Enabled = true;
            }
        }

        private void frmStoreChangeCommandManager_Load(object sender, EventArgs e)
        {
            btnSearch.Enabled = SystemConfig.objSessionUser.IsPermission("PM_STORECHANGECOMMAND_SEARCH");
            btnUpdate.Visible = SystemConfig.objSessionUser.IsPermission("PM_STORECHANGECOMMAND_UPDATENOTE");
            mnuItemDelete.Enabled = SystemConfig.objSessionUser.IsPermission("PM_STORECHANGECOMMAND_DELETE") || SystemConfig.objSessionUser.IsPermission("PM_STORECHANGECOMMAND_DELETEALL");
            if (!LoadStoreChangeOrderTypeInfo())
            {
                btnCreateStoreChangeOrder.Enabled = false;
            }
            LoadComboBox();
            btnUpdate.Enabled = false;
        }

        /// <summary>
        /// Load tham số Form
        /// </summary>
        private bool LoadStoreChangeOrderTypeInfo()
        {
            int intStoreChangeOrderTypeID = 0;
            if (this.Tag != null)
            {
                Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                try
                {
                    intStoreChangeOrderTypeID = Convert.ToInt32(hstbParam["StoreChangeOrderTypeID"]);
                }
                catch (Exception objExce)
                {
                    SystemErrorWS.Insert("Lỗi khi lấy tham số form", objExce.ToString(), DUIInventory_Globals.ModuleName);
                    MessageBox.Show(this, "Lỗi khi lấy tham số form", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            new ERP.MasterData.PLC.MD.PLCStoreChangeOrderType().LoadInfo(ref objStoreChangeOrderType, intStoreChangeOrderTypeID);
            if (objStoreChangeOrderType==null)
                return false;
            this.Text +=" "+ objStoreChangeOrderType.StoreChangeOrderTypeName;
            btnUpdate.Enabled = false;
            return true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                objStoreChangeCommandList = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand>();
                foreach (DataRow rowEdit in ((DataTable)flexListStoreChangeCommand.DataSource).Select("IsEdit = 1"))
                {
                    ERP.Inventory.PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand objStoreChangeCommand = new PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand();
                    objStoreChangeCommand.StoreChangeCommandID = Convert.ToString(rowEdit["StoreChangeCommandID"]).Trim();
                    objStoreChangeCommand.Note = Convert.ToString(rowEdit["Note"]).Trim();
                    objStoreChangeCommandList.Add(objStoreChangeCommand);
                }

                if (objPLCStoreChangeCommand.UpdateNote(objStoreChangeCommandList))
                {
                    MessageBox.Show(this, "Cập nhật ghi chú lệnh xuất chuyển kho thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }
                btnSearch_Click(sender, null);
                btnUpdate.Enabled = false;
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi cập nhật ghi chú lệnh chuyển kho", objEx.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi cập nhật ghi chú lệnh chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void flexListStoreChangeCommand_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Row < 1) return;
            if (e.Col == flexListStoreChangeCommand.Cols["Note"].Index)
            {
                flexListStoreChangeCommand[e.Row, "IsEdit"] = 1;
                btnUpdate.Enabled = SystemConfig.objSessionUser.IsPermission("PM_STORECHANGECOMMAND_UPDATENOTE");
            }
        }

        private void flexListStoreChangeCommand_SetupEditor(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            TextBox txtEditor = flexListStoreChangeCommand.Editor as TextBox;
            String strColName = Convert.ToString(flexListStoreChangeCommand.Cols[e.Col].Name).Trim().ToUpper();
            switch (strColName)
            {
                case "NOTE": txtEditor.MaxLength = 1500;
                    break;
            }
        }

        private void mnuItemDelete_Click(object sender, EventArgs e)
        {
            if (!CheckInputDelete()) return;
            try
            {
                objStoreChangeCommandList = new List<PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand>();
                for (int i = 1; i < flexListStoreChangeCommand.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(flexListStoreChangeCommand[i, "IsSelect"]))
                    {
                        PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand objStoreChangeCommand = new PLC.StoreChangeCommand.WSStoreChangeCommand.StoreChangeCommand();
                        objStoreChangeCommand.DeletedUser = SystemConfig.objSessionUser.UserName.Trim();
                        objStoreChangeCommand.StoreChangeCommandID = Convert.ToString(flexListStoreChangeCommand[i, "StoreChangeCommandID"]).Trim();
                        objStoreChangeCommand.StoreChangeOrderID = Convert.ToString(flexListStoreChangeCommand[i, "StoreChangeOrderID"]).Trim();
                        objStoreChangeCommandList.Add(objStoreChangeCommand);
                    }
                }
                if (objStoreChangeCommandList.Count > 0)
                {
                    objPLCStoreChangeCommand.DeleteMulti(objStoreChangeCommandList);
                    MessageBox.Show(this, "Xóa lệnh xuất chuyển kho thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi xóa lệnh xuất chuyển kho", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi xóa lệnh xuất chuyển kho", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            btnSearch_Click(sender, null);
        }

        private void btnCreateStoreChangeOrder_Click(object sender, EventArgs e)
        {
            if (!CheckInputCreateOrder())
                return;
            CreateStoreChangeOrder();
            btnSearch_Click(null,null);
        }

        private void flexListStoreChangeCommand_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (flexListStoreChangeCommand.Cols[e.Col].Name.ToUpper() == "ISSELECT")
            {
                if (e.Row >= flexListStoreChangeCommand.Rows.Fixed)
                {
                    if (Convert.ToBoolean(flexListStoreChangeCommand[e.Row, "IsCreateStoreChangeOrder"]))
                    {
                        flexListStoreChangeCommand[e.Row, e.Col] = false;
                        e.Cancel = true;
                        return;
                    }
                    DataTable tblStoreChangeCommand = flexListStoreChangeCommand.DataSource as DataTable;
                    tblStoreChangeCommand = Globals.DataTableSelect(tblStoreChangeCommand, "IsSelect = 1");
                    if (tblStoreChangeCommand.Rows.Count > 0 && !objStoreChangeOrderType.IsAutoReview)
                    {
                        string strStoreChangeCommandIDList = GetStoreChangeCommandIDList(tblStoreChangeCommand, Convert.ToInt32(flexListStoreChangeCommand[e.Row, "FromStoreID"])
                            , Convert.ToInt32(flexListStoreChangeCommand[e.Row, "ToStoreID"]));
                        if (strStoreChangeCommandIDList == string.Empty)
                        {
                            flexListStoreChangeCommand[e.Row, e.Col] = false;
                            e.Cancel = true;
                            return;
                        }
                    }
                }
            }
        }

        private String GetStoreChangeCommandIDList(DataTable tblStoreChangeCommand, int intFromStoreID, int intToStoreID)
        {
            String strResult = "";
            String strSelectExp = "FromStoreID = " + intFromStoreID.ToString() + " and ToStoreID = " + intToStoreID.ToString();
            DataRow[] arrRow = tblStoreChangeCommand.Select(strSelectExp);
            foreach (DataRow objRow in arrRow)
            {
                strResult += "'" + Convert.ToString(objRow["StoreChangeCommandID"]).Trim() + "',";
            }
            if (strResult.Length > 0)
                strResult = strResult.Substring(0, strResult.Length - 1);
            return strResult;
        }

    }
}
