﻿namespace ERP.Inventory.DUI.IMEI
{
    partial class frmProductIMEI_OrderIndexSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnSearchStatus = new System.Windows.Forms.Panel();
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.label11 = new System.Windows.Forms.Label();
            this.gpSearch = new System.Windows.Forms.GroupBox();
            this.cboIsInStock = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboBrandIDList = new Library.AppControl.ComboBoxControl.ComboBoxBrand();
            this.cboSubGroupIDList = new Library.AppControl.ComboBoxControl.ComboBoxSubGroup();
            this.cboProductList1 = new ERP.MasterData.DUI.CustomControl.Product.cboProductList();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cboMainGroup = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnExcel = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dtpDateTo = new System.Windows.Forms.DateTimePicker();
            this.dtpDateFrom = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkActiveItem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkSystemItem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.mnuDepartment = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmdSearchDepartment = new System.Windows.Forms.ToolStripMenuItem();
            this.tmrFormatFlex = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.pnSearchStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            this.gpSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActiveItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSystemItem)).BeginInit();
            this.mnuDepartment.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pnSearchStatus);
            this.groupBox1.Controls.Add(this.gpSearch);
            this.groupBox1.Controls.Add(this.grdData);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1099, 611);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // pnSearchStatus
            // 
            this.pnSearchStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnSearchStatus.Controls.Add(this.marqueeProgressBarControl1);
            this.pnSearchStatus.Controls.Add(this.label11);
            this.pnSearchStatus.Location = new System.Drawing.Point(321, 259);
            this.pnSearchStatus.Name = "pnSearchStatus";
            this.pnSearchStatus.Size = new System.Drawing.Size(456, 88);
            this.pnSearchStatus.TabIndex = 7;
            this.pnSearchStatus.Visible = false;
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.EditValue = 0;
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(25, 46);
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Properties.LookAndFeel.SkinName = "The Asphalt World";
            this.marqueeProgressBarControl1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(406, 25);
            this.marqueeProgressBarControl1.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label11.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label11.Location = new System.Drawing.Point(24, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(416, 23);
            this.label11.TabIndex = 3;
            this.label11.Text = "Đang tìm kiếm dữ liệu. Vui lòng chờ trong giây lát...";
            // 
            // gpSearch
            // 
            this.gpSearch.Controls.Add(this.cboIsInStock);
            this.gpSearch.Controls.Add(this.label1);
            this.gpSearch.Controls.Add(this.cboBrandIDList);
            this.gpSearch.Controls.Add(this.cboSubGroupIDList);
            this.gpSearch.Controls.Add(this.cboProductList1);
            this.gpSearch.Controls.Add(this.label2);
            this.gpSearch.Controls.Add(this.label7);
            this.gpSearch.Controls.Add(this.cboMainGroup);
            this.gpSearch.Controls.Add(this.label5);
            this.gpSearch.Controls.Add(this.btnExcel);
            this.gpSearch.Controls.Add(this.btnSearch);
            this.gpSearch.Controls.Add(this.dtpDateTo);
            this.gpSearch.Controls.Add(this.dtpDateFrom);
            this.gpSearch.Controls.Add(this.label3);
            this.gpSearch.Controls.Add(this.label6);
            this.gpSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.gpSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpSearch.Location = new System.Drawing.Point(4, 19);
            this.gpSearch.Margin = new System.Windows.Forms.Padding(4);
            this.gpSearch.Name = "gpSearch";
            this.gpSearch.Padding = new System.Windows.Forms.Padding(4);
            this.gpSearch.Size = new System.Drawing.Size(1091, 85);
            this.gpSearch.TabIndex = 0;
            this.gpSearch.TabStop = false;
            this.gpSearch.Text = "Tìm kiếm";
            // 
            // cboIsInStock
            // 
            this.cboIsInStock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIsInStock.DropDownWidth = 250;
            this.cboIsInStock.Items.AddRange(new object[] {
            "-- Tất cả --",
            "Không còn tồn kho",
            "Còn tồn kho"});
            this.cboIsInStock.Location = new System.Drawing.Point(744, 52);
            this.cboIsInStock.Name = "cboIsInStock";
            this.cboIsInStock.Size = new System.Drawing.Size(238, 24);
            this.cboIsInStock.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(667, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 16);
            this.label1.TabIndex = 11;
            this.label1.Text = "Tồn kho:";
            // 
            // cboBrandIDList
            // 
            this.cboBrandIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBrandIDList.Location = new System.Drawing.Point(744, 22);
            this.cboBrandIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboBrandIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBrandIDList.Name = "cboBrandIDList";
            this.cboBrandIDList.Size = new System.Drawing.Size(238, 24);
            this.cboBrandIDList.TabIndex = 5;
            // 
            // cboSubGroupIDList
            // 
            this.cboSubGroupIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSubGroupIDList.Location = new System.Drawing.Point(426, 22);
            this.cboSubGroupIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboSubGroupIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboSubGroupIDList.Name = "cboSubGroupIDList";
            this.cboSubGroupIDList.Size = new System.Drawing.Size(238, 24);
            this.cboSubGroupIDList.TabIndex = 3;
            this.cboSubGroupIDList.Load += new System.EventHandler(this.cboSubGroupIDList_Load);
            // 
            // cboProductList1
            // 
            this.cboProductList1.BackColor = System.Drawing.Color.Transparent;
            this.cboProductList1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductList1.Location = new System.Drawing.Point(426, 54);
            this.cboProductList1.Margin = new System.Windows.Forms.Padding(4);
            this.cboProductList1.Name = "cboProductList1";
            this.cboProductList1.ProductIDList = "";
            this.cboProductList1.Size = new System.Drawing.Size(238, 23);
            this.cboProductList1.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(667, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nhà SX:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(349, 56);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 16);
            this.label7.TabIndex = 9;
            this.label7.Text = "Sản phẩm:";
            // 
            // cboMainGroup
            // 
            this.cboMainGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMainGroup.DropDownWidth = 250;
            this.cboMainGroup.Items.AddRange(new object[] {
            "-- Chọn ngành hàng --"});
            this.cboMainGroup.Location = new System.Drawing.Point(102, 22);
            this.cboMainGroup.Name = "cboMainGroup";
            this.cboMainGroup.Size = new System.Drawing.Size(238, 24);
            this.cboMainGroup.TabIndex = 1;
            this.cboMainGroup.SelectionChangeCommitted += new System.EventHandler(this.cboMainGroupID_SelectionChangeCommitted);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(348, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "Nhóm hàng:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // btnExcel
            // 
            this.btnExcel.Enabled = false;
            this.btnExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcel.Location = new System.Drawing.Point(985, 49);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(101, 27);
            this.btnExcel.TabIndex = 14;
            this.btnExcel.Text = "   Xuất &Excel";
            this.btnExcel.UseVisualStyleBackColor = true;
            this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(985, 21);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(101, 27);
            this.btnSearch.TabIndex = 13;
            this.btnSearch.Text = "&Tìm kiếm";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dtpDateTo
            // 
            this.dtpDateTo.CustomFormat = "dd/MM/yyyy";
            this.dtpDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateTo.Location = new System.Drawing.Point(225, 52);
            this.dtpDateTo.Name = "dtpDateTo";
            this.dtpDateTo.Size = new System.Drawing.Size(115, 22);
            this.dtpDateTo.TabIndex = 8;
            // 
            // dtpDateFrom
            // 
            this.dtpDateFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateFrom.Location = new System.Drawing.Point(102, 51);
            this.dtpDateFrom.Name = "dtpDateFrom";
            this.dtpDateFrom.Size = new System.Drawing.Size(115, 22);
            this.dtpDateFrom.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 54);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Ngày thiết lập:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "Ngành hàng:";
            // 
            // grdData
            // 
            this.grdData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdData.Location = new System.Drawing.Point(4, 111);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chkIsSelect,
            this.chkActiveItem,
            this.chkSystemItem});
            this.grdData.Size = new System.Drawing.Size(1091, 493);
            this.grdData.TabIndex = 9;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Preview.Options.UseFont = true;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewData.OptionsView.ShowFooter = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            // 
            // chkActiveItem
            // 
            this.chkActiveItem.AutoHeight = false;
            this.chkActiveItem.Name = "chkActiveItem";
            this.chkActiveItem.ValueChecked = ((short)(1));
            this.chkActiveItem.ValueUnchecked = ((short)(0));
            // 
            // chkSystemItem
            // 
            this.chkSystemItem.AutoHeight = false;
            this.chkSystemItem.Name = "chkSystemItem";
            this.chkSystemItem.ValueChecked = ((short)(1));
            this.chkSystemItem.ValueUnchecked = ((short)(0));
            // 
            // mnuDepartment
            // 
            this.mnuDepartment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdSearchDepartment});
            this.mnuDepartment.Name = "mnuGrid";
            this.mnuDepartment.Size = new System.Drawing.Size(165, 26);
            // 
            // cmdSearchDepartment
            // 
            this.cmdSearchDepartment.Name = "cmdSearchDepartment";
            this.cmdSearchDepartment.Size = new System.Drawing.Size(164, 22);
            this.cmdSearchDepartment.Text = "Chọn phòng ban";
            // 
            // tmrFormatFlex
            // 
            this.tmrFormatFlex.Enabled = true;
            this.tmrFormatFlex.Interval = 1000;
            this.tmrFormatFlex.Tick += new System.EventHandler(this.tmrFormatFlex_Tick);
            // 
            // frmProductIMEI_OrderIndexSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 611);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmProductIMEI_OrderIndexSearch";
            this.ShowIcon = false;
            this.Text = "Mức ưu tiên xuất kho theo mã sản phẩm lấy IMEI tự động";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmProductIMEI_OrderIndexSearch_FormClosing);
            this.Load += new System.EventHandler(this.frmSearchDetailExpense_Load);
            this.groupBox1.ResumeLayout(false);
            this.pnSearchStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            this.gpSearch.ResumeLayout(false);
            this.gpSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActiveItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSystemItem)).EndInit();
            this.mnuDepartment.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gpSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DateTimePicker dtpDateTo;
        private System.Windows.Forms.DateTimePicker dtpDateFrom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ContextMenuStrip mnuDepartment;
        private System.Windows.Forms.ToolStripMenuItem cmdSearchDepartment;
        private System.Windows.Forms.Button btnExcel;
        private System.Windows.Forms.Panel pnSearchStatus;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Timer tmrFormatFlex;
        //private AccReports.Admin.CachedrptLedgerReport cachedrptLedgerReport1;
        //private AccReports.Admin.CachedrptLedgerReport cachedrptLedgerReport2;
        //private AccReports.Admin.CachedrptLedgerReport cachedrptLedgerReport3;
        //private AccReports.Admin.CachedrptLedgerReport cachedrptLedgerReport4;
        private Library.AppControl.ComboBoxControl.ComboBoxBrand cboBrandIDList;
        private Library.AppControl.ComboBoxControl.ComboBoxSubGroup cboSubGroupIDList;
        private MasterData.DUI.CustomControl.Product.cboProductList cboProductList1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboMainGroup;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboIsInStock;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkActiveItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkSystemItem;
    }
}