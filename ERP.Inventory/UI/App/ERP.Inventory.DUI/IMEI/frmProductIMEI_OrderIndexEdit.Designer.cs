﻿namespace ERP.Inventory.DUI.IMEI
{
    partial class frmProductIMEI_OrderIndexEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProductIMEI_OrderIndexEdit));
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.mnuIMEI = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemInputExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkActiveItem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chkSystemItem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.btnImportExcel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            this.mnuIMEI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActiveItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSystemItem)).BeginInit();
            this.SuspendLayout();
            // 
            // grdData
            // 
            this.grdData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdData.ContextMenuStrip = this.mnuIMEI;
            this.grdData.Location = new System.Drawing.Point(1, 2);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chkIsSelect,
            this.chkActiveItem,
            this.chkSystemItem});
            this.grdData.Size = new System.Drawing.Size(908, 526);
            this.grdData.TabIndex = 8;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            // 
            // mnuIMEI
            // 
            this.mnuIMEI.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemExportExcel,
            this.mnuItemInputExcel});
            this.mnuIMEI.Name = "contextMenuStrip1";
            this.mnuIMEI.Size = new System.Drawing.Size(155, 48);
            // 
            // mnuItemExportExcel
            // 
            this.mnuItemExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemExportExcel.Image")));
            this.mnuItemExportExcel.Name = "mnuItemExportExcel";
            this.mnuItemExportExcel.Size = new System.Drawing.Size(154, 22);
            this.mnuItemExportExcel.Text = "Xuất Excel mẫu";
            this.mnuItemExportExcel.Click += new System.EventHandler(this.mnuItemExportExcel_Click);
            // 
            // mnuItemInputExcel
            // 
            this.mnuItemInputExcel.Image = ((System.Drawing.Image)(resources.GetObject("mnuItemInputExcel.Image")));
            this.mnuItemInputExcel.Name = "mnuItemInputExcel";
            this.mnuItemInputExcel.Size = new System.Drawing.Size(154, 22);
            this.mnuItemInputExcel.Text = "Nhập từ Excel";
            this.mnuItemInputExcel.Click += new System.EventHandler(this.mnuItemInputExcel_Click);
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Preview.Options.UseFont = true;
            this.grdViewData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.Row.Options.UseFont = true;
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewData.OptionsNavigation.UseTabKey = false;
            this.grdViewData.OptionsView.ShowAutoFilterRow = true;
            this.grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewData.OptionsView.ShowFooter = true;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            // 
            // chkActiveItem
            // 
            this.chkActiveItem.AutoHeight = false;
            this.chkActiveItem.Name = "chkActiveItem";
            this.chkActiveItem.ValueChecked = ((short)(1));
            this.chkActiveItem.ValueUnchecked = ((short)(0));
            // 
            // chkSystemItem
            // 
            this.chkSystemItem.AutoHeight = false;
            this.chkSystemItem.Name = "chkSystemItem";
            this.chkSystemItem.ValueChecked = ((short)(1));
            this.chkSystemItem.ValueUnchecked = ((short)(0));
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(814, 534);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(95, 25);
            this.btnUpdate.TabIndex = 9;
            this.btnUpdate.Text = "   Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportExcel.Location = new System.Drawing.Point(1, 534);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(129, 25);
            this.btnExportExcel.TabIndex = 28;
            this.btnExportExcel.Text = "   Xuất excel mẫu";
            this.btnExportExcel.UseVisualStyleBackColor = true;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnImportExcel
            // 
            this.btnImportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnImportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnImportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImportExcel.Location = new System.Drawing.Point(136, 534);
            this.btnImportExcel.Name = "btnImportExcel";
            this.btnImportExcel.Size = new System.Drawing.Size(119, 25);
            this.btnImportExcel.TabIndex = 29;
            this.btnImportExcel.Text = "   Nhập từ excel";
            this.btnImportExcel.UseVisualStyleBackColor = true;
            this.btnImportExcel.Click += new System.EventHandler(this.btnImportExcel_Click);
            // 
            // frmProductIMEI_OrderIndexEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 562);
            this.Controls.Add(this.btnExportExcel);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnImportExcel);
            this.Controls.Add(this.grdData);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmProductIMEI_OrderIndexEdit";
            this.ShowIcon = false;
            this.Text = "Cập nhật mức ưu tiên xuất kho theo mã sản phẩm lấy IMEI tự động ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmProductIMEI_OrderIndexEdit_FormClosing);
            this.Load += new System.EventHandler(this.frmIMEISalesInfoManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            this.mnuIMEI.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActiveItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSystemItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        //private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repChkIsReviewed;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkActiveItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkSystemItem;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ContextMenuStrip mnuIMEI;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcel;
        private System.Windows.Forms.ToolStripMenuItem mnuItemInputExcel;
        private System.Windows.Forms.Button btnExportExcel;
        private System.Windows.Forms.Button btnImportExcel;

    }
}