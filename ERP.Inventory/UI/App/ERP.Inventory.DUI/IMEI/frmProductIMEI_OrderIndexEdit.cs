﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using ERP.Inventory.PLC.SM;
using ERP.Inventory.PLC.SM.WSSequenceIMEI;
using ERP.Inventory.DUI.IMEISale;
using System.IO;

namespace ERP.Inventory.DUI.IMEI
{
    /// <summary>
    /// Cập nhật mức ưu tiên xuất kho theo mã sản phẩm lấy IMEI tự động 
    /// </summary>
    public partial class frmProductIMEI_OrderIndexEdit : Form
    {
        #region Variable

        private PLC.SM.PLCSequenceIMEI objPLCSequenceIMEI = new PLC.SM.PLCSequenceIMEI();
        #endregion

        #region Constructor
        public frmProductIMEI_OrderIndexEdit()
        {
            InitializeComponent();
        }
        #endregion

        #region Event
        private void frmIMEISalesInfoManager_Load(object sender, EventArgs e)
        {
            InitControl();
            FormatGrid();

        }

        private void cmdExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(this.grdData);
        }

        #endregion

        #region Method

        private void InitControl()
        {
            string[] fieldNames = new string[] { "OrderIndex", "ProductID", "ProductName", "IMEI", "IsSuccess" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);          
        }


        private bool FormatGrid()
        {
            try
            {
                grdViewData.Columns["IMEI"].Caption = "IMEI";
                grdViewData.Columns["ProductID"].Caption = "Mã sản phẩm";
                grdViewData.Columns["ProductName"].Caption = "Tên sản phẩm";
                grdViewData.Columns["OrderIndex"].Caption = "Mức ưu tiên";
                grdViewData.Columns["IsSuccess"].Caption = "Thành công";
                
                grdViewData.OptionsView.ColumnAutoWidth = false;
                grdViewData.Columns["IMEI"].Width = 200;
                grdViewData.Columns["ProductID"].Width = 200;
                grdViewData.Columns["ProductName"].Width = 300;
                grdViewData.Columns["OrderIndex"].Width = 100;
                grdViewData.Columns["IsSuccess"].Width = 100;


                grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewData.OptionsView.ShowAutoFilterRow = true;
                grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewData.OptionsView.ShowFooter = false;
                grdViewData.OptionsView.ShowGroupPanel = false;
                grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewData.ColumnPanelRowHeight = 40;

                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTxtOrderIndex = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                repTxtOrderIndex.MaxLength = 10;
                repTxtOrderIndex.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
                repTxtOrderIndex.Mask.EditMask = @"[1-9][0-9]{0,8}";//@"\d{1,9}";

                grdViewData.Columns["OrderIndex"].ColumnEdit = repTxtOrderIndex;
                repTxtOrderIndex.Click += new EventHandler(repTxtOrderIndex_Click);       
                repTxtOrderIndex.KeyUp += new KeyEventHandler(repTxtOrderIndex_KeyUp);
                repTxtOrderIndex.Leave += new EventHandler(repTxtOrderIndex_Leave);
                grdViewData.Columns["OrderIndex"].OptionsColumn.AllowEdit = true;
                grdViewData.Columns["IsSuccess"].Visible = false;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách thông tin bán hàng của lô IMEI");
                SystemErrorWS.Insert("Định dạng lưới danh sách thông tin bán hàng của lô IMEI", objExce.ToString(), this.Name + " -> FormatGrid", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private int intOrderBefore = 0;
        private void repTxtOrderIndex_Click(object sender, EventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0)
            {
                return;
            }
            DevExpress.XtraEditors.TextEdit txt = (DevExpress.XtraEditors.TextEdit)sender;
            if (string.IsNullOrEmpty(txt.Text.Trim()))
            {
                return;
            }
            intOrderBefore = Convert.ToInt32(txt.Text.Trim());
        }

        private void repTxtOrderIndex_Leave(object sender, EventArgs e)
        {
            //if (grdViewData.FocusedRowHandle < 0)
            //{
            //    return;
            //}
            //DevExpress.XtraEditors.TextEdit txt = (DevExpress.XtraEditors.TextEdit)sender;
            //string strIMEI = grdViewData.GetFocusedRowCellValue("IMEI").ToString().Trim();
            //if (string.IsNullOrEmpty(txt.Text.Trim()))
            //{
            //    return;
            //}
            //int intOrderIndex = Convert.ToInt32(txt.Text.Trim());
            //List<SequenceIMEI> dtbTable = (List<SequenceIMEI>)grdData.DataSource;
            //if (intOrderIndex > intOrderBefore)
            //{
            //    foreach (var item in dtbTable)
            //    {
            //        int intOrderIndexTemp = Convert.ToInt32(item["OrderIndex"]);
            //        string strIMEITemp = item["IMEI"].ToString().Trim();
            //        if (strIMEITemp == strIMEI)
            //        {
            //           // item["ISEDIT"] = 1;
            //        }
            //        else if (intOrderIndexTemp <= intOrderIndex && intOrderIndexTemp > intOrderBefore)
            //        {
            //            intOrderIndexTemp--;
            //            item["OrderIndex"] = intOrderIndexTemp;
            //           // item["ISEDIT"] = 1;
            //        }

            //    }
            //}
            //else
            //{
            //    foreach (DataRow item in dtbTable.Rows)
            //    {
            //        int intOrderIndexTemp = Convert.ToInt32(item["OrderIndex"]);
            //        string strIMEITemp = item["IMEI"].ToString().Trim();
            //        if (strIMEITemp == strIMEI)
            //        {
            //            //item["ISEDIT"] = 1;
            //        }
            //        else if (intOrderIndexTemp >= intOrderIndex && intOrderIndexTemp < intOrderBefore)
            //        {
            //            intOrderIndexTemp++;
            //            item["OrderIndex"] = intOrderIndexTemp;
            //            //item["ISEDIT"] = 1;
            //        }
            //    }
            //}

            //dtbTable.AcceptChanges();
            //dtbTable.DefaultView.Sort = "OrderIndex asc";
            //grdData.DataSource = dtbTable;
        }

        private void repTxtOrderIndex_KeyUp(object sender, KeyEventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0)
            {
                return;
            }
            if (e.KeyCode == Keys.Enter)
            {
                DevExpress.XtraEditors.TextEdit txt = (DevExpress.XtraEditors.TextEdit)sender;
                string strIMEI = grdViewData.GetFocusedRowCellValue("IMEI").ToString().Trim();
                if (string.IsNullOrEmpty(txt.Text.Trim()))
                {
                    return;
                }
                int intOrderIndex = Convert.ToInt32(txt.Text.Trim());
                DataTable dtbTable = (DataTable)grdData.DataSource;
                foreach (DataRow item in dtbTable.Rows)
                {
                    int intOrderIndexTemp = Convert.ToInt32(item["OrderIndex"]);
                    string strIMEITemp = item["IMEI"].ToString().Trim();
                    if (intOrderIndexTemp >= intOrderIndex && intOrderIndexTemp != 100000 && strIMEITemp != strIMEI)
	                {
                        intOrderIndexTemp++;
                        item["OrderIndex"] = intOrderIndexTemp;
	                }
                    
                }
                dtbTable.DefaultView.Sort = "OrderIndex asc";
                grdData.DataSource = dtbTable;
            }
            
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            SequenceIMEI[] lstRewardPromotion_IMEI = ((List<SequenceIMEI>)grdData.DataSource).ToArray();
            objPLCSequenceIMEI.Insert(ref lstRewardPromotion_IMEI);
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            else
            {
                MessageBoxObject.ShowInfoMessage(this, "Hoàn thành cập nhật mức ưu tiên theo IMEI!");
                grdData.DataSource = lstRewardPromotion_IMEI.ToList();
                grdViewData.Columns["IsSuccess"].Visible = true;
                btnUpdate.Enabled = false;
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.GridControl grdTemp = new DevExpress.XtraGrid.GridControl();
            DevExpress.XtraGrid.Views.Grid.GridView grvTemp = new DevExpress.XtraGrid.Views.Grid.GridView();
            grdTemp.MainView = grvTemp;
            string[] fieldNames = new string[] { "Mức ưu tiên", "Mã sản phẩm", "IMEI"};
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdTemp, fieldNames);
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdTemp);
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            mnuItemInputExcel_Click(null, null);
        }

        private bool ValidateData(ref DataTable dt)
        {
            if (dt == null || dt.Rows.Count == 0) return false;
            if (dt.Columns.Count != 3)
            {
                MessageBoxObject.ShowWarningMessage(this, "File excel không đúng định dạng!");
                return false;
            }
            //dt.Rows.RemoveAt(0);
            //int i = 0;
            //dt.Columns[i++].ColumnName = "ORDERINDEX";
            //dt.Columns[i++].ColumnName = "PRODUCTID";
            //dt.Columns[i++].ColumnName = "IMEI";         

            //DataColumn dtcProductName = new DataColumn("PRODUCTNAME", typeof(string));
            //dt.Columns.Add(dtcProductName);
            //DataColumn dtcIsError = new DataColumn("ISERROR", typeof(bool));
            //dtcIsError.DefaultValue = false;
            //dt.Columns.Add(dtcIsError);
            //DataColumn dtcNote = new DataColumn("NOTE", typeof(string));
            //dt.Columns.Add(dtcNote);
            //dt.Columns["PRODUCTNAME"].SetOrdinal(dt.Columns["PRODUCTID"].Ordinal + 1);

            //ERP.MasterData.PLC.MD.WSProduct.Product objProduct = new MasterData.PLC.MD.WSProduct.Product();
            //foreach (DataRow row in dt.Rows)
            //{
            //    //RewardPoint
            //    string strOrderIndex = Convert.ToString(row["ORDERINDEX"]).Trim();
            //    if (strOrderIndex == string.Empty)
            //    {
            //        row["IsError"] = true;
            //        row["NOTE"] = "Mức ưu tiên không được để trống";
            //        continue;
            //    }
            //    ulong uIntRewardPoint = 0;
            //    bool bolResult = ulong.TryParse(strOrderIndex, out uIntRewardPoint);
            //    if (!bolResult)
            //    {
            //        row["IsError"] = true;
            //        row["NOTE"] = "Mức ưu tiên không đúng định dạng";
            //        continue;
            //    }
            //    row["ORDERINDEX"] = uIntRewardPoint;

            //    //Product
            //    string strProductID = Convert.ToString(row["PRODUCTID"]).Trim();
            //    if (strProductID == string.Empty)
            //    {
            //        row["IsError"] = true;
            //        row["NOTE"] = "Mã sản phẩm không được để trống ";
            //        continue;
            //    }
            //    objProduct = new MasterData.PLC.MD.WSProduct.Product();
            //    objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
            //    if (objProduct == null || objProduct.ProductID == null)
            //    {
            //        row["IsError"] = true;
            //        row["NOTE"] = "Mã sản phẩm không tồn tại ";
            //        continue;
            //    }
            //    row["PRODUCTNAME"] = objProduct.ProductName;
                              
            //    //IMEI
            //    string strIMEI = Convert.ToString(row["IMEI"]).Trim();
            //    if (strIMEI == string.Empty)
            //    {
            //        row["IsError"] = true;
            //        row["NOTE"] = "IMEI không được để trống";
            //        continue;
            //    }
            //    row["IMEI"] = strIMEI;

            //}
            //dt.AcceptChanges();
            return true;
        }

        private void mnuItemInputExcel_Click(object sender, EventArgs e)
        {
            if (grdViewData.RowCount > 0 && !grdViewData.Columns["IsSuccess"].Visible)
            {
              if(MessageBox.Show("Dữ liệu trên lưới chưa được cập nhật. Bạn có muốn nhập dữ liệu khác không?", "Thống báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.No)
              {
                return;
              }
            }
            OpenFileDialog Openfile = new OpenFileDialog();
            Openfile.Filter = "Excel 2003|*.xls|Excel New|*.xlsx";
            if (Openfile.ShowDialog() == DialogResult.Cancel) return;
            try
            {
                DataTable dt = new DataTable();
                dt = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(Openfile.FileName);
                if (!ValidateData(ref dt))
                    return;
                frmProductIMEI_OrderIndexExcel frm = new frmProductIMEI_OrderIndexExcel();
                frm.SourceTable = dt;
                frm.ShowDialog();
                List<SequenceIMEI> lstRewardPromotion_IMEI = new List<SequenceIMEI>();
                if (frm.IsUpdate)
                {
                    foreach (DataRow row in frm.SourceTable.Rows)
                    {
                        if (!Convert.ToBoolean(row["IsError"]))
                        {
                            //SequenceIMEI objRewardPromotion_IMEI = lstRewardPromotion_IMEI.Find(p => p.ProductID == strProductID && p.IMEI == strIMEI);
                            //if (objRewardPromotion_IMEI == null)
                            {
                                SequenceIMEI objRewardPromotion_IMEI = new SequenceIMEI();
                                objRewardPromotion_IMEI.ProductID = Convert.ToString(row["ProductID"]).Trim();;
                                objRewardPromotion_IMEI.ProductName = Convert.ToString(row["ProductName"]).Trim();
                                objRewardPromotion_IMEI.IMEI = Convert.ToString(row["IMEI"]).Trim();;
                                objRewardPromotion_IMEI.OrderIndex = Convert.ToInt32(row["OrderIndex"]);
                                objRewardPromotion_IMEI.CreatedUser = Library.AppCore.SystemConfig.objSessionUser.UserName;
                                lstRewardPromotion_IMEI.Add(objRewardPromotion_IMEI);
                            }  
                        }
                    }
                    grdData.DataSource = lstRewardPromotion_IMEI;
                    grdViewData.Columns["IsSuccess"].Visible = false;
                    btnUpdate.Enabled = lstRewardPromotion_IMEI.Count > 0;
                }
            }
            catch (Exception objex)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin từ file Excel!", objex.Message.ToString());
            }
        }

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            btnExportExcel_Click(null, null);
        }

        private void frmProductIMEI_OrderIndexEdit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(this, "Bạn có chắc muốn đóng màn hình này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                e.Cancel = true;
        }
       
    }
}
