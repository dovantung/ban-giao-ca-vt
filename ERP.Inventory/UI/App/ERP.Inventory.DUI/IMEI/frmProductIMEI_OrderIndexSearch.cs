﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using DevExpress.XtraGrid.Views.Grid;
using ERP.MasterData.DUI.Common;
using Library.AppCore;
using Library.AppCore.LoadControls;
using System.Threading;
using Library.AppCore.DataSource.FilterObject;

namespace ERP.Inventory.DUI.IMEI
{
    public partial class frmProductIMEI_OrderIndexSearch : Form
    {
        /// <summary>
        /// Created by : Cao Hữu Vũ Lam
        /// Date : 15/05/2013
        /// Mức ưu tiên xuất kho theo mã sản phẩm lấy IMEI tự động
        /// </summary>

        #region Variable

        //private string strPermission_Search = "ProductPrice_IMEI_Search";
        private string strPermission_ExportExcel = "SM_SEQUENCEIMEI_ExportExcel";
        private bool bolPermissionExportExcel = false;
        
        private bool bolIsLoadComplete = false;
        private DataTable dtbResource = null;
        private List<string> fieldNames = null;
        #endregion

        #region Constructor

        public frmProductIMEI_OrderIndexSearch()
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        private void InitControl()
        {
            string[] fieldNames = new string[] { "ORDERINDEX", "PRODUCTID", "PRODUCTNAME", "IMEI", "CREATEDDATE", "SYNCHDATE", "ISINSTOCK" };
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
        }

        private void LoadCombobox()
        {
            try
            {
                SetDataSource.SetMainGroup(this, cboMainGroup, true, Library.AppCore.Constant.EnumType.IsRequestIMEIType.REQUEST_IMEI,
                   Library.AppCore.Constant.EnumType.IsServiceType.ALL, Library.AppCore.Constant.EnumType.MainGroupPermissionType.VIEWREPORT);
                cboSubGroupIDList.InitControl(true, 0);
                cboBrandIDList.InitControl(true, 0);
                cboProductList1.InitControl();
                cboProductList1.Enabled = false;
                cboIsInStock.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, "Lỗi nạp combobox", ex.ToString());
            }
        }
                 
        private bool CheckInput()
        {
            if (!ERP.MasterData.DUI.Common.CommonFunction.EndDateValidation(dtpDateFrom.Value, dtpDateTo.Value, false))
            {
                dtpDateTo.Focus();
                MessageBox.Show(this, "Đến ngày không thể nhỏ hơn từ ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            
            DateTime dtFromDate = dtpDateFrom.Value;
            dtFromDate = dtFromDate.AddDays(30);
            DateTime dtToDate = dtpDateTo.Value;
            if (dtFromDate.CompareTo(dtToDate) < 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Chỉ cho tìm kiếm tối đa 31 ngày!");
                dtpDateTo.Focus();
                return false;
            }
            if (cboMainGroup.SelectedIndex == 0)
            {
                cboMainGroup.Focus();
                MessageBox.Show(this, "Vui lòng chọn ngành hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }


        private void SearchData()
        {
            int intMainGroupID = Convert.ToInt32(cboMainGroup.SelectedValue);
            string strSubGroupIDList = cboSubGroupIDList.SubGroupIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
            string strBrandIDList = cboBrandIDList.BrandIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
            string strProductIDList = cboProductList1.ProductIDList.Replace("><", ",").Replace("<", "").Replace(">", "");
            object[] objKeywords = new object[] 
                {                     
                    "@FromDate", dtpDateFrom.Checked ? Convert.ToDateTime(dtpDateFrom.Value) : Convert.ToDateTime("1800-01-01"),
                    "@ToDate", dtpDateTo.Checked ? Convert.ToDateTime(dtpDateTo.Value) : DateTime.Now,
                    "@ISINSTOCK", cboIsInStock.SelectedIndex - 1,
                    "@MainGroupID", intMainGroupID,
                    "@SubGroupIDList", strSubGroupIDList,
                    "@BrandIDList", strBrandIDList,
                    "@ProductIDList", strProductIDList
                };
            ERP.Inventory.PLC.SM.PLCSequenceIMEI objPLCSequenceIMEI = new PLC.SM.PLCSequenceIMEI();
            objPLCSequenceIMEI.SearchData(ref dtbResource, objKeywords);
            //ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
            //dtbResource = objPLCReportDataSource.GetDataSource("SM_SEQUENCEIMEI_SRH", objKeywords);

            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
            bolIsLoadComplete = true;
        }


        private bool FormatGrid()
        {
            try
            {
                grdViewData.Columns["IMEI"].Caption = "IMEI";
                grdViewData.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
                grdViewData.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
                grdViewData.Columns["ORDERINDEX"].Caption = "Mức ưu tiên";
                grdViewData.Columns["ISINSTOCK"].Caption = "Tồn kho";
                grdViewData.Columns["CREATEDDATE"].Caption = "Ngày thiết lập";
                grdViewData.Columns["SYNCHDATE"].Caption = "Thời gian đồng bộ";

                grdViewData.OptionsView.ColumnAutoWidth = false;
                grdViewData.Columns["IMEI"].Width = 200;
                grdViewData.Columns["PRODUCTID"].Width = 120;
                grdViewData.Columns["PRODUCTNAME"].Width = 200;
                grdViewData.Columns["ORDERINDEX"].Width = 100;
                grdViewData.Columns["ISINSTOCK"].Width = 100;
                grdViewData.Columns["CREATEDDATE"].Width = 120;
                grdViewData.Columns["SYNCHDATE"].Width = 100;

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsActive = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsActive.ValueChecked = ((decimal)(1));
                chkCheckBoxIsActive.ValueUnchecked = ((decimal)(0));
                grdViewData.Columns["ISINSTOCK"].ColumnEdit = chkCheckBoxIsActive;

                grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewData.OptionsView.ShowAutoFilterRow = true;
                grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewData.OptionsView.ShowFooter = false;
                grdViewData.OptionsView.ShowGroupPanel = false;
                grdViewData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grdViewData.ColumnPanelRowHeight = 40;

                grdViewData.Columns["CREATEDDATE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                grdViewData.Columns["CREATEDDATE"].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
                grdViewData.Columns["SYNCHDATE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                grdViewData.Columns["SYNCHDATE"].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách thông tin bán hàng của lô IMEI");
                SystemErrorWS.Insert("Định dạng lưới danh sách thông tin bán hàng của lô IMEI", objExce.ToString(), this.Name + " -> FormatGrid", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }
        
        #endregion

        #region Event

        private void frmSearchDetailExpense_Load(object sender, EventArgs e)
        {
            bolPermissionExportExcel = SystemConfig.objSessionUser.IsPermission(strPermission_ExportExcel);
            InitControl();
            LoadCombobox();
            FormatGrid();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            pnSearchStatus.Visible = true;
            pnSearchStatus.Refresh();
            EnableControl(false);
            SearchData();
            return;
            Thread objThread = new Thread(new ThreadStart(SearchData));
            objThread.Start();
            
        }

        private void cboMainGroupID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int intMainGroupID = -1;
            if (cboMainGroup.SelectedIndex > 0)
                intMainGroupID = Convert.ToInt32(cboMainGroup.SelectedValue);
            cboProductList1.Enabled = cboMainGroup.SelectedIndex > 0;
            cboSubGroupIDList.InitControl(true, intMainGroupID);
            cboBrandIDList.InitControl(true, intMainGroupID);
            cboSubGroupIDList_SelectionChangeCommitted(null, null);
        }

        private void cboSubGroupIDList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cboProductList1.InitControl(Convert.ToInt32(cboMainGroup.SelectedValue),
                cboSubGroupIDList.SubGroupIDList, cboBrandIDList.BrandIDList);
        }

        private void cboBrandIDList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cboSubGroupIDList_SelectionChangeCommitted(null, null);
        }

        private void cmd_Grid_Excel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);

        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }

        private void tmrFormatFlex_Tick(object sender, EventArgs e)
        {
            if (bolIsLoadComplete)
            {
                bolIsLoadComplete= false;
                grdData.DataSource = dtbResource;
                FormatGrid();
                btnExcel.Enabled = (bolPermissionExportExcel && dtbResource != null && dtbResource.Rows.Count > 0);
                pnSearchStatus.Visible = false;
                EnableControl(true);
            }            
        }

        private void EnableControl(bool bolIsEnable)
        {
            gpSearch.Enabled = bolIsEnable;
        }

        private void cbo_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            if (e.DisplayText == String.Empty)
                e.DisplayText = "Tất cả";
        }

        private void grdData_Click(object sender, EventArgs e)
        {

        }

        private void cboAccount_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            if (e.DisplayText == String.Empty)
                e.DisplayText = "Tất cả";
        }

        #endregion

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void cboSubGroupIDList_Load(object sender, EventArgs e)
        {

        }

        private void frmProductIMEI_OrderIndexSearch_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(this, "Bạn có chắc muốn đóng màn hình này không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                e.Cancel = true;
        }

        
       
 
    }
}
