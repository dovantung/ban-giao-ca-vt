﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using Library.AppCore.CustomControls.GridControl;

namespace ERP.Inventory.DUI.IMEI
{
    public partial class frmProductIMEIHistory : Form
    {

        #region Khai báo biến
        private PLC.IMEIChange.PLCIMEIChange objPLCIMEIChange = new PLC.IMEIChange.PLCIMEIChange();
        private ERP.Inventory.PLC.Input.PLCInputVoucher objPLCInputVoucher = new PLC.Input.PLCInputVoucher();
        private ERP.MasterData.PLC.MD.WSArea.ResultMessage objResultMessage = null;
        bool bolIsEnter = false;
        string strIMEIExist = string.Empty;
        private System.Threading.Thread objThread;
        private DataTable tblIMEIChange = null;
        private string strIMEIChange = string.Empty;
        private string strCustomerIDInput = string.Empty;
        private bool bolViewCustomerInput = false;
        #endregion

        #region Constructor
        public frmProductIMEIHistory()
        {
            InitializeComponent();
        }
        #endregion

        #region Các hàm sự kiện

        private void frmProductIMEIHistory_Activated(object sender, EventArgs e)
        {
            txtIMEI.Focus();
        }

        private void frmProductIMEIHistory_Load(object sender, EventArgs e)
        {
            InitControl();
        }

        private bool CheckInput()
        {
            if (txtIMEI.Text.Trim().Length == 0)
            {
                MessageBox.Show(this, "Vui lòng nhập IMEI cần tìm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtIMEI.Focus();
                return false;
            }
            return true;
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            if (!CheckInput()) return;
            LoadData();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }

        private void txtIMEI_KeyUp(object sender, KeyEventArgs e)
        {
            if (bolIsEnter)
            {
                bolIsEnter = false;
                return;
            }

            if (e.KeyCode == Keys.Enter)
            {
                bolIsEnter = true;
                if (!CheckInput()) return;
                LoadData();
            }
        }

        private void grdViewData_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (grdViewData.FocusedRowHandle < 0)
                    return;
                DataRow drFocus = (DataRow)grdViewData.GetDataRow(grdViewData.FocusedRowHandle);
                if (drFocus != null)
                {
                    if (!Convert.ToBoolean(drFocus["IsOutPutVoucher"]))
                    {

                        Input.frmInputVoucher frm = new Input.frmInputVoucher();
                        if (!string.IsNullOrEmpty(strCustomerIDInput) && !Convert.ToBoolean(drFocus["IsViewCustomerInput"]))
                            frm.evtLoadByProductIMEIHistory += new EventHandler(frm_evtLoadByProductIMEIHistory);
                        frm.InputVoucherID = drFocus["InputVoucherID"].ToString();
                        frm.IsEdit = true;
                        frm.LoadControl();
                        frm.ShowDialog();
                    }
                    else
                    {
                        string strOutputTypeID = AppConfig.GetStringConfigValue("PM_STORECHANGE_OUTPUTTYPEID").Replace(" ", "");
                        string[] strInputTypeName = drFocus["InputTypeName"].ToString().Split('-');
                        string[] strListID = strOutputTypeID.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        if (strListID.Contains(strInputTypeName[0].Trim()))
                        {
                            //MessageBoxObject.ShowWarningMessage(this, "Mã đơn hàng không có, nên không thể xem!");                        
                            Output.frmOutputVoucher frmOutputVoucher1 = new Output.frmOutputVoucher();
                            frmOutputVoucher1.strOutputVoucherID = drFocus["InputVoucherID"].ToString();
                            frmOutputVoucher1.ShowDialog();
                            return;
                        }
                        else
                            if (drFocus["ORDERID"].ToString().Trim().Length > 0 && drFocus["ORDERID"].ToString().Trim().Contains("SO"))
                            {
                                SalesAndServices.SaleOrders.DUI.frmSaleOrders frm = new SalesAndServices.SaleOrders.DUI.frmSaleOrders(drFocus["ORDERID"].ToString().Trim(), true);
                                frm.ShowDialog();
                            }                      
                    }
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Không xem được chi tiết chứng từ", objExce, ERP.MasterData.DUI.MasterData_Globals.ModuleName);
                MessageBox.Show(this, "Không xem được chi tiết chứng từ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frm_evtLoadByProductIMEIHistory(object sender, EventArgs e)
        {
            if (sender != null && e != null)
            {
                Input.frmInputVoucher frmInput = (Input.frmInputVoucher)sender;
                frmInput.Tag = "NotVisibleCustomer=True&NotUpdateInput=True";
            }
        }

        #endregion

        #region Các hàm hỗ trợ

        private void InitControl()
        {
            cboType.SelectedIndex = 0;
            cboInput.SelectedIndex = 0;
            string[] fieldNames = new string[] {"IMEI","INPUTVOUCHERID","INPUTTYPENAME","INVOICEID","INVOICESYMBOL","INPUTDATE","STORENAME","USERNAME",
                "PRODUCTID","PRODUCTNAME","PRODUCTSTATUSNAME","ISNEW","ISHASWARRANTY","ISSHOWPRODUCT","CUSTOMERNAME","CUSTOMERADDRESS","CUSTOMERPHONE","FIRSTINPUTTYPENAME",
                "ISOUTPUTVOUCHER","ISRETURNWITHFEE","ISWATING","ISCHECKREALINPUT","COSTPRICE","INPUTCONTENT"};
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, true, true, true, DevExpress.Utils.HorzAlignment.Center, DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            FormatGrid();
            lblNoteByIMEI.Text = "";
            tblIMEIChange = objPLCIMEIChange.SearchData(new object[] { "@FromDate", new DateTime(2015, 7, 1), "@ToDate", Library.AppCore.Globals.GetServerDateTime().AddDays(1) });
            bolViewCustomerInput = SystemConfig.objSessionUser.IsPermission("PM_HISTORYIMEI_VIEWCUSTOMER_INPUT");
            strCustomerIDInput = AppConfig.GetStringConfigValue("PM_HISTORYIMEI_CUSTOMERID_INPUT");
            if (string.IsNullOrEmpty(strCustomerIDInput))
                strCustomerIDInput = "";
            else
                strCustomerIDInput = strCustomerIDInput.Trim();
        }

        private void FormatGrid()
        {
            //Format chi nhánh
            FormatGridcontrol.CurrentInstance.SetColumnCaption(grdViewData,
                "IMEI", "IMEI",
                "INPUTVOUCHERID", "Mã chứng từ liên quan",
                "INPUTTYPENAME", "Hình thức nhập/xuất",
                "INVOICEID", "Số hóa đơn",
                "INVOICESYMBOL", "Ký hiệu hóa đơn",
                "INPUTDATE", "Ngày nhập/xuất",
                "STORENAME", "Tên kho",
                "USERNAME", "Người tạo",
                "PRODUCTID", "Mã sản phẩm",
                "PRODUCTNAME", "Tên sản phẩm",
                "PRODUCTSTATUSNAME", "Trạng thái tồn",
                "ISNEW", "Sản phẩm mới",
                "ISHASWARRANTY", "Có bảo hành",
                "ISSHOWPRODUCT", "Hàng trưng bày",
                "CUSTOMERNAME", "Khách hàng",
                "CUSTOMERADDRESS", "Địa chỉ",
                "CUSTOMERPHONE", "Điện thoại",
                "FIRSTINPUTTYPENAME", "Hình thức nhập đầu tiên",
                "ISOUTPUTVOUCHER", "Phiếu xuất",
                "ISRETURNWITHFEE", "NT có thu phí",
                "ISWATING", "Chờ xuất/chờ nhập",
                "ISCHECKREALINPUT", "Đã nhập/xuất hàng",

                "COSTPRICE", "Giá vốn",
                "INPUTCONTENT", "Nội dung");
            FormatGridcontrol.CurrentInstance.SetColumnIsCheckBox(grdViewData, "ISNEW", "ISHASWARRANTY", "ISSHOWPRODUCT", "ISOUTPUTVOUCHER", "ISRETURNWITHFEE", "ISWATING", "ISCHECKREALINPUT");
            FormatGridcontrol.CurrentInstance.SetColumnAllowEdit(grdViewData, false, "IMEI", "INPUTVOUCHERID", "INPUTTYPENAME", "INVOICEID", "INVOICESYMBOL", "STORENAME",
                "INPUTDATE", "USERNAME", "PRODUCTID", "PRODUCTNAME", "PRODUCTSTATUSNAME", "ISNEW", "ISHASWARRANTY", "ISSHOWPRODUCT", "CUSTOMERNAME", "CUSTOMERADDRESS", "CUSTOMERPHONE", "FIRSTINPUTTYPENAME",
                "ISOUTPUTVOUCHER", "ISRETURNWITHFEE", "ISWATING", "ISCHECKREALINPUT", "COSTPRICE", "INPUTCONTENT");
            //FormatGridcontrol.CurrentInstance.SetColumnVisible(grdViewData, false, "Staffuser", "ORDERID");
            grdViewData.ColumnPanelRowHeight = 25;
            grdViewData.Columns["IMEI"].Width = 160;
            grdViewData.Columns["INPUTVOUCHERID"].Width = 180;
            grdViewData.Columns["INPUTTYPENAME"].Width = 250;
            grdViewData.Columns["INVOICEID"].Width = 100;
            grdViewData.Columns["INVOICESYMBOL"].Width = 150;
            grdViewData.Columns["INPUTDATE"].Width = 120;
            grdViewData.Columns["STORENAME"].Width = 160;
            grdViewData.Columns["USERNAME"].Width = 200;
            grdViewData.Columns["PRODUCTID"].Width = 200;
            grdViewData.Columns["PRODUCTNAME"].Width = 250;
            grdViewData.Columns["PRODUCTSTATUSNAME"].Width = 120;
            grdViewData.Columns["ISNEW"].Width = 100;
            grdViewData.Columns["ISHASWARRANTY"].Width = 160;
            grdViewData.Columns["ISSHOWPRODUCT"].Width = 180;
            grdViewData.Columns["CUSTOMERNAME"].Width = 200;
            grdViewData.Columns["CUSTOMERADDRESS"].Width = 220;
            grdViewData.Columns["CUSTOMERPHONE"].Width = 130;
            grdViewData.Columns["FIRSTINPUTTYPENAME"].Width = 250;
            grdViewData.Columns["ISOUTPUTVOUCHER"].Width = 100;
            grdViewData.Columns["ISRETURNWITHFEE"].Width = 100;
            grdViewData.Columns["ISWATING"].Width = 100;
            grdViewData.Columns["ISCHECKREALINPUT"].Width = 140;
            grdViewData.Columns["INPUTCONTENT"].Width = 250;
            grdViewData.Columns["ISRETURNWITHFEE"].ToolTip = "Nhập trả có thu phí";

            grdViewData.Columns["INPUTDATE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            grdViewData.Columns["INPUTDATE"].DisplayFormat.FormatString = "dd/MM/yyy HH:mm";
            grdViewData.Columns["COSTPRICE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            grdViewData.Columns["COSTPRICE"].DisplayFormat.FormatString = "#,##0";
            grdViewData.Columns["COSTPRICE"].Visible = SystemConfig.objSessionUser.IsPermission("PRODUCTIMEIHISTORY_COSTPRICEVIEW");

            grdViewData.OptionsView.ShowAutoFilterRow = true;
            grdViewData.OptionsView.ShowFooter = false;
        }

        /// <summary>
        /// Hàm load dữ liệu lên lưới
        /// </summary>
        private void LoadData()
        {
            try
            {
                grdData.DataSource = null;
                lblNoteByIMEI.Text = string.Empty;
                object[] objKeyword = new object[]
                {
                    "@IMEI", txtIMEI.Text.Trim(),
                    "@IsIncludeDeleted", chkIsIncludeDeleted.Checked,
                    "@IsOutPutVoucher",cboType.SelectedIndex-1,
                    "@IsCheckRealInput",cboInput.SelectedIndex-1,
                    "@ViewCustomerInput", bolViewCustomerInput,
                    "@CustomerIDInput", strCustomerIDInput
                };
                DataTable tblProductIMEIHistory = objPLCInputVoucher.SearchDataProductIMEIHistory(objKeyword);
                if (tblProductIMEIHistory == null || tblProductIMEIHistory.Rows.Count == 0)
                {
                    if (string.IsNullOrEmpty(strIMEIChange))
                    {
                        DataRow[] drCheckIMEIChange = tblIMEIChange.Select("TOIMEI='" + txtIMEI.Text.Trim() + "'");
                        if (drCheckIMEIChange.Length > 0)
                        {
                            strIMEIChange = drCheckIMEIChange[0]["FROMIMEI"].ToString().Trim();
                        }
                    }
                    else
                    {
                        if (txtIMEI.Text.Trim() != strIMEIChange)
                        {
                            DataRow[] drCheckIMEIChange = tblIMEIChange.Select("TOIMEI='" + txtIMEI.Text.Trim() + "'");
                            if (drCheckIMEIChange.Length > 0)
                            {
                                strIMEIChange = drCheckIMEIChange[0]["FROMIMEI"].ToString().Trim();
                            }
                        }
                    }
                    if ((!string.IsNullOrEmpty(strIMEIChange)) && txtIMEI.Text.Trim() != strIMEIChange)
                    {
                        if (MessageBox.Show(this, "IMEI " + txtIMEI.Text.Trim() + " được chuyển đổi từ IMEI " + strIMEIChange + "\nBạn có muốn xem lịch sử IMEI " + strIMEIChange + " không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            txtIMEI.Text = strIMEIChange;
                            LoadData();
                        }
                        return;
                    }
                    MessageBoxObject.ShowInfoMessage(this, "IMEI này không có trong hệ thống!");
                    txtIMEI.Focus();
                }
                else
                {
                    FormatRowResult(ref tblProductIMEIHistory);
                    grdData.DataSource = tblProductIMEIHistory;
                    strIMEIExist = txtIMEI.Text.Trim();
                    objThread = new System.Threading.Thread(new System.Threading.ThreadStart(SetNoteByIMEI));
                    objThread.Start();
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Không thể đưa dữ liệu IMEI lên lưới", objExce, ERP.MasterData.DUI.MasterData_Globals.ModuleName);
                MessageBox.Show(this, "Không thể đưa dữ liệu IMEI lên lưới", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void SetNoteByIMEI()
        {
            try
            {
                string strResult = objPLCInputVoucher.GetNoteByIMEI(strIMEIExist);
                if (strResult.Trim().Length > 0)
                    lblNoteByIMEI.Text = "( " + strResult.Trim() + " )";
                else
                    lblNoteByIMEI.Text = "";
            }
            catch
            {
                lblNoteByIMEI.Text = "";
            }
        }
        /// <summary>
        /// Hàm check Dữ liệu
        /// </summary>
        private void CheckIMEILockFIFO()
        {
            try
            {
                object[] objKeyword = new object[]
                {
                    "@IMEI", txtIMEI.Text.Trim()
                };
                DataTable tblProductIMEILock = objPLCInputVoucher.SearchDataProductIMEILockFIFO(objKeyword);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return;
                }

                if (tblProductIMEILock == null || tblProductIMEILock.Rows.Count == 0)
                {
                    MessageBox.Show(this, "IMEI không tồn tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (tblProductIMEILock.Rows.Count > 0)
                {
                    DataRow dr = tblProductIMEILock.Rows[0];
                    if (Convert.ToBoolean(dr["ISLOCK"]))
                    {
                        string strMes = "Thông tin IMEI:\n- Kho tồn: {0}\n- Tên sản phẩm: {1}\n- Trạng thái khóa: {2}\n- Ngày khóa: {3}\n- Người khóa: {4}";
                        strMes = string.Format(strMes, dr["STORENAME"], dr["PRODUCTNAME"], "ĐANG KHÓA FIFO", Convert.ToDateTime(dr["CREATEDDATE"]).ToString("dd/MM/yyyy"), (dr["CREATEDUSER"].ToString() + " - " + dr["FULLNAME"].ToString()));
                        MessageBox.Show(this, strMes, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        string strMes = "Thông tin IMEI:\n- Kho tồn: {0}\n- Tên sản phẩm: {1}\n- Trạng thái khóa: {2}";
                        strMes = string.Format(strMes, dr["STORENAME"], dr["PRODUCTNAME"], "KHÔNG KHÓA FIFO");
                        MessageBox.Show(this, strMes, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    return;
                }
            }
            catch (Exception objExce)
            {
                SystemErrorWS.Insert("Lỗi kiểm tra thông tin IMEI khóa FIFO", objExce, ERP.MasterData.DUI.MasterData_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi kiểm tra thông tin IMEI khóa FIFO", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        private void btnCheckLockFIFO_Click(object sender, EventArgs e)
        {
            if (txtIMEI.Text.Trim().Length == 0)
            {
                MessageBox.Show(this, "Vui lòng nhập IMEI cần kiểm tra khóa FIFO!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtIMEI.Focus();
                return;
            }
            CheckIMEILockFIFO();
        }

        private void frmProductIMEIHistory_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (objThread != null)
                objThread.Abort();
        }
        #region An them dieu kien hien thi row trên lưới
        private void FormatRowResult(ref DataTable dtbRes)
        {
            DataTable dtbTemp = dtbRes.Copy();
            var lstOV = dtbRes.Select("STORECHANGEID IS NOT NULL AND ISCHECKREALINPUT=0 AND IsOutPutVoucher=1");
            DataTable dtbOv = new DataTable();
            if (lstOV != null && lstOV.Count() > 0)
            {
                dtbOv = lstOV.CopyToDataTable();
                for (int i = 0; i < dtbOv.Rows.Count; i++)
                {
                    string strStoreChangeID = dtbOv.Rows[i]["STORECHANGEID"].ToString();
                    var lstIV = dtbTemp.Select("IsOutPutVoucher=0 AND STORECHANGEID='" + strStoreChangeID.Trim() + "'");
                    if (lstIV != null && lstIV.Count() > 0)
                    {
                        DataTable dtbIV = lstIV.CopyToDataTable();
                        foreach (DataRow r in dtbIV.Rows)
                        {
                            var rowDel = dtbTemp.Select("InputVoucherID='" + r["InputVoucherID"].ToString().Trim() + "'");
                            if (r.RowState != DataRowState.Deleted)
                            {
                                dtbTemp.Rows.Remove(rowDel[0]);
                            }
                        }
                        dtbTemp.AcceptChanges();
                    }
                }
            }
            dtbRes = dtbTemp;
        }
        #endregion
    }
}
