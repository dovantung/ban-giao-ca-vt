﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Library.AppCore.LoadControls;

namespace ERP.Inventory.DUI.IMEI
{
    public partial class frmProductIMEIHistory_Old : Form
    {
        // Tra cứu thông tin IMEI trên hệ thống cũ
        #region Variable
        private DataTable dtbResource = null;
        private ERP.Report.PLC.PLCReportDataSource objPLCReportDataSource = new Report.PLC.PLCReportDataSource();
        private bool bolIsLoadComplete = false;
        private string strPermissionView = "PM_HISTORYIMEI_VIEW";
        object[] objKeywords = null;
        #endregion
        #region Constructor
        public frmProductIMEIHistory_Old()
        {
            InitializeComponent();
        }
        #endregion
        #region Method
       

        private bool ValidateData()
        {
            if(txtKeyWords.Text.Trim()==string.Empty)
            {
                MessageBox.Show(this, "Vui lòng nhập từ khóa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtKeyWords.Focus();
                return false;
            }
            return true;
        }
        private void EnableControl(bool bolIsEnable)
        {
            grpSearch.Enabled = bolIsEnable;
            mnuAction.Enabled = bolIsEnable;
        }
        private void LoadData()
        {
            objKeywords = new object[] { "@Keyword",txtKeyWords.Text.Trim() };
            dtbResource = objPLCReportDataSource.GetHeavyDataSource("PM_HISTORYIMEI_VIEW", objKeywords);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
            }
            bolIsLoadComplete = true;
        }
        #endregion

        #region Event
        private void frmProductIMEIHistory_Old_Load(object sender, EventArgs e)
        {
            cmdSearch.Enabled = Library.AppCore.SystemConfig.objSessionUser.IsPermission(strPermissionView);
            btnExportExcel.Enabled = cmdSearch.Enabled;
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (this.grdViewData.RowCount <= 0) return;
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(this.grdData);
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            if (!cmdSearch.Enabled)
                return;
            if (!ValidateData())
                return;
            string strContent = "Từ khóa:" + txtKeyWords.Text.Trim();
            Library.AppCore.SystemConfig.objSessionUser.InsertActionLog("Tra cứu IMEI trên hệ thống cũ", this.Name, "Tìm kiếm", strContent);
            //LoadData();
            //return;
            pnSearchStatus.Visible = true;
            pnSearchStatus.Refresh();
            EnableControl(false);
            Thread objThread = new Thread(new ThreadStart(LoadData));
            objThread.Start();
        }

        private void tmrFormatFlex_Tick(object sender, EventArgs e)
        {
            if (bolIsLoadComplete)
            {
                bolIsLoadComplete = false;
                grdData.DataSource = dtbResource;
                pnSearchStatus.Visible = false;
                EnableControl(true);
                btnExportExcel.Enabled   = itemExportExcel.Enabled = grdViewData.RowCount > 0;
            }
        }

        private void itemExportExcel_Click(object sender, EventArgs e)
        {
            btnExportExcel_Click(null, null);
        }
        
        private void txtKeyWords_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                this.cmdSearch.PerformClick();
        }
        #endregion
    }
}
