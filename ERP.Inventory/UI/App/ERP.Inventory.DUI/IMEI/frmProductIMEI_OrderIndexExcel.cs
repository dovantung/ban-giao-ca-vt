﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.LoadControls;
using Library.AppCore;

namespace ERP.Inventory.DUI.IMEI
{
    public partial class frmProductIMEI_OrderIndexExcel : Form
    {
        #region Variable

        private bool bolIsUpdate = false;
        private DataTable dtbSource = new DataTable();
	    private ERP.Inventory.PLC.PLCProductInStock objPLCProductInStock = new PLC.PLCProductInStock();
        private ERP.Inventory.PLC.WSProductInStock.ProductInStock objProductInStock = null;	 

        public bool IsUpdate
        {
            get { return bolIsUpdate; }
        }

        public DataTable SourceTable
        {
            get { return dtbSource; }
            set { dtbSource = value; }
        }
	    #endregion

        #region Constructor 

        public frmProductIMEI_OrderIndexExcel()
        {
            InitializeComponent();
        }

        #endregion

        #region Event		 

        private void frmProductIMEI_OrderIndexExcel_Load(object sender, EventArgs e)
        {
            string[] fieldNames = null;
            fieldNames = new string[] { "ORDERINDEX", "PRODUCTID", "PRODUCTNAME", "IMEI", "ISERROR", "NOTE" };
           
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvIMEI);
            FormatGridIMEI();
            grdData.DataSource = dtbSource;
            ValidateData(ref dtbSource);
            btnUpdate.Enabled = dtbSource.Rows.Count > 0;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Chương trình sẽ chỉ lấy những dữ liệu hợp lệ. Bạn có muốn tiếp tục không?", "Thống báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                bolIsUpdate = true;
            }
            Close();
        }

        private void grvIMEI_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {

        }

        private void grvIMEI_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView View = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            bool bolIsError = Convert.ToBoolean(View.GetRowCellValue(e.RowHandle, View.Columns["ISERROR"]));
            if (bolIsError)
                e.Appearance.BackColor = Color.Pink;
            //else if (category == "N/A")
            //    e.Appearance.BackColor = Color.Pink;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        #region Method

        private bool FormatGridIMEI()
        {
            try
            {
                grvIMEI.Columns["ORDERINDEX"].Caption = "Mức ưu tiên";
                grvIMEI.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
                grvIMEI.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
                grvIMEI.Columns["IMEI"].Caption = "IMEI";
                grvIMEI.Columns["ISERROR"].Caption = "Lỗi";
                grvIMEI.Columns["NOTE"].Caption = "Ghi lỗi";

                grvIMEI.OptionsView.ColumnAutoWidth = false;
                grvIMEI.Columns["PRODUCTID"].Width = 130;
                grvIMEI.Columns["PRODUCTNAME"].Width = 200;
                grvIMEI.Columns["IMEI"].Width = 80;
                grvIMEI.Columns["ORDERINDEX"].Width = 80;
                grvIMEI.Columns["ISERROR"].Width = 80;
                grvIMEI.Columns["NOTE"].Width = 500;

                DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkCheckBoxIsError = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
                chkCheckBoxIsError.ValueChecked = true;
                chkCheckBoxIsError.ValueUnchecked = false;
                grvIMEI.Columns["ISERROR"].ColumnEdit = chkCheckBoxIsError;

                grvIMEI.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grvIMEI.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grvIMEI.OptionsView.ShowAutoFilterRow = true;
                grvIMEI.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grvIMEI.OptionsView.ShowFooter = true;
                grvIMEI.OptionsView.ShowGroupPanel = false;
                grvIMEI.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                grvIMEI.ColumnPanelRowHeight = 40;

                grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "ISERROR";
                grvIMEI.Columns["NOTE"].SummaryItem.DisplayFormat = "{0:n0}";
                grvIMEI.Columns["NOTE"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;

               //grvIMEI.Columns["NOTE"].SummaryItem.FieldName = "NOTE";
                grvIMEI.Columns["ISERROR"].SummaryItem.DisplayFormat = "Tổng số dòng:";
                grvIMEI.Columns["ISERROR"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi định dạng lưới danh sách IMEI");
                SystemErrorWS.Insert("Lỗi định dạng lưới danh sách IMEI", objExce.ToString(), this.Name + " -> FormatGridIMEI", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }

        private bool ValidateData(ref DataTable dtbSource)
        {
            dtbSource.Rows.RemoveAt(0);
            int i = 0;
            dtbSource.Columns[i++].ColumnName = "ORDERINDEX";
            dtbSource.Columns[i++].ColumnName = "PRODUCTID";
            dtbSource.Columns[i++].ColumnName = "IMEI";

            DataColumn dtcProductName = new DataColumn("PRODUCTNAME", typeof(string));
            dtbSource.Columns.Add(dtcProductName);
            DataColumn dtcIsError = new DataColumn("ISERROR", typeof(bool));
            dtcIsError.DefaultValue = false;
            dtbSource.Columns.Add(dtcIsError);
            DataColumn dtcNote = new DataColumn("NOTE", typeof(string));
            dtbSource.Columns.Add(dtcNote);
            dtbSource.Columns["PRODUCTNAME"].SetOrdinal(dtbSource.Columns["PRODUCTID"].Ordinal + 1);

            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = new MasterData.PLC.MD.WSProduct.Product();
            foreach (DataRow row in dtbSource.Rows)
            {
                //RewardPoint
                string strOrderIndex = Convert.ToString(row["ORDERINDEX"]).Trim();
                if (strOrderIndex == string.Empty)
                {
                    row["IsError"] = true;
                    row["NOTE"] = "Mức ưu tiên không được để trống";
                    continue;
                }
                UInt32 uIntRewardPoint = 0;
                bool bolResult = UInt32.TryParse(strOrderIndex, out uIntRewardPoint);
                if (!bolResult || strOrderIndex.Length > 9)
                {
                    row["IsError"] = true;
                    row["NOTE"] = "Mức ưu tiên không đúng định dạng";
                    continue;
                }
                if (uIntRewardPoint == 0)
                {
                    row["IsError"] = true;
                    row["NOTE"] = "Mức ưu tiên phải lớn hơn 0";
                    continue;
                }
                row["ORDERINDEX"] = uIntRewardPoint;

                //Product
                string strProductID = Convert.ToString(row["PRODUCTID"]).Trim();
                if (strProductID == string.Empty)
                {
                    row["IsError"] = true;
                    row["NOTE"] = "Mã sản phẩm không được để trống ";
                    continue;
                }
                objProduct = new MasterData.PLC.MD.WSProduct.Product();
                objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                if (objProduct == null || objProduct.ProductID == null)
                {
                    row["IsError"] = true;
                    row["NOTE"] = "Mã sản phẩm không tồn tại ";
                    continue;
                }
                row["PRODUCTNAME"] = objProduct.ProductName;

                //IMEI
                string strIMEI = Convert.ToString(row["IMEI"]).Trim();
                if (strIMEI == string.Empty)
                {
                    row["IsError"] = true;
                    row["NOTE"] = "IMEI không được để trống";
                    continue;
                }
                ERP.Inventory.PLC.SM.PLCSequenceIMEI objPLCSequenceIMEI = new PLC.SM.PLCSequenceIMEI();
                bool bolIsResult = false;
                ERP.Inventory.PLC.SM.WSSequenceIMEI.ResultMessage objResultMessage = objPLCSequenceIMEI.CheckExist(ref bolIsResult, strIMEI, strProductID);
                if (objResultMessage.IsError)
                {
                    row["IsError"] = true;
                    row["NOTE"] = "Lỗi kiểm tra tồn kho Mã sản phẩm - IMEI";
                    continue;
                }
                if (!bolIsResult)
                {
                    row["IsError"] = true;
                    row["NOTE"] = "Mã sản phẩm - IMEI không còn tồn kho";
                    continue;
                }
                row["IMEI"] = strIMEI;
            }
            dtbSource.AcceptChanges();
            return true;
        }

        #endregion
    }
}
