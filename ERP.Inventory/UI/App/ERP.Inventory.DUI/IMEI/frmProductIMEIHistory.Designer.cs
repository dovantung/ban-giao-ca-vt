﻿namespace ERP.Inventory.DUI.IMEI
{
    partial class frmProductIMEIHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProductIMEIHistory));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblNoteByIMEI = new System.Windows.Forms.Label();
            this.btnCheckLockFIFO = new System.Windows.Forms.Button();
            this.cboInput = new System.Windows.Forms.ComboBox();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.cmdSearch = new System.Windows.Forms.Button();
            this.chkIsIncludeDeleted = new System.Windows.Forms.CheckBox();
            this.txtIMEI = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grdViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cachedrptInputVoucherReport1 = new ERP.Inventory.DUI.Reports.Input.CachedrptInputVoucherReport();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.lblNoteByIMEI);
            this.groupBox1.Controls.Add(this.btnCheckLockFIFO);
            this.groupBox1.Controls.Add(this.cboInput);
            this.groupBox1.Controls.Add(this.cboType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnExportExcel);
            this.groupBox1.Controls.Add(this.cmdSearch);
            this.groupBox1.Controls.Add(this.chkIsIncludeDeleted);
            this.groupBox1.Controls.Add(this.txtIMEI);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1001, 73);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // lblNoteByIMEI
            // 
            this.lblNoteByIMEI.AutoSize = true;
            this.lblNoteByIMEI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoteByIMEI.ForeColor = System.Drawing.Color.Blue;
            this.lblNoteByIMEI.Location = new System.Drawing.Point(44, 46);
            this.lblNoteByIMEI.Name = "lblNoteByIMEI";
            this.lblNoteByIMEI.Size = new System.Drawing.Size(127, 16);
            this.lblNoteByIMEI.TabIndex = 9;
            this.lblNoteByIMEI.Text = "Ghi chú theo IMEI";
            // 
            // btnCheckLockFIFO
            // 
            this.btnCheckLockFIFO.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.btnCheckLockFIFO.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCheckLockFIFO.Location = new System.Drawing.Point(749, 15);
            this.btnCheckLockFIFO.Name = "btnCheckLockFIFO";
            this.btnCheckLockFIFO.Size = new System.Drawing.Size(147, 25);
            this.btnCheckLockFIFO.TabIndex = 7;
            this.btnCheckLockFIFO.Text = "Kiểm tra khóa FIFO";
            this.btnCheckLockFIFO.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCheckLockFIFO.UseVisualStyleBackColor = true;
            this.btnCheckLockFIFO.Click += new System.EventHandler(this.btnCheckLockFIFO_Click);
            // 
            // cboInput
            // 
            this.cboInput.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInput.FormattingEnabled = true;
            this.cboInput.Items.AddRange(new object[] {
            "-- Tất cả --",
            "Chưa nhập/xuất hàng",
            "Đã nhập/xuất hàng"});
            this.cboInput.Location = new System.Drawing.Point(378, 16);
            this.cboInput.Name = "cboInput";
            this.cboInput.Size = new System.Drawing.Size(153, 24);
            this.cboInput.TabIndex = 4;
            // 
            // cboType
            // 
            this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboType.FormattingEnabled = true;
            this.cboType.Items.AddRange(new object[] {
            "-- Tất cả --",
            "Phiếu nhập",
            "Phiếu xuất"});
            this.cboType.Location = new System.Drawing.Point(279, 16);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(93, 24);
            this.cboType.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(209, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Loại phiếu:";
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportExcel.Location = new System.Drawing.Point(899, 15);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(98, 25);
            this.btnExportExcel.TabIndex = 8;
            this.btnExportExcel.Text = "    Xuất Excel";
            this.btnExportExcel.UseVisualStyleBackColor = true;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // cmdSearch
            // 
            this.cmdSearch.Image = ((System.Drawing.Image)(resources.GetObject("cmdSearch.Image")));
            this.cmdSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSearch.Location = new System.Drawing.Point(654, 15);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(92, 25);
            this.cmdSearch.TabIndex = 6;
            this.cmdSearch.Text = "   Tìm kiếm";
            this.cmdSearch.UseVisualStyleBackColor = true;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // chkIsIncludeDeleted
            // 
            this.chkIsIncludeDeleted.AutoSize = true;
            this.chkIsIncludeDeleted.Location = new System.Drawing.Point(536, 18);
            this.chkIsIncludeDeleted.Name = "chkIsIncludeDeleted";
            this.chkIsIncludeDeleted.Size = new System.Drawing.Size(116, 20);
            this.chkIsIncludeDeleted.TabIndex = 5;
            this.chkIsIncludeDeleted.Text = "Gồm phiếu hủy";
            this.chkIsIncludeDeleted.UseVisualStyleBackColor = true;
            // 
            // txtIMEI
            // 
            this.txtIMEI.Location = new System.Drawing.Point(48, 16);
            this.txtIMEI.Name = "txtIMEI";
            this.txtIMEI.Size = new System.Drawing.Size(157, 22);
            this.txtIMEI.TabIndex = 1;
            this.txtIMEI.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtIMEI_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "IMEI:";
            // 
            // grdData
            // 
            this.grdData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdData.Location = new System.Drawing.Point(3, 75);
            this.grdData.MainView = this.grdViewData;
            this.grdData.Name = "grdData";
            this.grdData.Size = new System.Drawing.Size(1001, 398);
            this.grdData.TabIndex = 1;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewData});
            // 
            // grdViewData
            // 
            this.grdViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewData.GridControl = this.grdData;
            this.grdViewData.Name = "grdViewData";
            this.grdViewData.OptionsView.ColumnAutoWidth = false;
            this.grdViewData.OptionsView.ShowGroupPanel = false;
            this.grdViewData.DoubleClick += new System.EventHandler(this.grdViewData_DoubleClick);
            // 
            // frmProductIMEIHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 481);
            this.Controls.Add(this.grdData);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmProductIMEIHistory";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tìm kiếm lịch sử IMEI";
            this.Activated += new System.EventHandler(this.frmProductIMEIHistory_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmProductIMEIHistory_FormClosing);
            this.Load += new System.EventHandler(this.frmProductIMEIHistory_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnExportExcel;
        private System.Windows.Forms.Button cmdSearch;
        private System.Windows.Forms.CheckBox chkIsIncludeDeleted;
        private System.Windows.Forms.TextBox txtIMEI;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewData;
        private Reports.Input.CachedrptInputVoucherReport cachedrptInputVoucherReport1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboType;
        private System.Windows.Forms.ComboBox cboInput;
        private System.Windows.Forms.Button btnCheckLockFIFO;
        private System.Windows.Forms.Label lblNoteByIMEI;
    }
}