﻿namespace ERP.Inventory.DUI.Barcode
{
    partial class frmPrintProductBarcode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrintProductBarcode));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cboSearchBy = new System.Windows.Forms.ComboBox();
            this.lblSearchBy = new DevExpress.XtraEditors.LabelControl();
            this.txtInputVoucherID = new System.Windows.Forms.TextBox();
            this.chkIsFromInputVoucher = new System.Windows.Forms.CheckBox();
            this.chkIsInstock = new System.Windows.Forms.CheckBox();
            this.cboStoreSearch = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cboProductList1 = new ERP.MasterData.DUI.CustomControl.Product.cboProductList();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.btnPrintQRCode = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrintProduct = new DevExpress.XtraEditors.SimpleButton();
            this.cmdSearch = new DevExpress.XtraEditors.SimpleButton();
            this.cboSubGroupID = new System.Windows.Forms.ComboBox();
            this.btnSetPrintQuantity = new DevExpress.XtraEditors.SimpleButton();
            this.cboQRSize = new System.Windows.Forms.ComboBox();
            this.cboIsNew = new System.Windows.Forms.ComboBox();
            this.cboMainGroupID = new System.Windows.Forms.ComboBox();
            this.txtTotalPrintQuantity = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtTotalQuantity = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtPrintQuantity = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cboBrandIDList = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.mnuStore = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuStoreID = new System.Windows.Forms.ToolStripMenuItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.flexData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.mnuProductSelect = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuProductAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuIMEItoExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboStoreSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalPrintQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrintQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBrandIDList.Properties)).BeginInit();
            this.mnuStore.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).BeginInit();
            this.mnuProductSelect.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.cboSearchBy);
            this.groupControl1.Controls.Add(this.lblSearchBy);
            this.groupControl1.Controls.Add(this.txtInputVoucherID);
            this.groupControl1.Controls.Add(this.chkIsFromInputVoucher);
            this.groupControl1.Controls.Add(this.chkIsInstock);
            this.groupControl1.Controls.Add(this.cboStoreSearch);
            this.groupControl1.Controls.Add(this.cboProductList1);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl11);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.btnPrintQRCode);
            this.groupControl1.Controls.Add(this.btnPrintProduct);
            this.groupControl1.Controls.Add(this.cmdSearch);
            this.groupControl1.Controls.Add(this.cboSubGroupID);
            this.groupControl1.Controls.Add(this.btnSetPrintQuantity);
            this.groupControl1.Controls.Add(this.cboQRSize);
            this.groupControl1.Controls.Add(this.cboIsNew);
            this.groupControl1.Controls.Add(this.cboMainGroupID);
            this.groupControl1.Controls.Add(this.txtTotalPrintQuantity);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.txtTotalQuantity);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txtPrintQuantity);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.cboBrandIDList);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1015, 150);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "Thông tin tìm kiếm";
            // 
            // cboSearchBy
            // 
            this.cboSearchBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSearchBy.FormattingEnabled = true;
            this.cboSearchBy.Location = new System.Drawing.Point(392, 112);
            this.cboSearchBy.Name = "cboSearchBy";
            this.cboSearchBy.Size = new System.Drawing.Size(100, 24);
            this.cboSearchBy.TabIndex = 14;
            this.cboSearchBy.Visible = false;
            // 
            // lblSearchBy
            // 
            this.lblSearchBy.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchBy.Location = new System.Drawing.Point(311, 116);
            this.lblSearchBy.Name = "lblSearchBy";
            this.lblSearchBy.Size = new System.Drawing.Size(56, 16);
            this.lblSearchBy.TabIndex = 35;
            this.lblSearchBy.Text = "Tìm theo:";
            this.lblSearchBy.Visible = false;
            // 
            // txtInputVoucherID
            // 
            this.txtInputVoucherID.Location = new System.Drawing.Point(493, 113);
            this.txtInputVoucherID.Name = "txtInputVoucherID";
            this.txtInputVoucherID.Size = new System.Drawing.Size(127, 22);
            this.txtInputVoucherID.TabIndex = 15;
            this.txtInputVoucherID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtInputVoucherID_KeyDown);
            // 
            // chkIsFromInputVoucher
            // 
            this.chkIsFromInputVoucher.AutoSize = true;
            this.chkIsFromInputVoucher.Location = new System.Drawing.Point(311, 115);
            this.chkIsFromInputVoucher.Name = "chkIsFromInputVoucher";
            this.chkIsFromInputVoucher.Size = new System.Drawing.Size(148, 20);
            this.chkIsFromInputVoucher.TabIndex = 13;
            this.chkIsFromInputVoucher.Text = "Tìm theo phiếu nhập";
            this.chkIsFromInputVoucher.UseVisualStyleBackColor = true;
            this.chkIsFromInputVoucher.CheckedChanged += new System.EventHandler(this.chkIsFromInputVoucher_CheckedChanged);
            // 
            // chkIsInstock
            // 
            this.chkIsInstock.AutoSize = true;
            this.chkIsInstock.Location = new System.Drawing.Point(105, 115);
            this.chkIsInstock.Name = "chkIsInstock";
            this.chkIsInstock.Size = new System.Drawing.Size(198, 20);
            this.chkIsInstock.TabIndex = 12;
            this.chkIsInstock.Text = "Chỉ hiện thị sản phẩm tồn kho";
            this.chkIsInstock.UseVisualStyleBackColor = true;
            // 
            // cboStoreSearch
            // 
            this.cboStoreSearch.Location = new System.Drawing.Point(75, 27);
            this.cboStoreSearch.Name = "cboStoreSearch";
            this.cboStoreSearch.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreSearch.Properties.Appearance.Options.UseFont = true;
            this.cboStoreSearch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboStoreSearch.Properties.DisplayMember = "STORENAME";
            this.cboStoreSearch.Properties.ValueMember = "STOREID";
            this.cboStoreSearch.Properties.View = this.searchLookUpEdit1View;
            this.cboStoreSearch.Size = new System.Drawing.Size(228, 22);
            this.cboStoreSearch.TabIndex = 1;
            this.cboStoreSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cboStoreSearch_KeyDown);
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Options.UseFont = true;
            this.searchLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11});
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // cboProductList1
            // 
            this.cboProductList1.BackColor = System.Drawing.Color.Transparent;
            this.cboProductList1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductList1.Location = new System.Drawing.Point(392, 55);
            this.cboProductList1.Margin = new System.Windows.Forms.Padding(4);
            this.cboProductList1.Name = "cboProductList1";
            this.cboProductList1.ProductIDList = "";
            this.cboProductList1.Size = new System.Drawing.Size(228, 23);
            this.cboProductList1.TabIndex = 5;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Location = new System.Drawing.Point(628, 59);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(70, 16);
            this.labelControl5.TabIndex = 14;
            this.labelControl5.Text = "Số lượng in:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(628, 31);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(70, 16);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = "Nhóm hàng:";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Location = new System.Drawing.Point(260, 87);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(126, 18);
            this.labelControl11.TabIndex = 7;
            this.labelControl11.Text = "Tổng số lượng in:";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Location = new System.Drawing.Point(7, 87);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(136, 18);
            this.labelControl10.TabIndex = 7;
            this.labelControl10.Text = "Tổng số lượng tồn:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(7, 59);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(47, 16);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "Nhà SX:";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Location = new System.Drawing.Point(311, 59);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(63, 16);
            this.labelControl6.TabIndex = 16;
            this.labelControl6.Text = "Sản phẩm:";
            // 
            // btnPrintQRCode
            // 
            this.btnPrintQRCode.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintQRCode.Appearance.Options.UseFont = true;
            this.btnPrintQRCode.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintQRCode.Image")));
            this.btnPrintQRCode.Location = new System.Drawing.Point(810, 113);
            this.btnPrintQRCode.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnPrintQRCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPrintQRCode.Name = "btnPrintQRCode";
            this.btnPrintQRCode.Size = new System.Drawing.Size(106, 25);
            this.btnPrintQRCode.TabIndex = 17;
            this.btnPrintQRCode.Text = "In QR code";
            this.btnPrintQRCode.Click += new System.EventHandler(this.btnPrintQRCode_Click);
            // 
            // btnPrintProduct
            // 
            this.btnPrintProduct.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintProduct.Appearance.Options.UseFont = true;
            this.btnPrintProduct.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintProduct.Image")));
            this.btnPrintProduct.Location = new System.Drawing.Point(810, 83);
            this.btnPrintProduct.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnPrintProduct.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPrintProduct.Name = "btnPrintProduct";
            this.btnPrintProduct.Size = new System.Drawing.Size(106, 25);
            this.btnPrintProduct.TabIndex = 11;
            this.btnPrintProduct.Text = "In sản phẩm";
            this.btnPrintProduct.Click += new System.EventHandler(this.btnPrintProduct_Click);
            // 
            // cmdSearch
            // 
            this.cmdSearch.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSearch.Appearance.Options.UseFont = true;
            this.cmdSearch.Image = ((System.Drawing.Image)(resources.GetObject("cmdSearch.Image")));
            this.cmdSearch.Location = new System.Drawing.Point(704, 83);
            this.cmdSearch.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cmdSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(100, 25);
            this.cmdSearch.TabIndex = 10;
            this.cmdSearch.Text = "Tìm kiếm";
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // cboSubGroupID
            // 
            this.cboSubGroupID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSubGroupID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSubGroupID.FormattingEnabled = true;
            this.cboSubGroupID.Location = new System.Drawing.Point(704, 27);
            this.cboSubGroupID.Name = "cboSubGroupID";
            this.cboSubGroupID.Size = new System.Drawing.Size(212, 24);
            this.cboSubGroupID.TabIndex = 3;
            // 
            // btnSetPrintQuantity
            // 
            this.btnSetPrintQuantity.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetPrintQuantity.Appearance.Options.UseFont = true;
            this.btnSetPrintQuantity.Image = ((System.Drawing.Image)(resources.GetObject("btnSetPrintQuantity.Image")));
            this.btnSetPrintQuantity.Location = new System.Drawing.Point(794, 55);
            this.btnSetPrintQuantity.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSetPrintQuantity.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSetPrintQuantity.Name = "btnSetPrintQuantity";
            this.btnSetPrintQuantity.Size = new System.Drawing.Size(122, 24);
            this.btnSetPrintQuantity.TabIndex = 7;
            this.btnSetPrintQuantity.Text = "Xét số lượng in";
            this.btnSetPrintQuantity.Click += new System.EventHandler(this.btnSetPrintQuantity_Click);
            // 
            // cboQRSize
            // 
            this.cboQRSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboQRSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboQRSize.FormattingEnabled = true;
            this.cboQRSize.Items.AddRange(new object[] {
            "3 x 1",
            "2 x 1"});
            this.cboQRSize.Location = new System.Drawing.Point(704, 113);
            this.cboQRSize.Name = "cboQRSize";
            this.cboQRSize.Size = new System.Drawing.Size(100, 24);
            this.cboQRSize.TabIndex = 16;
            // 
            // cboIsNew
            // 
            this.cboIsNew.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIsNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboIsNew.FormattingEnabled = true;
            this.cboIsNew.Items.AddRange(new object[] {
            "-Tất cả-",
            "Mới",
            "Cũ"});
            this.cboIsNew.Location = new System.Drawing.Point(546, 84);
            this.cboIsNew.Name = "cboIsNew";
            this.cboIsNew.Size = new System.Drawing.Size(74, 24);
            this.cboIsNew.TabIndex = 9;
            // 
            // cboMainGroupID
            // 
            this.cboMainGroupID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMainGroupID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroupID.FormattingEnabled = true;
            this.cboMainGroupID.Location = new System.Drawing.Point(392, 27);
            this.cboMainGroupID.Name = "cboMainGroupID";
            this.cboMainGroupID.Size = new System.Drawing.Size(228, 24);
            this.cboMainGroupID.TabIndex = 2;
            this.cboMainGroupID.SelectionChangeCommitted += new System.EventHandler(this.cboMainGroupID_SelectionChangeCommitted);
            // 
            // txtTotalPrintQuantity
            // 
            this.txtTotalPrintQuantity.Location = new System.Drawing.Point(392, 83);
            this.txtTotalPrintQuantity.Name = "txtTotalPrintQuantity";
            this.txtTotalPrintQuantity.Properties.Appearance.BackColor = System.Drawing.SystemColors.Highlight;
            this.txtTotalPrintQuantity.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPrintQuantity.Properties.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.txtTotalPrintQuantity.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalPrintQuantity.Properties.Appearance.Options.UseFont = true;
            this.txtTotalPrintQuantity.Properties.Appearance.Options.UseForeColor = true;
            this.txtTotalPrintQuantity.Properties.NullText = "0";
            this.txtTotalPrintQuantity.Properties.ReadOnly = true;
            this.txtTotalPrintQuantity.Size = new System.Drawing.Size(95, 24);
            this.txtTotalPrintQuantity.TabIndex = 18;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Location = new System.Drawing.Point(628, 117);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(64, 16);
            this.labelControl8.TabIndex = 3;
            this.labelControl8.Text = "Kích thước:";
            // 
            // txtTotalQuantity
            // 
            this.txtTotalQuantity.Location = new System.Drawing.Point(149, 84);
            this.txtTotalQuantity.Name = "txtTotalQuantity";
            this.txtTotalQuantity.Properties.Appearance.BackColor = System.Drawing.SystemColors.Highlight;
            this.txtTotalQuantity.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalQuantity.Properties.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.txtTotalQuantity.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalQuantity.Properties.Appearance.Options.UseFont = true;
            this.txtTotalQuantity.Properties.Appearance.Options.UseForeColor = true;
            this.txtTotalQuantity.Properties.NullText = "0";
            this.txtTotalQuantity.Properties.ReadOnly = true;
            this.txtTotalQuantity.Size = new System.Drawing.Size(95, 24);
            this.txtTotalQuantity.TabIndex = 18;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Location = new System.Drawing.Point(493, 88);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(47, 16);
            this.labelControl7.TabIndex = 3;
            this.labelControl7.Text = "Loại SP:";
            // 
            // txtPrintQuantity
            // 
            this.txtPrintQuantity.Location = new System.Drawing.Point(704, 56);
            this.txtPrintQuantity.Name = "txtPrintQuantity";
            this.txtPrintQuantity.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtPrintQuantity.Properties.Appearance.Options.UseFont = true;
            this.txtPrintQuantity.Properties.Mask.EditMask = "###";
            this.txtPrintQuantity.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPrintQuantity.Properties.MaxLength = 20;
            this.txtPrintQuantity.Properties.NullText = "1";
            this.txtPrintQuantity.Size = new System.Drawing.Size(61, 22);
            this.txtPrintQuantity.TabIndex = 6;
            this.txtPrintQuantity.Leave += new System.EventHandler(this.txtPrintQuantity_Leave);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(311, 31);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(73, 16);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Ngành hàng:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(7, 31);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(62, 16);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Chi nhánh:";
            // 
            // cboBrandIDList
            // 
            this.cboBrandIDList.EditValue = "";
            this.cboBrandIDList.Location = new System.Drawing.Point(75, 56);
            this.cboBrandIDList.Name = "cboBrandIDList";
            this.cboBrandIDList.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.cboBrandIDList.Properties.Appearance.Options.UseFont = true;
            this.cboBrandIDList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboBrandIDList.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.cboBrandIDList.Size = new System.Drawing.Size(228, 22);
            this.cboBrandIDList.TabIndex = 4;
            // 
            // mnuStore
            // 
            this.mnuStore.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuStoreID});
            this.mnuStore.Name = "mnuStore";
            this.mnuStore.Size = new System.Drawing.Size(179, 26);
            // 
            // mnuStoreID
            // 
            this.mnuStoreID.Name = "mnuStoreID";
            this.mnuStoreID.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.mnuStoreID.Size = new System.Drawing.Size(178, 22);
            this.mnuStoreID.Text = "Chọn chi nhánh";
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.flexData);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 150);
            this.groupControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1015, 432);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Thông tin sản phẩm";
            // 
            // flexData
            // 
            this.flexData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flexData.AutoClipboard = true;
            this.flexData.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.flexData.ContextMenuStrip = this.mnuProductSelect;
            this.flexData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flexData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flexData.Location = new System.Drawing.Point(2, 24);
            this.flexData.Name = "flexData";
            this.flexData.Rows.Count = 1;
            this.flexData.Rows.DefaultSize = 19;
            this.flexData.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.CellRange;
            this.flexData.Size = new System.Drawing.Size(1011, 406);
            this.flexData.StyleInfo = resources.GetString("flexData.StyleInfo");
            this.flexData.TabIndex = 3;
            this.flexData.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            this.flexData.AfterEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexData_AfterEdit);
            this.flexData.SetupEditor += new C1.Win.C1FlexGrid.RowColEventHandler(this.flexData_SetupEditor);
            // 
            // mnuProductSelect
            // 
            this.mnuProductSelect.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuProductAdd,
            this.mnuIMEItoExcel});
            this.mnuProductSelect.Name = "mnuProductSelect";
            this.mnuProductSelect.Size = new System.Drawing.Size(180, 48);
            // 
            // mnuProductAdd
            // 
            this.mnuProductAdd.Image = ((System.Drawing.Image)(resources.GetObject("mnuProductAdd.Image")));
            this.mnuProductAdd.Name = "mnuProductAdd";
            this.mnuProductAdd.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.mnuProductAdd.Size = new System.Drawing.Size(179, 22);
            this.mnuProductAdd.Text = "Thêm sản phẩm";
            this.mnuProductAdd.Click += new System.EventHandler(this.mnuProductAdd_Click);
            // 
            // mnuIMEItoExcel
            // 
            this.mnuIMEItoExcel.Image = ((System.Drawing.Image)(resources.GetObject("mnuIMEItoExcel.Image")));
            this.mnuIMEItoExcel.Name = "mnuIMEItoExcel";
            this.mnuIMEItoExcel.Size = new System.Drawing.Size(179, 22);
            this.mnuIMEItoExcel.Text = "Nhập IMEI từ Excel";
            this.mnuIMEItoExcel.Visible = false;
            this.mnuIMEItoExcel.Click += new System.EventHandler(this.mnuIMEItoExcel_Click);
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Mã kho";
            this.gridColumn10.FieldName = "STOREID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            this.gridColumn10.Width = 159;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Tên kho";
            this.gridColumn11.FieldName = "STORENAME";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            this.gridColumn11.Width = 959;
            // 
            // frmPrintProductBarcode
            // 
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1015, 582);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmPrintProductBarcode";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "In barcode sản phẩm";
            this.Load += new System.EventHandler(this.frmInputVoucher_PrintProduct_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboStoreSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalPrintQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrintQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBrandIDList.Properties)).EndInit();
            this.mnuStore.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flexData)).EndInit();
            this.mnuProductSelect.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton cmdSearch;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtTotalQuantity;
        private DevExpress.XtraEditors.TextEdit txtPrintQuantity;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cboBrandIDList;
        private System.Windows.Forms.ComboBox cboSubGroupID;
        private System.Windows.Forms.ComboBox cboMainGroupID;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton btnSetPrintQuantity;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.SimpleButton btnPrintProduct;
        private DevExpress.XtraEditors.TextEdit txtTotalPrintQuantity;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.ContextMenuStrip mnuStore;
        private System.Windows.Forms.ToolStripMenuItem mnuStoreID;
        private System.Windows.Forms.ContextMenuStrip mnuProductSelect;
        private System.Windows.Forms.ToolStripMenuItem mnuProductAdd;
        private C1.Win.C1FlexGrid.C1FlexGrid flexData;
        private MasterData.DUI.CustomControl.Product.cboProductList cboProductList1;
        private DevExpress.XtraEditors.SearchLookUpEdit cboStoreSearch;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private System.Windows.Forms.ComboBox cboIsNew;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SimpleButton btnPrintQRCode;
        private System.Windows.Forms.ComboBox cboQRSize;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private System.Windows.Forms.CheckBox chkIsInstock;
        private System.Windows.Forms.TextBox txtInputVoucherID;
        private System.Windows.Forms.CheckBox chkIsFromInputVoucher;
        private System.Windows.Forms.ToolStripMenuItem mnuIMEItoExcel;
        private System.Windows.Forms.ComboBox cboSearchBy;
        private DevExpress.XtraEditors.LabelControl lblSearchBy;
    }
}