﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppControl;
using Library.AppCore;
using Library.AppCore.LoadControls;

/*
 *  Created by: Nguyễn Quốc Hiệp
 *  Date      : 27/06/2017
 *  Desc      : Form hiển thị các dòng lỗi khi nhập từ file excel
*/

namespace ERP.Inventory.DUI.Barcode
{
    public partial class frmProduct_BarcodeError : Form
    {
        #region Khai báo biến
        private DataTable dtbData = new DataTable();
        #endregion

        #region Phương thức
        public DataTable Data
        {
            get { return dtbData; }
            set { dtbData = value; }
        }
        public frmProduct_BarcodeError()
        {
            InitializeComponent();
        }
        #endregion

        #region Sự kiện
        private void grdQuotation_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        #endregion

        #region Các hàm hỗ trợ
        private void LoadData()
        {
            try
            {
                grData.DataSource = dtbData;
            }
            catch (Exception exce)
            {
                SystemErrorWS.Insert("Lỗi khi danh sách mã sản phẩm nạp từ excel bị lỗi!", exce, DUIInventory_Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Lỗi khi danh sách mã sản phẩm nạp từ excel bị lỗi!");
            }
        }

        private void grdViewData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                Clipboard.SetText(grdViewData.GetFocusedDisplayText());
                e.Handled = true;
            }
        }
        #endregion

    }
}
