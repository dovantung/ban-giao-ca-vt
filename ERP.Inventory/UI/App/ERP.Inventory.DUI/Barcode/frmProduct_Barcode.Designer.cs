﻿namespace ERP.Inventory.DUI.Barcode
{
    partial class frmProduct_Barcode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProduct_Barcode));
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnGenBarcode = new System.Windows.Forms.Button();
            this.btnPrintBarcode = new System.Windows.Forms.Button();
            this.btnUndo = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.grdProduct_Barcode = new DevExpress.XtraGrid.GridControl();
            this.mnuAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemImportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdUnCheckAll = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdCheckAll = new System.Windows.Forms.ToolStripMenuItem();
            this.grdViewProduct_Barcode = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ISSELECT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chkIsSelect = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spePrice = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.txtNote = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.speOldPrice = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.btnSearch = new System.Windows.Forms.Button();
            this.cboMainGroupIDList = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboRVLStatus = new System.Windows.Forms.ComboBox();
            this.cboProductList = new ERP.MasterData.DUI.CustomControl.Product.cboProductList();
            this.cboBrandIDList = new Library.AppControl.ComboBoxControl.ComboBoxBrand();
            this.mnuReview = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProduct_Barcode)).BeginInit();
            this.mnuAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProduct_Barcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speOldPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.VistaTimeProperties)).BeginInit();
            this.mnuReview.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(375, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nhà sản xuất:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 35);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 16);
            this.label10.TabIndex = 4;
            this.label10.Text = "Sản phẩm:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Info;
            this.panel1.Controls.Add(this.btnExportExcel);
            this.panel1.Controls.Add(this.btnGenBarcode);
            this.panel1.Controls.Add(this.btnPrintBarcode);
            this.panel1.Controls.Add(this.btnUndo);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 456);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(953, 38);
            this.panel1.TabIndex = 10;
            // 
            // btnGenBarcode
            // 
            this.btnGenBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenBarcode.Enabled = false;
            this.btnGenBarcode.Image = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.btnGenBarcode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGenBarcode.Location = new System.Drawing.Point(735, 7);
            this.btnGenBarcode.Name = "btnGenBarcode";
            this.btnGenBarcode.Size = new System.Drawing.Size(117, 25);
            this.btnGenBarcode.TabIndex = 2;
            this.btnGenBarcode.Text = "   Gen Barcode";
            this.btnGenBarcode.UseVisualStyleBackColor = true;
            this.btnGenBarcode.Click += new System.EventHandler(this.btnGenBarcode_Click);
            // 
            // btnPrintBarcode
            // 
            this.btnPrintBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrintBarcode.Enabled = false;
            this.btnPrintBarcode.Image = global::ERP.Inventory.DUI.Properties.Resources.barcode_icon;
            this.btnPrintBarcode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrintBarcode.Location = new System.Drawing.Point(5, 7);
            this.btnPrintBarcode.Name = "btnPrintBarcode";
            this.btnPrintBarcode.Size = new System.Drawing.Size(79, 25);
            this.btnPrintBarcode.TabIndex = 0;
            this.btnPrintBarcode.Text = "In Tem";
            this.btnPrintBarcode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrintBarcode.UseVisualStyleBackColor = true;
            this.btnPrintBarcode.Click += new System.EventHandler(this.btnPrintBarcode_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUndo.Image = global::ERP.Inventory.DUI.Properties.Resources.undo;
            this.btnUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUndo.Location = new System.Drawing.Point(634, 7);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(95, 25);
            this.btnUndo.TabIndex = 1;
            this.btnUndo.Text = "Bỏ qua";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Visible = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(853, 7);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(95, 25);
            this.btnUpdate.TabIndex = 1;
            this.btnUpdate.Text = "   Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // grdProduct_Barcode
            // 
            this.grdProduct_Barcode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdProduct_Barcode.ContextMenuStrip = this.mnuAction;
            this.grdProduct_Barcode.Location = new System.Drawing.Point(0, 61);
            this.grdProduct_Barcode.MainView = this.grdViewProduct_Barcode;
            this.grdProduct_Barcode.Name = "grdProduct_Barcode";
            this.grdProduct_Barcode.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chkIsSelect,
            this.spePrice,
            this.txtNote,
            this.repositoryItemDateEdit1,
            this.speOldPrice,
            this.repositoryItemDateEdit2});
            this.grdProduct_Barcode.Size = new System.Drawing.Size(953, 389);
            this.grdProduct_Barcode.TabIndex = 9;
            this.grdProduct_Barcode.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdViewProduct_Barcode});
            // 
            // mnuAction
            // 
            this.mnuAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemExportExcel,
            this.mnuItemImportExcel,
            this.cmdUnCheckAll,
            this.cmdCheckAll});
            this.mnuAction.Name = "mnuAction";
            this.mnuAction.Size = new System.Drawing.Size(193, 92);
            // 
            // mnuItemExportExcel
            // 
            this.mnuItemExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportExcel.Name = "mnuItemExportExcel";
            this.mnuItemExportExcel.Size = new System.Drawing.Size(192, 22);
            this.mnuItemExportExcel.Text = "Xuất mẫu Excel";
            this.mnuItemExportExcel.Visible = false;
            this.mnuItemExportExcel.Click += new System.EventHandler(this.mnuItemDelete_Click);
            // 
            // mnuItemImportExcel
            // 
            this.mnuItemImportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemImportExcel.Name = "mnuItemImportExcel";
            this.mnuItemImportExcel.Size = new System.Drawing.Size(192, 22);
            this.mnuItemImportExcel.Text = "Nạp, xuất dữ liệu Excel";
            this.mnuItemImportExcel.Click += new System.EventHandler(this.mnuItemImportExcel_Click);
            // 
            // cmdUnCheckAll
            // 
            this.cmdUnCheckAll.Enabled = false;
            this.cmdUnCheckAll.Image = global::ERP.Inventory.DUI.Properties.Resources.close;
            this.cmdUnCheckAll.Name = "cmdUnCheckAll";
            this.cmdUnCheckAll.Size = new System.Drawing.Size(192, 22);
            this.cmdUnCheckAll.Text = "Bỏ chọn tất cả";
            this.cmdUnCheckAll.Click += new System.EventHandler(this.cmdUnCheckAll_Click);
            // 
            // cmdCheckAll
            // 
            this.cmdCheckAll.Enabled = false;
            this.cmdCheckAll.Image = global::ERP.Inventory.DUI.Properties.Resources.check_review;
            this.cmdCheckAll.Name = "cmdCheckAll";
            this.cmdCheckAll.Size = new System.Drawing.Size(192, 22);
            this.cmdCheckAll.Text = "Chọn tất cả";
            this.cmdCheckAll.Click += new System.EventHandler(this.cmdCheckAll_Click);
            // 
            // grdViewProduct_Barcode
            // 
            this.grdViewProduct_Barcode.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProduct_Barcode.Appearance.FocusedRow.Options.UseFont = true;
            this.grdViewProduct_Barcode.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.grdViewProduct_Barcode.Appearance.HeaderPanel.Options.UseFont = true;
            this.grdViewProduct_Barcode.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProduct_Barcode.Appearance.Preview.Options.UseFont = true;
            this.grdViewProduct_Barcode.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grdViewProduct_Barcode.Appearance.Row.Options.UseFont = true;
            this.grdViewProduct_Barcode.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ISSELECT,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn1,
            this.gridColumn5,
            this.gridColumn13,
            this.gridColumn12,
            this.gridColumn11,
            this.gridColumn6,
            this.gridColumn16,
            this.gridColumn15,
            this.gridColumn14});
            this.grdViewProduct_Barcode.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdViewProduct_Barcode.GridControl = this.grdProduct_Barcode;
            this.grdViewProduct_Barcode.IndicatorWidth = 35;
            this.grdViewProduct_Barcode.Name = "grdViewProduct_Barcode";
            this.grdViewProduct_Barcode.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grdViewProduct_Barcode.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grdViewProduct_Barcode.OptionsNavigation.UseTabKey = false;
            this.grdViewProduct_Barcode.OptionsView.ColumnAutoWidth = false;
            this.grdViewProduct_Barcode.OptionsView.ShowAutoFilterRow = true;
            this.grdViewProduct_Barcode.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grdViewProduct_Barcode.OptionsView.ShowFooter = true;
            this.grdViewProduct_Barcode.OptionsView.ShowGroupPanel = false;
            this.grdViewProduct_Barcode.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.grdViewProduct_Barcode_ShowingEditor);
            this.grdViewProduct_Barcode.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.grdViewProduct_Barcode_ValidatingEditor);
            // 
            // ISSELECT
            // 
            this.ISSELECT.AppearanceHeader.Options.UseTextOptions = true;
            this.ISSELECT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ISSELECT.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ISSELECT.Caption = "Chọn";
            this.ISSELECT.ColumnEdit = this.chkIsSelect;
            this.ISSELECT.FieldName = "ISSELECT";
            this.ISSELECT.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.ISSELECT.Name = "ISSELECT";
            this.ISSELECT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ISSELECT.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.ISSELECT.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.ISSELECT.Visible = true;
            this.ISSELECT.VisibleIndex = 0;
            this.ISSELECT.Width = 46;
            // 
            // chkIsSelect
            // 
            this.chkIsSelect.AutoHeight = false;
            this.chkIsSelect.Name = "chkIsSelect";
            this.chkIsSelect.ValueChecked = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.chkIsSelect.ValueUnchecked = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "Mã sản phẩm";
            this.gridColumn2.FieldName = "PRODUCTID";
            this.gridColumn2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 100;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "Tên sản phẩm";
            this.gridColumn3.FieldName = "PRODUCTNAME";
            this.gridColumn3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "PRODUCTNAME", "Tổng:{0:N0}")});
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 200;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn4.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn4.Caption = "Mã NSX";
            this.gridColumn4.FieldName = "BRANDID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Width = 100;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn7.Caption = "Tên nhà sản xuất";
            this.gridColumn7.FieldName = "BRANDNAME";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn7.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            this.gridColumn7.Width = 200;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn8.Caption = "Địa chỉ";
            this.gridColumn8.FieldName = "BRANDADDRESS";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 4;
            this.gridColumn8.Width = 96;
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn9.Caption = "Xuất xứ";
            this.gridColumn9.FieldName = "SETUPCOUNTRY";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 5;
            this.gridColumn9.Width = 90;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn10.Caption = "Năm SX";
            this.gridColumn10.FieldName = "SETUPYEAR";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 6;
            this.gridColumn10.Width = 80;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "Barcode";
            this.gridColumn1.FieldName = "BARCODE";
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 15;
            this.gridColumn1.Width = 130;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.Caption = "Định lượng";
            this.gridColumn5.FieldName = "QUANTIFIED";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 7;
            this.gridColumn5.Width = 100;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.Caption = "Dung lượng";
            this.gridColumn13.FieldName = "CAPACITY";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 8;
            this.gridColumn13.Width = 100;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.Caption = "Chất liệu";
            this.gridColumn12.FieldName = "MATERITION";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 9;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn11.Caption = "Công dụng";
            this.gridColumn11.FieldName = "USES";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 10;
            this.gridColumn11.Width = 100;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "Dùng cho sản phẩm";
            this.gridColumn6.FieldName = "USEPRODUCT";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 11;
            this.gridColumn6.Width = 150;
            // 
            // gridColumn16
            // 
            this.gridColumn16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn16.Caption = "Bảo quản";
            this.gridColumn16.FieldName = "PRESERVE";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 12;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.Caption = "HDSD";
            this.gridColumn15.FieldName = "HOWUSE";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 13;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.Caption = "Nhập khẩu và phân phối";
            this.gridColumn14.FieldName = "DISTRIBUTE";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 14;
            this.gridColumn14.Width = 180;
            // 
            // spePrice
            // 
            this.spePrice.AutoHeight = false;
            this.spePrice.DisplayFormat.FormatString = "#,##0.##";
            this.spePrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spePrice.EditFormat.FormatString = "#,##0.##";
            this.spePrice.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spePrice.Mask.EditMask = "\\d{0,12}";
            this.spePrice.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.spePrice.MaxLength = 13;
            this.spePrice.Name = "spePrice";
            this.spePrice.NullText = "0";
            // 
            // txtNote
            // 
            this.txtNote.AutoHeight = false;
            this.txtNote.MaxLength = 200;
            this.txtNote.Name = "txtNote";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // speOldPrice
            // 
            this.speOldPrice.AutoHeight = false;
            this.speOldPrice.DisplayFormat.FormatString = "#,##0.##";
            this.speOldPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.speOldPrice.EditFormat.FormatString = "#,##0.##";
            this.speOldPrice.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.speOldPrice.Name = "speOldPrice";
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            this.repositoryItemDateEdit2.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(732, 32);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(90, 25);
            this.btnSearch.TabIndex = 8;
            this.btnSearch.Text = "     Tìm kiếm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cboMainGroupIDList
            // 
            this.cboMainGroupIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroupIDList.Location = new System.Drawing.Point(103, 5);
            this.cboMainGroupIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroupIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroupIDList.Name = "cboMainGroupIDList";
            this.cboMainGroupIDList.Size = new System.Drawing.Size(268, 24);
            this.cboMainGroupIDList.TabIndex = 1;
            this.cboMainGroupIDList.SelectionChangeCommitted += new System.EventHandler(this.cboMainGroupIDList_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ngành hàng:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(375, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tình trạng:";
            // 
            // cboRVLStatus
            // 
            this.cboRVLStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRVLStatus.FormattingEnabled = true;
            this.cboRVLStatus.Items.AddRange(new object[] {
            "Chưa có Barcode",
            "Đã có Barcode"});
            this.cboRVLStatus.Location = new System.Drawing.Point(466, 32);
            this.cboRVLStatus.Name = "cboRVLStatus";
            this.cboRVLStatus.Size = new System.Drawing.Size(260, 24);
            this.cboRVLStatus.TabIndex = 7;
            // 
            // cboProductList
            // 
            this.cboProductList.BackColor = System.Drawing.Color.Transparent;
            this.cboProductList.ConsignmentType = -1;
            this.cboProductList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProductList.Location = new System.Drawing.Point(103, 32);
            this.cboProductList.Margin = new System.Windows.Forms.Padding(4);
            this.cboProductList.Name = "cboProductList";
            this.cboProductList.ProductIDList = "";
            this.cboProductList.Size = new System.Drawing.Size(267, 23);
            this.cboProductList.TabIndex = 5;
            // 
            // cboBrandIDList
            // 
            this.cboBrandIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBrandIDList.Location = new System.Drawing.Point(466, 5);
            this.cboBrandIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboBrandIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboBrandIDList.Name = "cboBrandIDList";
            this.cboBrandIDList.Size = new System.Drawing.Size(260, 24);
            this.cboBrandIDList.TabIndex = 3;
            this.cboBrandIDList.SelectionChangeCommitted += new System.EventHandler(this.cboBrandIDList_SelectionChangeCommitted);
            // 
            // mnuReview
            // 
            this.mnuReview.Enabled = false;
            this.mnuReview.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1});
            this.mnuReview.Name = "contextMenuStrip1";
            this.mnuReview.Size = new System.Drawing.Size(61, 10);
            this.mnuReview.Opening += new System.ComponentModel.CancelEventHandler(this.mnuReview_Opening);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(57, 6);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportExcel.Enabled = false;
            this.btnExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportExcel.Location = new System.Drawing.Point(90, 7);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(97, 25);
            this.btnExportExcel.TabIndex = 3;
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExportExcel.UseVisualStyleBackColor = true;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // frmProduct_Barcode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(953, 494);
            this.Controls.Add(this.cboBrandIDList);
            this.Controls.Add(this.cboProductList);
            this.Controls.Add(this.cboRVLStatus);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cboMainGroupIDList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.grdProduct_Barcode);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProduct_Barcode";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tạo Barcode sản phẩm";
            this.Load += new System.EventHandler(this.frmPromotionQuotationAdd_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProduct_Barcode)).EndInit();
            this.mnuAction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdViewProduct_Barcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speOldPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            this.mnuReview.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnPrintBarcode;
        private System.Windows.Forms.Button btnUpdate;
        private DevExpress.XtraGrid.GridControl grdProduct_Barcode;
        private DevExpress.XtraGrid.Views.Grid.GridView grdViewProduct_Barcode;
        private DevExpress.XtraGrid.Columns.GridColumn ISSELECT;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chkIsSelect;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtNote;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit spePrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private System.Windows.Forms.ContextMenuStrip mnuAction;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcel;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit speOldPrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private System.Windows.Forms.Button btnGenBarcode;
        private System.Windows.Forms.Button btnSearch;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroupIDList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboRVLStatus;
        private System.Windows.Forms.ToolStripMenuItem mnuItemImportExcel;
        private MasterData.DUI.CustomControl.Product.cboProductList cboProductList;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private Library.AppControl.ComboBoxControl.ComboBoxBrand cboBrandIDList;
        private System.Windows.Forms.ToolStripMenuItem cmdUnCheckAll;
        private System.Windows.Forms.ToolStripMenuItem cmdCheckAll;
        private System.Windows.Forms.ContextMenuStrip mnuReview;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnExportExcel;
    }
}