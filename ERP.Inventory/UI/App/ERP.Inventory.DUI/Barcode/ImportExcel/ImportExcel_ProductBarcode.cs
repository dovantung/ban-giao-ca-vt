﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ERP.MasterData.DUI.CustomControl.ImportExcel;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Barcode.ImportExcel
{
    public class ImportExcel_ProductBarcode : IExcelImport
    {
        public DataTable dtb_data = null;
        ClsImportExcel objClsImportExcel;
        public ImportExcel_ProductBarcode()
        {
            objClsImportExcel = new ClsImportExcel(this);
            objClsImportExcel.InitializeColumnGridView(
                new DataColumnInfor("PRODUCTID", "Mã sản phẩm", typeof(string), 120, true, 1, DataColumnInfor.TypeShow.ALL),
                new DataColumnInfor("BRANDNAME", "Tên nhà sản xuất", typeof(string), 150, true, 2, DataColumnInfor.TypeShow.ALL),
                new DataColumnInfor("BRANDADDRESS", "Địa chỉ", typeof(string), 100, true, 3, DataColumnInfor.TypeShow.ALL),
                new DataColumnInfor("SETUPCOUNTRY", "Xuất xứ", typeof(string), 70, true, 4, DataColumnInfor.TypeShow.ALL),
                new DataColumnInfor("SETUPYEAR", "Năm SX", typeof(string), 70, true, 5, DataColumnInfor.TypeShow.ALL),
                new DataColumnInfor("QUANTIFIED", "Định lượng", typeof(string), 85, true, 6, DataColumnInfor.TypeShow.ALL),
                new DataColumnInfor("CAPACITY", "Dung lượng", typeof(string), 85, true, 7, DataColumnInfor.TypeShow.ALL),
                new DataColumnInfor("MATERITION", "Chất liệu", typeof(string), 75, true, 8, DataColumnInfor.TypeShow.ALL),
                new DataColumnInfor("USES", "Công dụng", typeof(string), 85, true, 9, DataColumnInfor.TypeShow.ALL),
                new DataColumnInfor("USEPRODUCT", "Dùng cho sản phẩm", typeof(string), 150, true, 10, DataColumnInfor.TypeShow.ALL),
                new DataColumnInfor("PRESERVE", "Bảo quản", typeof(string), 75, true, 11, DataColumnInfor.TypeShow.ALL),
                new DataColumnInfor("HOWUSE", "HDSD", typeof(string), 75, true, 12, DataColumnInfor.TypeShow.ALL),
                new DataColumnInfor("DISTRIBUTE", "Nhập khẩu và phân phối", typeof(string), 200, true, 13, DataColumnInfor.TypeShow.ALL),
                new DataColumnInfor("PRODUCTNAME", "Tên sản phẩm", typeof(string), 120, false, 14, DataColumnInfor.TypeShow.ONLY_GRID_DATA)
                );
        }

        public string IExcelImport_FormTitle
        {
            get
            {
                return "Nạp danh sách Gen Barcode";
            }
        }

        public List<object> IExcelImport_ReturnDataImport(DataTable dtbData, DataRow drExcelFile, out bool IsError, out string strMessenger)
        {
            IsError = false;
            strMessenger = string.Empty;
            string strProductID = (drExcelFile[0] ?? "").ToString().Trim();
            string strBRANDNAME = (drExcelFile.ItemArray.Count() <= 1 ? "" : drExcelFile[1].ToString().Trim());
            string strBRANDADDRESS = (drExcelFile.ItemArray.Count() <= 2 ? "" : drExcelFile[2].ToString().Trim());
            string strSETUPCOUNTRY = (drExcelFile.ItemArray.Count() <= 3 ? "" : drExcelFile[3].ToString().Trim());
            string strSETUPYEAR = (drExcelFile.ItemArray.Count() <= 4 ? "": drExcelFile[4].ToString().Trim());
            string strQUANTIFIED = (drExcelFile.ItemArray.Count() <= 5 ? "" : drExcelFile[5].ToString().Trim());
            string strCAPACITY = (drExcelFile.ItemArray.Count() <= 6 ? "" : drExcelFile[6].ToString().Trim());
            string strMATERITION = (drExcelFile.ItemArray.Count() <= 7 ? "" : drExcelFile[7].ToString().Trim());
            string strUSES = (drExcelFile.ItemArray.Count() <= 8 ? "" : drExcelFile[8].ToString().Trim());
            string strUESPRODUCT = (drExcelFile.ItemArray.Count() <= 9 ? "" : drExcelFile[9].ToString().Trim());
            string strPRESERVE = (drExcelFile.ItemArray.Count() <= 10 ? "" : drExcelFile[10].ToString().Trim());
            string strHOWUSE = (drExcelFile.ItemArray.Count() <= 11 ? "" : drExcelFile[11].ToString().Trim());
            string strDISTRIBUTE = (drExcelFile.ItemArray.Count() <= 12 ? "" : drExcelFile[12].ToString().Trim());
            string strProductName = "";
            List<object> objReturn;
            try
            {
                if (string.IsNullOrEmpty(strProductID))
                {
                    throw (new Exception("Mã sản phẩm không được bỏ trống!"));
                }
                if (dtb_data != null)
                {
                    DataRow[] rowExistUser = dtb_data.Select("ProductID = '" + strProductID + "'");
                    if (rowExistUser == null || rowExistUser.Length < 1)
                        throw (new Exception("Mã sản phẩm không tồn tại!"));
                    else
                    {
                        if (rowExistUser[0]["BARCODE"].ToString() != string.Empty)
                            throw (new Exception("Mã sản phẩm đã có Barcode!"));
                    }

                    DataRow[] arrDR = dtb_data.Select(string.Format("ProductID = '" + strProductID + "'"));
                    if (arrDR != null && arrDR.Length > 0)
                    {
                        if (Convert.ToBoolean(arrDR[0]["ISREQUESTIMEI"]) == true)
                            throw (new Exception("Sản phẩm có IMEI không được tạo Barcode!"));

                        strProductName = arrDR[0]["ProductName"].ToString();
                        // Thêm cột strBRANDNAME
                        if (strBRANDNAME.Length > 500)
                            throw (new Exception("Nhà sản xuất vượt quá 500 ký tự!"));

                        // Thêm cột BRANDADDRESS
                        if (strBRANDADDRESS.Length > 500)
                            throw (new Exception("Địa chỉ vượt quá 500 ký tự!"));

                        // Thêm cột SETUPCOUNTRY
                        if (strSETUPCOUNTRY.Length > 500)
                            throw (new Exception("Xuất xứ vượt quá 500 ký tự!"));

                        // Thêm cột SETUPYEAR
                        
                        if (strSETUPYEAR.Length > 100)
                            throw (new Exception("Năm sản xuất vượt quá 100 ký tự!"));

                        // Thêm cột QUANTIFIED 
                        if (strSETUPYEAR.Length > 100)
                            throw (new Exception("Định lượng vượt quá 100 ký tự!"));

                        // Thêm cột CAPACITY
                        if (strSETUPYEAR.Length > 100)
                            throw (new Exception("Dung lượng vượt quá 100 ký tự!"));

                        // Thêm cột MATERITION
                        if (strMATERITION.Length > 500)
                            throw (new Exception("Chất liệu vượt quá 500 ký tự!"));

                        // Thêm cột USES
                        if (strUSES.Length > 500)
                            throw (new Exception("Công dụng vượt quá 500 ký tự!"));

                        // Thêm cột UESPRODUCT
                        if (strUESPRODUCT.Length > 500)
                            throw (new Exception("Dùng cho sản phẩm vượt quá 500 ký tự!"));

                        // Thêm cột PRESERVE
                        if (strPRESERVE.Length > 500)
                            throw (new Exception("Bảo quản vượt quá 500 ký tự!"));

                        // Thêm cột HOWUSE
                        if (strHOWUSE.Length > 2000)
                            throw (new Exception("HDSD vượt quá 2000 ký tự!"));

                        // Thêm cột DISTRIBUTE
                        if (strDISTRIBUTE.Length > 200)
                            throw (new Exception("Nhập khẩu và phân phối vượt quá 200 ký tự!"));
                    }
                    else
                    {
                        throw (new Exception("Mã sản phẩm không tồn tại!"));
                    }

                }
                if (dtbData != null)
                {
                    var abc = dtbData.Select("ProductID ='" + strProductID + "'");
                    if (abc.Any())
                    {
                        throw (new Exception("Mã sản phẩm đã tồn tại!"));
                    }
                }
            }
            catch(Exception ex)
            {

                IsError = true;
                if (string.IsNullOrEmpty(strMessenger))
                    strMessenger = ex.Message;
                else strMessenger += ", " + ex.Message;
            }
            finally
            {
                objReturn =  new List<object> { strProductID, strBRANDNAME, strBRANDADDRESS, strSETUPCOUNTRY, strSETUPYEAR, strQUANTIFIED, strCAPACITY,
                    strMATERITION, strUSES, strUESPRODUCT, strPRESERVE, strHOWUSE, strDISTRIBUTE, strProductName };
            }
            return objReturn;
        }   


        public void Import(ref DataTable dtbSimIssue)
        {
           if(objClsImportExcel.DoImportExcel())
            {
                if (objClsImportExcel.dtbReturn != null)
                {
                    DataTable dtb = new DataTable();
                    dtb = objClsImportExcel.dtbReturn;
                    dtb.Columns.Remove("ISERROR");
                    dtb.Columns.Remove("NOTE");
                    dtbSimIssue = dtb;
                    MessageBox.Show("Import Excel danh sách thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
