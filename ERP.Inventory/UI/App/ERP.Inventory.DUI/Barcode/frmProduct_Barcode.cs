﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppControl;
using Library.AppCore.LoadControls;
using ERP.Inventory.PLC;
using ERP.MasterData.DUI.Common;
using Library.AppCore;
using Library.AppCore.DataSource;
using Library.AppCore.Constant;
using ERP.Inventory.DUI.Barcode.ImportExcel;
using DevExpress.XtraGrid.Views.Grid;
using System.Collections;

namespace ERP.Inventory.DUI.Barcode
{
    public partial class frmProduct_Barcode : Form
    {
        #region variables

        private int intCustomerID = -1;
        private DataTable dtbProduct_BarcodeData = null;
        private DataTable dtbProduct = null;
        private int intReportID = 0;
        PLCProductGenBarcode objPLCProductGenBarcode = new PLCProductGenBarcode();
        PLC.WSProductGenBarcode.ResultMessage objResultMessage = new PLC.WSProductGenBarcode.ResultMessage();
        private bool bolIsPermissionUpdate = SystemConfig.objSessionUser.IsPermission("MD_PRODUCT_BARCODE_ADD");
        string strMD_MAINGROUP_BARCODE = Library.AppCore.AppConfig.GetStringConfigValue("MD_MAINGROUP_BARCODE");
        string strMD_STRINGBARCODE = Library.AppCore.AppConfig.GetStringConfigValue("MD_STRINGBARCODE");
        private int intFocus = -1;
        private bool IsEdit = false;
        private string strProductID = string.Empty;
        object[] objKeyword = new object[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        #endregion

        #region public methods    

        public int CustomerID
        {
            get { return intCustomerID; }
            set { intCustomerID = value; }
        }

        public frmProduct_Barcode()
        {
            InitializeComponent();
        }
        #endregion

        #region functions
        private void InitControl()
        {
            dtbProduct_BarcodeData = CreateDataTableData();
            grdProduct_Barcode.DataSource = dtbProduct_BarcodeData;
            grdProduct_Barcode.RefreshDataSource();
            this.mnuAction.Opening += new CancelEventHandler(mnuAction_Opening);
            this.btnUpdate.Click += new EventHandler(btnUpdate_Click);
            this.grdViewProduct_Barcode.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(grdViewPromotionQuotation_CellValueChanging);
            this.btnUndo.Click += new EventHandler(btnUndo_Click);
            this.grdViewProduct_Barcode.KeyDown += new KeyEventHandler(grdViewPromotionQuotation_KeyDown);
            this.grdViewProduct_Barcode.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(grdViewPromotionQuotation_CellValueChanging);
            cboRVLStatus.SelectedIndex = 0;
        }

        private void LoadComboBox()
        {

            cboMainGroupIDList.InitControl(true, true, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL,
                    Library.AppCore.Constant.EnumType.IsServiceType.ALL, Library.AppCore.Constant.EnumType.MainGroupPermissionType.ALL, "MainGroupID in (" + strMD_MAINGROUP_BARCODE + ")");
            string strOtherCondition = "<" + strMD_MAINGROUP_BARCODE.Replace(",", "><") + ">";
            cboMainGroupIDList.IsReturnAllWhenNotChoose = false;
            cboBrandIDList.InitControl(false, strOtherCondition);
            cboProductList.InitControl(strOtherCondition,
                string.Empty, string.Empty);
            cboRVLStatus.SelectedIndex = 0;
        }

        private DataTable CreateDataTableData()
        {
            DataTable dt = new DataTable("ProductData");
            DataColumn colIsSelect = new DataColumn("ISSELECT", typeof(decimal));
            colIsSelect.DefaultValue = 0;
            dt.Columns.Add(colIsSelect);
            dt.Columns.Add("PRODUCTID", typeof(String));
            dt.Columns.Add("PRODUCTNAME", typeof(String));
            dt.Columns.Add("BRANDNAME", typeof(string));
            dt.Columns.Add("BRANDADDRESS", typeof(string));
            dt.Columns.Add("SETUPCOUNTRY", typeof(string));
            dt.Columns.Add("SETUPYEAR", typeof(string));
            dt.Columns.Add("BARCODE", typeof(string));
            dt.Columns.Add("QUANTIFIED", typeof(string));
            dt.Columns.Add("CAPACITY", typeof(string));
            dt.Columns.Add("MATERITION", typeof(string));
            dt.Columns.Add("USES", typeof(string));
            dt.Columns.Add("USEPRODUCT", typeof(string));
            dt.Columns.Add("PRESERVE", typeof(string));
            dt.Columns.Add("HOWUSE", typeof(string));
            dt.Columns.Add("DISTRIBUTE", typeof(string));

            dt.Columns["ISSELECT"].Caption = "Chọn";
            dt.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
            dt.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
            dt.Columns["BRANDNAME"].Caption = "Tên nhà sản xuất";
            dt.Columns["BRANDADDRESS"].Caption = "Địa chỉ";
            dt.Columns["SETUPCOUNTRY"].Caption = "Xuất xứ";
            dt.Columns["SETUPYEAR"].Caption = "Năm SX";
            dt.Columns["SETUPYEAR"].Caption = "Barcode";
            dt.Columns["QUANTIFIED"].Caption = "Định lượng";
            dt.Columns["CAPACITY"].Caption = "Dung lượng";
            dt.Columns["MATERITION"].Caption = "Chất liệu";
            dt.Columns["USES"].Caption = "Công dụng";
            dt.Columns["USEPRODUCT"].Caption = "Dùng cho sản phẩm";
            dt.Columns["PRESERVE"].Caption = "Bảo quản";
            dt.Columns["HOWUSE"].Caption = "HDSD";
            dt.Columns["DISTRIBUTE"].Caption = "Nhập khẩu và phân phối";

            return dt;
        }

        private bool ValidateData()
        {
            DataRow[] dr = ((DataTable)grdProduct_Barcode.DataSource).Select("ISSELECT = 1");
            if (dr.Length == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn ít nhất 1 dòng!");
                return false;
            }
            return true;
        }

        private bool UpdataData()
        {
            try
            {
                if (!IsEdit)
                    objPLCProductGenBarcode.Insert(UpdateProduct_GenBarcode());
                else
                    objPLCProductGenBarcode.Update(UpdateProduct_GenBarcode());

                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                    return false;
                }
                if (!IsEdit)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Đã thêm thành công Gen Barcode");
                }
                else
                    MessageBoxObject.ShowWarningMessage(this, "Đã cập nhật thành công Gen Barcode");
                IsEdit = true;
                btnPrintBarcode.Enabled = true;
                DataTable dtbProduct = Library.AppCore.DataSource.GetDataSource.GetProduct();
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi Gen Barcode");
                SystemErrorWS.Insert("Lỗi Gen Barcode", objExce.ToString(), this.Name + " -> Update", Globals.ModuleName);
                return false;
            }
            return true;
        }
        #endregion

        #region events
        private void frmPromotionQuotationAdd_Load(object sender, EventArgs e)
        {
            if (IsHaveTagForm())
                LoadComboBox();
            InitControl();
        }

        public bool IsHaveTagForm()
        {
            // Lay Thong tin loai xuat muong hang 
            if (this.Tag != null && this.Tag.ToString().Trim() != string.Empty)
            {
                try
                {
                    Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                    if (hstbParam.ContainsKey("REPORTID"))
                    {
                        intReportID = hstbParam["REPORTID"] != null ? Convert.ToInt32(hstbParam["REPORTID"]) : 0;
                        return true;
                    }
                }
                catch { }
            }
            return false;
        }
        void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
                return;
            if (!UpdataData())
                return;
            //this.Close();
        }

        void grdViewPromotionQuotation_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

        }

        void btnUndo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void grdViewPromotionQuotation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                Clipboard.SetText(grdViewProduct_Barcode.GetFocusedDisplayText());
                e.Handled = true;
            }
        }
        #endregion

        private void mnuItemDelete_Click(object sender, EventArgs e)
        {
            DataTable dtb = (DataTable)grdProduct_Barcode.DataSource;
            if (dtb == null)
                return;
            List<string> lstFields = new List<string>();
            List<int> lstWidths = new List<int>();
            DevExpress.XtraGrid.GridControl grdTemp = new DevExpress.XtraGrid.GridControl();
            DevExpress.XtraGrid.Views.Grid.GridView grvTemp = new DevExpress.XtraGrid.Views.Grid.GridView();
            grdTemp.MainView = grvTemp;
            lstFields.Add(grdViewProduct_Barcode.Columns["PRODUCTID"].Caption); // 1
            lstWidths.Add(grdViewProduct_Barcode.Columns["PRODUCTID"].Width);
            lstFields.Add(grdViewProduct_Barcode.Columns["BRANDNAME"].Caption); //3
            lstWidths.Add(grdViewProduct_Barcode.Columns["BRANDNAME"].Width);
            lstFields.Add(grdViewProduct_Barcode.Columns["BRANDADDRESS"].Caption); //3
            lstWidths.Add(grdViewProduct_Barcode.Columns["BRANDADDRESS"].Width);
            lstFields.Add(grdViewProduct_Barcode.Columns["SETUPCOUNTRY"].Caption); //4
            lstWidths.Add(grdViewProduct_Barcode.Columns["SETUPCOUNTRY"].Width);
            lstFields.Add(grdViewProduct_Barcode.Columns["SETUPYEAR"].Caption);//5
            lstWidths.Add(grdViewProduct_Barcode.Columns["SETUPYEAR"].Width);
            lstFields.Add(grdViewProduct_Barcode.Columns["QUANTIFIED"].Caption);//7
            lstWidths.Add(grdViewProduct_Barcode.Columns["QUANTIFIED"].Width);
            lstFields.Add(grdViewProduct_Barcode.Columns["CAPACITY"].Caption);//8
            lstWidths.Add(grdViewProduct_Barcode.Columns["CAPACITY"].Width);
            lstFields.Add(grdViewProduct_Barcode.Columns["MATERITION"].Caption);//9
            lstWidths.Add(grdViewProduct_Barcode.Columns["MATERITION"].Width);
            lstFields.Add(grdViewProduct_Barcode.Columns["USES"].Caption);//10
            lstWidths.Add(grdViewProduct_Barcode.Columns["USES"].Width);
            lstFields.Add(grdViewProduct_Barcode.Columns["USEPRODUCT"].Caption);//11
            lstWidths.Add(grdViewProduct_Barcode.Columns["USEPRODUCT"].Width);
            lstFields.Add(grdViewProduct_Barcode.Columns["PRESERVE"].Caption);//12
            lstWidths.Add(grdViewProduct_Barcode.Columns["PRESERVE"].Width);
            lstFields.Add(grdViewProduct_Barcode.Columns["HOWUSE"].Caption);//13
            lstWidths.Add(grdViewProduct_Barcode.Columns["HOWUSE"].Width);
            lstFields.Add(grdViewProduct_Barcode.Columns["DISTRIBUTE"].Caption);//14
            lstWidths.Add(grdViewProduct_Barcode.Columns["DISTRIBUTE"].Width);
            GridCustom.FormatGrid(grvTemp, lstFields.ToArray(), lstFields.ToArray(), lstWidths.ToArray(), false);
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdTemp, DevExpress.XtraPrinting.TextExportMode.Text, false);
        }

        private void cboMainGroupIDList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboMainGroupIDList.MainGroupIDList == "")
            {
                string strOtherCondition = "<" + strMD_MAINGROUP_BARCODE.Replace(",", "><") + ">";
                cboBrandIDList.InitControl(true, strOtherCondition);
                cboProductList.InitControl(strOtherCondition, string.Empty, cboBrandIDList.BrandIDList, false, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL, Library.AppCore.Constant.EnumType.IsServiceType.ALL, Library.AppCore.Constant.EnumType.MainGroupPermissionType.ALL);
            }
            else
            {
                cboBrandIDList.InitControl(true, cboMainGroupIDList.MainGroupIDList);
                cboProductList.InitControl(cboMainGroupIDList.MainGroupIDList, string.Empty, cboBrandIDList.BrandIDList, false, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL, Library.AppCore.Constant.EnumType.IsServiceType.ALL, Library.AppCore.Constant.EnumType.MainGroupPermissionType.ALL);
            }

            //  cboProductList.InitControl(cboMainGroupIDList.MainGroupIDList,
            //string.Empty, cboBrandIDList.BrandIDList);
        }

        private void cboBrandIDList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cboMainGroupIDList.MainGroupIDList == "")
            {
                string strOtherCondition = "<" + strMD_MAINGROUP_BARCODE.Replace(",", "><") + ">";
                cboProductList.InitControl(strOtherCondition, string.Empty, cboBrandIDList.BrandIDList, false, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL, Library.AppCore.Constant.EnumType.IsServiceType.ALL, Library.AppCore.Constant.EnumType.MainGroupPermissionType.ALL);
            }
            else
                cboProductList.InitControl(cboMainGroupIDList.MainGroupIDList, string.Empty, cboBrandIDList.BrandIDList, false, Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL, Library.AppCore.Constant.EnumType.IsServiceType.ALL, Library.AppCore.Constant.EnumType.MainGroupPermissionType.ALL);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchData(cboMainGroupIDList.MainGroupIDList, cboBrandIDList.BrandIDList, cboProductList.ProductIDList, cboRVLStatus.SelectedIndex);
        }

        private void SearchData(string MainGroupIDList, string BrandIDList, string ProductIDList, int Status)
        {
            string strOtherCondition = string.Empty;
            if (MainGroupIDList == "")
                strOtherCondition = "<" + strMD_MAINGROUP_BARCODE.Replace(",", "><") + ">";
            else
                strOtherCondition = MainGroupIDList;

            object[] objKeywords = new object[]
                   {
                        "@MaingroupIDList", strOtherCondition,
                        "@BrandIDList", BrandIDList,
                        "@ProductIDList", ProductIDList,
                        "@IsStatus" ,  Status,
                        "@ISREQUESTIMEI", 0
                   };
            objResultMessage = objPLCProductGenBarcode.SearchData(ref dtbProduct, objKeywords);
            if (objResultMessage.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin danh sách Barcode!");
                return;
            }
            foreach (DataRow row in dtbProduct.Rows)
                row["ISSELECT"] = cboRVLStatus.SelectedIndex == 0 ? false : true;
            grdProduct_Barcode.DataSource = dtbProduct;
            btnPrintBarcode.Enabled = cboRVLStatus.SelectedIndex == 0 ? false : true;
            btnGenBarcode.Enabled = !btnPrintBarcode.Enabled;
            if (btnGenBarcode.Enabled)
                btnGenBarcode.Enabled = bolIsPermissionUpdate;

            if (btnPrintBarcode.Enabled)
                btnUpdate.Enabled = bolIsPermissionUpdate;
            else
                btnUpdate.Enabled = false;
         //   btnUpdate.Enabled = btnPrintBarcode.Enabled;
            //btnUpdate.Enabled =
             btnExportExcel.Enabled = grdViewProduct_Barcode.DataRowCount > 0;
            IsEdit = btnPrintBarcode.Enabled;
        }

        private void mnuItemImportExcel_Click(object sender, EventArgs e)
        {
            if (grdViewProduct_Barcode.RowCount > 0)
            {
                if (MessageBox.Show(this, "Trên lưới đã có dữ liệu.\nChương trình sẽ xóa dữ liệu cũ và thay thế bằng dữ liệu mới", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    return;
            }
            //string strProductList = "<";
            //strProductList += string.Join("><", dt.Rows.OfType<DataRow>().Select(r => r[0].ToString().Trim()));
            //strProductList += ">";
            string strOtherCondition = string.Empty;
            if (cboMainGroupIDList.MainGroupIDList == "")
                strOtherCondition = "<" + strMD_MAINGROUP_BARCODE.Replace(",", "><") + ">";
            else
                strOtherCondition = cboMainGroupIDList.MainGroupIDList;
            object[] objKeywords = new object[]
              {
                        "@MaingroupIDList", cboMainGroupIDList.MainGroupIDList,
                        "@BrandIDList", "",
                        "@ProductIDList", "",
                        "@IsStatus" ,  2
              };
            DataTable dtb = null;
            objResultMessage = objPLCProductGenBarcode.SearchData(ref dtb, objKeywords);
            if (objResultMessage.IsError)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin danh sách Barcode!");
                return;
            }
            DataTable dtbTemp = null;
            ImportExcel_ProductBarcode frm = new ImportExcel_ProductBarcode();
            frm.dtb_data = dtb;
            frm.Import(ref dtbTemp);
            if (dtbTemp != null)
            {
                grdProduct_Barcode.DataSource = dtbTemp;
                grdViewProduct_Barcode.RefreshData();
                if (dtbTemp.Rows.Count > 0)
                {
                    dtbTemp.Columns.Add("BARCODE", typeof(string));
                    foreach (DataRow dr in dtbTemp.Rows)
                    {
                        if (!dtbTemp.Columns.Contains("ISSELECT"))
                            dtbTemp.Columns.Add("ISSELECT", typeof(decimal));
                        dr["ISSELECT"] = 1;
                    }
                    btnPrintBarcode.Enabled = false;
                    btnUpdate.Enabled = false;
                    btnGenBarcode.Enabled = !btnPrintBarcode.Enabled;
                    if (btnGenBarcode.Enabled)
                        btnGenBarcode.Enabled = bolIsPermissionUpdate;
                }
                btnGenBarcode.Enabled = !btnPrintBarcode.Enabled;
                if (btnGenBarcode.Enabled)
                    btnGenBarcode.Enabled = bolIsPermissionUpdate;
                if (btnPrintBarcode.Enabled)
                    btnUpdate.Enabled = bolIsPermissionUpdate;
                else
                    btnUpdate.Enabled = false;
               // btnUpdate.Enabled = btnPrintBarcode.Enabled;
                IsEdit = btnPrintBarcode.Enabled;
                LoadComboBox();
            }


            //if (cboMainGroupIDList.MainGroupIDList == string.Empty)
            //{
            //    MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn ngành hàng!");
            //    cboMainGroupIDList.Focus();
            //    return;
            //}
            //if (cboBrandIDList.BrandID < 0)
            //{
            //    MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn nhà sản xuất!");
            //    cboBrandIDList.Focus();
            //    return;
            //}
            //if (grdViewProduct_Barcode.RowCount > 0)
            //{
            //    if (MessageBox.Show(this, "Trên lưới đã có dữ liệu.\nChương trình sẽ xóa dữ liệu cũ và thay thế bằng dữ liệu mới", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
            //        return;
            //}
            //OpenFileDialog Openfile = new OpenFileDialog();
            //Openfile.Filter = "Excel 2003|*.xls|Excel New|*.xlsx";
            //if (Openfile.ShowDialog() == DialogResult.Cancel) return;
            //try
            //{
            //    DataTable dt = new DataTable();
            //    dt = Library.AppCore.LoadControls.C1ExcelObject.OpenExcel2DataTable(Openfile.FileName);
            //    ValidateData(dt);
            //    btnPrintBarcode.Enabled = false;
            //    btnGenBarcode.Enabled = !btnPrintBarcode.Enabled;
            //    btnUpdate.Enabled = btnPrintBarcode.Enabled;
            //    IsEdit = btnPrintBarcode.Enabled;
            //}
            //catch (Exception objex)
            //{
            //    Library.AppCore.SystemErrorWS.Insert("Lỗi nạp thông tin báo giá khuyến mãi chi nhánh nhà cung cấp từ file Excel!", objex, DUIInventory_Globals.ModuleName);
            //    MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin báo giá khuyến mãi chi nhánh nhà cung cấp từ file Excel!", objex.Message.ToString());
            //}
        }

        private void ValidateData(DataTable dt)
        {
            try
            {
                if (dt == null || dt.Rows.Count == 0) return;
                if (dt.Columns.Count != 13)
                {
                    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột trong File Excle).");
                    return;
                }
                if (!"Mã sản phẩm".Equals(dt.Rows[0][0].ToString().Trim()))
                {
                    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột 'Mã sản phẩm' trong File Excle).");
                    return;
                }
                if (!"Tên nhà sản xuất".Equals(dt.Rows[0][1].ToString().Trim()))
                {
                    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột 'Tên nhà sản xuất' trong File Excle).");
                    return;
                }
                if (!"Địa chỉ".Equals(dt.Rows[0][2].ToString().Trim()))
                {
                    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột 'Địa chỉ' trong File Excle).");
                    return;
                }
                if (!"Xuất xứ".Equals(dt.Rows[0][3].ToString().Trim()))
                {
                    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột 'Xuất xứ' trong File Excle).");
                    return;
                }
                if (!"Năm SX".Equals(dt.Rows[0][4].ToString().Trim()))
                {
                    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột 'Năm SX' trong File Excle).");
                    return;
                }
                //if (!"Barcode".Equals(dt.Rows[0][5].ToString().Trim()))
                //{
                //    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột 'Barcode' trong File Excle).");
                //    return;
                //}
                if (!"Định lượng".Equals(dt.Rows[0][5].ToString().Trim()))
                {
                    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột 'Định lượng' trong File Excle).");
                    return;
                }
                if (!"Dung lượng".Equals(dt.Rows[0][6].ToString().Trim()))
                {
                    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột 'Dung lượng' trong File Excle).");
                    return;
                }
                if (!"Chất liệu".Equals(dt.Rows[0][7].ToString().Trim()))
                {
                    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột 'Chất liệu' trong File Excle).");
                    return;
                }
                if (!"Công dụng".Equals(dt.Rows[0][8].ToString().Trim()))
                {
                    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột 'Công dụng' trong File Excle).");
                    return;
                }
                if (!"Dùng cho sản phẩm".Equals(dt.Rows[0][9].ToString().Trim()))
                {
                    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột 'Dùng cho sản phẩm' trong File Excle).");
                    return;
                }
                if (!"Bảo quản".Equals(dt.Rows[0][10].ToString().Trim()))
                {
                    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột 'Bảo quản' trong File Excle).");
                    return;
                }
                if (!"HDSD".Equals(dt.Rows[0][11].ToString().Trim()))
                {
                    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột 'HDSD' trong File Excle).");
                    return;
                }
                if (!"Nhập khẩu và phân phối".Equals(dt.Rows[0][12].ToString().Trim()))
                {
                    MessageBoxObject.ShowWarningMessage(this, "File nhập không đúng định dạng.\nVui lòng kiểm tra lại( Thiếu cột 'Nhập khẩu và phân phối' trong File Excle).");
                    return;
                }
                string strProductList = "<";
                strProductList += string.Join("><", dt.Rows.OfType<DataRow>().Select(r => r[0].ToString().Trim()));
                strProductList += ">";
                object[] objKeywords = new object[]
                  {
                        "@MaingroupIDList", "",
                        "@BrandIDList", "",
                        "@ProductIDList", strProductList,
                        "@IsStatus" ,  2
                  };
                DataTable dtb = null;
                objResultMessage = objPLCProductGenBarcode.SearchData(ref dtb, objKeywords);
                if (objResultMessage.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp thông tin danh sách Barcode!");
                    return;
                }

                DataTable dtbTemp = (grdProduct_Barcode.DataSource as DataTable).Clone();
                DataTable dtbError = new DataTable("dtbError");
                dtbError.Columns.Add("ERROR", typeof(String));
                dtbError.Columns["ERROR"].Caption = "Cột lỗi";
                dtbError.Columns.Add("PRODUCTID", typeof(String));
                dtbError.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
                dtbError.Columns.Add("ERRORNOTE", typeof(String));
                dtbError.Columns["ERRORNOTE"].Caption = "Nội dung lỗi";
                for (int i = 1; i < dt.Rows.Count; i++)
                {
                    //if (Convert.ToBoolean(dt.Rows[i - 1]["ISSELECT"]) == false) continue;

                    DataRow dr = dtbError.NewRow();
                    dr["ERRORNOTE"] = "Ok";
                    #region CheckProductID
                    string ColumnExcelName = "";
                    ColumnExcelName = objKeyword[0].ToString();
                    string strProductID = Convert.ToString(dt.Rows[i][0]).Trim();
                    string strProductName = "";
                    string strBrandName = "";
                    dr[0] = "(" + ColumnExcelName + ", " + i + ")";
                    if (string.IsNullOrEmpty(strProductID))
                    {
                        dr["ERRORNOTE"] = "Mã sản phẩm không được bỏ trống.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }
                    dr[1] = strProductID;
                    DataRow[] rowExistUser = dtb.Select("ProductID = '" + strProductID + "'");
                    if (rowExistUser == null || rowExistUser.Length < 1)
                    {
                        dr["ERRORNOTE"] = "Mã sản phẩm không tồn tại.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }
                    else
                    {
                        if (rowExistUser[0]["BARCODE"].ToString() != string.Empty)
                        {
                            dr["ERRORNOTE"] = "Mã sản phẩm đã có Barcode.";
                            dtbError.Rows.Add(dr);
                            continue;
                        }
                        else
                        {
                            strProductName = rowExistUser[0]["ProductName"].ToString();
                            strBrandName = rowExistUser[0]["BrandName"].ToString();
                        }
                    }
                    DataRow[] row = dtbTemp.Select("ProductID = '" + strProductID + "'");
                    if (row == null || row.Length > 0)
                    {
                        dr["ERRORNOTE"] = "Mã sản phẩm đã tồn tại.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }
                    #endregion

                    // Thêm cột strBRANDNAME
                    string strBRANDNAME = Convert.ToString(dt.Rows[i][1]).Trim();
                    if (strBRANDNAME.Length > 500)
                    {
                        ColumnExcelName = objKeyword[1].ToString();
                        dr[0] = "(" + ColumnExcelName + ", " + i + ")";
                        dr["ERRORNOTE"] = "Nhà sản xuất vượt quá 500 ký tự.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }

                    // Thêm cột BRANDADDRESS
                    string strBRANDADDRESS = Convert.ToString(dt.Rows[i][2]).Trim();
                    if (strBRANDADDRESS.Length > 500)
                    {
                        ColumnExcelName = objKeyword[2].ToString();
                        dr[0] = "(" + ColumnExcelName + ", " + i + ")";
                        dr["ERRORNOTE"] = "Địa chỉ vượt quá 500 ký tự.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }

                    // Thêm cột SETUPCOUNTRY
                    string strSETUPCOUNTRY = Convert.ToString(dt.Rows[i][3]).Trim();
                    if (strSETUPCOUNTRY.Length > 500)
                    {
                        ColumnExcelName = objKeyword[3].ToString();
                        dr[0] = "(" + ColumnExcelName + ", " + i + ")";
                        dr["ERRORNOTE"] = "Xuất xứ vượt quá 500 ký tự.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }

                    // Thêm cột SETUPYEAR
                    string strSETUPYEAR = Convert.ToString(dt.Rows[i][4]).Trim();
                    if (strSETUPYEAR.Length > 100)
                    {
                        ColumnExcelName = objKeyword[4].ToString();
                        dr[0] = "(" + ColumnExcelName + ", " + i + ")";
                        dr["ERRORNOTE"] = "Năm sản xuất vượt quá 100 ký tự.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }

                    // Thêm cột QUANTIFIED
                    string strQUANTIFIED = Convert.ToString(dt.Rows[i][5]).Trim();
                    if (strSETUPYEAR.Length > 100)
                    {
                        ColumnExcelName = objKeyword[5].ToString();
                        dr[0] = "(" + ColumnExcelName + ", " + i + ")";
                        dr["ERRORNOTE"] = "Định lượng vượt quá 100 ký tự.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }

                    // Thêm cột CAPACITY
                    string strCAPACITY = Convert.ToString(dt.Rows[i][6]).Trim();
                    if (strSETUPYEAR.Length > 100)
                    {
                        ColumnExcelName = objKeyword[6].ToString();
                        dr[0] = "(" + ColumnExcelName + ", " + i + ")";
                        dr["ERRORNOTE"] = "Dung lượng vượt quá 100 ký tự.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }

                    // Thêm cột MATERITION
                    string strMATERITION = Convert.ToString(dt.Rows[i][7]).Trim();
                    if (strMATERITION.Length > 500)
                    {
                        ColumnExcelName = objKeyword[7].ToString();
                        dr[0] = "(" + ColumnExcelName + ", " + i + ")";
                        dr["ERRORNOTE"] = "Chất liệu vượt quá 500 ký tự.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }

                    // Thêm cột USES
                    string strUSES = Convert.ToString(dt.Rows[i][8]).Trim();
                    if (strUSES.Length > 500)
                    {
                        ColumnExcelName = objKeyword[8].ToString();
                        dr[0] = "(" + ColumnExcelName + ", " + i + ")";
                        dr["ERRORNOTE"] = "Công dụng vượt quá 500 ký tự.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }

                    // Thêm cột UESPRODUCT
                    string strUESPRODUCT = Convert.ToString(dt.Rows[i][9]).Trim();
                    if (strUESPRODUCT.Length > 500)
                    {
                        ColumnExcelName = objKeyword[9].ToString();
                        dr[0] = "(" + ColumnExcelName + ", " + i + ")";
                        dr["ERRORNOTE"] = "Dùng cho sản phẩm vượt quá 500 ký tự.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }

                    // Thêm cột PRESERVE
                    string strPRESERVE = Convert.ToString(dt.Rows[i][10]).Trim();
                    if (strPRESERVE.Length > 500)
                    {
                        ColumnExcelName = objKeyword[10].ToString();
                        dr[0] = "(" + ColumnExcelName + ", " + i + ")";
                        dr["ERRORNOTE"] = "Bảo quản vượt quá 500 ký tự.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }

                    // Thêm cột HOWUSE
                    string strHOWUSE = Convert.ToString(dt.Rows[i][11]).Trim();
                    if (strHOWUSE.Length > 2000)
                    {
                        ColumnExcelName = objKeyword[11].ToString();
                        dr[0] = "(" + ColumnExcelName + ", " + i + ")";
                        dr["ERRORNOTE"] = "HDSD vượt quá 2000 ký tự.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }

                    // Thêm cột DISTRIBUTE
                    string strDISTRIBUTE = Convert.ToString(dt.Rows[i][12]).Trim();
                    if (strDISTRIBUTE.Length > 200)
                    {
                        ColumnExcelName = objKeyword[12].ToString();
                        dr[0] = "(" + ColumnExcelName + ", " + i + ")";
                        dr["ERRORNOTE"] = "Nhập khẩu và phân phối vượt quá 200 ký tự.";
                        dtbError.Rows.Add(dr);
                        continue;
                    }
                    if (dr["ERRORNOTE"].ToString().Trim() == "Ok")
                    {
                        ERP.MasterData.PLC.MD.WSProduct.Product objProduct = new MasterData.PLC.MD.WSProduct.Product();
                        objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(strProductID);
                        DataRow drow = dtbTemp.NewRow();
                        drow["PRODUCTID"] = strProductID;
                        drow["PRODUCTNAME"] = strProductName;
                        drow["BRANDNAME"] = strBrandName;
                        drow["BRANDADDRESS"] = strBRANDADDRESS;
                        drow["SETUPCOUNTRY"] = strSETUPCOUNTRY;
                        drow["SETUPYEAR"] = strSETUPYEAR;
                        drow["QUANTIFIED"] = strQUANTIFIED;
                        drow["CAPACITY"] = strCAPACITY;
                        drow["MATERITION"] = strMATERITION;
                        drow["USES"] = strUSES;
                        drow["USEPRODUCT"] = strUESPRODUCT;
                        drow["PRESERVE"] = strPRESERVE;
                        drow["HOWUSE"] = strHOWUSE;
                        drow["DISTRIBUTE"] = strDISTRIBUTE;
                        drow["ISSELECT"] = 1;
                        dtbTemp.Rows.Add(drow);
                    }
                }
                if (dtbError.Rows.Count > 0)
                {
                    frmProduct_BarcodeError frm = new frmProduct_BarcodeError();
                    frm.Data = dtbError;
                    frm.ShowDialog();
                }

                grdProduct_Barcode.DataSource = dtbTemp;
                grdViewProduct_Barcode.RefreshData();


                if (dtbTemp.Rows.Count > 0)
                {
                    btnPrintBarcode.Enabled = false;
                    btnUpdate.Enabled = false;
                    btnGenBarcode.Enabled = !btnPrintBarcode.Enabled;
                    if (btnGenBarcode.Enabled)
                        btnGenBarcode.Enabled = bolIsPermissionUpdate;
                }
            }
            catch (Exception ex)
            {
                Library.AppCore.SystemErrorWS.Insert("Lỗi kiểm tra dữ liệu từ excel.", ex, DUIInventory_Globals.ModuleName);
                MessageBoxObject.ShowWarningMessage(this, "Lỗi kiểm tra dữ liệu từ excel.");
            }
        }

        private void btnGenBarcode_Click(object sender, EventArgs e)
        {
            DataRow[] dr = ((DataTable)grdProduct_Barcode.DataSource).Select("ISSELECT = 1");
            if (dr.Length == 0)
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn ít nhất 1 dòng!");
                return;
            }

            try
            {
                DataTable dt = (DataTable)grdProduct_Barcode.DataSource;
                string BarcodeBegin = string.Empty;
                objPLCProductGenBarcode.GenBarcode(dr[0]["PRODUCTID"].ToString(), strMD_STRINGBARCODE, ref BarcodeBegin);
                dr[0]["BARCODE"] = BarcodeBegin;
                for (int i = 1; i < dr.Length; i++)
                {
                    if (GenBarcode(dr[i - 1]["Barcode"].ToString()) == -1)
                    {
                        MessageBoxObject.ShowWarningMessage(this, "Hết số Gen Barcode");
                        return;
                    }
                    else
                        dr[i]["Barcode"] = GenBarcode(dr[i - 1]["Barcode"].ToString());
                }

                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp);
                    return;
                }
                grdProduct_Barcode.DataSource = Library.AppCore.DataTableClass.Select(dt, "ISSELECT = 1");
                //string strProductList = "<";
                //strProductList += string.Join("><", dt.Rows.OfType<DataRow>().Select(r => r["ProductID"].ToString().Trim()));
                //strProductList += ">";
                //SearchData(string.Empty, string.Empty, strProductList, 1);
                btnUpdate.Enabled = bolIsPermissionUpdate;
             //   btnUpdate.Enabled = true;
                IsEdit = false;
                btnGenBarcode.Enabled = IsEdit;
                if (btnGenBarcode.Enabled)
                    btnGenBarcode.Enabled = bolIsPermissionUpdate;
                btnPrintBarcode.Enabled = IsEdit;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi Gen Barcode");
                SystemErrorWS.Insert("Lỗi Gen Barcode", objExce.ToString(), this.Name + " -> Insert", Globals.ModuleName);
                return;
            }
        }

        private decimal GenBarcode(string Barcode)
        {
            decimal NewBarcode = 0;
            int N13 = 0;
            string MaxBarcode = Barcode.Trim().Substring(0, 12);
            //89352664
            if (Convert.ToDecimal(MaxBarcode) < Convert.ToDecimal(strMD_STRINGBARCODE + "9999"))
                NewBarcode = (Convert.ToDecimal(MaxBarcode) - Convert.ToDecimal(strMD_STRINGBARCODE + "0000") + 1) + Convert.ToDecimal(strMD_STRINGBARCODE + "0000");
            else
            {
                NewBarcode = -1;
                return -1;
            }

            for (int i = 1; i <= 12; i++)
            {
                if ((i % 2) == 0)
                    N13 += Convert.ToInt32(NewBarcode.ToString().Trim().Substring(i - 1, 1)) * 3;
                else
                    N13 += Convert.ToInt32(NewBarcode.ToString().Trim().Substring(i - 1, 1));
            }

            N13 = 10 - Convert.ToInt32(N13.ToString().Substring(N13.ToString().Length - 1, 1));
            if (N13 != 10)
                NewBarcode = Convert.ToDecimal(NewBarcode.ToString().Trim() + N13);
            else
                NewBarcode = Convert.ToDecimal(NewBarcode.ToString().Trim() + "0");
            return NewBarcode;
        }

        private List<PLC.WSProductGenBarcode.Product_GenBarcode> UpdateProduct_GenBarcode()
        {
            List<PLC.WSProductGenBarcode.Product_GenBarcode> objlstProduct_GenBarcode = new List<PLC.WSProductGenBarcode.Product_GenBarcode>();
            DataTable dt = (DataTable)grdProduct_Barcode.DataSource;

            foreach (DataRow row in dt.Rows)
            {
                if (Convert.ToDecimal(row["ISSELECT"]) == 0) continue;
                PLC.WSProductGenBarcode.Product_GenBarcode objProduct_GenBarcode = new PLC.WSProductGenBarcode.Product_GenBarcode();
                objProduct_GenBarcode.ProductID = Convert.ToString(row["ProductID"]);
                objProduct_GenBarcode.BRANDNAME = Convert.ToString(row["BrandName"]);
                objProduct_GenBarcode.BrandAddress = Convert.ToString(row["BRANDADDRESS"]);
                objProduct_GenBarcode.SetUPCountry = Convert.ToString(row["SETUPCOUNTRY"]);
                objProduct_GenBarcode.SetUPYear = Convert.ToString(row["SETUPYEAR"]);
                objProduct_GenBarcode.QUANTIFIED = Convert.ToString(row["QUANTIFIED"]);
                objProduct_GenBarcode.CAPACITY = Convert.ToString(row["CAPACITY"]);
                objProduct_GenBarcode.MATERITION = Convert.ToString(row["MATERITION"]);
                objProduct_GenBarcode.USES = Convert.ToString(row["USES"]);
                objProduct_GenBarcode.USEProduct = Convert.ToString(row["USEPRODUCT"]);
                objProduct_GenBarcode.PRESERVE = Convert.ToString(row["PRESERVE"]);
                objProduct_GenBarcode.HOWUSE = Convert.ToString(row["HOWUSE"]);
                objProduct_GenBarcode.DISTRIBUTE = Convert.ToString(row["DISTRIBUTE"]);
                if (Convert.ToString(row["BARCODE"]) == string.Empty)
                    objProduct_GenBarcode.BARCode = 0;
                else
                    objProduct_GenBarcode.BARCode = Convert.ToDecimal(row["BARCODE"]);
                objlstProduct_GenBarcode.Add(objProduct_GenBarcode);
            }
            return objlstProduct_GenBarcode;
        }

        private void cmdCheckAll_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)grdProduct_Barcode.DataSource;
            if (dt != null || dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                    row["ISSELECT"] = true;
            }
        }

        private void cmdUnCheckAll_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)grdProduct_Barcode.DataSource;
            if (dt != null || dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                    row["ISSELECT"] = false;
            }
        }

        private void mnuReview_Opening(object sender, CancelEventArgs e)
        {
            DataRow dr = grdViewProduct_Barcode.GetFocusedDataRow();
            cmdUnCheckAll.Enabled = cmdCheckAll.Enabled = dr == null ? false : true;
        }

        private void btnPrintBarcode_Click(object sender, EventArgs e)
        {

            if (grdProduct_Barcode.DataSource == null)
                return;
            try
            {
                DataSet dsReport = FormatBarcode();
                if (dsReport.Tables[0].Rows.Count < 1 && dsReport.Tables[1].Rows.Count < 1)
                {
                    MessageBox.Show(this, "Không tìm thấy sản phẩm cần in", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                //int intReportID = 156;//Library.AppCore.AppConfig.GetIntConfigValue("PRINTPRODUCTQRCODE_REPORTID");
                ERP.MasterData.PLC.MD.WSReport.Report objReport = MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID);
                objReport.PrinterTypeID = "BarcodePrinter";
                objReport.DataSetSource = dsReport;
                ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport);
            }
            catch (Exception objExc)
            {
                SystemErrorWS.Insert("Lỗi in sản phẩm", objExc, DUIInventory_Globals.ModuleName);
                MessageBox.Show("Lỗi in sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private DataSet FormatBarcode()
        {
            DataTable dtbMaster = new DataTable("RPT_GENBARCODE_MASTER");
            dtbMaster.Columns.Add("MASTERID", typeof(int));
            DataRow rowMS = dtbMaster.NewRow();
            rowMS[0] = 1;
            dtbMaster.Rows.Add(rowMS);

            DataTable dtbFormat = new DataTable("RPT_PRODUCT_GENBARCODE");
            dtbFormat.Columns.Add("MASTERID", typeof(int));
            dtbFormat.Columns.Add("PRODUCTID", typeof(string));
            dtbFormat.Columns.Add("PRODUCTNAME", typeof(string));
            dtbFormat.Columns.Add("BRANDNAME", typeof(string));
            dtbFormat.Columns.Add("BRANDADDRESS", typeof(string));
            dtbFormat.Columns.Add("SETUPCOUNTRY", typeof(string));
            dtbFormat.Columns.Add("BARCODE", typeof(string));
            dtbFormat.Columns.Add("SETUPYEAR", typeof(string));
            dtbFormat.Columns.Add("QUANTIFIED", typeof(string));
            dtbFormat.Columns.Add("CAPACITY", typeof(string));
            dtbFormat.Columns.Add("MATERITION", typeof(string));
            dtbFormat.Columns.Add("USES", typeof(string));
            dtbFormat.Columns.Add("USEPRODUCT", typeof(string));
            dtbFormat.Columns.Add("PRESERVE", typeof(string));
            dtbFormat.Columns.Add("HOWUSE", typeof(string));
            dtbFormat.Columns.Add("DISTRIBUTE", typeof(string));

            DataTable dtbQRCODE = new DataTable("TEMP_GENBARCODE");
            dtbQRCODE.Columns.Add("BARCODE", typeof(string));
            dtbQRCODE.Columns.Add("QRCODE", System.Type.GetType("System.Byte[]"));
            dtbQRCODE.Columns.Add("DRAWBARCODE", System.Type.GetType("System.Byte[]"));
            //dtbFormat.Columns.Add("BARCODE", System.Type.GetType("System.Byte[]"));

            DataSet ds = new DataSet();
            DataTable dt = (DataTable)grdProduct_Barcode.DataSource;
            int i = 0;
            foreach (DataRow row in dt.Rows)
            {

                if (Convert.ToDecimal(row["ISSELECT"]) == 0) continue;
                if (i == 4)
                    i = 1;
                else
                    i++;
                //DataTable dtbNew = new DataTable();
                //dtbNew = dtbFormat.Clone();
                DataRow dr = dtbFormat.NewRow();
                dr["MASTERID"] = i == 4 ? 1 : 0;
                dr["PRODUCTID"] = Convert.ToString(row["PRODUCTID"]);
                dr["PRODUCTNAME"] = Convert.ToString(row["PRODUCTNAME"]);
                dr["BRANDNAME"] = Convert.ToString(row["BRANDNAME"]);
                dr["BRANDADDRESS"] = Convert.ToString(row["BRANDADDRESS"]);
                dr["SETUPCOUNTRY"] = Convert.ToString(row["SETUPCOUNTRY"]);
                dr["SETUPYEAR"] = Convert.ToString(row["SETUPYEAR"]);
                dr["QUANTIFIED"] = Convert.ToString(row["QUANTIFIED"]);
                dr["CAPACITY"] = Convert.ToString(row["CAPACITY"]);
                dr["BARCODE"] = Convert.ToString(row["BARCODE"]);
                dr["MATERITION"] = Convert.ToString(row["MATERITION"]);
                dr["USES"] = Convert.ToString(row["USES"]);
                dr["USEPRODUCT"] = Convert.ToString(row["USEPRODUCT"]);
                dr["PRESERVE"] = Convert.ToString(row["PRESERVE"]);
                dr["HOWUSE"] = Convert.ToString(row["HOWUSE"]);
                dr["DISTRIBUTE"] = Convert.ToString(row["DISTRIBUTE"]);

                //dtbNew.Rows.Add(dr);
                //dtbNew.TableName = "RPT_PRODUCT_GENBARCODE" + i++;
                //ds.Tables.Add(dtbNew);
                dtbFormat.Rows.Add(dr);
                DataRow drQRCODE = dtbQRCODE.NewRow();
                drQRCODE["BARCODE"] = Convert.ToString(row["BARCODE"]);
                drQRCODE["QRCODE"] = GetQRCodeImage(row["BARCODE"].ToString().Trim());
                drQRCODE["DRAWBARCODE"] = DrawBarcode(row["BARCODE"].ToString().Trim());
                dtbQRCODE.Rows.Add(drQRCODE);

            }

            ds.Tables.Add(dtbFormat);
            ds.Tables.Add(dtbQRCODE);
            ds.Tables.Add(dtbMaster);
            //ds.Relations.Add("MASTERID_PK",
            //                    ds.Tables["RPT_GENBARCODE_MASTER"].Columns["MASTERID"],
            //                    ds.Tables["RPT_PRODUCT_GENBARCODE"].Columns["MASTERID"]);
            return ds;
        }

        private byte[] GetQRCodeImage(string strBarcode)
        {
            byte[] arrByte = null;
            string strContent = strBarcode.Trim();
            Image img = Library.Barcode.QRCode.Encode(strContent, 2, Encoding.UTF8);
            if (img != null)
                arrByte = GetImageBytes(img);
            return arrByte;
        }

        public byte[] GetImageBytes(System.Drawing.Image image)
        {
            byte[] byteArray = new byte[1024];
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                stream.Close();
                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        private void grdViewProduct_Barcode_ShowingEditor(object sender, CancelEventArgs e)
        {
            DataRow dr = grdViewProduct_Barcode.GetFocusedDataRow();
            //if ( IsEdit == false && !btnGenBarcode.Enabled)
            //    e.Cancel = true;
        }

        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            DataRow dr = grdViewProduct_Barcode.GetFocusedDataRow();
            cmdUnCheckAll.Enabled = cmdCheckAll.Enabled = dr == null ? false : true;
        }

        private void grdViewProduct_Barcode_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = sender as GridView;
            // if (view.FocusedColumn.FieldName == "STT")
            //{
            if (e.Value == null) return;
            string error = string.Empty;
            // Thêm cột strBRANDNAME
            if (view.FocusedColumn.FieldName == "BRANDNAME")
                if (e.Value.ToString().Length > 500)
                    error = "Nhà sản xuất vượt quá 500 ký tự.";


            // Thêm cột BRANDADDRESS
            if (view.FocusedColumn.FieldName == "BRANDADDRESS")
                if (e.Value.ToString().Length > 500)
                    error = "Địa chỉ vượt quá 500 ký tự.";


            // Thêm cột SETUPCOUNTRY
            if (view.FocusedColumn.FieldName == "SETUPCOUNTRY")
                if (e.Value.ToString().Length > 500)
                    error = "Xuất xứ vượt quá 500 ký tự.";

            // Thêm cột SETUPYEAR
            if (view.FocusedColumn.FieldName == "SETUPYEAR")
                if (e.Value.ToString().Length > 100)
                    error = "Năm sản xuất vượt quá 100 ký tự.";

            // Thêm cột QUANTIFIED
            if (view.FocusedColumn.FieldName == "QUANTIFIED")
                if (e.Value.ToString().Length > 100)
                    error = "Định lượng vượt quá 100 ký tự.";


            // Thêm cột CAPACITY
            if (view.FocusedColumn.FieldName == "CAPACITY")
                if (e.Value.ToString().Length > 100)
                    error = "Dung lượng vượt quá 100 ký tự.";

            // Thêm cột MATERITION
            if (view.FocusedColumn.FieldName == "MATERITION")
                if (e.Value.ToString().Length > 500)
                    error = "Chất liệu vượt quá 500 ký tự.";

            // Thêm cột USES
            if (view.FocusedColumn.FieldName == "USES")
                if (e.Value.ToString().Length > 500)
                    error = "Công dụng vượt quá 500 ký tự.";

            // Thêm cột UESPRODUCT
            if (view.FocusedColumn.FieldName == "USEPRODUCT")
                if (e.Value.ToString().Length > 500)
                    error = "Dùng cho sản phẩm vượt quá 500 ký tự.";

            // Thêm cột PRESERVE
            if (view.FocusedColumn.FieldName == "PRESERVE")
                if (e.Value.ToString().Length > 500)
                    error = "Bảo quản vượt quá 500 ký tự.";

            // Thêm cột HOWUSE
            if (view.FocusedColumn.FieldName == "HOWUSE")
                if (e.Value.ToString().Length > 2000)
                    error = "HDSD vượt quá 2000 ký tự.";

            // Thêm cột DISTRIBUTE
            if (view.FocusedColumn.FieldName == "DISTRIBUTE")
                if (e.Value.ToString().Length > 200)
                    error = "Nhập khẩu và phân phối vượt quá 200 ký tự.";

            if (error != "")
            {

                e.Valid = false;
                e.ErrorText = error;
            }
        }



        private byte[] DrawBarcode(string strBarcode)
        {
            byte[] arrByte = null;
            strBarcode = strBarcode.Trim();
            System.Drawing.Image img = null;
            BarcodeLib.Barcode barC = new BarcodeLib.Barcode();
            barC.Alignment = BarcodeLib.AlignmentPositions.CENTER;
            BarcodeLib.TYPE type = BarcodeLib.TYPE.UNSPECIFIED;
            type = BarcodeLib.TYPE.CODE128B;
            string strPre = String.Empty;
            for (int t = 0; t <= 7 - strBarcode.Length; t++)
            {
                strPre = "0" + strPre;
            }
            strBarcode = strPre + strBarcode;
            if (type != BarcodeLib.TYPE.UNSPECIFIED)
            {
                barC.LabelPosition = BarcodeLib.LabelPositions.TOPLEFT;
                img = barC.Encode(type, strBarcode, System.Drawing.Color.Black, System.Drawing.Color.White, 400, 30);
            }
            if (img != null)
            {
                int intWidth = img.Width;
                int intHeigth = img.Height;
                if (img != null)
                    using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
                    {
                        img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                        stream.Close();
                        arrByte = stream.ToArray();
                    }
            }
            return arrByte;
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (!btnExportExcel.Enabled)
                return;
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdProduct_Barcode, DevExpress.XtraPrinting.TextExportMode.Text, true, string.Empty, false);
        }
    }
}
