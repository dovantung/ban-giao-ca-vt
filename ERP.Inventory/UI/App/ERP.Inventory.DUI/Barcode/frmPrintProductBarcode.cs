﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using Library.AppCore;
using ERP.Inventory.PLC.Barcode;
using ERP.MasterData.DUI.CustomControl.ImportExcel;

namespace ERP.Inventory.DUI.Barcode
{
    public partial class frmPrintProductBarcode : DevExpress.XtraEditors.XtraForm, IExcelImport
    {
        #region Variable
        private string strInputVoucherID = string.Empty;
        private String strPermission_PrintProduct = "Inventory_PrintProductBarcode";
        private bool IsIMEI = true;
        #endregion

        #region Hàm khởi tạo
        public frmPrintProductBarcode()
        {
            InitializeComponent();
            objClsImportExcel = new ClsImportExcel(this);
        }
        public frmPrintProductBarcode(string strInputVoucherID)
        {
            InitializeComponent();
            objClsImportExcel = new ClsImportExcel(this);
            this.strInputVoucherID = strInputVoucherID;
            cboIsNew.SelectedIndex = 0;

        }
        #endregion

        #region Function

        /// <summary>
        /// Nạp dữ liệu cho combobox
        /// </summary>
        private void LoadComboBox()
        {
            try
            {
                System.Windows.Forms.ComboBox cboStore_Temp = new System.Windows.Forms.ComboBox();
                Library.AppCore.LoadControls.SetDataSource.SetStore(this, cboStore_Temp, false, 0, 0, 0, Library.AppCore.Constant.EnumType.StorePermissionType.ALL,
                    Library.AppCore.Constant.EnumType.AreaType.AREA, "ISACTIVE = 1");
                DataTable dtbStore = cboStore_Temp.DataSource as DataTable;
                dtbStore.Rows.RemoveAt(0);
                cboStoreSearch.Properties.DataSource = dtbStore;
                cboStoreSearch.Properties.NullValuePromptShowForEmptyValue = true;
                cboStoreSearch.Properties.NullText = "-Chọn kho nhập-";
                Library.AppCore.LoadControls.SetDataSource.SetMainGroup(this, cboMainGroupID);
                Library.AppCore.LoadControls.SetDataSource.SetSubGroup(this, cboSubGroupID, 0);
                Library.AppCore.LoadControls.SetDataSource.SetBrand(this, cboBrandIDList, 0);
                cboProductList1.InitControl();


            }
            catch (Exception objExc)
            {
                SystemErrorWS.Insert("Lỗi nạp dữ liệu combobox", objExc.ToString(), DUIInventory_Globals.ModuleName);
                MessageBox.Show("Lỗi nạp dữ liệu combobox", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
        int CountPrint = 2;
        private void GetPrintBacodeType()
        {
           
            if (this.Tag != null)
            {
                Hashtable hstbParam = Library.AppCore.Other.ConvertObject.ConvertTagFormToHashtable(this.Tag.ToString().Trim());
                try
                {
                    IsIMEI = Convert.ToBoolean(hstbParam["IMEI"]);
                }
                catch (Exception objExce)
                {
                    SystemErrorWS.Insert("Lỗi khi lấy tham số form", objExce.ToString(), DUIInventory_Globals.ModuleName);
                    MessageBox.Show(this, "Lỗi khi lấy tham số form", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
                IsIMEI = false;
            if (IsIMEI)
            {
                this.Text = "In barcode IMEI";
                btnPrintProduct.Text = "In IMEI";
                cboSearchBy.Items.Add("Phiếu nhập");
                cboSearchBy.Items.Add("IMEI");
                cboSearchBy.SelectedIndex = 0;
                mnuIMEItoExcel.Visible = true;
                mnuProductAdd.Visible = false;
                lblSearchBy.Visible = true;
                chkIsFromInputVoucher.Visible = false;
                cboSearchBy.Visible = true;
            }
            chkIsInstock.Visible = !IsIMEI;
        }

        /// <summary>
        /// Nạp danh sách sản phẩm
        /// </summary>
        private void LoadProduct()
        {
            if (!chkIsFromInputVoucher.Checked)
            {
                if (cboStoreSearch.EditValue == null)
                {
                    MessageBox.Show(this, "Vui lòng chọn chi nhánh", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboStoreSearch.Focus();
                    return;
                }
                if (cboMainGroupID.SelectedIndex < 1)
                {
                    MessageBox.Show(this, "Vui lòng chọn ngành hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cboMainGroupID.Focus();
                    return;
                }
            }
            else
                if (txtInputVoucherID.Text.Trim() == string.Empty)
                {
                    MessageBox.Show(this, "Vui lòng nhập mã phiếu nhập", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtInputVoucherID.Focus();
                    return;
                }
            DataTable dtbBarcode = IsIMEI == false ? GetProduct() : GetIMEI();
            txtTotalPrintQuantity.Text = "0";
            if (dtbBarcode == null || dtbBarcode.Rows.Count <= 0)
            {
                txtTotalQuantity.Text = "0";
            }
            else
                txtTotalQuantity.Text = Convert.ToString(Convert.ToDecimal(Globals.IsNull(dtbBarcode.Compute("Sum(Quantity)", string.Empty), 0)));
            flexData.DataSource = dtbBarcode;
            FormatFlex();
        }

        /// <summary>
        /// Định dạng lưới
        /// </summary>
        private void FormatFlex()
        {
            for (int i = 0; i < flexData.Cols.Count; i++)
            {
                flexData.Cols[i].Visible = false;
                flexData.Cols[i].AllowEditing = false;
            }
            flexData.Cols[0].Visible = false;
            flexData.Cols["IsSelect"].Caption = "Chọn";
            flexData.Cols["IsSelect"].Visible = true;
            flexData.Cols["IsSelect"].AllowEditing = true;
            flexData.Cols["ProductID"].Caption = "Mã sản phẩm";
            flexData.Cols["ProductID"].Visible = true;
            flexData.Cols["ProductName"].Caption = "Tên Sản phẩm";
            flexData.Cols["ProductName"].Visible = true;
            flexData.Cols["Quantity"].Visible = true;
            flexData.Cols["PrintQuantity"].Visible = true;
            if (IsIMEI)
            {
                flexData.Cols["IMEI"].Caption = "IMEI";
                flexData.Cols["IMEI"].Visible = true;
                flexData.Cols["IMEI"].Width = 150;
                flexData.Cols["Quantity"].Visible = false;
                flexData.Cols["PrintQuantity"].Visible = false;
                flexData.Cols["IsNew"].Caption = "Mới";
                flexData.Cols["IsNew"].Visible = true;
                flexData.Cols["IsNew"].AllowEditing = true;
                flexData.Cols["IsNew"].DataType = typeof(bool);
                flexData.Cols["IsNew"].Width = 50;
            }
            flexData.Cols["Quantity"].Caption = "Số lượng tồn";
            flexData.Cols["PrintQuantity"].Caption = "Số lượng in";
            flexData.Cols["IsSelect"].Width = 50;
            flexData.Cols["ProductID"].Width = 120;
            flexData.Cols["ProductName"].Width = 240;
            flexData.Cols["Quantity"].Width = 100;
            flexData.Cols["PrintQuantity"].Width = 100;
            flexData.Cols["Quantity"].Format = "#,##0.####";
            flexData.Cols["PrintQuantity"].Format = "##0";

            for (int i = 0; i < flexData.Cols.Count; i++)
            {
                flexData.Cols[i].AllowEditing = false;
                flexData.Cols[i].AllowDragging = false;
            }
            flexData.Cols["IsSelect"].DataType = typeof(bool);
            flexData.Cols["IsSelect"].AllowEditing = true;
            flexData.Cols["PrintQuantity"].AllowEditing = true;

            //TGDD.ERP.Common.UI.WinApp.LoadControls.C1FlexGridObject.SetBackColor(flexData, SystemColors.Info);
            //TGDD.ERP.Common.UI.WinApp.LoadControls.C1FlexGridObject.SetBackColor(flexData, SystemColors.Info);
            C1.Win.C1FlexGrid.CellStyle style = flexData.Styles.Add("flexStyle");
            style.WordWrap = true;
            style.Font = new System.Drawing.Font("Microsoft Sans Serif", 10, FontStyle.Bold);
            style.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            C1.Win.C1FlexGrid.CellRange range = flexData.GetCellRange(0, 0, 0, flexData.Cols.Count - 1);
            range.Style = style;
            flexData.Rows[0].Height = 35;

            if (flexData.Rows.Count > flexData.Rows.Fixed)
            {
                btnSetPrintQuantity.Enabled = true;
                btnPrintProduct.Enabled = true;
                btnPrintQRCode.Enabled = true;
                cboQRSize.Enabled = true;
                mnuProductAdd.Enabled = true;
            }
            else
            {
                btnSetPrintQuantity.Enabled = false;
                btnPrintProduct.Enabled = false;
                btnPrintQRCode.Enabled = false;
                cboQRSize.Enabled = false;
                mnuProductAdd.Enabled = false;
            }
        }


        /// <summary>
        /// Định dạng bảng 3 cột trước khi in
        /// </summary>
        /// <param name="dtbData"></param>
        /// <returns></returns>
        private DataTable FormatTableQR_2x1()
        {
            DataTable dtbFormat = new DataTable();
            dtbFormat.Columns.Add("ProductName1");
            dtbFormat.Columns.Add("QRCode1", System.Type.GetType("System.Byte[]"));
            dtbFormat.Columns.Add("PRODUCTID1");
            dtbFormat.Columns.Add("ProductName2");
            dtbFormat.Columns.Add("QRCode2", System.Type.GetType("System.Byte[]"));
            dtbFormat.Columns.Add("PRODUCTID2");
            dtbFormat.Columns.Add("ProductName3");
            dtbFormat.Columns.Add("QRCode3", System.Type.GetType("System.Byte[]"));
            dtbFormat.Columns.Add("PRODUCTID3");

            dtbFormat.Columns.Add("ProductName4");
            dtbFormat.Columns.Add("QRCode4", System.Type.GetType("System.Byte[]"));
            dtbFormat.Columns.Add("PRODUCTID4");

            dtbFormat.Columns.Add("ProductName5");
            dtbFormat.Columns.Add("QRCode5", System.Type.GetType("System.Byte[]"));
            dtbFormat.Columns.Add("PRODUCTID5");

            decimal decNumRow = 1;
            DataRow rowFormat = null;
            for (int i = flexData.Rows.Fixed; i < flexData.Rows.Count; i++)
            {
                if (flexData[i, "IsSelect"] != DBNull.Value && Convert.ToBoolean(flexData[i, "IsSelect"]))
                {
                    int intPrintQuantity = Convert.ToInt32(flexData[i, "PrintQuantity"]);
                    if (intPrintQuantity < 1)
                        continue;
                    for (int iQuantity = 1; iQuantity <= intPrintQuantity; iQuantity++)
                    {
                        if (rowFormat == null)
                            rowFormat = dtbFormat.NewRow();
                        int intResult = (int)(decNumRow % 2);
                        if (intResult == 0)
                        {
                            rowFormat["ProductName2"] = flexData[i, "ProductName"];
                            rowFormat["PRODUCTID2"] = IsIMEI == false ? flexData[i, "PRODUCTID"] : flexData[i, "IMEI"];
                            string strMainGroupName = string.Empty;
                            string strModelName = string.Empty;
                            if (!Convert.IsDBNull(flexData[i, "ModelName"]) && flexData[i, "ModelName"].ToString() != string.Empty)
                                strModelName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "ModelName"].ToString().Trim());
                            if (!Convert.IsDBNull(flexData[i, "MainGroupName"]) && flexData[i, "MainGroupName"].ToString() != string.Empty)
                                strMainGroupName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "MainGroupName"].ToString().Trim());
                            rowFormat["QRCode2"] = GetQRCodeImage(rowFormat["PRODUCTID2"].ToString().Trim(), rowFormat["ProductName2"].ToString(), strMainGroupName, strModelName);
                            dtbFormat.Rows.Add(rowFormat);
                            rowFormat = null;
                        }
                        //else if (intResult == 2)
                        //{
                        //    rowFormat["ProductName2"] = flexData[i, "ProductName"];
                        //    rowFormat["PRODUCTID2"] = IsIMEI == false ? flexData[i, "PRODUCTID"] : flexData[i, "IMEI"];
                        //    string strMainGroupName = string.Empty;
                        //    string strModelName = string.Empty;
                        //    if (!Convert.IsDBNull(flexData[i, "ModelName"]) && flexData[i, "ModelName"].ToString() != string.Empty)
                        //        strModelName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "ModelName"].ToString().Trim());
                        //    if (!Convert.IsDBNull(flexData[i, "MainGroupName"]) && flexData[i, "MainGroupName"].ToString() != string.Empty)
                        //        strMainGroupName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "MainGroupName"].ToString().Trim());
                        //    rowFormat["QRCode2"] = GetQRCodeImage(rowFormat["PRODUCTID2"].ToString().Trim(), rowFormat["ProductName2"].ToString(), strMainGroupName, strModelName);
                        //}
                        //else
                        //    if (intResult == 3)
                        //    {
                        //        rowFormat["ProductName3"] = flexData[i, "ProductName"];
                        //        rowFormat["PRODUCTID3"] = IsIMEI == false ? flexData[i, "PRODUCTID"] : flexData[i, "IMEI"];
                        //        string strMainGroupName = string.Empty;
                        //        string strModelName = string.Empty;
                        //        if (!Convert.IsDBNull(flexData[i, "ModelName"]) && flexData[i, "ModelName"].ToString() != string.Empty)
                        //            strModelName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "ModelName"].ToString().Trim());
                        //        if (!Convert.IsDBNull(flexData[i, "MainGroupName"]) && flexData[i, "MainGroupName"].ToString() != string.Empty)
                        //            strMainGroupName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "MainGroupName"].ToString().Trim());
                        //        rowFormat["QRCode3"] = GetQRCodeImage(rowFormat["PRODUCTID3"].ToString().Trim(), rowFormat["ProductName3"].ToString(), strMainGroupName, strModelName);
                        //    }
                        //    else
                        //        if (intResult == 4)
                        //        {
                        //            rowFormat["ProductName4"] = flexData[i, "ProductName"];
                        //            rowFormat["PRODUCTID4"] = IsIMEI == false ? flexData[i, "PRODUCTID"] : flexData[i, "IMEI"];
                        //            string strMainGroupName = string.Empty;
                        //            string strModelName = string.Empty;
                        //            if (!Convert.IsDBNull(flexData[i, "ModelName"]) && flexData[i, "ModelName"].ToString() != string.Empty)
                        //                strModelName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "ModelName"].ToString().Trim());
                        //            if (!Convert.IsDBNull(flexData[i, "MainGroupName"]) && flexData[i, "MainGroupName"].ToString() != string.Empty)
                        //                strMainGroupName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "MainGroupName"].ToString().Trim());
                        //            rowFormat["QRCode4"] = GetQRCodeImage(rowFormat["PRODUCTID4"].ToString().Trim(), rowFormat["ProductName4"].ToString(), strMainGroupName, strModelName);
                        //        }
                        else
                        {
                            rowFormat["ProductName1"] = flexData[i, "ProductName"];
                            rowFormat["PRODUCTID1"] = IsIMEI == false ? flexData[i, "PRODUCTID"] : flexData[i, "IMEI"];
                            string strMainGroupName = string.Empty;
                            string strModelName = string.Empty;
                            if (!Convert.IsDBNull(flexData[i, "ModelName"]) && flexData[i, "ModelName"].ToString() != string.Empty)
                                strModelName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "ModelName"].ToString().Trim());
                            if (!Convert.IsDBNull(flexData[i, "MainGroupName"]) && flexData[i, "MainGroupName"].ToString() != string.Empty)
                                strMainGroupName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "MainGroupName"].ToString().Trim());
                            rowFormat["QRCode1"] = GetQRCodeImage(rowFormat["PRODUCTID1"].ToString().Trim(), rowFormat["ProductName1"].ToString(), strMainGroupName, strModelName);
                        }
                        decNumRow++;
                    }
                }
            }
            if (rowFormat != null)
            {
                dtbFormat.Rows.Add(rowFormat);
            }
            return dtbFormat;
        }

        /// <summary>
        /// Định dạng bảng 3 cột trước khi in
        /// </summary>
        /// <param name="dtbData"></param>
        /// <returns></returns>
        private DataTable FormatTableQR31()
        {
            DataTable dtbFormat = new DataTable();
            dtbFormat.Columns.Add("ProductName1");
            dtbFormat.Columns.Add("QRCode1", System.Type.GetType("System.Byte[]"));
            dtbFormat.Columns.Add("PRODUCTID1");
            dtbFormat.Columns.Add("ProductName2");
            dtbFormat.Columns.Add("QRCode2", System.Type.GetType("System.Byte[]"));
            dtbFormat.Columns.Add("PRODUCTID2");
            dtbFormat.Columns.Add("ProductName3");
            dtbFormat.Columns.Add("QRCode3", System.Type.GetType("System.Byte[]"));
            dtbFormat.Columns.Add("PRODUCTID3");

            dtbFormat.Columns.Add("ProductName4");
            dtbFormat.Columns.Add("QRCode4", System.Type.GetType("System.Byte[]"));
            dtbFormat.Columns.Add("PRODUCTID4");

            dtbFormat.Columns.Add("ProductName5");
            dtbFormat.Columns.Add("QRCode5", System.Type.GetType("System.Byte[]"));
            dtbFormat.Columns.Add("PRODUCTID5");

            decimal decNumRow = 1;
            DataRow rowFormat = null;
            for (int i = flexData.Rows.Fixed; i < flexData.Rows.Count; i++)
            {
                if (flexData[i, "IsSelect"] != DBNull.Value && Convert.ToBoolean(flexData[i, "IsSelect"]))
                {
                    int intPrintQuantity = Convert.ToInt32(flexData[i, "PrintQuantity"]);
                    if (intPrintQuantity < 1)
                        continue;
                    for (int iQuantity = 1; iQuantity <= intPrintQuantity; iQuantity++)
                    {
                        if (rowFormat == null)
                            rowFormat = dtbFormat.NewRow();
                        int intResult = (int)(decNumRow % 3);
                        if (intResult == 0)
                        {
                            rowFormat["ProductName3"] = flexData[i, "ProductName"];
                            rowFormat["PRODUCTID3"] = IsIMEI == false ? flexData[i, "PRODUCTID"] : flexData[i, "IMEI"];
                            string strMainGroupName = string.Empty;
                            string strModelName = string.Empty;
                            if (!Convert.IsDBNull(flexData[i, "ModelName"]) && flexData[i, "ModelName"].ToString() != string.Empty)
                                strModelName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "ModelName"].ToString().Trim());
                            if (!Convert.IsDBNull(flexData[i, "MainGroupName"]) && flexData[i, "MainGroupName"].ToString() != string.Empty)
                                strMainGroupName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "MainGroupName"].ToString().Trim());
                            rowFormat["QRCode3"] = GetQRCodeImage_31(rowFormat["PRODUCTID3"].ToString().Trim(), rowFormat["ProductName3"].ToString(), strMainGroupName, strModelName);
                            dtbFormat.Rows.Add(rowFormat);
                            rowFormat = null;
                        }
                        else if (intResult == 2)
                        {
                            rowFormat["ProductName2"] = flexData[i, "ProductName"];
                            rowFormat["PRODUCTID2"] = IsIMEI == false ? flexData[i, "PRODUCTID"] : flexData[i, "IMEI"];
                            string strMainGroupName = string.Empty;
                            string strModelName = string.Empty;
                            if (!Convert.IsDBNull(flexData[i, "ModelName"]) && flexData[i, "ModelName"].ToString() != string.Empty)
                                strModelName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "ModelName"].ToString().Trim());
                            if (!Convert.IsDBNull(flexData[i, "MainGroupName"]) && flexData[i, "MainGroupName"].ToString() != string.Empty)
                                strMainGroupName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "MainGroupName"].ToString().Trim());
                            rowFormat["QRCode2"] = GetQRCodeImage_31(rowFormat["PRODUCTID2"].ToString().Trim(), rowFormat["ProductName2"].ToString(), strMainGroupName, strModelName);
                        }
                        //else
                        //    if (intResult == 3)
                        //    {
                        //        rowFormat["ProductName3"] = flexData[i, "ProductName"];
                        //        rowFormat["PRODUCTID3"] = IsIMEI == false ? flexData[i, "PRODUCTID"] : flexData[i, "IMEI"];
                        //        string strMainGroupName = string.Empty;
                        //        string strModelName = string.Empty;
                        //        if (!Convert.IsDBNull(flexData[i, "ModelName"]) && flexData[i, "ModelName"].ToString() != string.Empty)
                        //            strModelName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "ModelName"].ToString().Trim());
                        //        if (!Convert.IsDBNull(flexData[i, "MainGroupName"]) && flexData[i, "MainGroupName"].ToString() != string.Empty)
                        //            strMainGroupName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "MainGroupName"].ToString().Trim());
                        //        rowFormat["QRCode3"] = GetQRCodeImage_31(rowFormat["PRODUCTID3"].ToString().Trim(), rowFormat["ProductName3"].ToString(), strMainGroupName, strModelName);
                        //    }
                        //    else
                        //        if (intResult == 4)
                        //        {
                        //            rowFormat["ProductName4"] = flexData[i, "ProductName"];
                        //            rowFormat["PRODUCTID4"] = IsIMEI == false ? flexData[i, "PRODUCTID"] : flexData[i, "IMEI"];
                        //            string strMainGroupName = string.Empty;
                        //            string strModelName = string.Empty;
                        //            if (!Convert.IsDBNull(flexData[i, "ModelName"]) && flexData[i, "ModelName"].ToString() != string.Empty)
                        //                strModelName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "ModelName"].ToString().Trim());
                        //            if (!Convert.IsDBNull(flexData[i, "MainGroupName"]) && flexData[i, "MainGroupName"].ToString() != string.Empty)
                        //                strMainGroupName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "MainGroupName"].ToString().Trim());
                        //            rowFormat["QRCode4"] = GetQRCodeImage_31(rowFormat["PRODUCTID4"].ToString().Trim(), rowFormat["ProductName4"].ToString(), strMainGroupName, strModelName);
                        //        }
                        else
                        {
                            rowFormat["ProductName1"] = flexData[i, "ProductName"];
                            rowFormat["PRODUCTID1"] = IsIMEI == false ? flexData[i, "PRODUCTID"] : flexData[i, "IMEI"];
                            string strMainGroupName = string.Empty;
                            string strModelName = string.Empty;
                            if (!Convert.IsDBNull(flexData[i, "ModelName"]) && flexData[i, "ModelName"].ToString() != string.Empty)
                                strModelName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "ModelName"].ToString().Trim());
                            if (!Convert.IsDBNull(flexData[i, "MainGroupName"]) && flexData[i, "MainGroupName"].ToString() != string.Empty)
                                strMainGroupName = Library.AppCore.ConvertData.ConvertToUnSignString(flexData[i, "MainGroupName"].ToString().Trim());
                            rowFormat["QRCode1"] = GetQRCodeImage_31(rowFormat["PRODUCTID1"].ToString().Trim(), rowFormat["ProductName1"].ToString(), strMainGroupName, strModelName);
                        }
                        decNumRow++;
                    }
                }
            }
            if (rowFormat != null)
            {
                dtbFormat.Rows.Add(rowFormat);
            }
            return dtbFormat;
        }

        private byte[] GetQRCodeImage(string strProductID, string strProductName, string strMainGroupName, string strModelName)
        {
            byte[] arrByte = null;
            string strContent = strProductID;//+ " " + strProductName;
            //string _MainGroup = Library.AppCore.ConvertData.ConvertToUnSignString(strMainGroupName.Trim()).Replace("  ", " ").Replace(" ", "-").Replace("(","").Replace(")","");
            //string _ModelName = Library.AppCore.ConvertData.ConvertToUnSignString(strModelName.Trim()).Replace("  ", " ").Replace(" ", "-").Replace("(", "-").Replace(")", "");
            //strContent += @" http://nhatcuong.com/" + _MainGroup + "/" + _ModelName;
            //strContent = strContent.Trim();
            Image img = Library.Barcode.QRCode.Encode(strContent, 2, Encoding.UTF8);
            if (img != null)
                arrByte = GetImageBytes(img);
            return arrByte;
        }

        private byte[] GetQRCodeImage_31(string strProductID, string strProductName, string strMainGroupName, string strModelName)
        {
            byte[] arrByte = null;
            string strContent = strProductID.Trim();//+ " " + strProductName;
            //string _MainGroup = Library.AppCore.ConvertData.ConvertToUnSignString(strMainGroupName.Trim()).Replace("  ", " ").Replace(" ", "-");
            //string _ModelName = Library.AppCore.ConvertData.ConvertToUnSignString(strModelName.Trim()).Replace("  ", " ").Replace(" ", "-");
            //strContent += @" http://nhatcuong.com/" + _MainGroup + "/" + _ModelName;
            Image img = Library.Barcode.QRCode.Encode(strContent, 2, Encoding.UTF8);
            if (img != null)
                arrByte = GetImageBytes(img);
            return arrByte;
        }

        public byte[] GetImageBytes(System.Drawing.Image image)
        {
            byte[] byteArray = new byte[1024];
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                stream.Close();
                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        /// <summary>
        /// Định dạng bảng 2 cột trước khi in
        /// </summary>
        /// <param name="dtbData"></param>
        /// <returns></returns>
        private DataTable FormatTableIMEI()
        {
            int CountPrint = cboQRSize.SelectedIndex == 0 ? 3 : 2;
            DataTable dtbFormat = new DataTable();
            dtbFormat.Columns.Add("ProductName1");
            dtbFormat.Columns.Add("PRODUCTID1");
            dtbFormat.Columns.Add("ProductName2");
            dtbFormat.Columns.Add("PRODUCTID2");
            dtbFormat.Columns.Add("ProductName3");
            dtbFormat.Columns.Add("PRODUCTID3");
            decimal decNumRow = 1;
            DataRow rowFormat = null;
            for (int i = flexData.Rows.Fixed; i < flexData.Rows.Count; i++)
            {
                if (flexData[i, "IsSelect"] != DBNull.Value && Convert.ToBoolean(flexData[i, "IsSelect"]))
                {
                    int intPrintQuantity = Convert.ToInt32(flexData[i, "PrintQuantity"]);
                    if (intPrintQuantity < 1)
                        continue;
                    for (int iQuantity = 1; iQuantity <= intPrintQuantity; iQuantity++)
                    {
                        if (rowFormat == null)
                            rowFormat = dtbFormat.NewRow();
                        int intResult = (int)(decNumRow % CountPrint);
                        string strProductName = (flexData[i, "ProductName"] ?? "").ToString();
                        if (strProductName.Length > 50) strProductName = strProductName.Substring(0, 50) + "...";
                        if (CountPrint == 3)
                        {

                            if (intResult == 0)
                            {
                                rowFormat["ProductName3"] = strProductName;// flexData[i, "ProductName"];
                                rowFormat["PRODUCTID3"] = flexData[i, "IMEI"];
                                dtbFormat.Rows.Add(rowFormat);
                                rowFormat = null;
                            }
                            else if (intResult == 2)
                            {
                                rowFormat["ProductName2"] = strProductName;// flexData[i, "ProductName"];
                                rowFormat["PRODUCTID2"] = flexData[i, "IMEI"];
                            }
                            else
                            {
                                rowFormat["ProductName1"] = strProductName;// flexData[i, "ProductName"];
                                rowFormat["PRODUCTID1"] = flexData[i, "IMEI"];
                            }
                        }
                        else
                        {
                            if (intResult == 0)
                            {
                                rowFormat["ProductName2"] = strProductName;// flexData[i, "ProductName"];
                                rowFormat["PRODUCTID2"] = flexData[i, "IMEI"];
                                dtbFormat.Rows.Add(rowFormat);
                                rowFormat = null;
                            }
                            //else if (intResult == 2)
                            //{
                            //    rowFormat["ProductName2"] = flexData[i, "ProductName"];
                            //    rowFormat["PRODUCTID2"] = flexData[i, "IMEI"];
                            //}
                            else
                            {
                                rowFormat["ProductName1"] = strProductName;//flexData[i, "ProductName"];
                                rowFormat["PRODUCTID1"] = flexData[i, "IMEI"];
                            }
                        }
                        decNumRow++;
                    }
                }
            }
            if (rowFormat != null)
            {
                dtbFormat.Rows.Add(rowFormat);
            }
            return dtbFormat;
        }


        /// <summary>
        /// Định dạng bảng 2 cột trước khi in
        /// </summary>
        /// <param name="dtbData"></param>
        /// <returns></returns>
        private DataTable FormatTable()
        {
            DataTable dtbFormat = new DataTable();
            dtbFormat.Columns.Add("ProductName1");
            dtbFormat.Columns.Add("PRODUCTID1");
            dtbFormat.Columns.Add("ProductName2");
            dtbFormat.Columns.Add("PRODUCTID2");
            dtbFormat.Columns.Add("ProductName3");
            dtbFormat.Columns.Add("PRODUCTID3");
            decimal decNumRow = 1;
            DataRow rowFormat = null;
            for (int i = flexData.Rows.Fixed; i < flexData.Rows.Count; i++)
            {
                if (flexData[i, "IsSelect"] != DBNull.Value && Convert.ToBoolean(flexData[i, "IsSelect"]))
                {
                    int intPrintQuantity = Convert.ToInt32(flexData[i, "PrintQuantity"]);
                    if (intPrintQuantity < 1)
                        continue;
                    for (int iQuantity = 1; iQuantity <= intPrintQuantity; iQuantity++)
                    {
                        if (rowFormat == null)
                            rowFormat = dtbFormat.NewRow();
                        int intResult = (int)(decNumRow % 3);
                        if (intResult == 0)
                        {
                            rowFormat["ProductName3"] = flexData[i, "ProductName"];
                            rowFormat["PRODUCTID3"] = flexData[i, "PRODUCTID"];
                            dtbFormat.Rows.Add(rowFormat);
                            rowFormat = null;
                        }
                        else if (intResult == 2)
                        {
                            rowFormat["ProductName2"] = flexData[i, "ProductName"];
                            rowFormat["PRODUCTID2"] = flexData[i, "PRODUCTID"];
                        }
                        else
                        {
                            rowFormat["ProductName1"] = flexData[i, "ProductName"];
                            rowFormat["PRODUCTID1"] = flexData[i, "PRODUCTID"];
                        }
                        decNumRow++;
                    }
                }
            }
            if (rowFormat != null)
            {
                dtbFormat.Rows.Add(rowFormat);
            }
            return dtbFormat;
        }

        /// <summary>
        /// Nạp danh sách sản phẩm cần in
        /// </summary>
        /// <returns></returns>
        private DataTable GetProduct()
        {
            DataTable dtbData = null;
            int intMainGroupID = Convert.ToInt32(cboMainGroupID.SelectedValue);
            int intSubGroupID = Convert.ToInt32(cboSubGroupID.SelectedValue);
            string strProductID = cboProductList1.ProductIDList;
            String strProductIDList = strProductID.Replace("><", ",");
            strProductIDList = strProductIDList.Replace("<", "");
            strProductIDList = strProductIDList.Replace(">", "");
            //MessageBox.Show(strProductIDList);
            int intIsNew = -1;//tất cả
            if (cboIsNew.SelectedIndex == 1)
                intIsNew = 1;
            else
                if (cboIsNew.SelectedIndex == 2)
                    intIsNew = 0;
            string strBrandIDList = Library.AppCore.LoadControls.CheckedComboBoxEditObject.GetCheckedItemToString(cboBrandIDList);
            int intStoreID = Convert.ToInt32(cboStoreSearch.EditValue);
            string strInputVoucherID = string.Empty;
            if (chkIsFromInputVoucher.Checked)
            //if (!string.IsNullOrWhiteSpace(txtInputVoucherID.Text))
                strInputVoucherID = txtInputVoucherID.Text;
            cmdSearch.Enabled = false;

            try
            {
                PLC.Barcode.PLCProductBarcode objPLCProductBarcode = new PLCProductBarcode();
                dtbData = objPLCProductBarcode.GetCurrentInstockPrints(intMainGroupID, intSubGroupID, strProductIDList, strBrandIDList, intStoreID, intIsNew, chkIsInstock.Checked, strInputVoucherID);
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return null;
                }
            }
            catch (Exception objex)
            {
                SystemErrorWS.Insert("Lỗi nạp danh sách sản phẩm cần in", objex.ToString(), DUIInventory_Globals.ModuleName);
                return null;
            }
            finally
            {
                cmdSearch.Enabled = true;
            }
            return dtbData;
        }

        /// <summary>
        /// Nạp danh sách sản phẩm cần in
        /// </summary>
        /// <returns></returns>
        private DataTable GetIMEI()
        {
            DataTable dtbData = null;
            int intMainGroupID = Convert.ToInt32(cboMainGroupID.SelectedValue);
            int intSubGroupID = Convert.ToInt32(cboSubGroupID.SelectedValue);
            string strProductID = cboProductList1.ProductIDList;
            String strProductIDList = strProductID.Replace("><", ",");
            strProductIDList = strProductIDList.Replace("<", "");
            strProductIDList = strProductIDList.Replace(">", "");
            //MessageBox.Show(strProductIDList);
            int intIsNew = -1;//tất cả
            if (cboIsNew.SelectedIndex == 1)
                intIsNew = 1;
            else
                if (cboIsNew.SelectedIndex == 2)
                    intIsNew = 0;
            string strBrandIDList = Library.AppCore.LoadControls.CheckedComboBoxEditObject.GetCheckedItemToString(cboBrandIDList);
            int intStoreID = Convert.ToInt32(cboStoreSearch.EditValue);
            string strInputVoucherID = string.Empty;
            if (!string.IsNullOrWhiteSpace(txtInputVoucherID.Text))
                strInputVoucherID = txtInputVoucherID.Text;
            int intSearchBy = cboSearchBy.SelectedIndex;
            cmdSearch.Enabled = false;
            try
            {
                PLC.Barcode.PLCProductBarcode objPLCProductBarcode = new PLCProductBarcode();
                dtbData = objPLCProductBarcode.GetCurrentInstockPrints_IMEI(intMainGroupID, intSubGroupID, strProductIDList, strBrandIDList, intStoreID, intIsNew, intSearchBy == 0 ? strInputVoucherID : "");
                if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return null;
                }
                if (intSearchBy == 1)
                {
                    if (!string.IsNullOrWhiteSpace(txtInputVoucherID.Text))
                    {
                        var varData = dtbData.Select("IMEI ='" + (strInputVoucherID).Trim() + "'");
                        if (varData.Any())
                        {
                            dtbData = varData.CopyToDataTable();
                        }
                        else
                        {
                            dtbData = dtbData.Clone();
                        }
                    }
                }
            }
            catch (Exception objex)
            {
                SystemErrorWS.Insert("Lỗi nạp danh sách sản phẩm cần in", objex.ToString(), DUIInventory_Globals.ModuleName);
                return null;
            }
            finally
            {
                cmdSearch.Enabled = true;
            }
            return dtbData;
        }


        #endregion

        #region Event

        private void frmInputVoucher_PrintProduct_Load(object sender, EventArgs e)
        {
            GetPrintBacodeType();
            LoadComboBox();
            cmdSearch.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_PrintProduct);
            btnSetPrintQuantity.Enabled = false;
            btnPrintProduct.Enabled = false;
            btnPrintQRCode.Enabled = false;
            cboQRSize.Enabled = false;
            mnuProductAdd.Enabled = false;
            cboIsNew.SelectedIndex = 0;
            cboQRSize.SelectedIndex = 0;
            CreateColumImport();
        }

        private void btnPrintProduct_Click(object sender, EventArgs e)
        {
            if (flexData.DataSource == null)
                return;
            if (!Library.AppCore.LoadControls.C1FlexGridObject.CheckIsSelected(flexData, "IsSelect"))
            {
                MessageBox.Show(this, "Không tìm thấy sản phẩm cần in", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {

                DataTable dtbReport = IsIMEI == false ? FormatTable() : FormatTableIMEI();
                if (dtbReport.Rows.Count < 1)
                {
                    MessageBox.Show(this, "Không tìm thấy sản phẩm cần in", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                string strConfig = cboQRSize.SelectedIndex == 0 ? "PRINTIMEIBARCODE_31_REPORTID" : "PRINTIMEIBARCODE_REPORTID";
                int intReportID = IsIMEI == true ? Library.AppCore.AppConfig.GetIntConfigValue(strConfig) : Library.AppCore.AppConfig.GetIntConfigValue("PRINTPRODUCTBARCODE_REPORTID");
                ERP.MasterData.PLC.MD.WSReport.Report objReport = MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID);
                objReport.PrinterTypeID = "BarcodePrinter";
                objReport.DataTableSource = dtbReport;
                ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport);
            }
            catch (Exception objExc)
            {
                SystemErrorWS.Insert("Lỗi in sản phẩm", objExc, DUIInventory_Globals.ModuleName);
                MessageBox.Show("Lỗi in sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void cboMainGroupID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int intMainGroupID = 0;
            if (cboMainGroupID.SelectedIndex > 0)
                intMainGroupID = Convert.ToInt32(cboMainGroupID.SelectedValue);
            Library.AppCore.LoadControls.SetDataSource.SetSubGroup(this, cboSubGroupID, intMainGroupID);
            Library.AppCore.LoadControls.SetDataSource.SetBrand(this, cboBrandIDList, intMainGroupID);
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {            
            LoadProduct();
            cboQRSize.SelectedIndex = 0;
        }

        private void LoadProudctList()
        {
            string strBrandIDList = Library.AppCore.LoadControls.CheckedComboBoxEditObject.GetCheckedItemToString(cboBrandIDList);
            cboProductList1.InitControl(Convert.ToInt32(cboMainGroupID.SelectedValue), Convert.ToString(cboSubGroupID.SelectedValue), strBrandIDList, true);
        }

        private void btnSetPrintQuantity_Click(object sender, EventArgs e)
        {
            if (flexData.Rows.Count < flexData.Rows.Fixed)
                return;
            int intPrintQuantity = Convert.ToInt32(txtPrintQuantity.Text.Trim());
            if (intPrintQuantity < 1)
            {
                MessageBox.Show(this, "Vui lòng nhập số lượng in tối thiểu là 1", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPrintQuantity.Text = "1";
                txtPrintQuantity.Focus();
                return;
            }
            decimal decTotalQuantity = 0;
            for (int i = flexData.Rows.Fixed; i < flexData.Rows.Count; i++)
            {
                flexData[i, "PrintQuantity"] = intPrintQuantity;
                if (Convert.ToBoolean(flexData[i, "IsSelect"]))
                    decTotalQuantity += intPrintQuantity;
            }
            txtTotalPrintQuantity.Text = Convert.ToString(decTotalQuantity);
        }

        private void flexData_SetupEditor(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Col != flexData.Cols["PrintQuantity"].Index)
                return;
            TextBox txtEditor = flexData.Editor as TextBox;
            if (txtEditor != null)
            {
                txtEditor.MaxLength = 3;
            }
        }

        private void flexData_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (e.Row < flexData.Rows.Fixed)
                return;
            if (e.Col == flexData.Cols["IsSelect"].Index)
            {
                if (flexData[e.Row, "IsSelect"] == DBNull.Value)
                {
                    flexData[e.Row, "IsSelect"] = false;
                }
            }
            if (e.Col == flexData.Cols["PrintQuantity"].Index
                && (flexData[e.Row, "PrintQuantity"] == DBNull.Value || Convert.ToDecimal(flexData[e.Row, "PrintQuantity"]) < 1))
            {
                flexData[e.Row, "PrintQuantity"] = 1;
            }
            decimal decTotalPrintQuantity = 0;
            for (int i = flexData.Rows.Fixed; i < flexData.Rows.Count; i++)
            {
                if (Convert.ToBoolean(flexData[i, "IsSelect"]))
                    decTotalPrintQuantity += Convert.ToDecimal(flexData[i, "PrintQuantity"]);
            }
            txtTotalPrintQuantity.Text = Convert.ToString(decTotalPrintQuantity);
        }


        private void mnuProductAdd_Click(object sender, EventArgs e)
        {
            if (!mnuProductAdd.Enabled)
                return;
            if (flexData.DataSource == null)
            {
                MessageBox.Show(this, "Vui lòng tìm kiếm sản phẩm cần in trước khi thêm sản phẩm khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            ERP.MasterData.DUI.Search.frmProductSearch frmProductSearch1 = ERP.MasterData.DUI.MasterData_Globals.frmProductSearch;
            frmProductSearch1.IsMultiSelect = true;
            frmProductSearch1.MainGroupID = Convert.ToInt32(cboMainGroupID.SelectedValue);
            frmProductSearch1.IsLockMainGroupValue = true;
            frmProductSearch1.ShowDialog();
            if (frmProductSearch1.ProductList != null)
            {
                DataTable dtbSource = (DataTable)flexData.DataSource;
                dtbSource.AcceptChanges();

                decimal decTotalPrintQuantity = Convert.ToDecimal(txtTotalPrintQuantity.Text.Trim());
                foreach (var obj in frmProductSearch1.ProductList)
                {
                    DataRow row = dtbSource.NewRow();
                    row["IsSelect"] = true;
                    row["ProductID"] = obj.ProductID;
                    row["ProductName"] = obj.ProductName;
                    row["Quantity"] = 0;
                    row["PrintQuantity"] = txtPrintQuantity.Text.Trim();
                    decTotalPrintQuantity += Convert.ToDecimal(txtPrintQuantity.Text.Trim());
                    dtbSource.Rows.Add(row);
                }
                txtTotalPrintQuantity.Text = Convert.ToString(decTotalPrintQuantity);
            }
        }

        private void txtPrintQuantity_Leave(object sender, EventArgs e)
        {
            if (txtPrintQuantity.Text.Trim() == "0" || txtPrintQuantity.Text.Trim() == "") txtPrintQuantity.Text = "1";
        }

        private void cboStoreSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                cboStoreSearch.ShowPopup();
        }
        #endregion

        private void btnPrintQRCode_Click(object sender, EventArgs e)
        {
            if (flexData.DataSource == null)
                return;
            if (!Library.AppCore.LoadControls.C1FlexGridObject.CheckIsSelected(flexData, "IsSelect"))
            {
                MessageBox.Show(this, "Không tìm thấy sản phẩm cần in", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                DataTable dtbReport = cboQRSize.SelectedIndex == 0 ? FormatTableQR31() : FormatTableQR_2x1();
                if (dtbReport.Rows.Count < 1)
                {
                    MessageBox.Show(this, "Không tìm thấy sản phẩm cần in", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                int intReportID = Library.AppCore.AppConfig.GetIntConfigValue("PRINTPRODUCTQRCODE_REPORTID");
                ERP.MasterData.PLC.MD.WSReport.Report objReport = MasterData.PLC.MD.PLCReport.LoadInfoFromCache(intReportID);
                if (cboQRSize.SelectedIndex == 1)
                    objReport.ReportPath = objReport.ReportPath.Replace("rptPrintProductQRCode.rpt", "rptPrintProductQRCode_21.rpt");
                objReport.PrinterTypeID = "BarcodePrinter";
                objReport.DataTableSource = dtbReport;
                ERP.Report.DUI.DUIReportDataSource.ShowReport(this, objReport);
            }
            catch (Exception objExc)
            {
                SystemErrorWS.Insert("Lỗi in sản phẩm", objExc, DUIInventory_Globals.ModuleName);
                MessageBox.Show("Lỗi in sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void chkIsFromInputVoucher_CheckedChanged(object sender, EventArgs e)
        {
            txtInputVoucherID.Text = string.Empty;
            txtInputVoucherID.ReadOnly = !chkIsFromInputVoucher.Checked;
            if (txtInputVoucherID.ReadOnly)
                txtInputVoucherID.BackColor = System.Drawing.SystemColors.Info;
            else
                txtInputVoucherID.BackColor = Color.White;
        }
        
        private void txtInputVoucherID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cmdSearch_Click(null, null);
            }
        }

        #region Import Excel
        ClsImportExcel objClsImportExcel;
        DataTable dtbIMEI = new DataTable();
        public void CreateColumImport()
        {
            objClsImportExcel.InitializeColumnGridView(
                new DataColumnInfor("ISSELECT", "ISSELECT", typeof(bool), 200, true, 1, DataColumnInfor.TypeShow.ONLY_GRID_DATA),
                 new DataColumnInfor("ProductID", "Mã sản phẩm", typeof(string), 200, true, 2, DataColumnInfor.TypeShow.ONLY_GRID_IMPORT),
                 new DataColumnInfor("ProductNAME", "Tên sản phẩm", typeof(string), 220, true, 3, DataColumnInfor.TypeShow.ONLY_GRID_IMPORT),
                 new DataColumnInfor("IMEI", "IMEI", typeof(string), 150, true, 4, DataColumnInfor.TypeShow.ALL),
                 new DataColumnInfor("QUANTITY", "QUANTITY", typeof(decimal), 160, true, 5, DataColumnInfor.TypeShow.ONLY_GRID_DATA),
                 new DataColumnInfor("PRINTQUANTITY", "PRINTQUANTITY", typeof(decimal), 160, true, 6, DataColumnInfor.TypeShow.ONLY_GRID_DATA),
                 new DataColumnInfor("MODELNAME", "MODELNAME", typeof(string), 160, true, 7, DataColumnInfor.TypeShow.ONLY_GRID_DATA),
                 new DataColumnInfor("MAINGROUPNAME", "MAINGROUPNAME", typeof(string), 160, true, 8, DataColumnInfor.TypeShow.ONLY_GRID_DATA),
                 new DataColumnInfor("ISNEW", "ISNEW", typeof(decimal), 160, true, 9, DataColumnInfor.TypeShow.ONLY_GRID_DATA)
             );
        }

        public string IExcelImport_FormTitle
        {
            get { return "Nhập danh sách IMEI từ Excel"; }
        }

        public List<object> IExcelImport_ReturnDataImport(DataTable dtbData, DataRow drExcelFile, out bool IsError, out string strMessenger)
        {
            IsError = false;
            strMessenger = string.Empty;

            List<object> objReturn;

            string strProductID = string.Empty;
            string strProductNAME = string.Empty;
            string strIMEI = string.Empty;
            string strQUANTITY = "0";
            string strPRINTQUANTITY = "0";
            string strMODELNAME = string.Empty;
            string strMAINGROUPNAME = string.Empty;
            Decimal bolISNEW = 0;
            try
            {
                strIMEI = (drExcelFile[0] ?? "").ToString().Trim();

                var varResult = dtbData.AsEnumerable().Where(row => (row.Field<string>("IMEI") ?? "").ToString().Trim().ToUpper() == strIMEI.ToUpper());
                if (varResult.Any())
                    throw (new Exception("IMEI đã tồn tại trong file import!"));
                var objIMEI = dtbIMEI.Select("IMEI='" + strIMEI.ToUpper() + "'");
                if (objIMEI.Length > 0)
                {
                    strProductID = objIMEI[0]["ProductID"].ToString();
                    strProductNAME = objIMEI[0]["ProductNAME"].ToString();
                    strQUANTITY = (objIMEI[0]["QUANTITY"] ?? "0").ToString();
                    strPRINTQUANTITY = (objIMEI[0]["PRINTQUANTITY"] ?? "0").ToString();
                    strMODELNAME = objIMEI[0]["MODELNAME"].ToString();
                    strMAINGROUPNAME = objIMEI[0]["MAINGROUPNAME"].ToString();
                    Decimal.TryParse((objIMEI[0]["ISNEW"] ?? "0").ToString().Trim(), out bolISNEW);
                }
                else
                    throw (new Exception("IMEI không tồn tại trong hệ thống!"));

            }
            catch (Exception ex)
            {
                IsError = true;
                if (string.IsNullOrEmpty(strMessenger))
                    strMessenger = ex.Message;
                else strMessenger += ", " + ex.Message;

            }
            finally
            {
                objReturn = new List<object>() { true, strProductID, strProductNAME, strIMEI, Decimal.Parse(strQUANTITY), Decimal.Parse(strPRINTQUANTITY), strMODELNAME, strMAINGROUPNAME, bolISNEW };
            }
            return objReturn;

        }

        private void mnuIMEItoExcel_Click(object sender, EventArgs e)
        {
            PLC.Barcode.PLCProductBarcode objPLCProductBarcode = new PLCProductBarcode();
            dtbIMEI = objPLCProductBarcode.GetCurrentInstockPrints_IMEI(-1, -1, "", "", 0, -1, "");
            if (flexData.DataSource != null && flexData.Rows.Count - 1 > 0)
            {
                if (MessageBox.Show("Dữ liệu trên lưới sẽ bị xóa. Bạn có muốn tiếp tục không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
                {
                    flexData.DataSource = dtbIMEI.Clone();
                    txtTotalQuantity.Text = "0";
                    txtTotalPrintQuantity.Text = "0";
                    FormatFlex();
                    if (objClsImportExcel.DoImportExcel())
                    {
                        if (objClsImportExcel.dtbReturn != null)
                        {
                            int intotalQuantity = 0;
                            if (objClsImportExcel.dtbReturn == null || objClsImportExcel.dtbReturn.Rows.Count <= 0)
                            {
                                txtTotalQuantity.Text = "0";
                            }
                            else
                            {
                                foreach (DataRow dr in objClsImportExcel.dtbReturn.Rows)
                                {
                                    intotalQuantity += Convert.ToInt32(dr["Quantity"]);
                                }
                                txtTotalQuantity.Text = intotalQuantity.ToString();
                            }
                            flexData.DataSource = objClsImportExcel.dtbReturn;
                           
                            FormatFlex();
                            decimal decTotalPrintQuantity = 0;
                            for (int i = flexData.Rows.Fixed; i < flexData.Rows.Count; i++)
                            {
                                if (Convert.ToBoolean(flexData[i, "IsSelect"]))
                                    decTotalPrintQuantity += Convert.ToDecimal(flexData[i, "PrintQuantity"]);
                            }
                            txtTotalPrintQuantity.Text = Convert.ToString(decTotalPrintQuantity);
                            MessageBox.Show("Nhập IMEI từ Excel danh sách sản phẩm thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            else
            {
                if (objClsImportExcel.DoImportExcel())
                {
                    if (objClsImportExcel.dtbReturn != null)
                    {
                        int intotalQuantity = 0;
                        if (objClsImportExcel.dtbReturn == null || objClsImportExcel.dtbReturn.Rows.Count <= 0)
                        {
                            txtTotalQuantity.Text = "0";
                        }
                        else
                        {
                            foreach (DataRow dr in objClsImportExcel.dtbReturn.Rows)
                            {
                                intotalQuantity += Convert.ToInt32(dr["Quantity"]);
                            }
                            txtTotalQuantity.Text = intotalQuantity.ToString();
                        }
                        flexData.DataSource = objClsImportExcel.dtbReturn;
                        FormatFlex();
                        decimal decTotalPrintQuantity = 0;
                        for (int i = flexData.Rows.Fixed; i < flexData.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(flexData[i, "IsSelect"]))
                                decTotalPrintQuantity += Convert.ToDecimal(flexData[i, "PrintQuantity"]);
                        }
                        txtTotalPrintQuantity.Text = Convert.ToString(decTotalPrintQuantity);
                        MessageBox.Show("Nhập IMEI từ Excel danh sách sản phẩm thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        #endregion

    }
}