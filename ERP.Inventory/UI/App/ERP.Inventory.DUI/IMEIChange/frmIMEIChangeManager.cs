﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;
using DevExpress.XtraGrid.Views.Grid;

namespace ERP.Inventory.DUI.IMEIChange
{
    public partial class frmIMEIChangeManager : Form
    {
        #region Khai báo biến
        private string strPermission_ExportExcel = "PM_IMEICHANGE_EXPORTEXCEL";
        private string strPermission_Deleted = "PM_IMEICHANGE_DELETE";
        private bool bolPermission_Export = false;
        private bool bolPermission_Delete = false;
        private PLC.IMEIChange.PLCIMEIChange objPLCIMEIChange = new PLC.IMEIChange.PLCIMEIChange();
        #endregion
        #region Constructor
        public frmIMEIChangeManager()
        {
            InitializeComponent();
        }
        #endregion

        #region Event
        
        private void frmIMEIChangeManager_Load(object sender, EventArgs e)
        {
            LoadPermision();
            InitControl();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
            {
                return;
            }
            btnSearch.Enabled = false;
            LoadData();
            btnSearch.Enabled = true;
        }

        private void txtKeyWord_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch_Click(null, null);
            }
        }
        private void mnuAction_Opening(object sender, CancelEventArgs e)
        {
            mnuItemDelete.Enabled = false;
            mnuItemExport.Enabled = false;
            if (grdViewData.FocusedRowHandle < 0)
                return;
            DataRow dtRow = grdViewData.GetFocusedDataRow();
            mnuItemDelete.Enabled = (bolPermission_Delete && !Convert.ToBoolean(dtRow["ISDELETED"]));
            mnuItemExport.Enabled = bolPermission_Export && grdViewData.RowCount > 0;
        }
        private void mnuItemDelete_Click(object sender, EventArgs e)
        {
            if (grdViewData.FocusedRowHandle < 0)
                return;
            DataRow dtRow = grdViewData.GetFocusedDataRow();
            if (dtRow == null)
                return;
            if (MessageBox.Show(this, "Bạn có chắc muốn xóa đổi IMEI không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                return;
            }
            objPLCIMEIChange.Delete(Convert.ToString(dtRow["IMEIChangeID"]).Trim());
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return;
            }
            else
            {
                MessageBox.Show(this, "Xóa đổi IMEI thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnSearch_Click(null, null);
            }
        }

        private void mnuItemExport_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }
        #endregion

        #region Method

        private void LoadPermision()
        {
            bolPermission_Export = SystemConfig.objSessionUser.IsPermission(strPermission_ExportExcel);
            bolPermission_Delete = SystemConfig.objSessionUser.IsPermission(strPermission_Deleted);
        }

        private bool CheckInput()
        {
            if (dtpFromDateSearch.Value.Date > dtpToDateSearch.Value.Date)
            {
                MessageBoxObject.ShowWarningMessage(this, "Từ ngày không thể lớn hơn đến ngày!");
                dtpFromDateSearch.Focus();
                return false;
            }
            if (dtpFromDateSearch.Value.Date.AddYears(1) < dtpToDateSearch.Value.Date)
            {
                dtpToDateSearch.Focus();
                MessageBox.Show(this, "Vui lòng xem trong vòng 1 năm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private void InitControl()
        {
            string[] fieldNames = new string[] { "OUTPUTVOUCHERID", "PRODUCTID", "PRODUCTNAME", "FROMIMEI", 
                "TOIMEI", "CREATEDFULLNAME", "CREATEDDATE", "DELETEDFULLNAME", "DELETEDDATE"};
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.CreateColumns(grdData, true, false, true, true, fieldNames);
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grdViewData);
            FormatGrid();
            cboIsSearch.SelectedIndex = 0;
        }

        /// <summary>
        /// Hàm load dữ liệu lên lưới
        /// </summary>
        private bool LoadData()
        {
            string strOutputVoucherID = string.Empty;
            string strFromIMEI = string.Empty;
            string strToIMEI = string.Empty;
            string strKeyWord = txtKeyWord.Text.Trim();
            if (cboIsSearch.SelectedIndex == 0)
                strOutputVoucherID = strKeyWord;
            else if (cboIsSearch.SelectedIndex == 1)
                strToIMEI = strKeyWord;
            else if (cboIsSearch.SelectedIndex == 2)
                strFromIMEI = strKeyWord;
            object[] objKeywords = new object[]
            {
              "@FromDate", dtpFromDateSearch.Value,
              "@ToDate", dtpToDateSearch.Value,
              "@IsDeleted", chkIsDeleted.Checked,
              "@OutputVoucherID", strOutputVoucherID,
              "@FromIMEI", strFromIMEI,
              "@ToIMEI", strToIMEI
            };
            DataTable dtbData = null;
            dtbData = objPLCIMEIChange.SearchData(objKeywords);
            if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.Message, Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                return false;
            }
            grdData.DataSource = dtbData;
            grdViewData.Columns["DELETEDFULLNAME"].Visible = chkIsDeleted.Checked;
            grdViewData.Columns["DELETEDDATE"].Visible = chkIsDeleted.Checked;
            return true;
        }

        private bool FormatGrid()
        {
            try
            {
                grdViewData.Columns["OUTPUTVOUCHERID"].Caption = "Mã phiếu xuất";
                grdViewData.Columns["PRODUCTID"].Caption = "Mã sản phẩm";
                grdViewData.Columns["PRODUCTNAME"].Caption = "Tên sản phẩm";
                grdViewData.Columns["FROMIMEI"].Caption = "IMEI cũ";
                grdViewData.Columns["TOIMEI"].Caption = "IMEI mới";
                grdViewData.Columns["CREATEDFULLNAME"].Caption = "Người đổi";
                grdViewData.Columns["CREATEDDATE"].Caption = "Ngày đổi";
                grdViewData.Columns["DELETEDFULLNAME"].Caption = "Người hủy";
                grdViewData.Columns["DELETEDDATE"].Caption = "Ngày hủy";

                grdViewData.OptionsView.ColumnAutoWidth = false;
                grdViewData.Columns["OUTPUTVOUCHERID"].Width = 150;
                grdViewData.Columns["PRODUCTID"].Width = 130;
                grdViewData.Columns["PRODUCTNAME"].Width = 170;
                grdViewData.Columns["FROMIMEI"].Width = 130;
                grdViewData.Columns["TOIMEI"].Width = 130;
                grdViewData.Columns["CREATEDFULLNAME"].Width = 170;
                grdViewData.Columns["CREATEDDATE"].Width = 110;
                grdViewData.Columns["DELETEDFULLNAME"].Width = 170;
                grdViewData.Columns["DELETEDDATE"].Width = 110;
                grdViewData.Columns["DELETEDFULLNAME"].Visible = chkIsDeleted.Checked;
                grdViewData.Columns["DELETEDDATE"].Visible = chkIsDeleted.Checked;

                grdViewData.Columns["CREATEDDATE"].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
                grdViewData.Columns["CREATEDDATE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                grdViewData.Columns["DELETEDDATE"].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
                grdViewData.Columns["DELETEDDATE"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;

                grdViewData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                grdViewData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                grdViewData.OptionsView.ShowAutoFilterRow = true;
                grdViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                grdViewData.OptionsView.ShowFooter = false;
                grdViewData.OptionsView.ShowGroupPanel = false;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Định dạng lưới danh sách IMEI đổi");
                SystemErrorWS.Insert("Định dạng lưới danh sách IMEI đổi", objExce.ToString(), this.Name + " -> FormatGrid", DUIInventory_Globals.ModuleName);
                return false;
            }
            return true;
        }
        #endregion
        
    }
}
