﻿using Library.AppCore;
using Library.AppCore.LoadControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP.Inventory.DUI.Search
{
    public partial class frmMachineErrorSearch : Form
    {
        /// <summary>
        /// Created by : Nguyen Van Tai
        /// Created date : 30/11/2017
        /// Desc : Form tim kiem tra cuu loi
        /// </summary>

        public int MachineErrorId { get; set; }
        public string MachineErrorName { get; set; }

        private List<ErrorGroup> lstErrorGroup = null;

        public List<ErrorGroup> ListErrorGroup
        {
            get { return lstErrorGroup; }
            set { lstErrorGroup = value; }
        }


        public bool IsSelect { get; set; }
        // Hỗ trợ chọn nhiều
        public bool Multiselect { get; set; }
        public List<ErrorList> lstMachineError { get; set; }

        Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection chkGrid = null;

        public frmMachineErrorSearch()
        {
            InitializeComponent();
            InitControl();
            //LoadData();
            this.DialogResult = DialogResult.Cancel;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Top = 0;
        }

        private void InitControl()
        {
            try
            {
                //Load data combobox Nhóm lỗi
                DataTable dtbErrorGroup = Library.AppCore.DataSource.GetDataSource.GetMachineErrorGroup().Copy();
                //cboErrorGroup.InitControl(true, dtbErrorGroup, "MACHINEERRORGROUPID", "MACHINEERRORGROUPNAME", "-- Chọn nhóm lỗi --");
                cboErrorGroup.InitControl(true, dtbErrorGroup, "MACHINEERRORGROUPID", "MACHINEERRORGROUPNAME", "-- Chọn nhóm lỗi --");
                //if (this.ListMachineErrorGroupId > 0)
                //{
                //    cboErrorGroup.SetValue(this.ListMachineErrorGroupId);
                //}

            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp dữ liệu khởi tạo");
                SystemErrorWS.Insert("Lỗi nạp dữ liệu khởi tạo", objExce.ToString(), this.Name + " -> InitControl", Globals.ModuleName);
                return;
            }
        }
        private void LoadData()
        {
            try
            {
                object[] objKeywords = new object[]
                {
                    "@MachineErrorGroupIDList", cboErrorGroup.ColumnIDList.Replace("><", ", ").Replace("<", "").Replace(">", ""),
                    "@Keywords",txtKeywords.Text.Trim()
                };
                DataTable dtbData = new ERP.Report.PLC.PLCReportDataSource().GetDataSource("MD_MACHINEERROR_SRH3", objKeywords);
                grdData.DataSource = dtbData;
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi nạp dữ liệu");
                SystemErrorWS.Insert("Lỗi nạp dữ liệu", objExce.ToString(), this.Name + " -> LoadData", Globals.ModuleName);
                return;
            }
        }

        private void grdData_DoubleClick(object sender, EventArgs e)
        {
            if (!Multiselect)
                btnChoose_Click(null, null);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            try
            {
                //if (!Multiselect)
                //{
                //    if (grvData.FocusedRowHandle < 0) return;
                //    DataRow rFocused = grvData.GetFocusedDataRow();
                //    this.ListMachineErrorGroupId = int.Parse(rFocused["MACHINEERRORGROUPID"].ToString());
                //    this.MachineErrorId = int.Parse(rFocused["MACHINEERRORID"].ToString());
                //    this.MachineErrorGroupName = rFocused["MACHINEERRORGROUPNAME"].ToString();
                //    this.MachineErrorName = rFocused["MACHINEERRORNAME"].ToString();
                //}
                //else
                //  {
                lstMachineError = new List<ErrorList>();
                //if (string.IsNullOrEmpty(cboErrorGroup.ColumnIDList))
                //{
                //    MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn ít nhất một nhóm lỗi");
                //    return;
                //}
                if (chkGrid.SelectedCount < 1)
                {
                    MessageBoxObject.ShowWarningMessage(this, "Vui lòng chọn ít nhất một lỗi");
                    return;
                }
                lstErrorGroup = new List<ErrorGroup>();
                DataTable dtbErrorGroup = Library.AppCore.DataSource.GetDataSource.GetMachineErrorGroup().Copy();
                if (string.IsNullOrEmpty(cboErrorGroup.ColumnIDList))
                {
                    for (int i = 0; i < dtbErrorGroup.Rows.Count; i++)
                    {
                        DataRow row = dtbErrorGroup.Rows[i];
                        int intErrorGroupId = Convert.ToInt32(row["MACHINEERRORGROUPID"]);
                        if (CheckExistErrorList(intErrorGroupId) /*&& !lstErrorGroup.Any(x => x.MACHINEERRORGROUPID == intErrorGroupId)*/)
                        {
                            ErrorGroup objErrorGroup = new ErrorGroup();
                            objErrorGroup.MACHINEERRORGROUPID = Convert.ToInt32(row["MACHINEERRORGROUPID"]);
                            objErrorGroup.MACHINEERRORGROUPNAME = Convert.ToString(row["MACHINEERRORGROUPNAME"]).Trim();
                            lstErrorGroup.Add(objErrorGroup);
                        }
                    }
                }
                else
                {
                    string listSelect = cboErrorGroup.ColumnIDList.Trim();
                    for (int i = 0; i < dtbErrorGroup.Rows.Count; i++)
                    {
                        DataRow row = dtbErrorGroup.Rows[i];
                        if (listSelect.Contains("<" + Convert.ToInt32(row["MACHINEERRORGROUPID"]) + ">"))
                        {
                            ErrorGroup objErrorGroup = new ErrorGroup();
                            objErrorGroup.MACHINEERRORGROUPID = Convert.ToInt32(row["MACHINEERRORGROUPID"]);
                            objErrorGroup.MACHINEERRORGROUPNAME = Convert.ToString(row["MACHINEERRORGROUPNAME"]).Trim();
                            lstErrorGroup.Add(objErrorGroup);
                        }
                    }
                }

                for (int j = 0; j < lstErrorGroup.Count; j++)
                {
                    string strErrorGroupName = ListErrorGroup[j].MACHINEERRORGROUPNAME.ToString();
                    int intErrorGroupId = ListErrorGroup[j].MACHINEERRORGROUPID;
                    for (int i = 0; i < chkGrid.SelectedCount; i++)
                    {
                        ErrorList obj = new ErrorList();
                        //DataRow rFocused = grvData.GetFocusedDataRow();
                        DataRowView rowSelect = (DataRowView)chkGrid.GetSelectedRow(i);
                        if (Convert.ToInt32(rowSelect.Row["MACHINEERRORGROUPID"]) == intErrorGroupId)
                        {
                            obj.MachineErrorGroupId = int.Parse(rowSelect.Row["MACHINEERRORGROUPID"].ToString());
                            obj.MachineErrorId = int.Parse(rowSelect.Row["MACHINEERRORID"].ToString());
                            obj.MachineErrorGroupName = strErrorGroupName = rowSelect.Row["MACHINEERRORGROUPNAME"].ToString();
                            obj.MachineErrorName = rowSelect.Row["MACHINEERRORNAME"].ToString();
                            lstMachineError.Add(obj);
                        }
                    }

                }


                //   }
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception objExce)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi khi lấy dữ liệu máy lỗi");
                SystemErrorWS.Insert("Lỗi khi lấy dữ liệu máy lỗi", objExce.ToString(), this.Name + " -> LoadData", Globals.ModuleName);
                return;
            }
        }

        private void frmMachineErrorSearch_Load(object sender, EventArgs e)
        {

            if (Multiselect)
                chkGrid = new Library.AppCore.CustomControls.GridControl.GridCheckMarksSelection(grvData);
            //if (this.ListErrorGroup.Count > 0)
            //{
            //    foreach (var item in ListErrorGroup)
            //    {
            //        cboErrorGroup.SetValue(item.MACHINEERRORGROUPID);
            //    }
            //}
            LoadData();
        }
        private bool CheckExistErrorList(int intErrorGroupId)
        {
            for (int i = 0; i < chkGrid.SelectedCount; i++)
            {
                DataRowView rowSelect = (DataRowView)chkGrid.GetSelectedRow(i);
                if (Convert.ToInt32(rowSelect.Row["MACHINEERRORGROUPID"]) == intErrorGroupId)
                {
                    return true;
                }
            }
            return false;
        }
    }
    public class ErrorList
    {
        public int MachineErrorId { get; set; }
        public string MachineErrorName { get; set; }
        public int MachineErrorGroupId { get; set; }
        public string MachineErrorGroupName { get; set; }
    }
    public class ErrorGroup
    {
        private int intMACHINEERRORGROUPID;

        public int MACHINEERRORGROUPID
        {
            get { return intMACHINEERRORGROUPID; }
            set { intMACHINEERRORGROUPID = value; }
        }

        private string strMACHINEERRORGROUPNAME;

        public string MACHINEERRORGROUPNAME
        {
            get { return strMACHINEERRORGROUPNAME; }
            set { strMACHINEERRORGROUPNAME = value; }
        }
    }
}
