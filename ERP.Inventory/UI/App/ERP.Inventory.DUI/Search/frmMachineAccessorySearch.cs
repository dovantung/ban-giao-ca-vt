﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
//using HISERP.MasterData.ISR.PLC.WSINSGroupCost;

namespace ERP.Inventory.DUI.Search
{
    public partial class frmMachineAccessorySearch : Form
    {
        private int intMainGroupId;
        private ERP.Inventory.PLC.BorrowProduct.PLCBorrowProduct objPLCBorrowProduct = new PLC.BorrowProduct.PLCBorrowProduct();
        public DataTable dtbSelect;
        public DataTable dtbOldSelect;
        DataTable dtbData = null;
        public frmMachineAccessorySearch(int _intMainGroupId)
        {
            InitializeComponent();
            intMainGroupId = _intMainGroupId;
            
        }


        private void LoadData()
        {
            
            object[] objKeywords = new object[]
            {
                "@MAINGROUPID", intMainGroupId
            };
            objPLCBorrowProduct.SearchDataMachineAccessory(ref dtbData, objKeywords);
            if (dtbData != null)
            {
                if (dtbOldSelect != null)
                {
                    foreach (DataRow row in dtbData.Rows)
                    {
                        DataRow[] rowSelect = dtbOldSelect.Select(" MACHINEACCESSORYID='" + row["MACHINEACCESSORYID"].ToString() + "'");
                        if (rowSelect.Length > 0)
                        {
                            row["ISSELECTED"] = 1;
                            row["NOTE"] = rowSelect[0]["NOTE"];
                        }
                            
                    }
                }
                
                grdData.DataSource = dtbData;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (dtbSelect == null)
            {
                dtbSelect = new DataTable("TableAccessory");
                DataColumn[] col = new DataColumn[]{new DataColumn("MACHINEACCESSORYID", typeof(Int32))
                                                ,new DataColumn("MACHINEACCESSORYNAME", typeof(String))
                                                ,new DataColumn("ISOTHER", typeof(Decimal))
                                                ,new DataColumn("NOTE", typeof(String))
                                                };
                dtbSelect.Columns.AddRange(col);
            }

            foreach (DataRow objRow in dtbData.Rows)
            {
                if (objRow["ISSELECTED"].ToString() == "1")
                {
                    DataRow row = dtbSelect.NewRow();
                    row["MACHINEACCESSORYID"] = objRow["MACHINEACCESSORYID"];
                    row["MACHINEACCESSORYNAME"] = objRow["MACHINEACCESSORYNAME"].ToString();
                    row["NOTE"] = objRow["NOTE"].ToString();

                    dtbSelect.Rows.Add(row);
                }
                
            }
            this.DialogResult = DialogResult.OK;
        }

        private void frmMachineAccessorySearch_Load(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}
