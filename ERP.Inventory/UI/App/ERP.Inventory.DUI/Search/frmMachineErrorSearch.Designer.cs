﻿namespace ERP.Inventory.DUI.Search
{
    partial class frmMachineErrorSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboErrorGroup = new Library.AppControl.ComboBoxControl.ComboBoxCustom();
            this.btnChoose = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.grvData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reposClassify = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.reposISSELECTED = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.reposSIMTYPEID = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.reposPrice = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.reposISRELEASE = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reposClassify)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reposISSELECTED)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reposSIMTYPEID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reposPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reposISRELEASE)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cboErrorGroup);
            this.panel1.Controls.Add(this.btnChoose);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.txtKeywords);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(804, 35);
            this.panel1.TabIndex = 0;
            // 
            // cboErrorGroup
            // 
            this.cboErrorGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboErrorGroup.Location = new System.Drawing.Point(344, 6);
            this.cboErrorGroup.Margin = new System.Windows.Forms.Padding(0);
            this.cboErrorGroup.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboErrorGroup.Name = "cboErrorGroup";
            this.cboErrorGroup.Size = new System.Drawing.Size(231, 24);
            this.cboErrorGroup.TabIndex = 2;
            // 
            // btnChoose
            // 
            this.btnChoose.Image = global::ERP.Inventory.DUI.Properties.Resources.valid;
            this.btnChoose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnChoose.Location = new System.Drawing.Point(682, 6);
            this.btnChoose.Name = "btnChoose";
            this.btnChoose.Size = new System.Drawing.Size(100, 25);
            this.btnChoose.TabIndex = 4;
            this.btnChoose.Text = "Chọn";
            this.btnChoose.UseVisualStyleBackColor = true;
            this.btnChoose.Click += new System.EventHandler(this.btnChoose_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(578, 6);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(100, 25);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtKeywords
            // 
            this.txtKeywords.Location = new System.Drawing.Point(70, 7);
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.Size = new System.Drawing.Size(205, 22);
            this.txtKeywords.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(282, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nhóm lỗi:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Từ khóa:";
            // 
            // grdData
            // 
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(0, 35);
            this.grdData.MainView = this.grvData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.reposClassify,
            this.reposISSELECTED,
            this.reposSIMTYPEID,
            this.reposPrice,
            this.reposISRELEASE});
            this.grdData.Size = new System.Drawing.Size(804, 310);
            this.grdData.TabIndex = 5;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvData});
            this.grdData.DoubleClick += new System.EventHandler(this.grdData_DoubleClick);
            // 
            // grvData
            // 
            this.grvData.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvData.Appearance.FooterPanel.Options.UseFont = true;
            this.grvData.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grvData.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.grvData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grvData.AppearancePrint.GroupRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvData.AppearancePrint.GroupRow.Options.UseFont = true;
            this.grvData.ColumnPanelRowHeight = 25;
            this.grvData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.grvData.GridControl = this.grdData;
            this.grvData.GroupFormat = "{1} {2}";
            this.grvData.Name = "grvData";
            this.grvData.OptionsBehavior.CopyToClipboardWithColumnHeaders = false;
            this.grvData.OptionsBehavior.Editable = false;
            this.grvData.OptionsCustomization.AllowSort = false;
            this.grvData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grvData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grvData.OptionsNavigation.AutoMoveRowFocus = false;
            this.grvData.OptionsNavigation.UseTabKey = false;
            this.grvData.OptionsPrint.PrintHeader = false;
            this.grvData.OptionsPrint.UsePrintStyles = true;
            this.grvData.OptionsSelection.MultiSelect = true;
            this.grvData.OptionsView.ColumnAutoWidth = false;
            this.grvData.OptionsView.ShowAutoFilterRow = true;
            this.grvData.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn1.Caption = "Mã lỗi";
            this.gridColumn1.FieldName = "MACHINEERRORID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 120;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "Tên lỗi";
            this.gridColumn2.FieldName = "MACHINEERRORNAME";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 350;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "Nhóm lỗi";
            this.gridColumn3.FieldName = "MACHINEERRORGROUPNAME";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 300;
            // 
            // reposClassify
            // 
            this.reposClassify.AutoHeight = false;
            this.reposClassify.Name = "reposClassify";
            this.reposClassify.ValueChecked = 1;
            this.reposClassify.ValueUnchecked = 0;
            // 
            // reposISSELECTED
            // 
            this.reposISSELECTED.AutoHeight = false;
            this.reposISSELECTED.Name = "reposISSELECTED";
            this.reposISSELECTED.ValueChecked = 1;
            this.reposISSELECTED.ValueUnchecked = 0;
            // 
            // reposSIMTYPEID
            // 
            this.reposSIMTYPEID.AutoHeight = false;
            this.reposSIMTYPEID.Name = "reposSIMTYPEID";
            this.reposSIMTYPEID.NullText = "Chưa phân loại";
            // 
            // reposPrice
            // 
            this.reposPrice.AutoHeight = false;
            this.reposPrice.DisplayFormat.FormatString = "###,####,###,##0;";
            this.reposPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.reposPrice.EditFormat.FormatString = "###,####,###,##0;";
            this.reposPrice.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.reposPrice.Mask.EditMask = "###,####,###,##0;";
            this.reposPrice.Mask.UseMaskAsDisplayFormat = true;
            this.reposPrice.MaxLength = 12;
            this.reposPrice.Name = "reposPrice";
            // 
            // reposISRELEASE
            // 
            this.reposISRELEASE.AutoHeight = false;
            this.reposISRELEASE.Name = "reposISRELEASE";
            this.reposISRELEASE.ValueChecked = 1;
            this.reposISRELEASE.ValueUnchecked = 0;
            // 
            // frmMachineErrorSearch
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 345);
            this.Controls.Add(this.grdData);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmMachineErrorSearch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tìm kiếm lỗi máy";
            this.Load += new System.EventHandler(this.frmMachineErrorSearch_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reposClassify)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reposISSELECTED)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reposSIMTYPEID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reposPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reposISRELEASE)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtKeywords;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnChoose;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grvData;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit reposClassify;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit reposISSELECTED;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit reposSIMTYPEID;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit reposPrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit reposISRELEASE;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private Library.AppControl.ComboBoxControl.ComboBoxCustom cboErrorGroup;
    }
}