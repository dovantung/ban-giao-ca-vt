﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using ERP.Inventory.PLC.Warranty.WSWarrantyRegister;
using ERP.Inventory.PLC.Warranty;

namespace ERP.Inventory.DUI.Warranty
{
    public partial class frmAddWarrantyRegister : Form
    {
        public frmAddWarrantyRegister()
        {
            InitializeComponent();
        }

        #region Khai báo biến
        private List<WarrantyRegister> objWarrantyRegisterList = new List<WarrantyRegister>();
        #endregion

        private void frmAddWarrantyRegister_Load(object sender, EventArgs e)
        {
            LoadComboBox();
            InitWarrantyRegister();
        }

        private void InitWarrantyRegister()
        {
            try
            {
                grdData.DataSource = objWarrantyRegisterList;
                grdData.RefreshDataSource();
            }
            catch (Exception)
            {
                throw;
            }
        }


        private void LoadComboBox()
        {
            try
            {
                dtpOutputDate.Value = Globals.GetServerDateTime();
                Library.AppCore.LoadControls.SetDataSource.SetMainGroup(this,cboMainGroup, true);
                Library.AppCore.LoadControls.SetDataSource.SetSubGroup(this,cboSubGroup,0);
                Library.AppCore.LoadControls.SetDataSource.SetBrand(this,comboBoxBrand1,0);
                cboStoreList.InitControl(false);
            }
            catch (Exception objEx)
            {
                SystemErrorWS.Insert("Lỗi nạp combobox", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi nạp combobox", "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSelectProduct_Click(object sender, EventArgs e)
        {
             ERP.MasterData.DUI.Search.frmProductSearch frmProductSearch1 = ERP.MasterData.DUI.MasterData_Globals.frmProductSearch;
             frmProductSearch1.MainGroupPermissionType = Library.AppCore.Constant.EnumType.MainGroupPermissionType.VIEWREPORT;
             frmProductSearch1.MainGroupID = Convert.ToInt32(cboMainGroup.SelectedValue);
             frmProductSearch1.SubGroupID = Convert.ToInt32(cboSubGroup.SelectedValue);
             frmProductSearch1.BrandID = Convert.ToInt32(comboBoxBrand1.BrandID);
             frmProductSearch1.ShowDialog();
             if (frmProductSearch1.Product != null)
             {
                 txtProductID.Text = frmProductSearch1.Product.ProductID;
             }
             else
                 txtProductID.Text = string.Empty;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CheckInput())
                    return;
                AddProduct();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool CheckInput()
        {
            if (txtOutputVoucherID.Text.Trim() == string.Empty)
            {
                txtOutputVoucherID.Focus();
                MessageBox.Show(this, "Bạn chưa nhập mã phiếu xuất", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            
            if (txtIMEI.Text.Trim() == string.Empty)
            {
                txtIMEI.Focus();
                MessageBox.Show(this, "Bạn chưa nhập IMEI", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (txtCustomerPhone.Text.Trim() == string.Empty)
            {
                txtCustomerPhone.Focus();
                MessageBox.Show(this, "Bạn chưa nhập điện thoại khách hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (cboStoreList.StoreID<=0)
            {
                cboStoreList.Focus();
                MessageBox.Show(this, "Vui lòng chọn siêu thị", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
           
            if (txtProductID.Text.Trim() == string.Empty)
            {
                btnSelectProduct.Focus();
                MessageBox.Show(this, "Bạn chưa chọn sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(txtProductID.Text.Trim());
            if (objProduct == null || objProduct.ProductName == string.Empty)
            {
                MessageBox.Show(this, "Mã sản phẩm không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private void AddProduct()
        {
            
            //TGDD.ERP.Warranty.UI.APPDObjects.WarrantyRegister.DWarrantyRegister objDWarrantyRegister1 = new TGDD.ERP.Warranty.UI.APPDObjects.WarrantyRegister.DWarrantyRegister();
            //DataTable dtbIMEI_IsRegister_IsRegisterError = objDWarrantyRegister1.GetWarrantyRegisterSearch1();
            ////kiểm tra xem phiếu xuất đã đăng ký thành công chưa
            //if (dtbIMEI_IsRegister_IsRegisterError != null && dtbIMEI_IsRegister_IsRegisterError.Rows.Count > 0)
            //{
            //    DataRow[] dr1 = dtbIMEI_IsRegister_IsRegisterError.Select("TRIM(OUTPUTVOUCHERID) = '" + txtOutputVoucherID.Text.Trim() + "' AND ISREGISTER = 1 AND ISREGISTERERROR = 1");
            //    if (dr1 != null && dr1.Length < 1)
            //    {
            //        if (txtIMEI.Text.Trim() != string.Empty)
            //        {
            //            MessageBox.Show(this, "IMEI " + txtIMEI.Text.Trim() + " này đã được đăng ký bảo hành thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        }
            //        else
            //        {
            //            MessageBox.Show(this, "Mã sản phẩm " + txtProductID.Text.Trim() + " này đã được đăng ký bảo hành thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        }
            //        return;
            //    }
            //}
             ERP.MasterData.PLC.MD.WSProduct.Product objProduct = ERP.MasterData.PLC.MD.PLCProduct.LoadInfoFromCache(txtProductID.Text.Trim());
             if (objProduct == null || objProduct.ProductName == string.Empty)
                 return;
            var v_check=from o in objWarrantyRegisterList
                        where o.OutputVoucherID == txtOutputVoucherID.Text.Trim() 
                        && o.ProductID == txtProductID.Text.Trim()
                        && o.IMEI == txtIMEI.Text.Trim()
                        select o;
            if (v_check.Count() <= 0)
            {
                WarrantyRegister objRegister = new WarrantyRegister();
                objRegister.RegisterStoreID = cboStoreList.StoreID;
                objRegister.RegisterStoreName = cboStoreList.StoreName;
                objRegister.OutputVoucherID  = txtOutputVoucherID.Text.Trim();
                objRegister.OutputDate = dtpOutputDate.Value;
                objRegister.ProductID  = txtProductID.Text.Trim();
                objRegister.ProductName = objProduct.ProductName;
                objRegister.IMEI = txtIMEI.Text.Trim();
                objRegister.CustomerPhone = txtCustomerPhone.Text.Trim();
                objWarrantyRegisterList.Add(objRegister);
                grdData.RefreshDataSource();
            }
            txtOutputVoucherID.Text = string.Empty;
            txtCustomerPhone.Text = string.Empty;
            txtIMEI.Text = string.Empty;
            txtOutputVoucherID.Focus();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

        }

        private void cboMainGroup_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                int intMainGroupID = Convert.ToInt32(cboMainGroup.SelectedValue);
                //Library.AppCore.LoadControls.SetDataSource.SetBrand(this, cboBrand,intMainGroupID);
                comboBoxBrand1.InitControl(false, intMainGroupID);
                Library.AppCore.LoadControls.SetDataSource.SetSubGroup(this, cboSubGroup, intMainGroupID);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
