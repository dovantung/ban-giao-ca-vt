﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.Warranty
{
    /// <summary>
    /// Created by: Nguyễn Linh Tuấn
    /// </summary>
    public partial class frmWarrantyRegisterManager : Form
    {
        
        #region Constructor
        public frmWarrantyRegisterManager()
        {
            InitializeComponent();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvData);
        }
        
        #endregion

        #region Methods
    
        private void LoadComboBox()
        {
            cboMainGroupID.InitControl(false, true);
            //Library.AppCore.LoadControls.SetDataSource.SetMainGroup(this, cboMainGroupID, true);
            //ComboBox cbSubGroup = new ComboBox();
            //DataTable dtbSubGroup = Library.AppCore.DataSource.GetDataSource.GetSubGroup().Copy();
            //Library.AppCore.LoadControls.CheckedComboBoxEditObject.LoadDataFromDataTable(dtbSubGroup, cboSubGroupIDList);
            //cboSubGroupIDList.Text = "Tất cả";
            comboBoxBrand1.InitControl(true, -1);
            comboBoxBrand1.InitControl(true, 0);
            //DataTable dtbCustomer = Library.AppCore.DataSource.GetDataSource.GetCustomer().Copy();
            //Library.AppCore.LoadControls.CheckedComboBoxEditObject.LoadDataFromDataTable(dtbCustomer, cboCustomerIDList);
            cboCustomerIDList.InitControl(true);
            Library.AppCore.DataSource.FilterObject.StoreFilter objStoreFilter = new Library.AppCore.DataSource.FilterObject.StoreFilter();
            //objStoreFilter.CompanyID = Library.AppCore.SystemConfig.DefaultCompany.CompanyID;
            //objStoreFilter.IsCheckPermission = true;
            cboStoreID.InitControl(true, objStoreFilter);
            cboIsRegister.SelectedIndex = 0;
            cboIsRegisterError.SelectedIndex = 0;

        }



        private bool CheckInput()
        {
            if (cboMainGroupID.MainGroupID < 1)
            {
                MessageBox.Show(this, "Bạn chưa chọn ngành hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cboMainGroupID.Focus();
                return false;
            }

            if (radOutputDate.Checked)
            {
                if (dtToOutputDate.Value < dtFromOutputDate.Value)
                {
                    MessageBox.Show(this, "Từ ngày không được lớn hơn đến ngày!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    dtFromOutputDate.Focus();
                    return false;
                }
                if (dtToOutputDate.Value.Subtract(dtFromOutputDate.Value).Days > 31)
                {
                    MessageBox.Show(this, "Bạn chỉ được tìm kiếm trong vòng 31 ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    dtFromOutputDate.Focus();
                    return false;
                }
            }
            else if (radRegisterTime.Checked)
            {
                if (dtToRegisterTime.Value < dtFromRegisterTime.Value)
                {
                    MessageBox.Show(this, "Từ ngày không được lớn hơn đến ngày!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    dtFromOutputDate.Focus();
                    return false;
                }
                if (dtToRegisterTime.Value.Subtract(dtFromRegisterTime.Value).Days > 31)
                {
                    MessageBox.Show(this, "Bạn chỉ được tìm kiếm trong vòng 31 ngày", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    dtFromRegisterTime.Focus();
                    return false;
                }
            }
            return true;
        }
        
        #endregion

        #region Events
        private void frmWarrantyRegisterManager_Load(object sender, EventArgs e)
        {
            LoadComboBox();
        }


        private void cboMainGroupID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int intMainGroupID = Convert.ToInt32(cboMainGroupID.MainGroupID);
            comboBoxBrand1.InitControl(true, intMainGroupID);
            comboBoxBrand1.Text = "--Tất cả--";
            //ComboBox cbSubGroup = new ComboBox();
            //Library.AppCore.LoadControls.SetDataSource.SetSubGroup(this, cbSubGroup, intMainGroupID);
            //DataTable dtbSubGroup = cbSubGroup.DataSource as DataTable;
            //dtbSubGroup.Rows.RemoveAt(0);
            //Library.AppCore.LoadControls.CheckedComboBoxEditObject.LoadDataFromDataTable(dtbSubGroup, comboBoxBrand1);
            cboSubGroupIDList.InitControl(true, intMainGroupID);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;
            btnSearch.Enabled = false;
            try
            {
                ERP.Inventory.PLC.Warranty.PLCWarrantyRegister objPLCWarrantyRegister = new PLC.Warranty.PLCWarrantyRegister();
                DateTime? dtmFromOutputDate = null;
                DateTime? dtmToOutputDate = null;
                DateTime? dtmFromRegisterTime = null;
                DateTime? dtmToRegisterTime = null;
                if (radOutputDate.Checked)
                {
                    dtmFromOutputDate = dtFromOutputDate.Value;
                    dtmToOutputDate = dtToOutputDate.Value;
                }
                else
                {
                    dtmFromRegisterTime = dtFromRegisterTime.Value;
                    dtmToRegisterTime = dtToRegisterTime.Value;
                }
                string strSubgroupIDList = cboSubGroupIDList.SubGroupIDList;
                string strCustomerIDList = cboCustomerIDList.CustomerIDList;
                object[] objKeywords = new object[] { 
                      "@FromOutputDate"   ,dtmFromOutputDate,
                      "@ToOutputDate"     ,dtmToOutputDate,
                      "@FromRegisterTime" ,dtmFromRegisterTime,
                      "@ToRegisterTime"  , dtmToRegisterTime,
                      "@MainGroupID"    ,  Convert.ToInt32(cboMainGroupID.MainGroupID),
                      "@SubGroupIDList",   strSubgroupIDList,
                      "@BrandIDList",      comboBoxBrand1.BrandIDList,
                      "@StoreIDList",      cboStoreID.StoreIDList,
                      "@BuyInputCustomerIDList",strCustomerIDList,
                      "@IsRegister"       ,(cboIsRegister.SelectedIndex - 1),
                      "@IsRegisterError"  ,(cboIsRegisterError.SelectedIndex - 1),  
                      "@Keyword"           ,txtKeyword.Text.Trim(),
                      "@chkProductID"      ,chkProductID.Checked,
                      "@chkProductName"    ,chkProductName.Checked,
                      "@chkIMEI"           ,chkIMEI.Checked 
                };
                DataTable dtbResult = objPLCWarrantyRegister.SearchData(objKeywords);
                if (Library.AppCore.SystemConfig.objSessionUser.ResultMessageApp.IsError)
                {
                    Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    btnSearch.Enabled = true;
                    return;
                }

                grdData.DataSource = dtbResult;
                grdData.RefreshDataSource();
                mnuItemExportExcel.Enabled = true;
                btnSearch.Enabled = true;
                btnExportExcel.Enabled = true;
                if (grvData.RowCount <= 0)
                    btnExportExcel.Enabled = false;
            }
            catch (Exception objEx)
            {
                Library.AppCore.SystemErrorWS.Insert("Lỗi tìm kiếm đăng ký bảo hành", objEx, DUIInventory_Globals.ModuleName);
                MessageBox.Show(this, "Lỗi tìm kiếm đăng ký bảo hành", "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnSearch.Enabled = true;
            }
        }

        private void grvData_DoubleClick(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow drData = grvData.GetDataRow(grvData.FocusedRowHandle);
            frmWarrantyRegisterDetail frmWarrantyRegisterDetail1 = new frmWarrantyRegisterDetail();
            frmWarrantyRegisterDetail1.RegisterID = Convert.ToInt32(drData["WARRANTYREGISTERID"]);
            frmWarrantyRegisterDetail1.OutputVoucherID = Convert.ToString(drData["OUTPUTVOUCHERID"]);
            frmWarrantyRegisterDetail1.OutputDate = Convert.ToDateTime(drData["OUTPUTDATE"]);
            frmWarrantyRegisterDetail1.ProductName2 = Convert.ToString(drData["PRODUCTNAME"]);
            frmWarrantyRegisterDetail1.IMEI = Convert.ToString(drData["IMEI"]);
            frmWarrantyRegisterDetail1.CustomerID = Convert.ToInt32(drData["CUSTOMERID"]);
            frmWarrantyRegisterDetail1.CustomerName = Convert.ToString(drData["CUSTOMERNAME"]);
            frmWarrantyRegisterDetail1.CustomerAddress = Convert.ToString(drData["Customeraddress"]);
            frmWarrantyRegisterDetail1.CustomerPhone = Convert.ToString(drData["CUSTOMERPHONE"]);
            frmWarrantyRegisterDetail1.IsRegister = Convert.ToBoolean(drData["ISREGISTERED"]);
            frmWarrantyRegisterDetail1.RegisteredTime = Convert.ToDateTime(Globals.IsNull(drData["Registeredtime"], null));
            frmWarrantyRegisterDetail1.ReplyMessage = Convert.ToString(drData["REPLYMESSAGE"]);
            frmWarrantyRegisterDetail1.StoreName = Convert.ToString(drData["STORENAME"]);
            frmWarrantyRegisterDetail1.IsRegisterError = Convert.ToBoolean(drData["ISREGISTERERROR"]);
            frmWarrantyRegisterDetail1.BuyInputCustomerName = Convert.ToString(drData["BuyInputCustomerName"]);
            frmWarrantyRegisterDetail1.ShowDialog();
        }

        //private void mnuItemSelectCustomer_Click(object sender, EventArgs e)
        //{
        //    ERP.MasterData.DUI.Search.frmCustomerSearch frmCustomerSearch1 = new MasterData.DUI.Search.frmCustomerSearch(true);
        //    frmCustomerSearch1.ShowDialog();
        //    if (frmCustomerSearch1.ListSelected != null)
        //    {
        //        Library.AppCore.LoadControls.CheckedComboBoxEditObject.SetCheckedItem(cboCustomerIDList, frmCustomerSearch1.ListSelected);
        //    }
        //}

        private void mnuItemExportExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }

        private void mnuPopUpExportExcel_Opening(object sender, CancelEventArgs e)
        {
            mnuItemExportExcel.Enabled = true;
            if (grvData.RowCount <= 0)
                mnuItemExportExcel.Enabled = false;
        }


        //private void btnSearchCustomer_Click(object sender, EventArgs e)
        //{
        //    mnuItemSelectCustomer_Click(null, null);
        //}

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
          
            mnuItemExportExcel_Click(null, null);
        }
        #endregion

        

    }
}
