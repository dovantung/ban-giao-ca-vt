﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;

namespace ERP.Inventory.DUI.Warranty
{
    public partial class frmWarrantyRegisterDetail : Form
    {
        #region Khai báo
        private int intRegisterID = 0;
        private String strOutputVoucherID = "";
        private DateTime dtmOutputDate = DateTime.Now;
        private String strProductName2 = "";
        private String strIMEI = "";
        private String strCustomerName = "";
        private String strCustomerAddress = "";
        private String strCustomerPhone = "";
        private String strRegisterPhone = "";
        private bool bolIsRegister = false;
        private String strRegisterStatusName = "";
        private DateTime dtmRegisterTime = DateTime.Now;
        private String strReplyMessage = "";
        private String strStoreName = "";
        private bool bolIsRegisterError = false;
        private bool bolIsViewDetail = false;
        private bool bolIsRegisterOK = true;
        private String strRegisterExtendID = "";
        private String strInvoiceID = "";
        private String strBuyInputCustomerName = "";
        private String strUserUpDateReward = "";
        private DateTime dtmReceiveRewardDate = DateTime.Now;
        private bool bolIsReceivedReward = false;
        private bool bolIsHasReceiveRewardDate = false;
        #endregion

        #region Properties
        private int intCustomerID = 0;

        public int CustomerID
        {
            get { return intCustomerID; }
            set { intCustomerID = value; }
        }
        public int RegisterID
        {
            get { return intRegisterID; }
            set { intRegisterID = value; }
        }

        public String OutputVoucherID
        {
            get { return strOutputVoucherID; }
            set { strOutputVoucherID = value; }
        }

        public DateTime OutputDate
        {
            get { return dtmOutputDate; }
            set { dtmOutputDate = value; }
        }

        public String ProductName2
        {
            get { return strProductName2; }
            set { strProductName2 = value; }
        }

        public String IMEI
        {
            get { return strIMEI; }
            set { strIMEI = value; }
        }
        
        public String CustomerName
        {
            get { return strCustomerName; }
            set { strCustomerName = value; }
        }

        public String CustomerAddress
        {
            get { return strCustomerAddress; }
            set { strCustomerAddress = value; }
        }

        public String CustomerPhone
        {
            get { return strCustomerPhone; }
            set { strCustomerPhone = value; }
        }

        public String RegisterPhone
        {
            get { return strRegisterPhone; }
            set { strRegisterPhone = value; }
        }

        public bool IsRegister
        {
            get { return bolIsRegister; }
            set { bolIsRegister = value; }
        }
        
        public String RegisterStatusName
        {
            get { return strRegisterStatusName; }
            set { strRegisterStatusName = value; }
        }

        public DateTime RegisteredTime
        {
            get { return dtmRegisterTime; }
            set { dtmRegisterTime = value; }
        }

        public String ReplyMessage
        {
            get { return strReplyMessage; }
            set { strReplyMessage = value; }
        }

        public String StoreName
        {
            get { return strStoreName; }
            set { strStoreName = value; }
        }

        public String InvoiceID
        {
            get { return strInvoiceID; }
            set { strInvoiceID = value; }
        }

        public bool IsRegisterError
        {
            get { return bolIsRegisterError; }
            set { bolIsRegisterError = value; }
        }

        public frmWarrantyRegisterDetail()
        {
            InitializeComponent();
        }

        public bool IsRegisterOK
        {
            get { return bolIsRegisterOK; }
            set { bolIsRegisterOK = value; }
        }

        public String BuyInputCustomerName
        {
            get { return strBuyInputCustomerName; }
            set { strBuyInputCustomerName = value; }
        }

        public String UserUpDateReward
        {
            get { return strUserUpDateReward; }
            set { strUserUpDateReward = value; }
        }

        public bool IsReceivedReward
        {
            get { return bolIsReceivedReward; }
            set { bolIsReceivedReward = value; }
        }

        public DateTime ReceiveRewardDate
        {
            get { return dtmReceiveRewardDate; }
            set { dtmReceiveRewardDate = value; }
        }

        public bool IsHasReceiveRewardDate
        {
            get { return bolIsHasReceiveRewardDate; }
            set { bolIsHasReceiveRewardDate = value; }
        }

        #endregion

        #region Sự kiện

        private void frmWarrantyRegisterDetail_Load(object sender, EventArgs e)
        {
            txtRegisterID.Text = intRegisterID.ToString();
            txtOutputVoucherID.Text = strOutputVoucherID;
            dtOutputDate.Value = dtmOutputDate;
            txtProductName.Text = strProductName2;
            txtIMEI.Text = strIMEI;
            txtCustomerID.Text = intCustomerID.ToString();
            txtCustomerName.Text = strCustomerName;
            txtCustomerAddress.Text = strCustomerAddress;
            txtCustomerPhone.Text = strCustomerPhone;
            txtCustomerID.Text = intCustomerID.ToString(); ;
            chkIsRegister.Checked = bolIsRegister;
            if (dtmRegisterTime==DateTime.MinValue)
                dtRegisterTime.Text=string.Empty;
            txtReplyMessage.Text = strReplyMessage;
            txtStoreName.Text = strStoreName;
            chkIsRegisterError.Checked = bolIsRegisterError;
            txtBuyInputCustomerName.Text = strBuyInputCustomerName;
        }
        #endregion
    }
}