﻿namespace ERP.Inventory.DUI.Warranty
{
    partial class frmWarrantyRegisterManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.mnuSelectCustomer = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemSelectCustomer = new System.Windows.Forms.ToolStripMenuItem();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.dtToRegisterTime = new System.Windows.Forms.DateTimePicker();
            this.dtFromRegisterTime = new System.Windows.Forms.DateTimePicker();
            this.radRegisterTime = new System.Windows.Forms.RadioButton();
            this.radOutputDate = new System.Windows.Forms.RadioButton();
            this.cboIsRegisterError = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.chkIMEI = new System.Windows.Forms.CheckBox();
            this.chkProductName = new System.Windows.Forms.CheckBox();
            this.chkProductID = new System.Windows.Forms.CheckBox();
            this.txtKeyword = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dtToOutputDate = new System.Windows.Forms.DateTimePicker();
            this.dtFromOutputDate = new System.Windows.Forms.DateTimePicker();
            this.cboIsRegister = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxBrand1 = new Library.AppControl.ComboBoxControl.ComboBoxBrand();
            this.cboStoreID = new Library.AppControl.ComboBoxControl.ComboBoxStore();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cboMainGroupID = new Library.AppControl.ComboBoxControl.ComboBoxMainGroup();
            this.cboSubGroupIDList = new Library.AppControl.ComboBoxControl.ComboBoxSubGroup();
            this.cboCustomerIDList = new Library.AppControl.ComboBoxControl.ComboBoxCustomer();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.grdData = new DevExpress.XtraGrid.GridControl();
            this.mnuPopUpExportExcel = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.grvData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.mnuSelectCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            this.mnuPopUpExportExcel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuSelectCustomer
            // 
            this.mnuSelectCustomer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemSelectCustomer});
            this.mnuSelectCustomer.Name = "mnuSelectCustomer";
            this.mnuSelectCustomer.Size = new System.Drawing.Size(179, 26);
            // 
            // mnuItemSelectCustomer
            // 
            this.mnuItemSelectCustomer.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.mnuItemSelectCustomer.Name = "mnuItemSelectCustomer";
            this.mnuItemSelectCustomer.Size = new System.Drawing.Size(178, 22);
            this.mnuItemSelectCustomer.Text = "Chọn nhà cung cấp";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 111);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 16);
            this.label14.TabIndex = 38;
            this.label14.Text = "Nhà cung cấp:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 32);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 16);
            this.label13.TabIndex = 35;
            this.label13.Text = "Ngành hàng:";
            // 
            // dtToRegisterTime
            // 
            this.dtToRegisterTime.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtToRegisterTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtToRegisterTime.Location = new System.Drawing.Point(823, 58);
            this.dtToRegisterTime.Name = "dtToRegisterTime";
            this.dtToRegisterTime.Size = new System.Drawing.Size(140, 22);
            this.dtToRegisterTime.TabIndex = 9;
            // 
            // dtFromRegisterTime
            // 
            this.dtFromRegisterTime.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtFromRegisterTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFromRegisterTime.Location = new System.Drawing.Point(665, 58);
            this.dtFromRegisterTime.Name = "dtFromRegisterTime";
            this.dtFromRegisterTime.Size = new System.Drawing.Size(152, 22);
            this.dtFromRegisterTime.TabIndex = 8;
            // 
            // radRegisterTime
            // 
            this.radRegisterTime.AutoSize = true;
            this.radRegisterTime.Location = new System.Drawing.Point(544, 58);
            this.radRegisterTime.Name = "radRegisterTime";
            this.radRegisterTime.Size = new System.Drawing.Size(118, 20);
            this.radRegisterTime.TabIndex = 7;
            this.radRegisterTime.Text = "Ngày bảo hành";
            this.radRegisterTime.UseVisualStyleBackColor = true;
            // 
            // radOutputDate
            // 
            this.radOutputDate.AutoSize = true;
            this.radOutputDate.Checked = true;
            this.radOutputDate.Location = new System.Drawing.Point(544, 32);
            this.radOutputDate.Name = "radOutputDate";
            this.radOutputDate.Size = new System.Drawing.Size(119, 20);
            this.radOutputDate.TabIndex = 2;
            this.radOutputDate.TabStop = true;
            this.radOutputDate.Text = "Ngày xuất hàng";
            this.radOutputDate.UseVisualStyleBackColor = true;
            // 
            // cboIsRegisterError
            // 
            this.cboIsRegisterError.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIsRegisterError.DropDownWidth = 250;
            this.cboIsRegisterError.FormattingEnabled = true;
            this.cboIsRegisterError.Items.AddRange(new object[] {
            "--Tất cả--",
            "Không lỗi",
            "Có lỗi"});
            this.cboIsRegisterError.Location = new System.Drawing.Point(357, 81);
            this.cboIsRegisterError.Name = "cboIsRegisterError";
            this.cboIsRegisterError.Size = new System.Drawing.Size(175, 24);
            this.cboIsRegisterError.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(291, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 16);
            this.label8.TabIndex = 26;
            this.label8.Text = "Lỗi:";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::ERP.Inventory.DUI.Properties.Resources.search;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(868, 82);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(94, 25);
            this.btnSearch.TabIndex = 17;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // chkIMEI
            // 
            this.chkIMEI.AutoSize = true;
            this.chkIMEI.Location = new System.Drawing.Point(767, 109);
            this.chkIMEI.Name = "chkIMEI";
            this.chkIMEI.Size = new System.Drawing.Size(53, 20);
            this.chkIMEI.TabIndex = 16;
            this.chkIMEI.Text = "IMEI";
            this.chkIMEI.UseVisualStyleBackColor = true;
            // 
            // chkProductName
            // 
            this.chkProductName.AutoSize = true;
            this.chkProductName.Location = new System.Drawing.Point(648, 109);
            this.chkProductName.Name = "chkProductName";
            this.chkProductName.Size = new System.Drawing.Size(113, 20);
            this.chkProductName.TabIndex = 15;
            this.chkProductName.Text = "Tên sản phẩm";
            this.chkProductName.UseVisualStyleBackColor = true;
            // 
            // chkProductID
            // 
            this.chkProductID.AutoSize = true;
            this.chkProductID.Location = new System.Drawing.Point(542, 109);
            this.chkProductID.Name = "chkProductID";
            this.chkProductID.Size = new System.Drawing.Size(108, 20);
            this.chkProductID.TabIndex = 14;
            this.chkProductID.Text = "Mã sản phẩm";
            this.chkProductID.UseVisualStyleBackColor = true;
            // 
            // txtKeyword
            // 
            this.txtKeyword.Location = new System.Drawing.Point(357, 108);
            this.txtKeyword.Name = "txtKeyword";
            this.txtKeyword.Size = new System.Drawing.Size(175, 22);
            this.txtKeyword.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(291, 111);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 16);
            this.label9.TabIndex = 16;
            this.label9.Text = "Chuỗi tìm:";
            // 
            // dtToOutputDate
            // 
            this.dtToOutputDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtToOutputDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtToOutputDate.Location = new System.Drawing.Point(823, 30);
            this.dtToOutputDate.Name = "dtToOutputDate";
            this.dtToOutputDate.Size = new System.Drawing.Size(140, 22);
            this.dtToOutputDate.TabIndex = 4;
            // 
            // dtFromOutputDate
            // 
            this.dtFromOutputDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtFromOutputDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFromOutputDate.Location = new System.Drawing.Point(665, 30);
            this.dtFromOutputDate.Name = "dtFromOutputDate";
            this.dtFromOutputDate.Size = new System.Drawing.Size(152, 22);
            this.dtFromOutputDate.TabIndex = 3;
            // 
            // cboIsRegister
            // 
            this.cboIsRegister.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIsRegister.FormattingEnabled = true;
            this.cboIsRegister.Items.AddRange(new object[] {
            "--Tất cả--",
            "Chưa đăng ký",
            "Đã đăng ký"});
            this.cboIsRegister.Location = new System.Drawing.Point(357, 54);
            this.cboIsRegister.Name = "cboIsRegister";
            this.cboIsRegister.Size = new System.Drawing.Size(175, 24);
            this.cboIsRegister.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(291, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "Kho xuất:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(291, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Đăng ký:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Nhà sản xuất:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nhóm hàng:";
            // 
            // comboBoxBrand1
            // 
            this.comboBoxBrand1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxBrand1.Location = new System.Drawing.Point(105, 81);
            this.comboBoxBrand1.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxBrand1.MinimumSize = new System.Drawing.Size(24, 24);
            this.comboBoxBrand1.Name = "comboBoxBrand1";
            this.comboBoxBrand1.Size = new System.Drawing.Size(175, 24);
            this.comboBoxBrand1.TabIndex = 10;
            // 
            // cboStoreID
            // 
            this.cboStoreID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStoreID.Location = new System.Drawing.Point(357, 28);
            this.cboStoreID.Margin = new System.Windows.Forms.Padding(0);
            this.cboStoreID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboStoreID.Name = "cboStoreID";
            this.cboStoreID.Size = new System.Drawing.Size(175, 24);
            this.cboStoreID.TabIndex = 1;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.cboMainGroupID);
            this.groupControl1.Controls.Add(this.cboSubGroupIDList);
            this.groupControl1.Controls.Add(this.cboCustomerIDList);
            this.groupControl1.Controls.Add(this.chkProductName);
            this.groupControl1.Controls.Add(this.cboStoreID);
            this.groupControl1.Controls.Add(this.comboBoxBrand1);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.label14);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.label13);
            this.groupControl1.Controls.Add(this.label5);
            this.groupControl1.Controls.Add(this.dtToRegisterTime);
            this.groupControl1.Controls.Add(this.cboIsRegister);
            this.groupControl1.Controls.Add(this.dtFromRegisterTime);
            this.groupControl1.Controls.Add(this.dtFromOutputDate);
            this.groupControl1.Controls.Add(this.radRegisterTime);
            this.groupControl1.Controls.Add(this.dtToOutputDate);
            this.groupControl1.Controls.Add(this.radOutputDate);
            this.groupControl1.Controls.Add(this.label9);
            this.groupControl1.Controls.Add(this.cboIsRegisterError);
            this.groupControl1.Controls.Add(this.txtKeyword);
            this.groupControl1.Controls.Add(this.label8);
            this.groupControl1.Controls.Add(this.chkProductID);
            this.groupControl1.Controls.Add(this.btnExportExcel);
            this.groupControl1.Controls.Add(this.btnSearch);
            this.groupControl1.Controls.Add(this.chkIMEI);
            this.groupControl1.Controls.Add(this.label6);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.LookAndFeel.SkinName = "Blue";
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(968, 136);
            this.groupControl1.TabIndex = 52;
            this.groupControl1.Text = "Thông tin tìm kiếm";
            // 
            // cboMainGroupID
            // 
            this.cboMainGroupID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMainGroupID.Location = new System.Drawing.Point(105, 28);
            this.cboMainGroupID.Margin = new System.Windows.Forms.Padding(0);
            this.cboMainGroupID.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboMainGroupID.Name = "cboMainGroupID";
            this.cboMainGroupID.Size = new System.Drawing.Size(175, 24);
            this.cboMainGroupID.TabIndex = 0;
            this.cboMainGroupID.SelectionChangeCommitted += new System.EventHandler(this.cboMainGroupID_SelectionChangeCommitted);
            // 
            // cboSubGroupIDList
            // 
            this.cboSubGroupIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSubGroupIDList.Location = new System.Drawing.Point(105, 54);
            this.cboSubGroupIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboSubGroupIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboSubGroupIDList.Name = "cboSubGroupIDList";
            this.cboSubGroupIDList.Size = new System.Drawing.Size(175, 24);
            this.cboSubGroupIDList.TabIndex = 5;
            // 
            // cboCustomerIDList
            // 
            this.cboCustomerIDList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCustomerIDList.IsActivatedType = Library.AppCore.Constant.EnumType.IsActivatedType.ACTIVATED;
            this.cboCustomerIDList.IsRequireActive = true;
            this.cboCustomerIDList.Location = new System.Drawing.Point(105, 107);
            this.cboCustomerIDList.Margin = new System.Windows.Forms.Padding(0);
            this.cboCustomerIDList.MinimumSize = new System.Drawing.Size(24, 24);
            this.cboCustomerIDList.Name = "cboCustomerIDList";
            this.cboCustomerIDList.Size = new System.Drawing.Size(175, 24);
            this.cboCustomerIDList.TabIndex = 12;
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Enabled = false;
            this.btnExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.btnExportExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportExcel.Location = new System.Drawing.Point(868, 107);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(94, 25);
            this.btnExportExcel.TabIndex = 18;
            this.btnExportExcel.Text = "  Xuất excel";
            this.btnExportExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(815, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(12, 16);
            this.label6.TabIndex = 57;
            this.label6.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(815, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "-";
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.grdData);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 136);
            this.groupControl2.LookAndFeel.SkinName = "Blue";
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(968, 360);
            this.groupControl2.TabIndex = 53;
            this.groupControl2.Text = "Danh sách đăng ký";
            // 
            // grdData
            // 
            this.grdData.ContextMenuStrip = this.mnuPopUpExportExcel;
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.Location = new System.Drawing.Point(2, 23);
            this.grdData.MainView = this.grvData;
            this.grdData.Name = "grdData";
            this.grdData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemDateEdit1});
            this.grdData.Size = new System.Drawing.Size(964, 335);
            this.grdData.TabIndex = 21;
            this.grdData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvData});
            // 
            // mnuPopUpExportExcel
            // 
            this.mnuPopUpExportExcel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemExportExcel});
            this.mnuPopUpExportExcel.Name = "mnuPopUpExportExcel";
            this.mnuPopUpExportExcel.Size = new System.Drawing.Size(160, 26);
            this.mnuPopUpExportExcel.Opening += new System.ComponentModel.CancelEventHandler(this.mnuPopUpExportExcel_Opening);
            // 
            // mnuItemExportExcel
            // 
            this.mnuItemExportExcel.Image = global::ERP.Inventory.DUI.Properties.Resources.excel;
            this.mnuItemExportExcel.Name = "mnuItemExportExcel";
            this.mnuItemExportExcel.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.mnuItemExportExcel.Size = new System.Drawing.Size(159, 22);
            this.mnuItemExportExcel.Text = "Xuất ra Excel";
            this.mnuItemExportExcel.Click += new System.EventHandler(this.mnuItemExportExcel_Click);
            // 
            // grvData
            // 
            this.grvData.Appearance.FocusedRow.BackColor = System.Drawing.Color.Transparent;
            this.grvData.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Transparent;
            this.grvData.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvData.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grvData.Appearance.FocusedRow.Options.UseFont = true;
            this.grvData.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Red;
            this.grvData.Appearance.FooterPanel.Options.UseForeColor = true;
            this.grvData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.grvData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvData.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.grvData.Appearance.Preview.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvData.Appearance.Preview.Options.UseFont = true;
            this.grvData.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.grvData.Appearance.Row.Options.UseFont = true;
            this.grvData.ColumnPanelRowHeight = 40;
            this.grvData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn18,
            this.gridColumn2,
            this.gridColumn6,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn17,
            this.gridColumn15,
            this.gridColumn5});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition1.Expression = "[ENDWARRANTYDATE] < [CURRENTDATE] And [ISACTIVE]=True";
            styleFormatCondition1.Value1 = "True";
            styleFormatCondition1.Value2 = "True";
            styleFormatCondition2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            styleFormatCondition2.Appearance.Options.UseBackColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition2.Expression = "[ISACTIVE]=False";
            styleFormatCondition2.Value1 = "False";
            styleFormatCondition2.Value2 = "False";
            this.grvData.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1,
            styleFormatCondition2});
            this.grvData.GridControl = this.grdData;
            this.grvData.Name = "grvData";
            this.grvData.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.grvData.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grvData.OptionsView.ColumnAutoWidth = false;
            this.grvData.OptionsView.ShowAutoFilterRow = true;
            this.grvData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvData.OptionsView.ShowFooter = true;
            this.grvData.OptionsView.ShowGroupPanel = false;
            this.grvData.DoubleClick += new System.EventHandler(this.grvData_DoubleClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn1.Caption = "Mã phiếu xuất";
            this.gridColumn1.FieldName = "OUTPUTVOUCHERID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "OUTPUTVOUCHERID", "Tổng cộng:")});
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 130;
            // 
            // gridColumn18
            // 
            this.gridColumn18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn18.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn18.Caption = "Mã sản phẩm";
            this.gridColumn18.FieldName = "PRODUCTID";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn18.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 1;
            this.gridColumn18.Width = 130;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn2.Caption = "Tên sản phẩm";
            this.gridColumn2.FieldName = "PRODUCTNAME";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn6.Caption = "IMEI";
            this.gridColumn6.FieldName = "IMEI";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            this.gridColumn6.Width = 130;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn3.Caption = "Kho xuất";
            this.gridColumn3.FieldName = "STORENAME";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            this.gridColumn3.Width = 147;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn4.Caption = "Ngày xuất";
            this.gridColumn4.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn4.FieldName = "OUTPUTDATE";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 5;
            this.gridColumn4.Width = 145;
            // 
            // gridColumn12
            // 
            this.gridColumn12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn12.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn12.Caption = "Tên khách hàng";
            this.gridColumn12.FieldName = "CUSTOMERNAME";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 10;
            this.gridColumn12.Width = 126;
            // 
            // gridColumn13
            // 
            this.gridColumn13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn13.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn13.Caption = "Địa chỉ";
            this.gridColumn13.FieldName = "CUSTOMERADDRESS";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 11;
            this.gridColumn13.Width = 157;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn14.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn14.Caption = "Số điện thoại";
            this.gridColumn14.FieldName = "CUSTOMERPHONE";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 12;
            this.gridColumn14.Width = 97;
            // 
            // gridColumn8
            // 
            this.gridColumn8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn8.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn8.Caption = "Đã đăng ký";
            this.gridColumn8.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn8.FieldName = "ISREGISTERED";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 6;
            this.gridColumn8.Width = 99;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = ((short)(1));
            this.repositoryItemCheckEdit1.ValueUnchecked = ((short)(0));
            // 
            // gridColumn9
            // 
            this.gridColumn9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn9.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn9.Caption = "Thời gian đăng ký";
            this.gridColumn9.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.gridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn9.FieldName = "REGISTEREDTIME";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 7;
            this.gridColumn9.Width = 103;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn10.Caption = "Nội dung tin trả về";
            this.gridColumn10.FieldName = "REPLYMESSAGE";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 8;
            this.gridColumn10.Width = 119;
            // 
            // gridColumn17
            // 
            this.gridColumn17.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColumn17.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn17.Caption = "Mã đăng ký";
            this.gridColumn17.FieldName = "WARRANTYREGISTERID";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 9;
            this.gridColumn17.Width = 106;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn15.Caption = "Lỗi ĐK";
            this.gridColumn15.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn15.FieldName = "ISREGISTERERROR";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 13;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn5.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColumn5.Caption = "Nhà cung cấp";
            this.gridColumn5.FieldName = "BUYINPUTCUSTOMERNAME";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 14;
            this.gridColumn5.Width = 250;
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.EditFormat.FormatString = "N4";
            this.repositoryItemCalcEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemCalcEdit1.Mask.EditMask = "##,###,###,###.##";
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // frmWarrantyRegisterManager
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 496);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmWarrantyRegisterManager";
            this.ShowIcon = false;
            this.Text = "Quản lý danh sách IMEI đăng ký bảo hành";
            this.Load += new System.EventHandler(this.frmWarrantyRegisterManager_Load);
            this.mnuSelectCustomer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            this.mnuPopUpExportExcel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker dtToRegisterTime;
        private System.Windows.Forms.DateTimePicker dtFromRegisterTime;
        private System.Windows.Forms.RadioButton radRegisterTime;
        private System.Windows.Forms.RadioButton radOutputDate;
        private System.Windows.Forms.ComboBox cboIsRegisterError;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.CheckBox chkIMEI;
        private System.Windows.Forms.CheckBox chkProductName;
        private System.Windows.Forms.CheckBox chkProductID;
        private System.Windows.Forms.TextBox txtKeyword;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtToOutputDate;
        private System.Windows.Forms.DateTimePicker dtFromOutputDate;
        private System.Windows.Forms.ComboBox cboIsRegister;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Library.AppControl.ComboBoxControl.ComboBoxBrand comboBoxBrand1;
        private Library.AppControl.ComboBoxControl.ComboBoxStore cboStoreID;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip mnuSelectCustomer;
        private System.Windows.Forms.ToolStripMenuItem mnuItemSelectCustomer;
        private System.Windows.Forms.ContextMenuStrip mnuPopUpExportExcel;
        private System.Windows.Forms.ToolStripMenuItem mnuItemExportExcel;
        private DevExpress.XtraGrid.GridControl grdData;
        private DevExpress.XtraGrid.Views.Grid.GridView grvData;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private System.Windows.Forms.Button btnExportExcel;
        private Library.AppControl.ComboBoxControl.ComboBoxCustomer cboCustomerIDList;
        private Library.AppControl.ComboBoxControl.ComboBoxSubGroup cboSubGroupIDList;
        private Library.AppControl.ComboBoxControl.ComboBoxMainGroup cboMainGroupID;
    }
}