﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore.LoadControls;

namespace ERP.Inventory.DUI.Warranty
{
    public partial class frmAddWarrantyInfo : Form
    {
        private bool bolIsAdded = false;

        public bool IsAdded
        {
            get { return bolIsAdded; }
        }
        public string IMEI
        {
            get { return txtIMEI.Text.Trim(); }
        }

        public string OutputVoucherID
        {
            get { return txtOutputVoucherID.Text.Trim(); }
        }


        public frmAddWarrantyInfo()
        {
            InitializeComponent();
        }

        private void frmAddWarrantyInfo_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtOutputVoucherID.Text.Trim()))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập mã phiếu xuất !");
                txtOutputVoucherID.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtIMEI.Text.Trim()))
            {
                MessageBoxObject.ShowWarningMessage(this, "Vui lòng nhập IMEI !");
                txtIMEI.Focus();
                return;
            }
            bolIsAdded = true;
            this.Close();
        }
    }
}
