﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Library.AppCore;
using Library.AppCore.LoadControls;

namespace ERP.Inventory.DUI.Warranty
{
    public partial class frmWarrantyInfoManager : Form
    {
        #region Variable

        private string strPermission_Add = "PM_WARRANTYINFO_ADD";
        private PLC.Warranty.PLCWarrantyInfo objPLCWarrantyInfo = new PLC.Warranty.PLCWarrantyInfo();

        #endregion

        #region Constructor

        public frmWarrantyInfoManager()
        {
            InitializeComponent();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvData);
        }

        #endregion

        #region Method        

        private void frmWarrantyInfoManager_Load(object sender, EventArgs e)
        {
            LoadCombobox();
            Library.AppCore.CustomControls.GridControl.FormatGridcontrol.CurrentInstance.SetClipboard(grvData);
            mnuItemAdd.Enabled = SystemConfig.objSessionUser.IsPermission(strPermission_Add);
        }
        private void LoadCombobox()
        {
            try
            {
                cboCustomer.InitControl(false, Library.AppCore.Constant.EnumType.CustomerType.ISPROVIDER);
                Library.AppCore.LoadControls.SetDataSource.SetMainGroup(this, cboMainGroupID, false,
                  Library.AppCore.Constant.EnumType.IsRequestIMEIType.ALL,
                   Library.AppCore.Constant.EnumType.IsServiceType.ALL,
                   Library.AppCore.Constant.EnumType.MainGroupPermissionType.OUTPUT);
                comboBoxBrand1.InitControl(false, 0);
                Library.AppCore.LoadControls.SetDataSource.SetSubGroup(this, cboSubGroupID,0);
                cboStatus.SelectedIndex = 0;
            }   
            catch (Exception)
            {
                throw;
            }
        }

        private bool Insert(string strIMEI, string strOutputVoucherID)
        {
            try
            {
                if (!objPLCWarrantyInfo.InsertWarrantyInfo(strIMEI, strOutputVoucherID))
                {
                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                MessageBoxObject.ShowInfoMessage(this, "Thêm thông tin quản lý bảo hành thành công!");
                return true;
            }
            catch (Exception objExc)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi thông tin quản lý bảo hành!");
                SystemErrorWS.Insert("Lỗi thông tin quản lý bảo hành!", objExc.ToString(), this.Name + " -> UpdateData", Globals.ModuleName);
                return false;
            }
        }

        private bool CalWarrantyTime(string strIMEI, string strProductID)
        {
            try
            {
                if (!objPLCWarrantyInfo.CalWarrantyTime(strIMEI, strProductID))
                {
                    MessageBoxObject.ShowWarningMessage(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                    return false;
                }
                MessageBoxObject.ShowInfoMessage(this, "Tính lại thời gian bảo hành thành công!");
                return true;
            }
            catch (Exception objExc)
            {
                MessageBoxObject.ShowWarningMessage(this, "Lỗi thông tin quản lý bảo hành!");
                SystemErrorWS.Insert("Lỗi thông tin quản lý bảo hành!", objExc.ToString(), this.Name + " -> UpdateData", Globals.ModuleName);
                return false;
            }
        }
        #endregion

        #region Event        
        
        private void cboMainGroupID_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                int intMainGroupID = Convert.ToInt32(cboMainGroupID.SelectedValue);
                //Library.AppCore.LoadControls.SetDataSource.SetBrand(this, cboBrandID,intMainGroupID);
                comboBoxBrand1.InitControl(false, intMainGroupID);
                Library.AppCore.LoadControls.SetDataSource.SetSubGroup(this, cboSubGroupID, intMainGroupID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (cboMainGroupID.SelectedIndex == 0)
            {
                MessageBox.Show(this,"Bạn chưa chọn ngành hàng để tìm kiếm!","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }
            btnSearch.Enabled = false;
            List<object> objList = new List<object>();
            objList.AddRange(new object[] 
                                    { 
                                        "@Keyword",txtSearch.Text.Trim().ToUpper(),
                                        "@MainGroupID", cboMainGroupID.SelectedValue,
                                        "@SubGroupID", cboSubGroupID.SelectedValue,
                                        "@BrandID", comboBoxBrand1.BrandID,
                                        "@CustomerID",cboCustomer.CustomerID,
                                        "@Status", cboStatus.SelectedIndex-1,
                                        "@IsIMEI",chkIsIMEI.Checked,
                                        "@IsProduct",chkProductID.Checked
                                    });

            if (dtpFromDate.Checked) objList.AddRange(new object[] { "@FromDate", dtpFromDate.Value.Date });
            if (dtpToDate.Checked) objList.AddRange(new object[] { "@ToDate", dtpToDate.Value.Date });
            DataTable dtbData = new ERP.Inventory.PLC.Warranty.PLCWarrantyInfo().SearchData(objList.ToArray());
            if (SystemConfig.objSessionUser.ResultMessageApp.IsError)
            {
                Library.AppCore.CustomControls.Message.frmWarningMessage.Show(this, SystemConfig.objSessionUser.ResultMessageApp.Message, SystemConfig.objSessionUser.ResultMessageApp.MessageDetail);
                btnSearch.Enabled = true;
                return;
            }
            if (dtbData != null)
            {
                grdData.DataSource = dtbData;
                grdData.RefreshDataSource();
            }
            btnSearch.Enabled = true;
            mnuItemReview.Enabled = grvData.RowCount > 0;
        }

        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
            Library.AppCore.Other.GridDevExportToExcel.ExportToExcel(grdData);
        }

        private void mnuItemAdd_Click(object sender, EventArgs e)
        {
            frmAddWarrantyInfo frm = new frmAddWarrantyInfo();
            frm.ShowDialog();
            if (frm.IsAdded)
            {
                Insert(frm.IMEI, frm.OutputVoucherID);
            }
        }

        private void mnuItemReview_Click(object sender, EventArgs e)
        {
            if (grvData.FocusedRowHandle < 0)
                return;
            DataRow row = (DataRow)grvData.GetFocusedDataRow();
            string strIMEI = (Convert.ToString(row["IMEI"])).Trim();
            string strProductID = (Convert.ToString(row["ProductID"])).Trim();
            if (CalWarrantyTime(strIMEI, strProductID))
            {
                btnSearch_Click(null, null);
            }
        }

        #endregion
    }
}
