﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;
using ERP.Inventory.PLC.StoreChangeCommand.WSAutoSplitProduct;

namespace ERP.Inventory.PLC.StoreChangeCommand
{
    public class PLCAutoSplitProduct
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/StoreChangeCommand/WSAutoSplitProduct.asmx";
        private StoreChangeCommand.WSAutoSplitProduct.WSAutoSplitProduct objWSAutoSplitProductProxy = null;
        private ResultMessage objResultMessage = new ResultMessage();
        #endregion
        #region Property
        private WSAutoSplitProduct.WSAutoSplitProduct WSAutoSplitProductProxy
        {
            get
            {
                if (objWSAutoSplitProductProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSAutoSplitProductProxy = new WSAutoSplitProduct.WSAutoSplitProduct(); 
                    objWSAutoSplitProductProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSAutoSplitProductProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSAutoSplitProductProxy.EnableDecompression = true;
                    objWSAutoSplitProductProxy.Timeout = 180000;
                }
                return objWSAutoSplitProductProxy;
            }
        }
        #endregion
        #region Phương thức

        #region CHia hàng từ đơn hàng
        /// <summary>
        /// Kiểm tra trạng thái duyệt đơn hàng
        /// POM_Order_Check
        /// </summary>
        public int CheckOrderReviewStatus(string strOrderID)
        {
            int intResult = -1;
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSAutoSplitProductProxy.CheckOrderReviewStatus(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref intResult, strOrderID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSAutoSplitProductProxy.CheckOrderReviewStatus(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref intResult, strOrderID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra đơn hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return intResult;
        }

        /// <summary>
        /// Lấy thông tin số lượng bán bình quân - Chia hàng từ 1 đơn hàng
        /// PM_AutoSplitProduct_LstAveSale
        /// </summary>
        public DataTable GetAverageSale(string strOrderID, int intNumDay, string strAreaIDList, string strStoreIDList)
        {
            DataTable dtbData = null;
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSAutoSplitProductProxy.GetAverageSale(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, strOrderID, intNumDay, strAreaIDList, strStoreIDList);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSAutoSplitProductProxy.GetAverageSale(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, strOrderID, intNumDay, strAreaIDList, strStoreIDList);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thông tin số lượng bán bình quân -Chia hàng từ 1 đơn hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }

        /// <summary>
        /// Chia hàng từ đơn hàng
        /// PM_Rpt_InStock_ListByOrder
        /// </summary>
        /// <param name="objKeywords"></param>
        /// <returns></returns>
        public DataTable GetInStockByOrder(string strOrderID, string strAreaIDList, string strStoreIDList,int intIsCheckRealInput)
        {
            DataTable dtbDataResult = null;
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSAutoSplitProductProxy.GetInStockByOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, strOrderID, strAreaIDList, strStoreIDList, intIsCheckRealInput);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSAutoSplitProductProxy.GetInStockByOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, strOrderID, strAreaIDList, strStoreIDList, intIsCheckRealInput);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Chia hàng từ đơn hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDataResult;
        }

        /// <summary>
        /// Lấy danh sách sản phẩm từ đơn hàng tham gia vào chia hàng tự động
        /// POM_ORDERDETAIL_SPLITPRODUCT
        /// </summary>
        /// <param name="strOrderID"></param>
        /// <returns></returns>
        public DataTable GetProductListByOrderID(string strOrderID)
        {
            DataTable dtbDataResult = null;
            try
            {
                objResultMessage = WSAutoSplitProductProxy.GetProductListByOrderID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, strOrderID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSAutoSplitProductProxy.GetProductListByOrderID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, strOrderID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Lấy danh sách sản phẩm từ đơn hàng tham gia vào chia hàng tự động";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDataResult;
        } 
        #endregion

        #region Chia hàng tự động từ các kho

        /// <summary>
        /// lấy dữ liệu tồn kho theo kho
        /// RPT_ReportStoreInStock_GetList
        /// </summary>
        public DataTable GetStoreInStock(object[] objKeywords)
        {
            DataTable dtbData = null;
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSAutoSplitProductProxy.GetStoreInStock(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSAutoSplitProductProxy.GetStoreInStock(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy dữ liệu tồn kho theo kho chia hàng tự động từ các kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }

        /// <summary>
        /// lấy dữ liệu nhập theo xuất chuyển kho
        /// PM_INPUTVOUCHER_GETSTOCHANGE
        /// </summary>
        public DataTable GetInputVoucherByStorechange(object[] objKeywords)
        {
            DataTable dtbData = null;
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSAutoSplitProductProxy.GetInputVoucherByStorechange(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSAutoSplitProductProxy.GetInputVoucherByStorechange(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy dữ liệu nhập theo xuất chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }


        /// <summary>
        /// Tính số lượng bán trung bình theo kho
        /// PM_RPT_STOREINSTOCK_AVGSALE
        /// </summary>
        public DataTable GetAvgSaleStoreInStock(object[] objKeywords)
        {
            DataTable dtbData = null;
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSAutoSplitProductProxy.GetAvgSaleStoreInStock(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSAutoSplitProductProxy.GetAvgSaleStoreInStock(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi  Tính số lượng bán trung bình theo kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }

        /// <summary>
        /// nạp số lượng đã tạo lệnh chuyển kho, tạo yêu cầu
        /// GETAUTOSTORECHANGEQUANTITY
        /// </summary>
        public DataTable GetAutoStoreChangeQuantity(string strPruductIDList, string strFromStoreIDList, string strToStoreIDList)
        {
            DataTable dtbData = null;
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSAutoSplitProductProxy.GetAutoStoreChangeQuantity(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, strPruductIDList,strFromStoreIDList,strToStoreIDList);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSAutoSplitProductProxy.GetAutoStoreChangeQuantity(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, strPruductIDList, strFromStoreIDList, strToStoreIDList);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp số lượng đã tạo lệnh chuyển kho, tạo yêu cầu";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }
        #endregion

        #endregion
    }
}
