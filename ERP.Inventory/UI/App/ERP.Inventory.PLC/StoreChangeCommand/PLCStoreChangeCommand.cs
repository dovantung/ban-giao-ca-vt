﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;
using ERP.Inventory.PLC.StoreChangeCommand.WSStoreChangeCommand;
using System.ComponentModel;

namespace ERP.Inventory.PLC.StoreChangeCommand
{
    public class PLCStoreChangeCommand
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/StoreChangeCommand/WSStoreChangeCommand.asmx";
        private StoreChangeCommand.WSStoreChangeCommand.WSStoreChangeCommand objWSStoreChangeCommandProxy = null;
        private ResultMessage objResultMessage = new ResultMessage();
        #endregion
        #region Property
        private WSStoreChangeCommand.WSStoreChangeCommand WSStoreChangeCommandProxy
        {
            get
            {
                if (objWSStoreChangeCommandProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSStoreChangeCommandProxy = new WSStoreChangeCommand.WSStoreChangeCommand();
                    objWSStoreChangeCommandProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSStoreChangeCommandProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSStoreChangeCommandProxy.EnableDecompression = true;
                    objWSStoreChangeCommandProxy.Timeout = 180000;
                }
                return objWSStoreChangeCommandProxy;
            }
        }
        #endregion
        #region Phương thức
        /// <summary>
        /// thêm danh sách lệnh chuyển kho từ đơn hàng
        /// PM_StoreChangeCommand_CreateID
        /// PM_STORECHANGECOMMAND_ADD
        /// PM_STORECHANGECOMMANDDETAIL_ADD
        /// </summary>
        /// <param name="strOrderID"></param>
        /// <returns></returns>
        public bool InsertMulti(List<WSStoreChangeCommand.StoreChangeCommand> objStoreChangeCommandList)
        {
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeCommandProxy.InsertMulti(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeCommandProxy.InsertMulti(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm danh sách lệnh chuyển kho từ đơn hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool DeleteMulti(List<WSStoreChangeCommand.StoreChangeCommand> objStoreChangeCommandList)
        {
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeCommandProxy.DeleteMulti(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeCommandProxy.DeleteMulti(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.Delete;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi xóa danh sách lệnh chuyển kho từ đơn hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }


        /// <summary>
        /// cập nhật ghi chú danh sách lệnh chuyển kho từ đơn hàng
        /// </summary>
        /// <param name="strOrderID"></param>
        /// <returns></returns>
        public bool UpdateNote(List<WSStoreChangeCommand.StoreChangeCommand> objStoreChangeCommandList)
        {
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeCommandProxy.UpdateNote(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeCommandProxy.UpdateNote(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật ghi chú danh sách lệnh chuyển kho từ đơn hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }
        /// <summary>
        /// Tìm kiếm dữ liệu 
        /// PM_STORECHANGECOMMAND_SRH
        /// </summary>
        /// <param name="objKeywords"></param>
        /// <returns></returns>
        public DataTable SearchDataTranCompany(object[] objKeywords)
        {
            DataTable dtbDataResult = null;
            try
            {
                objResultMessage = WSStoreChangeCommandProxy.SearchDataTranCompany(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeCommandProxy.SearchDataTranCompany(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tìm kiếm Phương tiện vận chuyển";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDataResult;
        }
        /// <summary>
        /// Tìm kiếm dữ liệu 
        /// PM_STORECHANGECOMMAND_SRH
        /// </summary>
        /// <param name="objKeywords"></param>
        /// <returns></returns>
        public DataTable SearchData(object[] objKeywords)
        {
            DataTable dtbDataResult = null;
            try
            {
                objResultMessage = WSStoreChangeCommandProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeCommandProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tìm kiếm lệnh chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDataResult;
        }

        /// 12/09/2017: Đăng bổ sung 
        /// <summary>
        /// Tìm kiếm dữ liệu STORECHANGEORDERTYPE theo lệnh chuyển kho
        /// MD_STORECHANGEORDERTYPE_SRHCUSTOM
        /// </summary>
        /// <param name="objKeywords"></param>
        /// <returns></returns>
        public DataTable SearchDataStoreChangeOrderType(object[] objKeywords)
        {
            DataTable dtbDataResult = null;
            try
            {
                objResultMessage = WSStoreChangeCommandProxy.SearchDataStoreChangeOrderType(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeCommandProxy.SearchDataStoreChangeOrderType(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tìm kiếm loại yêu cầu chuyển chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDataResult;
        }

        /// 13/09/2017: Đăng bổ sung 
        /// <summary>
        /// thêm danh sách lệnh chuyển kho từ đơn hàng
        /// </summary>
        /// <param name="strOrderID"></param>
        /// <returns></returns>
        public bool InsertMultiAndStoreChangeOrder(List<WSStoreChangeCommand.StoreChangeCommand> objStoreChangeCommandList, StoreChangeOrder objStoreChangeOrder, DataTable tblFromStore)
        {
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeCommandProxy.InsertMultiAndStoreChangeOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray(), objStoreChangeOrder, tblFromStore);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeCommandProxy.InsertMultiAndStoreChangeOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray(), objStoreChangeOrder, tblFromStore);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm danh sách lệnh chuyển kho từ đơn hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool InsertMultiAndStoreChangeOrder2(List<WSStoreChangeCommand.StoreChangeCommand> objStoreChangeCommandList, StoreChangeOrder objStoreChangeOrder, DataTable tblFromStore, DataTable tblListIMEI)
        {
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeCommandProxy.InsertMultiAndStoreChangeOrder2(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray(), objStoreChangeOrder, tblFromStore, tblListIMEI);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeCommandProxy.InsertMultiAndStoreChangeOrder2(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray(), objStoreChangeOrder, tblFromStore, tblListIMEI);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm danh sách lệnh chuyển kho từ đơn hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }


        public bool InsertMultiAndStoreChangeOrder3(List<WSStoreChangeCommand.StoreChangeCommand> objStoreChangeCommandList, StoreChangeOrder objStoreChangeOrder, DataTable tblFromStore, DataTable tblListIMEI, DataTable tblNote)
        {
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeCommandProxy.InsertMultiAndStoreChangeOrder3(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray(), objStoreChangeOrder, tblFromStore, tblListIMEI, tblNote);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeCommandProxy.InsertMultiAndStoreChangeOrder3(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray(), objStoreChangeOrder, tblFromStore, tblListIMEI, tblNote);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm danh sách lệnh chuyển kho từ đơn hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool InsertMultiAndStoreChangeOrder4(List<WSStoreChangeCommand.StoreChangeCommand> objStoreChangeCommandList, StoreChangeOrder objStoreChangeOrder, DataTable tblFromStore, DataTable tblListIMEI)
        {
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeCommandProxy.InsertMultiAndStoreChangeOrder4(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray(), objStoreChangeOrder, tblFromStore, tblListIMEI);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeCommandProxy.InsertMultiAndStoreChangeOrder4(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray(), objStoreChangeOrder, tblFromStore, tblListIMEI);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm danh sách lệnh chuyển kho từ đơn hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }
        public ResultMessage InsertMultiAndStoreChangeOrder5(List<WSStoreChangeCommand.StoreChangeCommand> objStoreChangeCommandList, StoreChangeOrder objStoreChangeOrder, DataTable tblFromStore, DataTable tblNote)
        {
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeCommandProxy.InsertMultiAndStoreChangeOrder5(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray(), objStoreChangeOrder, tblFromStore, tblNote);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeCommandProxy.InsertMultiAndStoreChangeOrder5(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeCommandList.ToArray(), objStoreChangeOrder, tblFromStore, tblNote);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm danh sách lệnh chuyển kho từ đơn hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }
        public ResultMessage InsertAutoStoreChange(List<WSStoreChangeCommand.AutoStoreChangeContext> objAutoStoreChangeContext, int createdStoreID, bool isAutoCreatedOrder, ref List<StoreChangeOrderItemFailure> failedItem)
        {
            StoreChangeOrderItemFailure[] listFailed = null;
            failedItem = new List<StoreChangeOrderItemFailure>();
            objResultMessage = new ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeCommandProxy.InsertAutoStoreChange(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objAutoStoreChangeContext.ToArray(), createdStoreID, isAutoCreatedOrder, ref listFailed);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeCommandProxy.InsertAutoStoreChange(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                   PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objAutoStoreChangeContext.ToArray(), createdStoreID, isAutoCreatedOrder, ref listFailed);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tạo lệnh chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            if (listFailed != null)
                failedItem = listFailed.ToList();

            return objResultMessage;
        }
        public string GetStoreChangeCommandNewID(int intStoreID)
        {
            string strStoreChangeOrderID = string.Empty;
            try
            {
                objResultMessage = WSStoreChangeCommandProxy.GetStoreChangeCommandNewID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strStoreChangeOrderID, intStoreID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeCommandProxy.GetStoreChangeCommandNewID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strStoreChangeOrderID, intStoreID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy mã lệnh yêu cầu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strStoreChangeOrderID;
        }

        #endregion
    }
}


namespace ERP.Inventory.PLC.StoreChangeCommand.WSStoreChangeCommand
{
    public partial class AutoStoreChangeContext : ImportResult
    {
        public AutoStoreChangeContext()
        {
            IsValidInput = false;
        }
        public string FromStoreName { get; set; }
        public string ToStoreName { get; set; }
        public void SetMasterIsUrgentByDetail()
        {
            if (this.Detail != null && this.Detail.Any(d => d.IsUrgent))
            {
                this.IsUrgent = true;
            }
        }
        public bool IsAnyDetailSelected
        {
            get
            {
                return (!IsEmptyDetail && this.Detail.Any(d => d.IsSelected));
            }
        }
        public bool IsEmptyDetail
        {
            get
            {
                return (this.Detail == null || !this.Detail.Any());
            }
        }
        public void RemoveDetailIsSelected(bool isOnlyValidConsignmentType = false)
        {
            if (this.Detail != null && this.Detail.Any())
            {
                if (this.IsValidInput && this.IsSent)
                {
                    var details = this.Detail.ToList();
                    details.RemoveAll(d => d.IsSelected && ((isOnlyValidConsignmentType && d.IsValidConsignmentType) || !isOnlyValidConsignmentType));
                    this.Detail = details.ToArray();
                }
            }
        }
        public bool IsValidInput { get; set; }
        public bool IsSent { get; set; }
    }
    public partial class AutoStoreChangeContextDetail : ImportResultDetail
    {
        private const bool DEFAULT_ISURGENT = false;
        private const bool DEFAULT_ISSELECTED = true;
        public AutoStoreChangeContextDetail()
        {
            IsSelected = DEFAULT_ISSELECTED;
            IsUrgent = DEFAULT_ISURGENT;
            IsValidConsignmentType = false;
        }
        public bool IsUrgent { get; set; }
        public string ProductName { get; set; }
        public decimal FromInStockQuantity { get; set; }
        public decimal ToInStockQuantity { get; set; }
        public int IsFIFOError { get; set; }

    }

    public class ImportResult
    {
        public string ImportFromStoreID { get; set; }
        public string ImportToStoreID { get; set; }
        public string ImportProductStateID { get; set; }
        public string ImportProductStateName { get; set; }

    }
    public class ImportResultDetail
    {
        //Số lượng người dùng đưa vào
        public string ImportQuantity { get; set; }
        //Số lượng hệ thống tự động chia lại
        public decimal AutoQuantity { get; set; }

        public string ImportMessage { get; set; }
        public string ImportStatusText
        {
            get
            {
                switch (ImportStatus)
                {
                    case EImportResult.IsValid: { return "Hợp lệ"; }
                    case EImportResult.Invalid: { return "Không hợp lệ"; }
                    case EImportResult.Warning: { return "Cảnh báo"; }
                    default: { return "Không xác định"; }
                }
            }
        }
        private EImportResult _ImportStatus { get; set; }
        public EImportResult ImportStatus
        {
            get
            {
                //if (_ImportStatus == null)
                    //_ImportStatus = EImportResult.Invalid;
                return _ImportStatus;
            }
            set { _ImportStatus = value; }
        }
    }
    public enum EImportResult
    {
        IsValid = 0,
        Invalid = 1,
        Warning = 2
    }
}

