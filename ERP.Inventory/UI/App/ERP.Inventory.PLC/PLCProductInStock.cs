﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.AppCore;
using System.Data;

namespace ERP.Inventory.PLC
{
    public class PLCProductInStock
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/WSProductInStock.asmx";
        private WSProductInStock.WSProductInStock objWSProductInStockProxy = null;
        private WSProductInStock.ResultMessage objResultMessage = new WSProductInStock.ResultMessage();
        #endregion
        #region Property
        private WSProductInStock.WSProductInStock WSProductInStockProxy
        {
            get
            {
                if (objWSProductInStockProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSProductInStockProxy = new WSProductInStock.WSProductInStock();
                    objWSProductInStockProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    //objWSProductInStockProxy.Url = "http://localhost:3124/Inventory/WSProductInStock.asmx";
                    objWSProductInStockProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSProductInStockProxy.EnableDecompression = true;
                    objWSProductInStockProxy.Timeout = 180000;
                }
                return objWSProductInStockProxy;
            }
        }
        #endregion
        #region Constructor
        public PLCProductInStock()
        {

        }
        #endregion
        #region Public Methods
        /// <summary>
        /// Lấy thông tin sản phẩm tồn kho
        /// </summary>
        /// <param name="strBarcode">Mã SP, hoặc IMEI</param>
        /// <param name="intStoreID">Kho</param>
        /// <returns></returns>
        public WSProductInStock.ProductInStock LoadInfo(string strBarcode, int intStoreID)
        {
            return LoadInfo(strBarcode, intStoreID, 0, string.Empty, false);
        }
        public WSProductInStock.ProductInStock LoadInfo(string strBarcode, int intStoreID, int InStockStatusID)
        {
            return LoadInfo(strBarcode, intStoreID, 0, string.Empty, false, InStockStatusID);
        }
        /// <summary>
        /// Lấy thông tin sản phẩm tồn kho
        /// </summary>
        /// <param name="strBarcode">Mã SP, hoặc IMEI</param>
        /// <param name="intStoreID">Kho</param>
        /// <param name="intOutputTypeID">Hình thức xuất</param>
        /// <param name="strAutoGetIMEIList">DS IMEI đã lấy</param>
        /// <returns></returns>
        public WSProductInStock.ProductInStock LoadInfo(string strBarcode, int intStoreID, int intOutputTypeID, string strAutoGetIMEIList)
        {
            return LoadInfo(strBarcode, intStoreID, intOutputTypeID, strAutoGetIMEIList, true);
        }
        /// <summary>
        /// Lấy thông tin sản phẩm tồn kho
        /// </summary>
        /// <param name="strBarcode">Mã SP, hoặc IMEI</param>
        /// <param name="intStoreID">Kho</param>
        /// <param name="intOutputTypeID">Hình thức xuất</param>
        /// <param name="strAutoGetIMEIList">DS IMEI đã lấy</param>
        /// <param name="bolIsAutoGetIMEI">Có tự động lấy IMEI hay không</param>
        /// <returns></returns>
        public WSProductInStock.ProductInStock LoadInfo(string strBarcode, int intStoreID, int intOutputTypeID, string strAutoGetIMEIList, bool bolIsAutoGetIMEI)
        {
            WSProductInStock.ProductInStock objProductInStock = null;
            try
            {
                objResultMessage = WSProductInStockProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductInStock, strBarcode, intStoreID, intOutputTypeID,
                    strAutoGetIMEIList, bolIsAutoGetIMEI);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductInStock, strBarcode, intStoreID, intOutputTypeID,
                        strAutoGetIMEIList, bolIsAutoGetIMEI);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin sản phẩm tồn kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objProductInStock;
        }
        /// <summary>
        /// Lấy thông tin sản phẩm tồn kho theo trạng thái sản phẩm
        /// </summary>
        /// <param name="strBarcode">Mã SP, hoặc IMEI</param>
        /// <param name="intStoreID">Kho</param>
        /// <param name="intOutputTypeID">Hình thức xuất</param>
        /// <param name="strAutoGetIMEIList">DS IMEI đã lấy</param>
        /// <param name="bolIsAutoGetIMEI">Có tự động lấy IMEI hay không</param>
        /// <returns></returns>
        public WSProductInStock.ProductInStock LoadInfo(string strBarcode, int intStoreID, int intOutputTypeID,
            string strAutoGetIMEIList, bool bolIsAutoGetIMEI, int intInstockStatusID)
        {
            WSProductInStock.ProductInStock objProductInStock = null;
            try
            {
                objResultMessage = WSProductInStockProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductInStock, strBarcode, intStoreID, intOutputTypeID,
                    strAutoGetIMEIList, bolIsAutoGetIMEI, intInstockStatusID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductInStock, strBarcode, intStoreID, intOutputTypeID,
                        strAutoGetIMEIList, bolIsAutoGetIMEI, intInstockStatusID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin sản phẩm tồn kho theo trạng thái sản phẩm";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objProductInStock;
        }
        /// <summary>
        /// Lấy thông tin sản phẩm tồn kho theo đơn hàng-NCC
        /// </summary>
        /// <param name="strBarcode">Mã SP, hoặc IMEI</param>
        /// <param name="intStoreID">Kho</param>
        /// <param name="intOutputTypeID">Hình thức xuất</param>
        /// <param name="strAutoGetIMEIList">DS IMEI đã lấy</param>
        /// <param name="bolIsAutoGetIMEI">Có tự động lấy IMEI hay không</param>
        /// <returns></returns>
        public WSProductInStock.ProductInStock LoadInfo(string strBarcode, int intStoreID, int intOutputTypeID,
            string strAutoGetIMEIList, bool bolIsAutoGetIMEI, int intInstockStatusID, int intCustomerID)
        {
            WSProductInStock.ProductInStock objProductInStock = null;
            try
            {
                objResultMessage = WSProductInStockProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductInStock, strBarcode, intStoreID, intOutputTypeID,
                    strAutoGetIMEIList, bolIsAutoGetIMEI, intInstockStatusID, intCustomerID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductInStock, strBarcode, intStoreID, intOutputTypeID,
                        strAutoGetIMEIList, bolIsAutoGetIMEI, intInstockStatusID, intCustomerID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin sản phẩm tồn kho theo trạng thái sản phẩm";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objProductInStock;
        }

        #region Tuan Anh: Bo sung ban cap nhap trang thai
        /// <summary>
        /// Lấy thông tin sản phẩm tồn kho
        /// </summary>
        /// <param name="strBarcode">Mã SP, hoặc IMEI</param>
        /// <param name="intStoreID">Kho</param>
        /// <returns></returns>
        public WSProductInStock.ProductInStock LoadInfo(string strBarcode, int intStoreID, bool IsUpdateOrdering)
        {
            return LoadInfo(strBarcode, intStoreID, 0, string.Empty, false, IsUpdateOrdering);
        }
        public WSProductInStock.ProductInStock LoadInfo(string strBarcode, int intStoreID, int InStockStatusID, bool IsUpdateOrdering)
        {
            return LoadInfo(strBarcode, intStoreID, 0, string.Empty, false, InStockStatusID, IsUpdateOrdering);
        }
        /// <summary>
        /// Lấy thông tin sản phẩm tồn kho
        /// </summary>
        /// <param name="strBarcode">Mã SP, hoặc IMEI</param>
        /// <param name="intStoreID">Kho</param>
        /// <param name="intOutputTypeID">Hình thức xuất</param>
        /// <param name="strAutoGetIMEIList">DS IMEI đã lấy</param>
        /// <returns></returns>
        public WSProductInStock.ProductInStock LoadInfo(string strBarcode, int intStoreID, int intOutputTypeID,
            bool IsUpdateOrdering, string strAutoGetIMEIList)
        {
            return LoadInfo(strBarcode, intStoreID, intOutputTypeID, strAutoGetIMEIList, true, IsUpdateOrdering);
        }

        /// <summary>
        /// Lấy thông tin sản phẩm tồn kho
        /// </summary>
        /// <param name="strBarcode">Mã SP, hoặc IMEI</param>
        /// <param name="intStoreID">Kho</param>
        /// <param name="intOutputTypeID">Hình thức xuất</param>
        /// <param name="strAutoGetIMEIList">DS IMEI đã lấy</param>
        /// <param name="bolIsAutoGetIMEI">Có tự động lấy IMEI hay không</param>
        /// <returns></returns>
        public WSProductInStock.ProductInStock LoadInfo(string strBarcode, int intStoreID, int intOutputTypeID,
            string strAutoGetIMEIList, bool bolIsAutoGetIMEI, bool IsUpdateOrdering)
        {
            WSProductInStock.ProductInStock objProductInStock = null;
            try
            {
                objResultMessage = WSProductInStockProxy.LoadInfoAndUpdateIsOrdering(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductInStock, strBarcode, intStoreID, intOutputTypeID,
                    strAutoGetIMEIList, bolIsAutoGetIMEI, IsUpdateOrdering);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.LoadInfoAndUpdateIsOrdering(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductInStock, strBarcode, intStoreID, intOutputTypeID,
                        strAutoGetIMEIList, bolIsAutoGetIMEI, IsUpdateOrdering);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin sản phẩm tồn kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objProductInStock;
        }

        /// <summary>
        /// Lấy thông tin sản phẩm tồn kho theo trạng thái sản phẩm
        /// </summary>
        /// <param name="strBarcode">Mã SP, hoặc IMEI</param>
        /// <param name="intStoreID">Kho</param>
        /// <param name="intOutputTypeID">Hình thức xuất</param>
        /// <param name="strAutoGetIMEIList">DS IMEI đã lấy</param>
        /// <param name="bolIsAutoGetIMEI">Có tự động lấy IMEI hay không</param>
        /// <returns></returns>
        public WSProductInStock.ProductInStock LoadInfo(string strBarcode, int intStoreID, int intOutputTypeID,
            string strAutoGetIMEIList, bool bolIsAutoGetIMEI, int intInstockStatusID, bool IsUpdateOrdering)
        {
            WSProductInStock.ProductInStock objProductInStock = null;
            try
            {
                objResultMessage = WSProductInStockProxy.LoadInfoAndUpdateIsOrdering(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductInStock, strBarcode, intStoreID, intOutputTypeID,
                    strAutoGetIMEIList, bolIsAutoGetIMEI, intInstockStatusID, IsUpdateOrdering);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.LoadInfoAndUpdateIsOrdering(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductInStock, strBarcode, intStoreID, intOutputTypeID,
                        strAutoGetIMEIList, bolIsAutoGetIMEI, intInstockStatusID, IsUpdateOrdering);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin sản phẩm tồn kho theo trạng thái sản phẩm";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objProductInStock;
        }

        /// <summary>
        /// Lấy thông tin sản phẩm tồn kho theo đơn hàng-NCC
        /// </summary>
        /// <param name="strBarcode">Mã SP, hoặc IMEI</param>
        /// <param name="intStoreID">Kho</param>
        /// <param name="intOutputTypeID">Hình thức xuất</param>
        /// <param name="strAutoGetIMEIList">DS IMEI đã lấy</param>
        /// <param name="bolIsAutoGetIMEI">Có tự động lấy IMEI hay không</param>
        /// <returns></returns>
        public WSProductInStock.ProductInStock LoadInfo(string strBarcode, int intStoreID, int intOutputTypeID,
            string strAutoGetIMEIList, bool bolIsAutoGetIMEI, int intInstockStatusID, int intCustomerID, bool IsUpdateOrdering)
        {
            WSProductInStock.ProductInStock objProductInStock = null;
            try
            {
                objResultMessage = WSProductInStockProxy.LoadInfoAndUpdateIsOrdering(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductInStock, strBarcode, intStoreID, intOutputTypeID,
                    strAutoGetIMEIList, bolIsAutoGetIMEI, intInstockStatusID, intCustomerID, IsUpdateOrdering);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.LoadInfoAndUpdateIsOrdering(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductInStock, strBarcode, intStoreID, intOutputTypeID,
                        strAutoGetIMEIList, bolIsAutoGetIMEI, intInstockStatusID, intCustomerID, IsUpdateOrdering);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin sản phẩm tồn kho theo trạng thái sản phẩm";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objProductInStock;
        }

        #endregion













        public bool CheckIsExistIMEI(string strProductID, string strIMEI, int intStoreID, int intOutputTypeID)
        {
            return CheckIsExistIMEI(strProductID, strIMEI, intStoreID, intOutputTypeID, -1);
        }
        public bool CheckIsExistIMEI(string strProductID, string strIMEI, int intStoreID, int intOutputTypeID, bool bolIsNew)
        {
            return CheckIsExistIMEI(strProductID, strIMEI, intStoreID, intOutputTypeID, Convert.ToInt32(bolIsNew));
        }
        private bool CheckIsExistIMEI(string strProductID, string strIMEI, int intStoreID, int intOutputTypeID, int intIsNew)
        {
            //int intCheckDuplicateImeiAllProductValue = Library.AppCore.AppConfig.GetIntConfigValue("ISCHECKDUPLICATEIMEIALLPRODUCT");
            //int intCheckDuplicateImeiAllStoreValue = Library.AppCore.AppConfig.GetIntConfigValue("ISCHECKDUPLICATEIMEIALLSTORE");
            //if (intCheckDuplicateImeiAllProductValue > 0)
            //    strProductID = string.Empty;
            bool bolResult = false;
            try
            {
                objResultMessage = WSProductInStockProxy.CheckIMEIExists(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref bolResult, strProductID, strIMEI, intStoreID, intIsNew, intOutputTypeID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.CheckIMEIExists(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref bolResult, strProductID, strIMEI, intStoreID, intIsNew, intOutputTypeID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra tồn tại IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return bolResult;
        }

        /// <summary>
        /// ERP.PM_CURRENTINSTOCK_CHKIMEI
        ///* Application Config: ISCHECKDUPLICATEIMEIALLSTORE 
        ///    1: Kiểm tra IMEI trùng trên tất cả các kho  v_StoreID=-1
        ///    0: Chỉ kiểm tra kho hiện tại                v_StoreID > 0
        ///* Application Config: ISCHECKDUPLICATEIMEIALLPRODUCT   
        ///    1: Kiểm tra all                             v_ProductID=null
        ///    0: Chỉ kiểm tra trên 1 sp                   v_ProductID is not null
        /// </summary>
        public bool CheckIsExistIMEI(string strProductID, string strIMEI, int intStoreID, int intOutputTypeID, int intIsNew, bool bolCheckApplicationConfigProduct, bool bolCheckApplicationConfigStore)
        {
            int intCheckDuplicateImeiAllProductValue = Library.AppCore.AppConfig.GetIntConfigValue("ISCHECKDUPLICATEIMEIALLPRODUCT");
            int intCheckDuplicateImeiAllStoreValue = Library.AppCore.AppConfig.GetIntConfigValue("ISCHECKDUPLICATEIMEIALLSTORE");
            if (bolCheckApplicationConfigProduct)
            {
                if (intCheckDuplicateImeiAllProductValue > 0)
                    strProductID = string.Empty;
            }

            if (bolCheckApplicationConfigStore)
            {
                if (intCheckDuplicateImeiAllStoreValue > 0)
                    intStoreID = -1;
            }

            bool bolResult = false;
            try
            {
                objResultMessage = WSProductInStockProxy.CheckIMEIExists(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref bolResult, strProductID, strIMEI, intStoreID, intIsNew, intOutputTypeID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.CheckIMEIExists(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref bolResult, strProductID, strIMEI, intStoreID, intIsNew, intOutputTypeID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra tồn tại IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return bolResult;
        }

        public decimal GetQuantity(String strProductID, int intStoreID)
        {
            return GetQuantity(strProductID, intStoreID, -1, -1, -1);
        }

        public decimal GetQuantity(String strProductID, int intStoreID, int intIsNew)
        {
            return GetQuantity(strProductID, intStoreID, intIsNew, -1, -1);
        }

        public decimal GetQuantity(String strProductID, int intStoreID, int intIsNew, int intIsShowProduct, int intIsCheckRealInput)
        {
            WSProductInStock.ProductInStock objProductInStock = null;
            try
            {
                objResultMessage = WSProductInStockProxy.GetQuantity(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductInStock, strProductID, intStoreID, intIsNew, intIsShowProduct, intIsCheckRealInput);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.GetQuantity(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductInStock, strProductID, intStoreID, intIsNew, intIsShowProduct, intIsCheckRealInput);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin sản phẩm";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objProductInStock.Quantity;
        }

        public bool CheckAutoGenIMEI(string strProductID)
        {
            bool bolResult = false;
            try
            {
                objResultMessage = WSProductInStockProxy.CheckAutoGenIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref bolResult, strProductID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.CheckAutoGenIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref bolResult, strProductID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra có tự động lấy IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return bolResult;
        }

        public DataTable GetIMEIByQuantity(string strProductID, int intStoreID, int intQuantity)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSProductInStockProxy.GetIMEIByQuantity(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strProductID, intStoreID, intQuantity);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.GetIMEIByQuantity(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strProductID, intStoreID, intQuantity);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy IMEI theo số lượng cần";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;

        }

        public DataTable GetIMEIByQuantity(string strProductID, int intStoreID, int intQuantity, int intInStockStatusID)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSProductInStockProxy.GetIMEIByQuantity(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strProductID, intStoreID, intQuantity, intInStockStatusID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.GetIMEIByQuantity(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strProductID, intStoreID, intQuantity, intInStockStatusID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy IMEI theo số lượng cần";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;

        }

        public DataTable GetIMEIByQuantityCustom(object[] objKeywords)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSProductInStockProxy.GetIMEIByQuantityCustom(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.GetIMEIByQuantityCustom(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy IMEI theo số lượng cần";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;

        }

        public string CheckIMEICustom(string strIMEI)
        {
            string strProductID = string.Empty;
            try
            {
                objResultMessage = WSProductInStockProxy.CheckIMEICustom(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strProductID, strIMEI);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.CheckIMEICustom(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strProductID, strIMEI);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi check IMEI theo Product";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strProductID;
        }

        public DataTable CheckIMEIInStock1(object[] objKeywords)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSProductInStockProxy.CheckIMEIInStock1(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.CheckIMEIInStock1(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }
        #endregion
        public bool Update_IsOrdering(String xmlImei, int inIsOrdering, int intStoreID)
        {
            try
            {
                objResultMessage = WSProductInStockProxy.Update_IsOrderingByString(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, xmlImei, inIsOrdering, intStoreID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.Update_IsOrderingByString(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, xmlImei, inIsOrdering, intStoreID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật trạng thái đặt hàng tồn kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool Update_IsOrdering(DataTable dtbImei, int inIsOrdering, int intStoreID)
        {
            dtbImei.TableName = "IMEI";
            try
            {
                objResultMessage = WSProductInStockProxy.Update_IsOrdering(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, dtbImei, inIsOrdering, intStoreID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.Update_IsOrdering(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, dtbImei, inIsOrdering, intStoreID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật trạng thái đặt hàng tồn kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool Update_IsOrderingWithIMEI(string strImei, int inIsOrdering, int intStoreID)
        {
            DataTable dtbImeiPara = new DataTable();
            dtbImeiPara.Columns.Add("IMEI");
            dtbImeiPara.Rows.Add(strImei);
            dtbImeiPara.TableName = "IMEI";
            try
            {
                objResultMessage = WSProductInStockProxy.Update_IsOrdering(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, dtbImeiPara, inIsOrdering, intStoreID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.Update_IsOrdering(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, dtbImeiPara, inIsOrdering, intStoreID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật trạng thái đặt hàng tồn kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public DataTable GetFIFOList_ByProductID(string strProductID, string strIMEI, int intStoreID, int intOutputTypeID, int intMaxIMEICanShow = 4)
        {
            DataTable dtbFIFO = null;
            try
            {
                objResultMessage = WSProductInStockProxy.GetFIFOList_ByProductID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbFIFO, strProductID, strIMEI, intStoreID, intOutputTypeID, intMaxIMEICanShow);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.GetFIFOList_ByProductID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbFIFO, strProductID, strIMEI, intStoreID, intOutputTypeID, intMaxIMEICanShow);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy thông tin danh sách FIFO";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbFIFO;
        }

        public DataTable GetFIFOList_ByIMEI(String xmlImei, string strProductID, int intStoreID, int intOutputTypeID, int intMaxIMEICanShow = 4)
        {
            DataTable dtbFIFO = null;
            try
            {
                objResultMessage = WSProductInStockProxy.GetFIFOList_ByIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbFIFO, xmlImei, strProductID, intStoreID, intOutputTypeID, intMaxIMEICanShow);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.GetFIFOList_ByIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbFIFO, xmlImei, strProductID, intStoreID, intOutputTypeID, intMaxIMEICanShow);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy thông tin danh sách FIFO";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbFIFO;
        }

        public DataTable GetFIFOList_ByIMEI(DataTable dtbImei, string strProductID, int intStoreID, int intOutputTypeID, int intMaxIMEICanShow = 4)
        {
            DataTable dtbFIFO = null;
            dtbImei.TableName = "IMEI";
            try
            {
                objResultMessage = WSProductInStockProxy.GetFIFOList_ByIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbFIFO, dtbImei, strProductID, intStoreID, intOutputTypeID, intMaxIMEICanShow);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductInStockProxy.GetFIFOList_ByIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbFIFO, dtbImei, strProductID, intStoreID, intOutputTypeID, intMaxIMEICanShow);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductInStock.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật trạng thái đặt hàng tồn kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbFIFO;
        }

    }
}
