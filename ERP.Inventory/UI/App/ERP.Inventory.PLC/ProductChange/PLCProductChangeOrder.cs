﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;

namespace ERP.Inventory.PLC.ProductChange
{
    public class PLCProductChangeOrder
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/ProductChange/WSProductChangeOrder.asmx";
        private WSProductChangeOrder.WSProductChangeOrder objWSProductChangeOrderProxy = null;
        private WSProductChangeOrder.ResultMessage objResultMessage = new WSProductChangeOrder.ResultMessage();
        private ResultMessageApp objResultMessageApp = new ResultMessageApp();

        #endregion
        #region Property
        private WSProductChangeOrder.WSProductChangeOrder WSProductChangeOrderProxy
        {
            get
            {
                if (objWSProductChangeOrderProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSProductChangeOrderProxy = new WSProductChangeOrder.WSProductChangeOrder();
                    objWSProductChangeOrderProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSProductChangeOrderProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSProductChangeOrderProxy.EnableDecompression = true;
                    objWSProductChangeOrderProxy.Timeout = 180000;
                }
                return objWSProductChangeOrderProxy;
            }
        }

        public ResultMessageApp ResultMessageApp
        {
            get { return objResultMessageApp; }
            set { objResultMessageApp = value; }
        }
        #endregion
        #region Constructor
        public PLCProductChangeOrder()
        {

        }
        #endregion
        #region Phương thức

        public WSProductChangeOrder.ResultMessage LoadInfo(ref WSProductChangeOrder.ProductChangeOrder objProductChangeOrder, string strProductChangeOrderId)
        {
            WSProductChangeOrder.ResultMessage objResultMessage = new WSProductChangeOrder.ResultMessage();
            try
            {
                objResultMessage = WSProductChangeOrderProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductChangeOrder, strProductChangeOrderId);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductChangeOrderProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductChangeOrder, strProductChangeOrderId);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductChangeOrder.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin yêu cầu xuất đổi";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }

        public WSProductChangeOrder.ResultMessage CheckExistIMEI(ref string IMEIExist, List<WSProductChangeOrder.ProductChangeOrderDT> lstProductChangeOrderDT)
        {
            try
            {
                objResultMessage = WSProductChangeOrderProxy.CheckExistIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref IMEIExist, lstProductChangeOrderDT.ToArray());
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductChangeOrderProxy.CheckExistIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref IMEIExist, lstProductChangeOrderDT.ToArray());
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductChangeOrder.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra tồn kho IMEI-sản phẩm";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }

        public WSProductChangeOrder.ResultMessage CheckExistIMEISingle(ref string IMEIExist, string IMEI, string ProductID)
        {
            try
            {
                objResultMessage = WSProductChangeOrderProxy.CheckExistIMEISingle(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref IMEIExist, IMEI, ProductID);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductChangeOrderProxy.CheckExistIMEISingle(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref IMEIExist, IMEI, ProductID);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductChangeOrder.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra tồn kho IMEI-sản phẩm";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }
        public WSProductChangeOrder.ResultMessage SearchData(ref DataTable dtbResult, params object[] objKeywords)
        {
            WSProductChangeOrder.ResultMessage objResultMessage = new WSProductChangeOrder.ResultMessage();
            try
            {
                objResultMessage = WSProductChangeOrderProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductChangeOrderProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductChangeOrder.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách yêu cầu xuất đổi";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }
        public WSProductChangeOrder.ResultMessage Insert(WSProductChangeOrder.ProductChangeOrder objProductChangeOrder)
        {
            WSProductChangeOrder.ResultMessage objResultMessage = new WSProductChangeOrder.ResultMessage();
            try
            {
                objResultMessage = WSProductChangeOrderProxy.InsertProductChangeOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objProductChangeOrder);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductChangeOrderProxy.InsertProductChangeOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objProductChangeOrder);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductChangeOrder.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin yêu cầu xuất đổi";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }
        public WSProductChangeOrder.ResultMessage Update(WSProductChangeOrder.ProductChangeOrder objProductChangeOrder)
        {
            WSProductChangeOrder.ResultMessage objResultMessage = new WSProductChangeOrder.ResultMessage();
            try
            {
                objResultMessage = WSProductChangeOrderProxy.UpdateProductChangeOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objProductChangeOrder);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductChangeOrderProxy.UpdateProductChangeOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objProductChangeOrder);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductChangeOrder.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật thông tin yêu cầu xuất đổi";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }

        public WSProductChangeOrder.ResultMessage Delete(WSProductChangeOrder.ProductChangeOrder objProductChangeOrder)
        {
            WSProductChangeOrder.ResultMessage objResultMessage = new WSProductChangeOrder.ResultMessage();
            try
            {
                objResultMessage = WSProductChangeOrderProxy.DeleteProductChangeOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objProductChangeOrder);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductChangeOrderProxy.DeleteProductChangeOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objProductChangeOrder);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductChangeOrder.ErrorTypes.Delete;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi hủy thông tin yêu cầu xuất đổi";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }
        public WSProductChangeOrder.ResultMessage Approve(string productChangeOrderId, int reviewLevelId, string userName, bool unApprove, string Reason)
        {
            WSProductChangeOrder.ResultMessage objResultMessage = new WSProductChangeOrder.ResultMessage();
            try
            {
                objResultMessage = WSProductChangeOrderProxy.ApproveProductChangeOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, productChangeOrderId, reviewLevelId, userName, unApprove, Reason);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductChangeOrderProxy.ApproveProductChangeOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, productChangeOrderId, reviewLevelId, userName, unApprove, Reason);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductChangeOrder.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi duyệt thông tin yêu cầu xuất đổi";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }
        public DataTable GetAttachment(object[] objKeywords)
        {
            DataTable dtbAttachment = null;
            try
            {
                objResultMessage = WSProductChangeOrderProxy.GetAttachment(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbAttachment, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductChangeOrderProxy.GetAttachment(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbAttachment, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách tập tin đính kèm yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbAttachment;
        }
        #endregion;
    }
}
