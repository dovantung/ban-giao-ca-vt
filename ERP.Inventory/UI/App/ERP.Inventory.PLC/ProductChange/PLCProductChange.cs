﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;

namespace ERP.Inventory.PLC.ProductChange
{
    public class PLCProductChange
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/ProductChange/WSProductChange.asmx";
        private WSProductChange.WSProductChange objWSProductChangeProxy = null;
        private WSProductChange.ResultMessage objResultMessage = new WSProductChange.ResultMessage();
        private ResultMessageApp objResultMessageApp = new ResultMessageApp();

        #endregion
        #region Property
        private WSProductChange.WSProductChange WSProductChangeProxy
        {
            get
            {
                if (objWSProductChangeProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSProductChangeProxy = new WSProductChange.WSProductChange();
                    objWSProductChangeProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSProductChangeProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSProductChangeProxy.EnableDecompression = true;
                    objWSProductChangeProxy.Timeout = 180000;
                }
                return objWSProductChangeProxy;
            }
        }

        public ResultMessageApp ResultMessageApp
        {
            get { return objResultMessageApp; }
            set { objResultMessageApp = value; }
        }
        #endregion
        #region Phương thức


        public WSProductChange.ProductChange LoadInfo(string strInputOrOutputVoucherID)
        {
            WSProductChange.ProductChange objProductChange = null;
            try
            {
                objResultMessage = WSProductChangeProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductChange, strInputOrOutputVoucherID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductChangeProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProductChange, strInputOrOutputVoucherID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductChange.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy thông tin đổi hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objProductChange;
        }

        public DataTable GetListStoreInStock(int intStoreID)
        {
            DataTable dtbStoreInStock = null;
            try
            {
                objResultMessage = WSProductChangeProxy.GetListStoreInStock(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbStoreInStock, intStoreID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductChangeProxy.GetListStoreInStock(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbStoreInStock, intStoreID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy thông tin tồn kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbStoreInStock;
        }

        public bool InsertMasterAndDetail(WSProductChange.ProductChange objProductChange)
        {
            try
            {
                objResultMessage = WSProductChangeProxy.InsertMasterAndDetail(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objProductChange);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductChangeProxy.InsertMasterAndDetail(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objProductChange);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductChange.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi xuất đổi hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }
        
        #endregion;
    }
}
