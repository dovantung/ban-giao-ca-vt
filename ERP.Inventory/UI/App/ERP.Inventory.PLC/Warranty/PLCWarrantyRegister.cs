﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;
using WSWarrantyRegister= ERP.Inventory.PLC.Warranty.WSWarrantyRegister;

namespace ERP.Inventory.PLC.Warranty
{
    public class PLCWarrantyRegister
    {
        #region Variable
        private const String WS_CLASS_PATH = "Warranty/WSWarrantyRegister.asmx";
        private WSWarrantyRegister.WSWarrantyRegister objWSWarrantyRegisterProxy = null;
        private WSWarrantyRegister.ResultMessage objResultMessage = new WSWarrantyRegister.ResultMessage();
        #endregion
        #region Property
        private WSWarrantyRegister.WSWarrantyRegister WSWarrantyRegisterProxy
        {
            get
            {
                if (objWSWarrantyRegisterProxy== null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSWarrantyRegisterProxy = new WSWarrantyRegister.WSWarrantyRegister();
                    objWSWarrantyRegisterProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSWarrantyRegisterProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSWarrantyRegisterProxy.EnableDecompression = true;
                    objWSWarrantyRegisterProxy.Timeout = 180000;
                }
                return objWSWarrantyRegisterProxy;
            }
        }
        #endregion
        #region Phương thức
     
        /// <summary>
        /// Tìm kiếm bảo hành đăng ký bảo hành
        /// PM_WARRANTYREGISTER_SRH
        /// </summary>
        public DataTable SearchData(params object[] objKeywords)
        {
            DataTable dtbData = null;
            try
            {
                //List<object> objKeywordsList = new List<object>();
                //object[] objNewKeyWords = new object[] { };
                //for (int i = 0; i < objKeywords.Length; i++)
                //{
                //    objKeywordsList.Add(objKeywords[i]);
                //}
                //objNewKeyWords = objKeywordsList.ToArray();
                objResultMessage = WSWarrantyRegisterProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData,objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSWarrantyRegisterProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSWarrantyRegister.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Tìm kiếm đăng ký bảo hành";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }
        #endregion
    }
}
