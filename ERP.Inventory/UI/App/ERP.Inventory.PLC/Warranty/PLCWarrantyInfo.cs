﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;
using WSWarrantyInfo= ERP.Inventory.PLC.Warranty.WSWarrantyInfo;

namespace ERP.Inventory.PLC.Warranty
{
    public class PLCWarrantyInfo
    {
        #region Variable
        private const String WS_CLASS_PATH = "Warranty/WSWarrantyInfo.asmx";
        private WSWarrantyInfo.WSWarrantyInfo objWSWarrantyInfoProxy = null;
        private WSWarrantyInfo.ResultMessage objResultMessage = new WSWarrantyInfo.ResultMessage();
        #endregion
        #region Property
        private WSWarrantyInfo.WSWarrantyInfo WSWarrantyInfoProxy
        {
            get
            {
                if (objWSWarrantyInfoProxy== null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSWarrantyInfoProxy = new WSWarrantyInfo.WSWarrantyInfo();
                    objWSWarrantyInfoProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSWarrantyInfoProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSWarrantyInfoProxy.EnableDecompression = true;
                    objWSWarrantyInfoProxy.Timeout = 180000;
                }
                return objWSWarrantyInfoProxy;
            }
        }
        #endregion
        #region Phương thức
     
        /// <summary>
        /// Tìm kiếm thông tin bảo hành
        /// PM_WARRANTYINFO_SRH
        /// </summary>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public DataTable SearchData(object[] objKeywords)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSWarrantyInfoProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData,objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSWarrantyInfoProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSWarrantyInfo.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Tìm kiếm thông tin bảo hành";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }

        /// <summary>
        /// Thêm mới thông tin bảo hành
        /// PM_WARRANTYINFO_IMPV2
        /// </summary>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public bool InsertWarrantyInfo(string strIMEI, string strOutputVoucherID)
        {
            try
            {
                objResultMessage = WSWarrantyInfoProxy.InsertWarrantyInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strIMEI, strOutputVoucherID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSWarrantyInfoProxy.InsertWarrantyInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strIMEI, strOutputVoucherID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSWarrantyInfo.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm mới thông tin bảo hành";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        /// <summary>
        /// Thêm mới thông tin bảo hành
        /// PM_WARRANTYINFO_IMPV2
        /// </summary>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public bool CalWarrantyTime(string strIMEI, string strProductID)
        {
            try
            {
                objResultMessage = WSWarrantyInfoProxy.CalWarrantyTime(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strIMEI, strProductID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSWarrantyInfoProxy.CalWarrantyTime(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strIMEI, strProductID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSWarrantyInfo.ErrorTypes.Others;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tính lại thời gian bảo hành";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }
        #endregion
    }
}
