﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;

namespace ERP.Inventory.PLC.InputChangeOrder
{
    public class PLCInputChangeOrder
    {
        #region Constant
        public enum ReviewStatus
        {
            INIT = 0,
            //INPROCESS = 1,
            CANCEL = 2,
            ACCEPT = 3
        }
        public enum TargetType
        {
            PERSONAL = 1,
            COLLECTIVE = 2
        }
        #endregion
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/InputChangeOrder/WSInputChangeOrder.asmx";
        private WSInputChangeOrder.WSInputChangeOrder objWSInputChangeOrderProxy = null;
        private WSInputChangeOrder.ResultMessage objResultMessage = new WSInputChangeOrder.ResultMessage();
        private ResultMessageApp objResultMessageApp = new ResultMessageApp();

        #endregion
        #region Property
        private WSInputChangeOrder.WSInputChangeOrder WSInputChangeOrderProxy
        {
            get
            {
                if (objWSInputChangeOrderProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSInputChangeOrderProxy = new WSInputChangeOrder.WSInputChangeOrder();
                    objWSInputChangeOrderProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSInputChangeOrderProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSInputChangeOrderProxy.EnableDecompression = true;
                    objWSInputChangeOrderProxy.Timeout = 180000;
                }
                return objWSInputChangeOrderProxy;
            }
        }

        public ResultMessageApp ResultMessageApp
        {
            get { return objResultMessageApp; }
            set { objResultMessageApp = value; }
        }
        #endregion
        #region Phương thức

        public bool Insert(WSInputChangeOrder.InputChangeOrder objInputChangeOrder, DataTable dtbMachineErrorIDList)
        {
            try
            {
                objResultMessage = WSInputChangeOrderProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder, dtbMachineErrorIDList);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder, dtbMachineErrorIDList);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool CreateInputVoucherAndOutputVoucherAndInOutVoucher(WSInputChangeOrder.InputChangeOrder objInputChangeOrder, WSInputChangeOrder.InputVoucher objInputVoucherBO, WSInputChangeOrder.OutputVoucher objOutputVoucherBO, WSInputChangeOrder.Voucher objInOutVoucher, WSInputChangeOrder.Voucher objVoucherForReturnFee,ref string[] arrVATInvoiceList)
        {
            try
            {
                objResultMessage = WSInputChangeOrderProxy.CreateInputVoucherAndOutputVoucherAndInOutVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder, objInputVoucherBO, objOutputVoucherBO, objVoucherForReturnFee, objInOutVoucher, ref arrVATInvoiceList);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.CreateInputVoucherAndOutputVoucherAndInOutVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder, objInputVoucherBO, objOutputVoucherBO, objVoucherForReturnFee, objInOutVoucher, ref arrVATInvoiceList);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tạo phiếu nhập, phiếu xuất và phiếu thu(hoặc chi) cho yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool CreateInputVoucherAndOutputVoucherAndInOutVoucherViettel(WSInputChangeOrder.InputChangeOrder objInputChangeOrder, WSInputChangeOrder.InputVoucher objInputVoucherBO, WSInputChangeOrder.OutputVoucher objOutputVoucherBO, WSInputChangeOrder.Voucher objInOutVoucher, ref string strInputVoucherReturnId, ref string[] arrVATInvoiceList)
        {
            try
            {
                objResultMessage = WSInputChangeOrderProxy.CreateInputVoucherAndOutputVoucherAndInOutVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder, objInputVoucherBO, objOutputVoucherBO, null, objInOutVoucher, ref arrVATInvoiceList);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.CreateInputVoucherAndOutputVoucherAndInOutVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder, objInputVoucherBO, objOutputVoucherBO, null, objInOutVoucher, ref arrVATInvoiceList);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tạo phiếu nhập, phiếu xuất và phiếu thu(hoặc chi) cho yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool CreateInputVoucherAndOutVoucher(WSInputChangeOrder.InputChangeOrder objInputChangeOrder, WSInputChangeOrder.InputVoucher objInputVoucherBO, WSInputChangeOrder.Voucher objOutVoucher, WSInputChangeOrder.Voucher objVoucherForSaleOrder, ref string[] arrVATInvoiceList)
        {
            try
            {
                objResultMessage = WSInputChangeOrderProxy.CreateInputVoucherAndOutVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder, objInputVoucherBO, objOutVoucher, objVoucherForSaleOrder,ref arrVATInvoiceList);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.CreateInputVoucherAndOutVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder, objInputVoucherBO, objOutVoucher, objVoucherForSaleOrder, ref arrVATInvoiceList);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tạo phiếu nhập, phiếu xuất và phiếu thu(hoặc chi) cho yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool CreateInputVoucherAndOutVoucherViettel(WSInputChangeOrder.InputChangeOrder objInputChangeOrder, WSInputChangeOrder.InputVoucher objInputVoucherBO, WSInputChangeOrder.Voucher objOutVoucher, WSInputChangeOrder.Voucher objVoucherForReturnFee, WSInputChangeOrder.Voucher objVoucherForSaleOrder, ref string strInputVoucherReturnId, ref string[] arrVATInvoiceList)
        {
            try
            {
                objResultMessage = WSInputChangeOrderProxy.CreateInputVoucherAndOutVoucherViettel(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder, objInputVoucherBO, objOutVoucher, objVoucherForReturnFee, objVoucherForSaleOrder, ref strInputVoucherReturnId, ref arrVATInvoiceList);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.CreateInputVoucherAndOutVoucherViettel(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder, objInputVoucherBO, objOutVoucher, objVoucherForReturnFee, objVoucherForSaleOrder, ref strInputVoucherReturnId, ref arrVATInvoiceList);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tạo phiếu nhập, phiếu xuất và phiếu thu(hoặc chi) cho yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objKeywords"></param>
        /// <returns></returns>
        public DataTable SearchData(object[] objKeywords)
        {
            DataTable dtbInputChangeOrder = null;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbInputChangeOrder, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbInputChangeOrder, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbInputChangeOrder;
        }
        /// <summary>
        /// PM_OutputVoucherDetail_Return
        /// </summary>
        /// <param name="strOutputVoucherID"></param>
        /// <param name="intInputChangeOrderTypeID"></param>
        /// <returns></returns>
        public DataTable LoadOutputVoucherDetailForReturn(string strOutputVoucherID, int intInputChangeOrderTypeID)
        {
            DataTable dtbOVDetailReturn = null;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.LoadOutputVoucherDetailForReturn(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strOutputVoucherID, intInputChangeOrderTypeID, ref dtbOVDetailReturn);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.LoadOutputVoucherDetailForReturn(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strOutputVoucherID, intInputChangeOrderTypeID, ref dtbOVDetailReturn);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy chi tiết phiếu xuất cho yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbOVDetailReturn;
        }

        public DataTable LoadInputChangeOrderType(string strOutputVoucherID, int intApplyErrorType)
        {
            DataTable dtbDataType = null;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.LoadInputChangeOrderType(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strOutputVoucherID, intApplyErrorType, ref dtbDataType);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.LoadInputChangeOrderType(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strOutputVoucherID, intApplyErrorType, ref dtbDataType);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách loại nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDataType;
        }

        public DataTable GetReviewList(object[] objKeywords)
        {
            DataTable dtbReviewList = null;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.GetReviewList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbReviewList, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.GetReviewList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbReviewList, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy mức duyệt yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbReviewList;
        }

        public DataTable GetReviewList(int intInputChangeOrderType)
        {
            DataTable dtbReviewList = null;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.GetReviewList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbReviewList, intInputChangeOrderType);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.GetReviewList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbReviewList, intInputChangeOrderType);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy mức duyệt yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbReviewList;
        }

        public WSInputChangeOrder.InputChangeOrder LoadInfo(string strInputChangeOrderID)
        {
            WSInputChangeOrder.InputChangeOrder objInputChangeOrder = null;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objInputChangeOrder, strInputChangeOrderID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objInputChangeOrder, strInputChangeOrderID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy thông tin yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objInputChangeOrder;
        }

        public bool UpdateReviewList(WSInputChangeOrder.InputChangeOrder_ReviewList objInputChangeOrder_ReviewList, int intCountReviewList)
        {
            try
            {
                objResultMessage = WSInputChangeOrderProxy.UpdateReviewList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder_ReviewList, intCountReviewList);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.UpdateReviewList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder_ReviewList, intCountReviewList);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật mức duyệt của yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool UpdateProcess(WSInputChangeOrder.InputChangeOrder_WorkFlow objInputChangeOrder_WorkFlow)
        {
            try
            {
                objResultMessage = WSInputChangeOrderProxy.UpdateProcess(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder_WorkFlow);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.UpdateProcess(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder_WorkFlow);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật bước xử lý của yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool InsertComment(ref string strCommentID, WSInputChangeOrder.InputChangeOrder_Comment objInputChangeOrder_Comment)
        {
            try
            {
                objResultMessage = WSInputChangeOrderProxy.InsertComment(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strCommentID, objInputChangeOrder_Comment);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.InsertComment(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strCommentID, objInputChangeOrder_Comment);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm bình luận yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool DeleteComment(WSInputChangeOrder.InputChangeOrder_Comment objInputChangeOrder_Comment)
        {
            try
            {
                objResultMessage = WSInputChangeOrderProxy.DeleteComment(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder_Comment);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.DeleteComment(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder_Comment);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.Delete;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi xóa bình luận yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public DataTable GetDetail(object[] objKeywords)
        {
            DataTable dtbDetail = null;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.GetDetail(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDetail, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.GetDetail(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDetail, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách chi tiết yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDetail;
        }

        public DataTable GetListReturn(string strOutputVoucherID, string strInputChangeOrderID)
        {
            DataTable dtbReturnDetail = null;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.GetListReturn(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbReturnDetail, strOutputVoucherID, strInputChangeOrderID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.GetListReturn(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbReturnDetail, strOutputVoucherID, strInputChangeOrderID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách chi tiết yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbReturnDetail;
        }

        public DataTable GetAttachment(object[] objKeywords)
        {
            DataTable dtbAttachment = null;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.GetAttachment(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbAttachment, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.GetAttachment(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbAttachment, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách tập tin đính kèm yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbAttachment;
        }

        public DataTable GetWorkFlow(object[] objKeywords)
        {
            DataTable dtbWorkFlow = null;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.GetWorkFlow(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbWorkFlow, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.GetWorkFlow(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbWorkFlow, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách qui trình yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbWorkFlow;
        }

        public DataTable GetWorkFlow(int intInputChangeOrderType)
        {
            DataTable dtbWorkFlow = null;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.GetWorkFlow(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbWorkFlow, intInputChangeOrderType);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.GetWorkFlow(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbWorkFlow, intInputChangeOrderType);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách qui trình yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbWorkFlow;
        }

        public DataTable GetComment(object[] objKeywords)
        {
            DataTable dtbComment = null;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.GetComment(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbComment, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.GetComment(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbComment, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách bình luận của yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbComment;
        }

        public bool Delete(string strInputChangeOrderID, string strContentDelete)
        {
            try
            {
                objResultMessage = WSInputChangeOrderProxy.Delete(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strInputChangeOrderID, strContentDelete);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.Delete(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strInputChangeOrderID, strContentDelete);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.Delete;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi xóa yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool Update(WSInputChangeOrder.InputChangeOrder objInputChangeOrder, DataTable dtbMachineErrorIDList)
        {
            try
            {
                objResultMessage = WSInputChangeOrderProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder, dtbMachineErrorIDList);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputChangeOrder, dtbMachineErrorIDList);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật yêu cầu nhập đổi/trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool CheckDelete(string strInputVoucherID, string strOutVoucherID)
        {
            bool bolIsDeleted = false;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.CheckDelete(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref bolIsDeleted, strInputVoucherID, strOutVoucherID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.CheckDelete(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref bolIsDeleted, strInputVoucherID, strOutVoucherID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra xóa chứng từ liên quan: phiếu nhập, phiếu chi";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return bolIsDeleted;
        }

        public int UpdateCare(string strOutputVoucherDetailID)
        {
            int intResult = 0;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.UpdateCare(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref intResult, strOutputVoucherDetailID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.UpdateCare(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref intResult, strOutputVoucherDetailID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi hỗ trợ trả Care";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return intResult;
        }

        //nam bổ sung

        public DataTable GetMachineError(string strInputChangeOrderID)
        {
            DataTable dtbMachineError = null;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.GetMachineError(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbMachineError, strInputChangeOrderID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.GetMachineError(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbMachineError, strInputChangeOrderID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách lỗi đổi trả";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbMachineError;
        }

        public DataTable GetEvictionIMEI(object[] objKeywords)
        {
            DataTable dtbEvictionIMEI = null;
            try
            {
                objResultMessage = WSInputChangeOrderProxy.GetEvictionIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbEvictionIMEI, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputChangeOrderProxy.GetEvictionIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbEvictionIMEI, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách IMEI thu hồi";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbEvictionIMEI;
        }
        #endregion;
    }
}
