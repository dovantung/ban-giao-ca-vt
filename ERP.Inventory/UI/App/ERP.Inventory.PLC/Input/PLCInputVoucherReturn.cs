﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace ERP.Inventory.PLC.Input
{
    public class PLCInputVoucherReturn
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/Input/WSInputVoucherReturn.asmx";
        private WSInputVoucherReturn.WSInputVoucherReturn objWSInputVoucherReturnProxy = null;
        private WSInputVoucherReturn.ResultMessage objResultMessage = new WSInputVoucherReturn.ResultMessage();
        private Library.AppCore.ResultMessageApp objResultMessageApp = new Library.AppCore.ResultMessageApp();

        public Library.AppCore.ResultMessageApp ResultMessageApp
        {
            get { return objResultMessageApp; }
            set { objResultMessageApp = value; }
        }
        #endregion
        #region Property
        private WSInputVoucherReturn.WSInputVoucherReturn WSInputVoucherReturnProxy
        {
            get
            {
                if (objWSInputVoucherReturnProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSInputVoucherReturnProxy = new WSInputVoucherReturn.WSInputVoucherReturn();
                    objWSInputVoucherReturnProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSInputVoucherReturnProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSInputVoucherReturnProxy.EnableDecompression = true;
                    objWSInputVoucherReturnProxy.Timeout = 180000;
                }
                return objWSInputVoucherReturnProxy;
            }
        }
        #endregion
        #region Phương thức
        public DataTable LoadOutputVoucherDetailForReturn(String strOutputVoucherID, bool bolIsReturnWithFee, bool bolIsAllowReturnAllProduct)
        {
            DataTable tblResult = null;
            string strInputVoucherID = string.Empty;
            try
            {
                objResultMessage = WSInputVoucherReturnProxy.LoadOutputVoucherDetailForReturn(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strOutputVoucherID, bolIsReturnWithFee, bolIsAllowReturnAllProduct, ref tblResult);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherReturnProxy.LoadOutputVoucherDetailForReturn(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strOutputVoucherID, bolIsReturnWithFee, bolIsAllowReturnAllProduct, ref tblResult);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucherReturn.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy chi tiết phiếu xuất để nhập trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return tblResult;
        }

        public DataTable LoadOutputVoucherWarrantyExtend(String strOutputVoucherID, String strIMEI)
        {
            DataTable tblResult = null;
            string strInputVoucherID = string.Empty;
            try
            {
                objResultMessage = WSInputVoucherReturnProxy.LoadOutputVoucherWarrantyExtend(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strOutputVoucherID, strIMEI, ref tblResult);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherReturnProxy.LoadOutputVoucherWarrantyExtend(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strOutputVoucherID, strIMEI, ref tblResult);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucherReturn.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy phiếu xuất bảo hành mở rộng của imei nhập trả";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return tblResult;
        }

        public void InsertMasterAndDetailAndVoucher(ERP.Inventory.PLC.Input.WSInputVoucherReturn.InputVoucherReturn objInputVoucherReturn, ERP.Inventory.PLC.Input.WSInputVoucherReturn.Voucher objVoucher)
        {
           
            string strInputVoucherID = string.Empty;
            try
            {
                objResultMessage = WSInputVoucherReturnProxy.InsertMasterAndDetailAndVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputVoucherReturn, objVoucher);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherReturnProxy.InsertMasterAndDetailAndVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputVoucherReturn, objVoucher);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucherReturn.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm phiếu nhập trả hàng và phiếu chi";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            
        }

        public string CheckInputVoucherReturnByOutputVoucher(string strOutputVoucherID)
        {
            string strInputVoucherReturnID = string.Empty;
            try
            {
                objResultMessage = WSInputVoucherReturnProxy.CheckInputVoucherReturnByOutputVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strInputVoucherReturnID, strOutputVoucherID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherReturnProxy.CheckInputVoucherReturnByOutputVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strInputVoucherReturnID, strOutputVoucherID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucherReturn.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra phiếu nhập trả hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strInputVoucherReturnID;
        }

        public string GetVoucherAlert(String strSaleOrderID)
        {
            string strVoucherAlert = string.Empty;
            try
            {
                objResultMessage = WSInputVoucherReturnProxy.GetVoucherAlert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strSaleOrderID, ref strVoucherAlert);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherReturnProxy.GetVoucherAlert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strSaleOrderID, ref strVoucherAlert);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucherReturn.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy thông tin cảnh báo đơn hàng thanh toán thẻ";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strVoucherAlert;
        }

       
        #endregion

    }
}
