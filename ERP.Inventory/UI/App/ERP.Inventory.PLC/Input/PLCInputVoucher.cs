﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace ERP.Inventory.PLC.Input
{
    public class PLCInputVoucher
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/Input/WSInputVoucher.asmx";
        private WSInputVoucher.WSInputVoucher objWSInputVoucherProxy = null;
        private WSInputVoucher.ResultMessage objResultMessage = new WSInputVoucher.ResultMessage();
        #endregion
        #region Property
        private WSInputVoucher.WSInputVoucher WSInputVoucherProxy
        {
            get
            {
                if (objWSInputVoucherProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSInputVoucherProxy = new WSInputVoucher.WSInputVoucher();
                    objWSInputVoucherProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSInputVoucherProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSInputVoucherProxy.EnableDecompression = true;
                    objWSInputVoucherProxy.Timeout = 900000;
                }
                return objWSInputVoucherProxy;
            }
        }
        #endregion
        #region Phương thức
        public WSInputVoucher.InputVoucher LoadInfo(string strInputVoucherID)
        {
            WSInputVoucher.InputVoucher objInputVoucher = null;
            try
            {
                objResultMessage = WSInputVoucherProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objInputVoucher, strInputVoucherID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objInputVoucher, strInputVoucherID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objInputVoucher;
        }

        public string GetInputVoucherNewID(int intStoreID)
        {
            string strInputVoucherID = string.Empty;
            try
            {
                objResultMessage = WSInputVoucherProxy.GetInputVoucherNewID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strInputVoucherID, intStoreID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.GetInputVoucherNewID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strInputVoucherID, intStoreID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy mã phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strInputVoucherID;
        }

        /// <summary>
        /// Lấy mã phiếu chi
        /// </summary>
        /// <param name="intStoreID"></param>
        /// <returns></returns>
        public string GetVoucherNewID(int intStoreID)
        {
            string strInputVoucherID = string.Empty;
            try
            {
                objResultMessage = WSInputVoucherProxy.GetVoucherNewID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strInputVoucherID, intStoreID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.GetVoucherNewID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strInputVoucherID, intStoreID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy mã phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strInputVoucherID;
        }

        /// <summary>
        /// Lấy mã phiếu chi
        /// </summary>
        /// <param name="intStoreID"></param>
        /// <returns></returns>
        public bool CheckIsHasVatInvoice(string strInputVoucherID)
        {
            bool bolIsHasVATInvoice = false;
            try
            {
                objResultMessage = WSInputVoucherProxy.CheckIsHasVatInvoice(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strInputVoucherID, ref bolIsHasVATInvoice);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.CheckIsHasVatInvoice(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strInputVoucherID, ref bolIsHasVATInvoice);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra thông tin hóa đơn của phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return bolIsHasVATInvoice;
        }

        private byte[] ObjectToByteArray(object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }
        public string Insert(WSInputVoucher.InputVoucher objInputVoucher)
        {
            string strInputVoucherID = string.Empty;
            try
            {
                objResultMessage = WSInputVoucherProxy.InsertInputVoucherAndVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strInputVoucherID, objInputVoucher);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.InsertInputVoucherAndVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strInputVoucherID, objInputVoucher);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strInputVoucherID;
        }

        /// <summary>
        /// xóa thông tin phiếu nhập
        /// PM_INPUTVOUCHER_DELETE
        /// </summary>
        /// <param name="objInputVoucher"></param>
        /// <returns>--0 lỗi, 1 thành công, 2 xuất bán, 3 chuyển kho</returns>
        public int Delete(WSInputVoucher.InputVoucher objInputVoucher)
        {
            int intResult = 0;
            try
            {
                objResultMessage = WSInputVoucherProxy.Detete(ref intResult, PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputVoucher);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.Detete(ref intResult, PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputVoucher);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi xóa thông tin phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return intResult;
        }

        /// <summary>
        /// lấy danh sách file đính kèm theo mã phiếu nhập
        /// PM_INPUTATTACHMENT_INPUTID
        /// </summary>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public List<WSInputVoucher.InputVoucher_Attachment> GetListAttachmentByInputVoucherID(string strInputVoucherID)
        {
            WSInputVoucher.InputVoucher_Attachment[] InputVoucher_AttachmentList = null;
            try
            {
                objResultMessage = WSInputVoucherProxy.GetListAttachmentByInputVoucherID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID,
                    ref InputVoucher_AttachmentList, strInputVoucherID);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.GetListAttachmentByInputVoucherID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID,
                        ref InputVoucher_AttachmentList, strInputVoucherID);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy danh sách file đính kèm theo mã phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            if (InputVoucher_AttachmentList == null)
                return null;
            return new List<WSInputVoucher.InputVoucher_Attachment>(InputVoucher_AttachmentList);
        }

        public bool UpdateInputVoucher(WSInputVoucher.InputVoucher objInputVoucher)
        {
            try
            {
                objResultMessage = WSInputVoucherProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputVoucher);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputVoucher);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật thông tin phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        /// <summary>
        /// Cập nhật thông tin thực nhập của phiếu nhập
        /// PM_InputVcher_UpdChkRealInput
        /// </summary>
        public bool UpdateCheckRealInput(List<WSInputVoucher.InputVoucher> objInputVoucherList)
        {
            try
            {
                objResultMessage = WSInputVoucherProxy.UpdateCheckRealInput(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputVoucherList.ToArray());
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.UpdateCheckRealInput(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objInputVoucherList.ToArray());
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Cập nhật thông tin thực nhập của phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        /// <summary>
        /// Tìm kiếm phiếu nhập
        /// PM_INPUTVOUCHER_SRH
        /// </summary>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public DataTable SearchData(object[] objKeywords)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSInputVoucherProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Tìm kiếm phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }

        /// <summary>
        /// Tìm kiếm phiếu nhập
        /// PM_INPUTVOUCHER_SRH
        /// </summary>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public DataTable SearchData2(object[] objKeywords)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSInputVoucherProxy.SearchData2(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.SearchData2(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Tìm kiếm phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }


        /// <summary>
        /// Kiểm tra số lương IMEI cần thêm vào có vượt quá giớ hạn không
        /// PM_INPUTVOUCHER_GETMAXIMEI
        /// </summary>
        public int GetMaxIMEI(string strProductID, int intQuantity)
        {
            int intMaxIMEI = 0;
            try
            {
                objResultMessage = WSInputVoucherProxy.GetMaxIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref intMaxIMEI, strProductID, intQuantity);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.GetMaxIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref intMaxIMEI, strProductID, intQuantity);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Kiểm tra số lương IMEI cần thêm vào có vượt quá giớ hạn không";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return intMaxIMEI;
        }

        /// <summary>
        /// Tạo IMEI phát sinh tự động
        /// PM_INPUTVOUCHER_GENAUTOIMEI
        /// </summary>
        public DataTable GenAutoIMEI(string strProductID, int intQuantity)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSInputVoucherProxy.GenAutoIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strProductID, intQuantity);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.GenAutoIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strProductID, intQuantity);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Tạo IMEI phát sinh tự động";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }


        /// <summary>
        /// Tạo IMEI phát sinh tự động
        /// PM_INPUTVOUCHER_GENAUTOIMEI
        /// </summary>
        public DataTable Report_GetInputVoucherData(string strInputVoucherID)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSInputVoucherProxy.Report_GetInputVoucherData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strInputVoucherID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.Report_GetInputVoucherData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strInputVoucherID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy thông tin phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }

        /// <summary>
        ///  Nạp chi tiết phiếu nhập 
        /// PM_Rpt_InputVoucherDetailRpt
        /// </summary>
        public DataTable Report_GetInputVoucherDetailData(string strInputVoucherID)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSInputVoucherProxy.Report_GetInputVoucherDetailData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strInputVoucherID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.Report_GetInputVoucherDetailData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strInputVoucherID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy chi tiết phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }

        /// <summary>
        /// Tìm kiếm lịch sử IMEI
        /// MD_Product_IMEIHistory
        /// </summary>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public DataTable SearchDataProductIMEIHistory(object[] objKeywords)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSInputVoucherProxy.SearchDataProductIMEIHistory(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.SearchDataProductIMEIHistory(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Tìm kiếm thông tin lịch sử IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }

        /// <summary>
        /// Tìm kiếm thông tin IMEI khóa FIFO
        /// MD_PRODUCT_IMEILOCKFIFO
        /// </summary>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public DataTable SearchDataProductIMEILockFIFO(object[] objKeywords)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSInputVoucherProxy.SearchDataProductIMEILockFIFO(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.SearchDataProductIMEILockFIFO(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Tìm kiếm thông tin IMEI khóa FIFO";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }
        /// <summary>
        /// Lấy ghi chú (Làm giá theo IMEI) theo chuỗi IMEI tìm kiếm 
        /// trường hợp có tồn tại IMEI 
        /// </summary>
        public string GetNoteByIMEI(string strIMEI)
        {
            string strResult = string.Empty;
            try
            {
                objResultMessage = WSInputVoucherProxy.GetNoteByIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strResult, strIMEI);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.GetNoteByIMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strResult, strIMEI);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy ghi chú (Làm giá theo IMEI) theo chuỗi IMEI tìm kiếm ";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strResult;
        }

        /// <summary>
        /// Lấy dữ liệu export cho đơn hàng bán
        /// PM_INPUTVOUCHERDETAIL_EXPORT
        /// </summary>
        public DataTable GetExportDataToSaleOrder(string strInputVoucherID)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSInputVoucherProxy.GetExportDataToSaleOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strInputVoucherID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.GetExportDataToSaleOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strInputVoucherID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy dữ liệu export cho đơn hàng bán";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }

        public WSInputVoucher.ResultMessage InsertAll(
            WSInputVoucher.InputVoucher objInputVoucher,
            WSInputVoucher.InputVoucher objInputVoucherCenter,
            WSInputVoucher.StoreChangeOrder objStoreChangeOrder,
            WSInputVoucher.StoreChange objStoreChangeBO,
            WSInputVoucher.VAT_Invoice objVAT_Invoice,
            string strFormTypeBranchID,
            WSInputVoucher.VAT_PInvoice objVAT_PInvoice,
            ref string strInputvoucherID,
            ref string strInputVoucherCenter,
            ref string strStoreChangeID,
            ref string strStoreChangeOrderID,
            ref string strOutPutVoucherID,
            ref string strVATInvoiceID
            )
        {
            try
            {
                objResultMessage = WSInputVoucherProxy.InsertData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID,
                     objInputVoucher, objInputVoucherCenter, objStoreChangeOrder, objStoreChangeBO, objVAT_Invoice, strFormTypeBranchID, objVAT_PInvoice,
                     ref  strInputvoucherID, ref strInputVoucherCenter, ref  strStoreChangeID, ref strStoreChangeOrderID, ref  strOutPutVoucherID,ref strVATInvoiceID);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.InsertData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID,
                      objInputVoucher, objInputVoucherCenter, objStoreChangeOrder, objStoreChangeBO, objVAT_Invoice, strFormTypeBranchID, objVAT_PInvoice,
                      ref  strInputvoucherID, ref strInputVoucherCenter, ref  strStoreChangeID, ref strStoreChangeOrderID, ref  strOutPutVoucherID, ref strVATInvoiceID);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
                return objResultMessage;
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }


        /// <summary>
        /// Nguyen Van Tai bo sung
        /// </summary>
        /// <param name="dtbData"></param>
        /// <param name="objKeyword"></param>
        /// <returns></returns>
        public PLC.Input.WSInputVoucher.ResultMessage GetInfoDetailImei(ref DataTable dtbData, object[] objKeyword)
        {
            PLC.Input.WSInputVoucher.ResultMessage objResultMessage = new WSInputVoucher.ResultMessage();
            try
            {
                objResultMessage = WSInputVoucherProxy.GetInfoDetailImei(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeyword);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.GetInfoDetailImei(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeyword);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PLC.Input.WSInputVoucher.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra quyền trên SIM";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }

        /// <summary>
        /// lấy danh sách file đính kèm theo mã phiếu nhập
        /// PM_INPUTATTACHMENT_INPUTID
        /// </summary>
        /// <param name="strInputVoucherID"></param>
        /// <returns></returns>
        public List<WSInputVoucher.InputVoucherDetail> GetListByInputVoucherID(string strInputVoucherID)
        {
            WSInputVoucher.InputVoucherDetail[] InputVoucherDetail = null;
            try
            {
                objResultMessage = WSInputVoucherProxy.GetListByInputVoucherID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID,
                    ref InputVoucherDetail, strInputVoucherID);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInputVoucherProxy.GetListByInputVoucherID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID,
                        ref InputVoucherDetail, strInputVoucherID);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInputVoucher.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy danh sách file đính kèm theo mã phiếu nhập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            if (InputVoucherDetail == null)
                return null;
            return new List<WSInputVoucher.InputVoucherDetail>(InputVoucherDetail);
        }
        #endregion
    }
}
