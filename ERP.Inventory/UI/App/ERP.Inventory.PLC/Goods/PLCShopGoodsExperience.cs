﻿using Library.AppCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ERP.Inventory.PLC.Goods
{
     public class PLCShopGoodsExperience
    {
         #region Variable
        private const String WS_CLASS_PATH = "Goods/WSShopGoodsExperience.asmx";
        private WSShopGoodsExperience.WSShopGoodsExperience objWS = null;
        private WSShopGoodsExperience.ResultMessage objResultMessage = new WSShopGoodsExperience.ResultMessage();
        #endregion
        #region Property
        private WSShopGoodsExperience.WSShopGoodsExperience WSShopGoodsExperienceProxy
        {
            get
            {
                if (objWS == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWS = new Goods.WSShopGoodsExperience.WSShopGoodsExperience();
                    objWS.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWS.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWS.EnableDecompression = true;
                    objWS.Timeout = 180000;
                }
                return objWS;
            }
        }
        #endregion
        #region Constructor
        public PLCShopGoodsExperience()
        {

        }
        #endregion
        #region Public Methods
        public DataTable LoadDataShopGoodsExperience(object[] objKeywords)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSShopGoodsExperienceProxy.LoadDataShopGoodsExperience(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSShopGoodsExperienceProxy.LoadDataShopGoodsExperience(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = Goods.WSShopGoodsExperience.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tìm kiếm danh sách";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }
        public string CheckGoodsDmtbExists(object[] objKeywords)
        {
            string dtbResult = null;
            try
            {
                objResultMessage = WSShopGoodsExperienceProxy.CheckGoodsDmtbExists(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSShopGoodsExperienceProxy.CheckGoodsDmtbExists(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = Goods.WSShopGoodsExperience.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi check dữ liệu trên server";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }

        public string Insert(object[] objKeywords)
        {
            string dtbResult = null;
            try
            {
                objResultMessage = WSShopGoodsExperienceProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSShopGoodsExperienceProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
                return dtbResult;
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = Goods.WSShopGoodsExperience.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm dữ liệu trên server";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
                return "";
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
          
        }

        public string Update(object[] objKeywords)
        {
            string dtbResult = null;
            try
            {
                objResultMessage = WSShopGoodsExperienceProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSShopGoodsExperienceProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
                return dtbResult;
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = Goods.WSShopGoodsExperience.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi sửa dữ liệu trên server";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
                return "";
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);

        }

        public void Delete(object[] objKeywords)
        {
            try
            {
                objResultMessage = WSShopGoodsExperienceProxy.Delete(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objKeywords.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSShopGoodsExperienceProxy.Delete(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objKeywords.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = Goods.WSShopGoodsExperience.ErrorTypes.Delete;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi xóa dữ liệu trên server";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
               
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);

        }

        public void ImportByExcel(object[][] objKeywords)
        {
            try
            {
                objResultMessage = WSShopGoodsExperienceProxy.ImportByExcel(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objKeywords.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSShopGoodsExperienceProxy.ImportByExcel(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID,objKeywords.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = Goods.WSShopGoodsExperience.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi import dữ liệu trên server";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);

            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);

        }

        public DataTable SearchData(object[] objKeywords)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSShopGoodsExperienceProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSShopGoodsExperienceProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = Goods.WSShopGoodsExperience.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tìm kiếm";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }
        #endregion

    }
}
