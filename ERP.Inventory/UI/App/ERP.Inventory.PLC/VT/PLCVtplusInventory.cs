﻿using Library.AppCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ERP.Inventory.PLC.VT
{
    public class PLCVtplusInventory
    {
        #region Variable
        private const String WS_CLASS_PATH = "/VtPlus/WSVtplusInventory.asmx";
        private WSVtplusInventory.WSVtplusInventory objWSVtPlusProxy = null;
        #endregion

        #region Property
        private VT.WSVtplusInventory.WSVtplusInventory WSVTPlusPercentProxy
        {
            get
            {
                if (objWSVtPlusProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSVtPlusProxy = new WSVtplusInventory.WSVtplusInventory();
                    objWSVtPlusProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSVtPlusProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSVtPlusProxy.EnableDecompression = true;
                    objWSVtPlusProxy.Timeout = 2100000;
                }
                return objWSVtPlusProxy;
            }
        }
        #endregion

        #region Thêm service tích hợp Viettel ++
        /// <summary>
        /// Insert Add Point
        /// Đỗ Văn Tùng
        /// </summary>
        /// <param name="objBO"></param>
        /// <returns></returns>
        public VT.WSVtplusInventory.ResultMessage InsertAddPoint(VT.WSVtplusInventory.VTPlus_AddPoint objAddPoint)
        {
            VT.WSVtplusInventory.ResultMessage objResultMessage = new VT.WSVtplusInventory.ResultMessage();
            try
            {
                objResultMessage = WSVTPlusPercentProxy.Insert_AddPoint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objAddPoint);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSVTPlusPercentProxy.Insert_AddPoint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objAddPoint);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = VT.WSVtplusInventory.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin bảng lưu thông tin tích điểm";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }

        /// <summary>
        /// Đỗ Văn Tùng
        /// Lấy tỉ lệ tích điểm, ngày hết hạn, mã chương trình
        /// </summary>
        /// <param name="Keywords"></param>
        /// <returns></returns>
        public DataTable GetPercent(string Keywords, string Keywords2, string storeID)
        {
            VT.WSVtplusInventory.ResultMessage objResultMessage = new VT.WSVtplusInventory.ResultMessage();
            DataTable Percent = null;
            try
            {

                objResultMessage = WSVTPlusPercentProxy.GetPercent(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref Percent, Keywords, Keywords2, storeID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSVTPlusPercentProxy.GetPercent(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref Percent, Keywords, Keywords2, storeID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = VT.WSVtplusInventory.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy tỉ lệ Tích điểm!";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return Percent;
        }

        public VT.WSVtplusInventory.ResultMessage SelectVTPlusPoint(ref DataTable dtbData, string StoreProcedure, object[] objKeywords)
        {
            VT.WSVtplusInventory.ResultMessage objResultMessage = new VT.WSVtplusInventory.ResultMessage();
            try
            {
                objResultMessage = WSVTPlusPercentProxy.SelectVTPlusPoint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, StoreProcedure, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSVTPlusPercentProxy.SelectVTPlusPoint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, StoreProcedure, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = VT.WSVtplusInventory.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "lỗi select thông tin điểm";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }

        public VT.WSVtplusInventory.ResultMessage UpdateVTPlusPoint(string StoreProcedure, object[] objKeywords)
        {
            VT.WSVtplusInventory.ResultMessage objResultMessage = new VT.WSVtplusInventory.ResultMessage();
            try
            {
                objResultMessage = WSVTPlusPercentProxy.UpdateVTPlusPoint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, StoreProcedure, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSVTPlusPercentProxy.UpdateVTPlusPoint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, StoreProcedure, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = VT.WSVtplusInventory.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin điểm";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;

        }
        #endregion
    }
}
