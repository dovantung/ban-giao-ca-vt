﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using Library.AppCore;
using ERP.Inventory.PLC.SplitProduct.WSSplitProduct;

namespace ERP.Inventory.PLC.SplitProduct
{
    public class PLCSplitProduct
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/SplitProduct/WSSplitProduct.asmx";
        private WSSplitProduct.WSSplitProduct objWSSplitProductProxy = null;
        private WSSplitProduct.ResultMessage objResultMessage = new WSSplitProduct.ResultMessage();
        #endregion
        #region Property
        private WSSplitProduct.WSSplitProduct WSSplitProductProxy
        {
            get
            {
                if (objWSSplitProductProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSSplitProductProxy = new WSSplitProduct.WSSplitProduct();
                    objWSSplitProductProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSSplitProductProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSSplitProductProxy.EnableDecompression = true;
                    objWSSplitProductProxy.Timeout = 180000;
                }
                return objWSSplitProductProxy;
            }
        }
        #endregion
        #region Method

        public bool InsertData(WSSplitProduct.SplitProduct objSplitProduct)
        {
            try
            {
                objResultMessage = WSSplitProductProxy.InsertData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID,objSplitProduct);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSSplitProductProxy.InsertData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objSplitProduct);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSSplitProduct.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm mới tách IMEI làm linh kiện";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public WSSplitProduct.ResultMessage SearchData(ref DataTable dtbResult, object[] objKeywords)
        {
            try
            {
                objResultMessage = WSSplitProductProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSSplitProductProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSSplitProduct.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách tách IMEI làm linh kiện";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }
        #endregion
    }
}
