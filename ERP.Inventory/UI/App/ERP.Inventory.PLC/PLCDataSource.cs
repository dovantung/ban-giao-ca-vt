﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;
using System.IO;
using System.IO.Compression;

namespace ERP.Inventory.PLC
{
    public class PLCDataSource
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/Inventory/WSInventory.asmx";
        private ERP.Inventory.PLC.Inventory.WSInventory.WSInventory objWSInventoryProxy = null;
        private ERP.Inventory.PLC.Inventory.WSInventory.ResultMessage objResultMessage = new ERP.Inventory.PLC.Inventory.WSInventory.ResultMessage();
        private ResultMessageApp objResultMessageApp = new ResultMessageApp();
        #endregion

        #region Property
        private ERP.Inventory.PLC.Inventory.WSInventory.WSInventory WSInventoryProxy
        {
            get
            {
                if (objWSInventoryProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSInventoryProxy = new ERP.Inventory.PLC.Inventory.WSInventory.WSInventory();
                    objWSInventoryProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSInventoryProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSInventoryProxy.EnableDecompression = true;
                    objWSInventoryProxy.Timeout = 180000;
                }
                return objWSInventoryProxy;
            }
        }

        public ResultMessageApp ResultMessageApp
        {
            get { return objResultMessageApp; }
            set { objResultMessageApp = value; }
        }
        #endregion

        #region Constructor
        public PLCDataSource()
        {

        }
        #endregion

        #region Public Methods
        public DataTable GetHeavyDataSource(string strReportStoreName, params object[] objKeywords)
        {
            DataTable dtbResult = null;
            try
            {
                Byte[] objByte = null;
                objResultMessage = WSInventoryProxy.GetHeavyData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                   PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objByte, strReportStoreName, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInventoryProxy.GetHeavyData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objByte, strReportStoreName, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
                if (objByte != null)
                {
                    DataSet dsData = ConvertByteToDataSet(objByte);
                    dtbResult = dsData.Tables[0];
                    dtbResult.TableName = strReportStoreName;
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = Inventory.WSInventory.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp dữ liệu";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }
        private static DataSet ConvertByteToDataSet(byte[] objByte)
        {
            DataSet dsData = new DataSet();
            MemoryStream objMemoryStream = new MemoryStream(objByte);
            GZipStream objGZipStream = new GZipStream(objMemoryStream, CompressionMode.Decompress);
            dsData.ReadXml(objGZipStream);
            objGZipStream.Close();
            objMemoryStream.Close();
            return dsData;
        }
        #endregion
    }
}
