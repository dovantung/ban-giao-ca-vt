﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.AppCore;
namespace ERP.Inventory.PLC
{
    public class PLCInventory_Globals
    {
        public static string ModuleName = "ERP.Inventory.PLC";
        public static WebServiceHost WebServiceHostTransactionServices = SystemConfig.GetWebServiceHost("WSHost_ERPTransactionServices");
        public static WebServiceHost WebServiceHostTransport = SystemConfig.GetWebServiceHost("WSHost_ERPTransportServices");

    }
}
