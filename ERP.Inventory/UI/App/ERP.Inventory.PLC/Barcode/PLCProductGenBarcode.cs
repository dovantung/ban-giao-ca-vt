﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;

namespace ERP.Inventory.PLC
{
    public class PLCProductGenBarcode
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/Barcode/WSProductGenBarcode.asmx";
        private WSProductGenBarcode.WSProductGenBarcode objWSProductGenBarcodeProxy = null;
        private WSProductGenBarcode.ResultMessage objResultMessage = new WSProductGenBarcode.ResultMessage();
        #endregion

        #region Property
        private WSProductGenBarcode.WSProductGenBarcode WSProductGenBarcodeProxy
        {
            get
            {
                if (objWSProductGenBarcodeProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSProductGenBarcodeProxy = new WSProductGenBarcode.WSProductGenBarcode();
                    objWSProductGenBarcodeProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSProductGenBarcodeProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSProductGenBarcodeProxy.EnableDecompression = true;
                    objWSProductGenBarcodeProxy.Timeout = 180000;
                }
                return objWSProductGenBarcodeProxy;
            }
        }
        #endregion

        #region Constructor
        public PLCProductGenBarcode()
        {

        }
        #endregion

        #region Public Methods

        public WSProductGenBarcode.Product_GenBarcode LoadInfo(decimal decPlanOrder)
        {
            WSProductGenBarcode.Product_GenBarcode objProduct_GenBarcode = null;
            try
            {
                objResultMessage = WSProductGenBarcodeProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProduct_GenBarcode, decPlanOrder);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductGenBarcodeProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objProduct_GenBarcode, decPlanOrder);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductGenBarcode.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin sản phẩm Barcode";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objProduct_GenBarcode;
        }

       
        public bool Insert(List<WSProductGenBarcode.Product_GenBarcode> objlistProduct_GenBarcode)
        {
            try
            {
                objResultMessage = WSProductGenBarcodeProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objlistProduct_GenBarcode.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductGenBarcodeProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objlistProduct_GenBarcode.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
                if (!objResultMessage.IsError)
                {
                    Library.AppCore.DataSource.CacheSource.ClearProductCacheServer();
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductGenBarcode.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin sản phẩm Barcode";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool GenBarcode(String strProductID, String strStringBarcode, ref String strBarcode)
        {
            try
            {
                objResultMessage = WSProductGenBarcodeProxy.GenBarcode(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strProductID, strStringBarcode, ref strBarcode);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductGenBarcodeProxy.GenBarcode(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strProductID, strStringBarcode, ref strBarcode);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductGenBarcode.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin sản phẩm Barcode";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool Update(List<WSProductGenBarcode.Product_GenBarcode> objlistProduct_GenBarcode)
        {
            try
            {
                objResultMessage = WSProductGenBarcodeProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objlistProduct_GenBarcode.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductGenBarcodeProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objlistProduct_GenBarcode.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
                if (!objResultMessage.IsError)
                {
                    Library.AppCore.DataSource.CacheSource.ClearProductCacheServer();
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductGenBarcode.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật thông sản phẩm Barcode";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public WSProductGenBarcode.ResultMessage SearchData(ref DataTable dtbData, object[] objKeywords)
        {
            WSProductGenBarcode.ResultMessage objResultMessage = new WSProductGenBarcode.ResultMessage();
            try
            {
                objResultMessage = WSProductGenBarcodeProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductGenBarcodeProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
                return objResultMessage;
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductGenBarcode.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách sản phẩm Barcode";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
                SystemErrorWS.Insert("", objExc.ToString(), "PLCInventory_Globals -> SearchData", PLCInventory_Globals.ModuleName);
                return objResultMessage;
            }
        }
        #endregion

    }
}
