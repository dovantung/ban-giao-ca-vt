﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;

namespace ERP.Inventory.PLC.Barcode
{
    public class PLCProductBarcode
    {
      
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/Barcode/WSProductBarcode.asmx";
        private WSProductBarcode.WSProductBarcode objWSProductBarcodeProxy = null;
        private WSProductBarcode.ResultMessage objResultMessage = new WSProductBarcode.ResultMessage();
        #endregion
        
        #region Property
        private WSProductBarcode.WSProductBarcode WSProductBarcodeProxy
        {
            get
            {
                if (objWSProductBarcodeProxy== null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSProductBarcodeProxy = new WSProductBarcode.WSProductBarcode();
                    objWSProductBarcodeProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSProductBarcodeProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSProductBarcodeProxy.EnableDecompression = true;
                    objWSProductBarcodeProxy.Timeout = 180000;
                }
                return objWSProductBarcodeProxy;
            }
        }
        #endregion
     
        #region Phương thức
        /// <summary>
        /// Lấy danh sách tồn kho hiện tại để in
        /// PM_CurrentInstock_GetPrints
        /// </summary>
        public DataTable GetCurrentInstockPrints(int intMainGroupID, int intSubGroupID, string strProductID, string strBrandIDList, int intStoreID,int intIsNew,bool bolIsInStock, string strInputVoucherID)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSProductBarcodeProxy.GetCurrentInstockPrints(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, intMainGroupID, intSubGroupID, strProductID, strBrandIDList, intStoreID, intIsNew, bolIsInStock, strInputVoucherID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductBarcodeProxy.GetCurrentInstockPrints(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, intMainGroupID, intSubGroupID, strProductID, strBrandIDList, intStoreID, intIsNew, bolIsInStock, strInputVoucherID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductBarcode.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Lấy danh sách tồn kho hiện tại để in";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }

        /// <summary>
        /// Lấy danh sách tồn kho hiện tại để in
        /// PM_CurrentInstock_IMEI_GetPr
        /// </summary>
        public DataTable GetCurrentInstockPrints_IMEI(int intMainGroupID, int intSubGroupID, string strProductID, string strBrandIDList, int intStoreID, int intIsNew, string strInputVoucherID)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSProductBarcodeProxy.GetCurrentInstockPrints_IMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, intMainGroupID, intSubGroupID, strProductID, strBrandIDList, intStoreID, intIsNew, strInputVoucherID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSProductBarcodeProxy.GetCurrentInstockPrints_IMEI(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, intMainGroupID, intSubGroupID, strProductID, strBrandIDList, intStoreID, intIsNew, strInputVoucherID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSProductBarcode.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Lấy danh sách tồn kho hiện tại để in";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }
        #endregion
    }
}
