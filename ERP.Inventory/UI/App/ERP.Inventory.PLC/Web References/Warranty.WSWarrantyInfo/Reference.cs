﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.225
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.225.
// 
#pragma warning disable 1591

namespace ERP.Inventory.PLC.Warranty.WSWarrantyInfo {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.ComponentModel;
    using System.Xml.Serialization;
    using System.Data;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="WSWarrantyInfoSoap", Namespace="http://tempuri.org/")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(object[]))]
    public partial class WSWarrantyInfo : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback SearchDataOperationCompleted;
        
        private System.Threading.SendOrPostCallback InsertWarrantyInfoOperationCompleted;
        
        private System.Threading.SendOrPostCallback CalWarrantyTimeOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public WSWarrantyInfo() {
            this.Url = global::ERP.Inventory.PLC.Properties.Settings.Default.ERP_Inventory_PLC_Warranty_WSWarrantyInfo_WSWarrantyInfo;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event SearchDataCompletedEventHandler SearchDataCompleted;
        
        /// <remarks/>
        public event InsertWarrantyInfoCompletedEventHandler InsertWarrantyInfoCompleted;
        
        /// <remarks/>
        public event CalWarrantyTimeCompletedEventHandler CalWarrantyTimeCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Tìm kiếm thông tin bảo hành", RequestElementName="Tìm kiếm thông tin bảo hành", RequestNamespace="http://tempuri.org/", ResponseElementName="Tìm kiếm thông tin bảo hànhResponse", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("Tìm kiếm thông tin bảo hànhResult")]
        public ResultMessage SearchData(string strAuthenData, string strGUID, ref System.Data.DataTable dtResult, object[] objKeywords) {
            object[] results = this.Invoke("SearchData", new object[] {
                        strAuthenData,
                        strGUID,
                        dtResult,
                        objKeywords});
            dtResult = ((System.Data.DataTable)(results[1]));
            return ((ResultMessage)(results[0]));
        }
        
        /// <remarks/>
        public void SearchDataAsync(string strAuthenData, string strGUID, System.Data.DataTable dtResult, object[] objKeywords) {
            this.SearchDataAsync(strAuthenData, strGUID, dtResult, objKeywords, null);
        }
        
        /// <remarks/>
        public void SearchDataAsync(string strAuthenData, string strGUID, System.Data.DataTable dtResult, object[] objKeywords, object userState) {
            if ((this.SearchDataOperationCompleted == null)) {
                this.SearchDataOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSearchDataOperationCompleted);
            }
            this.InvokeAsync("SearchData", new object[] {
                        strAuthenData,
                        strGUID,
                        dtResult,
                        objKeywords}, this.SearchDataOperationCompleted, userState);
        }
        
        private void OnSearchDataOperationCompleted(object arg) {
            if ((this.SearchDataCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SearchDataCompleted(this, new SearchDataCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/InsertWarrantyInfo", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public ResultMessage InsertWarrantyInfo(string strAuthenData, string strGUID, string strIMEI, string strOutputVoucherID) {
            object[] results = this.Invoke("InsertWarrantyInfo", new object[] {
                        strAuthenData,
                        strGUID,
                        strIMEI,
                        strOutputVoucherID});
            return ((ResultMessage)(results[0]));
        }
        
        /// <remarks/>
        public void InsertWarrantyInfoAsync(string strAuthenData, string strGUID, string strIMEI, string strOutputVoucherID) {
            this.InsertWarrantyInfoAsync(strAuthenData, strGUID, strIMEI, strOutputVoucherID, null);
        }
        
        /// <remarks/>
        public void InsertWarrantyInfoAsync(string strAuthenData, string strGUID, string strIMEI, string strOutputVoucherID, object userState) {
            if ((this.InsertWarrantyInfoOperationCompleted == null)) {
                this.InsertWarrantyInfoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertWarrantyInfoOperationCompleted);
            }
            this.InvokeAsync("InsertWarrantyInfo", new object[] {
                        strAuthenData,
                        strGUID,
                        strIMEI,
                        strOutputVoucherID}, this.InsertWarrantyInfoOperationCompleted, userState);
        }
        
        private void OnInsertWarrantyInfoOperationCompleted(object arg) {
            if ((this.InsertWarrantyInfoCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertWarrantyInfoCompleted(this, new InsertWarrantyInfoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/CalWarrantyTime", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public ResultMessage CalWarrantyTime(string strAuthenData, string strGUID, string strIMEI, string strProductID) {
            object[] results = this.Invoke("CalWarrantyTime", new object[] {
                        strAuthenData,
                        strGUID,
                        strIMEI,
                        strProductID});
            return ((ResultMessage)(results[0]));
        }
        
        /// <remarks/>
        public void CalWarrantyTimeAsync(string strAuthenData, string strGUID, string strIMEI, string strProductID) {
            this.CalWarrantyTimeAsync(strAuthenData, strGUID, strIMEI, strProductID, null);
        }
        
        /// <remarks/>
        public void CalWarrantyTimeAsync(string strAuthenData, string strGUID, string strIMEI, string strProductID, object userState) {
            if ((this.CalWarrantyTimeOperationCompleted == null)) {
                this.CalWarrantyTimeOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCalWarrantyTimeOperationCompleted);
            }
            this.InvokeAsync("CalWarrantyTime", new object[] {
                        strAuthenData,
                        strGUID,
                        strIMEI,
                        strProductID}, this.CalWarrantyTimeOperationCompleted, userState);
        }
        
        private void OnCalWarrantyTimeOperationCompleted(object arg) {
            if ((this.CalWarrantyTimeCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CalWarrantyTimeCompleted(this, new CalWarrantyTimeCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.225")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class ResultMessage {
        
        private bool isErrorField;
        
        private ErrorTypes errorTypeField;
        
        private string messageField;
        
        private string messageDetailField;
        
        /// <remarks/>
        public bool IsError {
            get {
                return this.isErrorField;
            }
            set {
                this.isErrorField = value;
            }
        }
        
        /// <remarks/>
        public ErrorTypes ErrorType {
            get {
                return this.errorTypeField;
            }
            set {
                this.errorTypeField = value;
            }
        }
        
        /// <remarks/>
        public string Message {
            get {
                return this.messageField;
            }
            set {
                this.messageField = value;
            }
        }
        
        /// <remarks/>
        public string MessageDetail {
            get {
                return this.messageDetailField;
            }
            set {
                this.messageDetailField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.225")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public enum ErrorTypes {
        
        /// <remarks/>
        No_Error,
        
        /// <remarks/>
        LoadInfo,
        
        /// <remarks/>
        Insert,
        
        /// <remarks/>
        Update,
        
        /// <remarks/>
        Delete,
        
        /// <remarks/>
        SearchData,
        
        /// <remarks/>
        GetData,
        
        /// <remarks/>
        InvalidIV,
        
        /// <remarks/>
        TokenNotExist,
        
        /// <remarks/>
        TokenInvalid,
        
        /// <remarks/>
        CheckData,
        
        /// <remarks/>
        Others,
        
        /// <remarks/>
        UserDefine,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void SearchDataCompletedEventHandler(object sender, SearchDataCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class SearchDataCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal SearchDataCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ResultMessage Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ResultMessage)(this.results[0]));
            }
        }
        
        /// <remarks/>
        public System.Data.DataTable dtResult {
            get {
                this.RaiseExceptionIfNecessary();
                return ((System.Data.DataTable)(this.results[1]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void InsertWarrantyInfoCompletedEventHandler(object sender, InsertWarrantyInfoCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class InsertWarrantyInfoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal InsertWarrantyInfoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ResultMessage Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ResultMessage)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void CalWarrantyTimeCompletedEventHandler(object sender, CalWarrantyTimeCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class CalWarrantyTimeCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal CalWarrantyTimeCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ResultMessage Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ResultMessage)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591