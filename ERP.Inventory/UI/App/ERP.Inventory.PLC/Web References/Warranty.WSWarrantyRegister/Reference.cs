﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace ERP.Inventory.PLC.Warranty.WSWarrantyRegister {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    using System.Data;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="WSWarrantyRegisterSoap", Namespace="http://tempuri.org/")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(object[]))]
    public partial class WSWarrantyRegister : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback SearchDataOperationCompleted;
        
        private System.Threading.SendOrPostCallback InsertOperationCompleted;
        
        private System.Threading.SendOrPostCallback InsertMultiOperationCompleted;
        
        private System.Threading.SendOrPostCallback ReRegisterOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public WSWarrantyRegister() {
            this.Url = global::ERP.Inventory.PLC.Properties.Settings.Default.ERP_Inventory_PLC_Warranty_WSWarrantyRegister_WSWarrantyRegister;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event SearchDataCompletedEventHandler SearchDataCompleted;
        
        /// <remarks/>
        public event InsertCompletedEventHandler InsertCompleted;
        
        /// <remarks/>
        public event InsertMultiCompletedEventHandler InsertMultiCompleted;
        
        /// <remarks/>
        public event ReRegisterCompletedEventHandler ReRegisterCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Tìm kiếm đăng ký bảo hành", RequestElementName="Tìm kiếm đăng ký bảo hành", RequestNamespace="http://tempuri.org/", ResponseElementName="Tìm kiếm đăng ký bảo hànhResponse", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("Tìm kiếm đăng ký bảo hànhResult")]
        public ResultMessage SearchData(string strAuthenData, string strGUID, ref System.Data.DataTable dtResult, object[] objKeywords) {
            object[] results = this.Invoke("SearchData", new object[] {
                        strAuthenData,
                        strGUID,
                        dtResult,
                        objKeywords});
            dtResult = ((System.Data.DataTable)(results[1]));
            return ((ResultMessage)(results[0]));
        }
        
        /// <remarks/>
        public void SearchDataAsync(string strAuthenData, string strGUID, System.Data.DataTable dtResult, object[] objKeywords) {
            this.SearchDataAsync(strAuthenData, strGUID, dtResult, objKeywords, null);
        }
        
        /// <remarks/>
        public void SearchDataAsync(string strAuthenData, string strGUID, System.Data.DataTable dtResult, object[] objKeywords, object userState) {
            if ((this.SearchDataOperationCompleted == null)) {
                this.SearchDataOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSearchDataOperationCompleted);
            }
            this.InvokeAsync("SearchData", new object[] {
                        strAuthenData,
                        strGUID,
                        dtResult,
                        objKeywords}, this.SearchDataOperationCompleted, userState);
        }
        
        private void OnSearchDataOperationCompleted(object arg) {
            if ((this.SearchDataCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SearchDataCompleted(this, new SearchDataCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Insert", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public ResultMessage Insert(string strAuthenData, string strGUID, WarrantyRegister obj) {
            object[] results = this.Invoke("Insert", new object[] {
                        strAuthenData,
                        strGUID,
                        obj});
            return ((ResultMessage)(results[0]));
        }
        
        /// <remarks/>
        public void InsertAsync(string strAuthenData, string strGUID, WarrantyRegister obj) {
            this.InsertAsync(strAuthenData, strGUID, obj, null);
        }
        
        /// <remarks/>
        public void InsertAsync(string strAuthenData, string strGUID, WarrantyRegister obj, object userState) {
            if ((this.InsertOperationCompleted == null)) {
                this.InsertOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertOperationCompleted);
            }
            this.InvokeAsync("Insert", new object[] {
                        strAuthenData,
                        strGUID,
                        obj}, this.InsertOperationCompleted, userState);
        }
        
        private void OnInsertOperationCompleted(object arg) {
            if ((this.InsertCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertCompleted(this, new InsertCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/InsertMulti", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public ResultMessage InsertMulti(string strAuthenData, string strGUID, WarrantyRegister[] objWarrantyRegisterList) {
            object[] results = this.Invoke("InsertMulti", new object[] {
                        strAuthenData,
                        strGUID,
                        objWarrantyRegisterList});
            return ((ResultMessage)(results[0]));
        }
        
        /// <remarks/>
        public void InsertMultiAsync(string strAuthenData, string strGUID, WarrantyRegister[] objWarrantyRegisterList) {
            this.InsertMultiAsync(strAuthenData, strGUID, objWarrantyRegisterList, null);
        }
        
        /// <remarks/>
        public void InsertMultiAsync(string strAuthenData, string strGUID, WarrantyRegister[] objWarrantyRegisterList, object userState) {
            if ((this.InsertMultiOperationCompleted == null)) {
                this.InsertMultiOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertMultiOperationCompleted);
            }
            this.InvokeAsync("InsertMulti", new object[] {
                        strAuthenData,
                        strGUID,
                        objWarrantyRegisterList}, this.InsertMultiOperationCompleted, userState);
        }
        
        private void OnInsertMultiOperationCompleted(object arg) {
            if ((this.InsertMultiCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertMultiCompleted(this, new InsertMultiCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/ReRegister", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public ResultMessage ReRegister(string strAuthenData, string strGUID, WarrantyRegister obj) {
            object[] results = this.Invoke("ReRegister", new object[] {
                        strAuthenData,
                        strGUID,
                        obj});
            return ((ResultMessage)(results[0]));
        }
        
        /// <remarks/>
        public void ReRegisterAsync(string strAuthenData, string strGUID, WarrantyRegister obj) {
            this.ReRegisterAsync(strAuthenData, strGUID, obj, null);
        }
        
        /// <remarks/>
        public void ReRegisterAsync(string strAuthenData, string strGUID, WarrantyRegister obj, object userState) {
            if ((this.ReRegisterOperationCompleted == null)) {
                this.ReRegisterOperationCompleted = new System.Threading.SendOrPostCallback(this.OnReRegisterOperationCompleted);
            }
            this.InvokeAsync("ReRegister", new object[] {
                        strAuthenData,
                        strGUID,
                        obj}, this.ReRegisterOperationCompleted, userState);
        }
        
        private void OnReRegisterOperationCompleted(object arg) {
            if ((this.ReRegisterCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.ReRegisterCompleted(this, new ReRegisterCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2556.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class WarrantyRegister {
        
        private string productNameField;
        
        private string registerStoreNameField;
        
        private decimal warrantyRegisterIDField;
        
        private int warrantyRegisterTypeIDField;
        
        private System.Nullable<System.DateTime> outputDateField;
        
        private string outputVoucherIDField;
        
        private string outputVoucherDetailIDField;
        
        private string saleOrderIDField;
        
        private string saleOrderDetailIDField;
        
        private string productIDField;
        
        private string iMEIField;
        
        private string customerPhoneField;
        
        private string customerNameField;
        
        private string customerAddressField;
        
        private decimal registerStoreIDField;
        
        private bool isRegisterFromOrderField;
        
        private bool isRegisteredField;
        
        private System.Nullable<System.DateTime> registerEDTimeField;
        
        private decimal replyStatusIDField;
        
        private string replyMessageField;
        
        private System.Nullable<System.DateTime> replyTimeField;
        
        private bool iSRegisterErrorField;
        
        private string createdUserField;
        
        private System.Nullable<System.DateTime> createdDateField;
        
        private string updatedUserField;
        
        private System.Nullable<System.DateTime> updatedDateField;
        
        private bool isDeletedField;
        
        private string deletedUserField;
        
        private System.Nullable<System.DateTime> deletedDateField;
        
        /// <remarks/>
        public string ProductName {
            get {
                return this.productNameField;
            }
            set {
                this.productNameField = value;
            }
        }
        
        /// <remarks/>
        public string RegisterStoreName {
            get {
                return this.registerStoreNameField;
            }
            set {
                this.registerStoreNameField = value;
            }
        }
        
        /// <remarks/>
        public decimal WarrantyRegisterID {
            get {
                return this.warrantyRegisterIDField;
            }
            set {
                this.warrantyRegisterIDField = value;
            }
        }
        
        /// <remarks/>
        public int WarrantyRegisterTypeID {
            get {
                return this.warrantyRegisterTypeIDField;
            }
            set {
                this.warrantyRegisterTypeIDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> OutputDate {
            get {
                return this.outputDateField;
            }
            set {
                this.outputDateField = value;
            }
        }
        
        /// <remarks/>
        public string OutputVoucherID {
            get {
                return this.outputVoucherIDField;
            }
            set {
                this.outputVoucherIDField = value;
            }
        }
        
        /// <remarks/>
        public string OutputVoucherDetailID {
            get {
                return this.outputVoucherDetailIDField;
            }
            set {
                this.outputVoucherDetailIDField = value;
            }
        }
        
        /// <remarks/>
        public string SaleOrderID {
            get {
                return this.saleOrderIDField;
            }
            set {
                this.saleOrderIDField = value;
            }
        }
        
        /// <remarks/>
        public string SaleOrderDetailID {
            get {
                return this.saleOrderDetailIDField;
            }
            set {
                this.saleOrderDetailIDField = value;
            }
        }
        
        /// <remarks/>
        public string ProductID {
            get {
                return this.productIDField;
            }
            set {
                this.productIDField = value;
            }
        }
        
        /// <remarks/>
        public string IMEI {
            get {
                return this.iMEIField;
            }
            set {
                this.iMEIField = value;
            }
        }
        
        /// <remarks/>
        public string CustomerPhone {
            get {
                return this.customerPhoneField;
            }
            set {
                this.customerPhoneField = value;
            }
        }
        
        /// <remarks/>
        public string CustomerName {
            get {
                return this.customerNameField;
            }
            set {
                this.customerNameField = value;
            }
        }
        
        /// <remarks/>
        public string CustomerAddress {
            get {
                return this.customerAddressField;
            }
            set {
                this.customerAddressField = value;
            }
        }
        
        /// <remarks/>
        public decimal RegisterStoreID {
            get {
                return this.registerStoreIDField;
            }
            set {
                this.registerStoreIDField = value;
            }
        }
        
        /// <remarks/>
        public bool IsRegisterFromOrder {
            get {
                return this.isRegisterFromOrderField;
            }
            set {
                this.isRegisterFromOrderField = value;
            }
        }
        
        /// <remarks/>
        public bool IsRegistered {
            get {
                return this.isRegisteredField;
            }
            set {
                this.isRegisteredField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> RegisterEDTime {
            get {
                return this.registerEDTimeField;
            }
            set {
                this.registerEDTimeField = value;
            }
        }
        
        /// <remarks/>
        public decimal ReplyStatusID {
            get {
                return this.replyStatusIDField;
            }
            set {
                this.replyStatusIDField = value;
            }
        }
        
        /// <remarks/>
        public string ReplyMessage {
            get {
                return this.replyMessageField;
            }
            set {
                this.replyMessageField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> ReplyTime {
            get {
                return this.replyTimeField;
            }
            set {
                this.replyTimeField = value;
            }
        }
        
        /// <remarks/>
        public bool ISRegisterError {
            get {
                return this.iSRegisterErrorField;
            }
            set {
                this.iSRegisterErrorField = value;
            }
        }
        
        /// <remarks/>
        public string CreatedUser {
            get {
                return this.createdUserField;
            }
            set {
                this.createdUserField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> CreatedDate {
            get {
                return this.createdDateField;
            }
            set {
                this.createdDateField = value;
            }
        }
        
        /// <remarks/>
        public string UpdatedUser {
            get {
                return this.updatedUserField;
            }
            set {
                this.updatedUserField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> UpdatedDate {
            get {
                return this.updatedDateField;
            }
            set {
                this.updatedDateField = value;
            }
        }
        
        /// <remarks/>
        public bool IsDeleted {
            get {
                return this.isDeletedField;
            }
            set {
                this.isDeletedField = value;
            }
        }
        
        /// <remarks/>
        public string DeletedUser {
            get {
                return this.deletedUserField;
            }
            set {
                this.deletedUserField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> DeletedDate {
            get {
                return this.deletedDateField;
            }
            set {
                this.deletedDateField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2556.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class ResultMessage {
        
        private bool isErrorField;
        
        private ErrorTypes errorTypeField;
        
        private string messageField;
        
        private string messageDetailField;
        
        /// <remarks/>
        public bool IsError {
            get {
                return this.isErrorField;
            }
            set {
                this.isErrorField = value;
            }
        }
        
        /// <remarks/>
        public ErrorTypes ErrorType {
            get {
                return this.errorTypeField;
            }
            set {
                this.errorTypeField = value;
            }
        }
        
        /// <remarks/>
        public string Message {
            get {
                return this.messageField;
            }
            set {
                this.messageField = value;
            }
        }
        
        /// <remarks/>
        public string MessageDetail {
            get {
                return this.messageDetailField;
            }
            set {
                this.messageDetailField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2556.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public enum ErrorTypes {
        
        /// <remarks/>
        No_Error,
        
        /// <remarks/>
        LoadInfo,
        
        /// <remarks/>
        Insert,
        
        /// <remarks/>
        Update,
        
        /// <remarks/>
        Delete,
        
        /// <remarks/>
        SearchData,
        
        /// <remarks/>
        GetData,
        
        /// <remarks/>
        InvalidIV,
        
        /// <remarks/>
        TokenNotExist,
        
        /// <remarks/>
        TokenInvalid,
        
        /// <remarks/>
        CheckData,
        
        /// <remarks/>
        Others,
        
        /// <remarks/>
        UserDefine,
        
        /// <remarks/>
        UserMustReLogin,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    public delegate void SearchDataCompletedEventHandler(object sender, SearchDataCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class SearchDataCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal SearchDataCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ResultMessage Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ResultMessage)(this.results[0]));
            }
        }
        
        /// <remarks/>
        public System.Data.DataTable dtResult {
            get {
                this.RaiseExceptionIfNecessary();
                return ((System.Data.DataTable)(this.results[1]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    public delegate void InsertCompletedEventHandler(object sender, InsertCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class InsertCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal InsertCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ResultMessage Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ResultMessage)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    public delegate void InsertMultiCompletedEventHandler(object sender, InsertMultiCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class InsertMultiCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal InsertMultiCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ResultMessage Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ResultMessage)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    public delegate void ReRegisterCompletedEventHandler(object sender, ReRegisterCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.2556.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class ReRegisterCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal ReRegisterCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ResultMessage Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ResultMessage)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591