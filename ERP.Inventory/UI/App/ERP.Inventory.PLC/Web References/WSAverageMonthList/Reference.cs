﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18010
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.18010.
// 
#pragma warning disable 1591

namespace ERP.Inventory.PLC.WSAverageMonthList {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    using System.Data;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="WSAverageMonthListSoap", Namespace="http://tempuri.org/")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(object[]))]
    public partial class WSAverageMonthList : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback SearchDataOperationCompleted;
        
        private System.Threading.SendOrPostCallback SearchDataDetailOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public WSAverageMonthList() {
            this.Url = global::ERP.Inventory.PLC.Properties.Settings.Default.ERP_Inventory_PLC_WSAverageMonthList_WSAverageMonthList;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event SearchDataCompletedEventHandler SearchDataCompleted;
        
        /// <remarks/>
        public event SearchDataDetailCompletedEventHandler SearchDataDetailCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/SearchData", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public ResultMessage SearchData(string strAuthenData, string strGUID, ref System.Data.DataTable tblResult, object[] objKeywords) {
            object[] results = this.Invoke("SearchData", new object[] {
                        strAuthenData,
                        strGUID,
                        tblResult,
                        objKeywords});
            tblResult = ((System.Data.DataTable)(results[1]));
            return ((ResultMessage)(results[0]));
        }
        
        /// <remarks/>
        public void SearchDataAsync(string strAuthenData, string strGUID, System.Data.DataTable tblResult, object[] objKeywords) {
            this.SearchDataAsync(strAuthenData, strGUID, tblResult, objKeywords, null);
        }
        
        /// <remarks/>
        public void SearchDataAsync(string strAuthenData, string strGUID, System.Data.DataTable tblResult, object[] objKeywords, object userState) {
            if ((this.SearchDataOperationCompleted == null)) {
                this.SearchDataOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSearchDataOperationCompleted);
            }
            this.InvokeAsync("SearchData", new object[] {
                        strAuthenData,
                        strGUID,
                        tblResult,
                        objKeywords}, this.SearchDataOperationCompleted, userState);
        }
        
        private void OnSearchDataOperationCompleted(object arg) {
            if ((this.SearchDataCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SearchDataCompleted(this, new SearchDataCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/SearchDataDetail", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public ResultMessage SearchDataDetail(string strAuthenData, string strGUID, ref System.Data.DataTable tblResult, object[] objKeywords) {
            object[] results = this.Invoke("SearchDataDetail", new object[] {
                        strAuthenData,
                        strGUID,
                        tblResult,
                        objKeywords});
            tblResult = ((System.Data.DataTable)(results[1]));
            return ((ResultMessage)(results[0]));
        }
        
        /// <remarks/>
        public void SearchDataDetailAsync(string strAuthenData, string strGUID, System.Data.DataTable tblResult, object[] objKeywords) {
            this.SearchDataDetailAsync(strAuthenData, strGUID, tblResult, objKeywords, null);
        }
        
        /// <remarks/>
        public void SearchDataDetailAsync(string strAuthenData, string strGUID, System.Data.DataTable tblResult, object[] objKeywords, object userState) {
            if ((this.SearchDataDetailOperationCompleted == null)) {
                this.SearchDataDetailOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSearchDataDetailOperationCompleted);
            }
            this.InvokeAsync("SearchDataDetail", new object[] {
                        strAuthenData,
                        strGUID,
                        tblResult,
                        objKeywords}, this.SearchDataDetailOperationCompleted, userState);
        }
        
        private void OnSearchDataDetailOperationCompleted(object arg) {
            if ((this.SearchDataDetailCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SearchDataDetailCompleted(this, new SearchDataDetailCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class ResultMessage {
        
        private bool isErrorField;
        
        private ErrorTypes errorTypeField;
        
        private string messageField;
        
        private string messageDetailField;
        
        /// <remarks/>
        public bool IsError {
            get {
                return this.isErrorField;
            }
            set {
                this.isErrorField = value;
            }
        }
        
        /// <remarks/>
        public ErrorTypes ErrorType {
            get {
                return this.errorTypeField;
            }
            set {
                this.errorTypeField = value;
            }
        }
        
        /// <remarks/>
        public string Message {
            get {
                return this.messageField;
            }
            set {
                this.messageField = value;
            }
        }
        
        /// <remarks/>
        public string MessageDetail {
            get {
                return this.messageDetailField;
            }
            set {
                this.messageDetailField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public enum ErrorTypes {
        
        /// <remarks/>
        No_Error,
        
        /// <remarks/>
        LoadInfo,
        
        /// <remarks/>
        Insert,
        
        /// <remarks/>
        Update,
        
        /// <remarks/>
        Delete,
        
        /// <remarks/>
        SearchData,
        
        /// <remarks/>
        GetData,
        
        /// <remarks/>
        InvalidIV,
        
        /// <remarks/>
        TokenNotExist,
        
        /// <remarks/>
        TokenInvalid,
        
        /// <remarks/>
        CheckData,
        
        /// <remarks/>
        Others,
        
        /// <remarks/>
        UserDefine,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void SearchDataCompletedEventHandler(object sender, SearchDataCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class SearchDataCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal SearchDataCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ResultMessage Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ResultMessage)(this.results[0]));
            }
        }
        
        /// <remarks/>
        public System.Data.DataTable tblResult {
            get {
                this.RaiseExceptionIfNecessary();
                return ((System.Data.DataTable)(this.results[1]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void SearchDataDetailCompletedEventHandler(object sender, SearchDataDetailCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class SearchDataDetailCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal SearchDataDetailCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ResultMessage Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ResultMessage)(this.results[0]));
            }
        }
        
        /// <remarks/>
        public System.Data.DataTable tblResult {
            get {
                this.RaiseExceptionIfNecessary();
                return ((System.Data.DataTable)(this.results[1]));
            }
        }
    }
}

#pragma warning restore 1591