﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.AppCore;
using System.Data;

namespace ERP.Inventory.PLC.SM
{
    public class PLCSequenceIMEI
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/SM/WSSequenceIMEI.asmx";
        private WSSequenceIMEI.WSSequenceIMEI objWSSequenceIMEIProxy = null;
        private WSSequenceIMEI.ResultMessage objResultMessage = new WSSequenceIMEI.ResultMessage();
        #endregion
        #region Property
        private WSSequenceIMEI.WSSequenceIMEI WSSequenceIMEIProxy
        {
            get
            {
                if (objWSSequenceIMEIProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSSequenceIMEIProxy = new WSSequenceIMEI.WSSequenceIMEI();
                    objWSSequenceIMEIProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSSequenceIMEIProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSSequenceIMEIProxy.EnableDecompression = true;
                    objWSSequenceIMEIProxy.Timeout = 180000;
                }
                return objWSSequenceIMEIProxy;
            }
        }
        #endregion
        #region Method

        public bool Insert(ref WSSequenceIMEI.SequenceIMEI[] lstSequenceIMEI)
        {
            try
            {
                objResultMessage = WSSequenceIMEIProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref lstSequenceIMEI);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSSequenceIMEIProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref lstSequenceIMEI);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSSequenceIMEI.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật mức ưu tiên của  IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public WSSequenceIMEI.ResultMessage CheckExist(ref bool bolIsResult, string strIMEI, string strProductID)
        {
            try
            {
                objResultMessage = WSSequenceIMEIProxy.CheckExist(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref bolIsResult, strIMEI, strProductID);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSSequenceIMEIProxy.CheckExist(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref bolIsResult, strIMEI, strProductID);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSSequenceIMEI.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra tồn kho IMEI-sản phẩm";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }


        public WSSequenceIMEI.ResultMessage SearchData(ref DataTable dtbResult, object[] objKeywords)
        {
            try
            {
                objResultMessage = WSSequenceIMEIProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSSequenceIMEIProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSSequenceIMEI.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin mức ưu tiên IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }
        #endregion
    }
}
