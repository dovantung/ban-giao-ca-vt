﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;

namespace ERP.Inventory.PLC.Inventory
{
    public class PLCInventoryOnWay
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/Inventory/WSInventoryOnWay.asmx";
        private WSInventoryOnWay.WSInventoryOnWay objWSInventoryOnWayProxy = null;
        private WSInventoryOnWay.ResultMessage objResultMessage = new WSInventoryOnWay.ResultMessage();
        private ResultMessageApp objResultMessageApp = new ResultMessageApp();

        #endregion
        #region Property
        private WSInventoryOnWay.WSInventoryOnWay WSInventoryOnWayProxy
        {
            get
            {
                if (objWSInventoryOnWayProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSInventoryOnWayProxy = new WSInventoryOnWay.WSInventoryOnWay();
                    objWSInventoryOnWayProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSInventoryOnWayProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSInventoryOnWayProxy.EnableDecompression = true;
                    objWSInventoryOnWayProxy.Timeout = 180000;
                }
                return objWSInventoryOnWayProxy;
            }
        }

        public ResultMessageApp ResultMessageApp
        {
            get { return objResultMessageApp; }
            set { objResultMessageApp = value; }
        }
        #endregion

        public WSInventoryOnWay.ResultMessage InventoryOnWaySearch(ref List<WSInventoryOnWay.INV_InventoryOnWay> lstInventoryOnWay, object[] objKeyWords)
        {
            try
            {
                lstInventoryOnWay = new List<WSInventoryOnWay.INV_InventoryOnWay>();
                WSInventoryOnWay.INV_InventoryOnWay[] arrINV_InventoryOnWay = null;
                objResultMessage = WSInventoryOnWayProxy.InventoryOnWaySearch(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref arrINV_InventoryOnWay, objKeyWords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInventoryOnWayProxy.InventoryOnWaySearch(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref arrINV_InventoryOnWay, objKeyWords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }

                if (arrINV_InventoryOnWay != null)
                    lstInventoryOnWay = arrINV_InventoryOnWay.ToList();

            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInventoryOnWay.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tìm kiếm dữ liệu hàng đi đường";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }

        public WSInventoryOnWay.ResultMessage InventoryOnWayDetailSearch(ref List<WSInventoryOnWay.INV_InventoryOnWayDetail> lstINV_InventoryOnWayDetail, object[] objKeyWords)
        {
            try
            {
                lstINV_InventoryOnWayDetail = new List<WSInventoryOnWay.INV_InventoryOnWayDetail>();
                WSInventoryOnWay.INV_InventoryOnWayDetail[] arrINV_InventoryOnWayDetail = null;
                objResultMessage = WSInventoryOnWayProxy.InventoryOnWayDetailSearch(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref arrINV_InventoryOnWayDetail, objKeyWords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInventoryOnWayProxy.InventoryOnWayDetailSearch(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref arrINV_InventoryOnWayDetail, objKeyWords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }

                if (arrINV_InventoryOnWayDetail != null)
                    lstINV_InventoryOnWayDetail = arrINV_InventoryOnWayDetail.ToList();
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInventoryOnWay.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tìm kiếm dữ liệu chi tiết hàng đi đường";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }

        public WSInventoryOnWay.ResultMessage SearchDataDetail(ref List<WSInventoryOnWay.INV_InventoryOnWayDetail> lstINV_InventoryOnWayDetail, ref List<WSInventoryOnWay.INV_InventoryOnWay_AttachMent> lstINV_InventoryOnWay_AttachMent, object[] objKeyWords)
        {
            try
            {
                lstINV_InventoryOnWayDetail = new List<WSInventoryOnWay.INV_InventoryOnWayDetail>();
                lstINV_InventoryOnWay_AttachMent = new List<WSInventoryOnWay.INV_InventoryOnWay_AttachMent>();
                WSInventoryOnWay.INV_InventoryOnWayDetail[] arrINV_InventoryOnWayDetail = null;
                WSInventoryOnWay.INV_InventoryOnWay_AttachMent[] arrINV_InventoryOnWay_AttachMent = null;
                objResultMessage = WSInventoryOnWayProxy.SearchDataDetail(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref arrINV_InventoryOnWayDetail, ref arrINV_InventoryOnWay_AttachMent, objKeyWords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInventoryOnWayProxy.SearchDataDetail(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref arrINV_InventoryOnWayDetail, ref arrINV_InventoryOnWay_AttachMent,objKeyWords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }

                if (arrINV_InventoryOnWayDetail != null)
                    lstINV_InventoryOnWayDetail = arrINV_InventoryOnWayDetail.ToList();
                if (arrINV_InventoryOnWay_AttachMent != null)
                    lstINV_InventoryOnWay_AttachMent = arrINV_InventoryOnWay_AttachMent.ToList();
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInventoryOnWay.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tìm kiếm dữ liệu chi tiết hàng đi đường";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }

        public WSInventoryOnWay.ResultMessage InsertAndUpdate(List<WSInventoryOnWay.INV_InventoryOnWay> lstINV_InventoryOnWay, Boolean bolIsConfirm)
        {
            try
            {
                objResultMessage = WSInventoryOnWayProxy.InsertAndUpdate(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, lstINV_InventoryOnWay.ToArray(), bolIsConfirm);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInventoryOnWayProxy.InsertAndUpdate(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, lstINV_InventoryOnWay.ToArray(), bolIsConfirm);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInventoryOnWay.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật dữ liệu chi tiết hàng đi đường";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }


        public WSInventoryOnWay.ResultMessage AutoListInputVoucher(string lstInputVoucher,int isInputStore,string user,DateTime time)
        {
            try
            {
                objResultMessage = WSInventoryOnWayProxy.AutoListInputVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, lstInputVoucher, isInputStore, user, time);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInventoryOnWayProxy.AutoListInputVoucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, lstInputVoucher, isInputStore, user, time);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInventoryOnWay.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tự động giải trình hệ thống";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }

    }
}
