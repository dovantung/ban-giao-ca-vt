﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.AppCore;
using System.Data;

namespace ERP.Inventory.PLC.Inventory
{
    /// <summary>
    /// Created by : Nguyễn Văn Tài
    /// Created date : 04/07/2017
    /// </summary>
    public class PLCPInvoice
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/WSInventoryPrice.asmx";
        private WSInventoryPrice.WSInventoryPrice objWSInventoryPriceProxy = null;
        private WSInventoryPrice.ResultMessage objResultMessage = new WSInventoryPrice.ResultMessage();

        #endregion
        #region Property
        private WSInventoryPrice.WSInventoryPrice WSInventoryPriceProxy
        {
            get
            {
                if (objWSInventoryPriceProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSInventoryPriceProxy = new WSInventoryPrice.WSInventoryPrice();
                    objWSInventoryPriceProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSInventoryPriceProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSInventoryPriceProxy.EnableDecompression = true;
                    objWSInventoryPriceProxy.Timeout = 180000;
                }
                return objWSInventoryPriceProxy;
            }
        }
        #endregion

        public PLC.WSInventoryPrice.ResultMessage SearchData(ref DataTable dtbData, object[] objKeyword)
        {
            PLC.WSInventoryPrice.ResultMessage objResultMessage = new WSInventoryPrice.ResultMessage();
            try
            {
                objResultMessage = WSInventoryPriceProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeyword);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInventoryPriceProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeyword);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PLC.WSInventoryPrice.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin hóa đơn";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }

        public PLC.WSInventoryPrice.ResultMessage Insert(ref WSInventoryPrice.VatPInvoice objPInvoice, string[] arrPriceProtectID)
        {
            try
            {
                objResultMessage = WSInventoryPriceProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objPInvoice, arrPriceProtectID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSInventoryPriceProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objPInvoice, arrPriceProtectID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSInventoryPrice.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin hóa đơn hỗ trợ giá hàng tồn";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }
    }
}
