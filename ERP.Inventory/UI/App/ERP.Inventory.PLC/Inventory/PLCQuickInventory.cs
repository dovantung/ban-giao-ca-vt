﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;

namespace ERP.Inventory.PLC.Inventory
{
    public class PLCQuickInventory
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/Inventory/WSQuickInventory.asmx";
        private WSQuickInventory.WSQuickInventory objWSQuickInventoryProxy = null;
        private WSQuickInventory.ResultMessage objResultMessage = new WSQuickInventory.ResultMessage();
        private ResultMessageApp objResultMessageApp = new ResultMessageApp();

        #endregion
        #region Property
        private WSQuickInventory.WSQuickInventory WSQuickInventoryProxy
        {
            get
            {
                if (objWSQuickInventoryProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSQuickInventoryProxy = new WSQuickInventory.WSQuickInventory();
                    objWSQuickInventoryProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSQuickInventoryProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSQuickInventoryProxy.EnableDecompression = true;
                    objWSQuickInventoryProxy.Timeout = 180000;
                }
                return objWSQuickInventoryProxy;
            }
        }

        public ResultMessageApp ResultMessageApp
        {
            get { return objResultMessageApp; }
            set { objResultMessageApp = value; }
        }
        #endregion
        #region Phương thức
        public DataTable SearchData(int intStoreID, string strMainGroupListID, string strInventoryUser, string strReviewedUser,
            DateTime dtmInventoryDateFrom, DateTime dtmInventoryDateTo, DateTime dtmLockStockTimeFrom, DateTime dtmLockStockTimeTo, int intIsDeleted, int intIsReviewed, string strKeyword, int intType)
        {
            DataTable dtbQuickInventoryData = null;
            try
            {
                objResultMessage = WSQuickInventoryProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbQuickInventoryData, intStoreID, strMainGroupListID, strInventoryUser, strReviewedUser, dtmInventoryDateFrom, dtmInventoryDateTo, dtmLockStockTimeFrom, dtmLockStockTimeTo, intIsDeleted, intIsReviewed, strKeyword, intType);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSQuickInventoryProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbQuickInventoryData, intStoreID, strMainGroupListID, strInventoryUser, strReviewedUser, dtmInventoryDateFrom, dtmInventoryDateTo, dtmLockStockTimeFrom, dtmLockStockTimeTo, intIsDeleted, intIsReviewed, strKeyword, intType);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSQuickInventory.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách phiếu kiểm kê nhanh";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbQuickInventoryData;
        }

        public DataTable GetDetail(string strQuickInventoryID)
        {
            DataTable dtbDetail = null;
            try
            {
                objResultMessage = WSQuickInventoryProxy.GetDetail(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDetail, strQuickInventoryID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSQuickInventoryProxy.GetDetail(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDetail, strQuickInventoryID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSQuickInventory.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy dữ liệu chi tiết phiếu kiểm kê nhanh";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDetail;
        }

        public DataTable GetListBrandID(string strQuickInventoryID)
        {
            DataTable dtbDetail = null;
            try
            {
                objResultMessage = WSQuickInventoryProxy.GetListBrandID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDetail, strQuickInventoryID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSQuickInventoryProxy.GetListBrandID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDetail, strQuickInventoryID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSQuickInventory.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách mã phiếu kiểm kê nhanh - nhà sản xuất";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDetail;
        }

        public void GetInstockData(ref DataTable dtbInStockData, ref DataTable dtbInStockIMEI, DateTime dtmLockStockTime, int intStoreID, int intMainGroupID, int intSubGroupID, string strBrandIDList, int intIsNew, int intIsCheckRealInput)
        {
            try
            {
                objResultMessage = WSQuickInventoryProxy.GetInstockData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbInStockData, ref dtbInStockIMEI, dtmLockStockTime, intStoreID, intMainGroupID, intSubGroupID, strBrandIDList, intIsNew, intIsCheckRealInput);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSQuickInventoryProxy.GetInstockData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbInStockData, ref dtbInStockIMEI, dtmLockStockTime, intStoreID, intMainGroupID, intSubGroupID, strBrandIDList, intIsNew, intIsCheckRealInput);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSQuickInventory.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy dữ liệu chi tiết phiếu kiểm kê nhanh";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
        }

        public bool Delete(string strQuickInventoryID, string strDeletedNote)
        {
            try
            {
                objResultMessage = WSQuickInventoryProxy.Delete(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strQuickInventoryID, strDeletedNote);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSQuickInventoryProxy.Delete(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strQuickInventoryID, strDeletedNote);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSQuickInventory.ErrorTypes.Delete;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi xóa phiếu kiểm kê nhanh";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }
        #region 30/11/2017: Đăng bổ sung
        public void GetInstockDataViettel(ref DataTable dtbInStockData, ref DataTable dtbInStockIMEI, DateTime dtmLockStockTime, int intStoreID, string strMainGroupIDList, string strSubGroupIDList, string strBrandIDList, int intIsNew, int intIsCheckRealInput)
        {
            try
            {
                objResultMessage = WSQuickInventoryProxy.GetInstockData1(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbInStockData, ref dtbInStockIMEI, dtmLockStockTime, intStoreID, strMainGroupIDList, strSubGroupIDList, strBrandIDList, intIsNew, intIsCheckRealInput);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSQuickInventoryProxy.GetInstockData1(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbInStockData, ref dtbInStockIMEI, dtmLockStockTime, intStoreID, strMainGroupIDList, strSubGroupIDList, strBrandIDList, intIsNew, intIsCheckRealInput);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSQuickInventory.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy dữ liệu chi tiết phiếu kiểm kê nhanh";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
        }


        public bool InsertViettel(PLC.Inventory.WSQuickInventory.QuickInventory objQuickInventory, ref string strQuickInventoryID)
        {
            try
            {
                objResultMessage = WSQuickInventoryProxy.Insert1(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objQuickInventory, ref strQuickInventoryID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSQuickInventoryProxy.Insert1(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objQuickInventory, ref strQuickInventoryID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSQuickInventory.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm mới phiếu kiểm kê";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }


        public DataTable GetListMaingroupID(string strQuickInventoryID)
        {
            DataTable dtbDetail = null;
            try
            {
                objResultMessage = WSQuickInventoryProxy.GetListMaingroupID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDetail, strQuickInventoryID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSQuickInventoryProxy.GetListMaingroupID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDetail, strQuickInventoryID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSQuickInventory.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách mã phiếu kiểm kê nhanh - nhà sản xuất";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDetail;
        }

        public DataTable GetListSubGroupID(string strQuickInventoryID)
        {
            DataTable dtbDetail = null;
            try
            {
                objResultMessage = WSQuickInventoryProxy.GetListSubGroupID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDetail, strQuickInventoryID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSQuickInventoryProxy.GetListSubGroupID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDetail, strQuickInventoryID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSQuickInventory.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách mã phiếu kiểm kê nhanh - nhà sản xuất";
                objResultMessage.MessageDetail = objExc.ToString();
                Library.AppCore.SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            objResultMessageApp = new Library.AppCore.ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDetail;
        }
        #endregion

        #endregion;
    }
}
