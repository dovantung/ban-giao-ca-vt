﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.AppCore;
using System.Data;

namespace ERP.Inventory.PLC.PM
{
    public class PLCLotIMEISalesInfo_Comment
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/PM/WSLotIMEISalesInfo_Comment.asmx";
        private WSLotIMEISalesInfo_Comment.WSLotIMEISalesInfo_Comment objWSLotIMEISalesInfo_CommentProxy = null;
        private WSLotIMEISalesInfo_Comment.ResultMessage objResultMessage = new WSLotIMEISalesInfo_Comment.ResultMessage();
        #endregion
        #region Property
        private WSLotIMEISalesInfo_Comment.WSLotIMEISalesInfo_Comment WSLotIMEISalesInfo_CommentProxy
        {
            get
            {
                if (objWSLotIMEISalesInfo_CommentProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSLotIMEISalesInfo_CommentProxy = new WSLotIMEISalesInfo_Comment.WSLotIMEISalesInfo_Comment();
                    objWSLotIMEISalesInfo_CommentProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSLotIMEISalesInfo_CommentProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSLotIMEISalesInfo_CommentProxy.EnableDecompression = true;
                    objWSLotIMEISalesInfo_CommentProxy.Timeout = 180000;
                }
                return objWSLotIMEISalesInfo_CommentProxy;
            }
        }
        #endregion
        #region Method
        public WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment LoadInfo(int intCommentID)
        {
            WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment = null;
            try
            {
                objResultMessage = WSLotIMEISalesInfo_CommentProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objLotIMEISalesInfo_Comment, intCommentID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSLotIMEISalesInfo_CommentProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objLotIMEISalesInfo_Comment, intCommentID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSLotIMEISalesInfo_Comment.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin bình luận bán hàng của lô IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objLotIMEISalesInfo_Comment;
        }

        public bool Insert(WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment)
        {
            string strInputVoucherID = string.Empty;
            try
            {
                objResultMessage = WSLotIMEISalesInfo_CommentProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objLotIMEISalesInfo_Comment);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSLotIMEISalesInfo_CommentProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objLotIMEISalesInfo_Comment);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSLotIMEISalesInfo_Comment.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin bình luận bán hàng của lô IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool Update(WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment)
        {
            try
            {
                objResultMessage = WSLotIMEISalesInfo_CommentProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objLotIMEISalesInfo_Comment);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSLotIMEISalesInfo_CommentProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objLotIMEISalesInfo_Comment);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSLotIMEISalesInfo_Comment.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật thông tin bình luận bán hàng của lô IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool DeleteList(List<WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment> lstLotIMEISalesInfo_Comment)
        {
            try
            {
                objResultMessage = WSLotIMEISalesInfo_CommentProxy.DeteteList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, lstLotIMEISalesInfo_Comment.ToArray());
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSLotIMEISalesInfo_CommentProxy.DeteteList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, lstLotIMEISalesInfo_Comment.ToArray());
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSLotIMEISalesInfo_Comment.ErrorTypes.Delete;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi xóa thông tin bình luận bán hàng của lô IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public DataTable SearchData(object[] objKeywords)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSLotIMEISalesInfo_CommentProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSLotIMEISalesInfo_CommentProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSLotIMEISalesInfo_Comment.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Tìm kiếm bình luận bán hàng của lô IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }

        public bool UpdateData(WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment_Update, WSLotIMEISalesInfo_Comment.LotIMEISalesInfo_Comment objLotIMEISalesInfo_Comment_Insert)
        {
            try
            {
                objResultMessage = WSLotIMEISalesInfo_CommentProxy.UpdateData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objLotIMEISalesInfo_Comment_Update, objLotIMEISalesInfo_Comment_Insert);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSLotIMEISalesInfo_CommentProxy.UpdateData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objLotIMEISalesInfo_Comment_Update, objLotIMEISalesInfo_Comment_Insert);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSLotIMEISalesInfo_Comment.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật thông tin bình luận bán hàng của lô IMEI";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }
        #endregion
    }
}
