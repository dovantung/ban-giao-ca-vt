﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.AppCore;
using System.Data;

namespace ERP.Inventory.PLC.PM
{
    public class PLCIMEIExcludeFIFORequest
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/PM/WSIMEIExcludeFIFORequest.asmx";
        private WSIMEIExcludeFIFORequest.WSIMEIExcludeFIFORequest objWSIMEIExcludeFIFORequestProxy = null;
        private WSIMEIExcludeFIFORequest.ResultMessage objResultMessage = new WSIMEIExcludeFIFORequest.ResultMessage();
        #endregion
        #region Property
        private WSIMEIExcludeFIFORequest.WSIMEIExcludeFIFORequest WSIMEIExcludeFIFORequestProxy
        {
            get
            {
                if (objWSIMEIExcludeFIFORequestProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSIMEIExcludeFIFORequestProxy = new WSIMEIExcludeFIFORequest.WSIMEIExcludeFIFORequest();
                    objWSIMEIExcludeFIFORequestProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSIMEIExcludeFIFORequestProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSIMEIExcludeFIFORequestProxy.EnableDecompression = true;
                    objWSIMEIExcludeFIFORequestProxy.Timeout = 180000;
                }
                return objWSIMEIExcludeFIFORequestProxy;
            }
        }
        #endregion
        #region Method
        public WSIMEIExcludeFIFORequest.IMEIExcludeFIFORequest LoadInfo(string strIMEIExcludeFIFORequestID)
        {
            WSIMEIExcludeFIFORequest.IMEIExcludeFIFORequest objIMEIExcludeFIFORequest = null;
            try
            {
                objResultMessage = WSIMEIExcludeFIFORequestProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objIMEIExcludeFIFORequest, strIMEIExcludeFIFORequestID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSIMEIExcludeFIFORequestProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objIMEIExcludeFIFORequest, strIMEIExcludeFIFORequestID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSIMEIExcludeFIFORequest.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin Yêu cầu IMEI vi phạm FIFO";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objIMEIExcludeFIFORequest;
        }

        public bool Insert(WSIMEIExcludeFIFORequest.IMEIExcludeFIFORequest objIMEIExcludeFIFORequest)
        {
            string strInputVoucherID = string.Empty;
            try
            {
                objResultMessage = WSIMEIExcludeFIFORequestProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objIMEIExcludeFIFORequest);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSIMEIExcludeFIFORequestProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objIMEIExcludeFIFORequest);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSIMEIExcludeFIFORequest.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin Yêu cầu IMEI vi phạm FIFO";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool Update(WSIMEIExcludeFIFORequest.IMEIExcludeFIFORequest objIMEIExcludeFIFORequest)
        {
            try
            {
                objResultMessage = WSIMEIExcludeFIFORequestProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objIMEIExcludeFIFORequest);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSIMEIExcludeFIFORequestProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objIMEIExcludeFIFORequest);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSIMEIExcludeFIFORequest.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật thông tin Yêu cầu IMEI vi phạm FIFO";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool Delete(WSIMEIExcludeFIFORequest.IMEIExcludeFIFORequest objIMEIExcludeFIFORequest)
        {
            int intResult = 0;
            try
            {
                objResultMessage = WSIMEIExcludeFIFORequestProxy.Detete(ref intResult, PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objIMEIExcludeFIFORequest);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSIMEIExcludeFIFORequestProxy.Detete(ref intResult, PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objIMEIExcludeFIFORequest);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSIMEIExcludeFIFORequest.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi xóa thông tin Yêu cầu IMEI vi phạm FIFO";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public DataTable SearchData(object[] objKeywords)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSIMEIExcludeFIFORequestProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSIMEIExcludeFIFORequestProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSIMEIExcludeFIFORequest.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Tìm kiếm Yêu cầu IMEI vi phạm FIFO";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }

        public string GetIMEIExcludeFIFORequestNewID(int intStoreID)
        {
            string strIMEIExcludeFIFORequestID = string.Empty;
            try
            {
                objResultMessage = WSIMEIExcludeFIFORequestProxy.GetIMEIExcludeFIFORequestNewID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strIMEIExcludeFIFORequestID, intStoreID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSIMEIExcludeFIFORequestProxy.GetIMEIExcludeFIFORequestNewID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strIMEIExcludeFIFORequestID, intStoreID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSIMEIExcludeFIFORequest.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy mã yêu cầu IMEI vi phạm FIFO";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strIMEIExcludeFIFORequestID;
        }

        #endregion
    }
}
