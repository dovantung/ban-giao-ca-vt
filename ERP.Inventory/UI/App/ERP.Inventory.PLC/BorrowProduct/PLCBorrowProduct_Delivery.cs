﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;

namespace ERP.Inventory.PLC.BorrowProduct
{
    public class PLCBorrowProduct_Delivery
    {

        #region Variable
        private const String WS_CLASS_PATH = "Inventory/BorrowProduct/WSBorrowProduct_Delivery.asmx";
        private WSBorrowProduct_Delivery.WSBorrowProduct_Delivery objWSBorrowProduct_DeliveryProxy = null;
        private WSBorrowProduct_Delivery.ResultMessage objResultMessage = new WSBorrowProduct_Delivery.ResultMessage();
        private ResultMessageApp objResultMessageApp = new ResultMessageApp();

        #endregion

        #region Property
        private WSBorrowProduct_Delivery.WSBorrowProduct_Delivery WSBorrowProduct_DeliveryProxy
        {
            get
            {
                if (objWSBorrowProduct_DeliveryProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSBorrowProduct_DeliveryProxy = new WSBorrowProduct_Delivery.WSBorrowProduct_Delivery();
                    objWSBorrowProduct_DeliveryProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSBorrowProduct_DeliveryProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSBorrowProduct_DeliveryProxy.EnableDecompression = true;
                    objWSBorrowProduct_DeliveryProxy.Timeout = 180000;
                }
                return objWSBorrowProduct_DeliveryProxy;
            }
        }
        #endregion;

        #region Constructor
        public PLCBorrowProduct_Delivery()
        {

        }
        #endregion

        #region Public Methods
        public WSBorrowProduct_Delivery.ResultMessage LoadInfo(ref WSBorrowProduct_Delivery.BorrowProduct_Delivery objBorrowProduct_Delivery, string strBorrowProductID)
        {
            try
            {
                objResultMessage = WSBorrowProduct_DeliveryProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objBorrowProduct_Delivery, strBorrowProductID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSBorrowProduct_DeliveryProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objBorrowProduct_Delivery, strBorrowProductID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSBorrowProduct_Delivery.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin chứng từ mượn hàng hóa";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }

        public WSBorrowProduct_Delivery.ResultMessage Insert(WSBorrowProduct_Delivery.BorrowProduct_Delivery objBorrowProduct_Delivery,ref string strBorrowProductID)
        {
            try
            {
                objResultMessage = WSBorrowProduct_DeliveryProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objBorrowProduct_Delivery,ref strBorrowProductID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSBorrowProduct_DeliveryProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objBorrowProduct_Delivery,ref strBorrowProductID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSBorrowProduct_Delivery.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin chứng từ mượn hàng hóa";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }

        public WSBorrowProduct_Delivery.ResultMessage Update(WSBorrowProduct_Delivery.BorrowProduct_Delivery objBorrowProduct_Delivery)
        {
            try
            {
                objResultMessage = WSBorrowProduct_DeliveryProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objBorrowProduct_Delivery);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSBorrowProduct_DeliveryProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objBorrowProduct_Delivery);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSBorrowProduct_Delivery.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật thông tin chứng từ mượn hàng hóa";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }

        public WSBorrowProduct_Delivery.ResultMessage Delete(WSBorrowProduct_Delivery.BorrowProduct_Delivery objBorrowProduct_Delivery)
        {
            try
            {
                objResultMessage = WSBorrowProduct_DeliveryProxy.Delete(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objBorrowProduct_Delivery);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSBorrowProduct_DeliveryProxy.Delete(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objBorrowProduct_Delivery);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSBorrowProduct_Delivery.ErrorTypes.Delete;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi xóa thông tin chứng từ mượn hàng hóa";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }

        public WSBorrowProduct_Delivery.ResultMessage SearchData(ref DataTable dtbResult, params object[] objKeywords)
        {
            try
            {
                objResultMessage = WSBorrowProduct_DeliveryProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSBorrowProduct_DeliveryProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSBorrowProduct_Delivery.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách chứng từ mượn hàng hóa";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }
        #endregion

    }
}
