﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;

namespace ERP.Inventory.PLC.StoreChange
{
    public class PLCStoreChangeOrder
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/StoreChange/WSStoreChangeOrder.asmx";
        private PM.WSStoreChangeOrder.WSStoreChangeOrder objWSStoreChangeOrderProxy = null;
        private PM.WSStoreChangeOrder.ResultMessage objResultMessage = new PM.WSStoreChangeOrder.ResultMessage();
        //
        private const String WS_CLASS_PATH2 = "TransportOrder/WSTransportOrder.asmx";
        private WSTransportOrder.WSTransportOrder objWSTransportOrderProxy = null;

        #endregion
        #region Property
        private PM.WSStoreChangeOrder.WSStoreChangeOrder WSStoreChangeOrderProxy
        {
            get
            {
                if (objWSStoreChangeOrderProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSStoreChangeOrderProxy = new PM.WSStoreChangeOrder.WSStoreChangeOrder();
                    objWSStoreChangeOrderProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSStoreChangeOrderProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSStoreChangeOrderProxy.EnableDecompression = true;
                    objWSStoreChangeOrderProxy.Timeout = 180000;
                }
                return objWSStoreChangeOrderProxy;
            }
        }


        private WSTransportOrder.WSTransportOrder WSTransportOrderProxy
        {
            get
            {
                if (objWSTransportOrderProxy == null || !PLCInventory_Globals.WebServiceHostTransport.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransport.HandShake();
                    objWSTransportOrderProxy = new WSTransportOrder.WSTransportOrder();
                    objWSTransportOrderProxy.Url = PLCInventory_Globals.WebServiceHostTransport.HostURL + WS_CLASS_PATH2;
                    objWSTransportOrderProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransport.CookieContainer;
                    objWSTransportOrderProxy.EnableDecompression = true;
                    objWSTransportOrderProxy.Timeout = 180000;
                }
                return objWSTransportOrderProxy;
            }
        }


        #endregion
        #region Phương thức


        /// <summary>
        /// Gui van don sang ViettelPost
        /// </summary>
        /// <param name="objTP_TransportOrder"></param>
        /// <returns></returns>
        public WSTransportOrder.ResultMessage SendRequest(WSTransportOrder.TP_TransportOrder objTP_TransportOrder, string strWorkFLowID)
        {
            WSTransportOrder.ResultMessage objResultMessage = new WSTransportOrder.ResultMessage();
            try
            {
                objResultMessage = WSTransportOrderProxy.SendRequest(PLCInventory_Globals.WebServiceHostTransport.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransport.GUID, objTP_TransportOrder, strWorkFLowID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransport.IsHandShake = false;
                    objResultMessage = WSTransportOrderProxy.SendRequest(PLCInventory_Globals.WebServiceHostTransport.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransport.GUID, objTP_TransportOrder, strWorkFLowID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSTransportOrder.ErrorTypes.Others;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi gửi thông tin vận đơn";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }
        public PM.WSStoreChangeOrder.StoreChangeOrder LoadInfo(string strStoreChangeOrderID, int intStoreChangeOrderTypeID)
        {
            PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrder = null;
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objStoreChangeOrder, strStoreChangeOrderID, intStoreChangeOrderTypeID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objStoreChangeOrder, strStoreChangeOrderID, intStoreChangeOrderTypeID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin yêu cầu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objStoreChangeOrder;
        }
        public string GetStoreChangeOrderNewID(int intStoreID)
        {
            string strStoreChangeOrderID = string.Empty;
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.GetStoreChangeOrderNewID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strStoreChangeOrderID, intStoreID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.GetStoreChangeOrderNewID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strStoreChangeOrderID, intStoreID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy mã yêu cầu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strStoreChangeOrderID;
        }


        public string Insert(PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrder)
        {
            string strStoreChangeOrderID = string.Empty;
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeOrder, ref strStoreChangeOrderID);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.Insert(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeOrder, ref strStoreChangeOrderID);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin yêu cầu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strStoreChangeOrderID;
        }
        public bool InsertStoreChangeOrderAttachment(PM.WSStoreChangeOrder.StoreChangeOrderAttachment objStoreChangeOrderAttachment)
        {
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.InsertStoreChangeOrderAttachment(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeOrderAttachment);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.InsertStoreChangeOrderAttachment(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeOrderAttachment);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin file đính kèm";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        //HUENT66
        public bool ReturnInstock(PM.WSStoreChangeOrder.StoreChangeOrder StoreChangeOrder)
        {
            bool checkreturn = false;
            try
            {
                checkreturn = WSStoreChangeOrderProxy.ReturnInstock(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, StoreChangeOrder);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    checkreturn = WSStoreChangeOrderProxy.ReturnInstock(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, StoreChangeOrder);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra tồn kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return checkreturn;
        }

        public bool UpdateStoreChangeOrderAttachment(PM.WSStoreChangeOrder.StoreChangeOrderAttachment objStoreChangeOrderAttachment)
        {
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.UpdateStoreChangeOrderAttachment(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeOrderAttachment);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.UpdateStoreChangeOrderAttachment(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeOrderAttachment);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật thông tin file đính kèm";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }


        public bool Update(PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrder)
        {
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeOrder);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.Update(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeOrder);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật thông tin yêu cầu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public bool CreateStoreChangeOrder(PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrder, DataTable tblStoreChangeCommand, DataTable tblFromStore)
        {
            objResultMessage = new PM.WSStoreChangeOrder.ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.CreateStoreChangeOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID,
                    objStoreChangeOrder, tblStoreChangeCommand, tblFromStore);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.CreateStoreChangeOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID,
                    objStoreChangeOrder, tblStoreChangeCommand, tblFromStore);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tạo yêu cầu chuyển kho từ lệnh chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        /// <summary>
        /// Đảo hàng từ kho chính sang kho phụ và ngược lại
        /// </summary>
        /// <param name="objStoreChangeOrder"></param>
        /// <param name="tblStoreChangeDetail"></param>
        /// <returns></returns>
        public bool CreateStoreChangeOrderShowProduct(PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrder, DataTable tblStoreChangeDetail)
        {
            objResultMessage = new PM.WSStoreChangeOrder.ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.CreateStoreChangeOrderShowProduct(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID,
                    objStoreChangeOrder, tblStoreChangeDetail);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.CreateStoreChangeOrderShowProduct(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID,
                      objStoreChangeOrder, tblStoreChangeDetail);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tạo yêu cầu chuyển kho từ đảo hàng bày mẫu";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        /// <summary>
        /// Chuyển từ kho chính sang kho phụ
        /// </summary>
        /// <param name="objStoreChangeOrder"></param>
        /// <param name="tblStoreChangeDetail"></param>
        /// <returns></returns>
        public bool CreateStoreChangeOrderShowProduct_1(PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrder, DataTable tblStoreChangeDetail)
        {
            objResultMessage = new PM.WSStoreChangeOrder.ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.CreateStoreChangeOrderShowProduct_1(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID,
                    objStoreChangeOrder, tblStoreChangeDetail);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.CreateStoreChangeOrderShowProduct_1(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID,
                      objStoreChangeOrder, tblStoreChangeDetail);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Chuyển từ kho chính sang kho phụ";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }


        public bool Delete(PM.WSStoreChangeOrder.StoreChangeOrder objStoreChangeOrder)
        {
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.Delete(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeOrder);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.Delete(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeOrder);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi xóa thông tin yêu cầu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }



        public bool DeleteList(List<PM.WSStoreChangeOrder.StoreChangeOrder> lstStoreChangeOrder)
        {
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.DeleteList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, lstStoreChangeOrder.ToArray());
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.DeleteList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, lstStoreChangeOrder.ToArray());
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi xóa thông tin yêu cầu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }
        /// <summary>
        /// Tìm kiếm yêu cầu chuyển kho
        /// </summary>
        /// <param name="strStoreChangeOrderID"></param>
        /// <returns></returns>
        public DataTable SearchData(object[] objKeywords)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Tìm kiếm yêu cầu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }
        /// <summary>
        /// Tìm kiếm yêu cầu chuyển kho khác
        /// </summary>
        /// <param name="strStoreChangeOrderID"></param>
        /// <returns></returns>
        public DataTable SearchDataOtherTransfer(object[] objKeywords)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.SearchDataOtherTransfer(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.SearchDataOtherTransfer(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Tìm kiếm yêu cầu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }

        /// <summary>
        /// 
        /// Lấy danh sách maingroup từ chi tiết yêu cầu chuyển kho
        /// </summary>
        /// <param name="strStoreChangeOrderID"></param>
        /// <returns></returns>
        public DataTable CreateMainGroupMenuItem(string strStoreChangeOrderID)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.CreateMainGroupMenuItem(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strStoreChangeOrderID, ref dtbData);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.CreateMainGroupMenuItem(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strStoreChangeOrderID, ref dtbData);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Lấy danh sách maingroup từ chi tiết yêu cầu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }

        /////
        ///// load danh sach chi tiet phieu y.c ck
        ///// PM_StoreChangeOrderDetail_LSC
        ///// </summary>
        //public DataTable LoadDetailForStoreChange(int intMainGroupID, string strStoreChangeOrderID)
        //{
        //    DataTable dtbData = null;
        //    try
        //    {
        //        objResultMessage = WSStoreChangeOrderProxy.LoadDetailForStoreChange(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
        //            PLCInventory_Globals.WebServiceHostTransactionServices.GUID, intMainGroupID, strStoreChangeOrderID, ref dtbData);
        //        if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
        //        {
        //            PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
        //            objResultMessage = WSStoreChangeOrderProxy.LoadDetailForStoreChange(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
        //            PLCInventory_Globals.WebServiceHostTransactionServices.GUID, intMainGroupID, strStoreChangeOrderID, ref dtbData);
        //            Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
        //        }
        //    }
        //    catch (System.Exception objExc)
        //    {
        //        objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.GetData;
        //        objResultMessage.IsError = true;
        //        objResultMessage.Message = "Lỗi Lấy danh sách maingroup từ chi tiết yêu cầu chuyển kho";
        //        objResultMessage.MessageDetail = objExc.ToString();
        //        SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
        //            PLCInventory_Globals.ModuleName);
        //    }
        //    new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
        //    return dtbData;
        //}

        ///
        /// load danh sach chi tiet phieu y.c ck
        /// PM_StoreChangeOrderDetail_LSC
        /// </summary>
        public DataTable LoadDetailForStoreChange(string strMainGroupIDList, string strStoreChangeOrderID, int intInputTypeID, int intOutputTypeID)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.LoadDetailForStoreChange(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strMainGroupIDList, strStoreChangeOrderID, intInputTypeID, intOutputTypeID, ref dtbData);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.LoadDetailForStoreChange(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strMainGroupIDList, strStoreChangeOrderID, intInputTypeID, intOutputTypeID, ref dtbData);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Lấy danh sách maingroup từ chi tiết yêu cầu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }

        ///// <summary>
        ///// Nạp danh sách xuất chuyển kho cho nhiều ngành hàng
        ///// </summary>
        ///// <param name="ListMainGroupID"></param>
        ///// <param name="strStoreChangeOrderID"></param>
        ///// <returns></returns>
        //public DataTable LoadDetailForStoreChange(List<string> ListMainGroupID, string strStoreChangeOrderID)
        //{
        //    DataTable dtbData = null;
        //    try
        //    {
        //        objResultMessage = WSStoreChangeOrderProxy.LoadDetailForStoreChangeList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
        //            PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ListMainGroupID.ToArray(), strStoreChangeOrderID, ref dtbData);
        //        if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
        //        {
        //            PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
        //            objResultMessage = WSStoreChangeOrderProxy.LoadDetailForStoreChangeList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
        //            PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ListMainGroupID.ToArray(), strStoreChangeOrderID, ref dtbData);
        //            Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
        //        }
        //    }
        //    catch (System.Exception objExc)
        //    {
        //        objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.GetData;
        //        objResultMessage.IsError = true;
        //        objResultMessage.Message = "Lỗi Lấy danh sách maingroup từ chi tiết yêu cầu chuyển kho";
        //        objResultMessage.MessageDetail = objExc.ToString();
        //        SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
        //            PLCInventory_Globals.ModuleName);
        //    }
        //    new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
        //    return dtbData;
        //}

        /// load danh sach chi tiet phieu y.c ck
        /// PM_STORECHANGEORDER_IDFRCMD
        /// </summary>
        public DataTable LoadStoreChangeOrderDetailFromCommand(string strStoreChangeCommandIDList)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.LoadStoreChangeOrderDetailFromCommand(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strStoreChangeCommandIDList, ref dtbData);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.LoadStoreChangeOrderDetailFromCommand(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strStoreChangeCommandIDList, ref dtbData);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Lấy danh sách maingroup từ chi tiết yêu cầu chuyển kho từ lệnh chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }

        public bool ReviewStoreChangeOrder(string strStoreChangeOrderID)
        {
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.ReviewStoreChangeOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strStoreChangeOrderID);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.ReviewStoreChangeOrder(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strStoreChangeOrderID);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi duyệt thông tin yêu cầu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        /// <summary>
        /// 
        /// Nạp danh sách quyền trên ngành hàng theo user tự truyền
        /// </summary>
        /// <param name="strStoreChangeOrderID"></param>
        /// <returns></returns>
        public DataTable LoadMainGroupPermission(string strUserName)
        {
            DataTable dtbData = null;
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.LoadMainGroupPermission(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strUserName, ref dtbData);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.LoadMainGroupPermission(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, strUserName, ref dtbData);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách quyền trên ngành hàng";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbData;
        }

        public bool CheckStorePermission(int intFromStoreID, int intToStoreID, string strUserName, ref bool bolFromStorePermission, ref bool bolToStorePermission)
        {
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.CheckStorePermission(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, intFromStoreID, intToStoreID, strUserName, ref bolFromStorePermission, ref bolToStorePermission);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.CheckStorePermission(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, intFromStoreID, intToStoreID, strUserName, ref bolFromStorePermission, ref bolToStorePermission);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PM.WSStoreChangeOrder.ErrorTypes.CheckData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra thông tin quyền trên kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }


        public PLC.PM.WSStoreChangeOrder.ResultMessage GetOutputAll(ref DataTable dtbData, object[] objKeyword)
        {
            PLC.PM.WSStoreChangeOrder.ResultMessage objResultMessage = new PM.WSStoreChangeOrder.ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeOrderProxy.GetOutputAll(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeyword);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeOrderProxy.GetOutputAll(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbData, objKeyword);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = PLC.PM.WSStoreChangeOrder.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi kiểm tra thông tin xuất tất cả ngành hàng trên yêu cầu xuất chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            return objResultMessage;
        }


        #endregion
    }
}
