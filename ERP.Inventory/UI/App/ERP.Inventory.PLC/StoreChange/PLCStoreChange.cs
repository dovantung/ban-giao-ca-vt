﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Library.AppCore;

namespace ERP.Inventory.PLC.StoreChange
{
    /// <summary>
    /// Nguyễn Linh Tuấn
    /// </summary>
    public class PLCStoreChange
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/StoreChange/WSStoreChange.asmx";
        private StoreChange.WSStoreChange.WSStoreChange objWSStoreChangeProxy = null;
        private StoreChange.WSStoreChange.ResultMessage objResultMessage = new StoreChange.WSStoreChange.ResultMessage();
        #endregion
        #region Property
        private StoreChange.WSStoreChange.WSStoreChange WSStoreChangeProxy
        {
            get
            {
                if (objWSStoreChangeProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSStoreChangeProxy = new StoreChange.WSStoreChange.WSStoreChange();
                    objWSStoreChangeProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSStoreChangeProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSStoreChangeProxy.EnableDecompression = true;
                    objWSStoreChangeProxy.Timeout = 1800000;
                }
                return objWSStoreChangeProxy;
            }
        }
        #endregion
        #region Phương thức

        public StoreChange.WSStoreChange.StoreChange LoadInfo(string strStoreChangeID)
        {
            StoreChange.WSStoreChange.StoreChange objStoreChange = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objStoreChange, strStoreChangeID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.LoadInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objStoreChange, strStoreChangeID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = StoreChange.WSStoreChange.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin xuất chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objStoreChange;
        }

        public StoreChange.WSStoreChange.StoreChange LoadInfo(string strStoreChangeID,string outputvoucherid,string inputvoucherid,DateTime storechangedate)
        {
            StoreChange.WSStoreChange.StoreChange objStoreChange = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.LoadInfo_v1(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objStoreChange, strStoreChangeID,outputvoucherid,inputvoucherid,storechangedate);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.LoadInfo_v1(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref objStoreChange, strStoreChangeID, outputvoucherid, inputvoucherid, storechangedate);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = StoreChange.WSStoreChange.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin xuất chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objStoreChange;
        }

        public string GetStoreChangeNewID(int intStoreID)
        {
            string strStoreChangeID = string.Empty;
            try
            {
                objResultMessage = WSStoreChangeProxy.GetStoreChangeNewID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strStoreChangeID, intStoreID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.GetStoreChangeNewID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref strStoreChangeID, intStoreID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = StoreChange.WSStoreChange.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy mã xuất chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strStoreChangeID;
        }

        /// <summary>
        /// Tìm kiếm dữ liệu 
        /// PM_STORECHANGE_SRH
        /// </summary>
        /// <param name="objKeywords"></param>
        /// <returns></returns>
        public DataTable SearchData(object[] objKeywords)
        {
            DataTable dtbDataResult = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = StoreChange.WSStoreChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tìm kiếm phiếu xuất chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDataResult;
        }

        public DataTable LoadStoreChangeDetail(string strStoreChangeID)
        {
            DataTable dtbDataResult = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.LoadStoreChangeDetail(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, strStoreChangeID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.LoadStoreChangeDetail(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, strStoreChangeID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = StoreChange.WSStoreChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi load chi tiet phiếu xuất chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDataResult;
        }

        /// <summary>
        /// Lấy thông tin kích thước tối đa của hàng hóa của phiếu chuyển kho
        /// PM_StoreChange_TransferedList
        /// </summary>
        /// <param name="strStoreChangeIDList"></param>
        /// <returns></returns>
        public DataTable GetProductInfoMaxSize(string strStoreChangeIDList)
        {
            DataTable dtbDataResult = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.GetProductInfoMaxSize(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, strStoreChangeIDList);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.GetProductInfoMaxSize(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, strStoreChangeIDList);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = StoreChange.WSStoreChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Lấy thông tin hàng hóa có kích thước tối đa của phiếu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDataResult;
        }

        public string InsertStoreChange(DataTable dtbStoreChangeDetail, StoreChange.WSStoreChange.StoreChange objStoreChange, ref string strOutPutVoucherID, int intStoreChangeOrderTypeID = 0, int intCreateVATInvoice = 0)
        {
            string strStoreChangeID = string.Empty;
            try
            {
                objResultMessage = WSStoreChangeProxy.InsertStoreChange(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, dtbStoreChangeDetail, objStoreChange, ref strStoreChangeID, ref strOutPutVoucherID,  intStoreChangeOrderTypeID, intCreateVATInvoice);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.InsertStoreChange(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, dtbStoreChangeDetail, objStoreChange, ref strStoreChangeID, ref strOutPutVoucherID, intStoreChangeOrderTypeID, intCreateVATInvoice);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSStoreChange.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strStoreChangeID;
        }


        public bool UpdateStoreChange(StoreChange.WSStoreChange.StoreChange objStoreChange, bool bolIsUpdateAll)
        {
            try
            {
                objResultMessage = WSStoreChangeProxy.UpdateStoreChange(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChange, bolIsUpdateAll);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.UpdateStoreChange(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChange, bolIsUpdateAll);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSStoreChange.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cap nhat chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        /// <summary>
        /// cập nhật nhiều phiếu chuyển kho
        /// </summary>
        /// <param name="objStoreChangeList"></param>
        /// <returns></returns>
        public bool UpdateStoreChangeList(List<StoreChange.WSStoreChange.StoreChange> objStoreChangeList)
        {
            try
            {
                objResultMessage = WSStoreChangeProxy.UpdateStoreChangeList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeList.ToArray());
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.UpdateStoreChangeList(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChangeList.ToArray());
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSStoreChange.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật nhiều phiếu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        /// <summary>
        /// Lấy thông tin tồn kho chính và kho trưng bày
        /// PM_STORECHANGE_GETINSTOCK
        /// </summary>
        /// <param name="objKeywords"></param>
        /// <returns></returns>
        public DataSet GetStoreChange_GetInstock(object[] objKeywords)
        {
            DataSet dsDataResult = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.GetStoreChange_GetInstock(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dsDataResult, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.GetStoreChange_GetInstock(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dsDataResult, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = StoreChange.WSStoreChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Lấy thông tin tồn kho chính và kho trưng bày";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dsDataResult;
        }


        /// <summary>
        /// Lấy thông tin tồn kho chính mà không có ở kho trưng bày
        /// PM_STORECHANGE_GETINSTOCK_1
        /// </summary>
        /// <param name="objKeywords"></param>
        /// <returns></returns>
        public DataTable GetStoreChange_GetInstock_1(object[] objKeywords)
        {
            DataTable dtDataResult = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.GetStoreChange_GetInstock_1(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtDataResult, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.GetStoreChange_GetInstock_1(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtDataResult, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = StoreChange.WSStoreChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Lấy thông tin tồn kho chính mà không có ở kho trưng bày";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtDataResult;
        }

        public DataTable GetStoreChange_BySCOID(string strStoreChangeOrderID)
        {
            DataTable dtDataResult = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.GetStoreChange_BySCOID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtDataResult, strStoreChangeOrderID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.GetStoreChange_BySCOID(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtDataResult, strStoreChangeOrderID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = StoreChange.WSStoreChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Lấy thông tin tồn của phiếu xuất chuyển";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtDataResult;
        }

        public string GetStoreChangeOrder_GetCode(string strStoreChangeOrderID)
        {
            DataTable dtDataResult = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.GetStoreChangeOrder_GetCode(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtDataResult, strStoreChangeOrderID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.GetStoreChangeOrder_GetCode(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtDataResult, strStoreChangeOrderID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = StoreChange.WSStoreChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Lấy mã phiếu của in biên bản hàng hóa";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            if (dtDataResult != null && dtDataResult.Rows.Count > 0)
                return dtDataResult.Rows[0]["CODE"].ToString();
            else
                return string.Empty;
        }

        public DataTable GetStoreChangeRPT_GetInfo(string strStoreChangeID)
        {
            DataTable dtDataResult = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.GetStoreChangeRPT_GetInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtDataResult, strStoreChangeID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.GetStoreChangeRPT_GetInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtDataResult, strStoreChangeID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = StoreChange.WSStoreChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Lấy thông tin in phiếu xuất kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtDataResult;
        }

        public DataTable GetStoreChangeRPT_GetData(string strStoreChangeID)
        {
            DataTable dtDataResult = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.GetStoreChangeRPT_GetData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtDataResult, strStoreChangeID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.GetStoreChangeRPT_GetData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtDataResult, strStoreChangeID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = StoreChange.WSStoreChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi Lấy thông tin in phiếu xuất kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtDataResult;
        }

        /// <summary>
        /// 
        /// Lấy dữ liệu xuất kho kiêm vận chuyển nội bộ
        /// </summary>
        /// <param name="strStoreChangeOrderID"></param>
        /// <returns></returns>
        public DataTable Report_GetOutputVoucherTransferData(string strStoreChangeID)
        {
            DataTable dtbResult = null;
            try
            {
                //objResultMessage = WSInputVoucherProxy.Report_GetInputVoucherData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                //    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strInputVoucherID);
                objResultMessage = WSStoreChangeProxy.Report_GetOutputVoucherTranferData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strStoreChangeID);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.Report_GetOutputVoucherTranferData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strStoreChangeID);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSStoreChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy thông tin phiếu xuất";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }
        //03/10/2017: Đăng bổ sung
        public DataTable GetFormTypebyStoreChangeOrderType(params object[] objKeywords)
        {
            DataTable dtbFormType = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.GetFormTypebyStoreChangeOrderType(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbFormType, objKeywords.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.GetFormTypebyStoreChangeOrderType(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbFormType, objKeywords.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSStoreChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi lấy dữ liệu thiết lập";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbFormType;
        }
        //Nam bổ sung
        public WSStoreChange.ResultMessage InsertConfigPrint(WSStoreChange.StoredChange_CurrentVoucher objStoredChange_CurrentVoucher)
        {
            WSStoreChange.ResultMessage objResultMessage = new WSStoreChange.ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeProxy.InsertConfigPrint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoredChange_CurrentVoucher);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.InsertConfigPrint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoredChange_CurrentVoucher);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSStoreChange.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm thông tin cấu hình in kim";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }
        public WSStoreChange.ResultMessage UpdateConfigPrint(WSStoreChange.StoredChange_CurrentVoucher objStoredChange_CurrentVoucher)
        {
            WSStoreChange.ResultMessage objResultMessage = new WSStoreChange.ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeProxy.UpdateConfigPrint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoredChange_CurrentVoucher);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.UpdateConfigPrint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoredChange_CurrentVoucher);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSStoreChange.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật thông tin cấu hình in kim";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }

        public WSStoreChange.StoredChange_CurrentVoucher LoadInfoConfigPrint(int intStoreID)
        {
            WSStoreChange.StoredChange_CurrentVoucher objStoredChange_CurrentVoucher = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.LoadConfigPrint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, intStoreID, ref objStoredChange_CurrentVoucher);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.LoadConfigPrint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, intStoreID, ref objStoredChange_CurrentVoucher);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSStoreChange.ErrorTypes.LoadInfo;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin yêu cầu chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objStoredChange_CurrentVoucher;
        }

        public WSStoreChange.ResultMessage UpdateStoreChange_Voucher(WSStoreChange.StoreChange_CurrentVoucherUpdate objStoreChange_CurrentVoucherUpdate, ref bool bolIsDuplicate)
        {
            WSStoreChange.ResultMessage objResultMessage = new WSStoreChange.ResultMessage();
            try
            {
                objResultMessage = WSStoreChangeProxy.UpdateStoreChange_Voucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChange_CurrentVoucherUpdate, ref bolIsDuplicate);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.UpdateStoreChange_Voucher(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, objStoreChange_CurrentVoucherUpdate, ref bolIsDuplicate);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSStoreChange.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi cập nhật thông tin cấu phiếu xuất từ chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return objResultMessage;
        }

        public DataTable GetHistoryPrint(string strStoreChangeId)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.GetHistoryPrint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strStoreChangeId);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.GetHistoryPrint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, strStoreChangeId);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSStoreChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp thông tin lịch sử in kim";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }

        public DataTable SearchDataConfigPrint(object[] objKeywords)
        {
            DataTable dtbDataResult = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.SearchDataConfigPrint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, objKeywords);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.SearchDataConfigPrint(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbDataResult, objKeywords);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = StoreChange.WSStoreChange.ErrorTypes.GetData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tìm Thông tin cấu hình In kim";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbDataResult;
        }

        //06/01/2018: Đăng bổ sung
        public string InsertStoreChangeNew(DataTable dtbStoreChangeDetail, StoreChange.WSStoreChange.StoreChange objStoreChange, ref string strOutPutVoucherID, ref List<string> lstStoreChangeID, ref List<string> lstVATInvoiceID, int intStoreChangeOrderTypeID = 0, int intCreateVATInvoice = 0, int intPrepareVAT = 15)
        {
            string strStoreChangeID = string.Empty;
            string[] arrStoreChangeID = null;
            string[] arrVATInvoiceID = null;
            try
            {
                objResultMessage = WSStoreChangeProxy.InsertStoreChangeNew(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, dtbStoreChangeDetail, objStoreChange, ref strStoreChangeID, ref strOutPutVoucherID, ref arrStoreChangeID, ref  arrVATInvoiceID, intStoreChangeOrderTypeID, intCreateVATInvoice, intPrepareVAT);
                if (CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSStoreChangeProxy.InsertStoreChangeNew(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(), PLCInventory_Globals.WebServiceHostTransactionServices.GUID, dtbStoreChangeDetail, objStoreChange, ref strStoreChangeID, ref strOutPutVoucherID, ref arrStoreChangeID, ref  arrVATInvoiceID, intStoreChangeOrderTypeID, intCreateVATInvoice, intPrepareVAT);
                    CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
                lstStoreChangeID = arrStoreChangeID.ToList();
                lstVATInvoiceID = arrVATInvoiceID.ToList();
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSStoreChange.ErrorTypes.Insert;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi thêm chuyển kho";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return strStoreChangeID;
        }


        #endregion

        public enum ReportActionType
        {
            VIEW = 1,
            PRINT = 2,
        }
    }
}
