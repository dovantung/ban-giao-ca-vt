﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.AppCore;
using System.Data;

namespace ERP.Inventory.PLC.BGT
{
    public class PLCBeginTermMoney
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/BGT/WSBeginTermMoney.asmx";
        private WSBeginTermMoney.WSBeginTermMoney objWSBeginTermMoneyProxy = null;
        private WSBeginTermMoney.ResultMessage objResultMessage=new WSBeginTermMoney.ResultMessage();
        #endregion
        #region Property
        private WSBeginTermMoney.WSBeginTermMoney WSBeginTermMoneyProxy
        {
            get
            {
                if (objWSBeginTermMoneyProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSBeginTermMoneyProxy=new WSBeginTermMoney.WSBeginTermMoney();
                    objWSBeginTermMoneyProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSBeginTermMoneyProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSBeginTermMoneyProxy.EnableDecompression = true;
                    objWSBeginTermMoneyProxy.Timeout = 180000;
                }
                return objWSBeginTermMoneyProxy;
            }
        }
        #endregion
        #region Constructor
        public PLCBeginTermMoney()
        {

        }
        #endregion
        #region Public Methods

        public bool CalcAll(bool bolIsCalAll, DateTime dtBeginTermMoneyDate)
        {
            try
            {
                objResultMessage = WSBeginTermMoneyProxy.CalcAll(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, bolIsCalAll, dtBeginTermMoneyDate);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSBeginTermMoneyProxy.CalcAll(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, bolIsCalAll, dtBeginTermMoneyDate);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSBeginTermMoney.ErrorTypes.Others;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tính số dư tiền đầu ngày";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }

        public DataTable SearchDataMoneyInfo(params object[] objKeywords)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSBeginTermMoneyProxy.SearchDataMoneyInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSBeginTermMoneyProxy.SearchDataMoneyInfo(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSBeginTermMoney.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách tồn kho đẩu kỳ";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }

        public DataTable SearchDataMoneyInfoLog(params object[] objKeywords)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSBeginTermMoneyProxy.SearchDataMoneyInfoLog(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSBeginTermMoneyProxy.SearchDataMoneyInfoLog(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSBeginTermMoney.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách nhật ký tồn kho đẩu kỳ";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }
        #endregion
    }
}
