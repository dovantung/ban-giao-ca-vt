﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.AppCore;
using System.Data;

namespace ERP.Inventory.PLC.BGT
{
    public class PLCCumulationTimeOff
    {
        #region Variable
        private const String WS_CLASS_PATH = "Inventory/BGT/WSCumulationTimeOff.asmx";
        private WSCumulationTimeOff.WSCumulationTimeOff objWSCumulationTimeOffProxy = null;
        private WSCumulationTimeOff.ResultMessage objResultMessage=new WSCumulationTimeOff.ResultMessage();
        #endregion
        #region Property
        private WSCumulationTimeOff.WSCumulationTimeOff WSCumulationTimeOffProxy
        {
            get
            {
                if (objWSCumulationTimeOffProxy == null || !PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake)
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.HandShake();
                    objWSCumulationTimeOffProxy=new WSCumulationTimeOff.WSCumulationTimeOff();
                    objWSCumulationTimeOffProxy.Url = PLCInventory_Globals.WebServiceHostTransactionServices.HostURL + WS_CLASS_PATH;
                    objWSCumulationTimeOffProxy.CookieContainer = PLCInventory_Globals.WebServiceHostTransactionServices.CookieContainer;
                    objWSCumulationTimeOffProxy.EnableDecompression = true;
                    objWSCumulationTimeOffProxy.Timeout = 180000;
                }
                return objWSCumulationTimeOffProxy;
            }
        }
        #endregion
        #region Constructor
        public PLCCumulationTimeOff()
        {

        }
        #endregion
        #region Public Methods

        public bool Calculate(int intNewYear)
        {
            try
            {
                objResultMessage = WSCumulationTimeOffProxy.Calculate(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, intNewYear);
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSCumulationTimeOffProxy.Calculate(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, intNewYear);
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSCumulationTimeOff.ErrorTypes.Update;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi tính số ngày nghỉ tích lũy theo từng nhân viên";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return !objResultMessage.IsError;
        }
        public DataTable SearchData(params object[] objKeywords)
        {
            DataTable dtbResult = null;
            try
            {
                objResultMessage = WSCumulationTimeOffProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                    PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                if (Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, true))
                {
                    PLCInventory_Globals.WebServiceHostTransactionServices.IsHandShake = false;
                    objResultMessage = WSCumulationTimeOffProxy.SearchData(PLCInventory_Globals.WebServiceHostTransactionServices.CreateAuthenticateData(),
                        PLCInventory_Globals.WebServiceHostTransactionServices.GUID, ref dtbResult, objKeywords.ToArray());
                    Library.AppCore.CheckError.CheckMissSession((int)objResultMessage.ErrorType, false);
                }
            }
            catch (System.Exception objExc)
            {
                objResultMessage.ErrorType = WSCumulationTimeOff.ErrorTypes.SearchData;
                objResultMessage.IsError = true;
                objResultMessage.Message = "Lỗi nạp danh sách số ngày nghỉ tích lũy theo từng nhân viên";
                objResultMessage.MessageDetail = objExc.ToString();
                SystemErrorWS.Insert(objResultMessage.Message, objResultMessage.MessageDetail, objResultMessage.ErrorType.ToString(),
                    PLCInventory_Globals.ModuleName);
            }
            new ResultMessageApp(objResultMessage.IsError, (int)objResultMessage.ErrorType, objResultMessage.Message, objResultMessage.MessageDetail);
            return dtbResult;
        }
        #endregion
    }
}
